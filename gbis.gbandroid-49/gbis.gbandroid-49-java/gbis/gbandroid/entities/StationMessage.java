package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;
import gbis.gbandroid.utils.Base64;
import gbis.gbandroid.utils.FieldEncryption;
import java.util.ArrayList;
import java.util.List;

public class StationMessage extends StationDetailed
{
  public static final Parcelable.Creator<StationMessage> CREATOR = new n();

  @SerializedName("InlineAds")
  private List<AdInLine> adsInLine;
  private boolean air;

  @SerializedName("ATM")
  private boolean atm;
  private boolean cStore;
  private boolean carwash;
  private boolean diesel;
  private String dieselComments;
  private boolean e85;
  private int isFav;
  private String midComments;
  private boolean midgradeGas;
  private int numPumps;
  private int obsoletePriceTime;
  private boolean open247;
  private boolean payAtPump;
  private boolean payphone;
  private String phone;
  private String photoMedium;
  private String premComments;
  private boolean premiumGas;
  private PriceShareMessages priceShareMessages;
  private PriceValidation priceValidation;

  @SerializedName("Validation")
  private String priceValidationEncrypted;
  private boolean propane;
  private String regComments;
  private boolean regularGas;
  private boolean restaurant;
  private boolean restrooms;
  private boolean serviceStation;
  private String stationAlias;
  private List<StationPhotos> stationPhotos;
  private boolean truckStop;

  public StationMessage()
  {
  }

  private StationMessage(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private PriceValidation decodePriceValidation(String paramString)
  {
    PriceValidation localPriceValidation = new PriceValidation();
    try
    {
      String[] arrayOfString = FieldEncryption.decryptTripleDES(Base64.decode(paramString)).split("\\|");
      localPriceValidation.setExpectedRegularPrice(Double.parseDouble(arrayOfString[0]));
      localPriceValidation.setCommonIncreaseRegular(Double.parseDouble(arrayOfString[1]));
      localPriceValidation.setCommonDecreaseRegular(Double.parseDouble(arrayOfString[2]));
      localPriceValidation.setOffsetMidgrade(Double.parseDouble(arrayOfString[3]));
      localPriceValidation.setOffsetPremium(Double.parseDouble(arrayOfString[4]));
      localPriceValidation.setExpectedDieselPrice(Double.parseDouble(arrayOfString[5]));
      localPriceValidation.setCommonIncreaseDiesel(Double.parseDouble(arrayOfString[6]));
      localPriceValidation.setCommonDecreaseDiesel(Double.parseDouble(arrayOfString[7]));
      localPriceValidation.setRadius(Double.parseDouble(arrayOfString[8]));
      return localPriceValidation;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return localPriceValidation;
  }

  public StationMessage copy()
  {
    StationMessage localStationMessage = new StationMessage();
    localStationMessage.address = this.address;
    localStationMessage.air = this.air;
    localStationMessage.atm = this.atm;
    localStationMessage.carwash = this.carwash;
    localStationMessage.city = this.city;
    localStationMessage.country = this.country;
    localStationMessage.cStore = this.cStore;
    localStationMessage.crossStreet = this.crossStreet;
    localStationMessage.dieselPrice = this.dieselPrice;
    localStationMessage.dieselTimeSpotted = this.dieselTimeSpotted;
    localStationMessage.dieselMemberId = this.dieselMemberId;
    localStationMessage.dieselCar = this.dieselCar;
    localStationMessage.dieselComments = this.dieselComments;
    localStationMessage.diesel = this.diesel;
    localStationMessage.e85 = this.e85;
    localStationMessage.gasBrandId = this.gasBrandId;
    localStationMessage.gasBrandVersion = this.gasBrandVersion;
    localStationMessage.stationId = this.stationId;
    localStationMessage.isFav = this.isFav;
    localStationMessage.latitude = this.latitude;
    localStationMessage.longitude = this.longitude;
    localStationMessage.photoMedium = this.photoMedium;
    localStationMessage.midPrice = this.midPrice;
    localStationMessage.midTimeSpotted = this.midTimeSpotted;
    localStationMessage.midMemberId = this.midMemberId;
    localStationMessage.midCar = this.midCar;
    localStationMessage.midComments = this.midComments;
    localStationMessage.midgradeGas = this.midgradeGas;
    localStationMessage.notIntersection = this.notIntersection;
    localStationMessage.numPumps = this.numPumps;
    localStationMessage.obsoletePriceTime = this.obsoletePriceTime;
    localStationMessage.open247 = this.open247;
    localStationMessage.payAtPump = this.payAtPump;
    localStationMessage.payphone = this.payphone;
    localStationMessage.phone = this.phone;
    localStationMessage.postalCode = this.postalCode;
    localStationMessage.premPrice = this.premPrice;
    localStationMessage.premTimeSpotted = this.premTimeSpotted;
    localStationMessage.premMemberId = this.premMemberId;
    localStationMessage.premCar = this.premCar;
    localStationMessage.premComments = this.premComments;
    localStationMessage.premiumGas = this.premiumGas;
    localStationMessage.propane = this.propane;
    localStationMessage.regPrice = this.regPrice;
    localStationMessage.regTimeSpotted = this.regTimeSpotted;
    localStationMessage.regMemberId = this.regMemberId;
    localStationMessage.regCar = this.regCar;
    localStationMessage.regComments = this.regComments;
    localStationMessage.regularGas = this.regularGas;
    localStationMessage.restaurant = this.restaurant;
    localStationMessage.restrooms = this.restrooms;
    localStationMessage.serviceStation = this.serviceStation;
    localStationMessage.state = this.state;
    localStationMessage.stationName = this.stationName;
    localStationMessage.stationAlias = this.stationAlias;
    localStationMessage.timeOffset = this.timeOffset;
    localStationMessage.truckStop = this.truckStop;
    localStationMessage.stationPhotos = this.stationPhotos;
    localStationMessage.adsInLine = this.adsInLine;
    localStationMessage.priceValidationEncrypted = this.priceValidationEncrypted;
    localStationMessage.priceShareMessages = this.priceShareMessages;
    return localStationMessage;
  }

  public int describeContents()
  {
    return 0;
  }

  public List<AdInLine> getAdsInLine()
  {
    return this.adsInLine;
  }

  public String getDieselComments()
  {
    return this.dieselComments;
  }

  public String getMidComments()
  {
    return this.midComments;
  }

  public int getNumPumps()
  {
    return this.numPumps;
  }

  public int getObsoletePriceTime()
  {
    return this.obsoletePriceTime;
  }

  public String getPhone()
  {
    return this.phone;
  }

  public String getPhotoMedium()
  {
    return this.photoMedium;
  }

  public String getPremComments()
  {
    return this.premComments;
  }

  public PriceShareMessages getPriceShareMessages()
  {
    return this.priceShareMessages;
  }

  public PriceValidation getPriceValidation()
  {
    if (this.priceValidation == null)
      this.priceValidation = decodePriceValidation(this.priceValidationEncrypted);
    return this.priceValidation;
  }

  public String getRegComments()
  {
    return this.regComments;
  }

  public String getStationAlias()
  {
    return this.stationAlias;
  }

  public List<StationPhotos> getStationPhotos()
  {
    return this.stationPhotos;
  }

  public boolean hasAir()
  {
    return this.air;
  }

  public boolean hasAtm()
  {
    return this.atm;
  }

  public boolean hasCStore()
  {
    return this.cStore;
  }

  public boolean hasCarWash()
  {
    return this.carwash;
  }

  public boolean hasDiesel()
  {
    return this.diesel;
  }

  public boolean hasE85()
  {
    return this.e85;
  }

  public boolean hasMidgradeGas()
  {
    return this.midgradeGas;
  }

  public boolean hasOpen247()
  {
    return this.open247;
  }

  public boolean hasPayAtPump()
  {
    return this.payAtPump;
  }

  public boolean hasPayphone()
  {
    return this.payphone;
  }

  public boolean hasPremiumGas()
  {
    return this.premiumGas;
  }

  public boolean hasPropane()
  {
    return this.propane;
  }

  public boolean hasRegularGas()
  {
    return this.regularGas;
  }

  public boolean hasRestaurant()
  {
    return this.restaurant;
  }

  public boolean hasRestrooms()
  {
    return this.restrooms;
  }

  public boolean hasServiceStation()
  {
    return this.serviceStation;
  }

  public boolean hasTruckStop()
  {
    return this.truckStop;
  }

  public int isIsFav()
  {
    return this.isFav;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    super.readFromParcel(paramParcel);
    this.photoMedium = paramParcel.readString();
    this.phone = paramParcel.readString();
    this.stationAlias = paramParcel.readString();
    this.regComments = paramParcel.readString();
    this.midComments = paramParcel.readString();
    this.premComments = paramParcel.readString();
    this.dieselComments = paramParcel.readString();
    this.obsoletePriceTime = paramParcel.readInt();
    boolean[] arrayOfBoolean = new boolean[17];
    paramParcel.readBooleanArray(arrayOfBoolean);
    this.cStore = arrayOfBoolean[0];
    this.serviceStation = arrayOfBoolean[1];
    this.payAtPump = arrayOfBoolean[2];
    this.restaurant = arrayOfBoolean[3];
    this.restrooms = arrayOfBoolean[4];
    this.air = arrayOfBoolean[5];
    this.payphone = arrayOfBoolean[6];
    this.atm = arrayOfBoolean[7];
    this.open247 = arrayOfBoolean[8];
    this.truckStop = arrayOfBoolean[9];
    this.carwash = arrayOfBoolean[10];
    this.regularGas = arrayOfBoolean[11];
    this.midgradeGas = arrayOfBoolean[12];
    this.premiumGas = arrayOfBoolean[13];
    this.diesel = arrayOfBoolean[14];
    this.e85 = arrayOfBoolean[15];
    this.propane = arrayOfBoolean[16];
    this.numPumps = paramParcel.readInt();
    this.isFav = paramParcel.readInt();
    this.stationPhotos = new ArrayList();
    paramParcel.readList(this.stationPhotos, StationPhotos.class.getClassLoader());
    this.adsInLine = new ArrayList();
    paramParcel.readList(this.adsInLine, AdInLine.class.getClassLoader());
    this.priceValidationEncrypted = paramParcel.readString();
    this.priceValidation = ((PriceValidation)paramParcel.readParcelable(PriceValidation.class.getClassLoader()));
    this.priceShareMessages = ((PriceShareMessages)paramParcel.readParcelable(PriceShareMessages.class.getClassLoader()));
  }

  public void setAdsInLine(List<AdInLine> paramList)
  {
    this.adsInLine = paramList;
  }

  public void setAir(boolean paramBoolean)
  {
    this.air = paramBoolean;
  }

  public void setAtm(boolean paramBoolean)
  {
    this.atm = paramBoolean;
  }

  public void setCStore(boolean paramBoolean)
  {
    this.cStore = paramBoolean;
  }

  public void setCarWash(boolean paramBoolean)
  {
    this.carwash = paramBoolean;
  }

  public void setDiesel(boolean paramBoolean)
  {
    this.diesel = paramBoolean;
  }

  public void setDieselComments(String paramString)
  {
    this.dieselComments = paramString;
  }

  public void setE85(boolean paramBoolean)
  {
    this.e85 = paramBoolean;
  }

  public void setIsFav(int paramInt)
  {
    this.isFav = paramInt;
  }

  public void setMidComments(String paramString)
  {
    this.midComments = paramString;
  }

  public void setMidgradeGas(boolean paramBoolean)
  {
    this.midgradeGas = paramBoolean;
  }

  public void setNumPumps(int paramInt)
  {
    this.numPumps = paramInt;
  }

  public void setObsoletePriceTime(int paramInt)
  {
    this.obsoletePriceTime = paramInt;
  }

  public void setOpen247(boolean paramBoolean)
  {
    this.open247 = paramBoolean;
  }

  public void setPayAtPump(boolean paramBoolean)
  {
    this.payAtPump = paramBoolean;
  }

  public void setPayphone(boolean paramBoolean)
  {
    this.payphone = paramBoolean;
  }

  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }

  public void setPhotoMedium(String paramString)
  {
    this.photoMedium = paramString;
  }

  public void setPremComments(String paramString)
  {
    this.premComments = paramString;
  }

  public void setPremiumGas(boolean paramBoolean)
  {
    this.premiumGas = paramBoolean;
  }

  public void setPriceShareMessages(PriceShareMessages paramPriceShareMessages)
  {
    this.priceShareMessages = paramPriceShareMessages;
  }

  public void setPriceValidation(PriceValidation paramPriceValidation)
  {
    this.priceValidation = paramPriceValidation;
  }

  public void setPriceValidationEncrypted(String paramString)
  {
    this.priceValidationEncrypted = paramString;
  }

  public void setPropane(boolean paramBoolean)
  {
    this.propane = paramBoolean;
  }

  public void setRegComments(String paramString)
  {
    this.regComments = paramString;
  }

  public void setRegularGas(boolean paramBoolean)
  {
    this.regularGas = paramBoolean;
  }

  public void setRestaurant(boolean paramBoolean)
  {
    this.restaurant = paramBoolean;
  }

  public void setRestrooms(boolean paramBoolean)
  {
    this.restrooms = paramBoolean;
  }

  public void setServiceStation(boolean paramBoolean)
  {
    this.serviceStation = paramBoolean;
  }

  public void setStationAlias(String paramString)
  {
    this.stationAlias = paramString;
  }

  public void setStationPhotos(List<StationPhotos> paramList)
  {
    this.stationPhotos = paramList;
  }

  public void setTruckStop(boolean paramBoolean)
  {
    this.truckStop = paramBoolean;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.toString());
    localStringBuilder.append("Regular Comments: ");
    localStringBuilder.append(this.regComments);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Comments: ");
    localStringBuilder.append(this.midComments);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium Comments: ");
    localStringBuilder.append(this.premComments);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel Comments: ");
    localStringBuilder.append(this.dieselComments);
    localStringBuilder.append('\n');
    localStringBuilder.append("Photo Medium: ");
    localStringBuilder.append(this.photoMedium);
    localStringBuilder.append('\n');
    localStringBuilder.append("C Store: ");
    localStringBuilder.append(this.cStore);
    localStringBuilder.append('\n');
    localStringBuilder.append("Service Station: ");
    localStringBuilder.append(this.serviceStation);
    localStringBuilder.append('\n');
    localStringBuilder.append("Pay at Pump: ");
    localStringBuilder.append(this.payAtPump);
    localStringBuilder.append('\n');
    localStringBuilder.append("Restaurant: ");
    localStringBuilder.append(this.restaurant);
    localStringBuilder.append('\n');
    localStringBuilder.append("Restrooms: ");
    localStringBuilder.append(this.restrooms);
    localStringBuilder.append('\n');
    localStringBuilder.append("Air: ");
    localStringBuilder.append(this.air);
    localStringBuilder.append('\n');
    localStringBuilder.append("ATM: ");
    localStringBuilder.append(this.atm);
    localStringBuilder.append('\n');
    localStringBuilder.append("Open_247: ");
    localStringBuilder.append(this.open247);
    localStringBuilder.append('\n');
    localStringBuilder.append("Truck Stop: ");
    localStringBuilder.append(this.truckStop);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Wash: ");
    localStringBuilder.append(this.carwash);
    localStringBuilder.append('\n');
    localStringBuilder.append("Regular Gas: ");
    localStringBuilder.append(this.regularGas);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Gas: ");
    localStringBuilder.append(this.midgradeGas);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium Gas: ");
    localStringBuilder.append(this.premiumGas);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel Gas: ");
    localStringBuilder.append(this.diesel);
    localStringBuilder.append('\n');
    localStringBuilder.append("Number of Pumps: ");
    localStringBuilder.append(this.numPumps);
    localStringBuilder.append('\n');
    localStringBuilder.append("Is Favourite: ");
    localStringBuilder.append(this.isFav);
    localStringBuilder.append('\n');
    localStringBuilder.append("Time Offset: ");
    localStringBuilder.append(this.timeOffset);
    localStringBuilder.append('\n');
    localStringBuilder.append("Obsolete Price Time: ");
    localStringBuilder.append(this.obsoletePriceTime);
    localStringBuilder.append('\n');
    localStringBuilder.append("Ads In Line: ");
    localStringBuilder.append(this.adsInLine);
    localStringBuilder.append('\n');
    localStringBuilder.append("Price Validation: ");
    localStringBuilder.append(this.priceValidationEncrypted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Price Share Messages: ");
    localStringBuilder.append(this.priceShareMessages);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.photoMedium);
    paramParcel.writeString(this.phone);
    paramParcel.writeString(this.stationAlias);
    paramParcel.writeString(this.regComments);
    paramParcel.writeString(this.midComments);
    paramParcel.writeString(this.premComments);
    paramParcel.writeString(this.dieselComments);
    paramParcel.writeInt(this.obsoletePriceTime);
    boolean[] arrayOfBoolean = new boolean[17];
    arrayOfBoolean[0] = this.cStore;
    arrayOfBoolean[1] = this.serviceStation;
    arrayOfBoolean[2] = this.payAtPump;
    arrayOfBoolean[3] = this.restaurant;
    arrayOfBoolean[4] = this.restrooms;
    arrayOfBoolean[5] = this.air;
    arrayOfBoolean[6] = this.payphone;
    arrayOfBoolean[7] = this.atm;
    arrayOfBoolean[8] = this.open247;
    arrayOfBoolean[9] = this.truckStop;
    arrayOfBoolean[10] = this.carwash;
    arrayOfBoolean[11] = this.regularGas;
    arrayOfBoolean[12] = this.midgradeGas;
    arrayOfBoolean[13] = this.premiumGas;
    arrayOfBoolean[14] = this.diesel;
    arrayOfBoolean[15] = this.e85;
    arrayOfBoolean[16] = this.propane;
    paramParcel.writeBooleanArray(arrayOfBoolean);
    paramParcel.writeInt(this.numPumps);
    paramParcel.writeInt(this.isFav);
    paramParcel.writeList(this.stationPhotos);
    paramParcel.writeList(this.adsInLine);
    paramParcel.writeString(this.priceValidationEncrypted);
    paramParcel.writeParcelable(this.priceValidation, paramInt);
    paramParcel.writeParcelable(this.priceShareMessages, paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.StationMessage
 * JD-Core Version:    0.6.2
 */