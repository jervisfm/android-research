package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StationNameFilter
  implements Parcelable
{
  public static final Parcelable.Creator<StationNameFilter> CREATOR = new o();
  private boolean included;
  private String stationName;

  public StationNameFilter()
  {
  }

  private StationNameFilter(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public StationNameFilter(String paramString, boolean paramBoolean)
  {
    this.stationName = paramString;
    this.included = paramBoolean;
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.stationName = paramParcel.readString();
    boolean[] arrayOfBoolean = new boolean[1];
    paramParcel.readBooleanArray(arrayOfBoolean);
    this.included = arrayOfBoolean[0];
  }

  public int describeContents()
  {
    return 0;
  }

  public String getStationName()
  {
    return this.stationName;
  }

  public boolean isIncluded()
  {
    return this.included;
  }

  public void setIncluded(boolean paramBoolean)
  {
    this.included = paramBoolean;
  }

  public void setStationName(String paramString)
  {
    this.stationName = paramString;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.stationName);
    boolean[] arrayOfBoolean = new boolean[1];
    arrayOfBoolean[0] = this.included;
    paramParcel.writeBooleanArray(arrayOfBoolean);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.StationNameFilter
 * JD-Core Version:    0.6.2
 */