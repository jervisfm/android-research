package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class StationPhotos
  implements Parcelable
{
  public static final Parcelable.Creator<StationPhotos> CREATOR = new p();
  private String caption;
  private String carIcon;
  private String mediumPath;
  private String memberId;
  private int photoId;
  private String smallPath;
  private String thumbnailPath;
  private String title;

  public StationPhotos()
  {
  }

  private StationPhotos(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.photoId = paramParcel.readInt();
    this.thumbnailPath = paramParcel.readString();
    this.smallPath = paramParcel.readString();
    this.mediumPath = paramParcel.readString();
    this.title = paramParcel.readString();
    this.caption = paramParcel.readString();
    this.memberId = paramParcel.readString();
    this.carIcon = paramParcel.readString();
  }

  public StationPhotos copy()
  {
    StationPhotos localStationPhotos = new StationPhotos();
    localStationPhotos.caption = this.caption;
    localStationPhotos.carIcon = this.carIcon;
    localStationPhotos.mediumPath = this.mediumPath;
    localStationPhotos.memberId = this.memberId;
    localStationPhotos.photoId = this.photoId;
    localStationPhotos.smallPath = this.smallPath;
    localStationPhotos.thumbnailPath = this.thumbnailPath;
    localStationPhotos.title = this.title;
    return localStationPhotos;
  }

  public int describeContents()
  {
    return 0;
  }

  public String getCaption()
  {
    return this.caption;
  }

  public String getCarIcon()
  {
    return this.carIcon;
  }

  public String getMediumPath()
  {
    return this.mediumPath;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public int getPhotoId()
  {
    return this.photoId;
  }

  public String getSmallPath()
  {
    return this.smallPath;
  }

  public String getThumbnailPath()
  {
    return this.thumbnailPath;
  }

  public String getTitle()
  {
    return this.title;
  }

  public void setCaption(String paramString)
  {
    this.caption = paramString;
  }

  public void setCarIcon(String paramString)
  {
    this.carIcon = paramString;
  }

  public void setMediumPath(String paramString)
  {
    this.mediumPath = paramString;
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setPhotoId(String paramString)
  {
    this.photoId = new Integer(paramString).intValue();
  }

  public void setSmallPath(String paramString)
  {
    this.smallPath = paramString;
  }

  public void setThumbnailPath(String paramString)
  {
    this.thumbnailPath = paramString;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.photoId);
    paramParcel.writeString(this.thumbnailPath);
    paramParcel.writeString(this.smallPath);
    paramParcel.writeString(this.mediumPath);
    paramParcel.writeString(this.title);
    paramParcel.writeString(this.caption);
    paramParcel.writeString(this.memberId);
    paramParcel.writeString(this.carIcon);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.StationPhotos
 * JD-Core Version:    0.6.2
 */