package gbis.gbandroid.entities;

public class TicketMessage
{
  private int pointBalance;
  private int tickets;
  private int totalTickets;

  public TicketMessage copy()
  {
    TicketMessage localTicketMessage = new TicketMessage();
    localTicketMessage.tickets = this.tickets;
    localTicketMessage.totalTickets = this.totalTickets;
    localTicketMessage.pointBalance = this.pointBalance;
    return localTicketMessage;
  }

  public int getPointBalance()
  {
    return this.pointBalance;
  }

  public int getTickets()
  {
    return this.tickets;
  }

  public int getTotalTickets()
  {
    return this.totalTickets;
  }

  public void setPointBalance(String paramString)
  {
    this.pointBalance = new Integer(paramString).intValue();
  }

  public void setTickets(String paramString)
  {
    this.tickets = new Integer(paramString).intValue();
  }

  public void setTotalTickets(String paramString)
  {
    this.totalTickets = new Integer(paramString).intValue();
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Tickets: ");
    localStringBuilder.append(this.tickets);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Tickets: ");
    localStringBuilder.append(this.totalTickets);
    localStringBuilder.append('\n');
    localStringBuilder.append("Point Balance: ");
    localStringBuilder.append(this.pointBalance);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.TicketMessage
 * JD-Core Version:    0.6.2
 */