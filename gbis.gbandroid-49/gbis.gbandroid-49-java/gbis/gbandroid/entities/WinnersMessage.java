package gbis.gbandroid.entities;

public class WinnersMessage
  implements Comparable<WinnersMessage>
{
  private String car;
  private String cityTitle;
  private String endDate;
  private String imgUrl;
  private String memberId;
  private String prize;

  public int compareTo(WinnersMessage paramWinnersMessage)
  {
    return 1;
  }

  public WinnersMessage copy()
  {
    WinnersMessage localWinnersMessage = new WinnersMessage();
    localWinnersMessage.car = this.car;
    localWinnersMessage.cityTitle = this.cityTitle;
    localWinnersMessage.endDate = this.endDate;
    localWinnersMessage.imgUrl = this.imgUrl;
    localWinnersMessage.memberId = this.memberId;
    localWinnersMessage.prize = this.prize;
    return localWinnersMessage;
  }

  public String getCar()
  {
    return this.car;
  }

  public String getCityTitle()
  {
    return this.cityTitle;
  }

  public String getEndDate()
  {
    return this.endDate;
  }

  public String getImgUrl()
  {
    return this.imgUrl;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public String getPrize()
  {
    return this.prize;
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCityTitle(String paramString)
  {
    this.cityTitle = paramString;
  }

  public void setEndDate(String paramString)
  {
    this.endDate = paramString;
  }

  public void setImgUrl(String paramString)
  {
    this.imgUrl = paramString;
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setPrize(String paramString)
  {
    this.prize = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Member Id: ");
    localStringBuilder.append(this.memberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car: ");
    localStringBuilder.append(this.car);
    localStringBuilder.append('\n');
    localStringBuilder.append("City Title: ");
    localStringBuilder.append(this.cityTitle);
    localStringBuilder.append('\n');
    localStringBuilder.append("End Date: ");
    localStringBuilder.append(this.endDate);
    localStringBuilder.append('\n');
    localStringBuilder.append("Prize: ");
    localStringBuilder.append(this.prize);
    localStringBuilder.append('\n');
    localStringBuilder.append("Image URL: ");
    localStringBuilder.append(this.imgUrl);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.WinnersMessage
 * JD-Core Version:    0.6.2
 */