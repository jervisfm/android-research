package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class a
  implements Parcelable.Creator<AdInLine>
{
  private static AdInLine createFromParcel(Parcel paramParcel)
  {
    return new AdInLine(paramParcel, (byte)0);
  }

  private static AdInLine[] newArray(int paramInt)
  {
    return new AdInLine[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.a
 * JD-Core Version:    0.6.2
 */