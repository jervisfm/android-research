package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class b
  implements Parcelable.Creator<AwardsMessage>
{
  private static AwardsMessage createFromParcel(Parcel paramParcel)
  {
    return new AwardsMessage(paramParcel, (byte)0);
  }

  private static AwardsMessage[] newArray(int paramInt)
  {
    return new AwardsMessage[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.b
 * JD-Core Version:    0.6.2
 */