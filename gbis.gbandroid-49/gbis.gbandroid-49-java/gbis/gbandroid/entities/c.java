package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class c
  implements Parcelable.Creator<Center>
{
  private static Center createFromParcel(Parcel paramParcel)
  {
    return new Center(paramParcel, (byte)0);
  }

  private static Center[] newArray(int paramInt)
  {
    return new Center[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.c
 * JD-Core Version:    0.6.2
 */