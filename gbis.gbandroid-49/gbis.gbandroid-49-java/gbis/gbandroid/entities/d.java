package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class d
  implements Parcelable.Creator<ChoiceHotels>
{
  private static ChoiceHotels createFromParcel(Parcel paramParcel)
  {
    return new ChoiceHotels(paramParcel, (byte)0);
  }

  private static ChoiceHotels[] newArray(int paramInt)
  {
    return new ChoiceHotels[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.d
 * JD-Core Version:    0.6.2
 */