package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class e
  implements Parcelable.Creator<FavStationMessage>
{
  private static FavStationMessage createFromParcel(Parcel paramParcel)
  {
    return new FavStationMessage(paramParcel, (byte)0);
  }

  private static FavStationMessage[] newArray(int paramInt)
  {
    return new FavStationMessage[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.e
 * JD-Core Version:    0.6.2
 */