package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class f
  implements Parcelable.Creator<ListMessage>
{
  private static ListMessage createFromParcel(Parcel paramParcel)
  {
    return new ListMessage(paramParcel, (byte)0);
  }

  private static ListMessage[] newArray(int paramInt)
  {
    return new ListMessage[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.f
 * JD-Core Version:    0.6.2
 */