package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class g
  implements Parcelable.Creator<ListResults>
{
  private static ListResults createFromParcel(Parcel paramParcel)
  {
    return new ListResults(paramParcel, (byte)0);
  }

  private static ListResults[] newArray(int paramInt)
  {
    return new ListResults[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.g
 * JD-Core Version:    0.6.2
 */