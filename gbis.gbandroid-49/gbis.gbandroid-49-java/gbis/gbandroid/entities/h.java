package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class h
  implements Parcelable.Creator<ListStationFilter>
{
  private static ListStationFilter createFromParcel(Parcel paramParcel)
  {
    return new ListStationFilter(paramParcel, (byte)0);
  }

  private static ListStationFilter[] newArray(int paramInt)
  {
    return new ListStationFilter[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.h
 * JD-Core Version:    0.6.2
 */