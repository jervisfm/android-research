package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class i
  implements Parcelable.Creator<ListStationTab>
{
  private static ListStationTab createFromParcel(Parcel paramParcel)
  {
    return new ListStationTab(paramParcel, (byte)0);
  }

  private static ListStationTab[] newArray(int paramInt)
  {
    return new ListStationTab[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.i
 * JD-Core Version:    0.6.2
 */