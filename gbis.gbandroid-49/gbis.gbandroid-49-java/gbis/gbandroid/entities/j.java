package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class j
  implements Parcelable.Creator<MapResults>
{
  private static MapResults createFromParcel(Parcel paramParcel)
  {
    return new MapResults(paramParcel, (byte)0);
  }

  private static MapResults[] newArray(int paramInt)
  {
    return new MapResults[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.j
 * JD-Core Version:    0.6.2
 */