package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class k
  implements Parcelable.Creator<PriceShareMessages>
{
  private static PriceShareMessages createFromParcel(Parcel paramParcel)
  {
    return new PriceShareMessages(paramParcel, (byte)0);
  }

  private static PriceShareMessages[] newArray(int paramInt)
  {
    return new PriceShareMessages[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.k
 * JD-Core Version:    0.6.2
 */