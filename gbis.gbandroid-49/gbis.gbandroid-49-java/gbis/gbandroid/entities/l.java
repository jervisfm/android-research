package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class l
  implements Parcelable.Creator<PriceValidation>
{
  private static PriceValidation createFromParcel(Parcel paramParcel)
  {
    return new PriceValidation(paramParcel, (byte)0);
  }

  private static PriceValidation[] newArray(int paramInt)
  {
    return new PriceValidation[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.l
 * JD-Core Version:    0.6.2
 */