package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class m
  implements Parcelable.Creator<SmartPrompt>
{
  private static SmartPrompt createFromParcel(Parcel paramParcel)
  {
    return new SmartPrompt(paramParcel, (byte)0);
  }

  private static SmartPrompt[] newArray(int paramInt)
  {
    return new SmartPrompt[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.m
 * JD-Core Version:    0.6.2
 */