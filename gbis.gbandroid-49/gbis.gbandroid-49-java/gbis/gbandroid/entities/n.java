package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class n
  implements Parcelable.Creator<StationMessage>
{
  private static StationMessage createFromParcel(Parcel paramParcel)
  {
    return new StationMessage(paramParcel, (byte)0);
  }

  private static StationMessage[] newArray(int paramInt)
  {
    return new StationMessage[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.n
 * JD-Core Version:    0.6.2
 */