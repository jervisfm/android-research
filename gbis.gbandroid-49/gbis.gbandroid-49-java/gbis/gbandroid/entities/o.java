package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class o
  implements Parcelable.Creator<StationNameFilter>
{
  private static StationNameFilter createFromParcel(Parcel paramParcel)
  {
    return new StationNameFilter(paramParcel, (byte)0);
  }

  private static StationNameFilter[] newArray(int paramInt)
  {
    return new StationNameFilter[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.o
 * JD-Core Version:    0.6.2
 */