package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class p
  implements Parcelable.Creator<StationPhotos>
{
  private static StationPhotos createFromParcel(Parcel paramParcel)
  {
    return new StationPhotos(paramParcel, (byte)0);
  }

  private static StationPhotos[] newArray(int paramInt)
  {
    return new StationPhotos[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.p
 * JD-Core Version:    0.6.2
 */