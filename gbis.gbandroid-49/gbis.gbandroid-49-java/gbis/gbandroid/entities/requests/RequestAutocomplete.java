package gbis.gbandroid.entities.requests;

public class RequestAutocomplete
{
  private int numberToReturn;
  private String searchText;

  public RequestAutocomplete(String paramString, int paramInt)
  {
    this.searchText = paramString;
    this.numberToReturn = paramInt;
  }

  public int getNumberToReturn()
  {
    return this.numberToReturn;
  }

  public String getSearchText()
  {
    return this.searchText;
  }

  public void setNumberToReturn(int paramInt)
  {
    this.numberToReturn = paramInt;
  }

  public void setSearchText(String paramString)
  {
    this.searchText = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestAutocomplete
 * JD-Core Version:    0.6.2
 */