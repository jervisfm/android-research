package gbis.gbandroid.entities.requests;

public class RequestAwardDetails
{
  private int awardId;

  public int getAwardId()
  {
    return this.awardId;
  }

  public void setAwardId(int paramInt)
  {
    this.awardId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestAwardDetails
 * JD-Core Version:    0.6.2
 */