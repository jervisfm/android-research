package gbis.gbandroid.entities.requests;

public class RequestCrashReport extends RequestMessage
{
  private String deviceId;
  private String osName;
  private String osVersion;

  public String getDeviceId()
  {
    return this.deviceId;
  }

  public String getOsName()
  {
    return this.osName;
  }

  public String getOsVersion()
  {
    return this.osVersion;
  }

  public void setDeviceId(String paramString)
  {
    this.deviceId = paramString;
  }

  public void setOsName(String paramString)
  {
    this.osName = paramString;
  }

  public void setOsVersion(String paramString)
  {
    this.osVersion = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestCrashReport
 * JD-Core Version:    0.6.2
 */