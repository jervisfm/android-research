package gbis.gbandroid.entities.requests;

public class RequestFavAddList
{
  private boolean defaultList;
  private String listName;

  public String getListName()
  {
    return this.listName;
  }

  public boolean isDefaultList()
  {
    return this.defaultList;
  }

  public void setDefaultList(boolean paramBoolean)
  {
    this.defaultList = paramBoolean;
  }

  public void setListName(String paramString)
  {
    this.listName = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestFavAddList
 * JD-Core Version:    0.6.2
 */