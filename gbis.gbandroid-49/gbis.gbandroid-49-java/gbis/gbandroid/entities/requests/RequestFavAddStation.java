package gbis.gbandroid.entities.requests;

public class RequestFavAddStation
{
  private int listId;
  private String listName;
  private int stationId;

  public int getListId()
  {
    return this.listId;
  }

  public String getListName()
  {
    return this.listName;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public void setListId(int paramInt)
  {
    this.listId = paramInt;
  }

  public void setListName(String paramString)
  {
    this.listName = paramString;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestFavAddStation
 * JD-Core Version:    0.6.2
 */