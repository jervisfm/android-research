package gbis.gbandroid.entities.requests;

public class RequestFavRemoveList
{
  private int listId;

  public int getListId()
  {
    return this.listId;
  }

  public void setListId(int paramInt)
  {
    this.listId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestFavRemoveList
 * JD-Core Version:    0.6.2
 */