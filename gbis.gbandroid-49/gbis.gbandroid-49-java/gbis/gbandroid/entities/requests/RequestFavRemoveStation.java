package gbis.gbandroid.entities.requests;

public class RequestFavRemoveStation
{
  private int favoriteId;

  public int getFavoriteId()
  {
    return this.favoriteId;
  }

  public void setFavoriteId(int paramInt)
  {
    this.favoriteId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestFavRemoveStation
 * JD-Core Version:    0.6.2
 */