package gbis.gbandroid.entities.requests;

public class RequestFavStationList
{
  private int itemsPerPage;
  private int listId;
  private int pageNumber;

  public int getItemsPerPage()
  {
    return this.itemsPerPage;
  }

  public int getListId()
  {
    return this.listId;
  }

  public int getPageNumber()
  {
    return this.pageNumber;
  }

  public void setItemsPerPage(int paramInt)
  {
    this.itemsPerPage = paramInt;
  }

  public void setListId(int paramInt)
  {
    this.listId = paramInt;
  }

  public void setPageNumber(int paramInt)
  {
    this.pageNumber = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestFavStationList
 * JD-Core Version:    0.6.2
 */