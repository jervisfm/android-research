package gbis.gbandroid.entities.requests;

public class RequestGeocode
{
  private String address;
  private String city;
  private String postalCode;
  private String state;

  public String getAddress()
  {
    return this.address;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestGeocode
 * JD-Core Version:    0.6.2
 */