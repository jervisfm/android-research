package gbis.gbandroid.entities.requests;

public class RequestInit
{
  private int destributionMethod;
  private String deviceId;
  private String deviceModel;
  private String osName;
  private String osVersion;
  private String referrer;
  private String resolution;
  private String userToken;

  public int getDestributionMethod()
  {
    return this.destributionMethod;
  }

  public String getDeviceId()
  {
    return this.deviceId;
  }

  public String getDeviceModel()
  {
    return this.deviceModel;
  }

  public String getOsName()
  {
    return this.osName;
  }

  public String getOsVersion()
  {
    return this.osVersion;
  }

  public String getReferrer()
  {
    return this.referrer;
  }

  public String getResolution()
  {
    return this.resolution;
  }

  public String getUserToken()
  {
    return this.userToken;
  }

  public void setDestributionMethod(int paramInt)
  {
    this.destributionMethod = paramInt;
  }

  public void setDeviceId(String paramString)
  {
    this.deviceId = paramString;
  }

  public void setDeviceModel(String paramString)
  {
    this.deviceModel = paramString;
  }

  public void setOsName(String paramString)
  {
    this.osName = paramString;
  }

  public void setOsVersion(String paramString)
  {
    this.osVersion = paramString;
  }

  public void setReferrer(String paramString)
  {
    this.referrer = paramString;
  }

  public void setResolution(String paramString)
  {
    this.resolution = paramString;
  }

  public void setUserToken(String paramString)
  {
    this.userToken = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestInit
 * JD-Core Version:    0.6.2
 */