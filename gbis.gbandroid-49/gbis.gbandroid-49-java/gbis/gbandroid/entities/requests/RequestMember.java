package gbis.gbandroid.entities.requests;

public class RequestMember
{
  private String memberId;

  public String getMemberId()
  {
    return this.memberId;
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestMember
 * JD-Core Version:    0.6.2
 */