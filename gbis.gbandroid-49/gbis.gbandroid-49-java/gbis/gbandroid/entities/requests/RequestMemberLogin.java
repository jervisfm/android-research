package gbis.gbandroid.entities.requests;

public class RequestMemberLogin extends RequestMember
{
  private String password;

  public String getPassword()
  {
    return this.password;
  }

  public void setPassword(String paramString)
  {
    this.password = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestMemberLogin
 * JD-Core Version:    0.6.2
 */