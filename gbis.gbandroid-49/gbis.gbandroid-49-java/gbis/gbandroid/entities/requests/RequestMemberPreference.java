package gbis.gbandroid.entities.requests;

public class RequestMemberPreference
{
  private String name;
  private boolean value;

  public String getName()
  {
    return this.name;
  }

  public boolean isValue()
  {
    return this.value;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setValue(boolean paramBoolean)
  {
    this.value = paramBoolean;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestMemberPreference
 * JD-Core Version:    0.6.2
 */