package gbis.gbandroid.entities.requests;

public class RequestMemberRegister extends RequestMemberLogin
{
  private String email;
  private String postalCode;

  public String getEmail()
  {
    return this.email;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public void setEmail(String paramString)
  {
    this.email = paramString;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestMemberRegister
 * JD-Core Version:    0.6.2
 */