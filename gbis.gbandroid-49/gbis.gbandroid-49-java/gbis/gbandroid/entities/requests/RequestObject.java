package gbis.gbandroid.entities.requests;

import android.location.Location;

public class RequestObject<T>
{
  private int appSource;
  private double appVersion;
  private String authId;
  private String dateDevice;
  private String dateEastern;
  private boolean debug;
  private String key;
  private String memberId;
  private T parameters;
  private int source;
  private RequestObject<T>.Location userLocation;
  private int webServiceVersion;

  public int getAppSource()
  {
    return this.appSource;
  }

  public double getAppVersion()
  {
    return this.appVersion;
  }

  public String getAuthId()
  {
    return this.authId;
  }

  public String getDateDevice()
  {
    return this.dateDevice;
  }

  public String getDateEastern()
  {
    return this.dateEastern;
  }

  public String getKey()
  {
    return this.key;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public T getParameters()
  {
    return this.parameters;
  }

  public int getSource()
  {
    return this.source;
  }

  public RequestObject<T>.Location getUserLocation()
  {
    return this.userLocation;
  }

  public int getWebServiceVersion()
  {
    return this.webServiceVersion;
  }

  public boolean isDebug()
  {
    return this.debug;
  }

  public void setAppSource(int paramInt)
  {
    this.appSource = paramInt;
  }

  public void setAppVersion(double paramDouble)
  {
    this.appVersion = paramDouble;
  }

  public void setAuthId(String paramString)
  {
    this.authId = paramString;
  }

  public void setDateDevice(String paramString)
  {
    this.dateDevice = paramString;
  }

  public void setDateEastern(String paramString)
  {
    this.dateEastern = paramString;
  }

  public void setDebug(boolean paramBoolean)
  {
    this.debug = paramBoolean;
  }

  public void setKey(String paramString)
  {
    this.key = paramString;
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setParameters(T paramT)
  {
    this.parameters = paramT;
  }

  public void setSource(int paramInt)
  {
    this.source = paramInt;
  }

  public void setUserLocation(Location paramLocation)
  {
    this.userLocation = new Location(paramLocation);
  }

  public void setWebServiceVersion(int paramInt)
  {
    this.webServiceVersion = paramInt;
  }

  protected class Location
  {
    private float accuracy;
    private double latitude;
    private double longitude;

    public Location(Location arg2)
    {
      Object localObject;
      this.latitude = localObject.getLatitude();
      this.longitude = localObject.getLongitude();
      this.accuracy = localObject.getAccuracy();
    }

    public float getAccuracy()
    {
      return this.accuracy;
    }

    public double getLatitude()
    {
      return this.latitude;
    }

    public double getLongitude()
    {
      return this.longitude;
    }

    public void setAccuracy(float paramFloat)
    {
      this.accuracy = paramFloat;
    }

    public void setLatitude(double paramDouble)
    {
      this.latitude = paramDouble;
    }

    public void setLongitude(double paramDouble)
    {
      this.longitude = paramDouble;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestObject
 * JD-Core Version:    0.6.2
 */