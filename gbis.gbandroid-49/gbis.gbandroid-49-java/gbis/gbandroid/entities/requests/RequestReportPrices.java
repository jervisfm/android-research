package gbis.gbandroid.entities.requests;

public class RequestReportPrices
{
  private String car;
  private int carIconId;
  private String comments;
  private double dieselPrice;
  private double midgradePrice;
  private double premiumPrice;
  private double regularPrice;
  private int stationId;
  private String timeSpotted;

  public String getCar()
  {
    return this.car;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public String getComments()
  {
    return this.comments;
  }

  public double getDieselPrice()
  {
    return this.dieselPrice;
  }

  public double getMidgradePrice()
  {
    return this.midgradePrice;
  }

  public double getPremiumPrice()
  {
    return this.premiumPrice;
  }

  public double getRegularPrice()
  {
    return this.regularPrice;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public String getTimeSpotted()
  {
    return this.timeSpotted;
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCarIconId(int paramInt)
  {
    this.carIconId = paramInt;
  }

  public void setComments(String paramString)
  {
    this.comments = paramString;
  }

  public void setDieselPrice(double paramDouble)
  {
    this.dieselPrice = paramDouble;
  }

  public void setMidgradePrice(double paramDouble)
  {
    this.midgradePrice = paramDouble;
  }

  public void setPremiumPrice(double paramDouble)
  {
    this.premiumPrice = paramDouble;
  }

  public void setRegularPrice(double paramDouble)
  {
    this.regularPrice = paramDouble;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }

  public void setTimeSpotted(String paramString)
  {
    this.timeSpotted = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestReportPrices
 * JD-Core Version:    0.6.2
 */