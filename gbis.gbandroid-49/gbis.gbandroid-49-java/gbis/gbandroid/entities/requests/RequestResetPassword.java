package gbis.gbandroid.entities.requests;

public class RequestResetPassword
{
  private String email;

  public String getEmail()
  {
    return this.email;
  }

  public void setEmail(String paramString)
  {
    this.email = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestResetPassword
 * JD-Core Version:    0.6.2
 */