package gbis.gbandroid.entities.requests;

public class RequestShareAction extends RequestShareMessage
{
  private int action;
  private String shareMessage;
  private int stationId;
  private String userMessage;

  public int getAction()
  {
    return this.action;
  }

  public String getShareMessage()
  {
    return this.shareMessage;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public String getUserMessage()
  {
    return this.userMessage;
  }

  public void setAction(int paramInt)
  {
    this.action = paramInt;
  }

  public void setShareMessage(String paramString)
  {
    this.shareMessage = paramString;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }

  public void setUserMessage(String paramString)
  {
    this.userMessage = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestShareAction
 * JD-Core Version:    0.6.2
 */