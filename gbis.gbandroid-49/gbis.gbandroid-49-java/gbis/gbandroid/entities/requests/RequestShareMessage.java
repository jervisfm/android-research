package gbis.gbandroid.entities.requests;

public class RequestShareMessage
{
  private int socialNetworkId;

  public int getSocialNetworkId()
  {
    return this.socialNetworkId;
  }

  public void setSocialNetworkId(int paramInt)
  {
    this.socialNetworkId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestShareMessage
 * JD-Core Version:    0.6.2
 */