package gbis.gbandroid.entities.requests;

public class RequestSmartPrompt
{
  private int promptId;
  private boolean showAgain;
  private String userResponse;

  public RequestSmartPrompt copy()
  {
    RequestSmartPrompt localRequestSmartPrompt = new RequestSmartPrompt();
    localRequestSmartPrompt.userResponse = this.userResponse;
    localRequestSmartPrompt.promptId = this.promptId;
    localRequestSmartPrompt.showAgain = this.showAgain;
    return localRequestSmartPrompt;
  }

  public int getPromptId()
  {
    return this.promptId;
  }

  public String getUserResponse()
  {
    return this.userResponse;
  }

  public boolean isShowAgain()
  {
    return this.showAgain;
  }

  public void setPromptId(int paramInt)
  {
    this.promptId = paramInt;
  }

  public void setShowAgain(boolean paramBoolean)
  {
    this.showAgain = paramBoolean;
  }

  public void setUserResponse(String paramString)
  {
    this.userResponse = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestSmartPrompt
 * JD-Core Version:    0.6.2
 */