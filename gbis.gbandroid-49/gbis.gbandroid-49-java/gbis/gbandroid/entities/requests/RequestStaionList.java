package gbis.gbandroid.entities.requests;

public class RequestStaionList extends RequestStationCollection
{
  private String city;
  private int distance;
  private double latitude;
  private double longitude;
  private int numberToReturn;
  private int pageNumber;
  private String postalCode;
  private String state;

  public String getCity()
  {
    return this.city;
  }

  public int getDistance()
  {
    return this.distance;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public int getNumberToReturn()
  {
    return this.numberToReturn;
  }

  public int getPageNumber()
  {
    return this.pageNumber;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setDistance(int paramInt)
  {
    this.distance = paramInt;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setNumberToReturn(int paramInt)
  {
    this.numberToReturn = paramInt;
  }

  public void setPageNumber(int paramInt)
  {
    this.pageNumber = paramInt;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStaionList
 * JD-Core Version:    0.6.2
 */