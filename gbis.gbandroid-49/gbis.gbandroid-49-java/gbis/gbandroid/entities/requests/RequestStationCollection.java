package gbis.gbandroid.entities.requests;

public class RequestStationCollection
{
  private String fuelType;
  private boolean pricesRequired;

  public String getFuelType()
  {
    return this.fuelType;
  }

  public boolean isPricesRequired()
  {
    return this.pricesRequired;
  }

  public void setFuelType(String paramString)
  {
    this.fuelType = paramString;
  }

  public void setPricesRequired(boolean paramBoolean)
  {
    this.pricesRequired = paramBoolean;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStationCollection
 * JD-Core Version:    0.6.2
 */