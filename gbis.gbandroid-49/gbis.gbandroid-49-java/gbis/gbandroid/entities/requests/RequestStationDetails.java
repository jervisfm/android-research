package gbis.gbandroid.entities.requests;

public class RequestStationDetails
{
  private String resolution;
  private int stationId;

  public String getResolution()
  {
    return this.resolution;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public void setResolution(String paramString)
  {
    this.resolution = paramString;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStationDetails
 * JD-Core Version:    0.6.2
 */