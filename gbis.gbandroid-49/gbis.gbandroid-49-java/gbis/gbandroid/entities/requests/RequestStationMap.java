package gbis.gbandroid.entities.requests;

public class RequestStationMap extends RequestStationCollection
{
  private double northWestLatitude;
  private double northWestLongitude;
  private double southEastLatitude;
  private double southEastLongitude;
  private int viewportHeight;
  private double viewportPixelRatio;
  private int viewportWidth;

  public double getNorthWestLatitude()
  {
    return this.northWestLatitude;
  }

  public double getNorthWestLongitude()
  {
    return this.northWestLongitude;
  }

  public double getSouthEastLatitude()
  {
    return this.southEastLatitude;
  }

  public double getSouthEastLongitude()
  {
    return this.southEastLongitude;
  }

  public int getViewportHeight()
  {
    return this.viewportHeight;
  }

  public double getViewportPixelRatio()
  {
    return this.viewportPixelRatio;
  }

  public int getViewportWidth()
  {
    return this.viewportWidth;
  }

  public void setNorthWestLatitude(double paramDouble)
  {
    this.northWestLatitude = paramDouble;
  }

  public void setNorthWestLongitude(double paramDouble)
  {
    this.northWestLongitude = paramDouble;
  }

  public void setSouthEastLatitude(double paramDouble)
  {
    this.southEastLatitude = paramDouble;
  }

  public void setSouthEastLongitude(double paramDouble)
  {
    this.southEastLongitude = paramDouble;
  }

  public void setViewportHeight(int paramInt)
  {
    this.viewportHeight = paramInt;
  }

  public void setViewportPixelRatio(double paramDouble)
  {
    this.viewportPixelRatio = paramDouble;
  }

  public void setViewportWidth(int paramInt)
  {
    this.viewportWidth = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStationMap
 * JD-Core Version:    0.6.2
 */