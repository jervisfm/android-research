package gbis.gbandroid.entities.requests;

public class RequestStationPhotoUploader extends RequestMessage
{
  private String description;
  private int stationId;
  private String title;

  public String getDescription()
  {
    return this.description;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public String getTitle()
  {
    return this.title;
  }

  public void setDescription(String paramString)
  {
    this.description = paramString;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStationPhotoUploader
 * JD-Core Version:    0.6.2
 */