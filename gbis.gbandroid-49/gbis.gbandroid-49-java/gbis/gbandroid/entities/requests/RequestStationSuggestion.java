package gbis.gbandroid.entities.requests;

public class RequestStationSuggestion
{
  private String address;
  private boolean air;
  private boolean atm;
  private boolean cStore;
  private boolean carWash;
  private String city;
  private String crossStreet;
  private boolean diesel;
  private boolean e85;
  private String fuelBrand;
  private double latitude;
  private double longitude;
  private boolean midgradeGas;
  private int numberOfPumps;
  private boolean open247;
  private boolean overrideInsert;
  private boolean payAtPump;
  private boolean payphone;
  private String phone;
  private String postalCode;
  private boolean premiumGas;
  private boolean propane;
  private boolean regularGas;
  private boolean restaurant;
  private boolean restrooms;
  private String searchFuelType;
  private String searchTerms;
  private int searchType;
  private boolean serviceStation;
  private String state;
  private String stationName;
  private boolean truckStop;

  public String getAddress()
  {
    return this.address;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getCrossStreet()
  {
    return this.crossStreet;
  }

  public String getFuelBrand()
  {
    return this.fuelBrand;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public int getNumberOfPumps()
  {
    return this.numberOfPumps;
  }

  public String getPhone()
  {
    return this.phone;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getSearchFuelType()
  {
    return this.searchFuelType;
  }

  public String getSearchTerms()
  {
    return this.searchTerms;
  }

  public int getSearchType()
  {
    return this.searchType;
  }

  public String getState()
  {
    return this.state;
  }

  public String getStationName()
  {
    return this.stationName;
  }

  public boolean isAir()
  {
    return this.air;
  }

  public boolean isAtm()
  {
    return this.atm;
  }

  public boolean isCStore()
  {
    return this.cStore;
  }

  public boolean isCarWash()
  {
    return this.carWash;
  }

  public boolean isDiesel()
  {
    return this.diesel;
  }

  public boolean isE85()
  {
    return this.e85;
  }

  public boolean isMidgradeGas()
  {
    return this.midgradeGas;
  }

  public boolean isOpen247()
  {
    return this.open247;
  }

  public boolean isOverrideInsert()
  {
    return this.overrideInsert;
  }

  public boolean isPayAtPump()
  {
    return this.payAtPump;
  }

  public boolean isPayphone()
  {
    return this.payphone;
  }

  public boolean isPremiumGas()
  {
    return this.premiumGas;
  }

  public boolean isPropane()
  {
    return this.propane;
  }

  public boolean isRegularGas()
  {
    return this.regularGas;
  }

  public boolean isRestaurant()
  {
    return this.restaurant;
  }

  public boolean isRestrooms()
  {
    return this.restrooms;
  }

  public boolean isServiceStation()
  {
    return this.serviceStation;
  }

  public boolean isTruckStop()
  {
    return this.truckStop;
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setAir(boolean paramBoolean)
  {
    this.air = paramBoolean;
  }

  public void setAtm(boolean paramBoolean)
  {
    this.atm = paramBoolean;
  }

  public void setCStore(boolean paramBoolean)
  {
    this.cStore = paramBoolean;
  }

  public void setCarWash(boolean paramBoolean)
  {
    this.carWash = paramBoolean;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setCrossStreet(String paramString)
  {
    this.crossStreet = paramString;
  }

  public void setDiesel(boolean paramBoolean)
  {
    this.diesel = paramBoolean;
  }

  public void setE85(boolean paramBoolean)
  {
    this.e85 = paramBoolean;
  }

  public void setFuelBrand(String paramString)
  {
    this.fuelBrand = paramString;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setMidgradeGas(boolean paramBoolean)
  {
    this.midgradeGas = paramBoolean;
  }

  public void setNumberOfPumps(int paramInt)
  {
    this.numberOfPumps = paramInt;
  }

  public void setOpen247(boolean paramBoolean)
  {
    this.open247 = paramBoolean;
  }

  public void setOverrideInsert(boolean paramBoolean)
  {
    this.overrideInsert = paramBoolean;
  }

  public void setPayAtPump(boolean paramBoolean)
  {
    this.payAtPump = paramBoolean;
  }

  public void setPayphone(boolean paramBoolean)
  {
    this.payphone = paramBoolean;
  }

  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setPremiumGas(boolean paramBoolean)
  {
    this.premiumGas = paramBoolean;
  }

  public void setPropane(boolean paramBoolean)
  {
    this.propane = paramBoolean;
  }

  public void setRegularGas(boolean paramBoolean)
  {
    this.regularGas = paramBoolean;
  }

  public void setRestaurant(boolean paramBoolean)
  {
    this.restaurant = paramBoolean;
  }

  public void setRestrooms(boolean paramBoolean)
  {
    this.restrooms = paramBoolean;
  }

  public void setSearchFuelType(String paramString)
  {
    this.searchFuelType = paramString;
  }

  public void setSearchTerms(String paramString)
  {
    this.searchTerms = paramString;
  }

  public void setSearchType(int paramInt)
  {
    this.searchType = paramInt;
  }

  public void setServiceStation(boolean paramBoolean)
  {
    this.serviceStation = paramBoolean;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public void setStationName(String paramString)
  {
    this.stationName = paramString;
  }

  public void setTruckStop(boolean paramBoolean)
  {
    this.truckStop = paramBoolean;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestStationSuggestion
 * JD-Core Version:    0.6.2
 */