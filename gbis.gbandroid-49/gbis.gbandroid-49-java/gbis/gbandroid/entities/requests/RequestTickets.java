package gbis.gbandroid.entities.requests;

public class RequestTickets
{
  private String address;
  private String address2;
  private String city;
  private String email;
  private String firstName;
  private String lastName;
  private String postalCode;
  private String state;
  private int tickets;

  public String getAddress()
  {
    return this.address;
  }

  public String getAddress2()
  {
    return this.address2;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getEmail()
  {
    return this.email;
  }

  public String getFirstName()
  {
    return this.firstName;
  }

  public String getLastName()
  {
    return this.lastName;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public int getTickets()
  {
    return this.tickets;
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setAddress2(String paramString)
  {
    this.address2 = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setEmail(String paramString)
  {
    this.email = paramString;
  }

  public void setFirstName(String paramString)
  {
    this.firstName = paramString;
  }

  public void setLastName(String paramString)
  {
    this.lastName = paramString;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public void setTickets(int paramInt)
  {
    this.tickets = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestTickets
 * JD-Core Version:    0.6.2
 */