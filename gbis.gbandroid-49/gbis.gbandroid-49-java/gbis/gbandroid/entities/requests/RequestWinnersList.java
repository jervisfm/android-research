package gbis.gbandroid.entities.requests;

public class RequestWinnersList
{
  private int numberToReturn;
  private int pageNumber;

  public int getNumberToReturn()
  {
    return this.numberToReturn;
  }

  public int getPageNumber()
  {
    return this.pageNumber;
  }

  public void setNumberToReturn(int paramInt)
  {
    this.numberToReturn = paramInt;
  }

  public void setPageNumber(int paramInt)
  {
    this.pageNumber = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.requests.RequestWinnersList
 * JD-Core Version:    0.6.2
 */