package gbis.gbandroid.exceptions;

public class CustomConnectionException extends Exception
{
  private static final long serialVersionUID = -2501536688710781207L;

  public CustomConnectionException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.exceptions.CustomConnectionException
 * JD-Core Version:    0.6.2
 */