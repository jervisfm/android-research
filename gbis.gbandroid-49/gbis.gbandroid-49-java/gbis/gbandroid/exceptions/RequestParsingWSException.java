package gbis.gbandroid.exceptions;

public class RequestParsingWSException extends Exception
{
  private static final long serialVersionUID = -5047404030858364931L;

  public RequestParsingWSException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.exceptions.RequestParsingWSException
 * JD-Core Version:    0.6.2
 */