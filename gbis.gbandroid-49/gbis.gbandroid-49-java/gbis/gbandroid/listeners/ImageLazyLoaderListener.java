package gbis.gbandroid.listeners;

import android.widget.ImageView;

public abstract interface ImageLazyLoaderListener
{
  public abstract void onImageFailed(ImageView paramImageView);

  public abstract void onImageLoaded(ImageView paramImageView);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.listeners.ImageLazyLoaderListener
 * JD-Core Version:    0.6.2
 */