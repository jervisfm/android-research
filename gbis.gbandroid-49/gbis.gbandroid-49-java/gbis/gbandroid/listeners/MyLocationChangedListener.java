package gbis.gbandroid.listeners;

import android.location.Location;

public abstract interface MyLocationChangedListener
{
  public abstract void onMyLocationChanged(Location paramLocation);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.listeners.MyLocationChangedListener
 * JD-Core Version:    0.6.2
 */