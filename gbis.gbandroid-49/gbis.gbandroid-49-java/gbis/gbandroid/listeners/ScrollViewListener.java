package gbis.gbandroid.listeners;

import gbis.gbandroid.views.layouts.CustomScrollView;

public abstract interface ScrollViewListener
{
  public abstract void onScrollChanged(CustomScrollView paramCustomScrollView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.listeners.ScrollViewListener
 * JD-Core Version:    0.6.2
 */