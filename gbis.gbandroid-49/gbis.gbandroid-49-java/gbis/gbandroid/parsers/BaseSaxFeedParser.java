package gbis.gbandroid.parsers;

import gbis.gbandroid.exceptions.CustomConnectionException;
import gbis.gbandroid.exceptions.RequestParsingWSException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

public abstract class BaseSaxFeedParser
{
  private URL a;
  private int b = 30000;
  private int c = 20000;

  protected BaseSaxFeedParser()
  {
  }

  protected BaseSaxFeedParser(String paramString)
  {
    try
    {
      this.a = new URL(paramString);
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new RuntimeException(localMalformedURLException);
    }
  }

  protected InputStream getInputStream()
  {
    try
    {
      URLConnection localURLConnection = this.a.openConnection();
      localURLConnection.setConnectTimeout(this.c);
      localURLConnection.setReadTimeout(this.b);
      InputStream localInputStream = localURLConnection.getInputStream();
      return localInputStream;
    }
    catch (AssertionError localAssertionError)
    {
      throw new CustomConnectionException("Assertion Error: " + localAssertionError.getMessage());
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      throw new CustomConnectionException("SocketTimeout: " + localSocketTimeoutException.getMessage());
    }
    catch (Throwable localThrowable)
    {
      throw new RequestParsingWSException("General Exception: " + localThrowable.getMessage());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.BaseSaxFeedParser
 * JD-Core Version:    0.6.2
 */