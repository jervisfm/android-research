package gbis.gbandroid.parsers;

import gbis.gbandroid.entities.ResponseMessage;
import java.io.InputStream;

public abstract interface GenericFeedParser
{
  public abstract ResponseMessage parse(InputStream paramInputStream);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.GenericFeedParser
 * JD-Core Version:    0.6.2
 */