package gbis.gbandroid.parsers.external;

import android.sax.RootElement;
import android.util.Xml;
import android.util.Xml.Encoding;
import gbis.gbandroid.exceptions.CustomConnectionException;
import gbis.gbandroid.exceptions.RequestParsingWSException;
import gbis.gbandroid.parsers.BaseSaxFeedParser;
import java.io.InputStream;
import java.net.SocketTimeoutException;

public abstract class ExternalServiceSaxFeedParser extends BaseSaxFeedParser
{
  public ExternalServiceSaxFeedParser(String paramString)
  {
    super(paramString);
  }

  private Object a(InputStream paramInputStream)
  {
    RootElement localRootElement = getRootElement();
    Object localObject = parseService(localRootElement);
    try
    {
      Xml.parse(paramInputStream, Xml.Encoding.UTF_8, localRootElement.getContentHandler());
      return localObject;
    }
    catch (AssertionError localAssertionError)
    {
      throw new CustomConnectionException("Assertion Error: " + localAssertionError.getMessage());
    }
    catch (SocketTimeoutException localSocketTimeoutException)
    {
      throw new CustomConnectionException("SocketTimeout: " + localSocketTimeoutException.getMessage());
    }
    catch (Exception localException)
    {
      throw new RequestParsingWSException("General Exception: " + localException.getMessage());
    }
  }

  protected abstract RootElement getRootElement();

  public Object parseExternalService()
  {
    return a(getInputStream());
  }

  protected abstract Object parseService(RootElement paramRootElement);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.external.ExternalServiceSaxFeedParser
 * JD-Core Version:    0.6.2
 */