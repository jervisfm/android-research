package gbis.gbandroid.parsers.external;

import android.sax.Element;
import android.sax.RootElement;

public class OpenCellIDSaxFeedParser extends ExternalServiceSaxFeedParser
{
  public OpenCellIDSaxFeedParser(String paramString)
  {
    super(paramString);
  }

  protected RootElement getRootElement()
  {
    return new RootElement("rsp");
  }

  protected Object parseService(RootElement paramRootElement)
  {
    Double[] arrayOfDouble = new Double[2];
    paramRootElement.getChild("cell").setStartElementListener(new a(this, arrayOfDouble));
    return arrayOfDouble;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.external.OpenCellIDSaxFeedParser
 * JD-Core Version:    0.6.2
 */