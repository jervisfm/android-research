package gbis.gbandroid.parsers.external;

import android.sax.StartElementListener;
import org.xml.sax.Attributes;

final class a
  implements StartElementListener
{
  a(OpenCellIDSaxFeedParser paramOpenCellIDSaxFeedParser, Double[] paramArrayOfDouble)
  {
  }

  public final void start(Attributes paramAttributes)
  {
    String str1 = paramAttributes.getValue("lat");
    String str2 = paramAttributes.getValue("lon");
    this.b[0] = Double.valueOf(Double.parseDouble(str1));
    this.b[1] = Double.valueOf(Double.parseDouble(str2));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.external.a
 * JD-Core Version:    0.6.2
 */