package gbis.gbandroid.parsers.json;

import com.google.gbson.FieldNamingPolicy;
import com.google.gbson.Gson;
import com.google.gbson.GsonBuilder;
import gbis.gbandroid.entities.ResponseMessage;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BaseJsonParser<T>
{
  private URL a;
  private int b = 30000;
  private int c = 20000;

  public BaseJsonParser()
  {
  }

  public BaseJsonParser(String paramString)
  {
    try
    {
      this.a = new URL(paramString);
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new RuntimeException(localMalformedURLException);
    }
  }

  protected String getHttpResponseString(HttpURLConnection paramHttpURLConnection)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramHttpURLConnection.getResponseCode());
    localStringBuilder.append(" \n");
    localStringBuilder.append(paramHttpURLConnection.getResponseMessage());
    return localStringBuilder.toString();
  }

  // ERROR //
  protected InputStream getInputStream()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 27	gbis/gbandroid/parsers/json/BaseJsonParser:a	Ljava/net/URL;
    //   4: invokevirtual 73	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   7: checkcast 39	java/net/HttpURLConnection
    //   10: astore_2
    //   11: aload_2
    //   12: aload_0
    //   13: getfield 18	gbis/gbandroid/parsers/json/BaseJsonParser:c	I
    //   16: invokevirtual 77	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   19: aload_2
    //   20: aload_0
    //   21: getfield 16	gbis/gbandroid/parsers/json/BaseJsonParser:b	I
    //   24: invokevirtual 80	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   27: aload_2
    //   28: invokevirtual 82	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   31: astore 6
    //   33: aload 6
    //   35: areturn
    //   36: astore 5
    //   38: new 84	gbis/gbandroid/exceptions/CustomConnectionException
    //   41: dup
    //   42: new 36	java/lang/StringBuilder
    //   45: dup
    //   46: ldc 86
    //   48: invokespecial 87	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   51: aload 5
    //   53: invokevirtual 90	java/lang/AssertionError:getMessage	()Ljava/lang/String;
    //   56: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: ldc 92
    //   61: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: aload_0
    //   65: aload_2
    //   66: invokevirtual 94	gbis/gbandroid/parsers/json/BaseJsonParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   69: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: ldc 96
    //   74: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: aload_0
    //   78: getfield 27	gbis/gbandroid/parsers/json/BaseJsonParser:a	Ljava/net/URL;
    //   81: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   84: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   87: invokespecial 100	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   90: athrow
    //   91: astore_1
    //   92: new 84	gbis/gbandroid/exceptions/CustomConnectionException
    //   95: dup
    //   96: new 36	java/lang/StringBuilder
    //   99: dup
    //   100: ldc 102
    //   102: invokespecial 87	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   105: aload_1
    //   106: invokevirtual 103	java/io/IOException:getMessage	()Ljava/lang/String;
    //   109: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: ldc 105
    //   114: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: aload_0
    //   118: getfield 27	gbis/gbandroid/parsers/json/BaseJsonParser:a	Ljava/net/URL;
    //   121: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   124: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   127: invokespecial 100	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   130: athrow
    //   131: astore 4
    //   133: new 84	gbis/gbandroid/exceptions/CustomConnectionException
    //   136: dup
    //   137: new 36	java/lang/StringBuilder
    //   140: dup
    //   141: ldc 107
    //   143: invokespecial 87	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   146: aload 4
    //   148: invokevirtual 108	java/net/SocketTimeoutException:getMessage	()Ljava/lang/String;
    //   151: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: ldc 92
    //   156: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: aload_0
    //   160: aload_2
    //   161: invokevirtual 94	gbis/gbandroid/parsers/json/BaseJsonParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   164: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   167: ldc 96
    //   169: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   172: aload_0
    //   173: getfield 27	gbis/gbandroid/parsers/json/BaseJsonParser:a	Ljava/net/URL;
    //   176: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   179: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   182: invokespecial 100	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   185: athrow
    //   186: astore_3
    //   187: new 110	gbis/gbandroid/exceptions/RequestParsingWSException
    //   190: dup
    //   191: new 36	java/lang/StringBuilder
    //   194: dup
    //   195: ldc 112
    //   197: invokespecial 87	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   200: aload_3
    //   201: invokevirtual 113	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   204: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: ldc 92
    //   209: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: aload_0
    //   213: aload_2
    //   214: invokevirtual 94	gbis/gbandroid/parsers/json/BaseJsonParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   217: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: ldc 96
    //   222: invokevirtual 52	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   225: aload_0
    //   226: getfield 27	gbis/gbandroid/parsers/json/BaseJsonParser:a	Ljava/net/URL;
    //   229: invokevirtual 99	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   232: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   235: invokespecial 114	gbis/gbandroid/exceptions/RequestParsingWSException:<init>	(Ljava/lang/String;)V
    //   238: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   11	33	36	java/lang/AssertionError
    //   0	11	91	java/io/IOException
    //   11	33	91	java/io/IOException
    //   38	91	91	java/io/IOException
    //   133	186	91	java/io/IOException
    //   187	239	91	java/io/IOException
    //   11	33	131	java/net/SocketTimeoutException
    //   11	33	186	java/lang/Throwable
  }

  public ResponseMessage<T> parseResponseObject(InputStream paramInputStream, Type paramType)
  {
    return (ResponseMessage)new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create().fromJson(new InputStreamReader(paramInputStream), paramType);
  }

  public ResponseMessage<T> parseResponseObject(Type paramType)
  {
    return parseResponseObject(getInputStream(), paramType);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.json.BaseJsonParser
 * JD-Core Version:    0.6.2
 */