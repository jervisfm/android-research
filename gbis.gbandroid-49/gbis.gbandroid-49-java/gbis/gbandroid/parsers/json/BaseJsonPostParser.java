package gbis.gbandroid.parsers.json;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseJsonPostParser<T> extends BaseJsonParser<T>
{
  private URL a;
  private String b;
  private int c = 30000;
  private int d = 20000;

  public BaseJsonPostParser()
  {
  }

  public BaseJsonPostParser(String paramString1, String paramString2)
  {
    try
    {
      this.a = new URL(paramString1);
      this.b = paramString2;
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new RuntimeException(localMalformedURLException);
    }
  }

  public BaseJsonPostParser(String paramString, byte[] paramArrayOfByte)
  {
    try
    {
      this.a = new URL(paramString);
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      throw new RuntimeException(localMalformedURLException);
    }
  }

  // ERROR //
  protected java.io.InputStream getInputStream()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   4: invokevirtual 52	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   7: instanceof 54
    //   10: ifeq +108 -> 118
    //   13: aload_0
    //   14: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   17: invokevirtual 52	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   20: checkcast 54	javax/net/ssl/HttpsURLConnection
    //   23: astore 8
    //   25: aload 8
    //   27: astore_2
    //   28: aload_2
    //   29: aload_0
    //   30: getfield 20	gbis/gbandroid/parsers/json/BaseJsonPostParser:d	I
    //   33: invokevirtual 60	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   36: aload_2
    //   37: aload_0
    //   38: getfield 18	gbis/gbandroid/parsers/json/BaseJsonPostParser:c	I
    //   41: invokevirtual 63	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   44: aload_2
    //   45: iconst_1
    //   46: invokevirtual 67	java/net/HttpURLConnection:setDoInput	(Z)V
    //   49: aload_2
    //   50: iconst_1
    //   51: invokevirtual 70	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   54: aload_2
    //   55: iconst_1
    //   56: invokevirtual 73	java/net/HttpURLConnection:setUseCaches	(Z)V
    //   59: aload_2
    //   60: ldc 75
    //   62: invokevirtual 78	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   65: aload_2
    //   66: ldc 80
    //   68: ldc 82
    //   70: invokevirtual 85	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   73: aload_2
    //   74: invokevirtual 88	java/net/HttpURLConnection:connect	()V
    //   77: new 90	java/io/OutputStreamWriter
    //   80: dup
    //   81: aload_2
    //   82: invokevirtual 94	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   85: invokespecial 97	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
    //   88: astore 6
    //   90: aload 6
    //   92: aload_0
    //   93: getfield 32	gbis/gbandroid/parsers/json/BaseJsonPostParser:b	Ljava/lang/String;
    //   96: invokevirtual 100	java/io/OutputStreamWriter:write	(Ljava/lang/String;)V
    //   99: aload 6
    //   101: invokevirtual 103	java/io/OutputStreamWriter:flush	()V
    //   104: aload 6
    //   106: invokevirtual 106	java/io/OutputStreamWriter:close	()V
    //   109: aload_2
    //   110: invokevirtual 108	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   113: astore 7
    //   115: aload 7
    //   117: areturn
    //   118: aload_0
    //   119: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   122: invokevirtual 52	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   125: checkcast 56	java/net/HttpURLConnection
    //   128: astore_2
    //   129: goto -101 -> 28
    //   132: astore 5
    //   134: new 110	gbis/gbandroid/exceptions/CustomConnectionException
    //   137: dup
    //   138: new 112	java/lang/StringBuilder
    //   141: dup
    //   142: ldc 114
    //   144: invokespecial 115	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   147: aload 5
    //   149: invokevirtual 119	java/lang/AssertionError:getMessage	()Ljava/lang/String;
    //   152: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: ldc 125
    //   157: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   160: aload_0
    //   161: aload_2
    //   162: invokevirtual 129	gbis/gbandroid/parsers/json/BaseJsonPostParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   165: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   168: ldc 131
    //   170: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   173: aload_0
    //   174: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   177: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   180: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   183: invokespecial 138	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   186: athrow
    //   187: astore_1
    //   188: new 110	gbis/gbandroid/exceptions/CustomConnectionException
    //   191: dup
    //   192: new 112	java/lang/StringBuilder
    //   195: dup
    //   196: ldc 140
    //   198: invokespecial 115	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   201: aload_1
    //   202: invokevirtual 141	java/io/IOException:getMessage	()Ljava/lang/String;
    //   205: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: ldc 143
    //   210: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: aload_0
    //   214: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   217: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   220: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   223: invokespecial 138	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   226: athrow
    //   227: astore 4
    //   229: new 110	gbis/gbandroid/exceptions/CustomConnectionException
    //   232: dup
    //   233: new 112	java/lang/StringBuilder
    //   236: dup
    //   237: ldc 145
    //   239: invokespecial 115	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   242: aload 4
    //   244: invokevirtual 146	java/net/SocketTimeoutException:getMessage	()Ljava/lang/String;
    //   247: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   250: ldc 125
    //   252: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: aload_0
    //   256: aload_2
    //   257: invokevirtual 129	gbis/gbandroid/parsers/json/BaseJsonPostParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   260: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   263: ldc 131
    //   265: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   268: aload_0
    //   269: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   272: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   275: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   278: invokespecial 138	gbis/gbandroid/exceptions/CustomConnectionException:<init>	(Ljava/lang/String;)V
    //   281: athrow
    //   282: astore_3
    //   283: new 148	gbis/gbandroid/exceptions/RequestParsingWSException
    //   286: dup
    //   287: new 112	java/lang/StringBuilder
    //   290: dup
    //   291: ldc 150
    //   293: invokespecial 115	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   296: aload_3
    //   297: invokevirtual 151	java/lang/Throwable:getMessage	()Ljava/lang/String;
    //   300: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: ldc 125
    //   305: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: aload_0
    //   309: aload_2
    //   310: invokevirtual 129	gbis/gbandroid/parsers/json/BaseJsonPostParser:getHttpResponseString	(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    //   313: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   316: ldc 131
    //   318: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: aload_0
    //   322: getfield 30	gbis/gbandroid/parsers/json/BaseJsonPostParser:a	Ljava/net/URL;
    //   325: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   328: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   331: invokespecial 152	gbis/gbandroid/exceptions/RequestParsingWSException:<init>	(Ljava/lang/String;)V
    //   334: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   28	115	132	java/lang/AssertionError
    //   0	25	187	java/io/IOException
    //   28	115	187	java/io/IOException
    //   118	129	187	java/io/IOException
    //   134	187	187	java/io/IOException
    //   229	282	187	java/io/IOException
    //   283	335	187	java/io/IOException
    //   28	115	227	java/net/SocketTimeoutException
    //   28	115	282	java/lang/Throwable
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.parsers.json.BaseJsonPostParser
 * JD-Core Version:    0.6.2
 */