package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestStationSuggestion;
import java.lang.reflect.Type;

public class AddStationQuery<T, X> extends BaseQuery<T, RequestStationSuggestion>
{
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private boolean[] h;
  private int i;
  private double j;
  private double k;
  private String l;
  private int m;
  private boolean n;

  public AddStationQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296746));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestStationSuggestion getParameters()
  {
    RequestStationSuggestion localRequestStationSuggestion = new RequestStationSuggestion();
    localRequestStationSuggestion.setAddress(this.b);
    localRequestStationSuggestion.setFuelBrand("");
    localRequestStationSuggestion.setStationName(this.a);
    localRequestStationSuggestion.setCrossStreet(this.c);
    localRequestStationSuggestion.setCity(this.d);
    localRequestStationSuggestion.setState(this.e);
    localRequestStationSuggestion.setPostalCode(this.f);
    localRequestStationSuggestion.setPhone(this.g);
    localRequestStationSuggestion.setNumberOfPumps(this.i);
    localRequestStationSuggestion.setLatitude(this.j);
    localRequestStationSuggestion.setLongitude(this.k);
    localRequestStationSuggestion.setSearchTerms(this.l);
    localRequestStationSuggestion.setSearchType(this.m);
    localRequestStationSuggestion.setSearchFuelType(this.mPrefs.getString("fuelPreference", ""));
    localRequestStationSuggestion.setOverrideInsert(this.n);
    localRequestStationSuggestion.setRegularGas(this.h[0]);
    localRequestStationSuggestion.setMidgradeGas(this.h[1]);
    localRequestStationSuggestion.setPremiumGas(this.h[2]);
    localRequestStationSuggestion.setDiesel(this.h[3]);
    localRequestStationSuggestion.setE85(this.h[4]);
    localRequestStationSuggestion.setPropane(this.h[5]);
    localRequestStationSuggestion.setCStore(this.h[6]);
    localRequestStationSuggestion.setServiceStation(this.h[7]);
    localRequestStationSuggestion.setPayAtPump(this.h[8]);
    localRequestStationSuggestion.setRestaurant(this.h[9]);
    localRequestStationSuggestion.setRestrooms(this.h[10]);
    localRequestStationSuggestion.setAir(this.h[11]);
    localRequestStationSuggestion.setPayphone(this.h[12]);
    localRequestStationSuggestion.setAtm(this.h[13]);
    localRequestStationSuggestion.setOpen247(this.h[14]);
    localRequestStationSuggestion.setTruckStop(this.h[15]);
    localRequestStationSuggestion.setCarWash(this.h[16]);
    return localRequestStationSuggestion;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, boolean[] paramArrayOfBoolean, int paramInt1, double paramDouble1, double paramDouble2, String paramString8, int paramInt2, boolean paramBoolean)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramString3;
    this.d = paramString4;
    this.e = paramString5;
    this.f = paramString6;
    this.g = paramString7;
    this.h = paramArrayOfBoolean;
    this.i = paramInt1;
    this.j = paramDouble1;
    this.k = paramDouble2;
    this.l = paramString8;
    this.m = paramInt2;
    this.n = paramBoolean;
    return parsePostJson(a(), new a(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.AddStationQuery
 * JD-Core Version:    0.6.2
 */