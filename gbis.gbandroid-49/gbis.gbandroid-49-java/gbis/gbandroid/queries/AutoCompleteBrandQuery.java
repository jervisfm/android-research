package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestAutocomplete;
import java.lang.reflect.Type;

public class AutoCompleteBrandQuery<T, X> extends BaseQuery<T, RequestAutocomplete>
{
  private String a;

  public AutoCompleteBrandQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296747));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestAutocomplete getParameters()
  {
    return new RequestAutocomplete(this.a, -1);
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(a(), new b(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.AutoCompleteBrandQuery
 * JD-Core Version:    0.6.2
 */