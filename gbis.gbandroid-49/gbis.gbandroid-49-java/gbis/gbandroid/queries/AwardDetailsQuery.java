package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestAwardDetails;
import java.lang.reflect.Type;

public class AwardDetailsQuery<T, X> extends BaseQuery<T, RequestAwardDetails>
{
  private int a;

  public AwardDetailsQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296749));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestAwardDetails getParameters()
  {
    RequestAwardDetails localRequestAwardDetails = new RequestAwardDetails();
    localRequestAwardDetails.setAwardId(this.a);
    return localRequestAwardDetails;
  }

  public ResponseMessage<T> getResponseObject(int paramInt)
  {
    this.a = paramInt;
    return parsePostJson(a(), new d(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.AwardDetailsQuery
 * JD-Core Version:    0.6.2
 */