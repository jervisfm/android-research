package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import com.google.gbson.FieldNamingPolicy;
import com.google.gbson.Gson;
import com.google.gbson.GsonBuilder;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestObject;
import gbis.gbandroid.exceptions.CustomConnectionException;
import gbis.gbandroid.parsers.json.BaseJsonPostParser;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.FieldEncryption;
import java.lang.reflect.Type;
import java.net.URLEncoder;

public abstract class BaseQuery<T, X>
{
  protected static final int APP_SOURCE = 1;
  protected static final int SOURCE = 3;
  protected Context mContext;
  protected Location mLocation;
  protected SharedPreferences mPrefs;
  protected RequestObject<X> mRequestObject;
  protected Type mTypeParameterClass;

  public BaseQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    this.mContext = paramContext;
    this.mPrefs = paramSharedPreferences;
    this.mTypeParameterClass = paramType;
    this.mLocation = paramLocation;
  }

  private String a(String paramString)
  {
    try
    {
      if (paramString.equals(this.mContext.getString(2131296733)))
        return this.mContext.getString(2131296731);
      String str = this.mContext.getString(2131296732);
      return str;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return this.mContext.getString(2131296731);
  }

  private void a()
  {
    this.mRequestObject = new RequestObject();
    this.mRequestObject.setMemberId(this.mPrefs.getString("member_id", ""));
    this.mRequestObject.setAuthId(URLEncoder.encode(this.mPrefs.getString("auth_id", "")));
    this.mRequestObject.setAppVersion(Double.parseDouble(GBActivity.getVersion(this.mContext)));
    this.mRequestObject.setSource(3);
    this.mRequestObject.setDateDevice(DateUtils.getQueryTimeStampDevice());
    this.mRequestObject.setDateEastern(DateUtils.getQueryTimeStampEastern());
    this.mRequestObject.setUserLocation(this.mLocation);
    this.mRequestObject.setKey(b());
    this.mRequestObject.setAppSource(1);
    this.mRequestObject.setParameters(getParameters());
    this.mRequestObject.setWebServiceVersion(setWebServiceVersion());
  }

  private String b()
  {
    String str1 = this.mRequestObject.getMemberId() + this.mLocation.getAccuracy() + this.mRequestObject.getDateEastern() + this.mPrefs.getString("auth_id", "");
    try
    {
      String str2 = FieldEncryption.hashText(str1, "SHA-256");
      return str2;
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  protected String formURL(String paramString)
  {
    return formURL(getHost(), "", paramString);
  }

  protected String formURL(String paramString1, String paramString2)
  {
    return formURL(paramString1, "", paramString2);
  }

  protected String formURL(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(a(paramString1));
    localStringBuilder.append(paramString1);
    localStringBuilder.append(this.mContext.getString(2131296742));
    localStringBuilder.append(paramString3);
    localStringBuilder.append("?output=json");
    localStringBuilder.append(paramString2);
    String str = localStringBuilder.toString();
    a();
    return str;
  }

  protected String getHost()
  {
    return this.mPrefs.getString("host", "");
  }

  protected abstract X getParameters();

  protected ResponseMessage<T> parsePostJson(String paramString, Type paramType)
  {
    ResponseMessage localResponseMessage = new BaseJsonPostParser(paramString, new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create().toJson(this.mRequestObject, paramType)).parseResponseObject(this.mTypeParameterClass);
    if (localResponseMessage == null)
      throw new CustomConnectionException("ResponseObject was null");
    return localResponseMessage;
  }

  protected abstract int setWebServiceVersion();
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.BaseQuery
 * JD-Core Version:    0.6.2
 */