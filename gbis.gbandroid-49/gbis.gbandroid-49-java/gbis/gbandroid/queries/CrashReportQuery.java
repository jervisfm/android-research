package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestCrashReport;
import gbis.gbandroid.utils.DeviceIdUtility;
import java.lang.reflect.Type;
import org.openudid.OpenUDID_manager;

public class CrashReportQuery<T, X> extends BaseQuery<T, RequestCrashReport>
{
  private String a;

  public CrashReportQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296751));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  private String b()
  {
    OpenUDID_manager.sync(this.mContext);
    while (!OpenUDID_manager.isInitialized());
    String str = ((TelephonyManager)this.mContext.getSystemService("phone")).getDeviceId();
    if ((str == null) && (Integer.parseInt(Build.VERSION.SDK) >= 9))
      str = DeviceIdUtility.getSerialNumber();
    return str;
  }

  protected RequestCrashReport getParameters()
  {
    RequestCrashReport localRequestCrashReport = new RequestCrashReport();
    localRequestCrashReport.setOsName("android");
    localRequestCrashReport.setOsVersion(Build.VERSION.RELEASE);
    localRequestCrashReport.setDeviceId(b());
    localRequestCrashReport.setMessage(this.a);
    return localRequestCrashReport;
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(a(), new g(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.CrashReportQuery
 * JD-Core Version:    0.6.2
 */