package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFavAddList;
import java.lang.reflect.Type;

public class FavAddListQuery<T, X> extends BaseQuery<T, RequestFavAddList>
{
  private String a;

  public FavAddListQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296752));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFavAddList getParameters()
  {
    RequestFavAddList localRequestFavAddList = new RequestFavAddList();
    localRequestFavAddList.setListName(this.a);
    localRequestFavAddList.setDefaultList(false);
    return localRequestFavAddList;
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(a(), new h(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FavAddListQuery
 * JD-Core Version:    0.6.2
 */