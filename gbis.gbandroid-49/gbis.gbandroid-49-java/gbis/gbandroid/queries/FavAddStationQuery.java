package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFavAddStation;
import java.lang.reflect.Type;

public class FavAddStationQuery<T, X> extends BaseQuery<T, RequestFavAddStation>
{
  private int a;
  private int b;
  private String c;

  public FavAddStationQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296753));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFavAddStation getParameters()
  {
    RequestFavAddStation localRequestFavAddStation = new RequestFavAddStation();
    localRequestFavAddStation.setListId(this.a);
    localRequestFavAddStation.setListName(this.c);
    localRequestFavAddStation.setStationId(this.b);
    return localRequestFavAddStation;
  }

  public ResponseMessage<T> getResponseObject(int paramInt1, int paramInt2, String paramString)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramString;
    return parsePostJson(a(), new i(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FavAddStationQuery
 * JD-Core Version:    0.6.2
 */