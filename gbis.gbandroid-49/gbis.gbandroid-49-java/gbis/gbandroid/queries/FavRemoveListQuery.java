package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFavRemoveList;
import java.lang.reflect.Type;

public class FavRemoveListQuery<T, X> extends BaseQuery<T, RequestFavRemoveList>
{
  private int a;

  public FavRemoveListQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296755));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFavRemoveList getParameters()
  {
    RequestFavRemoveList localRequestFavRemoveList = new RequestFavRemoveList();
    localRequestFavRemoveList.setListId(this.a);
    return localRequestFavRemoveList;
  }

  public ResponseMessage<T> getResponseObject(int paramInt)
  {
    this.a = paramInt;
    return parsePostJson(a(), new k(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FavRemoveListQuery
 * JD-Core Version:    0.6.2
 */