package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFavRemoveStation;
import java.lang.reflect.Type;

public class FavRemoveStationQuery<T, X> extends BaseQuery<T, RequestFavRemoveStation>
{
  private int a;

  public FavRemoveStationQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296756));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFavRemoveStation getParameters()
  {
    RequestFavRemoveStation localRequestFavRemoveStation = new RequestFavRemoveStation();
    localRequestFavRemoveStation.setFavoriteId(this.a);
    return localRequestFavRemoveStation;
  }

  public ResponseMessage<T> getResponseObject(int paramInt)
  {
    this.a = paramInt;
    return parsePostJson(a(), new l(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FavRemoveStationQuery
 * JD-Core Version:    0.6.2
 */