package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFavStationList;
import java.lang.reflect.Type;

public class FavStationListQuery<T, X> extends BaseQuery<T, RequestFavStationList>
{
  private int a;
  private int b;
  private int c;

  public FavStationListQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296757));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFavStationList getParameters()
  {
    RequestFavStationList localRequestFavStationList = new RequestFavStationList();
    localRequestFavStationList.setListId(this.a);
    localRequestFavStationList.setPageNumber(this.b);
    localRequestFavStationList.setItemsPerPage(this.c);
    return localRequestFavStationList;
  }

  public ResponseMessage<T> getResponseObject(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    return parsePostJson(a(), new m(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FavStationListQuery
 * JD-Core Version:    0.6.2
 */