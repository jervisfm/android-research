package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestFeedback;
import java.lang.reflect.Type;

public class FeedBackQuery<T, X> extends BaseQuery<T, RequestFeedback>
{
  private String a;
  private String b;

  public FeedBackQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296758));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestFeedback getParameters()
  {
    RequestFeedback localRequestFeedback = new RequestFeedback();
    localRequestFeedback.setEmail(this.a);
    localRequestFeedback.setMessage(this.b);
    return localRequestFeedback;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2)
  {
    this.a = paramString1;
    this.b = paramString2;
    return parsePostJson(a(), new n(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.FeedBackQuery
 * JD-Core Version:    0.6.2
 */