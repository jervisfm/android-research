package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestGeocode;
import java.lang.reflect.Type;

public class GeocoderQuery<T, X> extends BaseQuery<T, RequestGeocode>
{
  private String a;
  private String b;
  private String c;
  private String d;

  public GeocoderQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296759));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestGeocode getParameters()
  {
    RequestGeocode localRequestGeocode = new RequestGeocode();
    localRequestGeocode.setAddress(this.a);
    localRequestGeocode.setCity(this.b);
    localRequestGeocode.setState(this.c);
    localRequestGeocode.setPostalCode(this.d);
    return localRequestGeocode;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    StringBuilder localStringBuilder = new StringBuilder(String.valueOf(paramString1));
    if (!paramString2.equals(""));
    for (String str = " and " + paramString2; ; str = "")
    {
      this.a = str;
      this.b = paramString3;
      this.c = paramString4;
      this.d = paramString5;
      return parsePostJson(a(), new o(this).getType());
    }
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.GeocoderQuery
 * JD-Core Version:    0.6.2
 */