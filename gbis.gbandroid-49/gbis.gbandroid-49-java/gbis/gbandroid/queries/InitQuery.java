package gbis.gbandroid.queries;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestInit;
import gbis.gbandroid.utils.DeviceIdUtility;
import java.lang.reflect.Type;
import org.openudid.OpenUDID_manager;

public class InitQuery<T, X> extends BaseQuery<T, RequestInit>
{
  public InitQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296733), this.mContext.getString(2131296743));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  private String b()
  {
    OpenUDID_manager.sync(this.mContext);
    while (!OpenUDID_manager.isInitialized());
    String str = ((TelephonyManager)this.mContext.getSystemService("phone")).getDeviceId();
    if ((str == null) && (Integer.parseInt(Build.VERSION.SDK) >= 9))
      str = DeviceIdUtility.getSerialNumber();
    return str;
  }

  private String c()
  {
    String str = this.mPrefs.getString("referralString", "");
    if (str.length() > 500)
      str = str.substring(0, 499);
    return str;
  }

  private String d()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    ((Activity)this.mContext).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    return localDisplayMetrics.widthPixels + "x" + localDisplayMetrics.heightPixels + ", " + localDisplayMetrics.density;
  }

  protected RequestInit getParameters()
  {
    RequestInit localRequestInit = new RequestInit();
    localRequestInit.setDestributionMethod(this.mPrefs.getInt("distributionMethod", 0));
    localRequestInit.setDeviceId(b());
    localRequestInit.setDeviceModel(Build.MODEL);
    localRequestInit.setOsName("android");
    localRequestInit.setOsVersion(Build.VERSION.RELEASE);
    localRequestInit.setReferrer(c());
    localRequestInit.setResolution(d());
    localRequestInit.setUserToken("");
    return localRequestInit;
  }

  public ResponseMessage<T> getResponseObject()
  {
    return parsePostJson(a(), new p(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.InitQuery
 * JD-Core Version:    0.6.2
 */