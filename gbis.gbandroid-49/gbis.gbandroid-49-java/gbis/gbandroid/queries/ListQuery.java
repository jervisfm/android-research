package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestStaionList;
import java.lang.reflect.Type;

public class ListQuery<T, X> extends BaseQuery<T, RequestStaionList>
{
  private double a;
  private double b;
  private String c;
  private String d;
  private String e;

  public ListQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296760));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestStaionList getParameters()
  {
    RequestStaionList localRequestStaionList = new RequestStaionList();
    localRequestStaionList.setLatitude(this.a);
    localRequestStaionList.setLongitude(this.b);
    localRequestStaionList.setCity(this.c);
    localRequestStaionList.setState(this.d);
    localRequestStaionList.setPostalCode(this.e);
    localRequestStaionList.setDistance(this.mPrefs.getInt("radiusPreference", 0));
    localRequestStaionList.setFuelType(this.mPrefs.getString("fuelPreference", ""));
    localRequestStaionList.setNumberToReturn(100);
    localRequestStaionList.setPageNumber(1);
    localRequestStaionList.setPricesRequired(false);
    return localRequestStaionList;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2, String paramString3, GeoPoint paramGeoPoint)
  {
    this.a = (paramGeoPoint.getLatitudeE6() / 1000000.0D);
    this.b = (paramGeoPoint.getLongitudeE6() / 1000000.0D);
    this.c = paramString1;
    this.d = paramString2;
    this.e = paramString3;
    return parsePostJson(a(), new q(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ListQuery
 * JD-Core Version:    0.6.2
 */