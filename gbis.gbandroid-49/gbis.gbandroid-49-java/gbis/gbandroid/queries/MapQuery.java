package gbis.gbandroid.queries;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestStationMap;
import java.lang.reflect.Type;

public class MapQuery<T, X> extends BaseQuery<T, RequestStationMap>
{
  private double a;
  private double b;
  private double c;
  private double d;
  private int e;
  private int f;

  public MapQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296761));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestStationMap getParameters()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    ((Activity)this.mContext).getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    RequestStationMap localRequestStationMap = new RequestStationMap();
    localRequestStationMap.setNorthWestLatitude(this.a);
    localRequestStationMap.setNorthWestLongitude(this.b);
    localRequestStationMap.setSouthEastLatitude(this.c);
    localRequestStationMap.setSouthEastLongitude(this.d);
    localRequestStationMap.setFuelType(this.mPrefs.getString("fuelPreference", ""));
    int i;
    if (this.f != 0)
    {
      i = this.f;
      localRequestStationMap.setViewportWidth(i);
      if (this.e == 0)
        break label155;
    }
    label155: for (int j = this.e; ; j = localDisplayMetrics.heightPixels)
    {
      localRequestStationMap.setViewportHeight(j);
      localRequestStationMap.setViewportPixelRatio(localDisplayMetrics.density);
      localRequestStationMap.setPricesRequired(this.mPrefs.getBoolean("noPricesPreference", true));
      return localRequestStationMap;
      i = localDisplayMetrics.widthPixels;
      break;
    }
  }

  public ResponseMessage<T> getResponseObject(GeoPoint paramGeoPoint, int paramInt1, int paramInt2)
  {
    return getResponseObject(paramGeoPoint, paramInt1, paramInt2, 0, 0);
  }

  public ResponseMessage<T> getResponseObject(GeoPoint paramGeoPoint, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.a = ((paramGeoPoint.getLatitudeE6() + paramInt1 / 2) / 1000000.0D);
    this.b = ((paramGeoPoint.getLongitudeE6() - paramInt2 / 2) / 1000000.0D);
    this.c = ((paramGeoPoint.getLatitudeE6() - paramInt1 / 2) / 1000000.0D);
    this.d = ((paramGeoPoint.getLongitudeE6() + paramInt2 / 2) / 1000000.0D);
    this.e = paramInt3;
    this.f = paramInt4;
    return parsePostJson(a(), new r(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.MapQuery
 * JD-Core Version:    0.6.2
 */