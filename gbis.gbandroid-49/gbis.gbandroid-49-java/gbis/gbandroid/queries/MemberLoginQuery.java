package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestMemberLogin;
import java.lang.reflect.Type;

public class MemberLoginQuery<T, X> extends BaseQuery<T, RequestMemberLogin>
{
  private String a;
  private String b;

  public MemberLoginQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296762));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestMemberLogin getParameters()
  {
    RequestMemberLogin localRequestMemberLogin = new RequestMemberLogin();
    localRequestMemberLogin.setMemberId(this.a);
    localRequestMemberLogin.setPassword(this.b);
    return localRequestMemberLogin;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2)
  {
    this.a = paramString1;
    this.b = paramString2;
    return parsePostJson(a(), new s(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.MemberLoginQuery
 * JD-Core Version:    0.6.2
 */