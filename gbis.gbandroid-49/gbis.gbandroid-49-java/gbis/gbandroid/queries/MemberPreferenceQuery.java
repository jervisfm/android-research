package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestMemberPreference;
import java.lang.reflect.Type;
import java.util.List;

public class MemberPreferenceQuery<T, X> extends BaseQuery<T, List<RequestMemberPreference>>
{
  private List<RequestMemberPreference> a;

  public MemberPreferenceQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296777));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected List<RequestMemberPreference> getParameters()
  {
    return this.a;
  }

  public ResponseMessage<T> getResponseObject(List<RequestMemberPreference> paramList)
  {
    this.a = paramList;
    return parsePostJson(a(), new t(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.MemberPreferenceQuery
 * JD-Core Version:    0.6.2
 */