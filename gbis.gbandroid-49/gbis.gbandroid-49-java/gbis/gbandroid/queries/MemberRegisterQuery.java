package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestMemberRegister;
import java.lang.reflect.Type;

public class MemberRegisterQuery<T, X> extends BaseQuery<T, RequestMemberRegister>
{
  private String a;
  private String b;
  private String c;
  private String d;

  public MemberRegisterQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296763));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestMemberRegister getParameters()
  {
    RequestMemberRegister localRequestMemberRegister = new RequestMemberRegister();
    localRequestMemberRegister.setMemberId(this.a);
    localRequestMemberRegister.setEmail(this.c);
    localRequestMemberRegister.setPassword(this.b);
    localRequestMemberRegister.setPostalCode(this.d);
    return localRequestMemberRegister;
  }

  public ResponseMessage<T> getResponseObject(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.a = paramString1;
    this.b = paramString3;
    this.c = paramString2;
    this.d = paramString4;
    return parsePostJson(a(), new v(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.MemberRegisterQuery
 * JD-Core Version:    0.6.2
 */