package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestMember;
import java.lang.reflect.Type;

public class MemberValidateQuery<T, X> extends BaseQuery<T, RequestMember>
{
  private String a;

  public MemberValidateQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296764));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestMember getParameters()
  {
    RequestMember localRequestMember = new RequestMember();
    localRequestMember.setMemberId(this.a);
    return localRequestMember;
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(a(), new w(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.MemberValidateQuery
 * JD-Core Version:    0.6.2
 */