package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestMessage;
import java.lang.reflect.Type;
import java.net.URLEncoder;

public class ProfilePhotoUploaderQuery<T, X> extends BaseQuery<T, RequestMessage>
{
  private String a;

  public ProfilePhotoUploaderQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  protected String formPhotoURL()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296766));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestMessage getParameters()
  {
    RequestMessage localRequestMessage = new RequestMessage();
    localRequestMessage.setMessage(URLEncoder.encode(this.a));
    return localRequestMessage;
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(formPhotoURL(), new y(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ProfilePhotoUploaderQuery
 * JD-Core Version:    0.6.2
 */