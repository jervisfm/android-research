package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestReportPricesFavorites;
import java.lang.reflect.Type;

public class ReportPricesFavoriteQuery<T, X> extends BaseQuery<T, RequestReportPricesFavorites>
{
  private int a;
  private int b;
  private double c;
  private double d;
  private double e;
  private double f;
  private String g;
  private int h;
  private String i;

  public ReportPricesFavoriteQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296768));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestReportPricesFavorites getParameters()
  {
    RequestReportPricesFavorites localRequestReportPricesFavorites = new RequestReportPricesFavorites();
    localRequestReportPricesFavorites.setCar(this.g);
    localRequestReportPricesFavorites.setCarIconId(this.h);
    localRequestReportPricesFavorites.setDieselPrice(this.f);
    localRequestReportPricesFavorites.setFavoriteId(this.b);
    localRequestReportPricesFavorites.setMidgradePrice(this.d);
    localRequestReportPricesFavorites.setPremiumPrice(this.e);
    localRequestReportPricesFavorites.setRegularPrice(this.c);
    localRequestReportPricesFavorites.setStationId(this.a);
    localRequestReportPricesFavorites.setTimeSpotted(this.i);
    return localRequestReportPricesFavorites;
  }

  public ResponseMessage<T> getResponseObject(int paramInt1, int paramInt2, Double paramDouble1, Double paramDouble2, Double paramDouble3, Double paramDouble4, String paramString1, int paramInt3, String paramString2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramDouble1.doubleValue();
    this.d = paramDouble2.doubleValue();
    this.e = paramDouble3.doubleValue();
    this.f = paramDouble4.doubleValue();
    this.g = paramString1;
    this.h = paramInt3;
    this.i = paramString2;
    return parsePostJson(a(), new z(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ReportPricesFavoriteQuery
 * JD-Core Version:    0.6.2
 */