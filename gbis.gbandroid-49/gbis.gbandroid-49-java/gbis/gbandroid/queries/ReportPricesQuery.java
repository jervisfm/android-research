package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestReportPrices;
import java.lang.reflect.Type;

public class ReportPricesQuery<T, X> extends BaseQuery<T, RequestReportPrices>
{
  private int a;
  private double b;
  private double c;
  private double d;
  private double e;
  private String f;
  private int g;
  private String h;
  private String i;

  public ReportPricesQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296767));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestReportPrices getParameters()
  {
    RequestReportPrices localRequestReportPrices = new RequestReportPrices();
    localRequestReportPrices.setCar(this.f);
    localRequestReportPrices.setCarIconId(this.g);
    localRequestReportPrices.setComments(this.i);
    localRequestReportPrices.setDieselPrice(this.e);
    localRequestReportPrices.setMidgradePrice(this.c);
    localRequestReportPrices.setPremiumPrice(this.d);
    localRequestReportPrices.setRegularPrice(this.b);
    localRequestReportPrices.setStationId(this.a);
    localRequestReportPrices.setTimeSpotted(this.h);
    return localRequestReportPrices;
  }

  public ResponseMessage<T> getResponseObject(int paramInt1, Double paramDouble1, Double paramDouble2, Double paramDouble3, Double paramDouble4, String paramString1, int paramInt2, String paramString2, String paramString3)
  {
    this.a = paramInt1;
    this.b = paramDouble1.doubleValue();
    this.c = paramDouble2.doubleValue();
    this.d = paramDouble3.doubleValue();
    this.e = paramDouble4.doubleValue();
    this.f = paramString1;
    this.g = paramInt2;
    this.h = paramString2;
    this.i = paramString3;
    return parsePostJson(a(), new aa(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ReportPricesQuery
 * JD-Core Version:    0.6.2
 */