package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestResetPassword;
import java.lang.reflect.Type;

public class ResetPasswordQuery<T, X> extends BaseQuery<T, RequestResetPassword>
{
  private String a;

  public ResetPasswordQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296769));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestResetPassword getParameters()
  {
    RequestResetPassword localRequestResetPassword = new RequestResetPassword();
    localRequestResetPassword.setEmail(this.a);
    return localRequestResetPassword;
  }

  public ResponseMessage<T> getResponseObject(String paramString)
  {
    this.a = paramString;
    return parsePostJson(a(), new ab(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ResetPasswordQuery
 * JD-Core Version:    0.6.2
 */