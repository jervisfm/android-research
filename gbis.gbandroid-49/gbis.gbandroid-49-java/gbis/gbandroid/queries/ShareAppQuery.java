package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestShareMessage;
import java.lang.reflect.Type;

public class ShareAppQuery<T, X> extends BaseQuery<T, RequestShareMessage>
{
  private int a;

  public ShareAppQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296770));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestShareMessage getParameters()
  {
    RequestShareMessage localRequestShareMessage = new RequestShareMessage();
    localRequestShareMessage.setSocialNetworkId(this.a);
    return localRequestShareMessage;
  }

  public ResponseMessage<T> getResponseObject(int paramInt)
  {
    this.a = paramInt;
    return parsePostJson(a(), new ac(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.ShareAppQuery
 * JD-Core Version:    0.6.2
 */