package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestSmartPrompt;
import java.lang.reflect.Type;

public class SmartPromptResponseQuery<T, X> extends BaseQuery<T, RequestSmartPrompt>
{
  private int a;
  private String b;
  private boolean c;

  public SmartPromptResponseQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296776));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestSmartPrompt getParameters()
  {
    RequestSmartPrompt localRequestSmartPrompt = new RequestSmartPrompt();
    localRequestSmartPrompt.setPromptId(this.a);
    localRequestSmartPrompt.setUserResponse(this.b);
    localRequestSmartPrompt.setShowAgain(this.c);
    return localRequestSmartPrompt;
  }

  public ResponseMessage<T> getResponseObject(int paramInt, String paramString, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramBoolean;
    return parsePostJson(a(), new ad(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.SmartPromptResponseQuery
 * JD-Core Version:    0.6.2
 */