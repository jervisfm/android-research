package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestShareAction;
import java.lang.reflect.Type;

public class SocialMediaActionQuery<T, X> extends BaseQuery<T, RequestShareAction>
{
  private int a;
  private int b;
  private int c;
  private String d;
  private String e;

  public SocialMediaActionQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296771));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestShareAction getParameters()
  {
    RequestShareAction localRequestShareAction = new RequestShareAction();
    localRequestShareAction.setAction(this.c);
    localRequestShareAction.setShareMessage(this.d);
    localRequestShareAction.setSocialNetworkId(this.b);
    localRequestShareAction.setStationId(this.a);
    localRequestShareAction.setUserMessage(this.e);
    return localRequestShareAction;
  }

  public ResponseMessage<T> getResponseObject(int paramInt1, int paramInt2, int paramInt3, String paramString1, String paramString2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
    this.d = paramString1;
    this.e = paramString2;
    return parsePostJson(a(), new ae(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.SocialMediaActionQuery
 * JD-Core Version:    0.6.2
 */