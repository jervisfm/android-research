package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.FieldEncryption;
import java.lang.reflect.Type;
import java.net.URLEncoder;

public class SocialMediaQuery<T, X> extends BaseQuery<T, Void>
{
  public SocialMediaQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  public String formLoginURL(int paramInt)
  {
    try
    {
      String str1 = this.mPrefs.getString("member_id", "") + "|" + paramInt + "|" + this.mPrefs.getString("auth_id", "") + "|3|" + DateUtils.getQueryTimeStampEastern();
      String str2 = this.mContext.getString(2131296782) + "?q=" + URLEncoder.encode(FieldEncryption.encode64(FieldEncryption.encryptTripleDES(str1)));
      return str2;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected Void getParameters()
  {
    return null;
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.SocialMediaQuery
 * JD-Core Version:    0.6.2
 */