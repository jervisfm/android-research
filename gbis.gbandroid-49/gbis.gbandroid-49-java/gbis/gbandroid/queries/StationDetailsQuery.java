package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.util.DisplayMetrics;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestStationDetails;
import gbis.gbandroid.utils.ImageUtils;
import java.lang.reflect.Type;

public class StationDetailsQuery<T, X> extends BaseQuery<T, RequestStationDetails>
{
  private int a;

  public StationDetailsQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296772));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestStationDetails getParameters()
  {
    float f = this.mContext.getResources().getDisplayMetrics().density;
    RequestStationDetails localRequestStationDetails = new RequestStationDetails();
    localRequestStationDetails.setStationId(this.a);
    localRequestStationDetails.setResolution(ImageUtils.getResolutionInText(f));
    return localRequestStationDetails;
  }

  public ResponseMessage<T> getResponseObject(int paramInt)
  {
    this.a = paramInt;
    return parsePostJson(a(), new af(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.StationDetailsQuery
 * JD-Core Version:    0.6.2
 */