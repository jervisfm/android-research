package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestStationPhotoUploader;
import java.lang.reflect.Type;
import java.net.URLEncoder;

public class StationPhotoUploaderQuery<T, X> extends BaseQuery<T, RequestStationPhotoUploader>
{
  private int a;
  private String b;
  private String c;

  public StationPhotoUploaderQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  protected String formPhotoURL()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296773));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestStationPhotoUploader getParameters()
  {
    RequestStationPhotoUploader localRequestStationPhotoUploader = new RequestStationPhotoUploader();
    localRequestStationPhotoUploader.setDescription(this.b);
    localRequestStationPhotoUploader.setMessage(URLEncoder.encode(this.c));
    localRequestStationPhotoUploader.setStationId(this.a);
    localRequestStationPhotoUploader.setTitle(this.mContext.getString(2131296387));
    return localRequestStationPhotoUploader;
  }

  public ResponseMessage<T> getResponseObject(int paramInt, String paramString1, String paramString2)
  {
    this.a = paramInt;
    this.b = paramString1;
    this.c = paramString2;
    return parsePostJson(formPhotoURL(), new ag(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.StationPhotoUploaderQuery
 * JD-Core Version:    0.6.2
 */