package gbis.gbandroid.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import gbis.gbandroid.entities.PrizeMemberMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.requests.RequestTickets;
import java.lang.reflect.Type;

public class TicketsQuery<T, X> extends BaseQuery<T, RequestTickets>
{
  private int a;
  private PrizeMemberMessage b;

  public TicketsQuery(Context paramContext, SharedPreferences paramSharedPreferences, Type paramType, Location paramLocation)
  {
    super(paramContext, paramSharedPreferences, paramType, paramLocation);
  }

  private String a()
  {
    try
    {
      String str = formURL(this.mContext.getString(2131296774));
      return str;
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected RequestTickets getParameters()
  {
    RequestTickets localRequestTickets = new RequestTickets();
    localRequestTickets.setAddress(this.b.getAddress1());
    localRequestTickets.setAddress2(this.b.getAddress2());
    localRequestTickets.setCity(this.b.getCity());
    localRequestTickets.setEmail(this.b.getEmail());
    localRequestTickets.setFirstName(this.b.getFirstName());
    localRequestTickets.setLastName(this.b.getLastName());
    localRequestTickets.setPostalCode(this.b.getPostalCode());
    localRequestTickets.setState(this.b.getState());
    localRequestTickets.setTickets(this.a);
    return localRequestTickets;
  }

  public ResponseMessage<T> getResponseObject(int paramInt, PrizeMemberMessage paramPrizeMemberMessage)
  {
    this.a = paramInt;
    this.b = paramPrizeMemberMessage;
    return parsePostJson(a(), new ah(this).getType());
  }

  protected int setWebServiceVersion()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.TicketsQuery
 * JD-Core Version:    0.6.2
 */