package gbis.gbandroid.queries.external;

import android.content.Context;

public abstract class ExternalServiceQuery
{
  protected Context mContext;

  public ExternalServiceQuery(Context paramContext)
  {
    this.mContext = paramContext;
  }

  protected abstract Object parseXML(String paramString);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.external.ExternalServiceQuery
 * JD-Core Version:    0.6.2
 */