package gbis.gbandroid.queries.external;

import android.content.Context;
import gbis.gbandroid.parsers.external.OpenCellIDSaxFeedParser;

public class OpenCellIDQuery extends ExternalServiceQuery
{
  public OpenCellIDQuery(Context paramContext)
  {
    super(paramContext);
  }

  protected String formURL(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    String str = "key=" + this.mContext.getString(2131296809) + "&mnc=" + paramInt1 + "&mcc=" + paramInt2 + "&lac=" + paramInt3 + "&cellid=" + paramInt4;
    return this.mContext.getString(2131296785) + str;
  }

  public Object getParsedObject(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return parseXML(formURL(paramInt1, paramInt2, paramInt3, paramInt4));
  }

  protected Object parseXML(String paramString)
  {
    return new OpenCellIDSaxFeedParser(paramString).parseExternalService();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.queries.external.OpenCellIDQuery
 * JD-Core Version:    0.6.2
 */