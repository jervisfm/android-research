package gbis.gbandroid.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class InstallReferral extends BroadcastReceiver
{
  private static final String[] a = { "param1", "param2" };

  public static Map<String, String> retrieveReferralParams(Context paramContext)
  {
    HashMap localHashMap = new HashMap();
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String[] arrayOfString = a;
    int i = arrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return localHashMap;
      String str1 = arrayOfString[j];
      String str2 = localSharedPreferences.getString(str1, null);
      if (str2 != null)
        localHashMap.put(str1, str2);
    }
  }

  public static void storeReferralParams(Context paramContext, Map<String, String> paramMap)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    String[] arrayOfString = a;
    int i = arrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        localEditor.commit();
        return;
      }
      String str1 = arrayOfString[j];
      String str2 = (String)paramMap.get(str1);
      if (str2 != null)
        localEditor.putString(str1, str2);
    }
  }

  public static void storeReferrerString(Context paramContext, String paramString)
  {
    if (paramString.length() > 500)
      paramString = paramString.substring(0, 499);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    localEditor.putString("referralString", paramString);
    localEditor.commit();
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      Bundle localBundle = paramIntent.getExtras();
      if (localBundle != null)
        localBundle.containsKey(null);
      if (!paramIntent.getAction().equals("com.android.vending.INSTALL_REFERRER"));
      String str1;
      do
      {
        return;
        str1 = paramIntent.getStringExtra("referrer");
      }
      while ((str1 == null) || (str1.length() == 0));
      try
      {
        String str2 = URLDecoder.decode(str1, "x-www-form-urlencoded");
        storeReferrerString(paramContext, str2);
        return;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
      }
    }
    catch (Exception localException)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.receivers.InstallReferral
 * JD-Core Version:    0.6.2
 */