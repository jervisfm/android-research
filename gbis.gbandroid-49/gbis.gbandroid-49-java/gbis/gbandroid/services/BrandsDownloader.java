package gbis.gbandroid.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.entities.Brand;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.BrandsDownloadQuery;
import gbis.gbandroid.services.base.GBService;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.ImageUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.List;

public class BrandsDownloader extends GBService
{
  private List<Brand> a;
  private int b;
  private Location c;
  protected SharedPreferences mPrefs;

  private void a()
  {
    new a(this).execute(new Object[0]);
  }

  private void a(int paramInt1, int paramInt2)
  {
    new b(paramInt1, paramInt2).execute(new Object[0]);
  }

  private void a(String paramString, int paramInt1, int paramInt2)
  {
    String str = String.valueOf(paramInt1);
    File localFile1 = new File(Environment.getExternalStorageDirectory() + "/Android/data/gbis.gbandroid/cache/Brands");
    localFile1.mkdirs();
    File localFile2 = new File(localFile1, str);
    try
    {
      InputStream localInputStream = new URL(paramString).openStream();
      FileOutputStream localFileOutputStream = new FileOutputStream(localFile2);
      ImageUtils.CopyStream(localInputStream, localFileOutputStream);
      localFileOutputStream.close();
      updateBrandVersion(paramInt1, paramInt2);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private void b()
  {
    Type localType = new a(this).getType();
    this.a = ((List)new BrandsDownloadQuery(this, this.mPrefs, localType, this.c).getResponseObject().getPayload());
  }

  private void b(int paramInt1, int paramInt2)
  {
    a(GBApplication.getInstance().getImageUrl(paramInt1, getDensity()), paramInt1, paramInt2);
  }

  private int c()
  {
    try
    {
      int i = this.b;
      this.b = (i - 1);
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle != null)
      if ((Location)localBundle.getParcelable("my location") == null)
        break label65;
    label65: for (this.c = new Location((Location)localBundle.getParcelable("my location")); ; this.c = new Location("NoLocation"))
    {
      openDB();
      a();
      return;
    }
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBService arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        if ((BrandsDownloader.a(BrandsDownloader.this) != null) && (BrandsDownloader.a(BrandsDownloader.this).size() != 0))
          break label45;
        BrandsDownloader.this.stopSelf();
      }
      while (true)
      {
        return;
        label45: BrandsDownloader.a(BrandsDownloader.this, BrandsDownloader.a(BrandsDownloader.this).size());
        for (int i = 0; i < BrandsDownloader.a(BrandsDownloader.this).size(); i++)
        {
          Brand localBrand = (Brand)BrandsDownloader.a(BrandsDownloader.this).get(i);
          BrandsDownloader.a(BrandsDownloader.this, localBrand.getGasBrandId(), localBrand.getGasBrandVersion());
        }
      }
    }

    protected final boolean queryWebService()
    {
      BrandsDownloader.b(BrandsDownloader.this);
      return true;
    }
  }

  private final class b extends AsyncTask<Object, Object, Boolean>
  {
    private int b;
    private int c;

    public b(int paramInt1, int arg3)
    {
      this.b = paramInt1;
      int i;
      this.c = i;
    }

    private Boolean a()
    {
      BrandsDownloader.b(BrandsDownloader.this, this.b, this.c);
      return null;
    }

    private void a(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (BrandsDownloader.c(BrandsDownloader.this) <= 1)
        BrandsDownloader.this.stopSelf();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.services.BrandsDownloader
 * JD-Core Version:    0.6.2
 */