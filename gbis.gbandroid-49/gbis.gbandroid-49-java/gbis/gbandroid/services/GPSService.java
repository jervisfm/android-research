package gbis.gbandroid.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.listeners.MyLocationChangedListener;
import gbis.gbandroid.services.base.GBService;
import gbis.gbandroid.utils.ConnectionUtils;

public class GPSService extends GBService
  implements LocationListener
{
  public static final float LOCATION_UPDATE_MIN_DISTANCE = 333.0F;
  public static final long LOCATION_UPDATE_MIN_TIME = 30002L;
  public static String LOCATION_UPDATE_TIME = "locationUpdateTime";
  private Location a;
  private boolean b;
  private final IBinder c = new GPSBinder();
  private MyLocationChangedListener d;
  protected LocationManager locationManager;

  private void a(Location paramLocation)
  {
    if (paramLocation != null)
    {
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
      localEditor.putFloat("lastLatitude", (float)paramLocation.getLatitude());
      localEditor.putFloat("lastLongitude", (float)paramLocation.getLongitude());
      localEditor.commit();
    }
  }

  public Location getLastKnownLocation()
  {
    boolean bool = this.locationManager.isProviderEnabled("gps");
    Location localLocation1 = null;
    if (bool)
      localLocation1 = this.locationManager.getLastKnownLocation("gps");
    if (this.locationManager.isProviderEnabled("network"))
    {
      Location localLocation2 = this.locationManager.getLastKnownLocation("network");
      if ((localLocation1 == null) || ((localLocation2 != null) && (ConnectionUtils.isBetterLocation(localLocation2, localLocation1))))
        return localLocation2;
    }
    if (ConnectionUtils.isBetterLocation(this.a, localLocation1))
      localLocation1 = this.a;
    a(localLocation1);
    return localLocation1;
  }

  public Location getSelfLocation()
  {
    try
    {
      long l = System.currentTimeMillis();
      if ((this.locationManager.isProviderEnabled("gps")) || (this.locationManager.isProviderEnabled("network")))
        while (true)
        {
          if ((this.a != null) || (System.currentTimeMillis() - l >= 5000L))
          {
            if (this.a == null)
              break;
            return this.a;
          }
          if (!this.b)
          {
            Location localLocation2 = this.locationManager.getLastKnownLocation("gps");
            if ((localLocation2 != null) && (ConnectionUtils.isBetterLocation(localLocation2, this.a)))
              this.a = localLocation2;
          }
          Location localLocation1 = this.locationManager.getLastKnownLocation("network");
          if ((localLocation1 != null) && (ConnectionUtils.isBetterLocation(localLocation1, this.a)))
            this.a = localLocation1;
        }
    }
    catch (Throwable localThrowable)
    {
    }
    return null;
  }

  public IBinder onBind(Intent paramIntent)
  {
    long l = 30002L;
    if (paramIntent.getExtras() != null)
      l = paramIntent.getExtras().getLong(LOCATION_UPDATE_TIME, l);
    startLocationListener(l);
    return this.c;
  }

  public void onCreate(Intent paramIntent, int paramInt)
  {
    super.onCreate();
    startLocationListener(30002L);
  }

  public void onDestroy()
  {
    stopLocationListener();
  }

  public void onLocationChanged(Location paramLocation)
  {
    if (ConnectionUtils.isBetterLocation(paramLocation, this.a))
    {
      this.a = paramLocation;
      if (this.d != null)
        this.d.onMyLocationChanged(this.a);
      ((GBApplication)getApplication()).setLastKnowLocation(paramLocation);
      a(this.a);
    }
  }

  public void onProviderDisabled(String paramString)
  {
  }

  public void onProviderEnabled(String paramString)
  {
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
    startLocationListener(30002L);
  }

  public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
  {
  }

  public boolean onUnbind(Intent paramIntent)
  {
    stopSelf();
    return super.onUnbind(paramIntent);
  }

  public void registerMyLocationChangedListener(MyLocationChangedListener paramMyLocationChangedListener)
  {
    this.d = paramMyLocationChangedListener;
  }

  public void startLocationListener()
  {
    startLocationListener(30002L);
  }

  public void startLocationListener(long paramLong)
  {
    if (this.locationManager == null)
      this.locationManager = ((LocationManager)getSystemService("location"));
    try
    {
      this.locationManager.requestLocationUpdates("network", paramLong, 333.0F, this);
      this.locationManager.requestLocationUpdates("gps", paramLong, 333.0F, this);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return;
    }
    catch (Exception localException)
    {
      this.b = true;
    }
  }

  public void stopLocationListener()
  {
    if (this.locationManager != null)
      this.locationManager.removeUpdates(this);
  }

  public class GPSBinder extends Binder
  {
    public GPSBinder()
    {
    }

    public GPSService getService()
    {
      return GPSService.this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.services.GPSService
 * JD-Core Version:    0.6.2
 */