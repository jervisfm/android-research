package gbis.gbandroid.services.base;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import gbis.gbandroid.utils.DBHelper;

public class GBService extends Service
{
  private DBHelper a;
  protected SharedPreferences mPrefs;

  protected void closeDB()
  {
    if (this.a != null)
      this.a.closeDB();
  }

  protected float getDensity()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    ((WindowManager)getSystemService("window")).getDefaultDisplay().getMetrics(localDisplayMetrics);
    return localDisplayMetrics.density;
  }

  protected boolean isDBOpen()
  {
    if (this.a != null)
      return this.a.isDBOpen();
    return false;
  }

  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }

  public void onCreate()
  {
    super.onCreate();
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
  }

  public void onDestroy()
  {
    super.onDestroy();
    closeDB();
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
  }

  protected boolean openDB()
  {
    if (this.a == null)
      this.a = new DBHelper(this);
    return this.a.openDB();
  }

  protected boolean updateBrandVersion(int paramInt1, int paramInt2)
  {
    if (!isDBOpen())
      openDB();
    return this.a.updateBrandVersionRow(paramInt1, paramInt2);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.services.base.GBService
 * JD-Core Version:    0.6.2
 */