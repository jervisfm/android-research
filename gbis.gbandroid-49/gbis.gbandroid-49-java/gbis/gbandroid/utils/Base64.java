package gbis.gbandroid.utils;

import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

public class Base64
{
  public static final int DECODE = 0;
  public static final int DONT_GUNZIP = 4;
  public static final int DO_BREAK_LINES = 8;
  public static final int ENCODE = 1;
  public static final int GZIP = 2;
  public static final int NO_OPTIONS = 0;
  public static final int ORDERED = 32;
  public static final int URL_SAFE = 16;
  private static final byte[] b;
  private static final byte[] c;
  private static final byte[] d;
  private static final byte[] e;
  private static final byte[] f;
  private static final byte[] g;

  static
  {
    if (!Base64.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      a = bool;
      b = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
      byte[] arrayOfByte1 = new byte[256];
      arrayOfByte1[0] = -9;
      arrayOfByte1[1] = -9;
      arrayOfByte1[2] = -9;
      arrayOfByte1[3] = -9;
      arrayOfByte1[4] = -9;
      arrayOfByte1[5] = -9;
      arrayOfByte1[6] = -9;
      arrayOfByte1[7] = -9;
      arrayOfByte1[8] = -9;
      arrayOfByte1[9] = -5;
      arrayOfByte1[10] = -5;
      arrayOfByte1[11] = -9;
      arrayOfByte1[12] = -9;
      arrayOfByte1[13] = -5;
      arrayOfByte1[14] = -9;
      arrayOfByte1[15] = -9;
      arrayOfByte1[16] = -9;
      arrayOfByte1[17] = -9;
      arrayOfByte1[18] = -9;
      arrayOfByte1[19] = -9;
      arrayOfByte1[20] = -9;
      arrayOfByte1[21] = -9;
      arrayOfByte1[22] = -9;
      arrayOfByte1[23] = -9;
      arrayOfByte1[24] = -9;
      arrayOfByte1[25] = -9;
      arrayOfByte1[26] = -9;
      arrayOfByte1[27] = -9;
      arrayOfByte1[28] = -9;
      arrayOfByte1[29] = -9;
      arrayOfByte1[30] = -9;
      arrayOfByte1[31] = -9;
      arrayOfByte1[32] = -5;
      arrayOfByte1[33] = -9;
      arrayOfByte1[34] = -9;
      arrayOfByte1[35] = -9;
      arrayOfByte1[36] = -9;
      arrayOfByte1[37] = -9;
      arrayOfByte1[38] = -9;
      arrayOfByte1[39] = -9;
      arrayOfByte1[40] = -9;
      arrayOfByte1[41] = -9;
      arrayOfByte1[42] = -9;
      arrayOfByte1[43] = 62;
      arrayOfByte1[44] = -9;
      arrayOfByte1[45] = -9;
      arrayOfByte1[46] = -9;
      arrayOfByte1[47] = 63;
      arrayOfByte1[48] = 52;
      arrayOfByte1[49] = 53;
      arrayOfByte1[50] = 54;
      arrayOfByte1[51] = 55;
      arrayOfByte1[52] = 56;
      arrayOfByte1[53] = 57;
      arrayOfByte1[54] = 58;
      arrayOfByte1[55] = 59;
      arrayOfByte1[56] = 60;
      arrayOfByte1[57] = 61;
      arrayOfByte1[58] = -9;
      arrayOfByte1[59] = -9;
      arrayOfByte1[60] = -9;
      arrayOfByte1[61] = -1;
      arrayOfByte1[62] = -9;
      arrayOfByte1[63] = -9;
      arrayOfByte1[64] = -9;
      arrayOfByte1[66] = 1;
      arrayOfByte1[67] = 2;
      arrayOfByte1[68] = 3;
      arrayOfByte1[69] = 4;
      arrayOfByte1[70] = 5;
      arrayOfByte1[71] = 6;
      arrayOfByte1[72] = 7;
      arrayOfByte1[73] = 8;
      arrayOfByte1[74] = 9;
      arrayOfByte1[75] = 10;
      arrayOfByte1[76] = 11;
      arrayOfByte1[77] = 12;
      arrayOfByte1[78] = 13;
      arrayOfByte1[79] = 14;
      arrayOfByte1[80] = 15;
      arrayOfByte1[81] = 16;
      arrayOfByte1[82] = 17;
      arrayOfByte1[83] = 18;
      arrayOfByte1[84] = 19;
      arrayOfByte1[85] = 20;
      arrayOfByte1[86] = 21;
      arrayOfByte1[87] = 22;
      arrayOfByte1[88] = 23;
      arrayOfByte1[89] = 24;
      arrayOfByte1[90] = 25;
      arrayOfByte1[91] = -9;
      arrayOfByte1[92] = -9;
      arrayOfByte1[93] = -9;
      arrayOfByte1[94] = -9;
      arrayOfByte1[95] = -9;
      arrayOfByte1[96] = -9;
      arrayOfByte1[97] = 26;
      arrayOfByte1[98] = 27;
      arrayOfByte1[99] = 28;
      arrayOfByte1[100] = 29;
      arrayOfByte1[101] = 30;
      arrayOfByte1[102] = 31;
      arrayOfByte1[103] = 32;
      arrayOfByte1[104] = 33;
      arrayOfByte1[105] = 34;
      arrayOfByte1[106] = 35;
      arrayOfByte1[107] = 36;
      arrayOfByte1[108] = 37;
      arrayOfByte1[109] = 38;
      arrayOfByte1[110] = 39;
      arrayOfByte1[111] = 40;
      arrayOfByte1[112] = 41;
      arrayOfByte1[113] = 42;
      arrayOfByte1[114] = 43;
      arrayOfByte1[115] = 44;
      arrayOfByte1[116] = 45;
      arrayOfByte1[117] = 46;
      arrayOfByte1[118] = 47;
      arrayOfByte1[119] = 48;
      arrayOfByte1[120] = 49;
      arrayOfByte1[121] = 50;
      arrayOfByte1[122] = 51;
      arrayOfByte1[123] = -9;
      arrayOfByte1[124] = -9;
      arrayOfByte1[125] = -9;
      arrayOfByte1[126] = -9;
      arrayOfByte1[127] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[''] = -9;
      arrayOfByte1[' '] = -9;
      arrayOfByte1['¡'] = -9;
      arrayOfByte1['¢'] = -9;
      arrayOfByte1['£'] = -9;
      arrayOfByte1['¤'] = -9;
      arrayOfByte1['¥'] = -9;
      arrayOfByte1['¦'] = -9;
      arrayOfByte1['§'] = -9;
      arrayOfByte1['¨'] = -9;
      arrayOfByte1['©'] = -9;
      arrayOfByte1['ª'] = -9;
      arrayOfByte1['«'] = -9;
      arrayOfByte1['¬'] = -9;
      arrayOfByte1['­'] = -9;
      arrayOfByte1['®'] = -9;
      arrayOfByte1['¯'] = -9;
      arrayOfByte1['°'] = -9;
      arrayOfByte1['±'] = -9;
      arrayOfByte1['²'] = -9;
      arrayOfByte1['³'] = -9;
      arrayOfByte1['´'] = -9;
      arrayOfByte1['µ'] = -9;
      arrayOfByte1['¶'] = -9;
      arrayOfByte1['·'] = -9;
      arrayOfByte1['¸'] = -9;
      arrayOfByte1['¹'] = -9;
      arrayOfByte1['º'] = -9;
      arrayOfByte1['»'] = -9;
      arrayOfByte1['¼'] = -9;
      arrayOfByte1['½'] = -9;
      arrayOfByte1['¾'] = -9;
      arrayOfByte1['¿'] = -9;
      arrayOfByte1['À'] = -9;
      arrayOfByte1['Á'] = -9;
      arrayOfByte1['Â'] = -9;
      arrayOfByte1['Ã'] = -9;
      arrayOfByte1['Ä'] = -9;
      arrayOfByte1['Å'] = -9;
      arrayOfByte1['Æ'] = -9;
      arrayOfByte1['Ç'] = -9;
      arrayOfByte1['È'] = -9;
      arrayOfByte1['É'] = -9;
      arrayOfByte1['Ê'] = -9;
      arrayOfByte1['Ë'] = -9;
      arrayOfByte1['Ì'] = -9;
      arrayOfByte1['Í'] = -9;
      arrayOfByte1['Î'] = -9;
      arrayOfByte1['Ï'] = -9;
      arrayOfByte1['Ð'] = -9;
      arrayOfByte1['Ñ'] = -9;
      arrayOfByte1['Ò'] = -9;
      arrayOfByte1['Ó'] = -9;
      arrayOfByte1['Ô'] = -9;
      arrayOfByte1['Õ'] = -9;
      arrayOfByte1['Ö'] = -9;
      arrayOfByte1['×'] = -9;
      arrayOfByte1['Ø'] = -9;
      arrayOfByte1['Ù'] = -9;
      arrayOfByte1['Ú'] = -9;
      arrayOfByte1['Û'] = -9;
      arrayOfByte1['Ü'] = -9;
      arrayOfByte1['Ý'] = -9;
      arrayOfByte1['Þ'] = -9;
      arrayOfByte1['ß'] = -9;
      arrayOfByte1['à'] = -9;
      arrayOfByte1['á'] = -9;
      arrayOfByte1['â'] = -9;
      arrayOfByte1['ã'] = -9;
      arrayOfByte1['ä'] = -9;
      arrayOfByte1['å'] = -9;
      arrayOfByte1['æ'] = -9;
      arrayOfByte1['ç'] = -9;
      arrayOfByte1['è'] = -9;
      arrayOfByte1['é'] = -9;
      arrayOfByte1['ê'] = -9;
      arrayOfByte1['ë'] = -9;
      arrayOfByte1['ì'] = -9;
      arrayOfByte1['í'] = -9;
      arrayOfByte1['î'] = -9;
      arrayOfByte1['ï'] = -9;
      arrayOfByte1['ð'] = -9;
      arrayOfByte1['ñ'] = -9;
      arrayOfByte1['ò'] = -9;
      arrayOfByte1['ó'] = -9;
      arrayOfByte1['ô'] = -9;
      arrayOfByte1['õ'] = -9;
      arrayOfByte1['ö'] = -9;
      arrayOfByte1['÷'] = -9;
      arrayOfByte1['ø'] = -9;
      arrayOfByte1['ù'] = -9;
      arrayOfByte1['ú'] = -9;
      arrayOfByte1['û'] = -9;
      arrayOfByte1['ü'] = -9;
      arrayOfByte1['ý'] = -9;
      arrayOfByte1['þ'] = -9;
      arrayOfByte1['ÿ'] = -9;
      c = arrayOfByte1;
      d = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95 };
      byte[] arrayOfByte2 = new byte[256];
      arrayOfByte2[0] = -9;
      arrayOfByte2[1] = -9;
      arrayOfByte2[2] = -9;
      arrayOfByte2[3] = -9;
      arrayOfByte2[4] = -9;
      arrayOfByte2[5] = -9;
      arrayOfByte2[6] = -9;
      arrayOfByte2[7] = -9;
      arrayOfByte2[8] = -9;
      arrayOfByte2[9] = -5;
      arrayOfByte2[10] = -5;
      arrayOfByte2[11] = -9;
      arrayOfByte2[12] = -9;
      arrayOfByte2[13] = -5;
      arrayOfByte2[14] = -9;
      arrayOfByte2[15] = -9;
      arrayOfByte2[16] = -9;
      arrayOfByte2[17] = -9;
      arrayOfByte2[18] = -9;
      arrayOfByte2[19] = -9;
      arrayOfByte2[20] = -9;
      arrayOfByte2[21] = -9;
      arrayOfByte2[22] = -9;
      arrayOfByte2[23] = -9;
      arrayOfByte2[24] = -9;
      arrayOfByte2[25] = -9;
      arrayOfByte2[26] = -9;
      arrayOfByte2[27] = -9;
      arrayOfByte2[28] = -9;
      arrayOfByte2[29] = -9;
      arrayOfByte2[30] = -9;
      arrayOfByte2[31] = -9;
      arrayOfByte2[32] = -5;
      arrayOfByte2[33] = -9;
      arrayOfByte2[34] = -9;
      arrayOfByte2[35] = -9;
      arrayOfByte2[36] = -9;
      arrayOfByte2[37] = -9;
      arrayOfByte2[38] = -9;
      arrayOfByte2[39] = -9;
      arrayOfByte2[40] = -9;
      arrayOfByte2[41] = -9;
      arrayOfByte2[42] = -9;
      arrayOfByte2[43] = -9;
      arrayOfByte2[44] = -9;
      arrayOfByte2[45] = 62;
      arrayOfByte2[46] = -9;
      arrayOfByte2[47] = -9;
      arrayOfByte2[48] = 52;
      arrayOfByte2[49] = 53;
      arrayOfByte2[50] = 54;
      arrayOfByte2[51] = 55;
      arrayOfByte2[52] = 56;
      arrayOfByte2[53] = 57;
      arrayOfByte2[54] = 58;
      arrayOfByte2[55] = 59;
      arrayOfByte2[56] = 60;
      arrayOfByte2[57] = 61;
      arrayOfByte2[58] = -9;
      arrayOfByte2[59] = -9;
      arrayOfByte2[60] = -9;
      arrayOfByte2[61] = -1;
      arrayOfByte2[62] = -9;
      arrayOfByte2[63] = -9;
      arrayOfByte2[64] = -9;
      arrayOfByte2[66] = 1;
      arrayOfByte2[67] = 2;
      arrayOfByte2[68] = 3;
      arrayOfByte2[69] = 4;
      arrayOfByte2[70] = 5;
      arrayOfByte2[71] = 6;
      arrayOfByte2[72] = 7;
      arrayOfByte2[73] = 8;
      arrayOfByte2[74] = 9;
      arrayOfByte2[75] = 10;
      arrayOfByte2[76] = 11;
      arrayOfByte2[77] = 12;
      arrayOfByte2[78] = 13;
      arrayOfByte2[79] = 14;
      arrayOfByte2[80] = 15;
      arrayOfByte2[81] = 16;
      arrayOfByte2[82] = 17;
      arrayOfByte2[83] = 18;
      arrayOfByte2[84] = 19;
      arrayOfByte2[85] = 20;
      arrayOfByte2[86] = 21;
      arrayOfByte2[87] = 22;
      arrayOfByte2[88] = 23;
      arrayOfByte2[89] = 24;
      arrayOfByte2[90] = 25;
      arrayOfByte2[91] = -9;
      arrayOfByte2[92] = -9;
      arrayOfByte2[93] = -9;
      arrayOfByte2[94] = -9;
      arrayOfByte2[95] = 63;
      arrayOfByte2[96] = -9;
      arrayOfByte2[97] = 26;
      arrayOfByte2[98] = 27;
      arrayOfByte2[99] = 28;
      arrayOfByte2[100] = 29;
      arrayOfByte2[101] = 30;
      arrayOfByte2[102] = 31;
      arrayOfByte2[103] = 32;
      arrayOfByte2[104] = 33;
      arrayOfByte2[105] = 34;
      arrayOfByte2[106] = 35;
      arrayOfByte2[107] = 36;
      arrayOfByte2[108] = 37;
      arrayOfByte2[109] = 38;
      arrayOfByte2[110] = 39;
      arrayOfByte2[111] = 40;
      arrayOfByte2[112] = 41;
      arrayOfByte2[113] = 42;
      arrayOfByte2[114] = 43;
      arrayOfByte2[115] = 44;
      arrayOfByte2[116] = 45;
      arrayOfByte2[117] = 46;
      arrayOfByte2[118] = 47;
      arrayOfByte2[119] = 48;
      arrayOfByte2[120] = 49;
      arrayOfByte2[121] = 50;
      arrayOfByte2[122] = 51;
      arrayOfByte2[123] = -9;
      arrayOfByte2[124] = -9;
      arrayOfByte2[125] = -9;
      arrayOfByte2[126] = -9;
      arrayOfByte2[127] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[''] = -9;
      arrayOfByte2[' '] = -9;
      arrayOfByte2['¡'] = -9;
      arrayOfByte2['¢'] = -9;
      arrayOfByte2['£'] = -9;
      arrayOfByte2['¤'] = -9;
      arrayOfByte2['¥'] = -9;
      arrayOfByte2['¦'] = -9;
      arrayOfByte2['§'] = -9;
      arrayOfByte2['¨'] = -9;
      arrayOfByte2['©'] = -9;
      arrayOfByte2['ª'] = -9;
      arrayOfByte2['«'] = -9;
      arrayOfByte2['¬'] = -9;
      arrayOfByte2['­'] = -9;
      arrayOfByte2['®'] = -9;
      arrayOfByte2['¯'] = -9;
      arrayOfByte2['°'] = -9;
      arrayOfByte2['±'] = -9;
      arrayOfByte2['²'] = -9;
      arrayOfByte2['³'] = -9;
      arrayOfByte2['´'] = -9;
      arrayOfByte2['µ'] = -9;
      arrayOfByte2['¶'] = -9;
      arrayOfByte2['·'] = -9;
      arrayOfByte2['¸'] = -9;
      arrayOfByte2['¹'] = -9;
      arrayOfByte2['º'] = -9;
      arrayOfByte2['»'] = -9;
      arrayOfByte2['¼'] = -9;
      arrayOfByte2['½'] = -9;
      arrayOfByte2['¾'] = -9;
      arrayOfByte2['¿'] = -9;
      arrayOfByte2['À'] = -9;
      arrayOfByte2['Á'] = -9;
      arrayOfByte2['Â'] = -9;
      arrayOfByte2['Ã'] = -9;
      arrayOfByte2['Ä'] = -9;
      arrayOfByte2['Å'] = -9;
      arrayOfByte2['Æ'] = -9;
      arrayOfByte2['Ç'] = -9;
      arrayOfByte2['È'] = -9;
      arrayOfByte2['É'] = -9;
      arrayOfByte2['Ê'] = -9;
      arrayOfByte2['Ë'] = -9;
      arrayOfByte2['Ì'] = -9;
      arrayOfByte2['Í'] = -9;
      arrayOfByte2['Î'] = -9;
      arrayOfByte2['Ï'] = -9;
      arrayOfByte2['Ð'] = -9;
      arrayOfByte2['Ñ'] = -9;
      arrayOfByte2['Ò'] = -9;
      arrayOfByte2['Ó'] = -9;
      arrayOfByte2['Ô'] = -9;
      arrayOfByte2['Õ'] = -9;
      arrayOfByte2['Ö'] = -9;
      arrayOfByte2['×'] = -9;
      arrayOfByte2['Ø'] = -9;
      arrayOfByte2['Ù'] = -9;
      arrayOfByte2['Ú'] = -9;
      arrayOfByte2['Û'] = -9;
      arrayOfByte2['Ü'] = -9;
      arrayOfByte2['Ý'] = -9;
      arrayOfByte2['Þ'] = -9;
      arrayOfByte2['ß'] = -9;
      arrayOfByte2['à'] = -9;
      arrayOfByte2['á'] = -9;
      arrayOfByte2['â'] = -9;
      arrayOfByte2['ã'] = -9;
      arrayOfByte2['ä'] = -9;
      arrayOfByte2['å'] = -9;
      arrayOfByte2['æ'] = -9;
      arrayOfByte2['ç'] = -9;
      arrayOfByte2['è'] = -9;
      arrayOfByte2['é'] = -9;
      arrayOfByte2['ê'] = -9;
      arrayOfByte2['ë'] = -9;
      arrayOfByte2['ì'] = -9;
      arrayOfByte2['í'] = -9;
      arrayOfByte2['î'] = -9;
      arrayOfByte2['ï'] = -9;
      arrayOfByte2['ð'] = -9;
      arrayOfByte2['ñ'] = -9;
      arrayOfByte2['ò'] = -9;
      arrayOfByte2['ó'] = -9;
      arrayOfByte2['ô'] = -9;
      arrayOfByte2['õ'] = -9;
      arrayOfByte2['ö'] = -9;
      arrayOfByte2['÷'] = -9;
      arrayOfByte2['ø'] = -9;
      arrayOfByte2['ù'] = -9;
      arrayOfByte2['ú'] = -9;
      arrayOfByte2['û'] = -9;
      arrayOfByte2['ü'] = -9;
      arrayOfByte2['ý'] = -9;
      arrayOfByte2['þ'] = -9;
      arrayOfByte2['ÿ'] = -9;
      e = arrayOfByte2;
      f = new byte[] { 45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122 };
      byte[] arrayOfByte3 = new byte[257];
      arrayOfByte3[0] = -9;
      arrayOfByte3[1] = -9;
      arrayOfByte3[2] = -9;
      arrayOfByte3[3] = -9;
      arrayOfByte3[4] = -9;
      arrayOfByte3[5] = -9;
      arrayOfByte3[6] = -9;
      arrayOfByte3[7] = -9;
      arrayOfByte3[8] = -9;
      arrayOfByte3[9] = -5;
      arrayOfByte3[10] = -5;
      arrayOfByte3[11] = -9;
      arrayOfByte3[12] = -9;
      arrayOfByte3[13] = -5;
      arrayOfByte3[14] = -9;
      arrayOfByte3[15] = -9;
      arrayOfByte3[16] = -9;
      arrayOfByte3[17] = -9;
      arrayOfByte3[18] = -9;
      arrayOfByte3[19] = -9;
      arrayOfByte3[20] = -9;
      arrayOfByte3[21] = -9;
      arrayOfByte3[22] = -9;
      arrayOfByte3[23] = -9;
      arrayOfByte3[24] = -9;
      arrayOfByte3[25] = -9;
      arrayOfByte3[26] = -9;
      arrayOfByte3[27] = -9;
      arrayOfByte3[28] = -9;
      arrayOfByte3[29] = -9;
      arrayOfByte3[30] = -9;
      arrayOfByte3[31] = -9;
      arrayOfByte3[32] = -5;
      arrayOfByte3[33] = -9;
      arrayOfByte3[34] = -9;
      arrayOfByte3[35] = -9;
      arrayOfByte3[36] = -9;
      arrayOfByte3[37] = -9;
      arrayOfByte3[38] = -9;
      arrayOfByte3[39] = -9;
      arrayOfByte3[40] = -9;
      arrayOfByte3[41] = -9;
      arrayOfByte3[42] = -9;
      arrayOfByte3[43] = -9;
      arrayOfByte3[44] = -9;
      arrayOfByte3[46] = -9;
      arrayOfByte3[47] = -9;
      arrayOfByte3[48] = 1;
      arrayOfByte3[49] = 2;
      arrayOfByte3[50] = 3;
      arrayOfByte3[51] = 4;
      arrayOfByte3[52] = 5;
      arrayOfByte3[53] = 6;
      arrayOfByte3[54] = 7;
      arrayOfByte3[55] = 8;
      arrayOfByte3[56] = 9;
      arrayOfByte3[57] = 10;
      arrayOfByte3[58] = -9;
      arrayOfByte3[59] = -9;
      arrayOfByte3[60] = -9;
      arrayOfByte3[61] = -1;
      arrayOfByte3[62] = -9;
      arrayOfByte3[63] = -9;
      arrayOfByte3[64] = -9;
      arrayOfByte3[65] = 11;
      arrayOfByte3[66] = 12;
      arrayOfByte3[67] = 13;
      arrayOfByte3[68] = 14;
      arrayOfByte3[69] = 15;
      arrayOfByte3[70] = 16;
      arrayOfByte3[71] = 17;
      arrayOfByte3[72] = 18;
      arrayOfByte3[73] = 19;
      arrayOfByte3[74] = 20;
      arrayOfByte3[75] = 21;
      arrayOfByte3[76] = 22;
      arrayOfByte3[77] = 23;
      arrayOfByte3[78] = 24;
      arrayOfByte3[79] = 25;
      arrayOfByte3[80] = 26;
      arrayOfByte3[81] = 27;
      arrayOfByte3[82] = 28;
      arrayOfByte3[83] = 29;
      arrayOfByte3[84] = 30;
      arrayOfByte3[85] = 31;
      arrayOfByte3[86] = 32;
      arrayOfByte3[87] = 33;
      arrayOfByte3[88] = 34;
      arrayOfByte3[89] = 35;
      arrayOfByte3[90] = 36;
      arrayOfByte3[91] = -9;
      arrayOfByte3[92] = -9;
      arrayOfByte3[93] = -9;
      arrayOfByte3[94] = -9;
      arrayOfByte3[95] = 37;
      arrayOfByte3[96] = -9;
      arrayOfByte3[97] = 38;
      arrayOfByte3[98] = 39;
      arrayOfByte3[99] = 40;
      arrayOfByte3[100] = 41;
      arrayOfByte3[101] = 42;
      arrayOfByte3[102] = 43;
      arrayOfByte3[103] = 44;
      arrayOfByte3[104] = 45;
      arrayOfByte3[105] = 46;
      arrayOfByte3[106] = 47;
      arrayOfByte3[107] = 48;
      arrayOfByte3[108] = 49;
      arrayOfByte3[109] = 50;
      arrayOfByte3[110] = 51;
      arrayOfByte3[111] = 52;
      arrayOfByte3[112] = 53;
      arrayOfByte3[113] = 54;
      arrayOfByte3[114] = 55;
      arrayOfByte3[115] = 56;
      arrayOfByte3[116] = 57;
      arrayOfByte3[117] = 58;
      arrayOfByte3[118] = 59;
      arrayOfByte3[119] = 60;
      arrayOfByte3[120] = 61;
      arrayOfByte3[121] = 62;
      arrayOfByte3[122] = 63;
      arrayOfByte3[123] = -9;
      arrayOfByte3[124] = -9;
      arrayOfByte3[125] = -9;
      arrayOfByte3[126] = -9;
      arrayOfByte3[127] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[''] = -9;
      arrayOfByte3[' '] = -9;
      arrayOfByte3['¡'] = -9;
      arrayOfByte3['¢'] = -9;
      arrayOfByte3['£'] = -9;
      arrayOfByte3['¤'] = -9;
      arrayOfByte3['¥'] = -9;
      arrayOfByte3['¦'] = -9;
      arrayOfByte3['§'] = -9;
      arrayOfByte3['¨'] = -9;
      arrayOfByte3['©'] = -9;
      arrayOfByte3['ª'] = -9;
      arrayOfByte3['«'] = -9;
      arrayOfByte3['¬'] = -9;
      arrayOfByte3['­'] = -9;
      arrayOfByte3['®'] = -9;
      arrayOfByte3['¯'] = -9;
      arrayOfByte3['°'] = -9;
      arrayOfByte3['±'] = -9;
      arrayOfByte3['²'] = -9;
      arrayOfByte3['³'] = -9;
      arrayOfByte3['´'] = -9;
      arrayOfByte3['µ'] = -9;
      arrayOfByte3['¶'] = -9;
      arrayOfByte3['·'] = -9;
      arrayOfByte3['¸'] = -9;
      arrayOfByte3['¹'] = -9;
      arrayOfByte3['º'] = -9;
      arrayOfByte3['»'] = -9;
      arrayOfByte3['¼'] = -9;
      arrayOfByte3['½'] = -9;
      arrayOfByte3['¾'] = -9;
      arrayOfByte3['¿'] = -9;
      arrayOfByte3['À'] = -9;
      arrayOfByte3['Á'] = -9;
      arrayOfByte3['Â'] = -9;
      arrayOfByte3['Ã'] = -9;
      arrayOfByte3['Ä'] = -9;
      arrayOfByte3['Å'] = -9;
      arrayOfByte3['Æ'] = -9;
      arrayOfByte3['Ç'] = -9;
      arrayOfByte3['È'] = -9;
      arrayOfByte3['É'] = -9;
      arrayOfByte3['Ê'] = -9;
      arrayOfByte3['Ë'] = -9;
      arrayOfByte3['Ì'] = -9;
      arrayOfByte3['Í'] = -9;
      arrayOfByte3['Î'] = -9;
      arrayOfByte3['Ï'] = -9;
      arrayOfByte3['Ð'] = -9;
      arrayOfByte3['Ñ'] = -9;
      arrayOfByte3['Ò'] = -9;
      arrayOfByte3['Ó'] = -9;
      arrayOfByte3['Ô'] = -9;
      arrayOfByte3['Õ'] = -9;
      arrayOfByte3['Ö'] = -9;
      arrayOfByte3['×'] = -9;
      arrayOfByte3['Ø'] = -9;
      arrayOfByte3['Ù'] = -9;
      arrayOfByte3['Ú'] = -9;
      arrayOfByte3['Û'] = -9;
      arrayOfByte3['Ü'] = -9;
      arrayOfByte3['Ý'] = -9;
      arrayOfByte3['Þ'] = -9;
      arrayOfByte3['ß'] = -9;
      arrayOfByte3['à'] = -9;
      arrayOfByte3['á'] = -9;
      arrayOfByte3['â'] = -9;
      arrayOfByte3['ã'] = -9;
      arrayOfByte3['ä'] = -9;
      arrayOfByte3['å'] = -9;
      arrayOfByte3['æ'] = -9;
      arrayOfByte3['ç'] = -9;
      arrayOfByte3['è'] = -9;
      arrayOfByte3['é'] = -9;
      arrayOfByte3['ê'] = -9;
      arrayOfByte3['ë'] = -9;
      arrayOfByte3['ì'] = -9;
      arrayOfByte3['í'] = -9;
      arrayOfByte3['î'] = -9;
      arrayOfByte3['ï'] = -9;
      arrayOfByte3['ð'] = -9;
      arrayOfByte3['ñ'] = -9;
      arrayOfByte3['ò'] = -9;
      arrayOfByte3['ó'] = -9;
      arrayOfByte3['ô'] = -9;
      arrayOfByte3['õ'] = -9;
      arrayOfByte3['ö'] = -9;
      arrayOfByte3['÷'] = -9;
      arrayOfByte3['ø'] = -9;
      arrayOfByte3['ù'] = -9;
      arrayOfByte3['ú'] = -9;
      arrayOfByte3['û'] = -9;
      arrayOfByte3['ü'] = -9;
      arrayOfByte3['ý'] = -9;
      arrayOfByte3['þ'] = -9;
      arrayOfByte3['ÿ'] = -9;
      arrayOfByte3[256] = -9;
      g = arrayOfByte3;
      return;
    }
  }

  private static int a(byte[] paramArrayOfByte1, int paramInt1, byte[] paramArrayOfByte2, int paramInt2, int paramInt3)
  {
    if (paramArrayOfByte1 == null)
      throw new NullPointerException("Source array was null.");
    if (paramArrayOfByte2 == null)
      throw new NullPointerException("Destination array was null.");
    if (3 >= paramArrayOfByte1.length)
    {
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = Integer.valueOf(paramArrayOfByte1.length);
      arrayOfObject2[1] = Integer.valueOf(0);
      throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", arrayOfObject2));
    }
    if ((paramInt2 < 0) || (paramInt2 + 2 >= paramArrayOfByte2.length))
    {
      Object[] arrayOfObject1 = new Object[2];
      arrayOfObject1[0] = Integer.valueOf(paramArrayOfByte2.length);
      arrayOfObject1[1] = Integer.valueOf(paramInt2);
      throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", arrayOfObject1));
    }
    byte[] arrayOfByte = c(paramInt3);
    if (paramArrayOfByte1[2] == 61)
    {
      paramArrayOfByte2[paramInt2] = ((byte)(((0xFF & arrayOfByte[paramArrayOfByte1[0]]) << 18 | (0xFF & arrayOfByte[paramArrayOfByte1[1]]) << 12) >>> 16));
      return 1;
    }
    if (paramArrayOfByte1[3] == 61)
    {
      int j = (0xFF & arrayOfByte[paramArrayOfByte1[0]]) << 18 | (0xFF & arrayOfByte[paramArrayOfByte1[1]]) << 12 | (0xFF & arrayOfByte[paramArrayOfByte1[2]]) << 6;
      paramArrayOfByte2[paramInt2] = ((byte)(j >>> 16));
      paramArrayOfByte2[(paramInt2 + 1)] = ((byte)(j >>> 8));
      return 2;
    }
    int i = (0xFF & arrayOfByte[paramArrayOfByte1[0]]) << 18 | (0xFF & arrayOfByte[paramArrayOfByte1[1]]) << 12 | (0xFF & arrayOfByte[paramArrayOfByte1[2]]) << 6 | 0xFF & arrayOfByte[paramArrayOfByte1[3]];
    paramArrayOfByte2[paramInt2] = ((byte)(i >> 16));
    paramArrayOfByte2[(paramInt2 + 1)] = ((byte)(i >> 8));
    paramArrayOfByte2[(paramInt2 + 2)] = ((byte)i);
    return 3;
  }

  private static byte[] a(byte[] paramArrayOfByte1, int paramInt1, int paramInt2, byte[] paramArrayOfByte2, int paramInt3, int paramInt4)
  {
    byte[] arrayOfByte = b(paramInt4);
    int i;
    if (paramInt2 > 0)
    {
      i = paramArrayOfByte1[paramInt1] << 24 >>> 8;
      label22: if (paramInt2 <= 1)
        break label112;
    }
    int n;
    label112: for (int j = paramArrayOfByte1[(paramInt1 + 1)] << 24 >>> 16; ; j = 0)
    {
      int k = j | i;
      int m = 0;
      if (paramInt2 > 2)
        m = paramArrayOfByte1[(paramInt1 + 2)] << 24 >>> 24;
      n = m | k;
      switch (paramInt2)
      {
      default:
        return paramArrayOfByte2;
        i = 0;
        break label22;
      case 3:
      case 2:
      case 1:
      }
    }
    paramArrayOfByte2[paramInt3] = arrayOfByte[(n >>> 18)];
    paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(0x3F & n >>> 12)];
    paramArrayOfByte2[(paramInt3 + 2)] = arrayOfByte[(0x3F & n >>> 6)];
    paramArrayOfByte2[(paramInt3 + 3)] = arrayOfByte[(n & 0x3F)];
    return paramArrayOfByte2;
    paramArrayOfByte2[paramInt3] = arrayOfByte[(n >>> 18)];
    paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(0x3F & n >>> 12)];
    paramArrayOfByte2[(paramInt3 + 2)] = arrayOfByte[(0x3F & n >>> 6)];
    paramArrayOfByte2[(paramInt3 + 3)] = 61;
    return paramArrayOfByte2;
    paramArrayOfByte2[paramInt3] = arrayOfByte[(n >>> 18)];
    paramArrayOfByte2[(paramInt3 + 1)] = arrayOfByte[(0x3F & n >>> 12)];
    paramArrayOfByte2[(paramInt3 + 2)] = 61;
    paramArrayOfByte2[(paramInt3 + 3)] = 61;
    return paramArrayOfByte2;
  }

  private static final byte[] b(int paramInt)
  {
    if ((paramInt & 0x10) == 16)
      return d;
    if ((paramInt & 0x20) == 32)
      return f;
    return b;
  }

  private static byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2, int paramInt1, int paramInt2)
  {
    a(paramArrayOfByte2, 0, paramInt1, paramArrayOfByte1, 0, paramInt2);
    return paramArrayOfByte1;
  }

  private static final byte[] c(int paramInt)
  {
    if ((paramInt & 0x10) == 16)
      return e;
    if ((paramInt & 0x20) == 32)
      return g;
    return c;
  }

  public static byte[] decode(String paramString)
  {
    return decode(paramString, 0);
  }

  // ERROR //
  public static byte[] decode(String paramString, int paramInt)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: ifnonnull +13 -> 16
    //   6: new 123	java/lang/NullPointerException
    //   9: dup
    //   10: ldc 177
    //   12: invokespecial 128	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   15: athrow
    //   16: aload_0
    //   17: ldc 179
    //   19: invokevirtual 182	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   22: astore 25
    //   24: aload 25
    //   26: astore 4
    //   28: aload 4
    //   30: iconst_0
    //   31: aload 4
    //   33: arraylength
    //   34: iload_1
    //   35: invokestatic 185	gbis/gbandroid/utils/Base64:decode	([BIII)[B
    //   38: astore 5
    //   40: iload_1
    //   41: iconst_4
    //   42: iand
    //   43: ifeq +138 -> 181
    //   46: iconst_1
    //   47: istore 6
    //   49: aload 5
    //   51: ifnull +117 -> 168
    //   54: aload 5
    //   56: arraylength
    //   57: iconst_4
    //   58: if_icmplt +110 -> 168
    //   61: iload 6
    //   63: ifne +105 -> 168
    //   66: ldc 186
    //   68: sipush 255
    //   71: aload 5
    //   73: iconst_0
    //   74: baload
    //   75: iand
    //   76: ldc 187
    //   78: aload 5
    //   80: iconst_1
    //   81: baload
    //   82: bipush 8
    //   84: ishl
    //   85: iand
    //   86: ior
    //   87: if_icmpne +81 -> 168
    //   90: sipush 2048
    //   93: newarray byte
    //   95: astore 7
    //   97: new 189	java/io/ByteArrayOutputStream
    //   100: dup
    //   101: invokespecial 190	java/io/ByteArrayOutputStream:<init>	()V
    //   104: astore 8
    //   106: new 192	java/io/ByteArrayInputStream
    //   109: dup
    //   110: aload 5
    //   112: invokespecial 195	java/io/ByteArrayInputStream:<init>	([B)V
    //   115: astore 9
    //   117: new 197	java/util/zip/GZIPInputStream
    //   120: dup
    //   121: aload 9
    //   123: invokespecial 200	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   126: astore 10
    //   128: aload 10
    //   130: aload 7
    //   132: invokevirtual 204	java/util/zip/GZIPInputStream:read	([B)I
    //   135: istore 20
    //   137: iload 20
    //   139: ifge +48 -> 187
    //   142: aload 8
    //   144: invokevirtual 208	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   147: astore 21
    //   149: aload 21
    //   151: astore 5
    //   153: aload 8
    //   155: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   158: aload 10
    //   160: invokevirtual 212	java/util/zip/GZIPInputStream:close	()V
    //   163: aload 9
    //   165: invokevirtual 213	java/io/ByteArrayInputStream:close	()V
    //   168: aload 5
    //   170: areturn
    //   171: astore_3
    //   172: aload_0
    //   173: invokevirtual 215	java/lang/String:getBytes	()[B
    //   176: astore 4
    //   178: goto -150 -> 28
    //   181: iconst_0
    //   182: istore 6
    //   184: goto -135 -> 49
    //   187: aload 8
    //   189: aload 7
    //   191: iconst_0
    //   192: iload 20
    //   194: invokevirtual 219	java/io/ByteArrayOutputStream:write	([BII)V
    //   197: goto -69 -> 128
    //   200: astore 15
    //   202: aload 10
    //   204: astore_2
    //   205: aload 9
    //   207: astore 16
    //   209: aload 15
    //   211: invokevirtual 222	java/io/IOException:printStackTrace	()V
    //   214: aload 8
    //   216: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   219: aload_2
    //   220: invokevirtual 212	java/util/zip/GZIPInputStream:close	()V
    //   223: aload 16
    //   225: invokevirtual 213	java/io/ByteArrayInputStream:close	()V
    //   228: aload 5
    //   230: areturn
    //   231: astore 19
    //   233: aload 5
    //   235: areturn
    //   236: astore 11
    //   238: aconst_null
    //   239: astore 8
    //   241: aconst_null
    //   242: astore 9
    //   244: aload 8
    //   246: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   249: aload_2
    //   250: invokevirtual 212	java/util/zip/GZIPInputStream:close	()V
    //   253: aload 9
    //   255: invokevirtual 213	java/io/ByteArrayInputStream:close	()V
    //   258: aload 11
    //   260: athrow
    //   261: astore 17
    //   263: goto -44 -> 219
    //   266: astore 18
    //   268: goto -45 -> 223
    //   271: astore 12
    //   273: goto -24 -> 249
    //   276: astore 13
    //   278: goto -25 -> 253
    //   281: astore 14
    //   283: goto -25 -> 258
    //   286: astore 22
    //   288: goto -130 -> 158
    //   291: astore 23
    //   293: goto -130 -> 163
    //   296: astore 24
    //   298: aload 5
    //   300: areturn
    //   301: astore 11
    //   303: aconst_null
    //   304: astore_2
    //   305: aconst_null
    //   306: astore 9
    //   308: goto -64 -> 244
    //   311: astore 11
    //   313: aconst_null
    //   314: astore_2
    //   315: goto -71 -> 244
    //   318: astore 11
    //   320: aload 10
    //   322: astore_2
    //   323: goto -79 -> 244
    //   326: astore 11
    //   328: aload 16
    //   330: astore 9
    //   332: goto -88 -> 244
    //   335: astore 15
    //   337: aconst_null
    //   338: astore 8
    //   340: aconst_null
    //   341: astore_2
    //   342: aconst_null
    //   343: astore 16
    //   345: goto -136 -> 209
    //   348: astore 15
    //   350: aconst_null
    //   351: astore_2
    //   352: aconst_null
    //   353: astore 16
    //   355: goto -146 -> 209
    //   358: astore 15
    //   360: aload 9
    //   362: astore 16
    //   364: aconst_null
    //   365: astore_2
    //   366: goto -157 -> 209
    //
    // Exception table:
    //   from	to	target	type
    //   16	24	171	java/io/UnsupportedEncodingException
    //   128	137	200	java/io/IOException
    //   142	149	200	java/io/IOException
    //   187	197	200	java/io/IOException
    //   223	228	231	java/lang/Exception
    //   97	106	236	finally
    //   214	219	261	java/lang/Exception
    //   219	223	266	java/lang/Exception
    //   244	249	271	java/lang/Exception
    //   249	253	276	java/lang/Exception
    //   253	258	281	java/lang/Exception
    //   153	158	286	java/lang/Exception
    //   158	163	291	java/lang/Exception
    //   163	168	296	java/lang/Exception
    //   106	117	301	finally
    //   117	128	311	finally
    //   128	137	318	finally
    //   142	149	318	finally
    //   187	197	318	finally
    //   209	214	326	finally
    //   97	106	335	java/io/IOException
    //   106	117	348	java/io/IOException
    //   117	128	358	java/io/IOException
  }

  public static byte[] decode(byte[] paramArrayOfByte)
  {
    return decode(paramArrayOfByte, 0, paramArrayOfByte.length, 0);
  }

  public static byte[] decode(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramArrayOfByte == null)
      throw new NullPointerException("Cannot decode null source array.");
    if ((paramInt1 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length))
    {
      Object[] arrayOfObject1 = new Object[3];
      arrayOfObject1[0] = Integer.valueOf(paramArrayOfByte.length);
      arrayOfObject1[1] = Integer.valueOf(paramInt1);
      arrayOfObject1[2] = Integer.valueOf(paramInt2);
      throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", arrayOfObject1));
    }
    if (paramInt2 == 0)
      return new byte[0];
    if (paramInt2 < 4)
      throw new IllegalArgumentException("Base64-encoded string must have at least four characters, but length specified was " + paramInt2);
    byte[] arrayOfByte1 = c(paramInt3);
    byte[] arrayOfByte2 = new byte[paramInt2 * 3 / 4];
    byte[] arrayOfByte3 = new byte[4];
    int i = paramInt1;
    int j = 0;
    int k = 0;
    int i2;
    if (i >= paramInt1 + paramInt2)
      i2 = k;
    do
    {
      byte[] arrayOfByte4 = new byte[i2];
      System.arraycopy(arrayOfByte2, 0, arrayOfByte4, 0, i2);
      return arrayOfByte4;
      int m = arrayOfByte1[(0xFF & paramArrayOfByte[i])];
      if (m < -5)
        break;
      if (m < -1)
        break label315;
      n = j + 1;
      arrayOfByte3[j] = paramArrayOfByte[i];
      if (n <= 3)
        break label308;
      i2 = k + a(arrayOfByte3, 0, arrayOfByte2, k, paramInt3);
    }
    while (paramArrayOfByte[i] == 61);
    int i1 = i2;
    int n = 0;
    while (true)
    {
      i++;
      k = i1;
      j = n;
      break;
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = Integer.valueOf(0xFF & paramArrayOfByte[i]);
      arrayOfObject2[1] = Integer.valueOf(i);
      throw new IOException(String.format("Bad Base64 input character decimal %d in array position %d", arrayOfObject2));
      label308: i1 = k;
      continue;
      label315: n = j;
      i1 = k;
    }
  }

  // ERROR //
  public static void decodeFileToFile(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 254	gbis/gbandroid/utils/Base64:decodeFromFile	(Ljava/lang/String;)[B
    //   4: astore_2
    //   5: new 256	java/io/BufferedOutputStream
    //   8: dup
    //   9: new 258	java/io/FileOutputStream
    //   12: dup
    //   13: aload_1
    //   14: invokespecial 259	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   17: invokespecial 262	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   20: astore_3
    //   21: aload_3
    //   22: aload_2
    //   23: invokevirtual 266	java/io/OutputStream:write	([B)V
    //   26: aload_3
    //   27: invokevirtual 267	java/io/OutputStream:close	()V
    //   30: return
    //   31: astore 4
    //   33: aconst_null
    //   34: astore_3
    //   35: aload 4
    //   37: athrow
    //   38: astore 5
    //   40: aload_3
    //   41: astore 6
    //   43: aload 6
    //   45: invokevirtual 267	java/io/OutputStream:close	()V
    //   48: aload 5
    //   50: athrow
    //   51: astore 7
    //   53: goto -5 -> 48
    //   56: astore 8
    //   58: return
    //   59: astore 5
    //   61: aconst_null
    //   62: astore 6
    //   64: goto -21 -> 43
    //   67: astore 4
    //   69: goto -34 -> 35
    //
    // Exception table:
    //   from	to	target	type
    //   5	21	31	java/io/IOException
    //   21	26	38	finally
    //   35	38	38	finally
    //   43	48	51	java/lang/Exception
    //   26	30	56	java/lang/Exception
    //   5	21	59	finally
    //   21	26	67	java/io/IOException
  }

  // ERROR //
  public static byte[] decodeFromFile(String paramString)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aconst_null
    //   3: astore_2
    //   4: new 269	java/io/File
    //   7: dup
    //   8: aload_0
    //   9: invokespecial 270	java/io/File:<init>	(Ljava/lang/String;)V
    //   12: astore_3
    //   13: aload_3
    //   14: invokevirtual 274	java/io/File:length	()J
    //   17: ldc2_w 275
    //   20: lcmp
    //   21: istore 7
    //   23: aconst_null
    //   24: astore_2
    //   25: iload 7
    //   27: ifle +51 -> 78
    //   30: new 173	java/io/IOException
    //   33: dup
    //   34: new 229	java/lang/StringBuilder
    //   37: dup
    //   38: ldc_w 278
    //   41: invokespecial 232	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   44: aload_3
    //   45: invokevirtual 274	java/io/File:length	()J
    //   48: invokevirtual 281	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   51: ldc_w 283
    //   54: invokevirtual 286	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: invokevirtual 240	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: invokespecial 249	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   63: athrow
    //   64: astore 6
    //   66: aload 6
    //   68: athrow
    //   69: astore 4
    //   71: aload_2
    //   72: invokevirtual 289	gbis/gbandroid/utils/Base64$InputStream:close	()V
    //   75: aload 4
    //   77: athrow
    //   78: aload_3
    //   79: invokevirtual 274	java/io/File:length	()J
    //   82: l2i
    //   83: newarray byte
    //   85: astore 8
    //   87: new 288	gbis/gbandroid/utils/Base64$InputStream
    //   90: dup
    //   91: new 291	java/io/BufferedInputStream
    //   94: dup
    //   95: new 293	java/io/FileInputStream
    //   98: dup
    //   99: aload_3
    //   100: invokespecial 296	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   103: invokespecial 297	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   106: iconst_0
    //   107: invokespecial 300	gbis/gbandroid/utils/Base64$InputStream:<init>	(Ljava/io/InputStream;I)V
    //   110: astore 9
    //   112: aload 9
    //   114: aload 8
    //   116: iload_1
    //   117: sipush 4096
    //   120: invokevirtual 303	gbis/gbandroid/utils/Base64$InputStream:read	([BII)I
    //   123: istore 10
    //   125: iload 10
    //   127: ifge +26 -> 153
    //   130: iload_1
    //   131: newarray byte
    //   133: astore 11
    //   135: aload 8
    //   137: iconst_0
    //   138: aload 11
    //   140: iconst_0
    //   141: iload_1
    //   142: invokestatic 246	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
    //   145: aload 9
    //   147: invokevirtual 289	gbis/gbandroid/utils/Base64$InputStream:close	()V
    //   150: aload 11
    //   152: areturn
    //   153: iload_1
    //   154: iload 10
    //   156: iadd
    //   157: istore_1
    //   158: goto -46 -> 112
    //   161: astore 5
    //   163: goto -88 -> 75
    //   166: astore 12
    //   168: aload 11
    //   170: areturn
    //   171: astore 4
    //   173: aload 9
    //   175: astore_2
    //   176: goto -105 -> 71
    //   179: astore 6
    //   181: aload 9
    //   183: astore_2
    //   184: goto -118 -> 66
    //
    // Exception table:
    //   from	to	target	type
    //   4	23	64	java/io/IOException
    //   30	64	64	java/io/IOException
    //   78	112	64	java/io/IOException
    //   4	23	69	finally
    //   30	64	69	finally
    //   66	69	69	finally
    //   78	112	69	finally
    //   71	75	161	java/lang/Exception
    //   145	150	166	java/lang/Exception
    //   112	125	171	finally
    //   130	145	171	finally
    //   112	125	179	java/io/IOException
    //   130	145	179	java/io/IOException
  }

  // ERROR //
  public static void decodeToFile(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: new 306	gbis/gbandroid/utils/Base64$OutputStream
    //   3: dup
    //   4: new 258	java/io/FileOutputStream
    //   7: dup
    //   8: aload_1
    //   9: invokespecial 259	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   12: iconst_0
    //   13: invokespecial 309	gbis/gbandroid/utils/Base64$OutputStream:<init>	(Ljava/io/OutputStream;I)V
    //   16: astore_2
    //   17: aload_2
    //   18: aload_0
    //   19: ldc 179
    //   21: invokevirtual 182	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   24: invokevirtual 310	gbis/gbandroid/utils/Base64$OutputStream:write	([B)V
    //   27: aload_2
    //   28: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   31: return
    //   32: astore_3
    //   33: aconst_null
    //   34: astore_2
    //   35: aload_3
    //   36: athrow
    //   37: astore 4
    //   39: aload_2
    //   40: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   43: aload 4
    //   45: athrow
    //   46: astore 5
    //   48: goto -5 -> 43
    //   51: astore 6
    //   53: return
    //   54: astore 4
    //   56: aconst_null
    //   57: astore_2
    //   58: goto -19 -> 39
    //   61: astore_3
    //   62: goto -27 -> 35
    //
    // Exception table:
    //   from	to	target	type
    //   0	17	32	java/io/IOException
    //   17	27	37	finally
    //   35	37	37	finally
    //   39	43	46	java/lang/Exception
    //   27	31	51	java/lang/Exception
    //   0	17	54	finally
    //   17	27	61	java/io/IOException
  }

  public static Object decodeToObject(String paramString)
  {
    return decodeToObject(paramString, 0, null);
  }

  // ERROR //
  public static Object decodeToObject(String paramString, int paramInt, java.lang.ClassLoader paramClassLoader)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: iload_1
    //   4: invokestatic 169	gbis/gbandroid/utils/Base64:decode	(Ljava/lang/String;I)[B
    //   7: astore 4
    //   9: new 192	java/io/ByteArrayInputStream
    //   12: dup
    //   13: aload 4
    //   15: invokespecial 195	java/io/ByteArrayInputStream:<init>	([B)V
    //   18: astore 5
    //   20: aload_2
    //   21: ifnonnull +31 -> 52
    //   24: new 320	java/io/ObjectInputStream
    //   27: dup
    //   28: aload 5
    //   30: invokespecial 321	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   33: astore_3
    //   34: aload_3
    //   35: invokevirtual 325	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   38: astore 12
    //   40: aload 5
    //   42: invokevirtual 213	java/io/ByteArrayInputStream:close	()V
    //   45: aload_3
    //   46: invokevirtual 326	java/io/ObjectInputStream:close	()V
    //   49: aload 12
    //   51: areturn
    //   52: new 328	gbis/gbandroid/utils/a
    //   55: dup
    //   56: aload 5
    //   58: aload_2
    //   59: invokespecial 331	gbis/gbandroid/utils/a:<init>	(Ljava/io/InputStream;Ljava/lang/ClassLoader;)V
    //   62: astore 6
    //   64: aload 6
    //   66: astore_3
    //   67: goto -33 -> 34
    //   70: astore 7
    //   72: aconst_null
    //   73: astore 5
    //   75: aload 7
    //   77: athrow
    //   78: astore 8
    //   80: aload 5
    //   82: invokevirtual 213	java/io/ByteArrayInputStream:close	()V
    //   85: aload_3
    //   86: invokevirtual 326	java/io/ObjectInputStream:close	()V
    //   89: aload 8
    //   91: athrow
    //   92: astore 11
    //   94: aconst_null
    //   95: astore 5
    //   97: aload 11
    //   99: athrow
    //   100: astore 9
    //   102: goto -17 -> 85
    //   105: astore 10
    //   107: goto -18 -> 89
    //   110: astore 13
    //   112: goto -67 -> 45
    //   115: astore 14
    //   117: aload 12
    //   119: areturn
    //   120: astore 8
    //   122: aconst_null
    //   123: astore_3
    //   124: aconst_null
    //   125: astore 5
    //   127: goto -47 -> 80
    //   130: astore 11
    //   132: goto -35 -> 97
    //   135: astore 7
    //   137: goto -62 -> 75
    //
    // Exception table:
    //   from	to	target	type
    //   9	20	70	java/io/IOException
    //   24	34	78	finally
    //   34	40	78	finally
    //   52	64	78	finally
    //   75	78	78	finally
    //   97	100	78	finally
    //   9	20	92	java/lang/ClassNotFoundException
    //   80	85	100	java/lang/Exception
    //   85	89	105	java/lang/Exception
    //   40	45	110	java/lang/Exception
    //   45	49	115	java/lang/Exception
    //   9	20	120	finally
    //   24	34	130	java/lang/ClassNotFoundException
    //   34	40	130	java/lang/ClassNotFoundException
    //   52	64	130	java/lang/ClassNotFoundException
    //   24	34	135	java/io/IOException
    //   34	40	135	java/io/IOException
    //   52	64	135	java/io/IOException
  }

  public static void encode(ByteBuffer paramByteBuffer1, ByteBuffer paramByteBuffer2)
  {
    byte[] arrayOfByte1 = new byte[3];
    byte[] arrayOfByte2 = new byte[4];
    while (true)
    {
      if (!paramByteBuffer1.hasRemaining())
        return;
      int i = Math.min(3, paramByteBuffer1.remaining());
      paramByteBuffer1.get(arrayOfByte1, 0, i);
      b(arrayOfByte2, arrayOfByte1, i, 0);
      paramByteBuffer2.put(arrayOfByte2);
    }
  }

  public static void encode(ByteBuffer paramByteBuffer, CharBuffer paramCharBuffer)
  {
    byte[] arrayOfByte1 = new byte[3];
    byte[] arrayOfByte2 = new byte[4];
    while (true)
    {
      if (!paramByteBuffer.hasRemaining())
        return;
      int i = Math.min(3, paramByteBuffer.remaining());
      paramByteBuffer.get(arrayOfByte1, 0, i);
      b(arrayOfByte2, arrayOfByte1, i, 0);
      for (int j = 0; j < 4; j++)
        paramCharBuffer.put((char)(0xFF & arrayOfByte2[j]));
    }
  }

  public static String encodeBytes(byte[] paramArrayOfByte)
  {
    String str1;
    try
    {
      String str2 = encodeBytes(paramArrayOfByte, 0, paramArrayOfByte.length, 0);
      str1 = str2;
      if ((!a) && (str1 == null))
        throw new AssertionError();
    }
    catch (IOException localIOException)
    {
      boolean bool;
      do
      {
        bool = a;
        str1 = null;
      }
      while (bool);
      throw new AssertionError(localIOException.getMessage());
    }
    return str1;
  }

  public static String encodeBytes(byte[] paramArrayOfByte, int paramInt)
  {
    return encodeBytes(paramArrayOfByte, 0, paramArrayOfByte.length, paramInt);
  }

  public static String encodeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    String str1;
    try
    {
      String str2 = encodeBytes(paramArrayOfByte, paramInt1, paramInt2, 0);
      str1 = str2;
      if ((!a) && (str1 == null))
        throw new AssertionError();
    }
    catch (IOException localIOException)
    {
      boolean bool;
      do
      {
        bool = a;
        str1 = null;
      }
      while (bool);
      throw new AssertionError(localIOException.getMessage());
    }
    return str1;
  }

  public static String encodeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    byte[] arrayOfByte = encodeBytesToBytes(paramArrayOfByte, paramInt1, paramInt2, paramInt3);
    try
    {
      String str = new String(arrayOfByte, "US-ASCII");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
    }
    return new String(arrayOfByte);
  }

  public static byte[] encodeBytesToBytes(byte[] paramArrayOfByte)
  {
    try
    {
      byte[] arrayOfByte2 = encodeBytesToBytes(paramArrayOfByte, 0, paramArrayOfByte.length, 0);
      arrayOfByte1 = arrayOfByte2;
      return arrayOfByte1;
    }
    catch (IOException localIOException)
    {
      boolean bool;
      do
      {
        bool = a;
        byte[] arrayOfByte1 = null;
      }
      while (bool);
      throw new AssertionError("IOExceptions only come from GZipping, which is turned off: " + localIOException.getMessage());
    }
  }

  // ERROR //
  public static byte[] encodeBytesToBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: ifnonnull +14 -> 18
    //   7: new 123	java/lang/NullPointerException
    //   10: dup
    //   11: ldc_w 389
    //   14: invokespecial 128	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   17: athrow
    //   18: iload_1
    //   19: ifge +28 -> 47
    //   22: new 138	java/lang/IllegalArgumentException
    //   25: dup
    //   26: new 229	java/lang/StringBuilder
    //   29: dup
    //   30: ldc_w 391
    //   33: invokespecial 232	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   36: iload_1
    //   37: invokevirtual 236	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   40: invokevirtual 240	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   43: invokespecial 147	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   46: athrow
    //   47: iload_2
    //   48: ifge +28 -> 76
    //   51: new 138	java/lang/IllegalArgumentException
    //   54: dup
    //   55: new 229	java/lang/StringBuilder
    //   58: dup
    //   59: ldc_w 393
    //   62: invokespecial 232	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   65: iload_2
    //   66: invokevirtual 236	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   69: invokevirtual 240	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   72: invokespecial 147	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   75: athrow
    //   76: iload_1
    //   77: iload_2
    //   78: iadd
    //   79: aload_0
    //   80: arraylength
    //   81: if_icmple +50 -> 131
    //   84: iconst_3
    //   85: anewarray 4	java/lang/Object
    //   88: astore 31
    //   90: aload 31
    //   92: iconst_0
    //   93: iload_1
    //   94: invokestatic 136	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   97: aastore
    //   98: aload 31
    //   100: iconst_1
    //   101: iload_2
    //   102: invokestatic 136	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   105: aastore
    //   106: aload 31
    //   108: iconst_2
    //   109: aload_0
    //   110: arraylength
    //   111: invokestatic 136	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   114: aastore
    //   115: new 138	java/lang/IllegalArgumentException
    //   118: dup
    //   119: ldc_w 395
    //   122: aload 31
    //   124: invokestatic 146	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   127: invokespecial 147	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   130: athrow
    //   131: iload_3
    //   132: iconst_2
    //   133: iand
    //   134: ifeq +110 -> 244
    //   137: new 189	java/io/ByteArrayOutputStream
    //   140: dup
    //   141: invokespecial 190	java/io/ByteArrayOutputStream:<init>	()V
    //   144: astore 19
    //   146: new 306	gbis/gbandroid/utils/Base64$OutputStream
    //   149: dup
    //   150: aload 19
    //   152: iload_3
    //   153: iconst_1
    //   154: ior
    //   155: invokespecial 309	gbis/gbandroid/utils/Base64$OutputStream:<init>	(Ljava/io/OutputStream;I)V
    //   158: astore 20
    //   160: new 397	java/util/zip/GZIPOutputStream
    //   163: dup
    //   164: aload 20
    //   166: invokespecial 398	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   169: astore 21
    //   171: aload 21
    //   173: aload_0
    //   174: iload_1
    //   175: iload_2
    //   176: invokevirtual 399	java/util/zip/GZIPOutputStream:write	([BII)V
    //   179: aload 21
    //   181: invokevirtual 400	java/util/zip/GZIPOutputStream:close	()V
    //   184: aload 21
    //   186: invokevirtual 400	java/util/zip/GZIPOutputStream:close	()V
    //   189: aload 20
    //   191: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   194: aload 19
    //   196: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   199: aload 19
    //   201: invokevirtual 208	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   204: astore 9
    //   206: aload 9
    //   208: areturn
    //   209: astore 22
    //   211: aconst_null
    //   212: astore 20
    //   214: aconst_null
    //   215: astore 23
    //   217: aload 22
    //   219: athrow
    //   220: astore 24
    //   222: aload 23
    //   224: astore 19
    //   226: aload 4
    //   228: invokevirtual 400	java/util/zip/GZIPOutputStream:close	()V
    //   231: aload 20
    //   233: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   236: aload 19
    //   238: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   241: aload 24
    //   243: athrow
    //   244: iload_3
    //   245: bipush 8
    //   247: iand
    //   248: ifeq +128 -> 376
    //   251: iconst_1
    //   252: istore 5
    //   254: iconst_4
    //   255: iload_2
    //   256: iconst_3
    //   257: idiv
    //   258: imul
    //   259: istore 6
    //   261: iload_2
    //   262: iconst_3
    //   263: irem
    //   264: ifle +118 -> 382
    //   267: iconst_4
    //   268: istore 7
    //   270: iload 7
    //   272: iload 6
    //   274: iadd
    //   275: istore 8
    //   277: iload 5
    //   279: ifeq +13 -> 292
    //   282: iload 8
    //   284: iload 8
    //   286: bipush 76
    //   288: idiv
    //   289: iadd
    //   290: istore 8
    //   292: iload 8
    //   294: newarray byte
    //   296: astore 9
    //   298: iload_2
    //   299: iconst_2
    //   300: isub
    //   301: istore 10
    //   303: iconst_0
    //   304: istore 11
    //   306: iconst_0
    //   307: istore 12
    //   309: iconst_0
    //   310: istore 13
    //   312: iload 13
    //   314: iload 10
    //   316: if_icmplt +72 -> 388
    //   319: iload 13
    //   321: iload_2
    //   322: if_icmpge +24 -> 346
    //   325: aload_0
    //   326: iload 13
    //   328: iload_1
    //   329: iadd
    //   330: iload_2
    //   331: iload 13
    //   333: isub
    //   334: aload 9
    //   336: iload 12
    //   338: iload_3
    //   339: invokestatic 161	gbis/gbandroid/utils/Base64:a	([BII[BII)[B
    //   342: pop
    //   343: iinc 12 4
    //   346: iload 12
    //   348: iconst_m1
    //   349: aload 9
    //   351: arraylength
    //   352: iadd
    //   353: if_icmpgt -147 -> 206
    //   356: iload 12
    //   358: newarray byte
    //   360: astore 17
    //   362: aload 9
    //   364: iconst_0
    //   365: aload 17
    //   367: iconst_0
    //   368: iload 12
    //   370: invokestatic 246	java/lang/System:arraycopy	(Ljava/lang/Object;ILjava/lang/Object;II)V
    //   373: aload 17
    //   375: areturn
    //   376: iconst_0
    //   377: istore 5
    //   379: goto -125 -> 254
    //   382: iconst_0
    //   383: istore 7
    //   385: goto -115 -> 270
    //   388: aload_0
    //   389: iload 13
    //   391: iload_1
    //   392: iadd
    //   393: iconst_3
    //   394: aload 9
    //   396: iload 12
    //   398: iload_3
    //   399: invokestatic 161	gbis/gbandroid/utils/Base64:a	([BII[BII)[B
    //   402: pop
    //   403: iload 11
    //   405: iconst_4
    //   406: iadd
    //   407: istore 15
    //   409: iload 5
    //   411: ifeq +25 -> 436
    //   414: iload 15
    //   416: bipush 76
    //   418: if_icmplt +18 -> 436
    //   421: aload 9
    //   423: iload 12
    //   425: iconst_4
    //   426: iadd
    //   427: bipush 10
    //   429: bastore
    //   430: iinc 12 1
    //   433: iconst_0
    //   434: istore 15
    //   436: iload 13
    //   438: iconst_3
    //   439: iadd
    //   440: istore 16
    //   442: iinc 12 4
    //   445: iload 15
    //   447: istore 11
    //   449: iload 16
    //   451: istore 13
    //   453: goto -141 -> 312
    //   456: astore 25
    //   458: goto -227 -> 231
    //   461: astore 26
    //   463: goto -227 -> 236
    //   466: astore 27
    //   468: goto -227 -> 241
    //   471: astore 28
    //   473: goto -284 -> 189
    //   476: astore 29
    //   478: goto -284 -> 194
    //   481: astore 30
    //   483: goto -284 -> 199
    //   486: astore 24
    //   488: aconst_null
    //   489: astore 20
    //   491: aconst_null
    //   492: astore 4
    //   494: aconst_null
    //   495: astore 19
    //   497: goto -271 -> 226
    //   500: astore 24
    //   502: aconst_null
    //   503: astore 20
    //   505: aconst_null
    //   506: astore 4
    //   508: goto -282 -> 226
    //   511: astore 24
    //   513: aconst_null
    //   514: astore 4
    //   516: goto -290 -> 226
    //   519: astore 24
    //   521: aload 21
    //   523: astore 4
    //   525: goto -299 -> 226
    //   528: astore 22
    //   530: aload 19
    //   532: astore 23
    //   534: aconst_null
    //   535: astore 20
    //   537: aconst_null
    //   538: astore 4
    //   540: goto -323 -> 217
    //   543: astore 22
    //   545: aload 19
    //   547: astore 23
    //   549: aconst_null
    //   550: astore 4
    //   552: goto -335 -> 217
    //   555: astore 22
    //   557: aload 21
    //   559: astore 4
    //   561: aload 19
    //   563: astore 23
    //   565: goto -348 -> 217
    //
    // Exception table:
    //   from	to	target	type
    //   137	146	209	java/io/IOException
    //   217	220	220	finally
    //   226	231	456	java/lang/Exception
    //   231	236	461	java/lang/Exception
    //   236	241	466	java/lang/Exception
    //   184	189	471	java/lang/Exception
    //   189	194	476	java/lang/Exception
    //   194	199	481	java/lang/Exception
    //   137	146	486	finally
    //   146	160	500	finally
    //   160	171	511	finally
    //   171	184	519	finally
    //   146	160	528	java/io/IOException
    //   160	171	543	java/io/IOException
    //   171	184	555	java/io/IOException
  }

  // ERROR //
  public static void encodeFileToFile(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 405	gbis/gbandroid/utils/Base64:encodeFromFile	(Ljava/lang/String;)Ljava/lang/String;
    //   4: astore_2
    //   5: new 256	java/io/BufferedOutputStream
    //   8: dup
    //   9: new 258	java/io/FileOutputStream
    //   12: dup
    //   13: aload_1
    //   14: invokespecial 259	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   17: invokespecial 262	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   20: astore_3
    //   21: aload_3
    //   22: aload_2
    //   23: ldc 179
    //   25: invokevirtual 182	java/lang/String:getBytes	(Ljava/lang/String;)[B
    //   28: invokevirtual 266	java/io/OutputStream:write	([B)V
    //   31: aload_3
    //   32: invokevirtual 267	java/io/OutputStream:close	()V
    //   35: return
    //   36: astore 4
    //   38: aconst_null
    //   39: astore_3
    //   40: aload 4
    //   42: athrow
    //   43: astore 5
    //   45: aload_3
    //   46: invokevirtual 267	java/io/OutputStream:close	()V
    //   49: aload 5
    //   51: athrow
    //   52: astore 6
    //   54: goto -5 -> 49
    //   57: astore 7
    //   59: return
    //   60: astore 5
    //   62: aconst_null
    //   63: astore_3
    //   64: goto -19 -> 45
    //   67: astore 4
    //   69: goto -29 -> 40
    //
    // Exception table:
    //   from	to	target	type
    //   5	21	36	java/io/IOException
    //   21	31	43	finally
    //   40	43	43	finally
    //   45	49	52	java/lang/Exception
    //   31	35	57	java/lang/Exception
    //   5	21	60	finally
    //   21	31	67	java/io/IOException
  }

  // ERROR //
  public static String encodeFromFile(String paramString)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: new 269	java/io/File
    //   5: dup
    //   6: aload_0
    //   7: invokespecial 270	java/io/File:<init>	(Ljava/lang/String;)V
    //   10: astore_2
    //   11: dconst_1
    //   12: ldc2_w 406
    //   15: aload_2
    //   16: invokevirtual 274	java/io/File:length	()J
    //   19: l2d
    //   20: dmul
    //   21: dadd
    //   22: d2i
    //   23: bipush 40
    //   25: invokestatic 410	java/lang/Math:max	(II)I
    //   28: newarray byte
    //   30: astore 8
    //   32: new 288	gbis/gbandroid/utils/Base64$InputStream
    //   35: dup
    //   36: new 291	java/io/BufferedInputStream
    //   39: dup
    //   40: new 293	java/io/FileInputStream
    //   43: dup
    //   44: aload_2
    //   45: invokespecial 296	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   48: invokespecial 297	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   51: iconst_1
    //   52: invokespecial 300	gbis/gbandroid/utils/Base64$InputStream:<init>	(Ljava/io/InputStream;I)V
    //   55: astore 7
    //   57: aload 7
    //   59: aload 8
    //   61: iload_1
    //   62: sipush 4096
    //   65: invokevirtual 303	gbis/gbandroid/utils/Base64$InputStream:read	([BII)I
    //   68: istore 9
    //   70: iload 9
    //   72: ifge +26 -> 98
    //   75: new 142	java/lang/String
    //   78: dup
    //   79: aload 8
    //   81: iconst_0
    //   82: iload_1
    //   83: ldc 179
    //   85: invokespecial 413	java/lang/String:<init>	([BIILjava/lang/String;)V
    //   88: astore 10
    //   90: aload 7
    //   92: invokevirtual 289	gbis/gbandroid/utils/Base64$InputStream:close	()V
    //   95: aload 10
    //   97: areturn
    //   98: iload_1
    //   99: iload 9
    //   101: iadd
    //   102: istore_1
    //   103: goto -46 -> 57
    //   106: astore 6
    //   108: aconst_null
    //   109: astore 7
    //   111: aload 6
    //   113: athrow
    //   114: astore_3
    //   115: aload 7
    //   117: astore 4
    //   119: aload 4
    //   121: invokevirtual 289	gbis/gbandroid/utils/Base64$InputStream:close	()V
    //   124: aload_3
    //   125: athrow
    //   126: astore 5
    //   128: goto -4 -> 124
    //   131: astore 11
    //   133: aload 10
    //   135: areturn
    //   136: astore_3
    //   137: aconst_null
    //   138: astore 4
    //   140: goto -21 -> 119
    //   143: astore 6
    //   145: goto -34 -> 111
    //
    // Exception table:
    //   from	to	target	type
    //   2	57	106	java/io/IOException
    //   57	70	114	finally
    //   75	90	114	finally
    //   111	114	114	finally
    //   119	124	126	java/lang/Exception
    //   90	95	131	java/lang/Exception
    //   2	57	136	finally
    //   57	70	143	java/io/IOException
    //   75	90	143	java/io/IOException
  }

  public static String encodeObject(Serializable paramSerializable)
  {
    return encodeObject(paramSerializable, 0);
  }

  // ERROR //
  public static String encodeObject(Serializable paramSerializable, int paramInt)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: ifnonnull +14 -> 17
    //   6: new 123	java/lang/NullPointerException
    //   9: dup
    //   10: ldc_w 420
    //   13: invokespecial 128	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: new 189	java/io/ByteArrayOutputStream
    //   20: dup
    //   21: invokespecial 190	java/io/ByteArrayOutputStream:<init>	()V
    //   24: astore_3
    //   25: new 306	gbis/gbandroid/utils/Base64$OutputStream
    //   28: dup
    //   29: aload_3
    //   30: iload_1
    //   31: iconst_1
    //   32: ior
    //   33: invokespecial 309	gbis/gbandroid/utils/Base64$OutputStream:<init>	(Ljava/io/OutputStream;I)V
    //   36: astore 4
    //   38: iload_1
    //   39: iconst_2
    //   40: iand
    //   41: ifeq +65 -> 106
    //   44: new 397	java/util/zip/GZIPOutputStream
    //   47: dup
    //   48: aload 4
    //   50: invokespecial 398	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   53: astore 6
    //   55: new 422	java/io/ObjectOutputStream
    //   58: dup
    //   59: aload 6
    //   61: invokespecial 423	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   64: astore_2
    //   65: aload_2
    //   66: aload_0
    //   67: invokevirtual 426	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   70: aload_2
    //   71: invokevirtual 427	java/io/ObjectOutputStream:close	()V
    //   74: aload 6
    //   76: invokevirtual 400	java/util/zip/GZIPOutputStream:close	()V
    //   79: aload 4
    //   81: invokevirtual 267	java/io/OutputStream:close	()V
    //   84: aload_3
    //   85: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   88: new 142	java/lang/String
    //   91: dup
    //   92: aload_3
    //   93: invokevirtual 208	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   96: ldc 179
    //   98: invokespecial 384	java/lang/String:<init>	([BLjava/lang/String;)V
    //   101: astore 17
    //   103: aload 17
    //   105: areturn
    //   106: new 422	java/io/ObjectOutputStream
    //   109: dup
    //   110: aload 4
    //   112: invokespecial 423	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   115: astore 5
    //   117: aload 5
    //   119: astore_2
    //   120: aconst_null
    //   121: astore 6
    //   123: goto -58 -> 65
    //   126: astore 7
    //   128: aconst_null
    //   129: astore 6
    //   131: aconst_null
    //   132: astore 4
    //   134: aconst_null
    //   135: astore_3
    //   136: aload 7
    //   138: athrow
    //   139: astore 8
    //   141: aload_2
    //   142: invokevirtual 427	java/io/ObjectOutputStream:close	()V
    //   145: aload 6
    //   147: invokevirtual 400	java/util/zip/GZIPOutputStream:close	()V
    //   150: aload 4
    //   152: invokevirtual 267	java/io/OutputStream:close	()V
    //   155: aload_3
    //   156: invokevirtual 211	java/io/ByteArrayOutputStream:close	()V
    //   159: aload 8
    //   161: athrow
    //   162: astore 18
    //   164: new 142	java/lang/String
    //   167: dup
    //   168: aload_3
    //   169: invokevirtual 208	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   172: invokespecial 385	java/lang/String:<init>	([B)V
    //   175: areturn
    //   176: astore 9
    //   178: goto -33 -> 145
    //   181: astore 10
    //   183: goto -33 -> 150
    //   186: astore 11
    //   188: goto -33 -> 155
    //   191: astore 12
    //   193: goto -34 -> 159
    //   196: astore 13
    //   198: goto -124 -> 74
    //   201: astore 14
    //   203: goto -124 -> 79
    //   206: astore 15
    //   208: goto -124 -> 84
    //   211: astore 16
    //   213: goto -125 -> 88
    //   216: astore 8
    //   218: aconst_null
    //   219: astore_2
    //   220: aconst_null
    //   221: astore 6
    //   223: aconst_null
    //   224: astore 4
    //   226: aconst_null
    //   227: astore_3
    //   228: goto -87 -> 141
    //   231: astore 8
    //   233: aconst_null
    //   234: astore_2
    //   235: aconst_null
    //   236: astore 6
    //   238: aconst_null
    //   239: astore 4
    //   241: goto -100 -> 141
    //   244: astore 8
    //   246: aconst_null
    //   247: astore_2
    //   248: aconst_null
    //   249: astore 6
    //   251: goto -110 -> 141
    //   254: astore 7
    //   256: aconst_null
    //   257: astore_2
    //   258: aconst_null
    //   259: astore 6
    //   261: aconst_null
    //   262: astore 4
    //   264: goto -128 -> 136
    //   267: astore 7
    //   269: aconst_null
    //   270: astore_2
    //   271: aconst_null
    //   272: astore 6
    //   274: goto -138 -> 136
    //   277: astore 7
    //   279: goto -143 -> 136
    //
    // Exception table:
    //   from	to	target	type
    //   17	25	126	java/io/IOException
    //   55	65	139	finally
    //   65	70	139	finally
    //   136	139	139	finally
    //   88	103	162	java/io/UnsupportedEncodingException
    //   141	145	176	java/lang/Exception
    //   145	150	181	java/lang/Exception
    //   150	155	186	java/lang/Exception
    //   155	159	191	java/lang/Exception
    //   70	74	196	java/lang/Exception
    //   74	79	201	java/lang/Exception
    //   79	84	206	java/lang/Exception
    //   84	88	211	java/lang/Exception
    //   17	25	216	finally
    //   25	38	231	finally
    //   44	55	244	finally
    //   106	117	244	finally
    //   25	38	254	java/io/IOException
    //   44	55	267	java/io/IOException
    //   106	117	267	java/io/IOException
    //   55	65	277	java/io/IOException
    //   65	70	277	java/io/IOException
  }

  // ERROR //
  public static void encodeToFile(byte[] paramArrayOfByte, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +14 -> 15
    //   4: new 123	java/lang/NullPointerException
    //   7: dup
    //   8: ldc_w 430
    //   11: invokespecial 128	java/lang/NullPointerException:<init>	(Ljava/lang/String;)V
    //   14: athrow
    //   15: new 306	gbis/gbandroid/utils/Base64$OutputStream
    //   18: dup
    //   19: new 258	java/io/FileOutputStream
    //   22: dup
    //   23: aload_1
    //   24: invokespecial 259	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   27: iconst_1
    //   28: invokespecial 309	gbis/gbandroid/utils/Base64$OutputStream:<init>	(Ljava/io/OutputStream;I)V
    //   31: astore_2
    //   32: aload_2
    //   33: aload_0
    //   34: invokevirtual 310	gbis/gbandroid/utils/Base64$OutputStream:write	([B)V
    //   37: aload_2
    //   38: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   41: return
    //   42: astore_3
    //   43: aconst_null
    //   44: astore_2
    //   45: aload_3
    //   46: athrow
    //   47: astore 4
    //   49: aload_2
    //   50: astore 5
    //   52: aload 5
    //   54: invokevirtual 311	gbis/gbandroid/utils/Base64$OutputStream:close	()V
    //   57: aload 4
    //   59: athrow
    //   60: astore 6
    //   62: goto -5 -> 57
    //   65: astore 7
    //   67: return
    //   68: astore 4
    //   70: aconst_null
    //   71: astore 5
    //   73: goto -21 -> 52
    //   76: astore_3
    //   77: goto -32 -> 45
    //
    // Exception table:
    //   from	to	target	type
    //   15	32	42	java/io/IOException
    //   32	37	47	finally
    //   45	47	47	finally
    //   52	57	60	java/lang/Exception
    //   37	41	65	java/lang/Exception
    //   15	32	68	finally
    //   32	37	76	java/io/IOException
  }

  public static class InputStream extends FilterInputStream
  {
    private boolean a;
    private int b;
    private byte[] c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private int h;
    private byte[] i;

    public InputStream(InputStream paramInputStream)
    {
      this(paramInputStream, 0);
    }

    public InputStream(InputStream paramInputStream, int paramInt)
    {
      super();
      this.h = paramInt;
      boolean bool2;
      if ((paramInt & 0x8) > 0)
      {
        bool2 = bool1;
        this.g = bool2;
        if ((paramInt & 0x1) <= 0)
          break label90;
        label34: this.a = bool1;
        if (!this.a)
          break label95;
      }
      label90: label95: for (int j = 4; ; j = 3)
      {
        this.d = j;
        this.c = new byte[this.d];
        this.b = -1;
        this.f = 0;
        this.i = Base64.a(paramInt);
        return;
        bool2 = false;
        break;
        bool1 = false;
        break label34;
      }
    }

    public int read()
    {
      byte[] arrayOfByte3;
      int i1;
      int i2;
      if (this.b < 0)
      {
        if (!this.a)
          break label113;
        arrayOfByte3 = new byte[3];
        i1 = 0;
        i2 = 0;
        if (i1 < 3)
          break label82;
        label31: if (i2 > 0)
        {
          Base64.a(arrayOfByte3, i2, this.c, this.h);
          this.b = 0;
          this.e = 4;
        }
      }
      else
      {
        if (this.b < 0)
          break label305;
        if (this.b < this.e)
          break label221;
      }
      label82: label113: int m;
      label206: 
      do
      {
        return -1;
        int i3 = this.in.read();
        if (i3 < 0)
          break label31;
        arrayOfByte3[i1] = ((byte)i3);
        i2++;
        i1++;
        break;
        byte[] arrayOfByte2 = new byte[4];
        for (m = 0; ; m++)
        {
          if (m >= 4);
          int n;
          do
          {
            if (m != 4)
              break label206;
            this.e = Base64.a(arrayOfByte2, this.c, this.h);
            this.b = 0;
            break;
            do
              n = this.in.read();
            while ((n >= 0) && (this.i[(n & 0x7F)] <= -5));
          }
          while (n < 0);
          arrayOfByte2[m] = ((byte)n);
        }
      }
      while (m == 0);
      throw new IOException("Improperly padded Base64 input.");
      label221: if ((this.a) && (this.g) && (this.f >= 76))
      {
        this.f = 0;
        return 10;
      }
      this.f = (1 + this.f);
      byte[] arrayOfByte1 = this.c;
      int j = this.b;
      this.b = (j + 1);
      int k = arrayOfByte1[j];
      if (this.b >= this.d)
        this.b = -1;
      return k & 0xFF;
      label305: throw new IOException("Error in Base64 code reading stream.");
    }

    public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      int j = 0;
      if (j >= paramInt2);
      do
      {
        return j;
        int k = read();
        if (k >= 0)
        {
          paramArrayOfByte[(paramInt1 + j)] = ((byte)k);
          j++;
          break;
        }
      }
      while (j != 0);
      return -1;
    }
  }

  public static class OutputStream extends FilterOutputStream
  {
    private boolean a;
    private int b;
    private byte[] c;
    private int d;
    private int e;
    private boolean f;
    private byte[] g;
    private boolean h;
    private int i;
    private byte[] j;

    public OutputStream(OutputStream paramOutputStream)
    {
      this(paramOutputStream, 1);
    }

    public OutputStream(OutputStream paramOutputStream, int paramInt)
    {
      super();
      boolean bool2;
      if ((paramInt & 0x8) != 0)
      {
        bool2 = bool1;
        this.f = bool2;
        if ((paramInt & 0x1) == 0)
          break label102;
        label29: this.a = bool1;
        if (!this.a)
          break label107;
      }
      label102: label107: for (int k = 3; ; k = 4)
      {
        this.d = k;
        this.c = new byte[this.d];
        this.b = 0;
        this.e = 0;
        this.h = false;
        this.g = new byte[4];
        this.i = paramInt;
        this.j = Base64.a(paramInt);
        return;
        bool2 = false;
        break;
        bool1 = false;
        break label29;
      }
    }

    public void close()
    {
      flushBase64();
      super.close();
      this.c = null;
      this.out = null;
    }

    public void flushBase64()
    {
      if (this.b > 0)
      {
        if (this.a)
        {
          this.out.write(Base64.a(this.g, this.c, this.b, this.i));
          this.b = 0;
        }
      }
      else
        return;
      throw new IOException("Base64 input not properly padded.");
    }

    public void resumeEncoding()
    {
      this.h = false;
    }

    public void suspendEncoding()
    {
      flushBase64();
      this.h = true;
    }

    public void write(int paramInt)
    {
      if (this.h)
        this.out.write(paramInt);
      do
      {
        do
        {
          do
          {
            return;
            if (!this.a)
              break;
            byte[] arrayOfByte2 = this.c;
            int n = this.b;
            this.b = (n + 1);
            arrayOfByte2[n] = ((byte)paramInt);
          }
          while (this.b < this.d);
          this.out.write(Base64.a(this.g, this.c, this.d, this.i));
          this.e = (4 + this.e);
          if ((this.f) && (this.e >= 76))
          {
            this.out.write(10);
            this.e = 0;
          }
          this.b = 0;
          return;
          if (this.j[(paramInt & 0x7F)] <= -5)
            break;
          byte[] arrayOfByte1 = this.c;
          int k = this.b;
          this.b = (k + 1);
          arrayOfByte1[k] = ((byte)paramInt);
        }
        while (this.b < this.d);
        int m = Base64.a(this.c, this.g, this.i);
        this.out.write(this.g, 0, m);
        this.b = 0;
        return;
      }
      while (this.j[(paramInt & 0x7F)] == -5);
      throw new IOException("Invalid character in Base64 data.");
    }

    public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    {
      if (this.h)
        this.out.write(paramArrayOfByte, paramInt1, paramInt2);
      while (true)
      {
        return;
        for (int k = 0; k < paramInt2; k++)
          write(paramArrayOfByte[(paramInt1 + k)]);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.Base64
 * JD-Core Version:    0.6.2
 */