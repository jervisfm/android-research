package gbis.gbandroid.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class CommonThreads
{
  public static final String GALLERY = "gallery";
  public static final String PHOTO = "photo";
  private Activity a;

  public CommonThreads(Activity paramActivity)
  {
    this.a = paramActivity;
  }

  private BitmapDrawable a(String paramString)
  {
    if (!a())
      throw new RuntimeException();
    return ImageUtils.drawable_from_url(paramString);
  }

  private boolean a()
  {
    return ConnectionUtils.isConnectedToAny(this.a);
  }

  public void startGalleryProgressThread(Handler paramHandler, String[] paramArrayOfString)
  {
    new a(paramHandler, paramArrayOfString).start();
  }

  public void startPhotoProgressThread(Handler paramHandler, String paramString)
  {
    new b(paramHandler, paramString).start();
  }

  private final class a extends Thread
  {
    Handler a;
    String[] b;

    public a(Handler paramArrayOfString, String[] arg3)
    {
      this.a = paramArrayOfString;
      Object localObject;
      this.b = localObject;
    }

    public final void run()
    {
      Message localMessage = this.a.obtainMessage();
      Bundle localBundle = new Bundle();
      try
      {
        if (!CommonThreads.a(CommonThreads.this))
          localBundle.putBoolean("result", false);
        while (true)
        {
          return;
          arrayOfBitmap = new Bitmap[this.b.length];
          int i = this.b.length;
          j = 0;
          if (j < i)
            break;
          localBundle.putBoolean("result", true);
          localBundle.putParcelableArray("gallery", arrayOfBitmap);
        }
      }
      catch (Exception localException)
      {
        while (true)
        {
          Bitmap[] arrayOfBitmap;
          int j;
          localBundle.putBoolean("result", false);
          return;
          try
          {
            arrayOfBitmap[j] = CommonThreads.a(CommonThreads.this, this.b[j]).getBitmap();
            j++;
          }
          catch (Throwable localThrowable)
          {
            while (true)
              localThrowable.printStackTrace();
          }
        }
      }
      finally
      {
        localMessage.setData(localBundle);
        this.a.sendMessage(localMessage);
      }
    }
  }

  private final class b extends Thread
  {
    Handler a;
    String b;

    public b(Handler paramString, String arg3)
    {
      this.a = paramString;
      Object localObject;
      this.b = localObject;
    }

    public final void run()
    {
      Message localMessage = this.a.obtainMessage();
      Bundle localBundle = new Bundle();
      try
      {
        if (!CommonThreads.a(CommonThreads.this))
          localBundle.putBoolean("result", false);
        while (true)
        {
          return;
          BitmapDrawable localBitmapDrawable = CommonThreads.a(CommonThreads.this, this.b);
          localBundle.putBoolean("result", true);
          localBundle.putParcelable("photo", localBitmapDrawable.getBitmap());
        }
      }
      catch (Exception localException)
      {
        localBundle.putBoolean("result", false);
        return;
      }
      finally
      {
        localMessage.setData(localBundle);
        this.a.sendMessage(localMessage);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.CommonThreads
 * JD-Core Version:    0.6.2
 */