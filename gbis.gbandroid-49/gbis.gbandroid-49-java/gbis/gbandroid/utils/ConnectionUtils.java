package gbis.gbandroid.utils;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public abstract class ConnectionUtils
{
  private static boolean a(String paramString1, String paramString2)
  {
    if (paramString1 == null)
      return paramString2 == null;
    return paramString1.equals(paramString2);
  }

  public static boolean isBetterLocation(Location paramLocation1, Location paramLocation2)
  {
    if (paramLocation2 == null);
    label44: int k;
    label71: label77: label81: 
    while (true)
    {
      return true;
      if (paramLocation1 == null)
        return false;
      long l = paramLocation1.getTime() - paramLocation2.getTime();
      int i;
      int j;
      if (l > 120000L)
      {
        i = 1;
        if (l >= -120000L)
          break label71;
        j = 1;
        if (l <= 0L)
          break label77;
      }
      for (k = 1; ; k = 0)
      {
        if (i != 0)
          break label81;
        if (j == 0)
          break label83;
        return false;
        i = 0;
        break;
        j = 0;
        break label44;
      }
    }
    label83: int m = (int)(paramLocation1.getAccuracy() - paramLocation2.getAccuracy());
    int n;
    label103: int i1;
    if (m > 0)
    {
      n = 1;
      if (m >= 0)
        break label173;
      i1 = 1;
      label111: if (m <= 200)
        break label179;
    }
    label173: label179: for (int i2 = 1; ; i2 = 0)
    {
      boolean bool = a(paramLocation1.getProvider(), paramLocation2.getProvider());
      if ((i1 != 0) || ((k != 0) && (n == 0)) || ((k != 0) && (i2 == 0) && (bool)))
        break;
      return false;
      n = 0;
      break label103;
      i1 = 0;
      break label111;
    }
  }

  public static boolean isConnectedToAny(Context paramContext)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    if ((localConnectivityManager.getActiveNetworkInfo() != null) && (localConnectivityManager.getActiveNetworkInfo().isConnected()))
      return true;
    while (true)
    {
      int i;
      boolean bool;
      if ((localConnectivityManager.getNetworkInfo(i) != null) && (localConnectivityManager.getNetworkInfo(i).isConnected()))
        bool = true;
      while ((i >= localConnectivityManager.getAllNetworkInfo().length) || (bool))
      {
        return bool;
        i++;
        continue;
        bool = false;
        i = 0;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.ConnectionUtils
 * JD-Core Version:    0.6.2
 */