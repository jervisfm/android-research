package gbis.gbandroid.utils;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityMap;
import gbis.gbandroid.activities.base.GBPreferenceActivity;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.exceptions.CustomConnectionException;
import gbis.gbandroid.exceptions.RequestParsingWSException;
import gbis.gbandroid.queries.CrashReportQuery;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Type;

public abstract class CustomAsyncTask extends AsyncTask<Object, Object, Boolean>
{
  private Context a;
  private boolean b;

  public CustomAsyncTask(Context paramContext)
  {
    this.a = paramContext;
    this.b = true;
  }

  public CustomAsyncTask(Context paramContext, boolean paramBoolean)
  {
    this.a = paramContext;
    this.b = paramBoolean;
  }

  private static String a(Exception paramException)
  {
    StringWriter localStringWriter = new StringWriter();
    paramException.printStackTrace(new PrintWriter(localStringWriter));
    return localStringWriter.toString();
  }

  private void a(String paramString1, String paramString2)
  {
    try
    {
      Type localType = new b(this).getType();
      CrashReportQuery localCrashReportQuery = new CrashReportQuery(this.a, PreferenceManager.getDefaultSharedPreferences(this.a), localType, new Location("NoLocation"));
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString1);
      localStringBuilder.append("\n");
      localStringBuilder.append(paramString2);
      localCrashReportQuery.getResponseObject(Base64.encodeBytes(localStringBuilder.toString().getBytes()));
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private boolean a()
  {
    return ConnectionUtils.isConnectedToAny(this.a);
  }

  protected Boolean doInBackground(Object[] paramArrayOfObject)
  {
    label108: String str1;
    try
    {
      if (!a())
      {
        if ((this.a instanceof GBActivity))
          ((GBActivity)this.a).setMessage(this.a.getString(2131296257));
        while (true)
        {
          return Boolean.valueOf(false);
          if (!(this.a instanceof GBActivityMap))
            break;
          ((GBActivityMap)this.a).setMessage(this.a.getString(2131296257));
        }
      }
    }
    catch (CustomConnectionException localCustomConnectionException)
    {
      while (true)
      {
        str3 = this.a.getString(2131296259);
        if (!(this.a instanceof GBActivity))
          break label216;
        ((GBActivity)this.a).setMessage(str3);
        return Boolean.valueOf(false);
        if (!(this.a instanceof GBPreferenceActivity))
          break;
        ((GBPreferenceActivity)this.a).setMessage(this.a.getString(2131296257));
      }
    }
    catch (RequestParsingWSException localRequestParsingWSException)
    {
      String str3;
      String str2 = this.a.getString(2131296256);
      if ((this.a instanceof GBActivity))
        ((GBActivity)this.a).setMessage(str2);
      while (true)
      {
        a(str2, a(localRequestParsingWSException));
        return Boolean.valueOf(false);
        break;
        Boolean localBoolean = Boolean.valueOf(queryWebService());
        return localBoolean;
        if ((this.a instanceof GBActivityMap))
        {
          ((GBActivityMap)this.a).setMessage(str3);
          break label108;
        }
        if ((this.a instanceof GBPreferenceActivity))
        {
          ((GBPreferenceActivity)this.a).setMessage(str3);
          break label108;
        }
        break label108;
        if ((this.a instanceof GBActivityMap))
          ((GBActivityMap)this.a).setMessage(str2);
        else if ((this.a instanceof GBPreferenceActivity))
          ((GBPreferenceActivity)this.a).setMessage(str2);
      }
    }
    catch (Exception localException)
    {
      label216: str1 = this.a.getString(2131296280);
      if (!(this.a instanceof GBActivity))
        break label378;
    }
    ((GBActivity)this.a).setMessage(str1);
    while (true)
    {
      a(str1, a(localException));
      return Boolean.valueOf(false);
      label378: if ((this.a instanceof GBActivityMap))
        ((GBActivityMap)this.a).setMessage(str1);
      else if ((this.a instanceof GBPreferenceActivity))
        ((GBPreferenceActivity)this.a).setMessage(str1);
    }
  }

  protected void onPostExecute(Boolean paramBoolean)
  {
    super.onPostExecute(paramBoolean);
    if ((this.a instanceof GBActivity))
    {
      if (this.b)
        ((GBActivity)this.a).showMessage();
      ResponseMessage localResponseMessage3 = ((GBActivity)this.a).getResponseObject();
      if (localResponseMessage3 != null)
        ((GBActivity)this.a).showMessage(localResponseMessage3.getResponseMessage());
      if (paramBoolean.booleanValue())
      {
        if (!(this.a instanceof GBActivity))
          break label231;
        ((GBActivity)this.a).checkAwardAndShow();
        ((GBActivity)this.a).checkSmartPromptAndShow();
      }
    }
    label231: 
    while (!(this.a instanceof GBActivityMap))
    {
      return;
      if ((this.a instanceof GBActivityMap))
      {
        if (this.b)
          ((GBActivityMap)this.a).showMessage();
        ResponseMessage localResponseMessage2 = ((GBActivityMap)this.a).getResponseObject();
        if (localResponseMessage2 == null)
          break;
        ((GBActivityMap)this.a).showMessage(localResponseMessage2.getResponseMessage());
        break;
      }
      if ((this.a instanceof GBPreferenceActivity))
      {
        if (this.b)
          ((GBPreferenceActivity)this.a).showMessage();
        ResponseMessage localResponseMessage1 = ((GBPreferenceActivity)this.a).getResponseObject();
        if (localResponseMessage1 == null)
          break;
        ((GBPreferenceActivity)this.a).showMessage(localResponseMessage1.getResponseMessage());
        break;
      }
      break;
    }
    ((GBActivityMap)this.a).checkAwardAndShow();
  }

  protected abstract boolean queryWebService();

  protected void setAnalyticsTrackEventScreenButton(GoogleAnalyticsTracker paramGoogleAnalyticsTracker, String paramString)
  {
    setAnalyticsTrackEventScreenButton(paramGoogleAnalyticsTracker, this.a.getString(2131296855), paramString);
  }

  protected void setAnalyticsTrackEventScreenButton(GoogleAnalyticsTracker paramGoogleAnalyticsTracker, String paramString1, String paramString2)
  {
    if (paramGoogleAnalyticsTracker != null)
      paramGoogleAnalyticsTracker.trackEvent(this.a.getString(2131296854), paramString2, paramString1, 0);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.CustomAsyncTask
 * JD-Core Version:    0.6.2
 */