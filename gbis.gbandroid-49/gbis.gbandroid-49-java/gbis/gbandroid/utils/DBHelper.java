package gbis.gbandroid.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper
{
  public static final int NONE = -1;
  public static final int ORDER_BY_FREQUENCY = 0;
  public static final int ORDER_BY_NAME = 1;
  public static final String TABLE_COLUMN_ADD_DATE = "add_dt";
  public static final String TABLE_COLUMN_BRAND_ID = "brand_id";
  public static final String TABLE_COLUMN_CITY_ZIP = "table_column_city_zip";
  public static final String TABLE_COLUMN_FREQUENCY = "table_column_frequency";
  public static final String TABLE_COLUMN_ID = "_id";
  public static final String TABLE_COLUMN_IMAGE_LOCATION = "image_location";
  public static final String TABLE_COLUMN_LATITUDE = "table_column_latitude";
  public static final String TABLE_COLUMN_LONGITUDE = "table_column_longitude";
  public static final String TABLE_COLUMN_NAME = "table_column_name";
  public static final String TABLE_COLUMN_VERSION = "version";
  private SQLiteDatabase a;
  private a b;
  private final String c = "gasBuddyDB2";
  private final String d = "tableSavedSearches";
  private final String e = "tableBrands";
  private final int f = 1;
  private final String g = "table_constraints";

  public DBHelper(Context paramContext)
  {
    this.b = new a(paramContext);
  }

  private static String a(String paramString)
  {
    return paramString.replaceAll("'", "''");
  }

  public boolean addBrandRow(int paramInt1, int paramInt2, String paramString)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("brand_id", Integer.valueOf(paramInt1));
    localContentValues.put("version", Integer.valueOf(paramInt2));
    localContentValues.put("image_location", paramString);
    localContentValues.put("add_dt", Long.valueOf(System.currentTimeMillis()));
    try
    {
      this.a.insertOrThrow("tableBrands", null, localContentValues);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB ERROR", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean addSearchesRow(String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    String str = a(paramString1);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("table_column_name", str);
    localContentValues.put("table_column_city_zip", paramString2);
    localContentValues.put("table_column_latitude", Double.valueOf(paramDouble1));
    localContentValues.put("table_column_longitude", Double.valueOf(paramDouble2));
    localContentValues.put("table_column_frequency", Integer.valueOf(1));
    try
    {
      this.a.insertOrThrow("tableSavedSearches", null, localContentValues);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB ERROR", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public void closeDB()
  {
    try
    {
      if (this.a != null)
        this.a.close();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public boolean deleteSearchesAllRows()
  {
    try
    {
      this.a.delete("tableSavedSearches", null, null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB ERROR", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean deleteSearchesRow(long paramLong)
  {
    try
    {
      this.a.delete("tableSavedSearches", "_id=" + paramLong, null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB ERROR", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean deleteSearchesRow(String paramString)
  {
    String str = a(paramString);
    try
    {
      this.a.delete("tableSavedSearches", "table_column_name='" + str + "'", null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB ERROR", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public Cursor getAllSearchesRows(int paramInt)
  {
    String str;
    switch (paramInt)
    {
    default:
      str = null;
    case 0:
    case 1:
    }
    try
    {
      while (true)
      {
        Cursor localCursor = this.a.query("tableSavedSearches", new String[] { "_id", "table_column_name", "table_column_city_zip", "table_column_latitude", "table_column_longitude", "table_column_frequency" }, null, null, null, null, str);
        return localCursor;
        str = "table_column_frequency DESC";
        continue;
        str = "UPPER(table_column_name)";
      }
    }
    catch (SQLException localSQLException)
    {
      Log.e("DB Error", localSQLException.toString());
      localSQLException.printStackTrace();
    }
    return null;
  }

  public Cursor getBrandRow(int paramInt)
  {
    try
    {
      Cursor localCursor = this.a.query("tableBrands", new String[] { "_id", "brand_id", "version", "image_location", "add_dt" }, "brand_id='" + paramInt + "'", null, null, null, null, null);
      return localCursor;
    }
    catch (SQLException localSQLException)
    {
      Log.e("DB ERROR", localSQLException.toString());
      localSQLException.printStackTrace();
    }
    return null;
  }

  public Cursor getSearchesRow(long paramLong)
  {
    try
    {
      Cursor localCursor = this.a.query("tableSavedSearches", new String[] { "_id", "table_column_name", "table_column_city_zip", "table_column_latitude", "table_column_longitude", "table_column_frequency" }, "_id=" + paramLong, null, null, null, null, null);
      return localCursor;
    }
    catch (SQLException localSQLException)
    {
      Log.e("DB ERROR", localSQLException.toString());
      localSQLException.printStackTrace();
    }
    return null;
  }

  public Cursor getSearchesRow(String paramString)
  {
    String str = a(paramString);
    try
    {
      Cursor localCursor = this.a.query("tableSavedSearches", new String[] { "_id", "table_column_name", "table_column_city_zip", "table_column_latitude", "table_column_longitude", "table_column_frequency" }, "table_column_name='" + str + "'", null, null, null, null, null);
      return localCursor;
    }
    catch (SQLException localSQLException)
    {
      Log.e("DB ERROR", localSQLException.toString());
      localSQLException.printStackTrace();
    }
    return null;
  }

  public Cursor getSearchesRowFromCityZip(String paramString)
  {
    String str = a(paramString);
    try
    {
      Cursor localCursor = this.a.query("tableSavedSearches", new String[] { "_id", "table_column_name", "table_column_city_zip", "table_column_latitude", "table_column_longitude", "table_column_frequency" }, "table_column_city_zip='" + str + "'", null, null, null, null, null);
      return localCursor;
    }
    catch (SQLException localSQLException)
    {
      Log.e("DB ERROR", localSQLException.toString());
      localSQLException.printStackTrace();
    }
    return null;
  }

  public boolean incrementSearchesFrequency(String paramString)
  {
    String str1 = a(paramString);
    String str2 = "UPDATE tableSavedSearches SET table_column_frequency=(table_column_frequency+1) WHERE table_column_name='" + str1 + "'";
    try
    {
      this.a.execSQL(str2);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB Error", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean isDBOpen()
  {
    if (this.a != null)
      return this.a.isOpen();
    return false;
  }

  public boolean openDB()
  {
    try
    {
      this.a = this.b.getWritableDatabase();
      return true;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return false;
  }

  public boolean updateBrandVersionRow(int paramInt1, int paramInt2)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("version", Integer.valueOf(paramInt2));
    try
    {
      this.a.update("tableBrands", localContentValues, "brand_id=" + paramInt1, null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB Error", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean updateSearchesName(long paramLong, String paramString)
  {
    String str = a(paramString);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("table_column_name", str);
    try
    {
      this.a.update("tableSavedSearches", localContentValues, "_id=" + paramLong, null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB Error", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean updateSearchesRow(long paramLong, String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    String str = a(paramString1);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("table_column_name", str);
    localContentValues.put("table_column_city_zip", paramString2);
    localContentValues.put("table_column_latitude", Double.valueOf(paramDouble1));
    localContentValues.put("table_column_longitude", Double.valueOf(paramDouble2));
    try
    {
      this.a.update("tableSavedSearches", localContentValues, "_id=" + paramLong, null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB Error", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean updateSearchesRow(String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    String str = a(paramString1);
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("table_column_name", str);
    localContentValues.put("table_column_city_zip", paramString2);
    localContentValues.put("table_column_latitude", Double.valueOf(paramDouble1));
    localContentValues.put("table_column_longitude", Double.valueOf(paramDouble2));
    try
    {
      this.a.update("tableSavedSearches", localContentValues, "table_column_name='" + str + "'", null);
      return true;
    }
    catch (Exception localException)
    {
      Log.e("DB Error", localException.toString());
      localException.printStackTrace();
    }
    return false;
  }

  private final class a extends SQLiteOpenHelper
  {
    public a(Context arg2)
    {
      super("gasBuddyDB2", null, 1);
    }

    private static void a(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("create table tableSavedSearches (_id integer primary key autoincrement not null,table_column_name text,table_column_city_zip text,table_column_latitude decimal(9,6),table_column_longitude decimal(9,6),table_column_frequency int,CONSTRAINT table_constraints UNIQUE (table_column_name)CONSTRAINT table_constraints UNIQUE (table_column_city_zip));");
    }

    private static boolean a(SQLiteDatabase paramSQLiteDatabase, String paramString)
    {
      Cursor localCursor = paramSQLiteDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + paramString + "'", null);
      return (localCursor != null) && (localCursor.getCount() > 0);
    }

    private static void b(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("create table tableBrands (_id integer primary key autoincrement not null,brand_id text,version integer,add_dt long,image_location text,CONSTRAINT table_constraints UNIQUE (brand_id));");
    }

    public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
      a(paramSQLiteDatabase);
      b(paramSQLiteDatabase);
    }

    public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
    {
      super.onOpen(paramSQLiteDatabase);
      if (!a(paramSQLiteDatabase, "tableSavedSearches"))
        a(paramSQLiteDatabase);
      if (!a(paramSQLiteDatabase, "tableBrands"))
        b(paramSQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS tableSavedSearches");
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS tableBrands");
      onCreate(paramSQLiteDatabase);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.DBHelper
 * JD-Core Version:    0.6.2
 */