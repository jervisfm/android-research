package gbis.gbandroid.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public abstract class DateUtils
{
  public static final int DAY_IN_MILLIS = 86400000;
  public static final int HOUR_IN_MILLIS = 3600000;
  public static final int MINUTE_IN_MILLIS = 60000;
  public static final long MONTH_IN_MILLIS = 2592000000L;

  private static boolean a(int paramInt)
  {
    if (paramInt % 400 == 0);
    do
    {
      return true;
      if (paramInt % 100 == 0)
        return false;
    }
    while (paramInt % 4 == 0);
    return false;
  }

  public static String changeToServerType(String paramString)
  {
    Object localObject = new Date();
    SimpleDateFormat localSimpleDateFormat;
    if (!paramString.equals("now"))
      localSimpleDateFormat = new SimpleDateFormat("M/dd/yyyy h:mm:ss a");
    try
    {
      Date localDate = localSimpleDateFormat.parse(paramString);
      localObject = localDate;
      return new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS").format((Date)localObject).replace(' ', 'T');
    }
    catch (ParseException localParseException)
    {
      while (true)
        localParseException.printStackTrace();
    }
  }

  public static String compareDateToNow(Date paramDate, int paramInt)
  {
    GregorianCalendar localGregorianCalendar = new GregorianCalendar();
    TimeZone localTimeZone = TimeZone.getTimeZone("EST5EDT");
    int i = localGregorianCalendar.getTimeZone().getOffset(localGregorianCalendar.getTimeInMillis()) / 3600000 - paramInt - localTimeZone.getOffset(localGregorianCalendar.getTimeInMillis()) / 3600000;
    long l = Math.abs(paramDate.getTime() - localGregorianCalendar.getTime().getTime() + i * 3600000);
    if (l < 86400000L)
    {
      if (l < 3600000L)
      {
        if ((int)(l / 60000L) > 1)
          return Integer.toString((int)(l / 60000L)) + " minutes ago";
        if ((int)(l / 60000L) <= 0)
          return "a moment ago";
        return Integer.toString((int)(l / 60000L)) + " minute ago";
      }
      if ((int)l / 3600000 > 1)
        return Integer.toString((int)(l / 3600000L)) + " hours ago";
      return Integer.toString((int)(l / 3600000L)) + " hour ago";
    }
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTimeInMillis(paramDate.getTime() + i);
    return new SimpleDateFormat("EEE h:mm a").format(new Date(localCalendar.getTimeInMillis()));
  }

  public static String getAge(Date paramDate)
  {
    int i = 0;
    Calendar localCalendar1 = Calendar.getInstance();
    Calendar localCalendar2 = Calendar.getInstance();
    localCalendar2.setTime(paramDate);
    int j = localCalendar1.get(1) - localCalendar2.get(1);
    int k = localCalendar1.get(2) - localCalendar2.get(2);
    int m = (int)Math.floor((localCalendar1.getTimeInMillis() - localCalendar2.getTimeInMillis()) / 86400000L);
    if ((k < 0) || ((k == 0) && (localCalendar1.get(5) - localCalendar2.get(5) < 0)));
    for (int n = j - 1; ; n = j)
    {
      Calendar localCalendar3;
      if (n != 0)
      {
        localCalendar3 = Calendar.getInstance();
        if (!a(localCalendar2.get(1)))
          break label408;
        localCalendar3.set(localCalendar2.get(1), 1, 29);
        if (!localCalendar2.after(localCalendar3))
          break label408;
      }
      label401: label408: for (int i2 = -1; ; i2 = 0)
      {
        localCalendar3.setTimeInMillis(localCalendar2.getTimeInMillis());
        if (i >= n)
        {
          if (a(localCalendar3.get(1)))
          {
            localCalendar3.set(localCalendar1.get(1), 1, 29);
            if (localCalendar1.after(localCalendar3))
              i2++;
          }
          if (i2 == 0)
            break label401;
        }
        for (int i1 = m % i2; ; i1 = m)
        {
          String str;
          if (n > 0)
            if (n > 1)
              str = n + " years";
          while (true)
          {
            label234: if (i1 > 0)
              if (i1 != 1)
                break label359;
            label359: for (str = str + " " + i1 + " day"; ; str = str + " " + i1 + " days")
            {
              if (str.equals(""))
                str = "Less than a day";
              return str;
              if (a(localCalendar3.get(1)))
                i2 += 366;
              while (true)
              {
                i++;
                localCalendar3.add(1, 1);
                break;
                i2 += 365;
              }
              str = n + " year";
              break label234;
            }
            str = "";
          }
        }
      }
    }
  }

  public static String getDateForWinners(Date paramDate)
  {
    return new SimpleDateFormat("MMMM d, yyyy").format(paramDate);
  }

  public static String getNowMD5()
  {
    Calendar localCalendar = Calendar.getInstance();
    int i = localCalendar.get(1);
    int j = localCalendar.get(5);
    int k = localCalendar.get(2);
    DecimalFormat localDecimalFormat = new DecimalFormat("00");
    return i + "_" + localDecimalFormat.format(k + 1) + "_" + localDecimalFormat.format(j);
  }

  public static int getNowOffset()
  {
    GregorianCalendar localGregorianCalendar = new GregorianCalendar();
    return localGregorianCalendar.getTimeZone().getOffset(localGregorianCalendar.getTimeInMillis()) / 3600000 - TimeZone.getTimeZone("EST5EDT").getOffset(localGregorianCalendar.getTimeInMillis()) / 3600000;
  }

  public static Date[] getPrevHours()
  {
    Calendar localCalendar1 = Calendar.getInstance();
    Calendar localCalendar2 = Calendar.getInstance();
    Date[] arrayOfDate = new Date[25];
    localCalendar2.setTimeInMillis(60000L * (localCalendar2.getTimeInMillis() / 60000L));
    int i;
    if (localCalendar2.getTimeInMillis() % 900000L == 0L)
    {
      if (localCalendar1.getTimeInMillis() - localCalendar2.getTimeInMillis() < 60000L)
        localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 900000L);
      i = 0;
      label73: if (localCalendar1.getTimeInMillis() - 7200000L < localCalendar2.getTimeInMillis())
        break label163;
      if (localCalendar2.getTimeInMillis() % 1800000L != 0L)
        localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 900000L);
      label114: if (localCalendar1.getTimeInMillis() - 21600000L < localCalendar2.getTimeInMillis())
        break label188;
    }
    while (true)
    {
      if (localCalendar1.getTimeInMillis() - 43200000L >= localCalendar2.getTimeInMillis())
      {
        return arrayOfDate;
        localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 60000L);
        break;
        label163: arrayOfDate[i] = localCalendar2.getTime();
        localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 900000L);
        i++;
        break label73;
        label188: arrayOfDate[i] = localCalendar2.getTime();
        localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 1800000L);
        i++;
        break label114;
      }
      arrayOfDate[i] = localCalendar2.getTime();
      localCalendar2.setTimeInMillis(localCalendar2.getTimeInMillis() - 3600000L);
      i++;
    }
  }

  public static String getQueryTimeStampDevice()
  {
    return new SimpleDateFormat("yyyy-MM-dd H:mm:ss").format(new Date());
  }

  public static String getQueryTimeStampEastern()
  {
    GregorianCalendar localGregorianCalendar = new GregorianCalendar();
    TimeZone localTimeZone = TimeZone.getTimeZone("EST5EDT");
    int i = localGregorianCalendar.getTimeZone().getOffset(localGregorianCalendar.getTimeInMillis()) - localTimeZone.getOffset(localGregorianCalendar.getTimeInMillis());
    return new SimpleDateFormat("yyyy-MM-dd H:mm:ss").format(new Date(localGregorianCalendar.getTimeInMillis() - i));
  }

  public static String getTimeNow()
  {
    return getTimeToString(Calendar.getInstance().getTime());
  }

  public static String getTimeToString(Date paramDate)
  {
    return new SimpleDateFormat("h:mm a").format(paramDate);
  }

  public static String getTimeToValue(Date paramDate)
  {
    return new SimpleDateFormat("M/dd/yyyy h:mm:ss a").format(paramDate);
  }

  public static Date toDateFormat(String paramString)
  {
    int i = Integer.valueOf(paramString.substring(0, 4)).intValue();
    int j = Integer.valueOf(paramString.substring(5, 7)).intValue();
    int k = Integer.valueOf(paramString.substring(8, 10)).intValue();
    int m = Integer.valueOf(paramString.substring(11, 13)).intValue();
    int n = Integer.valueOf(paramString.substring(14, 16)).intValue();
    int i1 = Integer.valueOf(paramString.substring(17, 19)).intValue();
    return new GregorianCalendar(i, j - 1, k, m, n, i1).getTime();
  }

  public static String toString(Date paramDate)
  {
    return new SimpleDateFormat("MMM d, yyyy").format(paramDate);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.DateUtils
 * JD-Core Version:    0.6.2
 */