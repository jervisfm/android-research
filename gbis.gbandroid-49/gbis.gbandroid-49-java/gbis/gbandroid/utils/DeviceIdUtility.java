package gbis.gbandroid.utils;

import android.os.Build;

public abstract class DeviceIdUtility
{
  public static String getSerialNumber()
  {
    return Build.SERIAL;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.DeviceIdUtility
 * JD-Core Version:    0.6.2
 */