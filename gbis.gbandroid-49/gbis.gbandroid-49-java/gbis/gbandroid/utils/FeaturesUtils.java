package gbis.gbandroid.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.ExifInterface;
import android.view.MotionEvent;
import java.io.File;

public abstract class FeaturesUtils
{
  @TargetApi(5)
  public static int getMotionEventPointCount(MotionEvent paramMotionEvent)
  {
    return paramMotionEvent.getPointerCount();
  }

  @TargetApi(7)
  public static boolean isSMSAvailable(Context paramContext)
  {
    return paramContext.getPackageManager().hasSystemFeature("android.hardware.telephony");
  }

  @TargetApi(5)
  public static int rotateCameraPhoto(File paramFile)
  {
    switch (new ExifInterface(paramFile.getAbsolutePath()).getAttributeInt("Orientation", 1))
    {
    case 4:
    case 5:
    case 7:
    default:
      return 0;
    case 8:
      return 270;
    case 3:
      return 180;
    case 6:
    }
    return 90;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.FeaturesUtils
 * JD-Core Version:    0.6.2
 */