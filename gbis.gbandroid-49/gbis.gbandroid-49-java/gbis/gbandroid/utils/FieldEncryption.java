package gbis.gbandroid.utils;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public abstract class FieldEncryption
{
  public static String decryptTripleDES(byte[] paramArrayOfByte)
  {
    SecretKeySpec localSecretKeySpec = new SecretKeySpec(FieldEncryptionHelper.getKy().getBytes("utf-8"), FieldEncryptionHelper.getSKS());
    IvParameterSpec localIvParameterSpec = new IvParameterSpec(new byte[8]);
    Cipher localCipher = Cipher.getInstance(FieldEncryptionHelper.getCInstance());
    localCipher.init(2, localSecretKeySpec, localIvParameterSpec);
    return new String(localCipher.doFinal(paramArrayOfByte), "UTF-8");
  }

  public static String encode64(byte[] paramArrayOfByte)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    Base64.OutputStream localOutputStream = new Base64.OutputStream(localByteArrayOutputStream);
    localOutputStream.write(paramArrayOfByte);
    localOutputStream.flush();
    localOutputStream.close();
    return localByteArrayOutputStream.toString();
  }

  public static byte[] encryptTripleDES(String paramString)
  {
    SecretKeySpec localSecretKeySpec = new SecretKeySpec(FieldEncryptionHelper.getKy().getBytes("utf-8"), FieldEncryptionHelper.getSKS());
    IvParameterSpec localIvParameterSpec = new IvParameterSpec(new byte[8]);
    Cipher localCipher = Cipher.getInstance(FieldEncryptionHelper.getCInstance());
    localCipher.init(1, localSecretKeySpec, localIvParameterSpec);
    return localCipher.doFinal(paramString.getBytes("utf-8"));
  }

  public static String hashText(String paramString1, String paramString2)
  {
    byte[] arrayOfByte = MessageDigest.getInstance(paramString2).digest(paramString1.getBytes("utf-8"));
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfByte.length)
        return localStringBuffer.toString();
      String str = Integer.toHexString(0xFF & arrayOfByte[i]);
      if (str.length() == 1)
        str = "0" + str;
      localStringBuffer.append(str);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.FieldEncryption
 * JD-Core Version:    0.6.2
 */