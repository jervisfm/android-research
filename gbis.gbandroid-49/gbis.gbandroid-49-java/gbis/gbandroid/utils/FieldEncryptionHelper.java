package gbis.gbandroid.utils;

public abstract class FieldEncryptionHelper
{
  public static String getCInstance()
  {
    String str = FieldEncryption.encode64(new byte[] { 12, 68, -98, 117, -17, -62, 4, 47, -49, 40, 36, -71, 61, -89, 93, -118, 120, 63 });
    return str.substring(0, -1 + str.length());
  }

  public static String getKy()
  {
    return FieldEncryption.encode64(new byte[] { -49, 22, -84, -85, 7, -85, 117, -9, 47, -73, 40, 33, 110, 123, -94, -114, 73, -26 });
  }

  public static String getSKS()
  {
    String str = FieldEncryption.encode64(new byte[] { 12, 68, -98, 117, -17, -62 });
    return str.substring(0, -2 + str.length());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.FieldEncryptionHelper
 * JD-Core Version:    0.6.2
 */