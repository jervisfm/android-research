package gbis.gbandroid.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Environment;
import android.widget.ImageView;
import gbis.gbandroid.listeners.ImageLazyLoaderListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;

public class ImageLoader
{
  public static final String DEFAULT_DIRECTORY = "/Android/data/gbis.gbandroid/cache/LazyList";
  d a = new d();
  c b = new c();
  private HashMap<String, Bitmap> c = new HashMap();
  private File d;
  private int e;
  private int f;
  private String g;
  private ImageLazyLoaderListener h;

  public ImageLoader(Context paramContext, int paramInt)
  {
    a(paramContext, paramInt, "/Android/data/gbis.gbandroid/cache/LazyList");
  }

  public ImageLoader(Context paramContext, int paramInt, String paramString)
  {
    a(paramContext, paramInt, paramString);
  }

  private static Bitmap a(File paramFile, int paramInt)
  {
    int i = 1;
    if (paramInt != 0);
    try
    {
      BitmapFactory.Options localOptions1 = new BitmapFactory.Options();
      localOptions1.inJustDecodeBounds = true;
      BitmapFactory.decodeStream(new FileInputStream(paramFile), null, localOptions1);
      int j = localOptions1.outWidth;
      int k = localOptions1.outHeight;
      while ((j / 2 >= paramInt) && (k / 2 >= paramInt))
      {
        j /= 2;
        k /= 2;
        i *= 2;
      }
      BitmapFactory.Options localOptions2 = new BitmapFactory.Options();
      localOptions2.inSampleSize = i;
      return BitmapFactory.decodeStream(new FileInputStream(paramFile), null, localOptions2);
      Bitmap localBitmap = BitmapFactory.decodeStream(new FileInputStream(paramFile));
      return localBitmap;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
    }
    return null;
  }

  private void a(Context paramContext, int paramInt, String paramString)
  {
    this.b.setPriority(4);
    this.e = paramInt;
    if (Environment.getExternalStorageState().equals("mounted"));
    for (this.d = new File(Environment.getExternalStorageDirectory(), paramString); ; this.d = paramContext.getCacheDir())
    {
      if (!this.d.exists())
        this.d.mkdirs();
      return;
    }
  }

  private void a(String paramString1, ImageView paramImageView, String paramString2)
  {
    this.a.a(paramImageView);
    b localb = new b(paramString1, paramImageView, paramString2);
    synchronized (d.a(this.a))
    {
      d.a(this.a).push(localb);
      d.a(this.a).notifyAll();
      if (this.b.getState() == Thread.State.NEW)
        this.b.start();
      return;
    }
  }

  public static Bitmap getBitmap(String paramString1, File paramFile, String paramString2, int paramInt)
  {
    File localFile = new File(paramFile, paramString2);
    Bitmap localBitmap1 = a(localFile, paramInt);
    if (localBitmap1 != null)
      return localBitmap1;
    try
    {
      InputStream localInputStream = new URL(paramString1).openStream();
      FileOutputStream localFileOutputStream = new FileOutputStream(localFile);
      ImageUtils.CopyStream(localInputStream, localFileOutputStream);
      localFileOutputStream.close();
      Bitmap localBitmap2 = a(localFile, paramInt);
      return localBitmap2;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public static Bitmap getExistentImage(Context paramContext, String paramString1, String paramString2, int paramInt)
  {
    return a(getImageFile(paramContext, paramString1, paramString2), paramInt);
  }

  public static File getImageDirectory(Context paramContext, String paramString)
  {
    if (Environment.getExternalStorageState().equals("mounted"));
    for (File localFile = new File(Environment.getExternalStorageDirectory(), paramString); ; localFile = paramContext.getCacheDir())
    {
      if (!localFile.exists())
        localFile.mkdirs();
      return localFile;
    }
  }

  public static File getImageFile(Context paramContext, String paramString1, String paramString2)
  {
    if (Environment.getExternalStorageState().equals("mounted"));
    for (File localFile = new File(Environment.getExternalStorageDirectory(), paramString1); ; localFile = paramContext.getCacheDir())
    {
      if (!localFile.exists())
        localFile.mkdirs();
      return new File(localFile, paramString2);
    }
  }

  public static boolean isImageExistant(Context paramContext, String paramString1, String paramString2)
  {
    if (Environment.getExternalStorageState().equals("mounted"));
    for (File localFile = new File(Environment.getExternalStorageDirectory(), paramString1); !localFile.exists(); localFile = paramContext.getCacheDir())
      return false;
    return new File(localFile, paramString2).exists();
  }

  public void clearCache()
  {
    this.c.clear();
    File[] arrayOfFile = this.d.listFiles();
    int i = arrayOfFile.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      arrayOfFile[j].delete();
    }
  }

  public void displayImage(String paramString, Activity paramActivity, ImageView paramImageView, boolean paramBoolean)
  {
    displayImage(paramString, paramActivity, paramImageView, paramBoolean, String.valueOf(paramString.hashCode()));
  }

  public void displayImage(String paramString, Activity paramActivity, ImageView paramImageView, boolean paramBoolean, int paramInt)
  {
    displayImage(paramString, paramActivity, paramImageView, paramBoolean, paramInt, String.valueOf(paramString.hashCode()));
  }

  public void displayImage(String paramString1, Activity paramActivity, ImageView paramImageView, boolean paramBoolean, int paramInt, String paramString2)
  {
    this.f = paramInt;
    displayImage(paramString1, paramActivity, paramImageView, paramBoolean, paramString2);
  }

  public void displayImage(String paramString1, Activity paramActivity, ImageView paramImageView, boolean paramBoolean, String paramString2)
  {
    this.g = paramString2;
    File localFile = new File(this.d, this.g);
    if (paramBoolean)
      localFile.delete();
    do
    {
      this.c.remove(this.g);
      while ((this.c.containsKey(this.g)) && (this.c.get(this.g) != null))
      {
        paramImageView.setImageBitmap((Bitmap)this.c.get(this.g));
        return;
        if (localFile.exists())
        {
          Bitmap localBitmap = getBitmap(paramString1, this.d, paramString2, this.f);
          this.c.put(this.g, localBitmap);
        }
      }
      a(paramString1, paramImageView, this.g);
    }
    while (this.e <= 0);
    paramImageView.setImageResource(this.e);
  }

  public void setOnLazyLoaderFinished(ImageLazyLoaderListener paramImageLazyLoaderListener)
  {
    this.h = paramImageLazyLoaderListener;
  }

  public void stopThread()
  {
    this.b.interrupt();
  }

  final class a
    implements Runnable
  {
    Bitmap a;
    ImageView b;

    public a(Bitmap paramImageView, ImageView arg3)
    {
      this.a = paramImageView;
      Object localObject;
      this.b = localObject;
    }

    public final void run()
    {
      if (this.a != null)
      {
        this.b.setImageBitmap(this.a);
        if (ImageLoader.d(ImageLoader.this) != null)
          ImageLoader.d(ImageLoader.this).onImageLoaded(this.b);
      }
      do
      {
        return;
        if (ImageLoader.e(ImageLoader.this) > 0)
          this.b.setImageResource(ImageLoader.e(ImageLoader.this));
      }
      while (ImageLoader.d(ImageLoader.this) == null);
      ImageLoader.d(ImageLoader.this).onImageFailed(this.b);
    }
  }

  private final class b
  {
    public String a;
    public ImageView b;
    public String c;

    public b(String paramImageView, ImageView paramString1, String arg4)
    {
      this.a = paramImageView;
      this.b = paramString1;
      Object localObject;
      this.c = localObject;
    }
  }

  final class c extends Thread
  {
    c()
    {
    }

    public final void run()
    {
      try
      {
        while (true)
        {
          if (ImageLoader.d.a(ImageLoader.this.a).size() == 0);
          synchronized (ImageLoader.d.a(ImageLoader.this.a))
          {
            ImageLoader.d.a(ImageLoader.this.a).wait();
            if (ImageLoader.d.a(ImageLoader.this.a).size() == 0);
          }
          synchronized (ImageLoader.d.a(ImageLoader.this.a))
          {
            ImageLoader.b localb = (ImageLoader.b)ImageLoader.d.a(ImageLoader.this.a).pop();
            Bitmap localBitmap = ImageLoader.getBitmap(localb.a, ImageLoader.a(ImageLoader.this), localb.c, ImageLoader.b(ImageLoader.this));
            if (localBitmap != null)
              ImageLoader.c(ImageLoader.this).put(localb.c, localBitmap);
            Object localObject2 = localb.b.getTag();
            if ((localObject2 != null) && (((String)localObject2).equals(localb.c)))
            {
              ImageLoader.a locala = new ImageLoader.a(ImageLoader.this, localBitmap, localb.b);
              ((Activity)localb.b.getContext()).runOnUiThread(locala);
            }
            if (Thread.interrupted())
            {
              return;
              localObject3 = finally;
              throw localObject3;
            }
          }
        }
      }
      catch (InterruptedException localInterruptedException)
      {
      }
    }
  }

  final class d
  {
    private Stack<ImageLoader.b> b = new Stack();

    d()
    {
    }

    public final void a(ImageView paramImageView)
    {
      int i = 0;
      while (true)
      {
        if (i >= this.b.size())
          return;
        if (((ImageLoader.b)this.b.get(i)).b == paramImageView)
          this.b.remove(i);
        else
          i++;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.ImageLoader
 * JD-Core Version:    0.6.2
 */