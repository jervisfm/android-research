package gbis.gbandroid.utils;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public abstract class ImageUtils
{
  public static void CopyStream(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    try
    {
      byte[] arrayOfByte = new byte[1024];
      while (true)
      {
        int i = paramInputStream.read(arrayOfByte, 0, 1024);
        if (i == -1)
          break;
        paramOutputStream.write(arrayOfByte, 0, i);
      }
    }
    catch (Exception localException)
    {
    }
  }

  public static BitmapDrawable drawable_from_url(String paramString)
  {
    return new BitmapDrawable((InputStream)new URL(paramString).getContent());
  }

  public static Drawable getCarFromName(Resources paramResources, String paramString)
  {
    int i = paramResources.getIdentifier(paramString.substring(0, -4 + paramString.toLowerCase().length()), "drawable", "gbis.gbandroid");
    if (i != 0)
      return paramResources.getDrawable(i);
    return null;
  }

  public static String getResolutionInText(float paramFloat)
  {
    if (paramFloat == 0.75D)
      return "ldpi";
    if (paramFloat == 1.0F)
      return "mdpi";
    if (paramFloat == 1.5D)
      return "hdpi";
    if (paramFloat == 2.0F)
      return "xhdpi";
    return "mdpi";
  }

  public static int getStationLogoId(Resources paramResources, String paramString)
  {
    return paramResources.getIdentifier("logo_" + paramString, "drawable", "gbis.gbandroid");
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.ImageUtils
 * JD-Core Version:    0.6.2
 */