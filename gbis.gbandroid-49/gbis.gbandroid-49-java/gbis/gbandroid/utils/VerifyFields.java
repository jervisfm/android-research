package gbis.gbandroid.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class VerifyFields
{
  public static boolean checkEmail(String paramString)
  {
    return Pattern.compile("^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$").matcher(paramString).matches();
  }

  public static String checkPostalCode(String paramString)
  {
    String str = paramString.replaceAll(" ", "");
    if ((str.length() == 6) && (Character.isDigit(str.charAt(1))) && (Character.isDigit(str.charAt(3))) && (Character.isDigit(str.charAt(5))) && (Character.isLetter(str.charAt(0))) && (Character.isLetter(str.charAt(2))) && (Character.isLetter(str.charAt(4))))
      return new StringBuffer(str).insert(3, " ").toString();
    return "";
  }

  public static boolean checkZip(String paramString)
  {
    int i = paramString.length();
    boolean bool = false;
    if (i == 5);
    try
    {
      Integer.parseInt(paramString);
      bool = true;
      return bool;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return false;
  }

  public static BigDecimal doubleToScale(double paramDouble, int paramInt)
  {
    return new BigDecimal(paramDouble).setScale(paramInt, 4);
  }

  public static String roundTwoDecimals(double paramDouble)
  {
    DecimalFormat localDecimalFormat = (DecimalFormat)NumberFormat.getNumberInstance(Locale.getDefault());
    localDecimalFormat.applyPattern("#.##");
    return localDecimalFormat.format(paramDouble);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.VerifyFields
 * JD-Core Version:    0.6.2
 */