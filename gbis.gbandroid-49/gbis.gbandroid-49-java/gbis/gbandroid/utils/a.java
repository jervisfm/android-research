package gbis.gbandroid.utils;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

final class a extends ObjectInputStream
{
  a(InputStream paramInputStream, ClassLoader paramClassLoader)
  {
    super(paramInputStream);
  }

  public final Class<?> resolveClass(ObjectStreamClass paramObjectStreamClass)
  {
    Class localClass = Class.forName(paramObjectStreamClass.getName(), false, this.a);
    if (localClass == null)
      localClass = super.resolveClass(paramObjectStreamClass);
    return localClass;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.utils.a
 * JD-Core Version:    0.6.2
 */