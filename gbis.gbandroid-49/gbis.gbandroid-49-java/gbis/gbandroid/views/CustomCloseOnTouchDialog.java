package gbis.gbandroid.views;

import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;

public class CustomCloseOnTouchDialog extends CustomDialog
{
  public CustomCloseOnTouchDialog(Context paramContext)
  {
    super(paramContext);
  }

  public CustomCloseOnTouchDialog(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 4)
    {
      dismiss();
      return true;
    }
    return super.dispatchTouchEvent(paramMotionEvent);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().setFlags(32, 32);
    getWindow().setFlags(262144, 262144);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomCloseOnTouchDialog
 * JD-Core Version:    0.6.2
 */