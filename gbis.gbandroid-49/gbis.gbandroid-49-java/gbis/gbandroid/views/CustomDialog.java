package gbis.gbandroid.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import gbis.gbandroid.GBApplication;

public class CustomDialog extends Dialog
{
  public CustomDialog(Context paramContext)
  {
    super(paramContext);
  }

  public CustomDialog(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  public static class Builder
  {
    private Context a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private Drawable g;
    private Bitmap h;
    private View i;
    private DialogInterface.OnClickListener j;
    private DialogInterface.OnClickListener k;
    private DialogInterface.OnClickListener l;
    private Button m;
    private Button n;
    private Button o;
    private GoogleAnalyticsTracker p;
    private String q;
    private boolean r;

    public Builder(Context paramContext)
    {
      a(paramContext, null, "", true);
    }

    public Builder(Context paramContext, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
    {
      a(paramContext, paramGoogleAnalyticsTracker, "", true);
    }

    public Builder(Context paramContext, GoogleAnalyticsTracker paramGoogleAnalyticsTracker, String paramString)
    {
      a(paramContext, paramGoogleAnalyticsTracker, paramString, true);
    }

    public Builder(Context paramContext, GoogleAnalyticsTracker paramGoogleAnalyticsTracker, String paramString, boolean paramBoolean)
    {
      a(paramContext, paramGoogleAnalyticsTracker, paramString, paramBoolean);
    }

    public Builder(Context paramContext, GoogleAnalyticsTracker paramGoogleAnalyticsTracker, boolean paramBoolean)
    {
      a(paramContext, paramGoogleAnalyticsTracker, "", paramBoolean);
    }

    private Dialog a(Dialog paramDialog, boolean paramBoolean)
    {
      View localView = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2130903055, null);
      this.m = ((Button)localView.findViewById(2131165237));
      this.n = ((Button)localView.findViewById(2131165238));
      this.o = ((Button)localView.findViewById(2131165239));
      paramDialog.addContentView(localView, new ViewGroup.LayoutParams(-1, -2));
      ((TextView)localView.findViewById(2131165232)).setText(this.b);
      int i1;
      if (this.g != null)
      {
        ((ImageView)localView.findViewById(2131165231)).setImageDrawable(this.g);
        if (this.d == null)
          break label400;
        this.m.setText(this.d);
        if (this.j == null)
          break label550;
        this.m.setOnClickListener(new a(this, paramDialog));
        i1 = 1;
      }
      while (true)
      {
        label156: if (this.e != null)
        {
          this.n.setText(this.e);
          i1 |= 4;
          if (this.l != null)
            this.n.setOnClickListener(new b(this, paramDialog));
          label203: if (this.f == null)
            break label427;
          this.o.setText(this.f);
          i1 |= 2;
          if (this.k != null)
            this.o.setOnClickListener(new c(this, paramDialog));
          label250: if (i1 != 1)
            break label439;
          a(this.m, localView);
          if (this.c == null)
            break label492;
          ((TextView)localView.findViewById(2131165234)).setText(this.c);
        }
        while (true)
        {
          label264: paramDialog.setContentView(localView);
          if ((this.p != null) && (this.r))
          {
            if (this.q.equals(""))
              this.q = (this.b + " Dialog");
            this.p.trackPageView(this.q);
          }
          return paramDialog;
          if (this.h != null)
          {
            ((ImageView)localView.findViewById(2131165231)).setImageBitmap(this.h);
            break;
          }
          ((ImageView)localView.findViewById(2131165231)).setVisibility(8);
          break;
          label400: this.m.setVisibility(8);
          i1 = 0;
          break label156;
          this.n.setVisibility(8);
          break label203;
          label427: this.o.setVisibility(8);
          break label250;
          label439: if (i1 == 4)
          {
            a(this.n, localView);
            break label264;
          }
          if (i1 == 2)
          {
            a(this.o, localView);
            break label264;
          }
          if (i1 != 0)
            break label264;
          localView.findViewById(2131165235).setVisibility(8);
          break label264;
          label492: if (this.i != null)
          {
            LinearLayout localLinearLayout = (LinearLayout)localView.findViewById(2131165233);
            localLinearLayout.removeAllViews();
            localLinearLayout.addView(this.i, new ViewGroup.LayoutParams(-1, -2));
            if (paramBoolean)
              localLinearLayout.getLayoutParams().width = -1;
          }
        }
        label550: i1 = 1;
      }
    }

    private void a(Context paramContext, GoogleAnalyticsTracker paramGoogleAnalyticsTracker, String paramString, boolean paramBoolean)
    {
      this.a = paramContext;
      this.p = paramGoogleAnalyticsTracker;
      this.q = paramString;
      this.r = paramBoolean;
    }

    private static void a(Button paramButton, View paramView)
    {
      LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)paramButton.getLayoutParams();
      localLayoutParams.gravity = 1;
      localLayoutParams.weight = 0.5F;
      paramButton.setLayoutParams(localLayoutParams);
      paramView.findViewById(2131165236).setVisibility(0);
      paramView.findViewById(2131165240).setVisibility(0);
    }

    public CustomDialog create()
    {
      return (CustomDialog)a(new CustomDialog(this.a, 2131361810), false);
    }

    public CustomDialog create(boolean paramBoolean)
    {
      if (paramBoolean)
        return (CustomDialog)a(new CustomDialog(this.a, 2131361811), paramBoolean);
      return create();
    }

    public CustomCloseOnTouchDialog createCloseOnTouchOutsideDialog()
    {
      return (CustomCloseOnTouchDialog)a(new CustomCloseOnTouchDialog(this.a, 2131361810), false);
    }

    public Button getNegativeButton()
    {
      return this.o;
    }

    public Button getNeutralButton()
    {
      return this.n;
    }

    public Button getPositiveButton()
    {
      return this.m;
    }

    protected void setAnalyticsTrackEventScreenButton(String paramString)
    {
      if (this.p != null)
        this.p.trackEvent(this.q, "ScreenButton", paramString, 0);
    }

    public Builder setContentView(View paramView)
    {
      this.i = paramView;
      return this;
    }

    public Builder setIcon(int paramInt)
    {
      this.g = this.a.getResources().getDrawable(paramInt);
      return this;
    }

    public Builder setIcon(Bitmap paramBitmap)
    {
      this.h = paramBitmap;
      return this;
    }

    public Builder setIcon(Drawable paramDrawable)
    {
      this.g = paramDrawable;
      return this;
    }

    public Builder setMessage(int paramInt)
    {
      this.c = this.a.getString(paramInt);
      return this;
    }

    public Builder setMessage(String paramString)
    {
      this.c = paramString;
      return this;
    }

    public Builder setNegativeButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.f = this.a.getString(paramInt);
      this.k = paramOnClickListener;
      return this;
    }

    public Builder setNegativeButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.f = paramString;
      this.k = paramOnClickListener;
      return this;
    }

    public Builder setNeutralButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.e = this.a.getString(paramInt);
      this.l = paramOnClickListener;
      return this;
    }

    public Builder setNeutralButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.e = paramString;
      this.l = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.d = this.a.getString(paramInt);
      this.j = paramOnClickListener;
      return this;
    }

    public Builder setPositiveButton(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.d = paramString;
      this.j = paramOnClickListener;
      return this;
    }

    public Builder setStationLogo(int paramInt, String paramString)
    {
      Bitmap localBitmap = GBApplication.getInstance().getLogo(paramInt, paramString);
      if (localBitmap != null)
      {
        setIcon(localBitmap);
        return this;
      }
      setIcon(this.a.getResources().getDrawable(2130837622));
      return this;
    }

    public Builder setTitle(int paramInt)
    {
      this.b = this.a.getString(paramInt);
      return this;
    }

    public Builder setTitle(String paramString)
    {
      this.b = paramString;
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomDialog
 * JD-Core Version:    0.6.2
 */