package gbis.gbandroid.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.widget.EditText;
import gbis.gbandroid.activities.report.ReportPrices.FuelType;
import gbis.gbandroid.activities.report.pricekeyboard.base.SafeSpannableStringBuilder.Factory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomEditTextForPrices extends EditText
{
  public static final int BACKGROUND_CLEAN = 0;
  public static final int BACKGROUND_INVALID = 1;
  public static final int BACKGROUND_VALID = 2;
  private static final View.OnLongClickListener g = new d();
  TextWatcher a = new e(this);
  private CompositeOnFocusChangeListener b;
  private boolean c;
  private ReportPrices.FuelType d = null;
  private boolean e = true;
  private boolean f = false;
  protected OnTextChangedListener onTextChangedListener;

  public CustomEditTextForPrices(Context paramContext)
  {
    super(paramContext);
    a();
  }

  public CustomEditTextForPrices(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }

  public CustomEditTextForPrices(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a();
  }

  private void a()
  {
    setEditableFactory(new SafeSpannableStringBuilder.Factory());
    this.b = new CompositeOnFocusChangeListener();
    setOnFocusChangeListener(this.b);
    addTextChangedListener(this.a);
    setOnLongClickListener(g);
  }

  public void enableTextWatcher()
  {
    this.f = false;
  }

  public ReportPrices.FuelType getFuelType()
  {
    return this.d;
  }

  public void haltTextWatcher()
  {
    this.f = true;
  }

  public boolean isNineCents()
  {
    return this.c;
  }

  public boolean onCheckIsTextEditor()
  {
    return !this.e;
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.c)
    {
      float f1 = getResources().getDisplayMetrics().density;
      Paint localPaint = new Paint();
      localPaint.setColor(getResources().getColor(2131230759));
      localPaint.setTextSize(f1 * 20.0F);
      localPaint.setAntiAlias(true);
      localPaint.setTypeface(Typeface.DEFAULT_BOLD);
      paramCanvas.drawText("9", -25 + getMeasuredWidth(), 30.0F, localPaint);
    }
  }

  public void registerOnFocusChangeListener(View.OnFocusChangeListener paramOnFocusChangeListener)
  {
    this.b.registerListener(paramOnFocusChangeListener);
  }

  public void setBackgroundHint(int paramInt)
  {
    if (getBackground().setLevel(paramInt))
      getBackground().invalidateSelf();
  }

  public void setBackgroundHintFromValidStatus(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return;
    case 0:
      getBackground().setLevel(2);
      return;
    case -1:
    case 1:
    }
    getBackground().setLevel(1);
  }

  public void setFuelType(ReportPrices.FuelType paramFuelType)
  {
    this.d = paramFuelType;
  }

  public void setNineCents(boolean paramBoolean)
  {
    this.c = paramBoolean;
  }

  public void setOnTextChangedListener(OnTextChangedListener paramOnTextChangedListener)
  {
    this.onTextChangedListener = paramOnTextChangedListener;
  }

  public void usePriceKeyboard(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }

  protected class CompositeOnFocusChangeListener
    implements View.OnFocusChangeListener
  {
    private List<View.OnFocusChangeListener> b = new ArrayList();

    protected CompositeOnFocusChangeListener()
    {
    }

    public void clearListeners()
    {
      this.b.clear();
    }

    public void onFocusChange(View paramView, boolean paramBoolean)
    {
      Iterator localIterator = this.b.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          return;
        ((View.OnFocusChangeListener)localIterator.next()).onFocusChange(paramView, paramBoolean);
      }
    }

    public void registerListener(View.OnFocusChangeListener paramOnFocusChangeListener)
    {
      this.b.add(paramOnFocusChangeListener);
    }
  }

  public static abstract interface OnTextChangedListener
  {
    public abstract void onTextChanged(CustomEditTextForPrices paramCustomEditTextForPrices, boolean paramBoolean, String paramString1, String paramString2, int paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomEditTextForPrices
 * JD-Core Version:    0.6.2
 */