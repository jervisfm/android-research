package gbis.gbandroid.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import com.google.android.maps.MapView;

public class CustomGrayableMapView extends MapView
{
  private boolean a;
  private String b;
  private Context c;

  public CustomGrayableMapView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.c = paramContext;
  }

  public CustomGrayableMapView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.c = paramContext;
  }

  public CustomGrayableMapView(Context paramContext, String paramString)
  {
    super(paramContext, paramString);
    this.c = paramContext;
  }

  protected void dispatchDraw(Canvas paramCanvas)
  {
    if (this.a)
    {
      RectF localRectF = new RectF();
      localRectF.set(0.0F, 0.0F, getMeasuredWidth(), getMeasuredHeight());
      Paint localPaint1 = new Paint();
      localPaint1.setARGB(175, 255, 255, 255);
      paramCanvas.drawRect(localRectF, localPaint1);
      float f = this.c.getResources().getDisplayMetrics().density;
      Paint localPaint2 = new Paint();
      localPaint2.setColor(-16777216);
      localPaint2.setTextSize(f * 20.0F);
      localPaint2.setTextAlign(Paint.Align.CENTER);
      localPaint2.setAntiAlias(true);
      localPaint2.setTypeface(Typeface.DEFAULT_BOLD);
      paramCanvas.drawText(this.b, getMeasuredWidth() / 2, getMeasuredHeight() / 2, localPaint2);
    }
    super.dispatchDraw(paramCanvas);
  }

  public void setGrayOut(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public void setGrayOut(boolean paramBoolean, String paramString)
  {
    this.a = paramBoolean;
    this.b = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomGrayableMapView
 * JD-Core Version:    0.6.2
 */