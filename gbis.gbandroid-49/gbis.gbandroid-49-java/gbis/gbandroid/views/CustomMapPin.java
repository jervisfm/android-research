package gbis.gbandroid.views;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

public class CustomMapPin extends Drawable
{
  private String a;
  private Bitmap b;
  private Bitmap c;
  private float d;

  public CustomMapPin(Bitmap paramBitmap1, Bitmap paramBitmap2, String paramString, float paramFloat)
  {
    this.b = paramBitmap1;
    this.a = paramString;
    this.c = paramBitmap2;
    this.d = paramFloat;
  }

  public void draw(Canvas paramCanvas)
  {
    Paint localPaint = new Paint();
    localPaint.setColor(-16777216);
    localPaint.setTextSize(12.0F * this.d);
    localPaint.setTextAlign(Paint.Align.CENTER);
    localPaint.setAntiAlias(true);
    localPaint.setTypeface(Typeface.DEFAULT_BOLD);
    paramCanvas.drawBitmap(this.b, -this.b.getWidth() / 2, -this.b.getHeight(), null);
    paramCanvas.drawBitmap(this.c, -this.c.getWidth() / 2, 3 + -this.b.getHeight(), null);
    paramCanvas.drawText(this.a, 0.0F, -19.0F * this.d, localPaint);
  }

  public int getIntrinsicHeight()
  {
    return this.b.getHeight();
  }

  public int getIntrinsicWidth()
  {
    return this.b.getWidth();
  }

  public int getOpacity()
  {
    return -3;
  }

  public void setAlpha(int paramInt)
  {
  }

  public void setColorFilter(ColorFilter paramColorFilter)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomMapPin
 * JD-Core Version:    0.6.2
 */