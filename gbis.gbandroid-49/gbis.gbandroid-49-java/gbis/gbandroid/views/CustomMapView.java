package gbis.gbandroid.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class CustomMapView extends MapView
{
  private GestureDetector a;

  public CustomMapView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }

  public CustomMapView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a();
  }

  public CustomMapView(Context paramContext, String paramString)
  {
    super(paramContext, paramString);
    a();
  }

  private void a()
  {
    this.a = new GestureDetector(new a((byte)0));
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.a != null)
      this.a.onTouchEvent(paramMotionEvent);
    return super.onTouchEvent(paramMotionEvent);
  }

  private final class a extends GestureDetector.SimpleOnGestureListener
  {
    private a()
    {
    }

    public final boolean onDoubleTap(MotionEvent paramMotionEvent)
    {
      CustomMapView.this.getController().zoomInFixing((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
      return super.onDoubleTap(paramMotionEvent);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomMapView
 * JD-Core Version:    0.6.2
 */