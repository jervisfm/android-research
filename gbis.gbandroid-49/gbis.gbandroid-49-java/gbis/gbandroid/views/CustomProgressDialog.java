package gbis.gbandroid.views;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class CustomProgressDialog extends Dialog
{
  public CustomProgressDialog(Context paramContext)
  {
    super(paramContext);
  }

  public CustomProgressDialog(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  public static class Builder
  {
    private Context a;
    private String b;

    public Builder(Context paramContext)
    {
      this.a = paramContext;
    }

    public CustomProgressDialog create()
    {
      CustomProgressDialog localCustomProgressDialog = new CustomProgressDialog(this.a, 2131361812);
      View localView = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2130903063, null);
      if (this.b != null)
        ((TextView)localView.findViewById(2131165263)).setText(this.b);
      localCustomProgressDialog.setContentView(localView);
      return localCustomProgressDialog;
    }

    public Builder setMessage(int paramInt)
    {
      this.b = this.a.getString(paramInt);
      return this;
    }

    public Builder setMessage(String paramString)
    {
      this.b = paramString;
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomProgressDialog
 * JD-Core Version:    0.6.2
 */