package gbis.gbandroid.views;

import android.content.Context;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewAutoResize extends TextView
{
  public static final float MIN_TEXT_SIZE = 20.0F;
  private OnTextResizeListener a;
  private boolean b = false;
  private float c = getTextSize();
  private float d = 0.0F;
  private float e = 20.0F;
  private float f = 1.0F;
  private float g = 0.0F;
  private boolean h = true;

  public CustomTextViewAutoResize(Context paramContext)
  {
    this(paramContext, null);
  }

  public CustomTextViewAutoResize(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public CustomTextViewAutoResize(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  private int a(CharSequence paramCharSequence, TextPaint paramTextPaint, int paramInt, float paramFloat)
  {
    paramTextPaint.setTextSize(paramFloat);
    return new StaticLayout(paramCharSequence, paramTextPaint, paramInt, Layout.Alignment.ALIGN_NORMAL, this.f, this.g, true).getHeight();
  }

  public boolean getAddEllipsis()
  {
    return this.h;
  }

  public float getMaxTextSize()
  {
    return this.d;
  }

  public float getMinTextSize()
  {
    return this.e;
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramBoolean) || (this.b))
      resizeText(paramInt3 - paramInt1 - getCompoundPaddingLeft() - getCompoundPaddingRight(), paramInt4 - paramInt2 - getCompoundPaddingBottom() - getCompoundPaddingTop());
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt1 != paramInt3) || (paramInt2 != paramInt4))
      this.b = true;
  }

  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    this.b = true;
    resetTextSize();
  }

  public void resetTextSize()
  {
    if (this.c > 0.0F)
    {
      super.setTextSize(0, this.c);
      this.d = this.c;
    }
  }

  public void resizeText()
  {
    int i = getHeight() - getPaddingBottom() - getPaddingTop();
    resizeText(getWidth() - getPaddingLeft() - getPaddingRight(), i);
  }

  public void resizeText(int paramInt1, int paramInt2)
  {
    CharSequence localCharSequence = getText();
    if ((localCharSequence == null) || (localCharSequence.length() == 0) || (paramInt2 <= 0) || (paramInt1 <= 0) || (this.c == 0.0F))
      return;
    TextPaint localTextPaint = getPaint();
    float f1 = localTextPaint.getTextSize();
    float f2;
    float f3;
    int j;
    if (this.d > 0.0F)
    {
      f2 = Math.min(this.c, this.d);
      int i = a(localCharSequence, localTextPaint, paramInt1, f2);
      f3 = f2;
      j = i;
    }
    StaticLayout localStaticLayout;
    int k;
    while (true)
    {
      if ((j <= paramInt2) || (f3 <= this.e))
      {
        if ((this.h) && (f3 == this.e) && (j > paramInt2))
        {
          localStaticLayout = new StaticLayout(localCharSequence, localTextPaint, paramInt1, Layout.Alignment.ALIGN_NORMAL, this.f, this.g, false);
          if (localStaticLayout.getLineCount() > 0)
          {
            k = -1 + localStaticLayout.getLineForVertical(paramInt2);
            if (k >= 0)
              break label271;
            setText("");
          }
        }
        localTextPaint.setTextSize(f3);
        setLineSpacing(this.g, this.f);
        if (this.a != null)
          this.a.onTextResize(this, f1, f3);
        this.b = false;
        return;
        f2 = this.c;
        break;
      }
      float f7 = Math.max(f3 - 2.0F, this.e);
      j = a(localCharSequence, localTextPaint, paramInt1, f7);
      f3 = f7;
    }
    label271: int m = localStaticLayout.getLineStart(k);
    int n = localStaticLayout.getLineEnd(k);
    float f4 = localStaticLayout.getLineWidth(k);
    float f5 = localTextPaint.measureText("...");
    float f6 = f4;
    int i1 = n;
    while (true)
    {
      if (paramInt1 >= f6 + f5)
      {
        setText(localCharSequence.subSequence(0, i1) + "...");
        break;
      }
      i1--;
      f6 = localTextPaint.measureText(localCharSequence.subSequence(m, i1 + 1).toString());
    }
  }

  public void setAddEllipsis(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }

  public void setLineSpacing(float paramFloat1, float paramFloat2)
  {
    super.setLineSpacing(paramFloat1, paramFloat2);
    this.f = paramFloat2;
    this.g = paramFloat1;
  }

  public void setMaxTextSize(float paramFloat)
  {
    this.d = paramFloat;
    requestLayout();
    invalidate();
  }

  public void setMinTextSize(float paramFloat)
  {
    this.e = paramFloat;
    requestLayout();
    invalidate();
  }

  public void setOnResizeListener(OnTextResizeListener paramOnTextResizeListener)
  {
    this.a = paramOnTextResizeListener;
  }

  public void setTextSize(float paramFloat)
  {
    super.setTextSize(paramFloat);
    this.c = getTextSize();
  }

  public void setTextSize(int paramInt, float paramFloat)
  {
    super.setTextSize(paramInt, paramFloat);
    this.c = getTextSize();
  }

  public static abstract interface OnTextResizeListener
  {
    public abstract void onTextResize(TextView paramTextView, float paramFloat1, float paramFloat2);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomTextViewAutoResize
 * JD-Core Version:    0.6.2
 */