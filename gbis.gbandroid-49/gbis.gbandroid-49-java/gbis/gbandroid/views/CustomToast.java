package gbis.gbandroid.views;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class CustomToast
{
  private Toast a;
  private String b;

  public CustomToast(Activity paramActivity, String paramString, int paramInt)
  {
    View localView = paramActivity.getLayoutInflater().inflate(2130903074, (ViewGroup)paramActivity.findViewById(2131165330));
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    TextView localTextView = (TextView)localView.findViewById(2131165331);
    this.b = paramString;
    localTextView.setText(paramString);
    localTextView.setMaxWidth(-35 + localDisplayMetrics.widthPixels);
    this.a = new Toast(paramActivity.getApplicationContext());
    this.a.setGravity(49, 0, 20);
    this.a.setView(localView);
    this.a.setDuration(paramInt);
  }

  public void cancel()
  {
    this.a.cancel();
  }

  public String getMessage()
  {
    return this.b;
  }

  public void setDuration(int paramInt)
  {
    this.a.setDuration(paramInt);
  }

  public void setGravity(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.setGravity(paramInt1, paramInt2, paramInt3);
  }

  public void show()
  {
    this.a.show();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomToast
 * JD-Core Version:    0.6.2
 */