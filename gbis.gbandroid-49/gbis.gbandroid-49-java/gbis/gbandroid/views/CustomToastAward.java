package gbis.gbandroid.views;

import android.app.Activity;
import android.graphics.drawable.ShapeDrawable;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import gbis.gbandroid.entities.AwardsMessage;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.ImageUtils;

public class CustomToastAward
{
  private Toast a;

  public CustomToastAward(Activity paramActivity, AwardsMessage paramAwardsMessage, int paramInt)
  {
    View localView1 = paramActivity.getLayoutInflater().inflate(2130903049, (ViewGroup)paramActivity.findViewById(2131165224));
    TextView localTextView = (TextView)localView1.findViewById(2131165225);
    ImageView localImageView = (ImageView)localView1.findViewById(2131165215);
    View localView2 = localView1.findViewById(2131165214);
    this.a = new Toast(paramActivity.getApplicationContext());
    this.a.setGravity(49, 0, 20);
    this.a.setView(localView1);
    this.a.setDuration(paramInt);
    localView2.setBackgroundDrawable(new ShapeDrawable(new f(this, paramAwardsMessage, localView2)));
    localTextView.setText(Html.fromHtml(paramAwardsMessage.getCompletedMessage()));
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    ImageLoader localImageLoader = new ImageLoader(paramActivity, 2130837604, "/Android/data/gbis.gbandroid/cache/Awards");
    String str = paramActivity.getString(2131296781) + ImageUtils.getResolutionInText(localDisplayMetrics.density) + "/" + paramAwardsMessage.getAwardId() + "/" + "onsmall.png";
    localImageView.setTag(String.valueOf(str.hashCode()));
    try
    {
      if (!str.equals(""))
      {
        localImageLoader.displayImage(str, paramActivity, localImageView, true, 0);
        return;
      }
      localImageView.setImageResource(2130837605);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public void setGravity(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a.setGravity(paramInt1, paramInt2, paramInt3);
  }

  public void show()
  {
    this.a.show();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomToastAward
 * JD-Core Version:    0.6.2
 */