package gbis.gbandroid.views;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

public class CustomTooltipDialog extends Dialog
{
  public CustomTooltipDialog(Context paramContext)
  {
    super(paramContext);
  }

  public CustomTooltipDialog(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 4)
    {
      dismiss();
      return true;
    }
    if (paramMotionEvent.getAction() == 0)
    {
      dismiss();
      return true;
    }
    return super.dispatchTouchEvent(paramMotionEvent);
  }

  public static class Builder
  {
    private Context a;
    private String b;

    public Builder(Context paramContext)
    {
      this.a = paramContext;
    }

    public CustomTooltipDialog create()
    {
      CustomTooltipDialog localCustomTooltipDialog = new CustomTooltipDialog(this.a, 2131361813);
      View localView = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2130903075, null);
      if (this.b != null)
        ((TextView)localView.findViewById(2131165333)).setText(this.b);
      localCustomTooltipDialog.setContentView(localView);
      WindowManager.LayoutParams localLayoutParams = localCustomTooltipDialog.getWindow().getAttributes();
      localLayoutParams.windowAnimations = 17432577;
      localLayoutParams.dimAmount = 0.25F;
      localCustomTooltipDialog.getWindow().addFlags(262144);
      localCustomTooltipDialog.getWindow().addFlags(32);
      localCustomTooltipDialog.getWindow().addFlags(2);
      localCustomTooltipDialog.getWindow().setAttributes(localLayoutParams);
      return localCustomTooltipDialog;
    }

    public Builder setMessage(int paramInt)
    {
      this.b = this.a.getString(paramInt);
      return this;
    }

    public Builder setMessage(String paramString)
    {
      this.b = paramString;
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.CustomTooltipDialog
 * JD-Core Version:    0.6.2
 */