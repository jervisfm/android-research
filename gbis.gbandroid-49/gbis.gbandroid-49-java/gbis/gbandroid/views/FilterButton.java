package gbis.gbandroid.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import gbis.gbandroid.R.styleable;

public class FilterButton extends RelativeLayout
{
  private String a;
  private Drawable b;

  public FilterButton(Context paramContext)
  {
    super(paramContext);
    LayoutInflater.from(paramContext).inflate(2130903080, this, true);
  }

  public FilterButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }

  public FilterButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
  }

  private void a()
  {
    ((TextView)findViewById(2131165366)).setText(this.a);
  }

  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    LayoutInflater.from(paramContext).inflate(2130903080, this, true);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FilterButton);
    this.a = localTypedArray.getString(0);
    this.b = localTypedArray.getDrawable(1);
    a();
    b();
    localTypedArray.recycle();
    setClickable(true);
    setFocusable(true);
  }

  private void b()
  {
    if (this.b != null)
      ((ImageView)findViewById(2131165365)).setImageDrawable(this.b);
  }

  public Drawable getIcon()
  {
    return this.b;
  }

  public String getText()
  {
    return this.a;
  }

  public void setIcon(Drawable paramDrawable)
  {
    this.b = paramDrawable;
    b();
  }

  public void setText(String paramString)
  {
    this.a = paramString;
    a();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.FilterButton
 * JD-Core Version:    0.6.2
 */