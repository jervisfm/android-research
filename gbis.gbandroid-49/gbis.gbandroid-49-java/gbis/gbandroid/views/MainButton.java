package gbis.gbandroid.views;

import android.graphics.drawable.Drawable;

public class MainButton
{
  private Drawable a;
  private String b;
  private int c;
  private Drawable d;
  private boolean e;

  public MainButton(Drawable paramDrawable, String paramString, int paramInt)
  {
    a(paramDrawable, paramString, paramInt, null, false);
  }

  public MainButton(Drawable paramDrawable1, String paramString, int paramInt, Drawable paramDrawable2, boolean paramBoolean)
  {
    a(paramDrawable1, paramString, paramInt, paramDrawable2, paramBoolean);
  }

  public MainButton(Drawable paramDrawable, String paramString, int paramInt, boolean paramBoolean)
  {
    a(paramDrawable, paramString, paramInt, null, paramBoolean);
  }

  private void a(Drawable paramDrawable1, String paramString, int paramInt, Drawable paramDrawable2, boolean paramBoolean)
  {
    this.a = paramDrawable1;
    this.b = paramString;
    this.c = paramInt;
    this.d = paramDrawable2;
    this.e = paramBoolean;
  }

  public Drawable getBadge()
  {
    return this.d;
  }

  public int getButtonId()
  {
    return this.c;
  }

  public Drawable getIcon()
  {
    return this.a;
  }

  public String getLabel()
  {
    return this.b;
  }

  public boolean isBadgeDisplayed()
  {
    return this.e;
  }

  public void setBadge(Drawable paramDrawable)
  {
    this.d = paramDrawable;
  }

  public void setBadgeDisplayed(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }

  public void setButtonId(int paramInt)
  {
    this.c = paramInt;
  }

  public void setIcon(Drawable paramDrawable)
  {
    this.a = paramDrawable;
  }

  public void setLabel(String paramString)
  {
    this.b = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.MainButton
 * JD-Core Version:    0.6.2
 */