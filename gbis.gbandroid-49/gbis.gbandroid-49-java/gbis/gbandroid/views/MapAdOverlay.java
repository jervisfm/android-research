package gbis.gbandroid.views;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import gbis.gbandroid.activities.map.MapStations;
import gbis.gbandroid.activities.map.MapStationsBase;
import gbis.gbandroid.entities.ChoiceHotels;
import gbis.gbandroid.utils.FeaturesUtils;
import java.util.ArrayList;

public class MapAdOverlay extends MyItemizedOverlay
{
  private MapStationsBase a;
  private ChoiceHotels b;
  private Dialog c;
  private Drawable d;
  private boolean e;
  private boolean f = false;

  public MapAdOverlay(Drawable paramDrawable, ChoiceHotels paramChoiceHotels, MapStationsBase paramMapStationsBase, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
  {
    super(boundCenterBottom(paramDrawable), paramGoogleAnalyticsTracker);
    a(paramDrawable, paramChoiceHotels, paramMapStationsBase, true);
  }

  public MapAdOverlay(Drawable paramDrawable, ChoiceHotels paramChoiceHotels, MapStationsBase paramMapStationsBase, boolean paramBoolean, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
  {
    super(boundCenterBottom(paramDrawable), paramGoogleAnalyticsTracker);
    a(paramDrawable, paramChoiceHotels, paramMapStationsBase, paramBoolean);
  }

  private void a(double paramDouble1, double paramDouble2)
  {
    GeoPoint localGeoPoint = ((MapStations)this.a).getMyLocation();
    if ((localGeoPoint != null) && (localGeoPoint.getLatitudeE6() != 0) && (localGeoPoint.getLongitudeE6() != 0))
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?saddr=" + localGeoPoint.getLatitudeE6() / 1000000.0D + "," + localGeoPoint.getLongitudeE6() / 1000000.0D + "&daddr=" + paramDouble1 + "," + paramDouble2));
      this.a.startActivity(localIntent);
      return;
    }
    ((MapStations)this.a).showMessage(this.a.getString(2131296279));
  }

  private void a(Drawable paramDrawable, ChoiceHotels paramChoiceHotels, MapStationsBase paramMapStationsBase, boolean paramBoolean)
  {
    this.d = paramDrawable;
    this.a = paramMapStationsBase;
    this.b = paramChoiceHotels;
    this.e = paramBoolean;
  }

  private void a(View paramView)
  {
    if (!this.b.getAddress2().equals(""));
    for (String str1 = ". "; ; str1 = "")
    {
      ((TextView)paramView.findViewById(2131165459)).setText(this.b.getAddress() + " " + str1 + " " + this.b.getAddress2());
      TextView localTextView1 = (TextView)paramView.findViewById(2131165460);
      StringBuilder localStringBuilder = new StringBuilder(String.valueOf(this.b.getCity()));
      if ((this.b.getState() != null) && (!this.b.getState().equals("")));
      for (String str2 = ", " + this.b.getState(); ; str2 = "")
      {
        localTextView1.setText(str2);
        TextView localTextView2 = (TextView)paramView.findViewById(2131165462);
        TextView localTextView3 = (TextView)paramView.findViewById(2131165461);
        localTextView2.setText(Html.fromHtml("<a href=\"" + this.b.getUrl() + "\">" + this.b.getClickText() + "</a>"));
        localTextView2.setMovementMethod(LinkMovementMethod.getInstance());
        localTextView3.setText(this.b.getPhone());
        localTextView3.setOnClickListener(new h(this));
        return;
      }
    }
  }

  private void a(String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.DIAL");
    localIntent.setData(Uri.parse("tel:" + paramString));
    this.a.startActivity(localIntent);
  }

  public void addOverlay(OverlayItem paramOverlayItem)
  {
    this.mOverlays.add(paramOverlayItem);
    populate();
  }

  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof MapAdOverlay)) && (this.b.getBrandId().equals(((MapAdOverlay)paramObject).b.getBrandId()));
  }

  public Drawable getDefaultMarker()
  {
    return this.d;
  }

  public int hashCode()
  {
    return this.b.getBrandId().hashCode();
  }

  protected boolean onTap(int paramInt)
  {
    if (this.f)
      return false;
    if ((this.e) && ((this.c == null) || (!this.c.isShowing())))
    {
      View localView = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2130903115, null);
      CustomDialog.Builder localBuilder = new CustomDialog.Builder(this.a, this.mTracker, this.a.getString(2131296850));
      localBuilder.setContentView(localView);
      localBuilder.setTitle(this.b.getName());
      localBuilder.setStationLogo(this.b.getAdId(), "/Android/data/gbis.gbandroid/cache/Ads");
      localBuilder.setPositiveButton(this.a.getString(2131296649), new g(this));
      this.c = localBuilder.createCloseOnTouchOutsideDialog();
      a(localView);
      this.c.show();
    }
    return true;
  }

  public boolean onTap(GeoPoint paramGeoPoint, MapView paramMapView)
  {
    if (this.f)
      return false;
    return super.onTap(paramGeoPoint, paramMapView);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent, MapView paramMapView)
  {
    if (Integer.parseInt(Build.VERSION.SDK) >= 5)
      if (paramMotionEvent.getAction() != 0)
        break label29;
    for (this.f = false; ; this.f = true)
      label29: 
      do
        return super.onTouchEvent(paramMotionEvent, paramMapView);
      while ((paramMotionEvent.getAction() != 2) || (FeaturesUtils.getMotionEventPointCount(paramMotionEvent) <= 1));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.MapAdOverlay
 * JD-Core Version:    0.6.2
 */