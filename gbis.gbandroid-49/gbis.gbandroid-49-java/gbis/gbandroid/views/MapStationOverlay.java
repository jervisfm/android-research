package gbis.gbandroid.views;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import gbis.gbandroid.activities.map.MapStations;
import gbis.gbandroid.activities.map.MapStationsBase;
import gbis.gbandroid.activities.report.ReportPrices;
import gbis.gbandroid.activities.station.StationDetails;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.FeaturesUtils;
import gbis.gbandroid.utils.VerifyFields;
import java.util.ArrayList;
import java.util.Date;

public class MapStationOverlay extends MyItemizedOverlay
{
  private MapStationsBase a;
  private SharedPreferences b;
  private ListMessage c;
  private String d;
  private Dialog e;
  private Drawable f;
  private boolean g;
  private boolean h = false;

  public MapStationOverlay(Drawable paramDrawable, ListMessage paramListMessage, MapStationsBase paramMapStationsBase, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
  {
    super(boundCenterBottom(paramDrawable), paramGoogleAnalyticsTracker);
    a(paramDrawable, paramListMessage, paramMapStationsBase, true);
  }

  public MapStationOverlay(Drawable paramDrawable, ListMessage paramListMessage, MapStationsBase paramMapStationsBase, boolean paramBoolean, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
  {
    super(boundCenterBottom(paramDrawable), paramGoogleAnalyticsTracker);
    a(paramDrawable, paramListMessage, paramMapStationsBase, paramBoolean);
  }

  private void a()
  {
    this.d = this.b.getString("fuelPreference", "");
  }

  private void a(Drawable paramDrawable, ListMessage paramListMessage, MapStationsBase paramMapStationsBase, boolean paramBoolean)
  {
    this.f = paramDrawable;
    this.a = paramMapStationsBase;
    this.c = paramListMessage;
    this.b = PreferenceManager.getDefaultSharedPreferences(this.a);
    a();
    this.g = paramBoolean;
  }

  private void a(View paramView)
  {
    label143: String str1;
    label156: TextView localTextView;
    StringBuilder localStringBuilder;
    if (this.c.getPrice() > 0.0D)
    {
      ((TextView)paramView.findViewById(2131165463)).setText(b());
      if (this.c.getCountry().equals("USA"))
      {
        ((TextView)paramView.findViewById(2131165464)).setText(VerifyFields.doubleToScale(this.c.getPrice(), 2));
        ((TextView)paramView.findViewById(2131165465)).setVisibility(0);
        Date localDate = DateUtils.toDateFormat(this.c.getTimeSpotted());
        ((TextView)paramView.findViewById(2131165466)).setText("Updated: " + DateUtils.compareDateToNow(localDate, this.c.getTimeOffset()));
        if (!this.c.isNotIntersection())
          break label403;
        str1 = "near";
        ((TextView)paramView.findViewById(2131165459)).setText(this.c.getAddress() + " " + str1 + " " + this.c.getCrossStreet());
        localTextView = (TextView)paramView.findViewById(2131165460);
        localStringBuilder = new StringBuilder(String.valueOf(this.c.getCity()));
        if ((this.c.getState() == null) || (this.c.getState().equals("")))
          break label409;
      }
    }
    label403: label409: for (String str2 = ", " + this.c.getState(); ; str2 = "")
    {
      localTextView.setText(str2);
      return;
      ((TextView)paramView.findViewById(2131165464)).setText(VerifyFields.doubleToScale(this.c.getPrice(), 1));
      ((TextView)paramView.findViewById(2131165465)).setVisibility(8);
      break;
      ((TextView)paramView.findViewById(2131165463)).setVisibility(8);
      ((TextView)paramView.findViewById(2131165464)).setVisibility(8);
      ((TextView)paramView.findViewById(2131165466)).setVisibility(8);
      break label143;
      str1 = "&";
      break label156;
    }
  }

  private String b()
  {
    if (this.d.equals(this.a.getString(2131296713)))
      return this.a.getString(2131296717) + ": ";
    if (this.d.equals(this.a.getString(2131296714)))
      return this.a.getString(2131296718) + ": ";
    if (this.d.equals(this.a.getString(2131296715)))
      return this.a.getString(2131296719) + ": ";
    if (this.d.equals(this.a.getString(2131296716)))
      return this.a.getString(2131296720) + ": ";
    return "";
  }

  private void c()
  {
    Intent localIntent = new Intent(this.a, StationDetails.class);
    localIntent.putExtra("station", this.c);
    ((MapStations)this.a).unregisterPreferencesListener();
    this.a.startActivityForResult(localIntent, 4);
    this.e.dismiss();
  }

  private void d()
  {
    GeoPoint localGeoPoint = ((MapStations)this.a).getMyLocation();
    double d1;
    double d2;
    Intent localIntent;
    if (localGeoPoint == null)
    {
      d1 = 0.0D;
      d2 = 0.0D;
      localIntent = new Intent(this.a, ReportPrices.class);
      localIntent.putExtra("station", this.c);
      localIntent.putExtra("my latitude", d1);
      localIntent.putExtra("my longitude", d2);
      localIntent.putExtra("time offset", this.c.getTimeOffset());
      if (!this.d.equals(this.a.getResources().getStringArray(2131099649)[0]))
        break label189;
      localIntent.putExtra("fuel regular", this.c.getPrice());
      localIntent.putExtra("time spotted regular", this.c.getTimeSpotted());
    }
    while (true)
    {
      ((MapStations)this.a).unregisterPreferencesListener();
      this.a.startActivityForResult(localIntent, 0);
      this.e.dismiss();
      return;
      d1 = localGeoPoint.getLatitudeE6() / 1000000.0D;
      d2 = localGeoPoint.getLongitudeE6() / 1000000.0D;
      break;
      label189: if (this.d.equals(this.a.getResources().getStringArray(2131099649)[1]))
      {
        localIntent.putExtra("fuel midgrade", this.c.getPrice());
        localIntent.putExtra("time spotted midgrade", this.c.getTimeSpotted());
      }
      else if (this.d.equals(this.a.getResources().getStringArray(2131099649)[2]))
      {
        localIntent.putExtra("fuel premium", this.c.getPrice());
        localIntent.putExtra("time spotted premium", this.c.getTimeSpotted());
      }
      else if (this.d.equals(this.a.getResources().getStringArray(2131099649)[3]))
      {
        localIntent.putExtra("fuel diesel", this.c.getPrice());
        localIntent.putExtra("time spotted diesel", this.c.getTimeSpotted());
      }
    }
  }

  public void addOverlay(OverlayItem paramOverlayItem)
  {
    this.mOverlays.add(paramOverlayItem);
    populate();
  }

  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof MapStationOverlay)) && (this.c.getStationId() == ((MapStationOverlay)paramObject).c.getStationId());
  }

  public Drawable getDefaultMarker()
  {
    return this.f;
  }

  public int hashCode()
  {
    return this.c.getStationId();
  }

  protected boolean onTap(int paramInt)
  {
    if (this.h)
      return false;
    if ((this.g) && ((this.e == null) || (!this.e.isShowing())))
    {
      this.a.showPinFront(this.c);
      View localView = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2130903116, null);
      CustomDialog.Builder localBuilder = new CustomDialog.Builder(this.a, this.mTracker, this.a.getString(2131296850));
      localBuilder.setContentView(localView);
      localBuilder.setTitle(this.c.getStationName());
      localBuilder.setStationLogo(this.c.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
      localBuilder.setPositiveButton(this.a.getString(2131296635), new i(this));
      localBuilder.setNegativeButton(this.a.getString(2131296636), new j(this));
      this.e = localBuilder.createCloseOnTouchOutsideDialog();
      a(localView);
      this.e.show();
    }
    return true;
  }

  public boolean onTap(GeoPoint paramGeoPoint, MapView paramMapView)
  {
    if (this.h)
      return false;
    return super.onTap(paramGeoPoint, paramMapView);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent, MapView paramMapView)
  {
    if (Integer.parseInt(Build.VERSION.SDK) >= 5)
      if (paramMotionEvent.getAction() != 0)
        break label29;
    for (this.h = false; ; this.h = true)
      label29: 
      do
        return super.onTouchEvent(paramMotionEvent, paramMapView);
      while ((paramMotionEvent.getAction() != 2) || (FeaturesUtils.getMotionEventPointCount(paramMotionEvent) <= 1));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.MapStationOverlay
 * JD-Core Version:    0.6.2
 */