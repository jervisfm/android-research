package gbis.gbandroid.views;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;

public class MyItemizedOverlay extends ItemizedOverlay<OverlayItem>
{
  protected ArrayList<OverlayItem> mOverlays = new ArrayList();
  protected GoogleAnalyticsTracker mTracker;

  public MyItemizedOverlay(Drawable paramDrawable, GoogleAnalyticsTracker paramGoogleAnalyticsTracker)
  {
    super(boundCenterBottom(paramDrawable));
    this.mTracker = paramGoogleAnalyticsTracker;
  }

  public void addOverlay(OverlayItem paramOverlayItem)
  {
    this.mOverlays.add(paramOverlayItem);
    populate();
  }

  protected OverlayItem createItem(int paramInt)
  {
    return (OverlayItem)this.mOverlays.get(paramInt);
  }

  public void draw(Canvas paramCanvas, MapView paramMapView, boolean paramBoolean)
  {
    super.draw(paramCanvas, paramMapView, false);
  }

  public int size()
  {
    return this.mOverlays.size();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.MyItemizedOverlay
 * JD-Core Version:    0.6.2
 */