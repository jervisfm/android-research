package gbis.gbandroid.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import gbis.gbandroid.activities.map.MapStationsBase;

public class StationsMapView extends CustomMapView
{
  private int a;
  private int b;
  private MapStationsBase c;

  public StationsMapView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public StationsMapView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public StationsMapView(Context paramContext, String paramString)
  {
    super(paramContext, paramString);
  }

  public void dispatchDraw(Canvas paramCanvas)
  {
    super.dispatchDraw(paramCanvas);
    if ((this.a != 0) && (this.b != 0))
    {
      if ((getLatitudeSpan() / this.a != 1) || (getLongitudeSpan() / this.b != 1))
        this.c.loadMapPins();
    }
    else
    {
      this.a = getLatitudeSpan();
      this.b = getLongitudeSpan();
    }
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((paramMotionEvent.getAction() == 1) && (this.c.getMapDistance() > 400.0D))
      this.c.loadMapPins();
    return super.onTouchEvent(paramMotionEvent);
  }

  public void setControl(MapStationsBase paramMapStationsBase)
  {
    this.c = paramMapStationsBase;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.StationsMapView
 * JD-Core Version:    0.6.2
 */