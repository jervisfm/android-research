package gbis.gbandroid.views;

import android.text.Editable;
import android.text.TextWatcher;

final class e
  implements TextWatcher
{
  boolean a = false;
  String b = null;
  String c = null;

  e(CustomEditTextForPrices paramCustomEditTextForPrices)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
    if (CustomEditTextForPrices.a(this.d));
    while (this.d.onTextChangedListener == null)
      return;
    this.d.onTextChangedListener.onTextChanged(this.d, this.a, this.c, this.b, this.d.getSelectionStart());
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt3 > 0);
    for (boolean bool = true; ; bool = false)
    {
      this.a = bool;
      this.b = paramCharSequence.toString();
      return;
    }
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    this.c = paramCharSequence.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.e
 * JD-Core Version:    0.6.2
 */