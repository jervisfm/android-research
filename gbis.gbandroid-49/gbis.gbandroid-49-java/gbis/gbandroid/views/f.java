package gbis.gbandroid.views;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;
import android.view.View;
import gbis.gbandroid.entities.AwardsMessage;

final class f extends Shape
{
  f(CustomToastAward paramCustomToastAward, AwardsMessage paramAwardsMessage, View paramView)
  {
  }

  public final void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setColor(Color.parseColor(this.b.getColor()));
    paramPaint.setAntiAlias(true);
    paramPaint.setShadowLayer(1.0F, 0.0F, 3.0F, -16777216);
    paramCanvas.drawCircle(this.c.getMeasuredWidth() / 2, this.c.getMeasuredHeight() / 2, 61.0F, paramPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.f
 * JD-Core Version:    0.6.2
 */