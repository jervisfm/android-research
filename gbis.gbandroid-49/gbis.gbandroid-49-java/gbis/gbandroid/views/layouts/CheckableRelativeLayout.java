package gbis.gbandroid.views.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.RelativeLayout;

public class CheckableRelativeLayout extends RelativeLayout
  implements Checkable
{
  private CheckBox a;

  public CheckableRelativeLayout(Context paramContext)
  {
    super(paramContext);
  }

  public CheckableRelativeLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public CheckableRelativeLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public boolean isChecked()
  {
    if (this.a != null)
      return this.a.isChecked();
    return false;
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    int i = getChildCount();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      View localView = getChildAt(j);
      if ((localView instanceof CheckBox))
        this.a = ((CheckBox)localView);
    }
  }

  public void setChecked(boolean paramBoolean)
  {
    if (this.a != null)
      this.a.setChecked(paramBoolean);
  }

  public void toggle()
  {
    if (this.a != null)
      this.a.toggle();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.layouts.CheckableRelativeLayout
 * JD-Core Version:    0.6.2
 */