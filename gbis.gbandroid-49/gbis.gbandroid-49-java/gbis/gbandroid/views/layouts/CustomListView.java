package gbis.gbandroid.views.layouts;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import gbis.gbandroid.animations.MyScaleAnimation;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CustomListView extends ListView
  implements AbsListView.OnScrollListener
{
  private OnRefreshListener a;
  private AbsListView.OnScrollListener b;
  private LayoutInflater c;
  private LinearLayout d;
  private TextView e;
  private ImageView f;
  private ProgressBar g;
  private int h;
  private int i;
  private RotateAnimation j;
  private RotateAnimation k;
  private int l;
  private int m;
  private int n;
  private float o;

  public CustomListView(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }

  public CustomListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext);
  }

  public CustomListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext);
  }

  private void a()
  {
    this.d.setPadding(this.d.getPaddingLeft(), this.m, this.d.getPaddingRight(), this.d.getPaddingBottom());
  }

  private void a(Context paramContext)
  {
    this.j = new RotateAnimation(0.0F, -180.0F, 1, 0.5F, 1, 0.5F);
    this.j.setInterpolator(new LinearInterpolator());
    this.j.setDuration(250L);
    this.j.setFillAfter(true);
    this.k = new RotateAnimation(-180.0F, 0.0F, 1, 0.5F, 1, 0.5F);
    this.k.setInterpolator(new LinearInterpolator());
    this.k.setDuration(250L);
    this.k.setFillAfter(true);
    this.c = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.d = ((LinearLayout)this.c.inflate(2130903098, null));
    this.e = ((TextView)this.d.findViewById(2131165393));
    this.f = ((ImageView)this.d.findViewById(2131165394));
    this.g = ((ProgressBar)this.d.findViewById(2131165392));
    this.f.setMinimumHeight(50);
    this.m = this.d.getPaddingTop();
    this.i = 1;
    addHeaderView(this.d, null, false);
    this.d.setOnClickListener(new a((byte)0));
    super.setOnScrollListener(this);
    a(this.d);
    this.l = this.d.getMeasuredHeight();
    this.o = getResources().getDisplayMetrics().density;
  }

  private void a(MotionEvent paramMotionEvent)
  {
    int i1 = 1;
    int i2 = paramMotionEvent.getHistorySize();
    try
    {
      int i8 = ((Integer)MotionEvent.class.getMethod("getPointerCount", new Class[0]).invoke(paramMotionEvent, new Object[0])).intValue();
      i1 = i8;
      label37: i3 = 0;
      if (i3 >= i2)
        return;
    }
    catch (IllegalArgumentException localIllegalArgumentException2)
    {
      throw localIllegalArgumentException2;
    }
    catch (IllegalAccessException localIllegalAccessException2)
    {
      while (true)
        System.err.println("unexpected " + localIllegalAccessException2);
    }
    catch (InvocationTargetException localInvocationTargetException2)
    {
      int i3;
      while (true)
        System.err.println("unexpected " + localInvocationTargetException2);
      int i4 = 0;
      while (true)
      {
        if (i4 >= i1)
        {
          i3++;
          break;
        }
        if (this.i == 3)
          if (isVerticalFadingEdgeEnabled())
            setVerticalScrollBarEnabled(false);
        try
        {
          Class[] arrayOfClass = new Class[2];
          arrayOfClass[0] = Integer.TYPE;
          arrayOfClass[1] = Integer.TYPE;
          Method localMethod = MotionEvent.class.getMethod("getHistoricalY", arrayOfClass);
          Object[] arrayOfObject = new Object[2];
          arrayOfObject[0] = Integer.valueOf(i4);
          arrayOfObject[1] = Integer.valueOf(i3);
          int i7 = ((Float)localMethod.invoke(paramMotionEvent, arrayOfObject)).intValue();
          i5 = i7;
          int i6 = (int)((i5 - this.n - this.l) / 1.7D);
          this.d.setPadding(this.d.getPaddingLeft(), i6, this.d.getPaddingRight(), this.d.getPaddingBottom());
          i4++;
        }
        catch (NoSuchMethodException localNoSuchMethodException2)
        {
          while (true)
            i5 = (int)paramMotionEvent.getHistoricalY(i3);
        }
        catch (IllegalArgumentException localIllegalArgumentException1)
        {
          throw localIllegalArgumentException1;
        }
        catch (IllegalAccessException localIllegalAccessException1)
        {
          while (true)
          {
            System.err.println("unexpected " + localIllegalAccessException1);
            i5 = 0;
          }
        }
        catch (InvocationTargetException localInvocationTargetException1)
        {
          while (true)
          {
            System.err.println("unexpected " + localInvocationTargetException1);
            int i5 = 0;
          }
        }
      }
    }
    catch (NoSuchMethodException localNoSuchMethodException1)
    {
      break label37;
    }
  }

  private static void a(View paramView)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (localLayoutParams == null)
      localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
    int i1 = ViewGroup.getChildMeasureSpec(0, 0, localLayoutParams.width);
    int i2 = localLayoutParams.height;
    if (i2 > 0);
    for (int i3 = View.MeasureSpec.makeMeasureSpec(i2, 1073741824); ; i3 = View.MeasureSpec.makeMeasureSpec(0, 0))
    {
      paramView.measure(i1, i3);
      return;
    }
  }

  private void b()
  {
    if (this.i != 1)
    {
      this.i = 1;
      a();
      this.e.setText(2131296304);
      this.f.setImageResource(2130837600);
      this.f.clearAnimation();
      this.f.setVisibility(8);
      this.g.setVisibility(8);
      this.d.getLayoutParams().height = -2;
    }
  }

  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
  }

  public void onRefresh()
  {
    if (this.a != null)
      this.a.onRefresh();
  }

  public void onRefreshComplete()
  {
    b();
    if (this.d.getBottom() > 0)
    {
      invalidateViews();
      setSelection(1);
    }
  }

  public void onRefreshComplete(CharSequence paramCharSequence)
  {
    onRefreshComplete();
  }

  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((this.h == 1) && (this.i != 4))
      if (paramInt1 == 0)
      {
        this.f.setVisibility(0);
        if (((this.d.getBottom() > this.l + 50.0F * this.o) || (this.d.getTop() >= 0)) && (this.i != 3))
        {
          this.e.setText(2131296306);
          this.f.clearAnimation();
          this.f.startAnimation(this.j);
          this.i = 3;
        }
      }
    while (true)
    {
      if (this.b != null)
        this.b.onScroll(paramAbsListView, paramInt1, paramInt2, paramInt3);
      return;
      if ((this.d.getBottom() < this.l + 25.0F * this.o) && (this.i != 2))
      {
        this.e.setText(2131296305);
        if (this.i != 1)
        {
          this.f.clearAnimation();
          this.f.startAnimation(this.k);
        }
        this.i = 2;
        continue;
        this.f.setVisibility(8);
        b();
        continue;
        if ((this.h == 2) && (paramInt1 == 0) && (this.i != 4))
          setSelection(1);
      }
    }
  }

  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
    this.h = paramInt;
    if (this.b != null)
      this.b.onScrollStateChanged(paramAbsListView, paramInt);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = (int)paramMotionEvent.getY();
    switch (paramMotionEvent.getAction())
    {
    default:
    case 1:
    case 0:
    case 2:
    }
    while (true)
    {
      return super.onTouchEvent(paramMotionEvent);
      if (!isVerticalScrollBarEnabled())
        setVerticalScrollBarEnabled(true);
      if ((getFirstVisiblePosition() == 0) && (this.i != 4))
        if (((this.d.getBottom() > this.l) || (this.d.getTop() >= 0)) && (this.i == 3))
        {
          this.i = 4;
          prepareForRefresh();
          onRefresh();
        }
        else if ((this.d.getBottom() < this.l) || (this.d.getTop() < 0))
        {
          b();
          setSelection(1);
          continue;
          this.n = i1;
          continue;
          a(paramMotionEvent);
        }
    }
  }

  public void prepareForRefresh()
  {
    this.d.startAnimation(new MyScaleAnimation(1.0F, 1.0F, 1.0F, 0.0F, 400, this.d, false));
    this.d.getAnimation().setAnimationListener(new a(this));
  }

  public void setAdapter(ListAdapter paramListAdapter)
  {
    super.setAdapter(paramListAdapter);
    setSelection(1);
  }

  public void setOnRefreshListener(OnRefreshListener paramOnRefreshListener)
  {
    this.a = paramOnRefreshListener;
  }

  public void setOnScrollListener(AbsListView.OnScrollListener paramOnScrollListener)
  {
    this.b = paramOnScrollListener;
  }

  public static abstract interface OnRefreshListener
  {
    public abstract void onRefresh();
  }

  private final class a
    implements View.OnClickListener
  {
    private a()
    {
    }

    public final void onClick(View paramView)
    {
      if (CustomListView.a(CustomListView.this) != 4)
      {
        CustomListView.this.prepareForRefresh();
        CustomListView.this.onRefresh();
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.layouts.CustomListView
 * JD-Core Version:    0.6.2
 */