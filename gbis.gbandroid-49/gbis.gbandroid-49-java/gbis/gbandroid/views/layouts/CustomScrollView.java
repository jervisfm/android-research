package gbis.gbandroid.views.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;
import gbis.gbandroid.listeners.ScrollViewListener;

public class CustomScrollView extends ScrollView
{
  private ScrollViewListener a = null;

  public CustomScrollView(Context paramContext)
  {
    super(paramContext);
  }

  public CustomScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public CustomScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (this.a != null)
      this.a.onScrollChanged(this, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void setOnScrollViewListener(ScrollViewListener paramScrollViewListener)
  {
    this.a = paramScrollViewListener;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.layouts.CustomScrollView
 * JD-Core Version:    0.6.2
 */