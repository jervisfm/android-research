package gbis.gbandroid.views.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

public class DashboardLayout extends ViewGroup
{
  private int a = 0;
  private int b = 0;

  public DashboardLayout(Context paramContext)
  {
    super(paramContext, null);
  }

  public DashboardLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet, 0);
  }

  public DashboardLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = paramInt3 - paramInt1;
    int j = paramInt4 - paramInt2;
    int k = getChildCount();
    int m = 0;
    for (int n = 0; ; n++)
    {
      if (n >= k)
      {
        if (m != 0)
          break;
        return;
      }
      if (getChildAt(n).getVisibility() != 8)
        m++;
    }
    int i1 = 2147483647;
    int i2 = 1;
    label67: int i3 = 1 + (m - 1) / i2;
    int i4 = (i - i2 * this.a) / (i2 + 1);
    int i5 = (j - i3 * this.b) / (i3 + 1);
    int i6 = Math.abs(i5 - i4);
    if (i3 * i2 != m)
      i6 *= 10;
    int i7;
    int i9;
    int i8;
    label164: int i12;
    int i13;
    int i14;
    int i15;
    label216: int i19;
    int i20;
    int i21;
    label307: int i22;
    if (i6 < i1)
    {
      if (i3 != 1)
        break label405;
      i7 = i3;
      i9 = i5;
      i8 = i4;
      int i10 = Math.max(0, i8);
      int i11 = Math.max(0, i9);
      i12 = (i - i10 * (i2 + 1)) / i2;
      i13 = (j - i11 * (i7 + 1)) / i7;
      i14 = 0;
      i15 = 0;
      if (i15 < k)
      {
        View localView = getChildAt(i15);
        if (localView.getVisibility() == 8)
          break label435;
        int i17 = i14 / i2;
        int i18 = i14 % i2;
        i19 = paramInt1 + i10 * (i18 + 1) + i12 * i18;
        i20 = paramInt2 + i11 * (i17 + 1) + i13 * i17;
        if ((i10 != 0) || (i18 != i2 - 1))
          break label415;
        i21 = paramInt3;
        if ((i11 != 0) || (i17 != i7 - 1))
          break label425;
        i22 = paramInt4;
        label325: localView.layout(i19, i20, i21, i22);
      }
    }
    label405: label415: label425: label435: for (int i16 = i14 + 1; ; i16 = i14)
    {
      i15++;
      i14 = i16;
      break label216;
      break;
      i2--;
      i7 = 1 + (m - 1) / i2;
      i8 = (i - i2 * this.a) / (i2 + 1);
      i9 = (j - i7 * this.b) / (i7 + 1);
      break label164;
      i2++;
      i1 = i6;
      break label67;
      i21 = i19 + i12;
      break label307;
      i22 = i20 + i13;
      break label325;
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = 0;
    this.a = 0;
    this.b = 0;
    int j = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt1), -2147483648);
    int k = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt1), -2147483648);
    int m = getChildCount();
    while (true)
    {
      if (i >= m)
      {
        setMeasuredDimension(resolveSize(this.a, paramInt1), resolveSize(this.b, paramInt2));
        return;
      }
      View localView = getChildAt(i);
      if (localView.getVisibility() != 8)
      {
        localView.measure(j, k);
        this.a = Math.max(this.a, localView.getMeasuredWidth());
        this.b = Math.max(this.b, localView.getMeasuredHeight());
      }
      i++;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.layouts.DashboardLayout
 * JD-Core Version:    0.6.2
 */