package gbis.gbandroid.views.layouts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;

public class DnDGridView extends GridView
{
  private final int a;
  private WindowManager b;
  private WindowManager.LayoutParams c;
  private Resources d = getResources();
  private ImageView e;
  private Rect f = new Rect();
  private DragListener g;
  private DropListner h;
  private int i;
  private int j;
  private int k;
  private int l;
  private int m;
  private int n;
  private int o;
  private int p;
  private int q;
  private Context r;
  private Bitmap s;
  private int t;
  private int u;
  private boolean v = false;
  private int w;

  public DnDGridView(Context paramContext)
  {
    super(paramContext);
    this.a = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.r = paramContext;
  }

  public DnDGridView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.a = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.r = paramContext;
  }

  public DnDGridView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.a = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.r = paramContext;
  }

  private int a(int paramInt1, int paramInt2)
  {
    Rect localRect = this.f;
    for (int i1 = -1 + getChildCount(); ; i1--)
    {
      if (i1 < 0)
        return -1;
      getChildAt(i1).getHitRect(localRect);
      if (localRect.contains(paramInt1, paramInt2))
        return i1 + getFirstVisiblePosition();
    }
  }

  private void a()
  {
    for (int i1 = 0; ; i1++)
    {
      View localView = getChildAt(i1);
      if (localView == null)
      {
        layoutChildren();
        localView = getChildAt(i1);
        if (localView == null)
          break;
      }
      ViewGroup.LayoutParams localLayoutParams = localView.getLayoutParams();
      localLayoutParams.height = this.t;
      localView.setLayoutParams(localLayoutParams);
      localView.setVisibility(0);
    }
  }

  private void a(int paramInt)
  {
    if (paramInt >= this.o / 3)
      this.p = (this.o / 3);
    if (paramInt <= 2 * this.o / 3)
      this.q = (2 * this.o / 3);
  }

  private void a(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    this.c = new WindowManager.LayoutParams();
    this.c.gravity = 51;
    this.c.x = (paramInt2 - this.k + this.m);
    this.c.y = (paramInt1 - this.j + this.l);
    this.c.height = -2;
    this.c.width = -2;
    this.c.flags = 408;
    this.c.format = -3;
    this.c.windowAnimations = 0;
    ImageView localImageView = new ImageView(this.r);
    localImageView.setBackgroundColor(this.d.getColor(2131230720));
    localImageView.setImageBitmap(paramBitmap);
    this.s = paramBitmap;
    this.b = ((WindowManager)this.r.getSystemService("window"));
    this.b.addView(localImageView, this.c);
    this.e = localImageView;
  }

  private int b(int paramInt1, int paramInt2)
  {
    if (this.w == 0)
      this.w = paramInt1;
    if (this.w < paramInt1);
    for (int i1 = paramInt1 - this.u / 8; ; i1 = paramInt1 + this.u / 8)
    {
      if (i1 < 11)
        i1 = 15;
      int i2 = a(i1, paramInt2);
      this.w = paramInt1;
      return i2;
    }
  }

  private void b()
  {
    int i1 = this.n - getFirstVisiblePosition();
    if (this.n > this.i)
      i1++;
    View localView1 = getChildAt(this.i - getFirstVisiblePosition());
    int i2 = 0;
    View localView2 = getChildAt(i2);
    int i3;
    int i4;
    int i5;
    if (localView2 != null)
    {
      i3 = this.t;
      if (localView2.equals(localView1))
        if (this.n == this.i)
        {
          i4 = i3;
          i5 = 4;
        }
    }
    while (true)
    {
      ViewGroup.LayoutParams localLayoutParams = localView2.getLayoutParams();
      localLayoutParams.height = i4;
      localView2.setLayoutParams(localLayoutParams);
      localView2.setVisibility(i5);
      i2++;
      break;
      i4 = i3;
      i5 = 4;
      continue;
      if ((i2 == i1) && (this.n < -1 + getCount()))
      {
        i4 = this.t;
        i5 = 0;
      }
      else
      {
        i4 = i3;
        i5 = 0;
      }
    }
  }

  private void c()
  {
    if (this.v)
    {
      this.v = false;
      if (this.e != null)
      {
        ((WindowManager)this.r.getSystemService("window")).removeView(this.e);
        this.e.setImageDrawable(null);
        this.e = null;
      }
      if (this.s != null)
      {
        this.s.recycle();
        this.s = null;
      }
    }
  }

  private void c(int paramInt1, int paramInt2)
  {
    this.c.y = (paramInt2 - this.j + this.l);
    this.c.x = (paramInt1 - this.k + this.m);
    this.b.updateViewLayout(this.e, this.c);
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if ((this.t == 0) || (this.u == 0))
    {
      this.t = getChildAt(0).getHeight();
      this.u = getChildAt(0).getWidth();
    }
    if (((this.g != null) || (this.h != null)) && (this.v))
      switch (paramMotionEvent.getAction())
      {
      case 1:
      default:
      case 0:
      case 2:
      }
    while (this.v)
    {
      return this.v;
      int i1 = (int)paramMotionEvent.getX();
      int i2 = (int)paramMotionEvent.getY();
      int i3 = pointToPosition(i1, i2);
      if (i3 != -1)
      {
        ViewGroup localViewGroup = (ViewGroup)getChildAt(i3 - getFirstVisiblePosition());
        this.j = (i2 - localViewGroup.getTop());
        this.k = (i1 - localViewGroup.getLeft());
        this.l = ((int)paramMotionEvent.getRawY() - i2);
        this.m = ((int)paramMotionEvent.getRawX() - i1);
        localViewGroup.findViewById(2131165440).getDrawingRect(this.f);
        localViewGroup.setDrawingCacheEnabled(true);
        a(Bitmap.createBitmap(localViewGroup.getDrawingCache()), i2, i1);
        this.n = i3;
        this.i = this.n;
        this.o = getHeight();
        int i4 = this.a;
        this.p = Math.min(i2 - i4, this.o / 3);
        this.q = Math.max(i4 + i2, 2 * this.o / 3);
        c(i1, i2);
      }
    }
    return super.onInterceptTouchEvent(paramMotionEvent);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!this.v)
      return super.onTouchEvent(paramMotionEvent);
    int i1;
    if (((this.g != null) || (this.h != null)) && (this.e != null))
    {
      i1 = paramMotionEvent.getAction();
      switch (i1)
      {
      default:
      case 1:
      case 3:
      case 0:
      case 2:
      }
    }
    label365: 
    while (true)
    {
      return true;
      Rect localRect = this.f;
      this.e.getDrawingRect(localRect);
      c();
      if ((this.h != null) && (this.n >= 0) && (this.n < getCount()))
        this.h.drop(this.i, this.n);
      a();
      continue;
      int i2 = (int)paramMotionEvent.getX();
      int i3 = (int)paramMotionEvent.getY();
      c(i2, i3);
      int i4 = b(i2, i3);
      if (i4 >= 0)
      {
        if ((i1 == 0) || (i4 != this.n))
        {
          if (this.g != null)
            this.g.drag(this.n, i4);
          this.n = i4;
          b();
        }
        a(i3);
        int i5;
        if (i3 > this.q)
          if (i3 > (this.o + this.q) / 2)
            i5 = 16;
        while (true)
        {
          if (i5 == 0)
            break label365;
          int i6 = pointToPosition(0, this.o / 2);
          if (i6 == -1)
            i6 = pointToPosition(0, 64 + this.o / 2);
          if (getChildAt(i6 - getFirstVisiblePosition()) == null)
            break;
          setSelection(i6);
          break;
          i5 = 4;
          continue;
          if (i3 < this.p)
          {
            if (i3 < this.p / 2)
            {
              i5 = -16;
            }
            else
            {
              i5 = -4;
              continue;
              return false;
            }
          }
          else
            i5 = 0;
        }
      }
    }
  }

  public void setDragListener(DragListener paramDragListener)
  {
    this.g = paramDragListener;
  }

  public void setDropListener(DropListner paramDropListner)
  {
    this.h = paramDropListner;
  }

  public void startDrag(View paramView)
  {
    int i1 = 0;
    this.v = true;
    paramView.clearFocus();
    paramView.setPressed(false);
    Animation localAnimation = AnimationUtils.loadAnimation(this.r, 2130968577);
    int i2 = getChildCount();
    while (true)
    {
      if (i1 >= i2)
        return;
      getChildAt(i1).startAnimation(localAnimation);
      i1++;
    }
  }

  public void stopDrag()
  {
    int i1 = getChildCount();
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
        return;
      getChildAt(i2).clearAnimation();
    }
  }

  public static abstract interface DragListener
  {
    public abstract void drag(int paramInt1, int paramInt2);
  }

  public static abstract interface DropListner
  {
    public abstract void drop(int paramInt1, int paramInt2);
  }

  public static abstract interface RemoveListener
  {
    public abstract void remove(int paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.views.layouts.DnDGridView
 * JD-Core Version:    0.6.2
 */