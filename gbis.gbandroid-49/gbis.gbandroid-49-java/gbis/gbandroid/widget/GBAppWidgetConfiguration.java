package gbis.gbandroid.widget;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import gbis.gbandroid.activities.base.GBActivity;

public class GBAppWidgetConfiguration extends GBActivity
  implements View.OnClickListener
{
  int a = 0;
  private RelativeLayout b;
  private RelativeLayout c;
  private RelativeLayout d;

  static int a(Context paramContext, int paramInt)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getInt("prefix_" + paramInt, 0);
  }

  private void a()
  {
    this.b = ((RelativeLayout)findViewById(2131165542));
    this.c = ((RelativeLayout)findViewById(2131165540));
    this.d = ((RelativeLayout)findViewById(2131165544));
    this.b.setOnClickListener(this);
    this.d.setOnClickListener(this);
    this.c.setOnClickListener(this);
  }

  private static void a(Context paramContext, int paramInt1, int paramInt2)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    localEditor.putInt("prefix_" + paramInt1, paramInt2);
    localEditor.commit();
  }

  public void onClick(View paramView)
  {
    int i;
    if (paramView == this.b)
      i = 94345;
    while (true)
    {
      a(this, this.a, i);
      GBAppWidgetProvider.a(this, i, this.a);
      Intent localIntent = new Intent();
      localIntent.putExtra("appWidgetId", this.a);
      setResult(-1, localIntent);
      finish();
      return;
      if (paramView == this.c)
      {
        i = 24213;
      }
      else
      {
        RelativeLayout localRelativeLayout = this.d;
        i = 0;
        if (paramView == localRelativeLayout)
          i = 24214;
      }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903123);
    a();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.a = localBundle.getInt("appWidgetId", 0);
    if (this.a == 0)
      finish();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return "";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.GBAppWidgetConfiguration
 * JD-Core Version:    0.6.2
 */