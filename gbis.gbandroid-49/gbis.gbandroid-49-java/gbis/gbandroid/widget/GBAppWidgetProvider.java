package gbis.gbandroid.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import gbis.gbandroid.entities.StationMessage;
import gbis.gbandroid.exceptions.CustomConnectionException;

public class GBAppWidgetProvider extends AppWidgetProvider
{
  private static int[] a = { 94345, 24212, 24213, 24214 };
  private static int b = 0;

  static void a(Context paramContext, int paramInt1, int paramInt2)
  {
    Log.d("ExampleAppWidgetProvider", "lalala " + paramInt1 + " lalala " + paramInt2);
    Intent localIntent = new Intent(paramContext, UpdateService.class);
    localIntent.putExtra("smId", paramInt1);
    localIntent.putExtra("appWidgetId", paramInt2);
    PendingIntent localPendingIntent = PendingIntent.getService(paramContext, 0, localIntent, 0);
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    localAlarmManager.cancel(localPendingIntent);
    localAlarmManager.setRepeating(3, SystemClock.elapsedRealtime(), 10000L, localPendingIntent);
  }

  static void a(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, StationMessage paramStationMessage)
  {
    Log.d("ExampleAppWidgetProvider", "updateAppWidget appWidgetId=" + paramInt + " titlePrefix=" + paramStationMessage.getStationId());
    RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903129);
    localRemoteViews.setTextViewText(2131165570, "Station: " + paramStationMessage.getStationName() + "Reg: " + paramStationMessage.getRegPrice());
    paramAppWidgetManager.updateAppWidget(paramInt, localRemoteViews);
  }

  private static void b(Context paramContext, int paramInt1, AppWidgetManager paramAppWidgetManager, int paramInt2)
  {
    new StationTask(paramContext, paramInt1, paramAppWidgetManager, paramInt2).execute(new Object[0]);
  }

  public void onDeleted(Context paramContext, int[] paramArrayOfInt)
  {
    Log.d("ExampleAppWidgetProvider", "onDeleted");
  }

  public void onDisabled(Context paramContext)
  {
    Log.d("ExampleAppWidgetProvider", "onDisabled");
    paramContext.getPackageManager().setComponentEnabledSetting(new ComponentName("gbis.gbandroid", ".widget.BroadcastReceiver"), 1, 1);
  }

  public void onEnabled(Context paramContext)
  {
    Log.d("ExampleAppWidgetProvider", "onEnabled");
    paramContext.getPackageManager().setComponentEnabledSetting(new ComponentName("gbis.gbandroid", ".widget.BroadcastReceiver"), 1, 1);
  }

  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    Log.d("ExampleAppWidgetProvider", "onUpdate");
    int i = paramArrayOfInt.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      int k = paramArrayOfInt[j];
      int m = GBAppWidgetConfiguration.a(paramContext, k);
      if (m != 0)
      {
        Intent localIntent = new Intent(paramContext, UpdateService.class);
        localIntent.putExtra("smId", m);
        localIntent.putExtra("appWidgetId", k);
        PendingIntent localPendingIntent = PendingIntent.getService(paramContext, 0, localIntent, 0);
        AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
        localAlarmManager.cancel(localPendingIntent);
        localAlarmManager.setRepeating(3, SystemClock.elapsedRealtime(), 10000L, localPendingIntent);
      }
    }
  }

  public static class StationTask extends AsyncTask<Object, Object, Boolean>
  {
    private int a = GBAppWidgetProvider.a()[GBAppWidgetProvider.b()];
    private Context b;
    private AppWidgetManager c;
    private int d;
    private StationMessage e;

    public StationTask(Context paramContext, int paramInt1, AppWidgetManager paramAppWidgetManager, int paramInt2)
    {
      this.b = paramContext;
      this.c = paramAppWidgetManager;
      this.d = paramInt2;
      GBAppWidgetProvider.a(1 + GBAppWidgetProvider.b());
      if (GBAppWidgetProvider.b() == 4)
        GBAppWidgetProvider.a(0);
    }

    protected Boolean doInBackground(Object[] paramArrayOfObject)
    {
      try
      {
        Boolean localBoolean = Boolean.valueOf(queryWebService());
        return localBoolean;
      }
      catch (CustomConnectionException localCustomConnectionException)
      {
        return Boolean.valueOf(false);
      }
      catch (Exception localException)
      {
      }
      return Boolean.valueOf(false);
    }

    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      Log.d("echale", "aca " + paramBoolean + " stationId " + this.e.getStationId());
      if (paramBoolean.booleanValue())
      {
        GBAppWidgetProvider.a(this.b, this.c, this.d, this.e);
        return;
      }
      Toast localToast = new Toast(this.b.getApplicationContext());
      localToast.setGravity(49, 0, 20);
      localToast.setDuration(1);
      localToast.setText("error");
      localToast.show();
    }

    protected boolean queryWebService()
    {
      return true;
    }
  }

  public static class UpdateService extends Service
  {
    public IBinder onBind(Intent paramIntent)
    {
      return null;
    }

    public void onStart(Intent paramIntent, int paramInt)
    {
      Bundle localBundle = paramIntent.getExtras();
      int i = localBundle.getInt("smId");
      int j = localBundle.getInt("appWidgetId");
      AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this);
      Log.d("WordWidget.UpdateService", "onStart()");
      GBAppWidgetProvider.a(this, i, localAppWidgetManager, j);
      Log.d("WordWidget.UpdateService", "widget updated");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.GBAppWidgetProvider
 * JD-Core Version:    0.6.2
 */