package gbis.gbandroid.widget;

import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.preference.PreferenceManager;
import com.google.ads.AdView;
import gbis.gbandroid.activities.favorites.Favourites;
import gbis.gbandroid.activities.favorites.PlacesList;
import gbis.gbandroid.activities.members.MemberLogin;
import gbis.gbandroid.activities.members.MemberRegistration;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.activities.search.SearchBar;
import gbis.gbandroid.activities.settings.Settings;
import gbis.gbandroid.activities.settings.ShareApp;
import gbis.gbandroid.activities.station.AddStationMap;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.services.GPSService;
import gbis.gbandroid.utils.ConnectionUtils;
import gbis.gbandroid.utils.DBHelper;

public class GBWidgetProvider extends AppWidgetProvider
{
  public static final String WIDGET_ID = "appWidgetId";
  protected static SharedPreferences prefs;
  protected static Resources res;
  protected AdView adBanner;
  protected boolean adsVisible;
  protected ResponseMessage<?> responseObject;

  protected static boolean addPlace(Context paramContext, String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.addSearchesRow(paramString1, paramString2, paramDouble1, paramDouble2);
    closeDB(localDBHelper);
    return bool;
  }

  protected static void closeDB(DBHelper paramDBHelper)
  {
    if (paramDBHelper != null)
      paramDBHelper.closeDB();
  }

  protected static boolean deleteAllPlaces(Context paramContext)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.deleteSearchesAllRows();
    closeDB(localDBHelper);
    return bool;
  }

  protected static boolean deletePlace(Context paramContext, String paramString)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.deleteSearchesRow(paramString);
    closeDB(localDBHelper);
    return bool;
  }

  protected static Cursor getAllPlaces(Context paramContext, int paramInt)
  {
    DBHelper localDBHelper = openDB(paramContext);
    Cursor localCursor = localDBHelper.getAllSearchesRows(paramInt);
    closeDB(localDBHelper);
    return localCursor;
  }

  protected static Cursor getPlace(Context paramContext, String paramString)
  {
    return openDB(paramContext).getSearchesRow(paramString);
  }

  protected static Cursor getPlaceFromCityZip(Context paramContext, String paramString)
  {
    DBHelper localDBHelper = openDB(paramContext);
    Cursor localCursor = localDBHelper.getSearchesRowFromCityZip(paramString);
    closeDB(localDBHelper);
    return localCursor;
  }

  protected static boolean incrementFrequencyOfPlace(Context paramContext, String paramString)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.incrementSearchesFrequency(paramString);
    closeDB(localDBHelper);
    return bool;
  }

  protected static DBHelper openDB(Context paramContext)
  {
    DBHelper localDBHelper = new DBHelper(paramContext);
    localDBHelper.openDB();
    return localDBHelper;
  }

  protected static boolean updatePlace(Context paramContext, String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.updateSearchesRow(paramString1, paramString2, paramDouble1, paramDouble2);
    closeDB(localDBHelper);
    return bool;
  }

  protected static boolean updatePlaceName(Context paramContext, long paramLong, String paramString)
  {
    DBHelper localDBHelper = openDB(paramContext);
    boolean bool = localDBHelper.updateSearchesName(paramLong, paramString);
    closeDB(localDBHelper);
    return bool;
  }

  protected boolean checkConnection(Context paramContext)
  {
    return ConnectionUtils.isConnectedToAny(paramContext);
  }

  protected GBWidgetProvider getActivity()
  {
    return this;
  }

  protected String getHost(Context paramContext)
  {
    return prefs.getString("host", paramContext.getString(2131296733));
  }

  protected void initialize(Context paramContext)
  {
    prefs = PreferenceManager.getDefaultSharedPreferences(paramContext);
    res = paramContext.getResources();
  }

  protected void launchSettings(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, Settings.class));
  }

  public void onDisabled(Context paramContext)
  {
    super.onDisabled(paramContext);
    paramContext.stopService(new Intent(paramContext, GPSService.class));
  }

  protected void setMessage(String paramString)
  {
  }

  protected void showAddStation(Context paramContext, Location paramLocation)
  {
    paramContext.startActivity(new GBIntent(paramContext, AddStationMap.class, paramLocation));
  }

  protected void showCityZipSearch(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, SearchBar.class));
  }

  protected void showFavourites(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, Favourites.class));
  }

  protected void showHome(Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, MainScreen.class);
    localIntent.setFlags(872415232);
    paramContext.startActivity(localIntent);
  }

  protected void showLogin(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, MemberLogin.class));
  }

  protected void showPlaces(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, PlacesList.class));
  }

  protected void showRegistration(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, MemberRegistration.class));
  }

  protected void showShare(Context paramContext)
  {
    paramContext.startActivity(new Intent(paramContext, ShareApp.class));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.GBWidgetProvider
 * JD-Core Version:    0.6.2
 */