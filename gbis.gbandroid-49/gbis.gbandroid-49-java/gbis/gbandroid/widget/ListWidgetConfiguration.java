package gbis.gbandroid.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBPreferenceActivity;
import gbis.gbandroid.entities.FavListMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.listeners.WidgetSettingsListener;
import gbis.gbandroid.queries.FavListQuery;
import gbis.gbandroid.services.GPSService;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;
import java.util.List;

public class ListWidgetConfiguration extends WidgetConfiguration
  implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener, WidgetSettingsListener
{
  public static final String DISTANCE_PREFERENCE = "widgetDistancePreference";
  public static final String FAVORITES_PREFERENCE = "widgetFavoritesPreference";
  public static final String FUEL_PREFERENCE = "widgetFuelPreference";
  public static final String LOCATION_TITLE_PREFERENCE = "widgetLocationTitle";
  public static final String MYLOCATION_PREFERENCE = "widgetMyLocationPreference";
  public static final String OTHERLOCATION_PREFERENCE = "widgetOtherLocationPreference";
  public static final String RADIUS_PREFERENCE = "widgetRadiusPreference";
  public static final String REFRESH_PREFERENCE = "widgetRefreshPreference";
  public static final String SAVEDSEARCHES_PREFERENCE = "widgetSavedSearchesPreference";
  public static final String SORT_PREFERENCE = "widgetSortPreference";
  private Preference a;
  private Preference b;
  private Preference c;
  private Preference d;
  private Preference e;
  private int f;
  private Preference g;
  private Preference h;
  private Preference i;
  private PreferenceCategory j;
  private Preference k;
  private String l;
  private List<FavListMessage> m;

  private void a()
  {
    this.a = findPreference("widgetFuelPreference");
    this.b = findPreference("widgetSortPreference");
    this.e = findPreference("widgetDistancePreference");
    this.c = findPreference("widgetRadiusPreference");
    this.d = findPreference("widgetRefreshPreference");
    this.g = findPreference("widgetMyLocationPreference");
    this.h = findPreference("widgetSavedSearchesPreference");
    this.k = findPreference("widgetFavoritesPreference");
    this.i = findPreference("widgetOtherLocationPreference");
    this.j = ((PreferenceCategory)findPreference("widgetLocationTitle"));
    this.a.setOnPreferenceClickListener(this);
    this.e.setOnPreferenceClickListener(this);
    this.b.setOnPreferenceClickListener(this);
    this.c.setOnPreferenceClickListener(this);
    this.d.setOnPreferenceClickListener(this);
    this.g.setOnPreferenceClickListener(this);
    this.h.setOnPreferenceClickListener(this);
    this.k.setOnPreferenceClickListener(this);
    this.i.setOnPreferenceClickListener(this);
    if (this.l.equals(""))
      this.k.setEnabled(false);
  }

  private void a(SeekBar paramSeekBar, TextView paramTextView)
  {
    String str;
    int n;
    if (this.mPrefs.getString("widgetDistancePreference", "").equals(""))
    {
      str = "miles";
      if (!str.equals("miles"))
        break label141;
      n = 15;
    }
    for (float f1 = 1.0F; ; f1 = 1.60934F)
    {
      paramSeekBar.setMax(n);
      paramSeekBar.setProgress(-5 + Math.round(f1 * this.mPrefs.getInt("widgetRadiusPreference", 0)));
      paramTextView.setText(String.valueOf(5 + paramSeekBar.getProgress()) + " " + str);
      paramSeekBar.setOnSeekBarChangeListener(new b(this, f1, paramTextView, str));
      return;
      str = this.mPrefs.getString("widgetDistancePreference", "");
      break;
      label141: n = 25;
    }
  }

  private void b()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903068, null);
    a((SeekBar)localView.findViewById(2131165290), (TextView)localView.findViewById(2131165291));
    localBuilder.setTitle(getString(2131296525));
    localBuilder.setContentView(localView);
    localBuilder.setPositiveButton(getString(2131296664), new a(this));
    localBuilder.create().show();
  }

  private void c()
  {
    Type localType = new c(this).getType();
    this.mResponseObject = new FavListQuery(this, this.mPrefs, localType, new Location("NoLocation")).getResponseObject();
    this.m = ((List)this.mResponseObject.getPayload());
  }

  private static String[] c(List<FavListMessage> paramList)
  {
    String[] arrayOfString = new String[paramList.size()];
    int n = paramList.size();
    for (int i1 = 0; ; i1++)
    {
      if (i1 >= n)
        return arrayOfString;
      arrayOfString[i1] = ((FavListMessage)paramList.get(i1)).getListName();
    }
  }

  private void d()
  {
    a locala = new a(this);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296402), locala);
  }

  private static String[] d(List<FavListMessage> paramList)
  {
    String[] arrayOfString = new String[paramList.size()];
    int n = paramList.size();
    for (int i1 = 0; ; i1++)
    {
      if (i1 >= n)
        return arrayOfString;
      arrayOfString[i1] = ((FavListMessage)paramList.get(i1)).getListId();
    }
  }

  protected void locationChosen(String paramString)
  {
    this.j.setTitle(getString(2131296516) + ": " + paramString);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    addPreferencesFromResource(2131034115);
    this.l = this.mPrefs.getString("member_id", "");
    a();
    String str = this.mPrefs.getString("LocationDisplayed_" + this.mAppWidgetId, "");
    if ((str != null) && (!str.equals("")))
      locationChosen(str);
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
      return super.onKeyDown(paramInt, paramKeyEvent);
    case 4:
    }
    ListWidgetProvider.a(this, this.mAppWidgetId);
    Intent localIntent = new Intent();
    localIntent.putExtra("appWidgetId", this.mAppWidgetId);
    setResult(-1, localIntent);
    finish();
    return true;
  }

  public boolean onPreferenceClick(Preference paramPreference)
  {
    if (paramPreference == this.a)
      showListDialog(this.res.getStringArray(2131099648), this.res.getStringArray(2131099649), "widgetFuelPreference", getString(2131296526));
    while (true)
    {
      return true;
      if (paramPreference == this.b)
      {
        showListDialog(this.res.getStringArray(2131099654), this.res.getStringArray(2131099655), "widgetSortPreference", getString(2131296528));
      }
      else if (paramPreference == this.e)
      {
        showListDialog(this.res.getStringArray(2131099650), this.res.getStringArray(2131099651), "widgetDistancePreference", getString(2131296527));
      }
      else if (paramPreference == this.c)
      {
        b();
      }
      else if (paramPreference == this.d)
      {
        showListDialog(this.res.getStringArray(2131099660), this.res.getStringArray(2131099661), "widgetRefreshPreference", getString(2131296530));
      }
      else if (paramPreference == this.g)
      {
        locationChosen(getString(2131296519));
        startService(new Intent(this, GPSService.class));
        storeLocation(130.0D, -90.0D);
      }
      else if (paramPreference == this.h)
      {
        showPlaces();
      }
      else if (paramPreference == this.i)
      {
        showOtherLocation(this);
      }
      else
      {
        if (paramPreference != this.k)
          break;
        d();
      }
    }
    return false;
  }

  public void onResume()
  {
    super.onResume();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this);
  }

  public void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
  {
    int n;
    int i1;
    if (paramString.equals("widgetFavoritesPreference"))
    {
      n = this.mPrefs.getInt("widgetFavoritesPreference", 0);
      i1 = this.m.size();
    }
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1);
      for (String str = ""; ; str = ((FavListMessage)this.m.get(i2)).getListName())
      {
        if (!str.equals(""))
        {
          storeLocation(str, n);
          locationChosen(str);
        }
        return;
        if (((FavListMessage)this.m.get(i2)).getListId() != n)
          break;
      }
    }
  }

  public void otherLocationEntered(String paramString)
  {
    storeLocation(paramString);
    locationChosen(paramString);
  }

  protected String setAnalyticsPageName()
  {
    return "";
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBPreferenceActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      ListWidgetConfiguration.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ListWidgetConfiguration.a(ListWidgetConfiguration.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          if (ListWidgetConfiguration.b(ListWidgetConfiguration.this).size() > 0)
          {
            String[] arrayOfString1 = ListWidgetConfiguration.a(ListWidgetConfiguration.b(ListWidgetConfiguration.this));
            String[] arrayOfString2 = ListWidgetConfiguration.b(ListWidgetConfiguration.b(ListWidgetConfiguration.this));
            ListWidgetConfiguration.a(ListWidgetConfiguration.this, arrayOfString1, arrayOfString2, "widgetFavoritesPreference", ListWidgetConfiguration.this.getString(2131296531));
            return;
          }
          ListWidgetConfiguration.this.showMessage(ListWidgetConfiguration.this.getString(2131296363));
          return;
        }
        ListWidgetConfiguration.this.showMessage();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      ListWidgetConfiguration.c(ListWidgetConfiguration.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.ListWidgetConfiguration
 * JD-Core Version:    0.6.2
 */