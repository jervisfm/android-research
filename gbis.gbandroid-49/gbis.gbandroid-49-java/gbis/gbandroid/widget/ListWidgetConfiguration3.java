package gbis.gbandroid.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import gbis.gbandroid.activities.base.GBActivitySearch;
import gbis.gbandroid.services.GPSService;

public class ListWidgetConfiguration3 extends GBActivitySearch
  implements View.OnClickListener
{
  int b = 0;

  public static PendingIntent makeControlPendingIntent(Context paramContext, String paramString, int paramInt)
  {
    Intent localIntent = new Intent(paramContext, GPSService.class);
    localIntent.setAction(paramString);
    localIntent.putExtra("appWidgetId", paramInt);
    localIntent.setData(Uri.withAppendedPath(Uri.parse("gblistwidget://widget/id/#" + paramString + paramInt), String.valueOf(paramInt)));
    return PendingIntent.getService(paramContext, 0, localIntent, 134217728);
  }

  public static void updateList(Context paramContext, int paramInt1, int paramInt2)
  {
    Intent localIntent = new Intent(paramContext, UpdateService.class);
    localIntent.putExtra("smId", paramInt1);
    localIntent.putExtra("appWidgetId", paramInt2);
    PendingIntent localPendingIntent = PendingIntent.getService(paramContext, 0, localIntent, 0);
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    localAlarmManager.cancel(localPendingIntent);
    localAlarmManager.setRepeating(3, SystemClock.elapsedRealtime(), 10000L, localPendingIntent);
  }

  public void onClick(View paramView)
  {
    ListWidgetProvider.a(this, this.b);
    Intent localIntent = new Intent();
    localIntent.putExtra("appWidgetId", this.b);
    setResult(-1, localIntent);
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.e("Chaaaaaaacnlas1", "oncreate");
    setContentView(2130903121);
    populateButtons();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.b = localBundle.getInt("appWidgetId", 0);
    if (this.b == 0)
      finish();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Log.e("Chaaaaaaacnlas1", "ondestoy");
  }

  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    Log.e("Chaaaaaaacnlas1", "onnewintent");
  }

  public void onPause()
  {
    super.onPause();
    Log.e("Chaaaaaaacnlas1", "onpause");
  }

  protected void onRestart()
  {
    super.onRestart();
    Log.e("Chaaaaaaacnlas1", "onrestart");
  }

  public void onResume()
  {
    super.onResume();
    Log.e("Chaaaaaaacnlas1", "onresume");
  }

  protected void onStop()
  {
    super.onStop();
    Log.e("Chaaaaaaacnlas1", "onstop");
  }

  protected void populateButtons()
  {
    super.populateButtons();
    this.buttonNearMe = findViewById(2131165446);
    this.buttonNearMe.setOnClickListener(this);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return "";
  }

  public static class UpdateService extends Service
  {
    public IBinder onBind(Intent paramIntent)
    {
      return null;
    }

    public void onStart(Intent paramIntent, int paramInt)
    {
      Log.d("WordWidget.UpdateService", "onStart()");
      Log.d("WordWidget.UpdateService", "widget updated");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.ListWidgetConfiguration3
 * JD-Core Version:    0.6.2
 */