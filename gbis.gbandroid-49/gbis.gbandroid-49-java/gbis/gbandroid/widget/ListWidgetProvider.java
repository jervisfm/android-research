package gbis.gbandroid.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.InitScreen;
import gbis.gbandroid.entities.FavStationMessage;
import gbis.gbandroid.entities.FavStationResults;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.exceptions.CustomConnectionException;
import gbis.gbandroid.queries.FavStationListQuery;
import gbis.gbandroid.queries.ListQuery;
import gbis.gbandroid.services.GPSService;
import gbis.gbandroid.utils.VerifyFields;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ListWidgetProvider extends GBWidgetProvider
{
  private static PendingIntent a(Context paramContext, int paramInt1, int paramInt2)
  {
    Intent localIntent;
    if (paramInt2 == 0)
    {
      localIntent = new Intent(paramContext, ListWidgetConfiguration.class);
      localIntent.putExtra("appWidgetId", paramInt1);
      localIntent.addFlags(268435456);
    }
    while (true)
    {
      return PendingIntent.getActivity(paramContext, 0, localIntent, 268435456);
      if (paramInt2 != 1)
        break;
      localIntent = new Intent(paramContext, InitScreen.class);
    }
    return null;
  }

  private static PendingIntent a(Context paramContext, int paramInt1, int paramInt2, List<Station> paramList, int paramInt3)
  {
    Intent localIntent = new Intent(paramContext, ChangeStationService.class);
    localIntent.putExtra("list", a(paramList));
    localIntent.putExtra("appWidgetId", paramInt2);
    localIntent.setData(Uri.withAppendedPath(Uri.parse("GasBuddyAndroid://widget/id/"), String.valueOf(paramInt2) + "/" + paramInt3));
    if (paramInt3 == 1)
      paramInt1++;
    while (true)
    {
      localIntent.putExtra("position", paramInt1);
      return PendingIntent.getService(paramContext, 0, localIntent, 134217728);
      if (paramInt3 == -1)
        paramInt1--;
    }
  }

  static void a(Context paramContext, int paramInt)
  {
    PendingIntent localPendingIntent = d(paramContext, paramInt);
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    localAlarmManager.cancel(localPendingIntent);
    localAlarmManager.setRepeating(3, SystemClock.elapsedRealtime(), prefs.getInt("widgetRefreshPreference", 3600000), localPendingIntent);
  }

  // ERROR //
  static void a(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt1, List<Station> paramList, int paramInt2)
  {
    // Byte code:
    //   0: new 153	android/widget/RemoteViews
    //   3: dup
    //   4: aload_0
    //   5: invokevirtual 156	android/content/Context:getPackageName	()Ljava/lang/String;
    //   8: ldc 157
    //   10: invokespecial 160	android/widget/RemoteViews:<init>	(Ljava/lang/String;I)V
    //   13: astore 5
    //   15: aload 5
    //   17: ldc 161
    //   19: aload_0
    //   20: iload_2
    //   21: iconst_0
    //   22: invokestatic 163	gbis/gbandroid/widget/ListWidgetProvider:a	(Landroid/content/Context;II)Landroid/app/PendingIntent;
    //   25: invokevirtual 167	android/widget/RemoteViews:setOnClickPendingIntent	(ILandroid/app/PendingIntent;)V
    //   28: aload 5
    //   30: ldc 168
    //   32: aload_0
    //   33: iload_2
    //   34: iconst_1
    //   35: invokestatic 163	gbis/gbandroid/widget/ListWidgetProvider:a	(Landroid/content/Context;II)Landroid/app/PendingIntent;
    //   38: invokevirtual 167	android/widget/RemoteViews:setOnClickPendingIntent	(ILandroid/app/PendingIntent;)V
    //   41: aload 5
    //   43: ldc 169
    //   45: aload_0
    //   46: iload_2
    //   47: invokestatic 108	gbis/gbandroid/widget/ListWidgetProvider:d	(Landroid/content/Context;I)Landroid/app/PendingIntent;
    //   50: invokevirtual 167	android/widget/RemoteViews:setOnClickPendingIntent	(ILandroid/app/PendingIntent;)V
    //   53: aload_3
    //   54: ifnull +718 -> 772
    //   57: iload 4
    //   59: aload_3
    //   60: invokeinterface 175 1 0
    //   65: if_icmpne +738 -> 803
    //   68: iconst_0
    //   69: istore 6
    //   71: iload 6
    //   73: iconst_m1
    //   74: if_icmpne +722 -> 796
    //   77: iconst_m1
    //   78: aload_3
    //   79: invokeinterface 175 1 0
    //   84: iadd
    //   85: istore 7
    //   87: aload_3
    //   88: invokeinterface 175 1 0
    //   93: ifeq +430 -> 523
    //   96: aload_3
    //   97: iload 7
    //   99: invokeinterface 179 2 0
    //   104: checkcast 181	gbis/gbandroid/entities/Station
    //   107: astore 8
    //   109: aload 5
    //   111: ldc 182
    //   113: aload 8
    //   115: invokevirtual 185	gbis/gbandroid/entities/Station:getStationName	()Ljava/lang/String;
    //   118: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   121: getstatic 132	gbis/gbandroid/widget/ListWidgetProvider:prefs	Landroid/content/SharedPreferences;
    //   124: ldc 191
    //   126: ldc 193
    //   128: invokeinterface 197 3 0
    //   133: astore 9
    //   135: aload 8
    //   137: aload_0
    //   138: aload 9
    //   140: invokevirtual 201	gbis/gbandroid/entities/Station:getPrice	(Landroid/content/Context;Ljava/lang/String;)D
    //   143: dconst_0
    //   144: dcmpl
    //   145: ifle +430 -> 575
    //   148: aload 8
    //   150: invokevirtual 204	gbis/gbandroid/entities/Station:getCountry	()Ljava/lang/String;
    //   153: ldc 206
    //   155: invokevirtual 210	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   158: ifeq +373 -> 531
    //   161: aload 5
    //   163: ldc 211
    //   165: new 57	java/lang/StringBuilder
    //   168: dup
    //   169: invokespecial 212	java/lang/StringBuilder:<init>	()V
    //   172: aload 8
    //   174: aload_0
    //   175: aload 9
    //   177: invokevirtual 201	gbis/gbandroid/entities/Station:getPrice	(Landroid/content/Context;Ljava/lang/String;)D
    //   180: iconst_2
    //   181: invokestatic 218	gbis/gbandroid/utils/VerifyFields:doubleToScale	(DI)Ljava/math/BigDecimal;
    //   184: invokevirtual 221	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   187: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   190: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   193: aload 5
    //   195: ldc 222
    //   197: iconst_0
    //   198: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   201: aload 5
    //   203: ldc 227
    //   205: aload 8
    //   207: aload_0
    //   208: aload 9
    //   210: invokevirtual 231	gbis/gbandroid/entities/Station:getTimeSpottedConverted	(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    //   213: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   216: aload 5
    //   218: ldc 227
    //   220: iconst_0
    //   221: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   224: aload 5
    //   226: ldc 232
    //   228: aload 8
    //   230: invokevirtual 235	gbis/gbandroid/entities/Station:getAddress	()Ljava/lang/String;
    //   233: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   236: new 57	java/lang/StringBuilder
    //   239: dup
    //   240: aload 8
    //   242: invokevirtual 238	gbis/gbandroid/entities/Station:getCity	()Ljava/lang/String;
    //   245: invokestatic 66	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   248: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   251: astore 10
    //   253: aload 8
    //   255: invokevirtual 241	gbis/gbandroid/entities/Station:getState	()Ljava/lang/String;
    //   258: ifnull +347 -> 605
    //   261: aload 8
    //   263: invokevirtual 241	gbis/gbandroid/entities/Station:getState	()Ljava/lang/String;
    //   266: ldc 193
    //   268: invokevirtual 210	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   271: ifne +334 -> 605
    //   274: new 57	java/lang/StringBuilder
    //   277: dup
    //   278: ldc 243
    //   280: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   283: aload 8
    //   285: invokevirtual 241	gbis/gbandroid/entities/Station:getState	()Ljava/lang/String;
    //   288: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   291: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   294: astore 11
    //   296: aload 5
    //   298: ldc 244
    //   300: aload 10
    //   302: aload 11
    //   304: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   307: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   310: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   313: aload 8
    //   315: instanceof 246
    //   318: ifeq +433 -> 751
    //   321: aload 8
    //   323: checkcast 246	gbis/gbandroid/entities/ListMessage
    //   326: invokevirtual 250	gbis/gbandroid/entities/ListMessage:getDistance	()D
    //   329: dconst_0
    //   330: dcmpl
    //   331: ifeq +420 -> 751
    //   334: getstatic 132	gbis/gbandroid/widget/ListWidgetProvider:prefs	Landroid/content/SharedPreferences;
    //   337: ldc 252
    //   339: ldc 193
    //   341: invokeinterface 197 3 0
    //   346: astore 15
    //   348: aload 15
    //   350: ldc 193
    //   352: invokevirtual 210	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   355: ifne +296 -> 651
    //   358: aload 15
    //   360: ldc 254
    //   362: invokevirtual 210	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   365: ifeq +247 -> 612
    //   368: new 57	java/lang/StringBuilder
    //   371: dup
    //   372: aload 8
    //   374: checkcast 246	gbis/gbandroid/entities/ListMessage
    //   377: invokevirtual 250	gbis/gbandroid/entities/ListMessage:getDistance	()D
    //   380: invokestatic 258	gbis/gbandroid/utils/VerifyFields:roundTwoDecimals	(D)Ljava/lang/String;
    //   383: invokestatic 66	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   386: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   389: ldc_w 260
    //   392: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   395: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   398: astore 17
    //   400: aload 17
    //   402: astore 12
    //   404: aload 5
    //   406: ldc_w 261
    //   409: aload 12
    //   411: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   414: aload 5
    //   416: ldc_w 262
    //   419: ldc_w 264
    //   422: new 57	java/lang/StringBuilder
    //   425: dup
    //   426: ldc_w 266
    //   429: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   432: aload 8
    //   434: invokevirtual 185	gbis/gbandroid/entities/Station:getStationName	()Ljava/lang/String;
    //   437: invokevirtual 269	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   440: ldc_w 271
    //   443: ldc 193
    //   445: invokevirtual 274	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   448: ldc_w 276
    //   451: ldc 193
    //   453: invokevirtual 274	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   456: ldc_w 278
    //   459: ldc 193
    //   461: invokevirtual 274	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   464: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   467: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   470: invokevirtual 284	java/lang/Class:getField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   473: aconst_null
    //   474: invokevirtual 289	java/lang/reflect/Field:getInt	(Ljava/lang/Object;)I
    //   477: invokevirtual 292	android/widget/RemoteViews:setImageViewResource	(II)V
    //   480: aload 5
    //   482: ldc_w 262
    //   485: iconst_0
    //   486: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   489: aload 5
    //   491: ldc_w 293
    //   494: aload_0
    //   495: iload 7
    //   497: iload_2
    //   498: aload_3
    //   499: iconst_1
    //   500: invokestatic 295	gbis/gbandroid/widget/ListWidgetProvider:a	(Landroid/content/Context;IILjava/util/List;I)Landroid/app/PendingIntent;
    //   503: invokevirtual 167	android/widget/RemoteViews:setOnClickPendingIntent	(ILandroid/app/PendingIntent;)V
    //   506: aload 5
    //   508: ldc_w 296
    //   511: aload_0
    //   512: iload 7
    //   514: iload_2
    //   515: aload_3
    //   516: iconst_m1
    //   517: invokestatic 295	gbis/gbandroid/widget/ListWidgetProvider:a	(Landroid/content/Context;IILjava/util/List;I)Landroid/app/PendingIntent;
    //   520: invokevirtual 167	android/widget/RemoteViews:setOnClickPendingIntent	(ILandroid/app/PendingIntent;)V
    //   523: aload_1
    //   524: iload_2
    //   525: aload 5
    //   527: invokevirtual 302	android/appwidget/AppWidgetManager:updateAppWidget	(ILandroid/widget/RemoteViews;)V
    //   530: return
    //   531: aload 5
    //   533: ldc 211
    //   535: new 57	java/lang/StringBuilder
    //   538: dup
    //   539: invokespecial 212	java/lang/StringBuilder:<init>	()V
    //   542: aload 8
    //   544: aload_0
    //   545: aload 9
    //   547: invokevirtual 201	gbis/gbandroid/entities/Station:getPrice	(Landroid/content/Context;Ljava/lang/String;)D
    //   550: iconst_1
    //   551: invokestatic 218	gbis/gbandroid/utils/VerifyFields:doubleToScale	(DI)Ljava/math/BigDecimal;
    //   554: invokevirtual 221	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   557: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   560: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   563: aload 5
    //   565: ldc 222
    //   567: bipush 8
    //   569: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   572: goto -371 -> 201
    //   575: aload 5
    //   577: ldc 211
    //   579: ldc_w 304
    //   582: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   585: aload 5
    //   587: ldc 222
    //   589: bipush 8
    //   591: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   594: aload 5
    //   596: ldc 227
    //   598: iconst_4
    //   599: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   602: goto -378 -> 224
    //   605: ldc 193
    //   607: astore 11
    //   609: goto -313 -> 296
    //   612: new 57	java/lang/StringBuilder
    //   615: dup
    //   616: ldc2_w 305
    //   619: aload 8
    //   621: checkcast 246	gbis/gbandroid/entities/ListMessage
    //   624: invokevirtual 250	gbis/gbandroid/entities/ListMessage:getDistance	()D
    //   627: dmul
    //   628: invokestatic 258	gbis/gbandroid/utils/VerifyFields:roundTwoDecimals	(D)Ljava/lang/String;
    //   631: invokestatic 66	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   634: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   637: ldc_w 308
    //   640: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   643: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   646: astore 12
    //   648: goto -244 -> 404
    //   651: aload 8
    //   653: invokevirtual 204	gbis/gbandroid/entities/Station:getCountry	()Ljava/lang/String;
    //   656: ldc 206
    //   658: invokevirtual 210	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   661: ifeq +38 -> 699
    //   664: new 57	java/lang/StringBuilder
    //   667: dup
    //   668: aload 8
    //   670: checkcast 246	gbis/gbandroid/entities/ListMessage
    //   673: invokevirtual 250	gbis/gbandroid/entities/ListMessage:getDistance	()D
    //   676: invokestatic 258	gbis/gbandroid/utils/VerifyFields:roundTwoDecimals	(D)Ljava/lang/String;
    //   679: invokestatic 66	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   682: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   685: ldc_w 260
    //   688: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   691: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   694: astore 12
    //   696: goto -292 -> 404
    //   699: new 57	java/lang/StringBuilder
    //   702: dup
    //   703: ldc2_w 305
    //   706: aload 8
    //   708: checkcast 246	gbis/gbandroid/entities/ListMessage
    //   711: invokevirtual 250	gbis/gbandroid/entities/ListMessage:getDistance	()D
    //   714: dmul
    //   715: invokestatic 258	gbis/gbandroid/utils/VerifyFields:roundTwoDecimals	(D)Ljava/lang/String;
    //   718: invokestatic 66	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   721: invokespecial 69	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   724: ldc_w 308
    //   727: invokevirtual 75	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   730: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   733: astore 16
    //   735: aload 16
    //   737: astore 12
    //   739: goto -335 -> 404
    //   742: astore 14
    //   744: ldc 193
    //   746: astore 12
    //   748: goto -344 -> 404
    //   751: ldc 193
    //   753: astore 12
    //   755: goto -351 -> 404
    //   758: astore 13
    //   760: aload 5
    //   762: ldc_w 262
    //   765: iconst_4
    //   766: invokevirtual 226	android/widget/RemoteViews:setViewVisibility	(II)V
    //   769: goto -280 -> 489
    //   772: aload 5
    //   774: ldc 182
    //   776: ldc_w 310
    //   779: invokevirtual 189	android/widget/RemoteViews:setTextViewText	(ILjava/lang/CharSequence;)V
    //   782: aload_0
    //   783: ldc_w 312
    //   786: iconst_0
    //   787: invokestatic 318	android/widget/Toast:makeText	(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    //   790: invokevirtual 321	android/widget/Toast:show	()V
    //   793: goto -270 -> 523
    //   796: iload 6
    //   798: istore 7
    //   800: goto -713 -> 87
    //   803: iload 4
    //   805: istore 6
    //   807: goto -736 -> 71
    //
    // Exception table:
    //   from	to	target	type
    //   334	400	742	java/lang/Exception
    //   612	648	742	java/lang/Exception
    //   651	696	742	java/lang/Exception
    //   699	735	742	java/lang/Exception
    //   414	489	758	java/lang/Exception
  }

  static void a(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt1, byte[] paramArrayOfByte, int paramInt2)
  {
    try
    {
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new ByteArrayInputStream(paramArrayOfByte));
      List localList = (List)localObjectInputStream.readObject();
      localObjectInputStream.close();
      a(paramContext, paramAppWidgetManager, paramInt1, localList, paramInt2);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  private static byte[] a(List<Station> paramList)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(localByteArrayOutputStream);
      localObjectOutputStream.writeObject(paramList);
      localObjectOutputStream.close();
      byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
      return arrayOfByte;
    }
    catch (IOException localIOException)
    {
    }
    return null;
  }

  private static List<ListMessage> b(Context paramContext, String paramString, GeoPoint paramGeoPoint)
  {
    try
    {
      Integer.parseInt(paramString);
      List localList = b(paramContext, "", "", paramString, paramGeoPoint);
      return localList;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      String str1 = VerifyFields.checkPostalCode(paramString);
      if (!str1.equals(""))
        return b(paramContext, "", "", str1, paramGeoPoint);
      int i = paramString.indexOf(",");
      if (i != -1)
      {
        String str2 = paramString.substring(0, i);
        Object localObject;
        if ((paramString.length() > i + 3) && (paramString.substring(i + 1, i + 2).contains(" ")))
        {
          String str4 = paramString.substring(i + 2);
          paramString = str2;
          localObject = str4;
        }
        while (true)
        {
          return b(paramContext, paramString, (String)localObject, "", paramGeoPoint);
          if ((paramString.length() > i + 2) && (!paramString.substring(i + 1, i + 2).contains(" ")))
          {
            String str3 = paramString.substring(i + 1);
            paramString = str2;
            localObject = str3;
          }
          else
          {
            localObject = "";
          }
        }
      }
    }
    return b(paramContext, paramString, "", "", paramGeoPoint);
  }

  private static List<ListMessage> b(Context paramContext, String paramString1, String paramString2, String paramString3, GeoPoint paramGeoPoint)
  {
    Type localType = new d().getType();
    Location localLocation = new Location("Widget");
    localLocation.setLatitude(paramGeoPoint.getLatitudeE6() / 1000000.0D);
    localLocation.setLongitude(paramGeoPoint.getLongitudeE6() / 1000000.0D);
    return ((ListResults)new ListQuery(paramContext, prefs, localType, localLocation).getResponseObject(paramString1, paramString2, paramString3, paramGeoPoint).getPayload()).getListMessages();
  }

  private static void b(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, String paramString)
  {
    Cursor localCursor = getPlace(paramContext, paramString);
    if (localCursor == null)
    {
      startCityZipThread(paramContext, paramAppWidgetManager, paramInt, paramString);
      return;
    }
    if (localCursor.getCount() <= 0)
    {
      localCursor.close();
      startCityZipThread(paramContext, paramAppWidgetManager, paramInt, paramString);
      return;
    }
    localCursor.moveToFirst();
    searchUsingPlaces(paramContext, paramAppWidgetManager, paramInt, localCursor);
  }

  private static List<FavStationMessage> c(Context paramContext, int paramInt)
  {
    Type localType = new e().getType();
    return ((FavStationResults)new FavStationListQuery(paramContext, prefs, localType, new Location("NoLocation")).getResponseObject(paramInt, 1, 100).getPayload()).getFavStationMessage();
  }

  private static PendingIntent d(Context paramContext, int paramInt)
  {
    Intent localIntent = new Intent(paramContext, UpdateService.class);
    localIntent.putExtra("appWidgetId", paramInt);
    return PendingIntent.getService(paramContext, 0, localIntent, 134217728);
  }

  protected static void searchUsingPlaces(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, Cursor paramCursor)
  {
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndex("table_column_latitude"));
    double d2 = paramCursor.getDouble(paramCursor.getColumnIndex("table_column_longitude"));
    Log.e("Places", d1 + ", " + d2);
    if ((d1 == 0.0D) && (d2 == 0.0D))
    {
      String str = paramCursor.getString(paramCursor.getColumnIndex("table_column_city_zip"));
      Log.e("Places2", str);
      startCityZipThread(paramContext, paramAppWidgetManager, paramInt, str);
    }
    while (true)
    {
      incrementFrequencyOfPlace(paramContext, paramCursor.getString(paramCursor.getColumnIndex("table_column_name")));
      paramCursor.close();
      return;
      Log.e("Places3", d1 + ", " + d2);
      startNearMeThread(paramContext, paramAppWidgetManager, paramInt, new GeoPoint((int)(d1 * 1000000.0D), (int)(d2 * 1000000.0D)));
    }
  }

  protected static void startCityZipThread(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, String paramString)
  {
    new ListTask(paramContext, paramAppWidgetManager, paramInt, paramString).execute(new Object[0]);
  }

  protected static void startFavThread(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt1, int paramInt2)
  {
    new FavoriteTask(paramContext, paramAppWidgetManager, paramInt1, paramInt2).execute(new Object[0]);
  }

  protected static void startNearMeThread(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, GeoPoint paramGeoPoint)
  {
    new ListTask(paramContext, paramAppWidgetManager, paramInt, paramGeoPoint).execute(new Object[0]);
  }

  public void onDeleted(Context paramContext, int[] paramArrayOfInt)
  {
    super.onDeleted(paramContext, paramArrayOfInt);
  }

  public void onDisabled(Context paramContext)
  {
    super.onDisabled(paramContext);
  }

  public void onEnabled(Context paramContext)
  {
    super.onEnabled(paramContext);
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    super.onReceive(paramContext, paramIntent);
  }

  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    super.onUpdate(paramContext, paramAppWidgetManager, paramArrayOfInt);
    if (prefs == null)
      initialize(paramContext);
  }

  public static class ChangeStationService extends Service
  {
    public IBinder onBind(Intent paramIntent)
    {
      return null;
    }

    public void onStart(Intent paramIntent, int paramInt)
    {
      Bundle localBundle = paramIntent.getExtras();
      int i = localBundle.getInt("appWidgetId");
      int j = localBundle.getInt("position", 0);
      byte[] arrayOfByte = localBundle.getByteArray("list");
      AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this);
      if (arrayOfByte == null)
      {
        ListWidgetProvider.a(this, localAppWidgetManager, i, ListWidgetProvider.prefs.getString("LocationDisplayed_" + i, ""));
        return;
      }
      ListWidgetProvider.a(this, localAppWidgetManager, i, arrayOfByte, j);
    }
  }

  public static class FavoriteTask extends AsyncTask<Object, Object, Boolean>
  {
    private int a;
    private Context b;
    private AppWidgetManager c;
    private int d;
    private List<FavStationMessage> e;

    public FavoriteTask(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt1, int paramInt2)
    {
      this.b = paramContext;
      this.c = paramAppWidgetManager;
      this.d = paramInt1;
      this.a = paramInt2;
    }

    protected Boolean doInBackground(Object[] paramArrayOfObject)
    {
      try
      {
        Boolean localBoolean = Boolean.valueOf(queryWebService());
        return localBoolean;
      }
      catch (CustomConnectionException localCustomConnectionException)
      {
        return Boolean.valueOf(false);
      }
      catch (Exception localException)
      {
      }
      return Boolean.valueOf(false);
    }

    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      ArrayList localArrayList = new ArrayList();
      if (paramBoolean.booleanValue())
      {
        int i = this.e.size();
        for (int j = 0; ; j++)
        {
          if (j >= i)
          {
            ListWidgetProvider.a(this.b, this.c, this.d, localArrayList, 0);
            return;
          }
          localArrayList.add((Station)this.e.get(j));
        }
      }
      Log.e("GBAPP", "error");
      ListWidgetProvider.a(this.b, this.c, this.d, localArrayList, 0);
    }

    protected boolean queryWebService()
    {
      this.e = ListWidgetProvider.b(this.b, this.a);
      return true;
    }
  }

  public static class ListTask extends AsyncTask<Object, Object, Boolean>
  {
    private String a;
    private Context b;
    private AppWidgetManager c;
    private int d;
    private List<ListMessage> e;
    private GeoPoint f;

    public ListTask(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt)
    {
      a(paramContext, paramAppWidgetManager, paramInt);
    }

    public ListTask(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, GeoPoint paramGeoPoint)
    {
      this.f = paramGeoPoint;
      a(paramContext, paramAppWidgetManager, paramInt);
    }

    public ListTask(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, String paramString)
    {
      this.a = paramString;
      a(paramContext, paramAppWidgetManager, paramInt);
    }

    private void a(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt)
    {
      this.b = paramContext;
      this.c = paramAppWidgetManager;
      this.d = paramInt;
    }

    protected Boolean doInBackground(Object[] paramArrayOfObject)
    {
      try
      {
        Boolean localBoolean = Boolean.valueOf(queryWebService());
        return localBoolean;
      }
      catch (CustomConnectionException localCustomConnectionException)
      {
        return Boolean.valueOf(false);
      }
      catch (Exception localException)
      {
      }
      return Boolean.valueOf(false);
    }

    protected void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      ArrayList localArrayList = new ArrayList();
      if (paramBoolean.booleanValue())
      {
        int i = this.e.size();
        for (int j = 0; ; j++)
        {
          if (j >= i)
          {
            ListWidgetProvider.a(this.b, this.c, this.d, localArrayList, 0);
            return;
          }
          localArrayList.add((Station)this.e.get(j));
        }
      }
      Log.e("GBAPP", "error");
      ListWidgetProvider.a(this.b, this.c, this.d, localArrayList, 0);
    }

    protected boolean queryWebService()
    {
      if (this.f == null)
        this.f = new GeoPoint(0, 0);
      if (this.a != null);
      for (this.e = ListWidgetProvider.a(this.b, this.a, this.f); ; this.e = ListWidgetProvider.a(this.b, "", "", "", this.f))
      {
        boolean bool = true;
        int j;
        do
        {
          int i;
          do
          {
            return bool;
            i = this.f.getLatitudeE6();
            bool = false;
          }
          while (i == 0);
          j = this.f.getLongitudeE6();
          bool = false;
        }
        while (j == 0);
      }
    }
  }

  public static class UpdateService extends Service
  {
    private static GPSService b;
    private static int c;
    private static SharedPreferences d;
    final Handler a = new g(this);
    private ServiceConnection e = new f(this);

    private void b()
    {
      try
      {
        Intent localIntent = new Intent(this, GPSService.class);
        localIntent.putExtra(GPSService.LOCATION_UPDATE_TIME, d.getInt("widgetRefreshPreference", 3600000) / 2);
        bindService(localIntent, this.e, 1);
        return;
      }
      catch (Exception localException)
      {
        Log.e("ERRORSISIMO", "asi es papa " + localException);
        localException.printStackTrace();
      }
    }

    private void c()
    {
      b.startLocationListener(d.getInt("widgetRefreshPreference", 3600000) / 2);
      AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this);
      Log.e("WIDGETLISTGPS", "retrieving gps " + System.currentTimeMillis());
      Location localLocation1 = getSelfLocation();
      if (localLocation1 == null)
        Log.e("WIDGETLISTGPS", "segundo");
      for (Location localLocation2 = getLastKnownLocation(); ; localLocation2 = localLocation1)
      {
        if (localLocation2 != null);
        for (GeoPoint localGeoPoint = new GeoPoint((int)(1000000.0D * localLocation2.getLatitude()), (int)(1000000.0D * localLocation2.getLongitude())); ; localGeoPoint = new GeoPoint(0, 0))
        {
          Log.e("WIDGETLISTGPS", "gps2 " + localGeoPoint + " " + System.currentTimeMillis());
          ListWidgetProvider.startNearMeThread(this, localAppWidgetManager, c, localGeoPoint);
          b.stopLocationListener();
          d();
          return;
        }
      }
    }

    private void d()
    {
      new a(this.a).start();
    }

    protected static Location getLastKnownLocation()
    {
      if (b != null)
        return b.getLastKnownLocation();
      return null;
    }

    protected static Location getSelfLocation()
    {
      if (b != null)
        Log.e("GPSBinder", "este es el mservice " + b.toString());
      while (b != null)
      {
        return b.getSelfLocation();
        Log.e("GPSBinder", "mservice es null");
      }
      return null;
    }

    public IBinder onBind(Intent paramIntent)
    {
      return null;
    }

    public void onCreate()
    {
      super.onCreate();
      d = PreferenceManager.getDefaultSharedPreferences(this);
      b();
    }

    public void onStart(Intent paramIntent, int paramInt)
    {
      c = paramIntent.getExtras().getInt("appWidgetId");
      int i = paramIntent.getIntExtra("position", 0);
      byte[] arrayOfByte = paramIntent.getByteArrayExtra("list");
      AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this);
      Log.e("WIDGET", c + " ");
      if (arrayOfByte == null)
      {
        String str = d.getString("LocationDisplayed_" + c, "");
        int j = d.getInt("LocationFavorite_" + c, 0);
        if (j == 0)
        {
          if (str.equals(ListWidgetProvider.res.getString(2131296519)))
          {
            if (b != null)
              c();
            return;
          }
          ListWidgetProvider.a(this, localAppWidgetManager, c, str);
          return;
        }
        ListWidgetProvider.startFavThread(this, localAppWidgetManager, c, j);
        return;
      }
      ListWidgetProvider.a(this, localAppWidgetManager, c, arrayOfByte, i);
    }

    private final class a extends Thread
    {
      Handler a;
      int b;

      a(Handler arg2)
      {
        Object localObject;
        this.a = localObject;
        this.b = 10000;
      }

      public final void run()
      {
        try
        {
          Log.e("GPSCLOCLTHREAD ", "entrando");
          sleep(this.b);
          ListWidgetProvider.UpdateService.a().startLocationListener(1000L);
          sleep(this.b);
          Log.e("GPSCLOCLTHREAD ", "saliendo");
          this.a.sendEmptyMessage(0);
          return;
        }
        catch (InterruptedException localInterruptedException)
        {
          while (true)
            localInterruptedException.printStackTrace();
        }
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.ListWidgetProvider
 * JD-Core Version:    0.6.2
 */