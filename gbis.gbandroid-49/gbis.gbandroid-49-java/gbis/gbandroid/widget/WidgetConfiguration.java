package gbis.gbandroid.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBPreferenceActivity;
import gbis.gbandroid.activities.favorites.PlacesList;
import gbis.gbandroid.entities.AutoCompMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.listeners.WidgetSettingsListener;
import gbis.gbandroid.queries.AutoCompleteCityQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class WidgetConfiguration extends GBPreferenceActivity
{
  public static final String LOCATION_CITYZIP = "LocationCityZip_";
  public static final String LOCATION_DISPLAYED = "LocationDisplayed_";
  public static final String LOCATION_FAVORITE = "LocationFavorite_";
  public static final String LOCATION_LATITUDE = "LocationLatitude_";
  public static final String LOCATION_LONGITUDE = "LocationLongitude_";
  protected static final int PLACES = 1;
  private AutoCompleteTextView a;
  protected int mAppWidgetId;

  private List<AutoCompMessage> a()
  {
    Type localType = new i(this).getType();
    return (List)new AutoCompleteCityQuery(this, this.mPrefs, localType, null).getResponseObject(this.a.getText().toString()).getPayload();
  }

  private void b()
  {
    this.a.setThreshold(2);
    this.a.addTextChangedListener(new j(this));
    ArrayList localArrayList = new ArrayList();
    ArrayAdapter localArrayAdapter = new ArrayAdapter(getApplicationContext(), 2130903122, localArrayList);
    this.a.setAdapter(localArrayAdapter);
  }

  protected abstract void locationChosen(String paramString);

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 1:
    }
    Bundle localBundle;
    do
    {
      do
        return;
      while (paramInt2 != -1);
      localBundle = paramIntent.getExtras();
    }
    while (localBundle == null);
    String str1 = localBundle.getString("place name");
    String str2 = localBundle.getString("city");
    if (str2 == null)
      str2 = "";
    showMessage(str1 + " " + str2 + " " + localBundle.getDouble("center latitude") + " " + localBundle.getDouble("center longitude"));
    storeLocation(str1, str2, localBundle.getDouble("center latitude"), localBundle.getDouble("center longitude"));
    locationChosen(str1);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.mAppWidgetId = localBundle.getInt("appWidgetId", 0);
    if (this.mAppWidgetId == 0)
    {
      this.mAppWidgetId = localBundle.getInt("appWidgetId", 0);
      if (this.mAppWidgetId == 0)
        finish();
    }
  }

  protected void showOtherLocation(WidgetSettingsListener paramWidgetSettingsListener)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903057, null);
    TextView localTextView = (TextView)localView.findViewById(2131165246);
    this.a = ((AutoCompleteTextView)localView.findViewById(2131165247));
    localBuilder.setTitle(getString(2131296516));
    localBuilder.setContentView(localView);
    localTextView.setText(getString(2131296514));
    b();
    localBuilder.setPositiveButton(getString(2131296664), new h(this, paramWidgetSettingsListener));
    localBuilder.create().show();
  }

  protected void showPlaces()
  {
    Intent localIntent = new Intent(this, PlacesList.class);
    localIntent.putExtra("WidgetCall", true);
    startActivityForResult(localIntent, 1);
  }

  protected void storeLocation(double paramDouble1, double paramDouble2)
  {
    storeLocation(getString(2131296519), "", paramDouble1, paramDouble2);
  }

  protected void storeLocation(String paramString)
  {
    storeLocation(paramString, paramString, 0.0D, 0.0D);
  }

  protected void storeLocation(String paramString, int paramInt)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putString("LocationDisplayed_" + this.mAppWidgetId, paramString);
    localEditor.putInt("LocationFavorite_" + this.mAppWidgetId, paramInt);
    localEditor.commit();
  }

  protected void storeLocation(String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putString("LocationDisplayed_" + this.mAppWidgetId, paramString1);
    localEditor.putString("LocationCityZip_" + this.mAppWidgetId, paramString2);
    localEditor.putInt("LocationLatitude_" + this.mAppWidgetId, (int)(paramDouble1 * 1000000.0D));
    localEditor.putInt("LocationLongitude_" + this.mAppWidgetId, (int)(paramDouble2 * 1000000.0D));
    localEditor.commit();
  }

  private final class a extends CustomAsyncTask
  {
    private List<AutoCompMessage> b;

    public a(GBPreferenceActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      List localList;
      ArrayList localArrayList;
      int i;
      int j;
      if (paramBoolean.booleanValue())
      {
        localList = this.b;
        localArrayList = new ArrayList();
        i = localList.size();
        j = 0;
      }
      while (true)
      {
        if (j >= i)
        {
          ArrayAdapter localArrayAdapter = new ArrayAdapter(WidgetConfiguration.this.getApplicationContext(), 2130903122, localArrayList);
          WidgetConfiguration.a(WidgetConfiguration.this).setAdapter(localArrayAdapter);
        }
        try
        {
          WidgetConfiguration.a(WidgetConfiguration.this).showDropDown();
          return;
          String str = ((AutoCompMessage)localList.get(j)).getCity() + ", " + ((AutoCompMessage)localList.get(j)).getState();
          if (!localArrayList.contains(str))
            localArrayList.add(str);
          j++;
        }
        catch (Exception localException)
        {
        }
      }
    }

    protected final boolean queryWebService()
    {
      this.b = WidgetConfiguration.b(WidgetConfiguration.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.WidgetConfiguration
 * JD-Core Version:    0.6.2
 */