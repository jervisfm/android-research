package gbis.gbandroid.widget;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

final class a
  implements DialogInterface.OnClickListener
{
  a(ListWidgetConfiguration paramListWidgetConfiguration)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this.a.getBaseContext()).edit();
    localEditor.putInt("widgetRadiusPreference", ListWidgetConfiguration.d(this.a));
    localEditor.commit();
    paramDialogInterface.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.a
 * JD-Core Version:    0.6.2
 */