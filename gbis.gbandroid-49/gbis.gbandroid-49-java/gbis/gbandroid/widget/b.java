package gbis.gbandroid.widget;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

final class b
  implements SeekBar.OnSeekBarChangeListener
{
  b(ListWidgetConfiguration paramListWidgetConfiguration, float paramFloat, TextView paramTextView, String paramString)
  {
  }

  public final void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    ListWidgetConfiguration.a(this.a, Math.round((paramInt + this.b) / this.c));
    this.d.setText(String.valueOf(paramInt + this.b) + " " + this.e);
  }

  public final void onStartTrackingTouch(SeekBar paramSeekBar)
  {
  }

  public final void onStopTrackingTouch(SeekBar paramSeekBar)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.b
 * JD-Core Version:    0.6.2
 */