package gbis.gbandroid.widget;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import gbis.gbandroid.services.GPSService.GPSBinder;

final class f
  implements ServiceConnection
{
  f(ListWidgetProvider.UpdateService paramUpdateService)
  {
  }

  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    Log.e("GPSBinder", "binded");
    ListWidgetProvider.UpdateService.a(((GPSService.GPSBinder)paramIBinder).getService());
    ListWidgetProvider.UpdateService.a(this.a);
  }

  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    Log.e("GPSBinder", "disconnected");
    ListWidgetProvider.UpdateService.a(null);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.f
 * JD-Core Version:    0.6.2
 */