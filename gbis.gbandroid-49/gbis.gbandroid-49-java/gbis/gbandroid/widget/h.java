package gbis.gbandroid.widget;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Editable;
import android.widget.AutoCompleteTextView;
import gbis.gbandroid.listeners.WidgetSettingsListener;

final class h
  implements DialogInterface.OnClickListener
{
  h(WidgetConfiguration paramWidgetConfiguration, WidgetSettingsListener paramWidgetSettingsListener)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str = WidgetConfiguration.a(this.a).getText().toString();
    if (!str.equals(""))
    {
      this.b.otherLocationEntered(str);
      paramDialogInterface.dismiss();
      return;
    }
    this.a.showMessage(this.a.getString(2131296285));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.h
 * JD-Core Version:    0.6.2
 */