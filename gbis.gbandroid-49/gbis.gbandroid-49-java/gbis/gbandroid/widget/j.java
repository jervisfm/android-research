package gbis.gbandroid.widget;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import java.util.regex.Pattern;

final class j
  implements TextWatcher
{
  j(WidgetConfiguration paramWidgetConfiguration)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (!Pattern.matches(".*\\p{Digit}.*", paramCharSequence))
    {
      if (paramCharSequence.length() != 2)
        break label43;
      new WidgetConfiguration.a(this.a, this.a).execute(new Object[0]);
    }
    label43: 
    while (paramCharSequence.length() > 0)
      return;
    WidgetConfiguration.a(this.a).dismissDropDown();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.widget.j
 * JD-Core Version:    0.6.2
 */