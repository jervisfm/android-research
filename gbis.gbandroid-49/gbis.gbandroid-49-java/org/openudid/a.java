package org.openudid;

import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Parcel;

final class a extends Binder
{
  a(OpenUDID_service paramOpenUDID_service)
  {
  }

  public final boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    SharedPreferences localSharedPreferences = this.a.getSharedPreferences("openudid_prefs", 0);
    paramParcel2.writeInt(paramParcel1.readInt());
    paramParcel2.writeString(localSharedPreferences.getString("openudid", null));
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     org.openudid.a
 * JD-Core Version:    0.6.2
 */