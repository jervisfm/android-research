.class Lcom/amazon/device/ads/AdBridge$1;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/amazon/device/ads/AdListener;


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/AdBridge;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/AdBridge;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/amazon/device/ads/AdBridge$1;->this$0:Lcom/amazon/device/ads/AdBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdCollapsed(Lcom/amazon/device/ads/AdLayout;)V
    .locals 2
    .parameter

    .prologue
    .line 104
    const-string v0, "AdBridge"

    const-string v1, "Default ad listener called - Ad Collapsed."

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public onAdExpanded(Lcom/amazon/device/ads/AdLayout;)V
    .locals 2
    .parameter

    .prologue
    .line 98
    const-string v0, "AdBridge"

    const-string v1, "Default ad listener called - Ad Will Expand."

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public onAdFailedToLoad(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdError;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 91
    const-string v0, "AdBridge"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Default ad listener called - Ad Failed to Load. Error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lcom/amazon/device/ads/AdError;->code_:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Error Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/amazon/device/ads/AdError;->getResponseMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public onAdLoaded(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdProperties;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 85
    const-string v0, "AdBridge"

    const-string v1, "Default ad listener called - AdLoaded."

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    return-void
.end method
