.class Lcom/amazon/device/ads/AdBridge;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdBridge$LocationAwareness;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AdBridge"


# instance fields
.field protected adLayout_:Lcom/amazon/device/ads/AdLayout;

.field protected adListener_:Lcom/amazon/device/ads/AdListener;

.field protected adSize_:Lcom/amazon/device/ads/AdLayout$AdSize;

.field protected adWindowHeight_:I

.field protected adWindowWidth_:I

.field protected context_:Landroid/content/Context;

.field protected currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

.field protected currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

.field protected invalidated_:Z

.field protected isLoading_:Z

.field protected locationAwareness_:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

.field protected locationPrecision_:I

.field protected location_:Landroid/location/Location;

.field protected scalingDensity_:F

.field protected timeout_:I

.field protected userAgent_:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdLayout$AdSize;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->adListener_:Lcom/amazon/device/ads/AdListener;

    .line 35
    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    .line 36
    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    .line 39
    iput v1, p0, Lcom/amazon/device/ads/AdBridge;->adWindowHeight_:I

    .line 40
    iput v1, p0, Lcom/amazon/device/ads/AdBridge;->adWindowWidth_:I

    .line 42
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/amazon/device/ads/AdBridge;->scalingDensity_:F

    .line 56
    invoke-virtual {p1}, Lcom/amazon/device/ads/AdLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    .line 57
    iput-object p1, p0, Lcom/amazon/device/ads/AdBridge;->adLayout_:Lcom/amazon/device/ads/AdLayout;

    .line 58
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdBridge;->initializeAdListener()V

    .line 59
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdBridge;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->location_:Landroid/location/Location;

    .line 60
    sget-object v0, Lcom/amazon/device/ads/AdBridge$LocationAwareness;->LOCATION_AWARENESS_NORMAL:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->locationAwareness_:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    .line 61
    const/4 v0, 0x6

    iput v0, p0, Lcom/amazon/device/ads/AdBridge;->locationPrecision_:I

    .line 62
    iput-boolean v1, p0, Lcom/amazon/device/ads/AdBridge;->isLoading_:Z

    .line 63
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/amazon/device/ads/AdBridge;->timeout_:I

    .line 64
    iput-object p2, p0, Lcom/amazon/device/ads/AdBridge;->adSize_:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 66
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->sf:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/amazon/device/ads/AdBridge;->scalingDensity_:F

    .line 73
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->userAgent_:Ljava/lang/String;

    .line 75
    return-void
.end method


# virtual methods
.method protected adClosedExpansion()V
    .locals 2

    .prologue
    .line 328
    const-string v0, "AdBridge"

    const-string v1, "adClosedExpansion"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 331
    new-instance v1, Lcom/amazon/device/ads/AdBridge$5;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/AdBridge$5;-><init>(Lcom/amazon/device/ads/AdBridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 342
    return-void
.end method

.method protected adExpanded()V
    .locals 2

    .prologue
    .line 310
    const-string v0, "AdBridge"

    const-string v1, "adExpanded"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 313
    new-instance v1, Lcom/amazon/device/ads/AdBridge$4;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/AdBridge$4;-><init>(Lcom/amazon/device/ads/AdBridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 324
    return-void
.end method

.method protected adFailed(Lcom/amazon/device/ads/AdError;)V
    .locals 2
    .parameter

    .prologue
    .line 293
    const-string v0, "AdBridge"

    const-string v1, "adFailed"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 295
    new-instance v1, Lcom/amazon/device/ads/AdBridge$3;

    invoke-direct {v1, p0, p1}, Lcom/amazon/device/ads/AdBridge$3;-><init>(Lcom/amazon/device/ads/AdBridge;Lcom/amazon/device/ads/AdError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 306
    return-void
.end method

.method protected adLoaded(Lcom/amazon/device/ads/AdProperties;)V
    .locals 2
    .parameter

    .prologue
    .line 275
    const-string v0, "AdBridge"

    const-string v1, "adLoaded"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 277
    new-instance v1, Lcom/amazon/device/ads/AdBridge$2;

    invoke-direct {v1, p0, p1}, Lcom/amazon/device/ads/AdBridge$2;-><init>(Lcom/amazon/device/ads/AdBridge;Lcom/amazon/device/ads/AdProperties;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 289
    return-void
.end method

.method protected destroy()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->releaseResources()V

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v0, :cond_1

    .line 215
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdRenderer;->removeView()V

    .line 216
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdRenderer;->destroy()V

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    .line 219
    :cond_1
    return-void
.end method

.method protected getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method protected getAdLayout()Lcom/amazon/device/ads/AdLayout;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->adLayout_:Lcom/amazon/device/ads/AdLayout;

    return-object v0
.end method

.method protected getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->adLayout_:Lcom/amazon/device/ads/AdLayout;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdLayout;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    return-object v0
.end method

.method protected getAdSize()Lcom/amazon/device/ads/AdLayout$AdSize;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->adSize_:Lcom/amazon/device/ads/AdLayout$AdSize;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    return-object v0
.end method

.method protected getLastKnownLocation()Landroid/location/Location;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v2, 0x0

    .line 431
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->locationAwareness_:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    sget-object v1, Lcom/amazon/device/ads/AdBridge$LocationAwareness;->LOCATION_AWARENESS_DISABLED:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    if-ne v0, v1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-object v2

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 439
    :try_start_0
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 453
    :goto_1
    :try_start_1
    const-string v3, "network"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    .line 464
    :goto_2
    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    .line 466
    :cond_2
    if-eqz v1, :cond_5

    if-eqz v0, :cond_5

    .line 468
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 470
    const-string v0, "AdBridge"

    const-string v2, "Setting lat/long using GPS"

    invoke-static {v0, v2}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    :goto_3
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->locationAwareness_:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    sget-object v2, Lcom/amazon/device/ads/AdBridge$LocationAwareness;->LOCATION_AWARENESS_TRUNCATED:Lcom/amazon/device/ads/AdBridge$LocationAwareness;

    if-ne v0, v2, :cond_3

    .line 492
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 493
    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    iget v2, p0, Lcom/amazon/device/ads/AdBridge;->locationPrecision_:I

    invoke-virtual {v0, v2, v6}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    .line 496
    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 498
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    .line 499
    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    iget v2, p0, Lcom/amazon/device/ads/AdBridge;->locationPrecision_:I

    invoke-virtual {v0, v2, v6}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    .line 502
    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    :cond_3
    move-object v2, v1

    .line 505
    goto :goto_0

    .line 443
    :catch_0
    move-exception v1

    const-string v1, "AdBridge"

    const-string v3, "Failed to retrieve GPS location: No permissions to access GPS"

    invoke-static {v1, v3}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 448
    goto :goto_1

    .line 447
    :catch_1
    move-exception v1

    const-string v1, "AdBridge"

    const-string v3, "Failed to retrieve GPS location: no GPS found"

    invoke-static {v1, v3}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_1

    .line 457
    :catch_2
    move-exception v0

    const-string v0, "AdBridge"

    const-string v3, "Failed to retrieve network location: No permissions to access network location"

    invoke-static {v0, v3}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 462
    goto :goto_2

    .line 461
    :catch_3
    move-exception v0

    const-string v0, "AdBridge"

    const-string v3, "Failed to retrieve network location: no network provider found"

    invoke-static {v0, v3}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_2

    .line 475
    :cond_4
    const-string v1, "AdBridge"

    const-string v2, "Setting lat/long using network"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 476
    goto :goto_3

    .line 479
    :cond_5
    if-eqz v1, :cond_6

    .line 481
    const-string v0, "AdBridge"

    const-string v2, "Setting lat/long using GPS, not network"

    invoke-static {v0, v2}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 486
    :cond_6
    const-string v1, "AdBridge"

    const-string v2, "Setting lat/long using network location, not GPS"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 487
    goto :goto_3
.end method

.method protected getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->location_:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 423
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdBridge;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->location_:Landroid/location/Location;

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->location_:Landroid/location/Location;

    return-object v0
.end method

.method protected getScalingDensity()F
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/amazon/device/ads/AdBridge;->scalingDensity_:F

    return v0
.end method

.method protected getTimeout()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/amazon/device/ads/AdBridge;->timeout_:I

    return v0
.end method

.method protected getWindowHeight()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/amazon/device/ads/AdBridge;->adWindowHeight_:I

    return v0
.end method

.method protected getWindowWidth()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/amazon/device/ads/AdBridge;->adWindowWidth_:I

    return v0
.end method

.method protected handleApplicationDefinedSpecialURL(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 403
    const-string v0, "AdBridge"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Special url clicked, but was not handled by SDK. Url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void
.end method

.method protected handleResponse(Lcom/amazon/device/ads/AdResponse;)V
    .locals 4
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdRenderer;->removeView()V

    .line 238
    :cond_0
    invoke-virtual {p1}, Lcom/amazon/device/ads/AdResponse;->getCreativeTypes()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;

    .line 240
    iget-object v2, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-static {v0, v2}, Lcom/amazon/device/ads/AdRendererFactory;->shouldCreateNewRenderer(Lcom/amazon/device/ads/AdResponse$AAXCreative;Lcom/amazon/device/ads/AdRenderer;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 242
    const-string v2, "AdBridge"

    const-string v3, "Creating new renderer"

    invoke-static {v2, v3}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v2, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdRenderer;->destroy()V

    .line 246
    :cond_2
    invoke-static {v0, p1, p0}, Lcom/amazon/device/ads/AdRendererFactory;->getAdRenderer(Lcom/amazon/device/ads/AdResponse$AAXCreative;Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)Lcom/amazon/device/ads/AdRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    .line 247
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v0, :cond_1

    .line 259
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-nez v0, :cond_5

    .line 261
    const-string v0, "No renderer returned, not loading an ad"

    .line 262
    const-string v1, "AdBridge"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    new-instance v1, Lcom/amazon/device/ads/AdError;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    .line 269
    :goto_1
    return-void

    .line 252
    :cond_4
    const-string v0, "AdBridge"

    const-string v1, "Re-using renderer"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/AdRenderer;->setAdResponse(Lcom/amazon/device/ads/AdResponse;)V

    goto :goto_0

    .line 267
    :cond_5
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdRenderer;->render()V

    goto :goto_1
.end method

.method protected initializeAdListener()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/amazon/device/ads/AdBridge$1;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/AdBridge$1;-><init>(Lcom/amazon/device/ads/AdBridge;)V

    iput-object v0, p0, Lcom/amazon/device/ads/AdBridge;->adListener_:Lcom/amazon/device/ads/AdListener;

    .line 107
    return-void
.end method

.method protected isAdLoading()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdBridge;->isLoading_:Z

    return v0
.end method

.method protected isInvalidated()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdBridge;->invalidated_:Z

    return v0
.end method

.method protected loadAd(Lcom/amazon/device/ads/AdTargetingOptions;)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x1

    .line 225
    new-instance v0, Lcom/amazon/device/ads/AdRequest;

    iget-object v3, p0, Lcom/amazon/device/ads/AdBridge;->adSize_:Lcom/amazon/device/ads/AdLayout$AdSize;

    iget v4, p0, Lcom/amazon/device/ads/AdBridge;->adWindowWidth_:I

    iget v5, p0, Lcom/amazon/device/ads/AdBridge;->adWindowHeight_:I

    iget-object v6, p0, Lcom/amazon/device/ads/AdBridge;->userAgent_:Ljava/lang/String;

    iget v7, p0, Lcom/amazon/device/ads/AdBridge;->timeout_:I

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/amazon/device/ads/AdRequest;-><init>(Lcom/amazon/device/ads/AdBridge;Lcom/amazon/device/ads/AdTargetingOptions;Lcom/amazon/device/ads/AdLayout$AdSize;IILjava/lang/String;I)V

    .line 228
    iput-boolean v8, p0, Lcom/amazon/device/ads/AdBridge;->isLoading_:Z

    .line 229
    new-instance v1, Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    invoke-direct {v1}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;-><init>()V

    iput-object v1, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    .line 230
    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRequestTask_:Lcom/amazon/device/ads/AdLoader$AdRequestTask;

    new-array v2, v8, [Lcom/amazon/device/ads/AdRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 231
    return-void
.end method

.method protected prepareToGoAway()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdRenderer;->prepareToGoAway()V

    .line 197
    :cond_0
    return-void
.end method

.method protected sendCommand(Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->currentAdRenderer_:Lcom/amazon/device/ads/AdRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/amazon/device/ads/AdRenderer;->sendCommand(Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    .line 204
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setAdSize(Lcom/amazon/device/ads/AdLayout$AdSize;)V
    .locals 0
    .parameter

    .prologue
    .line 153
    iput-object p1, p0, Lcom/amazon/device/ads/AdBridge;->adSize_:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 154
    return-void
.end method

.method protected setIsLoading(Z)V
    .locals 0
    .parameter

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/amazon/device/ads/AdBridge;->isLoading_:Z

    .line 119
    return-void
.end method

.method protected setListener(Lcom/amazon/device/ads/AdListener;)V
    .locals 0
    .parameter

    .prologue
    .line 189
    if-eqz p1, :cond_0

    .line 190
    iput-object p1, p0, Lcom/amazon/device/ads/AdBridge;->adListener_:Lcom/amazon/device/ads/AdListener;

    .line 191
    :cond_0
    return-void
.end method

.method protected setTimeout(I)V
    .locals 0
    .parameter

    .prologue
    .line 148
    iput p1, p0, Lcom/amazon/device/ads/AdBridge;->timeout_:I

    .line 149
    return-void
.end method

.method protected setWindowDimensions(II)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 163
    iput p1, p0, Lcom/amazon/device/ads/AdBridge;->adWindowHeight_:I

    .line 164
    iput p2, p0, Lcom/amazon/device/ads/AdBridge;->adWindowWidth_:I

    .line 165
    return-void
.end method

.method protected specialUrlClicked(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 357
    iget-object v0, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/amazon/device/ads/AmazonDeviceLauncher;->isWindowshopPresent(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 359
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, "shopping"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    const-string v1, "app-action"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 363
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    const-string v2, "detail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 366
    const-string v1, "asin"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/amazon/device/ads/AmazonDeviceLauncher;->launchWindowshopDetailPage(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 370
    :cond_2
    const-string v2, "search"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 372
    const-string v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 373
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lcom/amazon/device/ads/AdBridge;->context_:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/amazon/device/ads/AmazonDeviceLauncher;->luanchWindowshopSearchPage(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 376
    :cond_3
    const-string v0, "webview"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdBridge;->handleApplicationDefinedSpecialURL(Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_4
    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdBridge;->handleApplicationDefinedSpecialURL(Ljava/lang/String;)V

    goto :goto_0
.end method
