.class public final Lcom/amazon/device/ads/AdError;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field protected code_:I

.field protected message_:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/amazon/device/ads/AdError;->code_:I

    .line 32
    iput-object p2, p0, Lcom/amazon/device/ads/AdError;->message_:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/amazon/device/ads/AdError;->code_:I

    return v0
.end method

.method public final getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/amazon/device/ads/AdError;->message_:Ljava/lang/String;

    return-object v0
.end method
