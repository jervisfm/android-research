.class public final enum Lcom/amazon/device/ads/AdLayout$AdSize;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amazon/device/ads/AdLayout$AdSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_1024x50:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_300x250:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_300x50:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_600x90:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_728x90:Lcom/amazon/device/ads/AdLayout$AdSize;

.field public static final enum AD_SIZE_CUSTOM:Lcom/amazon/device/ads/AdLayout$AdSize;


# instance fields
.field public final height:I

.field public final size:Ljava/lang/String;

.field public final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 70
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_300x50"

    const/4 v2, 0x0

    const-string v3, "300x50"

    const/16 v4, 0x12c

    const/16 v5, 0x32

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 75
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_320x50"

    const/4 v2, 0x1

    const-string v3, "320x50"

    const/16 v4, 0x140

    const/16 v5, 0x32

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 80
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_300x250"

    const/4 v2, 0x2

    const-string v3, "300x250"

    const/16 v4, 0x12c

    const/16 v5, 0xfa

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x250:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 85
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_600x90"

    const/4 v2, 0x3

    const-string v3, "600x90"

    const/16 v4, 0x258

    const/16 v5, 0x5a

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_600x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 90
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_728x90"

    const/4 v2, 0x4

    const-string v3, "728x90"

    const/16 v4, 0x2d8

    const/16 v5, 0x5a

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_728x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 95
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_1024x50"

    const/4 v2, 0x5

    const-string v3, "1024x50"

    const/16 v4, 0x400

    const/16 v5, 0x32

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_1024x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 103
    new-instance v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    const-string v1, "AD_SIZE_CUSTOM"

    const/4 v2, 0x6

    const-string v3, "CUSTOM"

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdLayout$AdSize;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_CUSTOM:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 64
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/amazon/device/ads/AdLayout$AdSize;

    const/4 v1, 0x0

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x250:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_600x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_728x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_1024x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_CUSTOM:Lcom/amazon/device/ads/AdLayout$AdSize;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->$VALUES:[Lcom/amazon/device/ads/AdLayout$AdSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 112
    iput-object p3, p0, Lcom/amazon/device/ads/AdLayout$AdSize;->size:Ljava/lang/String;

    .line 113
    iput p4, p0, Lcom/amazon/device/ads/AdLayout$AdSize;->width:I

    .line 114
    iput p5, p0, Lcom/amazon/device/ads/AdLayout$AdSize;->height:I

    .line 115
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/amazon/device/ads/AdLayout$AdSize;
    .locals 3
    .parameter

    .prologue
    .line 119
    if-eqz p0, :cond_7

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_7

    .line 121
    const-string v0, "300x50"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 127
    :goto_0
    return-object v0

    .line 122
    :cond_0
    const-string v0, "320x50"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 123
    :cond_1
    const-string v0, "300x250"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_300x250:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 124
    :cond_2
    const-string v0, "600x90"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_600x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 125
    :cond_3
    const-string v0, "728x90"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_728x90:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 126
    :cond_4
    const-string v0, "1024x50"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_1024x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 127
    :cond_5
    const-string v0, "custom"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_CUSTOM:Lcom/amazon/device/ads/AdLayout$AdSize;

    goto :goto_0

    .line 130
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ad size: \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', see documentation for available ad sizes."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ad size string. See documentation for available ad sizes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/device/ads/AdLayout$AdSize;
    .locals 1
    .parameter

    .prologue
    .line 64
    const-class v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/AdLayout$AdSize;

    return-object v0
.end method

.method public static values()[Lcom/amazon/device/ads/AdLayout$AdSize;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/amazon/device/ads/AdLayout$AdSize;->$VALUES:[Lcom/amazon/device/ads/AdLayout$AdSize;

    invoke-virtual {v0}, [Lcom/amazon/device/ads/AdLayout$AdSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amazon/device/ads/AdLayout$AdSize;

    return-object v0
.end method
