.class public Lcom/amazon/device/ads/AdLayout;
.super Landroid/widget/FrameLayout;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdLayout$AdSize;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AmazonAdLayout"


# instance fields
.field private amznAdregistration_:Lcom/amazon/device/ads/InternalAdRegistration;

.field private attached_:Z

.field private bridge_:Lcom/amazon/device/ads/AdBridge;

.field private context_:Landroid/content/Context;

.field private hasRegisterBroadcastReciever_:Z

.field private hasSetAdUnitId_:Z

.field private isInForeground_:Z

.field private lastVisibility_:I

.field private screenStateReceiver_:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasSetAdUnitId_:Z

    .line 55
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    .line 56
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->attached_:Z

    .line 57
    const/16 v0, 0x8

    iput v0, p0, Lcom/amazon/device/ads/AdLayout;->lastVisibility_:I

    .line 180
    :try_start_0
    new-instance v0, Lcom/amazon/device/ads/ResourceLookup;

    const-string v2, "adsdk"

    invoke-direct {v0, v2, p1}, Lcom/amazon/device/ads/ResourceLookup;-><init>(Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 189
    :goto_0
    if-eqz v1, :cond_0

    .line 191
    const-string v0, "Amazon"

    invoke-virtual {v1, v0}, Lcom/amazon/device/ads/ResourceLookup;->getStyleableArray(Ljava/lang/String;)[I

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 192
    const-string v0, "Amazon"

    const-string v3, "adSize"

    invoke-virtual {v1, v0, v3}, Lcom/amazon/device/ads/ResourceLookup;->getStyleableId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 196
    :cond_0
    invoke-static {v0}, Lcom/amazon/device/ads/AdLayout$AdSize;->fromString(Ljava/lang/String;)Lcom/amazon/device/ads/AdLayout$AdSize;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/amazon/device/ads/AdLayout;->initialize(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V

    .line 197
    return-void

    .line 186
    :catch_0
    move-exception v0

    const-string v0, "custom"

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 54
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasSetAdUnitId_:Z

    .line 55
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    .line 56
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->attached_:Z

    .line 57
    const/16 v0, 0x8

    iput v0, p0, Lcom/amazon/device/ads/AdLayout;->lastVisibility_:I

    .line 153
    if-nez p2, :cond_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 156
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/AdLayout;->initialize(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V

    .line 157
    return-void
.end method

.method static synthetic access$000(Lcom/amazon/device/ads/AdLayout;)Z
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->isInForeground_:Z

    return v0
.end method

.method static synthetic access$100(Lcom/amazon/device/ads/AdLayout;)Lcom/amazon/device/ads/AdBridge;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    return-object v0
.end method

.method private initialize(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 206
    iput-object p1, p0, Lcom/amazon/device/ads/AdLayout;->context_:Landroid/content/Context;

    .line 212
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 215
    const-string v1, "AdLayout"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 218
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/AdLayout;->addView(Landroid/view/View;)V

    .line 242
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-static {p1}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdLayout;->amznAdregistration_:Lcom/amazon/device/ads/InternalAdRegistration;

    .line 223
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->isInForeground_:Z

    .line 225
    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/AdLayout;->setHorizontalScrollBarEnabled(Z)V

    .line 226
    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/AdLayout;->setVerticalScrollBarEnabled(Z)V

    .line 233
    invoke-static {p1}, Landroid/webkit/WebViewDatabase;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    move-result-object v0

    if-nez v0, :cond_2

    .line 235
    const-string v0, "AmazonAdLayout"

    const-string v1, "Disabling ads. Local cache file is inaccessible so ads will fail if we try to create a WebView. Details of this Android bug found at:http://code.google.com/p/android/issues/detail?id=10789"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 223
    goto :goto_1

    .line 241
    :cond_2
    invoke-virtual {p0, p2}, Lcom/amazon/device/ads/AdLayout;->createAdBridge(Lcom/amazon/device/ads/AdLayout$AdSize;)Lcom/amazon/device/ads/AdBridge;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    goto :goto_0
.end method

.method private registerScreenStateBroadcastReceiver()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 256
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    if-ne v0, v1, :cond_0

    .line 282
    :goto_0
    return-void

    .line 259
    :cond_0
    iput-boolean v1, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    .line 260
    new-instance v0, Lcom/amazon/device/ads/AdLayout$1;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/AdLayout$1;-><init>(Lcom/amazon/device/ads/AdLayout;)V

    iput-object v0, p0, Lcom/amazon/device/ads/AdLayout;->screenStateReceiver_:Landroid/content/BroadcastReceiver;

    .line 279
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 280
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->context_:Landroid/content/Context;

    iget-object v2, p0, Lcom/amazon/device/ads/AdLayout;->screenStateReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private unregisterScreenStateBroadcastReceiver()V
    .locals 2

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasRegisterBroadcastReciever_:Z

    .line 289
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->context_:Landroid/content/Context;

    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->screenStateReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 291
    :cond_0
    return-void
.end method


# virtual methods
.method public collapseAd()Z
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    const-string v1, "close"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/amazon/device/ads/AdBridge;->sendCommand(Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v0

    return v0
.end method

.method protected createAdBridge(Lcom/amazon/device/ads/AdLayout$AdSize;)Lcom/amazon/device/ads/AdBridge;
    .locals 1
    .parameter

    .prologue
    .line 246
    new-instance v0, Lcom/amazon/device/ads/AdBridge;

    invoke-direct {v0, p0, p1}, Lcom/amazon/device/ads/AdBridge;-><init>(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdLayout$AdSize;)V

    return-object v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 496
    invoke-direct {p0}, Lcom/amazon/device/ads/AdLayout;->unregisterScreenStateBroadcastReceiver()V

    .line 498
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->destroy()V

    .line 500
    :cond_0
    return-void
.end method

.method protected getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->amznAdregistration_:Lcom/amazon/device/ads/InternalAdRegistration;

    return-object v0
.end method

.method public getAdSize()Lcom/amazon/device/ads/AdLayout$AdSize;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdSize()Lcom/amazon/device/ads/AdLayout$AdSize;

    move-result-object v0

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getTimeout()I

    move-result v0

    return v0
.end method

.method public isAdLoading()Z
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->isAdLoading()Z

    move-result v0

    return v0
.end method

.method public loadAd(Lcom/amazon/device/ads/AdTargetingOptions;)Z
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x190

    const/4 v0, 0x1

    .line 407
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->isAdLoading()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 409
    const-string v0, "AmazonAdLayout"

    const-string v1, "Can\'t load an ad because ad loading is already in progress"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v0, 0x0

    .line 451
    :goto_0
    return v0

    .line 413
    :cond_0
    iget-boolean v1, p0, Lcom/amazon/device/ads/AdLayout;->hasSetAdUnitId_:Z

    if-nez v1, :cond_2

    .line 415
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->amznAdregistration_:Lcom/amazon/device/ads/InternalAdRegistration;

    invoke-virtual {v1}, Lcom/amazon/device/ads/InternalAdRegistration;->getAppId()Ljava/lang/String;

    move-result-object v1

    .line 416
    if-nez v1, :cond_1

    .line 418
    const-string v1, "Can\'t load an ad because App GUID has not been set. Did you forget to call AdRegistration.setAppGUID( ... )?"

    .line 419
    const-string v2, "AmazonAdLayout"

    invoke-static {v2, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    new-instance v3, Lcom/amazon/device/ads/AdError;

    invoke-direct {v3, v4, v1}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    goto :goto_0

    .line 423
    :cond_1
    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->hasSetAdUnitId_:Z

    .line 426
    :cond_2
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getAdSize()Lcom/amazon/device/ads/AdLayout$AdSize;

    move-result-object v1

    if-nez v1, :cond_3

    .line 428
    const-string v1, "Ad size is not defined."

    .line 429
    const-string v2, "AmazonAdLayout"

    invoke-static {v2, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v2, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    new-instance v3, Lcom/amazon/device/ads/AdError;

    invoke-direct {v3, v4, v1}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    goto :goto_0

    .line 433
    :cond_3
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getAdSize()Lcom/amazon/device/ads/AdLayout$AdSize;

    move-result-object v1

    sget-object v2, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_CUSTOM:Lcom/amazon/device/ads/AdLayout$AdSize;

    if-ne v1, v2, :cond_4

    .line 439
    const-string v1, "sz"

    invoke-virtual {p1, v1}, Lcom/amazon/device/ads/AdTargetingOptions;->containsAdvancedOption(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 441
    const-string v1, "Can\'t load an ad because a custom ad size is specified, but ad size dimensions are not configured using AdTargetingOptions.setAdvancedOption( ... ) with key \'sz\'."

    .line 442
    const-string v2, "AmazonAdLayout"

    invoke-static {v2, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v2, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    new-instance v3, Lcom/amazon/device/ads/AdError;

    invoke-direct {v3, v4, v1}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    goto :goto_0

    .line 449
    :cond_4
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->amznAdregistration_:Lcom/amazon/device/ads/InternalAdRegistration;

    invoke-virtual {v1}, Lcom/amazon/device/ads/InternalAdRegistration;->registerIfNeeded()V

    .line 450
    iget-object v1, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1, p1}, Lcom/amazon/device/ads/AdBridge;->loadAd(Lcom/amazon/device/ads/AdTargetingOptions;)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 295
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 297
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdLayout;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->attached_:Z

    .line 300
    invoke-direct {p0}, Lcom/amazon/device/ads/AdLayout;->registerScreenStateBroadcastReceiver()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->attached_:Z

    .line 307
    invoke-direct {p0}, Lcom/amazon/device/ads/AdLayout;->unregisterScreenStateBroadcastReceiver()V

    .line 308
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->prepareToGoAway()V

    .line 309
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 332
    sub-int v0, p4, p2

    .line 333
    sub-int v1, p5, p3

    .line 334
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 336
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdLayout;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2, v1, v0}, Lcom/amazon/device/ads/AdBridge;->setWindowDimensions(II)V

    goto :goto_0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .parameter

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->attached_:Z

    if-eqz v0, :cond_0

    .line 315
    iget v0, p0, Lcom/amazon/device/ads/AdLayout;->lastVisibility_:I

    if-eq v0, p1, :cond_0

    .line 317
    if-eqz p1, :cond_1

    .line 319
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->isInForeground_:Z

    .line 320
    invoke-direct {p0}, Lcom/amazon/device/ads/AdLayout;->unregisterScreenStateBroadcastReceiver()V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    if-nez p1, :cond_0

    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdLayout;->isInForeground_:Z

    goto :goto_0
.end method

.method public setListener(Lcom/amazon/device/ads/AdListener;)V
    .locals 1
    .parameter

    .prologue
    .line 462
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/AdBridge;->setListener(Lcom/amazon/device/ads/AdListener;)V

    .line 463
    return-void
.end method

.method public setTimeout(I)V
    .locals 1
    .parameter

    .prologue
    .line 352
    iget-object v0, p0, Lcom/amazon/device/ads/AdLayout;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/AdBridge;->setTimeout(I)V

    .line 353
    return-void
.end method
