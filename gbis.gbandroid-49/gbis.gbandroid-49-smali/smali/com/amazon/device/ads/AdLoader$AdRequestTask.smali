.class public Lcom/amazon/device/ads/AdLoader$AdRequestTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "AdRequestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/amazon/device/ads/AdRequest;",
        "Ljava/lang/Void;",
        "Lcom/amazon/device/ads/AdResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected bridge_:Lcom/amazon/device/ads/AdBridge;

.field protected client_:Lorg/apache/http/client/HttpClient;

.field protected error_:Lcom/amazon/device/ads/AdError;

.field protected exception_:Ljava/lang/Exception;

.field protected httpParams_:Lorg/apache/http/params/HttpParams;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 54
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->httpParams_:Lorg/apache/http/params/HttpParams;

    .line 55
    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    .line 56
    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    .line 57
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/amazon/device/ads/AdRequest;)Lcom/amazon/device/ads/AdResponse;
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 62
    aget-object v0, p1, v0

    iget-object v0, v0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    iput-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    .line 63
    const/4 v0, 0x0

    .line 67
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->fetchAd(Lcom/amazon/device/ads/AdRequest;)Lcom/amazon/device/ads/AdResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    .line 69
    :catch_0
    move-exception v1

    .line 71
    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->exception_:Ljava/lang/Exception;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 43
    check-cast p1, [Lcom/amazon/device/ads/AdRequest;

    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->doInBackground([Lcom/amazon/device/ads/AdRequest;)Lcom/amazon/device/ads/AdResponse;

    move-result-object v0

    return-object v0
.end method

.method protected fetchAd(Lcom/amazon/device/ads/AdRequest;)Lcom/amazon/device/ads/AdResponse;
    .locals 9
    .parameter

    .prologue
    const/4 v4, 0x0

    const/16 v8, 0x1f4

    .line 78
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Calling unsupported entrypoint"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->httpParams_:Lorg/apache/http/params/HttpParams;

    iget v1, p1, Lcom/amazon/device/ads/AdRequest;->timeout_:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 82
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->httpParams_:Lorg/apache/http/params/HttpParams;

    iget v1, p1, Lcom/amazon/device/ads/AdRequest;->timeout_:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 85
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->httpParams_:Lorg/apache/http/params/HttpParams;

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 87
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->httpParams_:Lorg/apache/http/params/HttpParams;

    invoke-direct {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    iput-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    .line 89
    iget-object v0, p1, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 90
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 92
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 94
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v2, 0xc8

    if-ne v0, v2, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-nez v0, :cond_2

    .line 97
    :cond_1
    const-string v0, "Network error reading data from ad server"

    .line 98
    new-instance v1, Lcom/amazon/device/ads/AdError;

    const/16 v2, 0x1f7

    invoke-direct {v1, v2, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 99
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_2
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 103
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 104
    const/16 v2, 0x1000

    new-array v2, v2, [B

    .line 105
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v6, -0x1

    if-eq v3, v6, :cond_3

    .line 106
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v2, v4, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 126
    :cond_3
    const/4 v3, 0x0

    .line 129
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 130
    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "ok"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 133
    :cond_4
    const-string v1, "html"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    const-string v2, "\\n"

    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\r"

    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\"

    const-string v6, ""

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 136
    sget-object v1, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ad Contents: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v1, "creativeTypes"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 142
    new-instance v1, Lcom/amazon/device/ads/AdProperties;

    invoke-direct {v1, v6}, Lcom/amazon/device/ads/AdProperties;-><init>(Lorg/json/JSONArray;)V

    .line 144
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v0, v4

    .line 145
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    if-ge v0, v4, :cond_5

    .line 149
    :try_start_1
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/amazon/device/ads/AdResponse$AAXCreative;->getCreative(I)Lcom/amazon/device/ads/AdResponse$AAXCreative;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 156
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 160
    :cond_5
    :try_start_2
    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 161
    invoke-static {v7}, Lcom/amazon/device/ads/AdResponse$AAXCreative;->determineTypeOrder(Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v3

    .line 171
    :goto_3
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 172
    const-string v4, "sz"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 173
    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 174
    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 176
    new-instance v0, Lcom/amazon/device/ads/AdResponse;

    invoke-direct/range {v0 .. v5}, Lcom/amazon/device/ads/AdResponse;-><init>(Lcom/amazon/device/ads/AdProperties;Ljava/lang/String;Ljava/util/ArrayList;II)V

    return-object v0

    .line 165
    :cond_6
    const-string v0, "No valid creative types found."

    .line 166
    new-instance v4, Lcom/amazon/device/ads/AdError;

    const/16 v6, 0x1f4

    invoke-direct {v4, v6, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v4, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 167
    sget-object v4, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 190
    :catch_0
    move-exception v0

    const-string v0, "Unable to parse response from ad server."

    .line 191
    new-instance v1, Lcom/amazon/device/ads/AdError;

    invoke-direct {v1, v8, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 192
    sget-object v1, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :goto_4
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "AAX did not return an ad"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_7
    :try_start_3
    const-string v1, "error"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 182
    const-string v2, "errorCode"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Server Message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 184
    new-instance v2, Lcom/amazon/device/ads/AdError;

    const/16 v3, 0x1f4

    invoke-direct {v2, v3, v1}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v2, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 185
    sget-object v2, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "; code: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    .line 194
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 197
    new-instance v1, Lcom/amazon/device/ads/AdError;

    invoke-direct {v1, v8, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 198
    sget-object v1, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :catch_2
    move-exception v4

    goto/16 :goto_2
.end method

.method protected onPostExecute(Lcom/amazon/device/ads/AdResponse;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 206
    if-nez p1, :cond_3

    .line 212
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdBridge;->setIsLoading(Z)V

    .line 213
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->exception_:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Exception caught while loading ad: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->exception_:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    sget-object v1, Lcom/amazon/device/ads/AdLoader;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->exception_:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 220
    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    if-nez v1, :cond_0

    .line 223
    new-instance v1, Lcom/amazon/device/ads/AdError;

    invoke-direct {v1, v3, v0}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    iget-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->error_:Lcom/amazon/device/ads/AdError;

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    .line 242
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->releaseResources()V

    .line 243
    return-void

    .line 235
    :cond_2
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    new-instance v1, Lcom/amazon/device/ads/AdError;

    const-string v2, "Unknown error ocurred."

    invoke-direct {v1, v3, v2}, Lcom/amazon/device/ads/AdError;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdBridge;->adFailed(Lcom/amazon/device/ads/AdError;)V

    goto :goto_0

    .line 240
    :cond_3
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/AdBridge;->handleResponse(Lcom/amazon/device/ads/AdResponse;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    check-cast p1, Lcom/amazon/device/ads/AdResponse;

    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->onPostExecute(Lcom/amazon/device/ads/AdResponse;)V

    return-void
.end method

.method public releaseResources()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 247
    monitor-enter p0

    .line 249
    :try_start_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 253
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->client_:Lorg/apache/http/client/HttpClient;

    .line 255
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    iput-object v1, p0, Lcom/amazon/device/ads/AdLoader$AdRequestTask;->exception_:Ljava/lang/Exception;

    .line 257
    return-void

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
