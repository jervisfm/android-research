.class public final Lcom/amazon/device/ads/AdRegistration;
.super Ljava/lang/Object;
.source "GBFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static final enableLogging(Landroid/content/Context;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration;->setLoggingEnabled(Z)V

    .line 82
    return-void
.end method

.method public static final enableTesting(Landroid/content/Context;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration;->setTestMode(Z)V

    .line 96
    return-void
.end method

.method public static final getVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 106
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getSDKVersionID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final registerApp(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 133
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->registerIfNeeded()V

    .line 134
    return-void
.end method

.method public static final setAppGUID(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration;->setApplicationId(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public static final setAppUniqueId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-static {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration;->setApplicationId(Ljava/lang/String;)V

    .line 71
    return-void
.end method
