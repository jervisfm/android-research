.class public Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRegistrationTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "A9Request"
.end annotation


# instance fields
.field protected amazonDeviceId_:Ljava/lang/String;

.field protected context_:Landroid/content/Context;

.field protected deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

.field protected deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

.field protected endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field protected requestTag_:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->context_:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->requestTag_:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 58
    iput-object p4, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    .line 59
    iput-object p5, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    .line 60
    iput-object p6, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->amazonDeviceId_:Ljava/lang/String;

    .line 61
    return-void
.end method
