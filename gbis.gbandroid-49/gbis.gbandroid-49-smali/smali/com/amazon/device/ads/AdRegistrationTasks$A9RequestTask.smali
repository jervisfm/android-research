.class public abstract Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRegistrationTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "A9RequestTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;",
        "Ljava/lang/Void;",
        "Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;",
        ">;"
    }
.end annotation


# instance fields
.field protected exception_:Ljava/lang/Exception;

.field protected httpClient_:Lorg/apache/http/client/HttpClient;

.field protected requestTag_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 82
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->requestTag_:Ljava/lang/String;

    .line 86
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 87
    sget v1, Lcom/amazon/device/ads/AdRegistrationTasks;->HTTP_TIMEOUT:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 88
    sget v1, Lcom/amazon/device/ads/AdRegistrationTasks;->HTTP_TIMEOUT:I

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 89
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 90
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    iput-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->httpClient_:Lorg/apache/http/client/HttpClient;

    .line 91
    return-void
.end method


# virtual methods
.method public varargs doInBackground([Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
    .locals 2
    .parameter

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 98
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, p1, v1

    iget-object v1, v1, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->requestTag_:Ljava/lang/String;

    iput-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->requestTag_:Ljava/lang/String;

    .line 99
    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->executeA9Request(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v1

    .line 103
    iput-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->exception_:Ljava/lang/Exception;

    goto :goto_0
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 77
    check-cast p1, [Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->doInBackground([Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;

    move-result-object v0

    return-object v0
.end method

.method protected abstract executeA9Request(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
.end method

.method protected onPostExecute(Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;)V
    .locals 3
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->exception_:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    .line 115
    const-string v0, "AmazonAdRegistrationTasks"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception caught while performing "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->requestTag_:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " request to A9: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->exception_:Ljava/lang/Exception;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    if-eqz p1, :cond_0

    .line 120
    invoke-virtual {p1}, Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;->execute()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    check-cast p1, Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;

    invoke-virtual {p0, p1}, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;->onPostExecute(Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;)V

    return-void
.end method
