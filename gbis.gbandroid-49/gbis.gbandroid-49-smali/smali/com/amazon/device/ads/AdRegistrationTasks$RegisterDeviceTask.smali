.class public Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;
.super Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRegistrationTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RegisterDeviceTask"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected executeA9Request(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
    .locals 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 136
    invoke-static {p1, v0}, Lcom/amazon/device/ads/AdRegistrationTasks;->buildDeviceInfoParams(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;Z)Ljava/lang/String;

    move-result-object v0

    .line 137
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 138
    const-string v2, "AmazonAdRegistrationTasks"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;->httpClient_:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 140
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 142
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 145
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SIS registration call failed: Status code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 150
    :cond_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->extractHttpResponse(Ljava/io/InputStream;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 155
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 157
    const-string v1, "rcode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/amazon/device/ads/AdRegistrationTasks;->getIntegerFromJSON(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v1

    .line 158
    const-string v2, "msg"

    const-string v3, ""

    invoke-static {v0, v2, v3}, Lcom/amazon/device/ads/AdRegistrationTasks;->getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    const-string v3, "adId"

    const-string v4, ""

    invoke-static {v0, v3, v4}, Lcom/amazon/device/ads/AdRegistrationTasks;->getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 162
    new-instance v1, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;

    iget-object v2, p1, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->context_:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v1

    .line 165
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "SIS failed registering device -- code: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", msg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :catch_0
    move-exception v0

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JSON error parsing return from SIS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 175
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method
