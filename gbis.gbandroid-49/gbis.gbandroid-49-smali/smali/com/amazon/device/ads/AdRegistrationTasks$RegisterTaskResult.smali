.class public Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;
.super Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRegistrationTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RegisterTaskResult"
.end annotation


# instance fields
.field protected adid_:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;-><init>(Landroid/content/Context;)V

    .line 189
    iput-object p2, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;->adid_:Ljava/lang/String;

    .line 190
    const-string v0, "AmazonAdRegistrationTasks"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Register Device returned adid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 3

    .prologue
    .line 196
    invoke-static {}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    .line 197
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;->adid_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;->context_:Landroid/content/Context;

    const-string v1, "AmazonMobileAds"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 200
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 201
    const-string v1, "amzn-ad-id"

    iget-object v2, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;->adid_:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 202
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 208
    invoke-static {}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;->adid_:Ljava/lang/String;

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    .line 210
    :cond_1
    return-void
.end method
