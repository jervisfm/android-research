.class public Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;
.super Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRegistrationTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "UpdateDeviceInfoTask"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected executeA9Request(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;)Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 224
    invoke-static {p1, v6}, Lcom/amazon/device/ads/AdRegistrationTasks;->buildDeviceInfoParams(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;Z)Ljava/lang/String;

    move-result-object v0

    .line 225
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 226
    const-string v2, "AmazonAdRegistrationTasks"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;->httpClient_:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 228
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 230
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 233
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SIS update_device_info call failed: Status code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 234
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 238
    :cond_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->extractHttpResponse(Ljava/io/InputStream;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 242
    :try_start_0
    new-instance v1, Lorg/json/JSONTokener;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 244
    const-string v1, "rcode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/amazon/device/ads/AdRegistrationTasks;->getIntegerFromJSON(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v1

    .line 245
    const-string v2, "msg"

    const-string v3, ""

    invoke-static {v0, v2, v3}, Lcom/amazon/device/ads/AdRegistrationTasks;->getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 251
    const-string v3, "adId"

    const-string v4, ""

    invoke-static {v0, v3, v4}, Lcom/amazon/device/ads/AdRegistrationTasks;->getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 252
    const-string v4, "idChanged"

    const-string v5, ""

    invoke-static {v0, v4, v5}, Lcom/amazon/device/ads/AdRegistrationTasks;->getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    if-ne v1, v6, :cond_4

    .line 256
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 259
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$NoOpA9TaskResult;

    iget-object v1, p1, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->context_:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amazon/device/ads/AdRegistrationTasks$NoOpA9TaskResult;-><init>(Landroid/content/Context;)V

    .line 264
    :goto_0
    return-object v0

    .line 261
    :cond_2
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;

    iget-object v1, p1, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->context_:Landroid/content/Context;

    invoke-direct {v0, v1, v3}, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;-><init>(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 274
    :catch_0
    move-exception v0

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JSON error parsing return from SIS: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 264
    :cond_3
    :try_start_1
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$NoOpA9TaskResult;

    iget-object v1, p1, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->context_:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amazon/device/ads/AdRegistrationTasks$NoOpA9TaskResult;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 268
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "SIS failed updating device info -- code: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", msg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 270
    const-string v1, "AmazonAdRegistrationTasks"

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
.end method
