.class final Lcom/amazon/device/ads/AdRegistrationTasks;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdRegistrationTasks$PingTask;,
        Lcom/amazon/device/ads/AdRegistrationTasks$NoOpA9TaskResult;,
        Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;,
        Lcom/amazon/device/ads/AdRegistrationTasks$RegisterTaskResult;,
        Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;,
        Lcom/amazon/device/ads/AdRegistrationTasks$A9RequestTask;,
        Lcom/amazon/device/ads/AdRegistrationTasks$A9TaskResult;,
        Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;
    }
.end annotation


# static fields
.field public static HTTP_TIMEOUT:I = 0x0

.field public static final LOG_TAG:Ljava/lang/String; = "AmazonAdRegistrationTasks"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x4e20

    sput v0, Lcom/amazon/device/ads/AdRegistrationTasks;->HTTP_TIMEOUT:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    return-void
.end method

.method protected static buildDeviceInfoParams(Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;Z)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 370
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->adSystemEndpoint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->adSystemHandler:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    if-eqz p1, :cond_b

    .line 374
    const-string v1, "/update_dev_info"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    const-string v1, "?adId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->amazonDeviceId_:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AmazonDeviceId is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :cond_0
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->amazonDeviceId_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const-string v1, "&dt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    :goto_0
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->dt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    const-string v1, "&app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->app:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    const-string v1, "&aud="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->aud:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    const-string v1, "&appId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 396
    const-string v1, "&sha1_mac="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_1
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 401
    const-string v1, "&sha1_udid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    :cond_2
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 406
    const-string v1, "&sha1_tel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    :cond_3
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 411
    const-string v1, "&sha1_serial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    :cond_4
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    if-eqz v1, :cond_5

    .line 416
    const-string v1, "&dinfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->urlEncodedJson:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    :cond_5
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->ua:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 421
    const-string v1, "&ua="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->ua:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    :cond_6
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-boolean v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_mac:Z

    if-eqz v1, :cond_7

    .line 426
    const-string v1, "&badMac=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    :cond_7
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-boolean v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_serial:Z

    if-eqz v1, :cond_8

    .line 430
    const-string v1, "&badSerial=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    :cond_8
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-boolean v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_tel:Z

    if-eqz v1, :cond_9

    .line 434
    const-string v1, "&badTel=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    :cond_9
    iget-object v1, p0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-boolean v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_udid:Z

    if-eqz v1, :cond_a

    .line 438
    const-string v1, "&badUdid=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    :cond_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 383
    :cond_b
    const-string v1, "/generate_did"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    const-string v1, "?dt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method protected static getIntegerFromJSON(Lorg/json/JSONObject;Ljava/lang/String;I)I
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 462
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 471
    :goto_0
    return p2

    .line 469
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected static getStringFromJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 447
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 456
    :goto_0
    return-object p2

    .line 454
    :catch_0
    move-exception v0

    goto :goto_0
.end method
