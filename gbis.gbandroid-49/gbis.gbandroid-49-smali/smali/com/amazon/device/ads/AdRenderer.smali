.class abstract Lcom/amazon/device/ads/AdRenderer;
.super Ljava/lang/Object;
.source "GBFile"


# static fields
.field protected static final AAX_REDIRECT_BETA:Ljava/lang/String; = "aax-beta.integ.amazon.com"

.field protected static final AAX_REDIRECT_GAMMA:Ljava/lang/String; = "aax-us-east.amazon-adsystem.com"

.field protected static final AAX_REDIRECT_PROD:Ljava/lang/String; = "aax-us-east.amazon-adsystem.com"

.field protected static final CORNERSTONE_BEST_ENDPOINT_BETA:Ljava/lang/String; = "d16g-cornerstone-bes.integ.amazon.com"

.field protected static final CORNERSTONE_BEST_ENDPOINT_PROD:Ljava/lang/String; = "pda-bes.amazon.com"

.field private static final LOG_TAG:Ljava/lang/String; = "AdRenderer"


# instance fields
.field protected ad_:Lcom/amazon/device/ads/AdResponse;

.field protected bridge_:Lcom/amazon/device/ads/AdBridge;

.field protected isDestroyed_:Z

.field protected redirectHosts_:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected scalingFactor_:D

.field protected usingDownscalingLogic_:Z

.field protected viewRemoved_:Z


# direct methods
.method protected constructor <init>(Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v3, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    .line 32
    iput-object v3, p0, Lcom/amazon/device/ads/AdRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    .line 34
    iput-boolean v2, p0, Lcom/amazon/device/ads/AdRenderer;->viewRemoved_:Z

    .line 35
    iput-boolean v2, p0, Lcom/amazon/device/ads/AdRenderer;->isDestroyed_:Z

    .line 37
    const-wide/high16 v0, 0x3ff0

    iput-wide v0, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    .line 38
    iput-boolean v2, p0, Lcom/amazon/device/ads/AdRenderer;->usingDownscalingLogic_:Z

    .line 40
    iput-object v3, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    .line 44
    iput-object p1, p0, Lcom/amazon/device/ads/AdRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    .line 45
    iput-object p2, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    .line 48
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    const-string v1, "aax-us-east.amazon-adsystem.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    const-string v1, "aax-us-east.amazon-adsystem.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    const-string v1, "aax-beta.integ.amazon.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    const-string v1, "pda-bes.amazon.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->redirectHosts_:Ljava/util/Set;

    const-string v1, "d16g-cornerstone-bes.integ.amazon.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdRenderer;->determineScalingFactor()V

    .line 55
    return-void
.end method


# virtual methods
.method protected adLoaded(Lcom/amazon/device/ads/AdProperties;)V
    .locals 2
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdBridge;->setIsLoading(Z)V

    .line 138
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/AdBridge;->adLoaded(Lcom/amazon/device/ads/AdProperties;)V

    .line 139
    return-void
.end method

.method protected abstract destroy()V
.end method

.method protected determineScalingFactor()V
    .locals 8

    .prologue
    .line 65
    iget-object v0, p0, Lcom/amazon/device/ads/AdRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget v0, v0, Lcom/amazon/device/ads/AdResponse;->adSizeWidth_:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getScalingDensity()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 66
    iget-object v1, p0, Lcom/amazon/device/ads/AdRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget v1, v1, Lcom/amazon/device/ads/AdResponse;->adSizeHeight_:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getScalingDensity()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 68
    iget-object v2, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getWindowWidth()I

    move-result v2

    int-to-double v2, v2

    int-to-double v4, v0

    div-double/2addr v2, v4

    .line 69
    iget-object v4, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v4}, Lcom/amazon/device/ads/AdBridge;->getWindowHeight()I

    move-result v4

    int-to-double v4, v4

    int-to-double v6, v1

    div-double/2addr v4, v6

    .line 71
    cmpg-double v6, v4, v2

    if-gez v6, :cond_1

    .line 72
    iput-wide v4, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    .line 76
    :goto_0
    iget-wide v2, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    const-wide/high16 v4, 0x3ff0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 84
    iget-wide v2, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    iget-object v4, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v4}, Lcom/amazon/device/ads/AdBridge;->getScalingDensity()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    .line 85
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/amazon/device/ads/AdRenderer;->usingDownscalingLogic_:Z

    .line 88
    :cond_0
    const-string v2, "AdRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SCALING params: scalingDensity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v4}, Lcom/amazon/device/ads/AdBridge;->getScalingDensity()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", windowWidth: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v4}, Lcom/amazon/device/ads/AdBridge;->getWindowWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", windowHeight: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v4}, Lcom/amazon/device/ads/AdBridge;->getWindowHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", adWidth: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", adHeight: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scalingFactor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void

    .line 74
    :cond_1
    iput-wide v2, p0, Lcom/amazon/device/ads/AdRenderer;->scalingFactor_:D

    goto :goto_0
.end method

.method protected isAdViewDestroyed()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdRenderer;->isDestroyed_:Z

    return v0
.end method

.method protected isAdViewRemoved()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdRenderer;->viewRemoved_:Z

    return v0
.end method

.method protected launchExternalBrowserForLink(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdRenderer;->isAdViewDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 102
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string p1, "about:blank"

    .line 103
    :cond_2
    const-string v0, "AdRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Final URI to show in browser: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 105
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    :try_start_0
    iget-object v1, p0, Lcom/amazon/device/ads/AdRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 113
    const-string v1, "market://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    const-string v1, "AdRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not handle market action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_3
    const-string v1, "AdRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not handle intent action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract prepareToGoAway()V
.end method

.method protected abstract removeView()V
.end method

.method protected abstract render()V
.end method

.method protected abstract sendCommand(Ljava/lang/String;Ljava/util/Map;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method

.method protected setAdResponse(Lcom/amazon/device/ads/AdResponse;)V
    .locals 0
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/amazon/device/ads/AdRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    .line 123
    return-void
.end method

.method protected abstract shouldReuse()Z
.end method
