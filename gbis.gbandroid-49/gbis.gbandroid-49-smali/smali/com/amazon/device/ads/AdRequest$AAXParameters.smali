.class final enum Lcom/amazon/device/ads/AdRequest$AAXParameters;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "AAXParameters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amazon/device/ads/AdRequest$AAXParameters;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum ADID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum APPID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum ATF:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum CHANNEL:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum DEVICE_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum GEOLOCATION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum MD5_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum PAGE_TYPE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum PUBLISHER_ASINS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum PUBLISHER_KEYWORDS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum SDK_VERSION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum SHA1_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum SIZE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum SLOT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum SLOT_POSITION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum TEST:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum USER_AGENT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

.field public static final enum USER_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;


# instance fields
.field private final defaultValue_:Ljava/lang/String;

.field final name_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 31
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "APPID"

    const-string v2, "appId"

    invoke-direct {v0, v1, v6, v2, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->APPID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 32
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "CHANNEL"

    const-string v2, "c"

    invoke-direct {v0, v1, v7, v2, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->CHANNEL:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 33
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "SIZE"

    const-string v2, "sz"

    invoke-direct {v0, v1, v8, v2, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SIZE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 34
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "PAGE_TYPE"

    const-string v2, "pt"

    invoke-direct {v0, v1, v9, v2, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PAGE_TYPE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 35
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "SLOT"

    const/4 v2, 0x4

    const-string v3, "slot"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SLOT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 36
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "PUBLISHER_KEYWORDS"

    const/4 v2, 0x5

    const-string v3, "pk"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PUBLISHER_KEYWORDS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 37
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "PUBLISHER_ASINS"

    const/4 v2, 0x6

    const-string v3, "pa"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PUBLISHER_ASINS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 38
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "USER_AGENT"

    const/4 v2, 0x7

    const-string v3, "ua"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_AGENT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 39
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "SDK_VERSION"

    const/16 v2, 0x8

    const-string v3, "adsdk"

    invoke-static {}, Lcom/amazon/device/ads/InternalAdRegistration;->getInstance()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/amazon/device/ads/InternalAdRegistration;->getSDKVersionID()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SDK_VERSION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 40
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "GEOLOCATION"

    const/16 v2, 0x9

    const-string v3, "geoloc"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->GEOLOCATION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 41
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "USER_INFO"

    const/16 v2, 0xa

    const-string v3, "uinfo"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 42
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "DEVICE_INFO"

    const/16 v2, 0xb

    const-string v3, "dinfo"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->DEVICE_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 43
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "TEST"

    const/16 v2, 0xc

    const-string v3, "isTest"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->TEST:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 44
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "ATF"

    const/16 v2, 0xd

    const-string v3, "atf"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->ATF:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 45
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "ADID"

    const/16 v2, 0xe

    const-string v3, "ad-id"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->ADID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 46
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "SHA1_UDID"

    const/16 v2, 0xf

    const-string v3, "sha1_udid"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SHA1_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 47
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "MD5_UDID"

    const/16 v2, 0x10

    const-string v3, "md5_udid"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->MD5_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 48
    new-instance v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    const-string v1, "SLOT_POSITION"

    const/16 v2, 0x11

    const-string v3, "sp"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/amazon/device/ads/AdRequest$AAXParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SLOT_POSITION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    .line 29
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/amazon/device/ads/AdRequest$AAXParameters;

    sget-object v1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->APPID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v1, v0, v6

    sget-object v1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->CHANNEL:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v1, v0, v7

    sget-object v1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SIZE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v1, v0, v8

    sget-object v1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PAGE_TYPE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SLOT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PUBLISHER_KEYWORDS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->PUBLISHER_ASINS:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_AGENT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SDK_VERSION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->GEOLOCATION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->DEVICE_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->TEST:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->ATF:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->ADID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SHA1_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->MD5_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SLOT_POSITION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->$VALUES:[Lcom/amazon/device/ads/AdRequest$AAXParameters;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->name_:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->defaultValue_:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/device/ads/AdRequest$AAXParameters;
    .locals 1
    .parameter

    .prologue
    .line 29
    const-class v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;

    return-object v0
.end method

.method public static values()[Lcom/amazon/device/ads/AdRequest$AAXParameters;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->$VALUES:[Lcom/amazon/device/ads/AdRequest$AAXParameters;

    invoke-virtual {v0}, [Lcom/amazon/device/ads/AdRequest$AAXParameters;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amazon/device/ads/AdRequest$AAXParameters;

    return-object v0
.end method


# virtual methods
.method public final getDefaultValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->defaultValue_:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrlComponent(C)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->name_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
