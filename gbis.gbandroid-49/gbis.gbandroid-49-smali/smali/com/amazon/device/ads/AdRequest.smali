.class Lcom/amazon/device/ads/AdRequest;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdRequest$AAXParameters;
    }
.end annotation


# static fields
.field protected static LOG_TAG:Ljava/lang/String;


# instance fields
.field protected bridge_:Lcom/amazon/device/ads/AdBridge;

.field protected opt_:Lcom/amazon/device/ads/AdTargetingOptions;

.field protected size_:Lcom/amazon/device/ads/AdLayout$AdSize;

.field protected timeout_:I

.field protected url_:Ljava/lang/StringBuilder;

.field protected userAgent_:Ljava/lang/String;

.field protected windowHeight_:I

.field protected windowWidth_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "AdRequest"

    sput-object v0, Lcom/amazon/device/ads/AdRequest;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/amazon/device/ads/AdBridge;Lcom/amazon/device/ads/AdTargetingOptions;Lcom/amazon/device/ads/AdLayout$AdSize;IILjava/lang/String;I)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    .line 91
    iput-object p1, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    .line 92
    iput-object p2, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    .line 93
    iput-object p3, p0, Lcom/amazon/device/ads/AdRequest;->size_:Lcom/amazon/device/ads/AdLayout$AdSize;

    .line 94
    iput-object p6, p0, Lcom/amazon/device/ads/AdRequest;->userAgent_:Ljava/lang/String;

    .line 95
    iput p7, p0, Lcom/amazon/device/ads/AdRequest;->timeout_:I

    .line 96
    iput p5, p0, Lcom/amazon/device/ads/AdRequest;->windowHeight_:I

    .line 97
    iput p4, p0, Lcom/amazon/device/ads/AdRequest;->windowWidth_:I

    .line 99
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdRequest;->initialize()V

    .line 100
    return-void
.end method

.method private getAAXParam(Lcom/amazon/device/ads/AdRequest$AAXParameters;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazon/device/ads/AdRequest$AAXParameters;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 148
    invoke-virtual {p1}, Lcom/amazon/device/ads/AdRequest$AAXParameters;->getDefaultValue()Ljava/lang/String;

    move-result-object v2

    .line 149
    iget-object v0, p1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->name_:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->name_:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    iget-object v2, p1, Lcom/amazon/device/ads/AdRequest$AAXParameters;->name_:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 159
    :cond_0
    if-nez v2, :cond_10

    .line 161
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SIZE:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->size_:Lcom/amazon/device/ads/AdLayout$AdSize;

    iget-object v3, v0, Lcom/amazon/device/ads/AdLayout$AdSize;->size:Ljava/lang/String;

    .line 259
    :cond_1
    :goto_0
    return-object v3

    .line 165
    :cond_2
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->APPID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getAppId()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 169
    :cond_3
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->ADID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_4

    .line 171
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getAmazonDeviceId()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 173
    :cond_4
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_AGENT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_5

    .line 175
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->userAgent_:Ljava/lang/String;

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 177
    :cond_5
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->DEVICE_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getDeviceNativeData()Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    if-eqz v0, :cond_7

    .line 184
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->isPortrait(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 185
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    const-string v1, "portrait"

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->orientation:Ljava/lang/String;

    .line 188
    :goto_1
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getDeviceNativeData()Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->getJsonEncodedWithOrientation()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 187
    :cond_6
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    const-string v1, "landscape"

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->orientation:Ljava/lang/String;

    goto :goto_1

    .line 190
    :cond_7
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->USER_INFO:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_9

    .line 192
    const/4 v3, 0x0

    .line 193
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 196
    :try_start_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdTargetingOptions;->getAge()I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_12

    .line 198
    const-string v0, "age"

    iget-object v5, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v5}, Lcom/amazon/device/ads/AdTargetingOptions;->getAge()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 202
    :goto_2
    :try_start_1
    iget-object v3, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v3}, Lcom/amazon/device/ads/AdTargetingOptions;->getGender()Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    move-result-object v3

    sget-object v5, Lcom/amazon/device/ads/AdTargetingOptions$Gender;->UNKNOWN:Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    if-eq v3, v5, :cond_8

    .line 204
    const-string v3, "gender"

    iget-object v5, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v5}, Lcom/amazon/device/ads/AdTargetingOptions;->getGender()Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    move-result-object v5

    iget-object v5, v5, Lcom/amazon/device/ads/AdTargetingOptions$Gender;->gender:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v1

    .line 213
    :cond_8
    :goto_3
    if-eqz v0, :cond_11

    .line 214
    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    move-object v3, v0

    .line 215
    goto/16 :goto_0

    .line 208
    :catch_0
    move-exception v0

    move v1, v3

    .line 210
    :goto_5
    sget-object v3, Lcom/amazon/device/ads/AdRequest;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JSON error creating \'ui\' object: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_3

    .line 216
    :cond_9
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->TEST:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_b

    .line 218
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->getTestMode()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "true"

    :goto_6
    move-object v3, v0

    goto/16 :goto_0

    :cond_a
    move-object v0, v3

    goto :goto_6

    .line 220
    :cond_b
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->GEOLOCATION:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_c

    .line 222
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdTargetingOptions;->isGeoLocationEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 225
    if-eqz v0, :cond_1

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 237
    :cond_c
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SHA1_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_d

    .line 239
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v3, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    goto/16 :goto_0

    .line 241
    :cond_d
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->MD5_UDID:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_e

    .line 243
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_udid:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v0

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v3, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_udid:Ljava/lang/String;

    goto/16 :goto_0

    .line 245
    :cond_e
    sget-object v0, Lcom/amazon/device/ads/AdRequest$AAXParameters;->SLOT:Lcom/amazon/device/ads/AdRequest$AAXParameters;

    if-ne p1, v0, :cond_10

    .line 252
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->isPortrait(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 253
    const-string v3, "portrait"

    goto/16 :goto_0

    .line 255
    :cond_f
    const-string v3, "landscape"

    goto/16 :goto_0

    .line 208
    :catch_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto/16 :goto_5

    :cond_10
    move-object v3, v2

    goto/16 :goto_0

    :cond_11
    move-object v0, v2

    goto/16 :goto_4

    :cond_12
    move v0, v3

    goto/16 :goto_2
.end method


# virtual methods
.method public initialize()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/InternalAdRegistration;->getEndpoint()Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    move-result-object v2

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->aaxEndpoint:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/amazon/device/ads/AdRequest;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getAdRegistration()Lcom/amazon/device/ads/InternalAdRegistration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/InternalAdRegistration;->getEndpoint()Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    move-result-object v2

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->aaxHandler:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const/4 v0, 0x1

    .line 108
    iget-object v2, p0, Lcom/amazon/device/ads/AdRequest;->opt_:Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdTargetingOptions;->getCopyOfAdvancedOptions()Ljava/util/HashMap;

    move-result-object v4

    .line 109
    invoke-static {}, Lcom/amazon/device/ads/AdRequest$AAXParameters;->values()[Lcom/amazon/device/ads/AdRequest$AAXParameters;

    move-result-object v5

    array-length v6, v5

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 113
    :try_start_0
    invoke-direct {p0, v7, v4}, Lcom/amazon/device/ads/AdRequest;->getAAXParam(Lcom/amazon/device/ads/AdRequest$AAXParameters;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v8

    .line 114
    if-eqz v8, :cond_0

    .line 116
    iget-object v9, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_1

    const/16 v2, 0x3f

    :goto_1
    invoke-virtual {v7, v2}, Lcom/amazon/device/ads/AdRequest$AAXParameters;->getUrlComponent(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 123
    :cond_0
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 116
    :cond_1
    const/16 v2, 0x26

    goto :goto_1

    .line 136
    :cond_2
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 138
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    const-string v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget-object v3, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    iget-object v0, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    iget-object v3, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 144
    :cond_3
    sget-object v0, Lcom/amazon/device/ads/AdRequest;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Generated AAX url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/amazon/device/ads/AdRequest;->url_:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-void

    .line 120
    :catch_0
    move-exception v2

    goto :goto_2
.end method
