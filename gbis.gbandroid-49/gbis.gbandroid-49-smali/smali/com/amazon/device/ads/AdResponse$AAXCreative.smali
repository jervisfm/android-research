.class public final enum Lcom/amazon/device/ads/AdResponse$AAXCreative;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/AdResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "AAXCreative"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/amazon/device/ads/AdResponse$AAXCreative;

.field public static final enum HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

.field public static final enum MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;


# instance fields
.field private final class_:Ljava/lang/String;

.field private final id_:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    new-instance v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;

    const-string v1, "HTML"

    const/16 v2, 0x3ef

    const-string v3, "com.amazon.device.ads.HtmlRenderer"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/amazon/device/ads/AdResponse$AAXCreative;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    .line 29
    new-instance v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;

    const-string v1, "MRAID1"

    const/16 v2, 0x3f8

    const-string v3, "com.amazon.device.ads.MraidRenderer"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/amazon/device/ads/AdResponse$AAXCreative;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/amazon/device/ads/AdResponse$AAXCreative;

    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    aput-object v1, v0, v4

    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    aput-object v1, v0, v5

    sput-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->$VALUES:[Lcom/amazon/device/ads/AdResponse$AAXCreative;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->id_:I

    .line 39
    iput-object p4, p0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->class_:Ljava/lang/String;

    .line 40
    return-void
.end method

.method static determineTypeOrder(Ljava/util/HashSet;)Ljava/util/ArrayList;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 76
    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-virtual {p0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_0
    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-virtual {p0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    sget-object v1, Lcom/amazon/device/ads/AdResponse$AAXCreative;->HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_1
    return-object v0
.end method

.method static getCreative(I)Lcom/amazon/device/ads/AdResponse$AAXCreative;
    .locals 3
    .parameter

    .prologue
    .line 49
    sparse-switch p0, :sswitch_data_0

    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid creative type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :sswitch_0
    sget-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->HTML:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    .line 54
    :goto_0
    return-object v0

    :sswitch_1
    sget-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->MRAID1:Lcom/amazon/device/ads/AdResponse$AAXCreative;

    goto :goto_0

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x3ef -> :sswitch_0
        0x3f8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/device/ads/AdResponse$AAXCreative;
    .locals 1
    .parameter

    .prologue
    .line 26
    const-class v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;

    return-object v0
.end method

.method public static values()[Lcom/amazon/device/ads/AdResponse$AAXCreative;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->$VALUES:[Lcom/amazon/device/ads/AdResponse$AAXCreative;

    invoke-virtual {v0}, [Lcom/amazon/device/ads/AdResponse$AAXCreative;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amazon/device/ads/AdResponse$AAXCreative;

    return-object v0
.end method


# virtual methods
.method final getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/amazon/device/ads/AdResponse$AAXCreative;->class_:Ljava/lang/String;

    return-object v0
.end method
