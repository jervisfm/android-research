.class Lcom/amazon/device/ads/AdResponse;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdResponse$AAXCreative;
    }
.end annotation


# instance fields
.field protected adSizeHeight_:I

.field protected adSizeWidth_:I

.field protected creativeTypes_:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
            ">;"
        }
    .end annotation
.end field

.field protected creative_:Ljava/lang/String;

.field protected properties_:Lcom/amazon/device/ads/AdProperties;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/AdProperties;Ljava/lang/String;Ljava/util/ArrayList;II)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazon/device/ads/AdProperties;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput v0, p0, Lcom/amazon/device/ads/AdResponse;->adSizeHeight_:I

    .line 94
    iput v0, p0, Lcom/amazon/device/ads/AdResponse;->adSizeWidth_:I

    .line 98
    iput-object p2, p0, Lcom/amazon/device/ads/AdResponse;->creative_:Ljava/lang/String;

    .line 99
    iput-object p1, p0, Lcom/amazon/device/ads/AdResponse;->properties_:Lcom/amazon/device/ads/AdProperties;

    .line 100
    iput-object p3, p0, Lcom/amazon/device/ads/AdResponse;->creativeTypes_:Ljava/util/ArrayList;

    .line 101
    iput p5, p0, Lcom/amazon/device/ads/AdResponse;->adSizeHeight_:I

    .line 102
    iput p4, p0, Lcom/amazon/device/ads/AdResponse;->adSizeWidth_:I

    .line 103
    return-void
.end method


# virtual methods
.method protected getAdSizeHeight()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/amazon/device/ads/AdResponse;->adSizeHeight_:I

    return v0
.end method

.method protected getAdSizeWidth()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/amazon/device/ads/AdResponse;->adSizeWidth_:I

    return v0
.end method

.method protected getCreative()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/amazon/device/ads/AdResponse;->creative_:Ljava/lang/String;

    return-object v0
.end method

.method protected getCreativeTypes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amazon/device/ads/AdResponse$AAXCreative;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/amazon/device/ads/AdResponse;->creativeTypes_:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getProperties()Lcom/amazon/device/ads/AdProperties;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/amazon/device/ads/AdResponse;->properties_:Lcom/amazon/device/ads/AdProperties;

    return-object v0
.end method
