.class public Lcom/amazon/device/ads/AdTargetingOptions;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdTargetingOptions$Gender;
    }
.end annotation


# instance fields
.field private advanced_:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private age_:I

.field private enableGeoTargeting_:Z

.field private gender_:Lcom/amazon/device/ads/AdTargetingOptions$Gender;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->enableGeoTargeting_:Z

    .line 33
    sget-object v0, Lcom/amazon/device/ads/AdTargetingOptions$Gender;->UNKNOWN:Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    iput-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->gender_:Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->age_:I

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    .line 36
    return-void
.end method


# virtual methods
.method public containsAdvancedOption(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public enableGeoLocation(Z)Lcom/amazon/device/ads/AdTargetingOptions;
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/amazon/device/ads/AdTargetingOptions;->enableGeoTargeting_:Z

    .line 54
    return-object p0
.end method

.method public getAdvancedOption(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAge()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->age_:I

    return v0
.end method

.method protected getCopyOfAdvancedOptions()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 164
    iget-object v1, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 165
    return-object v0
.end method

.method public getGender()Lcom/amazon/device/ads/AdTargetingOptions$Gender;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->gender_:Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    return-object v0
.end method

.method public isGeoLocationEnabled()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->enableGeoTargeting_:Z

    return v0
.end method

.method public setAdvancedOption(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/device/ads/AdTargetingOptions;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 131
    if-eqz p2, :cond_0

    .line 132
    iget-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    :goto_0
    return-object p0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdTargetingOptions;->advanced_:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setAge(I)Lcom/amazon/device/ads/AdTargetingOptions;
    .locals 0
    .parameter

    .prologue
    .line 102
    iput p1, p0, Lcom/amazon/device/ads/AdTargetingOptions;->age_:I

    .line 103
    return-object p0
.end method

.method public setGender(Lcom/amazon/device/ads/AdTargetingOptions$Gender;)Lcom/amazon/device/ads/AdTargetingOptions;
    .locals 0
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lcom/amazon/device/ads/AdTargetingOptions;->gender_:Lcom/amazon/device/ads/AdTargetingOptions$Gender;

    .line 78
    return-object p0
.end method
