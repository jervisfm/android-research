.class final Lcom/amazon/device/ads/AdVideoPlayer;
.super Landroid/widget/VideoView;
.source "GBFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# instance fields
.field private audioManager_:Landroid/media/AudioManager;

.field private contentUrl_:Ljava/lang/String;

.field private context_:Landroid/content/Context;

.field private listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

.field private playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

.field private released_:Z

.field private volumeBeforeMuting_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "AdVideoPlayer"

    sput-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->released_:Z

    .line 38
    iput-object p1, p0, Lcom/amazon/device/ads/AdVideoPlayer;->context_:Landroid/content/Context;

    .line 40
    invoke-virtual {p0, p0}, Lcom/amazon/device/ads/AdVideoPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 41
    invoke-virtual {p0, p0}, Lcom/amazon/device/ads/AdVideoPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 42
    invoke-virtual {p0, p0}, Lcom/amazon/device/ads/AdVideoPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 44
    new-instance v0, Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-direct {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    .line 45
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->context_:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->audioManager_:Landroid/media/AudioManager;

    .line 46
    return-void
.end method

.method private displayPlayerControls()V
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in displayPlayerControls"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->showControl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    new-instance v0, Landroid/widget/MediaController;

    iget-object v1, p0, Lcom/amazon/device/ads/AdVideoPlayer;->context_:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 107
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/AdVideoPlayer;->setMediaController(Landroid/widget/MediaController;)V

    .line 108
    invoke-virtual {v0, p0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 109
    invoke-virtual {v0}, Landroid/widget/MediaController;->requestFocus()Z

    .line 111
    :cond_0
    return-void
.end method

.method private loadPlayerContent()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->contentUrl_:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 90
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/AdVideoPlayer;->setVideoURI(Landroid/net/Uri;)V

    .line 91
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->startPlaying()V

    .line 92
    return-void
.end method

.method private removePlayerFromParent()V
    .locals 2

    .prologue
    .line 138
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in removePlayerFromParent"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 140
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 141
    :cond_0
    return-void
.end method


# virtual methods
.method public final mutePlayer()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 76
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in mutePlayer"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->audioManager_:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->volumeBeforeMuting_:I

    .line 78
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->audioManager_:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 79
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->doLoop()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->start()V

    .line 133
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    invoke-interface {v0}, Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;->onComplete()V

    .line 134
    :cond_1
    return-void

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->exitOnComplete()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    iget-boolean v0, v0, Lcom/amazon/device/ads/Controller$PlayerProperties;->inline:Z

    if-eqz v0, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->releasePlayer()V

    goto :goto_0
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->removePlayerFromParent()V

    .line 123
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    invoke-interface {v0}, Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;->onError()V

    .line 124
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    invoke-interface {v0}, Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;->onPrepared()V

    .line 117
    :cond_0
    return-void
.end method

.method public final playAudio()V
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in playAudio"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->loadPlayerContent()V

    .line 72
    return-void
.end method

.method public final playVideo()V
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in playVideo"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->doMute()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->mutePlayer()V

    .line 64
    :cond_0
    invoke-direct {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->loadPlayerContent()V

    .line 65
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->startPlaying()V

    .line 66
    return-void
.end method

.method public final releasePlayer()V
    .locals 2

    .prologue
    .line 145
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in releasePlayer"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-boolean v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->released_:Z

    if-eqz v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->released_:Z

    .line 151
    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->stopPlayback()V

    .line 152
    invoke-direct {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->removePlayerFromParent()V

    .line 154
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->doMute()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->unmutePlayer()V

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    invoke-interface {v0}, Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;->onComplete()V

    goto :goto_0
.end method

.method public final setListener(Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/amazon/device/ads/AdVideoPlayer;->listener_:Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;

    .line 58
    return-void
.end method

.method public final setPlayData(Lcom/amazon/device/ads/Controller$PlayerProperties;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->released_:Z

    .line 51
    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    .line 52
    :cond_0
    iput-object p2, p0, Lcom/amazon/device/ads/AdVideoPlayer;->contentUrl_:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public final startPlaying()V
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in startPlaying"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->displayPlayerControls()V

    .line 98
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->playerProperties_:Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-virtual {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amazon/device/ads/AdVideoPlayer;->start()V

    .line 99
    :cond_0
    return-void
.end method

.method public final unmutePlayer()V
    .locals 4

    .prologue
    .line 83
    sget-object v0, Lcom/amazon/device/ads/AdVideoPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "in unmutePlayer"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/amazon/device/ads/AdVideoPlayer;->audioManager_:Landroid/media/AudioManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/amazon/device/ads/AdVideoPlayer;->volumeBeforeMuting_:I

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 85
    return-void
.end method
