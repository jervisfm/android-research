.class public Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/HtmlRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "AdWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/HtmlRenderer;


# direct methods
.method protected constructor <init>(Lcom/amazon/device/ads/HtmlRenderer;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-boolean v0, v0, Lcom/amazon/device/ads/HtmlRenderer;->hasLoadedAd_:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amazon/device/ads/HtmlRenderer;->hasLoadedAd_:Z

    .line 95
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-object v1, v1, Lcom/amazon/device/ads/HtmlRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget-object v1, v1, Lcom/amazon/device/ads/AdResponse;->properties_:Lcom/amazon/device/ads/AdProperties;

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/HtmlRenderer;->adLoaded(Lcom/amazon/device/ads/AdProperties;)V

    .line 97
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 44
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 45
    iget-object v2, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-object v2, v2, Lcom/amazon/device/ads/HtmlRenderer;->redirectHosts_:Ljava/util/Set;

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    #calls: Lcom/amazon/device/ads/HtmlRenderer;->isHoneycombVersion()Z
    invoke-static {}, Lcom/amazon/device/ads/HtmlRenderer;->access$000()Z

    move-result v2

    if-nez v2, :cond_1

    .line 51
    const/4 v0, 0x0

    .line 86
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mopub"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tel"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "voicemail:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "sms:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mailto:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "geo:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "google.streetview:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 63
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 64
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 67
    :try_start_0
    iget-object v2, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-object v2, v2, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 71
    :catch_0
    move-exception v1

    const-string v1, "HtmlRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not handle intent with URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 75
    :cond_3
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "amazonmobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 77
    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    iget-object v1, v1, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1, p2}, Lcom/amazon/device/ads/AdBridge;->specialUrlClicked(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    :cond_4
    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;->this$0:Lcom/amazon/device/ads/HtmlRenderer;

    invoke-virtual {v1, p2}, Lcom/amazon/device/ads/HtmlRenderer;->launchExternalBrowserForLink(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
