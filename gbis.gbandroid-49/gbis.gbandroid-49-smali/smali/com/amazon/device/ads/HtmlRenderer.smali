.class Lcom/amazon/device/ads/HtmlRenderer;
.super Lcom/amazon/device/ads/AdRenderer;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HtmlRenderer"


# instance fields
.field protected adView_:Landroid/webkit/WebView;

.field protected hasLoadedAd_:Z


# direct methods
.method protected constructor <init>(Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/AdRenderer;-><init>(Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)V

    .line 30
    iput-boolean v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->hasLoadedAd_:Z

    .line 106
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p2}, Lcom/amazon/device/ads/AdBridge;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    .line 108
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 109
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setHorizontalScrollbarOverlay(Z)V

    .line 110
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 111
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 112
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 113
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 114
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 115
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 117
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    new-instance v1, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/HtmlRenderer$AdWebViewClient;-><init>(Lcom/amazon/device/ads/HtmlRenderer;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 118
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/amazon/device/ads/HtmlRenderer;->isHoneycombVersion()Z

    move-result v0

    return v0
.end method

.method private static isHoneycombVersion()Z
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected adLoaded(Lcom/amazon/device/ads/AdProperties;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, -0x2

    .line 163
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 167
    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getAdLayout()Lcom/amazon/device/ads/AdLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, v0}, Lcom/amazon/device/ads/AdLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    invoke-super {p0, p1}, Lcom/amazon/device/ads/AdRenderer;->adLoaded(Lcom/amazon/device/ads/AdProperties;)V

    .line 170
    return-void
.end method

.method protected destroy()V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->isDestroyed_:Z

    .line 192
    return-void
.end method

.method protected prepareToGoAway()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method protected removeView()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdLayout()Lcom/amazon/device/ads/AdLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdLayout;->removeView(Landroid/view/View;)V

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->viewRemoved_:Z

    .line 184
    return-void
.end method

.method protected render()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amazon/device/ads/HtmlRenderer;->isAdViewDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 127
    iput-boolean v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->hasLoadedAd_:Z

    .line 129
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget-object v0, v0, Lcom/amazon/device/ads/AdResponse;->creative_:Ljava/lang/String;

    .line 131
    iget-boolean v1, p0, Lcom/amazon/device/ads/HtmlRenderer;->usingDownscalingLogic_:Z

    if-eqz v1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    iget-wide v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->scalingFactor_:D

    const-wide/high16 v4, 0x4059

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 149
    :goto_1
    const-string v1, "<head>"

    const-string v2, "<head><script type=\"text/javascript\">htmlWillCallFinishLoad = 1;</script>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 151
    iget-object v0, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    const-string v1, "http://amazon-adsystem.amazon.com/"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/amazon/device/ads/HtmlRenderer;->adView_:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setInitialScale(I)V

    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<html><meta name=\"viewport\" content=\"width="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getWindowWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getWindowHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", initial-scale="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/amazon/device/ads/HtmlRenderer;->scalingFactor_:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected sendCommand(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method protected shouldReuse()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    return v0
.end method
