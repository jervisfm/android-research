.class public final enum Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/InternalAdRegistration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "AmazonAdEndpoint"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum DESKTOP:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum DEVO:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum DEVO_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum GAMMA:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum GAMMA_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum PROD:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field public static final enum PROD_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;


# instance fields
.field public final aaxEndpoint:Ljava/lang/String;

.field public final aaxHandler:Ljava/lang/String;

.field public final adSystemEndpoint:Ljava/lang/String;

.field public final adSystemHandler:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v1, "PROD"

    const-string v3, "s.amazon-adsystem.com"

    const-string v4, "/api3"

    const-string v5, "aax-us-east.amazon-adsystem.com"

    const-string v6, "/x/getad"

    invoke-direct/range {v0 .. v6}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 56
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "GAMMA"

    const-string v6, "s-gamma.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "aax-us-east-gamma.amazon-adsystem.com"

    const-string v9, "/x/getad"

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 57
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "DEVO"

    const-string v6, "s-beta.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "aax-beta.integ.amazon.com"

    const-string v9, "/x/getad"

    move v5, v11

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 58
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "DESKTOP"

    const-string v6, "s-beta.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "antonf-test.integ.amazon.com:2828"

    const-string v9, "/x/getad"

    move v5, v12

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DESKTOP:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 59
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "PROD_MSDK"

    const-string v6, "s.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "aax-us-east.amazon-adsystem.com"

    const-string v9, "/x/msdk"

    move v5, v13

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 60
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "GAMMA_MSDK"

    const/4 v5, 0x5

    const-string v6, "s-gamma.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "aax-us-east-gamma.amazon-adsystem.com"

    const-string v9, "/x/msdk"

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 61
    new-instance v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    const-string v4, "DEVO_MSDK"

    const/4 v5, 0x6

    const-string v6, "s-beta.amazon-adsystem.com"

    const-string v7, "/api3"

    const-string v8, "aax-beta.integ.amazon.com"

    const-string v9, "/x/msdk"

    invoke-direct/range {v3 .. v9}, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 53
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v1, v0, v10

    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v1, v0, v11

    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DESKTOP:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v1, v0, v12

    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->$VALUES:[Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-object p3, p0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->adSystemEndpoint:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->adSystemHandler:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->aaxEndpoint:Ljava/lang/String;

    .line 75
    iput-object p6, p0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->aaxHandler:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;
    .locals 1
    .parameter

    .prologue
    .line 53
    const-class v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    return-object v0
.end method

.method public static values()[Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->$VALUES:[Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    invoke-virtual {v0}, [Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    return-object v0
.end method
