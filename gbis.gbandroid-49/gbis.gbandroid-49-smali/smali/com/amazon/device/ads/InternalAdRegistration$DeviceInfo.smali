.class public Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/InternalAdRegistration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "DeviceInfo"
.end annotation


# instance fields
.field public app:Ljava/lang/String;

.field public appId:Ljava/lang/String;

.field public aud:Ljava/lang/String;

.field public bad_mac:Z

.field public bad_serial:Z

.field public bad_tel:Z

.field public bad_udid:Z

.field public dt:Ljava/lang/String;

.field public md5_mac:Ljava/lang/String;

.field public md5_serial:Ljava/lang/String;

.field public md5_tel:Ljava/lang/String;

.field public md5_udid:Ljava/lang/String;

.field public referrer:Ljava/lang/String;

.field public sha1_mac:Ljava/lang/String;

.field public sha1_serial:Ljava/lang/String;

.field public sha1_tel:Ljava/lang/String;

.field public sha1_udid:Ljava/lang/String;

.field public ua:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    const-string v0, "android"

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->dt:Ljava/lang/String;

    .line 144
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    .line 146
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    .line 147
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    .line 148
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    .line 149
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;

    .line 150
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->referrer:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->ua:Ljava/lang/String;

    .line 153
    iput-boolean v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_serial:Z

    .line 154
    iput-boolean v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_udid:Z

    .line 155
    iput-boolean v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_mac:Z

    .line 156
    iput-boolean v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_tel:Z

    .line 162
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_mac:Ljava/lang/String;

    .line 163
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_udid:Ljava/lang/String;

    .line 164
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_tel:Ljava/lang/String;

    .line 165
    iput-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_serial:Ljava/lang/String;

    .line 174
    const-string v0, "app"

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->app:Ljava/lang/String;

    .line 175
    const-string v0, "3p"

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->aud:Ljava/lang/String;

    .line 177
    invoke-static {p1, p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getMacAddress(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V

    .line 178
    invoke-static {p1, p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getUdid(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V

    .line 179
    invoke-static {p1, p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getTelephonyID(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V

    .line 181
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->setSerial()V

    .line 182
    return-void
.end method


# virtual methods
.method protected setSerial()V
    .locals 2

    .prologue
    .line 188
    :try_start_0
    const-class v0, Landroid/os/Build;

    const-string v1, "SERIAL"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 189
    const-class v1, Landroid/os/Build;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 198
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_serial:Z

    .line 209
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-static {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->generateSha1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    goto :goto_0
.end method
