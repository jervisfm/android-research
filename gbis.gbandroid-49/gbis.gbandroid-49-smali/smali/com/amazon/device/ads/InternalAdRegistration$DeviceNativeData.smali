.class public Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/InternalAdRegistration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "DeviceNativeData"
.end annotation


# instance fields
.field public json:Lorg/json/JSONObject;

.field public final make:Ljava/lang/String;

.field public final model:Ljava/lang/String;

.field public orientation:Ljava/lang/String;

.field public final os:Ljava/lang/String;

.field public final osVersion:Ljava/lang/String;

.field public final screenSize:Ljava/lang/String;

.field public final sf:Ljava/lang/String;

.field public urlEncodedJson:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->orientation:Ljava/lang/String;

    .line 99
    const-string v0, "Android"

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->os:Ljava/lang/String;

    .line 100
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->model:Ljava/lang/String;

    .line 101
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->make:Ljava/lang/String;

    .line 102
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->osVersion:Ljava/lang/String;

    .line 103
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 105
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 106
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->screenSize:Ljava/lang/String;

    .line 109
    iget v0, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->sf:Ljava/lang/String;

    .line 111
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "os"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->os:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "model"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->model:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 116
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "make"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->make:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 117
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "osVersion"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->osVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "screenSize"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->screenSize:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "sf"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->sf:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->urlEncodedJson:Ljava/lang/String;

    .line 125
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getJsonEncodedWithOrientation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 131
    :try_start_0
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    const-string v1, "orientation"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->orientation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->json:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->urlEncodedJson:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->urlEncodedJson:Ljava/lang/String;

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
