.class final Lcom/amazon/device/ads/InternalAdRegistration;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;,
        Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;,
        Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;
    }
.end annotation


# static fields
.field protected static final ADID_PREF_NAME:Ljava/lang/String; = "amzn-ad-id"

.field protected static final APPID_PREF_NAME:Ljava/lang/String; = "amzn-ad-app-id"

.field public static final LOG_TAG:Ljava/lang/String; = "AmazonAdRegistration"

.field protected static final PREFS_NAME:Ljava/lang/String; = "AmazonMobileAds"

.field protected static final SIS_CHECKIN_INTERVAL:J = 0x5265c00L

.field protected static final THIRD_PARTY_APP_DOMAIN:Ljava/lang/String; = "3p"

.field protected static final THIRD_PARTY_APP_NAME:Ljava/lang/String; = "app"

.field private static instance_:Lcom/amazon/device/ads/InternalAdRegistration;


# instance fields
.field protected amznDeviceId_:Ljava/lang/String;

.field protected context_:Landroid/content/Context;

.field protected deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

.field protected deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

.field protected enableLogging_:Z

.field protected endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

.field protected lastSISCheckin_:J

.field private sdkVersionID_:Ljava/lang/String;

.field private testMode_:Z

.field private usingMSDK_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    sput-object v0, Lcom/amazon/device/ads/InternalAdRegistration;->instance_:Lcom/amazon/device/ads/InternalAdRegistration;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 213
    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    .line 214
    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    .line 215
    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    .line 216
    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    .line 217
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->lastSISCheckin_:J

    .line 218
    iput-boolean v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK_:Z

    .line 219
    iput-boolean v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->enableLogging_:Z

    .line 228
    iput-object p1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    .line 229
    const-string v0, "AmazonMobileAds"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 231
    const-string v1, "0x6164616c706861"

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 259
    :goto_0
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getAmazonDeviceAdID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    .line 260
    new-instance v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    invoke-direct {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    .line 261
    new-instance v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    invoke-direct {v0, p1}, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    .line 262
    iput-boolean v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->testMode_:Z

    .line 264
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/InternalAdRegistration;->determineSDKVersion(Landroid/content/Context;)V

    .line 265
    return-void

    .line 234
    :pswitch_0
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    goto :goto_0

    .line 237
    :pswitch_1
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    goto :goto_0

    .line 240
    :pswitch_2
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    goto :goto_0

    .line 243
    :pswitch_3
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DESKTOP:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    goto :goto_0

    .line 246
    :pswitch_4
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->PROD_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 247
    iput-boolean v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK_:Z

    goto :goto_0

    .line 250
    :pswitch_5
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->GAMMA_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 251
    iput-boolean v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK_:Z

    goto :goto_0

    .line 254
    :pswitch_6
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;->DEVO_MSDK:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    .line 255
    iput-boolean v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK_:Z

    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    .line 708
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 714
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 715
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 719
    :catch_0
    move-exception v0

    const-string v0, ""

    .line 725
    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 733
    :goto_1
    return-object v0

    .line 725
    :cond_0
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 733
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 729
    :catch_1
    move-exception v0

    const-string v0, ""

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v0, ""

    goto :goto_1

    .line 723
    :catchall_0
    move-exception v0

    .line 725
    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 729
    throw v0

    :catch_3
    move-exception v0

    const-string v0, ""

    goto :goto_1
.end method

.method private decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 370
    if-eqz p1, :cond_0

    .line 372
    invoke-static {p2}, Lcom/amazon/device/ads/Utils;->getURLDecodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 375
    :cond_0
    return-object p2
.end method

.method private determineSDKVersion(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 738
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    .line 741
    :try_start_0
    const-string v0, "ad_resources/raw/version.json"

    invoke-static {p1, v0}, Lcom/amazon/device/ads/ResourceLookup;->getResourceFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 742
    if-eqz v0, :cond_0

    .line 744
    new-instance v1, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 745
    const-string v0, "sdk_version"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 753
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 754
    const-string v0, "(DEV)"

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    .line 757
    :cond_1
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "amaznAdSDK-android-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    .line 758
    return-void

    .line 755
    :cond_2
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    const-string v1, "x.x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(DEV)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected static final generateSha1Hash(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 685
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 686
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 687
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 689
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 690
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 691
    aget-byte v3, v1, v0

    and-int/lit16 v3, v3, 0xff

    or-int/lit16 v3, v3, 0x100

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 690
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 693
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 697
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_1
.end method

.method protected static final declared-synchronized getInstance()Lcom/amazon/device/ads/InternalAdRegistration;
    .locals 2

    .prologue
    .line 588
    const-class v0, Lcom/amazon/device/ads/InternalAdRegistration;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/amazon/device/ads/InternalAdRegistration;->instance_:Lcom/amazon/device/ads/InternalAdRegistration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method protected static final declared-synchronized getInstance(Landroid/content/Context;)Lcom/amazon/device/ads/InternalAdRegistration;
    .locals 2
    .parameter

    .prologue
    .line 575
    const-class v1, Lcom/amazon/device/ads/InternalAdRegistration;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration;->instance_:Lcom/amazon/device/ads/InternalAdRegistration;

    if-nez v0, :cond_0

    .line 576
    new-instance v0, Lcom/amazon/device/ads/InternalAdRegistration;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/InternalAdRegistration;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/amazon/device/ads/InternalAdRegistration;->instance_:Lcom/amazon/device/ads/InternalAdRegistration;

    .line 578
    :cond_0
    sget-object v0, Lcom/amazon/device/ads/InternalAdRegistration;->instance_:Lcom/amazon/device/ads/InternalAdRegistration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 575
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static final getMacAddress(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 647
    :try_start_0
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 649
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 650
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 651
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 653
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    .line 654
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_mac:Z

    .line 679
    :goto_0
    return-void

    .line 658
    :cond_1
    const-string v1, "((([0-9a-fA-F]){1,2}[-:]){5}([0-9a-fA-F]){1,2})"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 659
    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 660
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-nez v1, :cond_2

    .line 663
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    .line 664
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_mac:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 673
    :catch_0
    move-exception v0

    .line 675
    const-string v1, "AmazonAdRegistration"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to get WIFI Manager: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iput-object v4, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    goto :goto_0

    .line 668
    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->generateSha1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected static final getTelephonyID(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 611
    .line 614
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    .line 615
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 617
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-nez v1, :cond_0

    .line 641
    :goto_0
    return-void

    .line 624
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 625
    if-eqz v0, :cond_1

    const-string v1, "000000000000000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 627
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_tel:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 637
    const-string v1, "AmazonAdRegistration"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to get Telephony Manager: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    iput-object v4, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    goto :goto_0

    .line 631
    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->generateSha1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected static final getUdid(Landroid/content/Context;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 597
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 598
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "9774d56d682e549c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 600
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    .line 601
    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->bad_udid:Z

    .line 607
    :goto_0
    return-void

    .line 605
    :cond_1
    invoke-static {v0}, Lcom/amazon/device/ads/InternalAdRegistration;->generateSha1Hash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final getAmazonApplicationId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 501
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v1, "AmazonMobileAds"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 502
    const-string v1, "amzn-ad-app-id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getAmazonDeviceAdID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 495
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v1, "AmazonMobileAds"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 496
    const-string v1, "amzn-ad-id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getAmazonDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    return-object v0
.end method

.method protected final getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    return-object v0
.end method

.method protected final getDeviceInfo()Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    return-object v0
.end method

.method protected final getDeviceInfoParams(Z)Ljava/util/Map;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 390
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    if-nez v1, :cond_1

    .line 392
    const-string v1, "AmazonAdRegistration"

    const-string v2, "deviceInfo is null"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_0
    :goto_0
    return-object v0

    .line 396
    :cond_1
    const-string v1, "dt"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->dt:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    const-string v1, "app"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->app:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    const-string v1, "aud"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->aud:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    const-string v1, "appId"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 402
    const-string v1, "sha1_mac"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_mac:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    :cond_2
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_mac:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 407
    const-string v1, "md5_mac"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_mac:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    :cond_3
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 412
    const-string v1, "sha1_udid"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_udid:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    :cond_4
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_udid:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 417
    const-string v1, "md5_udid"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_udid:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    :cond_5
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 422
    const-string v1, "sha1_tel"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_tel:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    :cond_6
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_tel:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 427
    const-string v1, "md5_tel"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_tel:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    :cond_7
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 432
    const-string v1, "sha1_serial"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->sha1_serial:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    :cond_8
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_serial:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 437
    const-string v1, "md5_serial"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->md5_serial:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    :cond_9
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->referrer:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 442
    const-string v1, "referrer"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->referrer:Ljava/lang/String;

    invoke-direct {p0, p1, v2}, Lcom/amazon/device/ads/InternalAdRegistration;->decodeIfNeeded(ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method protected final getDeviceNativeData()Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    return-object v0
.end method

.method protected final getDeviceNativeDataParams()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 460
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 463
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->os:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 465
    const-string v1, "os"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->os:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->model:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 469
    const-string v1, "model"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->model:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    :cond_1
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->make:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 473
    const-string v1, "make"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->make:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    :cond_2
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->osVersion:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 477
    const-string v1, "osVersion"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->osVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    :cond_3
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->screenSize:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 482
    const-string v1, "screenSize"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->screenSize:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    :cond_4
    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v1, v1, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->sf:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 487
    const-string v1, "sf"

    iget-object v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v2, v2, Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;->sf:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    :cond_5
    return-object v0
.end method

.method protected final getEndpoint()Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    return-object v0
.end method

.method protected final getSDKVersionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->sdkVersionID_:Ljava/lang/String;

    return-object v0
.end method

.method protected final getTestMode()Z
    .locals 1

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->testMode_:Z

    return v0
.end method

.method protected final isLoggingEnabled()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->enableLogging_:Z

    return v0
.end method

.method protected final isRegistered()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 333
    const/4 v0, 0x1

    .line 335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final ping()V
    .locals 7

    .prologue
    .line 549
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v2, "Ping"

    iget-object v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iget-object v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v5, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v6, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;Ljava/lang/String;)V

    .line 551
    new-instance v1, Lcom/amazon/device/ads/AdRegistrationTasks$PingTask;

    invoke-direct {v1}, Lcom/amazon/device/ads/AdRegistrationTasks$PingTask;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/amazon/device/ads/AdRegistrationTasks$PingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 552
    return-void
.end method

.method protected final register()V
    .locals 7

    .prologue
    .line 533
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v2, "RegisterDevice"

    iget-object v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iget-object v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v5, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v6, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;Ljava/lang/String;)V

    .line 536
    new-instance v1, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;

    invoke-direct {v1}, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/amazon/device/ads/AdRegistrationTasks$RegisterDeviceTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 537
    return-void
.end method

.method protected final registerIfNeeded()V
    .locals 6

    .prologue
    .line 556
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 558
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 559
    iget-wide v2, p0, Lcom/amazon/device/ads/InternalAdRegistration;->lastSISCheckin_:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 561
    iput-wide v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->lastSISCheckin_:J

    .line 562
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->updateDeviceInfo()V

    .line 563
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->ping()V

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->register()V

    goto :goto_0
.end method

.method protected final setApplicationId(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 278
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 280
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ApplicationId must not be null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_1
    invoke-virtual {p0}, Lcom/amazon/device/ads/InternalAdRegistration;->getAmazonApplicationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 289
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v0, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    invoke-static {p1}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    .line 303
    :cond_2
    :goto_0
    return-void

    .line 295
    :cond_3
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    invoke-static {p1}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->appId:Ljava/lang/String;

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    .line 297
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v1, "AmazonMobileAds"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 298
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 299
    const-string v1, "amzn-ad-app-id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 300
    const-string v1, "amzn-ad-id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 301
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method protected final setApplicationName(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 307
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    invoke-static {p1}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->app:Ljava/lang/String;

    .line 308
    return-void
.end method

.method protected final setAuthenticationDomain(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 312
    iget-object v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    invoke-static {p1}, Lcom/amazon/device/ads/Utils;->getURLEncodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;->aud:Ljava/lang/String;

    .line 313
    return-void
.end method

.method protected final setLoggingEnabled(Z)V
    .locals 0
    .parameter

    .prologue
    .line 322
    iput-boolean p1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->enableLogging_:Z

    .line 323
    return-void
.end method

.method final setTestMode(Z)V
    .locals 0
    .parameter

    .prologue
    .line 345
    iput-boolean p1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->testMode_:Z

    .line 346
    return-void
.end method

.method protected final updateDeviceInfo()V
    .locals 7

    .prologue
    .line 541
    new-instance v0, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    iget-object v1, p0, Lcom/amazon/device/ads/InternalAdRegistration;->context_:Landroid/content/Context;

    const-string v2, "UpdateDeviceInfo"

    iget-object v3, p0, Lcom/amazon/device/ads/InternalAdRegistration;->endpoint_:Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;

    iget-object v4, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceInfo_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;

    iget-object v5, p0, Lcom/amazon/device/ads/InternalAdRegistration;->deviceNativeData_:Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;

    iget-object v6, p0, Lcom/amazon/device/ads/InternalAdRegistration;->amznDeviceId_:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/amazon/device/ads/InternalAdRegistration$AmazonAdEndpoint;Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;Ljava/lang/String;)V

    .line 544
    new-instance v1, Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;

    invoke-direct {v1}, Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/amazon/device/ads/AdRegistrationTasks$A9Request;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/amazon/device/ads/AdRegistrationTasks$UpdateDeviceInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 545
    return-void
.end method

.method protected final usingMSDK()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/amazon/device/ads/InternalAdRegistration;->usingMSDK_:Z

    return v0
.end method
