.class Lcom/amazon/device/ads/MraidBrowser$1;
.super Landroid/webkit/WebViewClient;
.source "GBFile"


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/MraidBrowser;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidBrowser;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 136
    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    #getter for: Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidBrowser;->access$100(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ad_resources/drawable/amazon_ads_leftarrow.png"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    :goto_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoForward()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    #getter for: Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidBrowser;->access$000(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ad_resources/drawable/amazon_ads_rightarrow.png"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 145
    :goto_1
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    #getter for: Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidBrowser;->access$100(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ad_resources/drawable/amazon_ads_unleftarrow.png"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    #getter for: Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidBrowser;->access$000(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ad_resources/drawable/amazon_ads_unrightarrow.png"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 129
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    #getter for: Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidBrowser;->access$000(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ad_resources/drawable/amazon_ads_unrightarrow.png"

    invoke-static {v1, v2}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 109
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MRAID error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 110
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 114
    if-nez p2, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    const-string v1, "market:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "tel:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "voicemail:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "sms:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "mailto:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "geo:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "google.streetview:"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser$1;->this$0:Lcom/amazon/device/ads/MraidBrowser;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidBrowser;->startActivity(Landroid/content/Intent;)V

    .line 121
    const/4 v0, 0x1

    goto :goto_0
.end method
