.class public Lcom/amazon/device/ads/MraidBrowser;
.super Landroid/app/Activity;
.source "GBFile"


# static fields
.field public static final URL_EXTRA:Ljava/lang/String; = "extra_url"


# instance fields
.field private mBrowserBackButton:Landroid/widget/ImageButton;

.field private mBrowserForwardButton:Landroid/widget/ImageButton;

.field private mCloseButton:Landroid/widget/ImageButton;

.field private mRefreshButton:Landroid/widget/ImageButton;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/amazon/device/ads/MraidBrowser;)Landroid/widget/ImageButton;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/amazon/device/ads/MraidBrowser;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method private createButton(Ljava/lang/String;)Landroid/widget/ImageButton;
    .locals 3
    .parameter

    .prologue
    const/4 v2, -0x2

    .line 90
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 91
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowser;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 94
    const/high16 v2, 0x41c8

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 95
    const/16 v2, 0x10

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 96
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 98
    return-object v0
.end method

.method private enableCookies()V
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 186
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 187
    return-void
.end method

.method private initializeButtons(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$3;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$3;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$4;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$4;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mRefreshButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$5;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$5;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$6;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$6;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    return-void
.end method

.method private initializeEntireView(Landroid/content/Intent;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, -0x2

    const/4 v4, -0x1

    .line 48
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 49
    const/16 v1, 0x2828

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 50
    const/high16 v1, 0x42c8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 51
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 53
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 54
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "resources/drawable/amazon_ads_bkgrnd.png"

    invoke-static {v2, v3}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 57
    const-string v1, "ad_resources/drawable/amazon_ads_leftarrow.png"

    invoke-direct {p0, v1}, Lcom/amazon/device/ads/MraidBrowser;->createButton(Ljava/lang/String;)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;

    .line 58
    const-string v1, "ad_resources/drawable/amazon_ads_rightarrow.png"

    invoke-direct {p0, v1}, Lcom/amazon/device/ads/MraidBrowser;->createButton(Ljava/lang/String;)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;

    .line 59
    const-string v1, "ad_resources/drawable/amazon_ads_refresh.png"

    invoke-direct {p0, v1}, Lcom/amazon/device/ads/MraidBrowser;->createButton(Ljava/lang/String;)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mRefreshButton:Landroid/widget/ImageButton;

    .line 60
    const-string v1, "ad_resources/drawable/amazon_ads_close.png"

    invoke-direct {p0, v1}, Lcom/amazon/device/ads/MraidBrowser;->createButton(Ljava/lang/String;)Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mCloseButton:Landroid/widget/ImageButton;

    .line 62
    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserBackButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 63
    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mBrowserForwardButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 64
    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mRefreshButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 65
    iget-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 67
    new-instance v1, Landroid/webkit/WebView;

    invoke-direct {v1, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    .line 68
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 70
    const/4 v2, 0x2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 71
    iget-object v2, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 74
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v2, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 77
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 80
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 81
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 82
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 86
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidBrowser;->setContentView(Landroid/view/View;)V

    .line 87
    return-void
.end method

.method private initializeWebView(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    const-string v1, "extra_url"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$1;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$1;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 148
    iget-object v0, p0, Lcom/amazon/device/ads/MraidBrowser;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/amazon/device/ads/MraidBrowser$2;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidBrowser$2;-><init>(Lcom/amazon/device/ads/MraidBrowser;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 156
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowser;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 37
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowser;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/view/Window;->setFeatureInt(II)V

    .line 39
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowser;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 40
    invoke-direct {p0, v0}, Lcom/amazon/device/ads/MraidBrowser;->initializeEntireView(Landroid/content/Intent;)V

    .line 41
    invoke-direct {p0, v0}, Lcom/amazon/device/ads/MraidBrowser;->initializeWebView(Landroid/content/Intent;)V

    .line 42
    invoke-direct {p0, v0}, Lcom/amazon/device/ads/MraidBrowser;->initializeButtons(Landroid/content/Intent;)V

    .line 43
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidBrowser;->enableCookies()V

    .line 44
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 192
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->stopSync()V

    .line 193
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 198
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 199
    return-void
.end method
