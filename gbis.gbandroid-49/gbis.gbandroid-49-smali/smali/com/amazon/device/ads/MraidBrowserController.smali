.class Lcom/amazon/device/ads/MraidBrowserController;
.super Lcom/amazon/device/ads/MraidAbstractController;
.source "GBFile"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "MraidBrowserController"


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidView;)V
    .locals 0
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/MraidAbstractController;-><init>(Lcom/amazon/device/ads/MraidView;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected executeAmazonMobileCallback(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getOnSpecialUrlClickListener()Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->close()V

    .line 18
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getOnSpecialUrlClickListener()Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;->onSpecialUrlClick(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V

    .line 20
    :cond_0
    return-void
.end method

.method protected open(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 24
    const-string v0, "MraidBrowserController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Opening in-app browser: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowserController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    .line 27
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 28
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "amazonmobile"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    invoke-virtual {p0, v0, p1}, Lcom/amazon/device/ads/MraidBrowserController;->executeAmazonMobileCallback(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 34
    :cond_0
    const-string v2, "d.url"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_1

    const-string v2, "amazonmobile:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    invoke-virtual {p0, v0, v1}, Lcom/amazon/device/ads/MraidBrowserController;->executeAmazonMobileCallback(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnOpenListener()Lcom/amazon/device/ads/MraidView$OnOpenListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 44
    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnOpenListener()Lcom/amazon/device/ads/MraidView$OnOpenListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/amazon/device/ads/MraidView$OnOpenListener;->onOpen(Lcom/amazon/device/ads/MraidView;)V

    .line 47
    :cond_2
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidBrowserController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 48
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/amazon/device/ads/MraidBrowser;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    const-string v2, "extra_url"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
