.class Lcom/amazon/device/ads/MraidCommandExpand;
.super Lcom/amazon/device/ads/MraidCommand;
.source "GBFile"


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/amazon/device/ads/MraidView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/MraidCommand;-><init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V

    .line 83
    return-void
.end method


# virtual methods
.method execute()V
    .locals 6

    .prologue
    .line 86
    const-string v0, "w"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandExpand;->getIntFromParamsForKey(Ljava/lang/String;)I

    move-result v2

    .line 87
    const-string v0, "h"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandExpand;->getIntFromParamsForKey(Ljava/lang/String;)I

    move-result v3

    .line 88
    const-string v0, "url"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandExpand;->getStringFromParamsForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    const-string v0, "shouldUseCustomClose"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandExpand;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v4

    .line 90
    const-string v0, "lockOrientation"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandExpand;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v5

    .line 92
    if-gtz v2, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/MraidCommandExpand;->mView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    iget v2, v0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenWidth:I

    .line 93
    :cond_0
    if-gtz v3, :cond_1

    iget-object v0, p0, Lcom/amazon/device/ads/MraidCommandExpand;->mView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    iget v3, v0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenHeight:I

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/MraidCommandExpand;->mView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Lcom/amazon/device/ads/MraidDisplayController;->expand(Ljava/lang/String;IIZZ)V

    .line 97
    return-void
.end method
