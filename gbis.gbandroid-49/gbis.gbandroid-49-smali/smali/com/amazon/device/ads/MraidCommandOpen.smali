.class Lcom/amazon/device/ads/MraidCommandOpen;
.super Lcom/amazon/device/ads/MraidCommand;
.source "GBFile"


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/amazon/device/ads/MraidView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/MraidCommand;-><init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V

    .line 142
    return-void
.end method


# virtual methods
.method execute()V
    .locals 2

    .prologue
    .line 145
    const-string v0, "url"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandOpen;->getStringFromParamsForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/amazon/device/ads/MraidCommandOpen;->mView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getBrowserController()Lcom/amazon/device/ads/MraidBrowserController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amazon/device/ads/MraidBrowserController;->open(Ljava/lang/String;)V

    .line 147
    return-void
.end method
