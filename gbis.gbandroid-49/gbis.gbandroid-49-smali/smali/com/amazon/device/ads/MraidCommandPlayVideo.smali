.class Lcom/amazon/device/ads/MraidCommandPlayVideo;
.super Lcom/amazon/device/ads/MraidCommand;
.source "GBFile"


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/amazon/device/ads/MraidView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/MraidCommand;-><init>(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)V

    return-void
.end method


# virtual methods
.method execute()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 107
    const/4 v0, 0x0

    .line 108
    const-string v1, "position"

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getIntArrayFromParamsFroKey(Ljava/lang/String;)[Ljava/lang/Integer;

    move-result-object v1

    .line 109
    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 111
    new-instance v0, Lcom/amazon/device/ads/Controller$Dimensions;

    invoke-direct {v0}, Lcom/amazon/device/ads/Controller$Dimensions;-><init>()V

    .line 112
    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/amazon/device/ads/Controller$Dimensions;->y:I

    .line 113
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/amazon/device/ads/Controller$Dimensions;->x:I

    .line 114
    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/amazon/device/ads/Controller$Dimensions;->width:I

    .line 115
    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/amazon/device/ads/Controller$Dimensions;->height:I

    move-object v8, v0

    .line 117
    :goto_0
    const-string v0, "url"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getStringFromParamsForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 118
    new-instance v0, Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-direct {v0}, Lcom/amazon/device/ads/Controller$PlayerProperties;-><init>()V

    .line 119
    const-string v1, "audioMuted"

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "autoPlay"

    invoke-virtual {p0, v2}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "controls"

    invoke-virtual {p0, v3}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v3

    const-string v5, "loop"

    invoke-virtual {p0, v5}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getBooleanFromParamsForKey(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "startStyle"

    invoke-virtual {p0, v6}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getStringFromParamsForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "stopStyle"

    invoke-virtual {p0, v7}, Lcom/amazon/device/ads/MraidCommandPlayVideo;->getStringFromParamsForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/amazon/device/ads/Controller$PlayerProperties;->setProperties(ZZZZZLjava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/amazon/device/ads/MraidCommandPlayVideo;->mView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v1

    invoke-virtual {v1, v9, v8, v0}, Lcom/amazon/device/ads/MraidDisplayController;->playVideo(Ljava/lang/String;Lcom/amazon/device/ads/Controller$Dimensions;Lcom/amazon/device/ads/Controller$PlayerProperties;)V

    .line 124
    return-void

    :cond_0
    move-object v8, v0

    goto :goto_0
.end method
