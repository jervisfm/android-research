.class Lcom/amazon/device/ads/MraidCommandRegistry;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/MraidCommandRegistry$MraidCommandFactory;
    }
.end annotation


# static fields
.field private static commandMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazon/device/ads/MraidCommandRegistry$MraidCommandFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 10
    sput-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    const-string v1, "close"

    new-instance v2, Lcom/amazon/device/ads/MraidCommandRegistry$1;

    invoke-direct {v2}, Lcom/amazon/device/ads/MraidCommandRegistry$1;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    sget-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    const-string v1, "expand"

    new-instance v2, Lcom/amazon/device/ads/MraidCommandRegistry$2;

    invoke-direct {v2}, Lcom/amazon/device/ads/MraidCommandRegistry$2;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    const-string v1, "usecustomclose"

    new-instance v2, Lcom/amazon/device/ads/MraidCommandRegistry$3;

    invoke-direct {v2}, Lcom/amazon/device/ads/MraidCommandRegistry$3;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    const-string v1, "open"

    new-instance v2, Lcom/amazon/device/ads/MraidCommandRegistry$4;

    invoke-direct {v2}, Lcom/amazon/device/ads/MraidCommandRegistry$4;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    const-string v1, "playVideo"

    new-instance v2, Lcom/amazon/device/ads/MraidCommandRegistry$5;

    invoke-direct {v2}, Lcom/amazon/device/ads/MraidCommandRegistry$5;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method static createCommand(Ljava/lang/String;Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidCommand;
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/amazon/device/ads/MraidView;",
            ")",
            "Lcom/amazon/device/ads/MraidCommand;"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v0, Lcom/amazon/device/ads/MraidCommandRegistry;->commandMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/MraidCommandRegistry$MraidCommandFactory;

    .line 43
    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/amazon/device/ads/MraidCommandRegistry$MraidCommandFactory;->create(Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidCommand;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
