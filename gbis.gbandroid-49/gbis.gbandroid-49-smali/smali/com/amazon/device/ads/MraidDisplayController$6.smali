.class Lcom/amazon/device/ads/MraidDisplayController$6;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/MraidDisplayController;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidDisplayController;)V
    .locals 0
    .parameter

    .prologue
    .line 492
    iput-object p1, p0, Lcom/amazon/device/ads/MraidDisplayController$6;->this$0:Lcom/amazon/device/ads/MraidDisplayController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete()V
    .locals 2

    .prologue
    .line 508
    const-string v0, "MraidDisplayController"

    const-string v1, "videoplayback complete"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController$6;->this$0:Lcom/amazon/device/ads/MraidDisplayController;

    const/4 v1, 0x0

    #setter for: Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z
    invoke-static {v0, v1}, Lcom/amazon/device/ads/MraidDisplayController;->access$202(Lcom/amazon/device/ads/MraidDisplayController;Z)Z

    .line 510
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController$6;->this$0:Lcom/amazon/device/ads/MraidDisplayController;

    #getter for: Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidDisplayController;->access$300(Lcom/amazon/device/ads/MraidDisplayController;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 511
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 512
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController$6;->this$0:Lcom/amazon/device/ads/MraidDisplayController;

    #getter for: Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;
    invoke-static {v1}, Lcom/amazon/device/ads/MraidDisplayController;->access$400(Lcom/amazon/device/ads/MraidDisplayController;)Lcom/amazon/device/ads/AdVideoPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 513
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController$6;->this$0:Lcom/amazon/device/ads/MraidDisplayController;

    #getter for: Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/amazon/device/ads/MraidDisplayController;->access$300(Lcom/amazon/device/ads/MraidDisplayController;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 514
    return-void
.end method

.method public onError()V
    .locals 0

    .prologue
    .line 502
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController$6;->onComplete()V

    .line 503
    return-void
.end method

.method public onPrepared()V
    .locals 0

    .prologue
    .line 497
    return-void
.end method
