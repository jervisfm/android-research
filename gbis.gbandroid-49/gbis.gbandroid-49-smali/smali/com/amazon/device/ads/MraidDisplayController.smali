.class Lcom/amazon/device/ads/MraidDisplayController;
.super Lcom/amazon/device/ads/MraidAbstractController;
.source "GBFile"


# static fields
.field private static final CLOSE_BUTTON_SIZE_DP:I = 0x32

.field private static final LOGTAG:Ljava/lang/String; = "MraidDisplayController"


# instance fields
.field private mAdWantsCustomCloseButton:Z

.field private mCloseButton:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field protected mDensity:F

.field private final mExpansionStyle:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

.field private mIsViewable:Z

.field private final mNativeCloseButtonStyle:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

.field private mOrientationBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mOriginalRequestedOrientation:I

.field mPlaceholderView:Landroid/widget/FrameLayout;

.field private mRegistered:Z

.field private mRootView:Landroid/widget/FrameLayout;

.field protected mScreenHeight:I

.field protected mScreenWidth:I

.field private mTwoPartExpansionView:Lcom/amazon/device/ads/MraidView;

.field private mViewHeight:I

.field private mViewIndexInParent:I

.field private mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

.field private mViewWidth:I

.field private scalingFactor_:D

.field private usingDownscalingLogic_:Z

.field private vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

.field private vidPlaying_:Z

.field private windowHeight_:I

.field private windowWidth_:I


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 121
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/MraidAbstractController;-><init>(Lcom/amazon/device/ads/MraidView;)V

    .line 47
    sget-object v1, Lcom/amazon/device/ads/MraidView$ViewState;->HIDDEN:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 62
    iput-boolean v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    .line 72
    new-instance v1, Lcom/amazon/device/ads/MraidDisplayController$1;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidDisplayController$1;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    iput-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOrientationBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 97
    iput v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenWidth:I

    .line 100
    iput v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenHeight:I

    .line 110
    iput-boolean v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    .line 122
    iput-object p2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mExpansionStyle:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

    .line 123
    iput-object p3, p0, Lcom/amazon/device/ads/MraidDisplayController;->mNativeCloseButtonStyle:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    .line 124
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getWindowHeight()I

    move-result v1

    iput v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->windowHeight_:I

    .line 125
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getWindowWidth()I

    move-result v1

    iput v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->windowWidth_:I

    .line 126
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->getScalingFactor()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->scalingFactor_:D

    .line 127
    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidView;->isUsingDownscalingLogic()Z

    move-result v1

    iput-boolean v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->usingDownscalingLogic_:Z

    .line 129
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    .line 130
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v0

    :cond_0
    iput v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOriginalRequestedOrientation:I

    .line 134
    new-instance v0, Lcom/amazon/device/ads/AdVideoPlayer;

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/amazon/device/ads/AdVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    .line 136
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->initialize()V

    .line 137
    return-void
.end method

.method static synthetic access$000(Lcom/amazon/device/ads/MraidDisplayController;)I
    .locals 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getDisplayRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/amazon/device/ads/MraidDisplayController;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/MraidDisplayController;->onOrientationChanged(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/amazon/device/ads/MraidDisplayController;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    return p1
.end method

.method static synthetic access$300(Lcom/amazon/device/ads/MraidDisplayController;)Landroid/widget/FrameLayout;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/amazon/device/ads/MraidDisplayController;)Lcom/amazon/device/ads/AdVideoPlayer;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    return-object v0
.end method

.method private createExpansionViewContainer(Landroid/view/View;II)Landroid/view/ViewGroup;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 363
    const/high16 v0, 0x4248

    iget v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mDensity:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 364
    if-ge p2, v0, :cond_0

    move p2, v0

    .line 365
    :cond_0
    if-ge p3, v0, :cond_1

    move p3, v0

    .line 367
    :cond_1
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 368
    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 370
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 371
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 372
    new-instance v2, Lcom/amazon/device/ads/MraidDisplayController$4;

    invoke-direct {v2, p0}, Lcom/amazon/device/ads/MraidDisplayController$4;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 378
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 382
    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setId(I)V

    .line 384
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, p1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 387
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 388
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 389
    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 391
    return-object v0
.end method

.method private getDisplayRotation()I
    .locals 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 198
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    return v0
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/amazon/device/ads/MraidView$ViewState;->LOADING:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 159
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->initializeScreenMetrics()V

    .line 160
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->registerRecievers()V

    .line 161
    return-void
.end method

.method private initializeScreenMetrics()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v6, 0x4064

    .line 169
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 170
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 171
    const-string v0, "window"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 172
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 173
    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mDensity:F

    .line 176
    instance-of v0, v1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 177
    check-cast v0, Landroid/app/Activity;

    .line 178
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 179
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 180
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 181
    iget v2, v1, Landroid/graphics/Rect;->top:I

    .line 182
    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 183
    sub-int/2addr v0, v2

    .line 187
    :goto_0
    iget v1, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 188
    iget v4, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v2, v4, v2

    sub-int v0, v2, v0

    .line 190
    int-to-double v1, v1

    iget v4, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v4, v4

    div-double v4, v6, v4

    mul-double/2addr v1, v4

    double-to-int v1, v1

    iput v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenWidth:I

    .line 191
    int-to-double v0, v0

    iget v2, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v2, v2

    div-double v2, v6, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenHeight:I

    .line 193
    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private onOrientationChanged(I)V
    .locals 3
    .parameter

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->initializeScreenMetrics()V

    .line 203
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 204
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    iget v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenWidth:I

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenHeight:I

    invoke-static {v1, v2}, Lcom/amazon/device/ads/MraidScreenSizeProperty;->createWithSize(II)Lcom/amazon/device/ads/MraidScreenSizeProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 207
    :cond_0
    return-void
.end method

.method private resetViewToDefaultState()V
    .locals 6

    .prologue
    .line 257
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 259
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 262
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/amazon/device/ads/MraidDisplayController;->setNativeCloseButtonEnabled(Z)V

    .line 263
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    .line 264
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 266
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->requestLayout()V

    .line 268
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mPlaceholderView:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 269
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewIndexInParent:I

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    iget v4, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewWidth:I

    iget v5, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewHeight:I

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 270
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mPlaceholderView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 271
    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 272
    return-void
.end method

.method private setOrientationLockEnabled(Z)V
    .locals 2
    .parameter

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 398
    :try_start_0
    check-cast v0, Landroid/app/Activity;

    .line 401
    if-eqz p1, :cond_0

    .line 403
    invoke-static {v0}, Lcom/amazon/device/ads/Utils;->determineCanonicalScreenOrientation(Landroid/app/Activity;)I

    move-result v1

    .line 408
    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 412
    :goto_1
    return-void

    .line 406
    :cond_0
    iget v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOriginalRequestedOrientation:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 410
    :catch_0
    move-exception v0

    const-string v0, "MraidDisplayController"

    const-string v1, "Unable to modify device orientation."

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private swapViewWithPlaceholderView()V
    .locals 6

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 343
    if-nez v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 345
    :cond_0
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mPlaceholderView:Landroid/widget/FrameLayout;

    .line 348
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 349
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 350
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v4

    if-eq v3, v4, :cond_1

    .line 349
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 353
    :cond_1
    iput v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewIndexInParent:I

    .line 354
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewHeight:I

    .line 355
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewWidth:I

    .line 356
    iget-object v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mPlaceholderView:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/amazon/device/ads/MraidView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/amazon/device/ads/MraidView;->getHeight()I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 358
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method protected checkViewable()Z
    .locals 1

    .prologue
    .line 463
    const/4 v0, 0x1

    return v0
.end method

.method protected close()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 234
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdVideoPlayer;->releasePlayer()V

    .line 237
    iput-boolean v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    sget-object v1, Lcom/amazon/device/ads/MraidView$ViewState;->EXPANDED:Lcom/amazon/device/ads/MraidView$ViewState;

    if-ne v0, v1, :cond_3

    .line 241
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->resetViewToDefaultState()V

    .line 242
    invoke-direct {p0, v2}, Lcom/amazon/device/ads/MraidDisplayController;->setOrientationLockEnabled(Z)V

    .line 243
    sget-object v0, Lcom/amazon/device/ads/MraidView$ViewState;->DEFAULT:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 244
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-static {v1}, Lcom/amazon/device/ads/MraidStateProperty;->createWithViewState(Lcom/amazon/device/ads/MraidView$ViewState;)Lcom/amazon/device/ads/MraidStateProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 251
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnCloseListener()Lcom/amazon/device/ads/MraidView$OnCloseListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 252
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnCloseListener()Lcom/amazon/device/ads/MraidView$OnCloseListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-interface {v0, v1, v2}, Lcom/amazon/device/ads/MraidView$OnCloseListener;->onClose(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$ViewState;)V

    .line 254
    :cond_2
    return-void

    .line 245
    :cond_3
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    sget-object v1, Lcom/amazon/device/ads/MraidView$ViewState;->DEFAULT:Lcom/amazon/device/ads/MraidView$ViewState;

    if-ne v0, v1, :cond_1

    .line 246
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->setVisibility(I)V

    .line 247
    sget-object v0, Lcom/amazon/device/ads/MraidView$ViewState;->HIDDEN:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 248
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-static {v1}, Lcom/amazon/device/ads/MraidStateProperty;->createWithViewState(Lcom/amazon/device/ads/MraidView$ViewState;)Lcom/amazon/device/ads/MraidStateProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    goto :goto_0
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 211
    :try_start_0
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOrientationBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :cond_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    .line 213
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Receiver not registered"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    throw v0
.end method

.method protected expand(Ljava/lang/String;IIZZ)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mExpansionStyle:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

    sget-object v1, Lcom/amazon/device/ads/MraidView$ExpansionStyle;->DISABLED:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    sget-object v1, Lcom/amazon/device/ads/MraidView$ViewState;->EXPANDED:Lcom/amazon/device/ads/MraidView$ViewState;

    if-ne v0, v1, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    if-eqz p1, :cond_2

    invoke-static {p1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    const-string v1, "expand"

    const-string v2, "URL passed to expand() was invalid."

    invoke-virtual {v0, v1, v2}, Lcom/amazon/device/ads/MraidView;->fireErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    :cond_2
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    .line 288
    invoke-virtual {p0, p4}, Lcom/amazon/device/ads/MraidDisplayController;->useCustomClose(Z)V

    .line 289
    invoke-direct {p0, p5}, Lcom/amazon/device/ads/MraidDisplayController;->setOrientationLockEnabled(Z)V

    .line 290
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidDisplayController;->swapViewWithPlaceholderView()V

    .line 292
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    .line 293
    if-eqz p1, :cond_3

    .line 294
    new-instance v0, Lcom/amazon/device/ads/MraidView;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->windowWidth_:I

    iget v3, p0, Lcom/amazon/device/ads/MraidDisplayController;->windowHeight_:I

    iget-wide v4, p0, Lcom/amazon/device/ads/MraidDisplayController;->scalingFactor_:D

    iget-boolean v6, p0, Lcom/amazon/device/ads/MraidDisplayController;->usingDownscalingLogic_:Z

    sget-object v7, Lcom/amazon/device/ads/MraidView$ExpansionStyle;->DISABLED:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

    sget-object v8, Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;->AD_CONTROLLED:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    sget-object v9, Lcom/amazon/device/ads/MraidView$PlacementType;->INLINE:Lcom/amazon/device/ads/MraidView$PlacementType;

    invoke-direct/range {v0 .. v9}, Lcom/amazon/device/ads/MraidView;-><init>(Landroid/content/Context;IIDZLcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;Lcom/amazon/device/ads/MraidView$PlacementType;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mTwoPartExpansionView:Lcom/amazon/device/ads/MraidView;

    .line 297
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mTwoPartExpansionView:Lcom/amazon/device/ads/MraidView;

    new-instance v1, Lcom/amazon/device/ads/MraidDisplayController$2;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidDisplayController$2;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->setOnCloseListener(Lcom/amazon/device/ads/MraidView$OnCloseListener;)V

    .line 302
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mTwoPartExpansionView:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0, p1}, Lcom/amazon/device/ads/MraidView;->loadUrl(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mTwoPartExpansionView:Lcom/amazon/device/ads/MraidView;

    .line 306
    :cond_3
    int-to-float v1, p2

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mDensity:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, p3

    iget v3, p0, Lcom/amazon/device/ads/MraidDisplayController;->mDensity:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {p0, v0, v1, v2}, Lcom/amazon/device/ads/MraidDisplayController;->createExpansionViewContainer(Landroid/view/View;II)Landroid/view/ViewGroup;

    move-result-object v1

    .line 308
    iget-object v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 315
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 316
    new-instance v1, Lcom/amazon/device/ads/MraidDisplayController$3;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidDisplayController$3;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 330
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mNativeCloseButtonStyle:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    sget-object v1, Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;->ALWAYS_VISIBLE:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    if-eq v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mAdWantsCustomCloseButton:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mNativeCloseButtonStyle:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    sget-object v1, Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;->ALWAYS_HIDDEN:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    if-eq v0, v1, :cond_5

    .line 333
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidDisplayController;->setNativeCloseButtonEnabled(Z)V

    .line 336
    :cond_5
    sget-object v0, Lcom/amazon/device/ads/MraidView$ViewState;->EXPANDED:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 337
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-static {v1}, Lcom/amazon/device/ads/MraidStateProperty;->createWithViewState(Lcom/amazon/device/ads/MraidView$ViewState;)Lcom/amazon/device/ads/MraidStateProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 338
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnExpandListener()Lcom/amazon/device/ads/MraidView$OnExpandListener;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnExpandListener()Lcom/amazon/device/ads/MraidView$OnExpandListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amazon/device/ads/MraidView$OnExpandListener;->onExpand(Lcom/amazon/device/ads/MraidView;)V

    goto/16 :goto_0
.end method

.method protected initializeJavaScriptState()V
    .locals 3

    .prologue
    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 221
    iget v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenWidth:I

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mScreenHeight:I

    invoke-static {v1, v2}, Lcom/amazon/device/ads/MraidScreenSizeProperty;->createWithSize(II)Lcom/amazon/device/ads/MraidScreenSizeProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    iget-boolean v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mIsViewable:Z

    invoke-static {v1}, Lcom/amazon/device/ads/MraidViewableProperty;->createWithViewable(Z)Lcom/amazon/device/ads/MraidViewableProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperties(Ljava/util/ArrayList;)V

    .line 225
    sget-object v0, Lcom/amazon/device/ads/MraidView$ViewState;->DEFAULT:Lcom/amazon/device/ads/MraidView$ViewState;

    iput-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 226
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-static {v1}, Lcom/amazon/device/ads/MraidStateProperty;->createWithViewState(Lcom/amazon/device/ads/MraidView$ViewState;)Lcom/amazon/device/ads/MraidStateProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 227
    return-void
.end method

.method protected isExpanded()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    sget-object v1, Lcom/amazon/device/ads/MraidView$ViewState;->EXPANDED:Lcom/amazon/device/ads/MraidView$ViewState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected playVideo(Ljava/lang/String;Lcom/amazon/device/ads/Controller$Dimensions;Lcom/amazon/device/ads/Controller$PlayerProperties;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 468
    const-string v0, "MraidDisplayController"

    const-string v1, "in playVideo"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    if-eqz v0, :cond_0

    .line 531
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-virtual {p3}, Lcom/amazon/device/ads/Controller$PlayerProperties;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 474
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v1, "player_dimensions"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 476
    const-string v1, "player_properties"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 479
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/amazon/device/ads/VideoActionHandler;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 480
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 481
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 485
    :catch_0
    move-exception v0

    const-string v0, "MraidDisplayController"

    const-string v1, "Failed to open VideoAction activity"

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    new-instance v1, Lcom/amazon/device/ads/Controller$PlayerProperties;

    invoke-direct {v1}, Lcom/amazon/device/ads/Controller$PlayerProperties;-><init>()V

    invoke-virtual {v0, v1, p1}, Lcom/amazon/device/ads/AdVideoPlayer;->setPlayData(Lcom/amazon/device/ads/Controller$PlayerProperties;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    new-instance v1, Lcom/amazon/device/ads/MraidDisplayController$6;

    invoke-direct {v1, p0}, Lcom/amazon/device/ads/MraidDisplayController$6;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdVideoPlayer;->setListener(Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;)V

    .line 517
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p2, Lcom/amazon/device/ads/Controller$Dimensions;->width:I

    iget v2, p2, Lcom/amazon/device/ads/Controller$Dimensions;->height:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 518
    iget v1, p2, Lcom/amazon/device/ads/Controller$Dimensions;->x:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 519
    iget v1, p2, Lcom/amazon/device/ads/Controller$Dimensions;->y:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 520
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/amazon/device/ads/AdVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 522
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 523
    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 524
    iget v1, p2, Lcom/amazon/device/ads/Controller$Dimensions;->x:I

    iget v2, p2, Lcom/amazon/device/ads/Controller$Dimensions;->y:I

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 525
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 528
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v3, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlaying_:Z

    .line 530
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->vidPlayer_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdVideoPlayer;->playVideo()V

    goto/16 :goto_0
.end method

.method protected registerRecievers()V
    .locals 4

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    .line 142
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOrientationBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 145
    :cond_0
    return-void
.end method

.method protected setNativeCloseButtonEnabled(Z)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 415
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRootView:Landroid/widget/FrameLayout;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 420
    if-eqz p1, :cond_3

    .line 421
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 422
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 423
    new-array v2, v7, [I

    const v3, -0x10100a7

    aput v3, v2, v6

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    const-string v5, "ad_resources/drawable/amazon_ads_close_button_normal.png"

    invoke-static {v4, v5}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 425
    new-array v2, v7, [I

    const v3, 0x10100a7

    aput v3, v2, v6

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/amazon/device/ads/MraidDisplayController;->mContext:Landroid/content/Context;

    const-string v5, "ad_resources/drawable/amazon_ads_close_button_pressed.png"

    invoke-static {v4, v5}, Lcom/amazon/device/ads/ResourceLookup;->bitmapFromJar(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 427
    new-instance v2, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    .line 428
    iget-object v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 429
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 430
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    new-instance v2, Lcom/amazon/device/ads/MraidDisplayController$5;

    invoke-direct {v2, p0}, Lcom/amazon/device/ads/MraidDisplayController$5;-><init>(Lcom/amazon/device/ads/MraidDisplayController;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 437
    :cond_2
    const/high16 v1, 0x4248

    iget v2, p0, Lcom/amazon/device/ads/MraidDisplayController;->mDensity:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f00

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 438
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v1, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 440
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 445
    :goto_1
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    .line 446
    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnCloseButtonStateChangeListener()Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 447
    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnCloseButtonStateChangeListener()Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;->onCloseButtonStateChange(Lcom/amazon/device/ads/MraidView;Z)V

    goto/16 :goto_0

    .line 442
    :cond_3
    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mCloseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method protected surfaceAd()V
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/amazon/device/ads/MraidViewableProperty;->createWithViewable(Z)Lcom/amazon/device/ads/MraidViewableProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 166
    return-void
.end method

.method protected unregisterRecievers()V
    .locals 2

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidDisplayController;->mRegistered:Z

    .line 151
    :try_start_0
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mOrientationBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected useCustomClose(Z)V
    .locals 3
    .parameter

    .prologue
    .line 452
    iput-boolean p1, p0, Lcom/amazon/device/ads/MraidDisplayController;->mAdWantsCustomCloseButton:Z

    .line 454
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidDisplayController;->getView()Lcom/amazon/device/ads/MraidView;

    move-result-object v1

    .line 455
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 456
    :goto_0
    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getOnCloseButtonStateChangeListener()Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 457
    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView;->getOnCloseButtonStateChangeListener()Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;->onCloseButtonStateChange(Lcom/amazon/device/ads/MraidView;Z)V

    .line 459
    :cond_0
    return-void

    .line 455
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
