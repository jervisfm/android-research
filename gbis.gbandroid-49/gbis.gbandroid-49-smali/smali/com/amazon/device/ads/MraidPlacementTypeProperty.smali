.class Lcom/amazon/device/ads/MraidPlacementTypeProperty;
.super Lcom/amazon/device/ads/MraidProperty;
.source "GBFile"


# instance fields
.field private final mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidView$PlacementType;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidProperty;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/amazon/device/ads/MraidPlacementTypeProperty;->mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;

    .line 24
    return-void
.end method

.method public static createWithType(Lcom/amazon/device/ads/MraidView$PlacementType;)Lcom/amazon/device/ads/MraidPlacementTypeProperty;
    .locals 1
    .parameter

    .prologue
    .line 28
    new-instance v0, Lcom/amazon/device/ads/MraidPlacementTypeProperty;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/MraidPlacementTypeProperty;-><init>(Lcom/amazon/device/ads/MraidView$PlacementType;)V

    return-object v0
.end method


# virtual methods
.method public toJsonPair()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "placementType: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/device/ads/MraidPlacementTypeProperty;->mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView$PlacementType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
