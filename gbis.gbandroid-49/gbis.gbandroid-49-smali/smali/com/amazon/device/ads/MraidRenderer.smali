.class Lcom/amazon/device/ads/MraidRenderer;
.super Lcom/amazon/device/ads/AdRenderer;
.source "GBFile"

# interfaces
.implements Lcom/amazon/device/ads/MraidView$OnCloseListener;
.implements Lcom/amazon/device/ads/MraidView$OnExpandListener;
.implements Lcom/amazon/device/ads/MraidView$OnReadyListener;
.implements Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "MraidRenderer"


# instance fields
.field protected mraidView_:Lcom/amazon/device/ads/MraidView;


# direct methods
.method protected constructor <init>(Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/amazon/device/ads/AdRenderer;-><init>(Lcom/amazon/device/ads/AdResponse;Lcom/amazon/device/ads/AdBridge;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected adLoaded(Lcom/amazon/device/ads/AdProperties;)V
    .locals 0
    .parameter

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/amazon/device/ads/AdRenderer;->adLoaded(Lcom/amazon/device/ads/AdProperties;)V

    .line 102
    return-void
.end method

.method protected destroy()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->destroy()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidRenderer;->isDestroyed_:Z

    .line 88
    :cond_0
    return-void
.end method

.method public onClose(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$ViewState;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidRenderer;->isAdViewRemoved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->adClosedExpansion()V

    .line 145
    :cond_0
    return-void
.end method

.method public onExpand(Lcom/amazon/device/ads/MraidView;)V
    .locals 1
    .parameter

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidRenderer;->isAdViewRemoved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->adExpanded()V

    .line 138
    :cond_0
    return-void
.end method

.method public onReady(Lcom/amazon/device/ads/MraidView;)V
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget-object v0, v0, Lcom/amazon/device/ads/AdResponse;->properties_:Lcom/amazon/device/ads/AdProperties;

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidRenderer;->adLoaded(Lcom/amazon/device/ads/AdProperties;)V

    .line 131
    return-void
.end method

.method public onSpecialUrlClick(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidRenderer;->isAdViewRemoved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0, p2}, Lcom/amazon/device/ads/AdBridge;->specialUrlClicked(Ljava/lang/String;)V

    .line 125
    :cond_0
    return-void
.end method

.method protected prepareToGoAway()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->prepareToGoAway()V

    .line 96
    :cond_0
    return-void
.end method

.method protected removeView()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->isInvalidated()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdLayout()Lcom/amazon/device/ads/AdLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdLayout;->removeAllViews()V

    .line 75
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidRenderer;->viewRemoved_:Z

    .line 76
    return-void
.end method

.method protected render()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 39
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidRenderer;->isAdViewDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Lcom/amazon/device/ads/MraidView;

    iget-object v1, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v2}, Lcom/amazon/device/ads/AdBridge;->getWindowWidth()I

    move-result v2

    iget-object v3, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v3}, Lcom/amazon/device/ads/AdBridge;->getWindowHeight()I

    move-result v3

    iget-wide v4, p0, Lcom/amazon/device/ads/MraidRenderer;->scalingFactor_:D

    iget-boolean v6, p0, Lcom/amazon/device/ads/MraidRenderer;->usingDownscalingLogic_:Z

    invoke-direct/range {v0 .. v6}, Lcom/amazon/device/ads/MraidView;-><init>(Landroid/content/Context;IIDZ)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    .line 46
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    iget-object v1, p0, Lcom/amazon/device/ads/MraidRenderer;->ad_:Lcom/amazon/device/ads/AdResponse;

    iget-object v1, v1, Lcom/amazon/device/ads/AdResponse;->creative_:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->loadHtmlData(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0, p0}, Lcom/amazon/device/ads/MraidView;->setOnReadyListener(Lcom/amazon/device/ads/MraidView$OnReadyListener;)V

    .line 49
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0, p0}, Lcom/amazon/device/ads/MraidView;->setOnSpecialUrlClickListener(Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0, p0}, Lcom/amazon/device/ads/MraidView;->setOnExpandListener(Lcom/amazon/device/ads/MraidView$OnExpandListener;)V

    .line 51
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0, p0}, Lcom/amazon/device/ads/MraidView;->setOnCloseListener(Lcom/amazon/device/ads/MraidView$OnCloseListener;)V

    .line 53
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdBridge;->getAdLayout()Lcom/amazon/device/ads/AdLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdLayout;->removeAllViews()V

    .line 54
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v7, v7, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 58
    iget-object v1, p0, Lcom/amazon/device/ads/MraidRenderer;->bridge_:Lcom/amazon/device/ads/AdBridge;

    invoke-virtual {v1}, Lcom/amazon/device/ads/AdBridge;->getAdLayout()Lcom/amazon/device/ads/AdLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v1, v2, v0}, Lcom/amazon/device/ads/AdLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected sendCommand(Ljava/lang/String;Ljava/util/Map;)Z
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 107
    const-string v0, "MraidRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendCommand: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "close"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/amazon/device/ads/MraidRenderer;->mraidView_:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->close()V

    .line 113
    const/4 v0, 0x1

    .line 117
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected shouldReuse()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method
