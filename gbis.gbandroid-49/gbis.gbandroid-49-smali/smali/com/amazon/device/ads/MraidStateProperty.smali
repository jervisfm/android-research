.class Lcom/amazon/device/ads/MraidStateProperty;
.super Lcom/amazon/device/ads/MraidProperty;
.source "GBFile"


# instance fields
.field private final mViewState:Lcom/amazon/device/ads/MraidView$ViewState;


# direct methods
.method constructor <init>(Lcom/amazon/device/ads/MraidView$ViewState;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/amazon/device/ads/MraidProperty;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/amazon/device/ads/MraidStateProperty;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    .line 61
    return-void
.end method

.method public static createWithViewState(Lcom/amazon/device/ads/MraidView$ViewState;)Lcom/amazon/device/ads/MraidStateProperty;
    .locals 1
    .parameter

    .prologue
    .line 64
    new-instance v0, Lcom/amazon/device/ads/MraidStateProperty;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/MraidStateProperty;-><init>(Lcom/amazon/device/ads/MraidView$ViewState;)V

    return-object v0
.end method


# virtual methods
.method public toJsonPair()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "state: \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/device/ads/MraidStateProperty;->mViewState:Lcom/amazon/device/ads/MraidView$ViewState;

    invoke-virtual {v1}, Lcom/amazon/device/ads/MraidView$ViewState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
