.class Lcom/amazon/device/ads/MraidView$MraidWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/MraidView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MraidWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/MraidView;


# direct methods
.method private constructor <init>(Lcom/amazon/device/ads/MraidView;)V
    .locals 0
    .parameter

    .prologue
    .line 442
    iput-object p1, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 442
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;-><init>(Lcom/amazon/device/ads/MraidView;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 493
    const-string v0, "MraidView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded resource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 480
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    #getter for: Lcom/amazon/device/ads/MraidView;->mHasFiredReadyEvent:Z
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView;->access$1100(Lcom/amazon/device/ads/MraidView;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 481
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    #getter for: Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView;->access$1200(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->initializeJavaScriptState()V

    .line 482
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    iget-object v1, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    #getter for: Lcom/amazon/device/ads/MraidView;->mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;
    invoke-static {v1}, Lcom/amazon/device/ads/MraidView;->access$1300(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidView$PlacementType;

    move-result-object v1

    invoke-static {v1}, Lcom/amazon/device/ads/MraidPlacementTypeProperty;->createWithType(Lcom/amazon/device/ads/MraidView$PlacementType;)Lcom/amazon/device/ads/MraidPlacementTypeProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/MraidView;->fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V

    .line 484
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->fireReadyEvent()V

    .line 485
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnReadyListener()Lcom/amazon/device/ads/MraidView$OnReadyListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidView;->getOnReadyListener()Lcom/amazon/device/ads/MraidView$OnReadyListener;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-interface {v0, v1}, Lcom/amazon/device/ads/MraidView$OnReadyListener;->onReady(Lcom/amazon/device/ads/MraidView;)V

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    const/4 v1, 0x1

    #setter for: Lcom/amazon/device/ads/MraidView;->mHasFiredReadyEvent:Z
    invoke-static {v0, v1}, Lcom/amazon/device/ads/MraidView;->access$1102(Lcom/amazon/device/ads/MraidView;Z)Z

    .line 487
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    #getter for: Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView;->access$1200(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidDisplayController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->surfaceAd()V

    .line 489
    :cond_1
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 446
    const-string v0, "MraidView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 448
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 452
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 453
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 455
    const-string v2, "mopub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 474
    :goto_0
    return v0

    .line 456
    :cond_0
    const-string v2, "mraid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 457
    iget-object v1, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    #calls: Lcom/amazon/device/ads/MraidView;->tryCommand(Ljava/net/URI;)Z
    invoke-static {v1, v2}, Lcom/amazon/device/ads/MraidView;->access$900(Lcom/amazon/device/ads/MraidView;Ljava/net/URI;)Z

    goto :goto_0

    .line 460
    :cond_1
    const-string v2, "amazonmobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 461
    iget-object v1, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    #getter for: Lcom/amazon/device/ads/MraidView;->mBrowserController:Lcom/amazon/device/ads/MraidBrowserController;
    invoke-static {v1}, Lcom/amazon/device/ads/MraidView;->access$1000(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidBrowserController;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v1, v2, p2}, Lcom/amazon/device/ads/MraidBrowserController;->executeAmazonMobileCallback(Lcom/amazon/device/ads/MraidView;Ljava/lang/String;)V

    goto :goto_0

    .line 465
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 466
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 468
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 471
    :try_start_0
    iget-object v2, p0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;->this$0:Lcom/amazon/device/ads/MraidView;

    invoke-virtual {v2}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 474
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
