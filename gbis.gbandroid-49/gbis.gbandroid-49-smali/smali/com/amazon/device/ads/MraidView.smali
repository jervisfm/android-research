.class Lcom/amazon/device/ads/MraidView;
.super Landroid/webkit/WebView;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazon/device/ads/MraidView$1;,
        Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;,
        Lcom/amazon/device/ads/MraidView$OnOpenListener;,
        Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;,
        Lcom/amazon/device/ads/MraidView$OnFailureListener;,
        Lcom/amazon/device/ads/MraidView$OnReadyListener;,
        Lcom/amazon/device/ads/MraidView$OnCloseListener;,
        Lcom/amazon/device/ads/MraidView$OnExpandListener;,
        Lcom/amazon/device/ads/MraidView$MraidWebChromeClient;,
        Lcom/amazon/device/ads/MraidView$MraidWebViewClient;,
        Lcom/amazon/device/ads/MraidView$PlacementType;,
        Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;,
        Lcom/amazon/device/ads/MraidView$ExpansionStyle;,
        Lcom/amazon/device/ads/MraidView$ViewState;,
        Lcom/amazon/device/ads/MraidView$MraidListenerInfo;
    }
.end annotation


# static fields
.field public static final AD_CONTAINER_LAYOUT_ID:I = 0x66

.field private static final LOGTAG:Ljava/lang/String; = "MraidView"

.field public static final MODAL_CONTAINER_LAYOUT_ID:I = 0x65

.field public static final PLACEHOLDER_VIEW_ID:I = 0x64


# instance fields
.field private mAttached:Z

.field private mBrowserController:Lcom/amazon/device/ads/MraidBrowserController;

.field private mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

.field private mGoingAway:Z

.field private mHasFiredReadyEvent:Z

.field private mLastVisibility:I

.field private mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

.field private final mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;

.field private mWebChromeClient:Landroid/webkit/WebChromeClient;

.field private mWebViewClient:Landroid/webkit/WebViewClient;

.field private scalingFactor_:D

.field private usingDownscalingLogic_:Z

.field private windowHeight_:I

.field private windowWidth_:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIDZ)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    sget-object v7, Lcom/amazon/device/ads/MraidView$ExpansionStyle;->ENABLED:Lcom/amazon/device/ads/MraidView$ExpansionStyle;

    sget-object v8, Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;->AD_CONTROLLED:Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;

    sget-object v9, Lcom/amazon/device/ads/MraidView$PlacementType;->INLINE:Lcom/amazon/device/ads/MraidView$PlacementType;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    move/from16 v6, p6

    invoke-direct/range {v0 .. v9}, Lcom/amazon/device/ads/MraidView;-><init>(Landroid/content/Context;IIDZLcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;Lcom/amazon/device/ads/MraidView$PlacementType;)V

    .line 128
    return-void
.end method

.method constructor <init>(Landroid/content/Context;IIDZLcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;Lcom/amazon/device/ads/MraidView$PlacementType;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 134
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 58
    iput-boolean v1, p0, Lcom/amazon/device/ads/MraidView;->mAttached:Z

    .line 59
    const/16 v0, 0x8

    iput v0, p0, Lcom/amazon/device/ads/MraidView;->mLastVisibility:I

    .line 89
    iput-boolean v1, p0, Lcom/amazon/device/ads/MraidView;->mGoingAway:Z

    .line 135
    iput-object p9, p0, Lcom/amazon/device/ads/MraidView;->mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;

    .line 136
    iput p3, p0, Lcom/amazon/device/ads/MraidView;->windowHeight_:I

    .line 137
    iput p2, p0, Lcom/amazon/device/ads/MraidView;->windowWidth_:I

    .line 138
    iput-wide p4, p0, Lcom/amazon/device/ads/MraidView;->scalingFactor_:D

    .line 139
    iput-boolean p6, p0, Lcom/amazon/device/ads/MraidView;->usingDownscalingLogic_:Z

    .line 140
    invoke-direct {p0, p7, p8}, Lcom/amazon/device/ads/MraidView;->initialize(Lcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;)V

    .line 141
    return-void
.end method

.method static synthetic access$1000(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidBrowserController;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mBrowserController:Lcom/amazon/device/ads/MraidBrowserController;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/amazon/device/ads/MraidView;)Z
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mHasFiredReadyEvent:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/amazon/device/ads/MraidView;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/amazon/device/ads/MraidView;->mHasFiredReadyEvent:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidDisplayController;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidView$PlacementType;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mPlacementType:Lcom/amazon/device/ads/MraidView$PlacementType;

    return-object v0
.end method

.method static synthetic access$900(Lcom/amazon/device/ads/MraidView;Ljava/net/URI;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/amazon/device/ads/MraidView;->tryCommand(Ljava/net/URI;)Z

    move-result v0

    return v0
.end method

.method private copyRawResourceToFilesDir(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/amazon/device/ads/ResourceLookup;->getResourceFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 415
    if-nez v1, :cond_0

    .line 416
    const-string v0, ""

    .line 439
    :goto_0
    return-object v0

    .line 418
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 420
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 423
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 428
    const/16 v2, 0x2000

    new-array v2, v2, [B

    .line 430
    :goto_1
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 431
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 434
    :catch_0
    move-exception v0

    const-string v0, ""

    .line 436
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 425
    :catch_2
    move-exception v0

    const-string v0, ""

    goto :goto_0

    .line 436
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 437
    :catch_3
    move-exception v1

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_2
    throw v0

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private initialize(Lcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 144
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setScrollContainer(Z)V

    .line 145
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setBackgroundColor(I)V

    .line 147
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setVerticalScrollBarEnabled(Z)V

    .line 148
    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setHorizontalScrollBarEnabled(Z)V

    .line 150
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 151
    invoke-virtual {p0}, Lcom/amazon/device/ads/MraidView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 153
    new-instance v0, Lcom/amazon/device/ads/MraidBrowserController;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/MraidBrowserController;-><init>(Lcom/amazon/device/ads/MraidView;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidView;->mBrowserController:Lcom/amazon/device/ads/MraidBrowserController;

    .line 154
    new-instance v0, Lcom/amazon/device/ads/MraidDisplayController;

    invoke-direct {v0, p0, p1, p2}, Lcom/amazon/device/ads/MraidDisplayController;-><init>(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$ExpansionStyle;Lcom/amazon/device/ads/MraidView$NativeCloseButtonStyle;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    .line 156
    new-instance v0, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;

    invoke-direct {v0, p0, v2}, Lcom/amazon/device/ads/MraidView$MraidWebViewClient;-><init>(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$1;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidView;->mWebViewClient:Landroid/webkit/WebViewClient;

    .line 157
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mWebViewClient:Landroid/webkit/WebViewClient;

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 159
    new-instance v0, Lcom/amazon/device/ads/MraidView$MraidWebChromeClient;

    invoke-direct {v0, p0, v2}, Lcom/amazon/device/ads/MraidView$MraidWebChromeClient;-><init>(Lcom/amazon/device/ads/MraidView;Lcom/amazon/device/ads/MraidView$1;)V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidView;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    .line 160
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mWebChromeClient:Landroid/webkit/WebChromeClient;

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 162
    new-instance v0, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    invoke-direct {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;-><init>()V

    iput-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    .line 163
    return-void
.end method

.method private notifyOnFailureListener()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnFailureListener:Lcom/amazon/device/ads/MraidView$OnFailureListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$200(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnFailureListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnFailureListener:Lcom/amazon/device/ads/MraidView$OnFailureListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$200(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnFailureListener;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/amazon/device/ads/MraidView$OnFailureListener;->onFailure(Lcom/amazon/device/ads/MraidView;)V

    .line 287
    :cond_0
    return-void
.end method

.method private tryCommand(Ljava/net/URI;)Z
    .locals 5
    .parameter

    .prologue
    .line 391
    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 392
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 393
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 394
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/NameValuePair;

    .line 395
    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 398
    :cond_0
    invoke-static {v1, v2, p0}, Lcom/amazon/device/ads/MraidCommandRegistry;->createCommand(Ljava/lang/String;Ljava/util/Map;Lcom/amazon/device/ads/MraidView;)Lcom/amazon/device/ads/MraidCommand;

    move-result-object v0

    .line 399
    if-nez v0, :cond_1

    .line 400
    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidView;->fireNativeCommandCompleteEvent(Ljava/lang/String;)V

    .line 401
    const/4 v0, 0x0

    .line 405
    :goto_1
    return v0

    .line 403
    :cond_1
    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidCommand;->execute()V

    .line 404
    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidView;->fireNativeCommandCompleteEvent(Ljava/lang/String;)V

    .line 405
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->destroy()V

    .line 220
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 221
    return-void
.end method

.method protected fireChangeEventForProperties(Ljava/util/ArrayList;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amazon/device/ads/MraidProperty;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 370
    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    .line 371
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 376
    :goto_0
    return-void

    .line 373
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window.mraidbridge.fireChangeEvent("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidView;->injectJavaScript(Ljava/lang/String;)V

    .line 375
    const-string v1, "MraidView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fire changes: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected fireChangeEventForProperty(Lcom/amazon/device/ads/MraidProperty;)V
    .locals 4
    .parameter

    .prologue
    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/amazon/device/ads/MraidProperty;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 365
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window.mraidbridge.fireChangeEvent("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ");"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/MraidView;->injectJavaScript(Ljava/lang/String;)V

    .line 366
    const-string v1, "MraidView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fire change: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/amazon/device/ads/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    return-void
.end method

.method protected fireErrorEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "window.mraidbridge.fireErrorEvent(\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\');"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->injectJavaScript(Ljava/lang/String;)V

    .line 380
    return-void
.end method

.method protected fireNativeCommandCompleteEvent(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "window.mraidbridge.nativeCallComplete(\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\');"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->injectJavaScript(Ljava/lang/String;)V

    .line 388
    return-void
.end method

.method protected fireReadyEvent()V
    .locals 1

    .prologue
    .line 383
    const-string v0, "window.mraidbridge.fireReadyEvent();"

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->injectJavaScript(Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method protected getBrowserController()Lcom/amazon/device/ads/MraidBrowserController;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mBrowserController:Lcom/amazon/device/ads/MraidBrowserController;

    return-object v0
.end method

.method protected getDisplayController()Lcom/amazon/device/ads/MraidDisplayController;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    return-object v0
.end method

.method public getOnCloseButtonStateChangeListener()Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnCloseButtonListener:Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$600(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnCloseListener()Lcom/amazon/device/ads/MraidView$OnCloseListener;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnCloseListener:Lcom/amazon/device/ads/MraidView$OnCloseListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$400(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnCloseListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnExpandListener()Lcom/amazon/device/ads/MraidView$OnExpandListener;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnExpandListener:Lcom/amazon/device/ads/MraidView$OnExpandListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$300(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnExpandListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnFailureListener()Lcom/amazon/device/ads/MraidView$OnFailureListener;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnFailureListener:Lcom/amazon/device/ads/MraidView$OnFailureListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$200(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnFailureListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnOpenListener()Lcom/amazon/device/ads/MraidView$OnOpenListener;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnOpenListener:Lcom/amazon/device/ads/MraidView$OnOpenListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$700(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnOpenListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnReadyListener()Lcom/amazon/device/ads/MraidView$OnReadyListener;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnReadyListener:Lcom/amazon/device/ads/MraidView$OnReadyListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$500(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnReadyListener;

    move-result-object v0

    return-object v0
.end method

.method public getOnSpecialUrlClickListener()Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #getter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnSpecialUrlClickListener:Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;
    invoke-static {v0}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$800(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;)Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;

    move-result-object v0

    return-object v0
.end method

.method protected getScalingFactor()D
    .locals 2

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/amazon/device/ads/MraidView;->scalingFactor_:D

    return-wide v0
.end method

.method protected getWindowHeight()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/amazon/device/ads/MraidView;->windowHeight_:I

    return v0
.end method

.method protected getWindowWidth()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/amazon/device/ads/MraidView;->windowWidth_:I

    return v0
.end method

.method protected injectJavaScript(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 360
    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 361
    :cond_0
    return-void
.end method

.method protected isUsingDownscalingLogic()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->usingDownscalingLogic_:Z

    return v0
.end method

.method public loadHtmlData(Ljava/lang/String;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 225
    const-string v0, "<html>"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 226
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->usingDownscalingLogic_:Z

    if-eqz v0, :cond_1

    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<html><head></head><body style=\'margin:0;padding:0;\'>"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</body></html>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 230
    iget-wide v2, p0, Lcom/amazon/device/ads/MraidView;->scalingFactor_:D

    const-wide/high16 v4, 0x4059

    mul-double/2addr v2, v4

    double-to-int v0, v2

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setInitialScale(I)V

    .line 250
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "file://"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "ad_resources/raw/amazon_ads_mraid.js"

    const-string v3, "mraid.js"

    invoke-direct {p0, v2, v3}, Lcom/amazon/device/ads/MraidView;->copyRawResourceToFilesDir(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    const-string v2, "<head>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<head><script src=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'></script>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 253
    const-string v3, "text/html"

    const-string v4, "UTF-8"

    move-object v0, p0

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/amazon/device/ads/MraidView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void

    .line 235
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->setInitialScale(I)V

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<html><meta name=\"viewport\" content=\"width="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/amazon/device/ads/MraidView;->windowWidth_:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", height="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/amazon/device/ads/MraidView;->windowHeight_:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", initial-scale="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/amazon/device/ads/MraidView;->scalingFactor_:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"/><head></head><body style=\'margin:0;padding:0;\'>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</body></html>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 6
    .parameter

    .prologue
    .line 257
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 258
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 259
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 262
    :try_start_0
    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 263
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_0

    .line 266
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 267
    const/16 v1, 0x1000

    new-array v1, v1, [B

    .line 268
    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 269
    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5, v3}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 273
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/amazon/device/ads/MraidView;->notifyOnFailureListener()V

    .line 281
    :goto_1
    return-void

    .line 276
    :catch_1
    move-exception v0

    invoke-direct {p0}, Lcom/amazon/device/ads/MraidView;->notifyOnFailureListener()V

    goto :goto_1

    .line 280
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amazon/device/ads/MraidView;->loadHtmlData(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mGoingAway:Z

    if-eqz v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mAttached:Z

    .line 189
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->registerRecievers()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 199
    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mAttached:Z

    .line 201
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->unregisterRecievers()V

    .line 204
    :cond_0
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 1
    .parameter

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mAttached:Z

    if-eqz v0, :cond_0

    .line 208
    iget v0, p0, Lcom/amazon/device/ads/MraidView;->mLastVisibility:I

    if-eq v0, p1, :cond_0

    .line 209
    if-eqz p1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mDisplayController:Lcom/amazon/device/ads/MraidDisplayController;

    invoke-virtual {v0}, Lcom/amazon/device/ads/MraidDisplayController;->unregisterRecievers()V

    .line 216
    :cond_0
    return-void
.end method

.method public prepareToGoAway()V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amazon/device/ads/MraidView;->mGoingAway:Z

    .line 196
    return-void
.end method

.method public setOnCloseButtonStateChange(Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;)V
    .locals 1
    .parameter

    .prologue
    .line 334
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnCloseButtonListener:Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$602(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;)Lcom/amazon/device/ads/MraidView$OnCloseButtonStateChangeListener;

    .line 335
    return-void
.end method

.method public setOnCloseListener(Lcom/amazon/device/ads/MraidView$OnCloseListener;)V
    .locals 1
    .parameter

    .prologue
    .line 310
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnCloseListener:Lcom/amazon/device/ads/MraidView$OnCloseListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$402(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnCloseListener;)Lcom/amazon/device/ads/MraidView$OnCloseListener;

    .line 311
    return-void
.end method

.method public setOnExpandListener(Lcom/amazon/device/ads/MraidView$OnExpandListener;)V
    .locals 1
    .parameter

    .prologue
    .line 302
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnExpandListener:Lcom/amazon/device/ads/MraidView$OnExpandListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$302(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnExpandListener;)Lcom/amazon/device/ads/MraidView$OnExpandListener;

    .line 303
    return-void
.end method

.method public setOnFailureListener(Lcom/amazon/device/ads/MraidView$OnFailureListener;)V
    .locals 1
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnFailureListener:Lcom/amazon/device/ads/MraidView$OnFailureListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$202(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnFailureListener;)Lcom/amazon/device/ads/MraidView$OnFailureListener;

    .line 327
    return-void
.end method

.method public setOnOpenListener(Lcom/amazon/device/ads/MraidView$OnOpenListener;)V
    .locals 1
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnOpenListener:Lcom/amazon/device/ads/MraidView$OnOpenListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$702(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnOpenListener;)Lcom/amazon/device/ads/MraidView$OnOpenListener;

    .line 343
    return-void
.end method

.method public setOnReadyListener(Lcom/amazon/device/ads/MraidView$OnReadyListener;)V
    .locals 1
    .parameter

    .prologue
    .line 318
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnReadyListener:Lcom/amazon/device/ads/MraidView$OnReadyListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$502(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnReadyListener;)Lcom/amazon/device/ads/MraidView$OnReadyListener;

    .line 319
    return-void
.end method

.method public setOnSpecialUrlClickListener(Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;)V
    .locals 1
    .parameter

    .prologue
    .line 350
    iget-object v0, p0, Lcom/amazon/device/ads/MraidView;->mListenerInfo:Lcom/amazon/device/ads/MraidView$MraidListenerInfo;

    #setter for: Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->mOnSpecialUrlClickListener:Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;
    invoke-static {v0, p1}, Lcom/amazon/device/ads/MraidView$MraidListenerInfo;->access$802(Lcom/amazon/device/ads/MraidView$MraidListenerInfo;Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;)Lcom/amazon/device/ads/MraidView$OnSpecialUrlClickListener;

    .line 351
    return-void
.end method
