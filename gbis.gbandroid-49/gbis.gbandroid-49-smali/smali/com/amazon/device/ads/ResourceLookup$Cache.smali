.class public Lcom/amazon/device/ads/ResourceLookup$Cache;
.super Ljava/util/LinkedHashMap;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazon/device/ads/ResourceLookup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Cache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x48fb1d31163681f3L


# instance fields
.field final synthetic this$0:Lcom/amazon/device/ads/ResourceLookup;


# direct methods
.method public constructor <init>(Lcom/amazon/device/ads/ResourceLookup;)V
    .locals 3
    .parameter

    .prologue
    .line 207
    iput-object p1, p0, Lcom/amazon/device/ads/ResourceLookup$Cache;->this$0:Lcom/amazon/device/ads/ResourceLookup;

    .line 208
    const/16 v0, 0x65

    const v1, 0x3f8ccccd

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 209
    return-void
.end method


# virtual methods
.method protected removedEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/amazon/device/ads/ResourceLookup$Cache;->size()I

    move-result v0

    const/16 v1, 0x65

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
