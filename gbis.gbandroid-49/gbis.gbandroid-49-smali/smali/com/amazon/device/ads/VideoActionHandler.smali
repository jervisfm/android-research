.class public Lcom/amazon/device/ads/VideoActionHandler;
.super Landroid/app/Activity;
.source "GBFile"


# instance fields
.field private layout_:Landroid/widget/RelativeLayout;

.field private player_:Lcom/amazon/device/ads/AdVideoPlayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private initPlayer(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 57
    const-string v0, "player_properties"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/amazon/device/ads/Controller$PlayerProperties;

    .line 59
    const-string v1, "player_dimensions"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/amazon/device/ads/Controller$Dimensions;

    .line 62
    new-instance v2, Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-direct {v2, p0}, Lcom/amazon/device/ads/AdVideoPlayer;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    .line 63
    iget-object v2, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    const-string v3, "url"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/amazon/device/ads/AdVideoPlayer;->setPlayData(Lcom/amazon/device/ads/Controller$PlayerProperties;Ljava/lang/String;)V

    .line 66
    if-nez v1, :cond_0

    .line 68
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 70
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 79
    :goto_0
    iget-object v1, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/amazon/device/ads/AdVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v0, p0, Lcom/amazon/device/ads/VideoActionHandler;->layout_:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-direct {p0, v0}, Lcom/amazon/device/ads/VideoActionHandler;->setPlayerListener(Lcom/amazon/device/ads/AdVideoPlayer;)V

    .line 82
    return-void

    .line 74
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v1, Lcom/amazon/device/ads/Controller$Dimensions;->width:I

    iget v3, v1, Lcom/amazon/device/ads/Controller$Dimensions;->height:I

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 75
    iget v2, v1, Lcom/amazon/device/ads/Controller$Dimensions;->y:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 76
    iget v1, v1, Lcom/amazon/device/ads/Controller$Dimensions;->x:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method private setPlayerListener(Lcom/amazon/device/ads/AdVideoPlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 34
    new-instance v0, Lcom/amazon/device/ads/VideoActionHandler$1;

    invoke-direct {v0, p0}, Lcom/amazon/device/ads/VideoActionHandler$1;-><init>(Lcom/amazon/device/ads/VideoActionHandler;)V

    invoke-virtual {p1, v0}, Lcom/amazon/device/ads/AdVideoPlayer;->setListener(Lcom/amazon/device/ads/AdVideoPlayer$AdVideoPlayerListener;)V

    .line 53
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/amazon/device/ads/VideoActionHandler;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 24
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/amazon/device/ads/VideoActionHandler;->layout_:Landroid/widget/RelativeLayout;

    .line 25
    iget-object v1, p0, Lcom/amazon/device/ads/VideoActionHandler;->layout_:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 27
    iget-object v1, p0, Lcom/amazon/device/ads/VideoActionHandler;->layout_:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1}, Lcom/amazon/device/ads/VideoActionHandler;->setContentView(Landroid/view/View;)V

    .line 28
    invoke-direct {p0, v0}, Lcom/amazon/device/ads/VideoActionHandler;->initPlayer(Landroid/os/Bundle;)V

    .line 29
    iget-object v0, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdVideoPlayer;->playVideo()V

    .line 30
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    invoke-virtual {v0}, Lcom/amazon/device/ads/AdVideoPlayer;->releasePlayer()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/device/ads/VideoActionHandler;->player_:Lcom/amazon/device/ads/AdVideoPlayer;

    .line 89
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 90
    return-void
.end method
