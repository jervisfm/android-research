.class public final Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/ads/mediation/NetworkExtras;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;,
        Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;
    }
.end annotation


# instance fields
.field private a:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;

.field private b:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;

.field private c:Ljava/lang/Integer;

.field private d:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;

.field private e:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;

.field private f:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;

.field private g:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;

.field private h:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    sget-object v0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;->UNKNOWN:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;

    iput-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->a:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;

    .line 68
    sget-object v0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;->UNKNOWN:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;

    iput-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->b:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;

    .line 95
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->c:Ljava/lang/Integer;

    .line 143
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->d:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;

    .line 195
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->e:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;

    .line 242
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->f:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;

    .line 296
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->g:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;

    .line 350
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->h:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;

    .line 377
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->i:Ljava/lang/Boolean;

    .line 404
    iput-object v1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final clearAdLocation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setAdLocation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearChildren()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setChildren(Ljava/lang/Boolean;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearEducation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setEducation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearEthnicity()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setEthnicity(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearIncomeInUsDollars()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setIncomeInUsDollars(Ljava/lang/Integer;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearInterstitialTime()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setInterstitialTime(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearMaritalStatus()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setMaritalStatus(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearOrientation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setOrientation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearPolitics()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setPolitics(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final clearPostalCode()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->setPostalCode(Ljava/lang/String;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;

    move-result-object v0

    return-object v0
.end method

.method public final getAdLocation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->a:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;

    return-object v0
.end method

.method public final getChildren()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getEducation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->h:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;

    return-object v0
.end method

.method public final getEthnicity()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->e:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;

    return-object v0
.end method

.method public final getIncomeInUsDollars()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getInterstitialTime()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->b:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;

    return-object v0
.end method

.method public final getMaritalStatus()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->d:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;

    return-object v0
.end method

.method public final getOrientation()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->f:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;

    return-object v0
.end method

.method public final getPolitics()Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->g:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;

    return-object v0
.end method

.method public final getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final setAdLocation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->a:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$AdLocation;

    .line 39
    return-object p0
.end method

.method public final setChildren(Ljava/lang/Boolean;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->i:Ljava/lang/Boolean;

    .line 384
    return-object p0
.end method

.method public final setEducation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->h:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Education;

    .line 357
    return-object p0
.end method

.method public final setEthnicity(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->e:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Ethnicity;

    .line 202
    return-object p0
.end method

.method public final setIncomeInUsDollars(Ljava/lang/Integer;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->c:Ljava/lang/Integer;

    .line 102
    return-object p0
.end method

.method public final setInterstitialTime(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->b:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$InterstitialTime;

    .line 75
    return-object p0
.end method

.method public final setMaritalStatus(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->d:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$MaritalStatus;

    .line 150
    return-object p0
.end method

.method public final setOrientation(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->f:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Orientation;

    .line 249
    return-object p0
.end method

.method public final setPolitics(Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->g:Lcom/google/ads/mediation/millennial/MillennialAdapterExtras$Politics;

    .line 303
    return-object p0
.end method

.method public final setPostalCode(Ljava/lang/String;)Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;
    .locals 0
    .parameter

    .prologue
    .line 410
    iput-object p1, p0, Lcom/google/ads/mediation/millennial/MillennialAdapterExtras;->j:Ljava/lang/String;

    .line 411
    return-object p0
.end method
