.class public Lcom/google/android/apps/analytics/Transaction;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/analytics/Transaction$Builder;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:D

.field private final d:D

.field private final e:D


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/Transaction$Builder;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->a(Lcom/google/android/apps/analytics/Transaction$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->b(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->c:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->c(Lcom/google/android/apps/analytics/Transaction$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->d(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->d:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->e(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->e:D

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/Transaction$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/Transaction;-><init>(Lcom/google/android/apps/analytics/Transaction$Builder;)V

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->a:Ljava/lang/String;

    return-object v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->b:Ljava/lang/String;

    return-object v0
.end method

.method final c()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->c:D

    return-wide v0
.end method

.method final d()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->d:D

    return-wide v0
.end method

.method final e()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->e:D

    return-wide v0
.end method
