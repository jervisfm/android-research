.class final Lcom/google/android/apps/analytics/c;
.super Ljava/lang/Object;


# instance fields
.field final a:J

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:I

.field final g:I

.field final h:I

.field i:Lcom/google/android/apps/analytics/b;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Lcom/google/android/apps/analytics/Transaction;

.field private t:Lcom/google/android/apps/analytics/Item;


# direct methods
.method constructor <init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/analytics/c;->a:J

    iput-object p3, p0, Lcom/google/android/apps/analytics/c;->b:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/analytics/c;->j:I

    iput p5, p0, Lcom/google/android/apps/analytics/c;->l:I

    iput p6, p0, Lcom/google/android/apps/analytics/c;->m:I

    iput p7, p0, Lcom/google/android/apps/analytics/c;->n:I

    iput p8, p0, Lcom/google/android/apps/analytics/c;->o:I

    iput-object p9, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/analytics/c;->d:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/apps/analytics/c;->e:Ljava/lang/String;

    iput p12, p0, Lcom/google/android/apps/analytics/c;->f:I

    iput p14, p0, Lcom/google/android/apps/analytics/c;->h:I

    iput p13, p0, Lcom/google/android/apps/analytics/c;->g:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/analytics/c;->p:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/c;->r:Z

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/analytics/c;Ljava/lang/String;)V
    .locals 16

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/apps/analytics/c;->a:J

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/analytics/c;->j:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/analytics/c;->l:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/analytics/c;->m:I

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/apps/analytics/c;->n:I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/apps/analytics/c;->o:I

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/apps/analytics/c;->d:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/analytics/c;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v13, v0, Lcom/google/android/apps/analytics/c;->f:I

    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/apps/analytics/c;->g:I

    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/android/apps/analytics/c;->h:I

    move-object/from16 v1, p0

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/analytics/c;-><init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/apps/analytics/c;->k:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/analytics/c;->k:I

    move-object/from16 v0, p1

    iget v1, v0, Lcom/google/android/apps/analytics/c;->p:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/analytics/c;->p:I

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/google/android/apps/analytics/c;->q:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/analytics/c;->q:Z

    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/google/android/apps/analytics/c;->r:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/analytics/c;->r:Z

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/c;->i:Lcom/google/android/apps/analytics/b;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/c;->i:Lcom/google/android/apps/analytics/b;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/c;->s:Lcom/google/android/apps/analytics/Transaction;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/c;->s:Lcom/google/android/apps/analytics/Transaction;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/analytics/c;->t:Lcom/google/android/apps/analytics/Item;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/analytics/c;->t:Lcom/google/android/apps/analytics/Item;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 15

    const-wide/16 v1, -0x1

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    move-object v0, p0

    move-object/from16 v3, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v12, p5

    move/from16 v13, p6

    move/from16 v14, p7

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/analytics/c;-><init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->j:I

    return v0
.end method

.method final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->j:I

    return-void
.end method

.method public final a(Lcom/google/android/apps/analytics/Item;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    const-string v1, "__##GOOGLEITEM##__"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempted to add an item to an event of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/analytics/c;->t:Lcom/google/android/apps/analytics/Item;

    return-void
.end method

.method public final a(Lcom/google/android/apps/analytics/Transaction;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    const-string v1, "__##GOOGLETRANSACTION##__"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempted to add a transction to an event of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/analytics/c;->s:Lcom/google/android/apps/analytics/Transaction;

    return-void
.end method

.method public final a(Lcom/google/android/apps/analytics/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/analytics/c;->i:Lcom/google/android/apps/analytics/b;

    return-void
.end method

.method final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/analytics/c;->q:Z

    return-void
.end method

.method final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->k:I

    return v0
.end method

.method final b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->k:I

    return-void
.end method

.method final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/analytics/c;->r:Z

    return-void
.end method

.method final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->l:I

    return v0
.end method

.method final c(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->l:I

    return-void
.end method

.method final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->m:I

    return v0
.end method

.method final d(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->m:I

    return-void
.end method

.method final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->n:I

    return v0
.end method

.method final e(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->n:I

    return-void
.end method

.method final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->o:I

    return v0
.end method

.method final f(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->o:I

    return-void
.end method

.method final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/c;->p:I

    return v0
.end method

.method final g(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/c;->p:I

    return-void
.end method

.method final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/analytics/c;->q:Z

    return v0
.end method

.method final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/analytics/c;->r:Z

    return v0
.end method

.method public final j()Lcom/google/android/apps/analytics/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/c;->i:Lcom/google/android/apps/analytics/b;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/analytics/Transaction;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/c;->s:Lcom/google/android/apps/analytics/Transaction;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/analytics/Item;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/c;->t:Lcom/google/android/apps/analytics/Item;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/analytics/c;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/analytics/c;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " random:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampCurrent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampPrevious:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampFirst:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " visits:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " category:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/c;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " label:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/c;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
