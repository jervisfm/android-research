.class final Lcom/google/android/apps/analytics/h$a$b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/apps/analytics/j$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/analytics/h$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/analytics/h$a;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/h$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/h$a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/h$a$b;-><init>(Lcom/google/android/apps/analytics/h$a;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/analytics/h$a;->a(Lcom/google/android/apps/analytics/h$a;I)I

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    invoke-static {v0, p1}, Lcom/google/android/apps/analytics/h$a;->b(Lcom/google/android/apps/analytics/h$a;I)I

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    invoke-static {v0}, Lcom/google/android/apps/analytics/h$a;->k(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/h$a$a;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    invoke-static {v0}, Lcom/google/android/apps/analytics/h$a;->k(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/h$a$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/h$a$a;->a()Lcom/google/android/apps/analytics/Hit;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/analytics/h$a$b;->a:Lcom/google/android/apps/analytics/h$a;

    invoke-static {v1}, Lcom/google/android/apps/analytics/h$a;->g(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/Dispatcher$Callbacks;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/apps/analytics/Hit;->b:J

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/analytics/Dispatcher$Callbacks;->hitDispatched(J)V

    goto :goto_0
.end method
