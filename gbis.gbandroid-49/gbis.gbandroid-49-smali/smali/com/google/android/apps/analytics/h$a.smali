.class final Lcom/google/android/apps/analytics/h$a;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/analytics/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/analytics/h$a$b;,
        Lcom/google/android/apps/analytics/h$a$a;
    }
.end annotation


# instance fields
.field volatile a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/analytics/j;

.field private final c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:J

.field private g:Lcom/google/android/apps/analytics/h$a$a;

.field private final h:Lcom/google/android/apps/analytics/Dispatcher$Callbacks;

.field private final i:Lcom/google/android/apps/analytics/h$a$b;

.field private final j:Lcom/google/android/apps/analytics/h;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/Dispatcher$Callbacks;Lcom/google/android/apps/analytics/j;Ljava/lang/String;Lcom/google/android/apps/analytics/h;)V
    .locals 2

    const-string v0, "DispatcherThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/android/apps/analytics/h$a;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/analytics/h$a;->g:Lcom/google/android/apps/analytics/h$a$a;

    iput-object p1, p0, Lcom/google/android/apps/analytics/h$a;->h:Lcom/google/android/apps/analytics/Dispatcher$Callbacks;

    iput-object p3, p0, Lcom/google/android/apps/analytics/h$a;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/analytics/h$a;->b:Lcom/google/android/apps/analytics/j;

    new-instance v0, Lcom/google/android/apps/analytics/h$a$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/analytics/h$a$b;-><init>(Lcom/google/android/apps/analytics/h$a;B)V

    iput-object v0, p0, Lcom/google/android/apps/analytics/h$a;->i:Lcom/google/android/apps/analytics/h$a$b;

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->b:Lcom/google/android/apps/analytics/j;

    iget-object v1, p0, Lcom/google/android/apps/analytics/h$a;->i:Lcom/google/android/apps/analytics/h$a$b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/j;->a(Lcom/google/android/apps/analytics/j$a;)V

    iput-object p4, p0, Lcom/google/android/apps/analytics/h$a;->j:Lcom/google/android/apps/analytics/h;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/analytics/Dispatcher$Callbacks;Ljava/lang/String;Lcom/google/android/apps/analytics/h;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/analytics/j;

    invoke-static {p3}, Lcom/google/android/apps/analytics/h;->a(Lcom/google/android/apps/analytics/h;)Lorg/apache/http/HttpHost;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/analytics/j;-><init>(Lorg/apache/http/HttpHost;)V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/apps/analytics/h$a;-><init>(Lcom/google/android/apps/analytics/Dispatcher$Callbacks;Lcom/google/android/apps/analytics/j;Ljava/lang/String;Lcom/google/android/apps/analytics/h;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/Dispatcher$Callbacks;Ljava/lang/String;Lcom/google/android/apps/analytics/h;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/analytics/h$a;-><init>(Lcom/google/android/apps/analytics/Dispatcher$Callbacks;Ljava/lang/String;Lcom/google/android/apps/analytics/h;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/h$a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/h$a;->d:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/h$a;I)I
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/analytics/h$a;->e:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/h$a;Lcom/google/android/apps/analytics/h$a$a;)Lcom/google/android/apps/analytics/h$a$a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/analytics/h$a;->g:Lcom/google/android/apps/analytics/h$a$a;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/h$a;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/h$a;->d:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/h$a;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/h$a;->f:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/apps/analytics/h$a;)J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/analytics/h$a;->f:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/analytics/h$a;->f:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/apps/analytics/h$a;)J
    .locals 2

    const-wide/16 v0, 0x2

    iput-wide v0, p0, Lcom/google/android/apps/analytics/h$a;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->j:Lcom/google/android/apps/analytics/h;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->b:Lcom/google/android/apps/analytics/j;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/Dispatcher$Callbacks;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->h:Lcom/google/android/apps/analytics/Dispatcher$Callbacks;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/analytics/h$a;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/h$a;->e:I

    return v0
.end method

.method static synthetic i(Lcom/google/android/apps/analytics/h$a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/h$a$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->i:Lcom/google/android/apps/analytics/h$a$b;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/analytics/h$a;)Lcom/google/android/apps/analytics/h$a$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->g:Lcom/google/android/apps/analytics/h$a$a;

    return-object v0
.end method


# virtual methods
.method public final a([Lcom/google/android/apps/analytics/Hit;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/analytics/h$a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/analytics/h$a$a;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/analytics/h$a$a;-><init>(Lcom/google/android/apps/analytics/h$a;[Lcom/google/android/apps/analytics/Hit;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final onLooperPrepared()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/h$a;->a:Landroid/os/Handler;

    return-void
.end method
