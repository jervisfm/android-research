.class public final Lcom/google/gbson/Gson;
.super Ljava/lang/Object;
.source "GBFile"


# static fields
.field static final a:Lcom/google/gbson/a;

.field static final b:Lcom/google/gbson/ap;

.field static final c:Lcom/google/gbson/ae;

.field static final d:Lcom/google/gbson/m;

.field private static final e:Lcom/google/gbson/ExclusionStrategy;


# instance fields
.field private final f:Lcom/google/gbson/ExclusionStrategy;

.field private final g:Lcom/google/gbson/ExclusionStrategy;

.field private final h:Lcom/google/gbson/m;

.field private final i:Lcom/google/gbson/ac;

.field private final j:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/google/gbson/a;

    invoke-direct {v0}, Lcom/google/gbson/a;-><init>()V

    sput-object v0, Lcom/google/gbson/Gson;->a:Lcom/google/gbson/a;

    .line 86
    new-instance v0, Lcom/google/gbson/ap;

    invoke-direct {v0}, Lcom/google/gbson/ap;-><init>()V

    sput-object v0, Lcom/google/gbson/Gson;->b:Lcom/google/gbson/ap;

    .line 88
    new-instance v0, Lcom/google/gbson/ae;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/google/gbson/ae;-><init>([I)V

    sput-object v0, Lcom/google/gbson/Gson;->c:Lcom/google/gbson/ae;

    .line 90
    new-instance v0, Lcom/google/gbson/an;

    new-instance v1, Lcom/google/gbson/p;

    invoke-direct {v1}, Lcom/google/gbson/p;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/gbson/an;-><init>(Lcom/google/gbson/m;)V

    sput-object v0, Lcom/google/gbson/Gson;->d:Lcom/google/gbson/m;

    .line 93
    invoke-static {}, Lcom/google/gbson/Gson;->a()Lcom/google/gbson/ExclusionStrategy;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/Gson;->e:Lcom/google/gbson/ExclusionStrategy;

    return-void

    .line 88
    :array_0
    .array-data 0x4
        0x80t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 148
    sget-object v1, Lcom/google/gbson/Gson;->e:Lcom/google/gbson/ExclusionStrategy;

    sget-object v2, Lcom/google/gbson/Gson;->e:Lcom/google/gbson/ExclusionStrategy;

    sget-object v3, Lcom/google/gbson/Gson;->d:Lcom/google/gbson/m;

    new-instance v4, Lcom/google/gbson/ac;

    invoke-static {}, Lcom/google/gbson/h;->d()Lcom/google/gbson/aj;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/gbson/ac;-><init>(Lcom/google/gbson/aj;)V

    invoke-static {}, Lcom/google/gbson/h;->a()Lcom/google/gbson/aj;

    move-result-object v6

    invoke-static {}, Lcom/google/gbson/h;->b()Lcom/google/gbson/aj;

    move-result-object v7

    const/4 v9, 0x1

    move-object v0, p0

    move v8, v5

    move v10, v5

    invoke-direct/range {v0 .. v10}, Lcom/google/gbson/Gson;-><init>(Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/m;Lcom/google/gbson/ac;ZLcom/google/gbson/aj;Lcom/google/gbson/aj;ZZZ)V

    .line 152
    return-void
.end method

.method constructor <init>(Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/m;Lcom/google/gbson/ac;ZLcom/google/gbson/aj;Lcom/google/gbson/aj;ZZZ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/ExclusionStrategy;",
            "Lcom/google/gbson/ExclusionStrategy;",
            "Lcom/google/gbson/m;",
            "Lcom/google/gbson/ac;",
            "Z",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p1, p0, Lcom/google/gbson/Gson;->f:Lcom/google/gbson/ExclusionStrategy;

    .line 161
    iput-object p2, p0, Lcom/google/gbson/Gson;->g:Lcom/google/gbson/ExclusionStrategy;

    .line 162
    iput-object p3, p0, Lcom/google/gbson/Gson;->h:Lcom/google/gbson/m;

    .line 163
    iput-object p4, p0, Lcom/google/gbson/Gson;->i:Lcom/google/gbson/ac;

    .line 164
    iput-boolean p5, p0, Lcom/google/gbson/Gson;->l:Z

    .line 165
    iput-object p6, p0, Lcom/google/gbson/Gson;->j:Lcom/google/gbson/aj;

    .line 166
    iput-object p7, p0, Lcom/google/gbson/Gson;->k:Lcom/google/gbson/aj;

    .line 167
    iput-boolean p8, p0, Lcom/google/gbson/Gson;->n:Z

    .line 168
    iput-boolean p9, p0, Lcom/google/gbson/Gson;->m:Z

    .line 169
    iput-boolean p10, p0, Lcom/google/gbson/Gson;->o:Z

    .line 170
    return-void
.end method

.method private static a()Lcom/google/gbson/ExclusionStrategy;
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 174
    sget-object v1, Lcom/google/gbson/Gson;->a:Lcom/google/gbson/a;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v1, Lcom/google/gbson/Gson;->b:Lcom/google/gbson/ap;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    sget-object v1, Lcom/google/gbson/Gson;->c:Lcom/google/gbson/ae;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    new-instance v1, Lcom/google/gbson/i;

    invoke-direct {v1, v0}, Lcom/google/gbson/i;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private static a(Ljava/lang/Object;Lcom/google/gbson/stream/JsonReader;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 474
    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/gbson/stream/JsonReader;->peek()Lcom/google/gbson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gbson/stream/JsonToken;->END_DOCUMENT:Lcom/google/gbson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    .line 475
    new-instance v0, Lcom/google/gbson/JsonIOException;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/gbson/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 477
    :catch_0
    move-exception v0

    .line 478
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 479
    :catch_1
    move-exception v0

    .line 480
    new-instance v1, Lcom/google/gbson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 481
    :cond_0
    return-void
.end method


# virtual methods
.method public final fromJson(Lcom/google/gbson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gbson/JsonElement;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 521
    invoke-virtual {p0, p1, p2}, Lcom/google/gbson/Gson;->fromJson(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 522
    invoke-static {p2}, Lcom/google/gbson/ak;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final fromJson(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gbson/JsonElement;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 545
    if-nez p1, :cond_0

    .line 546
    const/4 v0, 0x0

    .line 552
    :goto_0
    return-object v0

    .line 548
    :cond_0
    new-instance v0, Lcom/google/gbson/r;

    new-instance v1, Lcom/google/gbson/ObjectNavigator;

    iget-object v2, p0, Lcom/google/gbson/Gson;->f:Lcom/google/gbson/ExclusionStrategy;

    invoke-direct {v1, v2}, Lcom/google/gbson/ObjectNavigator;-><init>(Lcom/google/gbson/ExclusionStrategy;)V

    iget-object v2, p0, Lcom/google/gbson/Gson;->h:Lcom/google/gbson/m;

    iget-object v3, p0, Lcom/google/gbson/Gson;->k:Lcom/google/gbson/aj;

    iget-object v4, p0, Lcom/google/gbson/Gson;->i:Lcom/google/gbson/ac;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/gbson/r;-><init>(Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/aj;Lcom/google/gbson/ac;)V

    .line 551
    invoke-interface {v0, p1, p2}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final fromJson(Lcom/google/gbson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gbson/stream/JsonReader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/gbson/stream/JsonReader;->isLenient()Z

    move-result v1

    .line 495
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    .line 497
    :try_start_0
    invoke-static {p1}, Lcom/google/gbson/ao;->a(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    .line 498
    invoke-virtual {p0, v0, p2}, Lcom/google/gbson/Gson;->fromJson(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 500
    invoke-virtual {p1, v1}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    throw v0
.end method

.method public final fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 440
    new-instance v0, Lcom/google/gbson/stream/JsonReader;

    invoke-direct {v0, p1}, Lcom/google/gbson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 441
    invoke-virtual {p0, v0, p2}, Lcom/google/gbson/Gson;->fromJson(Lcom/google/gbson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 442
    invoke-static {v1, v0}, Lcom/google/gbson/Gson;->a(Ljava/lang/Object;Lcom/google/gbson/stream/JsonReader;)V

    .line 443
    invoke-static {p2}, Lcom/google/gbson/ak;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 466
    new-instance v0, Lcom/google/gbson/stream/JsonReader;

    invoke-direct {v0, p1}, Lcom/google/gbson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 467
    invoke-virtual {p0, v0, p2}, Lcom/google/gbson/Gson;->fromJson(Lcom/google/gbson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 468
    invoke-static {v1, v0}, Lcom/google/gbson/Gson;->a(Ljava/lang/Object;Lcom/google/gbson/stream/JsonReader;)V

    .line 469
    return-object v1
.end method

.method public final fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 389
    invoke-virtual {p0, p1, p2}, Lcom/google/gbson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 390
    invoke-static {p2}, Lcom/google/gbson/ak;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 413
    if-nez p1, :cond_0

    .line 414
    const/4 v0, 0x0

    .line 418
    :goto_0
    return-object v0

    .line 416
    :cond_0
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p0, v0, p2}, Lcom/google/gbson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final toJson(Lcom/google/gbson/JsonElement;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 324
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 325
    invoke-virtual {p0, p1, v0}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Ljava/lang/Appendable;)V

    .line 326
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toJson(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 237
    if-nez p1, :cond_0

    .line 238
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;)Ljava/lang/String;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/gbson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 259
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 260
    invoke-virtual {p0, p1, p2}, Lcom/google/gbson/Gson;->toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Ljava/lang/Appendable;)V

    .line 261
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toJson(Lcom/google/gbson/JsonElement;Lcom/google/gbson/stream/JsonWriter;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 357
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->isLenient()Z

    move-result v1

    .line 358
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->setLenient(Z)V

    .line 359
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->isHtmlSafe()Z

    move-result v2

    .line 360
    iget-boolean v0, p0, Lcom/google/gbson/Gson;->m:Z

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->setHtmlSafe(Z)V

    .line 362
    :try_start_0
    iget-boolean v0, p0, Lcom/google/gbson/Gson;->l:Z

    invoke-static {p1, v0, p2}, Lcom/google/gbson/ao;->a(Lcom/google/gbson/JsonElement;ZLcom/google/gbson/stream/JsonWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    invoke-virtual {p2, v1}, Lcom/google/gbson/stream/JsonWriter;->setLenient(Z)V

    .line 367
    invoke-virtual {p2, v2}, Lcom/google/gbson/stream/JsonWriter;->setHtmlSafe(Z)V

    .line 368
    return-void

    .line 363
    :catch_0
    move-exception v0

    .line 364
    :try_start_1
    new-instance v3, Lcom/google/gbson/JsonIOException;

    invoke-direct {v3, v0}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    :catchall_0
    move-exception v0

    invoke-virtual {p2, v1}, Lcom/google/gbson/stream/JsonWriter;->setLenient(Z)V

    .line 367
    invoke-virtual {p2, v2}, Lcom/google/gbson/stream/JsonWriter;->setHtmlSafe(Z)V

    throw v0
.end method

.method public final toJson(Lcom/google/gbson/JsonElement;Ljava/lang/Appendable;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 339
    :try_start_0
    iget-boolean v0, p0, Lcom/google/gbson/Gson;->n:Z

    if-eqz v0, :cond_0

    .line 340
    const-string v0, ")]}\'\n"

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 342
    :cond_0
    new-instance v0, Lcom/google/gbson/stream/JsonWriter;

    invoke-static {p2}, Lcom/google/gbson/ao;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 343
    iget-boolean v1, p0, Lcom/google/gbson/Gson;->o:Z

    if-eqz v1, :cond_1

    .line 344
    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/gbson/stream/JsonWriter;->setIndent(Ljava/lang/String;)V

    .line 346
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Lcom/google/gbson/stream/JsonWriter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    return-void

    .line 347
    :catch_0
    move-exception v0

    .line 348
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final toJson(Ljava/lang/Object;Ljava/lang/Appendable;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 279
    if-eqz p1, :cond_0

    .line 280
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/gbson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_0
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Ljava/lang/Appendable;)V

    goto :goto_0
.end method

.method public final toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/stream/JsonWriter;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 313
    invoke-virtual {p0, p1, p2}, Lcom/google/gbson/Gson;->toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Lcom/google/gbson/stream/JsonWriter;)V

    .line 314
    return-void
.end method

.method public final toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 303
    invoke-virtual {p0, p1, p2}, Lcom/google/gbson/Gson;->toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    .line 304
    invoke-virtual {p0, v0, p3}, Lcom/google/gbson/Gson;->toJson(Lcom/google/gbson/JsonElement;Ljava/lang/Appendable;)V

    .line 305
    return-void
.end method

.method public final toJsonTree(Ljava/lang/Object;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 194
    if-nez p1, :cond_0

    .line 195
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/gbson/Gson;->toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    goto :goto_0
.end method

.method public final toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 217
    new-instance v0, Lcom/google/gbson/v;

    new-instance v1, Lcom/google/gbson/ObjectNavigator;

    iget-object v2, p0, Lcom/google/gbson/Gson;->g:Lcom/google/gbson/ExclusionStrategy;

    invoke-direct {v1, v2}, Lcom/google/gbson/ObjectNavigator;-><init>(Lcom/google/gbson/ExclusionStrategy;)V

    iget-object v2, p0, Lcom/google/gbson/Gson;->h:Lcom/google/gbson/m;

    iget-boolean v3, p0, Lcom/google/gbson/Gson;->l:Z

    iget-object v4, p0, Lcom/google/gbson/Gson;->j:Lcom/google/gbson/aj;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/gbson/v;-><init>(Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;ZLcom/google/gbson/aj;)V

    .line 220
    invoke-virtual {v0, p1, p2}, Lcom/google/gbson/v;->serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 557
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{serializeNulls:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/gbson/Gson;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gbson/Gson;->j:Lcom/google/gbson/aj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",deserializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gbson/Gson;->k:Lcom/google/gbson/aj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",instanceCreators:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gbson/Gson;->i:Lcom/google/gbson/ac;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 567
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
