.class public final Lcom/google/gbson/GsonBuilder;
.super Ljava/lang/Object;
.source "GBFile"


# static fields
.field private static final a:Lcom/google/gbson/aa;

.field private static final b:Lcom/google/gbson/o;

.field private static final c:Lcom/google/gbson/k;

.field private static final d:Lcom/google/gbson/l;


# instance fields
.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/gbson/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/gbson/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private g:D

.field private h:Lcom/google/gbson/ae;

.field private i:Z

.field private j:Z

.field private k:Lcom/google/gbson/LongSerializationPolicy;

.field private l:Lcom/google/gbson/m;

.field private final m:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final o:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/gbson/aa;

    invoke-direct {v0}, Lcom/google/gbson/aa;-><init>()V

    sput-object v0, Lcom/google/gbson/GsonBuilder;->a:Lcom/google/gbson/aa;

    .line 69
    new-instance v0, Lcom/google/gbson/o;

    invoke-direct {v0}, Lcom/google/gbson/o;-><init>()V

    sput-object v0, Lcom/google/gbson/GsonBuilder;->b:Lcom/google/gbson/o;

    .line 72
    new-instance v0, Lcom/google/gbson/k;

    invoke-direct {v0}, Lcom/google/gbson/k;-><init>()V

    sput-object v0, Lcom/google/gbson/GsonBuilder;->c:Lcom/google/gbson/k;

    .line 75
    new-instance v0, Lcom/google/gbson/l;

    invoke-direct {v0}, Lcom/google/gbson/l;-><init>()V

    sput-object v0, Lcom/google/gbson/GsonBuilder;->d:Lcom/google/gbson/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    .line 109
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    sget-object v1, Lcom/google/gbson/Gson;->a:Lcom/google/gbson/a;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    sget-object v1, Lcom/google/gbson/Gson;->b:Lcom/google/gbson/ap;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/gbson/Gson;->a:Lcom/google/gbson/a;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/gbson/Gson;->b:Lcom/google/gbson/ap;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    const-wide/high16 v0, -0x4010

    iput-wide v0, p0, Lcom/google/gbson/GsonBuilder;->g:D

    .line 116
    iput-boolean v3, p0, Lcom/google/gbson/GsonBuilder;->i:Z

    .line 117
    iput-boolean v2, p0, Lcom/google/gbson/GsonBuilder;->v:Z

    .line 118
    iput-boolean v3, p0, Lcom/google/gbson/GsonBuilder;->u:Z

    .line 119
    sget-object v0, Lcom/google/gbson/Gson;->c:Lcom/google/gbson/ae;

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->h:Lcom/google/gbson/ae;

    .line 120
    iput-boolean v2, p0, Lcom/google/gbson/GsonBuilder;->j:Z

    .line 121
    sget-object v0, Lcom/google/gbson/LongSerializationPolicy;->DEFAULT:Lcom/google/gbson/LongSerializationPolicy;

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->k:Lcom/google/gbson/LongSerializationPolicy;

    .line 122
    sget-object v0, Lcom/google/gbson/Gson;->d:Lcom/google/gbson/m;

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->l:Lcom/google/gbson/m;

    .line 123
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->m:Lcom/google/gbson/aj;

    .line 124
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->n:Lcom/google/gbson/aj;

    .line 125
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->o:Lcom/google/gbson/aj;

    .line 126
    iput-boolean v2, p0, Lcom/google/gbson/GsonBuilder;->p:Z

    .line 127
    iput v4, p0, Lcom/google/gbson/GsonBuilder;->r:I

    .line 128
    iput v4, p0, Lcom/google/gbson/GsonBuilder;->s:I

    .line 129
    iput-boolean v2, p0, Lcom/google/gbson/GsonBuilder;->t:Z

    .line 130
    iput-boolean v2, p0, Lcom/google/gbson/GsonBuilder;->w:Z

    .line 131
    return-void
.end method

.method private a(Lcom/google/gbson/m;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 335
    new-instance v0, Lcom/google/gbson/an;

    invoke-direct {v0, p1}, Lcom/google/gbson/an;-><init>(Lcom/google/gbson/m;)V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->l:Lcom/google/gbson/m;

    .line 337
    return-object p0
.end method

.method private a(Ljava/lang/Class;Lcom/google/gbson/InstanceCreator;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/gbson/InstanceCreator",
            "<+TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->m:Lcom/google/gbson/aj;

    invoke-virtual {v0, p1, p2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 587
    return-object p0
.end method

.method private a(Ljava/lang/Class;Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/GsonBuilder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/gbson/JsonDeserializer",
            "<TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->o:Lcom/google/gbson/aj;

    new-instance v1, Lcom/google/gbson/t;

    invoke-direct {v1, p2}, Lcom/google/gbson/t;-><init>(Lcom/google/gbson/JsonDeserializer;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 600
    return-object p0
.end method

.method private a(Ljava/lang/Class;Lcom/google/gbson/JsonSerializer;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/gbson/JsonSerializer",
            "<TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->n:Lcom/google/gbson/aj;

    invoke-virtual {v0, p1, p2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 593
    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/InstanceCreator;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/InstanceCreator",
            "<+TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->m:Lcom/google/gbson/aj;

    invoke-virtual {v0, p1, p2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 519
    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/GsonBuilder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonDeserializer",
            "<TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->o:Lcom/google/gbson/aj;

    new-instance v1, Lcom/google/gbson/t;

    invoke-direct {v1, p2}, Lcom/google/gbson/t;-><init>(Lcom/google/gbson/JsonDeserializer;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 549
    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializer;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonSerializer",
            "<TT;>;)",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->n:Lcom/google/gbson/aj;

    invoke-virtual {v0, p1, p2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 534
    return-object p0
.end method

.method private static a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/gbson/aj",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 711
    invoke-virtual {p1, p0}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    invoke-virtual {p1, p0, p2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 714
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;IILcom/google/gbson/aj;Lcom/google/gbson/aj;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 692
    const/4 v0, 0x0

    .line 693
    if-eqz p0, :cond_2

    const-string v1, ""

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 694
    new-instance v0, Lcom/google/gbson/h$h;

    invoke-direct {v0, p0}, Lcom/google/gbson/h$h;-><init>(Ljava/lang/String;)V

    .line 699
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 700
    const-class v1, Ljava/util/Date;

    invoke-static {v1, p3, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 701
    const-class v1, Ljava/util/Date;

    invoke-static {v1, p4, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 702
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, p3, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 703
    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, p4, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 704
    const-class v1, Ljava/sql/Date;

    invoke-static {v1, p3, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 705
    const-class v1, Ljava/sql/Date;

    invoke-static {v1, p4, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/aj;Ljava/lang/Object;)V

    .line 707
    :cond_1
    return-void

    .line 695
    :cond_2
    if-eq p1, v3, :cond_0

    if-eq p2, v3, :cond_0

    .line 696
    new-instance v0, Lcom/google/gbson/h$h;

    invoke-direct {v0, p1, p2}, Lcom/google/gbson/h$h;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method public final addDeserializationExclusionStrategy(Lcom/google/gbson/ExclusionStrategy;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 388
    return-object p0
.end method

.method public final addSerializationExclusionStrategy(Lcom/google/gbson/ExclusionStrategy;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 371
    return-object p0
.end method

.method public final create()Lcom/google/gbson/Gson;
    .locals 11

    .prologue
    .line 635
    new-instance v2, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 637
    new-instance v3, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    invoke-direct {v3, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 639
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->h:Lcom/google/gbson/ae;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 640
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->h:Lcom/google/gbson/ae;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 642
    iget-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->i:Z

    if-nez v0, :cond_0

    .line 643
    sget-object v0, Lcom/google/gbson/GsonBuilder;->b:Lcom/google/gbson/o;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    sget-object v0, Lcom/google/gbson/GsonBuilder;->b:Lcom/google/gbson/o;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    :cond_0
    iget-wide v0, p0, Lcom/google/gbson/GsonBuilder;->g:D

    const-wide/high16 v4, -0x4010

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_1

    .line 647
    new-instance v0, Lcom/google/gbson/aw;

    iget-wide v4, p0, Lcom/google/gbson/GsonBuilder;->g:D

    invoke-direct {v0, v4, v5}, Lcom/google/gbson/aw;-><init>(D)V

    .line 649
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    :cond_1
    iget-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->j:Z

    if-eqz v0, :cond_2

    .line 653
    sget-object v0, Lcom/google/gbson/GsonBuilder;->c:Lcom/google/gbson/k;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 654
    sget-object v0, Lcom/google/gbson/GsonBuilder;->d:Lcom/google/gbson/l;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 657
    :cond_2
    sget-object v0, Lcom/google/gbson/h;->a:Lcom/google/gbson/aj;

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v6

    .line 659
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->n:Lcom/google/gbson/aj;

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/gbson/aj;->b(Lcom/google/gbson/aj;)V

    .line 660
    sget-object v0, Lcom/google/gbson/h;->b:Lcom/google/gbson/aj;

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v7

    .line 662
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->o:Lcom/google/gbson/aj;

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/gbson/aj;->b(Lcom/google/gbson/aj;)V

    .line 663
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->q:Ljava/lang/String;

    iget v1, p0, Lcom/google/gbson/GsonBuilder;->r:I

    iget v4, p0, Lcom/google/gbson/GsonBuilder;->s:I

    invoke-static {v0, v1, v4, v6, v7}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/String;IILcom/google/gbson/aj;Lcom/google/gbson/aj;)V

    .line 666
    iget-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->t:Z

    iget-object v1, p0, Lcom/google/gbson/GsonBuilder;->k:Lcom/google/gbson/LongSerializationPolicy;

    invoke-static {v0, v1}, Lcom/google/gbson/h;->a(ZLcom/google/gbson/LongSerializationPolicy;)Lcom/google/gbson/aj;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/gbson/aj;->a(Lcom/google/gbson/aj;)V

    .line 669
    invoke-static {}, Lcom/google/gbson/h;->c()Lcom/google/gbson/aj;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/gbson/aj;->a(Lcom/google/gbson/aj;)V

    .line 671
    iget-object v0, p0, Lcom/google/gbson/GsonBuilder;->m:Lcom/google/gbson/aj;

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v0

    .line 673
    invoke-static {}, Lcom/google/gbson/h;->d()Lcom/google/gbson/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gbson/aj;->a(Lcom/google/gbson/aj;)V

    .line 675
    invoke-virtual {v6}, Lcom/google/gbson/aj;->a()V

    .line 676
    invoke-virtual {v7}, Lcom/google/gbson/aj;->a()V

    .line 677
    iget-object v1, p0, Lcom/google/gbson/GsonBuilder;->m:Lcom/google/gbson/aj;

    invoke-virtual {v1}, Lcom/google/gbson/aj;->a()V

    .line 679
    new-instance v4, Lcom/google/gbson/ac;

    invoke-direct {v4, v0}, Lcom/google/gbson/ac;-><init>(Lcom/google/gbson/aj;)V

    .line 681
    new-instance v0, Lcom/google/gbson/Gson;

    new-instance v1, Lcom/google/gbson/i;

    invoke-direct {v1, v2}, Lcom/google/gbson/i;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/gbson/i;

    invoke-direct {v2, v3}, Lcom/google/gbson/i;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/google/gbson/GsonBuilder;->l:Lcom/google/gbson/m;

    iget-boolean v5, p0, Lcom/google/gbson/GsonBuilder;->p:Z

    iget-boolean v8, p0, Lcom/google/gbson/GsonBuilder;->w:Z

    iget-boolean v9, p0, Lcom/google/gbson/GsonBuilder;->u:Z

    iget-boolean v10, p0, Lcom/google/gbson/GsonBuilder;->v:Z

    invoke-direct/range {v0 .. v10}, Lcom/google/gbson/Gson;-><init>(Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/ExclusionStrategy;Lcom/google/gbson/m;Lcom/google/gbson/ac;ZLcom/google/gbson/aj;Lcom/google/gbson/aj;ZZZ)V

    .line 686
    return-object v0
.end method

.method public final disableHtmlEscaping()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->u:Z

    .line 410
    return-object p0
.end method

.method public final disableInnerClassSerialization()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->i:Z

    .line 287
    return-object p0
.end method

.method public final enableComplexMapKeySerialization()Lcom/google/gbson/GsonBuilder;
    .locals 2

    .prologue
    .line 275
    const-class v0, Ljava/util/Map;

    sget-object v1, Lcom/google/gbson/GsonBuilder;->a:Lcom/google/gbson/aa;

    invoke-virtual {p0, v0, v1}, Lcom/google/gbson/GsonBuilder;->registerTypeHierarchyAdapter(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/gbson/GsonBuilder;

    .line 276
    return-object p0
.end method

.method public final varargs excludeFieldsWithModifiers([I)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 157
    new-instance v0, Lcom/google/gbson/ae;

    invoke-direct {v0, p1}, Lcom/google/gbson/ae;-><init>([I)V

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->h:Lcom/google/gbson/ae;

    .line 158
    return-object p0
.end method

.method public final excludeFieldsWithoutExposeAnnotation()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->j:Z

    .line 183
    return-object p0
.end method

.method public final generateNonExecutableJson()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->w:Z

    .line 172
    return-object p0
.end method

.method public final registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 491
    instance-of v0, p2, Lcom/google/gbson/JsonSerializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gbson/JsonDeserializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gbson/InstanceCreator;

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gbson/internal/$Gson$Preconditions;->checkArgument(Z)V

    .line 493
    instance-of v0, p2, Lcom/google/gbson/InstanceCreator;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 494
    check-cast v0, Lcom/google/gbson/InstanceCreator;

    invoke-direct {p0, p1, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/InstanceCreator;)Lcom/google/gbson/GsonBuilder;

    .line 496
    :cond_1
    instance-of v0, p2, Lcom/google/gbson/JsonSerializer;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 497
    check-cast v0, Lcom/google/gbson/JsonSerializer;

    invoke-direct {p0, p1, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializer;)Lcom/google/gbson/GsonBuilder;

    .line 499
    :cond_2
    instance-of v0, p2, Lcom/google/gbson/JsonDeserializer;

    if-eqz v0, :cond_3

    .line 500
    check-cast p2, Lcom/google/gbson/JsonDeserializer;

    invoke-direct {p0, p1, p2}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/GsonBuilder;

    .line 502
    :cond_3
    return-object p0

    .line 491
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final registerTypeHierarchyAdapter(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/gbson/GsonBuilder;"
        }
    .end annotation

    .prologue
    .line 570
    instance-of v0, p2, Lcom/google/gbson/JsonSerializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gbson/JsonDeserializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gbson/InstanceCreator;

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gbson/internal/$Gson$Preconditions;->checkArgument(Z)V

    .line 572
    instance-of v0, p2, Lcom/google/gbson/InstanceCreator;

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 573
    check-cast v0, Lcom/google/gbson/InstanceCreator;

    invoke-direct {p0, p1, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/InstanceCreator;)Lcom/google/gbson/GsonBuilder;

    .line 575
    :cond_1
    instance-of v0, p2, Lcom/google/gbson/JsonSerializer;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 576
    check-cast v0, Lcom/google/gbson/JsonSerializer;

    invoke-direct {p0, p1, v0}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/JsonSerializer;)Lcom/google/gbson/GsonBuilder;

    .line 578
    :cond_2
    instance-of v0, p2, Lcom/google/gbson/JsonDeserializer;

    if-eqz v0, :cond_3

    .line 579
    check-cast p2, Lcom/google/gbson/JsonDeserializer;

    invoke-direct {p0, p1, p2}, Lcom/google/gbson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/GsonBuilder;

    .line 581
    :cond_3
    return-object p0

    .line 570
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final serializeNulls()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->p:Z

    .line 195
    return-object p0
.end method

.method public final serializeSpecialFloatingPointValues()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 624
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->t:Z

    .line 625
    return-object p0
.end method

.method public final setDateFormat(I)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 450
    iput p1, p0, Lcom/google/gbson/GsonBuilder;->r:I

    .line 451
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->q:Ljava/lang/String;

    .line 452
    return-object p0
.end method

.method public final setDateFormat(II)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 471
    iput p1, p0, Lcom/google/gbson/GsonBuilder;->r:I

    .line 472
    iput p2, p0, Lcom/google/gbson/GsonBuilder;->s:I

    .line 473
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gbson/GsonBuilder;->q:Ljava/lang/String;

    .line 474
    return-object p0
.end method

.method public final setDateFormat(Ljava/lang/String;)Lcom/google/gbson/GsonBuilder;
    .locals 0
    .parameter

    .prologue
    .line 431
    iput-object p1, p0, Lcom/google/gbson/GsonBuilder;->q:Ljava/lang/String;

    .line 432
    return-object p0
.end method

.method public final varargs setExclusionStrategies([Lcom/google/gbson/ExclusionStrategy;)Lcom/google/gbson/GsonBuilder;
    .locals 2
    .parameter

    .prologue
    .line 351
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/google/gbson/GsonBuilder;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 353
    iget-object v1, p0, Lcom/google/gbson/GsonBuilder;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 354
    return-object p0
.end method

.method public final setFieldNamingPolicy(Lcom/google/gbson/FieldNamingPolicy;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 312
    invoke-virtual {p1}, Lcom/google/gbson/FieldNamingPolicy;->a()Lcom/google/gbson/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/gbson/GsonBuilder;->a(Lcom/google/gbson/m;)Lcom/google/gbson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final setFieldNamingStrategy(Lcom/google/gbson/FieldNamingStrategy;)Lcom/google/gbson/GsonBuilder;
    .locals 1
    .parameter

    .prologue
    .line 324
    new-instance v0, Lcom/google/gbson/n;

    invoke-direct {v0, p1}, Lcom/google/gbson/n;-><init>(Lcom/google/gbson/FieldNamingStrategy;)V

    invoke-direct {p0, v0}, Lcom/google/gbson/GsonBuilder;->a(Lcom/google/gbson/m;)Lcom/google/gbson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final setLongSerializationPolicy(Lcom/google/gbson/LongSerializationPolicy;)Lcom/google/gbson/GsonBuilder;
    .locals 0
    .parameter

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/gbson/GsonBuilder;->k:Lcom/google/gbson/LongSerializationPolicy;

    .line 300
    return-object p0
.end method

.method public final setPrettyPrinting()Lcom/google/gbson/GsonBuilder;
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/GsonBuilder;->v:Z

    .line 398
    return-object p0
.end method

.method public final setVersion(D)Lcom/google/gbson/GsonBuilder;
    .locals 0
    .parameter

    .prologue
    .line 141
    iput-wide p1, p0, Lcom/google/gbson/GsonBuilder;->g:D

    .line 142
    return-object p0
.end method
