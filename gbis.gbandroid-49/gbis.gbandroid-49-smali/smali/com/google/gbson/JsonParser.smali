.class public final Lcom/google/gbson/JsonParser;
.super Ljava/lang/Object;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final parse(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    .locals 5
    .parameter

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/google/gbson/stream/JsonReader;->isLenient()Z

    move-result v1

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    .line 83
    :try_start_0
    invoke-static {p1}, Lcom/google/gbson/ao;->a(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/gbson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 94
    invoke-virtual {p1, v1}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    :goto_0
    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    :try_start_1
    new-instance v2, Lcom/google/gbson/JsonParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed parsing JSON source: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to Json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/gbson/JsonParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    throw v0

    .line 86
    :catch_1
    move-exception v0

    .line 87
    :try_start_2
    new-instance v2, Lcom/google/gbson/JsonParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed parsing JSON source: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to Json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/gbson/JsonParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 88
    :catch_2
    move-exception v0

    .line 89
    invoke-virtual {v0}, Lcom/google/gbson/JsonParseException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Ljava/io/EOFException;

    if-eqz v2, :cond_0

    .line 90
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 94
    invoke-virtual {p1, v1}, Lcom/google/gbson/stream/JsonReader;->setLenient(Z)V

    goto :goto_0

    .line 92
    :cond_0
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final parse(Ljava/io/Reader;)Lcom/google/gbson/JsonElement;
    .locals 3
    .parameter

    .prologue
    .line 57
    :try_start_0
    new-instance v0, Lcom/google/gbson/stream/JsonReader;

    invoke-direct {v0, p1}, Lcom/google/gbson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/google/gbson/JsonParser;->parse(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;

    move-result-object v1

    .line 59
    invoke-virtual {v1}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/google/gbson/stream/JsonReader;->peek()Lcom/google/gbson/stream/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/google/gbson/stream/JsonToken;->END_DOCUMENT:Lcom/google/gbson/stream/JsonToken;

    if-eq v0, v2, :cond_0

    .line 60
    new-instance v0, Lcom/google/gbson/JsonSyntaxException;

    const-string v1, "Did not consume the entire document."

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/gbson/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 63
    :catch_0
    move-exception v0

    .line 64
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 65
    :catch_1
    move-exception v0

    .line 66
    new-instance v1, Lcom/google/gbson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 67
    :catch_2
    move-exception v0

    .line 68
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 62
    :cond_0
    return-object v1
.end method

.method public final parse(Ljava/lang/String;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 44
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/gbson/JsonParser;->parse(Ljava/io/Reader;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method
