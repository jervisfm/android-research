.class final Lcom/google/gbson/aa;
.super Lcom/google/gbson/b;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gbson/b;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/util/Map",
        "<**>;>;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/util/Map",
        "<**>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/gbson/b;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonSerializationContext;",
            ")",
            "Lcom/google/gbson/JsonElement;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 122
    invoke-static {p1}, Lcom/google/gbson/aa;->a(Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v5

    .line 124
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 125
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 126
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    aget-object v8, v5, v2

    invoke-static {p2, v3, v8}, Lcom/google/gbson/aa;->a(Lcom/google/gbson/JsonSerializationContext;Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v8

    .line 127
    invoke-virtual {v8}, Lcom/google/gbson/JsonElement;->isJsonObject()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v8}, Lcom/google/gbson/JsonElement;->isJsonArray()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v3, v4

    :goto_1
    or-int/2addr v1, v3

    .line 128
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aget-object v3, v5, v4

    invoke-static {p2, v0, v3}, Lcom/google/gbson/aa;->a(Lcom/google/gbson/JsonSerializationContext;Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v3, v2

    .line 127
    goto :goto_1

    .line 132
    :cond_2
    if-eqz v1, :cond_4

    .line 133
    new-instance v1, Lcom/google/gbson/JsonArray;

    invoke-direct {v1}, Lcom/google/gbson/JsonArray;-><init>()V

    .line 134
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 135
    new-instance v3, Lcom/google/gbson/JsonArray;

    invoke-direct {v3}, Lcom/google/gbson/JsonArray;-><init>()V

    .line 136
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    invoke-virtual {v3, v0}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    .line 137
    add-int/lit8 v0, v2, 0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    invoke-virtual {v3, v0}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    .line 138
    invoke-virtual {v1, v3}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    .line 134
    add-int/lit8 v2, v2, 0x2

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 147
    :goto_3
    return-object v0

    .line 142
    :cond_4
    new-instance v1, Lcom/google/gbson/JsonObject;

    invoke-direct {v1}, Lcom/google/gbson/JsonObject;-><init>()V

    .line 143
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 144
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    invoke-virtual {v0}, Lcom/google/gbson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v2, 0x1

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    invoke-virtual {v1, v3, v0}, Lcom/google/gbson/JsonObject;->add(Ljava/lang/String;Lcom/google/gbson/JsonElement;)V

    .line 143
    add-int/lit8 v2, v2, 0x2

    goto :goto_4

    .line 146
    :cond_5
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {v1}, Lcom/google/gbson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/gbson/aa;->a(Ljava/lang/Object;ILjava/lang/Object;I)V

    move-object v0, v1

    .line 147
    goto :goto_3
.end method

.method private static a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;
    .locals 9
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/JsonElement;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")",
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 98
    invoke-static {p1, p2}, Lcom/google/gbson/aa;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;

    move-result-object v3

    .line 99
    invoke-static {p1}, Lcom/google/gbson/aa;->a(Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;

    move-result-object v4

    .line 100
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v1

    move v0, v2

    .line 102
    :goto_0
    invoke-virtual {v1}, Lcom/google/gbson/JsonArray;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 103
    invoke-virtual {v1, v0}, Lcom/google/gbson/JsonArray;->get(I)Lcom/google/gbson/JsonElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/gbson/JsonElement;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v5

    .line 104
    invoke-virtual {v5, v2}, Lcom/google/gbson/JsonArray;->get(I)Lcom/google/gbson/JsonElement;

    move-result-object v6

    aget-object v7, v4, v2

    invoke-interface {p2, v6, v7}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v6

    .line 105
    invoke-virtual {v5, v8}, Lcom/google/gbson/JsonArray;->get(I)Lcom/google/gbson/JsonElement;

    move-result-object v5

    aget-object v7, v4, v8

    invoke-interface {p2, v5, v7}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v5

    .line 106
    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {v1}, Lcom/google/gbson/JsonArray;->size()I

    move-result v0

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v1, v0, v3, v2}, Lcom/google/gbson/aa;->a(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 118
    :goto_1
    return-object v3

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonObject()Lcom/google/gbson/JsonObject;

    move-result-object v5

    .line 111
    invoke-virtual {v5}, Lcom/google/gbson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 112
    new-instance v7, Lcom/google/gbson/JsonPrimitive;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v7, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    aget-object v1, v4, v2

    invoke-interface {p2, v7, v1}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 113
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    aget-object v7, v4, v8

    invoke-interface {p2, v0, v7}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 114
    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 116
    :cond_2
    invoke-virtual {v5}, Lcom/google/gbson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v5, v0, v3, v1}, Lcom/google/gbson/aa;->a(Ljava/lang/Object;ILjava/lang/Object;I)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;ILjava/lang/Object;I)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 163
    if-eq p1, p3, :cond_0

    .line 164
    new-instance v0, Lcom/google/gbson/JsonSyntaxException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Input size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != output size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for input "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and output "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 152
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 153
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    .line 154
    array-length v1, v0

    if-eq v1, v2, :cond_1

    .line 155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MapAsArrayTypeAdapter cannot handle "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    new-array v0, v2, [Ljava/lang/reflect/Type;

    const/4 v1, 0x0

    const-class v2, Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Ljava/lang/Object;

    aput-object v2, v0, v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-static {p1, p2, p3}, Lcom/google/gbson/aa;->a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    check-cast p1, Ljava/util/Map;

    invoke-static {p1, p2, p3}, Lcom/google/gbson/aa;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method
