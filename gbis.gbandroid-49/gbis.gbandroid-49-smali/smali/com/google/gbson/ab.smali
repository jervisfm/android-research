.class final Lcom/google/gbson/ab;
.super Lcom/google/gbson/b;
.source "GBFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/gbson/b;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    new-instance v3, Lcom/google/gbson/JsonObject;

    invoke-direct {v3}, Lcom/google/gbson/JsonObject;-><init>()V

    .line 38
    const/4 v0, 0x0

    .line 39
    instance-of v1, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v1, :cond_3

    .line 40
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Types;->getRawType(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 41
    invoke-static {p1, v0}, Lcom/google/gbson/internal/$Gson$Types;->getMapKeyAndValueTypes(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    move-object v1, v0

    .line 44
    :goto_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 45
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 48
    if-nez v5, :cond_0

    .line 49
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v2

    .line 55
    :goto_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Lcom/google/gbson/JsonObject;->add(Ljava/lang/String;Lcom/google/gbson/JsonElement;)V

    goto :goto_1

    .line 51
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 53
    :goto_3
    invoke-static {p2, v5, v2}, Lcom/google/gbson/ab;->a(Lcom/google/gbson/JsonSerializationContext;Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v2

    goto :goto_2

    :cond_1
    move-object v2, v1

    .line 51
    goto :goto_3

    .line 57
    :cond_2
    return-object v3

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-static {p1, p2}, Lcom/google/gbson/ab;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;

    move-result-object v2

    .line 65
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Types;->getRawType(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/gbson/internal/$Gson$Types;->getMapKeyAndValueTypes(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v3

    .line 66
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonObject()Lcom/google/gbson/JsonObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 67
    new-instance v5, Lcom/google/gbson/JsonPrimitive;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v5, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    aget-object v1, v3, v1

    invoke-interface {p2, v5, v1}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    .line 68
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-interface {p2, v0, v5}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 69
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 71
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-static {p1, p2, p3}, Lcom/google/gbson/ab;->a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    check-cast p1, Ljava/util/Map;

    invoke-static {p1, p2, p3}, Lcom/google/gbson/ab;->a(Ljava/util/Map;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/google/gbson/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
