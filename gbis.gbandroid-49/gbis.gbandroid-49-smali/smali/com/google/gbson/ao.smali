.class final Lcom/google/gbson/ao;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gbson/ao$1;,
        Lcom/google/gbson/ao$a;
    }
.end annotation


# direct methods
.method static a(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 36
    const/4 v1, 0x1

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->peek()Lcom/google/gbson/stream/JsonToken;

    .line 39
    const/4 v1, 0x0

    .line 40
    invoke-static {p0}, Lcom/google/gbson/ao;->b(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gbson/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 46
    if-eqz v1, :cond_0

    .line 47
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_0
    new-instance v1, Lcom/google/gbson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 50
    :catch_1
    move-exception v0

    .line 51
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 52
    :catch_2
    move-exception v0

    .line 53
    new-instance v1, Lcom/google/gbson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 54
    :catch_3
    move-exception v0

    .line 55
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a(Ljava/lang/Appendable;)Ljava/io/Writer;
    .locals 2
    .parameter

    .prologue
    .line 146
    instance-of v0, p0, Ljava/io/Writer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/Writer;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/gbson/ao$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/gbson/ao$a;-><init>(Ljava/lang/Appendable;B)V

    move-object p0, v0

    goto :goto_0
.end method

.method static a(Lcom/google/gbson/JsonElement;ZLcom/google/gbson/stream/JsonWriter;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    :cond_0
    if-eqz p1, :cond_1

    .line 103
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->nullValue()Lcom/google/gbson/stream/JsonWriter;

    .line 143
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonPrimitive()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 107
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonPrimitive()Lcom/google/gbson/JsonPrimitive;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/google/gbson/JsonPrimitive;->isNumber()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109
    invoke-virtual {v0}, Lcom/google/gbson/JsonPrimitive;->getAsNumber()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/gbson/stream/JsonWriter;

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {v0}, Lcom/google/gbson/JsonPrimitive;->isBoolean()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 111
    invoke-virtual {v0}, Lcom/google/gbson/JsonPrimitive;->getAsBoolean()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->value(Z)Lcom/google/gbson/stream/JsonWriter;

    goto :goto_0

    .line 113
    :cond_4
    invoke-virtual {v0}, Lcom/google/gbson/JsonPrimitive;->getAsString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->value(Ljava/lang/String;)Lcom/google/gbson/stream/JsonWriter;

    goto :goto_0

    .line 116
    :cond_5
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonArray()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 117
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->beginArray()Lcom/google/gbson/stream/JsonWriter;

    .line 118
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    .line 120
    invoke-virtual {v0}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 121
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->nullValue()Lcom/google/gbson/stream/JsonWriter;

    goto :goto_1

    .line 124
    :cond_6
    invoke-static {v0, p1, p2}, Lcom/google/gbson/ao;->a(Lcom/google/gbson/JsonElement;ZLcom/google/gbson/stream/JsonWriter;)V

    goto :goto_1

    .line 126
    :cond_7
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->endArray()Lcom/google/gbson/stream/JsonWriter;

    goto :goto_0

    .line 128
    :cond_8
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonObject()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 129
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->beginObject()Lcom/google/gbson/stream/JsonWriter;

    .line 130
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonObject()Lcom/google/gbson/JsonObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/JsonObject;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 131
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gbson/JsonElement;

    .line 132
    if-nez p1, :cond_a

    invoke-virtual {v1}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v3

    if-nez v3, :cond_9

    .line 133
    :cond_a
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/google/gbson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gbson/stream/JsonWriter;

    .line 136
    invoke-static {v1, p1, p2}, Lcom/google/gbson/ao;->a(Lcom/google/gbson/JsonElement;ZLcom/google/gbson/stream/JsonWriter;)V

    goto :goto_2

    .line 138
    :cond_b
    invoke-virtual {p2}, Lcom/google/gbson/stream/JsonWriter;->endObject()Lcom/google/gbson/stream/JsonWriter;

    goto/16 :goto_0

    .line 141
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t write "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    .locals 3
    .parameter

    .prologue
    .line 60
    sget-object v0, Lcom/google/gbson/ao$1;->a:[I

    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->peek()Lcom/google/gbson/stream/JsonToken;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gbson/stream/JsonToken;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 62
    :pswitch_0
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    .line 86
    :goto_0
    return-object v0

    .line 64
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    .line 65
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-static {v1}, Lcom/google/gbson/JsonPrimitive;->a(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    goto :goto_0

    .line 67
    :pswitch_2
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->nextBoolean()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 69
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->nextNull()V

    .line 70
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    goto :goto_0

    .line 72
    :pswitch_4
    new-instance v0, Lcom/google/gbson/JsonArray;

    invoke-direct {v0}, Lcom/google/gbson/JsonArray;-><init>()V

    .line 73
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->beginArray()V

    .line 74
    :goto_1
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-static {p0}, Lcom/google/gbson/ao;->b(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    goto :goto_1

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->endArray()V

    goto :goto_0

    .line 80
    :pswitch_5
    new-instance v0, Lcom/google/gbson/JsonObject;

    invoke-direct {v0}, Lcom/google/gbson/JsonObject;-><init>()V

    .line 81
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->beginObject()V

    .line 82
    :goto_2
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/gbson/ao;->b(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->add(Ljava/lang/String;Lcom/google/gbson/JsonElement;)V

    goto :goto_2

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/google/gbson/stream/JsonReader;->endObject()V

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
