.class final Lcom/google/gbson/aw;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/ExclusionStrategy;


# instance fields
.field private final a:D


# direct methods
.method constructor <init>(D)V
    .locals 2
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gbson/internal/$Gson$Preconditions;->checkArgument(Z)V

    .line 34
    iput-wide p1, p0, Lcom/google/gbson/aw;->a:D

    .line 35
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/gbson/annotations/Since;)Z
    .locals 4
    .parameter

    .prologue
    .line 50
    if-eqz p1, :cond_0

    .line 51
    invoke-interface {p1}, Lcom/google/gbson/annotations/Since;->value()D

    move-result-wide v0

    .line 52
    iget-wide v2, p0, Lcom/google/gbson/aw;->a:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 56
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/gbson/annotations/Since;Lcom/google/gbson/annotations/Until;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/gbson/aw;->a(Lcom/google/gbson/annotations/Since;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/gbson/aw;->a(Lcom/google/gbson/annotations/Until;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/gbson/annotations/Until;)Z
    .locals 4
    .parameter

    .prologue
    .line 60
    if-eqz p1, :cond_0

    .line 61
    invoke-interface {p1}, Lcom/google/gbson/annotations/Until;->value()D

    move-result-wide v0

    .line 62
    iget-wide v2, p0, Lcom/google/gbson/aw;->a:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final shouldSkipClass(Ljava/lang/Class;)Z
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 42
    const-class v0, Lcom/google/gbson/annotations/Since;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/annotations/Since;

    const-class v1, Lcom/google/gbson/annotations/Until;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/gbson/annotations/Until;

    invoke-direct {p0, v0, v1}, Lcom/google/gbson/aw;->a(Lcom/google/gbson/annotations/Since;Lcom/google/gbson/annotations/Until;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final shouldSkipField(Lcom/google/gbson/FieldAttributes;)Z
    .locals 2
    .parameter

    .prologue
    .line 38
    const-class v0, Lcom/google/gbson/annotations/Since;

    invoke-virtual {p1, v0}, Lcom/google/gbson/FieldAttributes;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/annotations/Since;

    const-class v1, Lcom/google/gbson/annotations/Until;

    invoke-virtual {p1, v1}, Lcom/google/gbson/FieldAttributes;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/gbson/annotations/Until;

    invoke-direct {p0, v0, v1}, Lcom/google/gbson/aw;->a(Lcom/google/gbson/annotations/Since;Lcom/google/gbson/annotations/Until;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
