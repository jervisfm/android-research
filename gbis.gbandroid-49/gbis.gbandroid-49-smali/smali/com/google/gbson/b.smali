.class abstract Lcom/google/gbson/b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/util/Map",
        "<**>;>;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/util/Map",
        "<**>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static final a(Lcom/google/gbson/JsonSerializationContext;Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    check-cast p0, Lcom/google/gbson/v;

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/gbson/v;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method protected static final a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Map;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    check-cast p1, Lcom/google/gbson/r;

    .line 41
    invoke-virtual {p1}, Lcom/google/gbson/r;->a()Lcom/google/gbson/ag;

    move-result-object v0

    .line 42
    invoke-interface {v0, p0}, Lcom/google/gbson/ag;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
