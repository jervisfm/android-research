.class final Lcom/google/gbson/h$aa;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "aa"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 960
    invoke-direct {p0}, Lcom/google/gbson/h$aa;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 963
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-direct {v0, p0}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 968
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 960
    invoke-static {p1}, Lcom/google/gbson/h$aa;->a(Lcom/google/gbson/JsonElement;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 960
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/gbson/h$aa;->a(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 973
    const-class v0, Lcom/google/gbson/h$aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
