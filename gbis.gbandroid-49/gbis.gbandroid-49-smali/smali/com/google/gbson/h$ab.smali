.class final Lcom/google/gbson/h$ab;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ab"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/net/URI;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/net/URI;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/google/gbson/h$ab;-><init>()V

    return-void
.end method

.method private static a(Ljava/net/URI;)Lcom/google/gbson/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 555
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-virtual {p0}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/net/URI;
    .locals 2
    .parameter

    .prologue
    .line 560
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 561
    :catch_0
    move-exception v0

    .line 562
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 553
    invoke-static {p1}, Lcom/google/gbson/h$ab;->a(Lcom/google/gbson/JsonElement;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 553
    check-cast p1, Ljava/net/URI;

    invoke-static {p1}, Lcom/google/gbson/h$ab;->a(Ljava/net/URI;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 567
    const-class v0, Lcom/google/gbson/h$ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
