.class final Lcom/google/gbson/h$c;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 1011
    invoke-direct {p0}, Lcom/google/gbson/h$c;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Boolean;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 1014
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-direct {v0, p0}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/lang/Boolean;
    .locals 2
    .parameter

    .prologue
    .line 1020
    :try_start_0
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsBoolean()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 1021
    :catch_0
    move-exception v0

    .line 1022
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1023
    :catch_1
    move-exception v0

    .line 1024
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1011
    invoke-static {p1}, Lcom/google/gbson/h$c;->a(Lcom/google/gbson/JsonElement;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1011
    check-cast p1, Ljava/lang/Boolean;

    invoke-static {p1}, Lcom/google/gbson/h$c;->a(Ljava/lang/Boolean;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1030
    const-class v0, Lcom/google/gbson/h$c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
