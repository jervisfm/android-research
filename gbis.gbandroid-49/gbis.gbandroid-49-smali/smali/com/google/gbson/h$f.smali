.class final Lcom/google/gbson/h$f;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/util/Collection;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/util/Collection;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 625
    invoke-direct {p0}, Lcom/google/gbson/h$f;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Collection;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 628
    if-nez p0, :cond_0

    .line 629
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    .line 647
    :goto_0
    return-object v0

    .line 631
    :cond_0
    new-instance v2, Lcom/google/gbson/JsonArray;

    invoke-direct {v2}, Lcom/google/gbson/JsonArray;-><init>()V

    .line 632
    const/4 v0, 0x0

    .line 633
    instance-of v1, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v1, :cond_1

    .line 634
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Types;->getRawType(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 635
    invoke-static {p1, v0}, Lcom/google/gbson/internal/$Gson$Types;->getCollectionElementType(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 637
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 638
    if-nez v4, :cond_2

    .line 639
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    goto :goto_1

    .line 641
    :cond_2
    if-eqz v0, :cond_3

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 643
    :goto_2
    invoke-interface {p2, v4, v1}, Lcom/google/gbson/JsonSerializationContext;->serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;

    move-result-object v1

    .line 644
    invoke-virtual {v2, v1}, Lcom/google/gbson/JsonArray;->add(Lcom/google/gbson/JsonElement;)V

    goto :goto_1

    :cond_4
    move-object v1, v0

    .line 641
    goto :goto_2

    :cond_5
    move-object v0, v2

    .line 647
    goto :goto_0
.end method

.method private static a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Collection;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 652
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 667
    :goto_0
    return-object v0

    .line 657
    :cond_0
    invoke-static {p1, p2}, Lcom/google/gbson/h$f;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Collection;

    move-result-object v2

    .line 658
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Types;->getRawType(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/gbson/internal/$Gson$Types;->getCollectionElementType(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 659
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    .line 660
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 661
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 663
    :cond_2
    invoke-interface {p2, v0, v3}, Lcom/google/gbson/JsonDeserializationContext;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 664
    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 667
    goto :goto_0
.end method

.method private static a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Collection;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 672
    check-cast p1, Lcom/google/gbson/r;

    .line 673
    invoke-virtual {p1}, Lcom/google/gbson/r;->a()Lcom/google/gbson/ag;

    move-result-object v0

    .line 674
    invoke-interface {v0, p0}, Lcom/google/gbson/ag;->a(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 624
    invoke-static {p1, p2, p3}, Lcom/google/gbson/h$f;->a(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 624
    check-cast p1, Ljava/util/Collection;

    invoke-static {p1, p2, p3}, Lcom/google/gbson/h$f;->a(Ljava/util/Collection;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method
