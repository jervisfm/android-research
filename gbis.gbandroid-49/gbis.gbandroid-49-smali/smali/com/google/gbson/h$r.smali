.class final Lcom/google/gbson/h$r;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "r"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/util/GregorianCalendar;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/util/GregorianCalendar;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/google/gbson/h$r;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/GregorianCalendar;)Lcom/google/gbson/JsonElement;
    .locals 3
    .parameter

    .prologue
    .line 468
    new-instance v0, Lcom/google/gbson/JsonObject;

    invoke-direct {v0}, Lcom/google/gbson/JsonObject;-><init>()V

    .line 469
    const-string v1, "year"

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 470
    const-string v1, "month"

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 471
    const-string v1, "dayOfMonth"

    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 472
    const-string v1, "hourOfDay"

    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 473
    const-string v1, "minute"

    const/16 v2, 0xc

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 474
    const-string v1, "second"

    const/16 v2, 0xd

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/Number;)V

    .line 475
    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/util/GregorianCalendar;
    .locals 7
    .parameter

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsJsonObject()Lcom/google/gbson/JsonObject;

    move-result-object v0

    .line 481
    const-string v1, "year"

    invoke-virtual {v0, v1}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v1

    .line 482
    const-string v2, "month"

    invoke-virtual {v0, v2}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v2

    .line 483
    const-string v3, "dayOfMonth"

    invoke-virtual {v0, v3}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v3

    .line 484
    const-string v4, "hourOfDay"

    invoke-virtual {v0, v4}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v4

    .line 485
    const-string v5, "minute"

    invoke-virtual {v0, v5}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v5

    .line 486
    const-string v6, "second"

    invoke-virtual {v0, v6}, Lcom/google/gbson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/JsonElement;->getAsInt()I

    move-result v6

    .line 487
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 456
    invoke-static {p1}, Lcom/google/gbson/h$r;->a(Lcom/google/gbson/JsonElement;)Ljava/util/GregorianCalendar;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 456
    check-cast p1, Ljava/util/GregorianCalendar;

    invoke-static {p1}, Lcom/google/gbson/h$r;->a(Ljava/util/GregorianCalendar;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 492
    const-class v0, Lcom/google/gbson/h$r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
