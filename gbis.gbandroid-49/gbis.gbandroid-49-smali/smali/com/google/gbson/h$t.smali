.class final Lcom/google/gbson/h$t;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "t"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/util/Locale;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/util/Locale;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 587
    invoke-direct {p0}, Lcom/google/gbson/h$t;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Locale;)Lcom/google/gbson/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 590
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/util/Locale;
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 595
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    .line 596
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v2, "_"

    invoke-direct {v3, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 601
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 603
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 604
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 606
    :goto_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 607
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 609
    :goto_2
    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 610
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 614
    :goto_3
    return-object v0

    .line 611
    :cond_0
    if-nez v3, :cond_1

    .line 612
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3

    .line 614
    :cond_1
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, v0, v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3

    :cond_2
    move-object v3, v1

    goto :goto_2

    :cond_3
    move-object v2, v1

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 587
    invoke-static {p1}, Lcom/google/gbson/h$t;->a(Lcom/google/gbson/JsonElement;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 587
    check-cast p1, Ljava/util/Locale;

    invoke-static {p1}, Lcom/google/gbson/h$t;->a(Ljava/util/Locale;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 620
    const-class v0, Lcom/google/gbson/h$t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
