.class final Lcom/google/gbson/h$v;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "v"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gbson/LongSerializationPolicy;


# direct methods
.method private constructor <init>(Lcom/google/gbson/LongSerializationPolicy;)V
    .locals 0
    .parameter

    .prologue
    .line 757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 758
    iput-object p1, p0, Lcom/google/gbson/h$v;->a:Lcom/google/gbson/LongSerializationPolicy;

    .line 759
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/gbson/LongSerializationPolicy;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 754
    invoke-direct {p0, p1}, Lcom/google/gbson/h$v;-><init>(Lcom/google/gbson/LongSerializationPolicy;)V

    return-void
.end method

.method private a(Ljava/lang/Long;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 762
    iget-object v0, p0, Lcom/google/gbson/h$v;->a:Lcom/google/gbson/LongSerializationPolicy;

    invoke-virtual {v0, p1}, Lcom/google/gbson/LongSerializationPolicy;->serialize(Ljava/lang/Long;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 754
    check-cast p1, Ljava/lang/Long;

    invoke-direct {p0, p1}, Lcom/google/gbson/h$v;->a(Ljava/lang/Long;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 767
    const-class v0, Lcom/google/gbson/h$v;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
