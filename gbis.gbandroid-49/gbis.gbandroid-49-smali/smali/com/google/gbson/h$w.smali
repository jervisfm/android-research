.class final Lcom/google/gbson/h$w;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "w"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/lang/Number;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/google/gbson/h$w;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Number;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter

    .prologue
    .line 732
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-direct {v0, p0}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/lang/Number;
    .locals 2
    .parameter

    .prologue
    .line 738
    :try_start_0
    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsNumber()Ljava/lang/Number;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 739
    :catch_0
    move-exception v0

    .line 740
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 741
    :catch_1
    move-exception v0

    .line 742
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 743
    :catch_2
    move-exception v0

    .line 744
    new-instance v1, Lcom/google/gbson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gbson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 729
    invoke-static {p1}, Lcom/google/gbson/h$w;->a(Lcom/google/gbson/JsonElement;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 729
    check-cast p1, Ljava/lang/Number;

    invoke-static {p1}, Lcom/google/gbson/h$w;->a(Ljava/lang/Number;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 750
    const-class v0, Lcom/google/gbson/h$w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
