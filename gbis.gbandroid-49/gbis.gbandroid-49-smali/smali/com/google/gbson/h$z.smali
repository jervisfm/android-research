.class final Lcom/google/gbson/h$z;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializer;
.implements Lcom/google/gbson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gbson/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "z"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gbson/JsonDeserializer",
        "<",
        "Ljava/lang/StringBuilder;",
        ">;",
        "Lcom/google/gbson/JsonSerializer",
        "<",
        "Ljava/lang/StringBuilder;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0
    .parameter

    .prologue
    .line 977
    invoke-direct {p0}, Lcom/google/gbson/h$z;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;)Lcom/google/gbson/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 980
    new-instance v0, Lcom/google/gbson/JsonPrimitive;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Lcom/google/gbson/JsonElement;)Ljava/lang/StringBuilder;
    .locals 2
    .parameter

    .prologue
    .line 985
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/gbson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 977
    invoke-static {p1}, Lcom/google/gbson/h$z;->a(Lcom/google/gbson/JsonElement;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonSerializationContext;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 977
    check-cast p1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/gbson/h$z;->a(Ljava/lang/StringBuilder;)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 990
    const-class v0, Lcom/google/gbson/h$z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
