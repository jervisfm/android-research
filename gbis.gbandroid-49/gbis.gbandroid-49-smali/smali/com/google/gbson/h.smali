.class final Lcom/google/gbson/h;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gbson/h$g;,
        Lcom/google/gbson/h$c;,
        Lcom/google/gbson/h$y;,
        Lcom/google/gbson/h$z;,
        Lcom/google/gbson/h$aa;,
        Lcom/google/gbson/h$e;,
        Lcom/google/gbson/h$m;,
        Lcom/google/gbson/h$n;,
        Lcom/google/gbson/h$p;,
        Lcom/google/gbson/h$q;,
        Lcom/google/gbson/h$d;,
        Lcom/google/gbson/h$x;,
        Lcom/google/gbson/h$s;,
        Lcom/google/gbson/h$u;,
        Lcom/google/gbson/h$v;,
        Lcom/google/gbson/h$w;,
        Lcom/google/gbson/h$b;,
        Lcom/google/gbson/h$a;,
        Lcom/google/gbson/h$f;,
        Lcom/google/gbson/h$t;,
        Lcom/google/gbson/h$ad;,
        Lcom/google/gbson/h$ab;,
        Lcom/google/gbson/h$ac;,
        Lcom/google/gbson/h$o;,
        Lcom/google/gbson/h$i;,
        Lcom/google/gbson/h$r;,
        Lcom/google/gbson/h$k;,
        Lcom/google/gbson/h$l;,
        Lcom/google/gbson/h$j;,
        Lcom/google/gbson/h$h;
    }
.end annotation


# static fields
.field private static final A:Lcom/google/gbson/h$z;

.field private static final B:Lcom/google/gbson/h$y;

.field private static final C:Lcom/google/gbson/h$r;

.field private static final D:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final E:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final F:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field

.field static final a:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final c:Lcom/google/gbson/h$h;

.field private static final d:Lcom/google/gbson/h$j;

.field private static final e:Lcom/google/gbson/h$k;

.field private static final f:Lcom/google/gbson/h$l;

.field private static final g:Lcom/google/gbson/h$o;

.field private static final h:Lcom/google/gbson/h$ac;

.field private static final i:Lcom/google/gbson/h$ab;

.field private static final j:Lcom/google/gbson/h$ad;

.field private static final k:Lcom/google/gbson/h$t;

.field private static final l:Lcom/google/gbson/h$i;

.field private static final m:Lcom/google/gbson/h$f;

.field private static final n:Lcom/google/gbson/ab;

.field private static final o:Lcom/google/gbson/h$a;

.field private static final p:Lcom/google/gbson/h$b;

.field private static final q:Lcom/google/gbson/h$c;

.field private static final r:Lcom/google/gbson/h$d;

.field private static final s:Lcom/google/gbson/h$e;

.field private static final t:Lcom/google/gbson/h$m;

.field private static final u:Lcom/google/gbson/h$p;

.field private static final v:Lcom/google/gbson/h$s;

.field private static final w:Lcom/google/gbson/h$u;

.field private static final x:Lcom/google/gbson/h$w;

.field private static final y:Lcom/google/gbson/h$x;

.field private static final z:Lcom/google/gbson/h$aa;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    new-instance v0, Lcom/google/gbson/h$h;

    invoke-direct {v0}, Lcom/google/gbson/h$h;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->c:Lcom/google/gbson/h$h;

    .line 64
    new-instance v0, Lcom/google/gbson/h$j;

    invoke-direct {v0}, Lcom/google/gbson/h$j;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->d:Lcom/google/gbson/h$j;

    .line 66
    new-instance v0, Lcom/google/gbson/h$k;

    invoke-direct {v0}, Lcom/google/gbson/h$k;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->e:Lcom/google/gbson/h$k;

    .line 68
    new-instance v0, Lcom/google/gbson/h$l;

    invoke-direct {v0}, Lcom/google/gbson/h$l;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->f:Lcom/google/gbson/h$l;

    .line 72
    new-instance v0, Lcom/google/gbson/h$o;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$o;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->g:Lcom/google/gbson/h$o;

    .line 73
    new-instance v0, Lcom/google/gbson/h$ac;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$ac;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->h:Lcom/google/gbson/h$ac;

    .line 74
    new-instance v0, Lcom/google/gbson/h$ab;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$ab;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->i:Lcom/google/gbson/h$ab;

    .line 75
    new-instance v0, Lcom/google/gbson/h$ad;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$ad;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->j:Lcom/google/gbson/h$ad;

    .line 76
    new-instance v0, Lcom/google/gbson/h$t;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$t;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->k:Lcom/google/gbson/h$t;

    .line 77
    new-instance v0, Lcom/google/gbson/h$i;

    invoke-direct {v0}, Lcom/google/gbson/h$i;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->l:Lcom/google/gbson/h$i;

    .line 79
    new-instance v0, Lcom/google/gbson/h$f;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$f;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->m:Lcom/google/gbson/h$f;

    .line 80
    new-instance v0, Lcom/google/gbson/ab;

    invoke-direct {v0}, Lcom/google/gbson/ab;-><init>()V

    sput-object v0, Lcom/google/gbson/h;->n:Lcom/google/gbson/ab;

    .line 81
    new-instance v0, Lcom/google/gbson/h$a;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$a;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->o:Lcom/google/gbson/h$a;

    .line 82
    new-instance v0, Lcom/google/gbson/h$b;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$b;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->p:Lcom/google/gbson/h$b;

    .line 84
    new-instance v0, Lcom/google/gbson/h$c;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$c;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->q:Lcom/google/gbson/h$c;

    .line 85
    new-instance v0, Lcom/google/gbson/h$d;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$d;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->r:Lcom/google/gbson/h$d;

    .line 86
    new-instance v0, Lcom/google/gbson/h$e;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$e;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->s:Lcom/google/gbson/h$e;

    .line 87
    new-instance v0, Lcom/google/gbson/h$m;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$m;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->t:Lcom/google/gbson/h$m;

    .line 88
    new-instance v0, Lcom/google/gbson/h$p;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$p;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->u:Lcom/google/gbson/h$p;

    .line 89
    new-instance v0, Lcom/google/gbson/h$s;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$s;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->v:Lcom/google/gbson/h$s;

    .line 90
    new-instance v0, Lcom/google/gbson/h$u;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$u;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->w:Lcom/google/gbson/h$u;

    .line 91
    new-instance v0, Lcom/google/gbson/h$w;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$w;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->x:Lcom/google/gbson/h$w;

    .line 92
    new-instance v0, Lcom/google/gbson/h$x;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$x;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->y:Lcom/google/gbson/h$x;

    .line 93
    new-instance v0, Lcom/google/gbson/h$aa;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$aa;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->z:Lcom/google/gbson/h$aa;

    .line 94
    new-instance v0, Lcom/google/gbson/h$z;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$z;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->A:Lcom/google/gbson/h$z;

    .line 96
    new-instance v0, Lcom/google/gbson/h$y;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$y;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->B:Lcom/google/gbson/h$y;

    .line 99
    new-instance v0, Lcom/google/gbson/h$r;

    invoke-direct {v0, v1}, Lcom/google/gbson/h$r;-><init>(B)V

    sput-object v0, Lcom/google/gbson/h;->C:Lcom/google/gbson/h$r;

    .line 105
    invoke-static {}, Lcom/google/gbson/h;->e()Lcom/google/gbson/aj;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/h;->D:Lcom/google/gbson/aj;

    .line 107
    invoke-static {}, Lcom/google/gbson/h;->f()Lcom/google/gbson/aj;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/h;->a:Lcom/google/gbson/aj;

    .line 109
    invoke-static {}, Lcom/google/gbson/h;->g()Lcom/google/gbson/aj;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/h;->E:Lcom/google/gbson/aj;

    .line 111
    invoke-static {}, Lcom/google/gbson/h;->h()Lcom/google/gbson/aj;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/h;->b:Lcom/google/gbson/aj;

    .line 113
    invoke-static {}, Lcom/google/gbson/h;->i()Lcom/google/gbson/aj;

    move-result-object v0

    sput-object v0, Lcom/google/gbson/h;->F:Lcom/google/gbson/aj;

    return-void
.end method

.method private static a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;)",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 247
    new-instance v0, Lcom/google/gbson/t;

    invoke-direct {v0, p0}, Lcom/google/gbson/t;-><init>(Lcom/google/gbson/JsonDeserializer;)V

    return-object v0
.end method

.method static a()Lcom/google/gbson/aj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 255
    const/4 v0, 0x0

    sget-object v1, Lcom/google/gbson/LongSerializationPolicy;->DEFAULT:Lcom/google/gbson/LongSerializationPolicy;

    invoke-static {v0, v1}, Lcom/google/gbson/h;->a(ZLcom/google/gbson/LongSerializationPolicy;)Lcom/google/gbson/aj;

    move-result-object v0

    .line 257
    sget-object v1, Lcom/google/gbson/h;->a:Lcom/google/gbson/aj;

    invoke-virtual {v0, v1}, Lcom/google/gbson/aj;->b(Lcom/google/gbson/aj;)V

    .line 258
    return-object v0
.end method

.method static a(ZLcom/google/gbson/LongSerializationPolicy;)Lcom/google/gbson/aj;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/gbson/LongSerializationPolicy;",
            ")",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 270
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 274
    new-instance v1, Lcom/google/gbson/h$n;

    invoke-direct {v1, p0}, Lcom/google/gbson/h$n;-><init>(Z)V

    .line 276
    const-class v2, Ljava/lang/Double;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 277
    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 280
    new-instance v1, Lcom/google/gbson/h$q;

    invoke-direct {v1, p0}, Lcom/google/gbson/h$q;-><init>(Z)V

    .line 282
    const-class v2, Ljava/lang/Float;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 283
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 286
    new-instance v1, Lcom/google/gbson/h$v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/gbson/h$v;-><init>(Lcom/google/gbson/LongSerializationPolicy;B)V

    .line 288
    const-class v2, Ljava/lang/Long;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 289
    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v2, v1}, Lcom/google/gbson/aj;->b(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 291
    sget-object v1, Lcom/google/gbson/h;->D:Lcom/google/gbson/aj;

    invoke-virtual {v0, v1}, Lcom/google/gbson/aj;->a(Lcom/google/gbson/aj;)V

    .line 292
    return-object v0
.end method

.method static b()Lcom/google/gbson/aj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 262
    invoke-static {}, Lcom/google/gbson/h;->c()Lcom/google/gbson/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/aj;->b()Lcom/google/gbson/aj;

    move-result-object v0

    .line 264
    sget-object v1, Lcom/google/gbson/h;->b:Lcom/google/gbson/aj;

    invoke-virtual {v0, v1}, Lcom/google/gbson/aj;->b(Lcom/google/gbson/aj;)V

    .line 265
    return-object v0
.end method

.method static c()Lcom/google/gbson/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 296
    sget-object v0, Lcom/google/gbson/h;->E:Lcom/google/gbson/aj;

    return-object v0
.end method

.method static d()Lcom/google/gbson/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 300
    sget-object v0, Lcom/google/gbson/h;->F:Lcom/google/gbson/aj;

    return-object v0
.end method

.method private static e()Lcom/google/gbson/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 120
    const-class v1, Ljava/net/URL;

    sget-object v2, Lcom/google/gbson/h;->h:Lcom/google/gbson/h$ac;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 121
    const-class v1, Ljava/net/URI;

    sget-object v2, Lcom/google/gbson/h;->i:Lcom/google/gbson/h$ab;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 122
    const-class v1, Ljava/util/UUID;

    sget-object v2, Lcom/google/gbson/h;->j:Lcom/google/gbson/h$ad;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 123
    const-class v1, Ljava/util/Locale;

    sget-object v2, Lcom/google/gbson/h;->k:Lcom/google/gbson/h$t;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 124
    const-class v1, Ljava/util/Date;

    sget-object v2, Lcom/google/gbson/h;->c:Lcom/google/gbson/h$h;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 125
    const-class v1, Ljava/sql/Date;

    sget-object v2, Lcom/google/gbson/h;->d:Lcom/google/gbson/h$j;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 126
    const-class v1, Ljava/sql/Timestamp;

    sget-object v2, Lcom/google/gbson/h;->c:Lcom/google/gbson/h$h;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 127
    const-class v1, Ljava/sql/Time;

    sget-object v2, Lcom/google/gbson/h;->e:Lcom/google/gbson/h$k;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 128
    const-class v1, Ljava/util/Calendar;

    sget-object v2, Lcom/google/gbson/h;->C:Lcom/google/gbson/h$r;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 129
    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gbson/h;->C:Lcom/google/gbson/h$r;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 130
    const-class v1, Ljava/math/BigDecimal;

    sget-object v2, Lcom/google/gbson/h;->o:Lcom/google/gbson/h$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 131
    const-class v1, Ljava/math/BigInteger;

    sget-object v2, Lcom/google/gbson/h;->p:Lcom/google/gbson/h$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 134
    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gbson/h;->q:Lcom/google/gbson/h$c;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 135
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->q:Lcom/google/gbson/h$c;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 136
    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gbson/h;->r:Lcom/google/gbson/h$d;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 137
    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->r:Lcom/google/gbson/h$d;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 138
    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gbson/h;->s:Lcom/google/gbson/h$e;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 139
    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->s:Lcom/google/gbson/h$e;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 140
    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gbson/h;->v:Lcom/google/gbson/h$s;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 141
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->v:Lcom/google/gbson/h$s;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 142
    const-class v1, Ljava/lang/Number;

    sget-object v2, Lcom/google/gbson/h;->x:Lcom/google/gbson/h$w;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 143
    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gbson/h;->y:Lcom/google/gbson/h$x;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 144
    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->y:Lcom/google/gbson/h$x;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 145
    const-class v1, Ljava/lang/String;

    sget-object v2, Lcom/google/gbson/h;->z:Lcom/google/gbson/h$aa;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 146
    const-class v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/google/gbson/h;->A:Lcom/google/gbson/h$z;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 147
    const-class v1, Ljava/lang/StringBuffer;

    sget-object v2, Lcom/google/gbson/h;->B:Lcom/google/gbson/h$y;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 149
    invoke-virtual {v0}, Lcom/google/gbson/aj;->a()V

    .line 150
    return-object v0
.end method

.method private static f()Lcom/google/gbson/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 156
    const-class v1, Ljava/lang/Enum;

    sget-object v2, Lcom/google/gbson/h;->g:Lcom/google/gbson/h$o;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 157
    const-class v1, Ljava/net/InetAddress;

    sget-object v2, Lcom/google/gbson/h;->l:Lcom/google/gbson/h$i;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 158
    const-class v1, Ljava/util/Collection;

    sget-object v2, Lcom/google/gbson/h;->m:Lcom/google/gbson/h$f;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 159
    const-class v1, Ljava/util/Map;

    sget-object v2, Lcom/google/gbson/h;->n:Lcom/google/gbson/ab;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 160
    invoke-virtual {v0}, Lcom/google/gbson/aj;->a()V

    .line 161
    return-object v0
.end method

.method private static g()Lcom/google/gbson/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 167
    const-class v1, Ljava/net/URL;

    sget-object v2, Lcom/google/gbson/h;->h:Lcom/google/gbson/h$ac;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 168
    const-class v1, Ljava/net/URI;

    sget-object v2, Lcom/google/gbson/h;->i:Lcom/google/gbson/h$ab;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 169
    const-class v1, Ljava/util/UUID;

    sget-object v2, Lcom/google/gbson/h;->j:Lcom/google/gbson/h$ad;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 170
    const-class v1, Ljava/util/Locale;

    sget-object v2, Lcom/google/gbson/h;->k:Lcom/google/gbson/h$t;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 171
    const-class v1, Ljava/util/Date;

    sget-object v2, Lcom/google/gbson/h;->c:Lcom/google/gbson/h$h;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 172
    const-class v1, Ljava/sql/Date;

    sget-object v2, Lcom/google/gbson/h;->d:Lcom/google/gbson/h$j;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 173
    const-class v1, Ljava/sql/Timestamp;

    sget-object v2, Lcom/google/gbson/h;->f:Lcom/google/gbson/h$l;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 174
    const-class v1, Ljava/sql/Time;

    sget-object v2, Lcom/google/gbson/h;->e:Lcom/google/gbson/h$k;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 175
    const-class v1, Ljava/util/Calendar;

    sget-object v2, Lcom/google/gbson/h;->C:Lcom/google/gbson/h$r;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 176
    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gbson/h;->C:Lcom/google/gbson/h$r;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 177
    const-class v1, Ljava/math/BigDecimal;

    sget-object v2, Lcom/google/gbson/h;->o:Lcom/google/gbson/h$a;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 178
    const-class v1, Ljava/math/BigInteger;

    sget-object v2, Lcom/google/gbson/h;->p:Lcom/google/gbson/h$b;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 181
    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gbson/h;->q:Lcom/google/gbson/h$c;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 182
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->q:Lcom/google/gbson/h$c;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 183
    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gbson/h;->r:Lcom/google/gbson/h$d;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 184
    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->r:Lcom/google/gbson/h$d;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 185
    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gbson/h;->s:Lcom/google/gbson/h$e;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 186
    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->s:Lcom/google/gbson/h$e;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 187
    const-class v1, Ljava/lang/Double;

    sget-object v2, Lcom/google/gbson/h;->t:Lcom/google/gbson/h$m;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 188
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->t:Lcom/google/gbson/h$m;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 189
    const-class v1, Ljava/lang/Float;

    sget-object v2, Lcom/google/gbson/h;->u:Lcom/google/gbson/h$p;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 190
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->u:Lcom/google/gbson/h$p;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 191
    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gbson/h;->v:Lcom/google/gbson/h$s;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 192
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->v:Lcom/google/gbson/h$s;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 193
    const-class v1, Ljava/lang/Long;

    sget-object v2, Lcom/google/gbson/h;->w:Lcom/google/gbson/h$u;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 194
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->w:Lcom/google/gbson/h$u;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 195
    const-class v1, Ljava/lang/Number;

    sget-object v2, Lcom/google/gbson/h;->x:Lcom/google/gbson/h$w;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 196
    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gbson/h;->y:Lcom/google/gbson/h$x;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 197
    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    sget-object v2, Lcom/google/gbson/h;->y:Lcom/google/gbson/h$x;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 198
    const-class v1, Ljava/lang/String;

    sget-object v2, Lcom/google/gbson/h;->z:Lcom/google/gbson/h$aa;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 199
    const-class v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/google/gbson/h;->A:Lcom/google/gbson/h$z;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 200
    const-class v1, Ljava/lang/StringBuffer;

    sget-object v2, Lcom/google/gbson/h;->B:Lcom/google/gbson/h$y;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 202
    invoke-virtual {v0}, Lcom/google/gbson/aj;->a()V

    .line 203
    return-object v0
.end method

.method private static h()Lcom/google/gbson/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 207
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 209
    const-class v1, Ljava/lang/Enum;

    sget-object v2, Lcom/google/gbson/h;->g:Lcom/google/gbson/h$o;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 210
    const-class v1, Ljava/net/InetAddress;

    sget-object v2, Lcom/google/gbson/h;->l:Lcom/google/gbson/h$i;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 211
    const-class v1, Ljava/util/Collection;

    sget-object v2, Lcom/google/gbson/h;->m:Lcom/google/gbson/h$f;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 212
    const-class v1, Ljava/util/Map;

    sget-object v2, Lcom/google/gbson/h;->n:Lcom/google/gbson/ab;

    invoke-static {v2}, Lcom/google/gbson/h;->a(Lcom/google/gbson/JsonDeserializer;)Lcom/google/gbson/JsonDeserializer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 213
    invoke-virtual {v0}, Lcom/google/gbson/aj;->a()V

    .line 214
    return-object v0
.end method

.method private static i()Lcom/google/gbson/aj;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 219
    new-instance v0, Lcom/google/gbson/aj;

    invoke-direct {v0}, Lcom/google/gbson/aj;-><init>()V

    .line 221
    new-instance v1, Lcom/google/gbson/g;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Lcom/google/gbson/g;-><init>(I)V

    .line 224
    const-class v2, Ljava/util/Map;

    new-instance v3, Lcom/google/gbson/h$g;

    const-class v4, Ljava/util/LinkedHashMap;

    invoke-direct {v3, v4, v1}, Lcom/google/gbson/h$g;-><init>(Ljava/lang/Class;Lcom/google/gbson/g;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 228
    new-instance v2, Lcom/google/gbson/h$g;

    const-class v3, Ljava/util/ArrayList;

    invoke-direct {v2, v3, v1}, Lcom/google/gbson/h$g;-><init>(Ljava/lang/Class;Lcom/google/gbson/g;)V

    .line 230
    new-instance v3, Lcom/google/gbson/h$g;

    const-class v4, Ljava/util/LinkedList;

    invoke-direct {v3, v4, v1}, Lcom/google/gbson/h$g;-><init>(Ljava/lang/Class;Lcom/google/gbson/g;)V

    .line 232
    new-instance v4, Lcom/google/gbson/h$g;

    const-class v5, Ljava/util/HashSet;

    invoke-direct {v4, v5, v1}, Lcom/google/gbson/h$g;-><init>(Ljava/lang/Class;Lcom/google/gbson/g;)V

    .line 234
    new-instance v5, Lcom/google/gbson/h$g;

    const-class v6, Ljava/util/TreeSet;

    invoke-direct {v5, v6, v1}, Lcom/google/gbson/h$g;-><init>(Ljava/lang/Class;Lcom/google/gbson/g;)V

    .line 236
    const-class v1, Ljava/util/Collection;

    invoke-virtual {v0, v1, v2}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 237
    const-class v1, Ljava/util/Queue;

    invoke-virtual {v0, v1, v3}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 238
    const-class v1, Ljava/util/Set;

    invoke-virtual {v0, v1, v4}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 239
    const-class v1, Ljava/util/SortedSet;

    invoke-virtual {v0, v1, v5}, Lcom/google/gbson/aj;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 241
    invoke-virtual {v0}, Lcom/google/gbson/aj;->a()V

    .line 242
    return-object v0
.end method
