.class final Lcom/google/gbson/n;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/m;


# instance fields
.field private final a:Lcom/google/gbson/FieldNamingStrategy;


# direct methods
.method constructor <init>(Lcom/google/gbson/FieldNamingStrategy;)V
    .locals 1
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/FieldNamingStrategy;

    iput-object v0, p0, Lcom/google/gbson/n;->a:Lcom/google/gbson/FieldNamingStrategy;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gbson/FieldAttributes;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/gbson/n;->a:Lcom/google/gbson/FieldNamingStrategy;

    invoke-virtual {p1}, Lcom/google/gbson/FieldAttributes;->b()Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/gbson/FieldNamingStrategy;->translateName(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
