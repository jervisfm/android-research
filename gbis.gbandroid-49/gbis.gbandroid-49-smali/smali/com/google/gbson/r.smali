.class final Lcom/google/gbson/r;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonDeserializationContext;


# instance fields
.field private final a:Lcom/google/gbson/ObjectNavigator;

.field private final b:Lcom/google/gbson/m;

.field private final c:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/gbson/ac;


# direct methods
.method constructor <init>(Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/aj;Lcom/google/gbson/ac;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/ObjectNavigator;",
            "Lcom/google/gbson/m;",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;",
            "Lcom/google/gbson/ac;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    .line 38
    iput-object p2, p0, Lcom/google/gbson/r;->b:Lcom/google/gbson/m;

    .line 39
    iput-object p3, p0, Lcom/google/gbson/r;->c:Lcom/google/gbson/aj;

    .line 40
    iput-object p4, p0, Lcom/google/gbson/r;->d:Lcom/google/gbson/ac;

    .line 41
    return-void
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonArray;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonArray;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/google/gbson/q;

    iget-object v3, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v4, p0, Lcom/google/gbson/r;->b:Lcom/google/gbson/m;

    iget-object v5, p0, Lcom/google/gbson/r;->d:Lcom/google/gbson/ac;

    iget-object v6, p0, Lcom/google/gbson/r;->c:Lcom/google/gbson/aj;

    move-object v1, p2

    move-object v2, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/gbson/q;-><init>(Lcom/google/gbson/JsonArray;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V

    .line 67
    iget-object v1, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    new-instance v2, Lcom/google/gbson/ah;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v2, v3, p1, v4}, Lcom/google/gbson/ah;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)V

    invoke-virtual {v1, v2, v0}, Lcom/google/gbson/ObjectNavigator;->a(Lcom/google/gbson/ah;Lcom/google/gbson/ObjectNavigator$Visitor;)V

    .line 68
    invoke-virtual {v0}, Lcom/google/gbson/q;->getTarget()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonObject;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonObject;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/google/gbson/u;

    iget-object v3, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v4, p0, Lcom/google/gbson/r;->b:Lcom/google/gbson/m;

    iget-object v5, p0, Lcom/google/gbson/r;->d:Lcom/google/gbson/ac;

    iget-object v6, p0, Lcom/google/gbson/r;->c:Lcom/google/gbson/aj;

    move-object v1, p2

    move-object v2, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/gbson/u;-><init>(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V

    .line 76
    iget-object v1, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    new-instance v2, Lcom/google/gbson/ah;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v2, v3, p1, v4}, Lcom/google/gbson/ah;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)V

    invoke-virtual {v1, v2, v0}, Lcom/google/gbson/ObjectNavigator;->a(Lcom/google/gbson/ah;Lcom/google/gbson/ObjectNavigator$Visitor;)V

    .line 77
    invoke-virtual {v0}, Lcom/google/gbson/u;->getTarget()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonPrimitive;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/JsonPrimitive;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lcom/google/gbson/u;

    iget-object v3, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v4, p0, Lcom/google/gbson/r;->b:Lcom/google/gbson/m;

    iget-object v5, p0, Lcom/google/gbson/r;->d:Lcom/google/gbson/ac;

    iget-object v6, p0, Lcom/google/gbson/r;->c:Lcom/google/gbson/aj;

    move-object v1, p2

    move-object v2, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/gbson/u;-><init>(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V

    .line 85
    iget-object v1, p0, Lcom/google/gbson/r;->a:Lcom/google/gbson/ObjectNavigator;

    new-instance v2, Lcom/google/gbson/ah;

    invoke-virtual {p2}, Lcom/google/gbson/JsonPrimitive;->a()Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v3, p1, v4}, Lcom/google/gbson/ah;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)V

    invoke-virtual {v1, v2, v0}, Lcom/google/gbson/ObjectNavigator;->a(Lcom/google/gbson/ah;Lcom/google/gbson/ObjectNavigator$Visitor;)V

    .line 86
    invoke-virtual {v0}, Lcom/google/gbson/u;->getTarget()Ljava/lang/Object;

    move-result-object v0

    .line 87
    return-object v0
.end method


# virtual methods
.method final a()Lcom/google/gbson/ag;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/gbson/r;->d:Lcom/google/gbson/ac;

    return-object v0
.end method

.method public final deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gbson/JsonElement;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 56
    :goto_0
    return-object v0

    .line 51
    :cond_1
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->isJsonArray()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v0

    invoke-direct {p0, p2, v0, p0}, Lcom/google/gbson/r;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonArray;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->isJsonObject()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->getAsJsonObject()Lcom/google/gbson/JsonObject;

    move-result-object v0

    invoke-direct {p0, p2, v0, p0}, Lcom/google/gbson/r;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonObject;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_3
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->isJsonPrimitive()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 56
    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->getAsJsonPrimitive()Lcom/google/gbson/JsonPrimitive;

    move-result-object v0

    invoke-direct {p0, p2, v0, p0}, Lcom/google/gbson/r;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonPrimitive;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 58
    :cond_4
    new-instance v0, Lcom/google/gbson/JsonParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed parsing JSON source: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gbson/JsonParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
