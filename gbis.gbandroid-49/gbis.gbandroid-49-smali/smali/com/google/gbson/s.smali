.class abstract Lcom/google/gbson/s;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/ObjectNavigator$Visitor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/gbson/ObjectNavigator$Visitor;"
    }
.end annotation


# instance fields
.field protected final a:Lcom/google/gbson/ObjectNavigator;

.field protected final b:Lcom/google/gbson/m;

.field protected final c:Lcom/google/gbson/ag;

.field protected final d:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field protected e:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected final f:Lcom/google/gbson/JsonElement;

.field protected final g:Ljava/lang/reflect/Type;

.field protected final h:Lcom/google/gbson/JsonDeserializationContext;

.field protected i:Z


# direct methods
.method constructor <init>(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/JsonElement;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/ObjectNavigator;",
            "Lcom/google/gbson/m;",
            "Lcom/google/gbson/ag;",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;>;",
            "Lcom/google/gbson/JsonDeserializationContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/google/gbson/s;->g:Ljava/lang/reflect/Type;

    .line 49
    iput-object p3, p0, Lcom/google/gbson/s;->a:Lcom/google/gbson/ObjectNavigator;

    .line 50
    iput-object p4, p0, Lcom/google/gbson/s;->b:Lcom/google/gbson/m;

    .line 51
    iput-object p5, p0, Lcom/google/gbson/s;->c:Lcom/google/gbson/ag;

    .line 52
    iput-object p6, p0, Lcom/google/gbson/s;->d:Lcom/google/gbson/aj;

    .line 53
    invoke-static {p1}, Lcom/google/gbson/internal/$Gson$Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gbson/JsonElement;

    iput-object v0, p0, Lcom/google/gbson/s;->f:Lcom/google/gbson/JsonElement;

    .line 54
    iput-object p7, p0, Lcom/google/gbson/s;->h:Lcom/google/gbson/JsonDeserializationContext;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gbson/s;->i:Z

    .line 56
    return-void
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/gbson/s;)Ljava/lang/Object;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gbson/s",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/gbson/s;->a:Lcom/google/gbson/ObjectNavigator;

    new-instance v1, Lcom/google/gbson/ah;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Lcom/google/gbson/ah;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)V

    invoke-virtual {v0, v1, p2}, Lcom/google/gbson/ObjectNavigator;->a(Lcom/google/gbson/ah;Lcom/google/gbson/ObjectNavigator$Visitor;)V

    .line 113
    invoke-virtual {p2}, Lcom/google/gbson/s;->getTarget()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected final a(Lcom/google/gbson/JsonElement;Lcom/google/gbson/ai;)Ljava/lang/Object;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/JsonElement;",
            "Lcom/google/gbson/ai",
            "<",
            "Lcom/google/gbson/JsonDeserializer",
            "<*>;",
            "Lcom/google/gbson/ah;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 88
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/gbson/JsonElement;->isJsonNull()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_1
    iget-object v0, p2, Lcom/google/gbson/ai;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/gbson/ah;

    iget-object v1, v0, Lcom/google/gbson/ah;->a:Ljava/lang/reflect/Type;

    .line 92
    iget-object v0, p2, Lcom/google/gbson/ai;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/gbson/JsonDeserializer;

    iget-object v2, p0, Lcom/google/gbson/s;->h:Lcom/google/gbson/JsonDeserializationContext;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/gbson/JsonDeserializer;->deserialize(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonArray;)Ljava/lang/Object;
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 103
    new-instance v0, Lcom/google/gbson/q;

    invoke-virtual {p2}, Lcom/google/gbson/JsonArray;->getAsJsonArray()Lcom/google/gbson/JsonArray;

    move-result-object v1

    iget-object v3, p0, Lcom/google/gbson/s;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v4, p0, Lcom/google/gbson/s;->b:Lcom/google/gbson/m;

    iget-object v5, p0, Lcom/google/gbson/s;->c:Lcom/google/gbson/ag;

    iget-object v6, p0, Lcom/google/gbson/s;->d:Lcom/google/gbson/aj;

    iget-object v7, p0, Lcom/google/gbson/s;->h:Lcom/google/gbson/JsonDeserializationContext;

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/gbson/q;-><init>(Lcom/google/gbson/JsonArray;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V

    .line 106
    invoke-direct {p0, p1, v0}, Lcom/google/gbson/s;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/s;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/reflect/Type;Lcom/google/gbson/JsonElement;)Ljava/lang/Object;
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 96
    new-instance v0, Lcom/google/gbson/u;

    iget-object v3, p0, Lcom/google/gbson/s;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v4, p0, Lcom/google/gbson/s;->b:Lcom/google/gbson/m;

    iget-object v5, p0, Lcom/google/gbson/s;->c:Lcom/google/gbson/ag;

    iget-object v6, p0, Lcom/google/gbson/s;->d:Lcom/google/gbson/aj;

    iget-object v7, p0, Lcom/google/gbson/s;->h:Lcom/google/gbson/JsonDeserializationContext;

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/gbson/u;-><init>(Lcom/google/gbson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;Lcom/google/gbson/ag;Lcom/google/gbson/aj;Lcom/google/gbson/JsonDeserializationContext;)V

    .line 99
    invoke-direct {p0, p1, v0}, Lcom/google/gbson/s;->a(Ljava/lang/reflect/Type;Lcom/google/gbson/s;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public end(Lcom/google/gbson/ah;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    return-void
.end method

.method public getTarget()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/gbson/s;->i:Z

    if-nez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/gbson/s;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gbson/s;->e:Ljava/lang/Object;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gbson/s;->i:Z

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/gbson/s;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public start(Lcom/google/gbson/ah;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    return-void
.end method

.method public final visitUsingCustomHandler(Lcom/google/gbson/ah;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 76
    iget-object v1, p0, Lcom/google/gbson/s;->d:Lcom/google/gbson/aj;

    invoke-virtual {p1, v1}, Lcom/google/gbson/ah;->a(Lcom/google/gbson/aj;)Lcom/google/gbson/ai;

    move-result-object v1

    .line 77
    if-nez v1, :cond_0

    .line 78
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/google/gbson/s;->f:Lcom/google/gbson/JsonElement;

    invoke-virtual {p0, v2, v1}, Lcom/google/gbson/s;->a(Lcom/google/gbson/JsonElement;Lcom/google/gbson/ai;)Ljava/lang/Object;

    move-result-object v1

    .line 81
    iput-object v1, p0, Lcom/google/gbson/s;->e:Ljava/lang/Object;

    .line 82
    iput-boolean v0, p0, Lcom/google/gbson/s;->i:Z

    goto :goto_0
.end method
