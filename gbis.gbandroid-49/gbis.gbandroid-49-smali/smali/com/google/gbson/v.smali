.class final Lcom/google/gbson/v;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/gbson/JsonSerializationContext;


# instance fields
.field private final a:Lcom/google/gbson/ObjectNavigator;

.field private final b:Lcom/google/gbson/m;

.field private final c:Lcom/google/gbson/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final d:Z

.field private final e:Lcom/google/gbson/ad;


# direct methods
.method constructor <init>(Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;ZLcom/google/gbson/aj;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gbson/ObjectNavigator;",
            "Lcom/google/gbson/m;",
            "Z",
            "Lcom/google/gbson/aj",
            "<",
            "Lcom/google/gbson/JsonSerializer",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/gbson/v;->a:Lcom/google/gbson/ObjectNavigator;

    .line 39
    iput-object p2, p0, Lcom/google/gbson/v;->b:Lcom/google/gbson/m;

    .line 40
    iput-boolean p3, p0, Lcom/google/gbson/v;->d:Z

    .line 41
    iput-object p4, p0, Lcom/google/gbson/v;->c:Lcom/google/gbson/aj;

    .line 42
    new-instance v0, Lcom/google/gbson/ad;

    invoke-direct {v0}, Lcom/google/gbson/ad;-><init>()V

    iput-object v0, p0, Lcom/google/gbson/v;->e:Lcom/google/gbson/ad;

    .line 43
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)Lcom/google/gbson/JsonElement;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    if-nez p1, :cond_0

    .line 58
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    .line 60
    :cond_0
    new-instance v0, Lcom/google/gbson/w;

    iget-object v1, p0, Lcom/google/gbson/v;->a:Lcom/google/gbson/ObjectNavigator;

    iget-object v2, p0, Lcom/google/gbson/v;->b:Lcom/google/gbson/m;

    iget-boolean v3, p0, Lcom/google/gbson/v;->d:Z

    iget-object v4, p0, Lcom/google/gbson/v;->c:Lcom/google/gbson/aj;

    iget-object v6, p0, Lcom/google/gbson/v;->e:Lcom/google/gbson/ad;

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/gbson/w;-><init>(Lcom/google/gbson/ObjectNavigator;Lcom/google/gbson/m;ZLcom/google/gbson/aj;Lcom/google/gbson/JsonSerializationContext;Lcom/google/gbson/ad;)V

    .line 62
    iget-object v1, p0, Lcom/google/gbson/v;->a:Lcom/google/gbson/ObjectNavigator;

    new-instance v2, Lcom/google/gbson/ah;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/gbson/ah;-><init>(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)V

    invoke-virtual {v1, v2, v0}, Lcom/google/gbson/ObjectNavigator;->a(Lcom/google/gbson/ah;Lcom/google/gbson/ObjectNavigator$Visitor;)V

    .line 63
    invoke-virtual {v0}, Lcom/google/gbson/w;->a()Lcom/google/gbson/JsonElement;

    move-result-object v0

    goto :goto_0
.end method

.method public final serialize(Ljava/lang/Object;)Lcom/google/gbson/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 47
    invoke-static {}, Lcom/google/gbson/JsonNull;->c()Lcom/google/gbson/JsonNull;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/gbson/v;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)Lcom/google/gbson/JsonElement;

    move-result-object v0

    goto :goto_0
.end method

.method public final serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/gbson/JsonElement;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/gbson/v;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Z)Lcom/google/gbson/JsonElement;

    move-result-object v0

    return-object v0
.end method
