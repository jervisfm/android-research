.class public interface abstract Lcom/inmobi/androidsdk/ai/container/IMWebView$IMWebViewListener;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/inmobi/androidsdk/ai/container/IMWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IMWebViewListener"
.end annotation


# virtual methods
.method public abstract handleRequest(Ljava/lang/String;)V
.end method

.method public abstract onDismissAdScreen()Z
.end method

.method public abstract onEventFired()Z
.end method

.method public abstract onExpand()Z
.end method

.method public abstract onExpandClose()Z
.end method

.method public abstract onLeaveApplication()Z
.end method

.method public abstract onReady()Z
.end method

.method public abstract onResize()Z
.end method

.method public abstract onResizeClose()Z
.end method

.method public abstract onShowScreen()Z
.end method
