.class Lcom/millennialmedia/android/AdDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "GBFile"


# static fields
.field private static final ACID:Ljava/lang/String; = "acid"

.field private static final ACTIVITY:Ljava/lang/String; = "activity"

.field private static final ADS_TABLE_NAME:Ljava/lang/String; = "ads"

.field private static final AD_ID:Ljava/lang/String; = "adid"

.field private static final ANDROID_ANCHOR:Ljava/lang/String; = "anchor"

.field private static final ANDROID_ANCHOR_2:Ljava/lang/String; = "anchor2"

.field private static final ANDROID_POSITION:Ljava/lang/String; = "position"

.field private static final ANDROID_POSITION_2:Ljava/lang/String; = "position2"

.field private static final APPEARANCE_DELAY:Ljava/lang/String; = "appearancedelay"

.field private static final BUTTONS_TABLE_NAME:Ljava/lang/String; = "buttons"

.field private static final CACHE_COMPLETE:Ljava/lang/String; = "cachecomplete"

.field private static final CACHE_FAILED:Ljava/lang/String; = "cachefailed"

.field private static final CONTENT_LENGTH:Ljava/lang/String; = "contentlength"

.field private static final CONTENT_URL:Ljava/lang/String; = "contenturl"

.field private static final DB_NAME:Ljava/lang/String; = "millennialmedia.db"

.field private static final DB_VERSION:I = 0x24

.field private static final DEFERRED_VIEW_START:Ljava/lang/String; = "deferredviewstart"

.field private static final DURATION:Ljava/lang/String; = "duration"

.field private static final END_ACTIVITY:Ljava/lang/String; = "endactivity"

.field private static final END_OPACITY:Ljava/lang/String; = "endopacity"

.field private static final EXPIRATION:Ljava/lang/String; = "expiration"

.field private static final FADE_DURATION:Ljava/lang/String; = "fadeduration"

.field private static final IMAGE_URL:Ljava/lang/String; = "imageurl"

.field private static final INACTIVITY_TIMEOUT:Ljava/lang/String; = "inactivitytimeout"

.field private static final LINK_URL:Ljava/lang/String; = "linkurl"

.field private static final LOG:Ljava/lang/String; = "log"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final ON_COMPLETION:Ljava/lang/String; = "oncompletion"

.field private static final OVERLAY_ORIENTATION:Ljava/lang/String; = "overlayorientation"

.field private static final PADDING_BOTTOM:Ljava/lang/String; = "paddingbottom"

.field private static final PADDING_LEFT:Ljava/lang/String; = "paddingleft"

.field private static final PADDING_RIGHT:Ljava/lang/String; = "paddingright"

.field private static final PADDING_TOP:Ljava/lang/String; = "paddingtop"

.field private static final SD_CARD:Ljava/lang/String; = "sdcard"

.field private static final SHOW_CONTROLS:Ljava/lang/String; = "showcontrols"

.field private static final SHOW_COUNTDOWN:Ljava/lang/String; = "showcountdown"

.field private static final START_ACTIVITY:Ljava/lang/String; = "startactivity"

.field private static final START_OPACITY:Ljava/lang/String; = "startopacity"

.field private static final STAY_IN_PLAYER:Ljava/lang/String; = "stayInPlayer"

.field private static final TYPE:Ljava/lang/String; = "type"

.field private static final VIDEO_ERROR:Ljava/lang/String; = "videoError"

.field private static final _ID:Ljava/lang/String; = "id"


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 85
    const-string v0, "millennialmedia.db"

    const/4 v1, 0x0

    const/16 v2, 0x24

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 86
    invoke-virtual {p0}, Lcom/millennialmedia/android/AdDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 87
    return-void
.end method

.method private getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "I[TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 850
    :try_start_0
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 851
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 852
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 853
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 854
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V

    .line 855
    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->close()V

    .line 857
    if-eqz v0, :cond_0

    .line 858
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 866
    :goto_0
    return-object v0

    .line 861
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/StreamCorruptedException;->printStackTrace()V

    .line 866
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 862
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 863
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "[TT;)V"
        }
    .end annotation

    .prologue
    .line 879
    if-nez p3, :cond_0

    .line 902
    :goto_0
    return-void

    .line 885
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 887
    const/4 v0, 0x0

    :goto_1
    array-length v2, p3

    if-ge v0, v2, :cond_1

    .line 888
    aget-object v2, p3, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 887
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 891
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 892
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 893
    invoke-virtual {v2, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 894
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 895
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V

    .line 896
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 899
    invoke-virtual {p1, p2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 901
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method checkIfAdExists(Ljava/lang/String;)Z
    .locals 3
    .parameter

    .prologue
    .line 467
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT * FROM ads WHERE ads.name=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 469
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 471
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 472
    if-lez v1, :cond_0

    .line 474
    const/4 v0, 0x1

    .line 478
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 177
    :cond_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    monitor-exit p0

    return-void

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getAllExpiredAds()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 652
    iget-object v1, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "SELECT ads.expiration,ads.name FROM ads"

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 655
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    .line 656
    if-lez v5, :cond_3

    .line 659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 663
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move v2, v3

    .line 664
    :goto_0
    if-ge v2, v5, :cond_4

    .line 666
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 667
    const/4 v6, 0x1

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 668
    if-eqz v1, :cond_2

    .line 672
    :try_start_0
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v8, "EEE MMM dd HH:mm:ss zzz yyyy"

    invoke-direct {v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 673
    invoke-virtual {v7, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 674
    if-eqz v1, :cond_0

    .line 676
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    cmp-long v1, v7, v9

    if-gtz v1, :cond_0

    .line 678
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " is expired"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 680
    if-eqz v6, :cond_0

    .line 681
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 697
    :cond_0
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->isLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 698
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 664
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 684
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 693
    :cond_2
    if-eqz v6, :cond_0

    .line 694
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 704
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 709
    :goto_2
    return-object v0

    .line 708
    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_2
.end method

.method getAllVideoAds()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/millennialmedia/android/VideoAd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 416
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 418
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ads"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "name"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 420
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 421
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 423
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->getVideoAd(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;

    move-result-object v1

    .line 424
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 427
    :cond_0
    return-object v8
.end method

.method getButtonCountForAd(Ljava/lang/String;)I
    .locals 3
    .parameter

    .prologue
    .line 605
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT COUNT(*)  FROM ads,buttons WHERE ads.name=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' AND buttons."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "adid=ads."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 609
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 610
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 611
    return v1
.end method

.method getButtonsForAd(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/android/VideoImage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 530
    iget-object v1, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "SELECT DISTINCT buttons.imageurl,buttons.contentlength,buttons.linkurl,buttons.overlayorientation,buttons.activity,buttons.position,buttons.anchor,buttons.position2,buttons.anchor2,buttons.paddingleft,buttons.paddingtop,buttons.paddingright,buttons.paddingbottom,buttons.appearancedelay,buttons.inactivitytimeout,buttons.startopacity,buttons.endopacity,buttons.fadeduration,buttons.id FROM ads,buttons WHERE ads.name=\'"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\' AND buttons."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "adid=ads."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "id ORDER BY buttons."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "id"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 555
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v5

    .line 558
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 559
    if-lez v5, :cond_3

    .line 561
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v3

    .line 562
    :goto_0
    if-ge v2, v5, :cond_2

    .line 564
    new-instance v6, Lcom/millennialmedia/android/VideoImage;

    invoke-direct {v6}, Lcom/millennialmedia/android/VideoImage;-><init>()V

    .line 565
    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    .line 566
    const/4 v0, 0x1

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v6, Lcom/millennialmedia/android/VideoImage;->contentLength:J

    .line 567
    const/4 v0, 0x2

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/millennialmedia/android/VideoImage;->linkUrl:Ljava/lang/String;

    .line 568
    const/4 v0, 0x3

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/millennialmedia/android/VideoImage;->overlayOrientation:Ljava/lang/String;

    .line 569
    const/4 v0, 0x4

    new-array v7, v3, [Ljava/lang/String;

    invoke-direct {p0, v4, v0, v7}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v6, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    .line 570
    iget-object v0, v6, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 571
    new-array v0, v3, [Ljava/lang/String;

    iput-object v0, v6, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    .line 572
    :cond_0
    const/4 v0, 0x5

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->position:I

    .line 573
    const/4 v0, 0x6

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->anchor:I

    .line 574
    const/4 v0, 0x7

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->position2:I

    .line 575
    const/16 v0, 0x8

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->anchor2:I

    .line 576
    const/16 v0, 0x9

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->paddingLeft:I

    .line 577
    const/16 v0, 0xa

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->paddingTop:I

    .line 578
    const/16 v0, 0xb

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->paddingRight:I

    .line 579
    const/16 v0, 0xc

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->paddingBottom:I

    .line 580
    const/16 v0, 0xd

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v6, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    .line 581
    const/16 v0, 0xe

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v6, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    .line 582
    const/16 v0, 0xf

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    .line 583
    const/16 v0, 0x10

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, v6, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    .line 584
    const/16 v0, 0x11

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v6, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    .line 586
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    invoke-interface {v4}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 589
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 562
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 593
    :cond_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 594
    return-object v0
.end method

.method getCachedAdAcid(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 447
    .line 448
    iget-object v1, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT acid FROM ads WHERE ads.name=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 450
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 452
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 453
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 455
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 457
    return-object v0
.end method

.method getCachedAdId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 432
    .line 433
    iget-object v1, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT name FROM ads WHERE ads.acid=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 435
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 437
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 438
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 440
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 442
    return-object v0
.end method

.method getDeferredViewStart(Ljava/lang/String;)J
    .locals 4
    .parameter

    .prologue
    .line 762
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT ads.deferredviewstart FROM ads WHERE ads.name=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 764
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 765
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 766
    if-lez v3, :cond_0

    .line 768
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 769
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 771
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 772
    return-wide v0
.end method

.method getIdForAdName(Ljava/lang/String;)I
    .locals 9
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 822
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ads"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v8

    const-string v3, "ads.name= ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v8

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 825
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 827
    if-lez v0, :cond_0

    .line 829
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 830
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 832
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 833
    return v0

    :cond_0
    move v0, v8

    goto :goto_0
.end method

.method getVideoAd(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    .locals 13
    .parameter

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 260
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT DISTINCT ads.name,ads.acid,ads.contenturl,ads.expiration,ads.deferredviewstart,ads.oncompletion,ads.showcontrols,ads.startactivity,ads.endactivity,ads.duration,ads.contentlength,ads.stayInPlayer,ads.log,ads.id,ads.sdcard,ads.showcountdown,ads.cachecomplete,ads.cachefailed,ads.videoError FROM ads WHERE ads.name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 283
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 286
    if-lez v7, :cond_e

    .line 288
    new-instance v4, Lcom/millennialmedia/android/VideoAd;

    invoke-direct {v4}, Lcom/millennialmedia/android/VideoAd;-><init>()V

    .line 289
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 290
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    .line 291
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    .line 292
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->contentUrl:Ljava/lang/String;

    .line 293
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 296
    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v8, "EEE MMM dd HH:mm:ss zzz yyyy"

    invoke-direct {v3, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 297
    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->expiration:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :cond_0
    :goto_0
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/millennialmedia/android/VideoAd;->deferredViewStart:J

    .line 303
    const/4 v0, 0x5

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->onCompletionUrl:Ljava/lang/String;

    .line 304
    const/4 v0, 0x6

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 306
    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, v4, Lcom/millennialmedia/android/VideoAd;->showControls:Z

    .line 308
    const/4 v0, 0x7

    new-array v3, v2, [Ljava/lang/String;

    invoke-direct {p0, v6, v0, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->startActivity:[Ljava/lang/String;

    .line 309
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->startActivity:[Ljava/lang/String;

    if-nez v0, :cond_1

    .line 310
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->startActivity:[Ljava/lang/String;

    .line 311
    :cond_1
    const/16 v0, 0x8

    new-array v3, v2, [Ljava/lang/String;

    invoke-direct {p0, v6, v0, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->endActivity:[Ljava/lang/String;

    .line 312
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->endActivity:[Ljava/lang/String;

    if-nez v0, :cond_2

    .line 313
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->endActivity:[Ljava/lang/String;

    .line 314
    :cond_2
    const/16 v0, 0x9

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/millennialmedia/android/VideoAd;->duration:J

    .line 315
    const/16 v0, 0xa

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/millennialmedia/android/VideoAd;->contentLength:J

    .line 316
    const/16 v0, 0xb

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 317
    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, v4, Lcom/millennialmedia/android/VideoAd;->stayInPlayer:Z

    .line 319
    const/16 v0, 0xc

    new-array v3, v2, [Lcom/millennialmedia/android/VideoLogEvent;

    invoke-direct {p0, v6, v0, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/millennialmedia/android/VideoLogEvent;

    .line 320
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v4, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    .line 321
    if-eqz v0, :cond_5

    move v3, v2

    .line 323
    :goto_3
    array-length v8, v0

    if-ge v3, v8, :cond_5

    .line 324
    iget-object v8, v4, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 299
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 306
    goto :goto_1

    :cond_4
    move v0, v2

    .line 317
    goto :goto_2

    .line 327
    :cond_5
    const/16 v0, 0xd

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 329
    const/16 v0, 0xe

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 330
    if-ne v0, v1, :cond_b

    move v0, v1

    :goto_4
    iput-boolean v0, v4, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z

    .line 332
    const/16 v0, 0xf

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 333
    if-ne v0, v1, :cond_c

    move v0, v1

    :goto_5
    iput-boolean v0, v4, Lcom/millennialmedia/android/VideoAd;->showCountdown:Z

    .line 335
    const/16 v0, 0x10

    new-array v8, v2, [Ljava/lang/String;

    invoke-direct {p0, v6, v0, v8}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    .line 336
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    if-nez v0, :cond_6

    .line 337
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    .line 339
    :cond_6
    const/16 v0, 0x11

    new-array v8, v2, [Ljava/lang/String;

    invoke-direct {p0, v6, v0, v8}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    .line 340
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    if-nez v0, :cond_7

    .line 341
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    .line 343
    :cond_7
    const/16 v0, 0x12

    new-array v8, v2, [Ljava/lang/String;

    invoke-direct {p0, v6, v0, v8}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    .line 344
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    if-nez v0, :cond_8

    .line 345
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    .line 347
    :cond_8
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SELECT DISTINCT buttons.imageurl,buttons.contentlength,buttons.linkurl,buttons.overlayorientation,buttons.activity,buttons.position,buttons.anchor,buttons.position2,buttons.anchor2,buttons.paddingleft,buttons.paddingtop,buttons.paddingright,buttons.paddingbottom,buttons.appearancedelay,buttons.inactivitytimeout,buttons.startopacity,buttons.endopacity,buttons.fadeduration,buttons.id FROM ads,buttons WHERE buttons.adid="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " ORDER BY buttons.id"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 371
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 373
    if-lez v8, :cond_d

    .line 375
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v4, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    move v3, v2

    .line 377
    :goto_6
    if-ge v3, v8, :cond_d

    .line 379
    new-instance v7, Lcom/millennialmedia/android/VideoImage;

    invoke-direct {v7}, Lcom/millennialmedia/android/VideoImage;-><init>()V

    .line 381
    invoke-interface {v5, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    .line 382
    invoke-interface {v5, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v7, Lcom/millennialmedia/android/VideoImage;->contentLength:J

    .line 383
    invoke-interface {v5, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/millennialmedia/android/VideoImage;->linkUrl:Ljava/lang/String;

    .line 384
    invoke-interface {v5, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/millennialmedia/android/VideoImage;->overlayOrientation:Ljava/lang/String;

    .line 385
    const/4 v0, 0x4

    new-array v9, v2, [Ljava/lang/String;

    invoke-direct {p0, v5, v0, v9}, Lcom/millennialmedia/android/AdDatabaseHelper;->getArray(Landroid/database/Cursor;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v7, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    .line 386
    iget-object v0, v7, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    if-nez v0, :cond_9

    .line 387
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, v7, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    .line 388
    :cond_9
    const/4 v0, 0x5

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->position:I

    .line 389
    const/4 v0, 0x6

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->anchor:I

    .line 390
    const/4 v0, 0x7

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->position2:I

    .line 391
    const/16 v0, 0x8

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->anchor2:I

    .line 392
    const/16 v0, 0x9

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->paddingLeft:I

    .line 393
    const/16 v0, 0xa

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->paddingTop:I

    .line 394
    const/16 v0, 0xb

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->paddingRight:I

    .line 395
    const/16 v0, 0xc

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->paddingBottom:I

    .line 396
    const/16 v0, 0xd

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v7, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    .line 397
    const/16 v0, 0xe

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v7, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    .line 398
    const/16 v0, 0xf

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    .line 399
    const/16 v0, 0x10

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, v7, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    .line 400
    const/16 v0, 0x11

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    iput-wide v9, v7, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    .line 402
    iget-object v0, v4, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-interface {v5}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_a

    .line 405
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    .line 377
    :cond_a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_6

    :cond_b
    move v0, v2

    .line 330
    goto/16 :goto_4

    :cond_c
    move v0, v2

    .line 333
    goto/16 :goto_5

    .line 408
    :cond_d
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move-object v0, v4

    .line 410
    :goto_7
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 411
    return-object v0

    :cond_e
    move-object v0, v5

    goto :goto_7
.end method

.method isAdExpired(Ljava/lang/String;)Z
    .locals 6
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 720
    iget-object v2, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT ads.expiration FROM ads WHERE ads.name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 722
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 724
    if-lez v2, :cond_1

    .line 726
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 727
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 728
    if-eqz v2, :cond_0

    .line 734
    :try_start_0
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "EEE MMM dd HH:mm:ss zzz yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 735
    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 748
    :cond_0
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 749
    if-eqz v0, :cond_3

    .line 750
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    .line 751
    :goto_1
    return v0

    .line 736
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 744
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v0, v1

    .line 745
    goto :goto_1

    :cond_2
    move v0, v1

    .line 750
    goto :goto_1

    :cond_3
    move v0, v1

    .line 751
    goto :goto_1
.end method

.method isAdOnSDCard(Ljava/lang/String;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 620
    .line 621
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT sdcard FROM ads WHERE ads.name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 624
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 626
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 627
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 629
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 630
    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter

    .prologue
    .line 99
    const-string v0, "Creating cached ad database"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 101
    const-string v0, "CREATE TABLE ads (id INTEGER NOT NULL PRIMARY KEY,name TEXT,acid TEXT,type INTEGER,startactivity BLOB,endactivity BLOB,showcontrols INTEGER,contenturl TEXT,expiration TEXT,deferredviewstart BIGINT,oncompletion TEXT,duration BIGINT,contentlength BIGINT,stayInPlayer INTEGER,log BLOB,sdcard INTEGER,showcountdown INTEGER,cachecomplete BLOB,cachefailed BLOB,videoError BLOB);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 124
    const-string v0, "CREATE TABLE buttons (id INTEGER NOT NULL PRIMARY KEY,imageurl TEXT,contentlength BIGINT,linkurl TEXT,overlayorientation TEXT,activity BLOB,position INTEGER,anchor INTEGER,position2 INTEGER,anchor2 INTEGER,paddingtop INTEGER,paddingleft INTEGER,paddingbottom INTEGER,paddingright INTEGER,appearancedelay BIGINT,inactivitytimeout BIGINT,startopacity FLOAT,endopacity FLOAT,fadeduration BIGINT,adid INTEGER CONSTRAINT fk_buttons_ads_id REFERENCES ads(id) ON DELETE CASCADE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 149
    const-string v0, "CREATE TRIGGER fk_buttons_ads_id BEFORE DELETE ON ads FOR EACH ROW BEGIN DELETE from buttons WHERE buttons.adid=OLD.id; END;"

    .line 154
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .parameter

    .prologue
    .line 93
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upgrading database from version "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", which will destroy all old data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 163
    const-string v0, "DROP TABLE IF EXISTS ads"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 164
    const-string v0, "DROP TABLE IF EXISTS buttons"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0, p1}, Lcom/millennialmedia/android/AdDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 168
    return-void
.end method

.method purgeAdFromDb(Ljava/lang/String;)Z
    .locals 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 519
    iget-object v2, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "ads"

    const-string v4, "ads.name=?"

    new-array v5, v0, [Ljava/lang/String;

    aput-object p1, v5, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 520
    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method shouldShowBottomBar(Ljava/lang/String;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 491
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT DISTINCT showcontrols FROM ads WHERE ads.name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 496
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 500
    if-lez v0, :cond_1

    .line 502
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 504
    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 506
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 508
    if-ne v0, v1, :cond_0

    :goto_1
    return v1

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method storeAd(Lcom/millennialmedia/android/VideoAd;)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 187
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 188
    const-string v0, "name"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "acid"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "type"

    iget v2, p1, Lcom/millennialmedia/android/VideoAd;->type:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 191
    const-string v0, "startactivity"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->startActivity:[Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    const-string v0, "endactivity"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->endActivity:[Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    const-string v0, "showcontrols"

    iget-boolean v2, p1, Lcom/millennialmedia/android/VideoAd;->showControls:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 194
    const-string v0, "contenturl"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->contentUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :try_start_0
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->expiration:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 200
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE MMM dd HH:mm:ss zzz yyyy"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 201
    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->expiration:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    const-string v2, "expiration"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_0
    :goto_0
    const-string v0, "deferredviewstart"

    iget-wide v2, p1, Lcom/millennialmedia/android/VideoAd;->deferredViewStart:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 209
    const-string v0, "oncompletion"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->onCompletionUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v0, "duration"

    iget-wide v2, p1, Lcom/millennialmedia/android/VideoAd;->duration:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 211
    const-string v0, "contentlength"

    iget-wide v2, p1, Lcom/millennialmedia/android/VideoAd;->contentLength:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 212
    const-string v0, "stayInPlayer"

    iget-boolean v2, p1, Lcom/millennialmedia/android/VideoAd;->stayInPlayer:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 213
    const-string v0, "sdcard"

    iget-boolean v2, p1, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 214
    const-string v0, "showcountdown"

    iget-boolean v2, p1, Lcom/millennialmedia/android/VideoAd;->showCountdown:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 215
    const-string v0, "log"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    const-string v0, "cachecomplete"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 217
    const-string v0, "cachefailed"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    const-string v0, "videoError"

    iget-object v2, p1, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    invoke-direct {p0, v1, v0, v2}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "ads"

    invoke-virtual {v0, v2, v8, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 222
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 224
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 225
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 226
    const-string v5, "imageurl"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v5, "contentlength"

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->contentLength:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 228
    const-string v5, "linkurl"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->linkUrl:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v5, "overlayorientation"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->overlayOrientation:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-string v5, "activity"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    invoke-direct {p0, v4, v5, v6}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    const-string v5, "position"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->position:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 232
    const-string v5, "anchor"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->anchor:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 233
    const-string v5, "position2"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->position2:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 234
    const-string v5, "anchor2"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->anchor2:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 235
    const-string v5, "paddingtop"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->paddingTop:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    const-string v5, "paddingleft"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->paddingLeft:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    const-string v5, "paddingright"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->paddingRight:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 238
    const-string v5, "paddingbottom"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->paddingBottom:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const-string v5, "appearancedelay"

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 240
    const-string v5, "inactivitytimeout"

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 241
    const-string v5, "startopacity"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 242
    const-string v5, "endopacity"

    iget v6, v0, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 243
    const-string v5, "fadeduration"

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 244
    const-string v0, "adid"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 247
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "buttons"

    invoke-virtual {v0, v5, v8, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 222
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 205
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 249
    :cond_1
    return-void
.end method

.method updateAdData(Lcom/millennialmedia/android/VideoAd;)V
    .locals 11
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 777
    if-nez p1, :cond_1

    .line 818
    :cond_0
    return-void

    .line 780
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 781
    const-string v1, "startactivity"

    iget-object v3, p1, Lcom/millennialmedia/android/VideoAd;->startActivity:[Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 782
    const-string v1, "endactivity"

    iget-object v3, p1, Lcom/millennialmedia/android/VideoAd;->endActivity:[Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 783
    const-string v1, "log"

    iget-object v3, p1, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 784
    const-string v1, "deferredviewstart"

    new-instance v3, Ljava/lang/Long;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 786
    iget-object v1, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "ads"

    const-string v4, "ads.name=?"

    new-array v5, v10, [Ljava/lang/String;

    iget-object v6, p1, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 788
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/AdDatabaseHelper;->getIdForAdName(Ljava/lang/String;)I

    move-result v3

    .line 790
    if-lez v3, :cond_0

    move v1, v2

    .line 792
    :goto_0
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 794
    iget-object v0, p1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 795
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 797
    const-string v5, "linkurl"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->linkUrl:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    const-string v5, "overlayorientation"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->overlayOrientation:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    const-string v5, "activity"

    iget-object v6, v0, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    invoke-direct {p0, v4, v5, v6}, Lcom/millennialmedia/android/AdDatabaseHelper;->putArray(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 800
    const-string v5, "position"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->position:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 801
    const-string v5, "anchor"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->anchor:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 802
    const-string v5, "position2"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->position2:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 803
    const-string v5, "anchor2"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->anchor2:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 804
    const-string v5, "paddingtop"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->paddingTop:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 805
    const-string v5, "paddingleft"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->paddingLeft:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 806
    const-string v5, "paddingbottom"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->paddingBottom:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 807
    const-string v5, "paddingright"

    new-instance v6, Ljava/lang/Integer;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->paddingRight:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 808
    const-string v5, "appearancedelay"

    new-instance v6, Ljava/lang/Long;

    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    invoke-direct {v6, v7, v8}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 809
    const-string v5, "inactivitytimeout"

    new-instance v6, Ljava/lang/Long;

    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    invoke-direct {v6, v7, v8}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 810
    const-string v5, "startopacity"

    new-instance v6, Ljava/lang/Float;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {v6, v7}, Ljava/lang/Float;-><init>(F)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 811
    const-string v5, "endopacity"

    new-instance v6, Ljava/lang/Float;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    invoke-direct {v6, v7}, Ljava/lang/Float;-><init>(F)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 812
    const-string v5, "fadeduration"

    new-instance v6, Ljava/lang/Long;

    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-direct {v6, v7, v8}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 814
    iget-object v5, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "buttons"

    const-string v7, "buttons.adid=? AND buttons.imageurl=? "

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    aput-object v0, v8, v10

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 792
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0
.end method

.method updateAdOnSDCard(Ljava/lang/String;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 639
    iget-object v0, p0, Lcom/millennialmedia/android/AdDatabaseHelper;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE ads SET sdcard = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE ads.name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 643
    return-void
.end method
