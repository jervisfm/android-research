.class Lcom/millennialmedia/android/MMAdViewController$1;
.super Ljava/lang/Thread;
.source "GBFile"


# instance fields
.field final synthetic this$0:Lcom/millennialmedia/android/MMAdViewController;

.field final synthetic val$fetch:Z


# direct methods
.method constructor <init>(Lcom/millennialmedia/android/MMAdViewController;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 500
    iput-object p1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean p2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->val$fetch:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 503
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 504
    if-nez v0, :cond_0

    .line 506
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The reference to the ad view was broken."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v0, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    .line 734
    :goto_0
    return-void

    .line 511
    :cond_0
    iget-object v1, v0, Lcom/millennialmedia/android/MMAdView;->apid:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 513
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    .line 515
    const-string v0, "MillennialMediaSDK"

    const-string v1, "MMAdView found with a null apid. New ad requests on this MMAdView are disabled until an apid has been assigned."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v0, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    goto :goto_0

    .line 521
    :cond_1
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK;->isConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 531
    :try_start_0
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->getURLMetaValues(Lcom/millennialmedia/android/MMAdView;)Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$300(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)Ljava/lang/String;

    move-result-object v4

    .line 532
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewController;->getURLDeviceValues(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 533
    iget-boolean v1, v0, Lcom/millennialmedia/android/MMAdView;->testMode:Z

    if-eqz v1, :cond_2

    .line 535
    const-string v1, "MillennialMediaSDK"

    const-string v6, "*********** Advertising test mode is deprecated. Refer to wiki.millennialmedia.com for testing information. "

    invoke-static {v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    :cond_2
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v6, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->getAdType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v1, v6}, Lcom/millennialmedia/android/MMAdViewController;->access$400(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v1

    .line 538
    :try_start_1
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    .line 540
    iget v7, v6, Landroid/util/DisplayMetrics;->density:F

    .line 541
    iget v8, v6, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 542
    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 548
    new-instance v9, Ljava/lang/StringBuilder;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "http://androidsdk.ads.mp.mydas.mobi/getAd.php5?sdkapid="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v0, Lcom/millennialmedia/android/MMAdView;->apid:Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 549
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/millennialmedia/android/MMAdViewSDK;->getAuidOrHdid(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 550
    if-eqz v10, :cond_3

    .line 552
    const-string v11, "mmh_"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 553
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "&hdid="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "UTF-8"

    invoke-static {v10, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    :cond_3
    :goto_1
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/millennialmedia/android/MMAdViewSDK;->getMMdid(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 558
    if-eqz v10, :cond_4

    .line 559
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "&mmdid="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "UTF-8"

    invoke-static {v10, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    :cond_4
    iget-object v10, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->useragent:Ljava/lang/String;
    invoke-static {v10}, Lcom/millennialmedia/android/MMAdViewController;->access$500(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 561
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "&ua="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->useragent:Ljava/lang/String;
    invoke-static {v11}, Lcom/millennialmedia/android/MMAdViewController;->access$500(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    :goto_2
    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v10, :cond_5

    .line 565
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "&dm="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    :cond_5
    sget-object v10, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    if-eqz v10, :cond_6

    .line 567
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "&dv=Android"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v11, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-static {v11, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "&density="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "&hpx="

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "&wpx="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "&mmisdk="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "4.5.1-12.2.2.a"

    const-string v8, "UTF-8"

    invoke-static {v7, v8}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 575
    invoke-static {v4}, Lcom/millennialmedia/android/HandShake;->sharedHandShake(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;

    move-result-object v5

    iget-object v6, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Lcom/millennialmedia/android/HandShake;->canRequestVideo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 576
    const-string v4, "&video=true"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    :goto_3
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v4, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_7

    sget-object v4, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    const-string v5, "8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mounted"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 583
    :cond_7
    const-string v4, "&cachedvideo=false"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    :goto_4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    .line 591
    :goto_5
    if-eqz v2, :cond_8

    .line 592
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 593
    :cond_8
    const-string v2, "MillennialMediaSDK"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Calling for an advertisement: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :try_start_2
    new-instance v2, Lcom/millennialmedia/android/HttpGetRequest;

    invoke-direct {v2}, Lcom/millennialmedia/android/HttpGetRequest;-><init>()V

    .line 598
    invoke-virtual {v2, v1}, Lcom/millennialmedia/android/HttpGetRequest;->get(Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 599
    if-nez v4, :cond_d

    .line 601
    const-string v1, "MillennialMediaSDK"

    const-string v2, "HTTP response is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    .line 603
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 608
    :catch_0
    move-exception v1

    const-string v2, "MillennialMediaSDK"

    const-string v4, "HTTP error: "

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 555
    :cond_9
    :try_start_3
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "&auid="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "UTF-8"

    invoke-static {v10, v12}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :catch_1
    move-exception v4

    :goto_6
    move-object v13, v2

    move-object v2, v1

    move-object v1, v13

    goto :goto_5

    .line 563
    :cond_a
    const-string v10, "&ua=UNKNOWN"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 578
    :cond_b
    const-string v4, "&video=false"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 585
    :cond_c
    const-string v4, "&cachedvideo=true"

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_4

    .line 607
    :cond_d
    :try_start_4
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v5

    .line 610
    if-nez v5, :cond_e

    .line 612
    const-string v1, "MillennialMediaSDK"

    const-string v2, "Null HTTP entity"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    .line 614
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 618
    :cond_e
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-nez v2, :cond_f

    .line 620
    const-string v1, "MillennialMediaSDK"

    const-string v2, "Millennial ad return failed. Zero content length returned."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    .line 623
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 628
    :cond_f
    const-string v2, "Set-Cookie"

    invoke-interface {v4, v2}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v6

    .line 629
    array-length v7, v6

    move v2, v3

    :goto_7
    if-ge v2, v7, :cond_11

    aget-object v8, v6, v2

    .line 632
    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 633
    const-string v9, "MAC-ID="

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 634
    if-ltz v9, :cond_10

    .line 636
    const/16 v10, 0x3b

    invoke-virtual {v8, v10, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v10

    .line 637
    if-le v10, v9, :cond_10

    .line 638
    add-int/lit8 v9, v9, 0x7

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/millennialmedia/android/MMAdViewSDK;->macId:Ljava/lang/String;

    .line 629
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 642
    :cond_11
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v2

    .line 643
    if-eqz v2, :cond_1a

    .line 645
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_19

    .line 647
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "application/json"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 654
    :try_start_5
    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/HttpGetRequest;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    .line 655
    new-instance v2, Lcom/millennialmedia/android/VideoAd;

    invoke-direct {v2, v1}, Lcom/millennialmedia/android/VideoAd;-><init>(Ljava/lang/String;)V

    .line 657
    const-string v1, "MillennialMediaSDK"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Current environment: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    const-string v4, "mounted"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 659
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 664
    :cond_12
    invoke-virtual {v2}, Lcom/millennialmedia/android/VideoAd;->isValid()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 666
    const-string v0, "MillennialMediaSDK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Cached video ad JSON received: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->val$fetch:Z

    #calls: Lcom/millennialmedia/android/MMAdViewController;->handleCachedAdResponse(Lcom/millennialmedia/android/VideoAd;Z)V
    invoke-static {v0, v2, v1}, Lcom/millennialmedia/android/MMAdViewController;->access$600(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/VideoAd;Z)V

    .line 733
    :cond_13
    :goto_8
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v0, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    goto/16 :goto_0

    .line 661
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const-string v1, "MillennialMediaSDK"

    const-string v2, "Millennial ad return failed. Invalid response data."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 662
    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    const-string v2, "MillennialMediaSDK"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Millennial ad return failed. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iput-boolean v3, v1, Lcom/millennialmedia/android/MMAdViewController;->requestInProgress:Z

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 671
    :cond_14
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v6, "text/html"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 674
    const-string v2, "X-MM-Video"

    invoke-interface {v4, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 675
    if-eqz v2, :cond_15

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 677
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 678
    invoke-static {v2}, Lcom/millennialmedia/android/HandShake;->sharedHandShake(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;

    move-result-object v4

    iget-object v6, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    invoke-virtual {v4, v2, v6}, Lcom/millennialmedia/android/HandShake;->didReceiveVideoXHeader(Landroid/content/Context;Ljava/lang/String;)V

    .line 683
    :cond_15
    :try_start_6
    iget-boolean v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->val$fetch:Z

    if-eqz v2, :cond_16

    iget-object v2, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    const-string v4, "MMFullScreenAdLaunch"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 685
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/millennialmedia/android/HttpGetRequest;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedContentLaunch:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/millennialmedia/android/MMAdViewController;->access$702(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;

    .line 686
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const/4 v4, 0x0

    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedBaseUrlLaunch:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/millennialmedia/android/MMAdViewController;->access$802(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;

    .line 687
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedTimeLaunch:J
    invoke-static {v1, v4, v5}, Lcom/millennialmedia/android/MMAdViewController;->access$902(Lcom/millennialmedia/android/MMAdViewController;J)J

    .line 688
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adIsCaching(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$1000(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    .line 689
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const/4 v2, 0x1

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adDoneCaching(Lcom/millennialmedia/android/MMAdView;Z)V
    invoke-static {v1, v0, v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1100(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_8

    .line 704
    :catch_4
    move-exception v1

    .line 706
    const-string v2, "MillennialMediaSDK"

    const-string v4, "Exception raised in HTTP stream: "

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 707
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_8

    .line 691
    :cond_16
    :try_start_7
    iget-boolean v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->val$fetch:Z

    if-eqz v2, :cond_17

    iget-object v2, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    const-string v4, "MMFullScreenAdTransition"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 693
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/millennialmedia/android/HttpGetRequest;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedContentTransition:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/millennialmedia/android/MMAdViewController;->access$1202(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;

    .line 694
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const/4 v4, 0x0

    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedBaseUrlTransition:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/millennialmedia/android/MMAdViewController;->access$1302(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;

    .line 695
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->fetchedTimeTransition:J
    invoke-static {v1, v4, v5}, Lcom/millennialmedia/android/MMAdViewController;->access$1402(Lcom/millennialmedia/android/MMAdViewController;J)J

    .line 696
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adIsCaching(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$1000(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    .line 697
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const/4 v2, 0x1

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adDoneCaching(Lcom/millennialmedia/android/MMAdView;Z)V
    invoke-static {v1, v0, v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1100(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V

    goto/16 :goto_8

    .line 701
    :cond_17
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-interface {v5}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    invoke-static {v4}, Lcom/millennialmedia/android/HttpGetRequest;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "/"

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    #calls: Lcom/millennialmedia/android/MMAdViewController;->setWebViewContent(Ljava/lang/String;Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v2, v4, v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$1500(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_8

    .line 712
    :cond_18
    const-string v1, "MillennialMediaSDK"

    const-string v2, "Millennial ad return failed. Invalid mime type returned."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_8

    .line 718
    :cond_19
    const-string v1, "MillennialMediaSDK"

    const-string v2, "Millennial ad return failed. HTTP Header value null."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_8

    .line 724
    :cond_1a
    const-string v1, "MillennialMediaSDK"

    const-string v2, "Millennial ad return failed. HTTP Header is null."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_8

    .line 730
    :cond_1b
    const-string v1, "MillennialMediaSDK"

    const-string v2, "No network available, can\'t call for ads."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$1;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adFailed(Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$200(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_8

    :catch_5
    move-exception v1

    move-object v1, v2

    goto/16 :goto_6
.end method
