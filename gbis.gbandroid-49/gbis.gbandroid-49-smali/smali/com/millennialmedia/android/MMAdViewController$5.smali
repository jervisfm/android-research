.class Lcom/millennialmedia/android/MMAdViewController$5;
.super Ljava/lang/Thread;
.source "GBFile"


# instance fields
.field final synthetic this$0:Lcom/millennialmedia/android/MMAdViewController;


# direct methods
.method constructor <init>(Lcom/millennialmedia/android/MMAdViewController;)V
    .locals 0
    .parameter

    .prologue
    .line 907
    iput-object p1, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 912
    .line 916
    const-string v0, "Touch occured, opening ad..."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 918
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->urlString:Ljava/lang/String;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$1700(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1138
    :cond_0
    :goto_0
    return-void

    .line 921
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 922
    if-nez v0, :cond_2

    .line 924
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The reference to the ad view was broken."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 927
    :cond_2
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 928
    if-nez v1, :cond_3

    .line 930
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The ad view does not have a parent activity."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move-object v3, v2

    .line 937
    :cond_4
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->urlString:Ljava/lang/String;
    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1700(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v4

    .line 940
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v5, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->urlString:Ljava/lang/String;
    invoke-static {v5}, Lcom/millennialmedia/android/MMAdViewController;->access$1700(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 941
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 942
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 944
    const-string v5, "GET"

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 945
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 946
    iget-object v5, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    const-string v6, "Location"

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    #setter for: Lcom/millennialmedia/android/MMAdViewController;->urlString:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/millennialmedia/android/MMAdViewController;->access$1702(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;)Ljava/lang/String;

    .line 947
    const-string v5, "Content-Type"

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 948
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    .line 950
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Response: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 951
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "urlString: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->urlString:Ljava/lang/String;
    invoke-static {v6}, Lcom/millennialmedia/android/MMAdViewController;->access$1700(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 965
    const/16 v2, 0x12c

    if-lt v5, v2, :cond_5

    const/16 v2, 0x190

    if-lt v5, v2, :cond_4

    :cond_5
    move-object v2, v3

    .line 976
    :goto_1
    if-eqz v4, :cond_0

    .line 980
    if-nez v2, :cond_6

    .line 981
    const-string v2, ""

    .line 983
    :cond_6
    invoke-static {v4}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 984
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 985
    if-eqz v3, :cond_0

    .line 987
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 991
    :try_start_1
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "https"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    :cond_7
    const-string v4, "text/html"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 993
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/MMAdViewOverlayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 994
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 995
    const-string v2, "canAccelerate"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 996
    const-string v2, "overlayTransition"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTransition:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 997
    const-string v2, "transitionTime"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-wide v4, v4, Lcom/millennialmedia/android/MMAdViewController;->transitionTime:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 998
    const-string v2, "shouldResizeOverlay"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldResizeOverlay:I

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 999
    const-string v2, "shouldShowTitlebar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowTitlebar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1000
    const-string v2, "shouldShowBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1001
    const-string v2, "shouldEnableBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldEnableBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1002
    const-string v2, "shouldMakeOverlayTransparent"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldMakeOverlayTransparent:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1003
    const-string v2, "overlayTitle"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1004
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Accelerometer on?: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1007
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1008
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1132
    :catch_0
    move-exception v0

    .line 1134
    const-string v1, "MillennialMediaSDK"

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 953
    :catch_1
    move-exception v2

    move-object v2, v3

    .line 958
    goto/16 :goto_1

    .line 960
    :catch_2
    move-exception v2

    move-object v2, v3

    .line 963
    goto/16 :goto_1

    .line 1010
    :cond_8
    :try_start_2
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mmvideo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1012
    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 1013
    if-eqz v1, :cond_0

    .line 1015
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mmvideo: attempting to play video "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1016
    #calls: Lcom/millennialmedia/android/MMAdViewController;->checkIfAdExistsInDb(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z
    invoke-static {v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$1800(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1018
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    #calls: Lcom/millennialmedia/android/MMAdViewController;->checkIfAdExistsInFilesystem(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z
    invoke-static {v2, v1, v3}, Lcom/millennialmedia/android/MMAdViewController;->access$1900(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1021
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->checkIfExpired(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z
    invoke-static {v2, v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$2000(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1023
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/HandShake;->sharedHandShake(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;

    move-result-object v2

    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/millennialmedia/android/HandShake;->updateLastVideoViewedTime(Landroid/content/Context;Ljava/lang/String;)V

    .line 1024
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->playVideo(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)V
    invoke-static {v2, v1, v0}, Lcom/millennialmedia/android/MMAdViewController;->access$2100(Lcom/millennialmedia/android/MMAdViewController;Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)V

    goto/16 :goto_0

    .line 1028
    :cond_9
    const-string v0, "mmvideo: Ad is expired."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1033
    :cond_a
    const-string v0, "mmvideo: Ad is not in the filesystem."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1038
    :cond_b
    const-string v0, "mmvideo: Ad is not in the database."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1042
    :cond_c
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v4, "market"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1044
    const-string v0, "Android Market URL, launch the Market Application"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1045
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1046
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1047
    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1049
    :cond_d
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v4, "rtsp"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v4, "http"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "video/mp4"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "video/3gpp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1054
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Video, launch the video player for video at: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1055
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/VideoPlayer;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1056
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1057
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1058
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1060
    :cond_f
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "tel"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1062
    const-string v0, "Telephone Number, launch the phone"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1063
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1064
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1065
    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1067
    :cond_10
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "geo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1069
    const-string v0, "Google Maps"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1070
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1071
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1072
    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1074
    :cond_11
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1076
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".mp4"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".3gp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1079
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Video, launch the video player for video at: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1080
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/VideoPlayer;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1081
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1082
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1083
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1088
    :cond_13
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/MMAdViewOverlayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1089
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1090
    const-string v2, "canAccelerate"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1091
    const-string v2, "overlayTransition"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTransition:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1092
    const-string v2, "transitionTime"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-wide v4, v4, Lcom/millennialmedia/android/MMAdViewController;->transitionTime:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1093
    const-string v2, "shouldResizeOverlay"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldResizeOverlay:I

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1094
    const-string v2, "shouldShowTitlebar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowTitlebar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1095
    const-string v2, "shouldShowBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1096
    const-string v2, "shouldEnableBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldEnableBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1097
    const-string v2, "shouldMakeOverlayTransparent"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldMakeOverlayTransparent:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1098
    const-string v2, "overlayTitle"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1099
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Accelerometer on?: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1101
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1102
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1105
    :cond_14
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1108
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/MMAdViewOverlayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1109
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1110
    const-string v2, "canAccelerate"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1111
    const-string v2, "overlayTransition"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTransition:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1112
    const-string v2, "transitionTime"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-wide v4, v4, Lcom/millennialmedia/android/MMAdViewController;->transitionTime:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1113
    const-string v2, "shouldResizeOverlay"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldResizeOverlay:I

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1114
    const-string v2, "shouldShowTitlebar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowTitlebar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1115
    const-string v2, "shouldShowBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldShowBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1116
    const-string v2, "shouldEnableBottomBar"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldEnableBottomBar:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1117
    const-string v2, "shouldMakeOverlayTransparent"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->shouldMakeOverlayTransparent:Z

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1118
    const-string v2, "overlayTitle"

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-object v4, v4, Lcom/millennialmedia/android/MMAdViewController;->overlayTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1119
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Accelerometer on?: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewController$5;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    iget-boolean v4, v4, Lcom/millennialmedia/android/MMAdViewController;->canAccelerate:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1121
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1122
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1127
    :cond_15
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1128
    const/high16 v2, 0x2400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1129
    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method
