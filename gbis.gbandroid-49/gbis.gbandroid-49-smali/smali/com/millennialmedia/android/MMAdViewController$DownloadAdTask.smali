.class Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/android/MMAdViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadAdTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/millennialmedia/android/VideoAd;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/millennialmedia/android/MMAdViewController;


# direct methods
.method constructor <init>(Lcom/millennialmedia/android/MMAdViewController;)V
    .locals 2
    .parameter

    .prologue
    .line 1456
    iput-object p1, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1457
    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {p1}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 1458
    if-nez v0, :cond_0

    .line 1459
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The reference to the ad view was broken."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    :goto_0
    return-void

    .line 1461
    :cond_0
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/HandShake;->sharedHandShake(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;

    move-result-object v1

    iget-object v0, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/HandShake;->lockAdTypeDownload(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1452
    check-cast p1, [Lcom/millennialmedia/android/VideoAd;

    invoke-virtual {p0, p1}, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->doInBackground([Lcom/millennialmedia/android/VideoAd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/millennialmedia/android/VideoAd;)Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1487
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 1489
    if-nez v0, :cond_0

    .line 1491
    const-string v0, "MillennialMediaSDK"

    const-string v2, "The reference to the ad view was broken."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1534
    :goto_0
    return-object v0

    .line 1495
    :cond_0
    if-eqz p1, :cond_1

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 1496
    goto :goto_0

    .line 1498
    :cond_2
    aget-object v2, p1, v3

    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/millennialmedia/android/MMAdViewController;->initCachedAdDirectory(Lcom/millennialmedia/android/VideoAd;Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    .line 1499
    if-nez v4, :cond_3

    move-object v0, v1

    .line 1500
    goto :goto_0

    .line 1502
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downloading content to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1505
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->contentUrl:Ljava/lang/String;

    const-string v2, "video.dat"

    invoke-static {v1, v2, v4}, Lcom/millennialmedia/android/MMAdViewController;->downloadComponent(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z

    move-result v1

    .line 1506
    if-nez v1, :cond_4

    .line 1508
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MillennialMediaSettings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1509
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1510
    const-string v1, "pendingDownload"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1511
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1512
    aget-object v0, p1, v3

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    goto :goto_0

    :cond_4
    move v2, v3

    .line 1516
    :goto_1
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    .line 1518
    aget-object v1, p1, v3

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/android/VideoImage;

    .line 1519
    iget-object v5, v1, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/millennialmedia/android/VideoImage;->getImageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1, v4}, Lcom/millennialmedia/android/MMAdViewController;->downloadComponent(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z

    move-result v1

    .line 1520
    if-nez v1, :cond_5

    .line 1522
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MillennialMediaSettings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1523
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1524
    const-string v1, "pendingDownload"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1525
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1526
    aget-object v0, p1, v3

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 1516
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1530
    :cond_6
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MillennialMediaSettings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1531
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1532
    const-string v1, "pendingDownload"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1533
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1534
    aget-object v0, p1, v3

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1452
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 9
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1549
    .line 1551
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 1552
    if-nez v0, :cond_0

    .line 1554
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The reference to the ad view was broken."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1609
    :goto_0
    return-void

    .line 1557
    :cond_0
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "MillennialMediaSettings"

    invoke-virtual {v3, v5, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1558
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 1559
    if-eqz p1, :cond_2

    .line 1561
    const-string v3, "lastDownloadedAdName"

    invoke-interface {v6, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1562
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Download complete. LastDownloadedAdName: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1563
    const-string v3, "pendingDownload"

    invoke-interface {v5, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 1573
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/millennialmedia/android/HandShake;->sharedHandShake(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;

    move-result-object v3

    iget-object v7, v0, Lcom/millennialmedia/android/MMAdView;->adType:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/millennialmedia/android/HandShake;->unlockAdTypeDownload(Ljava/lang/String;)V

    .line 1577
    :try_start_0
    new-instance v3, Lcom/millennialmedia/android/AdDatabaseHelper;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Lcom/millennialmedia/android/AdDatabaseHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1578
    :try_start_1
    invoke-virtual {v3, p1}, Lcom/millennialmedia/android/AdDatabaseHelper;->getVideoAd(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;

    move-result-object v4

    .line 1579
    if-eqz v1, :cond_3

    .line 1581
    const-string v1, "downloadAttempts"

    const/4 v5, 0x0

    invoke-interface {v6, v1, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1582
    const-string v1, "Ad download completed successfully: TRUE"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1584
    const-string v1, "lastAdViewed"

    const/4 v5, 0x0

    invoke-interface {v6, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1585
    const-string v1, "Last ad viewed: FALSE"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1587
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, v4, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/millennialmedia/android/MMAdViewController;->cachedVideoWasAdded(Landroid/content/Context;Ljava/lang/String;)V

    .line 1588
    iget-object v1, v4, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    invoke-static {v1}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1604
    :goto_2
    invoke-virtual {v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    .line 1606
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1607
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adDoneCaching(Lcom/millennialmedia/android/MMAdView;Z)V
    invoke-static {v1, v0, v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1100(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V

    goto :goto_0

    .line 1570
    :cond_2
    const-string v1, "lastDownloadedAdName"

    invoke-interface {v6, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v1, v2

    .line 1571
    goto :goto_1

    .line 1592
    :cond_3
    :try_start_2
    const-string v1, "downloadAttempts"

    const/4 v7, 0x0

    invoke-interface {v5, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 1593
    const-string v5, "downloadAttempts"

    invoke-interface {v6, v5, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1594
    const-string v1, "Ad download completed successfully: FALSE"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1595
    iget-object v1, v4, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    invoke-static {v1}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1600
    :catch_0
    move-exception v1

    move-object v1, v3

    :goto_3
    :try_start_3
    const-string v3, "MillennialMediaSDK"

    const-string v4, "SQL error. Cannot complete ad download."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1604
    if-eqz v1, :cond_4

    .line 1605
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    .line 1606
    :cond_4
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1607
    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adDoneCaching(Lcom/millennialmedia/android/MMAdView;Z)V
    invoke-static {v1, v0, v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1100(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V

    goto/16 :goto_0

    .line 1604
    :catchall_0
    move-exception v1

    move-object v3, v4

    :goto_4
    if-eqz v3, :cond_5

    .line 1605
    invoke-virtual {v3}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    .line 1606
    :cond_5
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1607
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #calls: Lcom/millennialmedia/android/MMAdViewController;->adDoneCaching(Lcom/millennialmedia/android/MMAdView;Z)V
    invoke-static {v3, v0, v2}, Lcom/millennialmedia/android/MMAdViewController;->access$1100(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V

    throw v1

    .line 1604
    :catchall_1
    move-exception v1

    goto :goto_4

    :catchall_2
    move-exception v3

    move-object v8, v3

    move-object v3, v1

    move-object v1, v8

    goto :goto_4

    .line 1600
    :catch_1
    move-exception v1

    move-object v1, v4

    goto :goto_3
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewController$DownloadAdTask;->this$0:Lcom/millennialmedia/android/MMAdViewController;

    #getter for: Lcom/millennialmedia/android/MMAdViewController;->adViewRef:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewController;->access$100(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/MMAdView;

    .line 1469
    if-nez v0, :cond_0

    .line 1471
    const-string v0, "MillennialMediaSDK"

    const-string v1, "The reference to the ad view was broken."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    :goto_0
    return-void

    .line 1474
    :cond_0
    const-string v1, "DownloadAdTask onPreExecute"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1475
    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "MillennialMediaSettings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1476
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1477
    const-string v1, "pendingDownload"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1478
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1479
    const-string v0, "Setting pendingDownload to TRUE"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onProgressUpdate()V
    .locals 0

    .prologue
    .line 1542
    return-void
.end method
