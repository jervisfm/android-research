.class Lcom/millennialmedia/android/MMAdViewWebOverlay;
.super Landroid/widget/FrameLayout;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient;,
        Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayJSInterface;,
        Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;
    }
.end annotation


# static fields
.field private static final kTitleMarginX:I = 0x8

.field private static final kTitleMarginY:I = 0x9

.field private static final kTransitionDuration:I = 0xc8


# instance fields
.field private backButton:Landroid/widget/Button;

.field private close:Landroid/graphics/drawable/Drawable;

.field private closeDisabled:Landroid/graphics/drawable/Drawable;

.field private content:Landroid/widget/LinearLayout;

.field private forwardButton:Landroid/widget/Button;

.field private leftArrow:Landroid/graphics/drawable/Drawable;

.field private leftArrowDisabled:Landroid/graphics/drawable/Drawable;

.field private navBar:Landroid/widget/RelativeLayout;

.field private navCloseButton:Landroid/widget/Button;

.field private overlayUrl:Ljava/lang/String;

.field private rightArrow:Landroid/graphics/drawable/Drawable;

.field private rightArrowDisabled:Landroid/graphics/drawable/Drawable;

.field private title:Landroid/widget/TextView;

.field viewHandler:Landroid/os/Handler;

.field protected webView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Landroid/content/Context;IJLjava/lang/String;ZLjava/lang/String;ZZZ)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 539
    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$8;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$8;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    iput-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->viewHandler:Landroid/os/Handler;

    .line 95
    const/16 v1, 0x3ad6

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->setId(I)V

    .line 96
    if-nez p1, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v1, p1

    .line 99
    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;

    .line 100
    if-eqz v1, :cond_2

    .line 102
    iget-boolean v0, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->bottomBarVisible:Z

    move/from16 p8, v0

    .line 103
    iget-boolean v0, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->bottomBarEnabled:Z

    move/from16 p9, v0

    .line 104
    iget-object v2, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->webView:Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    .line 108
    :cond_2
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v2}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 110
    const/high16 v3, 0x3d80

    mul-float/2addr v3, v2

    int-to-float v4, p2

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 111
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v4, v5, v6, v3}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->setPadding(IIII)V

    .line 113
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    .line 114
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 115
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->addView(Landroid/view/View;)V

    .line 119
    if-eqz p6, :cond_3

    .line 121
    new-instance v3, Landroid/widget/RelativeLayout;

    invoke-direct {v3, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 122
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    const/high16 v4, -0x100

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 124
    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 125
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    .line 126
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    const/high16 v5, -0x100

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 129
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 130
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, 0x8

    const/16 v8, 0x9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 131
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 136
    new-instance v4, Landroid/widget/Button;

    invoke-direct {v4, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 137
    const/high16 v5, -0x100

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 138
    const-string v5, "Close"

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setTextColor(I)V

    .line 140
    new-instance v5, Lcom/millennialmedia/android/MMAdViewWebOverlay$1;

    invoke-direct {v5, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$1;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 158
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 159
    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 160
    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 167
    :cond_3
    new-instance v3, Landroid/webkit/WebView;

    invoke-direct {v3, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    .line 168
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setId(I)V

    .line 169
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 170
    const/high16 v4, 0x3f80

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 171
    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v4, v3}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    new-instance v4, Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient;

    invoke-direct {v4, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 173
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    new-instance v4, Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayJSInterface;

    invoke-direct {v4, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$OverlayJSInterface;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    const-string v5, "interface"

    invoke-virtual {v3, v4, v5}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    .line 175
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 176
    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 178
    if-eqz p10, :cond_4

    .line 180
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 181
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 188
    :goto_1
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 194
    const/high16 v3, 0x4248

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f00

    add-float/2addr v2, v3

    float-to-int v3, v2

    .line 195
    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    .line 196
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    const v4, -0x333334

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 198
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    const/16 v4, 0x12c

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 199
    new-instance v2, Landroid/widget/Button;

    invoke-direct {v2, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    .line 200
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    const/high16 v4, -0x100

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    .line 206
    :try_start_0
    const-string v4, "millennial_close.png"

    invoke-virtual {v2, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    .line 207
    const-string v5, "millennial_close.png"

    invoke-static {v4, v5}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->close:Landroid/graphics/drawable/Drawable;

    .line 208
    const-string v4, "millennial_close_disabled.png"

    invoke-virtual {v2, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 209
    const-string v4, "millennial_close_disabled.png"

    invoke-static {v2, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->closeDisabled:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_2
    move/from16 v0, p9

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->setCloseButtonListener(Z)V

    .line 216
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 217
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 218
    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 219
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    invoke-virtual {v3, v4, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 290
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 291
    if-eqz p8, :cond_5

    .line 292
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 295
    :goto_3
    if-nez v1, :cond_0

    .line 296
    invoke-direct {p0, p5, p3, p4}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->animateView(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 185
    :cond_4
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 186
    iget-object v3, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->content:Landroid/widget/LinearLayout;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 211
    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 294
    :cond_5
    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/millennialmedia/android/MMAdViewWebOverlay;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/millennialmedia/android/MMAdViewWebOverlay;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->dismiss(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/millennialmedia/android/MMAdViewWebOverlay;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private animateView(Ljava/lang/String;J)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const v4, 0x3f666666

    const/high16 v8, 0x3f00

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 306
    if-nez p1, :cond_0

    .line 307
    const-string p1, "bottomtotop"

    .line 308
    :cond_0
    const-string v0, "toptobottom"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, -0x4080

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 315
    invoke-virtual {v0, p2, p3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 316
    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$2;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$2;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 327
    const-string v1, "Translate down"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 376
    :goto_0
    return-void

    .line 330
    :cond_1
    const-string v0, "explode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    const v3, 0x3f8ccccd

    const v5, 0x3dcccccd

    move v6, v4

    move v7, v1

    move v9, v1

    move v10, v8

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 338
    invoke-virtual {v2, p2, p3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 339
    new-instance v0, Lcom/millennialmedia/android/MMAdViewWebOverlay$3;

    invoke-direct {v0, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$3;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v2, v0}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 349
    const-string v0, "Explode"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0, v2}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 357
    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, 0x3f80

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 362
    invoke-virtual {v0, p2, p3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 363
    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$4;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$4;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 373
    const-string v1, "Translate up"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private dismiss(Z)V
    .locals 4
    .parameter

    .prologue
    .line 519
    const-string v0, "Ad overlay closed"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 520
    invoke-virtual {p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 521
    if-nez v0, :cond_0

    .line 536
    :goto_0
    return-void

    .line 525
    :cond_0
    if-eqz p1, :cond_1

    .line 527
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 528
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 529
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 530
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 534
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method getNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 422
    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;

    invoke-direct {v1, v2}, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay$1;)V

    .line 423
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 425
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 426
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->bottomBarVisible:Z

    .line 427
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isEnabled()Z

    move-result v0

    iput-boolean v0, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->bottomBarEnabled:Z

    .line 428
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    iput-object v0, v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$NonConfigurationInstance;->webView:Landroid/webkit/WebView;

    .line 429
    return-object v1

    .line 426
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method goBack()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 387
    const/4 v0, 0x1

    .line 389
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method injectJS(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 413
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 414
    return-void
.end method

.method loadWebContent(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 398
    iput-object p1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->overlayUrl:Ljava/lang/String;

    .line 399
    invoke-virtual {p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK;->isConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->overlayUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 408
    :goto_0
    return-void

    .line 406
    :cond_0
    const-string v0, "MillennialMediaSDK"

    const-string v1, "No network available, can\'t load overlay."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected setBackButtonListener(Z)V
    .locals 2
    .parameter

    .prologue
    .line 465
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 467
    if-eqz p1, :cond_1

    .line 469
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->leftArrow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 470
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$6;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$6;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 484
    :cond_0
    :goto_0
    return-void

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->leftArrowDisabled:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 481
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->backButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected setCloseButtonListener(Z)V
    .locals 2
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 494
    if-eqz p1, :cond_1

    .line 496
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->close:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 497
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$7;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$7;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 503
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 511
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->closeDisabled:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 508
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->navCloseButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected setForwardButtonListener(Z)V
    .locals 2
    .parameter

    .prologue
    .line 438
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 440
    if-eqz p1, :cond_1

    .line 442
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->rightArrow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 443
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    new-instance v1, Lcom/millennialmedia/android/MMAdViewWebOverlay$5;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/MMAdViewWebOverlay$5;-><init>(Lcom/millennialmedia/android/MMAdViewWebOverlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->rightArrowDisabled:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 454
    iget-object v0, p0, Lcom/millennialmedia/android/MMAdViewWebOverlay;->forwardButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
