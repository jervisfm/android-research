.class Lcom/millennialmedia/android/MMNotification$1;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/millennialmedia/android/MMNotification;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$finalArguments:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/millennialmedia/android/MMNotification;Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/millennialmedia/android/MMNotification$1;->this$0:Lcom/millennialmedia/android/MMNotification;

    iput-object p2, p0, Lcom/millennialmedia/android/MMNotification$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 36
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/millennialmedia/android/MMNotification$1;->val$context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 37
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "title"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "title"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "message"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "message"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "cancelButton"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    const/4 v2, -0x2

    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v3, "cancelButton"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/millennialmedia/android/MMNotification$1;->this$0:Lcom/millennialmedia/android/MMNotification;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "buttons"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 45
    iget-object v0, p0, Lcom/millennialmedia/android/MMNotification$1;->val$finalArguments:Ljava/util/HashMap;

    const-string v2, "buttons"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 46
    array-length v2, v0

    if-lez v2, :cond_3

    .line 47
    const/4 v2, -0x3

    const/4 v3, 0x0

    aget-object v3, v0, v3

    iget-object v4, p0, Lcom/millennialmedia/android/MMNotification$1;->this$0:Lcom/millennialmedia/android/MMNotification;

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 48
    :cond_3
    array-length v2, v0

    if-le v2, v5, :cond_4

    .line 49
    const/4 v2, -0x1

    aget-object v0, v0, v5

    iget-object v3, p0, Lcom/millennialmedia/android/MMNotification$1;->this$0:Lcom/millennialmedia/android/MMNotification;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 51
    :cond_4
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 52
    return-void
.end method
