.class Lcom/millennialmedia/android/PreCacheWorker;
.super Ljava/lang/Thread;
.source "GBFile"


# static fields
.field private static busy:Z

.field private static currentDownload:Ljava/lang/String;


# instance fields
.field private appContext:Landroid/content/Context;

.field private cachedVideos:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, ""

    sput-object v0, Lcom/millennialmedia/android/PreCacheWorker;->currentDownload:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>([Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/millennialmedia/android/PreCacheWorker;->cachedVideos:[Ljava/lang/String;

    .line 44
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/android/PreCacheWorker;->appContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static isCurrentlyDownloading(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 38
    sget-object v0, Lcom/millennialmedia/android/PreCacheWorker;->currentDownload:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static declared-synchronized preCacheVideos([Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 29
    const-class v1, Lcom/millennialmedia/android/PreCacheWorker;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/millennialmedia/android/PreCacheWorker;->busy:Z

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/millennialmedia/android/PreCacheWorker;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/android/PreCacheWorker;-><init>([Ljava/lang/String;Landroid/content/Context;)V

    .line 32
    invoke-virtual {v0}, Lcom/millennialmedia/android/PreCacheWorker;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_0
    monitor-exit v1

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 53
    iget-object v6, p0, Lcom/millennialmedia/android/PreCacheWorker;->cachedVideos:[Ljava/lang/String;

    array-length v7, v6

    move v5, v2

    :goto_0
    if-ge v5, v7, :cond_a

    aget-object v0, v6, v5

    .line 57
    :try_start_0
    new-instance v1, Lcom/millennialmedia/android/HttpGetRequest;

    invoke-direct {v1}, Lcom/millennialmedia/android/HttpGetRequest;-><init>()V

    .line 58
    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/HttpGetRequest;->get(Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 59
    if-nez v0, :cond_1

    .line 61
    const-string v0, "Pre cache worker: HTTP response is null"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 53
    :cond_0
    :goto_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 64
    :cond_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 67
    if-nez v0, :cond_2

    .line 69
    const-string v0, "Pre cache worker: Null HTTP entity"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 65
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Pre cache worker HTTP error: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 73
    :cond_2
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    const-wide/16 v8, 0x0

    cmp-long v1, v3, v8

    if-nez v1, :cond_3

    .line 75
    const-string v0, "Pre cache worker: Millennial ad return failed. Zero content length returned."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :cond_3
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_0

    .line 82
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 84
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v3, "application/json"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/HttpGetRequest;->convertStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 92
    new-instance v8, Lcom/millennialmedia/android/VideoAd;

    invoke-direct {v8, v0}, Lcom/millennialmedia/android/VideoAd;-><init>(Ljava/lang/String;)V

    .line 94
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, v8, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 100
    :cond_4
    invoke-virtual {v8}, Lcom/millennialmedia/android/VideoAd;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/millennialmedia/android/PreCacheWorker;->appContext:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/millennialmedia/android/MMAdViewController;->initCachedAdDirectory(Lcom/millennialmedia/android/VideoAd;Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pre cache worker: Downloading content to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 107
    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->contentUrl:Ljava/lang/String;

    const-string v1, "video.dat"

    invoke-static {v0, v1, v4}, Lcom/millennialmedia/android/MMAdViewController;->downloadComponent(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z

    move-result v0

    .line 108
    if-eqz v0, :cond_b

    move v1, v2

    .line 111
    :goto_2
    iget-object v3, v8, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_b

    .line 113
    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 114
    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/millennialmedia/android/VideoImage;->getImageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v4}, Lcom/millennialmedia/android/MMAdViewController;->downloadComponent(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z

    move-result v3

    .line 115
    if-eqz v3, :cond_5

    .line 116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_2

    .line 97
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const-string v0, "Pre cache worker: Millennial ad return failed. Invalid response data."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 98
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const-string v0, "Pre cache worker: Millennial ad return failed. Invalid response data."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    move v4, v3

    .line 120
    :goto_3
    if-nez v4, :cond_6

    .line 122
    iget-object v0, p0, Lcom/millennialmedia/android/PreCacheWorker;->appContext:Landroid/content/Context;

    iget-object v1, v8, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/millennialmedia/android/MMAdViewController;->deleteAd(Landroid/content/Context;Ljava/lang/String;)V

    .line 123
    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->cacheFailed:[Ljava/lang/String;

    invoke-static {v0}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V

    .line 144
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pre cache worker: Caching completed successfully? "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 127
    :cond_6
    const/4 v3, 0x0

    .line 128
    const-string v0, "Pre cache worker: Storing ad in db."

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 131
    :try_start_2
    new-instance v1, Lcom/millennialmedia/android/AdDatabaseHelper;

    iget-object v0, p0, Lcom/millennialmedia/android/PreCacheWorker;->appContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/millennialmedia/android/AdDatabaseHelper;-><init>(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3

    .line 132
    :try_start_3
    invoke-virtual {v1, v8}, Lcom/millennialmedia/android/AdDatabaseHelper;->storeAd(Lcom/millennialmedia/android/VideoAd;)V

    .line 133
    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 134
    iget-object v0, p0, Lcom/millennialmedia/android/PreCacheWorker;->appContext:Landroid/content/Context;

    iget-object v3, v8, Lcom/millennialmedia/android/VideoAd;->acid:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/millennialmedia/android/MMAdViewController;->cachedVideoWasAdded(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_4

    .line 138
    :cond_7
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    .line 141
    :cond_8
    :goto_5
    iget-object v0, v8, Lcom/millennialmedia/android/VideoAd;->cacheComplete:[Ljava/lang/String;

    invoke-static {v0}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V

    goto :goto_4

    .line 135
    :catch_3
    move-exception v0

    move-object v1, v3

    :goto_6
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 138
    if-eqz v1, :cond_8

    .line 139
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    goto :goto_5

    .line 138
    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_7
    if-eqz v1, :cond_9

    .line 139
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    :cond_9
    throw v0

    .line 151
    :cond_a
    sput-boolean v2, Lcom/millennialmedia/android/PreCacheWorker;->busy:Z

    .line 152
    return-void

    .line 138
    :catchall_1
    move-exception v0

    goto :goto_7

    .line 135
    :catch_4
    move-exception v0

    goto :goto_6

    :cond_b
    move v4, v0

    goto :goto_3
.end method
