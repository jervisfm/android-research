.class final Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;
.super Ljava/lang/Thread;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/android/VideoPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "NetworkingThread"
.end annotation


# instance fields
.field locationString:Ljava/lang/String;

.field mimeTypeString:Ljava/lang/String;

.field final synthetic this$0:Lcom/millennialmedia/android/VideoPlayer;

.field urlString:Ljava/lang/String;

.field userAgent:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/millennialmedia/android/VideoPlayer;)V
    .locals 1
    .parameter

    .prologue
    .line 1065
    iput-object p1, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->this$0:Lcom/millennialmedia/android/VideoPlayer;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1067
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->userAgent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1070
    .line 1071
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->mimeTypeString:Ljava/lang/String;

    .line 1072
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->locationString:Ljava/lang/String;

    .line 1077
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->locationString:Ljava/lang/String;

    .line 1078
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1079
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 1080
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 1082
    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 1083
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->userAgent:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1084
    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->userAgent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    :cond_1
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 1086
    const-string v1, "Location"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    .line 1087
    const-string v1, "Content-Type"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->mimeTypeString:Ljava/lang/String;

    .line 1088
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 1090
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Response Code:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Response Message:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1091
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "urlString: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1106
    const/16 v0, 0x12c

    if-lt v1, v0, :cond_2

    const/16 v0, 0x190

    if-lt v1, v0, :cond_0

    .line 1107
    :cond_2
    :goto_0
    return-void

    .line 1101
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1093
    :catch_1
    move-exception v0

    goto :goto_0
.end method
