.class public Lcom/millennialmedia/android/VideoPlayer;
.super Landroid/app/Activity;
.source "GBFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/android/VideoPlayer$VideoServer;,
        Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;,
        Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;,
        Lcom/millennialmedia/android/VideoPlayer$DelayedAnimationListener;
    }
.end annotation


# static fields
.field private static final MESSAGE_DELAYED_BUTTON:I = 0x3

.field private static final MESSAGE_EVENTLOG_CHECK:I = 0x2

.field private static final MESSAGE_INACTIVITY_ANIMATION:I = 0x1


# instance fields
.field private buttonsLayout:Landroid/widget/RelativeLayout;

.field private controlsLayout:Landroid/widget/RelativeLayout;

.field private current:Ljava/lang/String;

.field private currentVideoPosition:I

.field private handler:Landroid/os/Handler;

.field private hudSeconds:Landroid/widget/TextView;

.field private hudStaticText:Landroid/widget/TextView;

.field private isCachedAd:Z

.field private lastVideoPosition:I

.field private logSet:Lcom/millennialmedia/android/EventLogSet;

.field private mPausePlay:Landroid/widget/Button;

.field private mRewind:Landroid/widget/Button;

.field private mStop:Landroid/widget/Button;

.field private mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

.field private paused:Z

.field private receiver:Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;

.field private relLayout:Landroid/widget/RelativeLayout;

.field private showBottomBar:Z

.field private showCountdownHud:Z

.field private videoAd:Lcom/millennialmedia/android/VideoAd;

.field private videoCompleted:Z

.field private videoCompletedOnce:Z

.field protected videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

.field private waitForUserPresent:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 76
    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    .line 80
    iput-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->showBottomBar:Z

    .line 82
    iput v0, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    .line 84
    iput-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    .line 1430
    return-void
.end method

.method static synthetic access$100(Lcom/millennialmedia/android/VideoPlayer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/android/VideoPlayer;->dispatchButtonClick(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/millennialmedia/android/VideoPlayer;)Lcom/millennialmedia/android/VideoAd;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/millennialmedia/android/VideoPlayer;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/millennialmedia/android/VideoPlayer;->videoPlayerOnCompletion(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1202(Lcom/millennialmedia/android/VideoPlayer;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/millennialmedia/android/VideoPlayer;->waitForUserPresent:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/millennialmedia/android/VideoPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->resumeVideo()V

    return-void
.end method

.method static synthetic access$200(Lcom/millennialmedia/android/VideoPlayer;)Lcom/millennialmedia/android/MillennialMediaView;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/millennialmedia/android/VideoPlayer;)Z
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    return v0
.end method

.method static synthetic access$302(Lcom/millennialmedia/android/VideoPlayer;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    return p1
.end method

.method static synthetic access$400(Lcom/millennialmedia/android/VideoPlayer;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/millennialmedia/android/VideoPlayer;->playVideo(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/millennialmedia/android/VideoPlayer;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$602(Lcom/millennialmedia/android/VideoPlayer;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/millennialmedia/android/VideoPlayer;->current:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/millennialmedia/android/VideoPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->dismiss()V

    return-void
.end method

.method static synthetic access$800(Lcom/millennialmedia/android/VideoPlayer;)Z
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    return v0
.end method

.method static synthetic access$900(Lcom/millennialmedia/android/VideoPlayer;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->hideHud()V

    return-void
.end method

.method private canFadeButtons()Z
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->stayInPlayer:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    if-eqz v0, :cond_0

    .line 376
    const/4 v0, 0x0

    .line 377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dismiss()V
    .locals 1

    .prologue
    .line 909
    const-string v0, "Video ad player closed"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 910
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->stopPlayback()V

    .line 913
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->finish()V

    .line 914
    return-void
.end method

.method private dispatchButtonClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .parameter
    .parameter

    .prologue
    const/4 v12, 0x2

    const/4 v3, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Button Clicked: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1118
    if-eqz p1, :cond_5

    .line 1120
    const-string v0, "mmsdk"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1122
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1123
    if-eqz v0, :cond_8

    .line 1125
    const-string v1, "restartVideo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1127
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v0, :cond_4

    .line 1129
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_4

    .line 1131
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v4, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    .line 1132
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    .line 1134
    if-eqz v4, :cond_4

    .line 1137
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 1138
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 1139
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1141
    iput v2, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    move v1, v2

    .line 1143
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "i: "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1146
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 1149
    if-eqz v0, :cond_3

    .line 1151
    iget-wide v5, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_0

    .line 1153
    iget-object v5, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    .line 1154
    iget-object v6, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 1155
    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v6, 0x3

    invoke-static {v5, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 1156
    iget-object v6, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    invoke-virtual {v6, v5, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1160
    :cond_0
    iget-wide v5, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_1

    .line 1162
    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-static {v5, v11, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 1163
    iget-object v6, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    iget-wide v9, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    add-long/2addr v7, v9

    iget-wide v9, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    add-long/2addr v7, v9

    invoke-virtual {v6, v5, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1166
    :cond_1
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    if-eqz v0, :cond_2

    .line 1167
    invoke-direct {p0, v11}, Lcom/millennialmedia/android/VideoPlayer;->showHud(Z)V

    .line 1168
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 1169
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-static {v5, v12}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v5

    const-wide/16 v6, 0x3e8

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1143
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1177
    :cond_4
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_8

    .line 1179
    invoke-direct {p0, v2}, Lcom/millennialmedia/android/VideoPlayer;->playVideo(I)V

    .line 1332
    :cond_5
    :goto_1
    return-void

    .line 1183
    :cond_6
    const-string v1, "endVideo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1185
    const-string v0, "End"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1186
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_8

    .line 1188
    iput-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->current:Ljava/lang/String;

    .line 1189
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->stopPlayback()V

    .line 1190
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_5

    .line 1193
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->dismiss()V

    goto :goto_1

    .line 1200
    :cond_7
    const-string v0, "Unrecognized mmsdk:// URL"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1228
    :cond_8
    :try_start_0
    new-instance v0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;

    invoke-direct {v0, p0}, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    .line 1229
    iput-object p1, v0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->urlString:Ljava/lang/String;

    .line 1230
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->setPriority(I)V

    .line 1231
    invoke-virtual {v0}, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->start()V

    .line 1232
    invoke-virtual {v0}, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->join()V

    .line 1233
    iget-object v1, v0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->mimeTypeString:Ljava/lang/String;

    .line 1234
    iget-object v0, v0, Lcom/millennialmedia/android/VideoPlayer$NetworkingThread;->locationString:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    move-object v3, v1

    .line 1237
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "locationString: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1240
    if-eqz v0, :cond_5

    .line 1242
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1250
    if-nez v3, :cond_9

    .line 1251
    const-string v3, ""

    .line 1255
    :cond_9
    :try_start_1
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mmsdk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1257
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "endVideo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1259
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_5

    .line 1261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->current:Ljava/lang/String;

    .line 1262
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->stopPlayback()V

    .line 1264
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->dismiss()V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1327
    :catch_0
    move-exception v0

    .line 1329
    const-string v1, "MillennialMediaSDK"

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1204
    :cond_a
    const-string v0, "mmbrowser"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1206
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1207
    if-eqz v0, :cond_8

    .line 1209
    const-string v1, "Launch browser"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1212
    :try_start_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1213
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1214
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 1215
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 1235
    :catch_2
    move-exception v0

    move-object v0, v3

    goto/16 :goto_2

    .line 1268
    :cond_b
    :try_start_3
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_c
    const-string v1, "text/html"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1270
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/MMAdViewOverlayActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1271
    const/high16 v2, 0x2400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1272
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1273
    const-string v0, "cachedAdView"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1274
    const-string v0, "overlayOrientation"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1275
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/millennialmedia/android/VideoPlayer;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 1277
    :cond_d
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "market"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1279
    const-string v1, "Android Market URL, launch the Market Application"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1280
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1281
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1282
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1284
    :cond_e
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "rtsp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "video/mp4"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "video/3gpp"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1287
    :cond_f
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->playVideo(I)V

    goto/16 :goto_1

    .line 1289
    :cond_10
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tel"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1291
    const-string v1, "Telephone Number, launch the phone"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1292
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1293
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1294
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1296
    :cond_11
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1299
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/millennialmedia/android/MMAdViewOverlayActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1300
    const/high16 v2, 0x2400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1301
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1302
    const-string v0, "cachedAdView"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1303
    const-string v0, "overlayOrientation"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1304
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/millennialmedia/android/VideoPlayer;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 1306
    :cond_12
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mmbrowser"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1308
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1309
    if-eqz v0, :cond_5

    .line 1311
    const-string v1, "Launch browser"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1312
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1313
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1314
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1320
    :cond_13
    const-string v1, "Uncertain about content, launch to browser"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1322
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1323
    const/high16 v0, 0x2400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1324
    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1
.end method

.method private hideHud()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 673
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 676
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 677
    :cond_1
    return-void
.end method

.method private pauseVideo()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 950
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_1

    .line 952
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->pause()V

    .line 955
    iput-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    .line 956
    const-string v0, "Video paused"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 958
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    .line 961
    :cond_1
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v0, :cond_2

    .line 963
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 964
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 965
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 966
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->stopServer()V

    .line 968
    :cond_2
    return-void
.end method

.method private playVideo(I)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 687
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 689
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "playVideo path: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 690
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 692
    :cond_0
    const-string v0, "Sorry. There was a problem playing the video"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 693
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    invoke-static {v0}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V

    .line 841
    :cond_1
    :goto_0
    return-void

    .line 699
    :cond_2
    const-string v1, "MillennialMediaSettings"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/millennialmedia/android/VideoPlayer;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 700
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 701
    const-string v2, "lastAdViewed"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 702
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 703
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    .line 704
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->current:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v1, :cond_6

    .line 706
    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v1, :cond_5

    .line 708
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v1, :cond_1

    .line 711
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v1, v1, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z

    if-eqz v1, :cond_4

    .line 713
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$6;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$6;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 725
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$7;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$7;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 731
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$8;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$8;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 738
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/MillennialMediaView;->setVideoURI(Landroid/net/Uri;)V

    .line 739
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->requestFocus()Z

    .line 740
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/android/MillennialMediaView;->seekTo(I)V

    .line 741
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->start()V

    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 826
    :catch_0
    move-exception v0

    .line 828
    const-string v1, "MillennialMediaSDK"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 830
    const-string v0, "MillennialMediaSettings"

    invoke-virtual {p0, v0, v4}, Lcom/millennialmedia/android/VideoPlayer;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 831
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 832
    const-string v1, "lastAdViewed"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 833
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 835
    const-string v0, "Sorry. There was a problem playing the video"

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 836
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_3

    .line 837
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->stopPlayback()V

    .line 838
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_1

    .line 839
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->videoError:[Ljava/lang/String;

    invoke-static {v0}, Lcom/millennialmedia/android/HttpGetRequest;->log([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 747
    :cond_4
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v0, p1, v1}, Lcom/millennialmedia/android/VideoPlayer;->startServer(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 753
    :cond_5
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->requestFocus()Z

    .line 754
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/android/MillennialMediaView;->seekTo(I)V

    .line 755
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->start()V

    .line 756
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    goto/16 :goto_0

    .line 761
    :cond_6
    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->current:Ljava/lang/String;

    .line 763
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v1, :cond_9

    .line 765
    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v1, :cond_8

    .line 767
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v1, :cond_1

    .line 770
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v1, v1, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z

    if-nez v1, :cond_7

    .line 772
    const-string v1, "Cached Ad. Starting Server"

    invoke-static {v1}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 773
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/millennialmedia/android/VideoPlayer;->startServer(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    .line 778
    :cond_7
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$9;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$9;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 790
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$10;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$10;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 796
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$11;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$11;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Lcom/millennialmedia/android/MillennialMediaView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 803
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/MillennialMediaView;->setVideoURI(Landroid/net/Uri;)V

    .line 804
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->requestFocus()Z

    .line 805
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/android/MillennialMediaView;->seekTo(I)V

    .line 806
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->start()V

    .line 807
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    goto/16 :goto_0

    .line 813
    :cond_8
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/MillennialMediaView;->setVideoURI(Landroid/net/Uri;)V

    .line 814
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->requestFocus()Z

    .line 815
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/android/MillennialMediaView;->seekTo(I)V

    .line 816
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->start()V

    .line 817
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z

    goto/16 :goto_0

    .line 822
    :cond_9
    const-string v0, "MillennialMediaSDK"

    const-string v1, "Video Player is Null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private resumeVideo()V
    .locals 10

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    .line 973
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_5

    .line 975
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    if-nez v0, :cond_5

    .line 977
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_4

    .line 979
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 980
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    if-eqz v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-wide v0, v0, Lcom/millennialmedia/android/VideoAd;->duration:J

    iget v2, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    div-long/2addr v0, v6

    .line 983
    cmp-long v2, v0, v4

    if-lez v2, :cond_3

    .line 985
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 986
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 993
    :cond_0
    :goto_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 995
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 998
    iget-wide v2, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->indexOfChild(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    .line 1000
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 1001
    iget-wide v2, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    iget v7, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    int-to-long v7, v7

    sub-long/2addr v2, v7

    .line 1002
    cmp-long v7, v2, v4

    if-gez v7, :cond_1

    .line 1003
    const-wide/16 v2, 0x1f4

    .line 1004
    :cond_1
    iget-object v7, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v7, v6, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1007
    :goto_2
    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    cmp-long v6, v6, v4

    if-lez v6, :cond_2

    .line 1009
    iget-object v6, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v7, 0x1

    invoke-static {v6, v7, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 1010
    iget-object v7, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v8, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    add-long/2addr v2, v8

    iget-wide v8, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    add-long/2addr v2, v8

    invoke-virtual {v7, v6, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 993
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 990
    :cond_3
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->hideHud()V

    goto :goto_0

    .line 1014
    :cond_4
    iget v0, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    invoke-direct {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->playVideo(I)V

    .line 1017
    :cond_5
    return-void

    :cond_6
    move-wide v2, v4

    goto :goto_2
.end method

.method private setButtonAlpha(Landroid/widget/ImageButton;F)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 382
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 383
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 384
    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 385
    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 386
    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 387
    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 388
    return-void
.end method

.method private showHud(Z)V
    .locals 10
    .parameter

    .prologue
    const/16 v9, 0xa

    const/4 v8, -0x1

    const-wide/16 v6, 0x3e8

    const/4 v2, -0x2

    const/4 v5, 0x0

    .line 620
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    if-nez v0, :cond_4

    .line 622
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 623
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 625
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    .line 626
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    const-string v3, " seconds remaining ..."

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 628
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v2, v5, v5, v3, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 630
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    .line 631
    if-eqz p1, :cond_2

    .line 633
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v2, :cond_1

    .line 634
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-wide v3, v3, Lcom/millennialmedia/android/VideoAd;->duration:J

    div-long/2addr v3, v6

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 645
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    const/16 v3, 0x191

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 646
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    const/16 v3, 0x192

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 649
    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 650
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 651
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    invoke-virtual {v2, v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 653
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 654
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v1, v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 655
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 669
    :goto_1
    return-void

    .line 638
    :cond_2
    iget v2, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    if-eqz v2, :cond_3

    .line 639
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    iget v3, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    div-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 640
    :cond_3
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v2, :cond_1

    .line 641
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-wide v3, v3, Lcom/millennialmedia/android/VideoAd;->duration:J

    div-long/2addr v3, v6

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 659
    :cond_4
    if-eqz p1, :cond_5

    .line 661
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_6

    .line 662
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-wide v1, v1, Lcom/millennialmedia/android/VideoAd;->duration:J

    div-long/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 666
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudStaticText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 664
    :cond_6
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private videoPlayerOnCompletion(Ljava/lang/String;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1025
    iput-boolean v6, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompletedOnce:Z

    iput-boolean v6, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    .line 1026
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->isInErrorState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1027
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->logSet:Lcom/millennialmedia/android/EventLogSet;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->logEndEvent(Lcom/millennialmedia/android/EventLogSet;)V

    .line 1028
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->stopServer()V

    .line 1029
    const-string v0, "Video player on complete"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1030
    if-eqz p1, :cond_1

    .line 1032
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/android/VideoPlayer;->dispatchButtonClick(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_2

    .line 1036
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->stayInPlayer:Z

    if-nez v0, :cond_3

    .line 1038
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->dismiss()V

    .line 1063
    :cond_2
    :goto_0
    return-void

    .line 1042
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    move v2, v3

    .line 1045
    :goto_1
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1047
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 1048
    iget-object v1, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget v4, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {p0, v1, v4}, Lcom/millennialmedia/android/VideoPlayer;->setButtonAlpha(Landroid/widget/ImageButton;F)V

    .line 1051
    iget-object v1, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1052
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v4, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget-object v5, v0, Lcom/millennialmedia/android/VideoImage;->layoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    move v4, v3

    .line 1053
    :goto_2
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v4, v1, :cond_5

    .line 1054
    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/android/VideoImage;

    iget-object v1, v1, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->bringChildToFront(Landroid/view/View;)V

    .line 1053
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 1055
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Button: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " alpha: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 1045
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1058
    :cond_6
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1059
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1060
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 503
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_4

    .line 505
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 508
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 510
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 511
    iget-object v2, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget v3, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {p0, v2, v3}, Lcom/millennialmedia/android/VideoPlayer;->setButtonAlpha(Landroid/widget/ImageButton;F)V

    .line 512
    iget-wide v2, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 515
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    invoke-static {v2, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 516
    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v4, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 508
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 521
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v6, :cond_3

    .line 523
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->canFadeButtons()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 525
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    iget v3, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    iget v4, v0, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    invoke-direct {v2, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 526
    iget-wide v3, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 527
    invoke-virtual {v2, v6}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 528
    invoke-virtual {v2, v6}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 529
    invoke-virtual {v2, v6}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 530
    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 533
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 535
    iget-object v2, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {p0, v2, v0}, Lcom/millennialmedia/android/VideoPlayer;->setButtonAlpha(Landroid/widget/ImageButton;F)V

    goto :goto_1

    .line 540
    :cond_4
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 11
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x1

    .line 411
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 495
    :cond_0
    :goto_0
    return v10

    .line 414
    :pswitch_0
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->canFadeButtons()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 417
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    iget v2, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    iget v3, v0, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 418
    iget-wide v2, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 419
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 420
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 421
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 422
    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 426
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 428
    :try_start_0
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 429
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v2, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->layoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :cond_1
    :goto_1
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    iget v2, v0, Lcom/millennialmedia/android/VideoImage;->toAlpha:F

    iget v3, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 432
    iget-wide v2, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 433
    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$DelayedAnimationListener;

    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget-object v4, v0, Lcom/millennialmedia/android/VideoImage;->layoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, p0, v3, v4}, Lcom/millennialmedia/android/VideoPlayer$DelayedAnimationListener;-><init>(Lcom/millennialmedia/android/VideoPlayer;Landroid/widget/ImageButton;Landroid/widget/RelativeLayout$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 434
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 435
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 436
    invoke-virtual {v1, v10}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 437
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Beginning animation to visibility. Fade duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Button: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 438
    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 430
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 443
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 445
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->getCurrentPosition()I

    move-result v5

    .line 446
    iget v0, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    if-le v5, v0, :cond_5

    .line 448
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_4

    .line 450
    iget v0, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    if-nez v0, :cond_2

    .line 451
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->logSet:Lcom/millennialmedia/android/EventLogSet;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->logBeginEvent(Lcom/millennialmedia/android/EventLogSet;)V

    :cond_2
    move v2, v3

    .line 452
    :goto_2
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 454
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->activities:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoLogEvent;

    .line 455
    if-eqz v0, :cond_3

    .line 457
    iget-wide v6, v0, Lcom/millennialmedia/android/VideoLogEvent;->position:J

    iget v1, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    int-to-long v8, v1

    cmp-long v1, v6, v8

    if-ltz v1, :cond_3

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoLogEvent;->position:J

    int-to-long v8, v5

    cmp-long v1, v6, v8

    if-gez v1, :cond_3

    move v4, v3

    .line 459
    :goto_3
    iget-object v1, v0, Lcom/millennialmedia/android/VideoLogEvent;->activities:[Ljava/lang/String;

    array-length v1, v1
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2

    if-ge v4, v1, :cond_3

    .line 463
    :try_start_2
    iget-object v1, v0, Lcom/millennialmedia/android/VideoLogEvent;->activities:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->logEvent(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    .line 459
    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 464
    :catch_1
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_4

    .line 491
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 452
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 470
    :cond_4
    :try_start_4
    iput v5, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    .line 474
    :cond_5
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    if-eqz v0, :cond_6

    .line 476
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-wide v0, v0, Lcom/millennialmedia/android/VideoAd;->duration:J

    int-to-long v2, v5

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 477
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_7

    .line 479
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 480
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->hudSeconds:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 484
    :cond_7
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->hideHud()V
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    .line 411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected logBeginEvent(Lcom/millennialmedia/android/EventLogSet;)V
    .locals 2
    .parameter

    .prologue
    .line 550
    if-eqz p1, :cond_0

    .line 552
    iget-object v0, p1, Lcom/millennialmedia/android/EventLogSet;->startActivity:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 556
    :try_start_0
    const-string v0, "Cached video begin event logged"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 557
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lcom/millennialmedia/android/EventLogSet;->startActivity:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 558
    iget-object v1, p1, Lcom/millennialmedia/android/EventLogSet;->startActivity:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->logEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 557
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 560
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 563
    :cond_0
    return-void
.end method

.method protected logButtonEvent(Lcom/millennialmedia/android/VideoImage;)V
    .locals 2
    .parameter

    .prologue
    .line 587
    const-string v0, "Cached video button event logged"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 589
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p1, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 590
    iget-object v1, p1, Lcom/millennialmedia/android/VideoImage;->activity:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->logEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 594
    :cond_0
    return-void
.end method

.method protected logEndEvent(Lcom/millennialmedia/android/EventLogSet;)V
    .locals 2
    .parameter

    .prologue
    .line 571
    const-string v0, "Cached video end event logged"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 573
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p1, Lcom/millennialmedia/android/EventLogSet;->endActivity:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 574
    iget-object v1, p1, Lcom/millennialmedia/android/EventLogSet;->endActivity:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/millennialmedia/android/VideoPlayer;->logEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 575
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 578
    :cond_0
    return-void
.end method

.method protected logEvent(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Logging event to: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 605
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/millennialmedia/android/VideoPlayer$5;

    invoke-direct {v1, p0, p1}, Lcom/millennialmedia/android/VideoPlayer$5;-><init>(Lcom/millennialmedia/android/VideoPlayer;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 616
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .parameter

    .prologue
    .line 110
    const v0, 0x1030007

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->setTheme(I)V

    .line 111
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    const-string v0, "Setting up the video player"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 114
    if-eqz p1, :cond_5

    .line 116
    const-string v0, "isCachedAd"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    .line 117
    const-string v0, "videoCompleted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    .line 118
    const-string v0, "videoCompletedOnce"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompletedOnce:Z

    .line 119
    const-string v0, "videoPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    .line 120
    const-string v0, "lastVideoPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    .line 130
    :goto_0
    new-instance v0, Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;-><init>(Lcom/millennialmedia/android/VideoPlayer;Lcom/millennialmedia/android/VideoPlayer$1;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->receiver:Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;

    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->receiver:Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/millennialmedia/android/VideoPlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 138
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    .line 139
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 140
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 142
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setDrawingCacheBackgroundColor(I)V

    .line 143
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 146
    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 147
    const/16 v1, 0x19a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 148
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 149
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 150
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    const/high16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setDrawingCacheBackgroundColor(I)V

    .line 153
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 154
    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 155
    new-instance v3, Lcom/millennialmedia/android/MillennialMediaView;

    invoke-direct {v3, p0}, Lcom/millennialmedia/android/MillennialMediaView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    .line 156
    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    const/16 v4, 0x19b

    invoke-virtual {v3, v4}, Lcom/millennialmedia/android/MillennialMediaView;->setId(I)V

    .line 157
    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v3}, Lcom/millennialmedia/android/MillennialMediaView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    const/4 v4, -0x2

    invoke-interface {v3, v4}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 158
    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    const/high16 v3, -0x100

    invoke-virtual {v2, v3}, Lcom/millennialmedia/android/MillennialMediaView;->setDrawingCacheBackgroundColor(I)V

    .line 161
    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x1

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Is Cached Ad: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 167
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v0, :cond_b

    .line 170
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    .line 173
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->setRequestedOrientation(I)V

    .line 174
    if-nez p1, :cond_7

    .line 178
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "adName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    const/4 v2, 0x0

    .line 182
    :try_start_0
    new-instance v1, Lcom/millennialmedia/android/AdDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/AdDatabaseHelper;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/millennialmedia/android/AdDatabaseHelper;->getVideoAd(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 188
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    .line 193
    :cond_0
    :goto_1
    new-instance v0, Lcom/millennialmedia/android/EventLogSet;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    invoke-direct {v0, v1}, Lcom/millennialmedia/android/EventLogSet;-><init>(Lcom/millennialmedia/android/VideoAd;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->logSet:Lcom/millennialmedia/android/EventLogSet;

    .line 194
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->showControls:Z

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showBottomBar:Z

    .line 197
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->showCountdown:Z

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    .line 208
    :cond_1
    :goto_2
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    .line 209
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x1a4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 212
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    if-eqz v0, :cond_2

    .line 214
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->showHud(Z)V

    .line 218
    :cond_2
    const/4 v0, 0x0

    .line 219
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v1, :cond_f

    .line 220
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    move-object v3, v0

    .line 222
    :goto_3
    if-eqz v3, :cond_b

    .line 224
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 227
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->storedOnSdCard:Z

    if-eqz v0, :cond_8

    .line 229
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "/.mmsyscache"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 232
    :goto_5
    new-instance v5, Landroid/widget/ImageButton;

    invoke-direct {v5, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 233
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iput-object v5, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v1, v1, Lcom/millennialmedia/android/VideoAd;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->imageUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string v6, "\\.[^\\.]*$"

    const-string v7, ".dat"

    invoke-virtual {v0, v6, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setImageURI(Landroid/net/Uri;)V

    .line 238
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v0, v1, v6, v7}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 239
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 240
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {p0, v5, v0}, Lcom/millennialmedia/android/VideoPlayer;->setButtonAlpha(Landroid/widget/ImageButton;F)V

    .line 241
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v5, v0}, Landroid/widget/ImageButton;->setId(I)V

    .line 242
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x2

    invoke-direct {v6, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Array #: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " View ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Relative to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->anchor:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->position:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->v(Ljava/lang/String;)V

    .line 246
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v1, v0, Lcom/millennialmedia/android/VideoImage;->position:I

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->anchor:I

    invoke-virtual {v6, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 247
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v1, v0, Lcom/millennialmedia/android/VideoImage;->position2:I

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->anchor2:I

    invoke-virtual {v6, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 248
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v1, v0, Lcom/millennialmedia/android/VideoImage;->paddingLeft:I

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v7, v0, Lcom/millennialmedia/android/VideoImage;->paddingTop:I

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v8, v0, Lcom/millennialmedia/android/VideoImage;->paddingRight:I

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget v0, v0, Lcom/millennialmedia/android/VideoImage;->paddingBottom:I

    invoke-virtual {v6, v1, v7, v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 251
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 253
    iget-object v1, v0, Lcom/millennialmedia/android/VideoImage;->linkUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 255
    new-instance v1, Lcom/millennialmedia/android/VideoPlayer$1;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/android/VideoPlayer$1;-><init>(Lcom/millennialmedia/android/VideoPlayer;Lcom/millennialmedia/android/VideoImage;)V

    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    :cond_3
    iget-wide v7, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    const-wide/16 v9, 0x0

    cmp-long v1, v7, v9

    if-lez v1, :cond_9

    .line 267
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/android/VideoImage;

    iput-object v6, v1, Lcom/millennialmedia/android/VideoImage;->layoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 268
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v5, 0x3

    invoke-static {v1, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 269
    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    invoke-virtual {v5, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 277
    :goto_6
    iget-wide v5, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-lez v1, :cond_4

    .line 279
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-static {v1, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 280
    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->handler:Landroid/os/Handler;

    iget-wide v6, v0, Lcom/millennialmedia/android/VideoImage;->inactivityTimeout:J

    iget-wide v8, v0, Lcom/millennialmedia/android/VideoImage;->appearanceDelay:J

    add-long/2addr v6, v8

    iget-wide v8, v0, Lcom/millennialmedia/android/VideoImage;->fadeDuration:J

    add-long/2addr v6, v8

    invoke-virtual {v5, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 224
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_4

    .line 124
    :cond_5
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cached"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/millennialmedia/android/VideoPlayer;->currentVideoPosition:I

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompletedOnce:Z

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    goto/16 :goto_0

    .line 185
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_7
    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 188
    if-eqz v1, :cond_0

    .line 189
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    goto/16 :goto_1

    .line 188
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_8
    if-eqz v1, :cond_6

    .line 189
    invoke-virtual {v1}, Lcom/millennialmedia/android/AdDatabaseHelper;->close()V

    :cond_6
    throw v0

    .line 202
    :cond_7
    const-string v0, "videoAd"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoAd;

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    .line 203
    const-string v0, "logSet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/EventLogSet;

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->logSet:Lcom/millennialmedia/android/EventLogSet;

    .line 204
    const-string v0, "shouldShowBottomBar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showBottomBar:Z

    .line 205
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->showCountdown:Z

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showCountdownHud:Z

    goto/16 :goto_2

    .line 231
    :cond_8
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->getCacheDir()Ljava/io/File;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_5

    .line 273
    :cond_9
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 284
    :cond_a
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    :cond_b
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->showBottomBar:Z

    if-eqz v0, :cond_c

    .line 291
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    .line 292
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 293
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 294
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 297
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mRewind:Landroid/widget/Button;

    .line 298
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    .line 299
    new-instance v1, Landroid/widget/Button;

    invoke-direct {v1, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mStop:Landroid/widget/Button;

    .line 301
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mRewind:Landroid/widget/Button;

    const v2, 0x1080025

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 302
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    const v2, 0x1080023

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 303
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mStop:Landroid/widget/Button;

    const v2, 0x1080038

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 305
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 306
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 307
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 309
    const/16 v4, 0xe

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 310
    iget-object v4, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    invoke-virtual {v4, v5, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    const/4 v1, 0x0

    iget-object v4, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getId()I

    move-result v4

    invoke-virtual {v3, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 313
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mRewind:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 315
    const/16 v1, 0xb

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 316
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->mStop:Landroid/widget/Button;

    invoke-virtual {v1, v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mRewind:Landroid/widget/Button;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$2;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$2;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mPausePlay:Landroid/widget/Button;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$3;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$3;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mStop:Landroid/widget/Button;

    new-instance v2, Lcom/millennialmedia/android/VideoPlayer$4;

    invoke-direct {v2, p0}, Lcom/millennialmedia/android/VideoPlayer$4;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 356
    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    :cond_c
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_d

    .line 362
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 365
    :cond_d
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_e

    .line 367
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->bringChildToFront(Landroid/view/View;)V

    .line 370
    :cond_e
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->relLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->setContentView(Landroid/view/View;)V

    .line 371
    return-void

    .line 188
    :catchall_1
    move-exception v0

    goto/16 :goto_8

    .line 185
    :catch_1
    move-exception v0

    goto/16 :goto_7

    :cond_f
    move-object v3, v0

    goto/16 :goto_3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 920
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 921
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->receiver:Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;

    invoke-virtual {p0, v0}, Lcom/millennialmedia/android/VideoPlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 922
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    if-eqz v0, :cond_0

    .line 924
    invoke-virtual {p0}, Lcom/millennialmedia/android/VideoPlayer;->stopServer()V

    .line 926
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 899
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 900
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompletedOnce:Z

    if-nez v0, :cond_0

    .line 901
    const/4 v0, 0x1

    .line 903
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 886
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 887
    const-string v0, "MillennialMediaSDK"

    const-string v1, "VideoPlayer - onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->waitForUserPresent:Z

    if-nez v0, :cond_0

    .line 889
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->pauseVideo()V

    .line 890
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 875
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 876
    const-string v0, "MillennialMediaSDK"

    const-string v1, "VideoPlayer - onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->waitForUserPresent:Z

    if-nez v0, :cond_0

    .line 878
    invoke-direct {p0}, Lcom/millennialmedia/android/VideoPlayer;->resumeVideo()V

    .line 879
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 932
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_0

    .line 934
    const-string v0, "videoPosition"

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v1}, Lcom/millennialmedia/android/MillennialMediaView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 935
    const-string v0, "lastVideoPosition"

    iget v1, p0, Lcom/millennialmedia/android/VideoPlayer;->lastVideoPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 937
    :cond_0
    const-string v0, "isCachedAd"

    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->isCachedAd:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 938
    const-string v0, "videoCompleted"

    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 939
    const-string v0, "videoCompletedOnce"

    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompletedOnce:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 940
    const-string v0, "logSet"

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->logSet:Lcom/millennialmedia/android/EventLogSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 941
    const-string v0, "shouldShowBottomBar"

    iget-boolean v1, p0, Lcom/millennialmedia/android/VideoPlayer;->showBottomBar:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 942
    const-string v0, "videoAd"

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 944
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 945
    return-void
.end method

.method public onStart()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 847
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 848
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    if-eqz v0, :cond_2

    .line 850
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-boolean v0, v0, Lcom/millennialmedia/android/VideoAd;->stayInPlayer:Z

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoCompleted:Z

    if-ne v0, v1, :cond_2

    .line 852
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v1, v2

    .line 855
    :goto_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 857
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    .line 858
    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget v4, v0, Lcom/millennialmedia/android/VideoImage;->fromAlpha:F

    invoke-direct {p0, v3, v4}, Lcom/millennialmedia/android/VideoPlayer;->setButtonAlpha(Landroid/widget/ImageButton;F)V

    .line 859
    iget-object v3, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_0

    .line 860
    iget-object v3, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v4, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->layoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3, v4, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    move v3, v2

    .line 861
    :goto_1
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 862
    iget-object v4, p0, Lcom/millennialmedia/android/VideoPlayer;->buttonsLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoAd:Lcom/millennialmedia/android/VideoAd;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoAd;->buttons:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/android/VideoImage;

    iget-object v0, v0, Lcom/millennialmedia/android/VideoImage;->button:Landroid/widget/ImageButton;

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->bringChildToFront(Landroid/view/View;)V

    .line 861
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 855
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 867
    :cond_2
    return-void
.end method

.method public declared-synchronized startServer(Ljava/lang/String;IZ)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1341
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    if-nez v0, :cond_0

    .line 1343
    new-instance v0, Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    invoke-direct {v0, p0, p1, p3}, Lcom/millennialmedia/android/VideoPlayer$VideoServer;-><init>(Lcom/millennialmedia/android/VideoPlayer;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    .line 1344
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1345
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1346
    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    .line 1347
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_1

    .line 1349
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://localhost:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    iget-object v2, v2, Lcom/millennialmedia/android/VideoPlayer$VideoServer;->port:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/video.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MillennialMediaView;->setVideoURI(Landroid/net/Uri;)V

    .line 1350
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v1, Lcom/millennialmedia/android/VideoPlayer$12;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/VideoPlayer$12;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MillennialMediaView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1362
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v1, Lcom/millennialmedia/android/VideoPlayer$13;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/VideoPlayer$13;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MillennialMediaView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1368
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    new-instance v1, Lcom/millennialmedia/android/VideoPlayer$14;

    invoke-direct {v1, p0}, Lcom/millennialmedia/android/VideoPlayer$14;-><init>(Lcom/millennialmedia/android/VideoPlayer;)V

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MillennialMediaView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1375
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->requestFocus()Z

    .line 1376
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0, p2}, Lcom/millennialmedia/android/MillennialMediaView;->seekTo(I)V

    .line 1377
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->start()V

    .line 1378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/android/VideoPlayer;->paused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1385
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1382
    :cond_1
    :try_start_1
    const-string v0, "MillennialMediaSDK"

    const-string v1, "Null Video View"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopServer()V
    .locals 1

    .prologue
    .line 1392
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    if-eqz v0, :cond_1

    .line 1394
    const-string v0, "Stop video server"

    invoke-static {v0}, Lcom/millennialmedia/android/MMAdViewSDK$Log;->d(Ljava/lang/String;)V

    .line 1395
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    if-eqz v0, :cond_0

    .line 1396
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->mVideoView:Lcom/millennialmedia/android/MillennialMediaView;

    invoke-virtual {v0}, Lcom/millennialmedia/android/MillennialMediaView;->stopPlayback()V

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;

    invoke-virtual {v0}, Lcom/millennialmedia/android/VideoPlayer$VideoServer;->requestStop()V

    .line 1398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/android/VideoPlayer;->videoServer:Lcom/millennialmedia/android/VideoPlayer$VideoServer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400
    :cond_1
    monitor-exit p0

    return-void

    .line 1392
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
