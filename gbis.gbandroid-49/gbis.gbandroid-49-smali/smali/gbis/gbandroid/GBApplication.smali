.class public Lgbis/gbandroid/GBApplication;
.super Landroid/app/Application;
.source "GBFile"


# static fields
.field public static final ADS_DIRECTORY:Ljava/lang/String; = "/Android/data/gbis.gbandroid/cache/Ads"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_AUTOCOMPLETE:Ljava/lang/String; = "AutocompleteButton"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_CONTEXT_MENU:Ljava/lang/String; = "ContextMenu"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_MENU:Ljava/lang/String; = "MenuButton"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_PHONE:Ljava/lang/String; = "PhoneButton"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_SCREEN:Ljava/lang/String; = "ScreenButton"

.field public static final ANALYTICS_EVENT_ACTION_BUTTON_SEARCH:Ljava/lang/String; = "SearchButton"

.field public static final BASE_DIRECTORY:Ljava/lang/String; = "/Android/data/gbis.gbandroid/cache/"

.field public static final BRANDS_DIRECTORY:Ljava/lang/String; = "/Android/data/gbis.gbandroid/cache/Brands"

.field public static final BRANDS_GLOBAL_VERSION:I = 0x1

.field public static final TAG:Ljava/lang/String; = "GasBuddy"

.field private static a:Lgbis/gbandroid/GBApplication;


# instance fields
.field private b:Ljava/lang/Object;

.field private c:Landroid/location/Location;

.field private d:Ljava/lang/String;

.field private e:Lgbis/gbandroid/entities/AdsGSA;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private static a(F)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    const/high16 v1, 0x3f80

    const-wide/high16 v2, 0x3ff8

    .line 104
    cmpg-float v0, p0, v1

    if-gez v0, :cond_0

    .line 105
    const-string v0, "ldpi"

    .line 112
    :goto_0
    return-object v0

    .line 106
    :cond_0
    cmpl-float v0, p0, v1

    if-nez v0, :cond_1

    .line 107
    const-string v0, "mdpi"

    goto :goto_0

    .line 108
    :cond_1
    float-to-double v0, p0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    .line 109
    const-string v0, "hdpi"

    goto :goto_0

    .line 110
    :cond_2
    float-to-double v0, p0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    .line 111
    const-string v0, "xhdpi"

    goto :goto_0

    .line 112
    :cond_3
    const-string v0, "mdpi"

    goto :goto_0
.end method

.method public static getInstance()Lgbis/gbandroid/GBApplication;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lgbis/gbandroid/GBApplication;->a:Lgbis/gbandroid/GBApplication;

    return-object v0
.end method


# virtual methods
.method public getAdsGSA()Lgbis/gbandroid/entities/AdsGSA;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/GBApplication;->e:Lgbis/gbandroid/entities/AdsGSA;

    return-object v0
.end method

.method public getComplexObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/GBApplication;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public getImageAdUrl(IF)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    invoke-virtual {p0}, Lgbis/gbandroid/GBApplication;->getImageServer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    const-string v1, "/ads/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 96
    const-string v1, "/android/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-static {p2}, Lgbis/gbandroid/GBApplication;->a(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string v1, "/logo.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageServer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lgbis/gbandroid/GBApplication;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl(IF)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    invoke-virtual {p0}, Lgbis/gbandroid/GBApplication;->getImageServer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "/android/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-static {p2}, Lgbis/gbandroid/GBApplication;->a(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v1, "/logo.png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastKnownLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgbis/gbandroid/GBApplication;->c:Landroid/location/Location;

    return-object v0
.end method

.method public getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 117
    const-string v2, "mounted"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "mounted_ro"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 128
    :cond_1
    :goto_0
    return-object v0

    .line 124
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 38
    sput-object p0, Lgbis/gbandroid/GBApplication;->a:Lgbis/gbandroid/GBApplication;

    .line 39
    return-void
.end method

.method public setAdsGSA(Lgbis/gbandroid/entities/AdsGSA;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lgbis/gbandroid/GBApplication;->e:Lgbis/gbandroid/entities/AdsGSA;

    .line 63
    return-void
.end method

.method public setComplexObject(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lgbis/gbandroid/GBApplication;->b:Ljava/lang/Object;

    .line 47
    return-void
.end method

.method public setImageServer(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lgbis/gbandroid/GBApplication;->d:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setLastKnowLocation(Landroid/location/Location;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lgbis/gbandroid/GBApplication;->c:Landroid/location/Location;

    .line 55
    return-void
.end method
