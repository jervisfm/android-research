.class final Lgbis/gbandroid/InitScreen$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/InitScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/InitScreen;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/InitScreen;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 307
    iput-object p1, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    .line 308
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 309
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 313
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 314
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->a(Lgbis/gbandroid/InitScreen;)Lgbis/gbandroid/entities/InitializeMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 316
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->b(Lgbis/gbandroid/InitScreen;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->c(Lgbis/gbandroid/InitScreen;)V

    .line 318
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->d(Lgbis/gbandroid/InitScreen;)V

    .line 323
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-virtual {v0}, Lgbis/gbandroid/InitScreen;->finish()V

    goto :goto_0

    .line 322
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-virtual {v0}, Lgbis/gbandroid/InitScreen;->finish()V

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 6

    .prologue
    .line 327
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->e(Lgbis/gbandroid/InitScreen;)V

    .line 328
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 329
    iget-object v2, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v2}, Lgbis/gbandroid/InitScreen;->f(Lgbis/gbandroid/InitScreen;)J

    move-result-wide v2

    sub-long v2, v0, v2

    iget-object v4, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v4}, Lgbis/gbandroid/InitScreen;->g(Lgbis/gbandroid/InitScreen;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 330
    iget-object v2, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v2}, Lgbis/gbandroid/InitScreen;->g(Lgbis/gbandroid/InitScreen;)J

    move-result-wide v2

    iget-object v4, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v4}, Lgbis/gbandroid/InitScreen;->f(Lgbis/gbandroid/InitScreen;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 332
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    invoke-static {v0}, Lgbis/gbandroid/InitScreen;->a(Lgbis/gbandroid/InitScreen;)Lgbis/gbandroid/entities/InitializeMessage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 333
    iget-object v0, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    iget-object v1, p0, Lgbis/gbandroid/InitScreen$a;->a:Lgbis/gbandroid/InitScreen;

    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/InitScreen;->setMessage(Ljava/lang/String;)V

    .line 334
    const/4 v0, 0x0

    .line 336
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
