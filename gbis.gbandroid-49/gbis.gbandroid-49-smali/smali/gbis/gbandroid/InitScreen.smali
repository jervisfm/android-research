.class public Lgbis/gbandroid/InitScreen;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/InitScreen$a;,
        Lgbis/gbandroid/InitScreen$b;
    }
.end annotation


# instance fields
.field private a:J

.field private b:Lgbis/gbandroid/entities/InitializeMessage;

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    .line 39
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lgbis/gbandroid/InitScreen;->a:J

    .line 38
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/InitScreen;)Lgbis/gbandroid/entities/InitializeMessage;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lgbis/gbandroid/InitScreen$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/InitScreen$a;-><init>(Lgbis/gbandroid/InitScreen;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 69
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/InitScreen$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 70
    return-void
.end method

.method private a(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getAuthId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getAuthId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    const-string v0, "auth_id"

    iget-object v1, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/InitializeMessage;->getAuthId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 142
    :cond_0
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/InitScreen;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgbis/gbandroid/entities/ListResults;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lgbis/gbandroid/InitScreen;->showList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgbis/gbandroid/entities/ListResults;)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/Ads;)V
    .locals 3
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 151
    const-string v1, "adInit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getInit()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 152
    const-string v1, "adInitKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getInit()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 153
    const-string v1, "adInitUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getInit()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 155
    const-string v1, "adMain"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getHome()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 156
    const-string v1, "adMainKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getHome()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 157
    const-string v1, "adMainUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getHome()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 159
    const-string v1, "adList"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getList()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 160
    const-string v1, "adListKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getList()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 161
    const-string v1, "adListUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getList()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 163
    const-string v1, "adListTop"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListTop()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 164
    const-string v1, "adListTopKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListTop()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 165
    const-string v1, "adListTopUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListTop()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 167
    const-string v1, "adListBottom"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListBottom()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 168
    const-string v1, "adListBottomKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListBottom()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 169
    const-string v1, "adListBottomUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListBottom()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 171
    const-string v1, "adStation"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getDetails()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 172
    const-string v1, "adStationKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getDetails()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    const-string v1, "adStationUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getDetails()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 175
    const-string v1, "adProfile"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getProfile()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 176
    const-string v1, "adProfileKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getProfile()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 177
    const-string v1, "adProfileUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getProfile()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 179
    const-string v1, "adFavorites"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getFavorites()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 180
    const-string v1, "adFavoritesKey"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getFavorites()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getNetworkKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    const-string v1, "adFavoritesUnit"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getFavorites()Lgbis/gbandroid/entities/Ad;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Ad;->getUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 183
    const-string v1, "adInitTime"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getInitTime()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    const-string v1, "adListScrollTime"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListScrollTime()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 185
    const-string v1, "adListWaitTime"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Ads;->getListWaitTime()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 186
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 187
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 73
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    .line 78
    :goto_0
    new-instance v1, Lgbis/gbandroid/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/a;-><init>(Lgbis/gbandroid/InitScreen;)V

    invoke-virtual {v1}, Lgbis/gbandroid/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 79
    new-instance v2, Lgbis/gbandroid/queries/InitQuery;

    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-direct {v2, p0, v3, v1, v0}, Lgbis/gbandroid/queries/InitQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 80
    invoke-virtual {v2}, Lgbis/gbandroid/queries/InitQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/InitializeMessage;

    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    if-eqz v0, :cond_4

    .line 83
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 84
    invoke-direct {p0, v0}, Lgbis/gbandroid/InitScreen;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 85
    invoke-direct {p0, v0}, Lgbis/gbandroid/InitScreen;->b(Landroid/content/SharedPreferences$Editor;)V

    .line 86
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getAds()Lgbis/gbandroid/entities/Ads;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 89
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getAds()Lgbis/gbandroid/entities/Ads;

    move-result-object v0

    .line 92
    :goto_1
    invoke-direct {p0, v0}, Lgbis/gbandroid/InitScreen;->a(Lgbis/gbandroid/entities/Ads;)V

    .line 93
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getGSA()Lgbis/gbandroid/entities/AdsGSA;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/InitializeMessage;->getGSA()Lgbis/gbandroid/entities/AdsGSA;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/GBApplication;->setAdsGSA(Lgbis/gbandroid/entities/AdsGSA;)V

    .line 97
    :cond_0
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    const-string v1, "http://images.gasbuddy.com/b/"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/GBApplication;->setImageServer(Ljava/lang/String;)V

    .line 102
    :cond_1
    :goto_2
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->g()V

    .line 103
    return-void

    .line 77
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getLastLocationStored()Landroid/location/Location;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_3
    new-instance v0, Lgbis/gbandroid/entities/Ads;

    invoke-direct {v0}, Lgbis/gbandroid/entities/Ads;-><init>()V

    goto :goto_1

    .line 99
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    :cond_5
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->d()V

    goto :goto_2
.end method

.method private b(Landroid/content/SharedPreferences$Editor;)V
    .locals 2
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->b:Lgbis/gbandroid/entities/InitializeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/InitializeMessage;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 146
    :goto_0
    const-string v1, "host"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 147
    return-void

    .line 145
    :cond_0
    const v0, 0x7f0901dd

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/InitScreen;)Z
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-boolean v0, p0, Lgbis/gbandroid/InitScreen;->e:Z

    return v0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    const/16 v0, -0xb

    :try_start_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->encryptTripleDES(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->encode64([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 128
    :goto_0
    return-object v0

    .line 126
    :catch_0
    move-exception v0

    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->showMessage(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->finish()V

    .line 128
    const-string v0, ""

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/InitScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 280
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->j()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 134
    const-string v1, "auth_id"

    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/InitScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 251
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->h()V

    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 190
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->g()V

    .line 191
    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 192
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "fuelPreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 193
    const-string v3, "fuelPreference"

    const v4, 0x7f0901c8

    invoke-virtual {p0, v4}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 194
    :cond_0
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "noPricesPreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 195
    const-string v3, "noPricesPreference"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 196
    :cond_1
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "screenPreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 197
    const-string v3, "screenPreference"

    const-string v4, "home"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 198
    :cond_2
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "nearmePreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 199
    const-string v3, "nearmePreference"

    const-string v4, "cheap"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 200
    :cond_3
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "radiusPreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 201
    const-string v3, "radiusPreference"

    const/4 v4, 0x5

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 202
    :cond_4
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "auth_id"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 203
    const-string v3, "auth_id"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 204
    :cond_5
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "is_member"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 205
    const-string v3, "is_member"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 206
    :cond_6
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "initial_screen_date"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 207
    const-string v3, "initial_screen_date"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 211
    :cond_7
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "largeScreenBoolean"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 212
    const-string v3, "largeScreenBoolean"

    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->f()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 214
    :cond_8
    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "customKeyboardPreference"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 215
    const-string v3, "customKeyboardPreference"

    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->f()Z

    move-result v4

    if-eqz v4, :cond_a

    :goto_0
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 217
    :cond_9
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 218
    const-string v0, "Awards"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->cleanImageCache(Ljava/lang/String;)V

    .line 219
    return-void

    :cond_a
    move v0, v1

    .line 215
    goto :goto_0
.end method

.method static synthetic e(Lgbis/gbandroid/InitScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->b()V

    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/InitScreen;)J
    .locals 2
    .parameter

    .prologue
    .line 42
    iget-wide v0, p0, Lgbis/gbandroid/InitScreen;->d:J

    return-wide v0
.end method

.method private f()Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 223
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ge v1, v4, :cond_1

    .line 242
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    :try_start_0
    const-class v1, Landroid/content/res/Configuration;

    const-string v2, "screenLayout"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    .line 232
    const-class v2, Landroid/content/res/Configuration;

    const-string v3, "SCREENLAYOUT_SIZE_MASK"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 239
    and-int/2addr v1, v2

    if-ne v1, v4, :cond_0

    .line 240
    const/4 v0, 0x1

    goto :goto_0

    .line 233
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic g(Lgbis/gbandroid/InitScreen;)J
    .locals 2
    .parameter

    .prologue
    .line 39
    iget-wide v0, p0, Lgbis/gbandroid/InitScreen;->a:J

    return-wide v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "auth_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->c:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adInitTime"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lgbis/gbandroid/InitScreen;->a:J

    .line 249
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    .line 252
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "screenPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 253
    const-string v1, "nearme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v3

    if-eqz v0, :cond_1

    .line 256
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->k()V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->i()V

    .line 259
    const v0, 0x7f090017

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->showMessage(Ljava/lang/String;)V

    .line 271
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->finish()V

    goto :goto_0

    .line 262
    :cond_3
    const-string v1, "home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 263
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->i()V

    goto :goto_1

    .line 265
    :cond_4
    const-string v1, "favorites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->i()V

    .line 267
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 268
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->showFavourites()V

    goto :goto_1
.end method

.method static synthetic h(Lgbis/gbandroid/InitScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 275
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->i()V

    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/InitScreen;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/search/MainScreen;

    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 277
    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->startActivity(Landroid/content/Intent;)V

    .line 278
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 281
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 282
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 284
    :cond_0
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/services/BrandsDownloader;

    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 285
    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 287
    :cond_1
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/InitScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 294
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->l()V

    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 290
    new-instance v0, Lgbis/gbandroid/InitScreen$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/InitScreen$b;-><init>(Lgbis/gbandroid/InitScreen;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 291
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/InitScreen$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 292
    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const-wide v5, 0x412e848000000000L

    .line 295
    new-instance v0, Lgbis/gbandroid/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/b;-><init>(Lgbis/gbandroid/InitScreen;)V

    invoke-virtual {v0}, Lgbis/gbandroid/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 296
    new-instance v1, Lgbis/gbandroid/queries/ListQuery;

    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 297
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    .line 299
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 300
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    iget-object v3, p0, Lgbis/gbandroid/InitScreen;->myLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 303
    :goto_0
    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4, v0}, Lgbis/gbandroid/queries/ListQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/InitScreen;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 304
    return-void

    .line 302
    :cond_0
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v4, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    .line 47
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-wide/32 v3, 0x36ee80

    sub-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    .line 49
    const v0, 0x7f030034

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->setContentView(I)V

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/InitScreen;->d:J

    .line 51
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->e()V

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "auth_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->j()V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/InitScreen;->e:Z

    .line 57
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/InitScreen;->startGPSService()V

    .line 58
    invoke-direct {p0}, Lgbis/gbandroid/InitScreen;->a()V

    .line 59
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onResume()V

    .line 65
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 5

    .prologue
    .line 368
    iget-object v0, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adInit"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 369
    iget-object v1, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adInitKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 370
    iget-object v2, p0, Lgbis/gbandroid/InitScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adInitUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 371
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3, v2, v0}, Lgbis/gbandroid/InitScreen;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;I)V

    .line 372
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    const v0, 0x7f09022a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/InitScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected trackAnalytics()Z
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x0

    return v0
.end method
