.class public final Lgbis/gbandroid/R$color;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final black:I = 0x7f080004

.field public static final black_dialog_loading_text:I = 0x7f080026

.field public static final black_text:I = 0x7f080025

.field public static final blue:I = 0x7f08000b

.field public static final blue_background:I = 0x7f08000c

.field public static final blue_button_focus:I = 0x7f080017

.field public static final blue_button_label:I = 0x7f080019

.field public static final blue_button_press:I = 0x7f080018

.field public static final blue_button_stroke:I = 0x7f080016

.field public static final blue_dialog_station_address:I = 0x7f08001d

.field public static final blue_dialog_station_time:I = 0x7f08001e

.field public static final blue_dialog_title_label:I = 0x7f08001c

.field public static final blue_radius_bar:I = 0x7f080024

.field public static final blue_station_details_bck:I = 0x7f080020

.field public static final blue_station_features_bck:I = 0x7f080022

.field public static final blue_station_features_stroke:I = 0x7f080021

.field public static final blue_station_fuel_type_bck:I = 0x7f080023

.field public static final blue_station_photo_stroke:I = 0x7f08001f

.field public static final blue_title_default:I = 0x7f08001a

.field public static final blue_title_focus:I = 0x7f08001b

.field public static final bottom_button_margin:I = 0x7f080046

.field public static final button_background_focus:I = 0x7f080037

.field public static final button_background_focus_stroke:I = 0x7f080038

.field public static final button_background_press:I = 0x7f080035

.field public static final button_background_press_stroke:I = 0x7f080036

.field public static final button_filter_color_active:I = 0x7f080034

.field public static final button_filter_color_focus:I = 0x7f080033

.field public static final button_filter_color_normal:I = 0x7f080031

.field public static final button_filter_color_press:I = 0x7f080032

.field public static final button_filter_text:I = 0x7f08004e

.field public static final button_price_text:I = 0x7f08004f

.field public static final button_prices_bck_focus:I = 0x7f08003d

.field public static final button_prices_bck_normal:I = 0x7f08003b

.field public static final button_prices_bck_press:I = 0x7f08003c

.field public static final button_prices_bck_selected:I = 0x7f08003e

.field public static final button_prices_stroke:I = 0x7f08003a

.field public static final button_text:I = 0x7f08003f

.field public static final buttons_text:I = 0x7f080050

.field public static final custom_view_dark_bck:I = 0x7f08004d

.field public static final custom_view_light_bck:I = 0x7f08004c

.field public static final dialog_loading_bck:I = 0x7f080044

.field public static final dialog_loading_stroke:I = 0x7f080045

.field public static final dialog_stroke:I = 0x7f080041

.field public static final dialog_title_bck:I = 0x7f080042

.field public static final dialog_title_stroke:I = 0x7f080043

.field public static final diesel:I = 0x7f080012

.field public static final dim_gray:I = 0x7f080006

.field public static final gray_bck_fav:I = 0x7f08002c

.field public static final gray_division:I = 0x7f08002a

.field public static final gray_division_fav:I = 0x7f08002b

.field public static final gray_list_row_text:I = 0x7f080047

.field public static final gray_loading_bck:I = 0x7f08002d

.field public static final gray_loading_label:I = 0x7f08002e

.field public static final gray_text:I = 0x7f080029

.field public static final green_price:I = 0x7f080027

.field public static final green_price_cents:I = 0x7f080028

.field public static final hint_color:I = 0x7f080040

.field public static final light_gray:I = 0x7f080007

.field public static final light_sky_blue:I = 0x7f080005

.field public static final lime_green:I = 0x7f080008

.field public static final list_row_background:I = 0x7f080051

.field public static final list_row_price_background_focus:I = 0x7f080014

.field public static final list_row_price_background_normal:I = 0x7f080013

.field public static final list_row_price_background_press:I = 0x7f080015

.field public static final list_row_prices_background:I = 0x7f080052

.field public static final list_row_text:I = 0x7f080053

.field public static final midgrade:I = 0x7f080010

.field public static final opaque_red:I = 0x7f080001

.field public static final orange_dialog_stroke:I = 0x7f08002f

.field public static final orange_red:I = 0x7f08000a

.field public static final premium:I = 0x7f080011

.field public static final profile_pic_background:I = 0x7f08000d

.field public static final profile_pic_background_border:I = 0x7f08000e

.field public static final radius_bar_bck:I = 0x7f080049

.field public static final radius_bar_progress:I = 0x7f08004a

.field public static final radius_bar_secondary_progress:I = 0x7f08004b

.field public static final red:I = 0x7f080009

.field public static final regular:I = 0x7f08000f

.field public static final search_bar_background:I = 0x7f080039

.field public static final tab_gray_division:I = 0x7f080048

.field public static final tab_text:I = 0x7f080054

.field public static final translucent_red:I = 0x7f080002

.field public static final transparent:I = 0x7f080000

.field public static final white:I = 0x7f080003

.field public static final yellow_radius_bar:I = 0x7f080030


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
