.class public final Lgbis/gbandroid/R$drawable;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final aaa_dummy:I = 0x7f020000

.field public static final air:I = 0x7f020001

.field public static final arrow_down:I = 0x7f020002

.field public static final atm:I = 0x7f020003

.field public static final award_received_bck:I = 0x7f020004

.field public static final bck_widget_320x100:I = 0x7f020005

.field public static final bck_widget_424x74:I = 0x7f020006

.field public static final bluecar:I = 0x7f020007

.field public static final bluecarspeed:I = 0x7f020008

.field public static final browncar:I = 0x7f020009

.field public static final browncarspeed:I = 0x7f02000a

.field public static final button_arrow:I = 0x7f02000b

.field public static final button_awards:I = 0x7f02000c

.field public static final button_checkbox:I = 0x7f02000d

.field public static final button_default:I = 0x7f02000e

.field public static final button_default_focus:I = 0x7f02000f

.field public static final button_default_normal:I = 0x7f020010

.field public static final button_default_press:I = 0x7f020011

.field public static final button_dialog_focus:I = 0x7f020012

.field public static final button_dialog_normal:I = 0x7f020013

.field public static final button_dialog_press:I = 0x7f020014

.field public static final button_directions:I = 0x7f020015

.field public static final button_dropdown_normal:I = 0x7f020016

.field public static final button_dropdown_press:I = 0x7f020017

.field public static final button_dropdown_selected:I = 0x7f020018

.field public static final button_fav_dropdown:I = 0x7f020019

.field public static final button_fav_radio:I = 0x7f02001a

.field public static final button_find_gas_nearme:I = 0x7f02001b

.field public static final button_find_gas_nearme_focus:I = 0x7f02001c

.field public static final button_find_gas_nearme_normal:I = 0x7f02001d

.field public static final button_find_gas_nearme_press:I = 0x7f02001e

.field public static final button_list:I = 0x7f02001f

.field public static final button_logout:I = 0x7f020020

.field public static final button_maps:I = 0x7f020021

.field public static final button_member_login:I = 0x7f020022

.field public static final button_my_fav_station:I = 0x7f020023

.field public static final button_next:I = 0x7f020024

.field public static final button_previous:I = 0x7f020025

.field public static final button_price_report:I = 0x7f020026

.field public static final button_price_report_focus:I = 0x7f020027

.field public static final button_price_report_normal:I = 0x7f020028

.field public static final button_price_report_press:I = 0x7f020029

.field public static final button_prices:I = 0x7f02002a

.field public static final button_prize:I = 0x7f02002b

.field public static final button_reload:I = 0x7f02002c

.field public static final button_report_price:I = 0x7f02002d

.field public static final button_settings:I = 0x7f02002e

.field public static final button_slider:I = 0x7f02002f

.field public static final button_slider_focus:I = 0x7f020030

.field public static final button_slider_normal:I = 0x7f020031

.field public static final button_slider_press:I = 0x7f020032

.field public static final button_sort_by_distance:I = 0x7f020033

.field public static final button_sort_by_price:I = 0x7f020034

.field public static final button_upload_photo:I = 0x7f020035

.field public static final button_widget_arrow_down:I = 0x7f020036

.field public static final button_widget_arrow_down_normal:I = 0x7f020037

.field public static final button_widget_arrow_down_press:I = 0x7f020038

.field public static final button_widget_arrow_up:I = 0x7f020039

.field public static final button_widget_arrow_up_normal:I = 0x7f02003a

.field public static final button_widget_arrow_up_press:I = 0x7f02003b

.field public static final button_widget_refresh:I = 0x7f02003c

.field public static final button_widget_refresh_normal:I = 0x7f02003d

.field public static final button_widget_refresh_press:I = 0x7f02003e

.field public static final button_widget_settings:I = 0x7f02003f

.field public static final button_widget_settings_normal:I = 0x7f020040

.field public static final button_widget_settings_press:I = 0x7f020041

.field public static final c_store:I = 0x7f020042

.field public static final carwash:I = 0x7f020043

.field public static final checkbox_active:I = 0x7f020044

.field public static final com_android_inputmethod_latin_btn_keyboard_key_dark_normal:I = 0x7f020045

.field public static final custom_view_bck_center:I = 0x7f020046

.field public static final custom_view_bck_complete:I = 0x7f020047

.field public static final custom_view_bck_left:I = 0x7f020048

.field public static final custom_view_bck_right:I = 0x7f020049

.field public static final custom_view_stroke:I = 0x7f02004a

.field public static final dialog_loading_bck:I = 0x7f02004b

.field public static final dialog_title_bck:I = 0x7f02004c

.field public static final dialog_tooltip_bck:I = 0x7f02004d

.field public static final diesel:I = 0x7f02004e

.field public static final e85:I = 0x7f02004f

.field public static final gasbuddy_icon:I = 0x7f020050

.field public static final gasbuddy_logo:I = 0x7f020051

.field public static final greencar:I = 0x7f020052

.field public static final greencarspeed:I = 0x7f020053

.field public static final greycar:I = 0x7f020054

.field public static final greycarspeed:I = 0x7f020055

.field public static final icon_autocomplete_favorites:I = 0x7f020056

.field public static final icon_autocomplete_near_me:I = 0x7f020057

.field public static final icon_autocomplete_saved_searches:I = 0x7f020058

.field public static final icon_filter_distance:I = 0x7f020059

.field public static final icon_filter_price:I = 0x7f02005a

.field public static final icon_filter_station:I = 0x7f02005b

.field public static final icon_share_email:I = 0x7f02005c

.field public static final icon_share_facebook:I = 0x7f02005d

.field public static final icon_share_sms:I = 0x7f02005e

.field public static final icon_share_twitter:I = 0x7f02005f

.field public static final image_arrow:I = 0x7f020060

.field public static final image_arrow_direction:I = 0x7f020061

.field public static final image_arrow_left:I = 0x7f020062

.field public static final image_awards_badge:I = 0x7f020063

.field public static final image_awards_default:I = 0x7f020064

.field public static final image_awards_default_big:I = 0x7f020065

.field public static final image_checkmark:I = 0x7f020066

.field public static final image_crossmark:I = 0x7f020067

.field public static final image_fav_icon:I = 0x7f020068

.field public static final image_gasbuddy_splash:I = 0x7f020069

.field public static final image_winner_default:I = 0x7f02006a

.field public static final keyblue:I = 0x7f02006b

.field public static final keyblueorig:I = 0x7f02006c

.field public static final keyboard_blue:I = 0x7f02006d

.field public static final keyboard_blue_focus:I = 0x7f02006e

.field public static final keyboard_blue_normal:I = 0x7f02006f

.field public static final keyboard_blue_press:I = 0x7f020070

.field public static final keyboard_grey:I = 0x7f020071

.field public static final keyboard_grey_focus:I = 0x7f020072

.field public static final keyboard_grey_normal:I = 0x7f020073

.field public static final keyboard_grey_press:I = 0x7f020074

.field public static final list_pull_header_bck:I = 0x7f020075

.field public static final logo_generic:I = 0x7f020076

.field public static final logo_widget_list:I = 0x7f020077

.field public static final ltbluecar:I = 0x7f020078

.field public static final ltbluecarspeed:I = 0x7f020079

.field public static final main_button_stroke:I = 0x7f02007a

.field public static final main_button_stroke_land:I = 0x7f02007b

.field public static final main_search_button:I = 0x7f02007c

.field public static final map_loading_bck:I = 0x7f02007d

.field public static final map_pin:I = 0x7f02007e

.field public static final mappin_default:I = 0x7f02007f

.field public static final mappin_focus:I = 0x7f020080

.field public static final mappin_press:I = 0x7f020081

.field public static final menu_icon_add_fav:I = 0x7f020082

.field public static final menu_icon_create_list:I = 0x7f020083

.field public static final menu_icon_filter:I = 0x7f020084

.field public static final menu_icon_follow_me:I = 0x7f020085

.field public static final menu_icon_home:I = 0x7f020086

.field public static final menu_icon_list:I = 0x7f020087

.field public static final menu_icon_maps:I = 0x7f020088

.field public static final menu_icon_member_login:I = 0x7f020089

.field public static final menu_icon_my_location:I = 0x7f02008a

.field public static final menu_icon_places_add:I = 0x7f02008b

.field public static final menu_icon_places_clean:I = 0x7f02008c

.field public static final menu_icon_places_edit_remove:I = 0x7f02008d

.field public static final menu_icon_places_sort:I = 0x7f02008e

.field public static final menu_icon_remove_fav:I = 0x7f02008f

.field public static final menu_icon_remove_list:I = 0x7f020090

.field public static final menu_icon_remove_stations:I = 0x7f020091

.field public static final menu_icon_report_price:I = 0x7f020092

.field public static final menu_icon_search:I = 0x7f020093

.field public static final menu_icon_settings:I = 0x7f020094

.field public static final menu_icon_share:I = 0x7f020095

.field public static final menu_icon_upload_photo:I = 0x7f020096

.field public static final midgrade_gas:I = 0x7f020097

.field public static final my_profile_default_pic:I = 0x7f020098

.field public static final mylocation:I = 0x7f020099

.field public static final no_station_photos:I = 0x7f02009a

.field public static final nocar:I = 0x7f02009b

.field public static final open_247:I = 0x7f02009c

.field public static final orangecar:I = 0x7f02009d

.field public static final orangecarspeed:I = 0x7f02009e

.field public static final ourcar:I = 0x7f02009f

.field public static final pay_at_pump:I = 0x7f0200a0

.field public static final payphone:I = 0x7f0200a1

.field public static final premium_gas:I = 0x7f0200a2

.field public static final previous_button:I = 0x7f0200a3

.field public static final priceedit_levellist:I = 0x7f0200a4

.field public static final progressbar:I = 0x7f0200a5

.field public static final progressbar_circle_black:I = 0x7f0200a6

.field public static final progressbar_circle_white:I = 0x7f0200a7

.field public static final progressbar_large_circle_black:I = 0x7f0200a8

.field public static final propane:I = 0x7f0200a9

.field public static final purplecar:I = 0x7f0200aa

.field public static final purplecarspeed:I = 0x7f0200ab

.field public static final radiobutton_active:I = 0x7f0200ac

.field public static final radiobutton_disabled:I = 0x7f0200ad

.field public static final radius_bar:I = 0x7f0200ae

.field public static final redbike:I = 0x7f0200af

.field public static final redbikespeed:I = 0x7f0200b0

.field public static final redbronco:I = 0x7f0200b1

.field public static final redbroncospeed:I = 0x7f0200b2

.field public static final redcar:I = 0x7f0200b3

.field public static final redcarspeed:I = 0x7f0200b4

.field public static final redcon:I = 0x7f0200b5

.field public static final redconspeed:I = 0x7f0200b6

.field public static final redlimo:I = 0x7f0200b7

.field public static final redlimospeed:I = 0x7f0200b8

.field public static final redsedan:I = 0x7f0200b9

.field public static final redsedanspeed:I = 0x7f0200ba

.field public static final redsemi:I = 0x7f0200bb

.field public static final redsemispeed:I = 0x7f0200bc

.field public static final redspcar:I = 0x7f0200bd

.field public static final redspcarspeed:I = 0x7f0200be

.field public static final redtruck:I = 0x7f0200bf

.field public static final redtruckspeed:I = 0x7f0200c0

.field public static final redvan:I = 0x7f0200c1

.field public static final redvanspeed:I = 0x7f0200c2

.field public static final regular_gas:I = 0x7f0200c3

.field public static final restaurant:I = 0x7f0200c4

.field public static final restrooms:I = 0x7f0200c5

.field public static final search_button_focus:I = 0x7f0200c6

.field public static final search_button_normal:I = 0x7f0200c7

.field public static final search_button_press:I = 0x7f0200c8

.field public static final search_field_bck:I = 0x7f0200c9

.field public static final service_station:I = 0x7f0200ca

.field public static final spinner_black_16:I = 0x7f0200cb

.field public static final spinner_black_48:I = 0x7f0200cc

.field public static final spinner_white_16:I = 0x7f0200cd

.field public static final spinner_white_48:I = 0x7f0200ce

.field public static final station_fuel_type_bck:I = 0x7f0200cf

.field public static final tab_active:I = 0x7f0200d0

.field public static final tab_button:I = 0x7f0200d1

.field public static final tab_default:I = 0x7f0200d2

.field public static final tab_focus:I = 0x7f0200d3

.field public static final tab_press:I = 0x7f0200d4

.field public static final truckstop:I = 0x7f0200d5

.field public static final vk_backspace:I = 0x7f0200d6

.field public static final vk_microphone:I = 0x7f0200d7

.field public static final vk_microphone_invert:I = 0x7f0200d8

.field public static final widget_editbox:I = 0x7f0200d9

.field public static final widget_editbox_focus:I = 0x7f0200da

.field public static final widget_editbox_normal:I = 0x7f0200db

.field public static final widget_editbox_normal_green:I = 0x7f0200dc

.field public static final widget_editbox_normal_red:I = 0x7f0200dd

.field public static final widget_editbox_press:I = 0x7f0200de

.field public static final winners_photo_bck:I = 0x7f0200df


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
