.class public final Lgbis/gbandroid/R$id;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ad_layout:I = 0x7f0700a6

.field public static final ad_layout_background:I = 0x7f07010b

.field public static final ad_station_details_image:I = 0x7f070004

.field public static final ad_station_details_logo:I = 0x7f070005

.field public static final ad_station_details_price:I = 0x7f070007

.field public static final ad_station_details_price_layout2:I = 0x7f070006

.field public static final ad_station_details_price_tenth_cent:I = 0x7f070008

.field public static final ad_station_details_text:I = 0x7f070009

.field public static final add_station_address:I = 0x7f07000b

.field public static final add_station_address_label:I = 0x7f07000a

.field public static final add_station_city:I = 0x7f07000f

.field public static final add_station_city_label:I = 0x7f07000e

.field public static final add_station_cross_street:I = 0x7f07000d

.field public static final add_station_cross_street_label:I = 0x7f07000c

.field public static final add_station_features_label:I = 0x7f070014

.field public static final add_station_features_layout:I = 0x7f070017

.field public static final add_station_mappin:I = 0x7f070019

.field public static final add_station_name:I = 0x7f070013

.field public static final add_station_name_label:I = 0x7f070012

.field public static final add_station_pumps:I = 0x7f070016

.field public static final add_station_pumps_label:I = 0x7f070015

.field public static final add_station_state:I = 0x7f070011

.field public static final add_station_state_label:I = 0x7f070010

.field public static final amenity:I = 0x7f07001b

.field public static final amenity_icon:I = 0x7f07001c

.field public static final amenity_legend:I = 0x7f07001d

.field public static final appwidget_address:I = 0x7f070183

.field public static final appwidget_button_next:I = 0x7f07017f

.field public static final appwidget_button_prev:I = 0x7f070180

.field public static final appwidget_city:I = 0x7f070184

.field public static final appwidget_distance:I = 0x7f07017d

.field public static final appwidget_logo:I = 0x7f070176

.field public static final appwidget_navigation_layout:I = 0x7f07017e

.field public static final appwidget_price:I = 0x7f07017a

.field public static final appwidget_price_layout:I = 0x7f070179

.field public static final appwidget_price_tenth_cent:I = 0x7f07017b

.field public static final appwidget_refresh:I = 0x7f070177

.field public static final appwidget_settings:I = 0x7f070178

.field public static final appwidget_station_icon:I = 0x7f070181

.field public static final appwidget_station_name:I = 0x7f070182

.field public static final appwidget_time:I = 0x7f07017c

.field public static final autocomplete_icon:I = 0x7f0700e1

.field public static final autocomplete_text:I = 0x7f0700e2

.field public static final award_details_history:I = 0x7f070026

.field public static final award_details_levels_layout:I = 0x7f070027

.field public static final award_details_message:I = 0x7f070025

.field public static final award_details_progresbar:I = 0x7f070023

.field public static final award_details_progressbar_text:I = 0x7f070024

.field public static final award_details_title:I = 0x7f070022

.field public static final award_level:I = 0x7f070021

.field public static final award_logo:I = 0x7f07001f

.field public static final award_logo_background:I = 0x7f07001e

.field public static final award_received_message:I = 0x7f070029

.field public static final award_title:I = 0x7f070020

.field public static final awards_grid:I = 0x7f07002b

.field public static final awards_received_layout:I = 0x7f070028

.field public static final awards_title:I = 0x7f07002a

.field public static final bottom:I = 0x7f070001

.field public static final context_menu_fav_remove_station:I = 0x7f070187

.field public static final context_menu_fav_report_prices:I = 0x7f070186

.field public static final context_menu_station_fav_add:I = 0x7f0701a3

.field public static final context_menu_station_report_prices:I = 0x7f0701a0

.field public static final context_menu_station_show_map:I = 0x7f0701a2

.field public static final context_menu_station_upload_photo:I = 0x7f0701a1

.field public static final context_menu_station_view:I = 0x7f07019f

.field public static final contextmenu_list:I = 0x7f07002d

.field public static final dialog_content:I = 0x7f070031

.field public static final dialog_edittext:I = 0x7f07003f

.field public static final dialog_icon:I = 0x7f07002f

.field public static final dialog_label:I = 0x7f07003e

.field public static final dialog_layout_buttons:I = 0x7f070033

.field public static final dialog_left_space:I = 0x7f070034

.field public static final dialog_list:I = 0x7f070047

.field public static final dialog_list_other_item:I = 0x7f07004b

.field public static final dialog_list_other_item_checkbox:I = 0x7f07004c

.field public static final dialog_list_other_item_label:I = 0x7f07004a

.field public static final dialog_list_title:I = 0x7f07004d

.field public static final dialog_loading_message:I = 0x7f07004f

.field public static final dialog_loading_progress:I = 0x7f07004e

.field public static final dialog_message:I = 0x7f070032

.field public static final dialog_negativeButton:I = 0x7f070037

.field public static final dialog_neutralButton:I = 0x7f070036

.field public static final dialog_not_show_again:I = 0x7f070091

.field public static final dialog_positiveButton:I = 0x7f070035

.field public static final dialog_prize_contact_info_instructions:I = 0x7f070057

.field public static final dialog_report_prices_button_arrow:I = 0x7f07008e

.field public static final dialog_report_prices_button_price:I = 0x7f07008b

.field public static final dialog_report_prices_button_price_layout:I = 0x7f07008a

.field public static final dialog_report_prices_button_price_tenth_cent:I = 0x7f07008c

.field public static final dialog_report_prices_button_time:I = 0x7f07008d

.field public static final dialog_right_space:I = 0x7f070038

.field public static final dialog_title:I = 0x7f070030

.field public static final dialog_toast_layout:I = 0x7f070092

.field public static final dialog_toast_message:I = 0x7f070093

.field public static final dialog_tooltip_layout:I = 0x7f070094

.field public static final dialog_tooltip_message:I = 0x7f070095

.field public static final dialog_top_parent:I = 0x7f07002e

.field public static final favourite_button_next_icon:I = 0x7f0700a4

.field public static final favourite_button_previous_icon:I = 0x7f0700a2

.field public static final favourite_list_buttons_layout:I = 0x7f0700a0

.field public static final favourite_list_buttons_stroke:I = 0x7f0700a5

.field public static final favourite_list_displaying_label:I = 0x7f07009a

.field public static final favourite_list_fuel_type_division:I = 0x7f07009f

.field public static final favourite_list_fuel_type_label:I = 0x7f07009c

.field public static final favourite_list_fuel_type_layout:I = 0x7f07009b

.field public static final favourite_list_label:I = 0x7f070098

.field public static final favourite_list_next_button:I = 0x7f0700a3

.field public static final favourite_list_previous_button:I = 0x7f0700a1

.field public static final favourite_list_spinner:I = 0x7f070099

.field public static final favourite_list_spinner_layout:I = 0x7f070097

.field public static final favourite_list_station_label:I = 0x7f07009e

.field public static final favourite_list_status_label:I = 0x7f07009d

.field public static final favourite_station_address:I = 0x7f0700a8

.field public static final favourite_station_city:I = 0x7f0700a9

.field public static final favourite_station_fuel_update:I = 0x7f0700b3

.field public static final favourite_station_msl_match_status:I = 0x7f0700b4

.field public static final favourite_station_name:I = 0x7f0700a7

.field public static final favourite_station_price:I = 0x7f0700b0

.field public static final favourite_station_price_layout:I = 0x7f0700ae

.field public static final favourite_station_price_layout2:I = 0x7f0700af

.field public static final favourite_station_price_tenth_cent:I = 0x7f0700b1

.field public static final favourite_station_remove_button:I = 0x7f0700ad

.field public static final favourite_station_time:I = 0x7f0700b2

.field public static final feedback_comments:I = 0x7f070042

.field public static final feedback_sender:I = 0x7f070041

.field public static final feedback_sender_label:I = 0x7f070040

.field public static final filter_button_distance:I = 0x7f070045

.field public static final filter_button_icon:I = 0x7f0700b5

.field public static final filter_button_label:I = 0x7f0700b6

.field public static final filter_button_layout:I = 0x7f070043

.field public static final filter_button_price:I = 0x7f070046

.field public static final filter_button_station_name:I = 0x7f070044

.field public static final filter_distance:I = 0x7f070048

.field public static final filter_instructions:I = 0x7f0700b7

.field public static final filter_price:I = 0x7f070049

.field public static final filter_price_all_stations:I = 0x7f0700b9

.field public static final filter_radiusbar:I = 0x7f0700b8

.field public static final gb_checked_text_view:I = 0x7f07002c

.field public static final icon:I = 0x7f0700f0

.field public static final initial_screen_bottom_layout:I = 0x7f0700c2

.field public static final initial_screen_bottom_text:I = 0x7f0700be

.field public static final initial_screen_buttons_layout:I = 0x7f0700bf

.field public static final initial_screen_gbslogan2:I = 0x7f0700c3

.field public static final initial_screen_login:I = 0x7f0700c0

.field public static final initial_screen_no:I = 0x7f0700c1

.field public static final initial_screen_register:I = 0x7f0700c4

.field public static final keyboard_drawer:I = 0x7f070088

.field public static final left:I = 0x7f070002

.field public static final list_button_reload_icon:I = 0x7f0700c9

.field public static final list_button_sort_icon:I = 0x7f0700cb

.field public static final list_button_sort_label:I = 0x7f0700cc

.field public static final list_button_to_map_icon:I = 0x7f0700ce

.field public static final list_progress:I = 0x7f0700ab

.field public static final list_progress_area:I = 0x7f0700aa

.field public static final list_pull_header_image:I = 0x7f0700d2

.field public static final list_pull_header_progress:I = 0x7f0700d0

.field public static final list_pull_header_text:I = 0x7f0700d1

.field public static final list_remove_button_layout:I = 0x7f0700ac

.field public static final list_row_divider:I = 0x7f0700e3

.field public static final list_row_no_items_click:I = 0x7f0700ef

.field public static final list_row_no_items_message:I = 0x7f0700ee

.field public static final list_tabs_bottom_division:I = 0x7f0700c6

.field public static final list_tabs_layout:I = 0x7f0700c5

.field public static final list_with_title_button:I = 0x7f0700d4

.field public static final list_with_title_list:I = 0x7f0700d5

.field public static final list_with_title_title:I = 0x7f0700d3

.field public static final login_password:I = 0x7f070051

.field public static final login_username:I = 0x7f070050

.field public static final main_button_badge:I = 0x7f070102

.field public static final main_button_icon:I = 0x7f070100

.field public static final main_button_label:I = 0x7f070101

.field public static final main_button_nearme:I = 0x7f07010a

.field public static final main_button_nearme_layout:I = 0x7f07010c

.field public static final main_button_table:I = 0x7f070109

.field public static final main_button_table_layout:I = 0x7f070108

.field public static final main_favourites:I = 0x7f0700fe

.field public static final main_find_by_zip:I = 0x7f0700fb

.field public static final main_find_nearest:I = 0x7f0700fc

.field public static final main_gb_image:I = 0x7f07003a

.field public static final main_gb_image_layout:I = 0x7f070039

.field public static final main_gb_layout:I = 0x7f070103

.field public static final main_gb_loading_image:I = 0x7f0700bc

.field public static final main_gb_loading_image_layout:I = 0x7f0700bb

.field public static final main_gb_loading_layout:I = 0x7f0700bd

.field public static final main_gb_loading_screen:I = 0x7f0700ba

.field public static final main_login:I = 0x7f0700ff

.field public static final main_screen:I = 0x7f0700fa

.field public static final main_search_layout:I = 0x7f070104

.field public static final main_view_profile:I = 0x7f0700fd

.field public static final map_button_to_list_icon:I = 0x7f070112

.field public static final map_buttons_layout:I = 0x7f07010e

.field public static final map_mapview:I = 0x7f070018

.field public static final map_progress:I = 0x7f070110

.field public static final map_progress_area:I = 0x7f07010f

.field public static final map_station_address:I = 0x7f070113

.field public static final map_station_city:I = 0x7f070114

.field public static final map_station_fuel_label:I = 0x7f070117

.field public static final map_station_phone:I = 0x7f070115

.field public static final map_station_price:I = 0x7f070118

.field public static final map_station_price_tenth_cents:I = 0x7f070119

.field public static final map_station_price_time:I = 0x7f07011a

.field public static final map_station_url:I = 0x7f070116

.field public static final map_tabs_layout:I = 0x7f07010d

.field public static final map_to_list:I = 0x7f070111

.field public static final map_zoom_message_label:I = 0x7f07001a

.field public static final match_station_address:I = 0x7f0700ec

.field public static final match_station_checkbox:I = 0x7f0700e9

.field public static final match_station_city:I = 0x7f0700ed

.field public static final match_station_icon:I = 0x7f0700ea

.field public static final match_station_name:I = 0x7f0700eb

.field public static final member_id_layout:I = 0x7f070143

.field public static final near_me_buttons_layout:I = 0x7f0700c7

.field public static final near_me_buttons_top_division:I = 0x7f0700cf

.field public static final near_me_list_refresh:I = 0x7f0700c8

.field public static final near_me_list_sort:I = 0x7f0700ca

.field public static final near_me_list_to_map:I = 0x7f0700cd

.field public static final near_me_row_address:I = 0x7f0700de

.field public static final near_me_row_brand_name:I = 0x7f0700dd

.field public static final near_me_row_city:I = 0x7f0700df

.field public static final near_me_row_distance:I = 0x7f0700e0

.field public static final near_me_row_fuel_update:I = 0x7f0700db

.field public static final near_me_row_price:I = 0x7f0700d8

.field public static final near_me_row_price_layout:I = 0x7f0700d6

.field public static final near_me_row_price_layout2:I = 0x7f0700d7

.field public static final near_me_row_price_tenth_cent:I = 0x7f0700d9

.field public static final near_me_row_time:I = 0x7f0700da

.field public static final near_me_station_icon:I = 0x7f0700dc

.field public static final noitemrowlist:I = 0x7f0700f9

.field public static final option_menu_fav_add_list:I = 0x7f070188

.field public static final option_menu_fav_remove_list:I = 0x7f070189

.field public static final option_menu_fav_remove_station:I = 0x7f07018a

.field public static final option_menu_list_filter:I = 0x7f07018c

.field public static final option_menu_list_home:I = 0x7f07018f

.field public static final option_menu_list_places:I = 0x7f07018e

.field public static final option_menu_list_search:I = 0x7f070190

.field public static final option_menu_list_settings:I = 0x7f07018b

.field public static final option_menu_list_to_map:I = 0x7f07018d

.field public static final option_menu_main_screen_share:I = 0x7f070191

.field public static final option_menu_map_follow_me:I = 0x7f070193

.field public static final option_menu_map_home:I = 0x7f070197

.field public static final option_menu_map_search:I = 0x7f070195

.field public static final option_menu_map_settings:I = 0x7f070196

.field public static final option_menu_map_to_list:I = 0x7f070194

.field public static final option_menu_map_to_my_location:I = 0x7f070192

.field public static final option_menu_places_clean_list:I = 0x7f07019b

.field public static final option_menu_places_edit_delete:I = 0x7f07019a

.field public static final option_menu_places_settings:I = 0x7f070199

.field public static final option_menu_places_sort:I = 0x7f070198

.field public static final option_menu_profile_logout:I = 0x7f07019d

.field public static final option_menu_profile_upload_photo:I = 0x7f07019c

.field public static final option_menu_report_settings:I = 0x7f07019e

.field public static final option_menu_station_fav:I = 0x7f0701a7

.field public static final option_menu_station_report_prices:I = 0x7f0701a4

.field public static final option_menu_station_search:I = 0x7f0701a9

.field public static final option_menu_station_settings:I = 0x7f0701a8

.field public static final option_menu_station_show_map:I = 0x7f0701a6

.field public static final option_menu_station_upload_photo:I = 0x7f0701a5

.field public static final photo_large:I = 0x7f07011b

.field public static final photo_large_caption:I = 0x7f07011f

.field public static final photo_large_caption_label:I = 0x7f07011e

.field public static final photo_large_next:I = 0x7f070121

.field public static final photo_large_previous:I = 0x7f070120

.field public static final photo_large_title:I = 0x7f07011d

.field public static final photo_large_title_label:I = 0x7f07011c

.field public static final places_buttons_layout:I = 0x7f0700e6

.field public static final places_edit_button:I = 0x7f0700e7

.field public static final places_location:I = 0x7f0700e5

.field public static final places_name:I = 0x7f0700e4

.field public static final places_remove_button:I = 0x7f0700e8

.field public static final playKeyboard:I = 0x7f070122

.field public static final preference_about_feedback:I = 0x7f07003d

.field public static final preference_about_privacy_policy:I = 0x7f07003c

.field public static final preference_about_version:I = 0x7f07003b

.field public static final preference_radius_seekbar:I = 0x7f07006a

.field public static final preference_radius_seekbar_value:I = 0x7f07006b

.field public static final prize_contact_info_address:I = 0x7f07005c

.field public static final prize_contact_info_address2:I = 0x7f07005e

.field public static final prize_contact_info_address_verification:I = 0x7f07005d

.field public static final prize_contact_info_city:I = 0x7f07005f

.field public static final prize_contact_info_city_verification:I = 0x7f070060

.field public static final prize_contact_info_email:I = 0x7f070065

.field public static final prize_contact_info_email_verification:I = 0x7f070066

.field public static final prize_contact_info_first_name:I = 0x7f070058

.field public static final prize_contact_info_first_name_verification:I = 0x7f070059

.field public static final prize_contact_info_last_name:I = 0x7f07005a

.field public static final prize_contact_info_last_name_verification:I = 0x7f07005b

.field public static final prize_contact_info_state_spinner:I = 0x7f070061

.field public static final prize_contact_info_state_verification:I = 0x7f070062

.field public static final prize_contact_info_zip:I = 0x7f070063

.field public static final prize_contact_info_zip_verification:I = 0x7f070064

.field public static final prize_date:I = 0x7f070053

.field public static final prize_explanation:I = 0x7f070069

.field public static final prize_limit:I = 0x7f070056

.field public static final prize_points_per_report:I = 0x7f070068

.field public static final prize_tickets:I = 0x7f070055

.field public static final prize_tickets_label:I = 0x7f070054

.field public static final prize_title:I = 0x7f070052

.field public static final prize_total_points:I = 0x7f070067

.field public static final profile_button_awards:I = 0x7f07013d

.field public static final profile_button_awards_icon:I = 0x7f07013e

.field public static final profile_button_logout:I = 0x7f07013f

.field public static final profile_button_logout_icon:I = 0x7f070140

.field public static final profile_button_upload_picture_icon:I = 0x7f07013c

.field public static final profile_consecutive_days:I = 0x7f070139

.field public static final profile_consecutive_days_label:I = 0x7f070138

.field public static final profile_forum_posts:I = 0x7f070147

.field public static final profile_forum_posts_label:I = 0x7f070146

.field public static final profile_forum_title:I = 0x7f070145

.field public static final profile_forum_title_label:I = 0x7f070144

.field public static final profile_gasbuddy_age:I = 0x7f07012c

.field public static final profile_gasbuddy_age_label:I = 0x7f07012b

.field public static final profile_join_date:I = 0x7f07012a

.field public static final profile_join_date_label:I = 0x7f070129

.field public static final profile_overall_rank:I = 0x7f070135

.field public static final profile_overall_rank_30_days:I = 0x7f070137

.field public static final profile_overall_rank_30_days_label:I = 0x7f070136

.field public static final profile_overall_rank_label:I = 0x7f070134

.field public static final profile_picture:I = 0x7f070126

.field public static final profile_picture_loading_progress:I = 0x7f070142

.field public static final profile_point_balance:I = 0x7f07012f

.field public static final profile_point_balance_label:I = 0x7f07012e

.field public static final profile_points_today:I = 0x7f070131

.field public static final profile_points_today_label:I = 0x7f070130

.field public static final profile_site_label:I = 0x7f070127

.field public static final profile_site_name:I = 0x7f070128

.field public static final profile_stats_label:I = 0x7f07012d

.field public static final profile_title:I = 0x7f070141

.field public static final profile_total_points:I = 0x7f070133

.field public static final profile_total_points_label:I = 0x7f070132

.field public static final profile_upload_picture:I = 0x7f07013b

.field public static final profile_user_car:I = 0x7f070124

.field public static final profile_user_member_id:I = 0x7f070125

.field public static final register_email:I = 0x7f070070

.field public static final register_email_verification:I = 0x7f070071

.field public static final register_password:I = 0x7f070074

.field public static final register_password_verification:I = 0x7f070075

.field public static final register_username:I = 0x7f07006c

.field public static final register_username_progress:I = 0x7f07006e

.field public static final register_username_progress_area:I = 0x7f07006d

.field public static final register_username_verification:I = 0x7f07006f

.field public static final register_zip:I = 0x7f070072

.field public static final register_zip_verification:I = 0x7f070073

.field public static final report_comments:I = 0x7f070086

.field public static final report_comments_label:I = 0x7f070085

.field public static final report_diesel_price:I = 0x7f070080

.field public static final report_diesel_price_button:I = 0x7f070082

.field public static final report_diesel_price_edittext:I = 0x7f070081

.field public static final report_fields_container:I = 0x7f070076

.field public static final report_midgrade_price:I = 0x7f07007a

.field public static final report_midgrade_price_button:I = 0x7f07007c

.field public static final report_midgrade_price_edittext:I = 0x7f07007b

.field public static final report_premium_price:I = 0x7f07007d

.field public static final report_premium_price_button:I = 0x7f07007f

.field public static final report_premium_price_edittext:I = 0x7f07007e

.field public static final report_price_button_row:I = 0x7f070089

.field public static final report_regular_price:I = 0x7f070077

.field public static final report_regular_price_button:I = 0x7f070079

.field public static final report_regular_price_edittext:I = 0x7f070078

.field public static final report_submitprices_button:I = 0x7f070087

.field public static final report_time_spotted_label:I = 0x7f070083

.field public static final report_time_spotted_spinner:I = 0x7f070084

.field public static final right:I = 0x7f070003

.field public static final search_autocomplete:I = 0x7f070107

.field public static final search_bar:I = 0x7f070148

.field public static final search_city_result:I = 0x7f07014a

.field public static final search_label:I = 0x7f070105

.field public static final search_layout:I = 0x7f070149

.field public static final search_zip_city_button:I = 0x7f070106

.field public static final selected_station_address:I = 0x7f070153

.field public static final selected_station_ammenities_title:I = 0x7f07015e

.field public static final selected_station_button_center_map:I = 0x7f070168

.field public static final selected_station_button_center_map_icon:I = 0x7f070169

.field public static final selected_station_button_report:I = 0x7f070158

.field public static final selected_station_button_report_price:I = 0x7f070166

.field public static final selected_station_button_report_price_icon:I = 0x7f070167

.field public static final selected_station_button_upload_picture:I = 0x7f070164

.field public static final selected_station_button_upload_picture_icon:I = 0x7f070165

.field public static final selected_station_buttons_layout:I = 0x7f07013a

.field public static final selected_station_city:I = 0x7f070154

.field public static final selected_station_distance:I = 0x7f070157

.field public static final selected_station_features_loading:I = 0x7f07015f

.field public static final selected_station_gallery:I = 0x7f070163

.field public static final selected_station_gallery_loading:I = 0x7f070162

.field public static final selected_station_is_fav:I = 0x7f07014e

.field public static final selected_station_layout:I = 0x7f07014c

.field public static final selected_station_layout_ammenities:I = 0x7f070160

.field public static final selected_station_logo:I = 0x7f07014d

.field public static final selected_station_logo_layout:I = 0x7f070123

.field public static final selected_station_name:I = 0x7f07014f

.field public static final selected_station_phone:I = 0x7f070156

.field public static final selected_station_photo_collection_title:I = 0x7f070161

.field public static final selected_station_picture:I = 0x7f070151

.field public static final selected_station_picture_layout:I = 0x7f070150

.field public static final selected_station_picture_upload_label:I = 0x7f070152

.field public static final selected_station_postal_cd:I = 0x7f070155

.field public static final selected_station_price_comments:I = 0x7f07015d

.field public static final selected_station_price_diesel:I = 0x7f07015c

.field public static final selected_station_price_midgrade:I = 0x7f07015a

.field public static final selected_station_price_premium:I = 0x7f07015b

.field public static final selected_station_price_regular:I = 0x7f070159

.field public static final selected_station_scroll_view:I = 0x7f07014b

.field public static final share_email:I = 0x7f070090

.field public static final share_sms:I = 0x7f07008f

.field public static final station_mapview:I = 0x7f07016a

.field public static final station_price_row_fuel_type:I = 0x7f07016b

.field public static final station_price_row_memeber:I = 0x7f070172

.field public static final station_price_row_price:I = 0x7f07016e

.field public static final station_price_row_price_layout:I = 0x7f07016c

.field public static final station_price_row_price_layout2:I = 0x7f07016d

.field public static final station_price_row_price_tenth_cent:I = 0x7f07016f

.field public static final station_price_row_time:I = 0x7f070171

.field public static final station_price_row_time_layout:I = 0x7f070170

.field public static final station_price_row_update:I = 0x7f070173

.field public static final tab_division:I = 0x7f070175

.field public static final tab_text:I = 0x7f070174

.field public static final text:I = 0x7f0700f1

.field public static final top:I = 0x7f070000

.field public static final vk_speechkey:I = 0x7f070185

.field public static final webview:I = 0x7f070096

.field public static final winner_row_car_icon:I = 0x7f0700f5

.field public static final winner_row_date:I = 0x7f0700f7

.field public static final winner_row_name:I = 0x7f0700f4

.field public static final winner_row_photo:I = 0x7f0700f3

.field public static final winner_row_price_layout:I = 0x7f0700f2

.field public static final winner_row_prize:I = 0x7f0700f8

.field public static final winner_row_site:I = 0x7f0700f6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
