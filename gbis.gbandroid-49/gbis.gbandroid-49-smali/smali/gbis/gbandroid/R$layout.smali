.class public final Lgbis/gbandroid/R$layout;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final ad_bar:I = 0x7f030000

.field public static final ad_bar_big:I = 0x7f030001

.field public static final ad_station_details:I = 0x7f030002

.field public static final add_station_address:I = 0x7f030003

.field public static final add_station_details:I = 0x7f030004

.field public static final add_station_map:I = 0x7f030005

.field public static final amenity:I = 0x7f030006

.field public static final award:I = 0x7f030007

.field public static final award_details:I = 0x7f030008

.field public static final award_received:I = 0x7f030009

.field public static final awards:I = 0x7f03000a

.field public static final checkbox_with_text:I = 0x7f03000b

.field public static final contextmenu_list:I = 0x7f03000c

.field public static final contextmenuitem_list:I = 0x7f03000d

.field public static final customkeyboard:I = 0x7f03000e

.field public static final dialog:I = 0x7f03000f

.field public static final dialog_about:I = 0x7f030010

.field public static final dialog_edittext:I = 0x7f030011

.field public static final dialog_feedback:I = 0x7f030012

.field public static final dialog_filter:I = 0x7f030013

.field public static final dialog_list:I = 0x7f030014

.field public static final dialog_list_other_item:I = 0x7f030015

.field public static final dialog_list_with_title:I = 0x7f030016

.field public static final dialog_loading:I = 0x7f030017

.field public static final dialog_login:I = 0x7f030018

.field public static final dialog_prize:I = 0x7f030019

.field public static final dialog_prizecontactinfo:I = 0x7f03001a

.field public static final dialog_prizeinfo:I = 0x7f03001b

.field public static final dialog_radiusbar:I = 0x7f03001c

.field public static final dialog_register:I = 0x7f03001d

.field public static final dialog_report_prices:I = 0x7f03001e

.field public static final dialog_report_prices_button:I = 0x7f03001f

.field public static final dialog_share:I = 0x7f030020

.field public static final dialog_smart_prompt:I = 0x7f030021

.field public static final dialog_toast:I = 0x7f030022

.field public static final dialog_tooltip:I = 0x7f030023

.field public static final dialog_webview:I = 0x7f030024

.field public static final favlist:I = 0x7f030025

.field public static final favrowdelete:I = 0x7f030026

.field public static final favrowlist:I = 0x7f030027

.field public static final filter_button:I = 0x7f030028

.field public static final filter_distance:I = 0x7f030029

.field public static final filter_price:I = 0x7f03002a

.field public static final gb_list_checked_item:I = 0x7f03002b

.field public static final gb_preference:I = 0x7f03002c

.field public static final gb_preference_category:I = 0x7f03002d

.field public static final gb_preference_checkbox:I = 0x7f03002e

.field public static final gb_preference_dialog:I = 0x7f03002f

.field public static final gb_preference_list:I = 0x7f030030

.field public static final gb_preference_title:I = 0x7f030031

.field public static final gb_spinner_dropdown_item:I = 0x7f030032

.field public static final gb_spinner_item:I = 0x7f030033

.field public static final init_screen:I = 0x7f030034

.field public static final initial_screen:I = 0x7f030035

.field public static final initial_screen_signup:I = 0x7f030036

.field public static final initialize:I = 0x7f030037

.field public static final list:I = 0x7f030038

.field public static final list3:I = 0x7f030039

.field public static final list_pull_header:I = 0x7f03003a

.field public static final list_with_title:I = 0x7f03003b

.field public static final listrow:I = 0x7f03003c

.field public static final listrow_autocomplete:I = 0x7f03003d

.field public static final listrow_divider:I = 0x7f03003e

.field public static final listrow_edit_delete:I = 0x7f03003f

.field public static final listrow_match_station:I = 0x7f030040

.field public static final listrow_more_items:I = 0x7f030041

.field public static final listrow_no_items:I = 0x7f030042

.field public static final listrow_text:I = 0x7f030043

.field public static final listrow_text_image:I = 0x7f030044

.field public static final listrow_winner:I = 0x7f030045

.field public static final listrownoitem:I = 0x7f030046

.field public static final main_backup:I = 0x7f030047

.field public static final main_buttons:I = 0x7f030048

.field public static final main_screen:I = 0x7f030049

.field public static final map:I = 0x7f03004a

.field public static final mapaddialog:I = 0x7f03004b

.field public static final mapstationdialog:I = 0x7f03004c

.field public static final photo_large:I = 0x7f03004d

.field public static final pricekeyboard_drawer:I = 0x7f03004e

.field public static final profile:I = 0x7f03004f

.field public static final profile3:I = 0x7f030050

.field public static final searchbar:I = 0x7f030051

.field public static final searchitem:I = 0x7f030052

.field public static final station:I = 0x7f030053

.field public static final station_map:I = 0x7f030054

.field public static final station_old:I = 0x7f030055

.field public static final station_price_row:I = 0x7f030056

.field public static final tab_list:I = 0x7f030057

.field public static final tabhost:I = 0x7f030058

.field public static final widget_list:I = 0x7f030059

.field public static final widget_trend:I = 0x7f03005a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
