.class public final Lgbis/gbandroid/R$string;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_station_address:I = 0x7f090058

.field public static final add_station_city:I = 0x7f09005c

.field public static final add_station_cross_street:I = 0x7f090059

.field public static final add_station_geocoder_error:I = 0x7f090065

.field public static final add_station_gps_no_address:I = 0x7f090064

.field public static final add_station_name:I = 0x7f090057

.field public static final add_station_no_address:I = 0x7f090060

.field public static final add_station_no_city:I = 0x7f090061

.field public static final add_station_no_name:I = 0x7f09005f

.field public static final add_station_no_state:I = 0x7f090062

.field public static final add_station_no_station_selected:I = 0x7f090063

.field public static final add_station_path:I = 0x7f0901ea

.field public static final add_station_phone:I = 0x7f09005a

.field public static final add_station_pumps:I = 0x7f09005e

.field public static final add_station_state:I = 0x7f09005d

.field public static final add_station_title:I = 0x7f090056

.field public static final add_station_zip:I = 0x7f09005b

.field public static final admob_mediation_id:I = 0x7f090216

.field public static final admob_publisher_id:I = 0x7f090215

.field public static final adwhirl_home_sdk_key:I = 0x7f090223

.field public static final adwhirl_init_sdk_key:I = 0x7f090222

.field public static final adwhirl_list_sdk_key:I = 0x7f090224

.field public static final adwhirl_sdk_key:I = 0x7f090221

.field public static final amazon_ads_key:I = 0x7f090225

.field public static final analytics_session:I = 0x7f090226

.field public static final app_name:I = 0x7f0901bd

.field public static final award_details_history:I = 0x7f0900ca

.field public static final award_details_level:I = 0x7f0900c9

.field public static final award_details_progress_text:I = 0x7f0900c8

.field public static final award_level:I = 0x7f0900cb

.field public static final awards_details_path:I = 0x7f0901ed

.field public static final awards_images_path:I = 0x7f09020d

.field public static final awards_list_path:I = 0x7f0901ec

.field public static final brand_autocomplete_path:I = 0x7f0901eb

.field public static final brands_download_path:I = 0x7f0901ee

.field public static final button_about_feedback:I = 0x7f090156

.field public static final button_add_station:I = 0x7f09015f

.field public static final button_awards:I = 0x7f09018d

.field public static final button_dialog_about_privacy_policy:I = 0x7f0901a6

.field public static final button_dialog_cancel:I = 0x7f090199

.field public static final button_dialog_change_tickets:I = 0x7f0901a5

.field public static final button_dialog_done:I = 0x7f09019c

.field public static final button_dialog_dont_know:I = 0x7f09019d

.field public static final button_dialog_get_tickets:I = 0x7f0901a0

.field public static final button_dialog_learn_more:I = 0x7f0901a1

.field public static final button_dialog_login:I = 0x7f090195

.field public static final button_dialog_member_suggestion:I = 0x7f09019f

.field public static final button_dialog_no:I = 0x7f09019b

.field public static final button_dialog_ok:I = 0x7f090198

.field public static final button_dialog_places:I = 0x7f0901a3

.field public static final button_dialog_register:I = 0x7f090197

.field public static final button_dialog_report:I = 0x7f0901a4

.field public static final button_dialog_reset_password:I = 0x7f090196

.field public static final button_dialog_skip:I = 0x7f09019e

.field public static final button_dialog_winners:I = 0x7f0901a2

.field public static final button_dialog_yes:I = 0x7f09019a

.field public static final button_edit:I = 0x7f090162

.field public static final button_fav_add_list:I = 0x7f090176

.field public static final button_fav_add_station:I = 0x7f090175

.field public static final button_fav_manage_favorites:I = 0x7f090178

.field public static final button_fav_next:I = 0x7f090174

.field public static final button_fav_previous:I = 0x7f090173

.field public static final button_fav_remove_list:I = 0x7f090179

.field public static final button_fav_remove_station:I = 0x7f090177

.field public static final button_feedback_send:I = 0x7f09017a

.field public static final button_filter:I = 0x7f090168

.field public static final button_filter_apply:I = 0x7f090193

.field public static final button_home:I = 0x7f09015e

.field public static final button_initial_screen_yes_login:I = 0x7f090170

.field public static final button_initial_screen_yes_signup:I = 0x7f090171

.field public static final button_main_favourite:I = 0x7f09016b

.field public static final button_main_login:I = 0x7f09016d

.field public static final button_main_nearme:I = 0x7f090169

.field public static final button_main_places:I = 0x7f09016f

.field public static final button_main_prize:I = 0x7f09016e

.field public static final button_main_profile:I = 0x7f09016c

.field public static final button_main_settings:I = 0x7f09016a

.field public static final button_map_follow_me:I = 0x7f09017d

.field public static final button_map_my_location:I = 0x7f09017f

.field public static final button_map_station_details:I = 0x7f09017b

.field public static final button_map_station_report:I = 0x7f09017c

.field public static final button_map_stop_follow_me:I = 0x7f09017e

.field public static final button_menu:I = 0x7f090194

.field public static final button_photo_next:I = 0x7f090182

.field public static final button_photo_next_landscape:I = 0x7f090184

.field public static final button_photo_previous:I = 0x7f090183

.field public static final button_photo_previous_landscape:I = 0x7f090185

.field public static final button_places:I = 0x7f090163

.field public static final button_places_clear_list:I = 0x7f090167

.field public static final button_places_edit_delete:I = 0x7f090166

.field public static final button_places_sort_by_frequency:I = 0x7f090164

.field public static final button_places_sort_by_name:I = 0x7f090165

.field public static final button_profile_logout:I = 0x7f09018c

.field public static final button_reload:I = 0x7f09015a

.field public static final button_remove:I = 0x7f090161

.field public static final button_report_clear:I = 0x7f090172

.field public static final button_search:I = 0x7f09015d

.field public static final button_settings:I = 0x7f09015c

.field public static final button_share:I = 0x7f09018e

.field public static final button_share_app_email:I = 0x7f090190

.field public static final button_share_app_facebook:I = 0x7f090191

.field public static final button_share_app_sms:I = 0x7f09018f

.field public static final button_share_app_twitter:I = 0x7f090192

.field public static final button_sort_by_distance:I = 0x7f090157

.field public static final button_sort_by_price:I = 0x7f090158

.field public static final button_station_directions:I = 0x7f090189

.field public static final button_station_report_price:I = 0x7f090187

.field public static final button_station_report_prices:I = 0x7f090188

.field public static final button_station_show_on_map:I = 0x7f09018a

.field public static final button_station_upload_photo:I = 0x7f090180

.field public static final button_station_upload_photo_not_member:I = 0x7f090181

.field public static final button_station_upload_picture:I = 0x7f090186

.field public static final button_station_view:I = 0x7f09018b

.field public static final button_to_list:I = 0x7f09015b

.field public static final button_to_map:I = 0x7f090159

.field public static final button_use_my_location:I = 0x7f090160

.field public static final canada_red_prices_url:I = 0x7f0901d2

.field public static final cenlat:I = 0x7f0901bf

.field public static final cenlong:I = 0x7f0901c0

.field public static final city_autocomplete_path:I = 0x7f0901e8

.field public static final city_autocomplete_url:I = 0x7f0901d7

.field public static final context_menu_list_directions:I = 0x7f0901b9

.field public static final context_menu_list_report_prices:I = 0x7f0901b7

.field public static final context_menu_list_show_on_map:I = 0x7f0901ba

.field public static final context_menu_list_upload_picture:I = 0x7f0901b8

.field public static final context_menu_list_view_station:I = 0x7f0901b6

.field public static final crash_report_path:I = 0x7f0901ef

.field public static final db_error:I = 0x7f090002

.field public static final default_fuel_type:I = 0x7f0901c8

.field public static final dialog_about_contact:I = 0x7f090130

.field public static final dialog_about_version:I = 0x7f09012f

.field public static final dialog_fav_new_list:I = 0x7f09013c

.field public static final dialog_feedback_app:I = 0x7f09012e

.field public static final dialog_feedback_comments:I = 0x7f09013b

.field public static final dialog_feedback_sender_email:I = 0x7f09013a

.field public static final dialog_feedback_sender_member:I = 0x7f090139

.field public static final dialog_filter_distance:I = 0x7f090140

.field public static final dialog_filter_distance_instructions:I = 0x7f090142

.field public static final dialog_filter_price:I = 0x7f090141

.field public static final dialog_filter_price_instructions:I = 0x7f090143

.field public static final dialog_filter_station_name:I = 0x7f09013f

.field public static final dialog_like_app:I = 0x7f09012b

.field public static final dialog_login_password:I = 0x7f090132

.field public static final dialog_login_username:I = 0x7f090131

.field public static final dialog_message_error_station_gallery:I = 0x7f09009b

.field public static final dialog_message_loading:I = 0x7f09008d

.field public static final dialog_message_loading_awards:I = 0x7f090095

.field public static final dialog_message_loading_fav_add_station:I = 0x7f090091

.field public static final dialog_message_loading_fav_list:I = 0x7f090092

.field public static final dialog_message_loading_fav_remove_list:I = 0x7f090093

.field public static final dialog_message_loading_fav_remove_station:I = 0x7f090094

.field public static final dialog_message_loading_favorites:I = 0x7f09008a

.field public static final dialog_message_loading_getting_ticket:I = 0x7f090090

.field public static final dialog_message_loading_more_stations:I = 0x7f090088

.field public static final dialog_message_loading_pins:I = 0x7f09008b

.field public static final dialog_message_loading_prices:I = 0x7f09008c

.field public static final dialog_message_loading_prize_info:I = 0x7f09008e

.field public static final dialog_message_loading_station_features:I = 0x7f090099

.field public static final dialog_message_loading_station_gallery:I = 0x7f09009a

.field public static final dialog_message_loading_stations:I = 0x7f090086

.field public static final dialog_message_loading_stations_near_me:I = 0x7f090085

.field public static final dialog_message_loading_stations_places:I = 0x7f090087

.field public static final dialog_message_loading_title:I = 0x7f090084

.field public static final dialog_message_loading_winners:I = 0x7f09008f

.field public static final dialog_message_no_internet:I = 0x7f090096

.field public static final dialog_message_places_exists:I = 0x7f09009e

.field public static final dialog_message_places_name_exists:I = 0x7f09009d

.field public static final dialog_message_places_no_name:I = 0x7f09009c

.field public static final dialog_message_places_succesful:I = 0x7f09009f

.field public static final dialog_message_reloading_stations:I = 0x7f090089

.field public static final dialog_message_settings_all_stations:I = 0x7f090097

.field public static final dialog_message_settings_smart_prompt:I = 0x7f090098

.field public static final dialog_places_clear_message:I = 0x7f09013e

.field public static final dialog_places_label:I = 0x7f09013d

.field public static final dialog_prize_contact_info_address:I = 0x7f090147

.field public static final dialog_prize_contact_info_city:I = 0x7f090148

.field public static final dialog_prize_contact_info_email:I = 0x7f09014b

.field public static final dialog_prize_contact_info_first_name:I = 0x7f090145

.field public static final dialog_prize_contact_info_instructions:I = 0x7f09014c

.field public static final dialog_prize_contact_info_last_name:I = 0x7f090146

.field public static final dialog_prize_contact_info_legend:I = 0x7f09014d

.field public static final dialog_prize_contact_info_state:I = 0x7f090149

.field public static final dialog_prize_contact_info_zip:I = 0x7f09014a

.field public static final dialog_prize_info_points_per_report:I = 0x7f09014e

.field public static final dialog_prize_tickets:I = 0x7f090144

.field public static final dialog_rate_app:I = 0x7f09012d

.field public static final dialog_register_email:I = 0x7f090134

.field public static final dialog_register_instructions:I = 0x7f090138

.field public static final dialog_register_password:I = 0x7f090135

.field public static final dialog_register_password_confirm:I = 0x7f090136

.field public static final dialog_register_username:I = 0x7f090133

.field public static final dialog_register_zip:I = 0x7f090137

.field public static final dialog_reset_password_instructions:I = 0x7f09014f

.field public static final dialog_share_app:I = 0x7f09012c

.field public static final dialog_title_about:I = 0x7f09010a

.field public static final dialog_title_add_station_address:I = 0x7f090126

.field public static final dialog_title_add_station_details:I = 0x7f090127

.field public static final dialog_title_add_station_map:I = 0x7f090125

.field public static final dialog_title_attention:I = 0x7f090124

.field public static final dialog_title_distance:I = 0x7f09010f

.field public static final dialog_title_error:I = 0x7f090115

.field public static final dialog_title_fav_station_add_list:I = 0x7f09011a

.field public static final dialog_title_fav_station_add_station:I = 0x7f090119

.field public static final dialog_title_fav_station_remove_list:I = 0x7f09011b

.field public static final dialog_title_fav_station_remove_station:I = 0x7f09011c

.field public static final dialog_title_favorite:I = 0x7f090113

.field public static final dialog_title_feedback:I = 0x7f090114

.field public static final dialog_title_feedback_app:I = 0x7f09011f

.field public static final dialog_title_filter:I = 0x7f090121

.field public static final dialog_title_fuel_type:I = 0x7f09010e

.field public static final dialog_title_help:I = 0x7f090108

.field public static final dialog_title_initial_screen:I = 0x7f090111

.field public static final dialog_title_like_app:I = 0x7f09011d

.field public static final dialog_title_login:I = 0x7f09010b

.field public static final dialog_title_nearme_search_type:I = 0x7f090110

.field public static final dialog_title_places:I = 0x7f090120

.field public static final dialog_title_privacy_policy:I = 0x7f090109

.field public static final dialog_title_prize:I = 0x7f090117

.field public static final dialog_title_prize_info:I = 0x7f090118

.field public static final dialog_title_rate_app:I = 0x7f09011e

.field public static final dialog_title_refresh_rate:I = 0x7f090112

.field public static final dialog_title_register:I = 0x7f09010c

.field public static final dialog_title_register_member_suggest:I = 0x7f090122

.field public static final dialog_title_report_not_logged_in:I = 0x7f090116

.field public static final dialog_title_reset_password:I = 0x7f090129

.field public static final dialog_title_search_radius:I = 0x7f09010d

.field public static final dialog_title_share:I = 0x7f090128

.field public static final dialog_title_smart_prompt:I = 0x7f09012a

.field public static final dialog_title_warning:I = 0x7f090123

.field public static final diesel_name:I = 0x7f0901d0

.field public static final diesel_type:I = 0x7f0901cc

.field public static final fav_report_path:I = 0x7f090200

.field public static final fav_station_add_list_error:I = 0x7f090072

.field public static final fav_station_add_list_no_input:I = 0x7f090070

.field public static final fav_station_add_list_path:I = 0x7f0901f0

.field public static final fav_station_add_list_success:I = 0x7f090071

.field public static final fav_station_add_station_path:I = 0x7f0901f1

.field public static final fav_station_by_list_path:I = 0x7f0901f5

.field public static final fav_station_current_list:I = 0x7f090078

.field public static final fav_station_displaying_none_text:I = 0x7f09006f

.field public static final fav_station_displaying_text:I = 0x7f09006e

.field public static final fav_station_list_path:I = 0x7f0901f2

.field public static final fav_station_no_stations:I = 0x7f09006b

.field public static final fav_station_no_stations_footer_message:I = 0x7f09006d

.field public static final fav_station_no_stations_footer_title:I = 0x7f09006c

.field public static final fav_station_not_matched:I = 0x7f090077

.field public static final fav_station_remove_list_error:I = 0x7f090074

.field public static final fav_station_remove_list_not_selected_error:I = 0x7f090075

.field public static final fav_station_remove_list_path:I = 0x7f0901f3

.field public static final fav_station_remove_list_success:I = 0x7f090073

.field public static final fav_station_remove_station_error:I = 0x7f090076

.field public static final fav_station_remove_station_path:I = 0x7f0901f4

.field public static final fav_station_station:I = 0x7f09007a

.field public static final fav_station_status:I = 0x7f090079

.field public static final feedback_error:I = 0x7f090069

.field public static final feedback_error_empty:I = 0x7f09006a

.field public static final feedback_path:I = 0x7f0901f6

.field public static final feedback_sent:I = 0x7f090068

.field public static final fuel_type:I = 0x7f0901c7

.field public static final geocode_path:I = 0x7f0901f7

.field public static final google_admob_ads_favorites_screen:I = 0x7f09021f

.field public static final google_admob_ads_home_screen:I = 0x7f090219

.field public static final google_admob_ads_id:I = 0x7f090217

.field public static final google_admob_ads_init_screen:I = 0x7f09021a

.field public static final google_admob_ads_list_screen:I = 0x7f09021b

.field public static final google_admob_ads_list_screen_bottom:I = 0x7f09021d

.field public static final google_admob_ads_list_screen_top:I = 0x7f09021c

.field public static final google_admob_ads_profile_screen:I = 0x7f09021e

.field public static final google_admob_ads_station_screen:I = 0x7f090220

.field public static final google_admob_search_ads_id:I = 0x7f090218

.field public static final google_analytics_page_feedback:I = 0x7f090235

.field public static final google_analytics_page_home:I = 0x7f09022d

.field public static final google_analytics_page_init_loading:I = 0x7f09022a

.field public static final google_analytics_page_init_login:I = 0x7f09022c

.field public static final google_analytics_page_init_sing_up:I = 0x7f09022b

.field public static final google_analytics_page_member_award_details:I = 0x7f09023d

.field public static final google_analytics_page_member_award_list:I = 0x7f09023c

.field public static final google_analytics_page_member_favorites_list:I = 0x7f090243

.field public static final google_analytics_page_member_favorites_list_add:I = 0x7f090244

.field public static final google_analytics_page_member_favorites_list_remove:I = 0x7f090245

.field public static final google_analytics_page_member_favorites_station_add:I = 0x7f090246

.field public static final google_analytics_page_member_favorites_station_remove:I = 0x7f090247

.field public static final google_analytics_page_member_forgot_password:I = 0x7f090239

.field public static final google_analytics_page_member_login:I = 0x7f090237

.field public static final google_analytics_page_member_prize_contact_info:I = 0x7f090242

.field public static final google_analytics_page_member_prize_learn_more:I = 0x7f09023f

.field public static final google_analytics_page_member_prize_terms_and_conditions:I = 0x7f090241

.field public static final google_analytics_page_member_prize_tickets:I = 0x7f09023e

.field public static final google_analytics_page_member_prize_winners_list:I = 0x7f090240

.field public static final google_analytics_page_member_profile:I = 0x7f09023a

.field public static final google_analytics_page_member_profile_uplad_photo:I = 0x7f09023b

.field public static final google_analytics_page_member_register:I = 0x7f090238

.field public static final google_analytics_page_member_suggest_station_address:I = 0x7f090249

.field public static final google_analytics_page_member_suggest_station_features:I = 0x7f09024a

.field public static final google_analytics_page_member_suggest_station_map:I = 0x7f090248

.field public static final google_analytics_page_member_suggest_station_match:I = 0x7f09024b

.field public static final google_analytics_page_report_price:I = 0x7f090233

.field public static final google_analytics_page_report_price_favorite:I = 0x7f090234

.field public static final google_analytics_page_search:I = 0x7f09022e

.field public static final google_analytics_page_settings:I = 0x7f09022f

.field public static final google_analytics_page_settings_about:I = 0x7f090230

.field public static final google_analytics_page_settings_help:I = 0x7f090231

.field public static final google_analytics_page_settings_privacy_policy:I = 0x7f090232

.field public static final google_analytics_page_share_app:I = 0x7f090236

.field public static final google_analytics_page_station_details:I = 0x7f09024c

.field public static final google_analytics_page_station_details_map:I = 0x7f09024e

.field public static final google_analytics_page_station_details_photo_viewer:I = 0x7f09024d

.field public static final google_analytics_page_station_details_upload_photo:I = 0x7f09024f

.field public static final google_analytics_page_station_details_upload_photo_description:I = 0x7f090250

.field public static final google_analytics_page_stations_filter:I = 0x7f090255

.field public static final google_analytics_page_stations_list:I = 0x7f090251

.field public static final google_analytics_page_stations_map:I = 0x7f090252

.field public static final google_analytics_page_stations_saved_searches:I = 0x7f090253

.field public static final google_analytics_page_stations_saved_searches_add:I = 0x7f090254

.field public static final google_analytics_request:I = 0x7f090256

.field public static final google_analytics_request_favorite:I = 0x7f090258

.field public static final google_analytics_request_normal:I = 0x7f090257

.field public static final google_analytics_request_report_price:I = 0x7f090259

.field public static final google_analytics_request_sign_up:I = 0x7f09025a

.field public static final google_map_key:I = 0x7f090213

.field public static final google_map_key_debug:I = 0x7f090212

.field public static final google_map_key_debug_old:I = 0x7f090214

.field public static final help_path:I = 0x7f09020b

.field public static final host:I = 0x7f0901dd

.field public static final host1:I = 0x7f0901e0

.field public static final host2:I = 0x7f0901e3

.field public static final host3:I = 0x7f0901e2

.field public static final host4:I = 0x7f0901e1

.field public static final host_godzilla:I = 0x7f0901e5

.field public static final host_load:I = 0x7f0901de

.field public static final host_path:I = 0x7f0901e6

.field public static final host_t:I = 0x7f0901e4

.field public static final host_test:I = 0x7f0901df

.field public static final init_message:I = 0x7f090005

.field public static final init_path:I = 0x7f0901e7

.field public static final initial_screen_description:I = 0x7f0900b0

.field public static final initial_screen_gbaccount:I = 0x7f0900ae

.field public static final initial_screen_gbslogan:I = 0x7f0900ac

.field public static final initial_screen_gbslogan2:I = 0x7f0900ad

.field public static final initial_screen_signup:I = 0x7f0900af

.field public static final internet_error:I = 0x7f090001

.field public static final list_division_other:I = 0x7f090034

.field public static final list_division_search_terms:I = 0x7f090033

.field public static final list_division_tap:I = 0x7f090035

.field public static final list_filter_error:I = 0x7f09002c

.field public static final list_no_more_stations_found_message:I = 0x7f090023

.field public static final list_no_prices:I = 0x7f09002d

.field public static final list_no_stations_filter_click:I = 0x7f090027

.field public static final list_no_stations_filter_message:I = 0x7f090028

.field public static final list_no_stations_filter_message_old:I = 0x7f09002b

.field public static final list_no_stations_filter_old:I = 0x7f09002a

.field public static final list_no_stations_found:I = 0x7f090021

.field public static final list_no_stations_found_add_click:I = 0x7f090025

.field public static final list_no_stations_found_add_message:I = 0x7f090026

.field public static final list_no_stations_found_click:I = 0x7f090024

.field public static final list_no_stations_found_message:I = 0x7f090022

.field public static final list_no_stations_found_unsupported_country:I = 0x7f090020

.field public static final list_no_stations_near_by:I = 0x7f090029

.field public static final list_path:I = 0x7f0901f8

.field public static final list_pull_header_pull:I = 0x7f090031

.field public static final list_pull_header_release:I = 0x7f090032

.field public static final list_pull_header_tap:I = 0x7f090030

.field public static final list_report:I = 0x7f09002f

.field public static final list_tooltip_report_prices:I = 0x7f090036

.field public static final list_update:I = 0x7f09002e

.field public static final list_url:I = 0x7f0901d8

.field public static final login_no_nickname:I = 0x7f09007d

.field public static final login_no_password:I = 0x7f09007e

.field public static final login_path:I = 0x7f0901fa

.field public static final login_succesful:I = 0x7f09007b

.field public static final login_unsuccesful:I = 0x7f09007c

.field public static final login_url:I = 0x7f0901d9

.field public static final main_screen_fatal_error:I = 0x7f090018

.field public static final main_screen_gps_error:I = 0x7f090017

.field public static final main_screen_init_error:I = 0x7f090019

.field public static final main_screen_init_null:I = 0x7f09001a

.field public static final map_gps_error:I = 0x7f09001f

.field public static final map_path:I = 0x7f0901f9

.field public static final map_zoom_message:I = 0x7f09001e

.field public static final match_station_instructions:I = 0x7f090067

.field public static final match_station_title:I = 0x7f090066

.field public static final maxlat:I = 0x7f0901c1

.field public static final maxlong:I = 0x7f0901c2

.field public static final member_feature_error:I = 0x7f090004

.field public static final member_preferences_update_path:I = 0x7f090209

.field public static final midgrade_name:I = 0x7f0901ce

.field public static final midgrade_type:I = 0x7f0901ca

.field public static final minlat:I = 0x7f0901c3

.field public static final minlong:I = 0x7f0901c4

.field public static final open_cell_id_key:I = 0x7f090229

.field public static final open_cell_id_path:I = 0x7f090211

.field public static final photo_description_air:I = 0x7f0901ad

.field public static final photo_description_carwash:I = 0x7f0901b0

.field public static final photo_description_front:I = 0x7f0901b1

.field public static final photo_description_intersection:I = 0x7f0901b3

.field public static final photo_description_other:I = 0x7f0901b5

.field public static final photo_description_pump:I = 0x7f0901ae

.field public static final photo_description_restaurant:I = 0x7f0901b4

.field public static final photo_description_side:I = 0x7f0901b2

.field public static final photo_description_store:I = 0x7f0901af

.field public static final photo_description_title:I = 0x7f0901ac

.field public static final photo_large_caption:I = 0x7f0901a8

.field public static final photo_large_title:I = 0x7f0901a7

.field public static final photo_selector_camera:I = 0x7f0901aa

.field public static final photo_selector_gallery:I = 0x7f0901ab

.field public static final photo_selector_title:I = 0x7f0901a9

.field public static final pin_prices_url:I = 0x7f0901d4

.field public static final places_no_places_found:I = 0x7f0900b9

.field public static final places_no_places_found_instructions:I = 0x7f0900b8

.field public static final places_search_term:I = 0x7f0900b6

.field public static final places_search_term_gps:I = 0x7f0900b7

.field public static final premium_name:I = 0x7f0901cf

.field public static final premium_type:I = 0x7f0901cb

.field public static final privacy_policy_path:I = 0x7f09020c

.field public static final prize_address_not_valid:I = 0x7f0900b3

.field public static final prize_city_not_valid:I = 0x7f0900b4

.field public static final prize_first_name_not_valid:I = 0x7f0900b1

.field public static final prize_get_tickets_path:I = 0x7f090206

.field public static final prize_info_path:I = 0x7f0901fd

.field public static final prize_last_name_not_valid:I = 0x7f0900b2

.field public static final prize_more_tickets_error:I = 0x7f090080

.field public static final prize_no_tickets_error:I = 0x7f090082

.field public static final prize_state_not_valid:I = 0x7f0900b5

.field public static final prize_tickets_max:I = 0x7f090081

.field public static final profile_30_day_rank:I = 0x7f0900c3

.field public static final profile_consecutive_days:I = 0x7f0900c5

.field public static final profile_error:I = 0x7f0900c7

.field public static final profile_forum_posts:I = 0x7f0900c4

.field public static final profile_forum_title:I = 0x7f0900be

.field public static final profile_gasbuddy_age:I = 0x7f0900bd

.field public static final profile_home_site:I = 0x7f0900bb

.field public static final profile_joined_date:I = 0x7f0900bc

.field public static final profile_logout_message:I = 0x7f09007f

.field public static final profile_overall_rank:I = 0x7f0900c2

.field public static final profile_path:I = 0x7f0901e9

.field public static final profile_point_balance:I = 0x7f0900c0

.field public static final profile_points_today:I = 0x7f0900c1

.field public static final profile_screen_title:I = 0x7f0900ba

.field public static final profile_statistics:I = 0x7f0900bf

.field public static final profile_total_points:I = 0x7f0900c6

.field public static final protocol:I = 0x7f0901dc

.field public static final protocol_secure:I = 0x7f0901db

.field public static final register_email_existed:I = 0x7f0900a2

.field public static final register_email_not_valid:I = 0x7f0900a1

.field public static final register_password_length_not_valid:I = 0x7f0900a6

.field public static final register_password_not_matched:I = 0x7f0900a0

.field public static final register_path:I = 0x7f0901fb

.field public static final register_succesful:I = 0x7f0900a9

.field public static final register_unsuccesful:I = 0x7f0900aa

.field public static final register_username_existed:I = 0x7f0900a5

.field public static final register_username_length_not_valid:I = 0x7f0900a4

.field public static final register_username_not_valid:I = 0x7f0900a3

.field public static final register_zip_not_found:I = 0x7f0900a8

.field public static final register_zip_not_valid:I = 0x7f0900a7

.field public static final regular_name:I = 0x7f0901cd

.field public static final regular_type:I = 0x7f0901c9

.field public static final report_comments:I = 0x7f090154

.field public static final report_diesel:I = 0x7f090153

.field public static final report_midgrade:I = 0x7f090151

.field public static final report_path:I = 0x7f0901ff

.field public static final report_premium:I = 0x7f090152

.field public static final report_prices_empty_price:I = 0x7f090016

.field public static final report_prices_error:I = 0x7f090009

.field public static final report_prices_invalid_prices:I = 0x7f09000a

.field public static final report_prices_no_input_error:I = 0x7f09000b

.field public static final report_prices_no_login_message:I = 0x7f09000c

.field public static final report_prices_sent_member:I = 0x7f090006

.field public static final report_prices_sent_member_limit:I = 0x7f090007

.field public static final report_prices_sent_visitor:I = 0x7f090008

.field public static final report_prices_speechinput_dialog_neutral:I = 0x7f090015

.field public static final report_prices_speechinput_dialog_positive:I = 0x7f090014

.field public static final report_prices_speechinput_message_incomprehensible:I = 0x7f090010

.field public static final report_prices_speechinput_message_questionable:I = 0x7f090011

.field public static final report_prices_speechinput_prompt:I = 0x7f09000f

.field public static final report_prices_speechinput_title_incomprehensible:I = 0x7f090012

.field public static final report_prices_speechinput_title_questionable:I = 0x7f090013

.field public static final report_prices_validate_distance:I = 0x7f09000e

.field public static final report_prices_validate_price:I = 0x7f09000d

.field public static final report_regular:I = 0x7f090150

.field public static final report_test_path:I = 0x7f09020a

.field public static final report_time_spotted:I = 0x7f090155

.field public static final report_url:I = 0x7f0901da

.field public static final reset_password_path:I = 0x7f090201

.field public static final screen_url:I = 0x7f0901d5

.field public static final search_autotext_hint:I = 0x7f0901d1

.field public static final search_city_error:I = 0x7f09001c

.field public static final search_city_error_empty:I = 0x7f09001d

.field public static final search_hint:I = 0x7f0901be

.field public static final search_instructions:I = 0x7f0900ab

.field public static final search_location_error:I = 0x7f09001b

.field public static final server_error:I = 0x7f090000

.field public static final set_preferences:I = 0x7f0901bc

.field public static final settings_about_title:I = 0x7f0900ec

.field public static final settings_all_stations_summary:I = 0x7f0900da

.field public static final settings_all_stations_title:I = 0x7f0900d9

.field public static final settings_custom_keyboard_summary:I = 0x7f0900e8

.field public static final settings_custom_keyboard_title:I = 0x7f0900e7

.field public static final settings_distance_dialog_title:I = 0x7f0900d6

.field public static final settings_distance_km:I = 0x7f0900d8

.field public static final settings_distance_miles:I = 0x7f0900d7

.field public static final settings_distance_summary:I = 0x7f0900d5

.field public static final settings_distance_title:I = 0x7f0900d4

.field public static final settings_dont_show_again:I = 0x7f090106

.field public static final settings_favorites_summary:I = 0x7f090101

.field public static final settings_favorites_title:I = 0x7f090100

.field public static final settings_feedback_summary:I = 0x7f0900ee

.field public static final settings_feedback_title:I = 0x7f0900ed

.field public static final settings_fuel_type_dialog_title:I = 0x7f0900d3

.field public static final settings_fuel_type_summary:I = 0x7f0900d2

.field public static final settings_fuel_type_title:I = 0x7f0900d1

.field public static final settings_gasbuddy_title:I = 0x7f0900e0

.field public static final settings_gps_summary:I = 0x7f0900fd

.field public static final settings_gps_title:I = 0x7f0900fc

.field public static final settings_help_summary:I = 0x7f0900f0

.field public static final settings_help_title:I = 0x7f0900ef

.field public static final settings_initial_screen_dialog_title:I = 0x7f0900e3

.field public static final settings_initial_screen_favorites:I = 0x7f0900e6

.field public static final settings_initial_screen_home:I = 0x7f0900e4

.field public static final settings_initial_screen_nearme:I = 0x7f0900e5

.field public static final settings_initial_screen_summary:I = 0x7f0900e2

.field public static final settings_initial_screen_title:I = 0x7f0900e1

.field public static final settings_location_title:I = 0x7f090104

.field public static final settings_login_summary:I = 0x7f0900dd

.field public static final settings_login_title:I = 0x7f0900dc

.field public static final settings_logout_summary:I = 0x7f0900df

.field public static final settings_logout_title:I = 0x7f0900de

.field public static final settings_members_title:I = 0x7f0900db

.field public static final settings_other_location_summary:I = 0x7f090103

.field public static final settings_other_location_title:I = 0x7f090102

.field public static final settings_radius_summary:I = 0x7f0900f2

.field public static final settings_radius_title:I = 0x7f0900f1

.field public static final settings_rate_summary:I = 0x7f0900f4

.field public static final settings_rate_title:I = 0x7f0900f3

.field public static final settings_refresh_summary:I = 0x7f0900fb

.field public static final settings_refresh_title:I = 0x7f0900fa

.field public static final settings_saved_searches_summary:I = 0x7f0900ff

.field public static final settings_saved_searches_title:I = 0x7f0900fe

.field public static final settings_search_title:I = 0x7f0900d0

.field public static final settings_share_app_summary:I = 0x7f0900f7

.field public static final settings_share_app_title:I = 0x7f0900f6

.field public static final settings_share_title:I = 0x7f0900f5

.field public static final settings_smart_prompt_summary:I = 0x7f0900ea

.field public static final settings_smart_prompt_title:I = 0x7f0900e9

.field public static final settings_sort_summary:I = 0x7f0900f9

.field public static final settings_sort_title:I = 0x7f0900f8

.field public static final settings_support_title:I = 0x7f0900eb

.field public static final settings_widget_title:I = 0x7f090105

.field public static final share_app_default_message:I = 0x7f0900cd

.field public static final share_app_default_title:I = 0x7f0900cc

.field public static final share_app_path:I = 0x7f090202

.field public static final smart_prompt_response_path:I = 0x7f090208

.field public static final social_media_action_succesful:I = 0x7f0900ce

.field public static final social_media_login_required:I = 0x7f0900cf

.field public static final social_network_action_path:I = 0x7f090203

.field public static final social_network_error_path:I = 0x7f090210

.field public static final social_network_service_path:I = 0x7f09020e

.field public static final social_network_success_path:I = 0x7f09020f

.field public static final spnlat:I = 0x7f0901c5

.field public static final spnlong:I = 0x7f0901c6

.field public static final states_red_prices_url:I = 0x7f0901d3

.field public static final station_fav_added:I = 0x7f09003f

.field public static final station_fav_error:I = 0x7f090041

.field public static final station_fav_login_required:I = 0x7f09003e

.field public static final station_fav_other:I = 0x7f090040

.field public static final station_features:I = 0x7f090044

.field public static final station_features_air_hose:I = 0x7f090051

.field public static final station_features_atm:I = 0x7f090055

.field public static final station_features_car_wash:I = 0x7f09004c

.field public static final station_features_convenience_store:I = 0x7f09004b

.field public static final station_features_diesel:I = 0x7f090048

.field public static final station_features_ethanol:I = 0x7f090049

.field public static final station_features_midgrade:I = 0x7f090046

.field public static final station_features_open_24_7:I = 0x7f090053

.field public static final station_features_pay_at_pump:I = 0x7f090050

.field public static final station_features_pay_phone:I = 0x7f090052

.field public static final station_features_premium:I = 0x7f090047

.field public static final station_features_propane:I = 0x7f09004a

.field public static final station_features_regular:I = 0x7f090045

.field public static final station_features_restaurant:I = 0x7f09004e

.field public static final station_features_restrooms:I = 0x7f09004d

.field public static final station_features_service_station:I = 0x7f090054

.field public static final station_features_truck_stop:I = 0x7f09004f

.field public static final station_features_unavailable:I = 0x7f090037

.field public static final station_gps_error:I = 0x7f090042

.field public static final station_login_required:I = 0x7f09003d

.field public static final station_no_features:I = 0x7f0901bb

.field public static final station_no_prices:I = 0x7f09003c

.field public static final station_path:I = 0x7f090204

.field public static final station_photos:I = 0x7f090043

.field public static final station_picture_taken_error:I = 0x7f09003a

.field public static final station_picture_taken_file_not_found:I = 0x7f09003b

.field public static final station_picture_taken_message:I = 0x7f090039

.field public static final station_picture_taken_uploading_message:I = 0x7f090038

.field public static final station_url:I = 0x7f0901d6

.field public static final timeout_error:I = 0x7f090003

.field public static final upload_image_title:I = 0x7f090083

.field public static final upload_profile_image_path:I = 0x7f0901fe

.field public static final upload_station_image_path:I = 0x7f090205

.field public static final validate_member_path:I = 0x7f0901fc

.field public static final widget_location_displayed_nearme:I = 0x7f090107

.field public static final winners_path:I = 0x7f090207

.field public static final xad_production_key:I = 0x7f090228

.field public static final xad_production_url:I = 0x7f090227


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
