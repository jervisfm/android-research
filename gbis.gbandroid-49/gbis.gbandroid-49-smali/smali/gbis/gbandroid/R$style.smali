.class public final Lgbis/gbandroid/R$style;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final DialogTextButton:I = 0x7f0a0001

.field public static final DialogTextTitle:I = 0x7f0a0002

.field public static final GBTheme:I = 0x7f0a0000

.field public static final GBTheme_Dialog:I = 0x7f0a0012

.field public static final GBTheme_Dialog_FullScreen:I = 0x7f0a0013

.field public static final GBTheme_Dialog_Progress:I = 0x7f0a0014

.field public static final GBTheme_Dialog_Tooltip:I = 0x7f0a0015

.field public static final GBTheme_Preferences:I = 0x7f0a000a

.field public static final GBTheme_Translucent:I = 0x7f0a000b

.field public static final MapStationStyle:I = 0x7f0a0016

.field public static final Preference:I = 0x7f0a0006

.field public static final PreferenceCategoryLayout:I = 0x7f0a0010

.field public static final PreferenceCategoryText:I = 0x7f0a000f

.field public static final PreferenceScreenTitle:I = 0x7f0a0011

.field public static final Preference_CheckBoxPreference:I = 0x7f0a0007

.field public static final Preference_DialogPreference:I = 0x7f0a0009

.field public static final Preference_ListPreference:I = 0x7f0a0008

.field public static final PriceButtonText:I = 0x7f0a0003

.field public static final Widget:I = 0x7f0a000c

.field public static final Widget_AbsListView:I = 0x7f0a000d

.field public static final Widget_AutoCompleteTextView:I = 0x7f0a001e

.field public static final Widget_Button:I = 0x7f0a0022

.field public static final Widget_DropDownItem:I = 0x7f0a0020

.field public static final Widget_DropDownItem_Spinner:I = 0x7f0a0021

.field public static final Widget_EditText:I = 0x7f0a001c

.field public static final Widget_EditText_Price:I = 0x7f0a001d

.field public static final Widget_ListView:I = 0x7f0a000e

.field public static final Widget_ProgressBar:I = 0x7f0a0017

.field public static final Widget_ProgressBar_Large:I = 0x7f0a0018

.field public static final Widget_ProgressBar_Large_Black:I = 0x7f0a001a

.field public static final Widget_ProgressBar_Small:I = 0x7f0a0019

.field public static final Widget_ProgressBar_Small_Black:I = 0x7f0a001b

.field public static final Widget_Spinner:I = 0x7f0a001f

.field public static final contextMenuItem:I = 0x7f0a0005

.field public static final gbListCheckedItemStyle:I = 0x7f0a0004


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
