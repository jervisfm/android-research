.class public final Lgbis/gbandroid/R$styleable;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Amazon:[I = null

.field public static final Amazon_adSize:I = 0x0

.field public static final FilterButton:[I = null

.field public static final FilterButton_filterButtonIcon:I = 0x1

.field public static final FilterButton_filterButtonText:I = 0x0

.field public static final Keyboard:[I = null

.field public static final KeyboardView:[I = null

.field public static final KeyboardView_keyTextColor:I = 0x4

.field public static final KeyboardView_keyTextSize:I = 0x2

.field public static final KeyboardView_keyboardViewStyle:I = 0x0

.field public static final KeyboardView_keysBackground:I = 0x1

.field public static final KeyboardView_labelTextSize:I = 0x3

.field public static final KeyboardView_shadowColor:I = 0x6

.field public static final KeyboardView_shadowRadius:I = 0x7

.field public static final KeyboardView_verticalCorrection:I = 0x5

.field public static final Keyboard_Key:[I = null

.field public static final Keyboard_Key_codes:I = 0x1

.field public static final Keyboard_Key_iconPreview:I = 0x6

.field public static final Keyboard_Key_isModifier:I = 0x3

.field public static final Keyboard_Key_isRepeatable:I = 0x5

.field public static final Keyboard_Key_isSticky:I = 0x4

.field public static final Keyboard_Key_keyBackground:I = 0xa

.field public static final Keyboard_Key_keyEdgeFlags:I = 0x2

.field public static final Keyboard_Key_keyIcon:I = 0x9

.field public static final Keyboard_Key_keyLabel:I = 0x8

.field public static final Keyboard_Key_keyOutputText:I = 0x7

.field public static final Keyboard_Key_keyboardMode:I = 0x0

.field public static final Keyboard_Row:[I = null

.field public static final Keyboard_Row_keyboardMode:I = 0x1

.field public static final Keyboard_Row_rowEdgeFlags:I = 0x0

.field public static final Keyboard_horizontalGap:I = 0x2

.field public static final Keyboard_keyHeight:I = 0x1

.field public static final Keyboard_keyWidth:I = 0x0

.field public static final Keyboard_verticalGap:I = 0x3

.field public static final ShowStation:[I = null

.field public static final ShowStation_android_galleryItemBackground:I = 0x0

.field public static final com_admob_android_ads_AdView:[I = null

.field public static final com_admob_android_ads_AdView_backgroundColor:I = 0x0

.field public static final com_admob_android_ads_AdView_keywords:I = 0x3

.field public static final com_admob_android_ads_AdView_primaryTextColor:I = 0x1

.field public static final com_admob_android_ads_AdView_refreshInterval:I = 0x4

.field public static final com_admob_android_ads_AdView_secondaryTextColor:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1861
    new-array v0, v3, [I

    .line 1862
    const/high16 v1, 0x7f01

    aput v1, v0, v2

    .line 1861
    sput-object v0, Lgbis/gbandroid/R$styleable;->Amazon:[I

    .line 1890
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lgbis/gbandroid/R$styleable;->FilterButton:[I

    .line 1933
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lgbis/gbandroid/R$styleable;->Keyboard:[I

    .line 2044
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lgbis/gbandroid/R$styleable;->KeyboardView:[I

    .line 2204
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lgbis/gbandroid/R$styleable;->Keyboard_Key:[I

    .line 2388
    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lgbis/gbandroid/R$styleable;->Keyboard_Row:[I

    .line 2433
    new-array v0, v3, [I

    .line 2434
    const v1, 0x101004c

    aput v1, v0, v2

    .line 2433
    sput-object v0, Lgbis/gbandroid/R$styleable;->ShowStation:[I

    .line 2460
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lgbis/gbandroid/R$styleable;->com_admob_android_ads_AdView:[I

    .line 1850
    return-void

    .line 1890
    nop

    :array_0
    .array-data 0x4
        0x1t 0x0t 0x1t 0x7ft
        0x7t 0x0t 0x1t 0x7ft
    .end array-data

    .line 1933
    :array_1
    .array-data 0x4
        0x10t 0x0t 0x1t 0x7ft
        0x11t 0x0t 0x1t 0x7ft
        0x12t 0x0t 0x1t 0x7ft
        0x13t 0x0t 0x1t 0x7ft
    .end array-data

    .line 2044
    :array_2
    .array-data 0x4
        0x8t 0x0t 0x1t 0x7ft
        0x9t 0x0t 0x1t 0x7ft
        0xat 0x0t 0x1t 0x7ft
        0xbt 0x0t 0x1t 0x7ft
        0xct 0x0t 0x1t 0x7ft
        0xdt 0x0t 0x1t 0x7ft
        0xet 0x0t 0x1t 0x7ft
        0xft 0x0t 0x1t 0x7ft
    .end array-data

    .line 2204
    :array_3
    .array-data 0x4
        0x15t 0x0t 0x1t 0x7ft
        0x16t 0x0t 0x1t 0x7ft
        0x17t 0x0t 0x1t 0x7ft
        0x18t 0x0t 0x1t 0x7ft
        0x19t 0x0t 0x1t 0x7ft
        0x1at 0x0t 0x1t 0x7ft
        0x1bt 0x0t 0x1t 0x7ft
        0x1ct 0x0t 0x1t 0x7ft
        0x1dt 0x0t 0x1t 0x7ft
        0x1et 0x0t 0x1t 0x7ft
        0x1ft 0x0t 0x1t 0x7ft
    .end array-data

    .line 2388
    :array_4
    .array-data 0x4
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
    .end array-data

    .line 2460
    :array_5
    .array-data 0x4
        0x2t 0x0t 0x1t 0x7ft
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
        0x6t 0x0t 0x1t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
