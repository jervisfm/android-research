.class public Lgbis/gbandroid/activities/awards/AwardDetails;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/awards/AwardDetails$a;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/entities/AwardsMessage;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/awards/AwardDetails;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 61
    const v0, 0x7f070022

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method

.method private b()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v10, :cond_0

    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    .line 85
    :goto_0
    const v0, 0x7f070021

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 86
    const v0, 0x7f07001f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 87
    const v0, 0x7f07001e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 88
    const v0, 0x7f070023

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ProgressBar;

    .line 89
    const v0, 0x7f070024

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 90
    const v0, 0x7f070025

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    .line 91
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v4, 0x7f020064

    const-string v5, "/Android/data/gbis.gbandroid/cache/Awards"

    invoke-direct {v0, p0, v4, v5}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 92
    new-instance v4, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v5, Lgbis/gbandroid/activities/awards/a;

    invoke-direct {v5, p0, v2}, Lgbis/gbandroid/activities/awards/a;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/view/View;)V

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 101
    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 102
    iget-object v2, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/AwardsMessage;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getAwarded()I

    move-result v1

    if-ne v1, v10, :cond_2

    const-string v1, "on"

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "big.png"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    const v4, 0x7f09020d

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    invoke-static {v4}, Lgbis/gbandroid/utils/ImageUtils;->getResolutionInText(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v4}, Lgbis/gbandroid/entities/AwardsMessage;->getAwardId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 107
    :try_start_0
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 108
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_2
    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getAwarded()I

    move-result v0

    if-ne v0, v10, :cond_5

    .line 116
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Lgbis/gbandroid/activities/awards/b;

    invoke-direct {v1, p0, v6}, Lgbis/gbandroid/activities/awards/b;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/widget/TextView;)V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 126
    const v1, 0x7f0900cb

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 128
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->c()V

    .line 129
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 130
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getTotalTimes()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c8

    mul-float/2addr v1, v0

    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getActionCount()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 131
    const v0, 0x7f0900c8

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v9

    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {v7, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 133
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Lgbis/gbandroid/activities/awards/c;

    invoke-direct {v1, p0, v7}, Lgbis/gbandroid/activities/awards/c;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/widget/ProgressBar;)V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 142
    new-instance v1, Lgbis/gbandroid/activities/awards/d;

    invoke-direct {v1, p0, v0, v7}, Lgbis/gbandroid/activities/awards/d;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/graphics/drawable/Drawable;Landroid/widget/ProgressBar;)V

    .line 156
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v2, v9

    aput-object v1, v2, v10

    aput-object v1, v2, v11

    .line 157
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 158
    const/high16 v1, 0x102

    invoke-virtual {v0, v9, v1}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 159
    const v1, 0x102000f

    invoke-virtual {v0, v10, v1}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 160
    const v1, 0x102000d

    invoke-virtual {v0, v11, v1}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 161
    invoke-virtual {v7, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 171
    :goto_3
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    goto/16 :goto_0

    .line 103
    :cond_2
    const-string v1, "off"

    goto/16 :goto_1

    .line 110
    :cond_3
    const v0, 0x7f020065

    :try_start_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 111
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 163
    :cond_4
    invoke-virtual {v7, v13}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 164
    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 167
    :cond_5
    invoke-virtual {v7, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 168
    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method static synthetic b(Lgbis/gbandroid/activities/awards/AwardDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->b()V

    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const v13, 0x7f080025

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 174
    const-string v4, ""

    .line 176
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->d()Z

    move-result v0

    if-nez v0, :cond_1

    move v1, v2

    .line 180
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 181
    if-le v6, v1, :cond_0

    .line 182
    const v0, 0x7f070026

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 183
    const v0, 0x7f070027

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    move-object v5, v4

    move v4, v1

    .line 184
    :goto_1
    if-lt v4, v6, :cond_2

    .line 221
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 179
    goto :goto_0

    .line 185
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/AwardsMessage;

    .line 186
    new-instance v7, Landroid/widget/TableRow;

    invoke-direct {v7, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 187
    new-instance v8, Landroid/widget/TableLayout$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x2

    invoke-direct {v8, v9, v10}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    new-instance v8, Landroid/widget/TextView;

    invoke-direct {v8, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 189
    new-instance v9, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v9, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(I)V

    .line 190
    iget v10, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v10, v10

    mul-int/lit8 v10, v10, 0xa

    iget v11, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v11, v11

    mul-int/lit8 v11, v11, 0xa

    invoke-virtual {v9, v3, v10, v11, v3}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 191
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    iget v9, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v9, v9

    mul-int/lit8 v9, v9, 0xa

    iget v10, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v10, v10

    mul-int/lit8 v10, v10, 0x2

    iget v11, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v11, v11

    mul-int/lit8 v11, v11, 0xa

    iget v12, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    float-to-int v12, v12

    mul-int/lit8 v12, v12, 0xa

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 193
    iget-object v9, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 194
    const/high16 v9, 0x4180

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 195
    const/16 v9, 0x11

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 196
    new-instance v9, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v10, Lgbis/gbandroid/activities/awards/e;

    invoke-direct {v10, p0, v1, v8}, Lgbis/gbandroid/activities/awards/e;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Lgbis/gbandroid/entities/AwardsMessage;Landroid/widget/TextView;)V

    invoke-direct {v9, v10}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 206
    const v10, 0x7f0900cb

    invoke-virtual {p0, v10}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-array v11, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v3

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 209
    new-instance v9, Landroid/widget/TextView;

    invoke-direct {v9, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 210
    new-instance v10, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v10, v2}, Landroid/widget/TableRow$LayoutParams;-><init>(I)V

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    iget-object v10, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 212
    const/high16 v10, 0x4160

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextSize(F)V

    .line 213
    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 214
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getDescription()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getAddDate()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v11

    invoke-static {v11}, Lgbis/gbandroid/utils/DateUtils;->getDateForWinners(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-virtual {v0, v7}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 216
    invoke-virtual {v7, v8}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 217
    invoke-virtual {v7, v9}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0900c9

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getDescription()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getAddDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/utils/DateUtils;->getDateForWinners(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 184
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_1
.end method

.method static synthetic c(Lgbis/gbandroid/activities/awards/AwardDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->e()V

    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/awards/AwardDetails;)Lgbis/gbandroid/entities/AwardsMessage;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    return-object v0
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getActionCount()I

    move-result v2

    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getTotalTimes()I

    move-result v0

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/awards/AwardDetails;)F
    .locals 1
    .parameter

    .prologue
    .line 44
    iget v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    return v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 229
    new-instance v0, Lgbis/gbandroid/activities/awards/f;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/awards/f;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/awards/f;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 230
    new-instance v1, Lgbis/gbandroid/queries/AwardDetailsQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AwardDetailsQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 231
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getAwardId()I

    move-result v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/AwardDetailsQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 232
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->b:Ljava/util/List;

    .line 233
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lgbis/gbandroid/activities/awards/AwardDetails$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/awards/AwardDetails$a;-><init>(Lgbis/gbandroid/activities/awards/AwardDetails;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 237
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/awards/AwardDetails$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 238
    const v1, 0x7f090095

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 239
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 48
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->setContentView(I)V

    .line 50
    invoke-virtual {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->getDensity()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->c:F

    .line 51
    invoke-virtual {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    const-string v1, "award"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/AwardDetails;->a:Lgbis/gbandroid/entities/AwardsMessage;

    .line 56
    :goto_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->f()V

    .line 57
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->a()V

    .line 58
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/awards/AwardDetails;->finish()V

    goto :goto_0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    const v0, 0x7f09023d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
