.class final Lgbis/gbandroid/activities/awards/Awards$a;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/awards/Awards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/AwardsMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/Awards;

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lgbis/gbandroid/utils/ImageLoader;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/awards/Awards;Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030007

    .line 123
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    .line 124
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 125
    iput v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->b:I

    .line 126
    iput-object p3, p0, Lgbis/gbandroid/activities/awards/Awards$a;->c:Ljava/util/List;

    .line 127
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v1, 0x7f020064

    const-string v2, "/Android/data/gbis.gbandroid/cache/Awards"

    invoke-direct {v0, p1, v1, v2}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->d:Lgbis/gbandroid/utils/ImageLoader;

    .line 128
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/awards/Awards$a;)Lgbis/gbandroid/activities/awards/Awards;
    .locals 1
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    return-object v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 132
    if-nez p2, :cond_0

    .line 133
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/Awards;->a(Lgbis/gbandroid/activities/awards/Awards;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/awards/Awards$a;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 135
    new-instance v1, Lgbis/gbandroid/activities/awards/Awards$c;

    invoke-direct {v1}, Lgbis/gbandroid/activities/awards/Awards$c;-><init>()V

    .line 136
    const v0, 0x7f070020

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/awards/Awards$c;->a:Landroid/widget/TextView;

    .line 137
    const v0, 0x7f070021

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/awards/Awards$c;->b:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f07001f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/awards/Awards$c;->c:Landroid/widget/ImageView;

    .line 139
    const v0, 0x7f07001e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lgbis/gbandroid/activities/awards/Awards$c;->d:Landroid/view/View;

    .line 141
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v1

    .line 144
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    .line 145
    iget-object v1, v3, Lgbis/gbandroid/activities/awards/Awards$c;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Lgbis/gbandroid/activities/awards/h;

    invoke-direct {v2, p0, v0, v3}, Lgbis/gbandroid/activities/awards/h;-><init>(Lgbis/gbandroid/activities/awards/Awards$a;Lgbis/gbandroid/entities/AwardsMessage;Lgbis/gbandroid/activities/awards/Awards$c;)V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 156
    iget-object v2, v3, Lgbis/gbandroid/activities/awards/Awards$c;->d:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 157
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v1

    if-lez v1, :cond_1

    .line 158
    iget-object v1, v3, Lgbis/gbandroid/activities/awards/Awards$c;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    const v4, 0x7f0900cb

    invoke-virtual {v2, v4}, Lgbis/gbandroid/activities/awards/Awards;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Lgbis/gbandroid/activities/awards/i;

    invoke-direct {v2, p0, v0, v3}, Lgbis/gbandroid/activities/awards/i;-><init>(Lgbis/gbandroid/activities/awards/Awards$a;Lgbis/gbandroid/entities/AwardsMessage;Lgbis/gbandroid/activities/awards/Awards$c;)V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 169
    iget-object v2, v3, Lgbis/gbandroid/activities/awards/Awards$c;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 170
    iget-object v1, v3, Lgbis/gbandroid/activities/awards/Awards$c;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getLevel()I

    move-result v1

    if-lez v1, :cond_2

    const-string v1, "on"

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "small.png"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    const v5, 0x7f09020d

    invoke-virtual {v4, v5}, Lgbis/gbandroid/activities/awards/Awards;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v4}, Lgbis/gbandroid/activities/awards/Awards;->b(Lgbis/gbandroid/activities/awards/Awards;)F

    move-result v4

    invoke-static {v4}, Lgbis/gbandroid/utils/ImageUtils;->getResolutionInText(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getAwardId()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 175
    iget-object v0, v3, Lgbis/gbandroid/activities/awards/Awards$c;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 177
    :try_start_0
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$a;->d:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/Awards$a;->a:Lgbis/gbandroid/activities/awards/Awards;

    iget-object v3, v3, Lgbis/gbandroid/activities/awards/Awards$c;->c:Landroid/widget/ImageView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_3
    return-object p2

    .line 143
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/awards/Awards$c;

    move-object v3, v0

    goto/16 :goto_0

    .line 172
    :cond_1
    iget-object v1, v3, Lgbis/gbandroid/activities/awards/Awards$c;->b:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 173
    :cond_2
    const-string v1, "off"

    goto/16 :goto_2

    .line 180
    :cond_3
    :try_start_1
    iget-object v0, v3, Lgbis/gbandroid/activities/awards/Awards$c;->c:Landroid/widget/ImageView;

    const v1, 0x7f020064

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 181
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
