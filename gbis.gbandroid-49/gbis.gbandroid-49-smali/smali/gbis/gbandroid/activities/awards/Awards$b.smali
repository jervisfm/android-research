.class final Lgbis/gbandroid/activities/awards/Awards$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/awards/Awards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/Awards;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/awards/Awards;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 196
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    .line 197
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 198
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 222
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/awards/Awards;->finish()V

    .line 223
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 202
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 204
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/Awards;->c(Lgbis/gbandroid/activities/awards/Awards;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/Awards;->d(Lgbis/gbandroid/activities/awards/Awards;)V

    .line 208
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/Awards;->e(Lgbis/gbandroid/activities/awards/Awards;)V

    .line 211
    :goto_1
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/awards/Awards;->finish()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards$b;->a:Lgbis/gbandroid/activities/awards/Awards;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/Awards;->f(Lgbis/gbandroid/activities/awards/Awards;)V

    .line 216
    const/4 v0, 0x1

    return v0
.end method
