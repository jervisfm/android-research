.class public Lgbis/gbandroid/activities/awards/Awards;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/awards/Awards$a;,
        Lgbis/gbandroid/activities/awards/Awards$b;,
        Lgbis/gbandroid/activities/awards/Awards$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgbis/gbandroid/activities/base/GBActivity;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field public static final AWARDS_DIRECTORY:Ljava/lang/String; = "/Android/data/gbis.gbandroid/cache/Awards"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation
.end field

.field private b:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/awards/Awards;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 68
    const v0, 0x7f07002a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09018d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/awards/Awards;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const v0, 0x7f07002b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 70
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    new-instance v2, Lgbis/gbandroid/activities/awards/Awards$a;

    invoke-direct {v2, p0, p0, v1}, Lgbis/gbandroid/activities/awards/Awards$a;-><init>(Lgbis/gbandroid/activities/awards/Awards;Landroid/content/Context;Ljava/util/List;)V

    .line 72
    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    return-void
.end method

.method private a(J)V
    .locals 2
    .parameter

    .prologue
    .line 108
    invoke-virtual {p0}, Lgbis/gbandroid/activities/awards/Awards;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 109
    const-string v1, "awards_cached_images_time"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 110
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 111
    return-void
.end method

.method private a(Lgbis/gbandroid/entities/AwardsMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 114
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/awards/AwardDetails;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/Awards;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 115
    const-string v1, "award"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 116
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/awards/Awards;->startActivityForResult(Landroid/content/Intent;I)V

    .line 117
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/awards/Awards;)F
    .locals 1
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lgbis/gbandroid/activities/awards/Awards;->b:F

    return v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 77
    const v0, 0x7f07002b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 78
    new-instance v1, Lgbis/gbandroid/activities/awards/Awards$a;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/Awards;->a:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2}, Lgbis/gbandroid/activities/awards/Awards$a;-><init>(Lgbis/gbandroid/activities/awards/Awards;Landroid/content/Context;Ljava/util/List;)V

    .line 79
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 80
    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 81
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/awards/Awards;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 85
    new-instance v0, Lgbis/gbandroid/activities/awards/g;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/awards/g;-><init>(Lgbis/gbandroid/activities/awards/Awards;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/awards/g;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 86
    new-instance v1, Lgbis/gbandroid/queries/AwardsListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/Awards;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/Awards;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AwardsListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 87
    invoke-virtual {v1}, Lgbis/gbandroid/queries/AwardsListQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->a:Ljava/util/List;

    .line 89
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lgbis/gbandroid/activities/awards/Awards$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/awards/Awards$b;-><init>(Lgbis/gbandroid/activities/awards/Awards;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 93
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/awards/Awards$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 94
    const v1, 0x7f090095

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/awards/Awards;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/awards/Awards;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 95
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/awards/Awards;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/Awards;->b()V

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/awards/Awards;)V
    .locals 1
    .parameter

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->setAwardBadgeFlag(Z)V

    return-void
.end method

.method private e()Z
    .locals 7

    .prologue
    .line 98
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/Awards;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "awards_cached_images_time"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide v3, 0x9a7ec800L

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/awards/Awards;->a(J)V

    .line 101
    const/4 v0, 0x1

    .line 103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lgbis/gbandroid/activities/awards/Awards;)V
    .locals 0
    .parameter

    .prologue
    .line 84
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/Awards;->c()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 51
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lgbis/gbandroid/activities/awards/Awards;->getDensity()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/awards/Awards;->b:F

    .line 54
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/Awards;->a()V

    .line 55
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/Awards;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "awards_cached_images_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/awards/Awards;->a(J)V

    .line 57
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/Awards;->d()V

    .line 58
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 63
    invoke-direct {p0}, Lgbis/gbandroid/activities/awards/Awards;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "/Android/data/gbis.gbandroid/cache/Awards"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->cleanImageCache(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 230
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->a(Lgbis/gbandroid/entities/AwardsMessage;)V

    .line 231
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 236
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    const v0, 0x7f09023c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/awards/Awards;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
