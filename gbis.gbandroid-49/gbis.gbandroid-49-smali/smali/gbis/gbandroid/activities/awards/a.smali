.class final Lgbis/gbandroid/activities/awards/a;
.super Landroid/graphics/drawable/shapes/Shape;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/AwardDetails;

.field private final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/a;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    iput-object p2, p0, Lgbis/gbandroid/activities/awards/a;->b:Landroid/view/View;

    .line 92
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/Shape;-><init>()V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/a;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->d(Lgbis/gbandroid/activities/awards/AwardDetails;)Lgbis/gbandroid/entities/AwardsMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 97
    const/high16 v0, 0x3f80

    const/4 v1, 0x0

    const/high16 v2, 0x4040

    const/high16 v3, -0x100

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 98
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/a;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Lgbis/gbandroid/activities/awards/a;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const v2, 0x42ba999a

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/a;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    invoke-static {v3}, Lgbis/gbandroid/activities/awards/AwardDetails;->e(Lgbis/gbandroid/activities/awards/AwardDetails;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 99
    return-void
.end method
