.class final Lgbis/gbandroid/activities/awards/b;
.super Landroid/graphics/drawable/shapes/Shape;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/AwardDetails;

.field private final synthetic b:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/widget/TextView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/b;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    iput-object p2, p0, Lgbis/gbandroid/activities/awards/b;->b:Landroid/widget/TextView;

    .line 116
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/Shape;-><init>()V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4120

    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/awards/b;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    invoke-static {v0}, Lgbis/gbandroid/activities/awards/AwardDetails;->d(Lgbis/gbandroid/activities/awards/AwardDetails;)Lgbis/gbandroid/entities/AwardsMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 121
    const/high16 v0, 0x3f80

    const/high16 v1, 0x4040

    const/high16 v2, -0x100

    invoke-virtual {p2, v0, v3, v1, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 122
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lgbis/gbandroid/activities/awards/b;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/b;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 123
    invoke-virtual {p1, v0, v4, v4, p2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 124
    return-void
.end method
