.class final Lgbis/gbandroid/activities/awards/c;
.super Landroid/graphics/drawable/shapes/Shape;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/AwardDetails;

.field private final synthetic b:Landroid/widget/ProgressBar;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/widget/ProgressBar;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/c;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    iput-object p2, p0, Lgbis/gbandroid/activities/awards/c;->b:Landroid/widget/ProgressBar;

    .line 133
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/Shape;-><init>()V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4120

    const/4 v3, 0x0

    .line 136
    const-string v0, "#6d6e71"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 138
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lgbis/gbandroid/activities/awards/c;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/c;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 139
    invoke-virtual {p1, v0, v4, v4, p2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 140
    return-void
.end method
