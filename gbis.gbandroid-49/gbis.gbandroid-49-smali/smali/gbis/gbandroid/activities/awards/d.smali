.class final Lgbis/gbandroid/activities/awards/d;
.super Landroid/graphics/drawable/ClipDrawable;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/awards/AwardDetails;

.field private final synthetic b:Landroid/widget/ProgressBar;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/awards/AwardDetails;Landroid/graphics/drawable/Drawable;Landroid/widget/ProgressBar;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/awards/d;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    iput-object p3, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    .line 142
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter

    .prologue
    const/high16 v7, 0x4120

    const/high16 v6, 0x4118

    const/high16 v5, 0x3f00

    const/4 v4, 0x0

    .line 145
    invoke-super {p0, p1}, Landroid/graphics/drawable/ClipDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 146
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 147
    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 149
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    int-to-float v2, v2

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 150
    invoke-virtual {p1, v1, v7, v7, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 151
    iget-object v1, p0, Lgbis/gbandroid/activities/awards/d;->a:Lgbis/gbandroid/activities/awards/AwardDetails;

    invoke-static {v1}, Lgbis/gbandroid/activities/awards/AwardDetails;->d(Lgbis/gbandroid/activities/awards/AwardDetails;)Lgbis/gbandroid/entities/AwardsMessage;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AwardsMessage;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 152
    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    iget-object v3, p0, Lgbis/gbandroid/activities/awards/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 153
    invoke-virtual {p1, v1, v6, v6, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 154
    return-void
.end method
