.class public abstract Lgbis/gbandroid/activities/base/GBActivity;
.super Landroid/app/Activity;
.source "GBFile"

# interfaces
.implements Lcom/amazon/device/ads/AdListener;
.implements Lcom/google/ads/AdListener;
.implements Lgbis/gbandroid/listeners/MyLocationChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBActivity$a;,
        Lgbis/gbandroid/activities/base/GBActivity$b;
    }
.end annotation


# static fields
.field protected static final ACTIVITY_CREATE:I = 0x1

.field protected static final ADS_SUPPLIER_ADMOB:I = 0x2

.field protected static final ADS_SUPPLIER_AMAZON:I = 0x10

.field protected static final ADS_SUPPLIER_CANWEST:I = 0x11

.field protected static final ADS_SUPPLIER_DFP:I = 0x1

.field protected static final ADS_SUPPLIER_DFP_AND_SEARCH_ADS:I = 0x7

.field protected static final ADS_SUPPLIER_OFF:I = 0x0

.field protected static final ADS_SUPPLIER_PONTIFLEX:I = 0xa

.field protected static final ADS_SUPPLIER_SEARCH_ADS:I = 0x6

.field protected static final ADS_SUPPLIER_XAD:I = 0x5

.field protected static final AD_MEDIUM_BANNER:I = 0x1

.field protected static final AD_STANDARD_BANNER:I = 0x0

.field protected static final FILTER_STATIONS:I = 0x5

.field protected static final MEMBER_LOGGED_IN:I = 0x6

.field protected static final REPORT_PRICE:I = 0x3

.field public static final RESULT:Ljava/lang/String; = "result"

.field protected static final RESULT_REPORT:I = 0x3

.field protected static final SEARCH_PERFORMED:I = 0x4

.field public static final WIDGET_CALL:Ljava/lang/String; = "WidgetCall"


# instance fields
.field private a:Lgbis/gbandroid/utils/DBHelper;

.field protected adBanner:Landroid/view/View;

.field protected adsVisible:Z

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/EditText;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lgbis/gbandroid/services/GPSService;

.field private d:Landroid/content/ServiceConnection;

.field protected gpsCorrupted:Z

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mRes:Landroid/content/res/Resources;

.field protected mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation
.end field

.field protected mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

.field protected myLocation:Landroid/location/Location;

.field protected progress:Landroid/app/Dialog;

.field protected unstackedToast:Lgbis/gbandroid/views/CustomToast;

.field protected userMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 1035
    new-instance v0, Lgbis/gbandroid/activities/base/a;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/a;-><init>(Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->d:Landroid/content/ServiceConnection;

    .line 105
    return-void
.end method

.method private static a(I)Lcom/google/ads/AdSize;
    .locals 1
    .parameter

    .prologue
    .line 726
    sget-object v0, Lcom/google/ads/AdSize;->BANNER:Lcom/google/ads/AdSize;

    .line 727
    packed-switch p0, :pswitch_data_0

    .line 735
    :goto_0
    return-object v0

    .line 729
    :pswitch_0
    sget-object v0, Lcom/google/ads/AdSize;->BANNER:Lcom/google/ads/AdSize;

    goto :goto_0

    .line 732
    :pswitch_1
    sget-object v0, Lcom/google/ads/AdSize;->IAB_MRECT:Lcom/google/ads/AdSize;

    goto :goto_0

    .line 727
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivity;)Lgbis/gbandroid/services/GPSService;
    .locals 1
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 459
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivity$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/base/GBActivity$a;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 460
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 461
    return-void
.end method

.method private a(ILjava/lang/String;Z)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 452
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivity$b;

    move-object v1, p0

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/base/GBActivity$b;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/activities/base/GBActivity;ILjava/lang/String;Z)V

    .line 453
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 454
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "smartPromptPreference"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->a()V

    .line 456
    :cond_0
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivity;ILjava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1061
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivity;->b(ILjava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/services/GPSService;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/AwardsMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 290
    new-instance v0, Lgbis/gbandroid/views/CustomToastAward;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToastAward;-><init>(Landroid/app/Activity;Lgbis/gbandroid/entities/AwardsMessage;I)V

    .line 291
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToastAward;->show()V

    .line 292
    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListResults;)V
    .locals 6
    .parameter

    .prologue
    .line 539
    :try_start_0
    new-instance v0, Landroid/location/Geocoder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 540
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListResults;->getCenter()Lgbis/gbandroid/entities/Center;

    move-result-object v3

    .line 541
    invoke-virtual {v3}, Lgbis/gbandroid/entities/Center;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v3}, Lgbis/gbandroid/entities/Center;->getLongitude()D

    move-result-wide v3

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 542
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 543
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 544
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 545
    const-string v1, "US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 546
    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 549
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;Lcom/google/ads/AdSize;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 869
    new-instance v0, Lcom/google/ads/AdView;

    invoke-direct {v0, p0, p6, p1}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 870
    const/16 v0, 0xe

    const/4 v1, -0x1

    invoke-virtual {p3, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 871
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 872
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p4, v0, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 873
    new-instance v1, Lcom/google/ads/AdRequest;

    invoke-direct {v1}, Lcom/google/ads/AdRequest;-><init>()V

    .line 874
    if-eqz p5, :cond_0

    .line 875
    invoke-virtual {v1, p5}, Lcom/google/ads/AdRequest;->setLocation(Landroid/location/Location;)Lcom/google/ads/AdRequest;

    .line 876
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, v1}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 877
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, p0}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 878
    invoke-virtual {v1, p2}, Lcom/google/ads/AdRequest;->setNetworkExtras(Lcom/google/ads/mediation/NetworkExtras;)Lcom/google/ads/AdRequest;

    .line 879
    return-void
.end method

.method private a(Z)V
    .locals 5
    .parameter

    .prologue
    .line 474
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 475
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 476
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 482
    :cond_0
    return-void

    .line 477
    :cond_1
    if-eqz p1, :cond_2

    .line 478
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f080040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHintTextColor(I)V

    .line 476
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 480
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    const/high16 v4, 0x7f08

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHintTextColor(I)V

    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 882
    new-instance v0, Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-direct {v0}, Lcom/amazon/device/ads/AdTargetingOptions;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdTargetingOptions;->enableGeoLocation(Z)Lcom/amazon/device/ads/AdTargetingOptions;

    move-result-object v1

    .line 883
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/amazon/device/ads/AdLayout;

    invoke-virtual {v0, p0}, Lcom/amazon/device/ads/AdLayout;->setListener(Lcom/amazon/device/ads/AdListener;)V

    .line 884
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/amazon/device/ads/AdLayout;

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdLayout;->loadAd(Lcom/amazon/device/ads/AdTargetingOptions;)Z

    .line 885
    return-void
.end method

.method private b(ILjava/lang/String;Z)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1062
    new-instance v0, Lgbis/gbandroid/activities/base/j;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/j;-><init>(Lgbis/gbandroid/activities/base/GBActivity;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/j;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1063
    new-instance v1, Lgbis/gbandroid/queries/SmartPromptResponseQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1064
    invoke-virtual {v1, p1, p2, p3}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->getResponseObject(ILjava/lang/String;Z)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1065
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1093
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->d()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 987
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->trackAnalytics()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 988
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackPageView(Ljava/lang/String;)V

    .line 989
    const-string v0, "Analytics"

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsCustomVariables()V

    .line 992
    :cond_0
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1094
    new-instance v0, Lgbis/gbandroid/activities/base/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/b;-><init>(Lgbis/gbandroid/activities/base/GBActivity;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1095
    new-instance v1, Lgbis/gbandroid/queries/MemberPreferenceQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberPreferenceQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1096
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->getResponseObject(Ljava/util/List;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1097
    return-void
.end method

.method private e()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1101
    new-instance v1, Lgbis/gbandroid/entities/requests/RequestMemberPreference;

    invoke-direct {v1}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;-><init>()V

    .line 1102
    const-string v2, "SmartPrompt"

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->setName(Ljava/lang/String;)V

    .line 1103
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "smartPromptPreference"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->setValue(Z)V

    .line 1104
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1105
    return-object v0
.end method

.method public static getVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 1026
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1027
    const-string v1, "gbis.gbandroid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1028
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1031
    :goto_0
    return-object v0

    .line 1029
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 1031
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method protected OnSmartPromptPressedNo(Landroid/content/DialogInterface;IZ)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 438
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 439
    const-string v0, "N"

    invoke-direct {p0, p2, v0, p3}, Lgbis/gbandroid/activities/base/GBActivity;->a(ILjava/lang/String;Z)V

    .line 440
    return-void
.end method

.method protected OnSmartPromptPressedYes(Landroid/content/DialogInterface;IZ)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 433
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 434
    const-string v0, "Y"

    invoke-direct {p0, p2, v0, p3}, Lgbis/gbandroid/activities/base/GBActivity;->a(ILjava/lang/String;Z)V

    .line 435
    return-void
.end method

.method protected addBrand(II)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 679
    const-string v0, "/Android/data/gbis.gbandroid/cache/Brands"

    .line 680
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v1, p1, p2, v0}, Lgbis/gbandroid/utils/DBHelper;->addBrandRow(IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected addPlace(Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 643
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/DBHelper;->addSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v0

    return v0
.end method

.method protected addToEditTextList(Landroid/widget/EditText;)V
    .locals 1
    .parameter

    .prologue
    .line 485
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 486
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    .line 487
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    return-void
.end method

.method protected adjustAdsInConfiguration(Landroid/content/res/Configuration;)V
    .locals 2
    .parameter

    .prologue
    .line 718
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 719
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->hideAds()V

    .line 723
    :cond_1
    :goto_0
    return-void

    .line 721
    :cond_2
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 722
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->showAds()V

    goto :goto_0
.end method

.method public checkAwardAndShow()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getAwards()Ljava/util/List;

    move-result-object v2

    .line 297
    if-eqz v2, :cond_0

    .line 298
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 299
    if-lez v3, :cond_0

    .line 300
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 311
    :cond_0
    return-void

    .line 301
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    .line 303
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getAwarded()I

    move-result v4

    if-ne v4, v5, :cond_2

    .line 304
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->a(Lgbis/gbandroid/entities/AwardsMessage;)V

    .line 305
    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/base/GBActivity;->setAwardBadgeFlag(Z)V

    .line 300
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected checkConnection()Z
    .locals 1

    .prologue
    .line 229
    invoke-static {p0}, Lgbis/gbandroid/utils/ConnectionUtils;->isConnectedToAny(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public checkSmartPromptAndShow()V
    .locals 5

    .prologue
    .line 364
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getSmartPrompts()Ljava/util/List;

    move-result-object v2

    .line 366
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->isSmartPromptOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 368
    if-lez v3, :cond_0

    .line 369
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 377
    :cond_0
    return-void

    .line 370
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/SmartPrompt;

    .line 371
    invoke-virtual {v0}, Lgbis/gbandroid/entities/SmartPrompt;->getPromptId()I

    move-result v4

    if-ltz v4, :cond_2

    .line 372
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->showSmartPrompt(Lgbis/gbandroid/entities/SmartPrompt;)V

    .line 369
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected cleanImageCache()V
    .locals 1

    .prologue
    .line 491
    const-string v0, "/Android/data/gbis.gbandroid/cache/LazyList"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->cleanImageCache(Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method protected cleanImageCache(Ljava/lang/String;)V
    .locals 6
    .parameter

    .prologue
    .line 496
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 497
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 500
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 501
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 502
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 503
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v3, :cond_3

    .line 506
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 508
    :cond_1
    return-void

    .line 499
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 503
    :cond_3
    aget-object v4, v2, v1

    .line 504
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 503
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected closeDB()V
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    if-eqz v0, :cond_0

    .line 699
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->closeDB()V

    .line 700
    :cond_0
    return-void
.end method

.method protected deleteAllPlaces()Z
    .locals 1

    .prologue
    .line 651
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->deleteSearchesAllRows()Z

    move-result v0

    return v0
.end method

.method protected deletePlace(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 647
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->deleteSearchesRow(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected fillGSAAdRequestUIParameteres(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 888
    if-eqz p2, :cond_0

    .line 889
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getAnchorTextColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setAnchorTextColor(I)V

    .line 890
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setBackgroundColor(I)V

    .line 891
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getBackgroundGradient()Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->getFromColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 892
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getBackgroundGradient()Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->getToColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    .line 891
    invoke-virtual {p1, v0, v1}, Lcom/google/ads/searchads/SearchAdRequest;->setBackgroundGradient(II)V

    .line 893
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getBorderColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setBorderColor(I)V

    .line 894
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getBorderThickness()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setBorderThickness(I)V

    .line 895
    sget-object v0, Lcom/google/ads/searchads/SearchAdRequest$BorderType;->SOLID:Lcom/google/ads/searchads/SearchAdRequest$BorderType;

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setBorderType(Lcom/google/ads/searchads/SearchAdRequest$BorderType;)V

    .line 896
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getDescriptionTextColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setDescriptionTextColor(I)V

    .line 897
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getFontFace()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setFontFace(Ljava/lang/String;)V

    .line 898
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getHeaderTextColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setHeaderTextColor(I)V

    .line 899
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getHeaderTextSize()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setHeaderTextSize(I)V

    .line 901
    :cond_0
    return-void
.end method

.method protected getActivity()Lgbis/gbandroid/activities/base/GBActivity;
    .locals 0

    .prologue
    .line 233
    return-object p0
.end method

.method protected getAllPlaces(I)Landroid/database/Cursor;
    .locals 1
    .parameter

    .prologue
    .line 675
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getAllSearchesRows(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getBrand(I)Landroid/database/Cursor;
    .locals 1
    .parameter

    .prologue
    .line 684
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getBrandRow(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getDensity()F
    .locals 2

    .prologue
    .line 237
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 238
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 239
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method protected getGPSServiceConnector()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->d:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method protected getGSAQuery(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 904
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 905
    if-eqz p3, :cond_0

    .line 906
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 907
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getKeywords()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 908
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 909
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AdsGSA;->getKeywords()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 924
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 911
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    const/high16 v2, 0x7f06

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 912
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f060001

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 913
    const/4 v0, 0x0

    .line 914
    :goto_1
    array-length v4, v3

    if-lt v0, v4, :cond_3

    .line 920
    :cond_2
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 922
    const-string v0, " gas prices"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 915
    :cond_3
    aget-object v4, v3, v0

    iget-object v5, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "fuelPreference"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 916
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getHost()Ljava/lang/String;
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "host"

    const v2, 0x7f0901dd

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLastKnownLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0}, Lgbis/gbandroid/services/GPSService;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 712
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/GBApplication;

    invoke-virtual {v0}, Lgbis/gbandroid/GBApplication;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method protected getLastLocationStored()Landroid/location/Location;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 862
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 863
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "lastLatitude"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 864
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "lastLongitude"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 865
    return-object v0
.end method

.method protected getPlace(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .parameter

    .prologue
    .line 667
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getSearchesRow(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected getPlaceFromCityZip(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .parameter

    .prologue
    .line 671
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getSearchesRowFromCityZip(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method protected hideAds()V
    .locals 2

    .prologue
    .line 933
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 935
    :cond_0
    return-void
.end method

.method protected incrementFrequencyOfPlace(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 663
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->incrementSearchesFrequency(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isDBOpen()Z
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->isDBOpen()Z

    move-result v0

    .line 705
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isSmartPromptOn()Z
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "smartPromptPreference"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected launchSettings()V
    .locals 3

    .prologue
    .line 563
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/Settings;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 564
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 565
    return-void
.end method

.method protected loadDialog(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 320
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 322
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 323
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    .line 324
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 325
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 327
    :cond_0
    return-void
.end method

.method protected loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 347
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 348
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 349
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 350
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    .line 351
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 352
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/base/d;

    invoke-direct {v1, p0, p2}, Lgbis/gbandroid/activities/base/d;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 359
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 361
    :cond_0
    return-void
.end method

.method protected loadDialog(Ljava/lang/String;Ljava/lang/Thread;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 332
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 333
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    .line 334
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 335
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/base/c;

    invoke-direct {v1, p0, p2}, Lgbis/gbandroid/activities/base/c;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/Thread;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 342
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 344
    :cond_0
    return-void
.end method

.method public onAdCollapsed(Lcom/amazon/device/ads/AdLayout;)V
    .locals 0
    .parameter

    .prologue
    .line 978
    return-void
.end method

.method public onAdExpanded(Lcom/amazon/device/ads/AdLayout;)V
    .locals 0
    .parameter

    .prologue
    .line 974
    return-void
.end method

.method public onAdFailedToLoad(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdError;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 982
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->b()V

    .line 983
    return-void
.end method

.method public onAdLoaded(Lcom/amazon/device/ads/AdLayout;Lcom/amazon/device/ads/AdProperties;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 970
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 145
    if-eqz v0, :cond_0

    .line 146
    const-string v1, "my location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 147
    if-eqz v0, :cond_0

    .line 148
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    .line 150
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 151
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setMyLocationToZeros()V

    .line 152
    :cond_1
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 153
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->progress:Landroid/app/Dialog;

    .line 154
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 155
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adsVisible:Z

    .line 157
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->trackAnalytics()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    invoke-static {}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->getInstance()Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    .line 159
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const v1, 0x7f090226

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->startNewSession(Ljava/lang/String;Landroid/content/Context;)V

    .line 162
    :cond_2
    const v0, 0x7f090225

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/amazon/device/ads/AdRegistration;->setAppUniqueId(Landroid/content/Context;Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter

    .prologue
    .line 210
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PhoneButton"

    const v2, 0x7f090194

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 196
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 197
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->dispatch()Z

    .line 199
    :cond_0
    return-void
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 940
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 944
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->hideAds()V

    .line 946
    return-void
.end method

.method public onGPSServiceConnected()V
    .locals 1

    .prologue
    .line 1048
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    .line 1049
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 1050
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setMyLocationToZeros()V

    .line 1051
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/services/GPSService;->registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V

    .line 1052
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAdsAfterGPSServiceConnected()V

    .line 1053
    return-void
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 951
    return-void
.end method

.method public onMyLocationChanged(Landroid/location/Location;)V
    .locals 0
    .parameter

    .prologue
    .line 1057
    if-eqz p1, :cond_0

    .line 1058
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    .line 1059
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter

    .prologue
    .line 216
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MenuButton"

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 217
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 184
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->stopGPSService()V

    .line 185
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 956
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 961
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->showAds()V

    .line 963
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 176
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->startGPSService()V

    .line 177
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->c()V

    .line 179
    return-void
.end method

.method public onSearchRequested()Z
    .locals 4

    .prologue
    .line 203
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PhoneButton"

    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 204
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->showCityZipSearch()V

    .line 205
    const/4 v0, 0x1

    return v0
.end method

.method protected onSmartPromptCancelled(Landroid/content/DialogInterface;IZ)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 448
    const-string v0, ""

    invoke-direct {p0, p2, v0, p3}, Lgbis/gbandroid/activities/base/GBActivity;->a(ILjava/lang/String;Z)V

    .line 449
    return-void
.end method

.method protected onSmartPromptPressedDontKnow(Landroid/content/DialogInterface;IZ)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 443
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 444
    const-string v0, "?"

    invoke-direct {p0, p2, v0, p3}, Lgbis/gbandroid/activities/base/GBActivity;->a(ILjava/lang/String;Z)V

    .line 445
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 190
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->closeDB()V

    .line 191
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->stopGPSService()V

    .line 192
    return-void
.end method

.method protected openDB()Z
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    if-nez v0, :cond_0

    .line 693
    new-instance v0, Lgbis/gbandroid/utils/DBHelper;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/DBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    .line 694
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->openDB()Z

    move-result v0

    return v0
.end method

.method protected registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V
    .locals 1
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->c:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/services/GPSService;->registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V

    .line 226
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 747
    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 748
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 760
    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    .line 761
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 765
    .line 766
    if-nez p4, :cond_3

    .line 767
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adsVisible:Z

    move-object v1, p1

    .line 774
    :goto_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v5

    .line 775
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-nez v0, :cond_1

    .line 776
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getLastLocationStored()Landroid/location/Location;

    move-result-object v5

    .line 777
    :cond_1
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v2, -0x2

    invoke-direct {v3, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 778
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adsVisible:Z

    if-eqz v0, :cond_2

    .line 779
    packed-switch p4, :pswitch_data_0

    .line 850
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 851
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 852
    const/16 v1, 0xe

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 853
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 854
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p5, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 859
    :cond_2
    :goto_1
    return-void

    .line 768
    :cond_3
    const/4 v0, 0x1

    if-ne p4, v0, :cond_6

    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f090217

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 782
    :pswitch_1
    new-instance v2, Lcom/google/ads/doubleclick/DfpExtras;

    invoke-direct {v2}, Lcom/google/ads/doubleclick/DfpExtras;-><init>()V

    invoke-static {p2}, Lgbis/gbandroid/activities/base/GBActivity;->a(I)Lcom/google/ads/AdSize;

    move-result-object v6

    move-object v0, p0

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lgbis/gbandroid/activities/base/GBActivity;->a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;Lcom/google/ads/AdSize;)V

    goto :goto_1

    .line 785
    :pswitch_2
    new-instance v2, Lcom/google/ads/doubleclick/DfpExtras;

    invoke-direct {v2}, Lcom/google/ads/doubleclick/DfpExtras;-><init>()V

    .line 786
    const-string v0, "nk"

    const-string v4, "prtnr"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    .line 787
    const-string v0, "pr"

    const-string v4, "gb"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    .line 788
    const-string v0, "cnt"

    const-string v4, "auto"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    .line 789
    invoke-static {p2}, Lgbis/gbandroid/activities/base/GBActivity;->a(I)Lcom/google/ads/AdSize;

    move-result-object v6

    move-object v0, p0

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lgbis/gbandroid/activities/base/GBActivity;->a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;Lcom/google/ads/AdSize;)V

    goto :goto_1

    .line 792
    :pswitch_3
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 793
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 794
    const v0, 0x7f090227

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    const-string v0, "?v=1.1"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 796
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&k="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f090228

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 797
    const-string v0, "&uid=1234567890"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 798
    const-string v0, "&size=320x50"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 799
    if-eqz v5, :cond_4

    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&lat="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "&log="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 803
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 804
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 805
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v1, 0x43a0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getDensity()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 806
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p5, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 809
    :pswitch_4
    new-instance v2, Lcom/google/ads/searchads/SearchAdRequest;

    invoke-direct {v2}, Lcom/google/ads/searchads/SearchAdRequest;-><init>()V

    .line 810
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/GBApplication;->getAdsGSA()Lgbis/gbandroid/entities/AdsGSA;

    move-result-object v0

    .line 812
    const-string v4, "denver co shopping male"

    invoke-virtual {v2, v4}, Lcom/google/ads/searchads/SearchAdRequest;->setQuery(Ljava/lang/String;)V

    .line 813
    invoke-virtual {p0, v2, v0}, Lgbis/gbandroid/activities/base/GBActivity;->fillGSAAdRequestUIParameteres(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;)V

    .line 814
    invoke-virtual {p0, v2, v0, p6}, Lgbis/gbandroid/activities/base/GBActivity;->getGSAQuery(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setQuery(Ljava/lang/String;)V

    .line 815
    new-instance v0, Lcom/google/ads/AdView;

    new-instance v4, Lcom/google/ads/AdSize;

    const/16 v6, 0x140

    const/16 v7, 0x3c

    invoke-direct {v4, v6, v7}, Lcom/google/ads/AdSize;-><init>(II)V

    invoke-direct {v0, p0, v4, v1}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 816
    const/16 v0, 0xe

    const/4 v1, -0x1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 817
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 818
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p5, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 819
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x4270

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getDensity()F

    move-result v3

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 820
    if-eqz v5, :cond_5

    .line 821
    invoke-virtual {v2, v5}, Lcom/google/ads/searchads/SearchAdRequest;->setLocation(Landroid/location/Location;)Lcom/google/ads/AdRequest;

    .line 822
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, v2}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 823
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, p0}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    goto/16 :goto_1

    .line 826
    :pswitch_5
    const/4 v4, 0x6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 829
    :pswitch_6
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 830
    const-string v1, "javascript:<script type=\"text/javascript\">\npontiflex_data = {};\npontiflex_ad = {pid: 3425, options: {}};\npontiflex_ad.options.view = \"320x50\";\npontiflex_ad.options.appName = \"Sample App\";\n</script>\n<script type=\"text/javascript\" src=\"http://hhymxdkkdt5c.pflexads.com/html-sdk/mobilewebAds.js\"></script>"

    .line 837
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 838
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    check-cast v0, Landroid/webkit/WebView;

    const-string v2, "text/html"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 840
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v1, 0x43a0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getDensity()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/high16 v2, 0x4248

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getDensity()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 841
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p5, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 844
    :pswitch_7
    new-instance v0, Lcom/amazon/device/ads/AdLayout;

    sget-object v1, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    invoke-direct {v0, p0, v1}, Lcom/amazon/device/ads/AdLayout;-><init>(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    .line 845
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v1, 0x43a0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getDensity()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 846
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    invoke-virtual {p5, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 847
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;->b()V

    goto/16 :goto_1

    :cond_6
    move-object v1, p1

    goto/16 :goto_0

    .line 779
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method

.method protected setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 751
    const v0, 0x7f0700a6

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    .line 752
    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    .line 753
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 739
    const-string v0, ""

    invoke-virtual {p0, p1, p2, p3, v0}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 740
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;Ljava/lang/String;ILandroid/view/ViewGroup;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 756
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILandroid/view/ViewGroup;)V

    .line 757
    return-void
.end method

.method protected setAdConfiguration(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 743
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/base/GBActivity;->setAdConfiguration(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 744
    return-void
.end method

.method protected abstract setAdsAfterGPSServiceConnected()V
.end method

.method protected setAnalyticsCustomVariables()V
    .locals 6

    .prologue
    .line 999
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x1

    const-string v2, "AuthId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "auth_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 1000
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x2

    const-string v2, "AppVersion"

    invoke-static {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 1001
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x3

    const-string v2, "MemberId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "member_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 1002
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x4

    const-string v2, "FuelType"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "fuelPreference"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 1003
    return-void
.end method

.method protected abstract setAnalyticsPageName()Ljava/lang/String;
.end method

.method protected setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1006
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->trackAnalytics()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1008
    :cond_0
    return-void
.end method

.method protected setAnalyticsTrackEventAutocomplete(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1015
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AutocompleteButton"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1016
    return-void
.end method

.method protected setAnalyticsTrackEventContextMenu(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1019
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ContextMenu"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1020
    return-void
.end method

.method protected setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1011
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ScreenButton"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lgbis/gbandroid/activities/base/GBActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1012
    return-void
.end method

.method protected setAwardBadgeFlag(Z)V
    .locals 2
    .parameter

    .prologue
    .line 314
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 315
    const-string v1, "awards_badge"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 316
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 317
    return-void
.end method

.method protected setEditBoxHintConfiguration()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 464
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_1

    .line 465
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->keyboard:I

    if-eq v0, v1, :cond_0

    .line 466
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v0, v2, :cond_1

    .line 467
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->keyboardHidden:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 468
    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->a(Z)V

    .line 471
    :goto_0
    return-void

    .line 470
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->a(Z)V

    goto :goto_0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 286
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    .line 287
    return-void
.end method

.method protected setMyLocationToZeros()V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    .line 222
    return-void
.end method

.method protected showAddStation(Landroid/location/Location;Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 598
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-direct {v0, p0, v1, p1}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 599
    const-string v1, "search terms"

    invoke-virtual {v0, v1, p2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    const-string v1, "search type"

    invoke-virtual {v0, v1, p3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 601
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 602
    return-void
.end method

.method protected showAds()V
    .locals 2

    .prologue
    .line 928
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adsVisible:Z

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->adBanner:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 930
    :cond_0
    return-void
.end method

.method protected showCameraDialog(Lgbis/gbandroid/entities/Station;)V
    .locals 3
    .parameter

    .prologue
    .line 620
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 621
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 622
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 623
    return-void
.end method

.method protected showCityZipSearch()V
    .locals 3

    .prologue
    .line 511
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/search/SearchBar;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 512
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 513
    return-void
.end method

.method protected showDirections(DDLandroid/location/Location;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 611
    invoke-virtual {p5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 612
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    .line 613
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/maps?saddr="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 614
    const-string v3, "&daddr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 613
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 612
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 615
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 617
    :cond_0
    return-void
.end method

.method protected showFavourites()V
    .locals 3

    .prologue
    .line 568
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 569
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 570
    return-void
.end method

.method protected showHome()V
    .locals 3

    .prologue
    .line 555
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/search/MainScreen;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 556
    const/high16 v1, 0x3400

    invoke-virtual {v0, v1}, Lgbis/gbandroid/content/GBIntent;->setFlags(I)Landroid/content/Intent;

    .line 557
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Lgbis/gbandroid/content/GBIntent;->addFlags(I)Landroid/content/Intent;

    .line 558
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 559
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->finish()V

    .line 560
    return-void
.end method

.method protected showList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgbis/gbandroid/entities/ListResults;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide v4, 0x412e848000000000L

    .line 516
    invoke-virtual {p4}, Lgbis/gbandroid/entities/ListResults;->getTotalStations()I

    move-result v0

    if-lez v0, :cond_2

    .line 517
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/list/ListStations;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 518
    const-string v1, "list"

    invoke-virtual {v0, v1, p4}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 519
    const-string v1, "city"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    const-string v1, "state"

    invoke-virtual {v0, v1, p2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 521
    const-string v1, "zip"

    invoke-virtual {v0, v1, p3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ""

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 523
    const-string v1, "list sort order"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 526
    :goto_0
    const-string v1, "center latitude"

    invoke-virtual {p4}, Lgbis/gbandroid/entities/ListResults;->getCenter()Lgbis/gbandroid/entities/Center;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Center;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 527
    const-string v1, "center longitude"

    invoke-virtual {p4}, Lgbis/gbandroid/entities/ListResults;->getCenter()Lgbis/gbandroid/entities/Center;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Center;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 528
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 529
    instance-of v0, p0, Lgbis/gbandroid/activities/search/SearchBar;

    if-eqz v0, :cond_0

    .line 530
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->setResult(I)V

    .line 531
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivity;->finish()V

    .line 535
    :cond_0
    :goto_1
    return-void

    .line 525
    :cond_1
    const-string v1, "list sort order"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 534
    :cond_2
    invoke-direct {p0, p4}, Lgbis/gbandroid/activities/base/GBActivity;->a(Lgbis/gbandroid/entities/ListResults;)V

    goto :goto_1
.end method

.method protected showLogin()V
    .locals 3

    .prologue
    .line 578
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/MemberLogin;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 579
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 580
    return-void
.end method

.method public showMessage()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->unstackedToast:Lgbis/gbandroid/views/CustomToast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->unstackedToast:Lgbis/gbandroid/views/CustomToast;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomToast;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage(Ljava/lang/String;Z)V

    .line 256
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public showMessage(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 263
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage(Ljava/lang/String;Z)V

    .line 264
    return-void
.end method

.method public showMessage(Ljava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 267
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    if-nez p2, :cond_3

    .line 273
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->unstackedToast:Lgbis/gbandroid/views/CustomToast;

    if-nez v0, :cond_2

    .line 274
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->unstackedToast:Lgbis/gbandroid/views/CustomToast;

    .line 276
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->unstackedToast:Lgbis/gbandroid/views/CustomToast;

    .line 281
    :goto_1
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 282
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    goto :goto_0

    .line 279
    :cond_3
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public showMessage(Z)V
    .locals 1
    .parameter

    .prologue
    .line 259
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->userMessage:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage(Ljava/lang/String;Z)V

    .line 260
    return-void
.end method

.method protected showPlaces()V
    .locals 3

    .prologue
    .line 573
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/favorites/PlacesList;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 574
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 575
    return-void
.end method

.method protected showRegistration()V
    .locals 1

    .prologue
    .line 583
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->showRegistration(Z)V

    .line 584
    return-void
.end method

.method protected showRegistration(Z)V
    .locals 3
    .parameter

    .prologue
    .line 587
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 588
    const-string v1, "register show skip"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 589
    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 590
    return-void
.end method

.method protected showShare()V
    .locals 3

    .prologue
    .line 593
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/ShareApp;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 594
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 595
    return-void
.end method

.method public showSmartPrompt(Lgbis/gbandroid/entities/SmartPrompt;)V
    .locals 5
    .parameter

    .prologue
    .line 384
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v2, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 386
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030021

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 387
    const v0, 0x7f070091

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 388
    const v1, 0x7f070032

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 389
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 390
    const v3, 0x7f09019a

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lgbis/gbandroid/activities/base/e;

    invoke-direct {v4, p0, p1, v0}, Lgbis/gbandroid/activities/base/e;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/entities/SmartPrompt;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 396
    const v3, 0x7f09019d

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lgbis/gbandroid/activities/base/f;

    invoke-direct {v4, p0, p1, v0}, Lgbis/gbandroid/activities/base/f;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/entities/SmartPrompt;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 402
    const v3, 0x7f09019b

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lgbis/gbandroid/activities/base/g;

    invoke-direct {v4, p0, p1, v0}, Lgbis/gbandroid/activities/base/g;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/entities/SmartPrompt;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNeutralButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 408
    new-instance v3, Lgbis/gbandroid/activities/base/h;

    invoke-direct {v3, p0, v0}, Lgbis/gbandroid/activities/base/h;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 419
    const v3, 0x7f09012a

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/base/GBActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 420
    invoke-virtual {p1}, Lgbis/gbandroid/entities/SmartPrompt;->getPrompt()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v2

    .line 422
    const/high16 v3, 0x41a0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 423
    new-instance v1, Lgbis/gbandroid/activities/base/i;

    invoke-direct {v1, p0, p1, v0}, Lgbis/gbandroid/activities/base/i;-><init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/entities/SmartPrompt;Landroid/widget/CheckedTextView;)V

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 429
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 430
    return-void
.end method

.method protected showStationOnMap(Lgbis/gbandroid/entities/Station;)V
    .locals 3
    .parameter

    .prologue
    .line 605
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/map/MapStationDialog;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivity;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 606
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 607
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startActivity(Landroid/content/Intent;)V

    .line 608
    return-void
.end method

.method protected startGPSService()V
    .locals 3

    .prologue
    .line 626
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/services/GPSService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 627
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 628
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivity;->d:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lgbis/gbandroid/activities/base/GBActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 629
    return-void
.end method

.method protected stopGPSService()V
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->d:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 634
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->d:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 636
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected trackAnalytics()Z
    .locals 1

    .prologue
    .line 995
    const/4 v0, 0x1

    return v0
.end method

.method protected updateBrandVersion(II)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 688
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1, p2}, Lgbis/gbandroid/utils/DBHelper;->updateBrandVersionRow(II)Z

    move-result v0

    return v0
.end method

.method protected updatePlace(Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 655
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/DBHelper;->updateSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v0

    return v0
.end method

.method protected updatePlaceName(JLjava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 659
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivity;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1, p2, p3}, Lgbis/gbandroid/utils/DBHelper;->updateSearchesName(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method
