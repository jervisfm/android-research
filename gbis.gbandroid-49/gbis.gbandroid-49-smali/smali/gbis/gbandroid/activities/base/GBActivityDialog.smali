.class public abstract Lgbis/gbandroid/activities/base/GBActivityDialog;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# instance fields
.field private a:I

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogButtons()V

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->e:Landroid/widget/Button;

    if-nez v0, :cond_0

    .line 124
    invoke-virtual {p0, v1, v1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogPositiveButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->d:Landroid/widget/Button;

    if-nez v0, :cond_1

    .line 126
    invoke-virtual {p0, v1, v1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogNegativeButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->c:Landroid/widget/Button;

    if-nez v0, :cond_2

    .line 128
    invoke-virtual {p0, v1, v1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogNeutralButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    .line 130
    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogIcon(Landroid/graphics/drawable/Drawable;)V

    .line 131
    :cond_3
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 132
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->e:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;)V

    .line 139
    :cond_4
    :goto_0
    return-void

    .line 133
    :cond_5
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    .line 134
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->c:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;)V

    goto :goto_0

    .line 135
    :cond_6
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 136
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->d:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;)V

    goto :goto_0

    .line 137
    :cond_7
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    if-nez v0, :cond_4

    .line 138
    const v0, 0x7f070033

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/Button;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 159
    invoke-virtual {p1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 160
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 161
    const/high16 v1, 0x3f00

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 162
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    const v0, 0x7f070034

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 164
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 165
    const v0, 0x7f070038

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 166
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 167
    return-void
.end method

.method private a(Landroid/widget/Button;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    if-eqz p2, :cond_1

    .line 143
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    or-int/2addr v0, p3

    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->a:I

    .line 145
    if-eqz p4, :cond_0

    .line 146
    new-instance v0, Lgbis/gbandroid/activities/base/k;

    invoke-direct {v0, p0, p4}, Lgbis/gbandroid/activities/base/k;-><init>(Lgbis/gbandroid/activities/base/GBActivityDialog;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected getGBActivityDialogNegativeButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->d:Landroid/widget/Button;

    return-object v0
.end method

.method protected getGBActivityDialogNeutralButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->c:Landroid/widget/Button;

    return-object v0
.end method

.method protected getGBActivityDialogPositiveButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->e:Landroid/widget/Button;

    return-object v0
.end method

.method protected getGBContentLayout()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->b:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 31
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setContentView(I)V

    .line 33
    const v0, 0x7f070031

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->b:Landroid/widget/LinearLayout;

    .line 34
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a()V

    .line 35
    return-void
.end method

.method protected abstract setGBActivityDialogButtons()V
.end method

.method protected setGBActivityDialogIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .parameter

    .prologue
    .line 58
    const v0, 0x7f07002f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    .line 59
    if-eqz p1, :cond_0

    .line 60
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setGBActivityDialogNegativeButton(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {p0, v0, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogNegativeButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method

.method protected setGBActivityDialogNegativeButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 95
    const v0, 0x7f070037

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->d:Landroid/widget/Button;

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->d:Landroid/widget/Button;

    const/4 v1, 0x2

    invoke-direct {p0, v0, p1, v1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    .line 97
    return-void
.end method

.method protected setGBActivityDialogNeutralButton(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-virtual {p0, v0, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogNeutralButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 102
    return-void
.end method

.method protected setGBActivityDialogNeutralButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 105
    const v0, 0x7f070036

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->c:Landroid/widget/Button;

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->c:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-direct {p0, v0, p1, v1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    .line 107
    return-void
.end method

.method protected setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v0, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->setGBActivityDialogPositiveButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 82
    return-void
.end method

.method protected setGBActivityDialogPositiveButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 85
    const v0, 0x7f070035

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->e:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->e:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->a(Landroid/widget/Button;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method

.method protected setGBActivityDialogStationIcon(Lgbis/gbandroid/entities/Station;)V
    .locals 3
    .parameter

    .prologue
    .line 67
    const v0, 0x7f07002f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    .line 68
    if-eqz p1, :cond_1

    .line 69
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v1

    const-string v2, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/GBApplication;->getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    .line 71
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 74
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 77
    :goto_1
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 76
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected setGBActivityDialogTitle(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 54
    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method

.method protected setGBViewInContentLayout(IZ)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->mInflater:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 45
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 46
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->b:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    .line 48
    const/4 v3, -0x2

    invoke-direct {v2, v4, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 46
    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    if-eqz p2, :cond_0

    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityDialog;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 51
    :cond_0
    return-void
.end method
