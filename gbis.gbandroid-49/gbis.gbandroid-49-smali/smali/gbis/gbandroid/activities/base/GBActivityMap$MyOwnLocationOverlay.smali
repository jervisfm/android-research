.class public Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;
.super Lcom/google/android/maps/MyLocationOverlay;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBActivityMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MyOwnLocationOverlay"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivityMap;

.field private b:Z

.field private c:Z

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/GBActivityMap;Landroid/content/Context;Lcom/google/android/maps/MapView;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 273
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->a:Lgbis/gbandroid/activities/base/GBActivityMap;

    .line 274
    invoke-direct {p0, p2, p3}, Lcom/google/android/maps/MyLocationOverlay;-><init>(Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    .line 275
    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->b:Z

    .line 276
    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->c:Z

    .line 277
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    .line 278
    return-void
.end method


# virtual methods
.method protected drawMyLocation(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Landroid/location/Location;Lcom/google/android/maps/GeoPoint;J)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/high16 v5, 0x4120

    .line 287
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->c:Z

    if-nez v0, :cond_0

    .line 289
    :try_start_0
    invoke-super/range {p0 .. p6}, Lcom/google/android/maps/MyLocationOverlay;->drawMyLocation(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Landroid/location/Location;Lcom/google/android/maps/GeoPoint;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->c:Z

    if-eqz v0, :cond_3

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 296
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020099

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    .line 297
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->f:I

    .line 298
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->g:I

    .line 300
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    .line 301
    invoke-virtual {p3}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/Projection;->metersToEquatorPixels(F)F

    move-result v1

    .line 302
    const/4 v2, 0x0

    invoke-interface {v0, p4, v2}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 303
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 304
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 305
    cmpl-float v2, v1, v5

    if-lez v2, :cond_2

    .line 306
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 307
    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 309
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 310
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v5, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 312
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->f:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->g:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->f:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v5, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->g:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 313
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 315
    :cond_3
    return-void

    .line 291
    :catch_0
    move-exception v0

    iput-boolean v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->c:Z

    goto/16 :goto_0
.end method

.method public isFollowMe()Z
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->b:Z

    return v0
.end method

.method public setFollowMe(Z)V
    .locals 0
    .parameter

    .prologue
    .line 280
    iput-boolean p1, p0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->b:Z

    .line 281
    return-void
.end method
