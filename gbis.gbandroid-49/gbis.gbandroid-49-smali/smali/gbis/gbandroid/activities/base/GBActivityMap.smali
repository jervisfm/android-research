.class public abstract Lgbis/gbandroid/activities/base/GBActivityMap;
.super Lcom/google/android/maps/MapActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;
    }
.end annotation


# static fields
.field public static final ACTIVITY_CREATE:I = 0x1

.field protected static final DEFAULT_ZOOM_LVL:I = 0xe

.field public static final REPORT_PRICE:I = 0x3

.field protected static final RESULT_REPORT:I = 0x3

.field public static final SEARCH_PERFORMED:I = 0x4


# instance fields
.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mRes:Landroid/content/res/Resources;

.field protected mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation
.end field

.field protected mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

.field protected myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

.field protected progress:Landroid/app/Dialog;

.field protected userMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackPageView(Ljava/lang/String;)V

    .line 245
    const-string v0, "Analytics"

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsCustomVariables()V

    .line 247
    return-void
.end method


# virtual methods
.method public checkAwardAndShow()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 144
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getAwards()Ljava/util/List;

    move-result-object v2

    .line 146
    if-eqz v2, :cond_0

    .line 147
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 148
    if-lez v3, :cond_0

    .line 149
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 160
    :cond_0
    return-void

    .line 150
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AwardsMessage;

    .line 152
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AwardsMessage;->getAwarded()I

    move-result v4

    if-ne v4, v5, :cond_2

    .line 153
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->showAward(Lgbis/gbandroid/entities/AwardsMessage;)V

    .line 154
    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAwardBadgeFlag(Z)V

    .line 149
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected checkConnection()Z
    .locals 1

    .prologue
    .line 112
    invoke-static {p0}, Lgbis/gbandroid/utils/ConnectionUtils;->isConnectedToAny(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected getActivity()Lgbis/gbandroid/activities/base/GBActivityMap;
    .locals 0

    .prologue
    .line 108
    return-object p0
.end method

.method protected getDensity()F
    .locals 2

    .prologue
    .line 195
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 196
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 197
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "host"

    const v2, 0x7f0901dd

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivityMap;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLocation()Landroid/location/Location;
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getLastFix()Landroid/location/Location;

    move-result-object v0

    .line 204
    if-nez v0, :cond_0

    .line 205
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 208
    :cond_0
    :goto_0
    return-object v0

    .line 207
    :cond_1
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 320
    const/4 v0, 0x0

    return v0
.end method

.method protected launchSettings()V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/Settings;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 226
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->startActivity(Landroid/content/Intent;)V

    .line 227
    return-void
.end method

.method protected loadDialog(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 171
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 172
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    .line 173
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 174
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 176
    :cond_0
    return-void
.end method

.method protected loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 181
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 182
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/base/l;

    invoke-direct {v1, p0, p2}, Lgbis/gbandroid/activities/base/l;-><init>(Lgbis/gbandroid/activities/base/GBActivityMap;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 190
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 192
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mInflater:Landroid/view/LayoutInflater;

    .line 70
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->progress:Landroid/app/Dialog;

    .line 71
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    .line 72
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mRes:Landroid/content/res/Resources;

    .line 73
    invoke-static {}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->getInstance()Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    .line 74
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const v1, 0x7f090226

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->startNewSession(Ljava/lang/String;Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter

    .prologue
    .line 97
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PhoneButton"

    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivityMap;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onDestroy()V

    .line 92
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->dispatch()Z

    .line 93
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter

    .prologue
    .line 103
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MenuButton"

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onResume()V

    .line 86
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->a()V

    .line 87
    return-void
.end method

.method protected setAnalyticsCustomVariables()V
    .locals 6

    .prologue
    .line 250
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x1

    const-string v2, "AuthId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "auth_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 251
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x2

    const-string v2, "AppVersion"

    invoke-static {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 252
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x3

    const-string v2, "MemberId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "member_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 253
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x4

    const-string v2, "FuelType"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "fuelPreference"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 254
    return-void
.end method

.method protected abstract setAnalyticsPageName()Ljava/lang/String;
.end method

.method protected setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 258
    return-void
.end method

.method protected setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 261
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScreenButton"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 262
    return-void
.end method

.method protected setAwardBadgeFlag(Z)V
    .locals 2
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 164
    const-string v1, "awards_badge"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 165
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 135
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->userMessage:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public showAward(Lgbis/gbandroid/entities/AwardsMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 139
    new-instance v0, Lgbis/gbandroid/views/CustomToastAward;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToastAward;-><init>(Landroid/app/Activity;Lgbis/gbandroid/entities/AwardsMessage;I)V

    .line 140
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToastAward;->show()V

    .line 141
    return-void
.end method

.method protected showCityZipSearch()V
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/search/SearchBar;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 221
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->startActivityForResult(Landroid/content/Intent;I)V

    .line 222
    return-void
.end method

.method protected showFavourites()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 231
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->startActivityForResult(Landroid/content/Intent;I)V

    .line 232
    return-void
.end method

.method protected showHome()V
    .locals 3

    .prologue
    .line 212
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/search/MainScreen;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 213
    const/high16 v1, 0x3400

    invoke-virtual {v0, v1}, Lgbis/gbandroid/content/GBIntent;->setFlags(I)Landroid/content/Intent;

    .line 214
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->startActivity(Landroid/content/Intent;)V

    .line 215
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setResult(I)V

    .line 216
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->finish()V

    .line 217
    return-void
.end method

.method public showMessage()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->userMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->userMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivityMap;->userMessage:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 129
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 130
    const-string v0, ""

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->setMessage(Ljava/lang/String;)V

    .line 132
    :cond_0
    return-void
.end method

.method public showMessage(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 120
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 122
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 124
    :cond_0
    return-void
.end method

.method protected showPlaces()V
    .locals 3

    .prologue
    .line 235
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 236
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->startActivity(Landroid/content/Intent;)V

    .line 237
    return-void
.end method
