.class final Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;
.super Landroid/widget/Filter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBActivitySearch$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;


# direct methods
.method private constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)V
    .locals 0
    .parameter

    .prologue
    .line 611
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch$a;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 611
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)V

    return-void
.end method


# virtual methods
.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7
    .parameter

    .prologue
    .line 614
    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 615
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 616
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 617
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-lt v2, v5, :cond_0

    .line 622
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v3, Landroid/widget/Filter$FilterResults;->count:I

    .line 623
    iput-object v4, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 624
    return-object v3

    .line 618
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    .line 619
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 620
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 629
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a(Lgbis/gbandroid/activities/base/GBActivitySearch$a;Ljava/util/List;)V

    .line 630
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 631
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->notifyDataSetChanged()V

    .line 634
    :goto_0
    return-void

    .line 633
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
