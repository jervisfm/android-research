.class final Lgbis/gbandroid/activities/base/GBActivitySearch$a;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBActivitySearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/AutocompletePlaces;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutocompletePlaces;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutocompletePlaces;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;

.field private f:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Landroid/content/Context;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IZ)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f03003d

    .line 558
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 559
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 560
    iput-object p3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->b:Ljava/util/List;

    .line 561
    iput-object p3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    .line 562
    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->d:I

    .line 563
    iput-boolean p4, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->f:Z

    .line 564
    if-eqz p4, :cond_0

    .line 565
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch$a;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->e:Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;

    .line 566
    :cond_0
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 553
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch$a;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 554
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)Z
    .locals 1
    .parameter

    .prologue
    .line 557
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->f:Z

    return v0
.end method


# virtual methods
.method public final a(I)Lgbis/gbandroid/entities/AutocompletePlaces;
    .locals 1
    .parameter

    .prologue
    .line 608
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 603
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 594
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->e:Lgbis/gbandroid/activities/base/GBActivitySearch$a$a;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a(I)Lgbis/gbandroid/entities/AutocompletePlaces;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 571
    if-nez p2, :cond_1

    .line 572
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 573
    new-instance v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;

    invoke-direct {v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$e;-><init>()V

    .line 574
    const v0, 0x7f0700e2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->a:Landroid/widget/TextView;

    .line 575
    const v0, 0x7f0700e1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->b:Landroid/widget/ImageView;

    .line 576
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 579
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    .line 580
    if-eqz v0, :cond_0

    .line 581
    iget-object v2, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->isPlace()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 583
    iget-object v2, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->b:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 584
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 585
    iget-object v1, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 589
    :cond_0
    :goto_1
    return-object p2

    .line 578
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivitySearch$e;

    move-object v1, v0

    goto :goto_0

    .line 587
    :cond_2
    iget-object v0, v1, Lgbis/gbandroid/activities/base/GBActivitySearch$e;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
