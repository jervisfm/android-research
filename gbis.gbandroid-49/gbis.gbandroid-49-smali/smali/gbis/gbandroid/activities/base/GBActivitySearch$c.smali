.class final Lgbis/gbandroid/activities/base/GBActivitySearch$c;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBActivitySearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutoCompMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 703
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 704
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;Z)V

    .line 705
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 709
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 710
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 711
    iget-object v4, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->b:Ljava/util/List;

    .line 712
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 713
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    .line 714
    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->getCount()I

    move-result v2

    .line 715
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v1, v3

    .line 716
    :goto_0
    if-lt v1, v2, :cond_1

    move v2, v3

    .line 718
    :goto_1
    if-lt v2, v6, :cond_2

    .line 727
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v5, v3}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Landroid/content/Context;Ljava/util/List;Z)V

    .line 728
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v1, v1, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 729
    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v1, v1, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 731
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    :cond_0
    :goto_2
    return-void

    .line 717
    :cond_1
    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->a(I)Lgbis/gbandroid/entities/AutocompletePlaces;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 719
    :cond_2
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/AutoCompMessage;

    .line 720
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AutoCompMessage;->getCity()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AutoCompMessage;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 721
    iget-object v7, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v5, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 722
    new-instance v7, Lgbis/gbandroid/entities/AutocompletePlaces;

    invoke-direct {v7, v1, v3}, Lgbis/gbandroid/entities/AutocompletePlaces;-><init>(Ljava/lang/String;Z)V

    .line 723
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 724
    invoke-virtual {v0, v7}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->add(Ljava/lang/Object;)V

    .line 718
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 738
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b(Lgbis/gbandroid/activities/base/GBActivitySearch;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->b:Ljava/util/List;

    .line 739
    const/4 v0, 0x1

    return v0
.end method
