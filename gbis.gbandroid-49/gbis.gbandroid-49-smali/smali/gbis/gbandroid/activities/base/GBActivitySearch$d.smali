.class final Lgbis/gbandroid/activities/base/GBActivitySearch$d;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBActivitySearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 645
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 646
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 647
    return-void
.end method

.method public constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 649
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 650
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 651
    iput-object p3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->b:Ljava/lang/String;

    .line 652
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 656
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 658
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 662
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 663
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->screenPreference:Ljava/lang/String;

    const-string v1, "home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    instance-of v0, v0, Lgbis/gbandroid/activities/search/MainScreen;

    if-eqz v0, :cond_2

    .line 664
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->finish()V

    .line 668
    :cond_1
    :goto_1
    return-void

    .line 666
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->screenPreference:Ljava/lang/String;

    const-string v1, "home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    instance-of v0, v0, Lgbis/gbandroid/activities/search/MainScreen;

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->autoComplete()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v1

    .line 673
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 674
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    .line 675
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    new-instance v2, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v2, v0, v0}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-static {v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;Lcom/google/android/maps/GeoPoint;)V

    .line 676
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    instance-of v0, v0, Lgbis/gbandroid/activities/favorites/PlacesList;

    if-eqz v0, :cond_1

    .line 677
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;)Z

    .line 687
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 679
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;)V

    goto :goto_0

    .line 681
    :cond_2
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    if-eqz v1, :cond_5

    .line 682
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 684
    :cond_5
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    const v3, 0x7f090017

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setMessage(Ljava/lang/String;)V

    goto :goto_1
.end method
