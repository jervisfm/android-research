.class public abstract Lgbis/gbandroid/activities/base/GBActivitySearch;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBActivitySearch$a;,
        Lgbis/gbandroid/activities/base/GBActivitySearch$b;,
        Lgbis/gbandroid/activities/base/GBActivitySearch$c;,
        Lgbis/gbandroid/activities/base/GBActivitySearch$d;,
        Lgbis/gbandroid/activities/base/GBActivitySearch$e;
    }
.end annotation


# static fields
.field public static final AWARD:Ljava/lang/String; = "award"

.field public static final BY_DISTANCE:I = 0x1

.field public static final BY_LOCATION:I = 0x3

.field public static final BY_PRICE:I = 0x2

.field public static final CENTER_LAT:Ljava/lang/String; = "center latitude"

.field public static final CENTER_LONG:Ljava/lang/String; = "center longitude"

.field public static final CITY:Ljava/lang/String; = "city"

.field public static final CONNECTION_ERROR:I = 0x9

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final CURRENCY:Ljava/lang/String; = "currency"

.field public static final FAVOURITE_ID:Ljava/lang/String; = "favourite id"

.field public static final FROM_MAP:Ljava/lang/String; = "from map"

.field public static final FUEL_DIESEL:Ljava/lang/String; = "fuel diesel"

.field public static final FUEL_MIDGRADE:Ljava/lang/String; = "fuel midgrade"

.field public static final FUEL_PREMIUM:Ljava/lang/String; = "fuel premium"

.field public static final FUEL_REGULAR:Ljava/lang/String; = "fuel regular"

.field public static final GB_LIST:Ljava/lang/String; = "list"

.field public static final GB_MAP:Ljava/lang/String; = "map"

.field public static final LIST_SORT_ORDER:Ljava/lang/String; = "list sort order"

.field public static final MAP_TOTAL_STATIONS:Ljava/lang/String; = "map total stations"

.field public static final MILES:I = 0x32

.field public static final MY_LAT:Ljava/lang/String; = "my latitude"

.field public static final MY_LOCATION:Ljava/lang/String; = "my location"

.field public static final MY_LONG:Ljava/lang/String; = "my longitude"

.field public static final PLACE_NAME:Ljava/lang/String; = "place name"

.field public static final PRICE:Ljava/lang/String; = "price"

.field public static final REGISTER_SHOW_SKIP:Ljava/lang/String; = "register show skip"

.field public static final RTN:I = 0x64

.field public static final SEARCH_TERMS:Ljava/lang/String; = "search terms"

.field public static final SEARCH_TYPE:Ljava/lang/String; = "search type"

.field public static final SM_ID:Ljava/lang/String; = "sm_id"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final STATION:Ljava/lang/String; = "station"

.field public static final STATION_ADDR:Ljava/lang/String; = "station address"

.field public static final STATION_NM:Ljava/lang/String; = "station_nm"

.field public static final STATION_PHOTOS:Ljava/lang/String; = "station photos"

.field public static final STATION_PHOTOS_POSITION:Ljava/lang/String; = "station photos position"

.field public static final TIME_OFFSET:Ljava/lang/String; = "time offset"

.field public static final TIME_SPOTTED:Ljava/lang/String; = "time spotted"

.field public static final TIME_SPOTTED_DIESEL:Ljava/lang/String; = "time spotted diesel"

.field public static final TIME_SPOTTED_MIDGRADE:Ljava/lang/String; = "time spotted midgrade"

.field public static final TIME_SPOTTED_PREMIUM:Ljava/lang/String; = "time spotted premium"

.field public static final TIME_SPOTTED_REGULAR:Ljava/lang/String; = "time spotted regular"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOTAL_NEAR:Ljava/lang/String; = "total near"

.field public static final TOTAL_STATIONS:Ljava/lang/String; = "total stations"

.field public static final ZIP:Ljava/lang/String; = "zip"


# instance fields
.field final a:Landroid/os/Handler;

.field protected activityLoading:Z

.field protected authId:Ljava/lang/String;

.field private b:Lcom/google/android/maps/GeoPoint;

.field protected buttonNearMe:Landroid/view/View;

.field protected buttonSearch:Landroid/widget/Button;

.field private c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

.field private d:I

.field private e:Z

.field protected fuelPreference:Ljava/lang/String;

.field protected memberId:Ljava/lang/String;

.field protected radiusPreference:I

.field protected screenPreference:Ljava/lang/String;

.field protected searchCityZip:Landroid/widget/AutoCompleteTextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    .line 122
    const/16 v0, 0x190

    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->d:I

    .line 743
    new-instance v0, Lgbis/gbandroid/activities/base/m;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/m;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->a:Landroid/os/Handler;

    .line 58
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch;)Lcom/google/android/maps/GeoPoint;
    .locals 1
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    return-object v0
.end method

.method private a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutoCompMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Lgbis/gbandroid/activities/base/t;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/t;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/t;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 185
    new-instance v1, Lgbis/gbandroid/queries/AutoCompleteCityQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 186
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 188
    return-object v0
.end method

.method private a(I)V
    .locals 3
    .parameter

    .prologue
    .line 235
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$b;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch$b;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Landroid/os/Handler;I)V

    .line 236
    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$b;->start()V

    .line 237
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->c(I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch;Lcom/google/android/maps/GeoPoint;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const-wide v5, 0x412e848000000000L

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->activityLoading:Z

    .line 193
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getPreferences()V

    .line 194
    new-instance v0, Lgbis/gbandroid/activities/base/u;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/base/u;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/u;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 195
    new-instance v1, Lgbis/gbandroid/queries/ListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 196
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    if-nez v0, :cond_1

    .line 197
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    .line 198
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 199
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->myLocation:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    .line 203
    :cond_1
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1, p1, p2, p3, v0}, Lgbis/gbandroid/queries/ListQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 204
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 205
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListResults;

    .line 206
    invoke-virtual {p0, p1, p2, p3, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->showList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgbis/gbandroid/entities/ListResults;)V

    .line 208
    :cond_2
    return-void

    .line 201
    :cond_3
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v4, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 500
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 501
    const-string v1, ""

    const-string v2, ""

    invoke-direct {p0, v1, v2, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    :goto_0
    const/4 v0, 0x1

    :cond_0
    return v0

    .line 503
    :catch_0
    move-exception v1

    invoke-static {p1}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 504
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 505
    const-string v0, ""

    const-string v2, ""

    invoke-direct {p0, v0, v2, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 507
    :cond_1
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 508
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 509
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 511
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x3

    if-le v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 512
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object p1, v0

    move-object v0, v1

    .line 520
    :goto_1
    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 514
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x2

    if-le v2, v3, :cond_3

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 515
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object p1, v0

    move-object v0, v1

    goto :goto_1

    .line 518
    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method static synthetic a(Ljava/util/List;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 691
    invoke-static {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/base/GBActivitySearch;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 183
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 467
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 468
    iget-boolean v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->e:Z

    if-nez v1, :cond_2

    .line 469
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getAllPlaces(I)Landroid/database/Cursor;

    move-result-object v1

    .line 470
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 472
    :cond_0
    new-instance v2, Lgbis/gbandroid/entities/AutocompletePlaces;

    const-string v3, "table_column_name"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lgbis/gbandroid/entities/AutocompletePlaces;-><init>(Ljava/lang/String;Z)V

    .line 473
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 474
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    .line 471
    if-nez v2, :cond_0

    .line 476
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 478
    :cond_2
    new-instance v1, Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, v4}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Landroid/content/Context;Ljava/util/List;Z)V

    .line 479
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 480
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/base/p;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/p;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 494
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2
    .parameter

    .prologue
    .line 407
    const-string v0, ""

    .line 408
    packed-switch p1, :pswitch_data_0

    .line 423
    :goto_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setAnalyticsTrackEventAutocomplete(Ljava/lang/String;)V

    .line 428
    return-void

    .line 410
    :pswitch_0
    const v0, 0x7f09016f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 411
    iget-boolean v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->e:Z

    if-nez v1, :cond_0

    .line 412
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->showPlaces()V

    goto :goto_0

    .line 414
    :cond_0
    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 417
    :pswitch_1
    const v0, 0x7f090169

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->startNearMeThread()V

    goto :goto_0

    .line 421
    :pswitch_2
    const v1, 0x7f09016b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    .line 422
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->showFavourites()V

    goto :goto_0

    .line 408
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic b(Lgbis/gbandroid/activities/base/GBActivitySearch;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 406
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b(I)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/base/GBActivitySearch;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 528
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 529
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 530
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 531
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getText()Ljava/lang/String;

    move-result-object v0

    .line 532
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 533
    if-ltz v1, :cond_1

    .line 534
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 535
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 536
    const-string v1, ""

    invoke-direct {p0, v2, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 539
    const-string v1, ""

    const-string v2, ""

    invoke-direct {p0, v0, v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 542
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 543
    const-string v1, ""

    const-string v2, ""

    invoke-direct {p0, v0, v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutocompletePlaces;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 692
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 693
    :goto_0
    if-lt v2, v3, :cond_0

    move v0, v1

    .line 698
    :goto_1
    return v0

    .line 694
    :cond_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    .line 695
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 696
    const/4 v0, 0x1

    goto :goto_1

    .line 693
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private c(I)V
    .locals 2
    .parameter

    .prologue
    .line 431
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 432
    iget-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->e:Z

    if-nez v0, :cond_2

    .line 433
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->getText()Ljava/lang/String;

    move-result-object v0

    .line 434
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getPlace(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 435
    if-eqz v0, :cond_0

    .line 436
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 439
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 448
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->startCityZipThread(Ljava/lang/String;)V

    .line 449
    :goto_0
    return-void

    .line 442
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 443
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchUsingPlaces(Landroid/database/Cursor;)V

    goto :goto_0

    .line 446
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->startCityZipThread(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/base/GBActivitySearch;)V
    .locals 0
    .parameter

    .prologue
    .line 466
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->b()V

    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/base/GBActivitySearch;)V
    .locals 1
    .parameter

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(I)V

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/base/GBActivitySearch;)I
    .locals 1
    .parameter

    .prologue
    .line 122
    iget v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->d:I

    return v0
.end method


# virtual methods
.method protected autoComplete()V
    .locals 3

    .prologue
    .line 256
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 258
    if-eqz v0, :cond_1

    .line 259
    const/4 v1, 0x1

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    .line 264
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 265
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lgbis/gbandroid/activities/base/v;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/base/v;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 291
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v2, Lgbis/gbandroid/activities/base/w;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/base/w;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 307
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 312
    if-eqz v0, :cond_0

    .line 313
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    .line 316
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/base/x;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/x;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 349
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/base/n;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/n;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 359
    return-void

    .line 261
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getPreferences()V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->fuelPreference:Ljava/lang/String;

    .line 241
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->memberId:Ljava/lang/String;

    .line 243
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "screenPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->screenPreference:Ljava/lang/String;

    .line 244
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "radiusPreference"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->radiusPreference:I

    .line 245
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "auth_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->authId:Ljava/lang/String;

    .line 247
    return-void
.end method

.method protected getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 127
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getPreferences()V

    .line 129
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onResume()V

    .line 135
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->openDB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->e:Z

    .line 137
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setAutocompleteWithQuickLinks()V

    .line 138
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 174
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 175
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 179
    :goto_0
    return-object v0

    .line 178
    :cond_0
    const-string v1, ""

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method protected populateButtons()V
    .locals 2

    .prologue
    .line 141
    const v0, 0x7f070106

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->buttonSearch:Landroid/widget/Button;

    .line 142
    const v0, 0x7f070107

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    .line 143
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->buttonSearch:Landroid/widget/Button;

    new-instance v1, Lgbis/gbandroid/activities/base/q;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/q;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->buttonNearMe:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->buttonNearMe:Landroid/view/View;

    new-instance v1, Lgbis/gbandroid/activities/base/r;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/r;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/base/s;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/s;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 170
    return-void
.end method

.method protected resetFocus()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    const/high16 v1, 0x3f80

    .line 362
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 364
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v7}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 365
    instance-of v0, p0, Lgbis/gbandroid/activities/search/MainScreen;

    if-eqz v0, :cond_0

    .line 366
    const v0, 0x7f07003a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 367
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 368
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 369
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 370
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 371
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    .line 372
    invoke-virtual {v6}, Landroid/widget/ImageView;->requestFocus()Z

    move-object v0, p0

    .line 373
    check-cast v0, Lgbis/gbandroid/activities/search/MainScreen;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->resizeElements(Landroid/content/res/Configuration;)V

    .line 374
    new-instance v0, Lgbis/gbandroid/animations/MyScaleAnimation;

    const/4 v3, 0x0

    iget v5, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->d:I

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/animations/MyScaleAnimation;-><init>(FFFFILandroid/view/View;Z)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 377
    :cond_0
    return-void
.end method

.method protected searchUsingPlaces(Landroid/database/Cursor;)V
    .locals 9
    .parameter

    .prologue
    const-wide v7, 0x412e848000000000L

    const-wide/16 v5, 0x0

    .line 452
    const-string v0, "table_column_latitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 453
    const-string v2, "table_column_longitude"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 454
    cmpl-double v4, v0, v5

    if-nez v4, :cond_0

    cmpl-double v4, v2, v5

    if-nez v4, :cond_0

    .line 455
    const-string v0, "table_column_city_zip"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 456
    const-string v1, "table_column_name"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->startPlacesThread(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :goto_0
    const-string v0, "table_column_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->incrementFrequencyOfPlace(Ljava/lang/String;)Z

    .line 464
    return-void

    .line 458
    :cond_0
    new-instance v4, Lcom/google/android/maps/GeoPoint;

    mul-double/2addr v0, v7

    double-to-int v0, v0

    mul-double v1, v2, v7

    double-to-int v1, v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v4, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->b:Lcom/google/android/maps/GeoPoint;

    .line 460
    const-string v0, "table_column_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->startPlacesThread(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setAutocompleteWithQuickLinks()V
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setAutocompleteWithQuickLinks(Z)V

    .line 381
    return-void
.end method

.method protected setAutocompleteWithQuickLinks(Z)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 384
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;->b(Lgbis/gbandroid/activities/base/GBActivitySearch$a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 386
    new-instance v1, Lgbis/gbandroid/entities/AutocompletePlaces;

    const v2, 0x7f09016f

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f020058

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lgbis/gbandroid/entities/AutocompletePlaces;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    .line 387
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    new-instance v1, Lgbis/gbandroid/entities/AutocompletePlaces;

    const v2, 0x7f090169

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f020057

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lgbis/gbandroid/entities/AutocompletePlaces;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    .line 389
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->memberId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 391
    new-instance v1, Lgbis/gbandroid/entities/AutocompletePlaces;

    const v2, 0x7f09016b

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->mRes:Landroid/content/res/Resources;

    const v4, 0x7f020056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lgbis/gbandroid/entities/AutocompletePlaces;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V

    .line 392
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_1
    new-instance v1, Lgbis/gbandroid/activities/base/GBActivitySearch$a;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/activities/base/GBActivitySearch$a;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Landroid/content/Context;Ljava/util/List;Z)V

    .line 395
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 396
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/base/o;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/o;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 404
    :cond_2
    return-void
.end method

.method protected startCityZipThread(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 211
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    .line 212
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 213
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 214
    return-void
.end method

.method protected startNearMeThread()V
    .locals 2

    .prologue
    .line 217
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    .line 218
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 219
    const v0, 0x7f090085

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 220
    return-void
.end method

.method protected startPlacesThread(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 223
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    .line 224
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 225
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 226
    return-void
.end method

.method protected startPlacesThread(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 229
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-direct {v0, p0, p0, p2}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    .line 230
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 231
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBActivitySearch;->c:Lgbis/gbandroid/activities/base/GBActivitySearch$d;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 232
    return-void
.end method
