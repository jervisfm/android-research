.class final Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/GBPreferenceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

.field private b:[Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/GBPreferenceActivity;Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v0, 0x7f030032

    const/4 v1, 0x0

    .line 285
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    .line 286
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 287
    iput-object p3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->b:[Ljava/lang/String;

    .line 288
    iput-object p4, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->c:[Ljava/lang/String;

    .line 289
    iput v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->e:I

    .line 290
    iput-object p5, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->d:Ljava/lang/String;

    .line 292
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p4, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    iput-boolean v1, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->f:Z

    goto :goto_0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 300
    .line 301
    if-nez p2, :cond_3

    .line 302
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 304
    check-cast v0, Landroid/widget/CheckedTextView;

    .line 305
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->b:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-boolean v2, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->f:Z

    if-eqz v2, :cond_1

    .line 307
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->c:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    iget-object v3, v3, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->d:Ljava/lang/String;

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 308
    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 317
    :goto_1
    return-object v1

    .line 310
    :cond_0
    invoke-virtual {v0, v6}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 312
    :cond_1
    iget-object v2, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->c:[Ljava/lang/String;

    aget-object v2, v2, p1

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    iget-object v3, v3, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v4, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;->d:Ljava/lang/String;

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 313
    invoke-virtual {v0, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 315
    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move-object v1, p2

    goto :goto_0
.end method
