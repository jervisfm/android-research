.class public abstract Lgbis/gbandroid/activities/base/GBPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;
    }
.end annotation


# static fields
.field public static final AD_FAVORITES:Ljava/lang/String; = "adFavorites"

.field public static final AD_FAVORITES_KEY:Ljava/lang/String; = "adFavoritesKey"

.field public static final AD_FAVORITES_UNIT:Ljava/lang/String; = "adFavoritesUnit"

.field public static final AD_INIT:Ljava/lang/String; = "adInit"

.field public static final AD_INIT_KEY:Ljava/lang/String; = "adInitKey"

.field public static final AD_INIT_TIME:Ljava/lang/String; = "adInitTime"

.field public static final AD_INIT_UNIT:Ljava/lang/String; = "adInitUnit"

.field public static final AD_LIST:Ljava/lang/String; = "adList"

.field public static final AD_LIST_BOTTOM:Ljava/lang/String; = "adListBottom"

.field public static final AD_LIST_BOTTOM_KEY:Ljava/lang/String; = "adListBottomKey"

.field public static final AD_LIST_BOTTOM_UNIT:Ljava/lang/String; = "adListBottomUnit"

.field public static final AD_LIST_KEY:Ljava/lang/String; = "adListKey"

.field public static final AD_LIST_SCROLL_TIME:Ljava/lang/String; = "adListScrollTime"

.field public static final AD_LIST_TOP:Ljava/lang/String; = "adListTop"

.field public static final AD_LIST_TOP_KEY:Ljava/lang/String; = "adListTopKey"

.field public static final AD_LIST_TOP_UNIT:Ljava/lang/String; = "adListTopUnit"

.field public static final AD_LIST_UNIT:Ljava/lang/String; = "adListUnit"

.field public static final AD_LIST_WAIT_TIME:Ljava/lang/String; = "adListWaitTime"

.field public static final AD_MAIN:Ljava/lang/String; = "adMain"

.field public static final AD_MAIN_KEY:Ljava/lang/String; = "adMainKey"

.field public static final AD_MAIN_UNIT:Ljava/lang/String; = "adMainUnit"

.field public static final AD_PROFILE:Ljava/lang/String; = "adProfile"

.field public static final AD_PROFILE_KEY:Ljava/lang/String; = "adProfileKey"

.field public static final AD_PROFILE_UNIT:Ljava/lang/String; = "adProfileUnit"

.field public static final AD_STATION:Ljava/lang/String; = "adStation"

.field public static final AD_STATION_KEY:Ljava/lang/String; = "adStationKey"

.field public static final AD_STATION_UNIT:Ljava/lang/String; = "adStationUnit"

.field public static final AUTH_ID:Ljava/lang/String; = "auth_id"

.field public static final CAR:Ljava/lang/String; = "car"

.field public static final CAR_ICON_ID:Ljava/lang/String; = "car_icon_id"

.field public static final CUSTOM_KEYBOARD_PREFERENCE:Ljava/lang/String; = "customKeyboardPreference"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DISTANCE_PREFERENCE:Ljava/lang/String; = "distancePreference"

.field public static final DISTRIBUTION_METHOD:Ljava/lang/String; = "distributionMethod"

.field public static final FUEL_PREFERENCE:Ljava/lang/String; = "fuelPreference"

.field public static final GB_APP_CATEGORY:Ljava/lang/String; = "GbAppCategory"

.field public static final HOST:Ljava/lang/String; = "host"

.field public static final INITIAL_SCREEN_DATE:Ljava/lang/String; = "initial_screen_date"

.field public static final IS_MEMBER:Ljava/lang/String; = "is_member"

.field public static final LARGE_SCREEN_BOOLEAN:Ljava/lang/String; = "largeScreenBoolean"

.field public static final LAST_LATITUDE:Ljava/lang/String; = "lastLatitude"

.field public static final LAST_LONGITUDE:Ljava/lang/String; = "lastLongitude"

.field public static final LAST_REPORTED_PRICE_DATE:Ljava/lang/String; = "lastReportedPriceDate"

.field public static final LIKE_APP:Ljava/lang/String; = "like_app"

.field public static final LIKE_APP_DATE:Ljava/lang/String; = "like_app_date"

.field public static final MEMBER:Ljava/lang/String; = "member"

.field public static final MEMBER_ID:Ljava/lang/String; = "member_id"

.field public static final MEMBER_PASSWORD:Ljava/lang/String; = "password"

.field public static final NEARME_PREFERENCE:Ljava/lang/String; = "nearmePreference"

.field public static final NO_PRICES_PREFERENCE:Ljava/lang/String; = "noPricesPreference"

.field public static final RADIUS_PREFERENCE:Ljava/lang/String; = "radiusPreference"

.field public static final REFERRER_STRING:Ljava/lang/String; = "referralString"

.field public static final SCREEN_PREFERENCE:Ljava/lang/String; = "screenPreference"

.field public static final SHARE_APP:Ljava/lang/String; = "share_app"

.field public static final SHARE_APP_DATE:Ljava/lang/String; = "share_app_date"

.field public static final SHOW_UPDATE_PRICE_DIALOG:Ljava/lang/String; = "showUpdatePriceDialog"

.field public static final SMART_PROMPT_PREFERENCE:Ljava/lang/String; = "smartPromptPreference"


# instance fields
.field private a:Ljava/lang/String;

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation
.end field

.field protected mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

.field protected myLocation:Landroid/location/Location;

.field protected progress:Landroid/app/Dialog;

.field protected res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackPageView(Ljava/lang/String;)V

    .line 257
    const-string v0, "Analytics"

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsCustomVariables()V

    .line 259
    return-void
.end method


# virtual methods
.method public getHost()Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "host"

    const v2, 0x7f0901dd

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method protected loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 186
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;

    .line 187
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->create()Lgbis/gbandroid/views/CustomProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    .line 188
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 189
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/base/z;

    invoke-direct {v1, p0, p2}, Lgbis/gbandroid/activities/base/z;-><init>(Lgbis/gbandroid/activities/base/GBPreferenceActivity;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 195
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 197
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->res:Landroid/content/res/Resources;

    .line 117
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mInflater:Landroid/view/LayoutInflater;

    .line 118
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->progress:Landroid/app/Dialog;

    .line 119
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    .line 120
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 121
    if-eqz v1, :cond_0

    .line 122
    const-string v0, "my location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 123
    if-eqz v0, :cond_1

    .line 124
    new-instance v2, Landroid/location/Location;

    const-string v0, "my location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v2, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->myLocation:Landroid/location/Location;

    .line 128
    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->getInstance()Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const v1, 0x7f090226

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->startNewSession(Ljava/lang/String;Landroid/content/Context;)V

    .line 130
    return-void

    .line 126
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMyLocationToZeros()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter

    .prologue
    .line 152
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PhoneButton"

    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 147
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->dispatch()Z

    .line 148
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter

    .prologue
    .line 158
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MenuButton"

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 140
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 141
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->a()V

    .line 142
    return-void
.end method

.method protected setAnalyticsCustomVariables()V
    .locals 6

    .prologue
    .line 262
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x1

    const-string v2, "AuthId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "auth_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 263
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x2

    const-string v2, "AppVersion"

    invoke-static {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 264
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x3

    const-string v2, "MemberId"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "member_id"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 265
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const/4 v1, 0x4

    const-string v2, "FuelType"

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, "fuelPreference"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->setCustomVar(ILjava/lang/String;Ljava/lang/String;)Z

    .line 266
    return-void
.end method

.method protected abstract setAnalyticsPageName()Ljava/lang/String;
.end method

.method protected setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 269
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 270
    return-void
.end method

.method protected setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ScreenButton"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 274
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->a:Ljava/lang/String;

    .line 181
    return-void
.end method

.method protected setMyLocationToZeros()V
    .locals 2

    .prologue
    .line 252
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->myLocation:Landroid/location/Location;

    .line 253
    return-void
.end method

.method protected showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 204
    .line 205
    new-instance v9, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v9, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 206
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030014

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 207
    invoke-virtual {v9, p4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 208
    invoke-virtual {v9, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 209
    const v1, 0x7f070047

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 210
    new-instance v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/base/GBPreferenceActivity$a;-><init>(Lgbis/gbandroid/activities/base/GBPreferenceActivity;Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 214
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p2, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    move v1, v8

    .line 217
    :goto_0
    array-length v2, p2

    if-lt v1, v2, :cond_1

    .line 230
    :cond_0
    invoke-virtual {v6, v1, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 231
    invoke-virtual {v9}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 232
    new-instance v1, Lgbis/gbandroid/activities/base/aa;

    invoke-direct {v1, p0, p2, p3, v0}, Lgbis/gbandroid/activities/base/aa;-><init>(Lgbis/gbandroid/activities/base/GBPreferenceActivity;[Ljava/lang/String;Ljava/lang/String;Landroid/app/Dialog;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 248
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 249
    return-void

    .line 217
    :catch_0
    move-exception v0

    move v0, v8

    move v1, v8

    goto :goto_0

    .line 218
    :cond_1
    if-eqz v0, :cond_2

    .line 219
    aget-object v2, p2, v1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v3, p3, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    :cond_2
    aget-object v2, p2, v1

    iget-object v3, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->mPrefs:Landroid/content/SharedPreferences;

    const-string v4, ""

    invoke-interface {v3, p3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public showMessage()V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 174
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 175
    const-string v0, ""

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMessage(Ljava/lang/String;)V

    .line 177
    :cond_0
    return-void
.end method

.method public showMessage(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 167
    new-instance v0, Lgbis/gbandroid/views/CustomToast;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 168
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 169
    return-void
.end method
