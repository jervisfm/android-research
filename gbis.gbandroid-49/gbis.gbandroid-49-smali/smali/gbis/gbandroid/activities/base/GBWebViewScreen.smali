.class public abstract Lgbis/gbandroid/activities/base/GBWebViewScreen;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/GBWebViewScreen$a;
    }
.end annotation


# instance fields
.field protected webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 20
    const v0, 0x7f070096

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/GBWebViewScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    .line 21
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 22
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBWebViewScreen;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    new-instance v1, Lgbis/gbandroid/activities/base/GBWebViewScreen$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lgbis/gbandroid/activities/base/GBWebViewScreen$a;-><init>(Lgbis/gbandroid/activities/base/GBWebViewScreen;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected abstract getUrl()Ljava/lang/String;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 15
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 16
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBWebViewScreen;->a()V

    .line 17
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 31
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lgbis/gbandroid/activities/base/GBWebViewScreen;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 35
    :goto_0
    const/4 v0, 0x1

    .line 37
    :goto_1
    return v0

    .line 34
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/GBWebViewScreen;->finish()V

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
