.class final Lgbis/gbandroid/activities/base/PhotoDialog$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/base/PhotoDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/PhotoDialog;

.field private b:[B


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/base/PhotoDialog;Lgbis/gbandroid/activities/base/GBActivity;[B)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    .line 244
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 245
    iput-object p3, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->b:[B

    .line 246
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 268
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 269
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->finish()V

    .line 270
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 250
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 252
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/PhotoDialog;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->finish()V

    .line 258
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/PhotoDialog$a;->b:[B

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->queryPhotoUploaderWebService([B)V

    .line 263
    const/4 v0, 0x1

    return v0
.end method
