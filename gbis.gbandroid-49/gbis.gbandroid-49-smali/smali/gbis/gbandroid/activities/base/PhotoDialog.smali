.class public abstract Lgbis/gbandroid/activities/base/PhotoDialog;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/base/PhotoDialog$a;
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private c:Landroid/net/Uri;

.field private d:Landroid/graphics/Bitmap;

.field private e:Lgbis/gbandroid/views/CustomDialog;

.field protected latitude:D

.field protected longitude:D

.field protected station:Lgbis/gbandroid/entities/Station;

.field protected title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->a:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->b:I

    .line 35
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 143
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "temp"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    .line 144
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 145
    const-string v1, "output"

    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 146
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->startActivityForResult(Landroid/content/Intent;I)V

    .line 147
    return-void
.end method

.method private a(I)V
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 213
    if-eqz p1, :cond_0

    .line 214
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 215
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 216
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    .line 218
    :cond_0
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/PhotoDialog;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->a()V

    return-void
.end method

.method private a([B)V
    .locals 2
    .parameter

    .prologue
    .line 223
    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->showMessage(Ljava/lang/String;)V

    .line 224
    new-instance v0, Lgbis/gbandroid/activities/base/PhotoDialog$a;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/base/PhotoDialog$a;-><init>(Lgbis/gbandroid/activities/base/PhotoDialog;Lgbis/gbandroid/activities/base/GBActivity;[B)V

    .line 225
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 226
    return-void
.end method

.method private static a(Landroid/graphics/Bitmap;)[B
    .locals 3
    .parameter

    .prologue
    .line 235
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 236
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 237
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 238
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lgbis/gbandroid/activities/base/PhotoDialog;->c()Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    .line 168
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 169
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 170
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 171
    invoke-static {v0}, Lgbis/gbandroid/utils/FeaturesUtils;->rotateCameraPhoto(Ljava/io/File;)I

    move-result v1

    .line 172
    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->a(I)V

    .line 174
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 177
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->postPhotoResized()V

    .line 180
    :goto_0
    return-void

    .line 179
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->getLastTakenPicture()V

    goto :goto_0
.end method

.method private static c()Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 208
    const/4 v1, 0x4

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 209
    return-object v0
.end method


# virtual methods
.method protected abstract getIntentExtras()V
.end method

.method public getLastTakenPicture()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 184
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "/dcim/100media"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    .line 185
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 187
    const/4 v1, 0x0

    aget-object v1, v3, v1

    .line 188
    array-length v4, v3

    move v2, v0

    :goto_0
    if-lt v2, v4, :cond_2

    .line 191
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->c:Landroid/net/Uri;

    .line 192
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lgbis/gbandroid/activities/base/PhotoDialog;->c()Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    .line 193
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x5

    if-lt v0, v2, :cond_0

    .line 194
    invoke-static {v1}, Lgbis/gbandroid/utils/FeaturesUtils;->rotateCameraPhoto(Ljava/io/File;)I

    move-result v0

    .line 195
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->a(I)V

    .line 197
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 200
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->postPhotoResized()V

    .line 204
    :goto_1
    return-void

    .line 188
    :cond_2
    aget-object v0, v3, v2

    .line 189
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    .line 188
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    const v0, 0x7f09003a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const v1, 0x7f09003a

    const/4 v0, -0x1

    .line 62
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 63
    packed-switch p1, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    if-eqz p2, :cond_0

    .line 68
    if-ne p2, v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->postPhotoTaken()V

    .line 71
    :try_start_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 74
    :try_start_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->getLastTakenPicture()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 76
    :catch_1
    move-exception v0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :pswitch_1
    if-eqz p2, :cond_0

    .line 85
    if-ne p2, v0, :cond_0

    .line 87
    :try_start_2
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 88
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->uploadPicture(Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 90
    :catch_2
    move-exception v0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->getIntentExtras()V

    .line 49
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->showCameraDialog()V

    .line 50
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 100
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog;->dismiss()V

    .line 102
    :cond_0
    return-void
.end method

.method protected abstract postPhotoResized()V
.end method

.method protected abstract postPhotoTaken()V
.end method

.method protected abstract queryPhotoUploaderWebService([B)V
.end method

.method public showCameraDialog()V
    .locals 5

    .prologue
    .line 105
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030016

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 107
    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->title:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 108
    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->station:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 109
    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->station:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 112
    :goto_0
    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 113
    const v2, 0x7f070047

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 114
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const v4, 0x7f0901aa

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 115
    const v4, 0x7f0901ab

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/base/PhotoDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 116
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x7f030032

    invoke-direct {v3, p0, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 117
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    new-instance v3, Lgbis/gbandroid/activities/base/ab;

    invoke-direct {v3, p0, v2}, Lgbis/gbandroid/activities/base/ab;-><init>(Lgbis/gbandroid/activities/base/PhotoDialog;[Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 132
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    .line 133
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    new-instance v1, Lgbis/gbandroid/activities/base/ac;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/ac;-><init>(Lgbis/gbandroid/activities/base/PhotoDialog;)V

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog;->show()V

    .line 140
    return-void

    .line 111
    :cond_0
    iget-object v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    goto :goto_0
.end method

.method protected uploadPhoto()V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->e:Lgbis/gbandroid/views/CustomDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog;->cancel()V

    .line 230
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->a([B)V

    .line 231
    iget-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 232
    return-void
.end method

.method public uploadPicture(Landroid/net/Uri;)V
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 150
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "latitude"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "longitude"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "orientation"

    aput-object v1, v2, v0

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    .line 151
    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/base/PhotoDialog;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 152
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 153
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 154
    const-string v2, "latitude"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iput-wide v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->latitude:D

    .line 155
    const-string v2, "longitude"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iput-wide v2, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->longitude:D

    .line 156
    const-string v2, "orientation"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 157
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lgbis/gbandroid/activities/base/PhotoDialog;->c()Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/base/PhotoDialog;->d:Landroid/graphics/Bitmap;

    .line 158
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/base/PhotoDialog;->a(I)V

    .line 159
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 163
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;->postPhotoResized()V

    .line 164
    return-void
.end method
