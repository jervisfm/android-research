.class final Lgbis/gbandroid/activities/base/a;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivity;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/a;->a:Lgbis/gbandroid/activities/base/GBActivity;

    .line 1035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1037
    iget-object v0, p0, Lgbis/gbandroid/activities/base/a;->a:Lgbis/gbandroid/activities/base/GBActivity;

    check-cast p2, Lgbis/gbandroid/services/GPSService$GPSBinder;

    invoke-virtual {p2}, Lgbis/gbandroid/services/GPSService$GPSBinder;->getService()Lgbis/gbandroid/services/GPSService;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->a(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/services/GPSService;)V

    .line 1038
    iget-object v0, p0, Lgbis/gbandroid/activities/base/a;->a:Lgbis/gbandroid/activities/base/GBActivity;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivity;->a(Lgbis/gbandroid/activities/base/GBActivity;)Lgbis/gbandroid/services/GPSService;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/services/GPSService;->startLocationListener()V

    .line 1039
    iget-object v0, p0, Lgbis/gbandroid/activities/base/a;->a:Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->onGPSServiceConnected()V

    .line 1040
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .parameter

    .prologue
    .line 1043
    iget-object v0, p0, Lgbis/gbandroid/activities/base/a;->a:Lgbis/gbandroid/activities/base/GBActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->a(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/services/GPSService;)V

    .line 1044
    return-void
.end method
