.class final Lgbis/gbandroid/activities/base/aa;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

.field private final synthetic b:[Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBPreferenceActivity;[Ljava/lang/String;Ljava/lang/String;Landroid/app/Dialog;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/aa;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    iput-object p2, p0, Lgbis/gbandroid/activities/base/aa;->b:[Ljava/lang/String;

    iput-object p3, p0, Lgbis/gbandroid/activities/base/aa;->c:Ljava/lang/String;

    iput-object p4, p0, Lgbis/gbandroid/activities/base/aa;->d:Landroid/app/Dialog;

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 234
    .line 236
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/activities/base/aa;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    const/4 v0, 0x1

    .line 239
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/base/aa;->a:Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 240
    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lgbis/gbandroid/activities/base/aa;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/aa;->b:[Ljava/lang/String;

    aget-object v2, v2, p3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 244
    :goto_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 245
    iget-object v0, p0, Lgbis/gbandroid/activities/base/aa;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 246
    return-void

    .line 243
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/base/aa;->c:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/aa;->b:[Ljava/lang/String;

    aget-object v2, v2, p3

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method
