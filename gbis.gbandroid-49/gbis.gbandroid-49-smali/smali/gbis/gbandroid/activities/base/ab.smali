.class final Lgbis/gbandroid/activities/base/ab;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/PhotoDialog;

.field private final synthetic b:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/PhotoDialog;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/ab;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    iput-object p2, p0, Lgbis/gbandroid/activities/base/ab;->b:[Ljava/lang/String;

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/base/ab;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/ab;->b:[Ljava/lang/String;

    aget-object v1, v1, p3

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/PhotoDialog;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 121
    if-nez p3, :cond_0

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/activities/base/ab;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/PhotoDialog;->a(Lgbis/gbandroid/activities/base/PhotoDialog;)V

    .line 130
    :goto_0
    return-void

    .line 124
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 125
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    iget-object v1, p0, Lgbis/gbandroid/activities/base/ab;->a:Lgbis/gbandroid/activities/base/PhotoDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lgbis/gbandroid/activities/base/PhotoDialog;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
