.class final Lgbis/gbandroid/activities/base/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivity;

.field private final synthetic b:Landroid/widget/CheckedTextView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivity;Landroid/widget/CheckedTextView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/h;->a:Lgbis/gbandroid/activities/base/GBActivity;

    iput-object p2, p0, Lgbis/gbandroid/activities/base/h;->b:Landroid/widget/CheckedTextView;

    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 411
    check-cast p1, Landroid/widget/CheckedTextView;

    .line 412
    invoke-virtual {p1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 413
    iget-object v0, p0, Lgbis/gbandroid/activities/base/h;->b:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/h;->a:Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 415
    const-string v2, "smartPromptPreference"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 416
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 417
    return-void

    :cond_0
    move v0, v2

    .line 412
    goto :goto_0

    :cond_1
    move v1, v2

    .line 413
    goto :goto_1
.end method
