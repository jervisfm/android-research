.class final Lgbis/gbandroid/activities/base/i;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivity;

.field private final synthetic b:Lgbis/gbandroid/entities/SmartPrompt;

.field private final synthetic c:Landroid/widget/CheckedTextView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivity;Lgbis/gbandroid/entities/SmartPrompt;Landroid/widget/CheckedTextView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/i;->a:Lgbis/gbandroid/activities/base/GBActivity;

    iput-object p2, p0, Lgbis/gbandroid/activities/base/i;->b:Lgbis/gbandroid/entities/SmartPrompt;

    iput-object p3, p0, Lgbis/gbandroid/activities/base/i;->c:Landroid/widget/CheckedTextView;

    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .parameter

    .prologue
    .line 426
    iget-object v1, p0, Lgbis/gbandroid/activities/base/i;->a:Lgbis/gbandroid/activities/base/GBActivity;

    iget-object v0, p0, Lgbis/gbandroid/activities/base/i;->b:Lgbis/gbandroid/entities/SmartPrompt;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/SmartPrompt;->getPromptId()I

    move-result v2

    iget-object v0, p0, Lgbis/gbandroid/activities/base/i;->c:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p1, v2, v0}, Lgbis/gbandroid/activities/base/GBActivity;->onSmartPromptCancelled(Landroid/content/DialogInterface;IZ)V

    .line 427
    return-void

    .line 426
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
