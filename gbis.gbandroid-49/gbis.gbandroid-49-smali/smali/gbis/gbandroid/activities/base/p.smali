.class final Lgbis/gbandroid/activities/base/p;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/p;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lgbis/gbandroid/activities/base/p;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutocompletePlaces;

    .line 483
    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutocompletePlaces;->isPlace()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    const-string v0, "SavedSearch"

    .line 488
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/base/p;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setAnalyticsTrackEventAutocomplete(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lgbis/gbandroid/activities/base/p;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v0, p3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->a(Lgbis/gbandroid/activities/base/GBActivitySearch;I)V

    .line 490
    iget-object v0, p0, Lgbis/gbandroid/activities/base/p;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->resetFocus()V

    .line 491
    return-void

    .line 487
    :cond_0
    const-string v0, "CityState"

    goto :goto_0
.end method
