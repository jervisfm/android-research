.class final Lgbis/gbandroid/activities/base/v;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter

    .prologue
    .line 268
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 272
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    const-string v0, ".*\\p{Digit}.*"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 277
    iget-object v0, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->c(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    .line 278
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 280
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivitySearch$c;

    iget-object v1, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getActivity()Lgbis/gbandroid/activities/base/GBActivity;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/activities/base/GBActivitySearch$c;-><init>(Lgbis/gbandroid/activities/base/GBActivitySearch;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 281
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivitySearch$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 289
    :cond_1
    :goto_0
    return-void

    .line 283
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 285
    iget-object v0, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->setAutocompleteWithQuickLinks()V

    .line 286
    iget-object v0, p0, Lgbis/gbandroid/activities/base/v;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->d(Lgbis/gbandroid/activities/base/GBActivitySearch;)V

    goto :goto_0
.end method
