.class final Lgbis/gbandroid/activities/base/x;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/base/GBActivitySearch;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/base/GBActivitySearch;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    .line 316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/base/x;)Lgbis/gbandroid/activities/base/GBActivitySearch;
    .locals 1
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    return-object v0
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 13
    .parameter
    .parameter

    .prologue
    const v5, 0x7f07003a

    const/4 v4, 0x0

    const/4 v12, 0x0

    const/high16 v1, 0x3f80

    .line 318
    if-eqz p2, :cond_1

    .line 319
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    instance-of v0, v0, Lgbis/gbandroid/activities/search/MainScreen;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/activities/base/GBActivitySearch;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 322
    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 323
    new-instance v0, Lgbis/gbandroid/animations/MyScaleAnimation;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->e(Lgbis/gbandroid/activities/base/GBActivitySearch;)I

    move-result v5

    const/4 v7, 0x1

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/animations/MyScaleAnimation;-><init>(FFFFILandroid/view/View;Z)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 324
    invoke-virtual {v6}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/base/y;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/base/y;-><init>(Lgbis/gbandroid/activities/base/x;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v0, v0, Lgbis/gbandroid/activities/base/GBActivitySearch;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    const v3, 0x7f0901d1

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    instance-of v0, v0, Lgbis/gbandroid/activities/search/MainScreen;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/activities/base/GBActivitySearch;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 342
    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    check-cast v0, Lgbis/gbandroid/activities/search/MainScreen;

    iget-object v2, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    iget-object v2, v2, Lgbis/gbandroid/activities/base/GBActivitySearch;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->resizeElements(Landroid/content/res/Configuration;)V

    .line 344
    new-instance v5, Lgbis/gbandroid/animations/MyScaleAnimation;

    iget-object v0, p0, Lgbis/gbandroid/activities/base/x;->a:Lgbis/gbandroid/activities/base/GBActivitySearch;

    invoke-static {v0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->e(Lgbis/gbandroid/activities/base/GBActivitySearch;)I

    move-result v10

    move v6, v1

    move v7, v1

    move v8, v4

    move v9, v1

    invoke-direct/range {v5 .. v12}, Lgbis/gbandroid/animations/MyScaleAnimation;-><init>(FFFFILandroid/view/View;Z)V

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
