.class final Lgbis/gbandroid/activities/favorites/Favourites$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 897
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 898
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 899
    iput-object p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->b:Ljava/lang/String;

    .line 900
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 904
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 906
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->c(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 908
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->o(Lgbis/gbandroid/activities/favorites/Favourites;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 910
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->p(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 911
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090071

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->setMessage(Ljava/lang/String;)V

    .line 915
    :cond_0
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->q(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 916
    return-void

    .line 913
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090072

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->setMessage(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 920
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;Ljava/lang/String;)V

    .line 921
    const/4 v0, 0x1

    return v0
.end method
