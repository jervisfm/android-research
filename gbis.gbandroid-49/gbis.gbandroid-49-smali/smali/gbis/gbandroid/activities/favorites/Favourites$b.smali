.class final Lgbis/gbandroid/activities/favorites/Favourites$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 859
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 860
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 861
    iput-boolean p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->b:Z

    .line 862
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 890
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 891
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->finish()V

    .line 892
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 866
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 868
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->c(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 870
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->l(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 872
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->l(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 873
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f09006b

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    .line 874
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->m(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 875
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->b:Z

    .line 877
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-boolean v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->b:Z

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;Z)V

    .line 880
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->n(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 885
    const/4 v0, 0x1

    return v0
.end method
