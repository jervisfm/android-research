.class final Lgbis/gbandroid/activities/favorites/Favourites$c;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 927
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 928
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 929
    iput p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->b:I

    .line 930
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 934
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 935
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->o(Lgbis/gbandroid/activities/favorites/Favourites;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 937
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->r(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/Spinner;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 938
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->s(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 939
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090073

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->setMessage(Ljava/lang/String;)V

    .line 943
    :cond_0
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->q(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 944
    return-void

    .line 941
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090074

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->setMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 948
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$c;->b:I

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;I)V

    .line 949
    const/4 v0, 0x1

    return v0
.end method
