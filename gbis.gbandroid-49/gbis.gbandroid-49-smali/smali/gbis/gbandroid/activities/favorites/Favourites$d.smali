.class final Lgbis/gbandroid/activities/favorites/Favourites$d;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/FavStationMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030026

    .line 772
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 773
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 774
    iput-object p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->b:Ljava/util/List;

    .line 775
    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->c:I

    .line 776
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites$d;)Lgbis/gbandroid/activities/favorites/Favourites;
    .locals 1
    .parameter

    .prologue
    .line 769
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    return-object v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 781
    if-nez p2, :cond_1

    .line 782
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 784
    new-instance v1, Lgbis/gbandroid/activities/favorites/Favourites$k;

    invoke-direct {v1}, Lgbis/gbandroid/activities/favorites/Favourites$k;-><init>()V

    .line 785
    const v0, 0x7f0700a7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->a:Landroid/widget/TextView;

    .line 786
    const v0, 0x7f0700a8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->b:Landroid/widget/TextView;

    .line 787
    const v0, 0x7f0700a9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->c:Landroid/widget/TextView;

    .line 788
    const v0, 0x7f0700ab

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->d:Landroid/widget/ProgressBar;

    .line 789
    const v0, 0x7f0700ad

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->e:Landroid/widget/Button;

    .line 791
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 794
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$d;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationMessage;

    .line 795
    if-eqz v0, :cond_0

    .line 797
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 799
    iget-object v3, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->c:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v2

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 800
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->d:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 801
    iget-object v1, v1, Lgbis/gbandroid/activities/favorites/Favourites$k;->e:Landroid/widget/Button;

    new-instance v2, Lgbis/gbandroid/activities/favorites/o;

    invoke-direct {v2, p0, p2, v0, p1}, Lgbis/gbandroid/activities/favorites/o;-><init>(Lgbis/gbandroid/activities/favorites/Favourites$d;Landroid/view/View;Lgbis/gbandroid/entities/FavStationMessage;I)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 808
    :cond_0
    return-object p2

    .line 793
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/favorites/Favourites$k;

    move-object v1, v0

    goto :goto_0

    .line 799
    :cond_2
    const-string v2, ""

    goto :goto_1
.end method
