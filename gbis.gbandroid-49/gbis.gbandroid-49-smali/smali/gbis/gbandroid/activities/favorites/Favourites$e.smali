.class final Lgbis/gbandroid/activities/favorites/Favourites$e;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 956
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 957
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 958
    iput p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->b:I

    .line 959
    iput p4, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->c:I

    .line 960
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f0700ab

    const/4 v4, 0x4

    .line 964
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 965
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->t(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 966
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 967
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->o(Lgbis/gbandroid/activities/favorites/Favourites;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 968
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->t(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/favorites/Favourites$d;

    .line 971
    :try_start_0
    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->c:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites$d;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/FavStationMessage;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 975
    :goto_0
    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites$d;->remove(Ljava/lang/Object;)V

    .line 976
    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/Favourites$d;->notifyDataSetChanged()V

    .line 977
    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/Favourites$d;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->q(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 988
    :cond_0
    :goto_1
    return-void

    .line 973
    :catch_0
    move-exception v1

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v1}, Lgbis/gbandroid/activities/favorites/Favourites;->t(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites$d;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/FavStationMessage;

    goto :goto_0

    .line 980
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v3, 0x7f090076

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    .line 981
    if-eqz v1, :cond_0

    .line 982
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 985
    :cond_2
    if-eqz v1, :cond_0

    .line 986
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$e;->b:I

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;I)V

    .line 993
    const/4 v0, 0x1

    return v0
.end method
