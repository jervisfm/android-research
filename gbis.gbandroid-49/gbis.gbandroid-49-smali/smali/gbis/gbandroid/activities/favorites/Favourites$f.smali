.class final Lgbis/gbandroid/activities/favorites/Favourites$f;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/FavStationMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lgbis/gbandroid/activities/favorites/Favourites$h;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030027

    .line 685
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 686
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 687
    iput-object p3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->b:Ljava/util/List;

    .line 688
    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->c:I

    .line 689
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 694
    if-nez p2, :cond_1

    .line 695
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 697
    new-instance v1, Lgbis/gbandroid/activities/favorites/Favourites$j;

    invoke-direct {v1}, Lgbis/gbandroid/activities/favorites/Favourites$j;-><init>()V

    .line 698
    const v0, 0x7f0700a7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->a:Landroid/widget/TextView;

    .line 699
    const v0, 0x7f0700a8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->b:Landroid/widget/TextView;

    .line 700
    const v0, 0x7f0700a9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->c:Landroid/widget/TextView;

    .line 701
    const v0, 0x7f0700b0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->d:Landroid/widget/TextView;

    .line 702
    const v0, 0x7f0700b1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->e:Landroid/widget/TextView;

    .line 703
    const v0, 0x7f0700b3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->f:Landroid/widget/TextView;

    .line 704
    const v0, 0x7f0700b2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->h:Landroid/widget/TextView;

    .line 705
    const v0, 0x7f0700b4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->g:Landroid/widget/TextView;

    .line 706
    const v0, 0x7f0700ae

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->i:Landroid/widget/RelativeLayout;

    .line 708
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 711
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationMessage;

    .line 712
    if-eqz v0, :cond_0

    .line 713
    new-instance v2, Lgbis/gbandroid/activities/favorites/Favourites$h;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-direct {v2, v3, v0}, Lgbis/gbandroid/activities/favorites/Favourites$h;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V

    iput-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->d:Lgbis/gbandroid/activities/favorites/Favourites$h;

    .line 714
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 715
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 716
    iget-object v3, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->c:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v2

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v3}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lgbis/gbandroid/entities/FavStationMessage;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v2

    .line 718
    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_4

    .line 719
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getCountry()Ljava/lang/String;

    move-result-object v4

    const-string v5, "USA"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 720
    iget-object v4, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->d:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x2

    invoke-static {v2, v3, v6}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 721
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 726
    :goto_2
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v4, 0x7f09002e

    invoke-virtual {v3, v4}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->h:Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v4, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v4}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lgbis/gbandroid/entities/FavStationMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 728
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->h:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 735
    :goto_3
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getMslMatchStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 751
    :goto_4
    iget-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->i:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->d:Lgbis/gbandroid/activities/favorites/Favourites$h;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 753
    :cond_0
    return-object p2

    .line 710
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/favorites/Favourites$j;

    move-object v1, v0

    goto/16 :goto_0

    .line 716
    :cond_2
    const-string v2, ""

    goto/16 :goto_1

    .line 723
    :cond_3
    iget-object v4, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->d:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 724
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 730
    :cond_4
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->d:Landroid/widget/TextView;

    const-string v3, "--"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites$f;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v4, 0x7f09002d

    invoke-virtual {v3, v4}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 732
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 733
    iget-object v2, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->h:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 737
    :pswitch_0
    iget-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->g:Landroid/widget/TextView;

    const-string v2, "Matched"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 740
    :pswitch_1
    iget-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->g:Landroid/widget/TextView;

    const-string v2, "Changed"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 746
    :pswitch_2
    iget-object v0, v1, Lgbis/gbandroid/activities/favorites/Favourites$j;->g:Landroid/widget/TextView;

    const-string v2, "Not Matched"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 735
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
