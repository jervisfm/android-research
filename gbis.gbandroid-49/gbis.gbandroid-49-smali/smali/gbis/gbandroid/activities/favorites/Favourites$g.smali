.class final Lgbis/gbandroid/activities/favorites/Favourites$g;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/Favourites;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 821
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 822
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 823
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 827
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 829
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->c(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 831
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->d(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 833
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->e(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->f(Lgbis/gbandroid/activities/favorites/Favourites;)I

    move-result v0

    const/16 v1, 0x32

    if-ge v0, v1, :cond_1

    .line 835
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v1, 0x7f0700a0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 836
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v1, 0x7f0700a5

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 837
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->g(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 847
    :cond_0
    :goto_1
    return-void

    .line 840
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->h(Lgbis/gbandroid/activities/favorites/Favourites;)I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v1}, Lgbis/gbandroid/activities/favorites/Favourites;->f(Lgbis/gbandroid/activities/favorites/Favourites;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x32

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_2

    .line 841
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->i(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 842
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->h(Lgbis/gbandroid/activities/favorites/Favourites;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 843
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->j(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites$g;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->k(Lgbis/gbandroid/activities/favorites/Favourites;)V

    .line 852
    const/4 v0, 0x1

    return v0
.end method
