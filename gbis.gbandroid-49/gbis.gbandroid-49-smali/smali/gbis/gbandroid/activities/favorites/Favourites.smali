.class public Lgbis/gbandroid/activities/favorites/Favourites;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/favorites/Favourites$a;,
        Lgbis/gbandroid/activities/favorites/Favourites$b;,
        Lgbis/gbandroid/activities/favorites/Favourites$c;,
        Lgbis/gbandroid/activities/favorites/Favourites$d;,
        Lgbis/gbandroid/activities/favorites/Favourites$e;,
        Lgbis/gbandroid/activities/favorites/Favourites$f;,
        Lgbis/gbandroid/activities/favorites/Favourites$g;,
        Lgbis/gbandroid/activities/favorites/Favourites$h;,
        Lgbis/gbandroid/activities/favorites/Favourites$i;,
        Lgbis/gbandroid/activities/favorites/Favourites$j;,
        Lgbis/gbandroid/activities/favorites/Favourites$k;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:I

.field private g:Ljava/lang/Object;

.field private h:Lgbis/gbandroid/utils/CustomAsyncTask;

.field private i:Landroid/widget/ListView;

.field private j:Landroid/widget/ListView;

.field private k:Landroid/app/Dialog;

.field private l:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/widget/Spinner;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 123
    new-instance v1, Lgbis/gbandroid/activities/favorites/Favourites$i;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites$i;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;B)V

    .line 124
    const v0, 0x7f0700a3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    .line 125
    const v0, 0x7f0700a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    .line 126
    const v0, 0x7f070099

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    .line 128
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    new-instance v2, Lgbis/gbandroid/activities/favorites/a;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/favorites/a;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    new-instance v2, Lgbis/gbandroid/activities/favorites/g;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/favorites/g;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 150
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->b()V

    .line 151
    return-void
.end method

.method private a(I)V
    .locals 4
    .parameter

    .prologue
    .line 419
    new-instance v0, Lgbis/gbandroid/activities/favorites/l;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/favorites/l;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/l;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 420
    new-instance v1, Lgbis/gbandroid/queries/FavRemoveListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavRemoveListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 421
    invoke-virtual {v1, p1}, Lgbis/gbandroid/queries/FavRemoveListQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 422
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v2, 0x0

    const v8, 0x7f060001

    const-wide/16 v6, 0x0

    .line 345
    const-string v0, "sm_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 346
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 347
    :goto_0
    if-lt v1, v4, :cond_1

    .line 382
    :cond_0
    :goto_1
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    .line 383
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/favorites/Favourites$f;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/Favourites$f;->notifyDataSetChanged()V

    .line 384
    return-void

    .line 348
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationMessage;

    .line 349
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    move-result v5

    if-ne v5, v3, :cond_5

    .line 350
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    aget-object v2, v3, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 351
    const-string v1, "fuel regular"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    .line 352
    const-string v1, "fuel regular"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/FavStationMessage;->setRegPrice(D)V

    .line 353
    const-string v1, "time spotted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setRegTimeSpotted(Ljava/lang/String;)V

    .line 354
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setTimeOffset(I)V

    goto :goto_1

    .line 357
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 358
    const-string v1, "fuel midgrade"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    .line 359
    const-string v1, "fuel midgrade"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/FavStationMessage;->setMidPrice(D)V

    .line 360
    const-string v1, "time spotted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setRegTimeSpotted(Ljava/lang/String;)V

    .line 361
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setTimeOffset(I)V

    goto/16 :goto_1

    .line 364
    :cond_3
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 365
    const-string v1, "fuel premium"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    .line 366
    const-string v1, "fuel premium"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/FavStationMessage;->setPremPrice(D)V

    .line 367
    const-string v1, "time spotted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setRegTimeSpotted(Ljava/lang/String;)V

    .line 368
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setTimeOffset(I)V

    goto/16 :goto_1

    .line 371
    :cond_4
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    const-string v1, "fuel diesel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    .line 373
    const-string v1, "fuel diesel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/FavStationMessage;->setDieselPrice(D)V

    .line 374
    const-string v1, "time spotted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setRegTimeSpotted(Ljava/lang/String;)V

    .line 375
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/FavStationMessage;->setTimeOffset(I)V

    goto/16 :goto_1

    .line 347
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0
.end method

.method private a(Landroid/view/View;II)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 565
    const v0, 0x7f0700ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 566
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$e;

    invoke-direct {v0, p0, p0, p2, p3}, Lgbis/gbandroid/activities/favorites/Favourites$e;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;II)V

    .line 567
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites$e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 568
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;DDLandroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p5}, Lgbis/gbandroid/activities/favorites/Favourites;->showDirections(DDLandroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 418
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/view/View;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 563
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 658
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/entities/FavStationMessage;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/Station;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->showStationOnMap(Lgbis/gbandroid/entities/Station;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 412
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/Favourites;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 430
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Z)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 6
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    .line 228
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const-string v2, "ContextMenu"

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03000c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 230
    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 231
    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getGasBrandId()I

    move-result v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v0, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 232
    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 233
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    .line 234
    const v0, 0x7f07002d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 235
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f06000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    .line 239
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    .line 240
    :cond_0
    const/4 v2, 0x2

    const v3, 0x7f0901ba

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 242
    :cond_1
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03000d

    invoke-direct {v2, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 243
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 244
    new-instance v1, Lgbis/gbandroid/activities/favorites/h;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/favorites/h;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 281
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 282
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 413
    new-instance v0, Lgbis/gbandroid/activities/favorites/k;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/favorites/k;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/k;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 414
    new-instance v1, Lgbis/gbandroid/queries/FavAddListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavAddListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 415
    invoke-virtual {v1, p1}, Lgbis/gbandroid/queries/FavAddListQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 416
    return-void
.end method

.method private a(Z)V
    .locals 3
    .parameter

    .prologue
    .line 431
    const v0, 0x7f070099

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    .line 432
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030033

    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->g()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->l:Landroid/widget/ArrayAdapter;

    .line 433
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->l:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030032

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 434
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->l:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 435
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 436
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    new-instance v1, Lgbis/gbandroid/activities/favorites/n;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/favorites/n;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 452
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 387
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 388
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 389
    const v0, 0x7f0700a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 390
    const v0, 0x7f0700a5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 391
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->c(I)V

    .line 392
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    .line 393
    return-void
.end method

.method private b(I)V
    .locals 4
    .parameter

    .prologue
    .line 425
    new-instance v0, Lgbis/gbandroid/activities/favorites/m;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/favorites/m;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/m;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 426
    new-instance v1, Lgbis/gbandroid/queries/FavRemoveStationQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavRemoveStationQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 427
    invoke-virtual {v1, p1}, Lgbis/gbandroid/queries/FavRemoveStationQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 428
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/Favourites;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(I)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 674
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->c(Lgbis/gbandroid/entities/FavStationMessage;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/Station;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->showCameraDialog(Lgbis/gbandroid/entities/Station;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/Favourites;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 4
    .parameter

    .prologue
    .line 659
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 660
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 661
    const-string v1, "favourite id"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getMemberFavId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 662
    const-string v1, "fuel regular"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getRegPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 663
    const-string v1, "time spotted regular"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getRegTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 664
    const-string v1, "fuel midgrade"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getMidPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 665
    const-string v1, "time spotted midgrade"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getMidTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 666
    const-string v1, "fuel premium"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getPremPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 667
    const-string v1, "time spotted premium"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getPremTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 668
    const-string v1, "fuel diesel"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getDieselPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 669
    const-string v1, "time spotted diesel"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getDieselTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 670
    const-string v1, "time offset"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getTimeOffset()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 671
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->startActivityForResult(Landroid/content/Intent;I)V

    .line 672
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 551
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$a;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites$a;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 552
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 553
    const v0, 0x7f090092

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 555
    return-void
.end method

.method private b(Z)V
    .locals 2
    .parameter

    .prologue
    .line 539
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$b;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites$b;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;Z)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 540
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 541
    const v0, 0x7f090092

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 542
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 397
    new-instance v0, Lgbis/gbandroid/activities/favorites/i;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/favorites/i;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/i;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 398
    new-instance v1, Lgbis/gbandroid/queries/FavListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 399
    invoke-virtual {v1}, Lgbis/gbandroid/queries/FavListQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 400
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    .line 401
    return-void
.end method

.method private c(I)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0xc

    .line 514
    const v0, 0x7f0700a6

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 515
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 516
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 517
    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 518
    const/4 v2, 0x2

    const v3, 0x7f0700a5

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 522
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 524
    return-void

    .line 520
    :cond_1
    if-nez p1, :cond_0

    .line 521
    const/4 v2, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/favorites/Favourites;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 81
    iput p1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 227
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/entities/FavStationMessage;)V

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/favorites/Favourites;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Ljava/lang/String;)V

    return-void
.end method

.method private c(Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 675
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/StationDetails;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 676
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 677
    const-string v1, "favourite id"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/FavStationMessage;->getMemberFavId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 678
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->startActivityForResult(Landroid/content/Intent;I)V

    .line 679
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 404
    new-instance v0, Lgbis/gbandroid/activities/favorites/j;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/favorites/j;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/j;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 405
    new-instance v1, Lgbis/gbandroid/queries/FavStationListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavStationListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 406
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListId()I

    move-result v0

    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    const/16 v3, 0x32

    invoke-virtual {v1, v0, v2, v3}, Lgbis/gbandroid/queries/FavStationListQuery;->getResponseObject(III)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 407
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationResults;

    .line 408
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationResults;->getFavStationMessage()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    .line 409
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationResults;->getTotalStations()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    .line 410
    return-void
.end method

.method private d(I)V
    .locals 2
    .parameter

    .prologue
    .line 558
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$c;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites$c;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 559
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 560
    const v0, 0x7f090093

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 561
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 468
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->f()V

    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/favorites/Favourites;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 83
    iput p1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->g:Ljava/lang/Object;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const v2, 0x7f07009c

    .line 455
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    const v1, 0x7f0901c9

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901cd

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    const v1, 0x7f0901ca

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 459
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901ce

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 461
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    const v1, 0x7f0901cb

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 462
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901cf

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 464
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    const v1, 0x7f0901cc

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901d0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/favorites/Favourites;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 557
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/favorites/Favourites;->d(I)V

    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/favorites/Favourites;)I
    .locals 1
    .parameter

    .prologue
    .line 82
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    return v0
.end method

.method private f()V
    .locals 7

    .prologue
    const v3, 0x7f07009a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 469
    new-instance v1, Lgbis/gbandroid/activities/favorites/Favourites$f;

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    invoke-direct {v1, p0, p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites$f;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/content/Context;Ljava/util/List;)V

    .line 470
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->e()V

    .line 471
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    .line 472
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 473
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    new-instance v1, Lgbis/gbandroid/activities/favorites/b;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/favorites/b;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 484
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    new-instance v1, Lgbis/gbandroid/activities/favorites/c;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/favorites/c;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 492
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    if-eqz v0, :cond_2

    .line 494
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    .line 495
    const v0, 0x7f0700a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 496
    const v0, 0x7f0700a5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 497
    invoke-direct {p0, v6}, Lgbis/gbandroid/activities/favorites/Favourites;->c(I)V

    .line 500
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x32

    add-int/lit8 v2, v0, 0x1

    .line 501
    add-int/lit8 v0, v2, 0x32

    iget v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    if-lt v0, v1, :cond_1

    .line 502
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    move v1, v0

    .line 505
    :goto_0
    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 506
    const v3, 0x7f09006e

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    const/4 v1, 0x2

    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 511
    return-void

    .line 504
    :cond_1
    add-int/lit8 v0, v2, 0x32

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_0

    .line 508
    :cond_2
    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09006f

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic g(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 1
    .parameter

    .prologue
    .line 513
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->c(I)V

    return-void
.end method

.method private g()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 527
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 528
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 529
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 531
    return-object v2

    .line 530
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 529
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/favorites/Favourites;)I
    .locals 1
    .parameter

    .prologue
    .line 81
    iget v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Z)V

    .line 536
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 545
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$g;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/favorites/Favourites$g;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 546
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 547
    const v0, 0x7f09008a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 548
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    return-object v0
.end method

.method private j()V
    .locals 7

    .prologue
    .line 571
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 572
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 573
    const v0, 0x7f07003f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 574
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    .line 575
    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    const/16 v6, 0x1e

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v3, v4

    .line 574
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 577
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 578
    const v3, 0x7f09011a

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 579
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 580
    const v2, 0x7f090175

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgbis/gbandroid/activities/favorites/d;

    invoke-direct {v3, p0, v0}, Lgbis/gbandroid/activities/favorites/d;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 592
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    .line 593
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 594
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    .line 597
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 598
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030014

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 599
    const v0, 0x7f070047

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 600
    const v3, 0x7f09011b

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 601
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 602
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 603
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030032

    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->g()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 604
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 605
    const v2, 0x7f090179

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgbis/gbandroid/activities/favorites/e;

    invoke-direct {v3, p0, v0}, Lgbis/gbandroid/activities/favorites/e;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/widget/ListView;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 615
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    .line 616
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 617
    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 403
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->d()V

    return-void
.end method

.method static synthetic l(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 620
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 621
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030014

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 622
    const v0, 0x7f070047

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->i:Landroid/widget/ListView;

    .line 623
    const v0, 0x7f09011c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 624
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 625
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->i:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 626
    new-instance v0, Lgbis/gbandroid/activities/favorites/Favourites$d;

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    invoke-direct {v0, p0, p0, v2}, Lgbis/gbandroid/activities/favorites/Favourites$d;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/content/Context;Ljava/util/List;)V

    .line 627
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->i:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 628
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    .line 629
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 630
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/favorites/f;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/favorites/f;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 636
    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 639
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    .line 640
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 641
    new-instance v1, Lgbis/gbandroid/activities/favorites/Favourites$f;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, p0, p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites$f;-><init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/content/Context;Ljava/util/List;)V

    .line 642
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030042

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 643
    const v0, 0x7f0700ee

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f09006c

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 644
    const v0, 0x7f0700ef

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f09006d

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 646
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 648
    :cond_0
    return-void
.end method

.method static synthetic m(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 638
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->m()V

    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 654
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->a:Ljava/lang/String;

    .line 656
    return-void
.end method

.method static synthetic n(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 396
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->c()V

    return-void
.end method

.method static synthetic o(Lgbis/gbandroid/activities/favorites/Favourites;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method static synthetic p(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 1
    .parameter

    .prologue
    .line 538
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Z)V

    return-void
.end method

.method static synthetic q(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->k:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic r(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/Spinner;
    .locals 1
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->m:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic s(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 534
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->h()V

    return-void
.end method

.method static synthetic t(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->i:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic u(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->j:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic v(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 544
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->i()V

    return-void
.end method

.method static synthetic w(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/location/Location;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 619
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->l()V

    return-void
.end method

.method static synthetic y(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 386
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->b()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 286
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 287
    packed-switch p1, :pswitch_data_0

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 289
    :pswitch_0
    if-eqz p2, :cond_0

    .line 292
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 294
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 295
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->finish()V

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 305
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 306
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adFavorites"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 307
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adFavoritesKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 308
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adFavoritesUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 309
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v6, :cond_1

    .line 310
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 311
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->showAds()V

    .line 316
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 317
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 318
    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    .line 320
    const v3, 0x7f030025

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/Favourites;->setContentView(I)V

    .line 321
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->a()V

    .line 322
    iput v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    .line 324
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->n:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 329
    invoke-direct {p0, v5}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Z)V

    .line 330
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    instance-of v0, v0, Lgbis/gbandroid/activities/favorites/Favourites$g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/CustomAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 331
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->f()V

    .line 335
    :cond_0
    :goto_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_3

    .line 336
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->showAds()V

    .line 342
    :goto_2
    return-void

    .line 313
    :cond_1
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 314
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->hideAds()V

    goto :goto_0

    .line 333
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    instance-of v0, v0, Lgbis/gbandroid/activities/favorites/Favourites$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->h:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/CustomAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    .line 334
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->m()V

    goto :goto_1

    .line 338
    :cond_3
    const v0, 0x7f0700a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 339
    invoke-direct {p0, v6}, Lgbis/gbandroid/activities/favorites/Favourites;->c(I)V

    goto :goto_2

    .line 341
    :cond_4
    invoke-direct {p0, v5}, Lgbis/gbandroid/activities/favorites/Favourites;->c(I)V

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 96
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->g:Ljava/lang/Object;

    .line 98
    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->setContentView(I)V

    .line 99
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->a()V

    .line 100
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->n()V

    .line 101
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->g:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->g:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "listPosition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    .line 103
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->g:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    .line 106
    :goto_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->h()V

    .line 107
    return-void

    .line 105
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 163
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 164
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 165
    const v1, 0x7f0b0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 166
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 120
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 187
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 188
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 202
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 190
    :pswitch_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->j()V

    goto :goto_0

    .line 193
    :pswitch_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->k()V

    goto :goto_0

    .line 196
    :pswitch_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->l()V

    goto :goto_0

    .line 199
    :pswitch_3
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->launchSettings()V

    goto :goto_0

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x7f070188
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 171
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 172
    :cond_0
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 173
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 178
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 179
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 182
    :goto_1
    return v1

    .line 175
    :cond_2
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 176
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 181
    :cond_3
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onResume()V

    .line 112
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->n()V

    .line 113
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 114
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    const-string v1, "listPosition"

    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 157
    const-string v1, "page"

    iget v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 158
    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1023
    if-nez p2, :cond_0

    .line 1024
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1026
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->showAds()V

    .line 1029
    :goto_0
    return-void

    .line 1028
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->hideAds()V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1033
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1045
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->n()V

    .line 1046
    const-string v0, "fuelPreference"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1047
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1048
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->i()V

    .line 1052
    :cond_0
    :goto_0
    return-void

    .line 1049
    :cond_1
    const-string v0, "member_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1050
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/Favourites;->finish()V

    goto :goto_0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 5

    .prologue
    .line 1037
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adFavorites"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1038
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adFavoritesKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1039
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/Favourites;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adFavoritesUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1040
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1041
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1056
    const v0, 0x7f090243

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
