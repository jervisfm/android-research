.class final Lgbis/gbandroid/activities/favorites/PlacesList$a;
.super Landroid/widget/CursorAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/favorites/PlacesList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/PlacesList;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 226
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    .line 227
    const/4 v0, 0x1

    invoke-direct {p0, p2, p3, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 228
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList$a;)Lgbis/gbandroid/activities/favorites/PlacesList;
    .locals 1
    .parameter

    .prologue
    .line 224
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v1, p1}, Lgbis/gbandroid/activities/favorites/PlacesList;->a(Lgbis/gbandroid/activities/favorites/PlacesList;I)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->a(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/database/Cursor;)V

    .line 284
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->d(Lgbis/gbandroid/activities/favorites/PlacesList;)Lgbis/gbandroid/activities/favorites/PlacesList$a;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->c(Lgbis/gbandroid/activities/favorites/PlacesList;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList$a;->changeCursor(Landroid/database/Cursor;)V

    .line 285
    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const v8, 0x7f0900b6

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;

    .line 249
    const-string v1, "table_column_name"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 250
    const-string v2, "table_column_city_zip"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 251
    const-string v3, "_id"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 252
    iget-object v5, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->a:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    if-eqz v2, :cond_0

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 254
    :cond_0
    iget-object v2, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->b:Landroid/widget/TextView;

    iget-object v5, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-virtual {v5, v8}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    const v8, 0x7f0900b7

    invoke-virtual {v7, v8}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    :goto_0
    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v2}, Lgbis/gbandroid/activities/favorites/PlacesList;->b(Lgbis/gbandroid/activities/favorites/PlacesList;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 258
    iget-object v2, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 259
    iget-object v2, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->e:Landroid/widget/Button;

    new-instance v5, Lgbis/gbandroid/activities/favorites/u;

    invoke-direct {v5, p0, v1, v3, v4}, Lgbis/gbandroid/activities/favorites/u;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList$a;Ljava/lang/String;J)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v0, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->d:Landroid/widget/Button;

    new-instance v2, Lgbis/gbandroid/activities/favorites/v;

    invoke-direct {v2, p0, v1}, Lgbis/gbandroid/activities/favorites/v;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList$a;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    :goto_1
    return-void

    .line 256
    :cond_1
    iget-object v5, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->b:Landroid/widget/TextView;

    iget-object v6, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-virtual {v6, v8}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 277
    :cond_2
    iget-object v0, v0, Lgbis/gbandroid/activities/favorites/PlacesList$b;->c:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->a(Lgbis/gbandroid/activities/favorites/PlacesList;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 235
    new-instance v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;

    invoke-direct {v2}, Lgbis/gbandroid/activities/favorites/PlacesList$b;-><init>()V

    .line 236
    const v0, 0x7f0700e4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;->a:Landroid/widget/TextView;

    .line 237
    const v0, 0x7f0700e5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;->b:Landroid/widget/TextView;

    .line 238
    const v0, 0x7f0700e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;->d:Landroid/widget/Button;

    .line 239
    const v0, 0x7f0700e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;->e:Landroid/widget/Button;

    .line 240
    const v0, 0x7f0700e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v2, Lgbis/gbandroid/activities/favorites/PlacesList$b;->c:Landroid/widget/RelativeLayout;

    .line 242
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 243
    return-object v1
.end method
