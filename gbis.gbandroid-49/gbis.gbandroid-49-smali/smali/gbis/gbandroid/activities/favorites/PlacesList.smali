.class public Lgbis/gbandroid/activities/favorites/PlacesList;
.super Lgbis/gbandroid/activities/base/GBActivitySearch;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/favorites/PlacesList$a;,
        Lgbis/gbandroid/activities/favorites/PlacesList$b;
    }
.end annotation


# instance fields
.field private b:Landroid/database/Cursor;

.field private c:Landroid/widget/ListView;

.field private d:Z

.field private e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;I)Landroid/database/Cursor;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/PlacesList;->getAllPlaces(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 116
    const v0, 0x7f0700d3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090120

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    const v0, 0x7f0700d4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lgbis/gbandroid/activities/favorites/p;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/favorites/p;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/database/Cursor;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;Ljava/lang/String;J)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/favorites/PlacesList;->a(Ljava/lang/String;J)V

    return-void
.end method

.method private a(Ljava/lang/String;J)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 173
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v2, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 174
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 175
    const v0, 0x7f07003e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 176
    const v1, 0x7f07003f

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 177
    const v4, 0x7f090120

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 178
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 179
    const v3, 0x7f09013d

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 181
    const v0, 0x7f090162

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lgbis/gbandroid/activities/favorites/r;

    invoke-direct {v3, p0, v1, p2, p3}, Lgbis/gbandroid/activities/favorites/r;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/widget/EditText;J)V

    invoke-virtual {v2, v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 195
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 197
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;JLjava/lang/String;)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lgbis/gbandroid/activities/favorites/PlacesList;->updatePlaceName(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/favorites/PlacesList;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/PlacesList;->deletePlace(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 126
    const v0, 0x7f0700d5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    new-instance v1, Lgbis/gbandroid/activities/favorites/q;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/favorites/q;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 143
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/database/Cursor;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/favorites/PlacesList;->searchUsingPlaces(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/favorites/PlacesList;)Z
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->d:Z

    return v0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/favorites/PlacesList;)Landroid/database/Cursor;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->getAllPlaces(I)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    .line 147
    new-instance v0, Lgbis/gbandroid/activities/favorites/PlacesList$a;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList$a;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    .line 148
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->d()V

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/favorites/PlacesList;)Lgbis/gbandroid/activities/favorites/PlacesList$a;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030042

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 157
    const v0, 0x7f0700ee

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0900b9

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 158
    const v0, 0x7f0700ef

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0900b8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 159
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->c:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 161
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const v2, 0x7f0700d4

    const/4 v1, 0x0

    .line 164
    iget-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->d:Z

    .line 165
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/PlacesList$a;->notifyDataSetChanged()V

    .line 166
    iget-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->d:Z

    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/PlacesList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 170
    :goto_1
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 169
    :cond_1
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/favorites/PlacesList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic e(Lgbis/gbandroid/activities/favorites/PlacesList;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->c()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 200
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 201
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/favorites/s;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/favorites/s;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 212
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/favorites/t;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/favorites/t;-><init>(Lgbis/gbandroid/activities/favorites/PlacesList;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 218
    const v1, 0x7f090123

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 219
    const v1, 0x7f09013e

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 220
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 222
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/favorites/PlacesList;)V
    .locals 0
    .parameter

    .prologue
    .line 163
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->e()V

    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/favorites/PlacesList;)Z
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->f:Z

    return v0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/favorites/PlacesList;)Z
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->deleteAllPlaces()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->setContentView(I)V

    .line 40
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    const-string v1, "WidgetCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->f:Z

    .line 44
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->a()V

    .line 45
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->b()V

    .line 46
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 62
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 63
    const v1, 0x7f0b0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 103
    packed-switch p1, :pswitch_data_0

    .line 111
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 105
    :pswitch_0
    iget-boolean v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->d:Z

    if-eqz v0, :cond_0

    .line 106
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->e()V

    .line 109
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->finish()V

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter

    .prologue
    const v3, 0x7f090165

    const/4 v0, 0x1

    .line 77
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 78
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 98
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 80
    :pswitch_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a(I)V

    .line 82
    const v1, 0x7f090164

    invoke-interface {p1, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 84
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->e:Lgbis/gbandroid/activities/favorites/PlacesList$a;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/PlacesList$a;->a(I)V

    .line 85
    invoke-interface {p1, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 89
    :pswitch_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->launchSettings()V

    goto :goto_0

    .line 92
    :pswitch_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->e()V

    goto :goto_0

    .line 95
    :pswitch_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->f()V

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x7f070198
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 70
    const/4 v0, 0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onResume()V

    .line 51
    invoke-direct {p0}, Lgbis/gbandroid/activities/favorites/PlacesList;->c()V

    .line 52
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onStop()V

    .line 57
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/PlacesList;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 58
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 300
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    const v0, 0x7f090253

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
