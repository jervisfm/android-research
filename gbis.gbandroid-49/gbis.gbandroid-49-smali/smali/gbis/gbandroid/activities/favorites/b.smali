.class final Lgbis/gbandroid/activities/favorites/b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 477
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationMessage;

    .line 478
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    move-result v1

    if-lez v1, :cond_0

    .line 479
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V

    .line 482
    :goto_0
    return-void

    .line 481
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/b;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090077

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
