.class final Lgbis/gbandroid/activities/favorites/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private final synthetic b:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Landroid/widget/ListView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iput-object p2, p0, Lgbis/gbandroid/activities/favorites/e;->b:Landroid/widget/ListView;

    .line 605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 608
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/e;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 609
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->l(Lgbis/gbandroid/activities/favorites/Favourites;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lgbis/gbandroid/activities/favorites/e;->b:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListId()I

    move-result v0

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/favorites/Favourites;->e(Lgbis/gbandroid/activities/favorites/Favourites;I)V

    .line 610
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 613
    :goto_0
    return-void

    .line 612
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/e;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    const v2, 0x7f090075

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
