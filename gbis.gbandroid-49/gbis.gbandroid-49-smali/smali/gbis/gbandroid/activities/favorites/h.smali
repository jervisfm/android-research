.class final Lgbis/gbandroid/activities/favorites/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/Favourites;

.field private final synthetic b:Lgbis/gbandroid/entities/FavStationMessage;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iput-object p2, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const-wide/16 v3, 0x0

    const v2, 0x7f090077

    .line 246
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->q(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 247
    packed-switch p3, :pswitch_data_0

    .line 279
    :goto_0
    return-void

    .line 249
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V

    goto :goto_0

    .line 252
    :pswitch_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    move-result v0

    if-lez v0, :cond_0

    .line 253
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/FavStationMessage;)V

    goto :goto_0

    .line 255
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :pswitch_2
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    move-result v0

    if-lez v0, :cond_2

    .line 259
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->w(Lgbis/gbandroid/activities/favorites/Favourites;)Landroid/location/Location;

    move-result-object v5

    .line 260
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v3

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v3

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/FavStationMessage;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/FavStationMessage;->getLongitude()D

    move-result-wide v3

    invoke-static/range {v0 .. v5}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;DDLandroid/location/Location;)V

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->a(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/Station;)V

    goto :goto_0

    .line 267
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :pswitch_3
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationMessage;->getStationId()I

    move-result v0

    if-lez v0, :cond_3

    .line 271
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->b:Lgbis/gbandroid/entities/FavStationMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->b(Lgbis/gbandroid/activities/favorites/Favourites;Lgbis/gbandroid/entities/Station;)V

    goto :goto_0

    .line 273
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/Favourites;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/Favourites;->showMessage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 276
    :pswitch_4
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/h;->a:Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/Favourites;->x(Lgbis/gbandroid/activities/favorites/Favourites;)V

    goto/16 :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
