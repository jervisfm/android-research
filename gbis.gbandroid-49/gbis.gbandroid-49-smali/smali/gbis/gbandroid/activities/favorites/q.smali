.class final Lgbis/gbandroid/activities/favorites/q;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/PlacesList;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/favorites/PlacesList;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->d(Lgbis/gbandroid/activities/favorites/PlacesList;)Lgbis/gbandroid/activities/favorites/PlacesList$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lgbis/gbandroid/activities/favorites/PlacesList$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 130
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->g(Lgbis/gbandroid/activities/favorites/PlacesList;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->b(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/database/Cursor;)V

    .line 141
    :goto_0
    return-void

    .line 133
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 134
    const-string v2, "center latitude"

    const-string v3, "table_column_latitude"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 135
    const-string v2, "center longitude"

    const-string v3, "table_column_longitude"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 136
    const-string v2, "city"

    const-string v3, "table_column_city_zip"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v2, "place name"

    const-string v3, "table_column_name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->setResult(ILandroid/content/Intent;)V

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/q;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->finish()V

    goto :goto_0
.end method
