.class final Lgbis/gbandroid/activities/favorites/r;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/favorites/PlacesList;

.field private final synthetic b:Landroid/widget/EditText;

.field private final synthetic c:J


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/favorites/PlacesList;Landroid/widget/EditText;J)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    iput-object p2, p0, Lgbis/gbandroid/activities/favorites/r;->b:Landroid/widget/EditText;

    iput-wide p3, p0, Lgbis/gbandroid/activities/favorites/r;->c:J

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/r;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    iget-wide v2, p0, Lgbis/gbandroid/activities/favorites/r;->c:J

    invoke-static {v1, v2, v3, v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->a(Lgbis/gbandroid/activities/favorites/PlacesList;JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-static {v0}, Lgbis/gbandroid/activities/favorites/PlacesList;->c(Lgbis/gbandroid/activities/favorites/PlacesList;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    .line 188
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 193
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    const v2, 0x7f09009d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    iget-object v1, p0, Lgbis/gbandroid/activities/favorites/r;->a:Lgbis/gbandroid/activities/favorites/PlacesList;

    const v2, 0x7f09009c

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/favorites/PlacesList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/favorites/PlacesList;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
