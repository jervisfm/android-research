.class public Lgbis/gbandroid/activities/initial/InitialScreen;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/initial/InitialScreen$a;,
        Lgbis/gbandroid/activities/initial/InitialScreen$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:I

.field private d:I

.field private e:Lgbis/gbandroid/activities/initial/InitialScreen$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/initial/InitialScreen;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->a:Landroid/widget/Button;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 65
    const v0, 0x7f0700c0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->a:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0700c1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->b:Landroid/widget/Button;

    .line 67
    new-instance v0, Lgbis/gbandroid/activities/initial/InitialScreen$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/initial/InitialScreen$a;-><init>(Lgbis/gbandroid/activities/initial/InitialScreen;B)V

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-direct {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->b()V

    .line 71
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/initial/InitialScreen;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/initial/InitialScreen;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const v2, 0x7f07003a

    .line 75
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/initial/InitialScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 76
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->d:I

    if-nez v1, :cond_0

    .line 77
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->c:I

    mul-int/lit8 v1, v1, 0x77

    div-int/lit16 v1, v1, 0x2f8

    .line 80
    :goto_0
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 81
    mul-int/lit16 v1, v1, 0x1e0

    div-int/lit8 v1, v1, 0x77

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 82
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/initial/InitialScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 83
    return-void

    .line 79
    :cond_0
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->c:I

    mul-int/lit8 v1, v1, 0x77

    div-int/lit16 v1, v1, 0x320

    goto :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/initial/InitialScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->showLogin()V

    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 92
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 94
    const-string v2, "initial_screen_date"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 95
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 96
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/initial/InitialScreen;)V
    .locals 1
    .parameter

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreen;->showRegistration(Z)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 31
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    new-instance v0, Lgbis/gbandroid/activities/initial/InitialScreen$b;

    invoke-direct {v0, p0, v3}, Lgbis/gbandroid/activities/initial/InitialScreen$b;-><init>(Lgbis/gbandroid/activities/initial/InitialScreen;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->e:Lgbis/gbandroid/activities/initial/InitialScreen$b;

    .line 33
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 34
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 35
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 36
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->c:I

    .line 37
    iput v3, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->d:I

    .line 45
    :goto_0
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreen;->setContentView(I)V

    .line 46
    invoke-direct {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->a()V

    .line 47
    invoke-direct {p0}, Lgbis/gbandroid/activities/initial/InitialScreen;->c()V

    .line 48
    return-void

    .line 41
    :cond_0
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->c:I

    .line 42
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->d:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 60
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->e:Lgbis/gbandroid/activities/initial/InitialScreen$b;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onStart()V

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreen;->e:Lgbis/gbandroid/activities/initial/InitialScreen$b;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 54
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    const v0, 0x7f09022c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
