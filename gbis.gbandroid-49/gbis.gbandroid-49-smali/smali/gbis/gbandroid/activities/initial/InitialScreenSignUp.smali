.class public Lgbis/gbandroid/activities/initial/InitialScreenSignUp;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/initial/InitialScreenSignUp$a;,
        Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:I

.field private d:I

.field private e:Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/initial/InitialScreenSignUp;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->a:Landroid/widget/Button;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 61
    const v0, 0x7f0700c1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->b:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0700c4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->a:Landroid/widget/Button;

    .line 63
    new-instance v0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp$a;-><init>(Lgbis/gbandroid/activities/initial/InitialScreenSignUp;B)V

    .line 64
    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-direct {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->b()V

    .line 67
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/initial/InitialScreenSignUp;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const v2, 0x7f07003a

    .line 71
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 72
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->d:I

    if-nez v1, :cond_0

    .line 73
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->c:I

    mul-int/lit8 v1, v1, 0x77

    div-int/lit16 v1, v1, 0x2f8

    .line 76
    :goto_0
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 77
    mul-int/lit16 v1, v1, 0x1e0

    div-int/lit8 v1, v1, 0x77

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 78
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    return-void

    .line 75
    :cond_0
    iget v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->c:I

    mul-int/lit8 v1, v1, 0x77

    div-int/lit16 v1, v1, 0x320

    goto :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/initial/InitialScreenSignUp;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->showRegistration()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    new-instance v0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;

    invoke-direct {v0, p0, v3}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;-><init>(Lgbis/gbandroid/activities/initial/InitialScreenSignUp;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->e:Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;

    .line 30
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 31
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 32
    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 33
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->c:I

    .line 34
    iput v3, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->d:I

    .line 42
    :goto_0
    const v0, 0x7f030036

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->setContentView(I)V

    .line 43
    invoke-direct {p0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->a()V

    .line 44
    return-void

    .line 38
    :cond_0
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->c:I

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->d:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 56
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->e:Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onStart()V

    .line 49
    iget-object v0, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->e:Lgbis/gbandroid/activities/initial/InitialScreenSignUp$b;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 50
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    const v0, 0x7f09022b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/initial/InitialScreenSignUp;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
