.class final Lgbis/gbandroid/activities/list/FilterStations$b;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/FilterStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;)V
    .locals 1
    .parameter

    .prologue
    .line 354
    iput-object p1, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->a:Lgbis/gbandroid/activities/list/FilterStations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->b:Ljava/util/List;

    .line 356
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->c:Ljava/util/List;

    .line 357
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 366
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v3

    .line 367
    :goto_0
    if-lt v2, v4, :cond_0

    .line 378
    return-void

    .line 368
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 369
    iget-object v1, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 370
    if-ne v0, p1, :cond_1

    .line 371
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 372
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 367
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 374
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 375
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 359
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$b;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_0
    return-void
.end method
