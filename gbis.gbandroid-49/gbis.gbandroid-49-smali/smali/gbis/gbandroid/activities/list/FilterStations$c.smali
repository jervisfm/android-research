.class final Lgbis/gbandroid/activities/list/FilterStations$c;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/FilterStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/StationNameFilter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f03002b

    .line 384
    iput-object p1, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->a:Lgbis/gbandroid/activities/list/FilterStations;

    .line 385
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 386
    iput-object p3, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->b:Ljava/util/List;

    .line 387
    iput v0, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->c:I

    .line 388
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 392
    if-nez p2, :cond_1

    .line 393
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->a:Lgbis/gbandroid/activities/list/FilterStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(Lgbis/gbandroid/activities/list/FilterStations;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 395
    new-instance v1, Lgbis/gbandroid/activities/list/FilterStations$d;

    invoke-direct {v1}, Lgbis/gbandroid/activities/list/FilterStations$d;-><init>()V

    .line 396
    const v0, 0x7f07002c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/FilterStations$d;->a:Landroid/widget/CheckedTextView;

    .line 397
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 400
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations$c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationNameFilter;

    .line 401
    if-eqz v0, :cond_0

    .line 402
    iget-object v2, v1, Lgbis/gbandroid/activities/list/FilterStations$d;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v1, v1, Lgbis/gbandroid/activities/list/FilterStations$d;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->isIncluded()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 405
    :cond_0
    return-object p2

    .line 399
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/list/FilterStations$d;

    move-object v1, v0

    goto :goto_0
.end method
