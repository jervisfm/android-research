.class public Lgbis/gbandroid/activities/list/FilterStations;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/list/FilterStations$a;,
        Lgbis/gbandroid/activities/list/FilterStations$b;,
        Lgbis/gbandroid/activities/list/FilterStations$c;,
        Lgbis/gbandroid/activities/list/FilterStations$d;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;"
        }
    .end annotation
.end field

.field private b:D

.field private c:D

.field private d:D

.field private e:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/FilterStations;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a(Landroid/widget/SeekBar;Landroid/widget/TextView;DDDIILjava/lang/String;ILgbis/gbandroid/entities/ListStationTab;)V
    .locals 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    sub-double v2, p5, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 253
    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 254
    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 255
    invoke-virtual/range {p13 .. p13}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v5

    .line 256
    if-nez p12, :cond_1

    .line 257
    invoke-virtual {v5}, Lgbis/gbandroid/entities/ListStationFilter;->getDistance()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-nez v2, :cond_0

    .line 258
    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Lgbis/gbandroid/entities/ListStationFilter;->setDistance(D)V

    .line 267
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    int-to-double v3, v3

    add-double v3, v3, p3

    move/from16 v0, p9

    int-to-double v6, v0

    div-double/2addr v3, v6

    move/from16 v0, p10

    invoke-static {v3, v4, v0}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    new-instance v2, Lgbis/gbandroid/activities/list/a;

    move-object v3, p0

    move/from16 v4, p12

    move-wide/from16 v6, p3

    move-wide/from16 v8, p7

    move/from16 v10, p9

    move-object/from16 v11, p2

    move/from16 v12, p10

    move-object/from16 v13, p11

    invoke-direct/range {v2 .. v13}, Lgbis/gbandroid/activities/list/a;-><init>(Lgbis/gbandroid/activities/list/FilterStations;ILgbis/gbandroid/entities/ListStationFilter;DDILandroid/widget/TextView;ILjava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 284
    return-void

    .line 260
    :cond_0
    invoke-virtual {v5}, Lgbis/gbandroid/entities/ListStationFilter;->getDistance()D

    move-result-wide v2

    mul-double v2, v2, p7

    move/from16 v0, p9

    int-to-double v6, v0

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    sub-double v2, v2, p3

    double-to-int v2, v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 262
    :cond_1
    invoke-virtual {v5}, Lgbis/gbandroid/entities/ListStationFilter;->getPrice()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-nez v2, :cond_2

    .line 263
    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Lgbis/gbandroid/entities/ListStationFilter;->setPrice(D)V

    goto :goto_0

    .line 265
    :cond_2
    invoke-virtual {v5}, Lgbis/gbandroid/entities/ListStationFilter;->getPrice()D

    move-result-wide v2

    mul-double v2, v2, p7

    move/from16 v0, p9

    int-to-double v6, v0

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    sub-double v2, v2, p3

    double-to-int v2, v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/SeekBar;Landroid/widget/TextView;ILgbis/gbandroid/entities/ListStationTab;)V
    .locals 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 210
    invoke-virtual/range {p4 .. p4}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v0

    .line 211
    packed-switch p3, :pswitch_data_0

    .line 248
    :goto_0
    return-void

    .line 213
    :pswitch_0
    iget-object v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "distancePreference"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "miles"

    .line 220
    :goto_1
    const-string v1, "miles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 221
    const-wide/high16 v1, 0x3ff0

    .line 224
    :goto_2
    iget-wide v3, p0, Lgbis/gbandroid/activities/list/FilterStations;->b:D

    mul-double/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    .line 225
    iget-wide v3, p0, Lgbis/gbandroid/activities/list/FilterStations;->c:D

    mul-double/2addr v3, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    .line 226
    const/4 v9, 0x1

    .line 227
    const/4 v10, 0x0

    move-wide v7, v1

    move-object v11, v0

    :goto_3
    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move/from16 v12, p3

    move-object/from16 v13, p4

    .line 247
    invoke-direct/range {v0 .. v13}, Lgbis/gbandroid/activities/list/FilterStations;->a(Landroid/widget/SeekBar;Landroid/widget/TextView;DDDIILjava/lang/String;ILgbis/gbandroid/entities/ListStationTab;)V

    goto :goto_0

    .line 217
    :cond_0
    const-string v0, "km"

    goto :goto_1

    .line 219
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "distancePreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 223
    :cond_2
    const-wide v1, 0x3ff9bfdb4cc25072L

    goto :goto_2

    .line 230
    :pswitch_1
    const-wide/high16 v7, 0x3ff0

    .line 231
    const-string v11, ""

    .line 232
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    const/16 v9, 0x64

    .line 234
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    const-wide/high16 v2, 0x4059

    mul-double v5, v0, v2

    .line 235
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    const-wide/high16 v2, 0x4059

    mul-double v3, v0, v2

    .line 236
    const/4 v10, 0x2

    goto :goto_3

    .line 238
    :cond_3
    const/4 v9, 0x1

    .line 239
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    .line 240
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    .line 241
    const/4 v10, 0x1

    .line 243
    goto :goto_3

    .line 211
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/FilterStations;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/FilterStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 3
    .parameter

    .prologue
    const-wide/16 v1, 0x0

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->a:Ljava/util/List;

    .line 60
    iput-wide v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->b:D

    .line 61
    iput-wide v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->c:D

    .line 62
    iput-wide v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    .line 63
    iput-wide v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    .line 64
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/FilterStations;->c(Lgbis/gbandroid/entities/ListStationTab;)V

    .line 65
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/FilterStations;->b(Lgbis/gbandroid/entities/ListStationTab;)V

    .line 66
    return-void
.end method

.method private static a(DLgbis/gbandroid/entities/ListMessage;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 178
    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lgbis/gbandroid/entities/ListMessage;)Z
    .locals 4
    .parameter

    .prologue
    .line 192
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 193
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lgbis/gbandroid/entities/ListMessage;)Z
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;",
            "Lgbis/gbandroid/entities/ListMessage;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 166
    :goto_0
    if-lt v2, v3, :cond_0

    move v0, v1

    .line 174
    :goto_1
    return v0

    .line 167
    :cond_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationNameFilter;

    .line 168
    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->getStationName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 169
    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->isIncluded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 172
    goto :goto_1

    .line 166
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Z
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 199
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v1

    .line 200
    :goto_0
    if-lt v2, v3, :cond_0

    move v0, v1

    .line 203
    :goto_1
    return v0

    .line 201
    :cond_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationNameFilter;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->getStationName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    const/4 v0, 0x1

    goto :goto_1

    .line 200
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private b(Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 8
    .parameter

    .prologue
    .line 69
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    .line 70
    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lgbis/gbandroid/entities/ListStationFilter;

    iget-wide v1, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    iget-wide v3, p0, Lgbis/gbandroid/activities/list/FilterStations;->b:D

    iget-object v5, p0, Lgbis/gbandroid/activities/list/FilterStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v6, "noPricesPreference"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->a:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lgbis/gbandroid/entities/ListStationFilter;-><init>(DDZLjava/util/List;)V

    .line 72
    invoke-virtual {p1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setStationFilter(Lgbis/gbandroid/entities/ListStationFilter;)V

    .line 74
    :cond_0
    return-void
.end method

.method private static b(DLgbis/gbandroid/entities/ListMessage;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v0

    cmpg-double v0, v0, p0

    if-gtz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    .line 188
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 10
    .parameter

    .prologue
    const-wide/16 v8, 0x0

    .line 79
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v2

    .line 80
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 81
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 99
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    cmpl-double v0, v0, v8

    if-nez v0, :cond_0

    .line 100
    iget-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    iput-wide v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    .line 101
    :cond_0
    return-void

    .line 82
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 83
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v4

    .line 84
    if-eqz v4, :cond_8

    .line 85
    iget-object v5, p0, Lgbis/gbandroid/activities/list/FilterStations;->a:Ljava/util/List;

    invoke-static {v5, v4}, Lgbis/gbandroid/activities/list/FilterStations;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 86
    iget-object v5, p0, Lgbis/gbandroid/activities/list/FilterStations;->a:Ljava/util/List;

    new-instance v6, Lgbis/gbandroid/entities/StationNameFilter;

    const/4 v7, 0x1

    invoke-direct {v6, v4, v7}, Lgbis/gbandroid/entities/StationNameFilter;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_2
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v4

    .line 88
    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->b:D

    cmpl-double v6, v4, v6

    if-lez v6, :cond_3

    .line 89
    iput-wide v4, p0, Lgbis/gbandroid/activities/list/FilterStations;->b:D

    .line 90
    :cond_3
    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->c:D

    cmpg-double v6, v4, v6

    if-ltz v6, :cond_4

    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->c:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_5

    .line 91
    :cond_4
    iput-wide v4, p0, Lgbis/gbandroid/activities/list/FilterStations;->c:D

    .line 92
    :cond_5
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v4

    .line 93
    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    cmpl-double v0, v4, v6

    if-lez v0, :cond_6

    .line 94
    iput-wide v4, p0, Lgbis/gbandroid/activities/list/FilterStations;->d:D

    .line 95
    :cond_6
    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    cmpg-double v0, v4, v6

    if-ltz v0, :cond_7

    iget-wide v6, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    cmpl-double v0, v6, v8

    if-nez v0, :cond_8

    :cond_7
    cmpl-double v0, v4, v8

    if-eqz v0, :cond_8

    .line 96
    iput-wide v4, p0, Lgbis/gbandroid/activities/list/FilterStations;->e:D

    .line 81
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static createFilter(Lgbis/gbandroid/entities/ListStationTab;Z)Lgbis/gbandroid/entities/ListStationFilter;
    .locals 16
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v9

    .line 107
    new-instance v10, Lgbis/gbandroid/entities/ListStationFilter;

    invoke-direct {v10}, Lgbis/gbandroid/entities/ListStationFilter;-><init>()V

    .line 108
    invoke-virtual {v10}, Lgbis/gbandroid/entities/ListStationFilter;->getStationNames()Ljava/util/List;

    move-result-object v11

    .line 109
    invoke-virtual {v10}, Lgbis/gbandroid/entities/ListStationFilter;->getDistance()D

    move-result-wide v4

    .line 110
    invoke-virtual {v10}, Lgbis/gbandroid/entities/ListStationFilter;->getPrice()D

    move-result-wide v6

    .line 111
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v12

    .line 112
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-lt v8, v12, :cond_0

    .line 126
    invoke-virtual {v10, v4, v5}, Lgbis/gbandroid/entities/ListStationFilter;->setDistance(D)V

    .line 127
    invoke-virtual {v10, v6, v7}, Lgbis/gbandroid/entities/ListStationFilter;->setPrice(D)V

    .line 128
    move/from16 v0, p1

    invoke-virtual {v10, v0}, Lgbis/gbandroid/entities/ListStationFilter;->setHasPrices(Z)V

    .line 129
    return-object v10

    .line 113
    :cond_0
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/ListMessage;

    .line 114
    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v2

    .line 115
    if-eqz v2, :cond_2

    .line 116
    invoke-static {v11, v2}, Lgbis/gbandroid/activities/list/FilterStations;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 117
    new-instance v3, Lgbis/gbandroid/entities/StationNameFilter;

    const/4 v13, 0x1

    invoke-direct {v3, v2, v13}, Lgbis/gbandroid/entities/StationNameFilter;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v11, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_1
    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    .line 119
    cmpl-double v13, v2, v4

    if-lez v13, :cond_4

    .line 121
    :goto_1
    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v4

    .line 122
    cmpl-double v1, v4, v6

    if-lez v1, :cond_3

    move-wide v14, v4

    move-wide v4, v2

    move-wide v1, v14

    .line 112
    :goto_2
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move-wide v6, v1

    goto :goto_0

    :cond_2
    move-wide v1, v6

    goto :goto_2

    :cond_3
    move-wide v4, v2

    move-wide v1, v6

    goto :goto_2

    :cond_4
    move-wide v2, v4

    goto :goto_1
.end method

.method private d(Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 14
    .parameter

    .prologue
    const v7, 0x7f07006b

    const v6, 0x7f07006a

    .line 287
    new-instance v9, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v9, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 288
    iget-object v0, p0, Lgbis/gbandroid/activities/list/FilterStations;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030013

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 289
    const v0, 0x7f070044

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/FilterButton;

    .line 290
    const v1, 0x7f070045

    invoke-virtual {v10, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/views/FilterButton;

    .line 291
    const v2, 0x7f070046

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lgbis/gbandroid/views/FilterButton;

    .line 292
    const v3, 0x7f070047

    invoke-virtual {v10, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 293
    const v4, 0x7f070048

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 294
    const v4, 0x7f070049

    invoke-virtual {v10, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 295
    invoke-virtual {v11, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    .line 296
    invoke-virtual {v11, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 297
    invoke-virtual {v12, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/SeekBar;

    .line 298
    invoke-virtual {v12, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 299
    const v8, 0x7f0700b9

    invoke-virtual {v10, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckedTextView;

    .line 301
    new-instance v13, Lgbis/gbandroid/activities/list/FilterStations$b;

    invoke-direct {v13, p0}, Lgbis/gbandroid/activities/list/FilterStations$b;-><init>(Lgbis/gbandroid/activities/list/FilterStations;)V

    .line 302
    invoke-virtual {v13, v0, v3}, Lgbis/gbandroid/activities/list/FilterStations$b;->a(Landroid/view/View;Landroid/view/View;)V

    .line 303
    invoke-virtual {v13, v1, v11}, Lgbis/gbandroid/activities/list/FilterStations$b;->a(Landroid/view/View;Landroid/view/View;)V

    .line 304
    invoke-virtual {v13, v2, v12}, Lgbis/gbandroid/activities/list/FilterStations$b;->a(Landroid/view/View;Landroid/view/View;)V

    .line 306
    invoke-virtual {v13, v0}, Lgbis/gbandroid/activities/list/FilterStations$b;->a(Landroid/view/View;)V

    .line 307
    new-instance v11, Lgbis/gbandroid/activities/list/FilterStations$a;

    invoke-direct {v11, p0, v13}, Lgbis/gbandroid/activities/list/FilterStations$a;-><init>(Lgbis/gbandroid/activities/list/FilterStations;Lgbis/gbandroid/activities/list/FilterStations$b;)V

    .line 309
    invoke-virtual {v0, v11}, Lgbis/gbandroid/views/FilterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 310
    invoke-virtual {v1, v11}, Lgbis/gbandroid/views/FilterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    invoke-virtual {v2, v11}, Lgbis/gbandroid/views/FilterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    new-instance v0, Lgbis/gbandroid/activities/list/b;

    invoke-direct {v0, p0, v8, p1}, Lgbis/gbandroid/activities/list/b;-><init>(Lgbis/gbandroid/activities/list/FilterStations;Landroid/widget/CheckedTextView;Lgbis/gbandroid/entities/ListStationTab;)V

    invoke-virtual {v8, v0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    new-instance v0, Lgbis/gbandroid/activities/list/FilterStations$c;

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationFilter;->getStationNames()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/list/FilterStations$c;-><init>(Lgbis/gbandroid/activities/list/FilterStations;Landroid/content/Context;Ljava/util/List;)V

    .line 322
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 323
    new-instance v1, Lgbis/gbandroid/activities/list/c;

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/activities/list/c;-><init>(Lgbis/gbandroid/activities/list/FilterStations;Lgbis/gbandroid/activities/list/FilterStations$c;)V

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 330
    const/4 v0, 0x0

    invoke-direct {p0, v4, v5, v0, p1}, Lgbis/gbandroid/activities/list/FilterStations;->a(Landroid/widget/SeekBar;Landroid/widget/TextView;ILgbis/gbandroid/entities/ListStationTab;)V

    .line 331
    const/4 v0, 0x1

    invoke-direct {p0, v6, v7, v0, p1}, Lgbis/gbandroid/activities/list/FilterStations;->a(Landroid/widget/SeekBar;Landroid/widget/TextView;ILgbis/gbandroid/entities/ListStationTab;)V

    .line 332
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices()Z

    move-result v0

    invoke-virtual {v8, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 334
    const v0, 0x7f090121

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/FilterStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 335
    invoke-virtual {v9, v10}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 336
    const v0, 0x7f090193

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/FilterStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/list/d;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/list/d;-><init>(Lgbis/gbandroid/activities/list/FilterStations;Lgbis/gbandroid/entities/ListStationTab;)V

    invoke-virtual {v9, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 346
    invoke-virtual {v9}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 347
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 348
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 349
    return-void
.end method

.method public static filterStations(Lgbis/gbandroid/entities/ListStationTab;)Ljava/util/List;
    .locals 13
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgbis/gbandroid/entities/ListStationTab;",
            ")",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const-wide/16 v11, 0x0

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 134
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v3

    .line 135
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v2

    .line 136
    if-eqz v2, :cond_9

    .line 137
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListStationFilter;->getStationNames()Ljava/util/List;

    move-result-object v4

    .line 138
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListStationFilter;->getDistance()D

    move-result-wide v5

    .line 139
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListStationFilter;->getPrice()D

    move-result-wide v7

    .line 140
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    .line 141
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices()Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v0

    .line 142
    :goto_0
    if-lt v2, v9, :cond_1

    :cond_0
    move-object v0, v1

    .line 161
    :goto_1
    return-object v0

    .line 143
    :cond_1
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 144
    invoke-static {v4, v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(Ljava/util/List;Lgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 145
    cmpl-double v10, v5, v11

    if-eqz v10, :cond_2

    invoke-static {v5, v6, v0}, Lgbis/gbandroid/activities/list/FilterStations;->b(DLgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 146
    :cond_2
    cmpl-double v10, v7, v11

    if-eqz v10, :cond_3

    invoke-static {v7, v8, v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(DLgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 147
    :cond_3
    invoke-static {v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(Lgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 148
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    move v2, v0

    .line 151
    :goto_2
    if-ge v2, v9, :cond_0

    .line 152
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 153
    invoke-static {v4, v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(Ljava/util/List;Lgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 154
    cmpl-double v10, v5, v11

    if-eqz v10, :cond_6

    invoke-static {v5, v6, v0}, Lgbis/gbandroid/activities/list/FilterStations;->b(DLgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 155
    :cond_6
    cmpl-double v10, v7, v11

    if-eqz v10, :cond_7

    invoke-static {v7, v8, v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(DLgbis/gbandroid/entities/ListMessage;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 156
    :cond_7
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 161
    :cond_9
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 43
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/FilterStations;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_1

    .line 46
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListStationTab;

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/FilterStations;->a(Lgbis/gbandroid/entities/ListStationTab;)V

    .line 49
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/FilterStations;->d(Lgbis/gbandroid/entities/ListStationTab;)V

    .line 56
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/FilterStations;->finish()V

    goto :goto_0

    .line 54
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/FilterStations;->finish()V

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .parameter

    .prologue
    .line 428
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/FilterStations;->finish()V

    .line 429
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 435
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 439
    const v0, 0x7f090255

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/FilterStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
