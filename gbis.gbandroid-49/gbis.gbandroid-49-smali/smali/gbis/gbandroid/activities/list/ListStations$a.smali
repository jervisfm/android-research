.class final Lgbis/gbandroid/activities/list/ListStations$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1788
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 1789
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 1790
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 1826
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 1827
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/views/layouts/CustomListView;->onRefreshComplete()V

    .line 1828
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 1794
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 1796
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->r(Lgbis/gbandroid/activities/list/ListStations;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1798
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1801
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->t(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1802
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1803
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$j;->clear()V

    .line 1804
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->n(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1805
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->v(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1807
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1808
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/views/layouts/CustomListView;)V

    .line 1809
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/views/layouts/CustomListView;->onRefreshComplete()V

    .line 1810
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->w(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1811
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 3

    .prologue
    .line 1816
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->x(Lgbis/gbandroid/activities/list/ListStations;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1817
    const/4 v0, 0x1

    .line 1820
    :goto_0
    return v0

    .line 1819
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$a;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->setMessage(Ljava/lang/String;)V

    .line 1820
    const/4 v0, 0x0

    goto :goto_0
.end method
