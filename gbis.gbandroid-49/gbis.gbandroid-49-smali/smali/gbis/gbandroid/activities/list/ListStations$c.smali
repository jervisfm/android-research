.class final Lgbis/gbandroid/activities/list/ListStations$c;
.super Ljava/lang/Thread;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private b:Landroid/os/Handler;

.field private c:J

.field private d:J


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/os/Handler;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1891
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$c;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1892
    iput-object p2, p0, Lgbis/gbandroid/activities/list/ListStations$c;->b:Landroid/os/Handler;

    .line 1893
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/activities/list/ListStations$c;->c:J

    .line 1894
    mul-int/lit16 v0, p3, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lgbis/gbandroid/activities/list/ListStations$c;->d:J

    .line 1895
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1897
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1898
    iget-wide v2, p0, Lgbis/gbandroid/activities/list/ListStations$c;->c:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lgbis/gbandroid/activities/list/ListStations$c;->d:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 1899
    iget-wide v2, p0, Lgbis/gbandroid/activities/list/ListStations$c;->d:J

    iget-wide v4, p0, Lgbis/gbandroid/activities/list/ListStations$c;->c:J

    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    .line 1901
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1902
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$c;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1903
    :cond_1
    return-void
.end method
