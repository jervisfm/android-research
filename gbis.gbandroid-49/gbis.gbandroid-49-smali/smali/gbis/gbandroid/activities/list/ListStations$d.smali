.class final Lgbis/gbandroid/activities/list/ListStations$d;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private b:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1833
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 1834
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 1835
    return-void
.end method

.method public constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;B)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1837
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 1838
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 1839
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->b:Z

    .line 1840
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .parameter

    .prologue
    .line 1844
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 1846
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->r(Lgbis/gbandroid/activities/list/ListStations;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1848
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1849
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->y(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/entities/ListResults;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListResults;)V

    .line 1850
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->z(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 1851
    iget-boolean v1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->b:Z

    if-nez v1, :cond_1

    .line 1852
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->n(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1854
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->A(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1855
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->B(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1871
    :cond_0
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->w(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1872
    return-void

    .line 1857
    :cond_1
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v2

    .line 1858
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->p(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1859
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1860
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-lt v1, v3, :cond_2

    .line 1862
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->C(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1864
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Landroid/widget/ListView;)V

    goto :goto_1

    .line 1861
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v4, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->add(Ljava/lang/Object;)V

    .line 1860
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1867
    :cond_3
    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->b:Z

    if-eqz v0, :cond_0

    .line 1868
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->j(Lgbis/gbandroid/activities/list/ListStations;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 1876
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$d;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->x(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1877
    const/4 v0, 0x1

    return v0
.end method
