.class final Lgbis/gbandroid/activities/list/ListStations$f;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lgbis/gbandroid/entities/ListMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private b:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/ListStations;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1686
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$f;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687
    iput p2, p0, Lgbis/gbandroid/activities/list/ListStations$f;->b:I

    .line 1688
    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/entities/ListMessage;)I
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x1

    const-wide/16 v5, 0x0

    .line 1691
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v3

    if-gez v3, :cond_1

    .line 1711
    :cond_0
    :goto_0
    return v0

    .line 1693
    :cond_1
    iget v3, p0, Lgbis/gbandroid/activities/list/ListStations$f;->b:I

    if-ne v3, v0, :cond_2

    .line 1694
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v0

    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    goto :goto_0

    .line 1696
    :cond_2
    iget v3, p0, Lgbis/gbandroid/activities/list/ListStations$f;->b:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    .line 1697
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    cmpl-double v3, v3, v5

    if-nez v3, :cond_3

    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    cmpl-double v3, v3, v5

    if-nez v3, :cond_0

    .line 1699
    :cond_3
    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    cmpl-double v3, v3, v5

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_4

    move v0, v1

    .line 1700
    goto :goto_0

    .line 1701
    :cond_4
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    .line 1702
    if-nez v3, :cond_6

    .line 1703
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getSortOrder()I

    move-result v3

    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getSortOrder()I

    move-result v4

    if-ge v3, v4, :cond_5

    move v0, v1

    .line 1704
    goto :goto_0

    .line 1705
    :cond_5
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getSortOrder()I

    move-result v1

    invoke-virtual {p2}, Lgbis/gbandroid/entities/ListMessage;->getSortOrder()I

    move-result v3

    if-gt v1, v3, :cond_0

    move v0, v2

    .line 1707
    goto :goto_0

    :cond_6
    move v0, v3

    .line 1709
    goto :goto_0

    :cond_7
    move v0, v2

    .line 1711
    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/ListMessage;

    check-cast p2, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/list/ListStations$f;->a(Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/entities/ListMessage;)I

    move-result v0

    return v0
.end method
