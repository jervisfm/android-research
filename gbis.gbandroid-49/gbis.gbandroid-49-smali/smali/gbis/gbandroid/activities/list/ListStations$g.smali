.class final Lgbis/gbandroid/activities/list/ListStations$g;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# direct methods
.method private constructor <init>(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1715
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgbis/gbandroid/activities/list/ListStations;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1715
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations$g;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1718
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->k(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1719
    const-string v0, "fuelPreference"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->l(Lgbis/gbandroid/activities/list/ListStations;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1720
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->m(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1728
    :cond_0
    :goto_0
    return-void

    .line 1721
    :cond_1
    const-string v0, "distancePreference"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1722
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->n(Lgbis/gbandroid/activities/list/ListStations;)V

    goto :goto_0

    .line 1723
    :cond_2
    const-string v0, "noPricesPreference"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1724
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->o(Lgbis/gbandroid/activities/list/ListStations;)Z

    move-result v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Z)V

    .line 1725
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->p(Lgbis/gbandroid/activities/list/ListStations;)V

    goto :goto_0

    .line 1726
    :cond_3
    const-string v0, "member_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1727
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$g;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->p(Lgbis/gbandroid/activities/list/ListStations;)V

    goto :goto_0
.end method
