.class final Lgbis/gbandroid/activities/list/ListStations$h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "h"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1943
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .parameter

    .prologue
    .line 1947
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$h;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->D(Lgbis/gbandroid/activities/list/ListStations;)Landroid/hardware/Sensor;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1957
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$h;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->E(Lgbis/gbandroid/activities/list/ListStations;)F

    move-result v0

    .line 1958
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$h;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;F)V

    .line 1959
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$h;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->E(Lgbis/gbandroid/activities/list/ListStations;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x4000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1960
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$h;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->F(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 1962
    :cond_0
    return-void
.end method
