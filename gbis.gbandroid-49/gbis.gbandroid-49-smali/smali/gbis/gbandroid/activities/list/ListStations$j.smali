.class final Lgbis/gbandroid/activities/list/ListStations$j;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "j"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/ListMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lgbis/gbandroid/activities/list/ListStations$i;

.field private e:Lgbis/gbandroid/utils/ImageLoader;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f03003c

    .line 1514
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 1515
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1516
    iput-object p3, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    .line 1517
    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->c:I

    .line 1518
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v1, 0x7f020076

    const-string v2, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-direct {v0, p1, v1, v2}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->e:Lgbis/gbandroid/utils/ImageLoader;

    .line 1519
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1654
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1523
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-gez v0, :cond_5

    .line 1524
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1525
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1526
    const v0, 0x7f0700e3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1531
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 1649
    :goto_0
    return-object v0

    .line 1532
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_1

    .line 1533
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->c(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1534
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v1, -0x5

    if-ne v0, v1, :cond_2

    .line 1535
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->d(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1536
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v1, -0x6

    if-ne v0, v1, :cond_3

    .line 1537
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030044

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1539
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1540
    const v0, 0x7f0700ee

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1541
    const v1, 0x7f0700ef

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1542
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v2

    const/4 v4, -0x2

    if-ne v2, v4, :cond_4

    .line 1543
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v4, 0x7f090027

    invoke-virtual {v2, v4}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1544
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f090028

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v3

    goto/16 :goto_0

    .line 1546
    :cond_4
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v4, 0x7f090026

    invoke-virtual {v2, v4}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1547
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f090025

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v3

    goto/16 :goto_0

    .line 1552
    :cond_5
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/list/ListStations$l;

    if-nez v0, :cond_b

    .line 1553
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1555
    new-instance v1, Lgbis/gbandroid/activities/list/ListStations$l;

    invoke-direct {v1}, Lgbis/gbandroid/activities/list/ListStations$l;-><init>()V

    .line 1556
    const v0, 0x7f0700d8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->a:Landroid/widget/TextView;

    .line 1557
    const v0, 0x7f0700d9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->b:Landroid/widget/TextView;

    .line 1558
    const v0, 0x7f0700da

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->c:Landroid/widget/TextView;

    .line 1559
    const v0, 0x7f0700db

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    .line 1560
    iget-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->j:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1561
    const v0, 0x7f0700dd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->e:Landroid/widget/TextView;

    .line 1562
    const v0, 0x7f0700d6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->k:Landroid/widget/RelativeLayout;

    .line 1563
    const v0, 0x7f0700de

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->f:Landroid/widget/TextView;

    .line 1564
    const v0, 0x7f0700df

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->g:Landroid/widget/TextView;

    .line 1565
    const v0, 0x7f0700e0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    .line 1566
    const v0, 0x7f0700dc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/list/ListStations$l;->i:Landroid/widget/ImageView;

    .line 1567
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v7, v1

    .line 1571
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lgbis/gbandroid/entities/ListMessage;

    .line 1572
    if-eqz v6, :cond_a

    .line 1573
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$i;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-direct {v0, v1, v6}, Lgbis/gbandroid/activities/list/ListStations$i;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->d:Lgbis/gbandroid/activities/list/ListStations$i;

    .line 1574
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->e:Landroid/widget/TextView;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1575
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_d

    .line 1576
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1577
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1578
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1583
    :goto_2
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v2}, Lgbis/gbandroid/activities/list/ListStations;->e(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1584
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1585
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1586
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->j:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x3

    const v2, 0x7f0700da

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1587
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    iget-object v1, v7, Lgbis/gbandroid/activities/list/ListStations$l;->j:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1596
    :goto_3
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->k:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->d:Lgbis/gbandroid/activities/list/ListStations$i;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1598
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->isNotIntersection()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1599
    const-string v0, "near"

    .line 1602
    :goto_4
    iget-object v1, v7, Lgbis/gbandroid/activities/list/ListStations$l;->f:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getCrossStreet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1603
    iget-object v1, v7, Lgbis/gbandroid/activities/list/ListStations$l;->g:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getCity()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1605
    :try_start_0
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_13

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_13

    .line 1606
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->g(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1607
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->g(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "miles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1608
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1622
    :goto_6
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v1

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v2}, Lgbis/gbandroid/activities/list/ListStations;->h(Lgbis/gbandroid/activities/list/ListStations;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/GBApplication;->getImageUrl(IF)Ljava/lang/String;

    move-result-object v1

    .line 1623
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->i:Landroid/widget/ImageView;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1625
    :try_start_1
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v0

    if-lez v0, :cond_14

    .line 1626
    const/4 v4, 0x0

    .line 1627
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->i(Lgbis/gbandroid/activities/list/ListStations;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1628
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    invoke-static {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;I)Landroid/database/Cursor;

    move-result-object v0

    .line 1629
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_7

    .line 1630
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandVersion()I

    move-result v3

    invoke-static {v0, v2, v3}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;II)Z

    .line 1631
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    invoke-static {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;I)Landroid/database/Cursor;

    move-result-object v0

    .line 1633
    :cond_7
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1634
    const-string v2, "version"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1635
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandVersion()I

    move-result v3

    if-le v3, v2, :cond_8

    .line 1636
    const/4 v4, 0x1

    .line 1637
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v3

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandVersion()I

    move-result v5

    invoke-static {v2, v3, v5}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;II)Z

    .line 1639
    :cond_8
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1641
    :cond_9
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$j;->e:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v3, v7, Lgbis/gbandroid/activities/list/ListStations$l;->i:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_a
    :goto_7
    move-object v0, p2

    .line 1649
    goto/16 :goto_0

    .line 1569
    :cond_b
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/list/ListStations$l;

    move-object v7, v0

    goto/16 :goto_1

    .line 1580
    :cond_c
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->a:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1581
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1589
    :cond_d
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->a:Landroid/widget/TextView;

    const-string v1, "--"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1590
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1591
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1592
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->c:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1593
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->j:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x3

    const v2, 0x7f0700d7

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1594
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->d:Landroid/widget/TextView;

    iget-object v1, v7, Lgbis/gbandroid/activities/list/ListStations$l;->j:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_3

    .line 1601
    :cond_e
    const-string v0, "&"

    goto/16 :goto_4

    .line 1603
    :cond_f
    const-string v0, ""

    goto/16 :goto_5

    .line 1610
    :cond_10
    :try_start_2
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    const-wide v4, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_6

    .line 1620
    :catch_0
    move-exception v0

    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 1612
    :cond_11
    :try_start_3
    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1613
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1615
    :cond_12
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v2

    const-wide v4, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 1617
    :cond_13
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_6

    .line 1643
    :cond_14
    :try_start_4
    iget-object v0, v7, Lgbis/gbandroid/activities/list/ListStations$l;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->j(Lgbis/gbandroid/activities/list/ListStations;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_7

    .line 1644
    :catch_1
    move-exception v0

    .line 1645
    iget-object v1, v7, Lgbis/gbandroid/activities/list/ListStations$l;->i:Landroid/widget/ImageView;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations$j;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v2}, Lgbis/gbandroid/activities/list/ListStations;->j(Lgbis/gbandroid/activities/list/ListStations;)Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020076

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1646
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_7
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 1665
    const/4 v0, 0x1

    return v0
.end method
