.class final Lgbis/gbandroid/activities/list/ListStations$k;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/list/ListStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "k"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# direct methods
.method private constructor <init>(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1743
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgbis/gbandroid/activities/list/ListStations;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1743
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations$k;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1746
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1747
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/views/layouts/CustomListView;)V

    .line 1748
    :cond_0
    if-eqz p2, :cond_1

    .line 1749
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 1752
    :goto_0
    return-void

    .line 1751
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations$k;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->requestFocus(I)Z

    goto :goto_0
.end method
