.class public Lgbis/gbandroid/activities/list/ListStations;
.super Lgbis/gbandroid/activities/base/GBActivityAds;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lgbis/gbandroid/listeners/MyLocationChangedListener;
.implements Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/list/ListStations$a;,
        Lgbis/gbandroid/activities/list/ListStations$b;,
        Lgbis/gbandroid/activities/list/ListStations$c;,
        Lgbis/gbandroid/activities/list/ListStations$d;,
        Lgbis/gbandroid/activities/list/ListStations$e;,
        Lgbis/gbandroid/activities/list/ListStations$f;,
        Lgbis/gbandroid/activities/list/ListStations$g;,
        Lgbis/gbandroid/activities/list/ListStations$h;,
        Lgbis/gbandroid/activities/list/ListStations$i;,
        Lgbis/gbandroid/activities/list/ListStations$j;,
        Lgbis/gbandroid/activities/list/ListStations$k;,
        Lgbis/gbandroid/activities/list/ListStations$l;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Z

.field private G:Lgbis/gbandroid/activities/list/ListStations$c;

.field private H:Landroid/view/View;

.field private I:Landroid/view/View;

.field private J:Landroid/hardware/SensorManager;

.field private K:Landroid/hardware/Sensor;

.field private L:Lgbis/gbandroid/activities/list/ListStations$h;

.field private M:F

.field private N:Landroid/widget/RelativeLayout;

.field private O:Landroid/widget/RelativeLayout;

.field private P:Landroid/widget/RelativeLayout;

.field private Q:Landroid/widget/RelativeLayout;

.field private R:Landroid/widget/RelativeLayout;

.field private S:Landroid/widget/RelativeLayout;

.field private T:Landroid/widget/RelativeLayout;

.field final a:Landroid/os/Handler;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListStationTab;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lgbis/gbandroid/entities/ListResults;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/maps/GeoPoint;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/view/View;

.field private o:[Landroid/widget/RelativeLayout;

.field private p:Lgbis/gbandroid/views/layouts/CustomListView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/app/Dialog;

.field private t:Lgbis/gbandroid/activities/list/ListStations$j;

.field private u:Lgbis/gbandroid/activities/list/ListStations$g;

.field private v:Lgbis/gbandroid/activities/list/ListStations$e;

.field private w:Lgbis/gbandroid/utils/CustomAsyncTask;

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;-><init>()V

    .line 1881
    new-instance v0, Lgbis/gbandroid/activities/list/e;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/list/e;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->a:Landroid/os/Handler;

    .line 109
    return-void
.end method

.method private A()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x1

    const-wide v8, 0x40c3878000000000L

    const/4 v7, -0x2

    const/4 v6, -0x3

    .line 1234
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 1235
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v1

    .line 1236
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/util/List;)I

    move-result v3

    invoke-virtual {v2, v3}, Lgbis/gbandroid/entities/ListResults;->setTotalNear(I)V

    .line 1237
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_5

    .line 1238
    new-instance v2, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v2}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 1239
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1240
    invoke-virtual {v2, v7}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 1243
    :goto_0
    invoke-virtual {v2, v8, v9}, Lgbis/gbandroid/entities/ListMessage;->setDistance(D)V

    .line 1244
    const/16 v0, 0x270f

    invoke-virtual {v2, v0}, Lgbis/gbandroid/entities/ListMessage;->setSortOrder(I)V

    .line 1245
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->isNear()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1246
    new-instance v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 1247
    invoke-virtual {v0, v10}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 1248
    const v3, 0x7f090033

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v5}, Lgbis/gbandroid/entities/ListResults;->getSearchTerms()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lgbis/gbandroid/entities/ListMessage;->setStationName(Ljava/lang/String;)V

    .line 1249
    invoke-interface {v1, v11, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1250
    new-instance v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 1251
    invoke-virtual {v0, v10}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 1252
    const v3, 0x7f090034

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lgbis/gbandroid/entities/ListMessage;->setStationName(Ljava/lang/String;)V

    .line 1253
    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListResults;->getTotalNear()I

    move-result v3

    if-nez v3, :cond_7

    .line 1254
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v3

    if-ne v3, v6, :cond_0

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v3

    if-ne v3, v7, :cond_2

    .line 1255
    :cond_1
    const/4 v3, 0x1

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1256
    :cond_2
    const/4 v2, 0x2

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1265
    :cond_3
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1266
    new-instance v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 1267
    invoke-virtual {v0, v6}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 1268
    invoke-virtual {v0, v8, v9}, Lgbis/gbandroid/entities/ListMessage;->setDistance(D)V

    .line 1269
    const/16 v2, 0x270f

    invoke-virtual {v0, v2}, Lgbis/gbandroid/entities/ListMessage;->setSortOrder(I)V

    .line 1270
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1272
    :cond_4
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->o()V

    .line 1274
    :cond_5
    return-void

    .line 1242
    :cond_6
    invoke-virtual {v2, v6}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    goto/16 :goto_0

    .line 1258
    :cond_7
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v3

    if-ne v3, v6, :cond_8

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_8
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v3

    if-ne v3, v7, :cond_a

    .line 1259
    :cond_9
    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListResults;->getTotalNear()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1260
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListResults;->getTotalNear()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 1262
    :cond_a
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListResults;->getTotalNear()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method static synthetic A(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 673
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->f()V

    return-void
.end method

.method private B()V
    .locals 3

    .prologue
    .line 1277
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    .line 1278
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "distancePreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->e:Ljava/lang/String;

    .line 1279
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "noPricesPreference"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->f:Z

    .line 1280
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    .line 1281
    return-void
.end method

.method static synthetic B(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 546
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->d()V

    return-void
.end method

.method private C()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1284
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Z)V

    .line 1285
    const v0, 0x7f090089

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 1286
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$d;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/list/ListStations$d;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 1287
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1288
    return-void
.end method

.method static synthetic C(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 1
    .parameter

    .prologue
    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->z:Z

    return-void
.end method

.method static synthetic D(Lgbis/gbandroid/activities/list/ListStations;)Landroid/hardware/Sensor;
    .locals 1
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->K:Landroid/hardware/Sensor;

    return-object v0
.end method

.method private D()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1291
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Z)V

    .line 1292
    const v0, 0x7f090089

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 1293
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/list/ListStations$a;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 1294
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1295
    return-void
.end method

.method static synthetic E(Lgbis/gbandroid/activities/list/ListStations;)F
    .locals 1
    .parameter

    .prologue
    .line 159
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->M:F

    return v0
.end method

.method private E()V
    .locals 2

    .prologue
    .line 1298
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$e;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getActivity()Lgbis/gbandroid/activities/base/GBActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/list/ListStations$e;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->v:Lgbis/gbandroid/activities/list/ListStations$e;

    .line 1299
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->v:Lgbis/gbandroid/activities/list/ListStations$e;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations$e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1300
    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->v:Lgbis/gbandroid/activities/list/ListStations$e;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 1301
    return-void
.end method

.method private F()V
    .locals 9

    .prologue
    const v2, 0x7f06000a

    const/4 v6, 0x0

    const-wide v7, 0x412e848000000000L

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1311
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    aget v1, v0, v6

    .line 1312
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    aget v2, v0, v4

    .line 1314
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 1315
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v3

    .line 1316
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-gtz v0, :cond_4

    .line 1317
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-gtz v0, :cond_3

    .line 1318
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v5, :cond_2

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1319
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lgbis/gbandroid/views/layouts/CustomListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 1326
    :goto_0
    if-eqz v0, :cond_0

    .line 1327
    new-instance v3, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v7

    double-to-int v4, v4

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getLongitude()D

    move-result-wide v5

    mul-double/2addr v5, v7

    double-to-int v0, v5

    invoke-direct {v3, v4, v0}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    .line 1329
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/list/f;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/list/f;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/f;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1330
    new-instance v3, Lgbis/gbandroid/queries/MapQuery;

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v5, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v3, p0, v4, v0, v5}, Lgbis/gbandroid/queries/MapQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1331
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v3, v0, v1, v2}, Lgbis/gbandroid/queries/MapQuery;->getResponseObject(Lcom/google/android/maps/GeoPoint;II)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1332
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->v:Lgbis/gbandroid/activities/list/ListStations$e;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$e;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1333
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/MapResults;

    .line 1334
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/MapResults;)V

    .line 1336
    :cond_1
    return-void

    .line 1321
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lgbis/gbandroid/views/layouts/CustomListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    goto :goto_0

    .line 1323
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/views/layouts/CustomListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    goto :goto_0

    .line 1325
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-virtual {v0, v4}, Lgbis/gbandroid/views/layouts/CustomListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    goto :goto_0
.end method

.method static synthetic F(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1505
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->J()V

    return-void
.end method

.method static synthetic G(Lgbis/gbandroid/activities/list/ListStations;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    return-object v0
.end method

.method private G()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide v4, 0x412e848000000000L

    .line 1339
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->B()V

    .line 1340
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 1341
    if-eqz v0, :cond_0

    .line 1342
    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    .line 1343
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v4

    double-to-int v1, v1

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    .line 1345
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/list/g;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/list/g;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/g;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1346
    new-instance v1, Lgbis/gbandroid/queries/ListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1347
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    if-nez v0, :cond_1

    .line 1348
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v6, v6}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    .line 1349
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->i:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->j:Ljava/lang/String;

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->k:Ljava/lang/String;

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1, v0, v2, v3, v4}, Lgbis/gbandroid/queries/ListQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1350
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/CustomAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1351
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListResults;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    .line 1354
    :cond_2
    return-void
.end method

.method static synthetic H(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    return-object v0
.end method

.method private H()V
    .locals 3

    .prologue
    .line 1407
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/list/FilterStations;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 1408
    const-string v1, "list"

    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1409
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1410
    return-void
.end method

.method private I()V
    .locals 8

    .prologue
    .line 1413
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v2, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1414
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1415
    const v0, 0x7f07003e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1416
    const v1, 0x7f07003f

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1417
    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    .line 1418
    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    const/16 v7, 0x1e

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v4, v5

    .line 1417
    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1420
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1421
    const v4, 0x7f090120

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1422
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1423
    const v3, 0x7f09013d

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1424
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getSearchTerms()Ljava/lang/String;

    move-result-object v0

    .line 1425
    if-eqz v0, :cond_0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1426
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1427
    :cond_0
    const v0, 0x7f0901a3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lgbis/gbandroid/activities/list/h;

    invoke-direct {v3, p0, v1}, Lgbis/gbandroid/activities/list/h;-><init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/widget/EditText;)V

    invoke-virtual {v2, v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1441
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 1442
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1443
    return-void
.end method

.method static synthetic I(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->showLogin()V

    return-void
.end method

.method static synthetic J(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->n:Landroid/view/View;

    return-object v0
.end method

.method private J()V
    .locals 1

    .prologue
    .line 1506
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$j;->notifyDataSetChanged()V

    .line 1507
    return-void
.end method

.method static synthetic K(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1406
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->H()V

    return-void
.end method

.method static synthetic L(Lgbis/gbandroid/activities/list/ListStations;)I
    .locals 1
    .parameter

    .prologue
    .line 956
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->s()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;)I
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1183
    move v1, v0

    move v2, v0

    .line 1184
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 1189
    :cond_0
    return v2

    .line 1185
    :cond_1
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->isNear()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186
    add-int/lit8 v2, v2, 0x1

    .line 1184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;I)Landroid/database/Cursor;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->getBrand(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;)Landroid/view/View;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1010
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    const/high16 v1, 0x7f03

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1011
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/high16 v3, 0x4248

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    .line 1012
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p3, p2, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;Ljava/lang/String;ILandroid/view/ViewGroup;)V

    .line 1013
    return-object v1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 281
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->J:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->L:Lgbis/gbandroid/activities/list/ListStations$h;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->J:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->L:Lgbis/gbandroid/activities/list/ListStations$h;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 284
    iput-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->L:Lgbis/gbandroid/activities/list/ListStations$h;

    .line 286
    :cond_0
    iput-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->K:Landroid/hardware/Sensor;

    .line 287
    iput-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->J:Landroid/hardware/SensorManager;

    .line 289
    :cond_1
    return-void
.end method

.method private a(I)V
    .locals 3
    .parameter

    .prologue
    .line 715
    if-nez p1, :cond_0

    .line 716
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->l:Landroid/widget/TextView;

    const v1, 0x7f090158

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->m:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 722
    :goto_0
    return-void

    .line 719
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->l:Landroid/widget/TextView;

    const v1, 0x7f090157

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->m:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const-wide/16 v5, 0x0

    const/4 v1, 0x0

    const v4, 0x7f060001

    .line 438
    const-string v0, "sm_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 440
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    const-string v0, "fuel regular"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-wide v3, v2

    .line 451
    :goto_0
    cmpl-double v0, v3, v5

    if-lez v0, :cond_0

    .line 452
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v5

    .line 453
    if-eqz v5, :cond_0

    .line 454
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    move v2, v1

    .line 455
    :goto_1
    if-lt v2, v6, :cond_4

    .line 464
    :goto_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v2

    .line 465
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 466
    :goto_3
    if-lt v1, v3, :cond_6

    .line 473
    :goto_4
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$j;->notifyDataSetChanged()V

    .line 476
    :cond_0
    return-void

    .line 443
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    const-string v0, "fuel midgrade"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-wide v3, v2

    goto :goto_0

    .line 446
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 447
    const-string v0, "fuel premium"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-wide v3, v2

    goto :goto_0

    .line 449
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 450
    const-string v0, "fuel diesel"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-wide v3, v2

    goto :goto_0

    .line 456
    :cond_4
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 457
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v8

    if-ne v8, v7, :cond_5

    .line 458
    invoke-virtual {v0, v3, v4}, Lgbis/gbandroid/entities/ListMessage;->setPrice(D)V

    .line 459
    const-string v2, "time spotted"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/entities/ListMessage;->setTimeSpotted(Ljava/lang/String;)V

    .line 460
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/entities/ListMessage;->setTimeOffset(I)V

    goto :goto_2

    .line 455
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 467
    :cond_6
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 468
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v4, -0x6

    if-ne v0, v4, :cond_7

    .line 469
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_4

    .line 466
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    :cond_8
    move-wide v3, v5

    goto/16 :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 1150
    new-instance v0, Lcom/amazon/device/ads/AdTargetingOptions;

    invoke-direct {v0}, Lcom/amazon/device/ads/AdTargetingOptions;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amazon/device/ads/AdTargetingOptions;->enableGeoLocation(Z)Lcom/amazon/device/ads/AdTargetingOptions;

    move-result-object v1

    move-object v0, p1

    .line 1151
    check-cast v0, Lcom/amazon/device/ads/AdLayout;

    new-instance v2, Lgbis/gbandroid/activities/list/p;

    invoke-direct {v2, p0, p1}, Lgbis/gbandroid/activities/list/p;-><init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/amazon/device/ads/AdLayout;->setListener(Lcom/amazon/device/ads/AdListener;)V

    .line 1166
    check-cast p1, Lcom/amazon/device/ads/AdLayout;

    invoke-virtual {p1, v1}, Lcom/amazon/device/ads/AdLayout;->loadAd(Lcom/amazon/device/ads/AdTargetingOptions;)Z

    .line 1167
    return-void
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 725
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 727
    iput-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    .line 729
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 730
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    .line 731
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v3, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 733
    :cond_1
    return-void
.end method

.method private a(Landroid/widget/RelativeLayout;)V
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f070175

    const/4 v1, 0x0

    .line 662
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-lt v0, v3, :cond_0

    .line 671
    return-void

    .line 662
    :cond_0
    aget-object v4, v2, v0

    .line 663
    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 664
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 665
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setSelected(Z)V

    .line 662
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 668
    :cond_1
    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 669
    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->setSelected(Z)V

    goto :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->hideAds()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;DDLandroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p5}, Lgbis/gbandroid/activities/list/ListStations;->showDirections(DDLandroid/location/Location;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 159
    iput p1, p0, Lgbis/gbandroid/activities/list/ListStations;->M:F

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Landroid/location/Location;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lgbis/gbandroid/activities/list/ListStations;->showAddStation(Landroid/location/Location;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations;->n:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Landroid/widget/ListView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 744
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->c(Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1445
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->c(Lgbis/gbandroid/entities/ListMessage;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListResults;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 530
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/ListResults;I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/Station;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->showStationOnMap(Lgbis/gbandroid/entities/Station;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/views/layouts/CustomListView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1201
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->b(Z)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListMessage;)V
    .locals 6
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    .line 292
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    const-string v2, "ContextMenu"

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 293
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03000c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 294
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 295
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v0, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 296
    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 297
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    .line 298
    const v0, 0x7f07002d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 299
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f06000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 305
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    .line 306
    :cond_0
    const/4 v2, 0x2

    const v3, 0x7f0901ba

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 308
    :cond_1
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f03000d

    invoke-direct {v2, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 309
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 310
    new-instance v2, Lgbis/gbandroid/activities/list/i;

    invoke-direct {v2, p0, p1, v1}, Lgbis/gbandroid/activities/list/i;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;[Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 343
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 344
    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListResults;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 531
    invoke-direct {p0, p1, v0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/ListResults;IZ)V

    .line 532
    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListResults;IZ)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 504
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 505
    if-nez v0, :cond_4

    .line 506
    new-instance v0, Lgbis/gbandroid/entities/ListStationTab;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListStationTab;-><init>()V

    .line 507
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 509
    :goto_0
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListResults;->getListMessages()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setListStations(Ljava/util/List;)V

    .line 510
    invoke-virtual {v1, p2}, Lgbis/gbandroid/entities/ListStationTab;->setPage(I)V

    .line 511
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListResults;->getTotalStations()I

    move-result v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setTotalStations(I)V

    .line 512
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListResults;->getTotalNear()I

    move-result v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setTotalNear(I)V

    .line 513
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setFuelType(Ljava/lang/String;)V

    .line 514
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setSortOrder(I)V

    .line 515
    if-eqz p3, :cond_0

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 516
    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v3

    .line 517
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 518
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    .line 519
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-lt v2, v5, :cond_2

    .line 524
    invoke-virtual {v1, v4}, Lgbis/gbandroid/entities/ListStationTab;->setListStationsFiltered(Ljava/util/List;)V

    .line 526
    :cond_0
    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 527
    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->f:Z

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/FilterStations;->createFilter(Lgbis/gbandroid/entities/ListStationTab;Z)Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setStationFilter(Lgbis/gbandroid/entities/ListStationFilter;)V

    .line 528
    :cond_1
    return-void

    .line 520
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 521
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v6

    if-lez v6, :cond_3

    .line 522
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 519
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Lgbis/gbandroid/entities/MapResults;)V
    .locals 7
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 1478
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    if-nez v2, :cond_0

    move-wide v2, v0

    .line 1485
    :goto_0
    new-instance v4, Lgbis/gbandroid/content/GBIntent;

    const-class v5, Lgbis/gbandroid/activities/map/MapStations;

    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v4, p0, v5, v6}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 1486
    const-string v5, "map"

    invoke-virtual {v4, v5, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1487
    const-string v5, "center latitude"

    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v6}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1488
    const-string v5, "center longitude"

    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v6}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1489
    const-string v5, "my latitude"

    invoke-virtual {v4, v5, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1490
    const-string v2, "my longitude"

    invoke-virtual {v4, v2, v0, v1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1491
    const v0, 0x7f0700cc

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f090157

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1492
    const-string v0, "list sort order"

    const/4 v1, 0x2

    invoke-virtual {v4, v0, v1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1495
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1496
    const/4 v0, 0x4

    invoke-virtual {p0, v4, v0}, Lgbis/gbandroid/activities/list/ListStations;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1497
    return-void

    .line 1482
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 1483
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    goto :goto_0

    .line 1494
    :cond_1
    const-string v0, "list sort order"

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method private a(Lgbis/gbandroid/entities/Station;)V
    .locals 3
    .parameter

    .prologue
    .line 1500
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 1501
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1502
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->startActivity(Landroid/content/Intent;)V

    .line 1503
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 689
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 690
    const-string v1, "fuelPreference"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 691
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 692
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1111
    new-instance v1, Lcom/google/ads/AdView;

    sget-object v0, Lcom/google/ads/AdSize;->BANNER:Lcom/google/ads/AdSize;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    .line 1112
    const/16 v0, 0xe

    const/4 v2, -0x1

    invoke-virtual {p3, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1113
    invoke-virtual {p4, v1, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1114
    new-instance v2, Lcom/google/ads/AdRequest;

    invoke-direct {v2}, Lcom/google/ads/AdRequest;-><init>()V

    .line 1115
    if-eqz p5, :cond_0

    .line 1116
    invoke-virtual {v2, p5}, Lcom/google/ads/AdRequest;->setLocation(Landroid/location/Location;)Lcom/google/ads/AdRequest;

    :cond_0
    move-object v0, v1

    .line 1117
    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, v2}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 1118
    check-cast v1, Lcom/google/ads/AdView;

    new-instance v0, Lgbis/gbandroid/activities/list/o;

    invoke-direct {v0, p0, v2, p4}, Lgbis/gbandroid/activities/list/o;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lcom/google/ads/AdRequest;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v0}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 1146
    invoke-virtual {v2, p2}, Lcom/google/ads/AdRequest;->setNetworkExtras(Lcom/google/ads/mediation/NetworkExtras;)Lcom/google/ads/AdRequest;

    .line 1147
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ILandroid/view/ViewGroup;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1017
    .line 1021
    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    .line 1022
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f090217

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1027
    :goto_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v5

    .line 1028
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-nez v0, :cond_1

    .line 1029
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getLastLocationStored()Landroid/location/Location;

    move-result-object v5

    .line 1030
    :cond_1
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v2, -0x2

    invoke-direct {v3, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1031
    packed-switch p3, :pswitch_data_0

    .line 1101
    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1102
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1103
    const/16 v2, 0xe

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1104
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1105
    invoke-virtual {p4, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1108
    :goto_1
    return-void

    .line 1034
    :pswitch_1
    new-instance v2, Lcom/google/ads/doubleclick/DfpExtras;

    invoke-direct {v2}, Lcom/google/ads/doubleclick/DfpExtras;-><init>()V

    move-object v0, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;)V

    goto :goto_1

    .line 1037
    :pswitch_2
    new-instance v2, Lcom/google/ads/doubleclick/DfpExtras;

    invoke-direct {v2}, Lcom/google/ads/doubleclick/DfpExtras;-><init>()V

    .line 1038
    const-string v0, "nk"

    const-string v4, "prtnr"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    .line 1039
    const-string v0, "pr"

    const-string v4, "gb"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    .line 1040
    const-string v0, "cnt"

    const-string v4, "auto"

    invoke-virtual {v2, v0, v4}, Lcom/google/ads/doubleclick/DfpExtras;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/doubleclick/DfpExtras;

    move-object v0, p0

    move-object v4, p4

    .line 1041
    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;Lcom/google/ads/doubleclick/DfpExtras;Landroid/widget/RelativeLayout$LayoutParams;Landroid/view/ViewGroup;Landroid/location/Location;)V

    goto :goto_1

    .line 1044
    :pswitch_3
    new-instance v1, Landroid/webkit/WebView;

    invoke-direct {v1, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 1045
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1046
    const v0, 0x7f090227

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1047
    const-string v0, "?v=1.1"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1048
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "&k="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f090228

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049
    const-string v0, "&uid=1234567890"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1050
    const-string v0, "&size=320x50"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1051
    if-eqz v5, :cond_2

    .line 1052
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "&lat="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "&log="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    move-object v0, v1

    .line 1055
    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 1056
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1057
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1058
    invoke-virtual {p4, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1061
    :pswitch_4
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/GBApplication;->getAdsGSA()Lgbis/gbandroid/entities/AdsGSA;

    move-result-object v0

    .line 1062
    new-instance v3, Lcom/google/ads/searchads/SearchAdRequest;

    invoke-direct {v3}, Lcom/google/ads/searchads/SearchAdRequest;-><init>()V

    .line 1064
    invoke-virtual {p0, v3, v0}, Lgbis/gbandroid/activities/list/ListStations;->fillGSAAdRequestUIParameteres(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;)V

    .line 1065
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v3, v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->getGSAQuery(Lcom/google/ads/searchads/SearchAdRequest;Lgbis/gbandroid/entities/AdsGSA;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/ads/searchads/SearchAdRequest;->setQuery(Ljava/lang/String;)V

    .line 1066
    new-instance v2, Lcom/google/ads/AdView;

    new-instance v0, Lcom/google/ads/AdSize;

    const/16 v4, 0x140

    const/16 v6, 0x3c

    invoke-direct {v0, v4, v6}, Lcom/google/ads/AdSize;-><init>(II)V

    invoke-direct {v2, p0, v0, v1}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    .line 1067
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v4, -0x2

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1068
    const/16 v1, 0xe

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1069
    invoke-virtual {p4, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1070
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x4270

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v4

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1071
    if-eqz v5, :cond_3

    .line 1072
    invoke-virtual {v3, v5}, Lcom/google/ads/searchads/SearchAdRequest;->setLocation(Landroid/location/Location;)Lcom/google/ads/AdRequest;

    :cond_3
    move-object v0, v2

    .line 1073
    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0, v3}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 1074
    check-cast v2, Lcom/google/ads/AdView;

    invoke-virtual {v2, p0}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    goto/16 :goto_1

    .line 1077
    :pswitch_5
    const/4 v0, 0x6

    invoke-direct {p0, p1, p2, v0, p4}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;Ljava/lang/String;ILandroid/view/ViewGroup;)V

    goto/16 :goto_1

    .line 1080
    :pswitch_6
    new-instance v1, Landroid/webkit/WebView;

    invoke-direct {v1, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 1081
    const-string v2, "javascript:<script type=\"text/javascript\">\npontiflex_data = {};\npontiflex_ad = {pid: 3425, options: {}};\npontiflex_ad.options.view = \"320x50\";\npontiflex_ad.options.appName = \"Sample App\";\n</script>\n<script type=\"text/javascript\" src=\"http://hhymxdkkdt5c.pflexads.com/html-sdk/mobilewebAds.js\"></script>"

    move-object v0, v1

    .line 1088
    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    move-object v0, v1

    .line 1089
    check-cast v0, Landroid/webkit/WebView;

    const-string v3, "text/html"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1091
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v2, 0x43a0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const/high16 v3, 0x4248

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1092
    invoke-virtual {p4, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 1095
    :pswitch_7
    new-instance v0, Lcom/amazon/device/ads/AdLayout;

    sget-object v1, Lcom/amazon/device/ads/AdLayout$AdSize;->AD_SIZE_320x50:Lcom/amazon/device/ads/AdLayout$AdSize;

    invoke-direct {v0, p0, v1}, Lcom/amazon/device/ads/AdLayout;-><init>(Landroid/content/Context;Lcom/amazon/device/ads/AdLayout$AdSize;)V

    .line 1096
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v2, 0x43a0

    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1097
    invoke-virtual {p4, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1098
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_4
    move-object v1, p1

    goto/16 :goto_0

    .line 1031
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method

.method private a(Z)V
    .locals 1
    .parameter

    .prologue
    .line 564
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 565
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->O:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 566
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 567
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 568
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 569
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 570
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 571
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/list/ListStations;II)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/list/ListStations;->addBrand(II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private b()Lgbis/gbandroid/entities/ListStationTab;
    .locals 5

    .prologue
    .line 489
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 490
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 491
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 492
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    .line 493
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    .line 495
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_3

    .line 500
    const/4 v0, 0x0

    :cond_2
    return-object v0

    .line 496
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListStationTab;

    .line 497
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getFuelType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 495
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2
    .parameter

    .prologue
    .line 1304
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$c;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1305
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$c;->interrupt()V

    .line 1306
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$c;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->a:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, p1}, Lgbis/gbandroid/activities/list/ListStations$c;-><init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/os/Handler;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    .line 1307
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$c;->start()V

    .line 1308
    return-void
.end method

.method private b(Landroid/widget/ListView;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 736
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 738
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 739
    iput-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    .line 740
    iput-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    .line 742
    :cond_0
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1149
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1469
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->d(Lgbis/gbandroid/entities/ListMessage;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/Station;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1499
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/Station;)V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventContextMenu(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lgbis/gbandroid/entities/ListMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 1369
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->B()V

    .line 1370
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    .line 1372
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->showLogin()V

    .line 1375
    :goto_0
    return-void

    .line 1374
    :cond_0
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->showCameraDialog(Lgbis/gbandroid/entities/Station;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 1
    .parameter

    .prologue
    .line 1202
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgbis/gbandroid/entities/ListStationFilter;->setHasPrices(Z)V

    .line 1203
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/list/ListStations;II)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/list/ListStations;->updateBrandVersion(II)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 7
    .parameter

    .prologue
    const-wide v5, 0x412e848000000000L

    const-wide/16 v3, 0x0

    .line 1357
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getSearchTerms()Ljava/lang/String;

    move-result-object v2

    .line 1358
    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1359
    const/4 v2, 0x0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    int-to-double v0, v0

    div-double v3, v0, v5

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    int-to-double v0, v0

    div-double v5, v0, v5

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/activities/list/ListStations;->addPlace(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v0

    .line 1361
    :goto_0
    return v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-wide v5, v3

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/activities/list/ListStations;->addPlace(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->H:Landroid/view/View;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 535
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 536
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v2

    .line 537
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getSortOrder()I

    move-result v3

    .line 538
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 539
    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v4, v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/ListResults;IZ)V

    .line 540
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 541
    invoke-virtual {v0, v2}, Lgbis/gbandroid/entities/ListStationTab;->setStationFilter(Lgbis/gbandroid/entities/ListStationFilter;)V

    .line 542
    invoke-virtual {v0, v3}, Lgbis/gbandroid/entities/ListStationTab;->setSortOrder(I)V

    .line 543
    invoke-static {v0}, Lgbis/gbandroid/activities/list/FilterStations;->filterStations(Lgbis/gbandroid/entities/ListStationTab;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListStationTab;->setListStationsFiltered(Ljava/util/List;)V

    .line 544
    return-void

    .line 539
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/widget/ListView;)V
    .locals 10
    .parameter

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x0

    const/4 v7, 0x0

    const v6, 0x7f0700ef

    const v5, 0x7f0700ee

    .line 745
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 746
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v1

    .line 747
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v2

    .line 748
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lt v3, v4, :cond_5

    .line 749
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v9, :cond_2

    .line 750
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/ListView;)V

    .line 751
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 753
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 754
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 756
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 758
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListResults;->getTotalStations()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getPage()I

    move-result v2

    mul-int/lit8 v2, v2, 0x64

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x3f80

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getPage()I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    .line 759
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 760
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 761
    iput-object v7, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    .line 763
    :cond_3
    invoke-virtual {p1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 764
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030041

    invoke-virtual {v0, v1, p1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    .line 765
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v7, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    goto :goto_0

    .line 769
    :cond_4
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->b(Landroid/widget/ListView;)V

    goto :goto_0

    .line 772
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v9, :cond_7

    .line 773
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/ListView;)V

    .line 774
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    new-instance v2, Lgbis/gbandroid/activities/list/k;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/list/k;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 780
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 781
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 782
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090027

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 783
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 785
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 786
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090028

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 789
    :cond_7
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->b(Landroid/widget/ListView;)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1368
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/entities/ListMessage;)V

    return-void
.end method

.method private c(Lgbis/gbandroid/entities/ListMessage;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x3

    const v4, 0x7f060001

    .line 1446
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 1447
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1448
    const-string v1, "time offset"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getTimeOffset()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1449
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1450
    const-string v1, "fuel regular"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1451
    const-string v1, "time spotted regular"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1465
    :cond_0
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1466
    invoke-virtual {p0, v0, v5}, Lgbis/gbandroid/activities/list/ListStations;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1467
    return-void

    .line 1453
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1454
    const-string v1, "fuel midgrade"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1455
    const-string v1, "time spotted midgrade"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1457
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1458
    const-string v1, "fuel premium"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1459
    const-string v1, "time spotted premium"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 1461
    :cond_3
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1462
    const-string v1, "fuel diesel"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 1463
    const-string v1, "time spotted diesel"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1356
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/list/ListStations;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->I:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 548
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->y:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 549
    const/4 v0, 0x2

    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    .line 558
    :goto_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 559
    iget v1, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListStationTab;->setSortOrder(I)V

    .line 560
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(I)V

    .line 561
    return-void

    .line 550
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->y:I

    if-ne v0, v2, :cond_1

    .line 551
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v2}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    .line 552
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->sort(Ljava/util/Comparator;)V

    .line 553
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    goto :goto_0

    .line 555
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->z()V

    .line 556
    iput v2, p0, Lgbis/gbandroid/activities/list/ListStations;->x:I

    goto :goto_0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/ListMessage;)V

    return-void
.end method

.method private d(Lgbis/gbandroid/entities/ListMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 1470
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/StationDetails;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 1471
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1472
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1473
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1474
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    const v6, 0x7f070174

    const v1, 0x7f030057

    const/4 v5, 0x1

    .line 574
    const v0, 0x7f0700ca

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    .line 575
    const v0, 0x7f0700c8

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->O:Landroid/widget/RelativeLayout;

    .line 576
    const v0, 0x7f0700cd

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->P:Landroid/widget/RelativeLayout;

    .line 577
    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    .line 578
    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    .line 579
    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    .line 580
    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    .line 581
    const v0, 0x7f0700c5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 582
    const v1, 0x7f0700cc

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->l:Landroid/widget/TextView;

    .line 583
    const v1, 0x7f0700cb

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->m:Landroid/widget/ImageView;

    .line 585
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/high16 v3, 0x4220

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v2, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 586
    const/high16 v1, 0x3f80

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 588
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0901cd

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 589
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 590
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 592
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0901ce

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 594
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 596
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0901cf

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 598
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 600
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0901d0

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 601
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 602
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 604
    const/4 v1, 0x4

    new-array v1, v1, [Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    aput-object v3, v1, v11

    iget-object v3, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    aput-object v3, v1, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    aput-object v4, v1, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    aput-object v4, v1, v3

    iput-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    .line 606
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 607
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 608
    const/16 v4, 0x50

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 609
    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v5, 0x7f08002a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 610
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 611
    iget-object v5, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v6, 0x7f08002a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 612
    new-instance v5, Landroid/view/View;

    invoke-direct {v5, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 613
    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v7, 0x7f08002a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 615
    new-instance v6, Landroid/view/View;

    invoke-direct {v6, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 616
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, 0x5

    const/4 v9, 0x2

    invoke-direct {v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 617
    new-instance v8, Landroid/view/View;

    invoke-direct {v8, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 618
    const/16 v9, 0x50

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 619
    iget-object v9, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v10, 0x7f080048

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 620
    iget-object v9, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v10, 0x7f080048

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 622
    invoke-virtual {v0, v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 623
    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 624
    invoke-virtual {v0, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 625
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 626
    invoke-virtual {v0, v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 627
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 628
    invoke-virtual {v0, v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 629
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 630
    invoke-virtual {v0, v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 632
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->f()V

    .line 634
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->O:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 636
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 637
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 642
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$b;

    invoke-direct {v0, p0, v11}, Lgbis/gbandroid/activities/list/ListStations$b;-><init>(Lgbis/gbandroid/activities/list/ListStations;B)V

    .line 643
    new-instance v1, Lgbis/gbandroid/activities/list/ListStations$k;

    invoke-direct {v1, p0, v11}, Lgbis/gbandroid/activities/list/ListStations$k;-><init>(Lgbis/gbandroid/activities/list/ListStations;B)V

    .line 644
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 645
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->O:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 646
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->P:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 647
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 648
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 649
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 650
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 652
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    new-instance v1, Lgbis/gbandroid/activities/list/j;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/list/j;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 659
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    return-object v0
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v2, 0x7f060001

    .line 674
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    aget-object v0, v0, v3

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 677
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 678
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    aget-object v0, v0, v4

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 680
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 681
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    aget-object v0, v0, v5

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 683
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->o:[Landroid/widget/RelativeLayout;

    aget-object v0, v0, v6

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    goto :goto_0
.end method

.method static synthetic g(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->e:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 696
    if-nez v0, :cond_0

    .line 697
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->C()V

    .line 703
    :goto_0
    return-void

    .line 699
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->j()V

    .line 700
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->h()V

    .line 701
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->m()V

    goto :goto_0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/list/ListStations;)F
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getDensity()F

    move-result v0

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 707
    :try_start_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    .line 708
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getSortOrder()I

    move-result v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 712
    :goto_0
    return-void

    .line 709
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 794
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/layouts/CustomListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    .line 795
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/layouts/CustomListView;->setOnRefreshListener(Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;)V

    .line 797
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    new-instance v1, Lgbis/gbandroid/activities/list/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/list/l;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 808
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->setTextFilterEnabled(Z)V

    .line 809
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    new-instance v1, Lgbis/gbandroid/activities/list/m;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/list/m;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 827
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    new-instance v1, Lgbis/gbandroid/activities/list/n;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/list/n;-><init>(Lgbis/gbandroid/activities/list/ListStations;)V

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 843
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/layouts/CustomListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 845
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/list/ListStations;)Z
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->isDBOpen()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lgbis/gbandroid/activities/list/ListStations;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 848
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    if-nez v0, :cond_0

    .line 849
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->i()V

    .line 850
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->y()V

    .line 851
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->k()V

    .line 852
    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 855
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 856
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getCenter()Lgbis/gbandroid/entities/Center;

    move-result-object v0

    .line 857
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 858
    const-string v2, "lastLatitude"

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Center;->getLatitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 859
    const-string v2, "lastLongitude"

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Center;->getLongitude()D

    move-result-wide v3

    double-to-float v0, v3

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 860
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 862
    :cond_1
    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1276
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->B()V

    return-void
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 866
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->x()V

    .line 867
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getSortOrder()I

    move-result v0

    .line 868
    if-eq v0, v2, :cond_0

    if-ne v0, v1, :cond_3

    .line 869
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v4

    if-nez v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v4

    if-nez v0, :cond_2

    .line 870
    :cond_1
    const v0, 0x7f090017

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;Z)V

    .line 883
    :goto_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->o()V

    .line 884
    return-void

    .line 872
    :cond_2
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v2}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    .line 873
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v1

    invoke-virtual {v1, v3}, Lgbis/gbandroid/entities/ListStationTab;->setSortOrder(I)V

    .line 874
    invoke-direct {p0, v3}, Lgbis/gbandroid/activities/list/ListStations;->a(I)V

    .line 875
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->sort(Ljava/util/Comparator;)V

    goto :goto_0

    .line 878
    :cond_3
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    .line 879
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v1

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/ListStationTab;->setSortOrder(I)V

    .line 880
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(I)V

    .line 881
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->sort(Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static synthetic l(Lgbis/gbandroid/activities/list/ListStations;)Z
    .locals 1
    .parameter

    .prologue
    .line 145
    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->A:Z

    return v0
.end method

.method private m()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 887
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getSortOrder()I

    move-result v1

    .line 888
    if-eq v1, v2, :cond_1

    .line 890
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->x()V

    .line 891
    if-eq v1, v3, :cond_0

    if-ne v1, v2, :cond_2

    .line 892
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v2}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    .line 895
    :goto_0
    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(I)V

    .line 896
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->sort(Ljava/util/Comparator;)V

    .line 897
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->o()V

    .line 899
    :cond_1
    return-void

    .line 894
    :cond_2
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v3}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    goto :goto_0
.end method

.method static synthetic m(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1283
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->C()V

    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 902
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getSortOrder()I

    move-result v0

    .line 903
    if-eq v0, v2, :cond_0

    .line 905
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->x()V

    .line 906
    if-ne v0, v1, :cond_1

    .line 907
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v2}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    .line 910
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/list/ListStations$j;->sort(Ljava/util/Comparator;)V

    .line 911
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->o()V

    .line 913
    :cond_0
    return-void

    .line 909
    :cond_1
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$f;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/list/ListStations$f;-><init>(Lgbis/gbandroid/activities/list/ListStations;I)V

    goto :goto_0
.end method

.method static synthetic n(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 847
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->j()V

    return-void
.end method

.method private o()V
    .locals 0

    .prologue
    .line 916
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->p()V

    .line 917
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->q()V

    .line 918
    return-void
.end method

.method static synthetic o(Lgbis/gbandroid/activities/list/ListStations;)Z
    .locals 1
    .parameter

    .prologue
    .line 119
    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->f:Z

    return v0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 921
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v0

    .line 922
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->H:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 923
    new-instance v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v1}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 924
    const/4 v2, -0x4

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 925
    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 927
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->I:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 928
    new-instance v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v1}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 929
    const/4 v2, -0x5

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 930
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 932
    :cond_1
    return-void
.end method

.method static synthetic p(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1192
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->y()V

    return-void
.end method

.method static synthetic q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;
    .locals 1
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 935
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v2

    .line 936
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 937
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 946
    :cond_0
    :goto_1
    return-void

    .line 938
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-lez v0, :cond_2

    .line 939
    new-instance v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 940
    const/4 v3, -0x6

    invoke-virtual {v0, v3}, Lgbis/gbandroid/entities/ListMessage;->setStationId(I)V

    .line 941
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    .line 937
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic r(Lgbis/gbandroid/activities/list/ListStations;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private r()Z
    .locals 6

    .prologue
    .line 949
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "lastReportedPriceDate"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 950
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x48190800

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 951
    const/4 v0, 0x0

    .line 953
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private s()I
    .locals 2

    .prologue
    .line 957
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 958
    const/4 v0, 0x2

    .line 962
    :goto_0
    return v0

    .line 959
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 960
    const/4 v0, 0x3

    goto :goto_0

    .line 962
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic s(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1310
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->F()V

    return-void
.end method

.method private t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 966
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 967
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    if-eqz v0, :cond_0

    .line 968
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getListMessages()Ljava/util/List;

    move-result-object v0

    .line 969
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 970
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    .line 971
    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getState()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 976
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 534
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->c()V

    return-void
.end method

.method static synthetic u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;
    .locals 1
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    return-object v0
.end method

.method private u()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 980
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adList"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 982
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adListScrollTime"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/list/ListStations;->B:I

    .line 983
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adListWaitTime"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/list/ListStations;->C:I

    .line 985
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adListKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 986
    iget-object v2, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adListUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 987
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lgbis/gbandroid/activities/list/ListStations;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 989
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->v()V

    .line 990
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->w()V

    .line 991
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 994
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adListTop"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations;->D:I

    .line 995
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adListTopKey"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 996
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adListTopUnit"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 997
    iget v2, p0, Lgbis/gbandroid/activities/list/ListStations;->D:I

    if-lez v2, :cond_0

    .line 998
    iget v2, p0, Lgbis/gbandroid/activities/list/ListStations;->D:I

    invoke-direct {p0, v0, v2, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->H:Landroid/view/View;

    .line 999
    :cond_0
    return-void
.end method

.method static synthetic v(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 901
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->n()V

    return-void
.end method

.method private w()V
    .locals 4

    .prologue
    .line 1002
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adListBottom"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations;->E:I

    .line 1003
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adListBottomKey"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1004
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adListBottomUnit"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1005
    iget v2, p0, Lgbis/gbandroid/activities/list/ListStations;->E:I

    if-lez v2, :cond_0

    .line 1006
    iget v2, p0, Lgbis/gbandroid/activities/list/ListStations;->E:I

    invoke-direct {p0, v0, v2, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->I:Landroid/view/View;

    .line 1007
    :cond_0
    return-void
.end method

.method static synthetic w(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 1
    .parameter

    .prologue
    .line 563
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Z)V

    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 1170
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v2

    .line 1171
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt v1, v0, :cond_1

    .line 1180
    :cond_0
    return-void

    .line 1173
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1174
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1175
    add-int/lit8 v1, v1, -0x1

    .line 1172
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic x(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1338
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->G()V

    return-void
.end method

.method static synthetic y(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/entities/ListResults;
    .locals 1
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1193
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/activities/list/FilterStations;->filterStations(Lgbis/gbandroid/entities/ListStationTab;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListStationTab;->setListStationsFiltered(Ljava/util/List;)V

    .line 1194
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->x()V

    .line 1195
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->A()V

    .line 1196
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/list/ListStations$j;-><init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    .line 1197
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->c(Landroid/widget/ListView;)V

    .line 1198
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->p:Lgbis/gbandroid/views/layouts/CustomListView;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/CustomListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1199
    return-void
.end method

.method static synthetic z(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/entities/ListStationTab;
    .locals 1
    .parameter

    .prologue
    .line 488
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 14

    .prologue
    const-wide v12, 0x412e848000000000L

    const-wide/high16 v10, 0x4014

    const-wide/16 v8, 0x0

    .line 1206
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1207
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStations()Ljava/util/List;

    move-result-object v2

    .line 1208
    if-eqz v2, :cond_0

    .line 1209
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1210
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 1223
    :cond_0
    return-void

    .line 1211
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 1212
    if-eqz v0, :cond_3

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    cmpl-double v4, v4, v8

    if-eqz v4, :cond_3

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    cmpl-double v4, v4, v8

    if-eqz v4, :cond_3

    .line 1213
    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v12

    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v6}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v6

    int-to-double v6, v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpl-double v4, v4, v10

    if-gtz v4, :cond_2

    iget-object v4, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v12

    iget-object v6, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v6}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v6

    int-to-double v6, v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpl-double v4, v4, v10

    if-lez v4, :cond_3

    .line 1214
    :cond_2
    new-instance v4, Landroid/location/Location;

    const-string v5, "Station"

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1215
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/location/Location;->setLatitude(D)V

    .line 1216
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getLongitude()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/location/Location;->setLongitude(D)V

    .line 1217
    iget-object v5, p0, Lgbis/gbandroid/activities/list/ListStations;->myLocation:Landroid/location/Location;

    invoke-virtual {v5, v4}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v4

    const/high16 v5, 0x447a

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x3ff9bfdf7e8038a0L

    div-double/2addr v4, v6

    .line 1218
    invoke-virtual {v0, v4, v5}, Lgbis/gbandroid/entities/ListMessage;->setDistance(D)V

    .line 1210
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x3

    const/4 v0, -0x1

    .line 396
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityAds;->onActivityResult(IILandroid/content/Intent;)V

    .line 397
    packed-switch p1, :pswitch_data_0

    .line 435
    :cond_0
    :goto_0
    return-void

    .line 399
    :pswitch_0
    if-eqz p2, :cond_0

    .line 401
    if-ne p2, v0, :cond_1

    .line 402
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->finish()V

    goto :goto_0

    .line 403
    :cond_1
    const/16 v0, 0x9

    if-ne p2, v0, :cond_2

    .line 404
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 405
    :cond_2
    if-ne p2, v1, :cond_0

    .line 407
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 410
    :pswitch_1
    if-eqz p2, :cond_0

    .line 412
    if-ne p2, v1, :cond_0

    .line 414
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 418
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 419
    const/4 v0, 0x0

    .line 420
    if-eqz p3, :cond_3

    .line 421
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 422
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListStationTab;

    .line 424
    :cond_3
    if-nez v0, :cond_4

    .line 425
    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_4
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->b()Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v1

    .line 428
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getListStationsFiltered()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/ListStationTab;->setListStationsFiltered(Ljava/util/List;)V

    .line 429
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationTab;->setStationFilter(Lgbis/gbandroid/entities/ListStationFilter;)V

    .line 430
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->y()V

    goto :goto_0

    .line 397
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 1978
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->N:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_1

    .line 1979
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->l()V

    .line 1980
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 2008
    :cond_0
    :goto_0
    return-void

    .line 1981
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->O:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_2

    .line 1982
    const v0, 0x7f09015a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1983
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->D()V

    goto :goto_0

    .line 1984
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->P:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_3

    .line 1985
    const v0, 0x7f090159

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1986
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->E()V

    goto :goto_0

    .line 1987
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_4

    .line 1988
    const v0, 0x7f0901cd

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1989
    const v0, 0x7f0901c9

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;)V

    .line 1990
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->Q:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    .line 1991
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->g()V

    goto :goto_0

    .line 1992
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_5

    .line 1993
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1994
    const v0, 0x7f0901ca

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;)V

    .line 1995
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->R:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    .line 1996
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->g()V

    goto :goto_0

    .line 1997
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_6

    .line 1998
    const v0, 0x7f0901cf

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1999
    const v0, 0x7f0901cb

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;)V

    .line 2000
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->S:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    .line 2001
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->g()V

    goto/16 :goto_0

    .line 2002
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    if-ne p1, v0, :cond_0

    .line 2003
    const v0, 0x7f0901d0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 2004
    const v0, 0x7f0901cc

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Ljava/lang/String;)V

    .line 2005
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->T:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->a(Landroid/widget/RelativeLayout;)V

    .line 2006
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->g()V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter

    .prologue
    .line 258
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 259
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->G:Lgbis/gbandroid/activities/list/ListStations$c;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$c;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->hideAds()V

    .line 261
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 171
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreate(Landroid/os/Bundle;)V

    .line 172
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/list/ListStations$g;-><init>(Lgbis/gbandroid/activities/list/ListStations;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    .line 173
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->B()V

    .line 174
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 175
    if-eqz v1, :cond_3

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->b:Ljava/util/List;

    .line 177
    const-string v0, "list"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListResults;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    if-eqz v0, :cond_2

    .line 179
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->openDB()Z

    .line 180
    const v0, 0x7f030038

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setContentView(I)V

    .line 181
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->u()V

    .line 182
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->e()V

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/entities/ListResults;I)V

    .line 184
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    const-string v2, "center latitude"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "center longitude"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->h:Lcom/google/android/maps/GeoPoint;

    .line 185
    const-string v0, "city"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->i:Ljava/lang/String;

    .line 186
    const-string v0, "state"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->j:Ljava/lang/String;

    .line 187
    const-string v0, "zip"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->k:Ljava/lang/String;

    .line 188
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->j()V

    .line 189
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getTotalStations()I

    .line 190
    const-string v0, "list sort order"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/list/ListStations;->y:I

    .line 192
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->y:I

    if-eqz v0, :cond_1

    .line 193
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->d()V

    .line 196
    :goto_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    .line 197
    if-eqz v0, :cond_0

    .line 198
    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    .line 199
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 206
    :cond_0
    :goto_1
    return-void

    .line 195
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->z()V

    goto :goto_0

    .line 202
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->finish()V

    goto :goto_1

    .line 204
    :cond_3
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->finish()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 348
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 349
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 350
    const v1, 0x7f0b0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 351
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 246
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onDestroy()V

    .line 247
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 254
    return-void
.end method

.method public onMyLocationChanged(Landroid/location/Location;)V
    .locals 1
    .parameter

    .prologue
    .line 1935
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onMyLocationChanged(Landroid/location/Location;)V

    .line 1936
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->z()V

    .line 1937
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->t:Lgbis/gbandroid/activities/list/ListStations$j;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$j;->notifyDataSetChanged()V

    .line 1938
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 356
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 357
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 391
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 360
    :pswitch_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->E()V

    goto :goto_0

    .line 363
    :pswitch_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->launchSettings()V

    goto :goto_0

    .line 366
    :pswitch_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->showHome()V

    goto :goto_0

    .line 369
    :pswitch_3
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->showCityZipSearch()V

    goto :goto_0

    .line 372
    :pswitch_4
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->openDB()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 373
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListResults;->getSearchTerms()Ljava/lang/String;

    move-result-object v1

    .line 374
    if-eqz v1, :cond_1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 375
    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getPlaceFromCityZip(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 377
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 378
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->I()V

    .line 381
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 380
    :cond_0
    const v2, 0x7f09009e

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    .line 383
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->I()V

    goto :goto_0

    .line 385
    :cond_2
    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :pswitch_5
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->H()V

    goto :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x7f07018b
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 233
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onPause()V

    .line 234
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->a()V

    .line 235
    return-void
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 1929
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f09015a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " Pull-to-refresh"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1930
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->D()V

    .line 1931
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 210
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onResume()V

    .line 211
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->openDB()Z

    .line 212
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    if-nez v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_0

    .line 215
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListResults;

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    .line 217
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->c:Lgbis/gbandroid/entities/ListResults;

    if-nez v0, :cond_1

    .line 218
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->finish()V

    .line 229
    :goto_0
    return-void

    .line 220
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/list/ListStations;->B()V

    .line 221
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->A:Z

    .line 224
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->u:Lgbis/gbandroid/activities/list/ListStations$g;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 225
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 226
    invoke-virtual {p0}, Lgbis/gbandroid/activities/list/ListStations;->showAds()V

    .line 227
    :cond_2
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->C:I

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->b(I)V

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 480
    const/4 v0, 0x0

    .line 481
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    .line 483
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->s:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 485
    :cond_0
    return-object v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1908
    add-int v0, p2, p3

    .line 1909
    iget-object v1, p0, Lgbis/gbandroid/activities/list/ListStations;->q:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1910
    if-ne v0, p4, :cond_0

    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->z:Z

    if-nez v0, :cond_0

    .line 1911
    iput-boolean v3, p0, Lgbis/gbandroid/activities/list/ListStations;->z:Z

    .line 1913
    new-instance v0, Lgbis/gbandroid/activities/list/ListStations$d;

    invoke-direct {v0, p0, p0, v2}, Lgbis/gbandroid/activities/list/ListStations$d;-><init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/activities/base/GBActivity;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 1914
    iget-object v0, p0, Lgbis/gbandroid/activities/list/ListStations;->w:Lgbis/gbandroid/utils/CustomAsyncTask;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1916
    :cond_0
    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->F:Z

    if-nez v0, :cond_1

    .line 1918
    iput-boolean v3, p0, Lgbis/gbandroid/activities/list/ListStations;->F:Z

    .line 1919
    iget v0, p0, Lgbis/gbandroid/activities/list/ListStations;->B:I

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->b(I)V

    .line 1921
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1925
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 239
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onStop()V

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/list/ListStations;->A:Z

    .line 242
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 1967
    invoke-virtual {p0, p0}, Lgbis/gbandroid/activities/list/ListStations;->registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V

    .line 1969
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1973
    const v0, 0x7f090251

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
