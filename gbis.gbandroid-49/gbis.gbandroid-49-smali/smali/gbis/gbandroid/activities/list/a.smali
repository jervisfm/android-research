.class final Lgbis/gbandroid/activities/list/a;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private final synthetic b:I

.field private final synthetic c:Lgbis/gbandroid/entities/ListStationFilter;

.field private final synthetic d:D

.field private final synthetic e:D

.field private final synthetic f:I

.field private final synthetic g:Landroid/widget/TextView;

.field private final synthetic h:I

.field private final synthetic i:Ljava/lang/String;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;ILgbis/gbandroid/entities/ListStationFilter;DDILandroid/widget/TextView;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/a;->a:Lgbis/gbandroid/activities/list/FilterStations;

    iput p2, p0, Lgbis/gbandroid/activities/list/a;->b:I

    iput-object p3, p0, Lgbis/gbandroid/activities/list/a;->c:Lgbis/gbandroid/entities/ListStationFilter;

    iput-wide p4, p0, Lgbis/gbandroid/activities/list/a;->d:D

    iput-wide p6, p0, Lgbis/gbandroid/activities/list/a;->e:D

    iput p8, p0, Lgbis/gbandroid/activities/list/a;->f:I

    iput-object p9, p0, Lgbis/gbandroid/activities/list/a;->g:Landroid/widget/TextView;

    iput p10, p0, Lgbis/gbandroid/activities/list/a;->h:I

    iput-object p11, p0, Lgbis/gbandroid/activities/list/a;->i:Ljava/lang/String;

    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 271
    iget v0, p0, Lgbis/gbandroid/activities/list/a;->b:I

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lgbis/gbandroid/activities/list/a;->c:Lgbis/gbandroid/entities/ListStationFilter;

    int-to-double v1, p2

    iget-wide v3, p0, Lgbis/gbandroid/activities/list/a;->d:D

    add-double/2addr v1, v3

    iget-wide v3, p0, Lgbis/gbandroid/activities/list/a;->e:D

    iget v5, p0, Lgbis/gbandroid/activities/list/a;->f:I

    int-to-double v5, v5

    mul-double/2addr v3, v5

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/ListStationFilter;->setDistance(D)V

    .line 275
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/a;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    int-to-double v2, p2

    iget-wide v4, p0, Lgbis/gbandroid/activities/list/a;->d:D

    add-double/2addr v2, v4

    iget v4, p0, Lgbis/gbandroid/activities/list/a;->f:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    iget v4, p0, Lgbis/gbandroid/activities/list/a;->h:I

    invoke-static {v2, v3, v4}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/list/a;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/a;->c:Lgbis/gbandroid/entities/ListStationFilter;

    int-to-double v1, p2

    iget-wide v3, p0, Lgbis/gbandroid/activities/list/a;->d:D

    add-double/2addr v1, v3

    iget-wide v3, p0, Lgbis/gbandroid/activities/list/a;->e:D

    iget v5, p0, Lgbis/gbandroid/activities/list/a;->f:I

    int-to-double v5, v5

    mul-double/2addr v3, v5

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/ListStationFilter;->setPrice(D)V

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .parameter

    .prologue
    .line 279
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .parameter

    .prologue
    .line 282
    return-void
.end method
