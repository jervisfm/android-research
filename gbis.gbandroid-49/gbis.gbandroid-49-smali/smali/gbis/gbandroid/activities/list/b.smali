.class final Lgbis/gbandroid/activities/list/b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private final synthetic b:Landroid/widget/CheckedTextView;

.field private final synthetic c:Lgbis/gbandroid/entities/ListStationTab;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;Landroid/widget/CheckedTextView;Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/b;->a:Lgbis/gbandroid/activities/list/FilterStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/b;->b:Landroid/widget/CheckedTextView;

    iput-object p3, p0, Lgbis/gbandroid/activities/list/b;->c:Lgbis/gbandroid/entities/ListStationTab;

    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lgbis/gbandroid/activities/list/b;->b:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 317
    :goto_0
    iget-object v1, p0, Lgbis/gbandroid/activities/list/b;->b:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 318
    iget-object v1, p0, Lgbis/gbandroid/activities/list/b;->c:Lgbis/gbandroid/entities/ListStationTab;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListStationTab;->getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/ListStationFilter;->setHasPrices(Z)V

    .line 319
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
