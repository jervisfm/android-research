.class final Lgbis/gbandroid/activities/list/c;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private final synthetic b:Lgbis/gbandroid/activities/list/FilterStations$c;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;Lgbis/gbandroid/activities/list/FilterStations$c;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/c;->a:Lgbis/gbandroid/activities/list/FilterStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/c;->b:Lgbis/gbandroid/activities/list/FilterStations$c;

    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 325
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationNameFilter;

    .line 326
    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationNameFilter;->isIncluded()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/StationNameFilter;->setIncluded(Z)V

    .line 327
    iget-object v0, p0, Lgbis/gbandroid/activities/list/c;->b:Lgbis/gbandroid/activities/list/FilterStations$c;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/FilterStations$c;->notifyDataSetChanged()V

    .line 328
    return-void

    .line 326
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
