.class final Lgbis/gbandroid/activities/list/d;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/FilterStations;

.field private final synthetic b:Lgbis/gbandroid/entities/ListStationTab;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/FilterStations;Lgbis/gbandroid/entities/ListStationTab;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/d;->a:Lgbis/gbandroid/activities/list/FilterStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/d;->b:Lgbis/gbandroid/entities/ListStationTab;

    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lgbis/gbandroid/activities/list/d;->b:Lgbis/gbandroid/entities/ListStationTab;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/d;->b:Lgbis/gbandroid/entities/ListStationTab;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/FilterStations;->filterStations(Lgbis/gbandroid/entities/ListStationTab;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListStationTab;->setListStationsFiltered(Ljava/util/List;)V

    .line 340
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 341
    const-string v1, "list"

    iget-object v2, p0, Lgbis/gbandroid/activities/list/d;->b:Lgbis/gbandroid/entities/ListStationTab;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 342
    iget-object v1, p0, Lgbis/gbandroid/activities/list/d;->a:Lgbis/gbandroid/activities/list/FilterStations;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lgbis/gbandroid/activities/list/FilterStations;->setResult(ILandroid/content/Intent;)V

    .line 343
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 344
    return-void
.end method
