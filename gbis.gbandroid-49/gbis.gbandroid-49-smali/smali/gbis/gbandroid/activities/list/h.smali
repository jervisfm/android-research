.class final Lgbis/gbandroid/activities/list/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private final synthetic b:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/h;->b:Landroid/widget/EditText;

    .line 1427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1430
    iget-object v0, p0, Lgbis/gbandroid/activities/list/h;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1431
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1432
    iget-object v1, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->c(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433
    iget-object v0, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09009f

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    .line 1434
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1439
    :goto_0
    return-void

    .line 1436
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09009d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 1438
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/h;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09009c

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
