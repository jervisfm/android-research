.class final Lgbis/gbandroid/activities/list/i;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;

.field private final synthetic b:Lgbis/gbandroid/entities/ListMessage;

.field private final synthetic c:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    iput-object p3, p0, Lgbis/gbandroid/activities/list/i;->c:[Ljava/lang/String;

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 312
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->G(Lgbis/gbandroid/activities/list/ListStations;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 313
    packed-switch p3, :pswitch_data_0

    .line 340
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->c:[Ljava/lang/String;

    aget-object v1, v1, p3

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)V

    .line 341
    return-void

    .line 315
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    goto :goto_0

    .line 318
    :pswitch_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    goto :goto_0

    .line 321
    :pswitch_2
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getLatitude()D

    move-result-wide v1

    iget-object v3, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListMessage;->getLongitude()D

    move-result-wide v3

    iget-object v5, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v5}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;DDLandroid/location/Location;)V

    goto :goto_0

    .line 324
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/Station;)V

    goto :goto_0

    .line 328
    :pswitch_3
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->c(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    goto :goto_0

    .line 331
    :pswitch_4
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->k(Lgbis/gbandroid/activities/list/ListStations;)V

    .line 332
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->H(Lgbis/gbandroid/activities/list/ListStations;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    const v2, 0x7f09003e

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/list/ListStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->showMessage(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->I(Lgbis/gbandroid/activities/list/ListStations;)V

    goto/16 :goto_0

    .line 337
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/i;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/i;->b:Lgbis/gbandroid/entities/ListMessage;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/Station;)V

    goto/16 :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
