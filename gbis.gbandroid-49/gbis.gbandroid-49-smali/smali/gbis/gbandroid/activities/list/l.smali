.class final Lgbis/gbandroid/activities/list/l;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/l;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 800
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 801
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v1

    if-lez v1, :cond_0

    .line 802
    iget-object v1, p0, Lgbis/gbandroid/activities/list/l;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->d(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    .line 803
    iget-object v0, p0, Lgbis/gbandroid/activities/list/l;->a:Lgbis/gbandroid/activities/list/ListStations;

    const-string v1, "ContextMenu"

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Ljava/lang/String;)V

    .line 804
    const/4 v0, 0x1

    .line 805
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
