.class final Lgbis/gbandroid/activities/list/n;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/list/ListStations;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    .line 827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 829
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 830
    if-eqz v0, :cond_0

    .line 831
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v1

    if-lez v1, :cond_1

    .line 832
    iget-object v1, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/list/ListStations;->b(Lgbis/gbandroid/activities/list/ListStations;Lgbis/gbandroid/entities/ListMessage;)V

    .line 841
    :cond_0
    :goto_0
    return-void

    .line 834
    :cond_1
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v1

    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 836
    iget-object v0, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->K(Lgbis/gbandroid/activities/list/ListStations;)V

    goto :goto_0

    .line 838
    :cond_2
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    .line 839
    iget-object v0, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v1}, Lgbis/gbandroid/activities/list/ListStations;->f(Lgbis/gbandroid/activities/list/ListStations;)Landroid/location/Location;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v2}, Lgbis/gbandroid/activities/list/ListStations;->y(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/entities/ListResults;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListResults;->getSearchTerms()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgbis/gbandroid/activities/list/n;->a:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v3}, Lgbis/gbandroid/activities/list/ListStations;->L(Lgbis/gbandroid/activities/list/ListStations;)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/activities/list/ListStations;->a(Lgbis/gbandroid/activities/list/ListStations;Landroid/location/Location;Ljava/lang/String;I)V

    goto :goto_0
.end method
