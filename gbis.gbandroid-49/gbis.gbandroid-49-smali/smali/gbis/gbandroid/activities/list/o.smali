.class final Lgbis/gbandroid/activities/list/o;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lcom/google/ads/AdListener;


# instance fields
.field a:I

.field final synthetic b:Lgbis/gbandroid/activities/list/ListStations;

.field private final synthetic c:Lcom/google/ads/AdRequest;

.field private final synthetic d:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/list/ListStations;Lcom/google/ads/AdRequest;Landroid/view/ViewGroup;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/list/o;->b:Lgbis/gbandroid/activities/list/ListStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/list/o;->c:Lcom/google/ads/AdRequest;

    iput-object p3, p0, Lgbis/gbandroid/activities/list/o;->d:Landroid/view/ViewGroup;

    .line 1118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1119
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/list/o;->a:I

    return-void
.end method


# virtual methods
.method public final onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 1144
    return-void
.end method

.method public final onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1133
    iget v0, p0, Lgbis/gbandroid/activities/list/o;->a:I

    if-gtz v0, :cond_1

    .line 1134
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->c:Lcom/google/ads/AdRequest;

    invoke-interface {p1, v0}, Lcom/google/ads/Ad;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 1135
    iget v0, p0, Lgbis/gbandroid/activities/list/o;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/list/o;->a:I

    .line 1141
    :cond_0
    :goto_0
    return-void

    .line 1137
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1138
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->b:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1139
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->b:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->q(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/views/layouts/CustomListView;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/views/layouts/CustomListView;->invalidateViews()V

    goto :goto_0
.end method

.method public final onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 1130
    return-void
.end method

.method public final onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 1127
    return-void
.end method

.method public final onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 1
    .parameter

    .prologue
    .line 1122
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->b:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1123
    iget-object v0, p0, Lgbis/gbandroid/activities/list/o;->b:Lgbis/gbandroid/activities/list/ListStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/list/ListStations;->u(Lgbis/gbandroid/activities/list/ListStations;)Lgbis/gbandroid/activities/list/ListStations$j;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/list/ListStations$j;->notifyDataSetChanged()V

    .line 1124
    :cond_0
    return-void
.end method
