.class final Lgbis/gbandroid/activities/map/MapStationDialog$a;
.super Lgbis/gbandroid/views/MyItemizedOverlay;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStationDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStationDialog;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStationDialog;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    .line 97
    invoke-static {p2}, Lgbis/gbandroid/activities/map/MapStationDialog$a;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lgbis/gbandroid/views/MyItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 98
    return-void
.end method


# virtual methods
.method protected final onTap(I)Z
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f070119

    const v6, 0x7f070118

    const v4, 0x7f070117

    const/4 v7, 0x4

    const/16 v5, 0x8

    .line 102
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-direct {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 104
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->a(Lgbis/gbandroid/activities/map/MapStationDialog;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03004c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 105
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 107
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v0

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v1, v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 108
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->c(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_1

    .line 109
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v3}, Lgbis/gbandroid/activities/map/MapStationDialog;->d(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v4}, Lgbis/gbandroid/activities/map/MapStationDialog;->c(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v3, "USA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    :goto_0
    const v0, 0x7f07011a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Updated: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v4}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v4

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    iget-object v6, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v6}, Lgbis/gbandroid/activities/map/MapStationDialog;->e(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lgbis/gbandroid/entities/Station;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :goto_1
    const v0, 0x7f070113

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v3}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v3

    invoke-virtual {v3}, Lgbis/gbandroid/entities/Station;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    const v0, 0x7f070114

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationDialog$a;->a:Lgbis/gbandroid/activities/map/MapStationDialog;

    invoke-static {v2}, Lgbis/gbandroid/activities/map/MapStationDialog;->b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Station;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 127
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 128
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/high16 v2, 0x3f00

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 129
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 130
    const/4 v0, 0x1

    return v0

    .line 114
    :cond_0
    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 117
    :cond_1
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    const v0, 0x7f07011a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
