.class public Lgbis/gbandroid/activities/map/MapStationDialog;
.super Lgbis/gbandroid/activities/base/GBActivityMap;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/map/MapStationDialog$a;
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/maps/MapView;

.field private b:Ljava/math/BigDecimal;

.field private c:Landroid/content/SharedPreferences;

.field private d:Ljava/lang/String;

.field private e:Lgbis/gbandroid/entities/Station;

.field private f:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/map/MapStationDialog;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a()V
    .locals 9

    .prologue
    const v5, 0x7f02007f

    const-wide v7, 0x412e848000000000L

    .line 75
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 77
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->a:Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v2

    .line 79
    const/16 v1, 0x10

    invoke-virtual {v2, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 80
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 81
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 82
    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    .line 83
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->f:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 84
    new-instance v1, Lgbis/gbandroid/views/CustomMapPin;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v5, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->f:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v6}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v4, v5, v0, v3}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    move-object v0, v1

    .line 87
    :goto_1
    new-instance v1, Lgbis/gbandroid/activities/map/MapStationDialog$a;

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v1, p0, v0, v3}, Lgbis/gbandroid/activities/map/MapStationDialog$a;-><init>(Lgbis/gbandroid/activities/map/MapStationDialog;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 88
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/Station;->getLatitude()D

    move-result-wide v3

    mul-double/2addr v3, v7

    double-to-int v3, v3

    iget-object v4, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v4}, Lgbis/gbandroid/entities/Station;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v7

    double-to-int v4, v4

    invoke-direct {v0, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 89
    invoke-virtual {v2, v0}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 90
    new-instance v2, Lcom/google/android/maps/OverlayItem;

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationDialog$a;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 92
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->a:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    return-void

    .line 84
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 86
    :cond_1
    new-instance v1, Lgbis/gbandroid/views/CustomMapPin;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v5, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020076

    invoke-static {v5, v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    sget-object v6, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v6}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v4, v5, v0, v3}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_2
.end method

.method static synthetic b(Lgbis/gbandroid/activities/map/MapStationDialog;)Lgbis/gbandroid/entities/Station;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->c:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    .line 141
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    const v1, 0x7f0901c9

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0901cd

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    .line 147
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    const v1, 0x7f0901ca

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0901ce

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    const v1, 0x7f0901cb

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0901cf

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    const v1, 0x7f0901cc

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0901d0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 156
    :cond_3
    const-string v0, ""

    goto/16 :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/math/BigDecimal;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    return-object v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 143
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/map/MapStationDialog;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityMap;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->setContentView(I)V

    .line 48
    const v0, 0x7f07016a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/MapView;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->a:Lcom/google/android/maps/MapView;

    .line 49
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_1

    .line 51
    const-string v1, "station"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    .line 52
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v0

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    .line 57
    :goto_0
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v1

    const-string v2, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/GBApplication;->getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->f:Landroid/graphics/Bitmap;

    .line 60
    :goto_1
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->c:Landroid/content/SharedPreferences;

    .line 61
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->b()V

    .line 62
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->populateButtons()V

    .line 63
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->a()V

    .line 64
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->b:Ljava/math/BigDecimal;

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationDialog;->finish()V

    goto :goto_1
.end method

.method public populateButtons()V
    .locals 3

    .prologue
    const v2, 0x7f07002f

    .line 67
    const v0, 0x7f070030

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->e:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/map/MapStationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/map/MapStationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationDialog;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const v0, 0x7f09024e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
