.class final Lgbis/gbandroid/activities/map/MapStations$a;
.super Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic b:Lgbis/gbandroid/activities/map/MapStations;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/content/Context;Lcom/google/android/maps/MapView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 521
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    .line 522
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;-><init>(Lgbis/gbandroid/activities/base/GBActivityMap;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    .line 523
    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .parameter

    .prologue
    .line 527
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->onLocationChanged(Landroid/location/Location;)V

    .line 528
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations$a;->isFollowMe()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->b(Lgbis/gbandroid/activities/map/MapStations;)Lcom/google/android/maps/MapController;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, v1, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 530
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->c(Lgbis/gbandroid/activities/map/MapStations;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->d(Lgbis/gbandroid/activities/map/MapStations;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$a;->b:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->e(Lgbis/gbandroid/activities/map/MapStations;)V

    .line 534
    :cond_0
    return-void
.end method
