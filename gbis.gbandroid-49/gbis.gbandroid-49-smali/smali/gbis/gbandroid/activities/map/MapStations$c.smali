.class final Lgbis/gbandroid/activities/map/MapStations$c;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStations;

.field private b:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 551
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    .line 552
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 553
    return-void
.end method

.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/activities/base/GBActivityMap;B)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 555
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    .line 556
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 557
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->b:Z

    .line 558
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    const v1, 0x7f07001a

    .line 562
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 563
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->f(Lgbis/gbandroid/activities/map/MapStations;)V

    .line 564
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->locateOnMap()V

    .line 566
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-boolean v0, v0, Lgbis/gbandroid/activities/map/MapStations;->zoomLevelFlag:Z

    if-nez v0, :cond_1

    .line 567
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 570
    :goto_0
    iget-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->b:Z

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->g(Lgbis/gbandroid/activities/map/MapStations;)V

    .line 573
    :cond_0
    return-void

    .line 569
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 577
    iget-boolean v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->b:Z

    if-nez v1, :cond_1

    .line 579
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v2, v2, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v2}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    iput-object v2, v1, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    .line 580
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/map/MapStations;->queryMapWebService()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    :cond_0
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 581
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "class android.view.ViewRoot$CalledFromWrongThreadException"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 586
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, v1, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, v1, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 587
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStations;->b(Lgbis/gbandroid/activities/map/MapStations;)Lcom/google/android/maps/MapController;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, v1, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 588
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v2, v2, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v3, v3, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v3}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v1, v0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    .line 589
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->queryMapWebService()V

    goto :goto_0

    .line 591
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations$c;->a:Lgbis/gbandroid/activities/map/MapStations;

    const v3, 0x7f09001f

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStations;->setMessage(Ljava/lang/String;)V

    goto :goto_1
.end method
