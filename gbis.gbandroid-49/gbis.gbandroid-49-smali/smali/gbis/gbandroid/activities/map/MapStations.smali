.class public Lgbis/gbandroid/activities/map/MapStations;
.super Lgbis/gbandroid/activities/map/MapStationsBase;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/map/MapStations$a;,
        Lgbis/gbandroid/activities/map/MapStations$b;,
        Lgbis/gbandroid/activities/map/MapStations$c;,
        Lgbis/gbandroid/activities/map/MapStations$d;
    }
.end annotation


# static fields
.field public static final COORDS:Ljava/lang/String; = "coordinates"


# instance fields
.field private a:Lcom/google/android/maps/MapController;

.field private b:Lgbis/gbandroid/activities/map/MapStations$d;

.field private c:Landroid/widget/RelativeLayout;

.field private d:Lgbis/gbandroid/activities/map/MapStations$b;

.field private e:Z

.field private f:Z

.field private g:Landroid/os/PowerManager$WakeLock;

.field private h:Lgbis/gbandroid/utils/DBHelper;

.field private i:Lgbis/gbandroid/utils/CustomAsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 204
    const v0, 0x7f07010f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->c:Landroid/widget/RelativeLayout;

    .line 205
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->g()V

    .line 206
    const v0, 0x7f070111

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 207
    new-instance v1, Lgbis/gbandroid/activities/map/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/map/a;-><init>(Lgbis/gbandroid/activities/map/MapStations;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    const v7, 0x7f060001

    .line 417
    const-string v1, "sm_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 419
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "fuelPreference"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 420
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 421
    const-string v1, "fuel regular"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    move-wide v2, v1

    .line 431
    :goto_0
    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 432
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 433
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    .line 434
    :goto_1
    if-lt v1, v4, :cond_4

    .line 443
    :goto_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->locateOnMap()V

    .line 446
    :cond_0
    return-void

    .line 423
    :cond_1
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 424
    const-string v1, "fuel midgrade"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    move-wide v2, v1

    goto :goto_0

    .line 426
    :cond_2
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 427
    const-string v1, "fuel premium"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    move-wide v2, v1

    goto :goto_0

    .line 429
    :cond_3
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 430
    const-string v1, "fuel diesel"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    move-wide v2, v1

    goto :goto_0

    .line 435
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    .line 436
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v5

    if-ne v5, v6, :cond_5

    .line 437
    invoke-virtual {v0, v2, v3}, Lgbis/gbandroid/entities/ListMessage;->setPrice(D)V

    .line 438
    const-string v1, "time spotted"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListMessage;->setTimeSpotted(Ljava/lang/String;)V

    .line 439
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getNowOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ListMessage;->setTimeOffset(I)V

    goto :goto_2

    .line 434
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_6
    move-wide v2, v4

    goto/16 :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 0
    .parameter

    .prologue
    .line 492
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->i()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/entities/ListResults;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 497
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/map/MapStations;->a(Lgbis/gbandroid/entities/ListResults;)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/ListResults;)V
    .locals 3
    .parameter

    .prologue
    .line 498
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/list/ListStations;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 499
    const-string v1, "list"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 500
    const-string v1, "center latitude"

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 501
    const-string v1, "center longitude"

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 502
    const-string v1, "city"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 503
    const-string v1, "state"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 504
    const-string v1, "zip"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->unregisterPreferencesListener()V

    .line 506
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->startActivityForResult(Landroid/content/Intent;I)V

    .line 507
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->setResult(I)V

    .line 508
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->finish()V

    .line 509
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/map/MapStations;)Lcom/google/android/maps/MapController;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 375
    new-instance v0, Lgbis/gbandroid/activities/map/MapStations$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/map/MapStations$b;-><init>(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->d:Lgbis/gbandroid/activities/map/MapStations$b;

    .line 376
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->d:Lgbis/gbandroid/activities/map/MapStations$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 377
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->d:Lgbis/gbandroid/activities/map/MapStations$b;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 378
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->cancel(Z)Z

    .line 383
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->e()V

    .line 384
    new-instance v0, Lgbis/gbandroid/activities/map/MapStations$c;

    invoke-direct {v0, p0, p0, v2}, Lgbis/gbandroid/activities/map/MapStations$c;-><init>(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/activities/base/GBActivityMap;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 385
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 386
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/map/MapStations;)Z
    .locals 1
    .parameter

    .prologue
    .line 542
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->k()Z

    move-result v0

    return v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 389
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    .line 390
    new-instance v0, Lgbis/gbandroid/activities/map/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/map/b;-><init>(Lgbis/gbandroid/activities/map/MapStations;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 391
    new-instance v1, Lgbis/gbandroid/queries/ListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getLocation()Landroid/location/Location;

    move-result-object v3

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 392
    const-string v0, ""

    const-string v2, ""

    const-string v3, ""

    iget-object v4, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1, v0, v2, v3, v4}, Lgbis/gbandroid/queries/ListQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 393
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/map/MapStations;)Z
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations;->f:Z

    return v0
.end method

.method private e()V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->h()V

    .line 401
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 0
    .parameter

    .prologue
    .line 380
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->c()V

    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    .line 449
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v2, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 450
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 451
    const v0, 0x7f07003e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 452
    const v1, 0x7f07003f

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 453
    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    .line 454
    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    const/16 v7, 0x1e

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v4, v5

    .line 453
    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 456
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 457
    const v4, 0x7f090120

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 458
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 459
    const v3, 0x7f09013d

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    const v0, 0x7f0901a3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lgbis/gbandroid/activities/map/c;

    invoke-direct {v3, p0, v1}, Lgbis/gbandroid/activities/map/c;-><init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/widget/EditText;)V

    invoke-virtual {v2, v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 474
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 476
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 0
    .parameter

    .prologue
    .line 482
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 483
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->c:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->c:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 485
    :cond_0
    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 1
    .parameter

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations;->f:Z

    return-void
.end method

.method static synthetic h(Lgbis/gbandroid/activities/map/MapStations;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->c:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->c:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 490
    :cond_0
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/map/MapStations;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 494
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->loadMapPins()V

    .line 495
    return-void
.end method

.method private j()D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000

    .line 538
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 539
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v3}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 538
    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 539
    const-wide v2, 0x405bc00000000000L

    .line 538
    mul-double/2addr v0, v2

    .line 539
    const-wide v2, 0x3f50624dd2f1a9fcL

    .line 538
    div-double/2addr v0, v2

    .line 539
    const-wide v2, 0x412e848000000000L

    .line 538
    div-double/2addr v0, v2

    return-wide v0
.end method

.method static synthetic j(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 0
    .parameter

    .prologue
    .line 388
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->d()V

    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/map/MapStations;)V
    .locals 0
    .parameter

    .prologue
    .line 374
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->b()V

    return-void
.end method

.method private k()Z
    .locals 4

    .prologue
    .line 543
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->j()D

    move-result-wide v0

    const-wide/high16 v2, 0x4079

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 544
    const/4 v0, 0x1

    .line 546
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected addPlace(Ljava/lang/String;)Z
    .locals 9
    .parameter

    .prologue
    const-wide v7, 0x412e848000000000L

    .line 479
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->h:Lgbis/gbandroid/utils/DBHelper;

    const/4 v2, 0x0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    int-to-double v3, v1

    div-double/2addr v3, v7

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v1

    int-to-double v5, v1

    div-double/2addr v5, v7

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/DBHelper;->addSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v0

    return v0
.end method

.method public closeDialog()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->progress:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 414
    return-void
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 409
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->getLocation()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public getMyLocation()Lcom/google/android/maps/GeoPoint;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    return-object v0
.end method

.method public loadMapPins()V
    .locals 2

    .prologue
    .line 364
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->cancel(Z)Z

    .line 366
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->e()V

    .line 367
    new-instance v0, Lgbis/gbandroid/activities/map/MapStations$c;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/map/MapStations$c;-><init>(Lgbis/gbandroid/activities/map/MapStations;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 368
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->i:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :goto_0
    return-void

    .line 370
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->g()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x3

    const/4 v0, -0x1

    .line 308
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/map/MapStationsBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 309
    packed-switch p1, :pswitch_data_0

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 311
    :pswitch_0
    if-eqz p2, :cond_0

    .line 314
    if-ne p2, v0, :cond_1

    .line 315
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->setResult(I)V

    .line 316
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->finish()V

    goto :goto_0

    .line 317
    :cond_1
    const/16 v0, 0x9

    if-ne p2, v0, :cond_2

    .line 318
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 319
    :cond_2
    if-ne p2, v1, :cond_0

    .line 320
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 323
    :pswitch_1
    if-eqz p2, :cond_0

    .line 325
    if-ne p2, v1, :cond_0

    .line 326
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 199
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 200
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->loadMapPins()V

    .line 201
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onCreate(Landroid/os/Bundle;)V

    .line 74
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v2

    .line 75
    const-string v1, ""

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->userMessage:Ljava/lang/String;

    .line 76
    iput-boolean v3, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    .line 77
    iput-boolean v4, p0, Lgbis/gbandroid/activities/map/MapStations;->f:Z

    .line 78
    new-instance v1, Lgbis/gbandroid/activities/map/MapStations$d;

    invoke-direct {v1, p0, v3}, Lgbis/gbandroid/activities/map/MapStations$d;-><init>(Lgbis/gbandroid/activities/map/MapStations;B)V

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->b:Lgbis/gbandroid/activities/map/MapStations$d;

    .line 79
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    .line 81
    :try_start_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 82
    if-eqz v3, :cond_1

    .line 83
    const v1, 0x7f03004a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->setContentView(I)V

    .line 84
    const v1, 0x7f070018

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/views/StationsMapView;

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 85
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v1, p0}, Lgbis/gbandroid/views/StationsMapView;->setControl(Lgbis/gbandroid/activities/map/MapStationsBase;)V

    .line 86
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v1}, Lgbis/gbandroid/views/StationsMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    .line 87
    if-nez v2, :cond_2

    .line 88
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 90
    const-string v1, "map"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/MapResults;

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapResults:Lgbis/gbandroid/entities/MapResults;

    .line 91
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const-string v2, "center latitude"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v4, "center longitude"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    .line 92
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapResults:Lgbis/gbandroid/entities/MapResults;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/MapResults;->getListMessage()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    .line 94
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/MapResults;->getTotalStations()I

    move-result v1

    .line 95
    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMoreStationsMessage(I)V

    .line 103
    :cond_0
    :goto_0
    new-instance v1, Lgbis/gbandroid/activities/map/MapStations$a;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-direct {v1, p0, p0, v2}, Lgbis/gbandroid/activities/map/MapStations$a;-><init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    .line 104
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v1}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapOverlays:Ljava/util/List;

    .line 105
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapOverlays:Ljava/util/List;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->a()V

    .line 107
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->locateOnMap()V

    .line 108
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 109
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/StationsMapView;->setBuiltInZoomControls(Z)V

    .line 110
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/StationsMapView;->setSatellite(Z)V

    .line 111
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableMyLocation()Z

    .line 112
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableCompass()Z

    .line 113
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 114
    const/4 v2, 0x6

    const-string v3, "WakeLock"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    .line 119
    :cond_1
    :goto_1
    return-void

    .line 98
    :cond_2
    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    move-object v0, v2

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    const-string v4, "zoom"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 99
    new-instance v3, Lcom/google/android/maps/GeoPoint;

    move-object v0, v2

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    const-string v4, "center latitude"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    check-cast v2, Landroid/os/Bundle;

    const-string v4, "center longitude"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v3, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    .line 100
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->queryMapWebService()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v1

    .line 117
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->finish()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 218
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 219
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 220
    const v1, 0x7f0b0004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 221
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onDestroy()V

    .line 189
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->removeAllViews()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 193
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->b:Lgbis/gbandroid/activities/map/MapStations$d;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 243
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 285
    :pswitch_0
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 246
    :pswitch_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->c()V

    goto :goto_0

    .line 250
    :pswitch_2
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f09017d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    const/4 v1, 0x1

    iput-boolean v1, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    .line 252
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 253
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    iget-boolean v2, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->setFollowMe(Z)V

    .line 254
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 261
    :catch_0
    move-exception v1

    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    .line 257
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 258
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    iget-boolean v2, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->setFollowMe(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 265
    :pswitch_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->b()V

    goto :goto_0

    .line 269
    :pswitch_4
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->showHome()V

    goto :goto_0

    .line 272
    :pswitch_5
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->launchSettings()V

    goto :goto_0

    .line 275
    :pswitch_6
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->showCityZipSearch()V

    goto :goto_0

    .line 278
    :pswitch_7
    new-instance v1, Lgbis/gbandroid/utils/DBHelper;

    invoke-direct {v1, p0}, Lgbis/gbandroid/utils/DBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->h:Lgbis/gbandroid/utils/DBHelper;

    .line 279
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->h:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v1}, Lgbis/gbandroid/utils/DBHelper;->openDB()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStations;->f()V

    goto :goto_0

    .line 282
    :cond_1
    const v1, 0x7f090002

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x7f07018e
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onPause()V

    .line 171
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->disableCompass()V

    .line 172
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->disableMyLocation()V

    .line 173
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->setFollowMe(Z)V

    .line 174
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 234
    iget-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    if-nez v0, :cond_0

    .line 235
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f09017d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 238
    :goto_0
    return v2

    .line 237
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f09017e

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onResume()V

    .line 124
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    .line 127
    const-string v1, "map"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/MapResults;

    .line 128
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0}, Lgbis/gbandroid/entities/MapResults;->getListMessage()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->listMessages:Ljava/util/List;

    .line 132
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    if-nez v0, :cond_1

    .line 133
    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/StationsMapView;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 134
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/StationsMapView;->setControl(Lgbis/gbandroid/activities/map/MapStationsBase;)V

    .line 135
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/StationsMapView;->setBuiltInZoomControls(Z)V

    .line 136
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/StationsMapView;->setSatellite(Z)V

    .line 138
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapOverlays:Ljava/util/List;

    if-nez v0, :cond_2

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapOverlays:Ljava/util/List;

    .line 140
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-nez v0, :cond_3

    .line 141
    new-instance v0, Lgbis/gbandroid/activities/map/MapStations$a;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/map/MapStations$a;-><init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    .line 142
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapOverlays:Ljava/util/List;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableMyLocation()Z

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableCompass()Z

    .line 146
    iget-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    if-eqz v0, :cond_7

    .line 147
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    if-nez v0, :cond_4

    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    .line 149
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-nez v0, :cond_5

    .line 150
    new-instance v0, Lgbis/gbandroid/activities/map/MapStations$a;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/map/MapStations$a;-><init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    .line 151
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->a:Lcom/google/android/maps/MapController;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 152
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    iget-boolean v1, p0, Lgbis/gbandroid/activities/map/MapStations;->e:Z

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->setFollowMe(Z)V

    .line 153
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_6

    .line 154
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 155
    const/4 v1, 0x6

    const-string v2, "WakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    .line 157
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 159
    :cond_7
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 299
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 300
    const-string v1, "zoom"

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v2}, Lgbis/gbandroid/views/StationsMapView;->getZoomLevel()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 301
    const-string v1, "center latitude"

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 302
    const-string v1, "center longitude"

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStations;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 303
    return-object v0
.end method

.method public onSearchRequested()Z
    .locals 4

    .prologue
    .line 290
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->setAnalyticsPageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PhoneButton"

    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/map/MapStations;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 291
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStations;->showCityZipSearch()V

    .line 292
    const/4 v0, 0x1

    return v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onStart()V

    .line 164
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->b:Lgbis/gbandroid/activities/map/MapStations$d;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 165
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onStop()V

    .line 181
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->h:Lgbis/gbandroid/utils/DBHelper;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->h:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->closeDB()V

    .line 183
    :cond_0
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626
    const v0, 0x7f090252

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterPreferencesListener()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStations;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStations;->b:Lgbis/gbandroid/activities/map/MapStations$d;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 397
    return-void
.end method
