.class public Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStationsBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LogoDownloadAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStationsBase;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:F

.field private e:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStationsBase;Landroid/content/Context;IFZ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 331
    iput-object p2, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->b:Landroid/content/Context;

    .line 332
    iput p3, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->c:I

    .line 333
    iput p4, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->d:F

    .line 334
    iput-boolean p5, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->e:Z

    .line 335
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->b:Landroid/content/Context;

    const-string v1, "/Android/data/gbis.gbandroid/cache/Ads"

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/ImageLoader;->getImageDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 340
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v1

    iget v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->c:I

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-static {v3}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(Lgbis/gbandroid/activities/map/MapStationsBase;)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/GBApplication;->getImageAdUrl(IF)Ljava/lang/String;

    move-result-object v1

    .line 341
    iget v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->c:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lgbis/gbandroid/utils/ImageLoader;->getBitmap(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 342
    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter

    .prologue
    .line 347
    if-eqz p1, :cond_0

    .line 348
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    iget v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->d:F

    iget-boolean v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->e:Z

    invoke-static {v0, v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(Lgbis/gbandroid/activities/map/MapStationsBase;FZ)V

    .line 350
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
