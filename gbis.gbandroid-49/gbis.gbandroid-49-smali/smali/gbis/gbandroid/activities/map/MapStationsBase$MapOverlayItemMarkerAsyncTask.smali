.class public Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStationsBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MapOverlayItemMarkerAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStationsBase;

.field private b:Lcom/google/android/maps/OverlayItem;

.field private c:Lgbis/gbandroid/views/StationsMapView;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStationsBase;Lcom/google/android/maps/OverlayItem;Lgbis/gbandroid/views/StationsMapView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 359
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 360
    iput-object p2, p0, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->b:Lcom/google/android/maps/OverlayItem;

    .line 361
    iput-object p3, p0, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->c:Lgbis/gbandroid/views/StationsMapView;

    .line 362
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .parameter

    .prologue
    .line 379
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 380
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 381
    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 382
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 383
    new-instance v1, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-direct {v1, v0}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 384
    invoke-virtual {v1}, Lorg/apache/http/entity/BufferedHttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 385
    return-object v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 366
    .line 369
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-static {v0}, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 370
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 371
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 5
    .parameter

    .prologue
    .line 390
    if-eqz p1, :cond_0

    .line 391
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 392
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 393
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->b:Lcom/google/android/maps/OverlayItem;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/OverlayItem;->setMarker(Landroid/graphics/drawable/Drawable;)V

    .line 394
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->c:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->invalidate()V

    .line 396
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
