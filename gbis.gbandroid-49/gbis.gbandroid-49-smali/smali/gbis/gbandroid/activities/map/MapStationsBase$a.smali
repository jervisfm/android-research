.class final Lgbis/gbandroid/activities/map/MapStationsBase$a;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/map/MapStationsBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lgbis/gbandroid/entities/ListMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStationsBase;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/map/MapStationsBase;)V
    .locals 0
    .parameter

    .prologue
    .line 312
    iput-object p1, p0, Lgbis/gbandroid/activities/map/MapStationsBase$a;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    return-void
.end method

.method private static a(Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/entities/ListMessage;)I
    .locals 4
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 316
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x1

    .line 320
    :goto_0
    return v0

    .line 318
    :cond_0
    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    .line 319
    const/4 v0, -0x1

    goto :goto_0

    .line 320
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/ListMessage;

    check-cast p2, Lgbis/gbandroid/entities/ListMessage;

    invoke-static {p1, p2}, Lgbis/gbandroid/activities/map/MapStationsBase$a;->a(Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/entities/ListMessage;)I

    move-result v0

    return v0
.end method
