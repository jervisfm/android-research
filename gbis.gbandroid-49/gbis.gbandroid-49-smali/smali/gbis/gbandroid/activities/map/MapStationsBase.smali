.class public abstract Lgbis/gbandroid/activities/map/MapStationsBase;
.super Lgbis/gbandroid/activities/base/GBActivityMap;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;,
        Lgbis/gbandroid/activities/map/MapStationsBase$MapOverlayItemMarkerAsyncTask;,
        Lgbis/gbandroid/activities/map/MapStationsBase$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field protected center:Lcom/google/android/maps/GeoPoint;

.field protected itemizedoverlay:Lgbis/gbandroid/views/MapStationOverlay;

.field protected listMessages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field protected logoDefault:Landroid/graphics/Bitmap;

.field protected mapOverlays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/maps/Overlay;",
            ">;"
        }
    .end annotation
.end field

.field protected mapResults:Lgbis/gbandroid/entities/MapResults;

.field protected mapView:Lgbis/gbandroid/views/StationsMapView;

.field protected mappinDefault:Landroid/graphics/Bitmap;

.field protected mappinFocus:Landroid/graphics/Bitmap;

.field protected mappinPress:Landroid/graphics/Bitmap;

.field protected myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

.field protected zoomLevelFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;-><init>()V

    .line 69
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    .line 70
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    .line 56
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/map/MapStationsBase;)F
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->getDensity()F

    move-result v0

    return v0
.end method

.method private a(I)Landroid/graphics/Bitmap;
    .locals 3
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 260
    :goto_0
    return-object v0

    .line 255
    :cond_0
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    const-string v1, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v0, p1, v1}, Lgbis/gbandroid/GBApplication;->getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_1

    .line 257
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 259
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->logoDefault:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->listMessages:Ljava/util/List;

    new-instance v1, Lgbis/gbandroid/activities/map/MapStationsBase$a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/map/MapStationsBase$a;-><init>(Lgbis/gbandroid/activities/map/MapStationsBase;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 308
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->listMessages:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 309
    return-void
.end method

.method private a(FZ)V
    .locals 10
    .parameter
    .parameter

    .prologue
    .line 148
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->listMessages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    const/4 v1, 0x0

    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_5

    .line 196
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    .line 197
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-lt v1, v2, :cond_6

    .line 200
    return-void

    .line 149
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgbis/gbandroid/entities/ListMessage;

    .line 151
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 152
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 153
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    const-wide/16 v8, 0x0

    cmpl-double v3, v3, v8

    if-eqz v3, :cond_4

    .line 154
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v3

    const-string v4, "USA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 155
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v3

    .line 156
    if-eqz p2, :cond_1

    .line 157
    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v8, 0x101009c

    aput v8, v4, v5

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinFocus:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 158
    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v8, 0x10100a1

    aput v8, v4, v5

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinPress:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 160
    :cond_1
    const/4 v4, 0x0

    new-array v4, v4, [I

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinDefault:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v8, v0, v3, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 176
    :goto_3
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    .line 177
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v3

    .line 178
    const/16 v4, -0xd

    rsub-int/lit8 v3, v3, 0x0

    add-int/lit8 v0, v0, -0xd

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v3, v0, v5}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 179
    new-instance v0, Lgbis/gbandroid/views/MapStationOverlay;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-object v3, p0

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/views/MapStationOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;ZLcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->itemizedoverlay:Lgbis/gbandroid/views/MapStationOverlay;

    .line 181
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getLatitude()D

    move-result-wide v3

    const-wide v8, 0x412e848000000000L

    mul-double/2addr v3, v8

    double-to-int v1, v3

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getLongitude()D

    move-result-wide v3

    const-wide v8, 0x412e848000000000L

    mul-double/2addr v3, v8

    double-to-int v3, v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 182
    new-instance v1, Lcom/google/android/maps/OverlayItem;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->itemizedoverlay:Lgbis/gbandroid/views/MapStationOverlay;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/MapStationOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 185
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->itemizedoverlay:Lgbis/gbandroid/views/MapStationOverlay;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 162
    :cond_2
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v3

    .line 163
    if-eqz p2, :cond_3

    .line 164
    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v8, 0x101009c

    aput v8, v4, v5

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinFocus:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 165
    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v8, 0x10100a1

    aput v8, v4, v5

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinPress:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 167
    :cond_3
    const/4 v4, 0x0

    new-array v4, v4, [I

    new-instance v5, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v8, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinDefault:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v8, v0, v3, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 170
    :cond_4
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v3

    const/4 v5, 0x2

    invoke-static {v3, v4, v5}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    .line 171
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, 0x101009c

    aput v5, v3, v4

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinFocus:Landroid/graphics/Bitmap;

    const-string v8, ""

    invoke-direct {v4, v5, v0, v8, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 172
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, 0x10100a1

    aput v5, v3, v4

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinPress:Landroid/graphics/Bitmap;

    const-string v8, ""

    invoke-direct {v4, v5, v0, v8, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 173
    const/4 v3, 0x0

    new-array v3, v3, [I

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinDefault:Landroid/graphics/Bitmap;

    const-string v8, ""

    invoke-direct {v4, v5, v0, v8, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 188
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/Overlay;

    .line 189
    instance-of v2, v0, Lgbis/gbandroid/views/MapStationOverlay;

    if-eqz v2, :cond_8

    .line 190
    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 191
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 192
    add-int/lit8 v0, v1, -0x1

    .line 187
    :goto_4
    add-int/lit8 v1, v0, 0x1

    goto/16 :goto_1

    .line 198
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 199
    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/Overlay;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    :cond_8
    move v0, v1

    goto :goto_4
.end method

.method private a(IFZ)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    new-instance v0, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;

    move-object v1, p0

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;-><init>(Lgbis/gbandroid/activities/map/MapStationsBase;Landroid/content/Context;IFZ)V

    .line 248
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStationsBase$LogoDownloadAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 250
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/map/MapStationsBase;FZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 202
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/map/MapStationsBase;->b(FZ)V

    return-void
.end method

.method private b(I)Landroid/graphics/Bitmap;
    .locals 3
    .parameter

    .prologue
    .line 264
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 271
    :goto_0
    return-object v0

    .line 266
    :cond_0
    const-string v0, "/Android/data/gbis.gbandroid/cache/Ads"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lgbis/gbandroid/utils/ImageLoader;->getExistentImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_1

    .line 268
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 270
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->logoDefault:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private b(FZ)V
    .locals 13
    .parameter
    .parameter

    .prologue
    const/4 v12, 0x1

    const-wide v10, 0x412e848000000000L

    const/4 v6, 0x0

    .line 204
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 206
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/MapResults;->getChoiceHotels()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 232
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 240
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    .line 241
    :goto_2
    if-lt v6, v1, :cond_4

    .line 244
    return-void

    .line 206
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgbis/gbandroid/entities/ChoiceHotels;

    .line 207
    const-string v0, "/Android/data/gbis.gbandroid/cache/Ads"

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getAdId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lgbis/gbandroid/utils/ImageLoader;->isImageExistant(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 209
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getAdId()I

    move-result v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->b(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 211
    new-array v3, v12, [I

    const v4, 0x101009c

    aput v4, v3, v6

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinFocus:Landroid/graphics/Bitmap;

    const-string v9, ""

    invoke-direct {v4, v5, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 212
    new-array v3, v12, [I

    const v4, 0x10100a1

    aput v4, v3, v6

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinPress:Landroid/graphics/Bitmap;

    const-string v9, ""

    invoke-direct {v4, v5, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 213
    new-array v3, v6, [I

    new-instance v4, Lgbis/gbandroid/views/CustomMapPin;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinDefault:Landroid/graphics/Bitmap;

    const-string v9, ""

    invoke-direct {v4, v5, v0, v9, p1}, Lgbis/gbandroid/views/CustomMapPin;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;F)V

    invoke-virtual {v1, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 216
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    .line 217
    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v3

    .line 218
    const/16 v4, -0xd

    rsub-int/lit8 v3, v3, 0x0

    add-int/lit8 v0, v0, -0xd

    invoke-virtual {v1, v4, v3, v0, v6}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 219
    new-instance v0, Lgbis/gbandroid/views/MapAdOverlay;

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-object v3, p0

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/views/MapAdOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;ZLcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 221
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getLatitude()D

    move-result-wide v3

    mul-double/2addr v3, v10

    double-to-int v3, v3

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v10

    double-to-int v4, v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 222
    new-instance v3, Lcom/google/android/maps/OverlayItem;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getBrandId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v4, v2}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-virtual {v0, v3}, Lgbis/gbandroid/views/MapAdOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 226
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 228
    :cond_1
    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getAdId()I

    move-result v0

    invoke-direct {p0, v0, p1, p2}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(IFZ)V

    goto/16 :goto_0

    .line 233
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/Overlay;

    .line 234
    instance-of v2, v0, Lgbis/gbandroid/views/MapAdOverlay;

    if-eqz v2, :cond_6

    .line 235
    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/MapResults;->isAdsOnTop()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 236
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 237
    add-int/lit8 v0, v1, -0x1

    .line 232
    :goto_3
    add-int/lit8 v1, v0, 0x1

    goto/16 :goto_1

    .line 242
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 243
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/Overlay;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public getMapDistance()D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000

    .line 298
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    if-nez v0, :cond_0

    .line 299
    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/StationsMapView;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 300
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->center:Lcom/google/android/maps/GeoPoint;

    if-nez v0, :cond_1

    .line 301
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->center:Lcom/google/android/maps/GeoPoint;

    .line 302
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v1}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 303
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->center:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v3}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    .line 302
    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 303
    const-wide v2, 0x405bc00000000000L

    .line 302
    mul-double/2addr v0, v2

    .line 303
    const-wide v2, 0x3f50624dd2f1a9fcL

    .line 302
    div-double/2addr v0, v2

    .line 303
    const-wide v2, 0x412e848000000000L

    .line 302
    div-double/2addr v0, v2

    return-wide v0
.end method

.method public abstract loadMapPins()V
.end method

.method protected locateOnMap()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->locateOnMap(Z)V

    .line 108
    return-void
.end method

.method protected locateOnMap(Z)V
    .locals 2
    .parameter

    .prologue
    .line 111
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->a()V

    .line 112
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    if-nez v0, :cond_0

    .line 113
    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/StationsMapView;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 114
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    .line 115
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 116
    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 117
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 130
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/MapResults;->isAdsOnTop()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    invoke-direct {p0, v0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(FZ)V

    .line 132
    invoke-direct {p0, v0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->b(FZ)V

    .line 137
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->invalidate()V

    .line 144
    :goto_1
    return-void

    .line 134
    :cond_1
    invoke-direct {p0, v0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->b(FZ)V

    .line 135
    invoke-direct {p0, v0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->a(FZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 74
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityMap;->onCreate(Landroid/os/Bundle;)V

    .line 75
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f02007f

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinDefault:Landroid/graphics/Bitmap;

    .line 76
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f020081

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinPress:Landroid/graphics/Bitmap;

    .line 77
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f020080

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mappinFocus:Landroid/graphics/Bitmap;

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f020076

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->logoDefault:Landroid/graphics/Bitmap;

    .line 79
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityMap;->onDestroy()V

    .line 84
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    .line 86
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->clear()V

    .line 88
    :cond_1
    return-void
.end method

.method protected queryMapWebService()V
    .locals 6

    .prologue
    .line 91
    new-instance v0, Lgbis/gbandroid/activities/map/d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/map/d;-><init>(Lgbis/gbandroid/activities/map/MapStationsBase;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/d;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 92
    new-instance v0, Lgbis/gbandroid/queries/MapQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->getLocation()Landroid/location/Location;

    move-result-object v3

    invoke-direct {v0, p0, v2, v1, v3}, Lgbis/gbandroid/queries/MapQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 93
    iget-object v1, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->center:Lcom/google/android/maps/GeoPoint;

    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v2}, Lgbis/gbandroid/views/StationsMapView;->getLatitudeSpan()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v3}, Lgbis/gbandroid/views/StationsMapView;->getLongitudeSpan()I

    move-result v3

    iget-object v4, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v4}, Lgbis/gbandroid/views/StationsMapView;->getHeight()I

    move-result v4

    iget-object v5, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v5}, Lgbis/gbandroid/views/StationsMapView;->getWidth()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/queries/MapQuery;->getResponseObject(Lcom/google/android/maps/GeoPoint;IIII)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 94
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/MapResults;

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    .line 95
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/MapResults;->getListMessage()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->listMessages:Ljava/util/List;

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapResults:Lgbis/gbandroid/entities/MapResults;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/MapResults;->getTotalStations()I

    move-result v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->showMoreStationsMessage(I)V

    .line 97
    return-void
.end method

.method protected showMoreStationsMessage(I)V
    .locals 1
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->listMessages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->zoomLevelFlag:Z

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->zoomLevelFlag:Z

    goto :goto_0
.end method

.method public showPinFront(Lgbis/gbandroid/entities/ListMessage;)V
    .locals 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 277
    const/4 v2, 0x0

    .line 279
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v4

    .line 280
    :goto_0
    if-lt v3, v5, :cond_0

    move-object v1, v2

    .line 291
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    invoke-virtual {v1, v4}, Lgbis/gbandroid/views/MapStationOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v0

    invoke-virtual {v1, v4}, Lgbis/gbandroid/views/MapStationOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v1

    const v2, 0x101009c

    invoke-virtual {v1, v2}, Lcom/google/android/maps/OverlayItem;->getMarker(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/OverlayItem;->setMarker(Landroid/graphics/drawable/Drawable;)V

    .line 295
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/Overlay;

    .line 282
    instance-of v1, v0, Lgbis/gbandroid/views/MapStationOverlay;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 283
    check-cast v1, Lgbis/gbandroid/views/MapStationOverlay;

    invoke-virtual {v1, v4}, Lgbis/gbandroid/views/MapStationOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v1

    .line 284
    invoke-virtual {v1}, Lcom/google/android/maps/OverlayItem;->getSnippet()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 285
    check-cast v1, Lgbis/gbandroid/views/MapStationOverlay;

    .line 286
    iget-object v2, p0, Lgbis/gbandroid/activities/map/MapStationsBase;->mapOverlays:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 280
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0
.end method
