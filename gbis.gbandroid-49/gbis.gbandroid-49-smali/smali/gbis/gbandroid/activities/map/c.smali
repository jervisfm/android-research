.class final Lgbis/gbandroid/activities/map/c;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/map/MapStations;

.field private final synthetic b:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/map/MapStations;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iput-object p2, p0, Lgbis/gbandroid/activities/map/c;->b:Landroid/widget/EditText;

    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 463
    iget-object v0, p0, Lgbis/gbandroid/activities/map/c;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 464
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 465
    iget-object v1, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/map/MapStations;->addPlace(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    const v2, 0x7f09009f

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    .line 467
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 472
    :goto_0
    return-void

    .line 469
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    const v2, 0x7f09009d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, p0, Lgbis/gbandroid/activities/map/c;->a:Lgbis/gbandroid/activities/map/MapStations;

    const v2, 0x7f09009c

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStations;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
