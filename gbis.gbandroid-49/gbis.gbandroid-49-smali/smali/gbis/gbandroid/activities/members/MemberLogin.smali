.class public Lgbis/gbandroid/activities/members/MemberLogin;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/members/MemberLogin$a;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/entities/LoginMessage;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/MemberLogin;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 76
    const v0, 0x7f070050

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->d:Landroid/widget/EditText;

    .line 77
    const v0, 0x7f070051

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->d:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->addToEditTextList(Landroid/widget/EditText;)V

    .line 79
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->addToEditTextList(Landroid/widget/EditText;)V

    .line 80
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->d:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/a;-><init>(Lgbis/gbandroid/activities/members/MemberLogin;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/b;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/b;-><init>(Lgbis/gbandroid/activities/members/MemberLogin;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    const v1, 0x7f070035

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 97
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/c;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/c;-><init>(Lgbis/gbandroid/activities/members/MemberLogin;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 107
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 132
    new-instance v0, Lgbis/gbandroid/activities/members/d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/d;-><init>(Lgbis/gbandroid/activities/members/MemberLogin;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/d;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 133
    new-instance v1, Lgbis/gbandroid/queries/MemberLoginQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberLogin;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/MemberLogin;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberLoginQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 134
    invoke-virtual {v1, p1, p2}, Lgbis/gbandroid/queries/MemberLoginQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 135
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/LoginMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->a:Lgbis/gbandroid/entities/LoginMessage;

    .line 136
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/members/MemberLogin;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->b:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 117
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->c:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->b:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberLogin;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 126
    :goto_0
    return v0

    .line 121
    :cond_0
    const v1, 0x7f09007e

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :cond_1
    const v1, 0x7f09007d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->setMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/members/MemberLogin;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    const v0, 0x7f09007b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->setMessage(Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x1

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/members/MemberLogin;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 140
    new-instance v0, Lgbis/gbandroid/activities/members/MemberLogin$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/members/MemberLogin$a;-><init>(Lgbis/gbandroid/activities/members/MemberLogin;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 141
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberLogin$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 142
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 143
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/members/MemberLogin;)V
    .locals 0
    .parameter

    .prologue
    .line 160
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->e()V

    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberLogin;->a:Lgbis/gbandroid/entities/LoginMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/LoginMessage;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x1

    .line 149
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 162
    const-string v1, "car"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberLogin;->a:Lgbis/gbandroid/entities/LoginMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/LoginMessage;->getCar()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 163
    const-string v1, "car_icon_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberLogin;->a:Lgbis/gbandroid/entities/LoginMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/LoginMessage;->getCarIconId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 164
    const-string v1, "member_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberLogin;->a:Lgbis/gbandroid/entities/LoginMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/LoginMessage;->getMemberId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 165
    const-string v1, "is_member"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 166
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 167
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/members/MemberLogin;)Z
    .locals 1
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->b()Z

    move-result v0

    return v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/ResetPassword;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberLogin;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 180
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->startActivity(Landroid/content/Intent;)V

    .line 181
    return-void
.end method

.method public static logout(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 170
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 171
    const-string v1, "car"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 172
    const-string v1, "car_icon_id"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 173
    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 174
    const-string v1, "awards_badge"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 175
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 176
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 56
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onActivityResult(IILandroid/content/Intent;)V

    .line 57
    packed-switch p1, :pswitch_data_0

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 59
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 60
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->setResult(I)V

    .line 61
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->finish()V

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 211
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 212
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->c()V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->getGBActivityDialogNegativeButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 214
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->f()V

    goto :goto_0

    .line 215
    :cond_2
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->getGBActivityDialogNeutralButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 216
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->showRegistration()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 51
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->setEditBoxHintConfiguration()V

    .line 52
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->a()V

    .line 46
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    const v0, 0x7f090237

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 68
    const v0, 0x7f030018

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/members/MemberLogin;->setGBViewInContentLayout(IZ)V

    .line 69
    const v0, 0x7f09010b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberLogin;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 70
    const v0, 0x7f090195

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/MemberLogin;->setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V

    .line 71
    const v0, 0x7f090197

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/MemberLogin;->setGBActivityDialogNeutralButton(ILandroid/view/View$OnClickListener;)V

    .line 72
    const v0, 0x7f090196

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/MemberLogin;->setGBActivityDialogNegativeButton(ILandroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method
