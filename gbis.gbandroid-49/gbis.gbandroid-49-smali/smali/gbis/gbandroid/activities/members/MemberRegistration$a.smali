.class final Lgbis/gbandroid/activities/members/MemberRegistration$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/members/MemberRegistration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/MemberRegistration;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/members/MemberRegistration;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 318
    iput-object p1, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    .line 319
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 320
    iput-object p3, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->b:Ljava/lang/String;

    .line 321
    iput-object p4, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->c:Ljava/lang/String;

    .line 322
    iput-object p5, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->d:Ljava/lang/String;

    .line 323
    iput-object p6, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->e:Ljava/lang/String;

    .line 324
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 328
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 330
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration$a;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->b(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/RegisterMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/RegisterMessage;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->b:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->d:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->c(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    .line 336
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->setResult(I)V

    .line 337
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->d(Lgbis/gbandroid/activities/members/MemberRegistration;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v2, 0x7f09025a

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration$a;->setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->finish()V

    .line 344
    :cond_0
    :goto_1
    return-void

    .line 340
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->e(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/RegisterMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/RegisterMessage;->isSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->e(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/RegisterMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/RegisterMessage;->getMemberIdSuggestions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->f(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 5

    .prologue
    .line 348
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->b:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->c:Ljava/lang/String;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->d:Ljava/lang/String;

    iget-object v4, p0, Lgbis/gbandroid/activities/members/MemberRegistration$a;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const/4 v0, 0x1

    return v0
.end method
