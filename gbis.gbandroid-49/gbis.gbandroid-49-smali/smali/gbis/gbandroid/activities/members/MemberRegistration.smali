.class public Lgbis/gbandroid/activities/members/MemberRegistration;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/members/MemberRegistration$a;,
        Lgbis/gbandroid/activities/members/MemberRegistration$b;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/entities/RegisterMessage;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f07006c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    .line 74
    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->c:Landroid/widget/EditText;

    .line 75
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    .line 76
    const v0, 0x7f070072

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->e:Landroid/widget/EditText;

    .line 77
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->addToEditTextList(Landroid/widget/EditText;)V

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->c:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->addToEditTextList(Landroid/widget/EditText;)V

    .line 79
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->addToEditTextList(Landroid/widget/EditText;)V

    .line 80
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->e:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->addToEditTextList(Landroid/widget/EditText;)V

    .line 81
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->b()V

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 83
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/MemberRegistration;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/MemberRegistration;ZZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 304
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 248
    new-instance v0, Lgbis/gbandroid/activities/members/n;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/n;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/n;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 249
    new-instance v1, Lgbis/gbandroid/queries/MemberRegisterQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberRegisterQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 250
    invoke-virtual {v1, p1, p2, p3, p4}, Lgbis/gbandroid/queries/MemberRegisterQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 251
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/RegisterMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    .line 252
    return-void
.end method

.method private a(ZZ)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 305
    const v0, 0x7f07006f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 306
    if-eqz p1, :cond_1

    .line 307
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 308
    if-eqz p2, :cond_0

    .line 309
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 314
    :goto_0
    return-void

    .line 311
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 313
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 297
    invoke-static {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/e;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/e;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/g;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/g;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->c:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/h;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/h;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 137
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/i;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/i;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 152
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/j;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/j;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 165
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->e:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/members/k;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/members/k;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 185
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 262
    new-instance v0, Lgbis/gbandroid/activities/members/MemberRegistration$a;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lgbis/gbandroid/activities/members/MemberRegistration$a;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 264
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 265
    return-void
.end method

.method static synthetic b(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 289
    invoke-static {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 6

    .prologue
    .line 188
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 189
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 190
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 191
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 192
    invoke-static {v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    const v0, 0x7f0900a4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    .line 218
    :goto_0
    return-void

    .line 195
    :cond_0
    invoke-static {v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->e(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :cond_1
    invoke-static {v3}, Lgbis/gbandroid/utils/VerifyFields;->checkEmail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 199
    const v0, 0x7f0900a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :cond_2
    invoke-static {v4}, Lgbis/gbandroid/activities/members/MemberRegistration;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 202
    const v0, 0x7f0900a6

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_3
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkZip(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 206
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 209
    :cond_4
    :try_start_0
    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 211
    :goto_1
    invoke-direct {p0, v2, v3, v4, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic c(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 273
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->g()V

    return-void
.end method

.method static synthetic c(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 282
    invoke-static {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/members/MemberRegistration;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    .line 221
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 222
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030014

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 223
    const v2, 0x7f090122

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 224
    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 225
    const v2, 0x7f070047

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 226
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 227
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030032

    iget-object v4, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    invoke-virtual {v4}, Lgbis/gbandroid/entities/RegisterMessage;->getMemberIdSuggestions()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 228
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 229
    const v2, 0x7f09019f

    new-instance v3, Lgbis/gbandroid/activities/members/l;

    invoke-direct {v3, p0}, Lgbis/gbandroid/activities/members/l;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 234
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 235
    new-instance v2, Lgbis/gbandroid/activities/members/m;

    invoke-direct {v2, p0, v0, v1}, Lgbis/gbandroid/activities/members/m;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;Landroid/widget/ListView;Lgbis/gbandroid/views/CustomDialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 244
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog;->show()V

    .line 245
    return-void
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 2
    .parameter

    .prologue
    .line 283
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    .line 284
    const/4 v0, 0x1

    .line 286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/RegisterMessage;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    return-object v0
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 255
    new-instance v0, Lgbis/gbandroid/activities/members/f;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/f;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/f;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 256
    new-instance v1, Lgbis/gbandroid/queries/MemberValidateQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberValidateQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 257
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/MemberValidateQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static e(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 290
    const-string v0, "[a-zA-Z_0-9]{3,}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 291
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const/4 v0, 0x1

    .line 294
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 268
    new-instance v0, Lgbis/gbandroid/activities/members/MemberRegistration$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/members/MemberRegistration$b;-><init>(Lgbis/gbandroid/activities/members/MemberRegistration;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 269
    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 270
    const v0, 0x7f07006e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 271
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 220
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->d()V

    return-void
.end method

.method private static f(Ljava/lang/String;)Z
    .locals 2
    .parameter

    .prologue
    .line 298
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    .line 299
    const/4 v0, 0x1

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 274
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 275
    const-string v1, "car"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/RegisterMessage;->getCar()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 276
    const-string v1, "car_icon_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/RegisterMessage;->getCarIconId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 277
    const-string v1, "member_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->a:Lgbis/gbandroid/entities/RegisterMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/RegisterMessage;->getMemberId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 278
    const-string v1, "is_member"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 279
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 280
    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/members/MemberRegistration;)Z
    .locals 1
    .parameter

    .prologue
    .line 254
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic i(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 267
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->f()V

    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic k(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic l(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic m(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/activities/members/MemberRegistration;->e:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 380
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 381
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->c()V

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getGBActivityDialogNegativeButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 383
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 60
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->setEditBoxHintConfiguration()V

    .line 61
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->a()V

    .line 55
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 390
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 394
    const v0, 0x7f090238

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 64
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->setGBViewInContentLayout(IZ)V

    .line 65
    const v0, 0x7f09010c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 66
    const v0, 0x7f090197

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V

    .line 67
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    const-string v1, "register show skip"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const v0, 0x7f09019e

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/MemberRegistration;->setGBActivityDialogNegativeButton(ILandroid/view/View$OnClickListener;)V

    .line 70
    :cond_0
    return-void
.end method
