.class final Lgbis/gbandroid/activities/members/Profile$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/members/Profile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/Profile;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/members/Profile;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 206
    iput-object p1, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    .line 207
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 208
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 232
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 233
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    .line 234
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 212
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 214
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(Lgbis/gbandroid/activities/members/Profile;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->b(Lgbis/gbandroid/activities/members/Profile;)V

    .line 218
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->c(Lgbis/gbandroid/activities/members/Profile;)V

    .line 219
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->d(Lgbis/gbandroid/activities/members/Profile;)V

    .line 222
    :goto_1
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile$a;->a:Lgbis/gbandroid/activities/members/Profile;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->e(Lgbis/gbandroid/activities/members/Profile;)V

    .line 227
    const/4 v0, 0x1

    return v0
.end method
