.class public Lgbis/gbandroid/activities/members/Profile;
.super Lgbis/gbandroid/activities/base/GBActivityAds;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgbis/gbandroid/listeners/ScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/members/Profile$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lgbis/gbandroid/entities/ProfileMessage;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/Profile;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 185
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###,###,###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 186
    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 76
    const v0, 0x7f07013f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->d:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v0, 0x7f07013b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->e:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    const v0, 0x7f07013d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->f:Landroid/view/View;

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method

.method private b()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 113
    const v0, 0x7f070125

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    const v1, 0x7f070124

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 115
    const v2, 0x7f070126

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 116
    const v3, 0x7f070128

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 117
    const v4, 0x7f07012a

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 118
    const v5, 0x7f07012c

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 119
    const v6, 0x7f070133

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 120
    const v7, 0x7f070135

    invoke-virtual {p0, v7}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 121
    const v8, 0x7f070137

    invoke-virtual {p0, v8}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 122
    const v9, 0x7f070139

    invoke-virtual {p0, v9}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 123
    const v10, 0x7f070131

    invoke-virtual {p0, v10}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 124
    const v11, 0x7f07012f

    invoke-virtual {p0, v11}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 126
    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    invoke-virtual {v1, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 128
    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 132
    iget-object v12, p0, Lgbis/gbandroid/activities/members/Profile;->b:Ljava/lang/String;

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getSiteName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getJoinDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getJoinDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->getAge(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getTotalPoints()I

    move-result v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getOverallRank()I

    move-result v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getOverallRank30Days()I

    move-result v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getConsecutiveDays()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getPointsToday()I

    move-result v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getPointBalance()I

    move-result v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/Profile;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getPicturePath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 149
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->d()V

    .line 150
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mRes:Landroid/content/res/Resources;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lgbis/gbandroid/utils/ImageUtils;->getCarFromName(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 153
    :cond_1
    return-void

    .line 138
    :catch_0
    move-exception v0

    const v0, 0x7f0900c7

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->showMessage(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    goto :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/members/Profile;)V
    .locals 0
    .parameter

    .prologue
    .line 169
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->e()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 156
    new-instance v0, Lgbis/gbandroid/activities/members/o;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/o;-><init>(Lgbis/gbandroid/activities/members/Profile;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/o;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 157
    new-instance v1, Lgbis/gbandroid/queries/MemberProfileQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/Profile;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberProfileQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 158
    invoke-virtual {v1}, Lgbis/gbandroid/queries/MemberProfileQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 159
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ProfileMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    .line 160
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/members/Profile;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->a()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 163
    const v0, 0x7f070126

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 164
    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ProfileMessage;->getPicturePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 165
    new-instance v1, Lgbis/gbandroid/utils/ImageLoader;

    const v2, 0x7f020098

    invoke-direct {v1, p0, v2}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;I)V

    .line 166
    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ProfileMessage;->getPicturePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p0, v0, v3}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;Z)V

    .line 167
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/members/Profile;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->b()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->a:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ProfileMessage;->getCar()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 172
    const-string v1, "car"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ProfileMessage;->getCar()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 173
    const-string v1, "car_icon_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ProfileMessage;->getCarIconId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 174
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 175
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->c:Lgbis/gbandroid/entities/ProfileMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ProfileMessage;->getCar()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->a:Ljava/lang/String;

    .line 177
    :cond_0
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/members/Profile;)V
    .locals 0
    .parameter

    .prologue
    .line 155
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->c()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "car"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->a:Ljava/lang/String;

    .line 181
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->b:Ljava/lang/String;

    .line 182
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 190
    const v0, 0x7f09007f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->showMessage(Ljava/lang/String;)V

    .line 191
    invoke-static {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->logout(Landroid/content/Context;)V

    .line 192
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 195
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 196
    const-string v1, "title"

    const v2, 0x7f0900ba

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->startActivity(Landroid/content/Intent;)V

    .line 198
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 201
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/awards/Awards;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 202
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->startActivity(Landroid/content/Intent;)V

    .line 203
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 245
    const-string v0, ""

    .line 246
    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->d:Landroid/view/View;

    if-ne p1, v1, :cond_1

    .line 247
    const v0, 0x7f09018c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 248
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->g()V

    .line 249
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    .line 257
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 258
    return-void

    .line 250
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->e:Landroid/view/View;

    if-ne p1, v1, :cond_2

    .line 251
    const v0, 0x7f090186

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 252
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->h()V

    goto :goto_0

    .line 253
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->f:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 254
    const v0, 0x7f09018d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 255
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->i()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 49
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->setContentView(I)V

    .line 52
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->f()V

    .line 54
    :try_start_0
    new-instance v0, Lgbis/gbandroid/activities/members/Profile$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/members/Profile$a;-><init>(Lgbis/gbandroid/activities/members/Profile;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 55
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/Profile$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 56
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/members/Profile;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->setResult(I)V

    .line 60
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 91
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 92
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 93
    const v1, 0x7f0b0006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onDestroy()V

    .line 72
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->cleanImageCache()V

    .line 73
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 99
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 100
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 109
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 102
    :pswitch_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->g()V

    .line 103
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->finish()V

    goto :goto_0

    .line 106
    :pswitch_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/Profile;->h()V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x7f07019c
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onResume()V

    .line 67
    return-void
.end method

.method public onScrollChanged(Lgbis/gbandroid/views/layouts/CustomScrollView;IIII)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 239
    if-eq p3, p5, :cond_0

    .line 240
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/Profile;->hideAds()V

    .line 241
    :cond_0
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 5

    .prologue
    .line 262
    iget-object v0, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adProfile"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 263
    iget-object v1, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adProfileKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 264
    iget-object v2, p0, Lgbis/gbandroid/activities/members/Profile;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adProfileUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 265
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/members/Profile;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 266
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    const v0, 0x7f09023a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/Profile;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
