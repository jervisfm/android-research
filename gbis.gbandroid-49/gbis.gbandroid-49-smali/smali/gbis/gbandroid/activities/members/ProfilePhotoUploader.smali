.class public Lgbis/gbandroid/activities/members/ProfilePhotoUploader;
.super Lgbis/gbandroid/activities/base/PhotoDialog;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;-><init>()V

    return-void
.end method


# virtual methods
.method protected getIntentExtras()V
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 24
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->title:Ljava/lang/String;

    .line 25
    return-void
.end method

.method protected postPhotoResized()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->uploadPhoto()V

    .line 30
    return-void
.end method

.method protected postPhotoTaken()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method protected queryPhotoUploaderWebService([B)V
    .locals 4
    .parameter

    .prologue
    .line 39
    new-instance v0, Lgbis/gbandroid/activities/members/p;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/p;-><init>(Lgbis/gbandroid/activities/members/ProfilePhotoUploader;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/p;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 40
    new-instance v1, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 41
    invoke-static {p1}, Lgbis/gbandroid/utils/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 42
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f09023b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ProfilePhotoUploader;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
