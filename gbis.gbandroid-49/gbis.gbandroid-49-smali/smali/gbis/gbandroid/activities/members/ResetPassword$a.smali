.class final Lgbis/gbandroid/activities/members/ResetPassword$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/members/ResetPassword;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/ResetPassword;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/members/ResetPassword;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    .line 70
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object p3, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->b:Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 78
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/ResetPassword;->a(Lgbis/gbandroid/activities/members/ResetPassword;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/ResetPassword;->b(Lgbis/gbandroid/activities/members/ResetPassword;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/ResetPassword;->b(Lgbis/gbandroid/activities/members/ResetPassword;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/ResetPassword;->finish()V

    .line 82
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->a:Lgbis/gbandroid/activities/members/ResetPassword;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/ResetPassword$a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/members/ResetPassword;->a(Lgbis/gbandroid/activities/members/ResetPassword;Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x1

    return v0
.end method
