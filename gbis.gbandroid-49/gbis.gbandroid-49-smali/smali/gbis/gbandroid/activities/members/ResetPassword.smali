.class public Lgbis/gbandroid/activities/members/ResetPassword;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/members/ResetPassword$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/ResetPassword;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 43
    const v0, 0x7f07003e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 44
    const v1, 0x7f07003f

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/ResetPassword;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lgbis/gbandroid/activities/members/ResetPassword;->a:Landroid/widget/EditText;

    .line 45
    iget-object v1, p0, Lgbis/gbandroid/activities/members/ResetPassword;->a:Landroid/widget/EditText;

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 46
    const v1, 0x7f090134

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 48
    const v2, 0x7f09014f

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v2, p0, Lgbis/gbandroid/activities/members/ResetPassword;->mRes:Landroid/content/res/Resources;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 50
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 51
    const/high16 v2, 0x4120

    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/ResetPassword;->getDensity()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v4, v4, v4, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 52
    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 53
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/members/ResetPassword;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/members/ResetPassword;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 56
    new-instance v0, Lgbis/gbandroid/activities/members/q;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/members/q;-><init>(Lgbis/gbandroid/activities/members/ResetPassword;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/q;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 57
    new-instance v1, Lgbis/gbandroid/queries/ResetPasswordQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/ResetPassword;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/members/ResetPassword;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ResetPasswordQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 58
    invoke-virtual {v1, p1}, Lgbis/gbandroid/queries/ResetPasswordQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 59
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/members/ResetPassword;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 62
    new-instance v0, Lgbis/gbandroid/activities/members/ResetPassword$a;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/members/ResetPassword$a;-><init>(Lgbis/gbandroid/activities/members/ResetPassword;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;)V

    .line 63
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/ResetPassword$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 65
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 93
    invoke-virtual {p0}, Lgbis/gbandroid/activities/members/ResetPassword;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lgbis/gbandroid/activities/members/ResetPassword;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    invoke-static {v0}, Lgbis/gbandroid/utils/VerifyFields;->checkEmail(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->b(Ljava/lang/String;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    const v0, 0x7f0900a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-direct {p0}, Lgbis/gbandroid/activities/members/ResetPassword;->a()V

    .line 33
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    const v0, 0x7f090239

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 37
    const v0, 0x7f030011

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/members/ResetPassword;->setGBViewInContentLayout(IZ)V

    .line 38
    const v0, 0x7f090129

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/members/ResetPassword;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 39
    const v0, 0x7f09017a

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/members/ResetPassword;->setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V

    .line 40
    return-void
.end method
