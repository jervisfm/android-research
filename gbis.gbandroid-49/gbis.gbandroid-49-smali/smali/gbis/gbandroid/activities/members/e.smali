.class final Lgbis/gbandroid/activities/members/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/MemberRegistration;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 89
    if-eqz p2, :cond_1

    .line 90
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 93
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 94
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v2, 0x7f0900a4

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0, v4, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;ZZ)V

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0, v3, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;ZZ)V

    .line 99
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->i(Lgbis/gbandroid/activities/members/MemberRegistration;)V

    goto :goto_0

    .line 101
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v1, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v2, 0x7f0900a3

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/activities/members/e;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0, v4, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;ZZ)V

    goto :goto_0
.end method
