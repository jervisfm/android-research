.class final Lgbis/gbandroid/activities/members/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/MemberRegistration;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 125
    if-nez p2, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->j(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v1, 0x7f070071

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 127
    iget-object v1, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->j(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkEmail(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->k(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 133
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->k(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/h;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v3, 0x7f0900a1

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
