.class final Lgbis/gbandroid/activities/members/k;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/MemberRegistration;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/members/MemberRegistration;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 168
    if-nez p2, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->n(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 169
    iget-object v0, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v1, 0x7f070073

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 170
    iget-object v1, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->n(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkZip(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 172
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    iget-object v1, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->k(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 177
    iget-object v1, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iget-object v2, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    const v3, 0x7f0900a7

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/members/MemberRegistration;->showMessage(Ljava/lang/String;)V

    .line 181
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    :cond_0
    return-void

    .line 179
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/members/k;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v1}, Lgbis/gbandroid/activities/members/MemberRegistration;->k(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
