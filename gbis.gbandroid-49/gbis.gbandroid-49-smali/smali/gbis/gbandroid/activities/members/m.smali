.class final Lgbis/gbandroid/activities/members/m;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/members/MemberRegistration;

.field private final synthetic b:Landroid/widget/ListView;

.field private final synthetic c:Lgbis/gbandroid/views/CustomDialog;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/members/MemberRegistration;Landroid/widget/ListView;Lgbis/gbandroid/views/CustomDialog;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/members/m;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    iput-object p2, p0, Lgbis/gbandroid/activities/members/m;->b:Landroid/widget/ListView;

    iput-object p3, p0, Lgbis/gbandroid/activities/members/m;->c:Lgbis/gbandroid/views/CustomDialog;

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 237
    check-cast p2, Landroid/widget/CheckedTextView;

    invoke-virtual {p2, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 238
    iget-object v0, p0, Lgbis/gbandroid/activities/members/m;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v0, p0, Lgbis/gbandroid/activities/members/m;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->e(Lgbis/gbandroid/activities/members/MemberRegistration;)Lgbis/gbandroid/entities/RegisterMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/RegisterMessage;->getMemberIdSuggestions()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lgbis/gbandroid/activities/members/m;->b:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v0, p0, Lgbis/gbandroid/activities/members/m;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0}, Lgbis/gbandroid/activities/members/MemberRegistration;->h(Lgbis/gbandroid/activities/members/MemberRegistration;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 240
    iget-object v0, p0, Lgbis/gbandroid/activities/members/m;->a:Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-static {v0, v3, v3}, Lgbis/gbandroid/activities/members/MemberRegistration;->a(Lgbis/gbandroid/activities/members/MemberRegistration;ZZ)V

    .line 241
    iget-object v0, p0, Lgbis/gbandroid/activities/members/m;->c:Lgbis/gbandroid/views/CustomDialog;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog;->dismiss()V

    .line 242
    return-void
.end method
