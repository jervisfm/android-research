.class final Lgbis/gbandroid/activities/prizes/Prize$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/prizes/Prize;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 585
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    .line 586
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 587
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 591
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 593
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/Prize;->a(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/Prize;->d(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/TicketMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/TicketMessage;->getTotalTickets()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/Prize;->d(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/TicketMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/TicketMessage;->getTickets()I

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/prizes/Prize;->finish()V

    .line 599
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize$b;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/Prize;->e(Lgbis/gbandroid/activities/prizes/Prize;)V

    .line 604
    const/4 v0, 0x1

    return v0
.end method
