.class public Lgbis/gbandroid/activities/prizes/Prize;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/prizes/Prize$a;,
        Lgbis/gbandroid/activities/prizes/Prize$b;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/entities/PrizeMemberMessage;

.field private b:Lgbis/gbandroid/entities/PrizeStringsMessage;

.field private c:Lgbis/gbandroid/entities/TicketMessage;

.field private d:I

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/EditText;

.field private j:Landroid/widget/EditText;

.field private k:Landroid/widget/EditText;

.field private l:Landroid/widget/Spinner;

.field private m:Landroid/app/Dialog;

.field private n:Landroid/app/Dialog;

.field private o:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 506
    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 507
    aget-object v0, v0, p1

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    .line 110
    new-instance v4, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v4, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 111
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 112
    const v0, 0x7f070052

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    const v1, 0x7f070053

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 114
    const v2, 0x7f070056

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 115
    const v3, 0x7f070055

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 116
    const v6, 0x7f090117

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 117
    invoke-virtual {v4, v5}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 118
    iget-object v6, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/PrizeStringsMessage;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->getDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->getLimit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    const v0, 0x7f0901a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/prizes/a;

    invoke-direct {v1, p0, v3}, Lgbis/gbandroid/activities/prizes/a;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/widget/EditText;)V

    invoke-virtual {v4, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 126
    const v0, 0x7f0901a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/prizes/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/l;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v4, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 137
    invoke-virtual {v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    .line 138
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/q;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/q;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 144
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/r;

    invoke-direct {v1, p0, v3}, Lgbis/gbandroid/activities/prizes/r;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/widget/EditText;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 162
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 163
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getTicketsAvailable()I

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 165
    const v0, 0x7f070054

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/prizes/s;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/prizes/s;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/t;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/t;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 184
    new-instance v0, Lgbis/gbandroid/activities/prizes/u;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/prizes/u;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 202
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 320
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/f;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/f;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 336
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/g;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/g;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 342
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/h;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/h;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 356
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/i;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/i;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 370
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/j;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/j;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 384
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/k;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/k;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 398
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 399
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    new-instance v1, Lgbis/gbandroid/activities/prizes/m;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/m;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 413
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/n;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/n;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 436
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/o;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/activities/prizes/o;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 454
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 455
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/prizes/p;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/p;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 465
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/prizes/Prize;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 510
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/prizes/Prize;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 511
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 512
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getTicketsAvailable()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 513
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->d:I

    .line 514
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 515
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->h()V

    .line 520
    :goto_0
    return-void

    .line 517
    :cond_0
    const v0, 0x7f090080

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 519
    :cond_1
    const v0, 0x7f090082

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 205
    new-instance v0, Lgbis/gbandroid/activities/prizes/v;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/prizes/v;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/prizes/v;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 206
    new-instance v1, Lgbis/gbandroid/queries/PrizeQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/Prize;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/Prize;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/PrizeQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 207
    invoke-virtual {v1}, Lgbis/gbandroid/queries/PrizeQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 208
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/PrizeResults;

    .line 209
    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeResults;->getPrizeMemberMessage()Lgbis/gbandroid/entities/PrizeMemberMessage;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    .line 210
    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeResults;->getPrizeStringsMessage()Lgbis/gbandroid/entities/PrizeStringsMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    .line 211
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->a()V

    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 214
    new-instance v0, Lgbis/gbandroid/activities/prizes/w;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/prizes/w;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/prizes/w;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 215
    new-instance v1, Lgbis/gbandroid/queries/TicketsQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/Prize;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/Prize;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/TicketsQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 216
    iget v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->d:I

    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->d()Lgbis/gbandroid/entities/PrizeMemberMessage;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lgbis/gbandroid/queries/TicketsQuery;->getResponseObject(ILgbis/gbandroid/entities/PrizeMemberMessage;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 217
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/TicketMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->c:Lgbis/gbandroid/entities/TicketMessage;

    .line 218
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 204
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->b()V

    return-void
.end method

.method private d()Lgbis/gbandroid/entities/PrizeMemberMessage;
    .locals 2

    .prologue
    .line 221
    new-instance v0, Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;-><init>()V

    .line 222
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setFirstName(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setLastName(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setAddress1(Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setAddress2(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setCity(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/prizes/Prize;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setState(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setPostalCode(Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->setEmail(Ljava/lang/String;)V

    .line 230
    return-object v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/TicketMessage;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->c:Lgbis/gbandroid/entities/TicketMessage;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 234
    new-instance v0, Lgbis/gbandroid/activities/prizes/Prize$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/prizes/Prize$a;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 235
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/prizes/Prize$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 236
    const v1, 0x7f09008e

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/prizes/Prize;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 237
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 213
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->c()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Lgbis/gbandroid/activities/prizes/Prize$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/prizes/Prize$b;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 241
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/prizes/Prize$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 242
    const v0, 0x7f090090

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->loadDialog(Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 245
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->g()V

    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 246
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v2, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 247
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03001b

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 248
    const v0, 0x7f090118

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 249
    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 250
    const v0, 0x7f070067

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 251
    const v3, 0x7f070069

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 252
    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/PrizeStringsMessage;->getTotalPoints()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->b:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->getExplanation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    const v0, 0x7f0901a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/prizes/b;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/b;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 260
    const v0, 0x7f0901a2

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/prizes/c;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/c;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNeutralButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 265
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    .line 266
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/prizes/d;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/prizes/d;-><init>(Lgbis/gbandroid/activities/prizes/Prize;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 272
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 273
    return-void
.end method

.method static synthetic h(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/PrizeMemberMessage;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    return-object v0
.end method

.method private h()V
    .locals 7

    .prologue
    .line 276
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 277
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03001a

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 278
    const v0, 0x7f090117

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 279
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 280
    const v0, 0x7f070058

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    .line 281
    const v0, 0x7f07005a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    .line 282
    const v0, 0x7f07005c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    .line 283
    const v0, 0x7f07005e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->h:Landroid/widget/EditText;

    .line 284
    const v0, 0x7f07005f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    .line 285
    const v0, 0x7f070063

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    .line 286
    const v0, 0x7f070065

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    .line 287
    const v0, 0x7f070061

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    .line 289
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 290
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 291
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 292
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->h:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 293
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 294
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->addToEditTextList(Landroid/widget/EditText;)V

    .line 297
    const v0, 0x7f070057

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f09014c

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lgbis/gbandroid/activities/prizes/Prize;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030033

    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 299
    const v3, 0x7f030032

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 300
    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 301
    const v0, 0x7f0901a0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lgbis/gbandroid/activities/prizes/e;

    invoke-direct {v3, p0, v1}, Lgbis/gbandroid/activities/prizes/e;-><init>(Lgbis/gbandroid/activities/prizes/Prize;Lgbis/gbandroid/views/CustomDialog$Builder;)V

    invoke-virtual {v1, v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 308
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    .line 309
    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/prizes/Prize;->a(Landroid/view/View;)V

    .line 310
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->j()V

    .line 311
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 312
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 315
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/prizes/WinnersList;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/Prize;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 316
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->startActivity(Landroid/content/Intent;)V

    .line 317
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 314
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->i()V

    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getFirstName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 470
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getLastName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 471
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 472
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress1()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 473
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 474
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress2()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 475
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 476
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getCity()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 477
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 478
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getState()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 479
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 480
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 481
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 482
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getEmail()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 483
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 484
    :cond_7
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/prizes/Prize;)Z
    .locals 1
    .parameter

    .prologue
    .line 522
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->l()Z

    move-result v0

    return v0
.end method

.method private k()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getState()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 488
    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f060012

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    move v0, v1

    move v2, v1

    .line 491
    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    if-eqz v0, :cond_2

    .line 497
    :cond_0
    if-eqz v0, :cond_1

    move v1, v2

    .line 502
    :cond_1
    return v1

    .line 492
    :cond_2
    aget-object v4, v3, v2

    iget-object v5, p0, Lgbis/gbandroid/activities/prizes/Prize;->a:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v5}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 493
    const/4 v0, 0x1

    goto :goto_0

    .line 495
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method static synthetic k(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic l(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private l()Z
    .locals 7

    .prologue
    .line 523
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 524
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 525
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 526
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 527
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    .line 528
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    .line 529
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 530
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    .line 531
    const v0, 0x7f0900b1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    .line 551
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 532
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 533
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    .line 535
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 536
    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_3

    .line 537
    const v0, 0x7f0900b4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 538
    :cond_3
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 539
    const v0, 0x7f0900b5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 541
    :cond_4
    invoke-static {v5}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    invoke-static {v5}, Lgbis/gbandroid/utils/VerifyFields;->checkZip(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 543
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 544
    :cond_5
    invoke-static {v6}, Lgbis/gbandroid/utils/VerifyFields;->checkEmail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 545
    const v0, 0x7f0900a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 547
    :cond_6
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->f()V

    .line 548
    const/4 v0, 0x1

    goto :goto_1
.end method

.method static synthetic m(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic n(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->g:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic o(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic p(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/Spinner;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->l:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic q(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->j:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic r(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->k:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 105
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 106
    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/Prize;->setEditBoxHintConfiguration()V

    .line 107
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 60
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/Prize;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 62
    if-eqz v0, :cond_3

    .line 63
    aget-object v1, v0, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 64
    aget-object v0, v0, v3

    check-cast v0, Landroid/app/Dialog;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    .line 65
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    aget-object v1, v0, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 68
    aget-object v0, v0, v3

    check-cast v0, Landroid/app/Dialog;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    .line 69
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 71
    :cond_2
    aget-object v1, v0, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 72
    aget-object v0, v0, v3

    check-cast v0, Landroid/app/Dialog;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 76
    :cond_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/Prize;->e()V

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-array v0, v4, [Ljava/lang/Object;

    .line 82
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    aput-object v1, v0, v3

    .line 85
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->m:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 100
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 90
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    aput-object v1, v0, v3

    .line 91
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->n:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 94
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 95
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    .line 96
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    aput-object v1, v0, v3

    .line 97
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/Prize;->o:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 100
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 612
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 616
    const v0, 0x7f09023e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
