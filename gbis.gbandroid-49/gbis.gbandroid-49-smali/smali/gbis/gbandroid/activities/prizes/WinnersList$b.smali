.class final Lgbis/gbandroid/activities/prizes/WinnersList$b;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/prizes/WinnersList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/WinnersMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/WinnersList;

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/WinnersMessage;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lgbis/gbandroid/utils/ImageLoader;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/prizes/WinnersList;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030045

    .line 72
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->a:Lgbis/gbandroid/activities/prizes/WinnersList;

    .line 73
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 74
    iput v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->b:I

    .line 75
    iput-object p3, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->c:Ljava/util/List;

    .line 76
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v1, 0x7f02006a

    invoke-direct {v0, p1, v1}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->d:Lgbis/gbandroid/utils/ImageLoader;

    .line 77
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    if-nez p2, :cond_1

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->a:Lgbis/gbandroid/activities/prizes/WinnersList;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->a(Lgbis/gbandroid/activities/prizes/WinnersList;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 84
    new-instance v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;

    invoke-direct {v1}, Lgbis/gbandroid/activities/prizes/WinnersList$a;-><init>()V

    .line 85
    const v0, 0x7f0700f7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->a:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0700f4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->b:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0700f6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->c:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0700f8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->d:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0700f5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->f:Landroid/widget/ImageView;

    .line 90
    const v0, 0x7f0700f3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/prizes/WinnersList$a;->e:Landroid/widget/ImageView;

    .line 92
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v1

    .line 95
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lgbis/gbandroid/entities/WinnersMessage;

    .line 96
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getEndDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lgbis/gbandroid/utils/DateUtils;->getDateForWinners(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getMemberId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getCityTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getPrize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getImgUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->a:Lgbis/gbandroid/activities/prizes/WinnersList;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->b(Lgbis/gbandroid/activities/prizes/WinnersList;)Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getCar()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lgbis/gbandroid/utils/ImageUtils;->getCarFromName(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    iget-object v2, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getImgUrl()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->d:Lgbis/gbandroid/utils/ImageLoader;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/WinnersMessage;->getImgUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/WinnersList$b;->a:Lgbis/gbandroid/activities/prizes/WinnersList;

    iget-object v3, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->e:Landroid/widget/ImageView;

    const/4 v4, 0x0

    const/16 v5, 0x64

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_1
    return-object p2

    .line 94
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/prizes/WinnersList$a;

    move-object v3, v0

    goto :goto_0

    .line 108
    :cond_2
    :try_start_1
    iget-object v0, v3, Lgbis/gbandroid/activities/prizes/WinnersList$a;->e:Landroid/widget/ImageView;

    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method
