.class public Lgbis/gbandroid/activities/prizes/WinnersList;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/prizes/WinnersList$a;,
        Lgbis/gbandroid/activities/prizes/WinnersList$b;,
        Lgbis/gbandroid/activities/prizes/WinnersList$c;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/WinnersMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/prizes/WinnersList;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 43
    const v0, 0x7f0700d5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 44
    new-instance v1, Lgbis/gbandroid/activities/prizes/WinnersList$b;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->a:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2}, Lgbis/gbandroid/activities/prizes/WinnersList$b;-><init>(Lgbis/gbandroid/activities/prizes/WinnersList;Landroid/content/Context;Ljava/util/List;)V

    .line 45
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/prizes/WinnersList;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lgbis/gbandroid/activities/prizes/x;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/prizes/x;-><init>(Lgbis/gbandroid/activities/prizes/WinnersList;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/prizes/x;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 57
    new-instance v1, Lgbis/gbandroid/queries/WinnersListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/WinnersListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 58
    invoke-virtual {v1}, Lgbis/gbandroid/queries/WinnersListQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 59
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->a:Ljava/util/List;

    .line 60
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/prizes/WinnersList;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/WinnersList;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lgbis/gbandroid/activities/prizes/WinnersList$c;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/prizes/WinnersList$c;-><init>(Lgbis/gbandroid/activities/prizes/WinnersList;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 64
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/prizes/WinnersList$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    const v1, 0x7f09008f

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/prizes/WinnersList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 66
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/prizes/WinnersList;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/WinnersList;->a()V

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/prizes/WinnersList;)V
    .locals 0
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/WinnersList;->b()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 36
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->setContentView(I)V

    .line 38
    invoke-direct {p0}, Lgbis/gbandroid/activities/prizes/WinnersList;->c()V

    .line 39
    const v0, 0x7f0700d3

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901a2

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/prizes/WinnersList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 51
    invoke-virtual {p0}, Lgbis/gbandroid/activities/prizes/WinnersList;->cleanImageCache()V

    .line 52
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const v0, 0x7f090240

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/prizes/WinnersList;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
