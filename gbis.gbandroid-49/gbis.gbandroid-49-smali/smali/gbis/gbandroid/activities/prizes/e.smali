.class final Lgbis/gbandroid/activities/prizes/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;

.field private final synthetic b:Lgbis/gbandroid/views/CustomDialog$Builder;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Lgbis/gbandroid/views/CustomDialog$Builder;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/e;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iput-object p2, p0, Lgbis/gbandroid/activities/prizes/e;->b:Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 303
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/e;->b:Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->getPositiveButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 304
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/e;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v0}, Lgbis/gbandroid/activities/prizes/Prize;->j(Lgbis/gbandroid/activities/prizes/Prize;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/e;->b:Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->getPositiveButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 306
    :cond_0
    return-void
.end method
