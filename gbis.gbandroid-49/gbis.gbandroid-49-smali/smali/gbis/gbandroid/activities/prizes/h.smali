.class final Lgbis/gbandroid/activities/prizes/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;

.field private final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/h;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iput-object p2, p0, Lgbis/gbandroid/activities/prizes/h;->b:Landroid/view/View;

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 345
    if-nez p2, :cond_0

    .line 346
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/h;->b:Landroid/view/View;

    const v1, 0x7f070059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 347
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/h;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->l(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 348
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/h;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 352
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 354
    :cond_0
    return-void

    .line 350
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/h;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
