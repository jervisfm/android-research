.class final Lgbis/gbandroid/activities/prizes/m;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;

.field private final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/m;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iput-object p2, p0, Lgbis/gbandroid/activities/prizes/m;->b:Landroid/view/View;

    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 402
    if-nez p2, :cond_0

    .line 403
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/m;->b:Landroid/view/View;

    const v1, 0x7f070062

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 404
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/m;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->p(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    if-eqz v1, :cond_1

    .line 405
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/m;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 409
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 411
    :cond_0
    return-void

    .line 407
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/m;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
