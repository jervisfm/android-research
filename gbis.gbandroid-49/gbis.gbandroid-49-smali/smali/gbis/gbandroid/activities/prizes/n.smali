.class final Lgbis/gbandroid/activities/prizes/n;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;

.field private final synthetic b:Landroid/view/View;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iput-object p2, p0, Lgbis/gbandroid/activities/prizes/n;->b:Landroid/view/View;

    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const v3, 0x7f020067

    .line 416
    if-nez p2, :cond_0

    .line 417
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/n;->b:Landroid/view/View;

    const v1, 0x7f070064

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 418
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->q(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 419
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->q(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 420
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkZip(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 421
    invoke-static {v1}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 424
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 426
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    const v3, 0x7f0900a7

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    .line 430
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 434
    :cond_0
    :goto_1
    return-void

    .line 428
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 432
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/n;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/prizes/Prize;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
