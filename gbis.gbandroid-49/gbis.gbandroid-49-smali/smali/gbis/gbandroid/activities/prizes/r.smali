.class final Lgbis/gbandroid/activities/prizes/r;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;

.field private final synthetic b:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iput-object p2, p0, Lgbis/gbandroid/activities/prizes/r;->b:Landroid/widget/EditText;

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 147
    const/16 v2, 0x42

    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 148
    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v2}, Lgbis/gbandroid/activities/prizes/Prize;->g(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;

    move-result-object v2

    const v3, 0x7f070035

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/r;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lgbis/gbandroid/activities/prizes/Prize;->a(Lgbis/gbandroid/activities/prizes/Prize;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    .line 159
    :goto_0
    return v0

    .line 152
    :cond_1
    iget-object v2, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v2}, Lgbis/gbandroid/activities/prizes/Prize;->g(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;

    move-result-object v2

    const v3, 0x7f070037

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->f(Lgbis/gbandroid/activities/prizes/Prize;)V

    .line 154
    iget-object v1, p0, Lgbis/gbandroid/activities/prizes/r;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v1}, Lgbis/gbandroid/activities/prizes/Prize;->g(Lgbis/gbandroid/activities/prizes/Prize;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 159
    goto :goto_0
.end method
