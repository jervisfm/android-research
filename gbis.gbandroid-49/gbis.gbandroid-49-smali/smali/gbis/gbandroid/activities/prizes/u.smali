.class final Lgbis/gbandroid/activities/prizes/u;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/prizes/Prize;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/prizes/Prize;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/prizes/u;->a:Lgbis/gbandroid/activities/prizes/Prize;

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v2

    :goto_1
    and-int/2addr v0, v3

    if-eqz v0, :cond_0

    .line 194
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 195
    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/u;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v3}, Lgbis/gbandroid/activities/prizes/Prize;->h(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/PrizeMemberMessage;

    move-result-object v3

    invoke-virtual {v3}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getTicketsAvailable()I

    move-result v3

    if-le v0, v3, :cond_0

    .line 196
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {p1, v0, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 197
    iget-object v0, p0, Lgbis/gbandroid/activities/prizes/u;->a:Lgbis/gbandroid/activities/prizes/Prize;

    iget-object v3, p0, Lgbis/gbandroid/activities/prizes/u;->a:Lgbis/gbandroid/activities/prizes/Prize;

    const v4, 0x7f090081

    invoke-virtual {v3, v4}, Lgbis/gbandroid/activities/prizes/Prize;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lgbis/gbandroid/activities/prizes/u;->a:Lgbis/gbandroid/activities/prizes/Prize;

    invoke-static {v4}, Lgbis/gbandroid/activities/prizes/Prize;->h(Lgbis/gbandroid/activities/prizes/Prize;)Lgbis/gbandroid/entities/PrizeMemberMessage;

    move-result-object v4

    invoke-virtual {v4}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getTicketsAvailable()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/prizes/Prize;->showMessage(Ljava/lang/String;)V

    .line 200
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 193
    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 190
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    return-void
.end method
