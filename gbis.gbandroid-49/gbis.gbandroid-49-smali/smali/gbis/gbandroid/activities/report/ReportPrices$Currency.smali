.class public final enum Lgbis/gbandroid/activities/report/ReportPrices$Currency;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/ReportPrices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Currency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lgbis/gbandroid/activities/report/ReportPrices$Currency;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CAN:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

.field public static final enum USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

.field private static final synthetic a:[Lgbis/gbandroid/activities/report/ReportPrices$Currency;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 83
    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    const-string v1, "USA"

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    const-string v1, "CAN"

    invoke-direct {v0, v1, v3}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->CAN:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    const/4 v0, 0x2

    new-array v0, v0, [Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    aput-object v1, v0, v2

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->CAN:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    aput-object v1, v0, v3

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->a:[Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgbis/gbandroid/activities/report/ReportPrices$Currency;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    return-object v0
.end method

.method public static values()[Lgbis/gbandroid/activities/report/ReportPrices$Currency;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->a:[Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    array-length v1, v0

    new-array v2, v1, [Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
