.class public final enum Lgbis/gbandroid/activities/report/ReportPrices$FuelType;
.super Ljava/lang/Enum;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/ReportPrices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FuelType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lgbis/gbandroid/activities/report/ReportPrices$FuelType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

.field public static final enum Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

.field public static final enum Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

.field public static final enum Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

.field private static final synthetic a:[Lgbis/gbandroid/activities/report/ReportPrices$FuelType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 84
    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    const-string v1, "Regular"

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    const-string v1, "Midgrade"

    invoke-direct {v0, v1, v3}, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    const-string v1, "Premium"

    invoke-direct {v0, v1, v4}, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    const-string v1, "Diesel"

    invoke-direct {v0, v1, v5}, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    const/4 v0, 0x4

    new-array v0, v0, [Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    aput-object v1, v0, v2

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    aput-object v1, v0, v3

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    aput-object v1, v0, v4

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    aput-object v1, v0, v5

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->a:[Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgbis/gbandroid/activities/report/ReportPrices$FuelType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    return-object v0
.end method

.method public static values()[Lgbis/gbandroid/activities/report/ReportPrices$FuelType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->a:[Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    array-length v1, v0

    new-array v2, v1, [Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
