.class final Lgbis/gbandroid/activities/report/ReportPrices$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/ReportPrices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/ReportPrices;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1099
    iput-object p1, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    .line 1100
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 1101
    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1126
    move v1, v2

    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->j(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getSmartPrompts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 1129
    :goto_1
    return v2

    .line 1127
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->j(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getSmartPrompts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/SmartPrompt;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/SmartPrompt;->getPromptId()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1128
    const/4 v2, 0x1

    goto :goto_1

    .line 1126
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f090259

    .line 1105
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 1107
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->c(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1109
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1110
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->d(Lgbis/gbandroid/activities/report/ReportPrices;)I

    move-result v0

    if-eqz v0, :cond_2

    .line 1111
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->e(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v4}, Lgbis/gbandroid/activities/report/ReportPrices;->d(Lgbis/gbandroid/activities/report/ReportPrices;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->showMessage(Ljava/lang/String;)V

    .line 1114
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->f(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 1115
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->g(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 1116
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->h(Lgbis/gbandroid/activities/report/ReportPrices;)I

    move-result v0

    if-nez v0, :cond_3

    .line 1117
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->i(Lgbis/gbandroid/activities/report/ReportPrices;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-virtual {v1, v5}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices$a;->setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 1120
    :goto_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->j(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getSmartPrompts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices$a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->k(Lgbis/gbandroid/activities/report/ReportPrices;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1121
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    .line 1123
    :cond_1
    return-void

    .line 1113
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    .line 1119
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->i(Lgbis/gbandroid/activities/report/ReportPrices;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-virtual {v1, v5}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    const v3, 0x7f090258

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices$a;->setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 1134
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$a;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->l(Lgbis/gbandroid/activities/report/ReportPrices;)Z

    move-result v0

    return v0
.end method
