.class final Lgbis/gbandroid/activities/report/ReportPrices$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/ReportPrices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/ReportPrices;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1043
    iput-object p1, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    .line 1044
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 1045
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 15
    .parameter

    .prologue
    .line 1049
    invoke-super/range {p0 .. p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 1050
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1051
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getRegPrice()D

    move-result-wide v1

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v3

    invoke-virtual {v3}, Lgbis/gbandroid/entities/StationMessage;->getRegTimeSpotted()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v4}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v4

    invoke-virtual {v4}, Lgbis/gbandroid/entities/StationMessage;->getMidPrice()D

    move-result-wide v4

    .line 1052
    iget-object v6, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v6}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v6

    invoke-virtual {v6}, Lgbis/gbandroid/entities/StationMessage;->getMidTimeSpotted()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v7}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v7

    invoke-virtual {v7}, Lgbis/gbandroid/entities/StationMessage;->getPremPrice()D

    move-result-wide v7

    iget-object v9, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v9}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v9

    invoke-virtual {v9}, Lgbis/gbandroid/entities/StationMessage;->getPremTimeSpotted()Ljava/lang/String;

    move-result-object v9

    .line 1053
    iget-object v10, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v10}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v10

    invoke-virtual {v10}, Lgbis/gbandroid/entities/StationMessage;->getDieselPrice()D

    move-result-wide v10

    iget-object v12, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v12}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v12

    invoke-virtual {v12}, Lgbis/gbandroid/entities/StationMessage;->getDieselTimeSpotted()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v13}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v13

    invoke-virtual {v13}, Lgbis/gbandroid/entities/StationMessage;->getTimeOffset()I

    move-result v13

    .line 1054
    iget-object v14, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v14}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v14

    invoke-virtual {v14}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v14

    .line 1051
    invoke-static/range {v0 .. v14}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;ILjava/lang/String;)V

    .line 1056
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getPriceValidation()Lgbis/gbandroid/entities/PriceValidation;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/entities/PriceValidation;)V

    .line 1058
    :cond_0
    return-void
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices$b;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 1063
    const/4 v0, 0x1

    return v0
.end method
