.class public Lgbis/gbandroid/activities/report/ReportPrices;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/report/ReportPrices$Currency;,
        Lgbis/gbandroid/activities/report/ReportPrices$FuelType;,
        Lgbis/gbandroid/activities/report/ReportPrices$a;,
        Lgbis/gbandroid/activities/report/ReportPrices$b;
    }
.end annotation


# static fields
.field public static final BLANK_PRICE:Ljava/lang/String; = "---"

.field public static final DECIMAL:Ljava/lang/Character;

.field public static final DECIMAL_STRING:Ljava/lang/String;

.field private static synthetic E:[I

.field private static a:Z

.field private static b:Z

.field private static final c:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lgbis/gbandroid/activities/report/ReportPrices$Currency;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Landroid/view/ViewGroup;

.field private C:Z

.field private D:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lgbis/gbandroid/activities/report/ReportPrices$FuelType;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Lgbis/gbandroid/entities/ReportMessage;

.field private i:Lgbis/gbandroid/entities/FavReportMessage;

.field private j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

.field private k:Ljava/lang/String;

.field private l:Lgbis/gbandroid/entities/StationMessage;

.field private m:Ljava/lang/Double;

.field private n:Ljava/lang/Double;

.field private o:Ljava/lang/Double;

.field private p:Ljava/lang/Double;

.field private q:I

.field private r:Lgbis/gbandroid/views/CustomEditTextForPrices;

.field private s:Lgbis/gbandroid/views/CustomEditTextForPrices;

.field private t:Lgbis/gbandroid/views/CustomEditTextForPrices;

.field private u:Lgbis/gbandroid/views/CustomEditTextForPrices;

.field private v:Landroid/widget/EditText;

.field private w:Landroid/widget/Spinner;

.field private x:Ljava/lang/Integer;

.field private y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

.field private z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 86
    const/16 v0, 0x2e

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 87
    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    .line 90
    sput-boolean v3, Lgbis/gbandroid/activities/report/ReportPrices;->a:Z

    .line 91
    const/4 v0, 0x0

    sput-boolean v0, Lgbis/gbandroid/activities/report/ReportPrices;->b:Z

    .line 95
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 98
    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->c:Ljava/util/EnumMap;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->c:Ljava/util/EnumMap;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->CAN:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    .line 128
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->x:Ljava/lang/Integer;

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    .line 81
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/StationMessage;
    .locals 1
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    return-object v0
.end method

.method private a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 1287
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, -0x4010

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1288
    :cond_0
    const/4 v0, 0x0

    .line 1298
    :goto_0
    return-object v0

    .line 1291
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v3, "max"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpl-double v0, v1, v3

    if-lez v0, :cond_2

    .line 1292
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1294
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v3, "min"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpg-double v0, v1, v3

    if-gez v0, :cond_3

    .line 1295
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1298
    :cond_3
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 639
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-object v0

    .line 640
    :cond_1
    const-string v1, "---"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 642
    invoke-static {}, Lgbis/gbandroid/activities/report/ReportPrices;->q()[I

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 644
    :pswitch_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 651
    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x8

    if-lt v1, v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v4, :cond_2

    .line 652
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 654
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v5, :cond_0

    .line 655
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 642
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/math/BigDecimal;I)Ljava/lang/String;
    .locals 5
    .parameter
    .parameter

    .prologue
    const v1, 0x7f09000d

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1360
    const-string v0, ""

    .line 1362
    if-lez p2, :cond_1

    .line 1363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "high"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1370
    :cond_0
    :goto_0
    return-object v0

    .line 1366
    :cond_1
    if-gez p2, :cond_0

    .line 1367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "low"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a()Ljava/util/EnumMap;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->c:Ljava/util/EnumMap;

    return-object v0
.end method

.method private a(DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;I)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 827
    const v2, 0x7f070079

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 828
    const v3, 0x7f07007c

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 829
    const v4, 0x7f07007f

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 830
    const v5, 0x7f070082

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 831
    const/4 v6, 0x0

    .line 833
    iget-object v7, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    sget-object v8, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    if-ne v7, v8, :cond_3

    const/4 v7, 0x2

    .line 835
    :goto_0
    const-wide/16 v8, 0x0

    cmpl-double v8, p1, v8

    if-eqz v8, :cond_a

    .line 837
    const-string v6, ""

    invoke-virtual {p3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 838
    invoke-static {p3}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    .line 839
    move/from16 v0, p13

    invoke-static {v6, v0}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v6

    .line 842
    :goto_1
    invoke-direct {p0, v2, p1, p2, v6}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Landroid/view/ViewGroup;DLjava/lang/String;)V

    .line 843
    new-instance v6, Lgbis/gbandroid/activities/report/b;

    invoke-direct {v6, p0, p1, p2, v7}, Lgbis/gbandroid/activities/report/b;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;DI)V

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 852
    const/4 v2, 0x1

    .line 854
    :goto_2
    const-wide/16 v8, 0x0

    cmpl-double v6, p4, v8

    if-eqz v6, :cond_0

    .line 856
    const-string v2, ""

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 857
    invoke-static/range {p6 .. p6}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 858
    move/from16 v0, p13

    invoke-static {v2, v0}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v2

    .line 862
    :goto_3
    invoke-direct {p0, v3, p4, p5, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Landroid/view/ViewGroup;DLjava/lang/String;)V

    .line 863
    new-instance v2, Lgbis/gbandroid/activities/report/c;

    invoke-direct {v2, p0, p4, p5, v7}, Lgbis/gbandroid/activities/report/c;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;DI)V

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 872
    const/4 v2, 0x1

    .line 874
    :cond_0
    const-wide/16 v8, 0x0

    cmpl-double v3, p7, v8

    if-eqz v3, :cond_1

    .line 876
    const-string v2, ""

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 877
    invoke-static/range {p9 .. p9}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 878
    move/from16 v0, p13

    invoke-static {v2, v0}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v2

    .line 883
    :goto_4
    move-wide/from16 v0, p7

    invoke-direct {p0, v4, v0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Landroid/view/ViewGroup;DLjava/lang/String;)V

    .line 884
    new-instance v2, Lgbis/gbandroid/activities/report/d;

    move-wide/from16 v0, p7

    invoke-direct {v2, p0, v0, v1, v7}, Lgbis/gbandroid/activities/report/d;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;DI)V

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 893
    const/4 v2, 0x1

    .line 895
    :cond_1
    const-wide/16 v3, 0x0

    cmpl-double v3, p10, v3

    if-eqz v3, :cond_9

    .line 897
    const-string v2, ""

    move-object/from16 v0, p12

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 898
    invoke-static/range {p12 .. p12}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 899
    move/from16 v0, p13

    invoke-static {v2, v0}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v2

    .line 904
    :goto_5
    move-wide/from16 v0, p10

    invoke-direct {p0, v5, v0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Landroid/view/ViewGroup;DLjava/lang/String;)V

    .line 905
    new-instance v2, Lgbis/gbandroid/activities/report/e;

    move-wide/from16 v0, p10

    invoke-direct {v2, p0, v0, v1, v7}, Lgbis/gbandroid/activities/report/e;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;DI)V

    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 914
    const/4 v2, 0x1

    move v3, v2

    .line 917
    :goto_6
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    .line 918
    const v2, 0x7f070089

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableRow;

    .line 919
    if-eqz v2, :cond_2

    .line 920
    if-nez v3, :cond_8

    .line 921
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 926
    :cond_2
    :goto_7
    return-void

    .line 833
    :cond_3
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 841
    :cond_4
    const-string v6, ""

    goto/16 :goto_1

    .line 860
    :cond_5
    const-string v2, ""

    goto/16 :goto_3

    .line 880
    :cond_6
    const-string v2, ""

    goto :goto_4

    .line 901
    :cond_7
    const-string v2, ""

    goto :goto_5

    .line 923
    :cond_8
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    :cond_9
    move v3, v2

    goto :goto_6

    :cond_a
    move v2, v6

    goto/16 :goto_2
.end method

.method private a(Landroid/content/Intent;)V
    .locals 11
    .parameter

    .prologue
    const v10, 0x7f090011

    const v9, 0x7f090010

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1436
    sget-boolean v0, Lgbis/gbandroid/activities/report/ReportPrices;->b:Z

    if-eqz v0, :cond_0

    .line 1437
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1440
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1443
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1447
    :cond_0
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1450
    const-string v1, "([2-9])(0\\s)(\\d)"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1452
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1453
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1454
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    const-string v4, ""

    invoke-virtual {v2, v0, v3, v4}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 1455
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 1456
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1460
    :cond_1
    const-string v1, "\\D+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1463
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 1464
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v1, v3, :cond_9

    .line 1465
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    invoke-virtual {v1, v0, v3}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1468
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->c:Ljava/util/EnumMap;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v0, v4}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v3, v0, :cond_2

    .line 1469
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->c:Ljava/util/EnumMap;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v0, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {v1, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1477
    :cond_2
    :goto_1
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1478
    invoke-virtual {p0, v9}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v7}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1514
    :goto_2
    return-void

    .line 1440
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1441
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1483
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x8

    if-le v0, v2, :cond_5

    .line 1484
    invoke-virtual {p0, v9}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v7}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 1489
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->d(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1490
    invoke-virtual {p0, v10}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v8}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 1495
    :cond_6
    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 1498
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmpg-double v0, v3, v5

    if-gez v0, :cond_7

    .line 1499
    invoke-virtual {p0, v9}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v7}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 1504
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    instance-of v0, v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    if-eqz v0, :cond_8

    .line 1505
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getFuelType()Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_8

    .line 1506
    invoke-virtual {p0, v10}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1, v8}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 1513
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method

.method private a(Landroid/view/ViewGroup;DLjava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    .line 809
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    if-ne v0, v1, :cond_2

    .line 811
    const v0, 0x7f07008c

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move v1, v2

    .line 816
    :goto_0
    const v0, 0x7f07008b

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2, p3, v1}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 817
    const-string v0, ""

    if-eq p4, v0, :cond_0

    .line 818
    const v0, 0x7f07008d

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 819
    :cond_0
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 820
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_1

    .line 821
    const v0, 0x7f07008e

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 822
    :cond_1
    return-void

    .line 813
    :cond_2
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 825
    invoke-direct/range {p0 .. p13}, Lgbis/gbandroid/activities/report/ReportPrices;->a(DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/entities/PriceValidation;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1244
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/entities/PriceValidation;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/views/CustomEditTextForPrices;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1373
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/Integer;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->x:Ljava/lang/Integer;

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/PriceValidation;)V
    .locals 9
    .parameter

    .prologue
    .line 1246
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    .line 1248
    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getExpectedRegularPrice()D

    move-result-wide v0

    .line 1249
    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getCommonIncreaseRegular()D

    move-result-wide v2

    add-double/2addr v2, v0

    .line 1250
    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getCommonDecreaseRegular()D

    move-result-wide v4

    add-double/2addr v4, v0

    .line 1252
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1253
    const-string v7, "min"

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1254
    const-string v7, "max"

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1255
    const-string v7, "expected"

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1256
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1, v6}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1258
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1259
    const-string v1, "min"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getOffsetMidgrade()D

    move-result-wide v6

    add-double/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    const-string v1, "max"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getOffsetMidgrade()D

    move-result-wide v6

    add-double/2addr v6, v2

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1261
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v6, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v1, v6, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1264
    const-string v1, "min"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getOffsetPremium()D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1265
    const-string v1, "max"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getOffsetPremium()D

    move-result-wide v4

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1266
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v2, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1269
    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getExpectedDieselPrice()D

    move-result-wide v0

    .line 1271
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1272
    const-string v3, "min"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getCommonDecreaseDiesel()D

    move-result-wide v4

    add-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1273
    const-string v3, "max"

    invoke-virtual {p1}, Lgbis/gbandroid/entities/PriceValidation;->getCommonIncreaseDiesel()D

    move-result-wide v4

    add-double/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1274
    const-string v3, "expected"

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1275
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1276
    return-void
.end method

.method private static a(Lgbis/gbandroid/views/CustomEditTextForPrices;)V
    .locals 1
    .parameter

    .prologue
    .line 760
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setNineCents(Z)V

    .line 761
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1390
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 1394
    const v1, 0x7f090014

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/n;

    invoke-direct {v2, p0, p2}, Lgbis/gbandroid/activities/report/n;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)V

    .line 1393
    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1406
    const v1, 0x7f090015

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/o;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/o;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 1405
    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNeutralButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1417
    if-eqz p3, :cond_0

    .line 1419
    const v1, 0x7f090013

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1418
    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1420
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1429
    :goto_0
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 1430
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1431
    return-void

    .line 1424
    :cond_0
    const v1, 0x7f090012

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1423
    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1425
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1426
    invoke-virtual {v0, v3, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)D
    .locals 3
    .parameter

    .prologue
    .line 683
    const-wide/high16 v0, -0x4010

    .line 685
    const-string v2, "---"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 687
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 692
    :cond_0
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    const v7, 0x7f070085

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 332
    const v0, 0x7f07002e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->B:Landroid/view/ViewGroup;

    .line 333
    const v0, 0x7f070076

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->A:Landroid/widget/LinearLayout;

    .line 339
    const v0, 0x7f070078

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 340
    const v0, 0x7f07007b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 341
    const v0, 0x7f07007e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 342
    const v0, 0x7f070081

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 343
    const v0, 0x7f070086

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    .line 346
    new-instance v0, Lgbis/gbandroid/activities/report/p;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/p;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 437
    new-instance v1, Lgbis/gbandroid/activities/report/q;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/q;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 461
    new-instance v2, Lgbis/gbandroid/activities/report/r;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/r;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 471
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnTextChangedListener(Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;)V

    .line 472
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnTextChangedListener(Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;)V

    .line 473
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnTextChangedListener(Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;)V

    .line 474
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnTextChangedListener(Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;)V

    .line 476
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 477
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 478
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 479
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 480
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 481
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 482
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 483
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 485
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 488
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setNextFocusDownId(I)V

    .line 491
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setNextFocusDownId(I)V

    .line 492
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setNextFocusDownId(I)V

    .line 493
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setNextFocusDownId(I)V

    .line 496
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setFuelType(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;)V

    .line 497
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setFuelType(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;)V

    .line 498
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setFuelType(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;)V

    .line 499
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setFuelType(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;)V

    .line 502
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    if-ne v0, v1, :cond_0

    .line 503
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 504
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 505
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 506
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 510
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->addToEditTextList(Landroid/widget/EditText;)V

    .line 511
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->addToEditTextList(Landroid/widget/EditText;)V

    .line 512
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->addToEditTextList(Landroid/widget/EditText;)V

    .line 513
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->addToEditTextList(Landroid/widget/EditText;)V

    .line 514
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->addToEditTextList(Landroid/widget/EditText;)V

    .line 520
    iget v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    if-eqz v0, :cond_2

    .line 521
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 522
    invoke-virtual {p0, v7}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/report/s;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/s;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 542
    const v0, 0x7f070084

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->w:Landroid/widget/Spinner;

    .line 544
    invoke-static {}, Lgbis/gbandroid/activities/report/ReportPrices;->d()Ljava/util/List;

    move-result-object v1

    .line 545
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030033

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {v2, p0, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 546
    const v0, 0x7f030032

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 547
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->w:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 548
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->w:Landroid/widget/Spinner;

    new-instance v2, Lgbis/gbandroid/activities/report/t;

    invoke-direct {v2, p0, v1}, Lgbis/gbandroid/activities/report/t;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 563
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 564
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setGBActivityDialogStationIcon(Lgbis/gbandroid/entities/Station;)V

    .line 570
    const v0, 0x7f070088

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    .line 571
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->getKeyboardView()Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    .line 573
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    new-instance v1, Lgbis/gbandroid/activities/report/u;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/u;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->setOnDrawerStateChangedListener(Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;)V

    .line 602
    sget-boolean v0, Lgbis/gbandroid/activities/report/ReportPrices;->a:Z

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "customKeyboardPreference"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 606
    :cond_1
    iput-boolean v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    .line 608
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v4}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 609
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v4}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 610
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v4}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 611
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v4}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 626
    :goto_1
    const v0, 0x7f070087

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 627
    new-instance v1, Lgbis/gbandroid/activities/report/v;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/v;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 634
    return-void

    .line 525
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 526
    invoke-virtual {p0, v7}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 614
    :cond_3
    iput-boolean v5, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    .line 616
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 617
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 618
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    .line 619
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0, v5}, Lgbis/gbandroid/views/CustomEditTextForPrices;->usePriceKeyboard(Z)V

    goto :goto_1
.end method

.method static synthetic b(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1068
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->i()V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/report/ReportPrices;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V
    .locals 3
    .parameter

    .prologue
    .line 1374
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getFuelType()Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    move-result-object v0

    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v0

    .line 1376
    if-eqz v0, :cond_0

    .line 1377
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setBackgroundHintFromValidStatus(I)V

    .line 1379
    :cond_0
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->k:Ljava/lang/String;

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1199
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 1201
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/i;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/i;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1208
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/j;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/j;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1214
    const v1, 0x7f090123

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1215
    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1216
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 1217
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1218
    return-void
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 665
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 666
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 667
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 668
    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v4}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    .line 669
    const-string v5, "---"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 677
    :cond_0
    :goto_0
    return v0

    .line 671
    :cond_1
    const-string v1, "---"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673
    :cond_2
    const-string v1, "---"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 675
    :cond_3
    const-string v1, "---"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/report/ReportPrices;)I
    .locals 1
    .parameter

    .prologue
    .line 115
    iget v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->q:I

    return v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .parameter

    .prologue
    .line 1350
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1352
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 1353
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1355
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private static d()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 764
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 765
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 766
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 767
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getTimeNow()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 768
    const-string v0, "now"

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 769
    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getPrevHours()[Ljava/util/Date;

    move-result-object v4

    .line 770
    array-length v5, v4

    .line 771
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    if-nez v6, :cond_1

    .line 776
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 777
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 779
    return-object v1

    .line 772
    :cond_1
    aget-object v6, v4, v0

    .line 773
    invoke-static {v6}, Lgbis/gbandroid/utils/DateUtils;->getTimeToString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 774
    invoke-static {v6}, Lgbis/gbandroid/utils/DateUtils;->getTimeToValue(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 771
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 784
    iget v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    if-nez v0, :cond_1

    .line 785
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ReportMessage;->getCarIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 786
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 787
    const-string v1, "car"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ReportMessage;->getCarIcon()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 788
    const-string v1, "car_icon_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ReportMessage;->getCarIconId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 789
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/FavReportMessage;->getCarIcon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 793
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 794
    const-string v1, "car"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/FavReportMessage;->getCarIcon()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 795
    const-string v1, "car_icon_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/FavReportMessage;->getCarIconId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 796
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 802
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "car"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    .line 803
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "car_icon_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->f:I

    .line 804
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->d:Ljava/lang/String;

    .line 805
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1174
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->m()V

    return-void
.end method

.method private g()V
    .locals 15

    .prologue
    const-wide/16 v13, 0x0

    .line 929
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 930
    if-eqz v0, :cond_4

    .line 931
    const-string v3, ""

    .line 932
    const-string v6, ""

    .line 933
    const-string v9, ""

    .line 934
    const-string v12, ""

    .line 936
    const-string v1, "fuel regular"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v1

    .line 937
    cmpl-double v4, v1, v13

    if-eqz v4, :cond_0

    .line 938
    const-string v3, "time spotted regular"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 940
    :cond_0
    const-string v4, "fuel midgrade"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 941
    cmpl-double v7, v4, v13

    if-eqz v7, :cond_1

    .line 942
    const-string v6, "time spotted midgrade"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 944
    :cond_1
    const-string v7, "fuel premium"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v7

    .line 945
    cmpl-double v10, v7, v13

    if-eqz v10, :cond_2

    .line 946
    const-string v9, "time spotted premium"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 948
    :cond_2
    const-string v10, "fuel diesel"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 949
    cmpl-double v13, v10, v13

    if-eqz v13, :cond_3

    .line 950
    const-string v12, "time spotted diesel"

    invoke-virtual {v0, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 952
    :cond_3
    const-string v13, "time offset"

    invoke-virtual {v0, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 955
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->toString()Ljava/lang/String;

    move-object v0, p0

    .line 954
    invoke-direct/range {v0 .. v13}, Lgbis/gbandroid/activities/report/ReportPrices;->a(DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;I)V

    .line 957
    :cond_4
    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1185
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->n()V

    return-void
.end method

.method static synthetic h(Lgbis/gbandroid/activities/report/ReportPrices;)I
    .locals 1
    .parameter

    .prologue
    .line 105
    iget v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    return v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1038
    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/report/ReportPrices$b;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 1039
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1040
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/report/ReportPrices;)Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    return-object v0
.end method

.method private i()V
    .locals 4

    .prologue
    .line 1069
    new-instance v0, Lgbis/gbandroid/activities/report/f;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/f;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/f;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 1070
    new-instance v1, Lgbis/gbandroid/queries/StationDetailsQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/StationDetailsQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1071
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/StationDetailsQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 1072
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    .line 1073
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1084
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->p()Ljava/lang/String;

    move-result-object v0

    .line 1085
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1086
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->k()V

    .line 1089
    :goto_0
    return-void

    .line 1088
    :cond_1
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1092
    new-instance v0, Lgbis/gbandroid/activities/report/ReportPrices$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/report/ReportPrices$a;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 1093
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 1094
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1095
    const v0, 0x7f0901a4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 1096
    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/report/ReportPrices;)Z
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->isSmartPromptOn()Z

    move-result v0

    return v0
.end method

.method private l()Z
    .locals 15

    .prologue
    const v14, 0x7f090009

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-wide/16 v12, 0x0

    .line 1140
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->myLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 1141
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->setMyLocationToZeros()V

    .line 1142
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    if-nez v0, :cond_3

    .line 1143
    new-instance v0, Lgbis/gbandroid/activities/report/g;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/g;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/g;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 1144
    new-instance v0, Lgbis/gbandroid/queries/ReportPricesQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v2, v1, v3}, Lgbis/gbandroid/queries/ReportPricesQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1145
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    iget-object v5, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    iget-object v6, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    iget v7, p0, Lgbis/gbandroid/activities/report/ReportPrices;->f:I

    iget-object v8, p0, Lgbis/gbandroid/activities/report/ReportPrices;->k:Ljava/lang/String;

    .line 1146
    iget-object v9, p0, Lgbis/gbandroid/activities/report/ReportPrices;->v:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1145
    invoke-virtual/range {v0 .. v9}, Lgbis/gbandroid/queries/ReportPricesQuery;->getResponseObject(ILjava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1147
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ReportMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    .line 1148
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ReportMessage;->getRegPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-nez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ReportMessage;->getMidPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-nez v0, :cond_1

    .line 1149
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ReportMessage;->getPremPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-nez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ReportMessage;->getDieselPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-eqz v0, :cond_2

    .line 1150
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->h:Lgbis/gbandroid/entities/ReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ReportMessage;->getTotalPointsAwarded()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->q:I

    .line 1151
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->e()V

    move v0, v11

    .line 1169
    :goto_0
    return v0

    .line 1154
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setMessage(Ljava/lang/String;)V

    move v0, v10

    .line 1155
    goto :goto_0

    .line 1158
    :cond_3
    new-instance v0, Lgbis/gbandroid/activities/report/h;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/h;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/h;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 1159
    new-instance v0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v2, v1, v3}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 1160
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v1

    iget v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    iget-object v5, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    iget-object v6, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    iget-object v7, p0, Lgbis/gbandroid/activities/report/ReportPrices;->e:Ljava/lang/String;

    iget v8, p0, Lgbis/gbandroid/activities/report/ReportPrices;->f:I

    .line 1161
    iget-object v9, p0, Lgbis/gbandroid/activities/report/ReportPrices;->k:Ljava/lang/String;

    .line 1160
    invoke-virtual/range {v0 .. v9}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->getResponseObject(IILjava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;ILjava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 1162
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavReportMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    .line 1163
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavReportMessage;->getMinPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavReportMessage;->getMinPriceId()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->doubleValue()D

    move-result-wide v0

    cmpl-double v0, v0, v12

    if-nez v0, :cond_5

    .line 1164
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setMessage(Ljava/lang/String;)V

    move v0, v10

    .line 1165
    goto :goto_0

    .line 1167
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->i:Lgbis/gbandroid/entities/FavReportMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavReportMessage;->getTotalPointsAwarded()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->q:I

    .line 1168
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->e()V

    move v0, v11

    .line 1169
    goto :goto_0
.end method

.method static synthetic l(Lgbis/gbandroid/activities/report/ReportPrices;)Z
    .locals 1
    .parameter

    .prologue
    .line 1139
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->l()Z

    move-result v0

    return v0
.end method

.method static synthetic m(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->A:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 1175
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1176
    const-string v1, "sm_id"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1177
    const-string v1, "fuel regular"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1178
    const-string v1, "fuel midgrade"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1179
    const-string v1, "fuel premium"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1180
    const-string v1, "fuel diesel"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1181
    const-string v1, "time spotted"

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->k:Ljava/lang/String;

    invoke-static {v2}, Lgbis/gbandroid/utils/DateUtils;->changeToServerType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1182
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->setResult(ILandroid/content/Intent;)V

    .line 1183
    return-void
.end method

.method static synthetic n(Lgbis/gbandroid/activities/report/ReportPrices;)Ljava/lang/Integer;
    .locals 1
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->x:Ljava/lang/Integer;

    return-object v0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 1186
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1187
    const-string v1, "lastReportedPriceDate"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1188
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1189
    return-void
.end method

.method static synthetic o(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    return-object v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1221
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1223
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/k;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/k;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1230
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/m;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/m;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1237
    const v1, 0x7f090116

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1238
    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 1239
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 1240
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1241
    return-void
.end method

.method static synthetic p(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/ReportPrices$Currency;
    .locals 1
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1304
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1305
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1306
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1307
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v3}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1308
    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    .line 1309
    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    .line 1310
    invoke-static {v2}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    .line 1311
    invoke-static {v3}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    .line 1313
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    .line 1314
    :cond_0
    const-string v2, ""

    .line 1345
    :cond_1
    :goto_0
    return-object v2

    .line 1316
    :cond_2
    const-string v2, ""

    .line 1319
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    if-ne v0, v1, :cond_5

    .line 1320
    const/4 v0, 0x2

    move v1, v0

    .line 1325
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v3, "expected"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpl-double v0, v3, v6

    if-eqz v0, :cond_4

    .line 1327
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpl-double v0, v3, v6

    if-lez v0, :cond_7

    .line 1328
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Regular:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->m:Ljava/lang/Double;

    invoke-direct {p0, v3, v4}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/math/BigDecimal;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1331
    :goto_2
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v6

    if-lez v2, :cond_3

    .line 1332
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4, v1}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Midgrade:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->n:Ljava/lang/Double;

    invoke-direct {p0, v3, v4}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/math/BigDecimal;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1335
    :cond_3
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v6

    if-lez v2, :cond_6

    .line 1336
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4, v1}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Premium:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    iget-object v4, p0, Lgbis/gbandroid/activities/report/ReportPrices;->o:Ljava/lang/Double;

    invoke-direct {p0, v3, v4}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v0, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/math/BigDecimal;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1341
    :cond_4
    :goto_3
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpl-double v0, v3, v6

    if-lez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->D:Ljava/util/EnumMap;

    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    invoke-virtual {v0, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v3, "expected"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    cmpl-double v0, v3, v6

    if-eqz v0, :cond_1

    .line 1342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3, v1}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v1

    sget-object v2, Lgbis/gbandroid/activities/report/ReportPrices$FuelType;->Diesel:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->p:Ljava/lang/Double;

    invoke-direct {p0, v2, v3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Ljava/math/BigDecimal;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 1322
    :cond_5
    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_6
    move-object v2, v0

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto/16 :goto_2
.end method

.method static synthetic q(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;
    .locals 1
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    return-object v0
.end method

.method private static synthetic q()[I
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->E:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->values()[Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->CAN:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->USA:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lgbis/gbandroid/activities/report/ReportPrices;->E:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static synthetic r(Lgbis/gbandroid/activities/report/ReportPrices;)Z
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    return v0
.end method

.method static synthetic s(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/view/ViewGroup;
    .locals 1
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->B:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic t(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;
    .locals 1
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    return-object v0
.end method

.method static synthetic u(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;
    .locals 1
    .parameter

    .prologue
    .line 118
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    return-object v0
.end method

.method static synthetic v(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;
    .locals 1
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    return-object v0
.end method

.method static synthetic w(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;
    .locals 1
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    return-object v0
.end method

.method static synthetic x(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1091
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->k()V

    return-void
.end method

.method static synthetic y(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->showLogin()V

    return-void
.end method

.method static synthetic z(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1083
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->j()V

    return-void
.end method


# virtual methods
.method protected OnSmartPromptPressedNo(Landroid/content/DialogInterface;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1545
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->OnSmartPromptPressedNo(Landroid/content/DialogInterface;IZ)V

    .line 1546
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    .line 1547
    return-void
.end method

.method protected OnSmartPromptPressedYes(Landroid/content/DialogInterface;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1533
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->OnSmartPromptPressedYes(Landroid/content/DialogInterface;IZ)V

    .line 1534
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    .line 1535
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 306
    packed-switch p1, :pswitch_data_0

    .line 315
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onActivityResult(IILandroid/content/Intent;)V

    .line 316
    return-void

    .line 309
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 310
    invoke-direct {p0, p3}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x4d2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .parameter

    .prologue
    .line 142
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 143
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 144
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 145
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->f()V

    .line 147
    if-eqz v2, :cond_4

    .line 148
    const-string v1, "station"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/Station;

    .line 149
    new-instance v3, Lgbis/gbandroid/entities/StationMessage;

    invoke-direct {v3}, Lgbis/gbandroid/entities/StationMessage;-><init>()V

    iput-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    .line 150
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgbis/gbandroid/entities/StationMessage;->setStationName(Ljava/lang/String;)V

    .line 151
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v4

    invoke-virtual {v3, v4}, Lgbis/gbandroid/entities/StationMessage;->setGasBrandId(I)V

    .line 152
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getStationId()I

    move-result v4

    invoke-virtual {v3, v4}, Lgbis/gbandroid/entities/StationMessage;->setStationId(I)V

    .line 153
    iget-object v3, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lgbis/gbandroid/entities/StationMessage;->setCountry(Ljava/lang/String;)V

    .line 154
    const-string v1, "favourite id"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->g:I

    .line 156
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices$Currency;->valueOf(Ljava/lang/String;)Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->j:Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    .line 158
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->b()V

    .line 160
    if-eqz v0, :cond_2

    .line 161
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    const/4 v1, 0x2

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    const/4 v1, 0x3

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 165
    const/4 v1, 0x4

    aget-object v0, v0, v1

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    .line 167
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getRegPrice()D

    move-result-wide v1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getRegTimeSpotted()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getMidPrice()D

    move-result-wide v4

    .line 169
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getMidTimeSpotted()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPremPrice()D

    move-result-wide v7

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPremTimeSpotted()Ljava/lang/String;

    move-result-object v9

    .line 170
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getDieselPrice()D

    move-result-wide v10

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getDieselTimeSpotted()Ljava/lang/String;

    move-result-object v12

    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getTimeOffset()I

    move-result v13

    .line 171
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-object v0, p0

    .line 168
    invoke-direct/range {v0 .. v13}, Lgbis/gbandroid/activities/report/ReportPrices;->a(DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;DLjava/lang/String;I)V

    .line 173
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPriceValidation()Lgbis/gbandroid/entities/PriceValidation;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/entities/PriceValidation;)V

    .line 175
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 176
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 177
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 199
    :cond_0
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/report/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/a;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 213
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->setEditBoxHintConfiguration()V

    .line 235
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "largeScreenBoolean"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 238
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lgbis/gbandroid/activities/report/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/l;-><init>(Lgbis/gbandroid/activities/report/ReportPrices;)V

    .line 251
    const-wide/16 v2, 0x12c

    .line 238
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 255
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 256
    const v0, 0x7f07002e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 259
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 260
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 262
    :cond_1
    return-void

    .line 182
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->h()V

    .line 183
    :cond_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->g()V

    goto :goto_0

    .line 187
    :cond_4
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 275
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 276
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 277
    const v1, 0x7f0b0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 278
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 716
    instance-of v0, p1, Lgbis/gbandroid/views/CustomEditTextForPrices;

    if-nez v0, :cond_1

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 720
    :cond_1
    check-cast p1, Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 722
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getFuelType()Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 727
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 728
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getFuelType()Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;Ljava/lang/Double;)Ljava/lang/Integer;

    move-result-object v0

    .line 731
    if-eqz p2, :cond_3

    .line 733
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "---"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    :cond_2
    const-string v0, ""

    invoke-virtual {p1, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 735
    invoke-virtual {p1, v3}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setBackgroundHint(I)V

    goto :goto_0

    .line 740
    :cond_3
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 741
    const-string v0, "---"

    invoke-virtual {p1, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 744
    invoke-virtual {p1, v3}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setBackgroundHint(I)V

    goto :goto_0

    .line 748
    :cond_4
    if-eqz v0, :cond_5

    .line 749
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setBackgroundHintFromValidStatus(I)V

    .line 752
    :cond_5
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->d(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setBackgroundHint(I)V

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 962
    sparse-switch p1, :sswitch_data_0

    .line 991
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    .line 965
    :sswitch_0
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 966
    iget-object v1, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->close()V

    goto :goto_1

    .line 972
    :sswitch_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->close()V

    goto :goto_0

    .line 977
    :sswitch_2
    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v2}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v2

    instance-of v2, v2, Lgbis/gbandroid/views/CustomEditTextForPrices;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    if-eqz v2, :cond_2

    .line 980
    :try_start_0
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_1

    .line 981
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "startTracking"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 987
    goto :goto_1

    .line 962
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_2
        0x54 -> :sswitch_1
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 997
    packed-switch p1, :pswitch_data_0

    .line 1009
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1000
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    instance-of v0, v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->y:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->toggle()V

    .line 1002
    const/4 v0, 0x1

    goto :goto_0

    .line 1004
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 997
    nop

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1015
    packed-switch p1, :pswitch_data_0

    .line 1025
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1018
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->z:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    instance-of v0, v0, Lgbis/gbandroid/views/CustomEditTextForPrices;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->C:Z

    if-eqz v0, :cond_0

    .line 1019
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x100

    if-nez v0, :cond_0

    .line 1020
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->openOptionsMenu()V

    goto :goto_0

    .line 1015
    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 283
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 284
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 289
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 286
    :pswitch_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->launchSettings()V

    .line 287
    const/4 v0, 0x1

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x7f07019e
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 267
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onResume()V

    .line 268
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->f()V

    .line 270
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 271
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 294
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 295
    const/4 v1, 0x0

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->r:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 296
    const/4 v1, 0x1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->s:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 297
    const/4 v1, 0x2

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->t:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 298
    const/4 v1, 0x3

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->u:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 299
    const/4 v1, 0x4

    iget-object v2, p0, Lgbis/gbandroid/activities/report/ReportPrices;->l:Lgbis/gbandroid/entities/StationMessage;

    aput-object v2, v0, v1

    .line 300
    return-object v0
.end method

.method protected onSmartPromptCancelled(Landroid/content/DialogInterface;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1551
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onSmartPromptCancelled(Landroid/content/DialogInterface;IZ)V

    .line 1552
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    .line 1553
    return-void
.end method

.method protected onSmartPromptPressedDontKnow(Landroid/content/DialogInterface;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1539
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onSmartPromptPressedDontKnow(Landroid/content/DialogInterface;IZ)V

    .line 1540
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->finish()V

    .line 1541
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 1520
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1524
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1525
    const-string v1, "favourite id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526
    const v0, 0x7f090234

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1528
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f090233

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 326
    const v0, 0x7f03001e

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->setGBViewInContentLayout(IZ)V

    .line 328
    return-void
.end method

.method public submitPricesClick()V
    .locals 2

    .prologue
    .line 698
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 699
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/ReportPrices;->showMessage(Ljava/lang/String;)V

    .line 711
    :goto_0
    return-void

    .line 704
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->f()V

    .line 706
    iget-object v0, p0, Lgbis/gbandroid/activities/report/ReportPrices;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 707
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->j()V

    goto :goto_0

    .line 709
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/ReportPrices;->o()V

    goto :goto_0
.end method
