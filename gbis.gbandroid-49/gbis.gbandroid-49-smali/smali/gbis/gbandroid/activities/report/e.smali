.class final Lgbis/gbandroid/activities/report/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/ReportPrices;

.field private final synthetic b:D

.field private final synthetic c:I


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/report/ReportPrices;DI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iput-wide p2, p0, Lgbis/gbandroid/activities/report/e;->b:D

    iput p4, p0, Lgbis/gbandroid/activities/report/e;->c:I

    .line 905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 908
    iget-object v0, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    const v2, 0x7f0901d0

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->b(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)V

    .line 909
    iget-object v0, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->w(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;

    move-result-object v0

    iget-wide v1, p0, Lgbis/gbandroid/activities/report/e;->b:D

    iget v3, p0, Lgbis/gbandroid/activities/report/e;->c:I

    invoke-static {v1, v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setText(Ljava/lang/CharSequence;)V

    .line 910
    iget-object v0, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->w(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    .line 911
    iget-object v0, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/e;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->w(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/views/CustomEditTextForPrices;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getNextFocusDownId()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/ReportPrices;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 912
    return-void
.end method
