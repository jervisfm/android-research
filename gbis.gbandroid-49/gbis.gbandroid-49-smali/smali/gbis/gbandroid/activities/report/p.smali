.class final Lgbis/gbandroid/activities/report/p;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/ReportPrices;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/report/p;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTextChanged(Lgbis/gbandroid/views/CustomEditTextForPrices;ZLjava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 352
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->haltTextWatcher()V

    .line 355
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 357
    iget-object v1, p0, Lgbis/gbandroid/activities/report/p;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    sget-object v2, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {p3, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lgbis/gbandroid/activities/report/ReportPrices;->a(Lgbis/gbandroid/activities/report/ReportPrices;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 358
    const/4 v2, 0x0

    .line 359
    const/4 v1, 0x0

    .line 363
    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 365
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v6, v5

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v6, :cond_8

    .line 369
    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    const/4 v2, 0x1

    .line 371
    :cond_0
    if-eqz v4, :cond_1

    .line 372
    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v5}, Ljava/lang/Character;->charValue()C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v3, v5, :cond_2

    .line 373
    :cond_1
    const/4 v2, 0x1

    .line 381
    :cond_2
    if-eqz v2, :cond_3

    .line 382
    sget-object v1, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->stripCharacter(Ljava/lang/Character;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 383
    const/4 v1, 0x0

    .line 387
    :cond_3
    if-nez p2, :cond_4

    .line 388
    sget-object v2, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    invoke-virtual {p4, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 390
    sget-object v3, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {p4, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-ltz v2, :cond_4

    .line 391
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 392
    const/4 v1, 0x0

    .line 396
    :cond_4
    if-eqz v4, :cond_7

    .line 398
    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    const/4 v3, 0x4

    if-le v2, v3, :cond_5

    if-nez v1, :cond_5

    .line 399
    const/4 v2, 0x4

    const/4 v3, 0x5

    invoke-virtual {v0, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 404
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v3

    if-gt v2, v3, :cond_6

    if-gtz v1, :cond_6

    .line 405
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 409
    :cond_6
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 410
    invoke-static {}, Lgbis/gbandroid/activities/report/ReportPrices;->a()Ljava/util/EnumMap;

    move-result-object v1

    iget-object v3, p0, Lgbis/gbandroid/activities/report/p;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v3}, Lgbis/gbandroid/activities/report/ReportPrices;->p(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/ReportPrices$Currency;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 412
    if-lt v2, v1, :cond_7

    .line 413
    if-le v2, v1, :cond_a

    .line 416
    invoke-virtual {v0, p4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->overwrite(Ljava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 419
    if-lez p5, :cond_7

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v0

    if-ge p5, v0, :cond_7

    .line 420
    iget-object v0, p0, Lgbis/gbandroid/activities/report/p;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->q(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getTarget()Landroid/widget/EditText;

    move-result-object v0

    add-int/lit8 v1, p5, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 431
    :cond_7
    :goto_1
    invoke-virtual {p1}, Lgbis/gbandroid/views/CustomEditTextForPrices;->enableTextWatcher()V

    .line 432
    return-void

    .line 365
    :cond_8
    aget-char v7, v5, v3

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    .line 366
    sget-object v8, Lgbis/gbandroid/activities/report/ReportPrices;->DECIMAL:Ljava/lang/Character;

    invoke-virtual {v7, v8}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    add-int/lit8 v1, v1, 0x1

    .line 365
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 423
    :cond_a
    if-eqz p2, :cond_7

    .line 425
    iget-object v0, p0, Lgbis/gbandroid/activities/report/p;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->q(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->focusNext()V

    goto :goto_1
.end method
