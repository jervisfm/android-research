.class public Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;
.super Landroid/widget/LinearLayout;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;
    }
.end annotation


# instance fields
.field a:Landroid/view/animation/Animation;

.field b:Landroid/view/animation/Animation;

.field private c:Landroid/content/Context;

.field private d:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

.field private e:Z

.field protected onDrawerStateChangedListener:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    .line 39
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->a()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    .line 33
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    .line 34
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->a()V

    .line 35
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    const v1, 0x7f03004e

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    const v0, 0x7f070122

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->d:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    const v1, 0x7f040003

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->a:Landroid/view/animation/Animation;

    .line 51
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    const v1, 0x7f040002

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->b:Landroid/view/animation/Animation;

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->a:Landroid/view/animation/Animation;

    new-instance v1, Lgbis/gbandroid/activities/report/pricekeyboard/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/report/pricekeyboard/a;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 87
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    if-eqz v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->b:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->onDrawerStateChangedListener:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->onDrawerStateChangedListener:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;

    invoke-interface {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;->onClose()V

    goto :goto_0
.end method

.method public getKeyboardView()Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->d:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    return v0
.end method

.method public open()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->c:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 106
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 108
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->a:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 112
    iput-boolean v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    goto :goto_0
.end method

.method public setOnDrawerStateChangedListener(Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->onDrawerStateChangedListener:Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;

    .line 96
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->e:Z

    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->open()V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer;->close()V

    goto :goto_0
.end method
