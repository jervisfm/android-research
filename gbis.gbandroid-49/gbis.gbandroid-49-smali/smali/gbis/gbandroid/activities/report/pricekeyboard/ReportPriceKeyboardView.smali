.class public Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;
.super Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;
.source "GBFile"


# static fields
.field public static final KEYCODE_CLEAR:I = -0x3

.field public static final KEYCODE_DISABLED:I = -0x3e7

.field public static final KEYCODE_NEXT:I = -0x2

.field public static final KEYCODE_VOICE:I = -0x1

.field public static final VOICE_RECOGNITION_REQUEST_CODE:I = 0x4d2


# instance fields
.field a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

.field private c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

.field private d:Landroid/content/Context;

.field private e:Landroid/widget/EditText;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->f:Z

    .line 55
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/pricekeyboard/b;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    .line 49
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    .line 51
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->f:Z

    .line 55
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/pricekeyboard/b;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    .line 42
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    .line 44
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a()V

    .line 45
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 103
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    const v2, 0x7f050001

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    .line 104
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->setKeyboard(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)V

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->setOnKeyboardActionListener(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;)V

    .line 108
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->setSpeechEnabled(Z)V

    .line 110
    return-void
.end method


# virtual methods
.method public focusNext()V
    .locals 5

    .prologue
    .line 168
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 172
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, v0

    .line 178
    :goto_1
    if-eqz v1, :cond_2

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-nez v2, :cond_3

    .line 185
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getNextFocusDownId()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 186
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getNextFocusDownId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 193
    :goto_2
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 180
    :cond_3
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_1

    .line 189
    :cond_4
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    .line 190
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    const/16 v3, 0x82

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method public getTarget()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method public requestSpeechInput(I)V
    .locals 2
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->requestSpeechInput(ILjava/lang/String;)V

    .line 149
    return-void
.end method

.method public requestSpeechInput(ILjava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 153
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->f:Z

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 157
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 158
    const-string v0, "android.speech.extra.LANGUAGE_MODEL"

    const-string v2, "free_form"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v0, "android.speech.extra.PROMPT"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public setSpeechEnabled(Z)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 128
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->f:Z

    .line 145
    :cond_0
    return-void

    .line 134
    :cond_1
    iput-boolean v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->f:Z

    .line 137
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getKeys()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 138
    iget-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    aget v2, v2, v4

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 139
    iget-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    const/16 v3, -0x3e7

    aput v3, v2, v4

    .line 140
    const/4 v2, 0x0

    iput-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    .line 141
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->background:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setTarget(Landroid/widget/EditText;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    .line 114
    return-void
.end method

.method public setTargetText(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->e:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method
