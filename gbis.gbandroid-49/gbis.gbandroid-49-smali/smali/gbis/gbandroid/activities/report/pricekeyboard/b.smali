.class final Lgbis/gbandroid/activities/report/pricekeyboard/b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(I[I)V
    .locals 12
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 59
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, -0x3e7

    if-ne p1, v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 65
    new-instance v0, Landroid/view/KeyEvent;

    .line 66
    const/4 v11, 0x6

    move-wide v3, v1

    move v6, p1

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    .line 65
    invoke-direct/range {v0 .. v11}, Landroid/view/KeyEvent;-><init>(JJIIIIIII)V

    .line 69
    packed-switch p1, :pswitch_data_0

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;)Z

    goto :goto_0

    .line 72
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->focusNext()V

    goto :goto_0

    .line 76
    :pswitch_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    const/16 v1, 0x4d2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->requestSpeechInput(I)V

    goto :goto_0

    .line 80
    :pswitch_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->a(Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPress(I)V
    .locals 2
    .parameter

    .prologue
    .line 91
    const/16 v0, -0x3e7

    if-eq p1, v0, :cond_0

    .line 92
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/b;->a:Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->performHapticFeedback(I)Z

    .line 94
    :cond_0
    return-void
.end method

.method public final onRelease(I)V
    .locals 0
    .parameter

    .prologue
    .line 96
    return-void
.end method

.method public final onText(Ljava/lang/CharSequence;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    return-void
.end method
