.class public Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Key"
.end annotation


# static fields
.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[I


# instance fields
.field private a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

.field public background:Landroid/graphics/drawable/Drawable;

.field public codes:[I

.field public edgeFlags:I

.field public gap:I

.field public height:I

.field public icon:Landroid/graphics/drawable/Drawable;

.field public iconPreview:Landroid/graphics/drawable/Drawable;

.field public label:Ljava/lang/CharSequence;

.field public modifier:Z

.field public on:Z

.field public popupCharacters:Ljava/lang/CharSequence;

.field public popupResId:I

.field public pressed:Z

.field public repeatable:Z

.field public sticky:Z

.field public text:Ljava/lang/CharSequence;

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 286
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->b:[I

    .line 291
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->c:[I

    .line 297
    new-array v0, v3, [I

    .line 298
    const v1, 0x101009f

    aput v1, v0, v2

    .line 297
    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->d:[I

    .line 301
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->e:[I

    .line 306
    new-array v0, v2, [I

    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->f:[I

    .line 309
    new-array v0, v3, [I

    .line 310
    const v1, 0x10100a7

    aput v1, v0, v2

    .line 309
    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->g:[I

    .line 228
    return-void

    .line 286
    :array_0
    .array-data 0x4
        0x9ft 0x0t 0x1t 0x1t
        0xa0t 0x0t 0x1t 0x1t
    .end array-data

    .line 291
    :array_1
    .array-data 0x4
        0xa7t 0x0t 0x1t 0x1t
        0x9ft 0x0t 0x1t 0x1t
        0xa0t 0x0t 0x1t 0x1t
    .end array-data

    .line 301
    :array_2
    .array-data 0x4
        0xa7t 0x0t 0x1t 0x1t
        0x9ft 0x0t 0x1t 0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;IILandroid/content/res/XmlResourceParser;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 332
    invoke-direct {p0, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;)V

    .line 334
    iput p3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    .line 335
    iput p4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    .line 337
    const-string v0, "GasBuddy"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Keyboard width: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    move-result-object v2

    invoke-static {v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->g(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-static {p5}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 340
    sget-object v1, Lgbis/gbandroid/R$styleable;->Keyboard:[I

    .line 339
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 344
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v1

    iget v2, p2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultWidth:I

    .line 342
    invoke-static {v0, v4, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    .line 347
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v1

    iget v2, p2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHeight:I

    .line 345
    invoke-static {v0, v5, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    .line 350
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v1

    iget v2, p2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHorizontalGap:I

    .line 348
    invoke-static {v0, v6, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->gap:I

    .line 351
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 352
    invoke-static {p5}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 353
    sget-object v1, Lgbis/gbandroid/R$styleable;->Keyboard_Key:[I

    .line 352
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 354
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->gap:I

    add-int/2addr v1, v2

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    .line 355
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 356
    invoke-virtual {v0, v5, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    .line 358
    iget v2, v1, Landroid/util/TypedValue;->type:I

    const/16 v3, 0x10

    if-eq v2, v3, :cond_0

    .line 359
    iget v2, v1, Landroid/util/TypedValue;->type:I

    const/16 v3, 0x11

    if-ne v2, v3, :cond_5

    .line 360
    :cond_0
    new-array v2, v5, [I

    iget v1, v1, Landroid/util/TypedValue;->data:I

    aput v1, v2, v4

    iput-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    .line 365
    :cond_1
    :goto_0
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    .line 366
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 367
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 368
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->iconPreview:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 367
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 372
    :cond_2
    const/4 v1, 0x5

    .line 371
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->repeatable:Z

    .line 373
    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->modifier:Z

    .line 376
    const/4 v1, 0x4

    .line 375
    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->sticky:Z

    .line 377
    invoke-virtual {v0, v6, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    .line 378
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    iget v2, p2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->rowEdgeFlags:I

    or-int/2addr v1, v2

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    .line 381
    const/16 v1, 0x9

    .line 380
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    .line 382
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 383
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 385
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->label:Ljava/lang/CharSequence;

    .line 386
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->text:Ljava/lang/CharSequence;

    .line 387
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->background:Landroid/graphics/drawable/Drawable;

    .line 389
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    if-nez v1, :cond_4

    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->label:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 390
    new-array v1, v5, [I

    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->label:Ljava/lang/CharSequence;

    invoke-interface {v2, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    aput v2, v1, v4

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    .line 392
    :cond_4
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 393
    return-void

    .line 361
    :cond_5
    iget v2, v1, Landroid/util/TypedValue;->type:I

    if-ne v2, v7, :cond_1

    .line 362
    iget-object v1, v1, Landroid/util/TypedValue;->string:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->a(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    goto/16 :goto_0
.end method

.method public constructor <init>(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;)V
    .locals 1
    .parameter

    .prologue
    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 315
    invoke-static {p1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    .line 316
    iget v0, p1, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHeight:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    .line 317
    iget v0, p1, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultWidth:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    .line 318
    iget v0, p1, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHorizontalGap:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->gap:I

    .line 319
    iget v0, p1, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->rowEdgeFlags:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    .line 320
    return-void
.end method

.method private static a(Ljava/lang/String;)[I
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 418
    .line 420
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    move v2, v1

    .line 421
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 422
    const-string v3, ","

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-gtz v0, :cond_0

    .line 426
    :goto_0
    new-array v2, v2, [I

    .line 428
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v0, ","

    invoke-direct {v3, p0, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :goto_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-nez v0, :cond_1

    .line 436
    return-object v2

    .line 431
    :cond_1
    add-int/lit8 v0, v1, 0x1

    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v2, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v0

    goto :goto_1

    .line 433
    :catch_0
    move-exception v1

    const-string v1, "Keyboard"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing keycodes "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public getCurrentDrawableState()[I
    .locals 2

    .prologue
    .line 480
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->f:[I

    .line 482
    iget-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->on:Z

    if-eqz v1, :cond_2

    .line 483
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    if-eqz v0, :cond_1

    .line 484
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->c:[I

    .line 501
    :cond_0
    :goto_0
    return-object v0

    .line 486
    :cond_1
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->b:[I

    goto :goto_0

    .line 489
    :cond_2
    iget-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->sticky:Z

    if-eqz v1, :cond_4

    .line 490
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    if-eqz v0, :cond_3

    .line 491
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->e:[I

    goto :goto_0

    .line 493
    :cond_3
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->d:[I

    goto :goto_0

    .line 496
    :cond_4
    iget-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    if-eqz v1, :cond_0

    .line 497
    sget-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->g:[I

    goto :goto_0
.end method

.method public isInside(II)Z
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 448
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_4

    move v0, v1

    .line 449
    :goto_0
    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    and-int/lit8 v3, v3, 0x2

    if-lez v3, :cond_5

    move v3, v1

    .line 450
    :goto_1
    iget v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    and-int/lit8 v4, v4, 0x4

    if-lez v4, :cond_6

    move v4, v1

    .line 451
    :goto_2
    iget v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->edgeFlags:I

    and-int/lit8 v5, v5, 0x8

    if-lez v5, :cond_7

    move v5, v1

    .line 452
    :goto_3
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    if-ge p1, v6, :cond_0

    if-eqz v0, :cond_8

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v0, v6

    if-gt p1, v0, :cond_8

    .line 453
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v0, v6

    if-lt p1, v0, :cond_1

    if-eqz v3, :cond_8

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    if-lt p1, v0, :cond_8

    .line 454
    :cond_1
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    if-ge p2, v0, :cond_2

    if-eqz v4, :cond_8

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    add-int/2addr v0, v3

    if-gt p2, v0, :cond_8

    .line 455
    :cond_2
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    add-int/2addr v0, v3

    if-lt p2, v0, :cond_3

    if-eqz v5, :cond_8

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    if-lt p2, v0, :cond_8

    .line 458
    :cond_3
    :goto_4
    return v1

    :cond_4
    move v0, v2

    .line 448
    goto :goto_0

    :cond_5
    move v3, v2

    .line 449
    goto :goto_1

    :cond_6
    move v4, v2

    .line 450
    goto :goto_2

    :cond_7
    move v5, v2

    .line 451
    goto :goto_3

    :cond_8
    move v1, v2

    .line 458
    goto :goto_4
.end method

.method public onPressed()V
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    .line 402
    return-void

    .line 401
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReleased(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 411
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->pressed:Z

    .line 412
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->sticky:Z

    if-eqz v0, :cond_0

    .line 413
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->on:Z

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->on:Z

    .line 415
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 411
    goto :goto_0

    :cond_2
    move v1, v2

    .line 413
    goto :goto_1
.end method

.method public squaredDistanceFrom(II)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 469
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int/2addr v0, p1

    .line 470
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sub-int/2addr v1, p2

    .line 471
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    return v0
.end method
