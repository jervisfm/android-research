.class public Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Row"
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

.field public defaultHeight:I

.field public defaultHorizontalGap:I

.field public defaultWidth:I

.field public mode:I

.field public rowEdgeFlags:I

.field public verticalGap:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;Landroid/content/res/XmlResourceParser;)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object p2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    .line 187
    invoke-static {p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 188
    sget-object v1, Lgbis/gbandroid/R$styleable;->Keyboard:[I

    .line 187
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 191
    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v1

    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v2

    .line 189
    invoke-static {v0, v4, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultWidth:I

    .line 194
    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v1

    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v2

    .line 192
    invoke-static {v0, v5, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHeight:I

    .line 196
    const/4 v1, 0x2

    .line 197
    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v2

    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->e(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v3

    .line 195
    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHorizontalGap:I

    .line 199
    const/4 v1, 0x3

    .line 200
    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v2

    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->f(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I

    move-result v3

    .line 198
    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->verticalGap:I

    .line 201
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 202
    invoke-static {p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 203
    sget-object v1, Lgbis/gbandroid/R$styleable;->Keyboard_Row:[I

    .line 202
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 204
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->rowEdgeFlags:I

    .line 205
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->mode:I

    .line 207
    return-void
.end method

.method public constructor <init>(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)V
    .locals 0
    .parameter

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    .line 183
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;
    .locals 1
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->a:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    return-object v0
.end method
