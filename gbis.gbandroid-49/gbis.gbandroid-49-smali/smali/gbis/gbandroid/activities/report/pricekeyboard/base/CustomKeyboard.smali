.class public Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;,
        Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;
    }
.end annotation


# static fields
.field public static final EDGE_BOTTOM:I = 0x8

.field public static final EDGE_LEFT:I = 0x1

.field public static final EDGE_RIGHT:I = 0x2

.field public static final EDGE_TOP:I = 0x4

.field public static final KEYCODE_ALT:I = -0x6

.field public static final KEYCODE_CANCEL:I = -0x3

.field public static final KEYCODE_DELETE:I = -0x5

.field public static final KEYCODE_DONE:I = -0x4

.field public static final KEYCODE_MODE_CHANGE:I = -0x2

.field public static final KEYCODE_SHIFT:I = -0x1

.field private static s:F


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:[[I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const v0, 0x3fe66666

    sput v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->s:F

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 511
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;-><init>(Landroid/content/Context;II)V

    .line 512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 521
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->g:I

    .line 522
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 523
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    .line 524
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->m:I

    .line 527
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a:I

    .line 528
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    .line 529
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    .line 530
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c:I

    .line 531
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    .line 532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->k:Ljava/util/List;

    .line 533
    iput p3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->n:I

    .line 534
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/Context;Landroid/content/res/XmlResourceParser;)V

    .line 535
    return-void
.end method

.method static a(Landroid/content/res/TypedArray;III)I
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 813
    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v0

    .line 814
    if-nez v0, :cond_1

    .line 822
    :cond_0
    :goto_0
    return p3

    .line 815
    :cond_1
    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 816
    invoke-virtual {p0, p1, p3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result p3

    goto :goto_0

    .line 817
    :cond_2
    iget v0, v0, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 819
    int-to-float v0, p3

    invoke-virtual {p0, p1, p2, p2, v0}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p3

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 129
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    return v0
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 663
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getMinWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0xa

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    .line 664
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    .line 665
    const/16 v0, 0x32

    new-array v0, v0, [[I

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->q:[[I

    .line 666
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 667
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    mul-int/lit8 v7, v0, 0xa

    .line 668
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    mul-int/lit8 v8, v0, 0x5

    move v5, v2

    .line 669
    :goto_0
    if-lt v5, v7, :cond_0

    .line 687
    return-void

    :cond_0
    move v4, v2

    .line 670
    :goto_1
    if-lt v4, v8, :cond_1

    .line 669
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    add-int/2addr v0, v5

    move v5, v0

    goto :goto_0

    :cond_1
    move v1, v2

    move v3, v2

    .line 672
    :goto_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 682
    new-array v0, v3, [I

    .line 683
    invoke-static {v6, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 684
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->q:[[I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    div-int v3, v4, v3

    mul-int/lit8 v3, v3, 0xa

    iget v9, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    div-int v9, v5, v9

    add-int/2addr v3, v9

    aput-object v0, v1, v3

    .line 670
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    add-int/2addr v0, v4

    move v4, v0

    goto :goto_1

    .line 673
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 674
    invoke-virtual {v0, v5, v4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->squaredDistanceFrom(II)I

    move-result v9

    iget v10, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    if-lt v9, v10, :cond_3

    .line 675
    iget v9, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    add-int/2addr v9, v5

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v0, v9, v4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->squaredDistanceFrom(II)I

    move-result v9

    iget v10, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    if-lt v9, v10, :cond_3

    .line 676
    iget v9, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    add-int/2addr v9, v5

    add-int/lit8 v9, v9, -0x1

    iget v10, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    add-int/2addr v10, v4

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v0, v9, v10}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->squaredDistanceFrom(II)I

    move-result v9

    .line 677
    iget v10, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    if-lt v9, v10, :cond_3

    .line 678
    iget v9, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    add-int/2addr v9, v4

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v0, v5, v9}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->squaredDistanceFrom(II)I

    move-result v0

    iget v9, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    if-ge v0, v9, :cond_4

    .line 679
    :cond_3
    add-int/lit8 v0, v3, 0x1

    aput v1, v6, v3

    move v3, v0

    .line 672
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Landroid/content/res/XmlResourceParser;)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 717
    .line 725
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object v5, v2

    move v4, v7

    move v3, v7

    move v8, v7

    move v9, v7

    .line 767
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {p2}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ne v0, v6, :cond_1

    .line 777
    :goto_1
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    sub-int v0, v4, v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->h:I

    .line 778
    return-void

    .line 731
    :cond_1
    const/4 v10, 0x2

    if-ne v0, v10, :cond_6

    .line 732
    :try_start_1
    invoke-interface {p2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 733
    const-string v10, "Row"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 736
    invoke-virtual {p0, v1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->createRowFromXml(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;

    move-result-object v2

    .line 737
    iget v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->mode:I

    if-eqz v0, :cond_2

    iget v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->mode:I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->n:I

    if-eq v0, v3, :cond_2

    move v0, v6

    .line 738
    :goto_2
    if-eqz v0, :cond_a

    .line 739
    invoke-static {p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/XmlResourceParser;)V

    move v3, v7

    move v8, v7

    .line 740
    goto :goto_0

    :cond_2
    move v0, v7

    .line 737
    goto :goto_2

    .line 742
    :cond_3
    const-string v10, "Key"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    move-object v0, p0

    move-object v5, p2

    .line 744
    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->createKeyFromXml(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;IILandroid/content/res/XmlResourceParser;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    move-result-object v0

    .line 745
    iget-object v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 746
    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    const/4 v9, 0x0

    aget v5, v5, v9

    const/4 v9, -0x1

    if-ne v5, v9, :cond_4

    .line 747
    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->f:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 748
    iget-object v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->g:I

    .line 749
    iget-object v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->k:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v0

    move v9, v6

    goto :goto_0

    .line 750
    :cond_4
    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    const/4 v9, 0x0

    aget v5, v5, v9

    const/4 v9, -0x6

    if-ne v5, v9, :cond_9

    .line 751
    iget-object v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->k:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v0

    move v9, v6

    goto :goto_0

    .line 753
    :cond_5
    const-string v10, "Keyboard"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 754
    invoke-direct {p0, v1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 773
    :catch_0
    move-exception v0

    .line 774
    const-string v1, "Keyboard"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Parse error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 756
    :cond_6
    const/4 v10, 0x3

    if-ne v0, v10, :cond_0

    .line 757
    if-eqz v9, :cond_7

    .line 759
    :try_start_2
    iget v0, v5, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->gap:I

    iget v9, v5, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v0, v9

    add-int/2addr v3, v0

    .line 760
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->i:I

    if-le v3, v0, :cond_8

    .line 761
    iput v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->i:I

    move v9, v7

    goto/16 :goto_0

    .line 763
    :cond_7
    if-eqz v8, :cond_0

    .line 765
    iget v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->verticalGap:I

    add-int/2addr v4, v0

    .line 766
    iget v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;->defaultHeight:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/2addr v4, v0

    move v8, v7

    goto/16 :goto_0

    :cond_8
    move v9, v7

    goto/16 :goto_0

    :cond_9
    move-object v5, v0

    move v9, v6

    goto/16 :goto_0

    :cond_a
    move v3, v7

    move v8, v6

    goto/16 :goto_0
.end method

.method private a(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 792
    invoke-static {p2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 793
    sget-object v1, Lgbis/gbandroid/R$styleable;->Keyboard:[I

    .line 792
    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 797
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    div-int/lit8 v2, v2, 0x4

    .line 795
    invoke-static {v0, v4, v1, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    .line 799
    const/4 v1, 0x1

    .line 800
    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->m:I

    const/16 v3, 0x32

    .line 798
    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c:I

    .line 802
    const/4 v1, 0x2

    .line 803
    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->l:I

    .line 801
    invoke-static {v0, v1, v2, v4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a:I

    .line 805
    const/4 v1, 0x3

    .line 806
    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->m:I

    .line 804
    invoke-static {v0, v1, v2, v4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a(Landroid/content/res/TypedArray;III)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    .line 807
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    int-to-float v1, v1

    sget v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->s:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    .line 808
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    mul-int/2addr v1, v2

    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->r:I

    .line 809
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 810
    return-void
.end method

.method private static a(Landroid/content/res/XmlResourceParser;)V
    .locals 2
    .parameter

    .prologue
    .line 783
    .line 786
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 789
    :cond_1
    return-void

    .line 784
    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 785
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Row"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 96
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    return v0
.end method

.method static synthetic c(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 132
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->m:I

    return v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 99
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c:I

    return v0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 93
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a:I

    return v0
.end method

.method static synthetic f(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 102
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    return v0
.end method

.method static synthetic g(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)I
    .locals 1
    .parameter

    .prologue
    .line 120
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->i:I

    return v0
.end method


# virtual methods
.method protected createKeyFromXml(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;IILandroid/content/res/XmlResourceParser;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 713
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;-><init>(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;IILandroid/content/res/XmlResourceParser;)V

    return-object v0
.end method

.method protected createRowFromXml(Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 708
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;

    invoke-direct {v0, p1, p0, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Row;-><init>(Landroid/content/res/Resources;Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;Landroid/content/res/XmlResourceParser;)V

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 635
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->h:I

    return v0
.end method

.method protected getHorizontalGap()I
    .locals 1

    .prologue
    .line 599
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a:I

    return v0
.end method

.method protected getKeyHeight()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c:I

    return v0
.end method

.method protected getKeyWidth()I
    .locals 1

    .prologue
    .line 623
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    return v0
.end method

.method public getKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->j:Ljava/util/List;

    return-object v0
.end method

.method public getMinWidth()I
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->i:I

    return v0
.end method

.method public getModifierKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->k:Ljava/util/List;

    return-object v0
.end method

.method public getNearestKeys(II)[I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 697
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->q:[[I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a()V

    .line 698
    :cond_0
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getMinWidth()I

    move-result v0

    if-ge p1, v0, :cond_1

    if-ltz p2, :cond_1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getHeight()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 699
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->p:I

    div-int v0, p2, v0

    mul-int/lit8 v0, v0, 0xa

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->o:I

    div-int v1, p1, v1

    add-int/2addr v0, v1

    .line 700
    const/16 v1, 0x32

    if-ge v0, v1, :cond_1

    .line 701
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->q:[[I

    aget-object v0, v1, v0

    .line 704
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [I

    goto :goto_0
.end method

.method public getShiftKeyIndex()I
    .locals 1

    .prologue
    .line 658
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->g:I

    return v0
.end method

.method protected getVerticalGap()I
    .locals 1

    .prologue
    .line 607
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    return v0
.end method

.method public isShifted()Z
    .locals 1

    .prologue
    .line 654
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->e:Z

    return v0
.end method

.method protected setHorizontalGap(I)V
    .locals 0
    .parameter

    .prologue
    .line 603
    iput p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->a:I

    .line 604
    return-void
.end method

.method protected setKeyHeight(I)V
    .locals 0
    .parameter

    .prologue
    .line 619
    iput p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->c:I

    .line 620
    return-void
.end method

.method protected setKeyWidth(I)V
    .locals 0
    .parameter

    .prologue
    .line 627
    iput p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->b:I

    .line 628
    return-void
.end method

.method public setShifted(Z)Z
    .locals 1
    .parameter

    .prologue
    .line 643
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->f:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->f:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    iput-boolean p1, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->on:Z

    .line 646
    :cond_0
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->e:Z

    if-eq v0, p1, :cond_1

    .line 647
    iput-boolean p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->e:Z

    .line 648
    const/4 v0, 0x1

    .line 650
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setVerticalGap(I)V
    .locals 0
    .parameter

    .prologue
    .line 611
    iput p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->d:I

    .line 612
    return-void
.end method
