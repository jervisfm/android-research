.class public Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;
.super Landroid/view/View;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;
    }
.end annotation


# static fields
.field private static N:I

.field private static final a:[I


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:J

.field private F:J

.field private G:[I

.field private H:Landroid/view/GestureDetector;

.field private I:I

.field private J:Z

.field private K:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

.field private L:Landroid/graphics/Rect;

.field private M:Landroid/graphics/drawable/Drawable;

.field private O:[I

.field private P:I

.field private Q:I

.field private R:J

.field private S:Z

.field private T:Z

.field private U:Landroid/graphics/Rect;

.field private V:Landroid/graphics/Bitmap;

.field private W:Landroid/graphics/Canvas;

.field b:Landroid/os/Handler;

.field private c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:F

.field private i:I

.field private j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

.field private k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Z

.field private s:Landroid/graphics/Paint;

.field private t:Landroid/graphics/Rect;

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 74
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, -0x5

    aput v2, v0, v1

    sput-object v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a:[I

    .line 134
    const/16 v0, 0xc

    sput v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->N:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 177
    const v0, 0x7f010008

    invoke-direct {p0, p1, p2, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x100

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 181
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    .line 107
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    .line 108
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    .line 109
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    .line 110
    iput v8, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->x:I

    .line 117
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 120
    const/16 v0, 0xc

    new-array v0, v0, [I

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->G:[I

    .line 122
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    .line 125
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->L:Landroid/graphics/Rect;

    .line 135
    sget v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->N:I

    new-array v0, v0, [I

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    .line 147
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->U:Landroid/graphics/Rect;

    .line 153
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/a;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/a;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    .line 183
    sget-object v0, Lgbis/gbandroid/R$styleable;->KeyboardView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 185
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    move v0, v1

    .line 189
    :goto_0
    if-lt v0, v3, :cond_0

    .line 211
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    .line 212
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    .line 213
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    .line 214
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->x:I

    .line 216
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->s:Landroid/graphics/Paint;

    .line 217
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 218
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->s:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 219
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->s:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 221
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->t:Landroid/graphics/Rect;

    .line 222
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->M:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 225
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e()V

    .line 226
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a()V

    .line 227
    return-void

    .line 190
    :cond_0
    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 193
    if-ne v4, v7, :cond_2

    .line 194
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->M:Landroid/graphics/drawable/Drawable;

    .line 189
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_2
    const/4 v5, 0x5

    if-ne v4, v5, :cond_3

    .line 196
    invoke-virtual {v2, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->l:I

    goto :goto_1

    .line 197
    :cond_3
    if-ne v4, v8, :cond_4

    .line 198
    const/16 v5, 0x12

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->f:I

    goto :goto_1

    .line 199
    :cond_4
    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 200
    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->g:I

    goto :goto_1

    .line 201
    :cond_5
    const/4 v5, 0x3

    if-ne v4, v5, :cond_6

    .line 202
    const/16 v5, 0xe

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e:I

    goto :goto_1

    .line 203
    :cond_6
    const/4 v5, 0x6

    if-ne v4, v5, :cond_7

    .line 204
    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->i:I

    goto :goto_1

    .line 205
    :cond_7
    const/4 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 206
    const/high16 v5, 0x4120

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    iput v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->h:F

    goto :goto_1
.end method

.method private a(II[I)I
    .locals 18
    .parameter
    .parameter
    .parameter

    .prologue
    .line 510
    move-object/from16 v0, p0

    iget-object v9, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 511
    const/4 v6, -0x1

    .line 512
    const/4 v3, -0x1

    .line 513
    move-object/from16 v0, p0

    iget v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    add-int/lit8 v4, v2, 0x1

    .line 514
    move-object/from16 v0, p0

    iget-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    const v5, 0x7fffffff

    invoke-static {v2, v5}, Ljava/util/Arrays;->fill([II)V

    .line 515
    move-object/from16 v0, p0

    iget-object v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getNearestKeys(II)[I

    move-result-object v10

    .line 516
    array-length v11, v10

    .line 517
    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-lt v8, v11, :cond_1

    .line 554
    const/4 v2, -0x1

    if-ne v6, v2, :cond_0

    move v6, v3

    .line 557
    :cond_0
    return v6

    .line 518
    :cond_1
    aget v2, v10, v8

    aget-object v12, v9, v2

    .line 519
    const/4 v2, 0x0

    .line 520
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->isInside(II)Z

    move-result v13

    .line 521
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->r:Z

    if-eqz v5, :cond_2

    .line 522
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v12, v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->squaredDistanceFrom(II)I

    move-result v2

    move-object/from16 v0, p0

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    if-lt v2, v5, :cond_3

    .line 523
    :cond_2
    if-eqz v13, :cond_a

    .line 524
    :cond_3
    iget-object v5, v12, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    const/4 v7, 0x0

    aget v5, v5, v7

    const/16 v7, 0x20

    if-le v5, v7, :cond_a

    .line 526
    iget-object v5, v12, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    array-length v14, v5

    .line 527
    if-ge v2, v4, :cond_9

    .line 529
    aget v5, v10, v8

    move v4, v2

    .line 532
    :goto_1
    if-eqz p3, :cond_8

    .line 534
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    array-length v7, v7

    if-lt v3, v7, :cond_4

    move v2, v4

    move v3, v5

    .line 550
    :goto_3
    if-eqz v13, :cond_7

    .line 551
    aget v4, v10, v8

    .line 517
    :goto_4
    add-int/lit8 v5, v8, 0x1

    move v8, v5

    move v6, v4

    move v4, v2

    goto :goto_0

    .line 535
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    aget v7, v7, v3

    if-le v7, v2, :cond_6

    .line 537
    move-object/from16 v0, p0

    iget-object v7, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    move-object/from16 v0, p0

    iget-object v15, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    add-int v16, v3, v14

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    sub-int v17, v17, v3

    sub-int v17, v17, v14

    .line 537
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v7, v3, v15, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 539
    add-int v7, v3, v14

    .line 540
    move-object/from16 v0, p3

    array-length v15, v0

    sub-int/2addr v15, v3

    sub-int/2addr v15, v14

    .line 539
    move-object/from16 v0, p3

    move-object/from16 v1, p3

    invoke-static {v0, v3, v1, v7, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 541
    const/4 v7, 0x0

    :goto_5
    if-lt v7, v14, :cond_5

    move v2, v4

    move v3, v5

    .line 545
    goto :goto_3

    .line 542
    :cond_5
    add-int v15, v3, v7

    iget-object v0, v12, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    move-object/from16 v16, v0

    aget v16, v16, v7

    aput v16, p3, v15

    .line 543
    move-object/from16 v0, p0

    iget-object v15, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->O:[I

    add-int v16, v3, v7

    aput v2, v15, v16

    .line 541
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 534
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    move v4, v6

    goto :goto_4

    :cond_8
    move v2, v4

    move v3, v5

    move v4, v6

    goto :goto_4

    :cond_9
    move v5, v3

    goto :goto_1

    :cond_a
    move v2, v4

    goto :goto_3
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->isShifted()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 338
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    .line 341
    :cond_0
    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/b;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/b;-><init>(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->H:Landroid/view/GestureDetector;

    .line 233
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->H:Landroid/view/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 234
    return-void
.end method

.method private a(I)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 592
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    .line 594
    iput p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    .line 596
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 597
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    if-eq v1, v0, :cond_1

    .line 598
    if-eq v1, v4, :cond_0

    array-length v0, v2

    if-le v0, v1, :cond_0

    .line 599
    aget-object v3, v2, v1

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->onReleased(Z)V

    .line 600
    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b(I)V

    .line 602
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    if-eq v0, v4, :cond_1

    array-length v0, v2

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    if-le v0, v1, :cond_1

    .line 603
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    aget-object v0, v2, v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->onPressed()V

    .line 604
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d:I

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b(I)V

    .line 607
    :cond_1
    return-void

    .line 599
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IIJ)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 561
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 562
    if-eq v1, v5, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 563
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    aget-object v2, v0, v1

    .line 564
    iget-object v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->text:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    iget-object v2, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->text:Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onText(Ljava/lang/CharSequence;)V

    .line 566
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    invoke-interface {v0, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onRelease(I)V

    .line 585
    :goto_0
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->P:I

    .line 586
    iput-wide p3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->R:J

    .line 588
    :cond_0
    return-void

    .line 568
    :cond_1
    iget-object v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    aget v0, v0, v6

    .line 570
    sget v3, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->N:I

    new-array v3, v3, [I

    .line 571
    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([II)V

    .line 572
    invoke-direct {p0, p1, p2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(II[I)I

    .line 574
    iget-boolean v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->S:Z

    if-eqz v4, :cond_2

    .line 575
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    if-eq v0, v5, :cond_3

    .line 576
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    const/4 v4, -0x5

    sget-object v5, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a:[I

    invoke-interface {v0, v4, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onKey(I[I)V

    .line 580
    :goto_1
    iget-object v0, v2, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    aget v0, v0, v2

    .line 582
    :cond_2
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    invoke-interface {v2, v0, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onKey(I[I)V

    .line 583
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    invoke-interface {v2, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onRelease(I)V

    goto :goto_0

    .line 578
    :cond_3
    iput v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    goto :goto_1
.end method

.method private a(JI)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x320

    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 785
    if-ne p3, v3, :cond_1

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    aget-object v0, v0, p3

    .line 787
    iget-object v1, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    array-length v1, v1

    if-le v1, v2, :cond_3

    .line 788
    iput-boolean v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->S:Z

    .line 789
    iget-wide v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->R:J

    add-long/2addr v1, v4

    cmp-long v1, p1, v1

    if-gez v1, :cond_2

    .line 790
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->P:I

    if-ne p3, v1, :cond_2

    .line 791
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    add-int/lit8 v1, v1, 0x1

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    array-length v0, v0

    rem-int v0, v1, v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    goto :goto_0

    .line 794
    :cond_2
    iput v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    goto :goto_0

    .line 798
    :cond_3
    iget-wide v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->R:J

    add-long/2addr v0, v4

    cmp-long v0, p1, v0

    if-gtz v0, :cond_4

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->P:I

    if-eq p3, v0, :cond_0

    .line 799
    :cond_4
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e()V

    goto :goto_0
.end method

.method private a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)V
    .locals 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 365
    if-nez p1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 367
    if-eqz v2, :cond_0

    .line 368
    array-length v3, v2

    move v1, v0

    .line 370
    :goto_1
    if-lt v0, v3, :cond_2

    .line 374
    if-ltz v1, :cond_0

    if-eqz v3, :cond_0

    .line 375
    int-to-float v0, v1

    const v1, 0x3fb33333

    mul-float/2addr v0, v1

    int-to-float v1, v3

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    .line 376
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    mul-int/2addr v0, v1

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->m:I

    goto :goto_0

    .line 371
    :cond_2
    aget-object v4, v2, v0

    .line 372
    iget v5, v4, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    iget v6, v4, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget v4, v4, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->gap:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;)Z
    .locals 1
    .parameter

    .prologue
    .line 754
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d()Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 22

    .prologue
    .line 396
    move-object/from16 v0, p0

    iget-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    .line 397
    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getWidth()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    .line 398
    new-instance v4, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->W:Landroid/graphics/Canvas;

    .line 399
    invoke-direct/range {p0 .. p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c()V

    .line 401
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->W:Landroid/graphics/Canvas;

    .line 402
    move-object/from16 v0, p0

    iget-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->U:Landroid/graphics/Rect;

    sget-object v5, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v8, v4, v5}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 404
    move-object/from16 v0, p0

    iget-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    if-nez v4, :cond_1

    .line 507
    :goto_0
    return-void

    .line 406
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->s:Landroid/graphics/Paint;

    .line 407
    move-object/from16 v0, p0

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->L:Landroid/graphics/Rect;

    .line 408
    move-object/from16 v0, p0

    iget-object v10, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->t:Landroid/graphics/Rect;

    .line 409
    move-object/from16 v0, p0

    iget v11, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    .line 410
    move-object/from16 v0, p0

    iget v12, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    .line 411
    move-object/from16 v0, p0

    iget-object v13, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 412
    move-object/from16 v0, p0

    iget-object v14, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->K:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 414
    const/16 v4, 0xff

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 415
    move-object/from16 v0, p0

    iget v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->g:I

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 416
    const/4 v4, 0x0

    .line 418
    if-eqz v14, :cond_2

    invoke-virtual {v8, v5}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 420
    iget v6, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    add-int/2addr v6, v11

    add-int/lit8 v6, v6, -0x1

    iget v7, v5, Landroid/graphics/Rect;->left:I

    if-gt v6, v7, :cond_2

    .line 421
    iget v6, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    add-int/2addr v6, v12

    add-int/lit8 v6, v6, -0x1

    iget v7, v5, Landroid/graphics/Rect;->top:I

    if-gt v6, v7, :cond_2

    .line 422
    iget v6, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v7, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v6, v7

    add-int/2addr v6, v11

    add-int/lit8 v6, v6, 0x1

    iget v7, v5, Landroid/graphics/Rect;->right:I

    if-lt v6, v7, :cond_2

    .line 423
    iget v6, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v7, v14, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    add-int/2addr v6, v7

    add-int/2addr v6, v12

    add-int/lit8 v6, v6, 0x1

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    if-lt v6, v5, :cond_2

    .line 424
    const/4 v4, 0x1

    .line 428
    :cond_2
    const/4 v5, 0x0

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v8, v5, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 429
    array-length v15, v13

    .line 430
    const/4 v5, 0x0

    move v7, v5

    :goto_1
    if-lt v7, v15, :cond_3

    .line 492
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->K:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 505
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->T:Z

    .line 506
    move-object/from16 v0, p0

    iget-object v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->U:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_0

    .line 431
    :cond_3
    aget-object v16, v13, v7

    .line 432
    if-eqz v4, :cond_4

    move-object/from16 v0, v16

    if-ne v14, v0, :cond_9

    .line 433
    :cond_4
    move-object/from16 v0, v16

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    move-object/from16 v0, v16

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v5, v6

    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getWidth()I

    move-result v6

    move-object/from16 v0, p0

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    move/from16 v17, v0

    sub-int v6, v6, v17

    if-lt v5, v6, :cond_5

    .line 438
    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getWidth()I

    move-result v5

    move-object/from16 v0, v16

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    sub-int/2addr v5, v6

    move-object/from16 v0, v16

    iput v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    .line 441
    :cond_5
    move-object/from16 v0, v16

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->background:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->M:Landroid/graphics/drawable/Drawable;

    .line 443
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->getCurrentDrawableState()[I

    move-result-object v6

    .line 444
    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 447
    move-object/from16 v0, v16

    iget-object v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->label:Ljava/lang/CharSequence;

    if-nez v6, :cond_b

    const/4 v6, 0x0

    .line 449
    :goto_3
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v17

    .line 450
    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 451
    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move/from16 v0, v18

    move/from16 v1, v17

    if-eq v0, v1, :cond_7

    .line 452
    :cond_6
    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    move/from16 v19, v0

    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    move/from16 v20, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 454
    :cond_7
    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    move/from16 v17, v0

    add-int v17, v17, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    move/from16 v18, v0

    add-int v18, v18, v12

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 455
    invoke-virtual {v5, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 457
    if-eqz v6, :cond_d

    .line 459
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v17, 0x1

    move/from16 v0, v17

    if-le v5, v0, :cond_c

    move-object/from16 v0, v16

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    array-length v5, v5

    const/16 v17, 0x2

    move/from16 v0, v17

    if-ge v5, v0, :cond_c

    .line 460
    move-object/from16 v0, p0

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e:I

    int-to-float v5, v5

    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 462
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 468
    :goto_4
    move-object/from16 v0, p0

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->h:F

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->i:I

    move/from16 v19, v0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v9, v5, v0, v1, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 471
    move-object/from16 v0, v16

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    sub-int v5, v5, v17

    iget v0, v10, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    sub-int v5, v5, v17

    div-int/lit8 v5, v5, 0x2

    iget v0, v10, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    add-int v5, v5, v17

    int-to-float v5, v5

    .line 472
    move-object/from16 v0, v16

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    move/from16 v17, v0

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    sub-int v17, v17, v18

    div-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual {v9}, Landroid/graphics/Paint;->getTextSize()F

    move-result v18

    invoke-virtual {v9}, Landroid/graphics/Paint;->descent()F

    move-result v19

    sub-float v18, v18, v19

    const/high16 v19, 0x4000

    div-float v18, v18, v19

    add-float v17, v17, v18

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-float v17, v17, v18

    .line 470
    move/from16 v0, v17

    invoke-virtual {v8, v6, v5, v0, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 476
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v5, v6, v0, v1}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 490
    :cond_8
    :goto_5
    move-object/from16 v0, v16

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    neg-int v5, v5

    sub-int/2addr v5, v11

    int-to-float v5, v5

    move-object/from16 v0, v16

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    neg-int v6, v6

    sub-int/2addr v6, v12

    int-to-float v6, v6

    invoke-virtual {v8, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 430
    :cond_9
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_1

    .line 441
    :cond_a
    move-object/from16 v0, v16

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->background:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_2

    .line 447
    :cond_b
    move-object/from16 v0, v16

    iget-object v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->label:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3

    .line 464
    :cond_c
    move-object/from16 v0, p0

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->f:I

    int-to-float v5, v5

    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 465
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v9, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_4

    .line 478
    :cond_d
    move-object/from16 v0, v16

    iget-object v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_8

    .line 479
    move-object/from16 v0, v16

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    iget v6, v10, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    iget v6, v10, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v6

    .line 480
    move-object/from16 v0, v16

    iget-object v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 479
    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 480
    iget v6, v10, Landroid/graphics/Rect;->left:I

    .line 479
    add-int/2addr v5, v6

    .line 481
    move-object/from16 v0, v16

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    sub-int v6, v6, v17

    iget v0, v10, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    sub-int v6, v6, v17

    .line 482
    move-object/from16 v0, v16

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v17

    .line 481
    sub-int v6, v6, v17

    div-int/lit8 v6, v6, 0x2

    .line 482
    iget v0, v10, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    .line 481
    add-int v6, v6, v17

    .line 483
    int-to-float v0, v5

    move/from16 v17, v0

    int-to-float v0, v6

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 484
    move-object/from16 v0, v16

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    .line 485
    move-object/from16 v0, v16

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v20

    move-object/from16 v0, v16

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v21

    .line 484
    invoke-virtual/range {v17 .. v21}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 486
    move-object/from16 v0, v16

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 487
    neg-int v5, v5

    int-to-float v5, v5

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {v8, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_5
.end method

.method private b(I)V
    .locals 7
    .parameter

    .prologue
    .line 617
    if-ltz p1, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    aget-object v0, v0, p1

    .line 621
    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->K:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 622
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->U:Landroid/graphics/Rect;

    iget v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    add-int/2addr v2, v3

    iget v3, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    add-int/2addr v3, v4

    .line 623
    iget v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v4, v5

    iget v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    add-int/2addr v4, v5

    iget v5, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v6, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    add-int/2addr v5, v6

    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    add-int/2addr v5, v6

    .line 622
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->union(IIII)V

    .line 624
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b()V

    .line 625
    iget v1, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    add-int/2addr v1, v2

    iget v2, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    add-int/2addr v2, v3

    .line 626
    iget v3, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->width:I

    add-int/2addr v3, v4

    iget v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    add-int/2addr v3, v4

    iget v4, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->height:I

    add-int/2addr v0, v4

    iget v4, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    add-int/2addr v0, v4

    .line 625
    invoke-virtual {p0, v1, v2, v3, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->invalidate(IIII)V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 611
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->U:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->union(IIII)V

    .line 612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->T:Z

    .line 613
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->invalidate()V

    .line 614
    return-void
.end method

.method private d()Z
    .locals 4

    .prologue
    .line 755
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    aget-object v0, v0, v1

    .line 756
    iget v1, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->x:I

    iget v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->y:I

    iget-wide v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->R:J

    invoke-direct {p0, v1, v0, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(IIJ)V

    .line 757
    const/4 v0, 0x1

    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 778
    const/4 v0, -0x1

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->P:I

    .line 779
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->Q:I

    .line 780
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->R:J

    .line 781
    iput-boolean v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->S:Z

    .line 782
    return-void
.end method


# virtual methods
.method public closing()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 763
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 764
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 765
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iput-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    .line 768
    iput-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->W:Landroid/graphics/Canvas;

    .line 769
    return-void
.end method

.method public getKeyboard()Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    return-object v0
.end method

.method protected getOnKeyboardActionListener()Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    return-object v0
.end method

.method public isProximityCorrectionEnabled()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->r:Z

    return v0
.end method

.method public isShifted()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->isShifted()Z

    move-result v0

    .line 312
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 773
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 774
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->closing()V

    .line 775
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 388
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 389
    iget-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->T:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 390
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b()V

    .line 392
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 393
    return-void
.end method

.method protected onLongPress(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;)Z
    .locals 1
    .parameter

    .prologue
    .line 635
    const/4 v0, 0x0

    return v0
.end method

.method public onMeasure(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 347
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    if-nez v0, :cond_0

    .line 348
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    add-int/2addr v0, v1

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->x:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->setMeasuredDimension(II)V

    .line 356
    :goto_0
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getMinWidth()I

    move-result v0

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    add-int/2addr v0, v1

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->v:I

    add-int/2addr v0, v1

    .line 351
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    add-int/lit8 v2, v0, 0xa

    if-ge v1, v2, :cond_1

    .line 352
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 354
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getHeight()I

    move-result v1

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    add-int/2addr v1, v2

    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->x:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 381
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    .line 384
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .parameter

    .prologue
    .line 640
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->u:I

    sub-int v2, v0, v1

    .line 641
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->l:I

    add-int/2addr v0, v1

    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->w:I

    sub-int v1, v0, v1

    .line 642
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 643
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    .line 644
    const/4 v5, 0x0

    invoke-direct {p0, v2, v1, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(II[I)I

    move-result v5

    .line 646
    iget-object v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->H:Landroid/view/GestureDetector;

    invoke-virtual {v6, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 647
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(I)V

    .line 648
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 649
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 650
    const/4 v0, 0x1

    .line 751
    :goto_0
    return v0

    .line 654
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 749
    :goto_1
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->n:I

    .line 750
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->o:I

    .line 751
    const/4 v0, 0x1

    goto :goto_0

    .line 656
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->J:Z

    .line 657
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->p:I

    .line 658
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->q:I

    .line 659
    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->B:I

    .line 660
    iput v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->C:I

    .line 661
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->E:J

    .line 662
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    .line 663
    const/4 v0, -0x1

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->A:I

    .line 664
    iput v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 665
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->y:J

    .line 666
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->y:J

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->z:J

    .line 667
    invoke-direct {p0, v3, v4, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(JI)V

    .line 668
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    const/4 v0, -0x1

    if-eq v5, v0, :cond_3

    .line 669
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    aget-object v0, v0, v5

    iget-object v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->codes:[I

    const/4 v4, 0x0

    aget v0, v0, v4

    .line 668
    :goto_2
    invoke-interface {v3, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;->onPress(I)V

    .line 670
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    aget-object v0, v0, v3

    iget-boolean v0, v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;->repeatable:Z

    if-eqz v0, :cond_1

    .line 671
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    .line 672
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->d()Z

    .line 673
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 674
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const-wide/16 v6, 0x190

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 676
    :cond_1
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 677
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v0, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 678
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const-wide/16 v6, 0x320

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 680
    :cond_2
    invoke-direct {p0, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(I)V

    goto :goto_1

    .line 669
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 684
    :pswitch_1
    const/4 v0, 0x0

    .line 685
    const/4 v6, -0x1

    if-eq v5, v6, :cond_4

    .line 686
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_6

    .line 687
    iput v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 688
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->y:J

    sub-long/2addr v3, v6

    iput-wide v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    .line 704
    :goto_3
    iget v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    if-eq v5, v3, :cond_4

    .line 705
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 706
    const/4 v3, -0x1

    iput v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    .line 709
    :cond_4
    if-nez v0, :cond_5

    .line 711
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 713
    const/4 v0, -0x1

    if-eq v5, v0, :cond_5

    .line 714
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v0, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 715
    iget-object v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const-wide/16 v6, 0x320

    invoke-virtual {v3, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 718
    :cond_5
    invoke-direct {p0, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(I)V

    goto/16 :goto_1

    .line 690
    :cond_6
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    if-ne v5, v6, :cond_7

    .line 691
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    iget-wide v8, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->z:J

    sub-long/2addr v3, v8

    add-long/2addr v3, v6

    iput-wide v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    .line 692
    const/4 v0, 0x1

    goto :goto_3

    .line 694
    :cond_7
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e()V

    .line 695
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    iput v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->A:I

    .line 696
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->n:I

    iput v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->B:I

    .line 697
    iget v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->o:I

    iput v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->C:I

    .line 699
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    add-long/2addr v3, v6

    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->z:J

    sub-long/2addr v3, v6

    .line 698
    iput-wide v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->E:J

    .line 700
    iput v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 701
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    goto :goto_3

    .line 722
    :pswitch_2
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 723
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 724
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b:Landroid/os/Handler;

    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 725
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    if-ne v5, v0, :cond_9

    .line 726
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    iget-wide v8, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->z:J

    sub-long v8, v3, v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    .line 734
    :goto_4
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    iget-wide v8, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->E:J

    cmp-long v0, v6, v8

    if-gez v0, :cond_a

    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->A:I

    const/4 v6, -0x1

    if-eq v0, v6, :cond_a

    .line 735
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->A:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 736
    iget v1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->B:I

    .line 737
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->C:I

    .line 739
    :goto_5
    const/4 v2, -0x1

    invoke-direct {p0, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(I)V

    .line 740
    iget-object v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->G:[I

    const/4 v6, -0x1

    invoke-static {v2, v6}, Ljava/util/Arrays;->fill([II)V

    .line 742
    iget v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    const/4 v6, -0x1

    if-ne v2, v6, :cond_8

    iget-boolean v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->J:Z

    if-nez v2, :cond_8

    .line 743
    invoke-direct {p0, v1, v0, v3, v4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(IIJ)V

    .line 745
    :cond_8
    invoke-direct {p0, v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->b(I)V

    .line 746
    const/4 v2, -0x1

    iput v2, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->I:I

    move v2, v1

    move v1, v0

    goto/16 :goto_1

    .line 728
    :cond_9
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->e()V

    .line 729
    iget v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    iput v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->A:I

    .line 730
    iget-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    add-long/2addr v6, v3

    iget-wide v8, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->z:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->E:J

    .line 731
    iput v5, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->D:I

    .line 732
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->F:J

    goto :goto_4

    :cond_a
    move v0, v1

    move v1, v2

    goto :goto_5

    .line 654
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setKeyboard(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)V
    .locals 2
    .parameter

    .prologue
    .line 256
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(I)V

    .line 259
    :cond_0
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    .line 260
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->getKeys()Ljava/util/List;

    move-result-object v0

    .line 261
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->j:[Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard$Key;

    .line 262
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->requestLayout()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->V:Landroid/graphics/Bitmap;

    .line 266
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c()V

    .line 267
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->a(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;)V

    .line 274
    return-void
.end method

.method public setOnKeyboardActionListener(Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;)V
    .locals 0
    .parameter

    .prologue
    .line 237
    iput-object p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->k:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView$OnKeyboardActionListener;

    .line 238
    return-void
.end method

.method public setProximityCorrectionEnabled(Z)V
    .locals 0
    .parameter

    .prologue
    .line 325
    iput-boolean p1, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->r:Z

    .line 326
    return-void
.end method

.method public setShifted(Z)Z
    .locals 1
    .parameter

    .prologue
    .line 292
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c:Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboard;->setShifted(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-direct {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/CustomKeyboardView;->c()V

    .line 296
    const/4 v0, 0x1

    .line 299
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
