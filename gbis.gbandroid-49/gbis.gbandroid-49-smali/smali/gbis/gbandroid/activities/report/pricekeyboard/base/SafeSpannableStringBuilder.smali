.class public Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
.super Landroid/text/SpannableStringBuilder;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder$Factory;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;II)V

    .line 23
    return-void
.end method


# virtual methods
.method public bridge synthetic delete(II)Landroid/text/Editable;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic delete(II)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-super {p0, p1, p2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    return-object v0
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;)Landroid/text/Editable;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;II)Landroid/text/Editable;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->insert(ILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic insert(ILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->insert(ILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 130
    const/4 v4, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v1, p1

    move v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public insert(ILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    move-object v0, p0

    move v1, p1

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public overwrite(Ljava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 55
    .line 58
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 69
    :goto_0
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, v3, v1, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 73
    :cond_0
    if-eqz p1, :cond_1

    .line 74
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 77
    :cond_1
    return-object p0

    .line 61
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    if-gt v1, v2, :cond_3

    move-object v4, v0

    move-object v0, p1

    move-object p1, v4

    .line 62
    goto :goto_0

    .line 65
    :cond_3
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 66
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p1, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;)Landroid/text/Editable;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual/range {p0 .. p5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    const/4 v4, 0x0

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public replace(IILjava/lang/CharSequence;II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 145
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v0

    if-le p2, v0, :cond_5

    .line 146
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    .line 149
    :goto_0
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le p5, v0, :cond_4

    .line 150
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 153
    :goto_1
    if-gez p1, :cond_3

    move v1, v4

    .line 154
    :goto_2
    if-le v1, v2, :cond_0

    move v1, v2

    .line 156
    :cond_0
    if-gez p4, :cond_2

    .line 157
    :goto_3
    if-le v4, v5, :cond_1

    move v4, v5

    :cond_1
    move-object v0, p0

    move-object v3, p3

    .line 159
    invoke-super/range {v0 .. v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    return-object v0

    :cond_2
    move v4, p4

    goto :goto_3

    :cond_3
    move v1, p1

    goto :goto_2

    :cond_4
    move v5, p5

    goto :goto_1

    :cond_5
    move v2, p2

    goto :goto_0
.end method

.method public replaceAllCharacters(Ljava/lang/Character;Ljava/lang/Character;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v1

    .line 87
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    .line 93
    return-object p0

    .line 88
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p2}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 87
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public stripCharacter(Ljava/lang/Character;)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;
    .locals 3
    .parameter

    .prologue
    .line 103
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v1

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_0
    if-lt v0, v1, :cond_0

    .line 116
    return-object p0

    .line 107
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Character;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 108
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v0, v2}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->delete(II)Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;

    .line 109
    if-lez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    .line 110
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 112
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v0

    if-le p2, v0, :cond_4

    .line 29
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v0

    .line 32
    :goto_0
    if-gez v0, :cond_0

    move v0, v1

    .line 36
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    if-le p1, v2, :cond_3

    .line 37
    invoke-virtual {p0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder;->length()I

    move-result v2

    .line 40
    :goto_1
    if-gez v2, :cond_2

    .line 44
    :goto_2
    if-le v1, v0, :cond_1

    move v1, v0

    .line 48
    :cond_1
    invoke-super {p0, v1, v0}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v2, p1

    goto :goto_1

    :cond_4
    move v0, p2

    goto :goto_0
.end method
