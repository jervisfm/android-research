.class final Lgbis/gbandroid/activities/report/u;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/activities/report/pricekeyboard/PriceKeyboardDrawer$OnDrawerStateChangedListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/report/ReportPrices;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/report/ReportPrices;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    .line 573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClose()V
    .locals 2

    .prologue
    .line 591
    const-string v0, "GasBuddy"

    const-string v1, "Closing keyboard"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    iget-object v0, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->m(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 594
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 595
    iget-object v1, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->m(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 597
    iget-object v0, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->s(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 598
    return-void
.end method

.method public final onOpen()V
    .locals 3

    .prologue
    .line 578
    const-string v0, "GasBuddy"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Opening keyboard, fulllayoutheight="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v2}, Lgbis/gbandroid/activities/report/ReportPrices;->n(Lgbis/gbandroid/activities/report/ReportPrices;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", priceKeyboardViewH="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v2}, Lgbis/gbandroid/activities/report/ReportPrices;->q(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    iget-object v0, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->m(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 582
    iget-object v1, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->n(Lgbis/gbandroid/activities/report/ReportPrices;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v2}, Lgbis/gbandroid/activities/report/ReportPrices;->q(Lgbis/gbandroid/activities/report/ReportPrices;)Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;

    move-result-object v2

    invoke-virtual {v2}, Lgbis/gbandroid/activities/report/pricekeyboard/ReportPriceKeyboardView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 583
    iget-object v1, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v1}, Lgbis/gbandroid/activities/report/ReportPrices;->m(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 585
    iget-object v0, p0, Lgbis/gbandroid/activities/report/u;->a:Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-static {v0}, Lgbis/gbandroid/activities/report/ReportPrices;->s(Lgbis/gbandroid/activities/report/ReportPrices;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->invalidate()V

    .line 586
    return-void
.end method
