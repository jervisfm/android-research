.class final Lgbis/gbandroid/activities/search/MainScreen$a;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/search/MainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/views/MainButton;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/search/MainScreen;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/views/MainButton;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:D


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/search/MainScreen;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030048

    .line 535
    iput-object p1, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    .line 536
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 537
    iput-object p3, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->b:Ljava/util/List;

    .line 538
    iput v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->c:I

    .line 539
    invoke-static {p1}, Lgbis/gbandroid/activities/search/MainScreen;->d(Lgbis/gbandroid/activities/search/MainScreen;)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->d:D

    .line 540
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 545
    if-nez p2, :cond_2

    .line 546
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->e(Lgbis/gbandroid/activities/search/MainScreen;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 547
    new-instance v2, Lgbis/gbandroid/activities/search/MainScreen$d;

    invoke-direct {v2}, Lgbis/gbandroid/activities/search/MainScreen$d;-><init>()V

    .line 548
    const v0, 0x7f070101

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->a:Landroid/widget/TextView;

    .line 549
    const v0, 0x7f070100

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->b:Landroid/widget/ImageView;

    .line 550
    const v0, 0x7f070102

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->c:Landroid/widget/ImageView;

    .line 551
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->f(Lgbis/gbandroid/activities/search/MainScreen;)Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 552
    const v0, 0x7f02007b

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 553
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const-wide/high16 v3, 0x4047

    iget-wide v5, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->d:D

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v0, v1, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 554
    iget-object v0, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 555
    iget-object v1, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 556
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 557
    const/16 v3, 0xe

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 558
    const/4 v3, 0x1

    const v4, 0x7f070100

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 559
    const/16 v3, 0xf

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 561
    const-wide v3, 0x4041800000000000L

    iget-wide v5, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->d:D

    mul-double/2addr v3, v5

    double-to-int v0, v3

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 562
    const/16 v0, 0xe

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 563
    const/16 v0, 0xf

    const/4 v3, -0x1

    invoke-virtual {v1, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 564
    const/16 v0, 0x9

    const/4 v3, -0x1

    invoke-virtual {v1, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 565
    iget-object v0, v2, Lgbis/gbandroid/activities/search/MainScreen$d;->a:Landroid/widget/TextView;

    const/high16 v1, 0x41a0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 566
    const/4 v0, 0x0

    const-wide/high16 v3, 0x4014

    iget-wide v5, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->d:D

    mul-double/2addr v3, v5

    double-to-int v1, v3

    const/4 v3, 0x0

    const-wide/high16 v4, 0x4014

    iget-wide v6, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->d:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {p2, v0, v1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 568
    :cond_0
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 572
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    .line 573
    if-eqz v0, :cond_1

    .line 574
    iget-object v2, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 575
    iget-object v2, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 576
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->isBadgeDisplayed()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 577
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 578
    iget-object v2, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getBadge()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 581
    :goto_1
    iget-object v1, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->c:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 584
    :goto_2
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setId(I)V

    .line 585
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 586
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 588
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v1}, Lgbis/gbandroid/activities/search/MainScreen;->g(Lgbis/gbandroid/activities/search/MainScreen;)I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 589
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 590
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 599
    :cond_1
    :goto_3
    return-object p2

    .line 570
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/search/MainScreen$d;

    move-object v1, v0

    goto :goto_0

    .line 580
    :cond_3
    iget-object v2, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->c:Landroid/widget/ImageView;

    const v3, 0x7f020063

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 583
    :cond_4
    iget-object v1, v1, Lgbis/gbandroid/activities/search/MainScreen$d;->c:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 592
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->g(Lgbis/gbandroid/activities/search/MainScreen;)I

    move-result v0

    if-eqz v0, :cond_6

    .line 593
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$a;->a:Lgbis/gbandroid/activities/search/MainScreen;

    const v1, 0x7f040001

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 594
    invoke-virtual {p2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 596
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method
