.class final Lgbis/gbandroid/activities/search/MainScreen$b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/search/MainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lgbis/gbandroid/views/MainButton;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/search/MainScreen;

.field private b:[I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/search/MainScreen;[I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 627
    iput-object p1, p0, Lgbis/gbandroid/activities/search/MainScreen$b;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 628
    iput-object p2, p0, Lgbis/gbandroid/activities/search/MainScreen$b;->b:[I

    .line 629
    return-void
.end method

.method private a(I)I
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 639
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen$b;->b:[I

    array-length v2, v0

    move v0, v1

    .line 640
    :goto_0
    if-lt v0, v2, :cond_1

    move v0, v1

    .line 643
    :cond_0
    return v0

    .line 641
    :cond_1
    iget-object v3, p0, Lgbis/gbandroid/activities/search/MainScreen$b;->b:[I

    aget v3, v3, v0

    if-eq v3, p1, :cond_0

    .line 640
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lgbis/gbandroid/views/MainButton;Lgbis/gbandroid/views/MainButton;)I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 633
    invoke-virtual {p1}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen$b;->a(I)I

    move-result v0

    .line 634
    invoke-virtual {p2}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v1

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen$b;->a(I)I

    move-result v1

    .line 635
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/views/MainButton;

    check-cast p2, Lgbis/gbandroid/views/MainButton;

    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/search/MainScreen$b;->a(Lgbis/gbandroid/views/MainButton;Lgbis/gbandroid/views/MainButton;)I

    move-result v0

    return v0
.end method
