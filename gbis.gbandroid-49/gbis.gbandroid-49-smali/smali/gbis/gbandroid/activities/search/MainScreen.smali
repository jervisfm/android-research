.class public Lgbis/gbandroid/activities/search/MainScreen;
.super Lgbis/gbandroid/activities/base/GBActivitySearch;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/search/MainScreen$a;,
        Lgbis/gbandroid/activities/search/MainScreen$b;,
        Lgbis/gbandroid/activities/search/MainScreen$c;,
        Lgbis/gbandroid/activities/search/MainScreen$d;
    }
.end annotation


# static fields
.field public static final LIKE_THIS_APP_TIMES:I = 0x4


# instance fields
.field private b:I

.field private c:J

.field private d:Lgbis/gbandroid/activities/search/MainScreen$c;

.field private e:Lgbis/gbandroid/activities/search/MainScreen$a;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/views/MainButton;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lgbis/gbandroid/views/layouts/DnDGridView;

.field private h:I

.field private i:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

.field private j:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;-><init>()V

    .line 684
    new-instance v0, Lgbis/gbandroid/activities/search/a;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/search/a;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->i:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    .line 695
    new-instance v0, Lgbis/gbandroid/activities/search/d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/search/d;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->j:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    .line 50
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    return-object v0
.end method

.method private a(I)Lgbis/gbandroid/views/MainButton;
    .locals 4
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 148
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 153
    const/4 v0, 0x0

    :cond_0
    return-object v0

    .line 149
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    .line 150
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 148
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 118
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->autoComplete()V

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->c()V

    .line 121
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "initial_screen_date"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 122
    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "is_member"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 124
    if-nez v2, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide v2, 0x9a7ec800L

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->r()V

    .line 133
    :cond_1
    :goto_0
    const v0, 0x7f07003a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusableInTouchMode(Z)V

    .line 134
    return-void

    .line 128
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 129
    const-string v1, "is_member"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/search/MainScreen;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lgbis/gbandroid/activities/search/MainScreen;->h:I

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/views/layouts/DnDGridView;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(I)Lgbis/gbandroid/views/MainButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/MainScreen$a;->remove(Ljava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(I)Lgbis/gbandroid/views/MainButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/MainScreen$a;->remove(Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(I)Lgbis/gbandroid/views/MainButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/MainScreen$a;->remove(Ljava/lang/Object;)V

    .line 141
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 142
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->h()V

    .line 144
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(I)Lgbis/gbandroid/views/MainButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/MainScreen$a;->remove(Ljava/lang/Object;)V

    .line 158
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016c

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 159
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020023

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016b

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 161
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f02002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016e

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 163
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->h()V

    .line 167
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 327
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->g()V

    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/search/MainScreen;)F
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getDensity()F

    move-result v0

    return v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 195
    const v0, 0x7f070109

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/layouts/DnDGridView;

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    .line 196
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->i:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->setDropListener(Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;)V

    .line 197
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->j:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->setDragListener(Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;)V

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    .line 199
    new-instance v0, Lgbis/gbandroid/activities/search/MainScreen$a;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/search/MainScreen$a;-><init>(Lgbis/gbandroid/activities/search/MainScreen;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    .line 200
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    const v0, 0x7f07010a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->buttonNearMe:Landroid/view/View;

    .line 202
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->buttonNearMe:Landroid/view/View;

    const v1, 0x7f070101

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 203
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->buttonNearMe:Landroid/view/View;

    const v2, 0x7f070100

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f02001b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 204
    const v1, 0x7f090169

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    const/high16 v1, 0x4160

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 206
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->buttonNearMe:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 209
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f02002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016a

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 210
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v0, Lgbis/gbandroid/views/MainButton;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020022

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x7f09016d

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lgbis/gbandroid/views/MainButton;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    .line 212
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->h()V

    .line 214
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->resizeElements(Landroid/content/res/Configuration;)V

    .line 215
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/search/MainScreen;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 291
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v2, :cond_1

    .line 292
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/layouts/DnDGridView;->setNumColumns(I)V

    .line 302
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->setNumColumns(I)V

    goto :goto_0

    .line 297
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->setNumColumns(I)V

    .line 298
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/layouts/DnDGridView;->setStretchMode(I)V

    goto :goto_0
.end method

.method static synthetic f(Lgbis/gbandroid/activities/search/MainScreen;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method private f()[I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "buttonOrderLoggedOut"

    const-string v3, "1|2|3"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x2

    new-array v3, v2, [I

    .line 317
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    move v2, v1

    .line 318
    :goto_1
    if-lt v2, v4, :cond_1

    .line 324
    return-object v3

    .line 315
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "buttonOrderLoggedIn"

    const-string v3, "1|2|4|5|6|7"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x7c

    if-eq v5, v6, :cond_2

    .line 320
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v1

    .line 321
    add-int/lit8 v1, v1, 0x1

    .line 318
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method static synthetic g(Lgbis/gbandroid/activities/search/MainScreen;)I
    .locals 1
    .parameter

    .prologue
    .line 74
    iget v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->h:I

    return v0
.end method

.method private g()V
    .locals 7

    .prologue
    .line 328
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 329
    const-string v2, ""

    .line 331
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    const-string v0, "buttonOrderLoggedOut"

    move-object v1, v0

    .line 335
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/search/MainScreen$a;->getCount()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    .line 336
    const/4 v0, 0x0

    move-object v3, v2

    move v2, v0

    :goto_1
    if-lt v2, v5, :cond_1

    .line 338
    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/search/MainScreen$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-interface {v4, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 340
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 341
    return-void

    .line 334
    :cond_0
    const-string v0, "buttonOrderLoggedIn"

    move-object v1, v0

    goto :goto_0

    .line 337
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/search/MainScreen$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "|"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 336
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    .line 344
    new-instance v0, Lgbis/gbandroid/activities/search/MainScreen$b;

    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->f()[I

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/search/MainScreen$b;-><init>(Lgbis/gbandroid/activities/search/MainScreen;[I)V

    .line 345
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->e:Lgbis/gbandroid/activities/search/MainScreen$a;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/search/MainScreen$a;->sort(Ljava/util/Comparator;)V

    .line 346
    return-void
.end method

.method static synthetic h(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getPreferences()V

    return-void
.end method

.method private i()V
    .locals 6

    .prologue
    .line 349
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "like_app"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    .line 350
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "like_app_date"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->c:J

    .line 351
    iget-wide v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->c:J

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 352
    iget v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    .line 353
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 354
    const-string v1, "like_app"

    iget v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 355
    const-string v1, "like_app_date"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 356
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 357
    iget v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 358
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->n()V

    .line 361
    :cond_0
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 290
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->e()V

    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    .line 364
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "share_app"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 365
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "share_app_date"

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    .line 366
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    sub-long/2addr v3, v5

    cmp-long v1, v1, v3

    if-gez v1, :cond_2

    .line 367
    add-int/lit8 v0, v0, 0x1

    .line 368
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->b:I

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    .line 369
    const/4 v0, 0x6

    .line 370
    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 371
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->p()V

    .line 372
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 373
    const-string v2, "share_app"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 374
    const-string v0, "share_app_date"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 375
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 377
    :cond_2
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 1
    .parameter

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->setAutocompleteWithQuickLinks(Z)V

    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/search/MainScreen;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 380
    move v1, v2

    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 388
    :goto_1
    return-void

    .line 381
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    .line 382
    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 383
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "awards_badge"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/MainButton;->setBadgeDisplayed(Z)V

    .line 384
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->invalidateViews()V

    goto :goto_1

    .line 380
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 391
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 393
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/e;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/e;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 400
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/f;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/f;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 406
    const v1, 0x7f09011e

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 407
    const v1, 0x7f09012d

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 408
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 409
    const v0, 0x7f070032

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x41a0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 410
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 411
    return-void
.end method

.method static synthetic l(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 156
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->c()V

    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 414
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 416
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/g;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/g;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 423
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/h;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/h;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 429
    const v1, 0x7f09011f

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 430
    const v1, 0x7f09012e

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 431
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 432
    const v0, 0x7f070032

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x41a0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 433
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 434
    return-void
.end method

.method static synthetic m(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 136
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->b()V

    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 437
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 439
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/i;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/i;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 446
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/j;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/j;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 453
    const v1, 0x7f09011d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 454
    const v1, 0x7f09012b

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 455
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 456
    const v0, 0x7f070032

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x41a0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 457
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 458
    return-void
.end method

.method static synthetic n(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 498
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->q()V

    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 461
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 463
    const v1, 0x7f090198

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/k;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/k;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 469
    const v1, 0x7f090115

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 470
    const v1, 0x7f090096

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 471
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 473
    return-void
.end method

.method static synthetic o(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 526
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->u()V

    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 476
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 478
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/b;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/b;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 485
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/search/c;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/search/c;-><init>(Lgbis/gbandroid/activities/search/MainScreen;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 491
    const v1, 0x7f090128

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 492
    const v1, 0x7f09012c

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 493
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 494
    const v0, 0x7f070032

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v2, 0x41a0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 495
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 496
    return-void
.end method

.method static synthetic p(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 390
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->l()V

    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 500
    :try_start_0
    const-string v0, "market://details?id=gbis.gbandroid"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 501
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 502
    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 506
    :goto_0
    return-void

    .line 503
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic q(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 413
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->m()V

    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 509
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/initial/InitialScreen;

    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 510
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->startActivity(Landroid/content/Intent;)V

    .line 511
    return-void
.end method

.method static synthetic r(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->showShare()V

    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    .line 514
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/Profile;

    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 515
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->startActivityForResult(Landroid/content/Intent;I)V

    .line 516
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 522
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/prizes/Prize;

    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 523
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->startActivityForResult(Landroid/content/Intent;I)V

    .line 524
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    .line 527
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/FeedBack;

    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 528
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->startActivity(Landroid/content/Intent;)V

    .line 529
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onActivityResult(IILandroid/content/Intent;)V

    .line 245
    packed-switch p1, :pswitch_data_0

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 247
    :pswitch_0
    const/16 v0, 0x9

    if-ne p2, v0, :cond_0

    .line 248
    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 649
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->resetFocus()V

    .line 650
    const-string v0, ""

    .line 651
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 681
    :goto_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 682
    return-void

    .line 653
    :pswitch_0
    const v0, 0x7f090169

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 654
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->startNearMeThread()V

    goto :goto_0

    .line 657
    :pswitch_1
    const v0, 0x7f09016a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 658
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->launchSettings()V

    goto :goto_0

    .line 661
    :pswitch_2
    const v0, 0x7f09016d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 663
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->showLogin()V

    goto :goto_0

    .line 666
    :pswitch_3
    const v0, 0x7f09016c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 667
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->s()V

    goto :goto_0

    .line 670
    :pswitch_4
    const v0, 0x7f09016b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 671
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->showFavourites()V

    goto :goto_0

    .line 674
    :pswitch_5
    const v0, 0x7f09016e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 675
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->t()V

    goto :goto_0

    .line 651
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0700a6

    .line 256
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 257
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 258
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    .line 259
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->hasFocus()Z

    move-result v3

    .line 260
    if-eqz v3, :cond_0

    .line 261
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->resetFocus()V

    .line 262
    :cond_0
    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 270
    const v1, 0x7f030049

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->setContentView(I)V

    .line 271
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->d()V

    .line 272
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->populateButtons()V

    .line 273
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->a()V

    .line 274
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->setAutocompleteWithQuickLinks()V

    .line 275
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->e()V

    .line 276
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 277
    if-eqz v3, :cond_1

    .line 278
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 280
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 281
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 283
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->adBanner:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 285
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->adBanner:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 287
    :cond_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->k()V

    .line 288
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 79
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreate(Landroid/os/Bundle;)V

    .line 80
    new-instance v0, Lgbis/gbandroid/activities/search/MainScreen$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/search/MainScreen$c;-><init>(Lgbis/gbandroid/activities/search/MainScreen;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->d:Lgbis/gbandroid/activities/search/MainScreen$c;

    .line 81
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getPreferences()V

    .line 82
    const v0, 0x7f030049

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->setContentView(I)V

    .line 83
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->d()V

    .line 84
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->populateButtons()V

    .line 85
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->checkConnection()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->o()V

    .line 92
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->a()V

    .line 89
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->i()V

    .line 90
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->j()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 219
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 220
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 221
    const v1, 0x7f0b0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 222
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onDestroy()V

    .line 113
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->d:Lgbis/gbandroid/activities/search/MainScreen$c;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .parameter

    .prologue
    .line 709
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->g:Lgbis/gbandroid/views/layouts/DnDGridView;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/layouts/DnDGridView;->startDrag(Landroid/view/View;)V

    .line 710
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 227
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 228
    const-string v0, "Click"

    const-string v1, "MenuButton"

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/activities/search/MainScreen;->setAnalyticsTrackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 229
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_0
    return v4

    .line 231
    :pswitch_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->showShare()V

    goto :goto_0

    .line 229
    :pswitch_data_0
    .packed-switch 0x7f070191
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onResume()V

    .line 97
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->screenPreference:Ljava/lang/String;

    const-string v1, "home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getPreferences()V

    .line 99
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->e()V

    .line 100
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/MainScreen;->k()V

    .line 101
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x1

    return v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onStart()V

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->d:Lgbis/gbandroid/activities/search/MainScreen$c;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 107
    return-void
.end method

.method protected populateButtons()V
    .locals 4

    .prologue
    .line 182
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->populateButtons()V

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const v1, 0x7f0900ab

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 185
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 186
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/MainScreen;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 187
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 188
    const/16 v1, 0x1e0

    if-ge v0, v1, :cond_0

    .line 189
    const v1, 0x7f07010c

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    add-int/lit8 v0, v0, -0x78

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->searchCityZip:Landroid/widget/AutoCompleteTextView;

    const v1, 0x7f0901d1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public resizeElements(Landroid/content/res/Configuration;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f07003a

    const/4 v3, -0x2

    .line 170
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 171
    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 172
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f020051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 173
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 174
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 175
    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 178
    :cond_0
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 5

    .prologue
    .line 715
    iget-object v0, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adMain"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 716
    iget-object v1, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adMainKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 717
    iget-object v2, p0, Lgbis/gbandroid/activities/search/MainScreen;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adMainUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 718
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/search/MainScreen;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 719
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 723
    const v0, 0x7f09022d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/MainScreen;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
