.class final Lgbis/gbandroid/activities/search/SearchBar$a;
.super Landroid/widget/RelativeLayout;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/search/SearchBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/search/SearchBar;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/search/SearchBar;Landroid/content/Context;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lgbis/gbandroid/activities/search/SearchBar$a;->a:Lgbis/gbandroid/activities/search/SearchBar;

    .line 92
    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 93
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030051

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 94
    return-void
.end method


# virtual methods
.method protected final onSizeChanged(IIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 99
    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar$a;->a:Lgbis/gbandroid/activities/search/SearchBar;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/SearchBar;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 100
    if-eqz p4, :cond_0

    if-ge p4, p2, :cond_0

    sub-int v1, p2, p4

    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 101
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar$a;->a:Lgbis/gbandroid/activities/search/SearchBar;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/SearchBar;->a(Lgbis/gbandroid/activities/search/SearchBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar$a;->a:Lgbis/gbandroid/activities/search/SearchBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/search/SearchBar;->setResult(I)V

    .line 103
    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar$a;->a:Lgbis/gbandroid/activities/search/SearchBar;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/search/SearchBar;->finish()V

    .line 106
    :cond_0
    return-void
.end method
