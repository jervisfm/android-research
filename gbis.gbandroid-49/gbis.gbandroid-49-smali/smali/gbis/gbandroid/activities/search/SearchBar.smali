.class public Lgbis/gbandroid/activities/search/SearchBar;
.super Lgbis/gbandroid/activities/base/GBActivitySearch;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/search/SearchBar$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 42
    const v0, 0x7f070106

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar;->buttonSearch:Landroid/widget/Button;

    .line 43
    const v0, 0x7f070148

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    new-instance v1, Lgbis/gbandroid/activities/search/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/search/l;-><init>(Lgbis/gbandroid/activities/search/SearchBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/search/SearchBar;)Z
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-boolean v0, p0, Lgbis/gbandroid/activities/search/SearchBar;->activityLoading:Z

    return v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->setResult(I)V

    .line 74
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->finish()V

    .line 75
    const/4 v0, 0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 25
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 27
    const v1, 0x10a0001

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 28
    const/high16 v1, 0x3f00

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 29
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/high16 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 30
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 31
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 32
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 33
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 34
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 35
    new-instance v0, Lgbis/gbandroid/activities/search/SearchBar$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/search/SearchBar$a;-><init>(Lgbis/gbandroid/activities/search/SearchBar;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->setContentView(Landroid/view/View;)V

    .line 36
    invoke-direct {p0}, Lgbis/gbandroid/activities/search/SearchBar;->a()V

    .line 37
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->populateButtons()V

    .line 38
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->autoComplete()V

    .line 39
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 60
    packed-switch p1, :pswitch_data_0

    .line 66
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 62
    :pswitch_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->setResult(I)V

    .line 63
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->finish()V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->setResult(I)V

    .line 54
    invoke-virtual {p0}, Lgbis/gbandroid/activities/search/SearchBar;->finish()V

    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const v0, 0x7f09022e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showMessage()V
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar;->userMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar;->userMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    new-instance v1, Lgbis/gbandroid/views/CustomToast;

    iget-object v0, p0, Lgbis/gbandroid/activities/search/SearchBar;->userMessage:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v2}, Lgbis/gbandroid/views/CustomToast;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 83
    const/16 v2, 0x31

    const/4 v3, 0x0

    .line 84
    const v0, 0x7f070149

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 83
    invoke-virtual {v1, v2, v3, v0}, Lgbis/gbandroid/views/CustomToast;->setGravity(III)V

    .line 85
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomToast;->show()V

    .line 86
    const-string v0, ""

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/search/SearchBar;->setMessage(Ljava/lang/String;)V

    .line 88
    :cond_0
    return-void
.end method
