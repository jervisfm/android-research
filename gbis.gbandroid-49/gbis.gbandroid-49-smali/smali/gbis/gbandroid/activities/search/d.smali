.class final Lgbis/gbandroid/activities/search/d;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/search/MainScreen;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/search/MainScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    .line 695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final drag(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 698
    iget-object v0, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/search/MainScreen$a;->getCount()I

    move-result v0

    if-ge p2, v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/activities/search/MainScreen$a;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p2, :cond_0

    if-ltz p1, :cond_0

    .line 699
    iget-object v0, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v0}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgbis/gbandroid/activities/search/MainScreen$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/MainButton;

    .line 700
    iget-object v1, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/search/MainScreen$a;->remove(Ljava/lang/Object;)V

    .line 701
    iget-object v1, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-static {v1}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;)Lgbis/gbandroid/activities/search/MainScreen$a;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lgbis/gbandroid/activities/search/MainScreen$a;->insert(Ljava/lang/Object;I)V

    .line 702
    iget-object v1, p0, Lgbis/gbandroid/activities/search/d;->a:Lgbis/gbandroid/activities/search/MainScreen;

    invoke-virtual {v0}, Lgbis/gbandroid/views/MainButton;->getButtonId()I

    move-result v0

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/search/MainScreen;->a(Lgbis/gbandroid/activities/search/MainScreen;I)V

    .line 704
    :cond_0
    return-void
.end method
