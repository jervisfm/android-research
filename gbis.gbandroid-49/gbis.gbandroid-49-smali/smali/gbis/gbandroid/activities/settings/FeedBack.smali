.class public Lgbis/gbandroid/activities/settings/FeedBack;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/settings/FeedBack$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/FeedBack;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f070041

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    .line 46
    const v0, 0x7f070042

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->c:Landroid/widget/EditText;

    .line 47
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    const v0, 0x7f070040

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09013a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    new-instance v1, Lgbis/gbandroid/activities/settings/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/settings/a;-><init>(Lgbis/gbandroid/activities/settings/FeedBack;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 61
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 63
    :cond_0
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/FeedBack;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/activities/settings/FeedBack;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 66
    new-instance v0, Lgbis/gbandroid/activities/settings/FeedBack$a;

    invoke-direct {v0, p0, p0, p1, p2}, Lgbis/gbandroid/activities/settings/FeedBack$a;-><init>(Lgbis/gbandroid/activities/settings/FeedBack;Lgbis/gbandroid/activities/base/GBActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/FeedBack$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 68
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 69
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    .line 79
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 72
    new-instance v0, Lgbis/gbandroid/activities/settings/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/settings/b;-><init>(Lgbis/gbandroid/activities/settings/FeedBack;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 73
    new-instance v1, Lgbis/gbandroid/queries/FeedBackQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/FeedBack;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/FeedBack;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FeedBackQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 74
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lgbis/gbandroid/queries/FeedBackQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 75
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 108
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/FeedBack;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/VerifyFields;->checkEmail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    const v0, 0x7f0900a1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->showMessage(Ljava/lang/String;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    .line 113
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/FeedBack;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/FeedBack;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 115
    const-string v0, ""

    .line 116
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/FeedBack;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/settings/FeedBack;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_3
    const v0, 0x7f09006a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/FeedBack;->b()V

    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/FeedBack;->a()V

    .line 36
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const v0, 0x7f090235

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f030012

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/settings/FeedBack;->setGBViewInContentLayout(IZ)V

    .line 40
    const v0, 0x7f090114

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/FeedBack;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 41
    const v0, 0x7f09017a

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/settings/FeedBack;->setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V

    .line 42
    return-void
.end method
