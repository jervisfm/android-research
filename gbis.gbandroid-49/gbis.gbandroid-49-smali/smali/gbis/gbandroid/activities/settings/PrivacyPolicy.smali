.class public Lgbis/gbandroid/activities/settings/PrivacyPolicy;
.super Lgbis/gbandroid/activities/base/GBWebViewScreen;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBWebViewScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    const v0, 0x7f09020c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f090232

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x7f030024

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/settings/PrivacyPolicy;->setGBViewInContentLayout(IZ)V

    .line 10
    const v0, 0x7f090109

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/PrivacyPolicy;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/PrivacyPolicy;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 11
    return-void
.end method
