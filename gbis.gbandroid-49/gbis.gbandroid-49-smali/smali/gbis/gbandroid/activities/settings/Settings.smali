.class public Lgbis/gbandroid/activities/settings/Settings;
.super Lgbis/gbandroid/activities/base/GBPreferenceActivity;
.source "GBFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/settings/Settings$a;
    }
.end annotation


# static fields
.field public static final AWARDS_BADGE:Ljava/lang/String; = "awards_badge"

.field public static final AWARDS_CACHED_IMAGES_TIME:Ljava/lang/String; = "awards_cached_images_time"

.field public static final SOCIAL_MEDIA_TOKEN_EXISTS_FACEBOOK:Ljava/lang/String; = "social_media_token_exists_facebook"

.field public static final SOCIAL_MEDIA_TOKEN_EXISTS_TWITTER:Ljava/lang/String; = "social_media_token_exists_twitter"


# instance fields
.field private a:Z

.field private b:Landroid/app/Dialog;

.field private c:Ljava/lang/String;

.field private d:Landroid/preference/Preference;

.field private e:Landroid/preference/CheckBoxPreference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/Preference;

.field private i:Landroid/preference/Preference;

.field private j:Landroid/preference/Preference;

.field private k:Landroid/preference/Preference;

.field private l:Landroid/preference/Preference;

.field private m:Landroid/preference/Preference;

.field private n:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 116
    const-string v0, "loginButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    .line 117
    const-string v0, "noPricesPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->e:Landroid/preference/CheckBoxPreference;

    .line 120
    const-string v0, "fuelPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->f:Landroid/preference/Preference;

    .line 121
    const-string v0, "distancePreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->g:Landroid/preference/Preference;

    .line 124
    const-string v0, "screenPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->h:Landroid/preference/Preference;

    .line 125
    const-string v0, "aboutButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->i:Landroid/preference/Preference;

    .line 126
    const-string v0, "supportButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->j:Landroid/preference/Preference;

    .line 127
    const-string v0, "helpButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->k:Landroid/preference/Preference;

    .line 128
    const-string v0, "shareButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->l:Landroid/preference/Preference;

    .line 129
    const-string v0, "rateButton"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->m:Landroid/preference/Preference;

    .line 130
    const-string v0, "smartPromptPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->n:Landroid/preference/CheckBoxPreference;

    .line 131
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/Settings;)V
    .locals 0
    .parameter

    .prologue
    .line 489
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->r()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->f:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 135
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->g:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 150
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 151
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->h:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 152
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->i:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 153
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->j:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 154
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->k:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 155
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->l:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 156
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->m:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 158
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->e:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 159
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->n:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 160
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/settings/Settings;)V
    .locals 0
    .parameter

    .prologue
    .line 217
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->g()V

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/settings/Settings;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method private c()V
    .locals 0

    .prologue
    .line 177
    invoke-static {p0}, Lgbis/gbandroid/activities/members/MemberLogin;->logout(Landroid/content/Context;)V

    .line 178
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    const v1, 0x7f0900dd

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    const v1, 0x7f0900dc

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/settings/Settings;->a:Z

    .line 184
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/settings/Settings;)V
    .locals 0
    .parameter

    .prologue
    .line 227
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->i()V

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/settings/Settings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->e:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v2, 0x7f0900df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/Settings;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    const v1, 0x7f0900de

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 189
    iput-boolean v4, p0, Lgbis/gbandroid/activities/settings/Settings;->a:Z

    .line 190
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/settings/Settings;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->n:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 193
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/members/MemberLogin;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 194
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V

    .line 195
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 218
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/FeedBack;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 219
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V

    .line 220
    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/settings/Settings;)V
    .locals 0
    .parameter

    .prologue
    .line 484
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->q()V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 223
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/ShareApp;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 224
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V

    .line 225
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 228
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/PrivacyPolicy;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 229
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V

    .line 230
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 234
    const-string v0, "GbAppCategory"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 235
    const-string v1, "customKeyboardPreference"

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 236
    return-void
.end method

.method private k()V
    .locals 7

    .prologue
    .line 239
    new-instance v3, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v3, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 240
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030010

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 241
    const v0, 0x7f07003d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 242
    const v1, 0x7f07003c

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 243
    const v2, 0x7f07003b

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 244
    new-instance v5, Lgbis/gbandroid/activities/settings/c;

    invoke-direct {v5, p0}, Lgbis/gbandroid/activities/settings/c;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    new-instance v0, Lgbis/gbandroid/activities/settings/e;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/settings/e;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    const v0, 0x7f09012f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0}, Lgbis/gbandroid/activities/base/GBActivity;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    const v0, 0x7f09010a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 260
    invoke-virtual {v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 261
    const v0, 0x7f090198

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/settings/f;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/settings/f;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v3, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 266
    invoke-virtual {v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    .line 267
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 268
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 271
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 272
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/settings/g;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/settings/g;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 279
    const v1, 0x7f090198

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/settings/h;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/settings/h;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 286
    const v1, 0x7f090123

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 287
    const v1, 0x7f090097

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 288
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    .line 289
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/settings/i;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/settings/i;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 296
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 299
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/settings/HelpScreen;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 300
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V

    .line 301
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 305
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "like_app"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ge v0, v3, :cond_0

    .line 306
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 307
    const-string v1, "like_app"

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 308
    const-string v1, "like_app_date"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 309
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 311
    :cond_0
    const-string v0, "market://details?id=gbis.gbandroid"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 312
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 313
    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 320
    new-instance v0, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 321
    const v1, 0x7f09019b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/settings/j;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/settings/j;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 328
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgbis/gbandroid/activities/settings/k;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/settings/k;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 336
    const v1, 0x7f090124

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 337
    const v1, 0x7f090098

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 338
    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    .line 339
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/settings/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/settings/l;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 345
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 346
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->c:Ljava/lang/String;

    .line 391
    return-void
.end method

.method private q()V
    .locals 2

    .prologue
    .line 485
    new-instance v0, Lgbis/gbandroid/activities/settings/Settings$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/settings/Settings$a;-><init>(Lgbis/gbandroid/activities/settings/Settings;Lgbis/gbandroid/activities/base/GBPreferenceActivity;)V

    .line 486
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/Settings$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 487
    return-void
.end method

.method private r()V
    .locals 4

    .prologue
    .line 490
    new-instance v0, Lgbis/gbandroid/activities/settings/d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/settings/d;-><init>(Lgbis/gbandroid/activities/settings/Settings;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/d;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 491
    new-instance v1, Lgbis/gbandroid/queries/MemberPreferenceQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/Settings;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/MemberPreferenceQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 492
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->s()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->getResponseObject(Ljava/util/List;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 493
    return-void
.end method

.method private s()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 497
    new-instance v1, Lgbis/gbandroid/entities/requests/RequestMemberPreference;

    invoke-direct {v1}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;-><init>()V

    .line 498
    const-string v2, "SmartPrompt"

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->setName(Ljava/lang/String;)V

    .line 499
    iget-object v2, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "smartPromptPreference"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->setValue(Z)V

    .line 500
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->addPreferencesFromResource(I)V

    .line 75
    iput-boolean v2, p0, Lgbis/gbandroid/activities/settings/Settings;->a:Z

    .line 76
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->a()V

    .line 77
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->p()V

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->e()V

    .line 80
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->b()V

    .line 81
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/Settings;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Dialog;

    .line 82
    if-eqz v0, :cond_1

    .line 83
    iput-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    .line 84
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 88
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "largeScreenBoolean"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->j()V

    .line 92
    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->onDestroy()V

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 103
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 460
    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/Settings;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 461
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->e:Landroid/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_1

    .line 462
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "noPricesPreference"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->l()V

    .line 476
    :cond_0
    :goto_0
    return v0

    .line 467
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->n:Landroid/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_3

    .line 468
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "smartPromptPreference"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 469
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->o()V

    goto :goto_0

    .line 472
    :cond_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->q()V

    goto :goto_0

    .line 476
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 406
    invoke-virtual {p1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 407
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->f:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 408
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const/high16 v1, 0x7f06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v3, 0x7f060001

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 410
    const-string v3, "fuelPreference"

    const v4, 0x7f09010e

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v3, v4}, Lgbis/gbandroid/activities/settings/Settings;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 455
    :goto_0
    return v0

    .line 412
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->g:Landroid/preference/Preference;

    if-ne p1, v0, :cond_1

    .line 413
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 414
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v3, 0x7f060003

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 415
    const-string v3, "distancePreference"

    const v4, 0x7f09010f

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v3, v4}, Lgbis/gbandroid/activities/settings/Settings;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 416
    goto :goto_0

    .line 417
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->d:Landroid/preference/Preference;

    if-ne p1, v0, :cond_3

    .line 418
    iget-boolean v0, p0, Lgbis/gbandroid/activities/settings/Settings;->a:Z

    if-eqz v0, :cond_2

    .line 420
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->f()V

    :goto_1
    move v0, v2

    .line 423
    goto :goto_0

    .line 422
    :cond_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->c()V

    goto :goto_1

    .line 424
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->h:Landroid/preference/Preference;

    if-ne p1, v0, :cond_5

    .line 425
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 426
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->res:Landroid/content/res/Resources;

    const v4, 0x7f060009

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 427
    iget-object v4, p0, Lgbis/gbandroid/activities/settings/Settings;->c:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 428
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 429
    const v0, 0x7f0900e6

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 430
    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 431
    new-array v0, v3, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 433
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 434
    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 435
    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 437
    :cond_4
    const-string v3, "screenPreference"

    const v4, 0x7f090111

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v1, v3, v4}, Lgbis/gbandroid/activities/settings/Settings;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 438
    goto/16 :goto_0

    .line 439
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->i:Landroid/preference/Preference;

    if-ne p1, v0, :cond_6

    .line 440
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->k()V

    move v0, v2

    .line 441
    goto/16 :goto_0

    .line 442
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->j:Landroid/preference/Preference;

    if-ne p1, v0, :cond_7

    .line 444
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->g()V

    move v0, v2

    .line 445
    goto/16 :goto_0

    .line 446
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->k:Landroid/preference/Preference;

    if-ne p1, v0, :cond_8

    .line 448
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->m()V

    move v0, v2

    .line 449
    goto/16 :goto_0

    .line 450
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->l:Landroid/preference/Preference;

    if-ne p1, v0, :cond_a

    .line 451
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->h()V

    :cond_9
    :goto_2
    move v0, v3

    .line 455
    goto/16 :goto_0

    .line 452
    :cond_a
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->m:Landroid/preference/Preference;

    if-ne p1, v0, :cond_9

    .line 453
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->n()V

    goto :goto_2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->onResume()V

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 97
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    .line 110
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/Settings;->b:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 112
    :cond_0
    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 395
    const-string v0, "member_id"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->p()V

    .line 397
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/Settings;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 398
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->e()V

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/Settings;->d()V

    goto :goto_0
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 481
    const v0, 0x7f09022f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/Settings;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
