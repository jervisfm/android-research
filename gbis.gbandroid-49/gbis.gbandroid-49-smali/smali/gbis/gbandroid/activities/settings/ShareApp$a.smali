.class final Lgbis/gbandroid/activities/settings/ShareApp$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/settings/ShareApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/settings/ShareApp;

.field private b:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/settings/ShareApp;Lgbis/gbandroid/activities/base/GBActivity;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    .line 115
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 116
    iput p3, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->b:I

    .line 117
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 121
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 123
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/ShareApp;->a(Lgbis/gbandroid/activities/settings/ShareApp;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    new-instance v1, Lgbis/gbandroid/entities/ShareMessage;

    invoke-direct {v1}, Lgbis/gbandroid/entities/ShareMessage;-><init>()V

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/settings/ShareApp;->a(Lgbis/gbandroid/activities/settings/ShareApp;Lgbis/gbandroid/entities/ShareMessage;)V

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/ShareApp;->b(Lgbis/gbandroid/activities/settings/ShareApp;)Lgbis/gbandroid/entities/ShareMessage;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    const v2, 0x7f0900cc

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/settings/ShareApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ShareMessage;->setSubject(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/ShareApp;->b(Lgbis/gbandroid/activities/settings/ShareApp;)Lgbis/gbandroid/entities/ShareMessage;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    const v2, 0x7f0900cd

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/settings/ShareApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/ShareMessage;->setMessage(Ljava/lang/String;)V

    .line 130
    :cond_0
    iget v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->b:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :goto_1
    return-void

    .line 132
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/ShareApp;->c(Lgbis/gbandroid/activities/settings/ShareApp;)V

    goto :goto_1

    .line 135
    :pswitch_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/ShareApp;->d(Lgbis/gbandroid/activities/settings/ShareApp;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->a:Lgbis/gbandroid/activities/settings/ShareApp;

    iget v1, p0, Lgbis/gbandroid/activities/settings/ShareApp$a;->b:I

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/settings/ShareApp;->a(Lgbis/gbandroid/activities/settings/ShareApp;I)V

    .line 142
    const/4 v0, 0x1

    return v0
.end method
