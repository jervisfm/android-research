.class public Lgbis/gbandroid/activities/settings/ShareApp;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/settings/ShareApp$a;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/entities/ShareMessage;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/ShareApp;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 44
    const v0, 0x7f07008f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->b:Landroid/widget/Button;

    .line 45
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/ShareApp;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->b:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 49
    :goto_0
    const v0, 0x7f070090

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->c:Landroid/widget/Button;

    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 4
    .parameter

    .prologue
    .line 63
    new-instance v0, Lgbis/gbandroid/activities/settings/m;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/settings/m;-><init>(Lgbis/gbandroid/activities/settings/ShareApp;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/m;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 64
    new-instance v1, Lgbis/gbandroid/queries/ShareAppQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/ShareApp;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/ShareApp;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/ShareAppQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 65
    invoke-virtual {v1, p1}, Lgbis/gbandroid/queries/ShareAppQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 66
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ShareMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    .line 67
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/ShareApp;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/settings/ShareApp;->a(I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/ShareApp;Lgbis/gbandroid/entities/ShareMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/settings/ShareApp;)Lgbis/gbandroid/entities/ShareMessage;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    return-object v0
.end method

.method private b(I)V
    .locals 2
    .parameter

    .prologue
    .line 70
    new-instance v0, Lgbis/gbandroid/activities/settings/ShareApp$a;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/settings/ShareApp$a;-><init>(Lgbis/gbandroid/activities/settings/ShareApp;Lgbis/gbandroid/activities/base/GBActivity;I)V

    .line 71
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/ShareApp$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 72
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/ShareApp;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 73
    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 54
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 55
    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    .line 57
    :cond_0
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    .line 58
    const/4 v0, 0x1

    goto :goto_0

    .line 59
    :cond_1
    invoke-static {p0}, Lgbis/gbandroid/utils/FeaturesUtils;->isSMSAvailable(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    const-string v1, "vnd.android-dir/mms-sms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v1, "sms_body"

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ShareMessage;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->startActivity(Landroid/content/Intent;)V

    .line 80
    return-void
.end method

.method private c(I)V
    .locals 2
    .parameter

    .prologue
    .line 107
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    sget-object v1, Lgbis/gbandroid/activities/settings/SocialMedia;->SOCIAL_NETOWRK:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 109
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/settings/ShareApp;->startActivityForResult(Landroid/content/Intent;I)V

    .line 110
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/settings/ShareApp;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/ShareApp;->c()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    const-string v1, "plain/text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v1, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ShareMessage;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/ShareApp;->a:Lgbis/gbandroid/entities/ShareMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ShareMessage;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string v1, "Send mail..."

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->startActivity(Landroid/content/Intent;)V

    .line 88
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/settings/ShareApp;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/ShareApp;->d()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 149
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/settings/ShareApp;->sendSMS(Landroid/view/View;)V

    .line 153
    :cond_0
    :goto_0
    check-cast p1, Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    .line 154
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/ShareApp;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 151
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/settings/ShareApp;->sendEmail(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/ShareApp;->a()V

    .line 36
    return-void
.end method

.method public sendEmail(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 95
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->b(I)V

    .line 96
    return-void
.end method

.method public sendSMS(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->b(I)V

    .line 92
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    const v0, 0x7f090236

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f030020

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/settings/ShareApp;->setGBViewInContentLayout(IZ)V

    .line 40
    const v0, 0x7f090128

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public shareFacebook(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 99
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->c(I)V

    .line 100
    return-void
.end method

.method public shareTwitter(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 103
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/settings/ShareApp;->c(I)V

    .line 104
    return-void
.end method
