.class final Lgbis/gbandroid/activities/settings/SocialMedia$a;
.super Landroid/webkit/WebViewClient;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/settings/SocialMedia;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/settings/SocialMedia;


# direct methods
.method private constructor <init>(Lgbis/gbandroid/activities/settings/SocialMedia;)V
    .locals 0
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgbis/gbandroid/activities/settings/SocialMedia;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/settings/SocialMedia$a;-><init>(Lgbis/gbandroid/activities/settings/SocialMedia;)V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 138
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 140
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->a(Lgbis/gbandroid/activities/settings/SocialMedia;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    const v1, 0x7f09020f

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->b(Lgbis/gbandroid/activities/settings/SocialMedia;)V

    .line 144
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    .line 148
    :cond_0
    :goto_1
    return-void

    .line 146
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    const v1, 0x7f090210

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 152
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 153
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/SocialMedia$a;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    const v2, 0x7f09008d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->a(Lgbis/gbandroid/activities/settings/SocialMedia;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 132
    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 133
    const/4 v0, 0x1

    return v0
.end method
