.class final Lgbis/gbandroid/activities/settings/SocialMedia$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/settings/SocialMedia;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/settings/SocialMedia;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/settings/SocialMedia;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 158
    iput-object p1, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    .line 159
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 160
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 164
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 166
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->a(Lgbis/gbandroid/activities/settings/SocialMedia;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->c(Lgbis/gbandroid/activities/settings/SocialMedia;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 169
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    const v2, 0x7f0900ce

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->showMessage(Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    .line 173
    :goto_1
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->d(Lgbis/gbandroid/activities/settings/SocialMedia;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia$b;->a:Lgbis/gbandroid/activities/settings/SocialMedia;

    invoke-static {v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->e(Lgbis/gbandroid/activities/settings/SocialMedia;)V

    .line 178
    const/4 v0, 0x1

    return v0
.end method
