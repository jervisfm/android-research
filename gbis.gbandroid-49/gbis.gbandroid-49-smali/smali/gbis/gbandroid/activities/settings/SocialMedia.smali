.class public Lgbis/gbandroid/activities/settings/SocialMedia;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/settings/SocialMedia$a;,
        Lgbis/gbandroid/activities/settings/SocialMedia$b;
    }
.end annotation


# static fields
.field public static final FACEBOOK:I = 0x5

.field public static SOCIAL_NETOWRK:Ljava/lang/String; = null

.field public static final TWITTER:I = 0x4

.field public static final TYPE_EMAIL:I = 0x2

.field public static final TYPE_SMS:I = 0x1


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "socialNetwork"

    sput-object v0, Lgbis/gbandroid/activities/settings/SocialMedia;->SOCIAL_NETOWRK:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/SocialMedia;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 60
    const v0, 0x7f070096

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    .line 61
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 62
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    new-instance v1, Lgbis/gbandroid/activities/settings/SocialMedia$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lgbis/gbandroid/activities/settings/SocialMedia$a;-><init>(Lgbis/gbandroid/activities/settings/SocialMedia;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 63
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 64
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/settings/SocialMedia;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/settings/SocialMedia;->loadDialog(Ljava/lang/String;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 67
    new-instance v0, Lgbis/gbandroid/queries/SocialMediaQuery;

    iget-object v1, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2, v3}, Lgbis/gbandroid/queries/SocialMediaQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    iget v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->c:I

    invoke-virtual {v0, v2}, Lgbis/gbandroid/queries/SocialMediaQuery;->formLoginURL(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/settings/SocialMedia;)V
    .locals 0
    .parameter

    .prologue
    .line 123
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->e()V

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/settings/SocialMedia;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->e()V

    .line 107
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->b()V

    goto :goto_0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/settings/SocialMedia;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->a()V

    return-void
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 110
    iget v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->c:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 112
    :pswitch_0
    iget-object v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "social_media_token_exists_twitter"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 114
    goto :goto_0

    .line 116
    :pswitch_1
    iget-object v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "social_media_token_exists_facebook"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 118
    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lgbis/gbandroid/activities/settings/SocialMedia$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/settings/SocialMedia$b;-><init>(Lgbis/gbandroid/activities/settings/SocialMedia;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 125
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 126
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 127
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/settings/SocialMedia;)V
    .locals 0
    .parameter

    .prologue
    .line 182
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->f()V

    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 183
    new-instance v0, Lgbis/gbandroid/activities/settings/n;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/settings/n;-><init>(Lgbis/gbandroid/activities/settings/SocialMedia;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/settings/n;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 184
    new-instance v0, Lgbis/gbandroid/queries/SocialMediaActionQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v2, v1, v3}, Lgbis/gbandroid/queries/SocialMediaActionQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 185
    iget v1, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->b:I

    iget v2, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->c:I

    iget v3, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->d:I

    iget-object v4, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->e:Ljava/lang/String;

    iget-object v5, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/queries/SocialMediaActionQuery;->getResponseObject(IIILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 186
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onActivityResult(IILandroid/content/Intent;)V

    .line 74
    packed-switch p1, :pswitch_data_0

    .line 82
    :goto_0
    return-void

    .line 76
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 77
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->c()V

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 41
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    sget-object v1, Lgbis/gbandroid/activities/settings/SocialMedia;->SOCIAL_NETOWRK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->c:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->b:I

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->d:I

    .line 47
    const-string v0, "This is the test message"

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->e:Ljava/lang/String;

    .line 48
    const-string v0, "This is the user message"

    iput-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->f:Ljava/lang/String;

    .line 49
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->a()V

    .line 50
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const v0, 0x7f0900cf

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->showMessage(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->showLogin()V

    .line 57
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->c()V

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 86
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 87
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/activities/settings/SocialMedia;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 91
    :goto_0
    const/4 v0, 0x1

    .line 93
    :goto_1
    return v0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/settings/SocialMedia;->finish()V

    goto :goto_0

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    const-string v0, ""

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 98
    const v0, 0x7f030024

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/settings/SocialMedia;->setGBViewInContentLayout(IZ)V

    .line 99
    const v0, 0x7f090128

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/settings/SocialMedia;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 100
    return-void
.end method
