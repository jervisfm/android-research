.class final Lgbis/gbandroid/activities/station/AddStationMap$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Z


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 778
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 779
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 780
    iput-boolean p3, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->b:Z

    .line 781
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .parameter

    .prologue
    .line 785
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 787
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->g(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 789
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->n(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 791
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMatchStationDialog()V

    .line 795
    :cond_0
    :goto_1
    return-void

    .line 793
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->finish()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 799
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-boolean v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$a;->b:Z

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Lgbis/gbandroid/activities/station/AddStationMap;Z)V

    .line 800
    const/4 v0, 0x1

    return v0
.end method
