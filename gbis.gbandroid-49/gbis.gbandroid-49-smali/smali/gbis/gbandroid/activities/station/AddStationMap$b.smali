.class final Lgbis/gbandroid/activities/station/AddStationMap$b;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Landroid/location/Address;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 806
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 807
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 808
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 812
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 814
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->g(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 816
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap$b;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 818
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->b:Landroid/location/Address;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V

    .line 819
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->b:Landroid/location/Address;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V

    .line 820
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->b:Landroid/location/Address;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V

    .line 821
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->b:Landroid/location/Address;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->d(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->d(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V

    .line 824
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const-wide v8, 0x412e848000000000L

    const/4 v7, 0x0

    .line 828
    new-instance v0, Landroid/location/Geocoder;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 830
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->o(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/views/StationsMapView;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    int-to-double v1, v1

    div-double/2addr v1, v8

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v3}, Lgbis/gbandroid/activities/station/AddStationMap;->o(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/views/StationsMapView;

    move-result-object v3

    invoke-virtual {v3}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v8

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 831
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 832
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->b:Landroid/location/Address;

    move v0, v6

    .line 840
    :goto_0
    return v0

    .line 835
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    const v2, 0x7f090064

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->setMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    .line 836
    goto :goto_0

    .line 839
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$b;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    const v2, 0x7f090065

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->setMessage(Ljava/lang/String;)V

    move v0, v7

    .line 840
    goto :goto_0
.end method
