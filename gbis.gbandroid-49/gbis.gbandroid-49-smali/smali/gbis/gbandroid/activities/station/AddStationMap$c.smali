.class final Lgbis/gbandroid/activities/station/AddStationMap$c;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutoCompMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 706
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 707
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;Z)V

    .line 708
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 7
    .parameter

    .prologue
    .line 712
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 713
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->b:Ljava/util/List;

    .line 715
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 716
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 717
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v4, :cond_1

    .line 723
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030052

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 724
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->e(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 726
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->e(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 729
    :cond_0
    :goto_1
    return-void

    .line 718
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/AutoCompMessage;

    .line 719
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutoCompMessage;->getCity()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AutoCompMessage;->getState()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 720
    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 721
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 717
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->f(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$c;->b:Ljava/util/List;

    .line 734
    const/4 v0, 0x1

    return v0
.end method
