.class final Lgbis/gbandroid/activities/station/AddStationMap$d;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/BrandAutoCompMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 676
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 677
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;Z)V

    .line 678
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .parameter

    .prologue
    .line 682
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 683
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->b:Ljava/util/List;

    .line 685
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 686
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 687
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v4, :cond_1

    .line 689
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030052

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 690
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 692
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    :cond_0
    :goto_1
    return-void

    .line 688
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/BrandAutoCompMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/BrandAutoCompMessage;->getBrand()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 687
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->d(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$d;->b:Ljava/util/List;

    .line 700
    const/4 v0, 0x1

    return v0
.end method
