.class final Lgbis/gbandroid/activities/station/AddStationMap$f;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Lgbis/gbandroid/entities/GeocodeMessage;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 741
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 742
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 743
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 747
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 749
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->g(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 751
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 752
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->h(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->b:Lgbis/gbandroid/entities/GeocodeMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/GeocodeMessage;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 753
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->h(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->b:Lgbis/gbandroid/entities/GeocodeMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/GeocodeMessage;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 754
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->i(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 755
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->j(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 756
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->k(Lgbis/gbandroid/activities/station/AddStationMap;)V

    .line 764
    :goto_1
    return-void

    .line 758
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->j(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 759
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->l(Lgbis/gbandroid/activities/station/AddStationMap;)V

    goto :goto_1

    .line 762
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    const v2, 0x7f090065

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 4

    .prologue
    .line 768
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->m(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/entities/GeocodeMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->b:Lgbis/gbandroid/entities/GeocodeMessage;

    .line 769
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->b:Lgbis/gbandroid/entities/GeocodeMessage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$f;->b:Lgbis/gbandroid/entities/GeocodeMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/GeocodeMessage;->getLatitude()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 770
    :cond_0
    const/4 v0, 0x0

    .line 772
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
