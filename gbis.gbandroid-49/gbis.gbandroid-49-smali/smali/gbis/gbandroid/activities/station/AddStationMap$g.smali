.class final Lgbis/gbandroid/activities/station/AddStationMap$g;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "g"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 846
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 847
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 848
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 852
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 853
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 854
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->p(Lgbis/gbandroid/activities/station/AddStationMap;)V

    .line 855
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->q(Lgbis/gbandroid/activities/station/AddStationMap;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 856
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->r(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 858
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->r(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 2

    .prologue
    .line 865
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$g;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->s(Lgbis/gbandroid/activities/station/AddStationMap;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 870
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 866
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "class android.view.ViewRoot$CalledFromWrongThreadException"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 868
    const/4 v0, 0x0

    goto :goto_0
.end method
