.class final Lgbis/gbandroid/activities/station/AddStationMap$h;
.super Landroid/widget/ArrayAdapter;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/AddStationMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lgbis/gbandroid/entities/StationMessage;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    const v0, 0x7f030040

    .line 631
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 632
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 633
    iput-object p3, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->b:Ljava/util/List;

    .line 634
    iput v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->c:I

    .line 635
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 640
    if-nez p2, :cond_1

    .line 641
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 643
    new-instance v1, Lgbis/gbandroid/activities/station/AddStationMap$i;

    invoke-direct {v1}, Lgbis/gbandroid/activities/station/AddStationMap$i;-><init>()V

    .line 644
    const v0, 0x7f0700eb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->a:Landroid/widget/TextView;

    .line 645
    const v0, 0x7f0700ec

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->b:Landroid/widget/TextView;

    .line 646
    const v0, 0x7f0700ed

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->c:Landroid/widget/TextView;

    .line 647
    const v0, 0x7f0700ea

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->d:Landroid/widget/ImageView;

    .line 649
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 652
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    .line 653
    if-eqz v0, :cond_0

    .line 654
    iget-object v2, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    iget-object v2, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v2, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 657
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v2

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getGasBrandId()I

    move-result v0

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v2, v0, v3}, Lgbis/gbandroid/GBApplication;->getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 658
    if-eqz v0, :cond_2

    .line 659
    iget-object v1, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 663
    :cond_0
    :goto_1
    return-object p2

    .line 651
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/activities/station/AddStationMap$i;

    move-object v1, v0

    goto :goto_0

    .line 661
    :cond_2
    iget-object v0, v1, Lgbis/gbandroid/activities/station/AddStationMap$i;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap$h;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
