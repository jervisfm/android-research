.class public Lgbis/gbandroid/activities/station/AddStationMap;
.super Lgbis/gbandroid/activities/map/MapStationsBase;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/station/AddStationMap$a;,
        Lgbis/gbandroid/activities/station/AddStationMap$b;,
        Lgbis/gbandroid/activities/station/AddStationMap$c;,
        Lgbis/gbandroid/activities/station/AddStationMap$d;,
        Lgbis/gbandroid/activities/station/AddStationMap$e;,
        Lgbis/gbandroid/activities/station/AddStationMap$f;,
        Lgbis/gbandroid/activities/station/AddStationMap$g;,
        Lgbis/gbandroid/activities/station/AddStationMap$h;,
        Lgbis/gbandroid/activities/station/AddStationMap$i;
    }
.end annotation


# static fields
.field public static final SEARCH_TYPE_CITY:I = 0x2

.field public static final SEARCH_TYPE_NEARME:I = 0x1

.field public static final SEARCH_TYPE_ZIP:I = 0x3


# instance fields
.field private A:Landroid/widget/TextView;

.field private a:Landroid/view/View;

.field private b:Landroid/app/Dialog;

.field private c:Landroid/app/Dialog;

.field private d:Landroid/app/Dialog;

.field private e:Landroid/location/Location;

.field private f:Landroid/widget/AutoCompleteTextView;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/TableLayout;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/CheckedTextView;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Lgbis/gbandroid/utils/CustomAsyncTask;

.field private r:Lgbis/gbandroid/utils/CustomAsyncTask;

.field private s:Landroid/widget/EditText;

.field private t:Landroid/widget/EditText;

.field private u:Landroid/widget/AutoCompleteTextView;

.field private v:Landroid/widget/Spinner;

.field private w:Ljava/lang/String;

.field private x:I

.field private y:Z

.field private z:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;-><init>()V

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->k:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->l:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->m:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->n:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->o:Ljava/lang/String;

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->p:Ljava/lang/String;

    .line 73
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/view/LayoutInflater;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 617
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 618
    aget-object v0, v0, p1

    return-object v0
.end method

.method static synthetic a(Landroid/location/Address;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 561
    invoke-static {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->e(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 595
    invoke-static {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 189
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    if-nez v0, :cond_1

    .line 190
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v2, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 191
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030005

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    .line 192
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v1, 0x7f070019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 193
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v3, 0x7f070018

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/views/StationsMapView;

    iput-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    .line 194
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v1, p0}, Lgbis/gbandroid/views/StationsMapView;->setControl(Lgbis/gbandroid/activities/map/MapStationsBase;)V

    .line 195
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v3, 0x7f07001a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->A:Landroid/widget/TextView;

    .line 196
    const v1, 0x7f090125

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 197
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 198
    const v1, 0x7f090182

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 199
    const v1, 0x7f090183

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 201
    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-le v1, v7, :cond_2

    .line 202
    new-instance v1, Lgbis/gbandroid/activities/station/a;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mRes:Landroid/content/res/Resources;

    iget-object v4, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mappinPress:Landroid/graphics/Bitmap;

    invoke-direct {v1, p0, v3, v4}, Lgbis/gbandroid/activities/station/a;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 219
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 220
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mappinPress:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0, v5, v5, v5, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 221
    invoke-virtual {v2, v6}, Lgbis/gbandroid/views/CustomDialog$Builder;->create(Z)Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    .line 222
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 224
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/station/i;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/i;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 230
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 231
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;-><init>(Lgbis/gbandroid/activities/base/GBActivityMap;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    .line 233
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapOverlays:Ljava/util/List;

    .line 234
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapOverlays:Ljava/util/List;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 236
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0, v6}, Lgbis/gbandroid/views/StationsMapView;->setBuiltInZoomControls(Z)V

    .line 237
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0, v6}, Lgbis/gbandroid/views/StationsMapView;->setSatellite(Z)V

    .line 239
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->d()V

    .line 241
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 243
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->loadMapPins()V

    .line 244
    return-void

    .line 211
    :cond_2
    new-instance v1, Lgbis/gbandroid/activities/station/h;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mappinPress:Landroid/graphics/Bitmap;

    invoke-direct {v1, p0, v3}, Lgbis/gbandroid/activities/station/h;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/AutoCompleteTextView;)V
    .locals 4
    .parameter

    .prologue
    .line 479
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 480
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f030052

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 481
    invoke-virtual {p1, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 482
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/widget/AutoCompleteTextView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 478
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Landroid/widget/AutoCompleteTextView;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/entities/StationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 520
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Lgbis/gbandroid/entities/StationMessage;)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->k:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/AddStationMap;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 432
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Z)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/StationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 521
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/StationDetails;

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getLocation()Landroid/location/Location;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 522
    const-string v1, "station"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 523
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->startActivityForResult(Landroid/content/Intent;I)V

    .line 524
    return-void
.end method

.method private a(Z)V
    .locals 18
    .parameter

    .prologue
    .line 433
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getLastFix()Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 434
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getLastFix()Landroid/location/Location;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    .line 441
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 442
    const/4 v10, 0x0

    .line 445
    :goto_1
    new-instance v1, Lgbis/gbandroid/activities/station/o;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lgbis/gbandroid/activities/station/o;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v1}, Lgbis/gbandroid/activities/station/o;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 446
    new-instance v1, Lgbis/gbandroid/queries/AddStationQuery;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgbis/gbandroid/activities/station/AddStationMap;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getLocation()Landroid/location/Location;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v3, v2, v4}, Lgbis/gbandroid/queries/AddStationQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 447
    move-object/from16 v0, p0

    iget-object v2, v0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lgbis/gbandroid/activities/station/AddStationMap;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgbis/gbandroid/activities/station/AddStationMap;->l:Ljava/lang/String;

    .line 448
    move-object/from16 v0, p0

    iget-object v5, v0, Lgbis/gbandroid/activities/station/AddStationMap;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgbis/gbandroid/activities/station/AddStationMap;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgbis/gbandroid/activities/station/AddStationMap;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lgbis/gbandroid/activities/station/AddStationMap;->p:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lgbis/gbandroid/activities/station/AddStationMap;->n()[Z

    move-result-object v9

    .line 449
    move-object/from16 v0, p0

    iget-object v11, v0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v11}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v11

    int-to-double v11, v11

    const-wide v13, 0x412e848000000000L

    div-double/2addr v11, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v13}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v13

    int-to-double v13, v13

    const-wide v15, 0x412e848000000000L

    div-double/2addr v13, v15

    .line 450
    move-object/from16 v0, p0

    iget-object v15, v0, Lgbis/gbandroid/activities/station/AddStationMap;->w:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v0, v0, Lgbis/gbandroid/activities/station/AddStationMap;->x:I

    move/from16 v16, v0

    move/from16 v17, p1

    .line 447
    invoke-virtual/range {v1 .. v17}, Lgbis/gbandroid/queries/AddStationQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ZIDDLjava/lang/String;IZ)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 451
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->j:Ljava/util/List;

    .line 452
    return-void

    .line 436
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->y:Z

    if-eqz v1, :cond_0

    .line 437
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 438
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    goto/16 :goto_0

    .line 444
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lgbis/gbandroid/activities/station/AddStationMap;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/content/res/Resources;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mRes:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic b(Landroid/location/Address;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 565
    invoke-static {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->f(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 596
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 597
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 247
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 248
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030004

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    .line 249
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f070013

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    .line 250
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f070016

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->g:Landroid/widget/EditText;

    .line 251
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f070017

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    .line 253
    const v0, 0x7f090127

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 254
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 255
    const v0, 0x7f090182

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 256
    const v0, 0x7f090183

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 257
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/station/j;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/j;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 264
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 265
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->f()V

    .line 266
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->j()V

    .line 267
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 268
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->m:Ljava/lang/String;

    return-void
.end method

.method private b(Z)V
    .locals 2
    .parameter

    .prologue
    .line 515
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$a;

    invoke-direct {v0, p0, p0, p1}, Lgbis/gbandroid/activities/station/AddStationMap$a;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;Z)V

    .line 516
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 517
    const v1, 0x7f09008d

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 518
    return-void
.end method

.method private c(Ljava/lang/String;)I
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 601
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f060012

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    move v0, v1

    move v2, v1

    .line 604
    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    if-eqz v0, :cond_1

    .line 610
    :cond_0
    if-eqz v0, :cond_3

    .line 613
    :goto_1
    return v2

    .line 605
    :cond_1
    aget-object v4, v3, v2

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 606
    const/4 v0, 0x1

    goto :goto_0

    .line 608
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    move v2, v1

    .line 613
    goto :goto_1
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic c(Landroid/location/Address;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 578
    invoke-static {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->g(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 271
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 272
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    .line 273
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f07000b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->s:Landroid/widget/EditText;

    .line 274
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f07000d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->t:Landroid/widget/EditText;

    .line 275
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f07000f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    .line 276
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    const v2, 0x7f070011

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->v:Landroid/widget/Spinner;

    .line 278
    const v0, 0x7f090126

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 279
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 280
    const v0, 0x7f090182

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 281
    const v0, 0x7f090183

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 282
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x7f030033

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 283
    const v2, 0x7f030032

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 284
    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->v:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 285
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    .line 286
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/station/k;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/k;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 294
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->e()V

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 296
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->n:Ljava/lang/String;

    return-void
.end method

.method static synthetic d(Landroid/location/Address;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 591
    invoke-static {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->h(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 471
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->m()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const-wide v5, 0x412e848000000000L

    .line 299
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableMyLocation()Z

    .line 300
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 304
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v5

    double-to-int v2, v2

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->o:Ljava/lang/String;

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/station/AddStationMap;Ljava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 600
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic e(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method private static e(Landroid/location/Address;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 562
    invoke-virtual {p0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 308
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/station/l;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/l;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 329
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/station/m;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/m;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 340
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Landroid/widget/AutoCompleteTextView;)V

    .line 341
    return-void
.end method

.method private static f(Landroid/location/Address;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 567
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 568
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    .line 569
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 570
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 574
    :goto_0
    return-object v0

    .line 572
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 574
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method static synthetic f(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 455
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->k()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 345
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/activities/station/n;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/n;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 364
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Landroid/widget/AutoCompleteTextView;)V

    .line 365
    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private static g(Landroid/location/Address;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 580
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v0

    .line 581
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 582
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 583
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 584
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 587
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 368
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$d;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/AddStationMap$d;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    .line 369
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 370
    return-void
.end method

.method static synthetic h(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/location/Location;
    .locals 1
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    return-object v0
.end method

.method private static h(Landroid/location/Address;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 592
    invoke-virtual {p0}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->q:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->q:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->cancel(Z)Z

    .line 375
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/AddStationMap$b;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->q:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 376
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->q:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 378
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->r:Lgbis/gbandroid/utils/CustomAsyncTask;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->r:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->cancel(Z)Z

    .line 383
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$f;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/AddStationMap$f;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->r:Lgbis/gbandroid/utils/CustomAsyncTask;

    .line 384
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->r:Lgbis/gbandroid/utils/CustomAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 385
    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->r:Lgbis/gbandroid/utils/CustomAsyncTask;

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 386
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method private j()V
    .locals 12

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v11, -0x1

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 389
    const/16 v3, 0x11

    new-array v5, v3, [Ljava/lang/String;

    const v3, 0x7f090045

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    const v3, 0x7f090046

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v4

    .line 390
    const v3, 0x7f090047

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    const v3, 0x7f090048

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    const/4 v3, 0x4

    .line 391
    const v6, 0x7f090049

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x5

    const v6, 0x7f09004a

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x6

    .line 392
    const v6, 0x7f09004b

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/4 v3, 0x7

    const v6, 0x7f090054

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0x8

    .line 393
    const v6, 0x7f090050

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0x9

    const v6, 0x7f09004e

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xa

    .line 394
    const v6, 0x7f09004d

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xb

    const v6, 0x7f090051

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xc

    .line 395
    const v6, 0x7f090052

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xd

    const v6, 0x7f090055

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xe

    .line 396
    const v6, 0x7f090053

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0xf

    const v6, 0x7f09004f

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    const/16 v3, 0x10

    .line 397
    const v6, 0x7f09004c

    invoke-virtual {p0, v6}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    .line 398
    new-instance v6, Lgbis/gbandroid/activities/station/AddStationMap$e;

    invoke-direct {v6, p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap$e;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;B)V

    .line 399
    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    if-nez v3, :cond_0

    .line 400
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    .line 401
    array-length v7, v5

    move v3, v0

    :goto_0
    if-lt v3, v7, :cond_2

    .line 410
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_1

    move v2, v1

    .line 414
    :cond_1
    new-instance v5, Landroid/widget/TableRow$LayoutParams;

    const/high16 v0, 0x3f80

    invoke-direct {v5, v11, v11, v0}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    .line 415
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v3, v4

    .line 416
    :goto_1
    if-le v3, v6, :cond_3

    .line 429
    return-void

    .line 401
    :cond_2
    aget-object v8, v5, v3

    .line 402
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f03000b

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 403
    invoke-virtual {v0, v6}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 404
    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setFocusable(Z)V

    .line 406
    iget-object v8, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 401
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 417
    :cond_3
    if-eq v3, v4, :cond_4

    rem-int v0, v3, v2

    if-ne v0, v4, :cond_5

    .line 418
    :cond_4
    new-instance v1, Landroid/widget/TableRow;

    invoke-direct {v1, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 419
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 420
    const/16 v0, 0x11

    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->setGravity(I)V

    .line 421
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    add-int/lit8 v7, v3, -0x1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 422
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    add-int/lit8 v7, v3, -0x1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 416
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 424
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    invoke-virtual {v1}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 425
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    add-int/lit8 v7, v3, -0x1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v5}, Landroid/widget/CheckedTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 426
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    add-int/lit8 v7, v3, -0x1

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    goto :goto_2
.end method

.method private k()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutoCompMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 456
    new-instance v0, Lgbis/gbandroid/activities/station/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/b;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 457
    new-instance v1, Lgbis/gbandroid/queries/AutoCompleteCityQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 458
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 459
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 460
    return-object v0
.end method

.method static synthetic k(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 188
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->a()V

    return-void
.end method

.method private l()Lgbis/gbandroid/entities/GeocodeMessage;
    .locals 6

    .prologue
    .line 464
    new-instance v0, Lgbis/gbandroid/activities/station/c;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/c;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/c;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 465
    new-instance v0, Lgbis/gbandroid/queries/GeocoderQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-direct {v0, p0, v2, v1, v3}, Lgbis/gbandroid/queries/GeocoderQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 466
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->k:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->l:Ljava/lang/String;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->m:Ljava/lang/String;

    iget-object v4, p0, Lgbis/gbandroid/activities/station/AddStationMap;->n:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/queries/GeocoderQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 467
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/GeocodeMessage;

    return-object v0
.end method

.method static synthetic l(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 298
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->d()V

    return-void
.end method

.method static synthetic m(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/entities/GeocodeMessage;
    .locals 1
    .parameter

    .prologue
    .line 463
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->l()Lgbis/gbandroid/entities/GeocodeMessage;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/BrandAutoCompMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    new-instance v0, Lgbis/gbandroid/activities/station/d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/d;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/d;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 473
    new-instance v1, Lgbis/gbandroid/queries/AutoCompleteBrandQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AutoCompleteBrandQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 474
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/AutoCompleteBrandQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 476
    return-object v0
.end method

.method static synthetic n(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->j:Ljava/util/List;

    return-object v0
.end method

.method private n()[Z
    .locals 4

    .prologue
    .line 485
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 486
    new-array v3, v2, [Z

    .line 487
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 489
    return-object v3

    .line 488
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    aput-boolean v0, v3, v1

    .line 487
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic o(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/views/StationsMapView;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    return-object v0
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->f:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 494
    const/4 v0, 0x1

    .line 497
    :goto_0
    return v0

    .line 496
    :cond_0
    const v0, 0x7f09005f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    .line 497
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic p(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 1
    .parameter

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->locateOnMap(Z)V

    return-void
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 502
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 503
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->v:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    const/4 v0, 0x1

    .line 511
    :goto_0
    return v0

    .line 506
    :cond_0
    const v0, 0x7f090062

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    .line 511
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 508
    :cond_1
    const v0, 0x7f090061

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    .line 510
    :cond_2
    const v0, 0x7f090060

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private q()V
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->s:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->k:Ljava/lang/String;

    .line 623
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->l:Ljava/lang/String;

    .line 624
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->u:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->m:Ljava/lang/String;

    .line 625
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->v:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->n:Ljava/lang/String;

    .line 626
    return-void
.end method

.method static synthetic q(Lgbis/gbandroid/activities/station/AddStationMap;)Z
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-boolean v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->zoomLevelFlag:Z

    return v0
.end method

.method static synthetic r(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->A:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic s(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->queryMapWebService()V

    return-void
.end method

.method static synthetic t(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->logoDefault:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic u(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mappinPress:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic v(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/activities/base/GBActivityMap;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getActivity()Lgbis/gbandroid/activities/base/GBActivityMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic w(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/Spinner;
    .locals 1
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->v:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic x(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 367
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->g()V

    return-void
.end method

.method static synthetic y(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 1
    .parameter

    .prologue
    .line 514
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Z)V

    return-void
.end method


# virtual methods
.method public loadMapPins()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->center:Lcom/google/android/maps/GeoPoint;

    .line 182
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->z:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->z:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 184
    :cond_0
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$g;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/AddStationMap$g;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->z:Landroid/os/AsyncTask;

    .line 185
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->z:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 186
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    const/4 v1, -0x2

    .line 884
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    if-ne p1, v0, :cond_2

    .line 885
    if-ne p2, v1, :cond_0

    .line 886
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 887
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->q()V

    .line 888
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->i()V

    .line 923
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage()V

    goto :goto_0

    .line 893
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    if-ne p1, v0, :cond_6

    .line 894
    if-ne p2, v1, :cond_4

    .line 895
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->h()V

    .line 896
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    if-nez v0, :cond_3

    .line 897
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->b()V

    goto :goto_0

    .line 899
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 902
    :cond_4
    if-ne p2, v2, :cond_0

    .line 904
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    if-nez v0, :cond_5

    .line 905
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->finish()V

    goto :goto_0

    .line 907
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 910
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    if-ne p1, v0, :cond_0

    .line 911
    if-ne p2, v1, :cond_7

    .line 912
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->b(Z)V

    goto :goto_0

    .line 915
    :cond_7
    if-ne p2, v2, :cond_0

    .line 916
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 917
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    if-nez v0, :cond_8

    .line 918
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->a()V

    goto :goto_0

    .line 920
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .parameter

    .prologue
    .line 147
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v2

    .line 150
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 154
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 155
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->j()V

    .line 157
    :cond_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->h:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 152
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    .line 109
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/map/MapStationsBase;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 111
    if-eqz v1, :cond_1

    .line 112
    const-string v0, "my location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    .line 113
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v2, v4

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->a()V

    .line 120
    :goto_0
    const-string v0, "search terms"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->w:Ljava/lang/String;

    .line 121
    const-string v0, "search type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->x:I

    .line 124
    :goto_1
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->y:Z

    .line 117
    new-instance v0, Landroid/location/Location;

    const-string v2, "reverseGeocoded"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->e:Landroid/location/Location;

    .line 118
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->c()V

    goto :goto_0

    .line 123
    :cond_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->c()V

    goto :goto_1
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 927
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    .line 928
    const/4 v1, 0x4

    if-ne p2, v1, :cond_2

    .line 929
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    if-ne p1, v1, :cond_0

    .line 930
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 931
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 950
    :goto_0
    return v0

    .line 934
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    if-ne p1, v1, :cond_2

    .line 935
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 936
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    .line 937
    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->d:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 939
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/AddStationMap;->finish()V

    goto :goto_0

    .line 950
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onPause()V

    .line 141
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->disableMyLocation()V

    .line 143
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lgbis/gbandroid/activities/map/MapStationsBase;->onResume()V

    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-direct {v0, p0, p0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;-><init>(Lgbis/gbandroid/activities/base/GBActivityMap;Landroid/content/Context;Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    .line 132
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mapView:Lgbis/gbandroid/views/StationsMapView;

    invoke-virtual {v0}, Lgbis/gbandroid/views/StationsMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->myLocationOverlay:Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap$MyOwnLocationOverlay;->enableMyLocation()Z

    .line 136
    :cond_1
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 955
    const v0, 0x7f090248

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public showMatchStationDialog()V
    .locals 4

    .prologue
    .line 527
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v1, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 528
    iget-object v0, p0, Lgbis/gbandroid/activities/station/AddStationMap;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030016

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 529
    const v0, 0x7f090066

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 530
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 531
    const v0, 0x7f07004d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f090067

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    const v0, 0x7f070047

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 533
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 534
    new-instance v2, Lgbis/gbandroid/activities/station/AddStationMap$h;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/AddStationMap;->j:Ljava/util/List;

    invoke-direct {v2, p0, p0, v3}, Lgbis/gbandroid/activities/station/AddStationMap$h;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/content/Context;Ljava/util/List;)V

    .line 535
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 536
    const v2, 0x7f09019a

    new-instance v3, Lgbis/gbandroid/activities/station/e;

    invoke-direct {v3, p0, v0}, Lgbis/gbandroid/activities/station/e;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/widget/ListView;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 546
    const v2, 0x7f09019b

    new-instance v3, Lgbis/gbandroid/activities/station/f;

    invoke-direct {v3, p0}, Lgbis/gbandroid/activities/station/f;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 552
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v1

    .line 553
    new-instance v2, Lgbis/gbandroid/activities/station/g;

    invoke-direct {v2, p0}, Lgbis/gbandroid/activities/station/g;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 558
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog;->show()V

    .line 559
    return-void
.end method
