.class final Lgbis/gbandroid/activities/station/FavAddStation$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/FavAddStation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/FavAddStation;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/FavAddStation;Lgbis/gbandroid/activities/base/GBActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 172
    iput-object p1, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    .line 173
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 174
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .parameter

    .prologue
    .line 178
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 180
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->a(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->b(Lgbis/gbandroid/activities/station/FavAddStation;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    const v2, 0x7f09003f

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->showMessage(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->setResult(I)V

    .line 186
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->finish()V

    .line 190
    :cond_0
    :goto_1
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    const v2, 0x7f090041

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation$a;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->c(Lgbis/gbandroid/activities/station/FavAddStation;)Z

    move-result v0

    return v0
.end method
