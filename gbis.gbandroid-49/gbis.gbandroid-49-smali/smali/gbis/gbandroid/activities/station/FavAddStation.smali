.class public Lgbis/gbandroid/activities/station/FavAddStation;
.super Lgbis/gbandroid/activities/base/GBActivityDialog;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/station/FavAddStation$a;,
        Lgbis/gbandroid/activities/station/FavAddStation$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Lgbis/gbandroid/entities/Station;

.field private e:Landroid/widget/ListView;

.field private f:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityDialog;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 82
    const v0, 0x7f070047

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    .line 83
    const v0, 0x7f07004d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030015

    invoke-virtual {v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 85
    const v1, 0x7f07004b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    .line 86
    const v1, 0x7f07004c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 87
    const v2, 0x7f07004a

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 88
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->d:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v4}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/station/FavAddStation;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 89
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->d:Lgbis/gbandroid/entities/Station;

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/station/FavAddStation;->setGBActivityDialogStationIcon(Lgbis/gbandroid/entities/Station;)V

    .line 90
    const v4, 0x7f090119

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    const v4, 0x7f090040

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v2, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    invoke-virtual {v2, v9}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 93
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v4, 0x7f030032

    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->d()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 94
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    new-instance v5, Lgbis/gbandroid/activities/station/p;

    invoke-direct {v5, p0}, Lgbis/gbandroid/activities/station/p;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;)V

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 102
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    new-array v5, v9, [Landroid/text/InputFilter;

    const/4 v6, 0x0

    .line 103
    new-instance v7, Landroid/text/InputFilter$LengthFilter;

    const/16 v8, 0x1e

    invoke-direct {v7, v8}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v7, v5, v6

    .line 102
    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 105
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 106
    new-instance v4, Lgbis/gbandroid/activities/station/q;

    invoke-direct {v4, p0, v1, v0}, Lgbis/gbandroid/activities/station/q;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;Landroid/widget/CheckBox;Landroid/widget/TextView;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 120
    iget-object v4, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    invoke-virtual {v4, v3, v10, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 121
    iget-object v3, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    invoke-virtual {v3, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    iget-object v2, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    new-instance v3, Lgbis/gbandroid/activities/station/r;

    invoke-direct {v3, p0, v0, v1}, Lgbis/gbandroid/activities/station/r;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 134
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/FavAddStation;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput p1, p0, Lgbis/gbandroid/activities/station/FavAddStation;->c:I

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/FavAddStation;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lgbis/gbandroid/activities/station/FavAddStation;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/FavAddStation;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Lgbis/gbandroid/activities/station/FavAddStation$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/FavAddStation$a;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 138
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 139
    const v0, 0x7f090091

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->loadDialog(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lgbis/gbandroid/activities/station/FavAddStation$b;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/FavAddStation$b;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 144
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 145
    const v1, 0x7f090092

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 146
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/FavAddStation;)Z
    .locals 1
    .parameter

    .prologue
    .line 164
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lgbis/gbandroid/activities/station/FavAddStation;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->a()V

    return-void
.end method

.method private d()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 151
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 153
    return-object v2

    .line 152
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 151
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 158
    new-instance v0, Lgbis/gbandroid/activities/station/s;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/s;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/s;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 159
    new-instance v1, Lgbis/gbandroid/queries/FavListQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/FavAddStation;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 160
    invoke-virtual {v1}, Lgbis/gbandroid/queries/FavListQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 161
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    .line 162
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/station/FavAddStation;)V
    .locals 0
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->e()V

    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method private f()Z
    .locals 4

    .prologue
    .line 165
    new-instance v0, Lgbis/gbandroid/activities/station/t;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/t;-><init>(Lgbis/gbandroid/activities/station/FavAddStation;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/t;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 166
    new-instance v1, Lgbis/gbandroid/queries/FavAddStationQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/FavAddStation;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavAddStationQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 167
    iget v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->c:I

    iget-object v2, p0, Lgbis/gbandroid/activities/station/FavAddStation;->d:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/Station;->getStationId()I

    move-result v2

    iget-object v3, p0, Lgbis/gbandroid/activities/station/FavAddStation;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lgbis/gbandroid/queries/FavAddStationQuery;->getResponseObject(IILjava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 168
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/station/FavAddStation;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 230
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->getGBActivityDialogPositiveButton()Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 231
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 232
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->a:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->a:Ljava/lang/String;

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    :cond_0
    const v0, 0x7f090070

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->showMessage(Ljava/lang/String;)V

    .line 242
    :cond_1
    :goto_0
    return-void

    .line 236
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->c:I

    .line 237
    :cond_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->b()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 52
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityDialog;->onCreate(Landroid/os/Bundle;)V

    .line 54
    :try_start_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    const-string v1, "station"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->d:Lgbis/gbandroid/entities/Station;

    .line 56
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 57
    if-eqz v0, :cond_0

    .line 58
    iput-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    .line 59
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->a()V

    .line 68
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 65
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->setResult(I)V

    .line 66
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/FavAddStation;->finish()V

    goto :goto_0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lgbis/gbandroid/activities/station/FavAddStation;->b:Ljava/util/List;

    return-object v0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    const v0, 0x7f090246

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected setGBActivityDialogButtons()V
    .locals 2

    .prologue
    .line 76
    const v0, 0x7f030016

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->setGBViewInContentLayout(IZ)V

    .line 77
    const v0, 0x7f090119

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->setGBActivityDialogTitle(Ljava/lang/String;)V

    .line 78
    const v0, 0x7f090175

    invoke-virtual {p0, v0, p0}, Lgbis/gbandroid/activities/station/FavAddStation;->setGBActivityDialogPositiveButton(ILandroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method
