.class public Lgbis/gbandroid/activities/station/PictureView;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lgbis/gbandroid/entities/StationMessage;

.field private b:I

.field private c:Landroid/view/View;

.field private d:Landroid/app/Dialog;

.field private e:Lgbis/gbandroid/utils/ImageLoader;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/PictureView;)I
    .locals 1
    .parameter

    .prologue
    .line 30
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    return v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 81
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03004d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 84
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getGasBrandId()I

    move-result v0

    const-string v2, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v1, v0, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 85
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 86
    const v0, 0x7f090182

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/PictureView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 87
    const v0, 0x7f090183

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/PictureView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 88
    const/4 v0, 0x0

    .line 90
    :try_start_0
    sget-object v2, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 91
    const/4 v0, 0x1

    .line 93
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create(Z)Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    .line 94
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/station/u;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/u;-><init>(Lgbis/gbandroid/activities/station/PictureView;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 100
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    new-instance v1, Lgbis/gbandroid/activities/station/v;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/v;-><init>(Lgbis/gbandroid/activities/station/PictureView;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 108
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->b()V

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 110
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->c()V

    .line 111
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 114
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    const v1, 0x7f07011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->f:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    const v1, 0x7f07011f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->g:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    const v1, 0x7f07011b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->h:Landroid/widget/ImageView;

    .line 117
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    .line 118
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    const v1, 0x7f070037

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    const v1, 0x7f070120

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->c:Landroid/view/View;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    .line 124
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_0
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lgbis/gbandroid/activities/station/w;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/w;-><init>(Lgbis/gbandroid/activities/station/PictureView;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->k:Landroid/view/GestureDetector;

    .line 137
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->h:Landroid/widget/ImageView;

    new-instance v1, Lgbis/gbandroid/activities/station/x;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/x;-><init>(Lgbis/gbandroid/activities/station/PictureView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 145
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/PictureView;)V
    .locals 0
    .parameter

    .prologue
    .line 172
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->e()V

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/PictureView;)Lgbis/gbandroid/entities/StationMessage;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 149
    :try_start_0
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :goto_0
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    iget-object v1, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    .line 154
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    :goto_1
    iget-object v1, p0, Lgbis/gbandroid/activities/station/PictureView;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationPhotos;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v1, p0, Lgbis/gbandroid/activities/station/PictureView;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationPhotos;->getCaption()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v1, p0, Lgbis/gbandroid/activities/station/PictureView;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationPhotos;->getMediumPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 161
    iget-object v1, p0, Lgbis/gbandroid/activities/station/PictureView;->e:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationPhotos;->getMediumPath()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lgbis/gbandroid/activities/station/PictureView;->h:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, p0, v2, v3}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;Z)V

    .line 165
    :goto_2
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 163
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f090001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/PictureView;->showMessage(Ljava/lang/String;)V

    goto :goto_2

    .line 156
    :cond_1
    :try_start_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private d()V
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    .line 169
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->c()V

    .line 170
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/station/PictureView;)V
    .locals 0
    .parameter

    .prologue
    .line 167
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->d()V

    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/station/PictureView;)Landroid/view/GestureDetector;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->k:Landroid/view/GestureDetector;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    .line 174
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->c()V

    .line 175
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 179
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    .line 180
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->d()V

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 183
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->e()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->j:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 189
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->d()V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->i:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 192
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->e()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter

    .prologue
    .line 75
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 76
    iget-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 77
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->a()V

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 43
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/PictureView;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 45
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v2, 0x7f02009a

    invoke-direct {v0, p0, v2}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->e:Lgbis/gbandroid/utils/ImageLoader;

    .line 46
    if-eqz v1, :cond_1

    .line 47
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 48
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/PictureView;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 50
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/PictureView;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    .line 55
    :goto_0
    const-string v0, "station"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/PictureView;->a:Lgbis/gbandroid/entities/StationMessage;

    .line 56
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/PictureView;->a()V

    .line 59
    :goto_1
    return-void

    .line 54
    :cond_0
    const-string v0, "station photos position"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/PictureView;->finish()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivity;->onDestroy()V

    .line 64
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/PictureView;->cleanImageCache()V

    .line 65
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lgbis/gbandroid/activities/station/PictureView;->b:I

    .line 70
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const v0, 0x7f09024d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/PictureView;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
