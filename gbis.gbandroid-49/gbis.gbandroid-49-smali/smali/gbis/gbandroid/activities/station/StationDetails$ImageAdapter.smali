.class public Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/listeners/ImageLazyLoaderListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/StationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageAdapter"
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lgbis/gbandroid/activities/station/StationDetails;

.field private c:Landroid/content/Context;

.field private d:Lgbis/gbandroid/utils/ImageLoader;

.field private e:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/activities/station/StationDetails;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 878
    iput-object p1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->b:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 879
    iput-object p2, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->c:Landroid/content/Context;

    .line 880
    sget-object v0, Lgbis/gbandroid/R$styleable;->ShowStation:[I

    invoke-virtual {p1, v0}, Lgbis/gbandroid/activities/station/StationDetails;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 881
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->a:I

    .line 882
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 883
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v1, 0x7f02009a

    invoke-direct {v0, p1, v1}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->d:Lgbis/gbandroid/utils/ImageLoader;

    .line 884
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->d:Lgbis/gbandroid/utils/ImageLoader;

    invoke-virtual {v0, p0}, Lgbis/gbandroid/utils/ImageLoader;->setOnLazyLoaderFinished(Lgbis/gbandroid/listeners/ImageLazyLoaderListener;)V

    .line 885
    iput-object p3, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->e:[Ljava/lang/String;

    .line 886
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->e:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 888
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 891
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 894
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 895
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->e:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 896
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->d:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->e:[Ljava/lang/String;

    aget-object v2, v2, p1

    iget-object v3, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->b:Lgbis/gbandroid/activities/station/StationDetails;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;Z)V

    .line 897
    iget v1, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 898
    return-object v0
.end method

.method public onImageFailed(Landroid/widget/ImageView;)V
    .locals 0
    .parameter

    .prologue
    .line 915
    return-void
.end method

.method public onImageLoaded(Landroid/widget/ImageView;)V
    .locals 3
    .parameter

    .prologue
    .line 905
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 906
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 907
    new-instance v2, Landroid/widget/Gallery$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 908
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;->b:Lgbis/gbandroid/activities/station/StationDetails;

    const v2, 0x7f070163

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    invoke-virtual {v0}, Landroid/widget/Gallery;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 909
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-le v1, v2, :cond_0

    .line 910
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 911
    :cond_0
    return-void
.end method
