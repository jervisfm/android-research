.class final Lgbis/gbandroid/activities/station/StationDetails$b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/activities/station/StationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/StationDetails;


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 995
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .parameter

    .prologue
    .line 999
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/StationDetails;->g(Lgbis/gbandroid/activities/station/StationDetails;)Landroid/hardware/Sensor;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1009
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/StationDetails;->h(Lgbis/gbandroid/activities/station/StationDetails;)F

    move-result v0

    .line 1010
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v1, v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Lgbis/gbandroid/activities/station/StationDetails;F)V

    .line 1011
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/StationDetails;->h(Lgbis/gbandroid/activities/station/StationDetails;)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x4000

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/StationDetails;->i(Lgbis/gbandroid/activities/station/StationDetails;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1012
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails$b;->a:Lgbis/gbandroid/activities/station/StationDetails;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/StationDetails;->i(Lgbis/gbandroid/activities/station/StationDetails;)Lgbis/gbandroid/entities/StationMessage;

    invoke-static {}, Lgbis/gbandroid/activities/station/StationDetails;->a()V

    .line 1014
    :cond_0
    return-void
.end method
