.class public Lgbis/gbandroid/activities/station/StationDetails;
.super Lgbis/gbandroid/activities/base/GBActivityAds;
.source "GBFile"

# interfaces
.implements Lgbis/gbandroid/listeners/ImageLazyLoaderListener;
.implements Lgbis/gbandroid/listeners/ScrollViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;,
        Lgbis/gbandroid/activities/station/StationDetails$a;,
        Lgbis/gbandroid/activities/station/StationDetails$b;,
        Lgbis/gbandroid/activities/station/StationDetails$c;,
        Lgbis/gbandroid/activities/station/StationDetails$d;
    }
.end annotation


# instance fields
.field a:Landroid/content/BroadcastReceiver;

.field private b:I

.field private c:Lgbis/gbandroid/entities/StationMessage;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lgbis/gbandroid/activities/station/StationDetails$a;

.field private g:I

.field private h:I

.field private i:Landroid/widget/TableLayout;

.field private j:Landroid/widget/TableRow;

.field private k:Landroid/view/ViewGroup;

.field private l:Landroid/view/ViewGroup;

.field private m:Landroid/view/ViewGroup;

.field private n:Landroid/view/ViewGroup;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/hardware/SensorManager;

.field private q:Landroid/hardware/Sensor;

.field private r:Lgbis/gbandroid/activities/station/StationDetails$b;

.field private s:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;-><init>()V

    .line 927
    new-instance v0, Lgbis/gbandroid/activities/station/y;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/y;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->a:Landroid/content/BroadcastReceiver;

    .line 85
    return-void
.end method

.method private a(ILandroid/widget/LinearLayout;[Landroid/view/ViewGroup;)I
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 791
    packed-switch p1, :pswitch_data_0

    .line 808
    const/4 v1, 0x0

    aput-object v1, p3, v0

    .line 810
    :goto_0
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    move v1, v0

    .line 811
    :goto_1
    if-lt v1, v2, :cond_0

    .line 814
    :goto_2
    return v0

    .line 793
    :pswitch_0
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    aput-object v1, p3, v0

    .line 794
    const/4 v0, 0x3

    goto :goto_2

    .line 796
    :pswitch_1
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    aput-object v1, p3, v0

    goto :goto_0

    .line 799
    :pswitch_2
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->l:Landroid/view/ViewGroup;

    aput-object v1, p3, v0

    goto :goto_0

    .line 802
    :pswitch_3
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->m:Landroid/view/ViewGroup;

    aput-object v1, p3, v0

    goto :goto_0

    .line 805
    :pswitch_4
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->n:Landroid/view/ViewGroup;

    aput-object v1, p3, v0

    goto :goto_0

    .line 812
    :cond_0
    aget-object v3, p3, v0

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 813
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 811
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 791
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 662
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 663
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 664
    const/4 v1, 0x1

    :goto_0
    if-lt v1, v3, :cond_0

    .line 666
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ": "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 665
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 664
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a()V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method private a(DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f060001

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    const v0, 0x7f090150

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->l:Landroid/view/ViewGroup;

    const v0, 0x7f090151

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 194
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->m:Landroid/view/ViewGroup;

    const v0, 0x7f090152

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->n:Landroid/view/ViewGroup;

    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 3
    .parameter

    .prologue
    .line 866
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/PictureView;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 867
    const-string v1, "station"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 868
    const-string v1, "station photos position"

    invoke-virtual {v0, v1, p1}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 869
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->startActivity(Landroid/content/Intent;)V

    .line 870
    return-void
.end method

.method private a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 201
    const v0, 0x7f07016e

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 202
    const v1, 0x7f07016f

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 203
    const v2, 0x7f070171

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 204
    const v3, 0x7f070172

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 205
    const v4, 0x7f070173

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 206
    const v5, 0x7f07016b

    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 207
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v6

    const v7, 0x7f070159

    if-ne v6, v7, :cond_0

    .line 208
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 209
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 210
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 212
    :cond_0
    invoke-virtual {v5, p7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    const-wide/16 v5, 0x0

    cmpl-double v5, p2, v5

    if-lez v5, :cond_3

    .line 214
    const-string v5, "USA"

    invoke-virtual {p4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 215
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x2

    invoke-static {p2, p3, v6}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    :goto_0
    if-eqz p5, :cond_1

    const-string v0, ""

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 222
    invoke-virtual {v2, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    :cond_1
    invoke-virtual {v3, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    :goto_1
    return-void

    .line 218
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    invoke-static {p2, p3, v6}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 227
    :cond_3
    const-string v5, "---"

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    const-string v0, "---"

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 230
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    const v0, 0x7f09002f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 490
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->d()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationDetails;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 105
    iput p1, p0, Lgbis/gbandroid/activities/station/StationDetails;->s:F

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationDetails;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 865
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/StationDetails;->a(I)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lgbis/gbandroid/activities/station/StationDetails;->setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lgbis/gbandroid/entities/Station;)V
    .locals 6
    .parameter

    .prologue
    const v2, 0x7f070155

    .line 236
    const v0, 0x7f070159

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    .line 237
    const v0, 0x7f07015a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->l:Landroid/view/ViewGroup;

    .line 238
    const v0, 0x7f07015b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->m:Landroid/view/ViewGroup;

    .line 239
    const v0, 0x7f07015c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->n:Landroid/view/ViewGroup;

    .line 240
    const v0, 0x7f07014f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomTextViewAutoResize;

    .line 241
    const v1, 0x7f07014b

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/views/layouts/CustomScrollView;

    invoke-virtual {v1, p0}, Lgbis/gbandroid/views/layouts/CustomScrollView;->setOnScrollViewListener(Lgbis/gbandroid/listeners/ScrollViewListener;)V

    .line 242
    const/high16 v1, 0x41a0

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->setMinTextSize(F)V

    .line 243
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->setText(Ljava/lang/CharSequence;)V

    .line 245
    const v0, 0x7f070153

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const v0, 0x7f070154

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    const v0, 0x7f070157

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->o:Landroid/widget/TextView;

    .line 248
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 249
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 250
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :goto_0
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/StationDetails;->b(Lgbis/gbandroid/entities/Station;)V

    .line 256
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    invoke-virtual {p1, p0, v0}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v1

    .line 257
    const-wide/16 v3, 0x0

    cmpl-double v0, v1, v3

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    invoke-virtual {p1, p0, v0}, Lgbis/gbandroid/entities/Station;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 259
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v4

    .line 260
    const-string v5, ""

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/activities/station/StationDetails;->a(DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_0
    instance-of v0, p1, Lgbis/gbandroid/entities/ListMessage;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 263
    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Lgbis/gbandroid/entities/Station;D)V

    .line 264
    :cond_1
    return-void

    .line 253
    :cond_2
    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lgbis/gbandroid/entities/Station;D)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const-wide v4, 0x3ff9bfdf7e8038a0L

    const-wide/16 v2, 0x0

    .line 366
    cmpl-double v0, p2, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 367
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "distancePreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 369
    :try_start_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 370
    const-string v1, "miles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->o:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2, p3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->o:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    mul-double v2, p2, v4

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 375
    :cond_2
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 376
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->o:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2, p3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 378
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->o:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    mul-double v2, p2, v4

    invoke-static {v2, v3}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 829
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 830
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->startActivity(Landroid/content/Intent;)V

    .line 831
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 648
    iget v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    .line 649
    iget v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 650
    new-instance v0, Landroid/widget/TableRow;

    invoke-direct {v0, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->j:Landroid/widget/TableRow;

    .line 651
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->j:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 653
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030006

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 654
    const v1, 0x7f07001c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 655
    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->j:Landroid/widget/TableRow;

    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 657
    iget v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v1, v1

    const/high16 v2, 0x42c8

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getDensity()F

    move-result v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 658
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    .line 659
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 695
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 696
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 697
    if-nez v0, :cond_0

    .line 698
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 701
    :cond_1
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->p:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->r:Lgbis/gbandroid/activities/station/StationDetails$b;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->p:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->r:Lgbis/gbandroid/activities/station/StationDetails$b;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 179
    iput-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->r:Lgbis/gbandroid/activities/station/StationDetails$b;

    .line 181
    :cond_0
    iput-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->q:Landroid/hardware/Sensor;

    .line 182
    iput-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->p:Landroid/hardware/SensorManager;

    .line 184
    :cond_1
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 327
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->c()V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/activities/station/StationDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 833
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/StationDetails;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Lgbis/gbandroid/entities/Station;)V
    .locals 7
    .parameter

    .prologue
    const v2, 0x7f020076

    const/4 v4, 0x0

    .line 267
    const v0, 0x7f07014d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 268
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const-string v1, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-direct {v0, p0, v2, v1}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 272
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v1

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getDensity()F

    move-result v5

    invoke-virtual {v1, v2, v5}, Lgbis/gbandroid/GBApplication;->getImageUrl(IF)Ljava/lang/String;

    move-result-object v1

    .line 273
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 275
    :try_start_0
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    if-lez v2, :cond_2

    .line 276
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getBrand(I)Landroid/database/Cursor;

    move-result-object v2

    .line 277
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_0

    .line 278
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandVersion()I

    move-result v5

    invoke-virtual {p0, v2, v5}, Lgbis/gbandroid/activities/station/StationDetails;->addBrand(II)Z

    .line 279
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getBrand(I)Landroid/database/Cursor;

    move-result-object v2

    .line 281
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 282
    const-string v5, "version"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 284
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandVersion()I

    move-result v5

    if-le v5, v2, :cond_1

    .line 285
    const/4 v4, 0x1

    .line 286
    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandVersion()I

    move-result v5

    invoke-virtual {p0, v2, v5}, Lgbis/gbandroid/activities/station/StationDetails;->updateBrandVersion(II)Z

    .line 288
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p1}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZILjava/lang/String;)V

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 834
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 835
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 836
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->startActivity(Landroid/content/Intent;)V

    .line 837
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 328
    const v0, 0x7f070166

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 329
    const v1, 0x7f070164

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 330
    const v2, 0x7f070168

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 331
    const v3, 0x7f070158

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 332
    new-instance v4, Lgbis/gbandroid/activities/station/ae;

    invoke-direct {v4, p0, v3}, Lgbis/gbandroid/activities/station/ae;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Landroid/widget/Button;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    new-instance v3, Lgbis/gbandroid/activities/station/af;

    invoke-direct {v3, p0}, Lgbis/gbandroid/activities/station/af;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    new-instance v0, Lgbis/gbandroid/activities/station/ag;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/ag;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    new-instance v0, Lgbis/gbandroid/activities/station/ah;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/ah;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 358
    new-instance v0, Lgbis/gbandroid/activities/station/StationDetails$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/station/StationDetails$c;-><init>(Lgbis/gbandroid/activities/station/StationDetails;B)V

    .line 359
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->l:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 533
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->i()V

    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/activities/station/StationDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 828
    invoke-direct {p0, p1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;)V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 491
    new-instance v0, Lgbis/gbandroid/activities/station/StationDetails$d;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/activities/station/StationDetails$d;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Lgbis/gbandroid/activities/base/GBActivity;)V

    .line 492
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/StationDetails$d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 493
    return-void
.end method

.method static synthetic d(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 713
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->n()V

    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 496
    const v0, 0x7f070151

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 497
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationPhotos;->getMediumPath()Ljava/lang/String;

    move-result-object v1

    .line 498
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 499
    new-instance v2, Lgbis/gbandroid/utils/ImageLoader;

    const v3, 0x7f02009a

    invoke-direct {v2, p0, v3}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;I)V

    .line 500
    invoke-virtual {v2, p0}, Lgbis/gbandroid/utils/ImageLoader;->setOnLazyLoaderFinished(Lgbis/gbandroid/listeners/ImageLazyLoaderListener;)V

    .line 501
    invoke-virtual {v2, v1, p0, v0, v4}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;Z)V

    .line 502
    return-void
.end method

.method static synthetic e(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 526
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->h()V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 505
    const v0, 0x7f070163

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    .line 506
    const v1, 0x7f070162

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 507
    new-instance v1, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;

    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->g()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, Lgbis/gbandroid/activities/station/StationDetails$ImageAdapter;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Landroid/content/Context;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 508
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setVisibility(I)V

    .line 509
    new-instance v1, Lgbis/gbandroid/activities/station/ai;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/ai;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 516
    return-void
.end method

.method static synthetic f(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 839
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->r()V

    return-void
.end method

.method static synthetic g(Lgbis/gbandroid/activities/station/StationDetails;)Landroid/hardware/Sensor;
    .locals 1
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->q:Landroid/hardware/Sensor;

    return-object v0
.end method

.method private g()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 519
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 520
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 521
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 523
    return-object v2

    .line 522
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationPhotos;->getMediumPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 521
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic h(Lgbis/gbandroid/activities/station/StationDetails;)F
    .locals 1
    .parameter

    .prologue
    .line 105
    iget v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->s:F

    return v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 527
    new-instance v0, Lgbis/gbandroid/activities/station/aj;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/aj;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/aj;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 528
    new-instance v1, Lgbis/gbandroid/queries/StationDetailsQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/StationDetailsQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 529
    iget v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->b:I

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/StationDetailsQuery;->getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 530
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    .line 531
    return-void
.end method

.method static synthetic i(Lgbis/gbandroid/activities/station/StationDetails;)Lgbis/gbandroid/entities/StationMessage;
    .locals 1
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    return-object v0
.end method

.method private i()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 534
    const v0, 0x7f070153

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 535
    const v1, 0x7f070154

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 536
    const v2, 0x7f070155

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 537
    const v3, 0x7f070156

    invoke-virtual {p0, v3}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 538
    const v4, 0x7f07014e

    invoke-virtual {p0, v4}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 541
    iget-object v5, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v5}, Lgbis/gbandroid/entities/StationMessage;->isNotIntersection()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 542
    const-string v5, "near"

    .line 545
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v7}, Lgbis/gbandroid/entities/StationMessage;->getAddress()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v6}, Lgbis/gbandroid/entities/StationMessage;->getCrossStreet()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v5}, Lgbis/gbandroid/entities/StationMessage;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v5}, Lgbis/gbandroid/entities/StationMessage;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 547
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 549
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPhone()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPhone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    new-instance v0, Lgbis/gbandroid/activities/station/ak;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/ak;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->isIsFav()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 561
    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 565
    :goto_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->j()V

    .line 566
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->k()V

    .line 567
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->l()V

    .line 569
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 570
    const v0, 0x7f070162

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 571
    const v0, 0x7f070161

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 572
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->e()V

    .line 573
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->f()V

    .line 577
    :goto_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->o()V

    .line 578
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    .line 579
    return-void

    .line 544
    :cond_1
    const-string v5, "&"

    goto/16 :goto_0

    .line 563
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 576
    :cond_3
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->n()V

    goto :goto_2
.end method

.method private j()V
    .locals 8

    .prologue
    .line 582
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->k:Landroid/view/ViewGroup;

    .line 583
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getRegPrice()D

    move-result-wide v2

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    const v5, 0x7f0901c9

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v5}, Lgbis/gbandroid/entities/StationMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 584
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getRegMemberId()Ljava/lang/String;

    move-result-object v6

    const v0, 0x7f090150

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    .line 582
    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->l:Landroid/view/ViewGroup;

    .line 586
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getMidPrice()D

    move-result-wide v2

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    const v5, 0x7f0901ca

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v5}, Lgbis/gbandroid/entities/StationMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 587
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getMidMemberId()Ljava/lang/String;

    move-result-object v6

    const v0, 0x7f090151

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    .line 585
    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->m:Landroid/view/ViewGroup;

    .line 589
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPremPrice()D

    move-result-wide v2

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    const v5, 0x7f0901cb

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v5}, Lgbis/gbandroid/entities/StationMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 590
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getPremMemberId()Ljava/lang/String;

    move-result-object v6

    const v0, 0x7f090152

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    .line 588
    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->n:Landroid/view/ViewGroup;

    .line 592
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getDieselPrice()D

    move-result-wide v2

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getCountry()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    const v5, 0x7f0901cc

    invoke-virtual {p0, v5}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p0, v5}, Lgbis/gbandroid/entities/StationMessage;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 593
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getDieselMemberId()Ljava/lang/String;

    move-result-object v6

    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    .line 591
    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/activities/station/StationDetails;->a(Landroid/view/ViewGroup;DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    return-void
.end method

.method static synthetic j(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 855
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->s()V

    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 597
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->h:I

    .line 598
    const v0, 0x7f070160

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    .line 599
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 601
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    invoke-virtual {v0, v3}, Landroid/widget/TableLayout;->setStretchAllColumns(Z)V

    .line 602
    new-instance v0, Landroid/widget/TableRow;

    invoke-direct {v0, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->j:Landroid/widget/TableRow;

    .line 603
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasRegularGas()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200c3

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 605
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasMidgradeGas()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    const v0, 0x7f090046

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020097

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 607
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasPremiumGas()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 608
    const v0, 0x7f090047

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200a2

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 609
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasDiesel()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 610
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02004e

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 611
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasE85()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 612
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02004f

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 613
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasPropane()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 614
    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200a9

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 615
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasCStore()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 616
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020042

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 617
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasServiceStation()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 618
    const v0, 0x7f090054

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200ca

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 619
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasPayAtPump()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 620
    const v0, 0x7f090050

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200a0

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 621
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasRestaurant()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 622
    const v0, 0x7f09004e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200c4

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 623
    :cond_9
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasRestrooms()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 624
    const v0, 0x7f09004d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200c5

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 625
    :cond_a
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasAir()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 626
    const v0, 0x7f090051

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020001

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 627
    :cond_b
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasPayphone()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 628
    const v0, 0x7f090052

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200a1

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 629
    :cond_c
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasAtm()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 630
    const v0, 0x7f090055

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020003

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 631
    :cond_d
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasOpen247()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 632
    const v0, 0x7f090053

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f02009c

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 633
    :cond_e
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasTruckStop()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 634
    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0200d5

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 635
    :cond_f
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->hasCarWash()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 636
    const v0, 0x7f09004c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f020043

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/lang/String;I)V

    .line 637
    :cond_10
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_11

    .line 638
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 639
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f0901bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 640
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 641
    const/high16 v1, 0x4180

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 642
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 643
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->i:Landroid/widget/TableLayout;

    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 645
    :cond_11
    return-void
.end method

.method static synthetic k(Lgbis/gbandroid/activities/station/StationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 703
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->m()V

    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    .line 670
    const v0, 0x7f07015d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 671
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 672
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getRegComments()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090150

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getMidComments()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090151

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getPremComments()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090152

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getDieselComments()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f090153

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 677
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 678
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 679
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 689
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 692
    :goto_1
    return-void

    .line 681
    :cond_0
    const-string v1, ""

    move-object v3, v1

    .line 682
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 687
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 683
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 684
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Lgbis/gbandroid/activities/station/StationDetails;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "\n"

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_2

    :cond_2
    const-string v1, ""

    goto :goto_3

    .line 691
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private m()V
    .locals 2

    .prologue
    .line 704
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->p()V

    .line 705
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    const v0, 0x7f09003d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->showMessage(Ljava/lang/String;)V

    .line 707
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->showLogin()V

    .line 711
    :goto_0
    return-void

    .line 710
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->showCameraDialog(Lgbis/gbandroid/entities/Station;)V

    goto :goto_0
.end method

.method private n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 714
    const v0, 0x7f070151

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 715
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mRes:Landroid/content/res/Resources;

    const v2, 0x7f02009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 716
    const v1, 0x7f070152

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 717
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 718
    new-instance v1, Lgbis/gbandroid/activities/station/al;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/al;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 725
    return-void
.end method

.method private o()V
    .locals 13

    .prologue
    .line 728
    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 729
    const/4 v1, 0x1

    new-array v5, v1, [Landroid/view/ViewGroup;

    .line 730
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getDensity()F

    move-result v6

    .line 731
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getAdsInLine()Ljava/util/List;

    move-result-object v7

    .line 732
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 733
    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-lt v4, v8, :cond_0

    .line 788
    return-void

    .line 734
    :cond_0
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/AdInLine;

    .line 735
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getPosition()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getPosition()I

    move-result v2

    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    .line 736
    new-instance v9, Landroid/widget/LinearLayout;

    invoke-direct {v9, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 737
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getPosition()I

    move-result v2

    invoke-direct {p0, v2, v0, v5}, Lgbis/gbandroid/activities/station/StationDetails;->a(ILandroid/widget/LinearLayout;[Landroid/view/ViewGroup;)I

    move-result v2

    invoke-virtual {v0, v9, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 738
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 739
    const/4 v3, 0x0

    aget-object v3, v5, v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 740
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getHeight()I

    move-result v10

    if-lez v10, :cond_2

    .line 741
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getDensity()F

    move-result v11

    mul-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 744
    :goto_1
    iget v10, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v11, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v12, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v3, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v2, v10, v11, v12, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 745
    new-instance v2, Landroid/webkit/WebView;

    invoke-direct {v2, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 746
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getHtml()Ljava/lang/String;

    move-result-object v3

    const-string v10, "text/html"

    const-string v11, "utf-8"

    invoke-virtual {v2, v3, v10, v11}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 748
    const/high16 v3, 0x4000

    mul-float/2addr v3, v6

    float-to-int v3, v3

    const/high16 v10, 0x4000

    mul-float/2addr v10, v6

    float-to-int v10, v10

    const/high16 v11, 0x4000

    mul-float/2addr v11, v6

    float-to-int v11, v11

    const/high16 v12, 0x4000

    mul-float/2addr v12, v6

    float-to-int v12, v12

    invoke-virtual {v9, v3, v10, v11, v12}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 749
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v3, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 750
    new-instance v3, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v10, Lgbis/gbandroid/activities/station/z;

    invoke-direct {v10, p0, v1, v9}, Lgbis/gbandroid/activities/station/z;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Lgbis/gbandroid/entities/AdInLine;Landroid/view/ViewGroup;)V

    invoke-direct {v3, v10}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 765
    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 766
    invoke-virtual {v1}, Lgbis/gbandroid/entities/AdInLine;->getUrl()Ljava/lang/String;

    move-result-object v3

    const-string v10, ""

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 767
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 768
    new-instance v3, Lgbis/gbandroid/activities/station/aa;

    invoke-direct {v3, p0, v1}, Lgbis/gbandroid/activities/station/aa;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Lgbis/gbandroid/entities/AdInLine;)V

    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 775
    new-instance v3, Lgbis/gbandroid/activities/station/ab;

    invoke-direct {v3, p0, v1}, Lgbis/gbandroid/activities/station/ab;-><init>(Lgbis/gbandroid/activities/station/StationDetails;Lgbis/gbandroid/entities/AdInLine;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 733
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_0

    .line 743
    :cond_2
    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getHeight()I

    move-result v10

    iput v10, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_1
.end method

.method private p()V
    .locals 3

    .prologue
    .line 818
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->d:Ljava/lang/String;

    .line 819
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->e:Ljava/lang/String;

    .line 820
    return-void
.end method

.method private q()V
    .locals 3

    .prologue
    .line 823
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 824
    const-string v1, "station"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 825
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->startActivityForResult(Landroid/content/Intent;I)V

    .line 826
    return-void
.end method

.method private r()V
    .locals 4

    .prologue
    .line 840
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/report/ReportPrices;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-direct {v0, p0, v1, v2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 841
    const-string v1, "station"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 842
    const-string v1, "time offset"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getTimeOffset()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 843
    const-string v1, "fuel regular"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getRegPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 844
    const-string v1, "time spotted regular"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getRegTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 845
    const-string v1, "fuel midgrade"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getMidPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 846
    const-string v1, "time spotted midgrade"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getMidTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 847
    const-string v1, "fuel premium"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getPremPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 848
    const-string v1, "time spotted premium"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getPremTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 849
    const-string v1, "fuel diesel"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getDieselPrice()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 850
    const-string v1, "time spotted diesel"

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getDieselTimeSpotted()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 851
    const-string v1, "favourite id"

    iget v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->g:I

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/content/GBIntent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 852
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->startActivityForResult(Landroid/content/Intent;I)V

    .line 853
    return-void
.end method

.method private s()V
    .locals 6

    .prologue
    .line 856
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v5

    .line 857
    if-eqz v5, :cond_0

    .line 858
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getLatitude()D

    move-result-wide v1

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/StationMessage;->getLongitude()D

    move-result-wide v3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/activities/station/StationDetails;->showDirections(DDLandroid/location/Location;)V

    .line 863
    :goto_0
    return-void

    .line 860
    :cond_0
    const v0, 0x7f090042

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->showMessage(Ljava/lang/String;)V

    .line 861
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->showStationOnMap(Lgbis/gbandroid/entities/Station;)V

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 976
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 977
    new-instance v0, Landroid/location/Location;

    const-string v1, "Station"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 978
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 979
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 980
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->myLocation:Landroid/location/Location;

    invoke-virtual {v1, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v0

    const/high16 v1, 0x447a

    div-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x3ff9bfdf7e8038a0L

    div-double/2addr v0, v2

    .line 981
    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-direct {p0, v2, v0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->a(Lgbis/gbandroid/entities/Station;D)V

    .line 984
    :cond_0
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x3

    const/4 v0, -0x1

    .line 449
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBActivityAds;->onActivityResult(IILandroid/content/Intent;)V

    .line 450
    packed-switch p1, :pswitch_data_0

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 452
    :pswitch_0
    if-eqz p2, :cond_0

    .line 455
    if-ne p2, v0, :cond_0

    .line 456
    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->setResult(I)V

    .line 457
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->finish()V

    goto :goto_0

    .line 461
    :pswitch_1
    if-eqz p2, :cond_0

    .line 464
    if-ne p2, v1, :cond_0

    .line 467
    invoke-virtual {p0, v1, p3}, Lgbis/gbandroid/activities/station/StationDetails;->setResult(ILandroid/content/Intent;)V

    .line 468
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->finish()V

    goto :goto_0

    .line 472
    :pswitch_2
    if-eqz p2, :cond_0

    .line 475
    if-ne p2, v0, :cond_0

    .line 476
    const v0, 0x7f07014e

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/StationMessage;->setIsFav(I)V

    goto :goto_0

    .line 450
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter

    .prologue
    .line 485
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 486
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    if-eqz v0, :cond_0

    .line 487
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->k()V

    .line 488
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 109
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreate(Landroid/os/Bundle;)V

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/activities/station/StationDetails;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 113
    new-instance v0, Lgbis/gbandroid/activities/station/StationDetails$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/activities/station/StationDetails$a;-><init>(Lgbis/gbandroid/activities/station/StationDetails;B)V

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->f:Lgbis/gbandroid/activities/station/StationDetails$a;

    .line 114
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->p()V

    .line 115
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 116
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->openDB()Z

    .line 118
    const v0, 0x7f030053

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->setContentView(I)V

    .line 119
    const-string v0, "station"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    .line 120
    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getStationId()I

    move-result v2

    iput v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->b:I

    .line 121
    const-string v2, "favourite id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->g:I

    .line 122
    invoke-direct {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->a(Lgbis/gbandroid/entities/Station;)V

    .line 123
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->d()V

    .line 126
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->finish()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 385
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 386
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 387
    const v1, 0x7f0b0009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 388
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onDestroy()V

    .line 151
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->cleanImageCache()V

    .line 152
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 154
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->f:Lgbis/gbandroid/activities/station/StationDetails$a;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onImageFailed(Landroid/widget/ImageView;)V
    .locals 2
    .parameter

    .prologue
    .line 1047
    const v0, 0x7f070152

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1048
    new-instance v0, Lgbis/gbandroid/activities/station/ad;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/ad;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1055
    return-void
.end method

.method public onImageLoaded(Landroid/widget/ImageView;)V
    .locals 1
    .parameter

    .prologue
    .line 1036
    new-instance v0, Lgbis/gbandroid/activities/station/ac;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/ac;-><init>(Lgbis/gbandroid/activities/station/StationDetails;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043
    return-void
.end method

.method public onMyLocationChanged(Landroid/location/Location;)V
    .locals 0
    .parameter

    .prologue
    .line 988
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onMyLocationChanged(Landroid/location/Location;)V

    .line 989
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->t()V

    .line 990
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 411
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivityAds;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 412
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 444
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 414
    :pswitch_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->r()V

    goto :goto_0

    .line 417
    :pswitch_1
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->s()V

    goto :goto_0

    .line 420
    :pswitch_2
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->m()V

    goto :goto_0

    .line 423
    :pswitch_3
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->launchSettings()V

    goto :goto_0

    .line 426
    :pswitch_4
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->showCityZipSearch()V

    goto :goto_0

    .line 429
    :pswitch_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7f090175

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 430
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->p()V

    .line 431
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 432
    const v1, 0x7f09003e

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/activities/station/StationDetails;->showMessage(Ljava/lang/String;)V

    .line 435
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->showLogin()V

    goto :goto_0

    .line 438
    :cond_0
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->q()V

    goto :goto_0

    .line 441
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->showFavourites()V

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x7f0701a4
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 144
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onPause()V

    .line 145
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->b()V

    .line 146
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x3

    .line 393
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    if-nez v1, :cond_0

    .line 394
    const/4 v0, 0x0

    .line 406
    :goto_0
    return v0

    .line 396
    :cond_0
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 397
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 398
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->c:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->isIsFav()I

    move-result v1

    if-nez v1, :cond_1

    .line 399
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f090175

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 400
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f020082

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 402
    :cond_1
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f090178

    invoke-virtual {p0, v2}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 403
    invoke-interface {p1, v3}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f02008f

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onResume()V

    .line 131
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->openDB()Z

    .line 132
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationDetails;->p()V

    .line 134
    return-void
.end method

.method public onScrollChanged(Lgbis/gbandroid/views/layouts/CustomScrollView;IIII)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 971
    if-eq p3, p5, :cond_0

    .line 972
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationDetails;->hideAds()V

    .line 973
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivityAds;->onStart()V

    .line 139
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->f:Lgbis/gbandroid/activities/station/StationDetails$a;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 140
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 5

    .prologue
    .line 1022
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "adStation"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1023
    iget-object v1, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "adFavoritesKey"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1024
    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationDetails;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "adFavoritesUnit"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1025
    invoke-virtual {p0, v1, v2, v0}, Lgbis/gbandroid/activities/station/StationDetails;->setAdConfiguration(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1026
    invoke-virtual {p0, p0}, Lgbis/gbandroid/activities/station/StationDetails;->registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V

    .line 1027
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1031
    const v0, 0x7f09024c

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
