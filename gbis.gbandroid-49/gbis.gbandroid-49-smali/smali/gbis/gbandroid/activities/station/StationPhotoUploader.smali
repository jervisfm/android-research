.class public Lgbis/gbandroid/activities/station/StationPhotoUploader;
.super Lgbis/gbandroid/activities/base/PhotoDialog;
.source "GBFile"


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/PhotoDialog;-><init>()V

    return-void
.end method

.method private a()V
    .locals 14

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 80
    new-instance v7, Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-direct {v7, p0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030016

    invoke-virtual {v0, v1, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 82
    const v0, 0x7f070047

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 83
    const v0, 0x7f07004d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030015

    invoke-virtual {v0, v2, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 85
    const v0, 0x7f07004b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 86
    const v0, 0x7f07004c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 87
    const v0, 0x7f07004a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f060010

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 89
    iget-object v8, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->station:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v8}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 90
    invoke-virtual {v3, v11}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 91
    iget-object v8, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->station:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v8}, Lgbis/gbandroid/entities/Station;->getGasBrandId()I

    move-result v8

    const-string v9, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v7, v8, v9}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 92
    const v8, 0x7f0901ac

    invoke-virtual {p0, v8}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const v8, 0x7f0901b5

    invoke-virtual {p0, v8}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {v7, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 95
    const v0, 0x7f090186

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/activities/station/an;

    invoke-direct {v1, p0, v7, v3, v4}, Lgbis/gbandroid/activities/station/an;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Lgbis/gbandroid/views/CustomDialog$Builder;Landroid/widget/ListView;Landroid/widget/EditText;)V

    invoke-virtual {v7, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 106
    new-instance v0, Lgbis/gbandroid/activities/station/ao;

    invoke-direct {v0, p0, v3}, Lgbis/gbandroid/activities/station/ao;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Landroid/widget/ListView;)V

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 114
    new-array v0, v11, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    .line 115
    new-instance v8, Landroid/text/InputFilter$LengthFilter;

    const/16 v9, 0x32

    invoke-direct {v8, v9}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v8, v0, v1

    .line 114
    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 117
    const/4 v0, 0x6

    invoke-virtual {v4, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 118
    new-instance v0, Lgbis/gbandroid/activities/station/ap;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lgbis/gbandroid/activities/station/ap;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Landroid/view/View;Landroid/widget/ListView;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/widget/TextView;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 136
    invoke-virtual {v3, v2, v12, v11}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 137
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030032

    const v2, 0x7f07002c

    invoke-direct {v0, p0, v1, v2, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 138
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 139
    invoke-virtual {v7}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 140
    new-instance v7, Lgbis/gbandroid/activities/station/aq;

    move-object v8, p0

    move-object v9, v3

    move-object v11, v4

    move-object v12, v6

    move-object v13, v5

    invoke-direct/range {v7 .. v13}, Lgbis/gbandroid/activities/station/aq;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Landroid/widget/ListView;[Ljava/lang/String;Landroid/widget/EditText;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 152
    new-instance v1, Lgbis/gbandroid/activities/station/ar;

    invoke-direct {v1, p0}, Lgbis/gbandroid/activities/station/ar;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 158
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 159
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationPhotoUploader;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->uploadPhoto()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/activities/station/StationPhotoUploader;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected getIntentExtras()V
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    const-string v1, "station"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->station:Lgbis/gbandroid/entities/Station;

    .line 54
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->finish()V

    goto :goto_0
.end method

.method protected postPhotoResized()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a()V

    .line 58
    return-void
.end method

.method protected postPhotoTaken()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 62
    invoke-virtual {p0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->latitude:D

    .line 65
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->longitude:D

    .line 70
    :goto_0
    return-void

    .line 67
    :cond_0
    iput-wide v1, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->latitude:D

    .line 68
    iput-wide v1, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->longitude:D

    goto :goto_0
.end method

.method protected queryPhotoUploaderWebService([B)V
    .locals 4
    .parameter

    .prologue
    .line 74
    new-instance v0, Lgbis/gbandroid/activities/station/am;

    invoke-direct {v0, p0}, Lgbis/gbandroid/activities/station/am;-><init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;)V

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/am;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 75
    new-instance v1, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->myLocation:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 76
    iget-object v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->station:Lgbis/gbandroid/entities/Station;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Station;->getStationId()I

    move-result v0

    iget-object v2, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a:Ljava/lang/String;

    invoke-static {p1}, Lgbis/gbandroid/utils/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->getResponseObject(ILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/activities/station/StationPhotoUploader;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 77
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 164
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    const v0, 0x7f09024f

    invoke-virtual {p0, v0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
