.class final Lgbis/gbandroid/activities/station/an;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

.field private final synthetic b:Lgbis/gbandroid/views/CustomDialog$Builder;

.field private final synthetic c:Landroid/widget/ListView;

.field private final synthetic d:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Lgbis/gbandroid/views/CustomDialog$Builder;Landroid/widget/ListView;Landroid/widget/EditText;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/an;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/an;->b:Lgbis/gbandroid/views/CustomDialog$Builder;

    iput-object p3, p0, Lgbis/gbandroid/activities/station/an;->c:Landroid/widget/ListView;

    iput-object p4, p0, Lgbis/gbandroid/activities/station/an;->d:Landroid/widget/EditText;

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lgbis/gbandroid/activities/station/an;->b:Lgbis/gbandroid/views/CustomDialog$Builder;

    invoke-virtual {v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->getPositiveButton()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 99
    iget-object v0, p0, Lgbis/gbandroid/activities/station/an;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 100
    iget-object v0, p0, Lgbis/gbandroid/activities/station/an;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/an;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a(Lgbis/gbandroid/activities/station/StationPhotoUploader;Ljava/lang/String;)V

    .line 101
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/an;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a(Lgbis/gbandroid/activities/station/StationPhotoUploader;)V

    .line 102
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 103
    iget-object v0, p0, Lgbis/gbandroid/activities/station/an;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->finish()V

    .line 104
    return-void
.end method
