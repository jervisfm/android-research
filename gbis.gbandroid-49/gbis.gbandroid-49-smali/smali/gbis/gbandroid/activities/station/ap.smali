.class final Lgbis/gbandroid/activities/station/ap;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Landroid/widget/ListView;

.field private final synthetic d:Landroid/widget/EditText;

.field private final synthetic e:Landroid/widget/CheckBox;

.field private final synthetic f:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Landroid/view/View;Landroid/widget/ListView;Landroid/widget/EditText;Landroid/widget/CheckBox;Landroid/widget/TextView;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/ap;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/ap;->b:Landroid/view/View;

    iput-object p3, p0, Lgbis/gbandroid/activities/station/ap;->c:Landroid/widget/ListView;

    iput-object p4, p0, Lgbis/gbandroid/activities/station/ap;->d:Landroid/widget/EditText;

    iput-object p5, p0, Lgbis/gbandroid/activities/station/ap;->e:Landroid/widget/CheckBox;

    iput-object p6, p0, Lgbis/gbandroid/activities/station/ap;->f:Landroid/widget/TextView;

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 121
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocusFromTouch()Z

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 124
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/ap;->c:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->c:Landroid/widget/ListView;

    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDescendantFocusability(I)V

    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 130
    iget-object v0, p0, Lgbis/gbandroid/activities/station/ap;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/activities/station/ap;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 133
    :cond_0
    return v2
.end method
