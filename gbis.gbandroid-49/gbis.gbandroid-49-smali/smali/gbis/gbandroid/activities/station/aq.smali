.class final Lgbis/gbandroid/activities/station/aq;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

.field private final synthetic b:Landroid/widget/ListView;

.field private final synthetic c:[Ljava/lang/String;

.field private final synthetic d:Landroid/widget/EditText;

.field private final synthetic e:Landroid/widget/TextView;

.field private final synthetic f:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/StationPhotoUploader;Landroid/widget/ListView;[Ljava/lang/String;Landroid/widget/EditText;Landroid/widget/TextView;Landroid/widget/CheckBox;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/aq;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/aq;->b:Landroid/widget/ListView;

    iput-object p3, p0, Lgbis/gbandroid/activities/station/aq;->c:[Ljava/lang/String;

    iput-object p4, p0, Lgbis/gbandroid/activities/station/aq;->d:Landroid/widget/EditText;

    iput-object p5, p0, Lgbis/gbandroid/activities/station/aq;->e:Landroid/widget/TextView;

    iput-object p6, p0, Lgbis/gbandroid/activities/station/aq;->f:Landroid/widget/CheckBox;

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->b:Landroid/widget/ListView;

    const/high16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDescendantFocusability(I)V

    .line 143
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/aq;->c:[Ljava/lang/String;

    aget-object v1, v1, p3

    invoke-static {v0, v1}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->a(Lgbis/gbandroid/activities/station/StationPhotoUploader;Ljava/lang/String;)V

    .line 144
    check-cast p2, Landroid/widget/CheckedTextView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->a:Lgbis/gbandroid/activities/station/StationPhotoUploader;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/StationPhotoUploader;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 146
    iget-object v1, p0, Lgbis/gbandroid/activities/station/aq;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 147
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->d:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lgbis/gbandroid/activities/station/aq;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 150
    return-void
.end method
