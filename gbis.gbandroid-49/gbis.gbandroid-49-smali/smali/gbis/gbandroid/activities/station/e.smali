.class final Lgbis/gbandroid/activities/station/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;

.field private final synthetic b:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;Landroid/widget/ListView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/e;->b:Landroid/widget/ListView;

    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 538
    iget-object v0, p0, Lgbis/gbandroid/activities/station/e;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 539
    iget-object v0, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->n(Lgbis/gbandroid/activities/station/AddStationMap;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/e;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/StationMessage;

    .line 540
    iget-object v1, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/station/AddStationMap;->a(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/entities/StationMessage;)V

    .line 541
    iget-object v0, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->finish()V

    .line 544
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/e;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/station/AddStationMap;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
