.class final Lgbis/gbandroid/activities/station/l;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/l;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter

    .prologue
    .line 311
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 315
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 318
    const-string v0, ".*\\p{Digit}.*"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 321
    new-instance v0, Lgbis/gbandroid/activities/station/AddStationMap$c;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/l;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    iget-object v2, p0, Lgbis/gbandroid/activities/station/l;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v2}, Lgbis/gbandroid/activities/station/AddStationMap;->v(Lgbis/gbandroid/activities/station/AddStationMap;)Lgbis/gbandroid/activities/base/GBActivityMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/activities/station/AddStationMap$c;-><init>(Lgbis/gbandroid/activities/station/AddStationMap;Lgbis/gbandroid/activities/base/GBActivityMap;)V

    .line 322
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/AddStationMap$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 325
    iget-object v0, p0, Lgbis/gbandroid/activities/station/l;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->e(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method
