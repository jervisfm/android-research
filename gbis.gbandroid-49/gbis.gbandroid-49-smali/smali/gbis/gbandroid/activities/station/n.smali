.class final Lgbis/gbandroid/activities/station/n;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/AddStationMap;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/AddStationMap;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/n;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    .line 345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter

    .prologue
    .line 348
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 351
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 354
    const-string v0, ".*\\p{Digit}.*"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 357
    iget-object v0, p0, Lgbis/gbandroid/activities/station/n;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->x(Lgbis/gbandroid/activities/station/AddStationMap;)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 360
    iget-object v0, p0, Lgbis/gbandroid/activities/station/n;->a:Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/AddStationMap;->c(Lgbis/gbandroid/activities/station/AddStationMap;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method
