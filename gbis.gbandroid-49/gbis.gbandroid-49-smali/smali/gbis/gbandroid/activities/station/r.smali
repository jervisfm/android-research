.class final Lgbis/gbandroid/activities/station/r;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/FavAddStation;

.field private final synthetic b:Landroid/widget/TextView;

.field private final synthetic c:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/FavAddStation;Landroid/widget/TextView;Landroid/widget/CheckBox;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/r;->b:Landroid/widget/TextView;

    iput-object p3, p0, Lgbis/gbandroid/activities/station/r;->c:Landroid/widget/CheckBox;

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 124
    iget-object v1, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->f(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->a(Lgbis/gbandroid/activities/station/FavAddStation;Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->h(Lgbis/gbandroid/activities/station/FavAddStation;)Ljava/util/List;

    move-result-object v0

    add-int/lit8 v2, p3, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListId()I

    move-result v0

    invoke-static {v1, v0}, Lgbis/gbandroid/activities/station/FavAddStation;->a(Lgbis/gbandroid/activities/station/FavAddStation;I)V

    .line 126
    check-cast p2, Landroid/widget/CheckedTextView;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/station/FavAddStation;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/FavAddStation;->g(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 129
    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->a:Lgbis/gbandroid/activities/station/FavAddStation;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/FavAddStation;->g(Lgbis/gbandroid/activities/station/FavAddStation;)Landroid/widget/EditText;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lgbis/gbandroid/activities/station/r;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 132
    return-void
.end method
