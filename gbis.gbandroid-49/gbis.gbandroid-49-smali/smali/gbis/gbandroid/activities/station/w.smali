.class final Lgbis/gbandroid/activities/station/w;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/PictureView;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/PictureView;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    .line 127
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 130
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/PictureView;->a(Lgbis/gbandroid/activities/station/PictureView;)I

    move-result v0

    if-lez v0, :cond_1

    .line 131
    iget-object v0, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/PictureView;->b(Lgbis/gbandroid/activities/station/PictureView;)V

    .line 134
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 132
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/PictureView;->a(Lgbis/gbandroid/activities/station/PictureView;)I

    move-result v0

    iget-object v1, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    invoke-static {v1}, Lgbis/gbandroid/activities/station/PictureView;->c(Lgbis/gbandroid/activities/station/PictureView;)Lgbis/gbandroid/entities/StationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lgbis/gbandroid/entities/StationMessage;->getStationPhotos()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 133
    iget-object v0, p0, Lgbis/gbandroid/activities/station/w;->a:Lgbis/gbandroid/activities/station/PictureView;

    invoke-static {v0}, Lgbis/gbandroid/activities/station/PictureView;->d(Lgbis/gbandroid/activities/station/PictureView;)V

    goto :goto_0
.end method
