.class final Lgbis/gbandroid/activities/station/z;
.super Landroid/graphics/drawable/shapes/Shape;
.source "GBFile"


# instance fields
.field final synthetic a:Lgbis/gbandroid/activities/station/StationDetails;

.field private final synthetic b:Lgbis/gbandroid/entities/AdInLine;

.field private final synthetic c:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lgbis/gbandroid/activities/station/StationDetails;Lgbis/gbandroid/entities/AdInLine;Landroid/view/ViewGroup;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/activities/station/z;->a:Lgbis/gbandroid/activities/station/StationDetails;

    iput-object p2, p0, Lgbis/gbandroid/activities/station/z;->b:Lgbis/gbandroid/entities/AdInLine;

    iput-object p3, p0, Lgbis/gbandroid/activities/station/z;->c:Landroid/view/ViewGroup;

    .line 750
    invoke-direct {p0}, Landroid/graphics/drawable/shapes/Shape;-><init>()V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4080

    const/high16 v3, 0x4000

    .line 753
    iget-object v0, p0, Lgbis/gbandroid/activities/station/z;->b:Lgbis/gbandroid/entities/AdInLine;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AdInLine;->getStrokeColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 754
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 755
    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 756
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 757
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/z;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    iget-object v2, p0, Lgbis/gbandroid/activities/station/z;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 758
    invoke-virtual {p1, v0, v4, v4, p2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 759
    iget-object v0, p0, Lgbis/gbandroid/activities/station/z;->b:Lgbis/gbandroid/entities/AdInLine;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/AdInLine;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 760
    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 761
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lgbis/gbandroid/activities/station/z;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    int-to-float v1, v1

    iget-object v2, p0, Lgbis/gbandroid/activities/station/z;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x4

    int-to-float v2, v2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 762
    invoke-virtual {p1, v0, v3, v3, p2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 763
    return-void
.end method
