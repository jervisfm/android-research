.class public Lgbis/gbandroid/animations/MyScaleAnimation;
.super Landroid/view/animation/ScaleAnimation;
.source "GBFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/ViewGroup$LayoutParams;

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(FFFFILandroid/view/View;Z)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 16
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->c:Z

    .line 17
    int-to-long v0, p5

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/animations/MyScaleAnimation;->setDuration(J)V

    .line 18
    iput-object p6, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    .line 19
    iput-boolean p7, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->c:Z

    .line 20
    invoke-virtual {p6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    .line 21
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    iput v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->d:I

    .line 22
    cmpl-float v0, p3, p4

    if-lez v0, :cond_1

    .line 23
    iput-boolean v2, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->e:Z

    .line 26
    :goto_1
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 25
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x3f80

    .line 30
    invoke-super {p0, p1, p2}, Landroid/view/animation/ScaleAnimation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 31
    cmpg-float v0, p1, v2

    if-gez v0, :cond_2

    .line 32
    iget-boolean v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->e:Z

    if-eqz v0, :cond_1

    .line 33
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->d:I

    int-to-float v1, v1

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 36
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->requestLayout()V

    .line 41
    :cond_0
    :goto_1
    return-void

    .line 35
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->b:Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->d:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 39
    :cond_2
    iget-boolean v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lgbis/gbandroid/animations/MyScaleAnimation;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
