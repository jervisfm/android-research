.class public Lgbis/gbandroid/entities/Ad;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private networkId:I

.field private networkKey:Ljava/lang/String;

.field private unitId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNetworkId()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lgbis/gbandroid/entities/Ad;->networkId:I

    return v0
.end method

.method public getNetworkKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/Ad;->networkKey:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/Ad;->unitId:Ljava/lang/String;

    return-object v0
.end method

.method public setNetworkId(I)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput p1, p0, Lgbis/gbandroid/entities/Ad;->networkId:I

    .line 14
    return-void
.end method

.method public setNetworkKey(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/Ad;->networkKey:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setUnitId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/Ad;->unitId:Ljava/lang/String;

    .line 30
    return-void
.end method
