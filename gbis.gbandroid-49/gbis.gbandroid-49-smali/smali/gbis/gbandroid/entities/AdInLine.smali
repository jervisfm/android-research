.class public Lgbis/gbandroid/entities/AdInLine;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/AdInLine;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private backgroundColor:Ljava/lang/String;

.field private height:I

.field private html:Ljava/lang/String;

.field private position:I

.field private strokeColor:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "BorderColor"
    .end annotation
.end field

.field private url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lgbis/gbandroid/entities/a;

    invoke-direct {v0}, Lgbis/gbandroid/entities/a;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/AdInLine;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/AdInLine;->readFromParcel(Landroid/os/Parcel;)V

    .line 105
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/AdInLine;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    .line 136
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    .line 139
    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/AdInLine;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lgbis/gbandroid/entities/AdInLine;

    invoke-direct {v0}, Lgbis/gbandroid/entities/AdInLine;-><init>()V

    .line 67
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    .line 68
    iget v1, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    iput v1, v0, Lgbis/gbandroid/entities/AdInLine;->height:I

    .line 69
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    .line 70
    iget v1, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    iput v1, v0, Lgbis/gbandroid/entities/AdInLine;->position:I

    .line 71
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    .line 73
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    return v0
.end method

.method public getHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    return v0
.end method

.method public getStrokeColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setBackgroundColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput p1, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    .line 59
    return-void
.end method

.method public setHtml(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput p1, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    .line 55
    return-void
.end method

.method public setStrokeColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    const-string v1, "HTML: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 82
    const-string v1, "Bck Color: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 85
    const-string v1, "Stroke Color: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    const-string v1, "URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v1, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 91
    const-string v1, "Height: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    iget v1, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    const-string v1, "Position: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget v1, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->backgroundColor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->strokeColor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/entities/AdInLine;->html:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    iget v0, p0, Lgbis/gbandroid/entities/AdInLine;->position:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget v0, p0, Lgbis/gbandroid/entities/AdInLine;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    return-void
.end method
