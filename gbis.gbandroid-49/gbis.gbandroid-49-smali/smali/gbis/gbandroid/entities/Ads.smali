.class public Lgbis/gbandroid/entities/Ads;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private details:Lgbis/gbandroid/entities/Ad;

.field private favorites:Lgbis/gbandroid/entities/Ad;

.field private home:Lgbis/gbandroid/entities/Ad;

.field private init:Lgbis/gbandroid/entities/Ad;

.field private initTime:I

.field private list:Lgbis/gbandroid/entities/Ad;

.field private listBottom:Lgbis/gbandroid/entities/Ad;

.field private listScrollTime:I

.field private listTop:Lgbis/gbandroid/entities/Ad;

.field private listWaitTime:I

.field private profile:Lgbis/gbandroid/entities/Ad;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDetails()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->details:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getFavorites()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->favorites:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getHome()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->home:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getInit()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->init:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getInitTime()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lgbis/gbandroid/entities/Ads;->initTime:I

    return v0
.end method

.method public getList()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->list:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getListBottom()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->listBottom:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getListScrollTime()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lgbis/gbandroid/entities/Ads;->listScrollTime:I

    return v0
.end method

.method public getListTop()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->listTop:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public getListWaitTime()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lgbis/gbandroid/entities/Ads;->listWaitTime:I

    return v0
.end method

.method public getProfile()Lgbis/gbandroid/entities/Ad;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/entities/Ads;->profile:Lgbis/gbandroid/entities/Ad;

    return-object v0
.end method

.method public setDetails(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->details:Lgbis/gbandroid/entities/Ad;

    .line 62
    return-void
.end method

.method public setFavorites(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->favorites:Lgbis/gbandroid/entities/Ad;

    .line 70
    return-void
.end method

.method public setHome(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->home:Lgbis/gbandroid/entities/Ad;

    .line 30
    return-void
.end method

.method public setInit(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->init:Lgbis/gbandroid/entities/Ad;

    .line 22
    return-void
.end method

.method public setInitTime(I)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput p1, p0, Lgbis/gbandroid/entities/Ads;->initTime:I

    .line 82
    return-void
.end method

.method public setList(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->list:Lgbis/gbandroid/entities/Ad;

    .line 38
    return-void
.end method

.method public setListBottom(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->listBottom:Lgbis/gbandroid/entities/Ad;

    .line 54
    return-void
.end method

.method public setListScrollTime(I)V
    .locals 0
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lgbis/gbandroid/entities/Ads;->listScrollTime:I

    .line 94
    return-void
.end method

.method public setListTop(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->listTop:Lgbis/gbandroid/entities/Ad;

    .line 46
    return-void
.end method

.method public setListWaitTime(I)V
    .locals 0
    .parameter

    .prologue
    .line 101
    iput p1, p0, Lgbis/gbandroid/entities/Ads;->listWaitTime:I

    .line 102
    return-void
.end method

.method public setProfile(Lgbis/gbandroid/entities/Ad;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lgbis/gbandroid/entities/Ads;->profile:Lgbis/gbandroid/entities/Ad;

    .line 78
    return-void
.end method
