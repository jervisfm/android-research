.class public Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/entities/AdsGSA;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BackgroundGradient"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/entities/AdsGSA;

.field private fromColor:Ljava/lang/String;

.field private toColor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/entities/AdsGSA;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->a:Lgbis/gbandroid/entities/AdsGSA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFromColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->fromColor:Ljava/lang/String;

    return-object v0
.end method

.method public getToColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->toColor:Ljava/lang/String;

    return-object v0
.end method

.method public setFromColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->fromColor:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setToColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 121
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;->toColor:Ljava/lang/String;

    .line 122
    return-void
.end method
