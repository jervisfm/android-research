.class public Lgbis/gbandroid/entities/AdsGSA;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;
    }
.end annotation


# instance fields
.field private anchorTextColor:Ljava/lang/String;

.field private backgroundColor:Ljava/lang/String;

.field private backgroundGradient:Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;

.field private borderColor:Ljava/lang/String;

.field private borderThickness:I

.field private borderType:I

.field private descriptionTextColor:Ljava/lang/String;

.field private fontFace:Ljava/lang/String;

.field private headerTextColor:Ljava/lang/String;

.field private headerTextSize:I

.field private keywords:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnchorTextColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->anchorTextColor:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->backgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getBackgroundGradient()Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->backgroundGradient:Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;

    return-object v0
.end method

.method public getBorderColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->borderColor:Ljava/lang/String;

    return-object v0
.end method

.method public getBorderThickness()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lgbis/gbandroid/entities/AdsGSA;->borderThickness:I

    return v0
.end method

.method public getBorderType()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lgbis/gbandroid/entities/AdsGSA;->borderType:I

    return v0
.end method

.method public getDescriptionTextColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->descriptionTextColor:Ljava/lang/String;

    return-object v0
.end method

.method public getFontFace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->fontFace:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderTextColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->headerTextColor:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderTextSize()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lgbis/gbandroid/entities/AdsGSA;->headerTextSize:I

    return v0
.end method

.method public getKeywords()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lgbis/gbandroid/entities/AdsGSA;->keywords:Ljava/lang/String;

    return-object v0
.end method

.method public setAnchorTextColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->anchorTextColor:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setBackgroundColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->backgroundColor:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setBackgroundGradient(Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->backgroundGradient:Lgbis/gbandroid/entities/AdsGSA$BackgroundGradient;

    .line 38
    return-void
.end method

.method public setBorderColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->borderColor:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setBorderThickness(I)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lgbis/gbandroid/entities/AdsGSA;->borderThickness:I

    .line 54
    return-void
.end method

.method public setBorderType(I)V
    .locals 0
    .parameter

    .prologue
    .line 61
    iput p1, p0, Lgbis/gbandroid/entities/AdsGSA;->borderType:I

    .line 62
    return-void
.end method

.method public setDescriptionTextColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->descriptionTextColor:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setFontFace(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->fontFace:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setHeaderTextColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->headerTextColor:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setHeaderTextSize(I)V
    .locals 0
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lgbis/gbandroid/entities/AdsGSA;->headerTextSize:I

    .line 94
    return-void
.end method

.method public setKeywords(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lgbis/gbandroid/entities/AdsGSA;->keywords:Ljava/lang/String;

    .line 102
    return-void
.end method
