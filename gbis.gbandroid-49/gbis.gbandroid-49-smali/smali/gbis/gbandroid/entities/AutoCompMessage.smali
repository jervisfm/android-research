.class public Lgbis/gbandroid/entities/AutoCompMessage;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/AutoCompMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private city:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "c"
    .end annotation
.end field

.field private state:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "s"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/AutoCompMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/AutoCompMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/AutoCompMessage;->compareTo(Lgbis/gbandroid/entities/AutoCompMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/AutoCompMessage;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lgbis/gbandroid/entities/AutoCompMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/AutoCompMessage;-><init>()V

    .line 29
    iget-object v1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AutoCompMessage;->city:Ljava/lang/String;

    .line 30
    iget-object v1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AutoCompMessage;->state:Ljava/lang/String;

    .line 31
    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lgbis/gbandroid/entities/AutoCompMessage;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/AutoCompMessage;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    iput-object p1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->city:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->state:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    iget-object v1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    iget-object v1, p0, Lgbis/gbandroid/entities/AutoCompMessage;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
