.class public Lgbis/gbandroid/entities/AutocompletePlaces;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private icon:Landroid/graphics/drawable/Drawable;

.field private isPlace:Z

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->text:Ljava/lang/String;

    .line 17
    iput-boolean p3, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->isPlace:Z

    .line 18
    invoke-virtual {p0, p2}, Lgbis/gbandroid/entities/AutocompletePlaces;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/AutocompletePlaces;->setText(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0, p2}, Lgbis/gbandroid/entities/AutocompletePlaces;->setPlace(Z)V

    .line 13
    return-void
.end method


# virtual methods
.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->icon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->text:Ljava/lang/String;

    return-object v0
.end method

.method public isPlace()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->isPlace:Z

    return v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->icon:Landroid/graphics/drawable/Drawable;

    .line 39
    return-void
.end method

.method public setPlace(Z)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-boolean p1, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->isPlace:Z

    .line 31
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->text:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/entities/AutocompletePlaces;->text:Ljava/lang/String;

    return-object v0
.end method
