.class public Lgbis/gbandroid/entities/AwardsMessage;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/AwardsMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private actionCount:I

.field private addDate:Ljava/lang/String;

.field private awardId:I

.field private awarded:I

.field private color:Ljava/lang/String;

.field private completedMessage:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private difference:I

.field private level:I

.field private message:Ljava/lang/String;

.field private milestoneId:I

.field private title:Ljava/lang/String;

.field private totalTimes:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lgbis/gbandroid/entities/b;

    invoke-direct {v0}, Lgbis/gbandroid/entities/b;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/AwardsMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/AwardsMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 179
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/AwardsMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/AwardsMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/AwardsMessage;->compareTo(Lgbis/gbandroid/entities/AwardsMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/AwardsMessage;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Lgbis/gbandroid/entities/AwardsMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/AwardsMessage;-><init>()V

    .line 127
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    .line 129
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    .line 130
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    .line 134
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    .line 135
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    .line 136
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    .line 137
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    .line 138
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    iput v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    .line 139
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    .line 140
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public getActionCount()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    return v0
.end method

.method public getAddDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    return-object v0
.end method

.method public getAwardId()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    return v0
.end method

.method public getAwarded()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    return v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getCompletedMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDifference()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    return v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getMilestoneId()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalTimes()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    .line 214
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    .line 216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    .line 218
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    .line 227
    return-void
.end method

.method public setActionCount(I)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    .line 75
    return-void
.end method

.method public setAddDate(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setAwardId(I)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    .line 47
    return-void
.end method

.method public setAwarded(I)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    .line 107
    return-void
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setCompletedMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setDifference(I)V
    .locals 0
    .parameter

    .prologue
    .line 122
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    .line 123
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    .line 35
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setMilestoneId(I)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    .line 55
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setTotalTimes(I)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput p1, p0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    .line 83
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    const-string v1, "Title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 149
    const-string v1, "Level: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    const-string v1, "Award Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155
    const-string v1, "Milestone Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    const-string v1, "Description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    const-string v1, "Message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    const-string v1, "Add Date: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    iget-object v1, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 199
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->level:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->color:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awardId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->milestoneId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 204
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->actionCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->totalTimes:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->addDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->awarded:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    iget-object v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->completedMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    iget v0, p0, Lgbis/gbandroid/entities/AwardsMessage;->difference:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    return-void
.end method
