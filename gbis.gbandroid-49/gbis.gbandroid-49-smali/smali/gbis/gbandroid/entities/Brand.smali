.class public Lgbis/gbandroid/entities/Brand;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private gasBrandId:I

.field private gasBrandVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getGasBrandId()I
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lgbis/gbandroid/entities/Brand;->gasBrandId:I

    return v0
.end method

.method public getGasBrandVersion()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lgbis/gbandroid/entities/Brand;->gasBrandVersion:I

    return v0
.end method

.method public setGasBrandId(I)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput p1, p0, Lgbis/gbandroid/entities/Brand;->gasBrandId:I

    .line 13
    return-void
.end method

.method public setGasBrandVersion(I)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput p1, p0, Lgbis/gbandroid/entities/Brand;->gasBrandVersion:I

    .line 21
    return-void
.end method
