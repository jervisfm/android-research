.class public Lgbis/gbandroid/entities/BrandAutoCompMessage;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/BrandAutoCompMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private brand:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "n"
    .end annotation
.end field

.field private count:I
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "c"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/BrandAutoCompMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/BrandAutoCompMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/BrandAutoCompMessage;->compareTo(Lgbis/gbandroid/entities/BrandAutoCompMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/BrandAutoCompMessage;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lgbis/gbandroid/entities/BrandAutoCompMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/BrandAutoCompMessage;-><init>()V

    .line 30
    iget-object v1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->brand:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->brand:Ljava/lang/String;

    .line 31
    iget v1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->count:I

    iput v1, v0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->count:I

    .line 32
    return-object v0
.end method

.method public getBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->brand:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->count:I

    return v0
.end method

.method public setBrand(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    iput-object p1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->brand:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public setCount(I)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput p1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->count:I

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    const-string v1, "Brand: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    iget-object v1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 41
    const-string v1, "Count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    iget v1, p0, Lgbis/gbandroid/entities/BrandAutoCompMessage;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
