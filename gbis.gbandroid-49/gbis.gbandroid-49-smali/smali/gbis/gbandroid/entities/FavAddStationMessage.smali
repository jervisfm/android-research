.class public Lgbis/gbandroid/entities/FavAddStationMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private listId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/FavAddStationMessage;
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lgbis/gbandroid/entities/FavAddStationMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/FavAddStationMessage;-><init>()V

    .line 18
    iget v1, p0, Lgbis/gbandroid/entities/FavAddStationMessage;->listId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavAddStationMessage;->listId:I

    .line 19
    return-object v0
.end method

.method public getLisId()I
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lgbis/gbandroid/entities/FavAddStationMessage;->listId:I

    return v0
.end method

.method public setListId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 12
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavAddStationMessage;->listId:I

    .line 13
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    const-string v1, "List Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    iget v1, p0, Lgbis/gbandroid/entities/FavAddStationMessage;->listId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
