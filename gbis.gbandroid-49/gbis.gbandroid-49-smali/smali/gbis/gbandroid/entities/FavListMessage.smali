.class public Lgbis/gbandroid/entities/FavListMessage;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/FavListMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultYn:Z

.field private listId:I

.field private listName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/FavListMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/FavListMessage;->compareTo(Lgbis/gbandroid/entities/FavListMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/FavListMessage;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/FavListMessage;-><init>()V

    .line 35
    iget-boolean v1, p0, Lgbis/gbandroid/entities/FavListMessage;->defaultYn:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/FavListMessage;->defaultYn:Z

    .line 36
    iget v1, p0, Lgbis/gbandroid/entities/FavListMessage;->listId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavListMessage;->listId:I

    .line 37
    iget-object v1, p0, Lgbis/gbandroid/entities/FavListMessage;->listName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavListMessage;->listName:Ljava/lang/String;

    .line 38
    return-object v0
.end method

.method public getListId()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lgbis/gbandroid/entities/FavListMessage;->listId:I

    return v0
.end method

.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgbis/gbandroid/entities/FavListMessage;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultYn()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lgbis/gbandroid/entities/FavListMessage;->defaultYn:Z

    return v0
.end method

.method public setDefaultYn(Z)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-boolean p1, p0, Lgbis/gbandroid/entities/FavListMessage;->defaultYn:Z

    .line 31
    return-void
.end method

.method public setListId(I)V
    .locals 0
    .parameter

    .prologue
    .line 14
    iput p1, p0, Lgbis/gbandroid/entities/FavListMessage;->listId:I

    .line 15
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/FavListMessage;->listName:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    const-string v1, "List Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    iget v1, p0, Lgbis/gbandroid/entities/FavListMessage;->listId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    const-string v1, "List Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget-object v1, p0, Lgbis/gbandroid/entities/FavListMessage;->listName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 50
    const-string v1, "Default Yn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget-boolean v1, p0, Lgbis/gbandroid/entities/FavListMessage;->defaultYn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
