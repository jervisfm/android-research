.class public Lgbis/gbandroid/entities/FavReportMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private carIcon:Ljava/lang/String;

.field private carIconId:I

.field private maxPriceId:Ljava/math/BigInteger;

.field private minPriceId:Ljava/math/BigInteger;

.field private pointBalance:I

.field private totalPointsAwarded:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/FavReportMessage;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lgbis/gbandroid/entities/FavReportMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/FavReportMessage;-><init>()V

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIcon:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->carIcon:Ljava/lang/String;

    .line 64
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIconId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->carIconId:I

    .line 65
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->minPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->minPriceId:Ljava/math/BigInteger;

    .line 66
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->maxPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->maxPriceId:Ljava/math/BigInteger;

    .line 67
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->totalPointsAwarded:I

    iput v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->totalPointsAwarded:I

    .line 68
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->pointBalance:I

    iput v1, v0, Lgbis/gbandroid/entities/FavReportMessage;->pointBalance:I

    .line 69
    return-object v0
.end method

.method public getCarIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIconId:I

    return v0
.end method

.method public getMaxPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->maxPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getMinPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->minPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPointBalance()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->pointBalance:I

    return v0
.end method

.method public getTotalPointsAwarded()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->totalPointsAwarded:I

    return v0
.end method

.method public setCarIcon(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIcon:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCarIconId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIconId:I

    .line 59
    return-void
.end method

.method public setMaxPriceId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 26
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, p1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->maxPriceId:Ljava/math/BigInteger;

    .line 27
    return-void
.end method

.method public setMinPriceId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 18
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, p1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->minPriceId:Ljava/math/BigInteger;

    .line 19
    return-void
.end method

.method public setPointBalance(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 38
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->pointBalance:I

    .line 39
    return-void
.end method

.method public setTotalPointsAwarded(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavReportMessage;->totalPointsAwarded:I

    .line 31
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    const-string v1, "Min Price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->minPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, "Midgrade Price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->maxPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "Total Points Awarded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->totalPointsAwarded:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, "Point Balance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->pointBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, "Car Icon: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIcon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, "Car Icon Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget v1, p0, Lgbis/gbandroid/entities/FavReportMessage;->carIconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
