.class public Lgbis/gbandroid/entities/FavStationMessage;
.super Lgbis/gbandroid/entities/StationDetailed;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgbis/gbandroid/entities/StationDetailed;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/FavStationMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private comments:Ljava/lang/String;

.field private diffFromAvg:D

.field private listId:I

.field private memberFavId:I

.field private mslMatchStatus:I
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "MSLMatchStatus"
    .end annotation
.end field

.field private siteAvg:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lgbis/gbandroid/entities/e;

    invoke-direct {v0}, Lgbis/gbandroid/entities/e;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/FavStationMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lgbis/gbandroid/entities/StationDetailed;-><init>()V

    .line 140
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lgbis/gbandroid/entities/StationDetailed;-><init>()V

    .line 143
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/FavStationMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 144
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/FavStationMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/FavStationMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/FavStationMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/FavStationMessage;->compareTo(Lgbis/gbandroid/entities/FavStationMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/FavStationMessage;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lgbis/gbandroid/entities/FavStationMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/FavStationMessage;-><init>()V

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->address:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->address:Ljava/lang/String;

    .line 69
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->city:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->country:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->country:Ljava/lang/String;

    .line 72
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->dieselPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->dieselPrice:D

    .line 73
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->dieselTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->dieselTimeSpotted:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->dieselMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->dieselMemberId:Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->dieselCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->dieselCar:Ljava/lang/String;

    .line 76
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    .line 77
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->gasBrandId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->gasBrandId:I

    .line 78
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->gasBrandVersion:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->gasBrandVersion:I

    .line 79
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->latitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->latitude:D

    .line 80
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    .line 81
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->longitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->longitude:D

    .line 82
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    .line 83
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->midPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->midPrice:D

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->midTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->midTimeSpotted:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->midMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->midMemberId:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->midCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->midCar:Ljava/lang/String;

    .line 87
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    .line 88
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->premPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->premPrice:D

    .line 89
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->premTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->premTimeSpotted:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->premMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->premMemberId:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->premCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->premCar:Ljava/lang/String;

    .line 92
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->regPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->regPrice:D

    .line 93
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->regTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->regTimeSpotted:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->regMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->regMemberId:Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->regCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->regCar:Ljava/lang/String;

    .line 96
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    .line 97
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->state:Ljava/lang/String;

    .line 98
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->stationId:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->stationId:I

    .line 99
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->stationName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->stationName:Ljava/lang/String;

    .line 100
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->timeOffset:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationMessage;->timeOffset:I

    .line 101
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    return-object v0
.end method

.method public getDiffFromAvg()D
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    return-wide v0
.end method

.method public getListId()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    return v0
.end method

.method public getMemberFavId()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    return v0
.end method

.method public getMslMatchStatus()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    return v0
.end method

.method public getSiteAvg()D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    return-wide v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 173
    invoke-super {p0, p1}, Lgbis/gbandroid/entities/StationDetailed;->readFromParcel(Landroid/os/Parcel;)V

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    .line 177
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    .line 178
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    .line 180
    return-void
.end method

.method public setComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setDiffFromAvg(D)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-wide p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    .line 52
    return-void
.end method

.method public setListId(I)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    .line 24
    return-void
.end method

.method public setMemberFavId(I)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    .line 28
    return-void
.end method

.method public setMslMatchStatus(I)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    .line 36
    return-void
.end method

.method public setSiteAvg(D)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-wide p1, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    .line 60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    invoke-super {p0}, Lgbis/gbandroid/entities/StationDetailed;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, "Member Favourite Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, "MSL Match Status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, "Station Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->stationId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, "Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, "List Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    const-string v1, "Diff From Avg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 126
    const-string v1, "Site Avg: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    iget-wide v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 128
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    const-string v1, "Time Offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    iget v1, p0, Lgbis/gbandroid/entities/FavStationMessage;->timeOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 163
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/entities/StationDetailed;->writeToParcel(Landroid/os/Parcel;I)V

    .line 164
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->memberFavId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 165
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->mslMatchStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    iget-object v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->comments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->listId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    iget-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->diffFromAvg:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 169
    iget-wide v0, p0, Lgbis/gbandroid/entities/FavStationMessage;->siteAvg:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 170
    return-void
.end method
