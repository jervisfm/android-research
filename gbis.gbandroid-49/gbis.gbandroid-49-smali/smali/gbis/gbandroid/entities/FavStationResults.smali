.class public Lgbis/gbandroid/entities/FavStationResults;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private favStationMessage:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "MemberFavoriteStationCollection"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private totalStations:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/FavStationResults;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lgbis/gbandroid/entities/FavStationResults;

    invoke-direct {v0}, Lgbis/gbandroid/entities/FavStationResults;-><init>()V

    .line 30
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationResults;->favStationMessage:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/FavStationResults;->favStationMessage:Ljava/util/List;

    .line 31
    iget v1, p0, Lgbis/gbandroid/entities/FavStationResults;->totalStations:I

    iput v1, v0, Lgbis/gbandroid/entities/FavStationResults;->totalStations:I

    .line 32
    return-object v0
.end method

.method public getFavStationMessage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    iget-object v0, p0, Lgbis/gbandroid/entities/FavStationResults;->favStationMessage:Ljava/util/List;

    return-object v0
.end method

.method public getTotalStations()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lgbis/gbandroid/entities/FavStationResults;->totalStations:I

    return v0
.end method

.method public setFavStationMessage(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    iput-object p1, p0, Lgbis/gbandroid/entities/FavStationResults;->favStationMessage:Ljava/util/List;

    .line 18
    return-void
.end method

.method public setTotalStations(I)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput p1, p0, Lgbis/gbandroid/entities/FavStationResults;->totalStations:I

    .line 26
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    const-string v1, "Fav Stations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    iget-object v1, p0, Lgbis/gbandroid/entities/FavStationResults;->favStationMessage:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 40
    const-string v1, "Total Stations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    iget v1, p0, Lgbis/gbandroid/entities/FavStationResults;->totalStations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 43
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
