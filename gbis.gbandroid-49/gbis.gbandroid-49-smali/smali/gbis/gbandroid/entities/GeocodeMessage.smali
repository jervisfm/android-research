.class public Lgbis/gbandroid/entities/GeocodeMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private address:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private latitude:D

.field private longitude:D

.field private postalCode:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/GeocodeMessage;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lgbis/gbandroid/entities/GeocodeMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/GeocodeMessage;-><init>()V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->address:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->address:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->city:Ljava/lang/String;

    .line 64
    iget-wide v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->latitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->latitude:D

    .line 65
    iget-wide v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->longitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->longitude:D

    .line 66
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->state:Ljava/lang/String;

    .line 67
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->postalCode:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/GeocodeMessage;->postalCode:Ljava/lang/String;

    .line 68
    return-object v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->longitude:D

    return-wide v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lgbis/gbandroid/entities/GeocodeMessage;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->address:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->city:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput-wide p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->latitude:D

    .line 50
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-wide p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->longitude:D

    .line 58
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->postalCode:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->state:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    const-string v1, "Address: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    const-string v1, "City: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, "State: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "Latitde: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-wide v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->latitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "Longitude: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-wide v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->longitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, "Postal Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget-object v1, p0, Lgbis/gbandroid/entities/GeocodeMessage;->postalCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
