.class public Lgbis/gbandroid/entities/InitializeMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private action:I

.field private ads:Lgbis/gbandroid/entities/Ads;

.field private authId:Ljava/lang/String;

.field private debug:Z

.field private gsa:Lgbis/gbandroid/entities/AdsGSA;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "GSA"
    .end annotation
.end field

.field private host:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/InitializeMessage;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Lgbis/gbandroid/entities/InitializeMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/InitializeMessage;-><init>()V

    .line 65
    iget v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->action:I

    iput v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->action:I

    .line 66
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->ads:Lgbis/gbandroid/entities/Ads;

    iput-object v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->ads:Lgbis/gbandroid/entities/Ads;

    .line 67
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->authId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->authId:Ljava/lang/String;

    .line 68
    iget-boolean v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->debug:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->debug:Z

    .line 69
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->gsa:Lgbis/gbandroid/entities/AdsGSA;

    iput-object v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->gsa:Lgbis/gbandroid/entities/AdsGSA;

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->host:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/InitializeMessage;->host:Ljava/lang/String;

    .line 71
    return-object v0
.end method

.method public getAction()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->action:I

    return v0
.end method

.method public getAds()Lgbis/gbandroid/entities/Ads;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->ads:Lgbis/gbandroid/entities/Ads;

    return-object v0
.end method

.method public getAuthId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->authId:Ljava/lang/String;

    return-object v0
.end method

.method public getGSA()Lgbis/gbandroid/entities/AdsGSA;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->gsa:Lgbis/gbandroid/entities/AdsGSA;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->host:Ljava/lang/String;

    return-object v0
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lgbis/gbandroid/entities/InitializeMessage;->debug:Z

    return v0
.end method

.method public setAction(I)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->action:I

    .line 21
    return-void
.end method

.method public setAds(Lgbis/gbandroid/entities/Ads;)V
    .locals 0
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->ads:Lgbis/gbandroid/entities/Ads;

    .line 53
    return-void
.end method

.method public setAuthId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->authId:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-boolean p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->debug:Z

    .line 37
    return-void
.end method

.method public setGSA(Lgbis/gbandroid/entities/AdsGSA;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->gsa:Lgbis/gbandroid/entities/AdsGSA;

    .line 57
    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lgbis/gbandroid/entities/InitializeMessage;->host:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    const-string v1, "Action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->action:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, "Auth Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->authId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "Host: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "Debug: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-boolean v1, p0, Lgbis/gbandroid/entities/InitializeMessage;->debug:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
