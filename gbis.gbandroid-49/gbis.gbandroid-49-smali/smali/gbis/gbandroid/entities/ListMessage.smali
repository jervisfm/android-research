.class public Lgbis/gbandroid/entities/ListMessage;
.super Lgbis/gbandroid/entities/Station;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgbis/gbandroid/entities/Station;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/ListMessage;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private car:Ljava/lang/String;

.field private carName:Ljava/lang/String;

.field private comments:Ljava/lang/String;

.field private distance:D

.field private isNear:Z

.field private memberId:Ljava/lang/String;

.field private price:D

.field private priceId:Ljava/math/BigInteger;

.field private sortOrder:I

.field private timeSpotted:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 192
    new-instance v0, Lgbis/gbandroid/entities/f;

    invoke-direct {v0}, Lgbis/gbandroid/entities/f;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/ListMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lgbis/gbandroid/entities/Station;-><init>()V

    .line 186
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 188
    invoke-direct {p0}, Lgbis/gbandroid/entities/Station;-><init>()V

    .line 189
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/ListMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 190
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/ListMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/ListMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/ListMessage;->compareTo(Lgbis/gbandroid/entities/ListMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/ListMessage;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Lgbis/gbandroid/entities/ListMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListMessage;-><init>()V

    .line 107
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->address:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->address:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->city:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->country:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->country:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->crossStreet:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->crossStreet:Ljava/lang/String;

    .line 114
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    .line 115
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->gasBrandId:I

    iput v1, v0, Lgbis/gbandroid/entities/ListMessage;->gasBrandId:I

    .line 116
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->gasBrandVersion:I

    iput v1, v0, Lgbis/gbandroid/entities/ListMessage;->gasBrandVersion:I

    .line 117
    iget-boolean v1, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    .line 118
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->latitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/ListMessage;->latitude:D

    .line 119
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->longitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/ListMessage;->longitude:D

    .line 120
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    .line 121
    iget-boolean v1, p0, Lgbis/gbandroid/entities/ListMessage;->notIntersection:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/ListMessage;->notIntersection:Z

    .line 122
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->postalCode:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->postalCode:Ljava/lang/String;

    .line 123
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/ListMessage;->price:D

    .line 124
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    .line 125
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->state:Ljava/lang/String;

    .line 126
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->stationId:I

    iput v1, v0, Lgbis/gbandroid/entities/ListMessage;->stationId:I

    .line 127
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->stationName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->stationName:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    .line 129
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->timeOffset:I

    iput v1, v0, Lgbis/gbandroid/entities/ListMessage;->timeOffset:I

    .line 130
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    iput v1, v0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    .line 131
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCarName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    return-object v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    return-wide v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    return-wide v0
.end method

.method public getPrice(Landroid/content/Context;Ljava/lang/String;)D
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 177
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    return-wide v0
.end method

.method public getPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getSortOrder()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    return v0
.end method

.method public getTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->timeOffset:I

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNear()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 224
    invoke-super {p0, p1}, Lgbis/gbandroid/entities/Station;->readFromParcel(Landroid/os/Parcel;)V

    .line 225
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    .line 233
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 234
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 235
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    .line 237
    return-void
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setCarName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setDistance(D)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-wide p1, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    .line 55
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setNear(Z)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput-boolean p1, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    .line 91
    return-void
.end method

.method public setPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-wide p1, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    .line 39
    return-void
.end method

.method public setPriceId(Ljava/math/BigInteger;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    .line 27
    return-void
.end method

.method public setSortOrder(I)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput p1, p0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    .line 103
    return-void
.end method

.method public setTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    invoke-super {p0}, Lgbis/gbandroid/entities/Station;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, "Distance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 141
    const-string v1, "Price Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 144
    const-string v1, "Price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-wide v1, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 147
    const-string v1, "Time Spotted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    const-string v1, "Time Offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget v1, p0, Lgbis/gbandroid/entities/ListMessage;->timeOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    const-string v1, "Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    const-string v1, "Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    const-string v1, "Car Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    const-string v1, "Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-object v1, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 165
    const-string v1, "Is Near: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    iget-boolean v1, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 209
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/entities/Station;->writeToParcel(Landroid/os/Parcel;I)V

    .line 210
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->priceId:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 211
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->price:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 212
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->timeSpotted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListMessage;->distance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 214
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->memberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->car:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->carName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lgbis/gbandroid/entities/ListMessage;->comments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lgbis/gbandroid/entities/ListMessage;->isNear:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 219
    iget v0, p0, Lgbis/gbandroid/entities/ListMessage;->sortOrder:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    return-void

    .line 210
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
