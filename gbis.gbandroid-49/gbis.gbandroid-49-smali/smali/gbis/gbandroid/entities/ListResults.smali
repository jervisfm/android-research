.class public Lgbis/gbandroid/entities/ListResults;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/ListResults;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private center:Lgbis/gbandroid/entities/Center;

.field private listMessages:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "PriceCollection"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private searchTerms:Ljava/lang/String;

.field private totalNear:I

.field private totalStations:I
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Total"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    new-instance v0, Lgbis/gbandroid/entities/g;

    invoke-direct {v0}, Lgbis/gbandroid/entities/g;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/ListResults;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/ListResults;->readFromParcel(Landroid/os/Parcel;)V

    .line 98
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/ListResults;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/ListResults;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lgbis/gbandroid/entities/ListResults;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ListResults;-><init>()V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    .line 64
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    .line 65
    iget v1, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    iput v1, v0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    .line 66
    iget v1, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    iput v1, v0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    .line 67
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public getCenter()Lgbis/gbandroid/entities/Center;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    return-object v0
.end method

.method public getListMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    return-object v0
.end method

.method public getSearchTerms()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalNear()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    return v0
.end method

.method public getTotalStations()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    .line 130
    const-class v0, Lgbis/gbandroid/entities/Center;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Center;

    iput-object v0, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    .line 131
    return-void
.end method

.method public setCenter(Lgbis/gbandroid/entities/Center;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    .line 54
    return-void
.end method

.method public setListMessages(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    iput-object p1, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    .line 26
    return-void
.end method

.method public setSearchTerms(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setTotalNear(I)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput p1, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    .line 34
    return-void
.end method

.method public setTotalStations(I)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput p1, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    .line 42
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    const-string v1, "Station Collection: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 75
    const-string v1, "Search Terms: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, "Center: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    const-string v1, "Search Terms: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget-object v1, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, "Total Near: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget v1, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, "Total Stations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget v1, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->listMessages:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 118
    iget v0, p0, Lgbis/gbandroid/entities/ListResults;->totalNear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget v0, p0, Lgbis/gbandroid/entities/ListResults;->totalStations:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->searchTerms:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lgbis/gbandroid/entities/ListResults;->center:Lgbis/gbandroid/entities/Center;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 122
    return-void
.end method
