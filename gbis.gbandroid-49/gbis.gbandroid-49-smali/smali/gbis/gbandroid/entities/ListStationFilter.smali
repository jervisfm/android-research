.class public Lgbis/gbandroid/entities/ListStationFilter;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/ListStationFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private distance:D

.field private hasPrices:Z

.field private price:D

.field private stationNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lgbis/gbandroid/entities/h;

    invoke-direct {v0}, Lgbis/gbandroid/entities/h;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/ListStationFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    .line 17
    return-void
.end method

.method public constructor <init>(DDZLjava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDZ",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lgbis/gbandroid/entities/ListStationFilter;->price:D

    .line 21
    iput-wide p3, p0, Lgbis/gbandroid/entities/ListStationFilter;->distance:D

    .line 22
    iput-boolean p5, p0, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices:Z

    .line 23
    iput-object p6, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    .line 24
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/ListStationFilter;->readFromParcel(Landroid/os/Parcel;)V

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/ListStationFilter;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->distance:D

    return-wide v0
.end method

.method public getPrice()D
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->price:D

    return-wide v0
.end method

.method public getStationNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    return-object v0
.end method

.method public hasPrices()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices:Z

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->price:D

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->distance:D

    .line 88
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 89
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 90
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices:Z

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    .line 92
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/StationNameFilter;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 93
    return-void
.end method

.method public setDistance(D)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-wide p1, p0, Lgbis/gbandroid/entities/ListStationFilter;->distance:D

    .line 40
    return-void
.end method

.method public setHasPrices(Z)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-boolean p1, p0, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices:Z

    .line 44
    return-void
.end method

.method public setPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-wide p1, p0, Lgbis/gbandroid/entities/ListStationFilter;->price:D

    .line 32
    return-void
.end method

.method public setStationNames(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    iput-object p1, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    .line 56
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 79
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->price:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 80
    iget-wide v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->distance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 81
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lgbis/gbandroid/entities/ListStationFilter;->hasPrices:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationFilter;->stationNames:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 83
    return-void
.end method
