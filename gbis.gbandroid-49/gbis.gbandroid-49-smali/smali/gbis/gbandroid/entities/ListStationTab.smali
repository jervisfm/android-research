.class public Lgbis/gbandroid/entities/ListStationTab;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/ListStationTab;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private fuelType:Ljava/lang/String;

.field private listStations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private listStationsFiltered:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private page:I

.field private sortOrder:I

.field private stationFilter:Lgbis/gbandroid/entities/ListStationFilter;

.field private totalNear:I

.field private totalStations:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lgbis/gbandroid/entities/i;

    invoke-direct {v0}, Lgbis/gbandroid/entities/i;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/ListStationTab;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/ListStationTab;->readFromParcel(Landroid/os/Parcel;)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/ListStationTab;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method public getFuelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->fuelType:Ljava/lang/String;

    return-object v0
.end method

.method public getListStations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStations:Ljava/util/List;

    return-object v0
.end method

.method public getListStationsFiltered()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStationsFiltered:Ljava/util/List;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->page:I

    return v0
.end method

.method public getSortOrder()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->sortOrder:I

    return v0
.end method

.method public getStationFilter()Lgbis/gbandroid/entities/ListStationFilter;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->stationFilter:Lgbis/gbandroid/entities/ListStationFilter;

    return-object v0
.end method

.method public getTotalNear()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalNear:I

    return v0
.end method

.method public getTotalStations()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalStations:I

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStations:Ljava/util/List;

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStations:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStationsFiltered:Ljava/util/List;

    .line 121
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStationsFiltered:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListStationTab;->sortOrder:I

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListStationTab;->page:I

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalStations:I

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalNear:I

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->fuelType:Ljava/lang/String;

    .line 127
    const-class v0, Lgbis/gbandroid/entities/ListStationFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListStationFilter;

    iput-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->stationFilter:Lgbis/gbandroid/entities/ListStationFilter;

    .line 128
    return-void
.end method

.method public setFuelType(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lgbis/gbandroid/entities/ListStationTab;->fuelType:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setListStations(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/ListStationTab;->listStations:Ljava/util/List;

    .line 25
    return-void
.end method

.method public setListStationsFiltered(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/entities/ListStationTab;->listStationsFiltered:Ljava/util/List;

    .line 29
    return-void
.end method

.method public setPage(I)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput p1, p0, Lgbis/gbandroid/entities/ListStationTab;->page:I

    .line 49
    return-void
.end method

.method public setSortOrder(I)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput p1, p0, Lgbis/gbandroid/entities/ListStationTab;->sortOrder:I

    .line 41
    return-void
.end method

.method public setStationFilter(Lgbis/gbandroid/entities/ListStationFilter;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lgbis/gbandroid/entities/ListStationTab;->stationFilter:Lgbis/gbandroid/entities/ListStationFilter;

    .line 77
    return-void
.end method

.method public setTotalNear(I)V
    .locals 0
    .parameter

    .prologue
    .line 60
    iput p1, p0, Lgbis/gbandroid/entities/ListStationTab;->totalNear:I

    .line 61
    return-void
.end method

.method public setTotalStations(I)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput p1, p0, Lgbis/gbandroid/entities/ListStationTab;->totalStations:I

    .line 57
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStations:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 108
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->listStationsFiltered:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 109
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->sortOrder:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->page:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalStations:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lgbis/gbandroid/entities/ListStationTab;->totalNear:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->fuelType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lgbis/gbandroid/entities/ListStationTab;->stationFilter:Lgbis/gbandroid/entities/ListStationFilter;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 115
    return-void
.end method
