.class public Lgbis/gbandroid/entities/LoginMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private car:Ljava/lang/String;

.field private carIconId:I

.field private memberId:Ljava/lang/String;

.field private signedIn:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/LoginMessage;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lgbis/gbandroid/entities/LoginMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/LoginMessage;-><init>()V

    .line 44
    iget-boolean v1, p0, Lgbis/gbandroid/entities/LoginMessage;->signedIn:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/LoginMessage;->signedIn:Z

    .line 45
    iget-object v1, p0, Lgbis/gbandroid/entities/LoginMessage;->car:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/LoginMessage;->car:Ljava/lang/String;

    .line 46
    iget v1, p0, Lgbis/gbandroid/entities/LoginMessage;->carIconId:I

    iput v1, v0, Lgbis/gbandroid/entities/LoginMessage;->carIconId:I

    .line 47
    iget-object v1, p0, Lgbis/gbandroid/entities/LoginMessage;->memberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/LoginMessage;->memberId:Ljava/lang/String;

    .line 48
    return-object v0
.end method

.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lgbis/gbandroid/entities/LoginMessage;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lgbis/gbandroid/entities/LoginMessage;->carIconId:I

    return v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/entities/LoginMessage;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 11
    iget-boolean v0, p0, Lgbis/gbandroid/entities/LoginMessage;->signedIn:Z

    return v0
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lgbis/gbandroid/entities/LoginMessage;->car:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setCarIconId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 31
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/LoginMessage;->carIconId:I

    .line 32
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lgbis/gbandroid/entities/LoginMessage;->memberId:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setSignedIn(Z)V
    .locals 0
    .parameter

    .prologue
    .line 15
    iput-boolean p1, p0, Lgbis/gbandroid/entities/LoginMessage;->signedIn:Z

    .line 16
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    const-string v1, "Signed In: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    iget-boolean v1, p0, Lgbis/gbandroid/entities/LoginMessage;->signedIn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 56
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 57
    const-string v1, "Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    iget-object v1, p0, Lgbis/gbandroid/entities/LoginMessage;->car:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 60
    const-string v1, "Car Icon Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget v1, p0, Lgbis/gbandroid/entities/LoginMessage;->carIconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, "Member ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    iget-object v1, p0, Lgbis/gbandroid/entities/LoginMessage;->memberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
