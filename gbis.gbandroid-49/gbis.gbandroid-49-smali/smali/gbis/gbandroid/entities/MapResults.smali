.class public Lgbis/gbandroid/entities/MapResults;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/MapResults;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private adsOnTop:Z

.field private choiceHotels:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Ads"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ChoiceHotels;",
            ">;"
        }
    .end annotation
.end field

.field private listMessages:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Stations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private totalStations:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lgbis/gbandroid/entities/j;

    invoke-direct {v0}, Lgbis/gbandroid/entities/j;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/MapResults;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/MapResults;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/MapResults;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/MapResults;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lgbis/gbandroid/entities/MapResults;

    invoke-direct {v0}, Lgbis/gbandroid/entities/MapResults;-><init>()V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    .line 54
    iget v1, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    iput v1, v0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    .line 55
    iget-object v1, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    .line 56
    iget-boolean v1, p0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    .line 57
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public getChoiceHotels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ChoiceHotels;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    return-object v0
.end method

.method public getListMessage()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    return-object v0
.end method

.method public getTotalStations()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    return v0
.end method

.method public isAdsOnTop()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    .line 104
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    .line 107
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 108
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 109
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 110
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    .line 111
    return-void
.end method

.method public setAdsOnTop(Z)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-boolean p1, p0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    .line 49
    return-void
.end method

.method public setChoiceHotels(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ChoiceHotels;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    iput-object p1, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    .line 41
    return-void
.end method

.method public setListMessage(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    .line 25
    return-void
.end method

.method public setTotalStations(I)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput p1, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    .line 33
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    const-string v1, "List Stations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    const-string v1, "Total Stations: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    iget v1, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->listMessages:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 96
    iget v0, p0, Lgbis/gbandroid/entities/MapResults;->totalStations:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget-object v0, p0, Lgbis/gbandroid/entities/MapResults;->choiceHotels:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 98
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lgbis/gbandroid/entities/MapResults;->adsOnTop:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 100
    return-void
.end method
