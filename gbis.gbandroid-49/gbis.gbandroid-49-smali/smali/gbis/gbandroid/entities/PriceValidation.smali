.class public Lgbis/gbandroid/entities/PriceValidation;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/PriceValidation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private commonDecreaseDiesel:D

.field private commonDecreaseRegular:D

.field private commonIncreaseDiesel:D

.field private commonIncreaseRegular:D

.field private expectedDieselPrice:D

.field private expectedRegularPrice:D

.field private offsetMidgrade:D

.field private offsetPremium:D

.field private radius:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lgbis/gbandroid/entities/l;

    invoke-direct {v0}, Lgbis/gbandroid/entities/l;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/PriceValidation;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/PriceValidation;->readFromParcel(Landroid/os/Parcel;)V

    .line 127
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/PriceValidation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedRegularPrice:D

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseRegular:D

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseRegular:D

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetPremium:D

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedDieselPrice:D

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseDiesel:D

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseDiesel:D

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->radius:D

    .line 167
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public getCommonDecreaseDiesel()D
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseDiesel:D

    return-wide v0
.end method

.method public getCommonDecreaseRegular()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseRegular:D

    return-wide v0
.end method

.method public getCommonIncreaseDiesel()D
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseDiesel:D

    return-wide v0
.end method

.method public getCommonIncreaseRegular()D
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseRegular:D

    return-wide v0
.end method

.method public getExpectedDieselPrice()D
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedDieselPrice:D

    return-wide v0
.end method

.method public getExpectedRegularPrice()D
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedRegularPrice:D

    return-wide v0
.end method

.method public getOffsetMidgrade()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    return-wide v0
.end method

.method public getOffsetPremium()D
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetPremium:D

    return-wide v0
.end method

.method public getRadius()D
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->radius:D

    return-wide v0
.end method

.method public setCommonDecreaseDiesel(D)V
    .locals 0
    .parameter

    .prologue
    .line 78
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseDiesel:D

    .line 79
    return-void
.end method

.method public setCommonDecreaseRegular(D)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseRegular:D

    .line 39
    return-void
.end method

.method public setCommonIncreaseDiesel(D)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseDiesel:D

    .line 71
    return-void
.end method

.method public setCommonIncreaseRegular(D)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseRegular:D

    .line 31
    return-void
.end method

.method public setExpectedDieselPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedDieselPrice:D

    .line 63
    return-void
.end method

.method public setExpectedRegularPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedRegularPrice:D

    .line 23
    return-void
.end method

.method public setOffsetMidgrade(D)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    .line 47
    return-void
.end method

.method public setOffsetPremium(D)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetPremium:D

    .line 55
    return-void
.end method

.method public setRadius(D)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-wide p1, p0, Lgbis/gbandroid/entities/PriceValidation;->radius:D

    .line 87
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    const-string v1, "Expected Regular Price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedRegularPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, "Common Increase Regular: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseRegular:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 97
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 98
    const-string v1, "Common Decrease Regular: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseRegular:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 100
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    const-string v1, "Offset Midgrade: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    const-string v1, "Offset Premium: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, "Expected Diesel Price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedDieselPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "Common Increase Diesel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseDiesel:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 113
    const-string v1, "Common Decrease Diesel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseDiesel:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, "Radius: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget-wide v1, p0, Lgbis/gbandroid/entities/PriceValidation;->radius:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 118
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 146
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedRegularPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 147
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseRegular:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 148
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseRegular:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 149
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetMidgrade:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 150
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->offsetPremium:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 151
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->expectedDieselPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 152
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonIncreaseDiesel:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 153
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->commonDecreaseDiesel:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 154
    iget-wide v0, p0, Lgbis/gbandroid/entities/PriceValidation;->radius:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 155
    return-void
.end method
