.class public Lgbis/gbandroid/entities/PrizeMemberMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private address1:Ljava/lang/String;

.field private address2:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private country:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private lastName:Ljava/lang/String;

.field private pointBalance:I

.field private postalCode:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private ticketsAvailable:I

.field private ticketsPurchased:I

.field private totalPoints:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/PrizeMemberMessage;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/PrizeMemberMessage;-><init>()V

    .line 125
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address1:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address1:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address2:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address2:Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->city:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->country:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->country:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->email:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->email:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->firstName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->firstName:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->lastName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->lastName:Ljava/lang/String;

    .line 132
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->pointBalance:I

    iput v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->pointBalance:I

    .line 133
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->postalCode:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->postalCode:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->state:Ljava/lang/String;

    .line 135
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsAvailable:I

    iput v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsAvailable:I

    .line 136
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsPurchased:I

    iput v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsPurchased:I

    .line 137
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->totalPoints:I

    iput v1, v0, Lgbis/gbandroid/entities/PrizeMemberMessage;->totalPoints:I

    .line 138
    return-object v0
.end method

.method public getAddress1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address1:Ljava/lang/String;

    return-object v0
.end method

.method public getAddress2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address2:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->lastName:Ljava/lang/String;

    return-object v0
.end method

.method public getPointBalance()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->pointBalance:I

    return v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketsAvailable()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsAvailable:I

    return v0
.end method

.method public getTicketsPurchased()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsPurchased:I

    return v0
.end method

.method public getTotalPoints()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->totalPoints:I

    return v0
.end method

.method public setAddress1(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address1:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setAddress2(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address2:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->city:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 80
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->country:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->email:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->firstName:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->lastName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setPointBalance(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 104
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->pointBalance:I

    .line 105
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->postalCode:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->state:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setTicketsAvailable(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 120
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsAvailable:I

    .line 121
    return-void
.end method

.method public setTicketsPurchased(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 112
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsPurchased:I

    .line 113
    return-void
.end method

.method public setTotalPoints(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->totalPoints:I

    .line 97
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    const-string v1, "Email: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 147
    const-string v1, "First Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    const-string v1, "Last Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    const-string v1, "Address1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 156
    const-string v1, "Address2: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->address2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    const-string v1, "City: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    const-string v1, "State: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 165
    const-string v1, "Country: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 168
    const-string v1, "Postal Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->postalCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    const-string v1, "Total Points: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->totalPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    const-string v1, "Point Balance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->pointBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 177
    const-string v1, "Tickets Purchased: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsPurchased:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    const-string v1, "Tickets Available: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    iget v1, p0, Lgbis/gbandroid/entities/PrizeMemberMessage;->ticketsAvailable:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 182
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
