.class public Lgbis/gbandroid/entities/PrizeResults;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "MemberPrizeDraw"
    .end annotation
.end field

.field private prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

.field private strings:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Strings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setMessagesStrings()Lgbis/gbandroid/entities/PrizeStringsMessage;
    .locals 3

    .prologue
    .line 41
    new-instance v1, Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-direct {v1}, Lgbis/gbandroid/entities/PrizeStringsMessage;-><init>()V

    .line 42
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->setExplanation(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->setTitle(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->setDate(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->setTotalPoints(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;->setLimit(Ljava/lang/String;)V

    .line 47
    return-object v1
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/PrizeResults;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lgbis/gbandroid/entities/PrizeResults;

    invoke-direct {v0}, Lgbis/gbandroid/entities/PrizeResults;-><init>()V

    .line 52
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeResults;->prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    .line 54
    return-object v0
.end method

.method public getPrizeMemberMessage()Lgbis/gbandroid/entities/PrizeMemberMessage;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;

    return-object v0
.end method

.method public getPrizeStringsMessage()Lgbis/gbandroid/entities/PrizeStringsMessage;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    if-nez v0, :cond_0

    .line 24
    invoke-direct {p0}, Lgbis/gbandroid/entities/PrizeResults;->setMessagesStrings()Lgbis/gbandroid/entities/PrizeStringsMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    .line 25
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    return-object v0
.end method

.method public getStrings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    return-object v0
.end method

.method public setPrizeMemberMessage(Lgbis/gbandroid/entities/PrizeMemberMessage;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;

    .line 20
    return-void
.end method

.method public setPrizeStringsMessage(Lgbis/gbandroid/entities/PrizeStringsMessage;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    .line 30
    return-void
.end method

.method public setStrings(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeResults;->strings:Ljava/util/List;

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    const-string v1, "Member Info: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeMemberMessage:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 62
    const-string v1, "Strings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeResults;->prizeStringsMessage:Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeStringsMessage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
