.class public Lgbis/gbandroid/entities/PrizeStringsMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private date:Ljava/lang/String;

.field private explanation:Ljava/lang/String;

.field private limit:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private totalPoints:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/PrizeStringsMessage;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lgbis/gbandroid/entities/PrizeStringsMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/PrizeStringsMessage;-><init>()V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->date:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeStringsMessage;->date:Ljava/lang/String;

    .line 54
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->explanation:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeStringsMessage;->explanation:Ljava/lang/String;

    .line 55
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->limit:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeStringsMessage;->limit:Ljava/lang/String;

    .line 56
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->title:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeStringsMessage;->title:Ljava/lang/String;

    .line 57
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->totalPoints:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/PrizeStringsMessage;->totalPoints:Ljava/lang/String;

    .line 58
    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getExplanation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->explanation:Ljava/lang/String;

    return-object v0
.end method

.method public getLimit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->limit:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalPoints()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->totalPoints:Ljava/lang/String;

    return-object v0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->date:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setExplanation(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->explanation:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setLimit(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->limit:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->title:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public setTotalPoints(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->totalPoints:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    const-string v1, "Title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    const-string v1, "Date: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 70
    const-string v1, "Explanation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->explanation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 73
    const-string v1, "Limit: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->limit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 76
    const-string v1, "Total Points: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget-object v1, p0, Lgbis/gbandroid/entities/PrizeStringsMessage;->totalPoints:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
