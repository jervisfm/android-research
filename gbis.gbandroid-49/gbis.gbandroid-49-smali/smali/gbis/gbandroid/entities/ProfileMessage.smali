.class public Lgbis/gbandroid/entities/ProfileMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private car:Ljava/lang/String;

.field private carIconId:I

.field private consecutiveDays:I

.field private forumPosts:I

.field private forumTitle:Ljava/lang/String;

.field private joinDate:Ljava/lang/String;

.field private overallRank:I

.field private overallRank30Days:I

.field private picturePath:Ljava/lang/String;

.field private pointBalance:I

.field private pointsToday:I

.field private site:Ljava/lang/String;

.field private siteName:Ljava/lang/String;

.field private totalPoints:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/ProfileMessage;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Lgbis/gbandroid/entities/ProfileMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ProfileMessage;-><init>()V

    .line 135
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->car:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->car:Ljava/lang/String;

    .line 136
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->carIconId:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->carIconId:I

    .line 137
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->consecutiveDays:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->consecutiveDays:I

    .line 138
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumPosts:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->forumPosts:I

    .line 139
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumTitle:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->forumTitle:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->joinDate:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->joinDate:Ljava/lang/String;

    .line 141
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank:I

    .line 142
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank30Days:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank30Days:I

    .line 143
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->picturePath:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->picturePath:Ljava/lang/String;

    .line 144
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointBalance:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->pointBalance:I

    .line 145
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointsToday:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->pointsToday:I

    .line 146
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->site:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->site:Ljava/lang/String;

    .line 147
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->siteName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->siteName:Ljava/lang/String;

    .line 148
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->totalPoints:I

    iput v1, v0, Lgbis/gbandroid/entities/ProfileMessage;->totalPoints:I

    .line 149
    return-object v0
.end method

.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->carIconId:I

    return v0
.end method

.method public getConsecutiveDays()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->consecutiveDays:I

    return v0
.end method

.method public getForumPosts()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumPosts:I

    return v0
.end method

.method public getForumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getJoinDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->joinDate:Ljava/lang/String;

    return-object v0
.end method

.method public getOverallRank()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank:I

    return v0
.end method

.method public getOverallRank30Days()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank30Days:I

    return v0
.end method

.method public getPicturePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->picturePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPointBalance()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointBalance:I

    return v0
.end method

.method public getPointsToday()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointsToday:I

    return v0
.end method

.method public getSite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->site:Ljava/lang/String;

    return-object v0
.end method

.method public getSiteName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->siteName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalPoints()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->totalPoints:I

    return v0
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->car:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setCarIconId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 82
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->carIconId:I

    .line 83
    return-void
.end method

.method public setConsecutiveDays(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 86
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->consecutiveDays:I

    .line 87
    return-void
.end method

.method public setForumPosts(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 94
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumPosts:I

    .line 95
    return-void
.end method

.method public setForumTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumTitle:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setJoinDate(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->joinDate:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setOverallRank(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 118
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank:I

    .line 119
    return-void
.end method

.method public setOverallRank30Days(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 126
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank30Days:I

    .line 127
    return-void
.end method

.method public setPicturePath(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->picturePath:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setPointBalance(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 110
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointBalance:I

    .line 111
    return-void
.end method

.method public setPointsToday(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 102
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointsToday:I

    .line 103
    return-void
.end method

.method public setSite(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->site:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setSiteName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/ProfileMessage;->siteName:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setTotalPoints(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/ProfileMessage;->totalPoints:I

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    const-string v1, "Site: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->site:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 158
    const-string v1, "Site Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->siteName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    const-string v1, "Join Date: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->joinDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 164
    const-string v1, "Forum Title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 167
    const-string v1, "Total Points: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->totalPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 169
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 170
    const-string v1, "Picture Path: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->picturePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 173
    const-string v1, "Overall Rank: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    const-string v1, "Overall Rank 30 Days: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->overallRank30Days:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 178
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    const-string v1, "Consecutive Days: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->consecutiveDays:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    const-string v1, "Forum Posts: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->forumPosts:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 185
    const-string v1, "Points Today: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointsToday:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    const-string v1, "Point Balance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->pointBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    const-string v1, "Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    iget-object v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->car:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 194
    const-string v1, "Car Icon Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    iget v1, p0, Lgbis/gbandroid/entities/ProfileMessage;->carIconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
