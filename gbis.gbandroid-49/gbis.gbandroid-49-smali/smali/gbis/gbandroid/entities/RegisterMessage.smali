.class public Lgbis/gbandroid/entities/RegisterMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private car:Ljava/lang/String;

.field private carIconId:I

.field private memberId:Ljava/lang/String;

.field private memberIdSuggestions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private signedIn:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberIdSuggestions:Ljava/util/List;

    .line 15
    return-void
.end method


# virtual methods
.method public addMemberIdSuggestions(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberIdSuggestions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public copy()Lgbis/gbandroid/entities/RegisterMessage;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lgbis/gbandroid/entities/RegisterMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/RegisterMessage;-><init>()V

    .line 59
    iget-boolean v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->signedIn:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/RegisterMessage;->signedIn:Z

    .line 60
    iget-object v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->car:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/RegisterMessage;->car:Ljava/lang/String;

    .line 61
    iget v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->carIconId:I

    iput v1, v0, Lgbis/gbandroid/entities/RegisterMessage;->carIconId:I

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/RegisterMessage;->memberId:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberIdSuggestions:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/RegisterMessage;->memberIdSuggestions:Ljava/util/List;

    .line 64
    return-object v0
.end method

.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->carIconId:I

    return v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberIdSuggestions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberIdSuggestions:Ljava/util/List;

    return-object v0
.end method

.method public isSignedIn()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->signedIn:Z

    return v0
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/entities/RegisterMessage;->car:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setCarIconId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 38
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->carIconId:I

    .line 39
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberId:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setSignedIn(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, p1}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lgbis/gbandroid/entities/RegisterMessage;->signedIn:Z

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    const-string v1, "Signed In: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-boolean v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->signedIn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 73
    const-string v1, "Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->car:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 76
    const-string v1, "Car Icon Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    iget v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->carIconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 79
    const-string v1, "Member ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v1, p0, Lgbis/gbandroid/entities/RegisterMessage;->memberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
