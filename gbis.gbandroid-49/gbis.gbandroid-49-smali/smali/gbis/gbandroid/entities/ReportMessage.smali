.class public Lgbis/gbandroid/entities/ReportMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private carIcon:Ljava/lang/String;

.field private carIconId:I

.field private dieselPrice:D

.field private dieselPriceId:Ljava/math/BigInteger;

.field private midPrice:D

.field private midPriceId:Ljava/math/BigInteger;

.field private pointBalance:I

.field private premPrice:D

.field private premPriceId:Ljava/math/BigInteger;

.field private regPrice:D

.field private regPriceId:Ljava/math/BigInteger;

.field private totalPointsAwarded:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/ReportMessage;
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lgbis/gbandroid/entities/ReportMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ReportMessage;-><init>()V

    .line 117
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIcon:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ReportMessage;->carIcon:Ljava/lang/String;

    .line 118
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIconId:I

    iput v1, v0, Lgbis/gbandroid/entities/ReportMessage;->carIconId:I

    .line 119
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/ReportMessage;->dieselPriceId:Ljava/math/BigInteger;

    .line 120
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->midPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/ReportMessage;->midPriceId:Ljava/math/BigInteger;

    .line 121
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->premPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/ReportMessage;->premPriceId:Ljava/math/BigInteger;

    .line 122
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->regPriceId:Ljava/math/BigInteger;

    iput-object v1, v0, Lgbis/gbandroid/entities/ReportMessage;->regPriceId:Ljava/math/BigInteger;

    .line 123
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->totalPointsAwarded:I

    iput v1, v0, Lgbis/gbandroid/entities/ReportMessage;->totalPointsAwarded:I

    .line 124
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->pointBalance:I

    iput v1, v0, Lgbis/gbandroid/entities/ReportMessage;->pointBalance:I

    .line 125
    return-object v0
.end method

.method public getCarIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lgbis/gbandroid/entities/ReportMessage;->carIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lgbis/gbandroid/entities/ReportMessage;->carIconId:I

    return v0
.end method

.method public getDieselPrice()D
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPrice:D

    return-wide v0
.end method

.method public getDieselPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getMidPrice()D
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lgbis/gbandroid/entities/ReportMessage;->midPrice:D

    return-wide v0
.end method

.method public getMidPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lgbis/gbandroid/entities/ReportMessage;->midPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPointBalance()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lgbis/gbandroid/entities/ReportMessage;->pointBalance:I

    return v0
.end method

.method public getPremPrice()D
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lgbis/gbandroid/entities/ReportMessage;->premPrice:D

    return-wide v0
.end method

.method public getPremPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lgbis/gbandroid/entities/ReportMessage;->premPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getRegPrice()D
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lgbis/gbandroid/entities/ReportMessage;->regPrice:D

    return-wide v0
.end method

.method public getRegPriceId()Ljava/math/BigInteger;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lgbis/gbandroid/entities/ReportMessage;->regPriceId:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getTotalPointsAwarded()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lgbis/gbandroid/entities/ReportMessage;->totalPointsAwarded:I

    return v0
.end method

.method public setCarIcon(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIcon:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setCarIconId(I)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput p1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIconId:I

    .line 113
    return-void
.end method

.method public setDieselPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 80
    iput-wide p1, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPrice:D

    .line 81
    return-void
.end method

.method public setDieselPriceId(Ljava/math/BigInteger;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPriceId:Ljava/math/BigInteger;

    .line 49
    return-void
.end method

.method public setMidPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-wide p1, p0, Lgbis/gbandroid/entities/ReportMessage;->midPrice:D

    .line 65
    return-void
.end method

.method public setMidPriceId(Ljava/math/BigInteger;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lgbis/gbandroid/entities/ReportMessage;->midPriceId:Ljava/math/BigInteger;

    .line 33
    return-void
.end method

.method public setPointBalance(I)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput p1, p0, Lgbis/gbandroid/entities/ReportMessage;->pointBalance:I

    .line 97
    return-void
.end method

.method public setPremPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-wide p1, p0, Lgbis/gbandroid/entities/ReportMessage;->premPrice:D

    .line 73
    return-void
.end method

.method public setPremPriceId(Ljava/math/BigInteger;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lgbis/gbandroid/entities/ReportMessage;->premPriceId:Ljava/math/BigInteger;

    .line 41
    return-void
.end method

.method public setRegPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-wide p1, p0, Lgbis/gbandroid/entities/ReportMessage;->regPrice:D

    .line 57
    return-void
.end method

.method public setRegPriceId(Ljava/math/BigInteger;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lgbis/gbandroid/entities/ReportMessage;->regPriceId:Ljava/math/BigInteger;

    .line 25
    return-void
.end method

.method public setTotalPointsAwarded(I)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput p1, p0, Lgbis/gbandroid/entities/ReportMessage;->totalPointsAwarded:I

    .line 89
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    const-string v1, "Regular price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->regPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 134
    const-string v1, "Midgrade price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->midPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, "Premium price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->premPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, "Diesel price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->dieselPriceId:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 143
    const-string v1, "Total Points Awarded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->totalPointsAwarded:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, "Point Balance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->pointBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 149
    const-string v1, "Car Icon: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIcon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 152
    const-string v1, "Car Icon Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    iget v1, p0, Lgbis/gbandroid/entities/ReportMessage;->carIconId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
