.class public Lgbis/gbandroid/entities/ResponseMessage;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private awards:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "AwardCollection"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation
.end field

.field private payload:Ljava/lang/Object;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Payload"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private responseCode:I
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "ResponseCode"
    .end annotation
.end field

.field private responseMessage:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "ResponseMessage"
    .end annotation
.end field

.field private smartPrompts:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "SmartPromptCollection"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/SmartPrompt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Lgbis/gbandroid/entities/ResponseMessage;->setAwards(Ljava/util/List;)V

    .line 22
    return-void
.end method


# virtual methods
.method public addAwards(Lgbis/gbandroid/entities/AwardsMessage;)V
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method public copy()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lgbis/gbandroid/entities/ResponseMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ResponseMessage;-><init>()V

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    .line 71
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->payload:Ljava/lang/Object;

    iput-object v1, v0, Lgbis/gbandroid/entities/ResponseMessage;->payload:Ljava/lang/Object;

    .line 72
    iget v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseCode:I

    iput v1, v0, Lgbis/gbandroid/entities/ResponseMessage;->responseCode:I

    .line 73
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseMessage:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ResponseMessage;->responseMessage:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->smartPrompts:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/ResponseMessage;->smartPrompts:Ljava/util/List;

    .line 75
    return-object v0
.end method

.method public getAwards()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    return-object v0
.end method

.method public getPayload()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->payload:Ljava/lang/Object;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseCode:I

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getSmartPrompts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/SmartPrompt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/entities/ResponseMessage;->smartPrompts:Ljava/util/List;

    return-object v0
.end method

.method public setAwards(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AwardsMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    iput-object p1, p0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    .line 58
    return-void
.end method

.method public setPayload(Ljava/lang/Object;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 53
    iput-object p1, p0, Lgbis/gbandroid/entities/ResponseMessage;->payload:Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public setResponseCode(I)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseCode:I

    .line 30
    return-void
.end method

.method public setResponseMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseMessage:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setSmartPrompts(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/SmartPrompt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/entities/ResponseMessage;->smartPrompts:Ljava/util/List;

    .line 46
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    const-string v1, "Response Code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    iget v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, "Response Message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->responseMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, "Payload: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->payload:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, "Awards: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v1, p0, Lgbis/gbandroid/entities/ResponseMessage;->awards:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
