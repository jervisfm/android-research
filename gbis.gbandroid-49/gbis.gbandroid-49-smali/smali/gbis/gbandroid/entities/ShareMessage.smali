.class public Lgbis/gbandroid/entities/ShareMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private message:Ljava/lang/String;

.field private subject:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/ShareMessage;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lgbis/gbandroid/entities/ShareMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/ShareMessage;-><init>()V

    .line 26
    iget-object v1, p0, Lgbis/gbandroid/entities/ShareMessage;->message:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ShareMessage;->message:Ljava/lang/String;

    .line 27
    iget-object v1, p0, Lgbis/gbandroid/entities/ShareMessage;->subject:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/ShareMessage;->subject:Ljava/lang/String;

    .line 28
    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/ShareMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lgbis/gbandroid/entities/ShareMessage;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/ShareMessage;->message:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lgbis/gbandroid/entities/ShareMessage;->subject:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    const-string v1, "Subject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    iget-object v1, p0, Lgbis/gbandroid/entities/ShareMessage;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 37
    const-string v1, "Message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    iget-object v1, p0, Lgbis/gbandroid/entities/ShareMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
