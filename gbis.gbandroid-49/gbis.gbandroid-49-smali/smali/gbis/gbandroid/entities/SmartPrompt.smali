.class public Lgbis/gbandroid/entities/SmartPrompt;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/SmartPrompt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private prompt:Ljava/lang/String;

.field private promptId:I
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "PromptID"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lgbis/gbandroid/entities/m;

    invoke-direct {v0}, Lgbis/gbandroid/entities/m;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/SmartPrompt;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 8
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/SmartPrompt;->readFromParcel(Landroid/os/Parcel;)V

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/SmartPrompt;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/SmartPrompt;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lgbis/gbandroid/entities/SmartPrompt;

    invoke-direct {v0}, Lgbis/gbandroid/entities/SmartPrompt;-><init>()V

    .line 31
    iget-object v1, p0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    .line 32
    iget v1, p0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    iput v1, v0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    .line 33
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public getPrompt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    return-object v0
.end method

.method public getPromptId()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    .line 67
    return-void
.end method

.method public setPrompt(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    iput-object p1, p0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public setPromptId(I)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput p1, p0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    .line 27
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->prompt:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget v0, p0, Lgbis/gbandroid/entities/SmartPrompt;->promptId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    return-void
.end method
