.class public abstract Lgbis/gbandroid/entities/StationDetailed;
.super Lgbis/gbandroid/entities/Station;
.source "GBFile"


# instance fields
.field protected dieselCar:Ljava/lang/String;

.field protected dieselMemberId:Ljava/lang/String;

.field protected dieselPrice:D

.field protected dieselTimeSpotted:Ljava/lang/String;

.field protected midCar:Ljava/lang/String;

.field protected midMemberId:Ljava/lang/String;

.field protected midPrice:D

.field protected midTimeSpotted:Ljava/lang/String;

.field protected premCar:Ljava/lang/String;

.field protected premMemberId:Ljava/lang/String;

.field protected premPrice:D

.field protected premTimeSpotted:Ljava/lang/String;

.field protected regCar:Ljava/lang/String;

.field protected regMemberId:Ljava/lang/String;

.field protected regPrice:D

.field protected regTimeSpotted:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lgbis/gbandroid/entities/Station;-><init>()V

    return-void
.end method


# virtual methods
.method public getDieselCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselCar:Ljava/lang/String;

    return-object v0
.end method

.method public getDieselMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselMemberId:Ljava/lang/String;

    return-object v0
.end method

.method public getDieselPrice()D
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    return-wide v0
.end method

.method public getDieselTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getMidCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midCar:Ljava/lang/String;

    return-object v0
.end method

.method public getMidMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midMemberId:Ljava/lang/String;

    return-object v0
.end method

.method public getMidPrice()D
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    return-wide v0
.end method

.method public getMidTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getPremCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premCar:Ljava/lang/String;

    return-object v0
.end method

.method public getPremMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premMemberId:Ljava/lang/String;

    return-object v0
.end method

.method public getPremPrice()D
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    return-wide v0
.end method

.method public getPremTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getPrice(Landroid/content/Context;Ljava/lang/String;)D
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 160
    const v0, 0x7f0901c9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    .line 172
    :goto_0
    return-wide v0

    .line 163
    :cond_0
    const v0, 0x7f0901ca

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    goto :goto_0

    .line 166
    :cond_1
    const v0, 0x7f0901cb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    goto :goto_0

    .line 169
    :cond_2
    const v0, 0x7f0901cc

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    goto :goto_0

    .line 172
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getRegCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regCar:Ljava/lang/String;

    return-object v0
.end method

.method public getRegMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regMemberId:Ljava/lang/String;

    return-object v0
.end method

.method public getRegPrice()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    return-wide v0
.end method

.method public getRegTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 177
    const v0, 0x7f0901c9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/entities/StationDetailed;->timeOffset:I

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v0

    .line 189
    :goto_0
    return-object v0

    .line 180
    :cond_0
    const v0, 0x7f0901ca

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/entities/StationDetailed;->timeOffset:I

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 183
    :cond_1
    const v0, 0x7f0901cb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/entities/StationDetailed;->timeOffset:I

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 186
    :cond_2
    const v0, 0x7f0901cc

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget v1, p0, Lgbis/gbandroid/entities/StationDetailed;->timeOffset:I

    invoke-static {v0, v1}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 270
    invoke-super {p0, p1}, Lgbis/gbandroid/entities/Station;->readFromParcel(Landroid/os/Parcel;)V

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    .line 276
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regMemberId:Ljava/lang/String;

    .line 277
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regCar:Ljava/lang/String;

    .line 278
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    .line 279
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midMemberId:Ljava/lang/String;

    .line 280
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midCar:Ljava/lang/String;

    .line 281
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    .line 282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premMemberId:Ljava/lang/String;

    .line 283
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premCar:Ljava/lang/String;

    .line 284
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    .line 285
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselMemberId:Ljava/lang/String;

    .line 286
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselCar:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public setDieselCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 151
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselCar:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setDieselMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselMemberId:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setDieselPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput-wide p1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    .line 132
    return-void
.end method

.method public setDieselTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 135
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public setMidCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->midCar:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setMidMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->midMemberId:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setMidPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-wide p1, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    .line 68
    return-void
.end method

.method public setMidTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setPremCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->premCar:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setPremMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->premMemberId:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setPremPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-wide p1, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    .line 100
    return-void
.end method

.method public setPremTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setRegCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->regCar:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setRegMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->regMemberId:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setRegPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-wide p1, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    .line 36
    return-void
.end method

.method public setRegTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    invoke-super {p0}, Lgbis/gbandroid/entities/Station;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string v1, "Regular price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 198
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 199
    const-string v1, "Regular time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    const-string v1, "Regular Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->regMemberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 205
    const-string v1, "Regular Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->regCar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 208
    const-string v1, "Midgrade price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 210
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 211
    const-string v1, "Midgrade time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 214
    const-string v1, "Midgrade Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->midMemberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 217
    const-string v1, "Midgrade Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->midCar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 220
    const-string v1, "Premium price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 223
    const-string v1, "Premium time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 226
    const-string v1, "Premium Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->premMemberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    const-string v1, "Premium Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->premCar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 232
    const-string v1, "Diesel price: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 234
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    const-string v1, "Diesel time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 238
    const-string v1, "Diesel Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselMemberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 241
    const-string v1, "Diesel Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    iget-object v1, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselCar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 244
    invoke-super {p0}, Lgbis/gbandroid/entities/Station;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 250
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/entities/Station;->writeToParcel(Landroid/os/Parcel;I)V

    .line 251
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 252
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 253
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 254
    iget-wide v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselPrice:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 255
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regTimeSpotted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regMemberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->regCar:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midTimeSpotted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midMemberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->midCar:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premTimeSpotted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premMemberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->premCar:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselTimeSpotted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselMemberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lgbis/gbandroid/entities/StationDetailed;->dieselCar:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 267
    return-void
.end method
