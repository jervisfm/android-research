.class public Lgbis/gbandroid/entities/StationMessage;
.super Lgbis/gbandroid/entities/StationDetailed;
.source "GBFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/StationMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private adsInLine:Ljava/util/List;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "InlineAds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AdInLine;",
            ">;"
        }
    .end annotation
.end field

.field private air:Z

.field private atm:Z
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "ATM"
    .end annotation
.end field

.field private cStore:Z

.field private carwash:Z

.field private diesel:Z

.field private dieselComments:Ljava/lang/String;

.field private e85:Z

.field private isFav:I

.field private midComments:Ljava/lang/String;

.field private midgradeGas:Z

.field private numPumps:I

.field private obsoletePriceTime:I

.field private open247:Z

.field private payAtPump:Z

.field private payphone:Z

.field private phone:Ljava/lang/String;

.field private photoMedium:Ljava/lang/String;

.field private premComments:Ljava/lang/String;

.field private premiumGas:Z

.field private priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

.field private priceValidation:Lgbis/gbandroid/entities/PriceValidation;

.field private priceValidationEncrypted:Ljava/lang/String;
    .annotation runtime Lcom/google/gbson/annotations/SerializedName;
        value = "Validation"
    .end annotation
.end field

.field private propane:Z

.field private regComments:Ljava/lang/String;

.field private regularGas:Z

.field private restaurant:Z

.field private restrooms:Z

.field private serviceStation:Z

.field private stationAlias:Ljava/lang/String;

.field private stationPhotos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationPhotos;",
            ">;"
        }
    .end annotation
.end field

.field private truckStop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 485
    new-instance v0, Lgbis/gbandroid/entities/n;

    invoke-direct {v0}, Lgbis/gbandroid/entities/n;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/StationMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 14
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 495
    invoke-direct {p0}, Lgbis/gbandroid/entities/StationDetailed;-><init>()V

    .line 496
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 498
    invoke-direct {p0}, Lgbis/gbandroid/entities/StationDetailed;-><init>()V

    .line 499
    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/StationMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 500
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 498
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/StationMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private decodePriceValidation(Ljava/lang/String;)Lgbis/gbandroid/entities/PriceValidation;
    .locals 4
    .parameter

    .prologue
    .line 315
    new-instance v1, Lgbis/gbandroid/entities/PriceValidation;

    invoke-direct {v1}, Lgbis/gbandroid/entities/PriceValidation;-><init>()V

    .line 317
    :try_start_0
    invoke-static {p1}, Lgbis/gbandroid/utils/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->decryptTripleDES([B)Ljava/lang/String;

    move-result-object v0

    .line 318
    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 319
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setExpectedRegularPrice(D)V

    .line 320
    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setCommonIncreaseRegular(D)V

    .line 321
    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setCommonDecreaseRegular(D)V

    .line 322
    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setOffsetMidgrade(D)V

    .line 323
    const/4 v2, 0x4

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setOffsetPremium(D)V

    .line 324
    const/4 v2, 0x5

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setExpectedDieselPrice(D)V

    .line 325
    const/4 v2, 0x6

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setCommonIncreaseDiesel(D)V

    .line 326
    const/4 v2, 0x7

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setCommonDecreaseDiesel(D)V

    .line 327
    const/16 v2, 0x8

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/entities/PriceValidation;->setRadius(D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    return-object v1

    .line 328
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/StationMessage;
    .locals 3

    .prologue
    .line 335
    new-instance v0, Lgbis/gbandroid/entities/StationMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/StationMessage;-><init>()V

    .line 336
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->address:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->address:Ljava/lang/String;

    .line 337
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    .line 338
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    .line 339
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    .line 340
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->city:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->city:Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->country:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->country:Ljava/lang/String;

    .line 342
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    .line 343
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->crossStreet:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->crossStreet:Ljava/lang/String;

    .line 344
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->dieselPrice:D

    .line 345
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->dieselTimeSpotted:Ljava/lang/String;

    .line 346
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->dieselMemberId:Ljava/lang/String;

    .line 347
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->dieselCar:Ljava/lang/String;

    .line 348
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    .line 349
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    .line 350
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    .line 351
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->gasBrandId:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->gasBrandId:I

    .line 352
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->gasBrandVersion:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->gasBrandVersion:I

    .line 353
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->stationId:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->stationId:I

    .line 354
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    .line 355
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->latitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->latitude:D

    .line 356
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->longitude:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->longitude:D

    .line 357
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    .line 358
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->midPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->midPrice:D

    .line 359
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->midTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->midTimeSpotted:Ljava/lang/String;

    .line 360
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->midMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->midMemberId:Ljava/lang/String;

    .line 361
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->midCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->midCar:Ljava/lang/String;

    .line 362
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    .line 363
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    .line 364
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->notIntersection:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->notIntersection:Z

    .line 365
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    .line 366
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    .line 367
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    .line 368
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    .line 369
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    .line 370
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    .line 371
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->postalCode:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->postalCode:Ljava/lang/String;

    .line 372
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->premPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->premPrice:D

    .line 373
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->premTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->premTimeSpotted:Ljava/lang/String;

    .line 374
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->premMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->premMemberId:Ljava/lang/String;

    .line 375
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->premCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->premCar:Ljava/lang/String;

    .line 376
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    .line 377
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    .line 378
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    .line 379
    iget-wide v1, p0, Lgbis/gbandroid/entities/StationMessage;->regPrice:D

    iput-wide v1, v0, Lgbis/gbandroid/entities/StationMessage;->regPrice:D

    .line 380
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->regTimeSpotted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->regTimeSpotted:Ljava/lang/String;

    .line 381
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->regMemberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->regMemberId:Ljava/lang/String;

    .line 382
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->regCar:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->regCar:Ljava/lang/String;

    .line 383
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    .line 384
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    .line 385
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    .line 386
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    .line 387
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    .line 388
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->state:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->state:Ljava/lang/String;

    .line 389
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->stationName:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->stationName:Ljava/lang/String;

    .line 390
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    .line 391
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->timeOffset:I

    iput v1, v0, Lgbis/gbandroid/entities/StationMessage;->timeOffset:I

    .line 392
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    .line 393
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    .line 394
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    .line 395
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    .line 396
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    .line 397
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x0

    return v0
.end method

.method public getAdsInLine()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AdInLine;",
            ">;"
        }
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    return-object v0
.end method

.method public getDieselComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    return-object v0
.end method

.method public getMidComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    return-object v0
.end method

.method public getNumPumps()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    return v0
.end method

.method public getObsoletePriceTime()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    return v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoMedium()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    return-object v0
.end method

.method public getPremComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    return-object v0
.end method

.method public getPriceShareMessages()Lgbis/gbandroid/entities/PriceShareMessages;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    return-object v0
.end method

.method public getPriceValidation()Lgbis/gbandroid/entities/PriceValidation;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    invoke-direct {p0, v0}, Lgbis/gbandroid/entities/StationMessage;->decodePriceValidation(Ljava/lang/String;)Lgbis/gbandroid/entities/PriceValidation;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    .line 311
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    return-object v0
.end method

.method public getRegComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    return-object v0
.end method

.method public getStationAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    return-object v0
.end method

.method public getStationPhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationPhotos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    return-object v0
.end method

.method public hasAir()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    return v0
.end method

.method public hasAtm()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    return v0
.end method

.method public hasCStore()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    return v0
.end method

.method public hasCarWash()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    return v0
.end method

.method public hasDiesel()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    return v0
.end method

.method public hasE85()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    return v0
.end method

.method public hasMidgradeGas()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    return v0
.end method

.method public hasOpen247()Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    return v0
.end method

.method public hasPayAtPump()Z
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    return v0
.end method

.method public hasPayphone()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    return v0
.end method

.method public hasPremiumGas()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    return v0
.end method

.method public hasPropane()Z
    .locals 1

    .prologue
    .line 253
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    return v0
.end method

.method public hasRegularGas()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    return v0
.end method

.method public hasRestaurant()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    return v0
.end method

.method public hasRestrooms()Z
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    return v0
.end method

.method public hasServiceStation()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    return v0
.end method

.method public hasTruckStop()Z
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    return v0
.end method

.method public isIsFav()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    return v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 531
    invoke-super {p0, p1}, Lgbis/gbandroid/entities/StationDetailed;->readFromParcel(Landroid/os/Parcel;)V

    .line 532
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    .line 533
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    .line 534
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    .line 535
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    .line 536
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    .line 537
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    .line 538
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    .line 539
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    .line 540
    const/16 v0, 0x11

    new-array v0, v0, [Z

    .line 541
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 542
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    .line 543
    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    .line 544
    const/4 v1, 0x2

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    .line 545
    const/4 v1, 0x3

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    .line 546
    const/4 v1, 0x4

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    .line 547
    const/4 v1, 0x5

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    .line 548
    const/4 v1, 0x6

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    .line 549
    const/4 v1, 0x7

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    .line 550
    const/16 v1, 0x8

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    .line 551
    const/16 v1, 0x9

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    .line 552
    const/16 v1, 0xa

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    .line 553
    const/16 v1, 0xb

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    .line 554
    const/16 v1, 0xc

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    .line 555
    const/16 v1, 0xd

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    .line 556
    const/16 v1, 0xe

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    .line 557
    const/16 v1, 0xf

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    .line 558
    const/16 v1, 0x10

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    .line 559
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    .line 560
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    .line 561
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    .line 562
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/StationPhotos;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 563
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    .line 564
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    const-class v1, Lgbis/gbandroid/entities/AdInLine;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 565
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    .line 566
    const-class v0, Lgbis/gbandroid/entities/PriceValidation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/PriceValidation;

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    .line 567
    const-class v0, Lgbis/gbandroid/entities/PriceShareMessages;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/PriceShareMessages;

    iput-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    .line 568
    return-void
.end method

.method public setAdsInLine(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AdInLine;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    .line 282
    return-void
.end method

.method public setAir(Z)V
    .locals 0
    .parameter

    .prologue
    .line 166
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    .line 167
    return-void
.end method

.method public setAtm(Z)V
    .locals 0
    .parameter

    .prologue
    .line 182
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    .line 183
    return-void
.end method

.method public setCStore(Z)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    .line 127
    return-void
.end method

.method public setCarWash(Z)V
    .locals 0
    .parameter

    .prologue
    .line 206
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    .line 207
    return-void
.end method

.method public setDiesel(Z)V
    .locals 0
    .parameter

    .prologue
    .line 233
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    .line 234
    return-void
.end method

.method public setDieselComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setE85(Z)V
    .locals 0
    .parameter

    .prologue
    .line 241
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    .line 242
    return-void
.end method

.method public setIsFav(I)V
    .locals 0
    .parameter

    .prologue
    .line 265
    iput p1, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    .line 266
    return-void
.end method

.method public setMidComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setMidgradeGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 217
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    .line 218
    return-void
.end method

.method public setNumPumps(I)V
    .locals 0
    .parameter

    .prologue
    .line 257
    iput p1, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    .line 258
    return-void
.end method

.method public setObsoletePriceTime(I)V
    .locals 0
    .parameter

    .prologue
    .line 114
    iput p1, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    .line 115
    return-void
.end method

.method public setOpen247(Z)V
    .locals 0
    .parameter

    .prologue
    .line 190
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    .line 191
    return-void
.end method

.method public setPayAtPump(Z)V
    .locals 0
    .parameter

    .prologue
    .line 142
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    .line 143
    return-void
.end method

.method public setPayphone(Z)V
    .locals 0
    .parameter

    .prologue
    .line 174
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    .line 175
    return-void
.end method

.method public setPhone(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setPhotoMedium(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setPremComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setPremiumGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 225
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    .line 226
    return-void
.end method

.method public setPriceShareMessages(Lgbis/gbandroid/entities/PriceShareMessages;)V
    .locals 0
    .parameter

    .prologue
    .line 297
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    .line 298
    return-void
.end method

.method public setPriceValidation(Lgbis/gbandroid/entities/PriceValidation;)V
    .locals 0
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    .line 306
    return-void
.end method

.method public setPriceValidationEncrypted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 289
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    .line 290
    return-void
.end method

.method public setPropane(Z)V
    .locals 0
    .parameter

    .prologue
    .line 249
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    .line 250
    return-void
.end method

.method public setRegComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setRegularGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 209
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    .line 210
    return-void
.end method

.method public setRestaurant(Z)V
    .locals 0
    .parameter

    .prologue
    .line 150
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    .line 151
    return-void
.end method

.method public setRestrooms(Z)V
    .locals 0
    .parameter

    .prologue
    .line 158
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    .line 159
    return-void
.end method

.method public setServiceStation(Z)V
    .locals 0
    .parameter

    .prologue
    .line 134
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    .line 135
    return-void
.end method

.method public setStationAlias(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setStationPhotos(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/StationPhotos;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    iput-object p1, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    .line 274
    return-void
.end method

.method public setTruckStop(Z)V
    .locals 0
    .parameter

    .prologue
    .line 198
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    .line 199
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 403
    invoke-super {p0}, Lgbis/gbandroid/entities/StationDetailed;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    const-string v1, "Regular Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 407
    const-string v1, "Midgrade Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 410
    const-string v1, "Premium Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 413
    const-string v1, "Diesel Comments: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 416
    const-string v1, "Photo Medium: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 419
    const-string v1, "C Store: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 421
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 422
    const-string v1, "Service Station: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 424
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 425
    const-string v1, "Pay at Pump: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 427
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 428
    const-string v1, "Restaurant: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 430
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 431
    const-string v1, "Restrooms: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 434
    const-string v1, "Air: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 436
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 437
    const-string v1, "ATM: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 439
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 440
    const-string v1, "Open_247: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 442
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 443
    const-string v1, "Truck Stop: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 445
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 446
    const-string v1, "Car Wash: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 449
    const-string v1, "Regular Gas: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 451
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 452
    const-string v1, "Midgrade Gas: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 454
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 455
    const-string v1, "Premium Gas: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 456
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 457
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 458
    const-string v1, "Diesel Gas: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    iget-boolean v1, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 460
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 461
    const-string v1, "Number of Pumps: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 463
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 464
    const-string v1, "Is Favourite: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 466
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 467
    const-string v1, "Time Offset: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->timeOffset:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 469
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 470
    const-string v1, "Obsolete Price Time: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    iget v1, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 472
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 473
    const-string v1, "Ads In Line: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 474
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 475
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 476
    const-string v1, "Price Validation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 479
    const-string v1, "Price Share Messages: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    iget-object v1, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 481
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 482
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 509
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/entities/StationDetailed;->writeToParcel(Landroid/os/Parcel;I)V

    .line 510
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->photoMedium:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->phone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationAlias:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 513
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->regComments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->midComments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->premComments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->dieselComments:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 517
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->obsoletePriceTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 518
    const/16 v0, 0x11

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->cStore:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->serviceStation:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->payAtPump:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->restaurant:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->restrooms:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->air:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->payphone:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x7

    .line 519
    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->atm:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->open247:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->truckStop:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->carwash:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->regularGas:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->midgradeGas:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->premiumGas:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xe

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->diesel:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->e85:Z

    aput-boolean v2, v0, v1

    const/16 v1, 0x10

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationMessage;->propane:Z

    aput-boolean v2, v0, v1

    .line 518
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 520
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->numPumps:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 521
    iget v0, p0, Lgbis/gbandroid/entities/StationMessage;->isFav:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 522
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->stationPhotos:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 523
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->adsInLine:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 524
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidationEncrypted:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 525
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceValidation:Lgbis/gbandroid/entities/PriceValidation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 526
    iget-object v0, p0, Lgbis/gbandroid/entities/StationMessage;->priceShareMessages:Lgbis/gbandroid/entities/PriceShareMessages;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 527
    return-void
.end method
