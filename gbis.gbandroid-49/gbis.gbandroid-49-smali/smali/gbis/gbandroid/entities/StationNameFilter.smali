.class public Lgbis/gbandroid/entities/StationNameFilter;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/StationNameFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private included:Z

.field private stationName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lgbis/gbandroid/entities/o;

    invoke-direct {v0}, Lgbis/gbandroid/entities/o;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/StationNameFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/StationNameFilter;->readFromParcel(Landroid/os/Parcel;)V

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/StationNameFilter;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lgbis/gbandroid/entities/StationNameFilter;->stationName:Ljava/lang/String;

    .line 12
    iput-boolean p2, p0, Lgbis/gbandroid/entities/StationNameFilter;->included:Z

    .line 13
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationNameFilter;->stationName:Ljava/lang/String;

    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 62
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 63
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lgbis/gbandroid/entities/StationNameFilter;->included:Z

    .line 64
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public getStationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lgbis/gbandroid/entities/StationNameFilter;->stationName:Ljava/lang/String;

    return-object v0
.end method

.method public isIncluded()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lgbis/gbandroid/entities/StationNameFilter;->included:Z

    return v0
.end method

.method public setIncluded(Z)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-boolean p1, p0, Lgbis/gbandroid/entities/StationNameFilter;->included:Z

    .line 29
    return-void
.end method

.method public setStationName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lgbis/gbandroid/entities/StationNameFilter;->stationName:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/entities/StationNameFilter;->stationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lgbis/gbandroid/entities/StationNameFilter;->included:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 57
    return-void
.end method
