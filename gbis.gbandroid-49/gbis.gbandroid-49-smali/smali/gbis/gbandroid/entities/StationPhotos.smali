.class public Lgbis/gbandroid/entities/StationPhotos;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lgbis/gbandroid/entities/StationPhotos;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private caption:Ljava/lang/String;

.field private carIcon:Ljava/lang/String;

.field private mediumPath:Ljava/lang/String;

.field private memberId:Ljava/lang/String;

.field private photoId:I

.field private smallPath:Ljava/lang/String;

.field private thumbnailPath:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lgbis/gbandroid/entities/p;

    invoke-direct {v0}, Lgbis/gbandroid/entities/p;-><init>()V

    sput-object v0, Lgbis/gbandroid/entities/StationPhotos;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/StationPhotos;->readFromParcel(Landroid/os/Parcel;)V

    .line 84
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lgbis/gbandroid/entities/StationPhotos;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    .line 122
    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/StationPhotos;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lgbis/gbandroid/entities/StationPhotos;

    invoke-direct {v0}, Lgbis/gbandroid/entities/StationPhotos;-><init>()V

    .line 67
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    .line 69
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    .line 71
    iget v1, p0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    iput v1, v0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    .line 72
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    .line 74
    iget-object v1, p0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    .line 75
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getMediumPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    return v0
.end method

.method public getSmallPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCarIcon(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setMediumPath(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setPhotoId(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 20
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    .line 21
    return-void
.end method

.method public setSmallPath(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setThumbnailPath(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 103
    iget v0, p0, Lgbis/gbandroid/entities/StationPhotos;->photoId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->thumbnailPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->smallPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->mediumPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->caption:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->memberId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lgbis/gbandroid/entities/StationPhotos;->carIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    return-void
.end method
