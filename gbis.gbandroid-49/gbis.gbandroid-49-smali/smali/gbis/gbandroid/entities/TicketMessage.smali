.class public Lgbis/gbandroid/entities/TicketMessage;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private pointBalance:I

.field private tickets:I

.field private totalTickets:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/TicketMessage;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lgbis/gbandroid/entities/TicketMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/TicketMessage;-><init>()V

    .line 35
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->tickets:I

    iput v1, v0, Lgbis/gbandroid/entities/TicketMessage;->tickets:I

    .line 36
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->totalTickets:I

    iput v1, v0, Lgbis/gbandroid/entities/TicketMessage;->totalTickets:I

    .line 37
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->pointBalance:I

    iput v1, v0, Lgbis/gbandroid/entities/TicketMessage;->pointBalance:I

    .line 38
    return-object v0
.end method

.method public getPointBalance()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lgbis/gbandroid/entities/TicketMessage;->pointBalance:I

    return v0
.end method

.method public getTickets()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lgbis/gbandroid/entities/TicketMessage;->tickets:I

    return v0
.end method

.method public getTotalTickets()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lgbis/gbandroid/entities/TicketMessage;->totalTickets:I

    return v0
.end method

.method public setPointBalance(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/TicketMessage;->pointBalance:I

    .line 31
    return-void
.end method

.method public setTickets(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 14
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/TicketMessage;->tickets:I

    .line 15
    return-void
.end method

.method public setTotalTickets(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/TicketMessage;->totalTickets:I

    .line 23
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    const-string v1, "Tickets: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->tickets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    const-string v1, "Total Tickets: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->totalTickets:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 50
    const-string v1, "Point Balance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget v1, p0, Lgbis/gbandroid/entities/TicketMessage;->pointBalance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
