.class public Lgbis/gbandroid/entities/WinnersMessage;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lgbis/gbandroid/entities/WinnersMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private car:Ljava/lang/String;

.field private cityTitle:Ljava/lang/String;

.field private endDate:Ljava/lang/String;

.field private imgUrl:Ljava/lang/String;

.field private memberId:Ljava/lang/String;

.field private prize:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lgbis/gbandroid/entities/WinnersMessage;)I
    .locals 1
    .parameter

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, Lgbis/gbandroid/entities/WinnersMessage;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/entities/WinnersMessage;->compareTo(Lgbis/gbandroid/entities/WinnersMessage;)I

    move-result v0

    return v0
.end method

.method public copy()Lgbis/gbandroid/entities/WinnersMessage;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lgbis/gbandroid/entities/WinnersMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/WinnersMessage;-><init>()V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->car:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->car:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->cityTitle:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->cityTitle:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->endDate:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->endDate:Ljava/lang/String;

    .line 65
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->imgUrl:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->imgUrl:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->memberId:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->memberId:Ljava/lang/String;

    .line 67
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->prize:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/WinnersMessage;->prize:Ljava/lang/String;

    .line 68
    return-object v0
.end method

.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCityTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->cityTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getEndDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->endDate:Ljava/lang/String;

    return-object v0
.end method

.method public getImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->imgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public getPrize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/entities/WinnersMessage;->prize:Ljava/lang/String;

    return-object v0
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->car:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setCityTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->cityTitle:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setEndDate(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->endDate:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setImgUrl(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->imgUrl:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->memberId:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public setPrize(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lgbis/gbandroid/entities/WinnersMessage;->prize:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    const-string v1, "Member Id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->memberId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 77
    const-string v1, "Car: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->car:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, "City Title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->cityTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "End Date: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->endDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 86
    const-string v1, "Prize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->prize:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, "Image URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    iget-object v1, p0, Lgbis/gbandroid/entities/WinnersMessage;->imgUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
