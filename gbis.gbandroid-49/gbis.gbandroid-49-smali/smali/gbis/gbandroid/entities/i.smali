.class final Lgbis/gbandroid/entities/i;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lgbis/gbandroid/entities/ListStationTab;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method

.method private static createFromParcel(Landroid/os/Parcel;)Lgbis/gbandroid/entities/ListStationTab;
    .locals 2
    .parameter

    .prologue
    .line 92
    new-instance v0, Lgbis/gbandroid/entities/ListStationTab;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/entities/ListStationTab;-><init>(Landroid/os/Parcel;B)V

    return-object v0
.end method

.method private static newArray(I)[Lgbis/gbandroid/entities/ListStationTab;
    .locals 1
    .parameter

    .prologue
    .line 96
    new-array v0, p0, [Lgbis/gbandroid/entities/ListStationTab;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-static {p1}, Lgbis/gbandroid/entities/i;->createFromParcel(Landroid/os/Parcel;)Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-static {p1}, Lgbis/gbandroid/entities/i;->newArray(I)[Lgbis/gbandroid/entities/ListStationTab;

    move-result-object v0

    return-object v0
.end method
