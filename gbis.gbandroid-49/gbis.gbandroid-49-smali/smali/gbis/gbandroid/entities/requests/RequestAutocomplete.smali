.class public Lgbis/gbandroid/entities/requests/RequestAutocomplete;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private numberToReturn:I

.field private searchText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->searchText:Ljava/lang/String;

    .line 9
    iput p2, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->numberToReturn:I

    .line 10
    return-void
.end method


# virtual methods
.method public getNumberToReturn()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->numberToReturn:I

    return v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public setNumberToReturn(I)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->numberToReturn:I

    .line 26
    return-void
.end method

.method public setSearchText(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestAutocomplete;->searchText:Ljava/lang/String;

    .line 18
    return-void
.end method
