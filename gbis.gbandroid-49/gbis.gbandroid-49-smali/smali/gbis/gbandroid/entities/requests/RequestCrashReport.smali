.class public Lgbis/gbandroid/entities/requests/RequestCrashReport;
.super Lgbis/gbandroid/entities/requests/RequestMessage;
.source "GBFile"


# instance fields
.field private deviceId:Ljava/lang/String;

.field private osName:Ljava/lang/String;

.field private osVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getOsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->osName:Ljava/lang/String;

    return-object v0
.end method

.method public getOsVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->osVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->deviceId:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setOsName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->osName:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public setOsVersion(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestCrashReport;->osVersion:Ljava/lang/String;

    .line 22
    return-void
.end method
