.class public Lgbis/gbandroid/entities/requests/RequestFavAddList;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private defaultList:Z

.field private listName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestFavAddList;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultList()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestFavAddList;->defaultList:Z

    return v0
.end method

.method public setDefaultList(Z)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestFavAddList;->defaultList:Z

    .line 21
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestFavAddList;->listName:Ljava/lang/String;

    .line 13
    return-void
.end method
