.class public Lgbis/gbandroid/entities/requests/RequestFavAddStation;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private listId:I

.field private listName:Ljava/lang/String;

.field private stationId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getListId()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->listId:I

    return v0
.end method

.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public getStationId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->stationId:I

    return v0
.end method

.method public setListId(I)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->listId:I

    .line 14
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->listName:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setStationId(I)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->stationId:I

    .line 22
    return-void
.end method
