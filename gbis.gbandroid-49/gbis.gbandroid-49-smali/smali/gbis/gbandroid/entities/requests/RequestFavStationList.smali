.class public Lgbis/gbandroid/entities/requests/RequestFavStationList;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private itemsPerPage:I

.field private listId:I

.field private pageNumber:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemsPerPage()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->itemsPerPage:I

    return v0
.end method

.method public getListId()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->listId:I

    return v0
.end method

.method public getPageNumber()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->pageNumber:I

    return v0
.end method

.method public setItemsPerPage(I)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->itemsPerPage:I

    .line 30
    return-void
.end method

.method public setListId(I)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->listId:I

    .line 14
    return-void
.end method

.method public setPageNumber(I)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestFavStationList;->pageNumber:I

    .line 22
    return-void
.end method
