.class public Lgbis/gbandroid/entities/requests/RequestFeedback;
.super Lgbis/gbandroid/entities/requests/RequestMessage;
.source "GBFile"


# instance fields
.field private email:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestFeedback;->email:Ljava/lang/String;

    return-object v0
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 11
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestFeedback;->email:Ljava/lang/String;

    .line 12
    return-void
.end method
