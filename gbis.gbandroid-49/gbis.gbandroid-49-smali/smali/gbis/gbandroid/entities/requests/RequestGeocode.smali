.class public Lgbis/gbandroid/entities/requests/RequestGeocode;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private address:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 14
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->address:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->city:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->postalCode:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestGeocode;->state:Ljava/lang/String;

    .line 31
    return-void
.end method
