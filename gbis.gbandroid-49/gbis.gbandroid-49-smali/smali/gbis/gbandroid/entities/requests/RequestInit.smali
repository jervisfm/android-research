.class public Lgbis/gbandroid/entities/requests/RequestInit;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private destributionMethod:I

.field private deviceId:Ljava/lang/String;

.field private deviceModel:Ljava/lang/String;

.field private osName:Ljava/lang/String;

.field private osVersion:Ljava/lang/String;

.field private referrer:Ljava/lang/String;

.field private resolution:Ljava/lang/String;

.field private userToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDestributionMethod()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->destributionMethod:I

    return v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->deviceModel:Ljava/lang/String;

    return-object v0
.end method

.method public getOsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->osName:Ljava/lang/String;

    return-object v0
.end method

.method public getOsVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->osVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getReferrer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->referrer:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getUserToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestInit;->userToken:Ljava/lang/String;

    return-object v0
.end method

.method public setDestributionMethod(I)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->destributionMethod:I

    .line 59
    return-void
.end method

.method public setDeviceId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->deviceId:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->deviceModel:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setOsName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->osName:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public setOsVersion(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->osVersion:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setReferrer(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->referrer:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->resolution:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setUserToken(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestInit;->userToken:Ljava/lang/String;

    .line 75
    return-void
.end method
