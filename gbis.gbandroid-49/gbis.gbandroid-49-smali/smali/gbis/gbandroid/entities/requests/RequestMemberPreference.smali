.class public Lgbis/gbandroid/entities/requests/RequestMemberPreference;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private name:Ljava/lang/String;

.field private value:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isValue()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->value:Z

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->name:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public setValue(Z)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestMemberPreference;->value:Z

    .line 21
    return-void
.end method
