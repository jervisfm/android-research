.class public Lgbis/gbandroid/entities/requests/RequestMemberRegister;
.super Lgbis/gbandroid/entities/requests/RequestMemberLogin;
.source "GBFile"


# instance fields
.field private email:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestMemberLogin;-><init>()V

    return-void
.end method


# virtual methods
.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestMemberRegister;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestMemberRegister;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestMemberRegister;->email:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestMemberRegister;->postalCode:Ljava/lang/String;

    .line 13
    return-void
.end method
