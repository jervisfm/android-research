.class public Lgbis/gbandroid/entities/requests/RequestObject$Location;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/entities/requests/RequestObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Location"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/entities/requests/RequestObject;

.field private accuracy:F

.field private latitude:D

.field private longitude:D


# direct methods
.method public constructor <init>(Lgbis/gbandroid/entities/requests/RequestObject;Landroid/location/Location;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 123
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->a:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->latitude:D

    .line 125
    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->longitude:D

    .line 126
    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->accuracy:F

    .line 127
    return-void
.end method


# virtual methods
.method public getAccuracy()F
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->accuracy:F

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->longitude:D

    return-wide v0
.end method

.method public setAccuracy(F)V
    .locals 0
    .parameter

    .prologue
    .line 150
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->accuracy:F

    .line 151
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 134
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->latitude:D

    .line 135
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 142
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestObject$Location;->longitude:D

    .line 143
    return-void
.end method
