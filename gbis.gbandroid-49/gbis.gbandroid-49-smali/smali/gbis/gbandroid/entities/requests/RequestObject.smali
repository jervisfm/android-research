.class public Lgbis/gbandroid/entities/requests/RequestObject;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/entities/requests/RequestObject$Location;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private appSource:I

.field private appVersion:D

.field private authId:Ljava/lang/String;

.field private dateDevice:Ljava/lang/String;

.field private dateEastern:Ljava/lang/String;

.field private debug:Z

.field private key:Ljava/lang/String;

.field private memberId:Ljava/lang/String;

.field private parameters:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private source:I

.field private userLocation:Lgbis/gbandroid/entities/requests/RequestObject$Location;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/requests/RequestObject",
            "<TT;>.",
            "Location;"
        }
    .end annotation
.end field

.field private webServiceVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppSource()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->appSource:I

    return v0
.end method

.method public getAppVersion()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->appVersion:D

    return-wide v0
.end method

.method public getAuthId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->authId:Ljava/lang/String;

    return-object v0
.end method

.method public getDateDevice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->dateDevice:Ljava/lang/String;

    return-object v0
.end method

.method public getDateEastern()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->dateEastern:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->memberId:Ljava/lang/String;

    return-object v0
.end method

.method public getParameters()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->parameters:Ljava/lang/Object;

    return-object v0
.end method

.method public getSource()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->source:I

    return v0
.end method

.method public getUserLocation()Lgbis/gbandroid/entities/requests/RequestObject$Location;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/requests/RequestObject",
            "<TT;>.",
            "Location;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->userLocation:Lgbis/gbandroid/entities/requests/RequestObject$Location;

    return-object v0
.end method

.method public getWebServiceVersion()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->webServiceVersion:I

    return v0
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->debug:Z

    return v0
.end method

.method public setAppSource(I)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->appSource:I

    .line 99
    return-void
.end method

.method public setAppVersion(D)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->appVersion:D

    .line 39
    return-void
.end method

.method public setAuthId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->authId:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setDateDevice(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->dateDevice:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setDateEastern(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->dateEastern:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->debug:Z

    .line 107
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->key:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setMemberId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->memberId:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setParameters(Ljava/lang/Object;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->parameters:Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public setSource(I)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->source:I

    .line 55
    return-void
.end method

.method public setUserLocation(Landroid/location/Location;)V
    .locals 1
    .parameter

    .prologue
    .line 82
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestObject$Location;

    invoke-direct {v0, p0, p1}, Lgbis/gbandroid/entities/requests/RequestObject$Location;-><init>(Lgbis/gbandroid/entities/requests/RequestObject;Landroid/location/Location;)V

    iput-object v0, p0, Lgbis/gbandroid/entities/requests/RequestObject;->userLocation:Lgbis/gbandroid/entities/requests/RequestObject$Location;

    .line 83
    return-void
.end method

.method public setWebServiceVersion(I)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestObject;->webServiceVersion:I

    .line 47
    return-void
.end method
