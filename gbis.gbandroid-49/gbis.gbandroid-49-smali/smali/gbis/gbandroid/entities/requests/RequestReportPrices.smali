.class public Lgbis/gbandroid/entities/requests/RequestReportPrices;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private car:Ljava/lang/String;

.field private carIconId:I

.field private comments:Ljava/lang/String;

.field private dieselPrice:D

.field private midgradePrice:D

.field private premiumPrice:D

.field private regularPrice:D

.field private stationId:I

.field private timeSpotted:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCar()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->car:Ljava/lang/String;

    return-object v0
.end method

.method public getCarIconId()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->carIconId:I

    return v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->comments:Ljava/lang/String;

    return-object v0
.end method

.method public getDieselPrice()D
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->dieselPrice:D

    return-wide v0
.end method

.method public getMidgradePrice()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->midgradePrice:D

    return-wide v0
.end method

.method public getPremiumPrice()D
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->premiumPrice:D

    return-wide v0
.end method

.method public getRegularPrice()D
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->regularPrice:D

    return-wide v0
.end method

.method public getStationId()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->stationId:I

    return v0
.end method

.method public getTimeSpotted()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->timeSpotted:Ljava/lang/String;

    return-object v0
.end method

.method public setCar(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->car:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setCarIconId(I)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->carIconId:I

    .line 84
    return-void
.end method

.method public setComments(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->comments:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setDieselPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->dieselPrice:D

    .line 52
    return-void
.end method

.method public setMidgradePrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->midgradePrice:D

    .line 36
    return-void
.end method

.method public setPremiumPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->premiumPrice:D

    .line 44
    return-void
.end method

.method public setRegularPrice(D)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->regularPrice:D

    .line 28
    return-void
.end method

.method public setStationId(I)V
    .locals 0
    .parameter

    .prologue
    .line 19
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->stationId:I

    .line 20
    return-void
.end method

.method public setTimeSpotted(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPrices;->timeSpotted:Ljava/lang/String;

    .line 60
    return-void
.end method
