.class public Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;
.super Lgbis/gbandroid/entities/requests/RequestReportPrices;
.source "GBFile"


# instance fields
.field private favoriteId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestReportPrices;-><init>()V

    return-void
.end method


# virtual methods
.method public getFavoriteId()I
    .locals 1

    .prologue
    .line 7
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->favoriteId:I

    return v0
.end method

.method public setFavoriteId(I)V
    .locals 0
    .parameter

    .prologue
    .line 11
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->favoriteId:I

    .line 12
    return-void
.end method
