.class public Lgbis/gbandroid/entities/requests/RequestShareAction;
.super Lgbis/gbandroid/entities/requests/RequestShareMessage;
.source "GBFile"


# instance fields
.field private action:I

.field private shareMessage:Ljava/lang/String;

.field private stationId:I

.field private userMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestShareMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getAction()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->action:I

    return v0
.end method

.method public getShareMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->shareMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getStationId()I
    .locals 1

    .prologue
    .line 10
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->stationId:I

    return v0
.end method

.method public getUserMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->userMessage:Ljava/lang/String;

    return-object v0
.end method

.method public setAction(I)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->action:I

    .line 23
    return-void
.end method

.method public setShareMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->shareMessage:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setStationId(I)V
    .locals 0
    .parameter

    .prologue
    .line 14
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->stationId:I

    .line 15
    return-void
.end method

.method public setUserMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestShareAction;->userMessage:Ljava/lang/String;

    .line 39
    return-void
.end method
