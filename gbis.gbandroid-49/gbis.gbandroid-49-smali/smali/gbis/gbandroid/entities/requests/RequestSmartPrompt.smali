.class public Lgbis/gbandroid/entities/requests/RequestSmartPrompt;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private promptId:I

.field private showAgain:Z

.field private userResponse:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copy()Lgbis/gbandroid/entities/requests/RequestSmartPrompt;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;-><init>()V

    .line 34
    iget-object v1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->userResponse:Ljava/lang/String;

    iput-object v1, v0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->userResponse:Ljava/lang/String;

    .line 35
    iget v1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->promptId:I

    iput v1, v0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->promptId:I

    .line 36
    iget-boolean v1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->showAgain:Z

    iput-boolean v1, v0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->showAgain:Z

    .line 37
    return-object v0
.end method

.method public getPromptId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->promptId:I

    return v0
.end method

.method public getUserResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->userResponse:Ljava/lang/String;

    return-object v0
.end method

.method public isShowAgain()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->showAgain:Z

    return v0
.end method

.method public setPromptId(I)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->promptId:I

    .line 22
    return-void
.end method

.method public setShowAgain(Z)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->showAgain:Z

    .line 30
    return-void
.end method

.method public setUserResponse(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->userResponse:Ljava/lang/String;

    .line 14
    return-void
.end method
