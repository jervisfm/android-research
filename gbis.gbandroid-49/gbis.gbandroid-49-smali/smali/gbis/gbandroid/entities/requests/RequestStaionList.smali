.class public Lgbis/gbandroid/entities/requests/RequestStaionList;
.super Lgbis/gbandroid/entities/requests/RequestStationCollection;
.source "GBFile"


# instance fields
.field private city:Ljava/lang/String;

.field private distance:I

.field private latitude:D

.field private longitude:D

.field private numberToReturn:I

.field private pageNumber:I

.field private postalCode:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestStationCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->distance:I

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 14
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->longitude:D

    return-wide v0
.end method

.method public getNumberToReturn()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->numberToReturn:I

    return v0
.end method

.method public getPageNumber()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->pageNumber:I

    return v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->city:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setDistance(I)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->distance:I

    .line 59
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 18
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->latitude:D

    .line 19
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->longitude:D

    .line 27
    return-void
.end method

.method public setNumberToReturn(I)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->numberToReturn:I

    .line 67
    return-void
.end method

.method public setPageNumber(I)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->pageNumber:I

    .line 75
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->postalCode:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStaionList;->state:Ljava/lang/String;

    .line 43
    return-void
.end method
