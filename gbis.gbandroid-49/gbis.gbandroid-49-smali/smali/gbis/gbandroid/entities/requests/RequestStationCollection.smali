.class public Lgbis/gbandroid/entities/requests/RequestStationCollection;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private fuelType:Ljava/lang/String;

.field private pricesRequired:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFuelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationCollection;->fuelType:Ljava/lang/String;

    return-object v0
.end method

.method public isPricesRequired()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationCollection;->pricesRequired:Z

    return v0
.end method

.method public setFuelType(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationCollection;->fuelType:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public setPricesRequired(Z)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationCollection;->pricesRequired:Z

    .line 21
    return-void
.end method
