.class public Lgbis/gbandroid/entities/requests/RequestStationDetails;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private resolution:Ljava/lang/String;

.field private stationId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationDetails;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getStationId()I
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationDetails;->stationId:I

    return v0
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationDetails;->resolution:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public setStationId(I)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationDetails;->stationId:I

    .line 13
    return-void
.end method
