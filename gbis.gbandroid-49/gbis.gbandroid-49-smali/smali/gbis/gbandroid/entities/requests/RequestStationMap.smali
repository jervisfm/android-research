.class public Lgbis/gbandroid/entities/requests/RequestStationMap;
.super Lgbis/gbandroid/entities/requests/RequestStationCollection;
.source "GBFile"


# instance fields
.field private northWestLatitude:D

.field private northWestLongitude:D

.field private southEastLatitude:D

.field private southEastLongitude:D

.field private viewportHeight:I

.field private viewportPixelRatio:D

.field private viewportWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestStationCollection;-><init>()V

    return-void
.end method


# virtual methods
.method public getNorthWestLatitude()D
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->northWestLatitude:D

    return-wide v0
.end method

.method public getNorthWestLongitude()D
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->northWestLongitude:D

    return-wide v0
.end method

.method public getSouthEastLatitude()D
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->southEastLatitude:D

    return-wide v0
.end method

.method public getSouthEastLongitude()D
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->southEastLongitude:D

    return-wide v0
.end method

.method public getViewportHeight()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportHeight:I

    return v0
.end method

.method public getViewportPixelRatio()D
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportPixelRatio:D

    return-wide v0
.end method

.method public getViewportWidth()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportWidth:I

    return v0
.end method

.method public setNorthWestLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 17
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->northWestLatitude:D

    .line 18
    return-void
.end method

.method public setNorthWestLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->northWestLongitude:D

    .line 26
    return-void
.end method

.method public setSouthEastLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->southEastLatitude:D

    .line 34
    return-void
.end method

.method public setSouthEastLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->southEastLongitude:D

    .line 42
    return-void
.end method

.method public setViewportHeight(I)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportHeight:I

    .line 58
    return-void
.end method

.method public setViewportPixelRatio(D)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportPixelRatio:D

    .line 66
    return-void
.end method

.method public setViewportWidth(I)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationMap;->viewportWidth:I

    .line 50
    return-void
.end method
