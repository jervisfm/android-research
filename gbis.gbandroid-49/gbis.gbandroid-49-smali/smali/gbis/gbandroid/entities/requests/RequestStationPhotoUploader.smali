.class public Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;
.super Lgbis/gbandroid/entities/requests/RequestMessage;
.source "GBFile"


# instance fields
.field private description:Ljava/lang/String;

.field private stationId:I

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lgbis/gbandroid/entities/requests/RequestMessage;-><init>()V

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getStationId()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->stationId:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->description:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setStationId(I)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->stationId:I

    .line 14
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->title:Ljava/lang/String;

    .line 22
    return-void
.end method
