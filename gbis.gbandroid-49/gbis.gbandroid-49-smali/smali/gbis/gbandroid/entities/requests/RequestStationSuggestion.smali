.class public Lgbis/gbandroid/entities/requests/RequestStationSuggestion;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private address:Ljava/lang/String;

.field private air:Z

.field private atm:Z

.field private cStore:Z

.field private carWash:Z

.field private city:Ljava/lang/String;

.field private crossStreet:Ljava/lang/String;

.field private diesel:Z

.field private e85:Z

.field private fuelBrand:Ljava/lang/String;

.field private latitude:D

.field private longitude:D

.field private midgradeGas:Z

.field private numberOfPumps:I

.field private open247:Z

.field private overrideInsert:Z

.field private payAtPump:Z

.field private payphone:Z

.field private phone:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private premiumGas:Z

.field private propane:Z

.field private regularGas:Z

.field private restaurant:Z

.field private restrooms:Z

.field private searchFuelType:Ljava/lang/String;

.field private searchTerms:Ljava/lang/String;

.field private searchType:I

.field private serviceStation:Z

.field private state:Ljava/lang/String;

.field private stationName:Ljava/lang/String;

.field private truckStop:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getCrossStreet()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->crossStreet:Ljava/lang/String;

    return-object v0
.end method

.method public getFuelBrand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->fuelBrand:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 247
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 255
    iget-wide v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->longitude:D

    return-wide v0
.end method

.method public getNumberOfPumps()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->numberOfPumps:I

    return v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFuelType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchFuelType:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchTerms()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchTerms:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchType()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchType:I

    return v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getStationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->stationName:Ljava/lang/String;

    return-object v0
.end method

.method public isAir()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->air:Z

    return v0
.end method

.method public isAtm()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->atm:Z

    return v0
.end method

.method public isCStore()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->cStore:Z

    return v0
.end method

.method public isCarWash()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->carWash:Z

    return v0
.end method

.method public isDiesel()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->diesel:Z

    return v0
.end method

.method public isE85()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->e85:Z

    return v0
.end method

.method public isMidgradeGas()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->midgradeGas:Z

    return v0
.end method

.method public isOpen247()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->open247:Z

    return v0
.end method

.method public isOverrideInsert()Z
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->overrideInsert:Z

    return v0
.end method

.method public isPayAtPump()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->payAtPump:Z

    return v0
.end method

.method public isPayphone()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->payphone:Z

    return v0
.end method

.method public isPremiumGas()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->premiumGas:Z

    return v0
.end method

.method public isPropane()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->propane:Z

    return v0
.end method

.method public isRegularGas()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->regularGas:Z

    return v0
.end method

.method public isRestaurant()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->restaurant:Z

    return v0
.end method

.method public isRestrooms()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->restrooms:Z

    return v0
.end method

.method public isServiceStation()Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->serviceStation:Z

    return v0
.end method

.method public isTruckStop()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->truckStop:Z

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->address:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setAir(Z)V
    .locals 0
    .parameter

    .prologue
    .line 195
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->air:Z

    .line 196
    return-void
.end method

.method public setAtm(Z)V
    .locals 0
    .parameter

    .prologue
    .line 211
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->atm:Z

    .line 212
    return-void
.end method

.method public setCStore(Z)V
    .locals 0
    .parameter

    .prologue
    .line 155
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->cStore:Z

    .line 156
    return-void
.end method

.method public setCarWash(Z)V
    .locals 0
    .parameter

    .prologue
    .line 235
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->carWash:Z

    .line 236
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->city:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setCrossStreet(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->crossStreet:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setDiesel(Z)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->diesel:Z

    .line 132
    return-void
.end method

.method public setE85(Z)V
    .locals 0
    .parameter

    .prologue
    .line 139
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->e85:Z

    .line 140
    return-void
.end method

.method public setFuelBrand(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->fuelBrand:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setLatitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 251
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->latitude:D

    .line 252
    return-void
.end method

.method public setLongitude(D)V
    .locals 0
    .parameter

    .prologue
    .line 259
    iput-wide p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->longitude:D

    .line 260
    return-void
.end method

.method public setMidgradeGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->midgradeGas:Z

    .line 116
    return-void
.end method

.method public setNumberOfPumps(I)V
    .locals 0
    .parameter

    .prologue
    .line 243
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->numberOfPumps:I

    .line 244
    return-void
.end method

.method public setOpen247(Z)V
    .locals 0
    .parameter

    .prologue
    .line 219
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->open247:Z

    .line 220
    return-void
.end method

.method public setOverrideInsert(Z)V
    .locals 0
    .parameter

    .prologue
    .line 291
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->overrideInsert:Z

    .line 292
    return-void
.end method

.method public setPayAtPump(Z)V
    .locals 0
    .parameter

    .prologue
    .line 171
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->payAtPump:Z

    .line 172
    return-void
.end method

.method public setPayphone(Z)V
    .locals 0
    .parameter

    .prologue
    .line 203
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->payphone:Z

    .line 204
    return-void
.end method

.method public setPhone(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->phone:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->postalCode:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setPremiumGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 123
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->premiumGas:Z

    .line 124
    return-void
.end method

.method public setPropane(Z)V
    .locals 0
    .parameter

    .prologue
    .line 147
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->propane:Z

    .line 148
    return-void
.end method

.method public setRegularGas(Z)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->regularGas:Z

    .line 108
    return-void
.end method

.method public setRestaurant(Z)V
    .locals 0
    .parameter

    .prologue
    .line 179
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->restaurant:Z

    .line 180
    return-void
.end method

.method public setRestrooms(Z)V
    .locals 0
    .parameter

    .prologue
    .line 187
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->restrooms:Z

    .line 188
    return-void
.end method

.method public setSearchFuelType(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 283
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchFuelType:Ljava/lang/String;

    .line 284
    return-void
.end method

.method public setSearchTerms(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 267
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchTerms:Ljava/lang/String;

    .line 268
    return-void
.end method

.method public setSearchType(I)V
    .locals 0
    .parameter

    .prologue
    .line 275
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->searchType:I

    .line 276
    return-void
.end method

.method public setServiceStation(Z)V
    .locals 0
    .parameter

    .prologue
    .line 163
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->serviceStation:Z

    .line 164
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->state:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setStationName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->stationName:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setTruckStop(Z)V
    .locals 0
    .parameter

    .prologue
    .line 227
    iput-boolean p1, p0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->truckStop:Z

    .line 228
    return-void
.end method
