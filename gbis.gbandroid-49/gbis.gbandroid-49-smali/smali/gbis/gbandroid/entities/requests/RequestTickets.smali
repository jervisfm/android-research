.class public Lgbis/gbandroid/entities/requests/RequestTickets;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private address:Ljava/lang/String;

.field private address2:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private lastName:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private tickets:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getAddress2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->address2:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->city:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->lastName:Ljava/lang/String;

    return-object v0
.end method

.method public getPostalCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getTickets()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->tickets:I

    return v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->address:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setAddress2(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->address2:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->city:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->email:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->firstName:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->lastName:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setPostalCode(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->postalCode:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->state:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setTickets(I)V
    .locals 0
    .parameter

    .prologue
    .line 19
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestTickets;->tickets:I

    .line 20
    return-void
.end method
