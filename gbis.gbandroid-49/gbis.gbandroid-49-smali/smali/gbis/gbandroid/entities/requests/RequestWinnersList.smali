.class public Lgbis/gbandroid/entities/requests/RequestWinnersList;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private numberToReturn:I

.field private pageNumber:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNumberToReturn()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestWinnersList;->numberToReturn:I

    return v0
.end method

.method public getPageNumber()I
    .locals 1

    .prologue
    .line 8
    iget v0, p0, Lgbis/gbandroid/entities/requests/RequestWinnersList;->pageNumber:I

    return v0
.end method

.method public setNumberToReturn(I)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestWinnersList;->numberToReturn:I

    .line 21
    return-void
.end method

.method public setPageNumber(I)V
    .locals 0
    .parameter

    .prologue
    .line 12
    iput p1, p0, Lgbis/gbandroid/entities/requests/RequestWinnersList;->pageNumber:I

    .line 13
    return-void
.end method
