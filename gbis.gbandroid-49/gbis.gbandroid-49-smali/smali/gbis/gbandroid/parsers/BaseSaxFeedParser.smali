.class public abstract Lgbis/gbandroid/parsers/BaseSaxFeedParser;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private a:Ljava/net/URL;

.field private b:I

.field private c:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/16 v0, 0x7530

    iput v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->b:I

    .line 15
    const/16 v0, 0x4e20

    iput v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->c:I

    .line 26
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/16 v0, 0x7530

    iput v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->b:I

    .line 15
    const/16 v0, 0x4e20

    iput v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->c:I

    .line 19
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->a:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 20
    :catch_0
    move-exception v0

    .line 21
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getInputStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 30
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->a:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 31
    iget v1, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->c:I

    invoke-virtual {v0, v1}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 32
    iget v1, p0, Lgbis/gbandroid/parsers/BaseSaxFeedParser;->b:I

    invoke-virtual {v0, v1}, Ljava/net/URLConnection;->setReadTimeout(I)V

    .line 33
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    new-instance v1, Lgbis/gbandroid/exceptions/CustomConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Assertion Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/CustomConnectionException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :catch_1
    move-exception v0

    .line 37
    new-instance v1, Lgbis/gbandroid/exceptions/CustomConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SocketTimeout: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/CustomConnectionException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 38
    :catch_2
    move-exception v0

    .line 39
    new-instance v1, Lgbis/gbandroid/exceptions/RequestParsingWSException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "General Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/RequestParsingWSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
