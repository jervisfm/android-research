.class public abstract Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;
.super Lgbis/gbandroid/parsers/BaseSaxFeedParser;
.source "GBFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lgbis/gbandroid/parsers/BaseSaxFeedParser;-><init>(Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method private a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 24
    invoke-virtual {p0}, Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;->getRootElement()Landroid/sax/RootElement;

    move-result-object v0

    .line 25
    invoke-virtual {p0, v0}, Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;->parseService(Landroid/sax/RootElement;)Ljava/lang/Object;

    move-result-object v1

    .line 27
    :try_start_0
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-virtual {v0}, Landroid/sax/RootElement;->getContentHandler()Lorg/xml/sax/ContentHandler;

    move-result-object v0

    invoke-static {p1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 35
    return-object v1

    .line 28
    :catch_0
    move-exception v0

    .line 29
    new-instance v1, Lgbis/gbandroid/exceptions/CustomConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Assertion Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/CustomConnectionException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 30
    :catch_1
    move-exception v0

    .line 31
    new-instance v1, Lgbis/gbandroid/exceptions/CustomConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SocketTimeout: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/CustomConnectionException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 32
    :catch_2
    move-exception v0

    .line 33
    new-instance v1, Lgbis/gbandroid/exceptions/RequestParsingWSException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "General Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lgbis/gbandroid/exceptions/RequestParsingWSException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected abstract getRootElement()Landroid/sax/RootElement;
.end method

.method public parseExternalService()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract parseService(Landroid/sax/RootElement;)Ljava/lang/Object;
.end method
