.class public Lgbis/gbandroid/parsers/external/OpenCellIDSaxFeedParser;
.super Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;
.source "GBFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lgbis/gbandroid/parsers/external/ExternalServiceSaxFeedParser;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected getRootElement()Landroid/sax/RootElement;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Landroid/sax/RootElement;

    const-string v1, "rsp"

    invoke-direct {v0, v1}, Landroid/sax/RootElement;-><init>(Ljava/lang/String;)V

    .line 24
    return-object v0
.end method

.method protected parseService(Landroid/sax/RootElement;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Double;

    .line 40
    const-string v1, "cell"

    invoke-virtual {p1, v1}, Landroid/sax/RootElement;->getChild(Ljava/lang/String;)Landroid/sax/Element;

    move-result-object v1

    .line 41
    new-instance v2, Lgbis/gbandroid/parsers/external/a;

    invoke-direct {v2, p0, v0}, Lgbis/gbandroid/parsers/external/a;-><init>(Lgbis/gbandroid/parsers/external/OpenCellIDSaxFeedParser;[Ljava/lang/Double;)V

    invoke-virtual {v1, v2}, Landroid/sax/Element;->setStartElementListener(Landroid/sax/StartElementListener;)V

    .line 50
    return-object v0
.end method
