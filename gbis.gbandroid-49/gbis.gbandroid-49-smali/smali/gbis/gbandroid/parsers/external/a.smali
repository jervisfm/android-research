.class final Lgbis/gbandroid/parsers/external/a;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/sax/StartElementListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/parsers/external/OpenCellIDSaxFeedParser;

.field private final synthetic b:[Ljava/lang/Double;


# direct methods
.method constructor <init>(Lgbis/gbandroid/parsers/external/OpenCellIDSaxFeedParser;[Ljava/lang/Double;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/parsers/external/a;->a:Lgbis/gbandroid/parsers/external/OpenCellIDSaxFeedParser;

    iput-object p2, p0, Lgbis/gbandroid/parsers/external/a;->b:[Ljava/lang/Double;

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final start(Lorg/xml/sax/Attributes;)V
    .locals 6
    .parameter

    .prologue
    .line 44
    const-string v0, "lat"

    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "lon"

    invoke-interface {p1, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 46
    iget-object v2, p0, Lgbis/gbandroid/parsers/external/a;->b:[Ljava/lang/Double;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v2, v3

    .line 47
    iget-object v0, p0, Lgbis/gbandroid/parsers/external/a;->b:[Ljava/lang/Double;

    const/4 v2, 0x1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v0, v2

    .line 48
    return-void
.end method
