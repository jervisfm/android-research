.class public Lgbis/gbandroid/queries/AddStationQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestStationSuggestion;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:[Z

.field private i:I

.field private j:D

.field private k:D

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 38
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/AddStationQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/AddStationQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 66
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestStationSuggestion;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;-><init>()V

    .line 75
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setAddress(Ljava/lang/String;)V

    .line 76
    const-string v1, ""

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setFuelBrand(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setStationName(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setCrossStreet(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setCity(Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setState(Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPostalCode(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPhone(Ljava/lang/String;)V

    .line 83
    iget v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->i:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setNumberOfPumps(I)V

    .line 84
    iget-wide v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->j:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setLatitude(D)V

    .line 85
    iget-wide v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->k:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setLongitude(D)V

    .line 86
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setSearchTerms(Ljava/lang/String;)V

    .line 87
    iget v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->m:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setSearchType(I)V

    .line 88
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "fuelPreference"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setSearchFuelType(Ljava/lang/String;)V

    .line 89
    iget-boolean v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->n:Z

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setOverrideInsert(Z)V

    .line 91
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x0

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setRegularGas(Z)V

    .line 92
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x1

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setMidgradeGas(Z)V

    .line 93
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x2

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPremiumGas(Z)V

    .line 94
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x3

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setDiesel(Z)V

    .line 95
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x4

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setE85(Z)V

    .line 96
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x5

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPropane(Z)V

    .line 97
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x6

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setCStore(Z)V

    .line 98
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/4 v2, 0x7

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setServiceStation(Z)V

    .line 99
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0x8

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPayAtPump(Z)V

    .line 100
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0x9

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setRestaurant(Z)V

    .line 101
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xa

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setRestrooms(Z)V

    .line 102
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xb

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setAir(Z)V

    .line 103
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xc

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setPayphone(Z)V

    .line 104
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xd

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setAtm(Z)V

    .line 105
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xe

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setOpen247(Z)V

    .line 106
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0xf

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setTruckStop(Z)V

    .line 107
    iget-object v1, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    const/16 v2, 0x10

    aget-boolean v1, v1, v2

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationSuggestion;->setCarWash(Z)V

    .line 108
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/AddStationQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestStationSuggestion;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[ZIDDLjava/lang/String;IZ)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[ZIDD",
            "Ljava/lang/String;",
            "IZ)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    iput-object p1, p0, Lgbis/gbandroid/queries/AddStationQuery;->a:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lgbis/gbandroid/queries/AddStationQuery;->b:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lgbis/gbandroid/queries/AddStationQuery;->c:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lgbis/gbandroid/queries/AddStationQuery;->d:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lgbis/gbandroid/queries/AddStationQuery;->e:Ljava/lang/String;

    .line 48
    iput-object p6, p0, Lgbis/gbandroid/queries/AddStationQuery;->f:Ljava/lang/String;

    .line 49
    iput-object p7, p0, Lgbis/gbandroid/queries/AddStationQuery;->g:Ljava/lang/String;

    .line 50
    iput-object p8, p0, Lgbis/gbandroid/queries/AddStationQuery;->h:[Z

    .line 51
    iput p9, p0, Lgbis/gbandroid/queries/AddStationQuery;->i:I

    .line 52
    iput-wide p10, p0, Lgbis/gbandroid/queries/AddStationQuery;->j:D

    .line 53
    iput-wide p12, p0, Lgbis/gbandroid/queries/AddStationQuery;->k:D

    .line 54
    move-object/from16 v0, p14

    iput-object v0, p0, Lgbis/gbandroid/queries/AddStationQuery;->l:Ljava/lang/String;

    .line 55
    move/from16 v0, p15

    iput v0, p0, Lgbis/gbandroid/queries/AddStationQuery;->m:I

    .line 56
    move/from16 v0, p16

    iput-boolean v0, p0, Lgbis/gbandroid/queries/AddStationQuery;->n:Z

    .line 58
    invoke-direct {p0}, Lgbis/gbandroid/queries/AddStationQuery;->a()Ljava/lang/String;

    move-result-object v1

    .line 59
    new-instance v2, Lgbis/gbandroid/queries/a;

    invoke-direct {v2, p0}, Lgbis/gbandroid/queries/a;-><init>(Lgbis/gbandroid/queries/AddStationQuery;)V

    invoke-virtual {v2}, Lgbis/gbandroid/queries/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 60
    invoke-virtual {p0, v1, v2}, Lgbis/gbandroid/queries/AddStationQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v1

    return-object v1
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x1

    return v0
.end method
