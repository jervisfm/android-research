.class public abstract Lgbis/gbandroid/queries/BaseQuery;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final APP_SOURCE:I = 0x1

.field protected static final SOURCE:I = 0x3


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mLocation:Landroid/location/Location;

.field protected mPrefs:Landroid/content/SharedPreferences;

.field protected mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/requests/RequestObject",
            "<TX;>;"
        }
    .end annotation
.end field

.field protected mTypeParameterClass:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lgbis/gbandroid/queries/BaseQuery;->mPrefs:Landroid/content/SharedPreferences;

    .line 37
    iput-object p3, p0, Lgbis/gbandroid/queries/BaseQuery;->mTypeParameterClass:Ljava/lang/reflect/Type;

    .line 38
    iput-object p4, p0, Lgbis/gbandroid/queries/BaseQuery;->mLocation:Landroid/location/Location;

    .line 39
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    const v2, 0x7f0901db

    .line 96
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901dd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901db

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 99
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901dc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestObject;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    .line 43
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "member_id"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setMemberId(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "auth_id"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setAuthId(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lgbis/gbandroid/activities/base/GBActivity;->getVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestObject;->setAppVersion(D)V

    .line 46
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setSource(I)V

    .line 47
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getQueryTimeStampDevice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setDateDevice(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-static {}, Lgbis/gbandroid/utils/DateUtils;->getQueryTimeStampEastern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setDateEastern(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mLocation:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setUserLocation(Landroid/location/Location;)V

    .line 50
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-direct {p0}, Lgbis/gbandroid/queries/BaseQuery;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setKey(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setAppSource(I)V

    .line 52
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-virtual {p0}, Lgbis/gbandroid/queries/BaseQuery;->getParameters()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setParameters(Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-virtual {p0}, Lgbis/gbandroid/queries/BaseQuery;->setWebServiceVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestObject;->setWebServiceVersion(I)V

    .line 54
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/requests/RequestObject;->getMemberId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mLocation:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/requests/RequestObject;->getDateEastern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "auth_id"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 58
    const-string v0, ""

    .line 60
    :try_start_0
    const-string v2, "SHA-256"

    invoke-static {v1, v2}, Lgbis/gbandroid/utils/FieldEncryption;->hashText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected formURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 74
    invoke-virtual {p0}, Lgbis/gbandroid/queries/BaseQuery;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p0, v0, v1, p1}, Lgbis/gbandroid/queries/BaseQuery;->formURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected formURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 78
    const-string v0, ""

    invoke-virtual {p0, p1, v0, p2}, Lgbis/gbandroid/queries/BaseQuery;->formURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected formURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    invoke-direct {p0, p1}, Lgbis/gbandroid/queries/BaseQuery;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mContext:Landroid/content/Context;

    const v2, 0x7f0901e6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    const-string v1, "?output=json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-direct {p0}, Lgbis/gbandroid/queries/BaseQuery;->a()V

    .line 91
    return-object v0
.end method

.method protected getHost()Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "host"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getParameters()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TX;"
        }
    .end annotation
.end method

.method protected parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/google/gbson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gbson/GsonBuilder;-><init>()V

    sget-object v1, Lcom/google/gbson/FieldNamingPolicy;->UPPER_CAMEL_CASE:Lcom/google/gbson/FieldNamingPolicy;

    invoke-virtual {v0, v1}, Lcom/google/gbson/GsonBuilder;->setFieldNamingPolicy(Lcom/google/gbson/FieldNamingPolicy;)Lcom/google/gbson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gbson/GsonBuilder;->create()Lcom/google/gbson/Gson;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lgbis/gbandroid/queries/BaseQuery;->mRequestObject:Lgbis/gbandroid/entities/requests/RequestObject;

    invoke-virtual {v0, v1, p2}, Lcom/google/gbson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    .line 109
    new-instance v1, Lgbis/gbandroid/parsers/json/BaseJsonPostParser;

    invoke-direct {v1, p1, v0}, Lgbis/gbandroid/parsers/json/BaseJsonPostParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lgbis/gbandroid/queries/BaseQuery;->mTypeParameterClass:Ljava/lang/reflect/Type;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/parsers/json/BaseJsonPostParser;->parseResponseObject(Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 111
    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lgbis/gbandroid/exceptions/CustomConnectionException;

    const-string v1, "ResponseObject was null"

    invoke-direct {v0, v1}, Lgbis/gbandroid/exceptions/CustomConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    return-object v0
.end method

.method protected abstract setWebServiceVersion()I
.end method
