.class public Lgbis/gbandroid/queries/CrashReportQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestCrashReport;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 29
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/CrashReportQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/CrashReportQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lgbis/gbandroid/queries/CrashReportQuery;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/openudid/OpenUDID_manager;->sync(Landroid/content/Context;)V

    .line 59
    :cond_0
    invoke-static {}, Lorg/openudid/OpenUDID_manager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lgbis/gbandroid/queries/CrashReportQuery;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 62
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 63
    if-nez v0, :cond_1

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x9

    if-lt v1, v2, :cond_1

    .line 64
    invoke-static {}, Lgbis/gbandroid/utils/DeviceIdUtility;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    .line 65
    :cond_1
    return-object v0
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestCrashReport;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestCrashReport;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestCrashReport;-><init>()V

    .line 50
    const-string v1, "android"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestCrashReport;->setOsName(Ljava/lang/String;)V

    .line 51
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestCrashReport;->setOsVersion(Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lgbis/gbandroid/queries/CrashReportQuery;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestCrashReport;->setDeviceId(Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/queries/CrashReportQuery;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestCrashReport;->setMessage(Ljava/lang/String;)V

    .line 54
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/CrashReportQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestCrashReport;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 32
    iput-object p1, p0, Lgbis/gbandroid/queries/CrashReportQuery;->a:Ljava/lang/String;

    .line 33
    invoke-direct {p0}, Lgbis/gbandroid/queries/CrashReportQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 34
    new-instance v1, Lgbis/gbandroid/queries/g;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/g;-><init>(Lgbis/gbandroid/queries/CrashReportQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/g;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 35
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/CrashReportQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method
