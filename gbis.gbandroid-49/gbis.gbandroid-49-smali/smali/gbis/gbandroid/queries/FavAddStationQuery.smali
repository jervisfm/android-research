.class public Lgbis/gbandroid/queries/FavAddStationQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestFavAddStation;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 26
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901f1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/FavAddStationQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestFavAddStation;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestFavAddStation;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestFavAddStation;-><init>()V

    .line 50
    iget v1, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->setListId(I)V

    .line 51
    iget-object v1, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->setListName(Ljava/lang/String;)V

    .line 52
    iget v1, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->b:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavAddStation;->setStationId(I)V

    .line 53
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/FavAddStationQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestFavAddStation;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(IILjava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->a:I

    .line 30
    iput p2, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->b:I

    .line 31
    iput-object p3, p0, Lgbis/gbandroid/queries/FavAddStationQuery;->c:Ljava/lang/String;

    .line 33
    invoke-direct {p0}, Lgbis/gbandroid/queries/FavAddStationQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 34
    new-instance v1, Lgbis/gbandroid/queries/i;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/i;-><init>(Lgbis/gbandroid/queries/FavAddStationQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/i;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 35
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/FavAddStationQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method
