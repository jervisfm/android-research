.class public Lgbis/gbandroid/queries/FavStationListQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestFavStationList;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 26
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/FavStationListQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901f5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/FavStationListQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 40
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestFavStationList;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestFavStationList;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestFavStationList;-><init>()V

    .line 49
    iget v1, p0, Lgbis/gbandroid/queries/FavStationListQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavStationList;->setListId(I)V

    .line 50
    iget v1, p0, Lgbis/gbandroid/queries/FavStationListQuery;->b:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavStationList;->setPageNumber(I)V

    .line 51
    iget v1, p0, Lgbis/gbandroid/queries/FavStationListQuery;->c:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestFavStationList;->setItemsPerPage(I)V

    .line 52
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/FavStationListQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestFavStationList;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(III)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/queries/FavStationListQuery;->a:I

    .line 30
    iput p2, p0, Lgbis/gbandroid/queries/FavStationListQuery;->b:I

    .line 31
    iput p3, p0, Lgbis/gbandroid/queries/FavStationListQuery;->c:I

    .line 32
    invoke-direct {p0}, Lgbis/gbandroid/queries/FavStationListQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 33
    new-instance v1, Lgbis/gbandroid/queries/m;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/m;-><init>(Lgbis/gbandroid/queries/FavStationListQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/m;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 34
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/FavStationListQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method
