.class public Lgbis/gbandroid/queries/GeocoderQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestGeocode;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 28
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/GeocoderQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/GeocoderQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    return-object v0

    .line 44
    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestGeocode;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestGeocode;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestGeocode;-><init>()V

    .line 52
    iget-object v1, p0, Lgbis/gbandroid/queries/GeocoderQuery;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestGeocode;->setAddress(Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/queries/GeocoderQuery;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestGeocode;->setCity(Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lgbis/gbandroid/queries/GeocoderQuery;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestGeocode;->setState(Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lgbis/gbandroid/queries/GeocoderQuery;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestGeocode;->setPostalCode(Ljava/lang/String;)V

    .line 56
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/GeocoderQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestGeocode;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " and "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/queries/GeocoderQuery;->a:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lgbis/gbandroid/queries/GeocoderQuery;->b:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lgbis/gbandroid/queries/GeocoderQuery;->c:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lgbis/gbandroid/queries/GeocoderQuery;->d:Ljava/lang/String;

    .line 35
    invoke-direct {p0}, Lgbis/gbandroid/queries/GeocoderQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 36
    new-instance v1, Lgbis/gbandroid/queries/o;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/o;-><init>(Lgbis/gbandroid/queries/GeocoderQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/o;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 37
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/GeocoderQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0

    .line 31
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method
