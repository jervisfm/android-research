.class public Lgbis/gbandroid/queries/InitQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestInit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 31
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/InitQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901dd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/queries/InitQuery;->mContext:Landroid/content/Context;

    const v2, 0x7f0901e7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/InitQuery;->formURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 42
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 63
    iget-object v0, p0, Lgbis/gbandroid/queries/InitQuery;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lorg/openudid/OpenUDID_manager;->sync(Landroid/content/Context;)V

    .line 64
    :cond_0
    invoke-static {}, Lorg/openudid/OpenUDID_manager;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lgbis/gbandroid/queries/InitQuery;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 67
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 68
    if-nez v0, :cond_1

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x9

    if-lt v1, v2, :cond_1

    .line 69
    invoke-static {}, Lgbis/gbandroid/utils/DeviceIdUtility;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    .line 70
    :cond_1
    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lgbis/gbandroid/queries/InitQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "referralString"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x1f4

    if-le v1, v2, :cond_0

    .line 87
    const/4 v1, 0x0

    const/16 v2, 0x1f3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 88
    :cond_0
    return-object v0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 92
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 93
    iget-object v0, p0, Lgbis/gbandroid/queries/InitQuery;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestInit;
    .locals 4

    .prologue
    .line 50
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestInit;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestInit;-><init>()V

    .line 51
    iget-object v1, p0, Lgbis/gbandroid/queries/InitQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "distributionMethod"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setDestributionMethod(I)V

    .line 52
    invoke-direct {p0}, Lgbis/gbandroid/queries/InitQuery;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setDeviceId(Ljava/lang/String;)V

    .line 53
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setDeviceModel(Ljava/lang/String;)V

    .line 54
    const-string v1, "android"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setOsName(Ljava/lang/String;)V

    .line 55
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setOsVersion(Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lgbis/gbandroid/queries/InitQuery;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setReferrer(Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lgbis/gbandroid/queries/InitQuery;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setResolution(Ljava/lang/String;)V

    .line 58
    const-string v1, ""

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestInit;->setUserToken(Ljava/lang/String;)V

    .line 59
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/InitQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestInit;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0}, Lgbis/gbandroid/queries/InitQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 35
    new-instance v1, Lgbis/gbandroid/queries/p;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/p;-><init>(Lgbis/gbandroid/queries/InitQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/p;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 36
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/InitQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method
