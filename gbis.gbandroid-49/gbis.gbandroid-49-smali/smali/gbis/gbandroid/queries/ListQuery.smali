.class public Lgbis/gbandroid/queries/ListQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestStaionList;",
        ">;"
    }
.end annotation


# instance fields
.field private a:D

.field private b:D

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 31
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/ListQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901f8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/ListQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 47
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestStaionList;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestStaionList;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestStaionList;-><init>()V

    .line 56
    iget-wide v1, p0, Lgbis/gbandroid/queries/ListQuery;->a:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setLatitude(D)V

    .line 57
    iget-wide v1, p0, Lgbis/gbandroid/queries/ListQuery;->b:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setLongitude(D)V

    .line 58
    iget-object v1, p0, Lgbis/gbandroid/queries/ListQuery;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setCity(Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lgbis/gbandroid/queries/ListQuery;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setState(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lgbis/gbandroid/queries/ListQuery;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setPostalCode(Ljava/lang/String;)V

    .line 61
    iget-object v1, p0, Lgbis/gbandroid/queries/ListQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "radiusPreference"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setDistance(I)V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/queries/ListQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v2, "fuelPreference"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setFuelType(Ljava/lang/String;)V

    .line 63
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setNumberToReturn(I)V

    .line 64
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setPageNumber(I)V

    .line 65
    invoke-virtual {v0, v4}, Lgbis/gbandroid/entities/requests/RequestStaionList;->setPricesRequired(Z)V

    .line 66
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/ListQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestStaionList;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/maps/GeoPoint;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-wide v2, 0x412e848000000000L

    .line 34
    invoke-virtual {p4}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/ListQuery;->a:D

    .line 35
    invoke-virtual {p4}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/ListQuery;->b:D

    .line 36
    iput-object p1, p0, Lgbis/gbandroid/queries/ListQuery;->c:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lgbis/gbandroid/queries/ListQuery;->d:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lgbis/gbandroid/queries/ListQuery;->e:Ljava/lang/String;

    .line 39
    invoke-direct {p0}, Lgbis/gbandroid/queries/ListQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 40
    new-instance v1, Lgbis/gbandroid/queries/q;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/q;-><init>(Lgbis/gbandroid/queries/ListQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/q;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 41
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/ListQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method
