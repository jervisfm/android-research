.class public Lgbis/gbandroid/queries/MapQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestStationMap;",
        ">;"
    }
.end annotation


# instance fields
.field private a:D

.field private b:D

.field private c:D

.field private d:D

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 33
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/MapQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901f9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/MapQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 54
    return-object v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestStationMap;
    .locals 5

    .prologue
    .line 62
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 63
    iget-object v0, p0, Lgbis/gbandroid/queries/MapQuery;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 65
    new-instance v2, Lgbis/gbandroid/entities/requests/RequestStationMap;

    invoke-direct {v2}, Lgbis/gbandroid/entities/requests/RequestStationMap;-><init>()V

    .line 66
    iget-wide v3, p0, Lgbis/gbandroid/queries/MapQuery;->a:D

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setNorthWestLatitude(D)V

    .line 67
    iget-wide v3, p0, Lgbis/gbandroid/queries/MapQuery;->b:D

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setNorthWestLongitude(D)V

    .line 68
    iget-wide v3, p0, Lgbis/gbandroid/queries/MapQuery;->c:D

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setSouthEastLatitude(D)V

    .line 69
    iget-wide v3, p0, Lgbis/gbandroid/queries/MapQuery;->d:D

    invoke-virtual {v2, v3, v4}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setSouthEastLongitude(D)V

    .line 70
    iget-object v0, p0, Lgbis/gbandroid/queries/MapQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "fuelPreference"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setFuelType(Ljava/lang/String;)V

    .line 71
    iget v0, p0, Lgbis/gbandroid/queries/MapQuery;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lgbis/gbandroid/queries/MapQuery;->f:I

    :goto_0
    invoke-virtual {v2, v0}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setViewportWidth(I)V

    .line 72
    iget v0, p0, Lgbis/gbandroid/queries/MapQuery;->e:I

    if-eqz v0, :cond_1

    iget v0, p0, Lgbis/gbandroid/queries/MapQuery;->e:I

    :goto_1
    invoke-virtual {v2, v0}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setViewportHeight(I)V

    .line 73
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-double v0, v0

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setViewportPixelRatio(D)V

    .line 74
    iget-object v0, p0, Lgbis/gbandroid/queries/MapQuery;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "noPricesPreference"

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lgbis/gbandroid/entities/requests/RequestStationMap;->setPricesRequired(Z)V

    .line 75
    return-object v2

    .line 71
    :cond_0
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_0

    .line 72
    :cond_1
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/MapQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestStationMap;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Lcom/google/android/maps/GeoPoint;II)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/maps/GeoPoint;",
            "II)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 48
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/queries/MapQuery;->getResponseObject(Lcom/google/android/maps/GeoPoint;IIII)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Lcom/google/android/maps/GeoPoint;IIII)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/maps/GeoPoint;",
            "IIII)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const-wide v2, 0x412e848000000000L

    .line 36
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    div-int/lit8 v1, p2, 0x2

    add-int/2addr v0, v1

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/MapQuery;->a:D

    .line 37
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    div-int/lit8 v1, p3, 0x2

    sub-int/2addr v0, v1

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/MapQuery;->b:D

    .line 38
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v0

    div-int/lit8 v1, p2, 0x2

    sub-int/2addr v0, v1

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/MapQuery;->c:D

    .line 39
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    div-int/lit8 v1, p3, 0x2

    add-int/2addr v0, v1

    int-to-double v0, v0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgbis/gbandroid/queries/MapQuery;->d:D

    .line 40
    iput p4, p0, Lgbis/gbandroid/queries/MapQuery;->e:I

    .line 41
    iput p5, p0, Lgbis/gbandroid/queries/MapQuery;->f:I

    .line 42
    invoke-direct {p0}, Lgbis/gbandroid/queries/MapQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 43
    new-instance v1, Lgbis/gbandroid/queries/r;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/r;-><init>(Lgbis/gbandroid/queries/MapQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/r;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 44
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/MapQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method
