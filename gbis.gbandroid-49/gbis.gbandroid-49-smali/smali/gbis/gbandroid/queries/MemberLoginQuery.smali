.class public Lgbis/gbandroid/queries/MemberLoginQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestMemberLogin;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 26
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/MemberLoginQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901fa

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/MemberLoginQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    return-object v0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestMemberLogin;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestMemberLogin;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestMemberLogin;-><init>()V

    .line 48
    iget-object v1, p0, Lgbis/gbandroid/queries/MemberLoginQuery;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestMemberLogin;->setMemberId(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lgbis/gbandroid/queries/MemberLoginQuery;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestMemberLogin;->setPassword(Ljava/lang/String;)V

    .line 50
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/MemberLoginQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestMemberLogin;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    iput-object p1, p0, Lgbis/gbandroid/queries/MemberLoginQuery;->a:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lgbis/gbandroid/queries/MemberLoginQuery;->b:Ljava/lang/String;

    .line 31
    invoke-direct {p0}, Lgbis/gbandroid/queries/MemberLoginQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 32
    new-instance v1, Lgbis/gbandroid/queries/s;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/s;-><init>(Lgbis/gbandroid/queries/MemberLoginQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/s;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 33
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/MemberLoginQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method
