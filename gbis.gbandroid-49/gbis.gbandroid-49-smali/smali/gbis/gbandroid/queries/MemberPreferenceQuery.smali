.class public Lgbis/gbandroid/queries/MemberPreferenceQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Ljava/util/List",
        "<",
        "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 25
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/MemberPreferenceQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090209

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->getParameters()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getParameters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/queries/MemberPreferenceQuery;->a:Ljava/util/List;

    return-object v0
.end method

.method public getResponseObject(Ljava/util/List;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/requests/RequestMemberPreference;",
            ">;)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/queries/MemberPreferenceQuery;->a:Ljava/util/List;

    .line 29
    invoke-direct {p0}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 30
    new-instance v1, Lgbis/gbandroid/queries/t;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/t;-><init>(Lgbis/gbandroid/queries/MemberPreferenceQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/t;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 31
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/MemberPreferenceQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method
