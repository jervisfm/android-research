.class public Lgbis/gbandroid/queries/MemberProfileQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 22
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/MemberProfileQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901e9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/MemberProfileQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 33
    return-object v0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/MemberProfileQuery;->getParameters()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected getParameters()Ljava/lang/Void;
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Lgbis/gbandroid/queries/MemberProfileQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 26
    new-instance v1, Lgbis/gbandroid/queries/u;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/u;-><init>(Lgbis/gbandroid/queries/MemberProfileQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/u;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 27
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/MemberProfileQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method
