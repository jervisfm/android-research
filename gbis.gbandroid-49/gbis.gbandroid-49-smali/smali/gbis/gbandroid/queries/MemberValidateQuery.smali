.class public Lgbis/gbandroid/queries/MemberValidateQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestMember;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 24
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/MemberValidateQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901fc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/MemberValidateQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 36
    return-object v0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestMember;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestMember;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestMember;-><init>()V

    .line 45
    iget-object v1, p0, Lgbis/gbandroid/queries/MemberValidateQuery;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestMember;->setMemberId(Ljava/lang/String;)V

    .line 46
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/MemberValidateQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestMember;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 27
    iput-object p1, p0, Lgbis/gbandroid/queries/MemberValidateQuery;->a:Ljava/lang/String;

    .line 28
    invoke-direct {p0}, Lgbis/gbandroid/queries/MemberValidateQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 29
    new-instance v1, Lgbis/gbandroid/queries/w;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/w;-><init>(Lgbis/gbandroid/queries/MemberValidateQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/w;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 30
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/MemberValidateQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method
