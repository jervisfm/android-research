.class public Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestMessage;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected formPhotoURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901fe

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestMessage;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestMessage;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestMessage;-><init>()V

    .line 46
    iget-object v1, p0, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestMessage;->setMessage(Ljava/lang/String;)V

    .line 47
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestMessage;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    iput-object p1, p0, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->a:Ljava/lang/String;

    .line 29
    invoke-virtual {p0}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->formPhotoURL()Ljava/lang/String;

    move-result-object v0

    .line 30
    new-instance v1, Lgbis/gbandroid/queries/y;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/y;-><init>(Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/y;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 31
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/ProfilePhotoUploaderQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method
