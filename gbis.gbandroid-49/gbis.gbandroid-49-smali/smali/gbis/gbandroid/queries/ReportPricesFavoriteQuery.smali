.class public Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:D

.field private d:D

.field private e:D

.field private f:D

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 32
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090200

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 53
    return-object v0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;-><init>()V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setCar(Ljava/lang/String;)V

    .line 63
    iget v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->h:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setCarIconId(I)V

    .line 64
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->f:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setDieselPrice(D)V

    .line 65
    iget v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->b:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setFavoriteId(I)V

    .line 66
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->d:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setMidgradePrice(D)V

    .line 67
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->e:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setPremiumPrice(D)V

    .line 68
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->c:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setRegularPrice(D)V

    .line 69
    iget v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setStationId(I)V

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;->setTimeSpotted(Ljava/lang/String;)V

    .line 71
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestReportPricesFavorites;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(IILjava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;ILjava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    iput p1, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->a:I

    .line 37
    iput p2, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->b:I

    .line 38
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->c:D

    .line 39
    invoke-virtual {p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->d:D

    .line 40
    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->e:D

    .line 41
    invoke-virtual {p6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->f:D

    .line 42
    iput-object p7, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->g:Ljava/lang/String;

    .line 43
    iput p8, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->h:I

    .line 44
    iput-object p9, p0, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->i:Ljava/lang/String;

    .line 45
    invoke-direct {p0}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 46
    new-instance v1, Lgbis/gbandroid/queries/z;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/z;-><init>(Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/z;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 47
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/ReportPricesFavoriteQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method
