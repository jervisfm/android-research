.class public Lgbis/gbandroid/queries/ReportPricesQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestReportPrices;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:D

.field private c:D

.field private d:D

.field private e:D

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 32
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f0901ff

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/ReportPricesQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 53
    return-object v0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestReportPrices;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestReportPrices;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestReportPrices;-><init>()V

    .line 62
    iget-object v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setCar(Ljava/lang/String;)V

    .line 63
    iget v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->g:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setCarIconId(I)V

    .line 64
    iget-object v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setComments(Ljava/lang/String;)V

    .line 65
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->e:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setDieselPrice(D)V

    .line 66
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->c:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setMidgradePrice(D)V

    .line 67
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->d:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setPremiumPrice(D)V

    .line 68
    iget-wide v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->b:D

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setRegularPrice(D)V

    .line 69
    iget v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setStationId(I)V

    .line 70
    iget-object v1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestReportPrices;->setTimeSpotted(Ljava/lang/String;)V

    .line 71
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/ReportPricesQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestReportPrices;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(ILjava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    iput p1, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->a:I

    .line 37
    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->b:D

    .line 38
    invoke-virtual {p3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->c:D

    .line 39
    invoke-virtual {p4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->d:D

    .line 40
    invoke-virtual {p5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iput-wide v0, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->e:D

    .line 41
    iput-object p6, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->f:Ljava/lang/String;

    .line 42
    iput p7, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->g:I

    .line 43
    iput-object p8, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->h:Ljava/lang/String;

    .line 44
    iput-object p9, p0, Lgbis/gbandroid/queries/ReportPricesQuery;->i:Ljava/lang/String;

    .line 45
    invoke-direct {p0}, Lgbis/gbandroid/queries/ReportPricesQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 46
    new-instance v1, Lgbis/gbandroid/queries/aa;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/aa;-><init>(Lgbis/gbandroid/queries/ReportPricesQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/aa;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 47
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/ReportPricesQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method
