.class public Lgbis/gbandroid/queries/SmartPromptResponseQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestSmartPrompt;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 26
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090208

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 40
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestSmartPrompt;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;-><init>()V

    .line 49
    iget v1, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->setPromptId(I)V

    .line 50
    iget-object v1, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->setUserResponse(Ljava/lang/String;)V

    .line 51
    iget-boolean v1, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->c:Z

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestSmartPrompt;->setShowAgain(Z)V

    .line 52
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestSmartPrompt;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(ILjava/lang/String;Z)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Z)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->a:I

    .line 30
    iput-object p2, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->b:Ljava/lang/String;

    .line 31
    iput-boolean p3, p0, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->c:Z

    .line 32
    invoke-direct {p0}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 33
    new-instance v1, Lgbis/gbandroid/queries/ad;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/ad;-><init>(Lgbis/gbandroid/queries/SmartPromptResponseQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/ad;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 34
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/SmartPromptResponseQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    return v0
.end method
