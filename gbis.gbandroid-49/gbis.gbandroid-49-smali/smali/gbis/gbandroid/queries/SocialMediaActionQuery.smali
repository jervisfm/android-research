.class public Lgbis/gbandroid/queries/SocialMediaActionQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestShareAction;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 28
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090203

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/SocialMediaActionQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 44
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestShareAction;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestShareAction;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestShareAction;-><init>()V

    .line 53
    iget v1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->c:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestShareAction;->setAction(I)V

    .line 54
    iget-object v1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestShareAction;->setShareMessage(Ljava/lang/String;)V

    .line 55
    iget v1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->b:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestShareAction;->setSocialNetworkId(I)V

    .line 56
    iget v1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestShareAction;->setStationId(I)V

    .line 57
    iget-object v1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestShareAction;->setUserMessage(Ljava/lang/String;)V

    .line 58
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/SocialMediaActionQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestShareAction;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(IIILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    iput p1, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->a:I

    .line 32
    iput p2, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->b:I

    .line 33
    iput p3, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->c:I

    .line 34
    iput-object p4, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->d:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lgbis/gbandroid/queries/SocialMediaActionQuery;->e:Ljava/lang/String;

    .line 36
    invoke-direct {p0}, Lgbis/gbandroid/queries/SocialMediaActionQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 37
    new-instance v1, Lgbis/gbandroid/queries/ae;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/ae;-><init>(Lgbis/gbandroid/queries/SocialMediaActionQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/ae;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 38
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/SocialMediaActionQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x1

    return v0
.end method
