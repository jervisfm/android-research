.class public Lgbis/gbandroid/queries/StationDetailsQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestStationDetails;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 25
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/StationDetailsQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090204

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/StationDetailsQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 37
    return-object v0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestStationDetails;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/queries/StationDetailsQuery;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 46
    new-instance v1, Lgbis/gbandroid/entities/requests/RequestStationDetails;

    invoke-direct {v1}, Lgbis/gbandroid/entities/requests/RequestStationDetails;-><init>()V

    .line 47
    iget v2, p0, Lgbis/gbandroid/queries/StationDetailsQuery;->a:I

    invoke-virtual {v1, v2}, Lgbis/gbandroid/entities/requests/RequestStationDetails;->setStationId(I)V

    .line 48
    invoke-static {v0}, Lgbis/gbandroid/utils/ImageUtils;->getResolutionInText(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/entities/requests/RequestStationDetails;->setResolution(Ljava/lang/String;)V

    .line 49
    return-object v1
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/StationDetailsQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestStationDetails;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(I)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    iput p1, p0, Lgbis/gbandroid/queries/StationDetailsQuery;->a:I

    .line 29
    invoke-direct {p0}, Lgbis/gbandroid/queries/StationDetailsQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 30
    new-instance v1, Lgbis/gbandroid/queries/af;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/af;-><init>(Lgbis/gbandroid/queries/StationDetailsQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/af;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 31
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/StationDetailsQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method
