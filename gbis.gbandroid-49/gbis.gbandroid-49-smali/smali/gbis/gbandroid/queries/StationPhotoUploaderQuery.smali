.class public Lgbis/gbandroid/queries/StationPhotoUploaderQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected formPhotoURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090205

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;-><init>()V

    .line 50
    iget-object v1, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->setDescription(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->setMessage(Ljava/lang/String;)V

    .line 52
    iget v1, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->setStationId(I)V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->mContext:Landroid/content/Context;

    const v2, 0x7f090083

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;->setTitle(Ljava/lang/String;)V

    .line 54
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestStationPhotoUploader;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(ILjava/lang/String;Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 30
    iput p1, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->a:I

    .line 31
    iput-object p2, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->c:Ljava/lang/String;

    .line 33
    invoke-virtual {p0}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->formPhotoURL()Ljava/lang/String;

    move-result-object v0

    .line 34
    new-instance v1, Lgbis/gbandroid/queries/ag;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/ag;-><init>(Lgbis/gbandroid/queries/StationPhotoUploaderQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/ag;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 35
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/StationPhotoUploaderQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method
