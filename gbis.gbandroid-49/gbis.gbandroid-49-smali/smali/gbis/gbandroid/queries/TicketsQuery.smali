.class public Lgbis/gbandroid/queries/TicketsQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestTickets;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lgbis/gbandroid/entities/PrizeMemberMessage;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 26
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/TicketsQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090206

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/TicketsQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    return-object v0

    .line 40
    :catch_0
    move-exception v0

    .line 41
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestTickets;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestTickets;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestTickets;-><init>()V

    .line 48
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setAddress(Ljava/lang/String;)V

    .line 49
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getAddress2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setAddress2(Ljava/lang/String;)V

    .line 50
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setCity(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setEmail(Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setFirstName(Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setLastName(Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setPostalCode(Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/PrizeMemberMessage;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setState(Ljava/lang/String;)V

    .line 56
    iget v1, p0, Lgbis/gbandroid/queries/TicketsQuery;->a:I

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestTickets;->setTickets(I)V

    .line 57
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/TicketsQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestTickets;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject(ILgbis/gbandroid/entities/PrizeMemberMessage;)Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lgbis/gbandroid/entities/PrizeMemberMessage;",
            ")",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    iput p1, p0, Lgbis/gbandroid/queries/TicketsQuery;->a:I

    .line 30
    iput-object p2, p0, Lgbis/gbandroid/queries/TicketsQuery;->b:Lgbis/gbandroid/entities/PrizeMemberMessage;

    .line 31
    invoke-direct {p0}, Lgbis/gbandroid/queries/TicketsQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 32
    new-instance v1, Lgbis/gbandroid/queries/ah;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/ah;-><init>(Lgbis/gbandroid/queries/TicketsQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/ah;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 33
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/TicketsQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method
