.class public Lgbis/gbandroid/queries/WinnersListQuery;
.super Lgbis/gbandroid/queries/BaseQuery;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "X:",
        "Ljava/lang/Object;",
        ">",
        "Lgbis/gbandroid/queries/BaseQuery",
        "<TT;",
        "Lgbis/gbandroid/entities/requests/RequestWinnersList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/queries/BaseQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 23
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/queries/WinnersListQuery;->mContext:Landroid/content/Context;

    const v1, 0x7f090207

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/queries/WinnersListQuery;->formURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 34
    return-object v0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected getParameters()Lgbis/gbandroid/entities/requests/RequestWinnersList;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lgbis/gbandroid/entities/requests/RequestWinnersList;

    invoke-direct {v0}, Lgbis/gbandroid/entities/requests/RequestWinnersList;-><init>()V

    .line 43
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestWinnersList;->setNumberToReturn(I)V

    .line 44
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/entities/requests/RequestWinnersList;->setPageNumber(I)V

    .line 45
    return-object v0
.end method

.method protected bridge synthetic getParameters()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lgbis/gbandroid/queries/WinnersListQuery;->getParameters()Lgbis/gbandroid/entities/requests/RequestWinnersList;

    move-result-object v0

    return-object v0
.end method

.method public getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Lgbis/gbandroid/queries/WinnersListQuery;->a()Ljava/lang/String;

    move-result-object v0

    .line 27
    new-instance v1, Lgbis/gbandroid/queries/ai;

    invoke-direct {v1, p0}, Lgbis/gbandroid/queries/ai;-><init>(Lgbis/gbandroid/queries/WinnersListQuery;)V

    invoke-virtual {v1}, Lgbis/gbandroid/queries/ai;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 28
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/queries/WinnersListQuery;->parsePostJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    return-object v0
.end method

.method protected setWebServiceVersion()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method
