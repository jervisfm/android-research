.class final Lgbis/gbandroid/services/BrandsDownloader$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/services/BrandsDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/services/BrandsDownloader;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/services/BrandsDownloader;Lgbis/gbandroid/services/base/GBService;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    .line 108
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 109
    return-void
.end method


# virtual methods
.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .parameter

    .prologue
    .line 113
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 114
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v0}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v0}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 116
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-virtual {v0}, Lgbis/gbandroid/services/BrandsDownloader;->stopSelf()V

    .line 126
    :cond_1
    return-void

    .line 118
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    iget-object v1, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v1}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;I)V

    .line 119
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v0}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v0}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Brand;

    .line 121
    iget-object v2, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Brand;->getGasBrandId()I

    move-result v3

    invoke-virtual {v0}, Lgbis/gbandroid/entities/Brand;->getGasBrandVersion()I

    move-result v0

    invoke-static {v2, v3, v0}, Lgbis/gbandroid/services/BrandsDownloader;->a(Lgbis/gbandroid/services/BrandsDownloader;II)V

    .line 119
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader$a;->a:Lgbis/gbandroid/services/BrandsDownloader;

    invoke-static {v0}, Lgbis/gbandroid/services/BrandsDownloader;->b(Lgbis/gbandroid/services/BrandsDownloader;)V

    .line 131
    const/4 v0, 0x1

    return v0
.end method
