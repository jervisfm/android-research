.class public Lgbis/gbandroid/services/BrandsDownloader;
.super Lgbis/gbandroid/services/base/GBService;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/services/BrandsDownloader$a;,
        Lgbis/gbandroid/services/BrandsDownloader$b;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/Brand;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Landroid/location/Location;

.field protected mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lgbis/gbandroid/services/base/GBService;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/services/BrandsDownloader;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader;->a:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lgbis/gbandroid/services/BrandsDownloader$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/services/BrandsDownloader$a;-><init>(Lgbis/gbandroid/services/BrandsDownloader;Lgbis/gbandroid/services/base/GBService;)V

    .line 63
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/services/BrandsDownloader$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 64
    return-void
.end method

.method private a(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 67
    new-instance v0, Lgbis/gbandroid/services/BrandsDownloader$b;

    invoke-direct {v0, p0, p1, p2}, Lgbis/gbandroid/services/BrandsDownloader$b;-><init>(Lgbis/gbandroid/services/BrandsDownloader;II)V

    .line 68
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/services/BrandsDownloader$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 69
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/services/BrandsDownloader;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36
    iput p1, p0, Lgbis/gbandroid/services/BrandsDownloader;->b:I

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/services/BrandsDownloader;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/services/BrandsDownloader;->a(II)V

    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 86
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 89
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 92
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    .line 93
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 94
    invoke-static {v0, v1}, Lgbis/gbandroid/utils/ImageUtils;->CopyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 95
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 96
    invoke-virtual {p0, p2, p3}, Lgbis/gbandroid/services/BrandsDownloader;->updateBrandVersion(II)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lgbis/gbandroid/services/a;

    invoke-direct {v0, p0}, Lgbis/gbandroid/services/a;-><init>(Lgbis/gbandroid/services/BrandsDownloader;)V

    invoke-virtual {v0}, Lgbis/gbandroid/services/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 74
    new-instance v1, Lgbis/gbandroid/queries/BrandsDownloadQuery;

    iget-object v2, p0, Lgbis/gbandroid/services/BrandsDownloader;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lgbis/gbandroid/services/BrandsDownloader;->c:Landroid/location/Location;

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/BrandsDownloadQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 75
    invoke-virtual {v1}, Lgbis/gbandroid/queries/BrandsDownloadQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader;->a:Ljava/util/List;

    .line 77
    return-void
.end method

.method private b(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {p0}, Lgbis/gbandroid/services/BrandsDownloader;->getDensity()F

    move-result v1

    invoke-virtual {v0, p1, v1}, Lgbis/gbandroid/GBApplication;->getImageUrl(IF)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-direct {p0, v0, p1, p2}, Lgbis/gbandroid/services/BrandsDownloader;->a(Ljava/lang/String;II)V

    .line 82
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/services/BrandsDownloader;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    invoke-direct {p0}, Lgbis/gbandroid/services/BrandsDownloader;->b()V

    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/services/BrandsDownloader;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/services/BrandsDownloader;->b(II)V

    return-void
.end method

.method private declared-synchronized c()I
    .locals 2

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgbis/gbandroid/services/BrandsDownloader;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lgbis/gbandroid/services/BrandsDownloader;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lgbis/gbandroid/services/BrandsDownloader;)I
    .locals 1
    .parameter

    .prologue
    .line 102
    invoke-direct {p0}, Lgbis/gbandroid/services/BrandsDownloader;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/services/base/GBService;->onStart(Landroid/content/Intent;I)V

    .line 42
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader;->mPrefs:Landroid/content/SharedPreferences;

    .line 43
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    const-string v0, "my location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 46
    if-eqz v0, :cond_1

    .line 47
    new-instance v2, Landroid/location/Location;

    const-string v0, "my location"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v2, p0, Lgbis/gbandroid/services/BrandsDownloader;->c:Landroid/location/Location;

    .line 51
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lgbis/gbandroid/services/BrandsDownloader;->openDB()Z

    .line 52
    invoke-direct {p0}, Lgbis/gbandroid/services/BrandsDownloader;->a()V

    .line 53
    return-void

    .line 49
    :cond_1
    new-instance v0, Landroid/location/Location;

    const-string v1, "NoLocation"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/services/BrandsDownloader;->c:Landroid/location/Location;

    goto :goto_0
.end method
