.class public Lgbis/gbandroid/services/GPSService;
.super Lgbis/gbandroid/services/base/GBService;
.source "GBFile"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/services/GPSService$GPSBinder;
    }
.end annotation


# static fields
.field public static final LOCATION_UPDATE_MIN_DISTANCE:F = 333.0f

.field public static final LOCATION_UPDATE_MIN_TIME:J = 0x7532L

.field public static LOCATION_UPDATE_TIME:Ljava/lang/String;


# instance fields
.field private a:Landroid/location/Location;

.field private b:Z

.field private final c:Landroid/os/IBinder;

.field private d:Lgbis/gbandroid/listeners/MyLocationChangedListener;

.field protected locationManager:Landroid/location/LocationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "locationUpdateTime"

    sput-object v0, Lgbis/gbandroid/services/GPSService;->LOCATION_UPDATE_TIME:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lgbis/gbandroid/services/base/GBService;-><init>()V

    .line 26
    new-instance v0, Lgbis/gbandroid/services/GPSService$GPSBinder;

    invoke-direct {v0, p0}, Lgbis/gbandroid/services/GPSService$GPSBinder;-><init>(Lgbis/gbandroid/services/GPSService;)V

    iput-object v0, p0, Lgbis/gbandroid/services/GPSService;->c:Landroid/os/IBinder;

    .line 19
    return-void
.end method

.method private a(Landroid/location/Location;)V
    .locals 4
    .parameter

    .prologue
    .line 132
    if-eqz p1, :cond_0

    .line 133
    invoke-virtual {p0}, Lgbis/gbandroid/services/GPSService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 134
    const-string v1, "lastLatitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 135
    const-string v1, "lastLongitude"

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    :cond_0
    return-void
.end method


# virtual methods
.method public getLastKnownLocation()Landroid/location/Location;
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 100
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 101
    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 102
    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    invoke-static {v1, v0}, Lgbis/gbandroid/utils/ConnectionUtils;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move-object v0, v1

    .line 108
    :goto_0
    return-object v0

    .line 105
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-static {v1, v0}, Lgbis/gbandroid/utils/ConnectionUtils;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 106
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    .line 107
    :cond_3
    invoke-direct {p0, v0}, Lgbis/gbandroid/services/GPSService;->a(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public getSelfLocation()Landroid/location/Location;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 74
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 75
    iget-object v3, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v4, "gps"

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 76
    iget-object v3, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v4, "network"

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 77
    :cond_0
    :goto_0
    iget-object v3, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    if-nez v3, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/16 v5, 0x1388

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3

    .line 87
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    if-eqz v1, :cond_2

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    .line 92
    :cond_2
    :goto_1
    return-object v0

    .line 78
    :cond_3
    iget-boolean v3, p0, Lgbis/gbandroid/services/GPSService;->b:Z

    if-nez v3, :cond_4

    .line 79
    iget-object v3, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v4, "gps"

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    .line 80
    if-eqz v3, :cond_4

    iget-object v4, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-static {v3, v4}, Lgbis/gbandroid/utils/ConnectionUtils;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 81
    iput-object v3, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    .line 83
    :cond_4
    iget-object v3, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v4, "network"

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    .line 84
    if-eqz v3, :cond_0

    iget-object v4, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-static {v3, v4}, Lgbis/gbandroid/utils/ConnectionUtils;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    iput-object v3, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .parameter

    .prologue
    const-wide/16 v0, 0x7532

    .line 113
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 114
    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    sget-object v3, Lgbis/gbandroid/services/GPSService;->LOCATION_UPDATE_TIME:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 115
    :cond_0
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/services/GPSService;->startLocationListener(J)V

    .line 116
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->c:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate(Landroid/content/Intent;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-super {p0}, Lgbis/gbandroid/services/base/GBService;->onCreate()V

    .line 31
    const-wide/16 v0, 0x7532

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/services/GPSService;->startLocationListener(J)V

    .line 32
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 68
    invoke-virtual {p0}, Lgbis/gbandroid/services/GPSService;->stopLocationListener()V

    .line 69
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-static {p1, v0}, Lgbis/gbandroid/utils/ConnectionUtils;->isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iput-object p1, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    .line 144
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->d:Lgbis/gbandroid/listeners/MyLocationChangedListener;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->d:Lgbis/gbandroid/listeners/MyLocationChangedListener;

    iget-object v1, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-interface {v0, v1}, Lgbis/gbandroid/listeners/MyLocationChangedListener;->onMyLocationChanged(Landroid/location/Location;)V

    .line 146
    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/services/GPSService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/GBApplication;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/GBApplication;->setLastKnowLocation(Landroid/location/Location;)V

    .line 147
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->a:Landroid/location/Location;

    invoke-direct {p0, v0}, Lgbis/gbandroid/services/GPSService;->a(Landroid/location/Location;)V

    .line 149
    :cond_1
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 154
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 159
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/services/base/GBService;->onStart(Landroid/content/Intent;I)V

    .line 37
    const-wide/16 v0, 0x7532

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/services/GPSService;->startLocationListener(J)V

    .line 38
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 164
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .parameter

    .prologue
    .line 121
    invoke-virtual {p0}, Lgbis/gbandroid/services/GPSService;->stopSelf()V

    .line 122
    invoke-super {p0, p1}, Lgbis/gbandroid/services/base/GBService;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public registerMyLocationChangedListener(Lgbis/gbandroid/listeners/MyLocationChangedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lgbis/gbandroid/services/GPSService;->d:Lgbis/gbandroid/listeners/MyLocationChangedListener;

    .line 42
    return-void
.end method

.method public startLocationListener()V
    .locals 2

    .prologue
    .line 45
    const-wide/16 v0, 0x7532

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/services/GPSService;->startLocationListener(J)V

    .line 46
    return-void
.end method

.method public startLocationListener(J)V
    .locals 6
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    if-nez v0, :cond_0

    .line 50
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/services/GPSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    .line 52
    :cond_0
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const v4, 0x43a68000

    move-wide v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const v4, 0x43a68000

    move-wide v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 57
    :catch_1
    move-exception v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/services/GPSService;->b:Z

    goto :goto_0
.end method

.method public stopLocationListener()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lgbis/gbandroid/services/GPSService;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 64
    :cond_0
    return-void
.end method
