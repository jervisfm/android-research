.class public Lgbis/gbandroid/services/base/GBService;
.super Landroid/app/Service;
.source "GBFile"


# instance fields
.field private a:Lgbis/gbandroid/utils/DBHelper;

.field protected mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected closeDB()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->closeDB()V

    .line 63
    :cond_0
    return-void
.end method

.method protected getDensity()F
    .locals 2

    .prologue
    .line 42
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 43
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/services/base/GBService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 44
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 45
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method protected isDBOpen()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->isDBOpen()Z

    move-result v0

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 26
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/services/base/GBService;->mPrefs:Landroid/content/SharedPreferences;

    .line 27
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 32
    invoke-virtual {p0}, Lgbis/gbandroid/services/base/GBService;->closeDB()V

    .line 33
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 20
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/services/base/GBService;->mPrefs:Landroid/content/SharedPreferences;

    .line 21
    return-void
.end method

.method protected openDB()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lgbis/gbandroid/utils/DBHelper;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/DBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    .line 57
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->openDB()Z

    move-result v0

    return v0
.end method

.method protected updateBrandVersion(II)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-virtual {p0}, Lgbis/gbandroid/services/base/GBService;->isDBOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lgbis/gbandroid/services/base/GBService;->openDB()Z

    .line 51
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/services/base/GBService;->a:Lgbis/gbandroid/utils/DBHelper;

    invoke-virtual {v0, p1, p2}, Lgbis/gbandroid/utils/DBHelper;->updateBrandVersionRow(II)Z

    move-result v0

    return v0
.end method
