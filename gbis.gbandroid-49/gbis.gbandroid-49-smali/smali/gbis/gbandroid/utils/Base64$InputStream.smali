.class public Lgbis/gbandroid/utils/Base64$InputStream;
.super Ljava/io/FilterInputStream;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/Base64;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InputStream"
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field private c:[B

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:I

.field private i:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .parameter

    .prologue
    .line 1662
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbis/gbandroid/utils/Base64$InputStream;-><init>(Ljava/io/InputStream;I)V

    .line 1663
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1688
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1689
    iput p2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->h:I

    .line 1690
    and-int/lit8 v0, p2, 0x8

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->g:Z

    .line 1691
    and-int/lit8 v0, p2, 0x1

    if-lez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->a:Z

    .line 1692
    iget-boolean v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    :goto_2
    iput v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->d:I

    .line 1693
    iget v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->d:I

    new-array v0, v0, [B

    iput-object v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->c:[B

    .line 1694
    const/4 v0, -0x1

    iput v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    .line 1695
    iput v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->f:I

    .line 1696
    invoke-static {p2}, Lgbis/gbandroid/utils/Base64;->a(I)[B

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->i:[B

    .line 1697
    return-void

    :cond_0
    move v0, v2

    .line 1690
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1691
    goto :goto_1

    .line 1692
    :cond_2
    const/4 v0, 0x3

    goto :goto_2
.end method


# virtual methods
.method public read()I
    .locals 8

    .prologue
    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 1710
    iget v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    if-gez v1, :cond_1

    .line 1711
    iget-boolean v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->a:Z

    if-eqz v1, :cond_4

    .line 1712
    new-array v4, v6, [B

    move v1, v2

    move v3, v2

    .line 1714
    :goto_0
    if-lt v1, v6, :cond_3

    .line 1727
    :cond_0
    if-lez v3, :cond_2

    .line 1728
    iget-object v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->c:[B

    iget v5, p0, Lgbis/gbandroid/utils/Base64$InputStream;->h:I

    invoke-static {v4, v3, v1, v5}, Lgbis/gbandroid/utils/Base64;->a([BI[BI)[B

    .line 1729
    iput v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    .line 1730
    iput v7, p0, Lgbis/gbandroid/utils/Base64$InputStream;->e:I

    .line 1770
    :cond_1
    :goto_1
    iget v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    if-ltz v1, :cond_c

    .line 1772
    iget v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    iget v3, p0, Lgbis/gbandroid/utils/Base64$InputStream;->e:I

    if-lt v1, v3, :cond_9

    .line 1791
    :cond_2
    :goto_2
    return v0

    .line 1715
    :cond_3
    iget-object v5, p0, Lgbis/gbandroid/utils/Base64$InputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 1718
    if-ltz v5, :cond_0

    .line 1719
    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 1720
    add-int/lit8 v3, v3, 0x1

    .line 1714
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1739
    :cond_4
    new-array v3, v7, [B

    move v1, v2

    .line 1741
    :goto_3
    if-lt v1, v7, :cond_6

    .line 1754
    :cond_5
    if-ne v1, v7, :cond_8

    .line 1755
    iget-object v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->c:[B

    iget v4, p0, Lgbis/gbandroid/utils/Base64$InputStream;->h:I

    invoke-static {v3, v1, v4}, Lgbis/gbandroid/utils/Base64;->a([B[BI)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->e:I

    .line 1756
    iput v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    goto :goto_1

    .line 1744
    :cond_6
    iget-object v4, p0, Lgbis/gbandroid/utils/Base64$InputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 1745
    if-ltz v4, :cond_7

    iget-object v5, p0, Lgbis/gbandroid/utils/Base64$InputStream;->i:[B

    and-int/lit8 v6, v4, 0x7f

    aget-byte v5, v5, v6

    const/4 v6, -0x5

    if-le v5, v6, :cond_6

    .line 1747
    :cond_7
    if-ltz v4, :cond_5

    .line 1748
    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 1741
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1758
    :cond_8
    if-eqz v1, :cond_2

    .line 1763
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Improperly padded Base64 input."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1776
    :cond_9
    iget-boolean v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->a:Z

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->g:Z

    if-eqz v1, :cond_a

    iget v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->f:I

    const/16 v3, 0x4c

    if-lt v1, v3, :cond_a

    .line 1777
    iput v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->f:I

    .line 1778
    const/16 v0, 0xa

    goto :goto_2

    .line 1781
    :cond_a
    iget v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->f:I

    .line 1785
    iget-object v1, p0, Lgbis/gbandroid/utils/Base64$InputStream;->c:[B

    iget v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    aget-byte v1, v1, v2

    .line 1787
    iget v2, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    iget v3, p0, Lgbis/gbandroid/utils/Base64$InputStream;->d:I

    if-lt v2, v3, :cond_b

    .line 1788
    iput v0, p0, Lgbis/gbandroid/utils/Base64$InputStream;->b:I

    .line 1791
    :cond_b
    and-int/lit16 v0, v1, 0xff

    goto :goto_2

    .line 1798
    :cond_c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error in Base64 code reading stream."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public read([BII)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1820
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p3, :cond_1

    .line 1833
    :cond_0
    :goto_1
    return v0

    .line 1821
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/utils/Base64$InputStream;->read()I

    move-result v1

    .line 1823
    if-ltz v1, :cond_2

    .line 1824
    add-int v2, p2, v0

    int-to-byte v1, v1

    aput-byte v1, p1, v2

    .line 1820
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1826
    :cond_2
    if-nez v0, :cond_0

    .line 1827
    const/4 v0, -0x1

    goto :goto_1
.end method
