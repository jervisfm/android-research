.class final Lgbis/gbandroid/utils/CommonThreads$a;
.super Ljava/lang/Thread;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/CommonThreads;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:Landroid/os/Handler;

.field b:[Ljava/lang/String;

.field final synthetic c:Lgbis/gbandroid/utils/CommonThreads;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/utils/CommonThreads;Landroid/os/Handler;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->c:Lgbis/gbandroid/utils/CommonThreads;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 64
    iput-object p2, p0, Lgbis/gbandroid/utils/CommonThreads$a;->a:Landroid/os/Handler;

    .line 65
    iput-object p3, p0, Lgbis/gbandroid/utils/CommonThreads$a;->b:[Ljava/lang/String;

    .line 66
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 69
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 71
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->c:Lgbis/gbandroid/utils/CommonThreads;

    invoke-static {v1}, Lgbis/gbandroid/utils/CommonThreads;->a(Lgbis/gbandroid/utils/CommonThreads;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    const-string v0, "result"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$a;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 92
    :goto_1
    return-void

    .line 74
    :cond_0
    :try_start_1
    iget-object v1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->b:[Ljava/lang/String;

    array-length v1, v1

    new-array v4, v1, [Landroid/graphics/Bitmap;

    .line 75
    iget-object v1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->b:[Ljava/lang/String;

    array-length v5, v1

    move v1, v0

    .line 76
    :goto_2
    if-lt v1, v5, :cond_1

    .line 83
    const-string v0, "result"

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 84
    const-string v0, "gallery"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "result"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$a;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 78
    :cond_1
    :try_start_3
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$a;->c:Lgbis/gbandroid/utils/CommonThreads;

    iget-object v6, p0, Lgbis/gbandroid/utils/CommonThreads$a;->b:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v0, v6}, Lgbis/gbandroid/utils/CommonThreads;->a(Lgbis/gbandroid/utils/CommonThreads;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    aput-object v0, v4, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 76
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 79
    :catch_1
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    .line 88
    :catchall_0
    move-exception v0

    .line 89
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 90
    iget-object v1, p0, Lgbis/gbandroid/utils/CommonThreads$a;->a:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 91
    throw v0
.end method
