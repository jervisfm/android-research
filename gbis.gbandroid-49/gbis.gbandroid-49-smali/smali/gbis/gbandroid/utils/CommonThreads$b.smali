.class final Lgbis/gbandroid/utils/CommonThreads$b;
.super Ljava/lang/Thread;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/CommonThreads;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field a:Landroid/os/Handler;

.field b:Ljava/lang/String;

.field final synthetic c:Lgbis/gbandroid/utils/CommonThreads;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/utils/CommonThreads;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lgbis/gbandroid/utils/CommonThreads$b;->c:Lgbis/gbandroid/utils/CommonThreads;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 37
    iput-object p2, p0, Lgbis/gbandroid/utils/CommonThreads$b;->a:Landroid/os/Handler;

    .line 38
    iput-object p3, p0, Lgbis/gbandroid/utils/CommonThreads$b;->b:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$b;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 42
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 44
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$b;->c:Lgbis/gbandroid/utils/CommonThreads;

    invoke-static {v0}, Lgbis/gbandroid/utils/CommonThreads;->a(Lgbis/gbandroid/utils/CommonThreads;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    const-string v0, "result"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$b;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 57
    :goto_1
    return-void

    .line 47
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$b;->c:Lgbis/gbandroid/utils/CommonThreads;

    iget-object v3, p0, Lgbis/gbandroid/utils/CommonThreads$b;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lgbis/gbandroid/utils/CommonThreads;->a(Lgbis/gbandroid/utils/CommonThreads;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 48
    const-string v3, "result"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 49
    const-string v3, "photo"

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "result"

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads$b;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 53
    :catchall_0
    move-exception v0

    .line 54
    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 55
    iget-object v2, p0, Lgbis/gbandroid/utils/CommonThreads$b;->a:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 56
    throw v0
.end method
