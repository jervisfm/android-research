.class public Lgbis/gbandroid/utils/CommonThreads;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/utils/CommonThreads$a;,
        Lgbis/gbandroid/utils/CommonThreads$b;
    }
.end annotation


# static fields
.field public static final GALLERY:Ljava/lang/String; = "gallery"

.field public static final PHOTO:Ljava/lang/String; = "photo"


# instance fields
.field private a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lgbis/gbandroid/utils/CommonThreads;->a:Landroid/app/Activity;

    .line 21
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/utils/CommonThreads;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lgbis/gbandroid/utils/CommonThreads;->a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .parameter

    .prologue
    .line 100
    invoke-direct {p0}, Lgbis/gbandroid/utils/CommonThreads;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 102
    :cond_0
    invoke-static {p1}, Lgbis/gbandroid/utils/ImageUtils;->drawable_from_url(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lgbis/gbandroid/utils/CommonThreads;->a:Landroid/app/Activity;

    invoke-static {v0}, Lgbis/gbandroid/utils/ConnectionUtils;->isConnectedToAny(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lgbis/gbandroid/utils/CommonThreads;)Z
    .locals 1
    .parameter

    .prologue
    .line 95
    invoke-direct {p0}, Lgbis/gbandroid/utils/CommonThreads;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public startGalleryProgressThread(Landroid/os/Handler;[Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 29
    new-instance v0, Lgbis/gbandroid/utils/CommonThreads$a;

    invoke-direct {v0, p0, p1, p2}, Lgbis/gbandroid/utils/CommonThreads$a;-><init>(Lgbis/gbandroid/utils/CommonThreads;Landroid/os/Handler;[Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0}, Lgbis/gbandroid/utils/CommonThreads$a;->start()V

    .line 31
    return-void
.end method

.method public startPhotoProgressThread(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 24
    new-instance v0, Lgbis/gbandroid/utils/CommonThreads$b;

    invoke-direct {v0, p0, p1, p2}, Lgbis/gbandroid/utils/CommonThreads$b;-><init>(Lgbis/gbandroid/utils/CommonThreads;Landroid/os/Handler;Ljava/lang/String;)V

    .line 25
    invoke-virtual {v0}, Lgbis/gbandroid/utils/CommonThreads$b;->start()V

    .line 26
    return-void
.end method
