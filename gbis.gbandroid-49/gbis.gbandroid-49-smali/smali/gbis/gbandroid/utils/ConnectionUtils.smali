.class public abstract Lgbis/gbandroid/utils/ConnectionUtils;
.super Ljava/lang/Object;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 77
    if-nez p0, :cond_1

    .line 78
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isBetterLocation(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 32
    if-nez p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    if-nez p0, :cond_2

    move v1, v2

    .line 37
    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    sub-long v4, v3, v5

    .line 41
    const-wide/32 v6, 0x1d4c0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    move v3, v1

    .line 42
    :goto_1
    const-wide/32 v6, -0x1d4c0

    cmp-long v0, v4, v6

    if-gez v0, :cond_4

    move v0, v1

    .line 43
    :goto_2
    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    move v5, v1

    .line 47
    :goto_3
    if-nez v3, :cond_0

    .line 50
    if-eqz v0, :cond_6

    move v1, v2

    .line 51
    goto :goto_0

    :cond_3
    move v3, v2

    .line 41
    goto :goto_1

    :cond_4
    move v0, v2

    .line 42
    goto :goto_2

    :cond_5
    move v5, v2

    .line 43
    goto :goto_3

    .line 55
    :cond_6
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    .line 56
    if-lez v0, :cond_9

    move v4, v1

    .line 57
    :goto_4
    if-gez v0, :cond_a

    move v3, v1

    .line 58
    :goto_5
    const/16 v6, 0xc8

    if-le v0, v6, :cond_b

    move v0, v1

    .line 61
    :goto_6
    invoke-virtual {p0}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v6

    .line 62
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v7

    .line 61
    invoke-static {v6, v7}, Lgbis/gbandroid/utils/ConnectionUtils;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 65
    if-nez v3, :cond_0

    .line 67
    if-eqz v5, :cond_7

    if-eqz v4, :cond_0

    .line 69
    :cond_7
    if-eqz v5, :cond_8

    if-nez v0, :cond_8

    if-nez v6, :cond_0

    :cond_8
    move v1, v2

    .line 72
    goto :goto_0

    :cond_9
    move v4, v2

    .line 56
    goto :goto_4

    :cond_a
    move v3, v2

    .line 57
    goto :goto_5

    :cond_b
    move v0, v2

    .line 58
    goto :goto_6
.end method

.method public static isConnectedToAny(Landroid/content/Context;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 11
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 14
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 23
    :goto_0
    return v2

    .line 18
    :cond_0
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    .line 17
    :goto_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v4

    array-length v4, v4

    if-ge v3, v4, :cond_1

    if-eqz v1, :cond_0

    :cond_1
    move v2, v1

    .line 23
    goto :goto_0

    .line 21
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v3, v1

    goto :goto_1
.end method
