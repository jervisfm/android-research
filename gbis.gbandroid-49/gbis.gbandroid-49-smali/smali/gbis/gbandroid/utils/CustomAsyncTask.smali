.class public abstract Lgbis/gbandroid/utils/CustomAsyncTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    iput-object p1, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->b:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    iput-object p1, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    .line 37
    iput-boolean p2, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->b:Z

    .line 38
    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 148
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 149
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 150
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 135
    :try_start_0
    new-instance v0, Lgbis/gbandroid/utils/b;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/b;-><init>(Lgbis/gbandroid/utils/CustomAsyncTask;)V

    invoke-virtual {v0}, Lgbis/gbandroid/utils/b;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 136
    new-instance v1, Lgbis/gbandroid/queries/CrashReportQuery;

    iget-object v2, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    iget-object v3, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Landroid/location/Location;

    const-string v5, "NoLocation"

    invoke-direct {v4, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3, v0, v4}, Lgbis/gbandroid/queries/CrashReportQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/Base64;->encodeBytes([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/CrashReportQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    invoke-static {v0}, Lgbis/gbandroid/utils/ConnectionUtils;->isConnectedToAny(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 76
    :try_start_0
    invoke-direct {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 77
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    iget-object v1, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V

    .line 85
    :goto_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 123
    :goto_1
    return-object v0

    .line 79
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    iget-object v1, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->setMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lgbis/gbandroid/exceptions/RequestParsingWSException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v1, 0x7f090003

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 90
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_4

    .line 91
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V

    .line 99
    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 81
    :cond_1
    :try_start_1
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    iget-object v1, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMessage(Ljava/lang/String;)V
    :try_end_1
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lgbis/gbandroid/exceptions/RequestParsingWSException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 100
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 101
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const/high16 v2, 0x7f09

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 102
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_7

    .line 103
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V

    .line 110
    :goto_3
    invoke-static {v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lgbis/gbandroid/utils/CustomAsyncTask;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 83
    :cond_2
    :try_start_2
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    goto :goto_0

    .line 87
    :cond_3
    invoke-virtual {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->queryWebService()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lgbis/gbandroid/exceptions/RequestParsingWSException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_1

    .line 92
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_5

    .line 93
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->setMessage(Ljava/lang/String;)V

    goto :goto_2

    .line 94
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    if-eqz v0, :cond_6

    .line 95
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMessage(Ljava/lang/String;)V

    goto :goto_2

    .line 96
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    goto :goto_2

    .line 104
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_8

    .line 105
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBActivityMap;->setMessage(Ljava/lang/String;)V

    goto :goto_3

    .line 106
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    if-eqz v0, :cond_9

    .line 107
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMessage(Ljava/lang/String;)V

    goto :goto_3

    .line 108
    :cond_9
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    goto :goto_3

    .line 112
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 113
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v2, 0x7f090018

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 114
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_a

    .line 115
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBActivity;->setMessage(Ljava/lang/String;)V

    .line 122
    :goto_4
    invoke-static {v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->a(Ljava/lang/Exception;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lgbis/gbandroid/utils/CustomAsyncTask;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    .line 116
    :cond_a
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_b

    .line 117
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBActivityMap;->setMessage(Ljava/lang/String;)V

    goto :goto_4

    .line 118
    :cond_b
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    if-eqz v0, :cond_c

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v0, v2}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->setMessage(Ljava/lang/String;)V

    goto :goto_4

    .line 120
    :cond_c
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    goto :goto_4
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_3

    .line 44
    iget-boolean v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->b:Z

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage()V

    .line 46
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_1

    .line 48
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ResponseMessage;->getResponseMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivity;->showMessage(Ljava/lang/String;)V

    .line 64
    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivity;

    if-eqz v0, :cond_8

    .line 66
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->checkAwardAndShow()V

    .line 67
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivity;->checkSmartPromptAndShow()V

    .line 71
    :cond_2
    :goto_1
    return-void

    .line 49
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_5

    .line 50
    iget-boolean v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->b:Z

    if-eqz v0, :cond_4

    .line 51
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->showMessage()V

    .line 52
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_1

    .line 54
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ResponseMessage;->getResponseMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBActivityMap;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    if-eqz v0, :cond_7

    .line 56
    iget-boolean v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->b:Z

    if-eqz v0, :cond_6

    .line 57
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->showMessage()V

    .line 58
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v1

    .line 59
    if-eqz v1, :cond_1

    .line 60
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBPreferenceActivity;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ResponseMessage;->getResponseMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    goto :goto_0

    .line 68
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    instance-of v0, v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    if-eqz v0, :cond_2

    .line 69
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    check-cast v0, Lgbis/gbandroid/activities/base/GBActivityMap;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/base/GBActivityMap;->checkAwardAndShow()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected abstract queryWebService()Z
.end method

.method protected setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v1, 0x7f090257

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;->setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method protected setAnalyticsTrackEventScreenButton(Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    if-eqz p1, :cond_0

    .line 155
    iget-object v0, p0, Lgbis/gbandroid/utils/CustomAsyncTask;->a:Landroid/content/Context;

    const v1, 0x7f090256

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p3, p2, v1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 156
    :cond_0
    return-void
.end method
