.class final Lgbis/gbandroid/utils/DBHelper$a;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/DBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/utils/DBHelper;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/utils/DBHelper;Landroid/content/Context;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 314
    iput-object p1, p0, Lgbis/gbandroid/utils/DBHelper$a;->a:Lgbis/gbandroid/utils/DBHelper;

    .line 315
    const-string v0, "gasBuddyDB2"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 316
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter

    .prologue
    .line 351
    const-string v0, "create table tableSavedSearches (_id integer primary key autoincrement not null,table_column_name text,table_column_city_zip text,table_column_latitude decimal(9,6),table_column_longitude decimal(9,6),table_column_frequency int,CONSTRAINT table_constraints UNIQUE (table_column_name)CONSTRAINT table_constraints UNIQUE (table_column_city_zip));"

    .line 361
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "select DISTINCT tbl_name from sqlite_master where tbl_name = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 342
    if-eqz v0, :cond_0

    .line 343
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 344
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter

    .prologue
    .line 365
    const-string v0, "create table tableBrands (_id integer primary key autoincrement not null,brand_id text,version integer,add_dt long,image_location text,CONSTRAINT table_constraints UNIQUE (brand_id));"

    .line 373
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 374
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .parameter

    .prologue
    .line 320
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper$a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 321
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper$a;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 322
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .parameter

    .prologue
    .line 333
    invoke-super {p0, p1}, Landroid/database/sqlite/SQLiteOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 334
    const-string v0, "tableSavedSearches"

    invoke-static {p1, v0}, Lgbis/gbandroid/utils/DBHelper$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper$a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 336
    :cond_0
    const-string v0, "tableBrands"

    invoke-static {p1, v0}, Lgbis/gbandroid/utils/DBHelper$a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper$a;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 338
    :cond_1
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 326
    const-string v0, "DROP TABLE IF EXISTS tableSavedSearches"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 327
    const-string v0, "DROP TABLE IF EXISTS tableBrands"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0, p1}, Lgbis/gbandroid/utils/DBHelper$a;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 329
    return-void
.end method
