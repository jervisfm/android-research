.class public Lgbis/gbandroid/utils/DBHelper;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/utils/DBHelper$a;
    }
.end annotation


# static fields
.field public static final NONE:I = -0x1

.field public static final ORDER_BY_FREQUENCY:I = 0x0

.field public static final ORDER_BY_NAME:I = 0x1

.field public static final TABLE_COLUMN_ADD_DATE:Ljava/lang/String; = "add_dt"

.field public static final TABLE_COLUMN_BRAND_ID:Ljava/lang/String; = "brand_id"

.field public static final TABLE_COLUMN_CITY_ZIP:Ljava/lang/String; = "table_column_city_zip"

.field public static final TABLE_COLUMN_FREQUENCY:Ljava/lang/String; = "table_column_frequency"

.field public static final TABLE_COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final TABLE_COLUMN_IMAGE_LOCATION:Ljava/lang/String; = "image_location"

.field public static final TABLE_COLUMN_LATITUDE:Ljava/lang/String; = "table_column_latitude"

.field public static final TABLE_COLUMN_LONGITUDE:Ljava/lang/String; = "table_column_longitude"

.field public static final TABLE_COLUMN_NAME:Ljava/lang/String; = "table_column_name"

.field public static final TABLE_COLUMN_VERSION:Ljava/lang/String; = "version"


# instance fields
.field private a:Landroid/database/sqlite/SQLiteDatabase;

.field private b:Lgbis/gbandroid/utils/DBHelper$a;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "gasBuddyDB2"

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->c:Ljava/lang/String;

    .line 18
    const-string v0, "tableSavedSearches"

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->d:Ljava/lang/String;

    .line 19
    const-string v0, "tableBrands"

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->e:Ljava/lang/String;

    .line 20
    const/4 v0, 0x1

    iput v0, p0, Lgbis/gbandroid/utils/DBHelper;->f:I

    .line 33
    const-string v0, "table_constraints"

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->g:Ljava/lang/String;

    .line 36
    new-instance v0, Lgbis/gbandroid/utils/DBHelper$a;

    invoke-direct {v0, p0, p1}, Lgbis/gbandroid/utils/DBHelper$a;-><init>(Lgbis/gbandroid/utils/DBHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->b:Lgbis/gbandroid/utils/DBHelper$a;

    .line 38
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 41
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addBrandRow(IILjava/lang/String;)Z
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 242
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 243
    const-string v1, "brand_id"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    const-string v1, "version"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 245
    const-string v1, "image_location"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v1, "add_dt"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 248
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tableBrands"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    const/4 v0, 0x1

    .line 253
    :goto_0
    return v0

    .line 250
    :catch_0
    move-exception v0

    .line 251
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 253
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 46
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 47
    const-string v3, "table_column_name"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v1, "table_column_city_zip"

    invoke-virtual {v2, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "table_column_latitude"

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 50
    const-string v1, "table_column_longitude"

    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 51
    const-string v1, "table_column_frequency"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 53
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "tableSavedSearches"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    return v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 58
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeDB()V
    .locals 1

    .prologue
    .line 289
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteSearchesAllRows()Z
    .locals 4

    .prologue
    .line 87
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tableSavedSearches"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 92
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteSearchesRow(J)Z
    .locals 4
    .parameter

    .prologue
    .line 64
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "tableSavedSearches"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 69
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteSearchesRow(Ljava/lang/String;)Z
    .locals 5
    .parameter

    .prologue
    .line 74
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tableSavedSearches"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "table_column_name=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 81
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllSearchesRows(I)Landroid/database/Cursor;
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 160
    .line 162
    packed-switch p1, :pswitch_data_0

    move-object v7, v8

    .line 175
    :goto_0
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 176
    const-string v1, "tableSavedSearches"

    .line 177
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "table_column_name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "table_column_city_zip"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 178
    const-string v4, "table_column_latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "table_column_longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "table_column_frequency"

    aput-object v4, v2, v3

    .line 179
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 175
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 185
    :goto_1
    return-object v0

    .line 164
    :pswitch_0
    const-string v7, "table_column_frequency DESC"

    goto :goto_0

    .line 167
    :pswitch_1
    const-string v7, "UPPER(table_column_name)"

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const-string v1, "DB Error"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v8

    goto :goto_1

    .line 162
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getBrandRow(I)Landroid/database/Cursor;
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 258
    .line 260
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 261
    const-string v1, "tableBrands"

    .line 262
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "brand_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "version"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 263
    const-string v4, "image_location"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "add_dt"

    aput-object v4, v2, v3

    .line 264
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "brand_id=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 265
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 260
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 271
    :goto_0
    return-object v0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    goto :goto_0
.end method

.method public getSearchesRow(J)Landroid/database/Cursor;
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 189
    .line 191
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 192
    const-string v1, "tableSavedSearches"

    .line 193
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "table_column_name"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "table_column_city_zip"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 194
    const-string v4, "table_column_latitude"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "table_column_longitude"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "table_column_frequency"

    aput-object v4, v2, v3

    .line 195
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 196
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 191
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    goto :goto_0
.end method

.method public getSearchesRow(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 206
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 209
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 210
    const-string v1, "tableSavedSearches"

    .line 211
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "table_column_name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "table_column_city_zip"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    .line 212
    const-string v5, "table_column_latitude"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "table_column_longitude"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "table_column_frequency"

    aput-object v5, v2, v4

    .line 213
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "table_column_name=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 214
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 209
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    .line 216
    :catch_0
    move-exception v0

    .line 217
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    goto :goto_0
.end method

.method public getSearchesRowFromCityZip(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 224
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 227
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 228
    const-string v1, "tableSavedSearches"

    .line 229
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "table_column_name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "table_column_city_zip"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    .line 230
    const-string v5, "table_column_latitude"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "table_column_longitude"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "table_column_frequency"

    aput-object v5, v2, v4

    .line 231
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "table_column_city_zip=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 232
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 227
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    const-string v1, "DB ERROR"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    move-object v0, v9

    goto :goto_0
.end method

.method public incrementSearchesFrequency(Ljava/lang/String;)Z
    .locals 3
    .parameter

    .prologue
    .line 145
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UPDATE tableSavedSearches SET table_column_frequency=(table_column_frequency+1) WHERE table_column_name=\'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 150
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    const-string v1, "DB Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 155
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDBOpen()Z
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    .line 309
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openDB()Z
    .locals 1

    .prologue
    .line 298
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->b:Lgbis/gbandroid/utils/DBHelper$a;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper$a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    const/4 v0, 0x1

    .line 302
    :goto_0
    return v0

    .line 300
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 302
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateBrandVersionRow(II)Z
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 275
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 276
    const-string v1, "version"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 278
    :try_start_0
    iget-object v1, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tableBrands"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "brand_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    const-string v1, "DB Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 283
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateSearchesName(JLjava/lang/String;)Z
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-static {p3}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 133
    const-string v2, "table_column_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tableSavedSearches"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    const-string v1, "DB Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 140
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateSearchesRow(JLjava/lang/String;Ljava/lang/String;DD)Z
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-static {p3}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 99
    const-string v2, "table_column_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "table_column_city_zip"

    invoke-virtual {v1, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "table_column_latitude"

    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 102
    const-string v0, "table_column_longitude"

    invoke-static {p7, p8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 104
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "tableSavedSearches"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    const/4 v0, 0x1

    .line 109
    :goto_0
    return v0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    const-string v1, "DB Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 109
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-static {p1}, Lgbis/gbandroid/utils/DBHelper;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 116
    const-string v2, "table_column_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v2, "table_column_city_zip"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v2, "table_column_latitude"

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 119
    const-string v2, "table_column_longitude"

    invoke-static {p5, p6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 121
    :try_start_0
    iget-object v2, p0, Lgbis/gbandroid/utils/DBHelper;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "tableSavedSearches"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "table_column_name=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    const-string v1, "DB Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    const/4 v0, 0x0

    goto :goto_0
.end method
