.class public abstract Lgbis/gbandroid/utils/FieldEncryptionHelper;
.super Ljava/lang/Object;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCInstance()Ljava/lang/String;
    .locals 3

    .prologue
    .line 16
    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 17
    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->encode64([B)Ljava/lang/String;

    move-result-object v0

    .line 18
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 16
    nop

    :array_0
    .array-data 0x1
        0xct
        0x44t
        0x9et
        0x75t
        0xeft
        0xc2t
        0x4t
        0x2ft
        0xcft
        0x28t
        0x24t
        0xb9t
        0x3dt
        0xa7t
        0x5dt
        0x8at
        0x78t
        0x3ft
    .end array-data
.end method

.method public static getKy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5
    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 6
    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->encode64([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 5
    :array_0
    .array-data 0x1
        0xcft
        0x16t
        0xact
        0xabt
        0x7t
        0xabt
        0x75t
        0xf7t
        0x2ft
        0xb7t
        0x28t
        0x21t
        0x6et
        0x7bt
        0xa2t
        0x8et
        0x49t
        0xe6t
    .end array-data
.end method

.method public static getSKS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 10
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    .line 11
    invoke-static {v0}, Lgbis/gbandroid/utils/FieldEncryption;->encode64([B)Ljava/lang/String;

    move-result-object v0

    .line 12
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 10
    :array_0
    .array-data 0x1
        0xct
        0x44t
        0x9et
        0x75t
        0xeft
        0xc2t
    .end array-data
.end method
