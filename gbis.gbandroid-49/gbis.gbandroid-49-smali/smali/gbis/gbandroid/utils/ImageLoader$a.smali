.class final Lgbis/gbandroid/utils/ImageLoader$a;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Landroid/widget/ImageView;

.field final synthetic c:Lgbis/gbandroid/utils/ImageLoader;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/utils/ImageLoader;Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 282
    iput-object p1, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p2, p0, Lgbis/gbandroid/utils/ImageLoader$a;->a:Landroid/graphics/Bitmap;

    .line 284
    iput-object p3, p0, Lgbis/gbandroid/utils/ImageLoader$a;->b:Landroid/widget/ImageView;

    .line 285
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 288
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 289
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader;->d(Lgbis/gbandroid/utils/ImageLoader;)Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader;->d(Lgbis/gbandroid/utils/ImageLoader;)Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader$a;->b:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lgbis/gbandroid/listeners/ImageLazyLoaderListener;->onImageLoaded(Landroid/widget/ImageView;)V

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader;->e(Lgbis/gbandroid/utils/ImageLoader;)I

    move-result v0

    if-lez v0, :cond_2

    .line 293
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v1}, Lgbis/gbandroid/utils/ImageLoader;->e(Lgbis/gbandroid/utils/ImageLoader;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 294
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader;->d(Lgbis/gbandroid/utils/ImageLoader;)Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$a;->c:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader;->d(Lgbis/gbandroid/utils/ImageLoader;)Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    move-result-object v0

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader$a;->b:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lgbis/gbandroid/listeners/ImageLazyLoaderListener;->onImageFailed(Landroid/widget/ImageView;)V

    goto :goto_0
.end method
