.class final Lgbis/gbandroid/utils/ImageLoader$c;
.super Ljava/lang/Thread;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/utils/ImageLoader;


# direct methods
.method constructor <init>(Lgbis/gbandroid/utils/ImageLoader;)V
    .locals 0
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 248
    :cond_0
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 249
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :try_start_1
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 249
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    :cond_1
    :try_start_2
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v1

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 255
    :try_start_3
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/utils/ImageLoader$b;

    .line 254
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 257
    :try_start_4
    iget-object v1, v0, Lgbis/gbandroid/utils/ImageLoader$b;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v2}, Lgbis/gbandroid/utils/ImageLoader;->a(Lgbis/gbandroid/utils/ImageLoader;)Ljava/io/File;

    move-result-object v2

    iget-object v3, v0, Lgbis/gbandroid/utils/ImageLoader$b;->c:Ljava/lang/String;

    iget-object v4, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v4}, Lgbis/gbandroid/utils/ImageLoader;->b(Lgbis/gbandroid/utils/ImageLoader;)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lgbis/gbandroid/utils/ImageLoader;->getBitmap(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 258
    if-eqz v2, :cond_2

    .line 259
    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    invoke-static {v1}, Lgbis/gbandroid/utils/ImageLoader;->c(Lgbis/gbandroid/utils/ImageLoader;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, v0, Lgbis/gbandroid/utils/ImageLoader$b;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_2
    iget-object v1, v0, Lgbis/gbandroid/utils/ImageLoader$b;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 261
    if-eqz v1, :cond_3

    check-cast v1, Ljava/lang/String;

    iget-object v3, v0, Lgbis/gbandroid/utils/ImageLoader$b;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 262
    new-instance v1, Lgbis/gbandroid/utils/ImageLoader$a;

    iget-object v3, p0, Lgbis/gbandroid/utils/ImageLoader$c;->a:Lgbis/gbandroid/utils/ImageLoader;

    iget-object v4, v0, Lgbis/gbandroid/utils/ImageLoader$b;->b:Landroid/widget/ImageView;

    invoke-direct {v1, v3, v2, v4}, Lgbis/gbandroid/utils/ImageLoader$a;-><init>(Lgbis/gbandroid/utils/ImageLoader;Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    .line 263
    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader$b;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 264
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 267
    :cond_3
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 273
    :catch_0
    move-exception v0

    goto :goto_0

    .line 254
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
.end method
