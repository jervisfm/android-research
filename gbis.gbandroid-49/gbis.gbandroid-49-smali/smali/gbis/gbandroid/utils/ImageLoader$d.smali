.class final Lgbis/gbandroid/utils/ImageLoader$d;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/utils/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "d"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/utils/ImageLoader;

.field private b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lgbis/gbandroid/utils/ImageLoader$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lgbis/gbandroid/utils/ImageLoader;)V
    .locals 1
    .parameter

    .prologue
    .line 229
    iput-object p1, p0, Lgbis/gbandroid/utils/ImageLoader$d;->a:Lgbis/gbandroid/utils/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$d;->b:Ljava/util/Stack;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;
    .locals 1
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$d;->b:Ljava/util/Stack;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;)V
    .locals 2
    .parameter

    .prologue
    .line 234
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$d;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 240
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$d;->b:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/utils/ImageLoader$b;

    iget-object v0, v0, Lgbis/gbandroid/utils/ImageLoader$b;->b:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_1

    .line 236
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader$d;->b:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 238
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
