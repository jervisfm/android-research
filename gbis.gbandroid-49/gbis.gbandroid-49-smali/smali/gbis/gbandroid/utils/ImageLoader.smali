.class public Lgbis/gbandroid/utils/ImageLoader;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/utils/ImageLoader$a;,
        Lgbis/gbandroid/utils/ImageLoader$b;,
        Lgbis/gbandroid/utils/ImageLoader$c;,
        Lgbis/gbandroid/utils/ImageLoader$d;
    }
.end annotation


# static fields
.field public static final DEFAULT_DIRECTORY:Ljava/lang/String; = "/Android/data/gbis.gbandroid/cache/LazyList"


# instance fields
.field a:Lgbis/gbandroid/utils/ImageLoader$d;

.field b:Lgbis/gbandroid/utils/ImageLoader$c;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/io/File;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Lgbis/gbandroid/listeners/ImageLazyLoaderListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    .line 222
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/ImageLoader$d;-><init>(Lgbis/gbandroid/utils/ImageLoader;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    .line 276
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader$c;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/ImageLoader$c;-><init>(Lgbis/gbandroid/utils/ImageLoader;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    .line 35
    const-string v0, "/Android/data/gbis.gbandroid/cache/LazyList"

    invoke-direct {p0, p1, p2, v0}, Lgbis/gbandroid/utils/ImageLoader;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    .line 222
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/ImageLoader$d;-><init>(Lgbis/gbandroid/utils/ImageLoader;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    .line 276
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader$c;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/ImageLoader$c;-><init>(Lgbis/gbandroid/utils/ImageLoader;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/utils/ImageLoader;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 40
    return-void
.end method

.method private static a(Ljava/io/File;I)Landroid/graphics/Bitmap;
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 185
    if-eqz p1, :cond_1

    .line 186
    :try_start_0
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 187
    const/4 v2, 0x1

    iput-boolean v2, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 188
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v4, 0x0

    invoke-static {v2, v4, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 190
    iget v2, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 193
    :goto_0
    div-int/lit8 v4, v2, 0x2

    if-lt v4, p1, :cond_0

    div-int/lit8 v4, v3, 0x2

    if-lt v4, p1, :cond_0

    .line 194
    div-int/lit8 v2, v2, 0x2

    .line 196
    div-int/lit8 v3, v3, 0x2

    .line 197
    mul-int/lit8 v1, v1, 0x2

    .line 192
    goto :goto_0

    .line 201
    :cond_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 202
    iput v1, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 203
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 207
    :goto_1
    return-object v0

    .line 205
    :cond_1
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    .line 207
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/utils/ImageLoader;)Ljava/io/File;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    return-object v0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/ImageLoader$c;->setPriority(I)V

    .line 45
    iput p2, p0, Lgbis/gbandroid/utils/ImageLoader;->e:I

    .line 48
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    .line 52
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 54
    :cond_0
    return-void

    .line 51
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-virtual {v0, p2}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Landroid/widget/ImageView;)V

    .line 147
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader$b;

    invoke-direct {v0, p0, p1, p2, p3}, Lgbis/gbandroid/utils/ImageLoader$b;-><init>(Lgbis/gbandroid/utils/ImageLoader;Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v1}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v1

    monitor-enter v1

    .line 149
    :try_start_0
    iget-object v2, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v2}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->a:Lgbis/gbandroid/utils/ImageLoader$d;

    invoke-static {v0}, Lgbis/gbandroid/utils/ImageLoader$d;->a(Lgbis/gbandroid/utils/ImageLoader$d;)Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 148
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/ImageLoader$c;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/ImageLoader$c;->start()V

    .line 156
    :cond_0
    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lgbis/gbandroid/utils/ImageLoader;)I
    .locals 1
    .parameter

    .prologue
    .line 30
    iget v0, p0, Lgbis/gbandroid/utils/ImageLoader;->f:I

    return v0
.end method

.method static synthetic c(Lgbis/gbandroid/utils/ImageLoader;)Ljava/util/HashMap;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic d(Lgbis/gbandroid/utils/ImageLoader;)Lgbis/gbandroid/listeners/ImageLazyLoaderListener;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->h:Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    return-object v0
.end method

.method static synthetic e(Lgbis/gbandroid/utils/ImageLoader;)I
    .locals 1
    .parameter

    .prologue
    .line 29
    iget v0, p0, Lgbis/gbandroid/utils/ImageLoader;->e:I

    return v0
.end method

.method public static getBitmap(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 162
    invoke-static {v1, p3}, Lgbis/gbandroid/utils/ImageLoader;->a(Ljava/io/File;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 177
    :goto_0
    return-object v0

    .line 168
    :cond_0
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    .line 170
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 171
    invoke-static {v0, v2}, Lgbis/gbandroid/utils/ImageUtils;->CopyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 172
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 173
    invoke-static {v1, p3}, Lgbis/gbandroid/utils/ImageLoader;->a(Ljava/io/File;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 177
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getExistentImage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    invoke-static {p0, p1, p2}, Lgbis/gbandroid/utils/ImageLoader;->getImageFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 112
    invoke-static {v0, p3}, Lgbis/gbandroid/utils/ImageLoader;->a(Ljava/io/File;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 113
    return-object v0
.end method

.method public static getImageDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 99
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 101
    :cond_0
    return-object v0

    .line 98
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public static getImageFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 87
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 89
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 90
    return-object v1

    .line 86
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public static isImageExistant(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 75
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    const/4 v0, 0x0

    .line 78
    :goto_1
    return v0

    .line 74
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public clearCache()V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 305
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 306
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    .line 308
    return-void

    .line 306
    :cond_0
    aget-object v3, v1, v0

    .line 307
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;Z)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZLjava/lang/String;)V

    .line 58
    return-void
.end method

.method public displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZI)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZILjava/lang/String;)V

    .line 62
    return-void
.end method

.method public displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZILjava/lang/String;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    iput p5, p0, Lgbis/gbandroid/utils/ImageLoader;->f:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p6

    .line 66
    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZLjava/lang/String;)V

    .line 67
    return-void
.end method

.method public displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZLjava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    iput-object p5, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    .line 122
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    iget-object v2, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 123
    if-eqz p4, :cond_2

    .line 124
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_0
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 132
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 138
    :cond_1
    :goto_1
    return-void

    .line 126
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->d:Ljava/io/File;

    iget v1, p0, Lgbis/gbandroid/utils/ImageLoader;->f:I

    invoke-static {p1, v0, p5, v1}, Lgbis/gbandroid/utils/ImageLoader;->getBitmap(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lgbis/gbandroid/utils/ImageLoader;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 134
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->g:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0}, Lgbis/gbandroid/utils/ImageLoader;->a(Ljava/lang/String;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 135
    iget v0, p0, Lgbis/gbandroid/utils/ImageLoader;->e:I

    if-lez v0, :cond_1

    .line 136
    iget v0, p0, Lgbis/gbandroid/utils/ImageLoader;->e:I

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public setOnLazyLoaderFinished(Lgbis/gbandroid/listeners/ImageLazyLoaderListener;)V
    .locals 0
    .parameter

    .prologue
    .line 141
    iput-object p1, p0, Lgbis/gbandroid/utils/ImageLoader;->h:Lgbis/gbandroid/listeners/ImageLazyLoaderListener;

    .line 142
    return-void
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lgbis/gbandroid/utils/ImageLoader;->b:Lgbis/gbandroid/utils/ImageLoader$c;

    invoke-virtual {v0}, Lgbis/gbandroid/utils/ImageLoader$c;->interrupt()V

    .line 226
    return-void
.end method
