.class public abstract Lgbis/gbandroid/utils/ImageUtils;
.super Ljava/lang/Object;
.source "GBFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static CopyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 33
    const/16 v0, 0x400

    :try_start_0
    new-array v0, v0, [B

    .line 37
    :goto_0
    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-virtual {p0, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 38
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 39
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    :cond_0
    return-void
.end method

.method public static drawable_from_url(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 2
    .parameter

    .prologue
    .line 27
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getContent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Ljava/io/InputStream;)V

    return-object v1
.end method

.method public static getCarFromName(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawable"

    const-string v2, "gbis.gbandroid"

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 19
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getResolutionInText(F)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    .line 47
    float-to-double v0, p0

    const-wide/high16 v2, 0x3fe8

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 48
    const-string v0, "ldpi"

    .line 55
    :goto_0
    return-object v0

    .line 49
    :cond_0
    const/high16 v0, 0x3f80

    cmpl-float v0, p0, v0

    if-nez v0, :cond_1

    .line 50
    const-string v0, "mdpi"

    goto :goto_0

    .line 51
    :cond_1
    float-to-double v0, p0

    const-wide/high16 v2, 0x3ff8

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    .line 52
    const-string v0, "hdpi"

    goto :goto_0

    .line 53
    :cond_2
    const/high16 v0, 0x4000

    cmpl-float v0, p0, v0

    if-nez v0, :cond_3

    .line 54
    const-string v0, "xhdpi"

    goto :goto_0

    .line 55
    :cond_3
    const-string v0, "mdpi"

    goto :goto_0
.end method

.method public static getStationLogoId(Landroid/content/res/Resources;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "logo_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawable"

    const-string v2, "gbis.gbandroid"

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
