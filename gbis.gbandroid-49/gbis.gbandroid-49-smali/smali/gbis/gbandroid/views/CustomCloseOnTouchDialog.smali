.class public Lgbis/gbandroid/views/CustomCloseOnTouchDialog;
.super Lgbis/gbandroid/views/CustomDialog;
.source "GBFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/CustomDialog;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/views/CustomDialog;-><init>(Landroid/content/Context;I)V

    .line 12
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 27
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;->dismiss()V

    .line 28
    const/4 v0, 0x1

    .line 30
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lgbis/gbandroid/views/CustomDialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x4

    const/16 v1, 0x20

    .line 19
    invoke-super {p0, p1}, Lgbis/gbandroid/views/CustomDialog;->onCreate(Landroid/os/Bundle;)V

    .line 20
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 21
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 22
    return-void
.end method
