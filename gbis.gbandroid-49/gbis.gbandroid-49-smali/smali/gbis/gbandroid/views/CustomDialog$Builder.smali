.class public Lgbis/gbandroid/views/CustomDialog$Builder;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/views/CustomDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/Bitmap;

.field private i:Landroid/view/View;

.field private j:Landroid/content/DialogInterface$OnClickListener;

.field private k:Landroid/content/DialogInterface$OnClickListener;

.field private l:Landroid/content/DialogInterface$OnClickListener;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

.field private q:Ljava/lang/String;

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    const-string v1, ""

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0, p3}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V

    .line 66
    return-void
.end method

.method private a(Landroid/app/Dialog;Z)Landroid/app/Dialog;
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v8, -0x2

    const v7, 0x7f07002f

    const/4 v6, -0x1

    const/16 v5, 0x8

    .line 302
    const/4 v2, 0x0

    .line 306
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 307
    const v3, 0x7f03000f

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 308
    const v0, 0x7f070035

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    .line 309
    const v0, 0x7f070036

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    .line 310
    const v0, 0x7f070037

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    .line 311
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v3, v0}, Landroid/app/Dialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    const v0, 0x7f070030

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    .line 314
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 319
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->d:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 320
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->j:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_f

    .line 323
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    new-instance v2, Lgbis/gbandroid/views/a;

    invoke-direct {v2, p0, p1}, Lgbis/gbandroid/views/a;-><init>(Lgbis/gbandroid/views/CustomDialog$Builder;Landroid/app/Dialog;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v1

    .line 333
    :goto_1
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 334
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    iget-object v4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 335
    or-int/lit8 v0, v0, 0x4

    .line 336
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->l:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v2, :cond_0

    .line 337
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    new-instance v4, Lgbis/gbandroid/views/b;

    invoke-direct {v4, p0, p1}, Lgbis/gbandroid/views/b;-><init>(Lgbis/gbandroid/views/CustomDialog$Builder;Landroid/app/Dialog;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    :cond_0
    :goto_2
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 348
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    iget-object v4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->f:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 349
    or-int/lit8 v0, v0, 0x2

    .line 350
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->k:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v2, :cond_1

    .line 351
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    new-instance v4, Lgbis/gbandroid/views/c;

    invoke-direct {v4, p0, p1}, Lgbis/gbandroid/views/c;-><init>(Lgbis/gbandroid/views/CustomDialog$Builder;Landroid/app/Dialog;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    :cond_1
    :goto_3
    if-ne v0, v1, :cond_b

    .line 362
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    invoke-static {v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/widget/Button;Landroid/view/View;)V

    .line 373
    :cond_2
    :goto_4
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->c:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 374
    const v0, 0x7f070032

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    :cond_3
    :goto_5
    invoke-virtual {p1, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 387
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->r:Z

    if-eqz v0, :cond_5

    .line 388
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 389
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " Dialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->q:Ljava/lang/String;

    .line 390
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackPageView(Ljava/lang/String;)V

    .line 392
    :cond_5
    return-object p1

    .line 315
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->h:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_7

    .line 316
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 318
    :cond_7
    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 331
    :cond_8
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    move v0, v2

    goto/16 :goto_1

    .line 345
    :cond_9
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_2

    .line 359
    :cond_a
    iget-object v2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    .line 364
    :cond_b
    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 365
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    invoke-static {v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/widget/Button;Landroid/view/View;)V

    goto :goto_4

    .line 367
    :cond_c
    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 368
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    invoke-static {v0, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/widget/Button;Landroid/view/View;)V

    goto/16 :goto_4

    .line 370
    :cond_d
    if-nez v0, :cond_2

    .line 371
    const v0, 0x7f070033

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 376
    :cond_e
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->i:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 377
    const v0, 0x7f070031

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 378
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 379
    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->i:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    .line 381
    invoke-direct {v2, v6, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 379
    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 382
    if-eqz p2, :cond_3

    .line 383
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v6, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto/16 :goto_5

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/views/CustomDialog$Builder;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    .line 75
    iput-object p3, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->q:Ljava/lang/String;

    .line 76
    iput-boolean p4, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->r:Z

    .line 77
    return-void
.end method

.method private static a(Landroid/widget/Button;Landroid/view/View;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 401
    invoke-virtual {p0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 402
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 403
    const/high16 v1, 0x3f00

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 404
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    const v0, 0x7f070034

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 406
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 407
    const v0, 0x7f070038

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 408
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 409
    return-void
.end method

.method static synthetic b(Lgbis/gbandroid/views/CustomDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->j:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic c(Lgbis/gbandroid/views/CustomDialog$Builder;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lgbis/gbandroid/views/CustomDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->l:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic e(Lgbis/gbandroid/views/CustomDialog$Builder;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lgbis/gbandroid/views/CustomDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->k:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public create()Lgbis/gbandroid/views/CustomDialog;
    .locals 3

    .prologue
    .line 278
    new-instance v0, Lgbis/gbandroid/views/CustomDialog;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    const v2, 0x7f0a0012

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog;-><init>(Landroid/content/Context;I)V

    .line 279
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/app/Dialog;Z)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomDialog;

    return-object v0
.end method

.method public create(Z)Lgbis/gbandroid/views/CustomDialog;
    .locals 3
    .parameter

    .prologue
    .line 286
    if-eqz p1, :cond_0

    .line 287
    new-instance v0, Lgbis/gbandroid/views/CustomDialog;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    const v2, 0x7f0a0013

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/views/CustomDialog;-><init>(Landroid/content/Context;I)V

    .line 288
    invoke-direct {p0, v0, p1}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/app/Dialog;Z)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomDialog;

    .line 290
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    goto :goto_0
.end method

.method public createCloseOnTouchOutsideDialog()Lgbis/gbandroid/views/CustomCloseOnTouchDialog;
    .locals 3

    .prologue
    .line 297
    new-instance v0, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    const v2, 0x7f0a0012

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;-><init>(Landroid/content/Context;I)V

    .line 298
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->a(Landroid/app/Dialog;Z)Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/views/CustomCloseOnTouchDialog;

    return-object v0
.end method

.method public getNegativeButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->o:Landroid/widget/Button;

    return-object v0
.end method

.method public getNeutralButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->n:Landroid/widget/Button;

    return-object v0
.end method

.method public getPositiveButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->m:Landroid/widget/Button;

    return-object v0
.end method

.method protected setAnalyticsTrackEventScreenButton(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 396
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->p:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->q:Ljava/lang/String;

    const-string v2, "ScreenButton"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 398
    :cond_0
    return-void
.end method

.method public setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->i:Landroid/view/View;

    .line 158
    return-object p0
.end method

.method public setIcon(I)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->g:Landroid/graphics/drawable/Drawable;

    .line 106
    return-object p0
.end method

.method public setIcon(Landroid/graphics/Bitmap;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->h:Landroid/graphics/Bitmap;

    .line 126
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->g:Landroid/graphics/drawable/Drawable;

    .line 116
    return-object p0
.end method

.method public setMessage(I)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->c:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->c:Ljava/lang/String;

    .line 86
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->f:Ljava/lang/String;

    .line 217
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->k:Landroid/content/DialogInterface$OnClickListener;

    .line 218
    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 228
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->f:Ljava/lang/String;

    .line 229
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->k:Landroid/content/DialogInterface$OnClickListener;

    .line 230
    return-object p0
.end method

.method public setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->e:Ljava/lang/String;

    .line 193
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 194
    return-object p0
.end method

.method public setNeutralButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 204
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->e:Ljava/lang/String;

    .line 205
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 206
    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 168
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->d:Ljava/lang/String;

    .line 169
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 170
    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->d:Ljava/lang/String;

    .line 181
    iput-object p2, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 182
    return-object p0
.end method

.method public setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 239
    invoke-static {}, Lgbis/gbandroid/GBApplication;->getInstance()Lgbis/gbandroid/GBApplication;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lgbis/gbandroid/GBApplication;->getLogo(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 240
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setIcon(Landroid/graphics/Bitmap;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 244
    :goto_0
    return-object p0

    .line 243
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Lgbis/gbandroid/views/CustomDialog$Builder;

    goto :goto_0
.end method

.method public setTitle(I)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->b:Ljava/lang/String;

    .line 136
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 145
    iput-object p1, p0, Lgbis/gbandroid/views/CustomDialog$Builder;->b:Ljava/lang/String;

    .line 146
    return-object p0
.end method
