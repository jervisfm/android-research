.class public Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/views/CustomEditTextForPrices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CompositeOnFocusChangeListener"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/views/CustomEditTextForPrices;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View$OnFocusChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V
    .locals 1
    .parameter

    .prologue
    .line 181
    iput-object p1, p0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->a:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public clearListeners()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 190
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    return-void

    .line 194
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnFocusChangeListener;

    .line 195
    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public registerListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    return-void
.end method
