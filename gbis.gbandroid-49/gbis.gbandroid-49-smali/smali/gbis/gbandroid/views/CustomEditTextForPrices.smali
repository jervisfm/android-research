.class public Lgbis/gbandroid/views/CustomEditTextForPrices;
.super Landroid/widget/EditText;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;,
        Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;
    }
.end annotation


# static fields
.field public static final BACKGROUND_CLEAN:I = 0x0

.field public static final BACKGROUND_INVALID:I = 0x1

.field public static final BACKGROUND_VALID:I = 0x2

.field private static final g:Landroid/view/View$OnLongClickListener;


# instance fields
.field a:Landroid/text/TextWatcher;

.field private b:Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;

.field private c:Z

.field private d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

.field private e:Z

.field private f:Z

.field protected onTextChangedListener:Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lgbis/gbandroid/views/d;

    invoke-direct {v0}, Lgbis/gbandroid/views/d;-><init>()V

    sput-object v0, Lgbis/gbandroid/views/CustomEditTextForPrices;->g:Landroid/view/View$OnLongClickListener;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->e:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    .line 151
    new-instance v0, Lgbis/gbandroid/views/e;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/e;-><init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->a:Landroid/text/TextWatcher;

    .line 59
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->a()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->e:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    .line 151
    new-instance v0, Lgbis/gbandroid/views/e;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/e;-><init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->a:Landroid/text/TextWatcher;

    .line 54
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->a()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->e:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    .line 151
    new-instance v0, Lgbis/gbandroid/views/e;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/e;-><init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->a:Landroid/text/TextWatcher;

    .line 49
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->a()V

    .line 50
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder$Factory;

    invoke-direct {v0}, Lgbis/gbandroid/activities/report/pricekeyboard/base/SafeSpannableStringBuilder$Factory;-><init>()V

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setEditableFactory(Landroid/text/Editable$Factory;)V

    .line 115
    new-instance v0, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;-><init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->b:Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;

    .line 116
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->b:Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 117
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->a:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    sget-object v0, Lgbis/gbandroid/views/CustomEditTextForPrices;->g:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 121
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/views/CustomEditTextForPrices;)Z
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    return v0
.end method


# virtual methods
.method public enableTextWatcher()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    .line 69
    return-void
.end method

.method public getFuelType()Lgbis/gbandroid/activities/report/ReportPrices$FuelType;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    return-object v0
.end method

.method public haltTextWatcher()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->f:Z

    .line 65
    return-void
.end method

.method public isNineCents()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->c:Z

    return v0
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .parameter

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 90
    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->c:Z

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 92
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 93
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080027

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    const/high16 v2, 0x41a0

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 95
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 97
    const-string v0, "9"

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getMeasuredWidth()I

    move-result v2

    add-int/lit8 v2, v2, -0x19

    int-to-float v2, v2

    const/high16 v3, 0x41f0

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 99
    :cond_0
    return-void
.end method

.method public registerOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->b:Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;

    invoke-virtual {v0, p1}, Lgbis/gbandroid/views/CustomEditTextForPrices$CompositeOnFocusChangeListener;->registerListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 129
    return-void
.end method

.method public setBackgroundHint(I)V
    .locals 1
    .parameter

    .prologue
    .line 132
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    .line 135
    :cond_0
    return-void
.end method

.method public setBackgroundHintFromValidStatus(I)V
    .locals 2
    .parameter

    .prologue
    .line 138
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 140
    :pswitch_0
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0

    .line 145
    :pswitch_1
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setFuelType(Lgbis/gbandroid/activities/report/ReportPrices$FuelType;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->d:Lgbis/gbandroid/activities/report/ReportPrices$FuelType;

    .line 77
    return-void
.end method

.method public setNineCents(Z)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-boolean p1, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->c:Z

    .line 73
    return-void
.end method

.method public setOnTextChangedListener(Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->onTextChangedListener:Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;

    .line 125
    return-void
.end method

.method public usePriceKeyboard(Z)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-boolean p1, p0, Lgbis/gbandroid/views/CustomEditTextForPrices;->e:Z

    .line 108
    return-void
.end method
