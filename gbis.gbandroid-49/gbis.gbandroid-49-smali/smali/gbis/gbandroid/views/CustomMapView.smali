.class public Lgbis/gbandroid/views/CustomMapView;
.super Lcom/google/android/maps/MapView;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/views/CustomMapView$a;
    }
.end annotation


# instance fields
.field private a:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomMapView;->a()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomMapView;->a()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lgbis/gbandroid/views/CustomMapView;->a()V

    .line 16
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lgbis/gbandroid/views/CustomMapView$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lgbis/gbandroid/views/CustomMapView$a;-><init>(Lgbis/gbandroid/views/CustomMapView;B)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomMapView;->a:Landroid/view/GestureDetector;

    .line 28
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/views/CustomMapView;->a:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lgbis/gbandroid/views/CustomMapView;->a:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 33
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/maps/MapView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
