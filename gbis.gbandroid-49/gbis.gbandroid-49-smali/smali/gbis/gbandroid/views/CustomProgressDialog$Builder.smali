.class public Lgbis/gbandroid/views/CustomProgressDialog$Builder;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/views/CustomProgressDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->a:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public create()Lgbis/gbandroid/views/CustomProgressDialog;
    .locals 4

    .prologue
    .line 52
    new-instance v1, Lgbis/gbandroid/views/CustomProgressDialog;

    iget-object v0, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->a:Landroid/content/Context;

    const v2, 0x7f0a0014

    invoke-direct {v1, v0, v2}, Lgbis/gbandroid/views/CustomProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 53
    iget-object v0, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 54
    const v2, 0x7f030017

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 56
    iget-object v0, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    const v0, 0x7f07004f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :cond_0
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomProgressDialog;->setContentView(Landroid/view/View;)V

    .line 59
    return-object v1
.end method

.method public setMessage(I)Lgbis/gbandroid/views/CustomProgressDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->b:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomProgressDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lgbis/gbandroid/views/CustomProgressDialog$Builder;->b:Ljava/lang/String;

    .line 38
    return-object p0
.end method
