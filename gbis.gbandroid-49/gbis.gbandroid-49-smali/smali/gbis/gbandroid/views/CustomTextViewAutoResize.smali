.class public Lgbis/gbandroid/views/CustomTextViewAutoResize;
.super Landroid/widget/TextView;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;
    }
.end annotation


# static fields
.field public static final MIN_TEXT_SIZE:F = 20.0f


# instance fields
.field private a:Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;

.field private b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->b:Z

    .line 42
    iput v1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    .line 45
    const/high16 v0, 0x41a0

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    .line 48
    const/high16 v0, 0x3f80

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->f:F

    .line 51
    iput v1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->g:F

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->h:Z

    .line 69
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getTextSize()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    .line 70
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 288
    invoke-virtual {p2, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 290
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->f:F

    iget v6, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->g:F

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 291
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    return v0
.end method


# virtual methods
.method public getAddEllipsis()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->h:Z

    return v0
.end method

.method public getMaxTextSize()F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    return v0
.end method

.method public getMinTextSize()F
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 195
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->b:Z

    if-eqz v0, :cond_1

    .line 196
    :cond_0
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getCompoundPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getCompoundPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 197
    sub-int v1, p5, p3

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getCompoundPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getCompoundPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 198
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->resizeText(II)V

    .line 200
    :cond_1
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    .line 201
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 88
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->b:Z

    .line 90
    :cond_1
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->b:Z

    .line 79
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->resetTextSize()V

    .line 80
    return-void
.end method

.method public resetTextSize()V
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    iget v1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    invoke-super {p0, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 186
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    .line 188
    :cond_0
    return-void
.end method

.method public resizeText()V
    .locals 3

    .prologue
    .line 208
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 209
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 210
    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->resizeText(II)V

    .line 211
    return-void
.end method

.method public resizeText(II)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 219
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 221
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    if-lez p1, :cond_0

    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 229
    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v9

    .line 231
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    iget v3, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 234
    :goto_1
    invoke-direct {p0, v1, v2, p1, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v3

    move v8, v0

    move v0, v3

    .line 237
    :goto_2
    if-le v0, p2, :cond_2

    iget v3, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    cmpl-float v3, v8, v3

    if-gtz v3, :cond_6

    .line 243
    :cond_2
    iget-boolean v3, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->h:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    cmpl-float v3, v8, v3

    if-nez v3, :cond_3

    if-le v0, p2, :cond_3

    .line 245
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->f:F

    iget v6, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->g:F

    move v3, p1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 247
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 250
    invoke-virtual {v0, p2}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v3

    add-int/lit8 v4, v3, -0x1

    .line 252
    if-gez v4, :cond_7

    .line 253
    const-string v0, ""

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->setText(Ljava/lang/CharSequence;)V

    .line 273
    :cond_3
    :goto_3
    invoke-virtual {v2, v8}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 274
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->g:F

    iget v1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->f:F

    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->setLineSpacing(FF)V

    .line 277
    iget-object v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->a:Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->a:Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;

    invoke-interface {v0, p0, v9, v8}, Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;->onTextResize(Landroid/widget/TextView;FF)V

    .line 282
    :cond_4
    iput-boolean v7, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->b:Z

    goto :goto_0

    .line 231
    :cond_5
    iget v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    goto :goto_1

    .line 238
    :cond_6
    const/high16 v0, 0x4000

    sub-float v0, v8, v0

    iget v3, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 239
    invoke-direct {p0, v1, v2, p1, v3}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v0

    move v8, v3

    goto :goto_2

    .line 257
    :cond_7
    invoke-virtual {v0, v4}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v5

    .line 258
    invoke-virtual {v0, v4}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v3

    .line 259
    invoke-virtual {v0, v4}, Landroid/text/StaticLayout;->getLineWidth(I)F

    move-result v0

    .line 260
    const-string v4, "..."

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    move v10, v3

    move v3, v0

    move v0, v10

    .line 263
    :goto_4
    int-to-float v6, p1

    add-float/2addr v3, v4

    cmpg-float v3, v6, v3

    if-ltz v3, :cond_8

    .line 266
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v7, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 264
    :cond_8
    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v5, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    goto :goto_4
.end method

.method public setAddEllipsis(Z)V
    .locals 0
    .parameter

    .prologue
    .line 169
    iput-boolean p1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->h:Z

    .line 170
    return-void
.end method

.method public setLineSpacing(FF)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 124
    iput p2, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->f:F

    .line 125
    iput p1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->g:F

    .line 126
    return-void
.end method

.method public setMaxTextSize(F)V
    .locals 0
    .parameter

    .prologue
    .line 133
    iput p1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->d:F

    .line 134
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->requestLayout()V

    .line 135
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->invalidate()V

    .line 136
    return-void
.end method

.method public setMinTextSize(F)V
    .locals 0
    .parameter

    .prologue
    .line 151
    iput p1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->e:F

    .line 152
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->requestLayout()V

    .line 153
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->invalidate()V

    .line 154
    return-void
.end method

.method public setOnResizeListener(Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->a:Lgbis/gbandroid/views/CustomTextViewAutoResize$OnTextResizeListener;

    .line 98
    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .parameter

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 106
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getTextSize()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    .line 107
    return-void
.end method

.method public setTextSize(IF)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 114
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 115
    invoke-virtual {p0}, Lgbis/gbandroid/views/CustomTextViewAutoResize;->getTextSize()F

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/CustomTextViewAutoResize;->c:F

    .line 116
    return-void
.end method
