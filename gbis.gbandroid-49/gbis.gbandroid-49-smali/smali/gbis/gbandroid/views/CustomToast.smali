.class public Lgbis/gbandroid/views/CustomToast;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private a:Landroid/widget/Toast;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 20
    const v2, 0x7f030022

    const v0, 0x7f070092

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 21
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 23
    const v0, 0x7f070093

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 24
    iput-object p2, p0, Lgbis/gbandroid/views/CustomToast;->b:Ljava/lang/String;

    .line 25
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    add-int/lit8 v2, v2, -0x23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 28
    new-instance v0, Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    .line 29
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    const/16 v2, 0x31

    const/4 v3, 0x0

    const/16 v4, 0x14

    invoke-virtual {v0, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 30
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 31
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p3}, Landroid/widget/Toast;->setDuration(I)V

    .line 32
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 48
    return-void
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->b:Ljava/lang/String;

    return-object v0
.end method

.method public setDuration(I)V
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setDuration(I)V

    .line 44
    return-void
.end method

.method public setGravity(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/Toast;->setGravity(III)V

    .line 40
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToast;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 36
    return-void
.end method
