.class public Lgbis/gbandroid/views/CustomToastAward;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private a:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lgbis/gbandroid/entities/AwardsMessage;I)V
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 30
    const v2, 0x7f030009

    const v0, 0x7f070028

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 31
    const v0, 0x7f070029

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 32
    const v2, 0x7f07001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 33
    const v2, 0x7f07001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 34
    new-instance v4, Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    .line 35
    iget-object v4, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    const/16 v5, 0x31

    const/16 v6, 0x14

    invoke-virtual {v4, v5, v7, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 36
    iget-object v4, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    invoke-virtual {v4, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 37
    iget-object v1, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    invoke-virtual {v1, p3}, Landroid/widget/Toast;->setDuration(I)V

    .line 38
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v4, Lgbis/gbandroid/views/f;

    invoke-direct {v4, p0, p2, v2}, Lgbis/gbandroid/views/f;-><init>(Lgbis/gbandroid/views/CustomToastAward;Lgbis/gbandroid/entities/AwardsMessage;Landroid/view/View;)V

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 47
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 48
    invoke-virtual {p2}, Lgbis/gbandroid/entities/AwardsMessage;->getCompletedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 51
    const-string v2, "onsmall.png"

    .line 58
    new-instance v0, Lgbis/gbandroid/utils/ImageLoader;

    const v4, 0x7f020064

    const-string v5, "/Android/data/gbis.gbandroid/cache/Awards"

    invoke-direct {v0, p1, v4, v5}, Lgbis/gbandroid/utils/ImageLoader;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 59
    new-instance v4, Ljava/lang/StringBuilder;

    const v5, 0x7f09020d

    invoke-virtual {p1, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-static {v1}, Lgbis/gbandroid/utils/ImageUtils;->getResolutionInText(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lgbis/gbandroid/entities/AwardsMessage;->getAwardId()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 62
    :try_start_0
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 63
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lgbis/gbandroid/utils/ImageLoader;->displayImage(Ljava/lang/String;Landroid/app/Activity;Landroid/widget/ImageView;ZI)V

    .line 69
    :goto_0
    return-void

    .line 65
    :cond_0
    const v0, 0x7f020065

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public setGravity(III)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/Toast;->setGravity(III)V

    .line 77
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lgbis/gbandroid/views/CustomToastAward;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 73
    return-void
.end method
