.class public Lgbis/gbandroid/views/CustomTooltipDialog$Builder;
.super Ljava/lang/Object;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/views/CustomTooltipDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->a:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public create()Lgbis/gbandroid/views/CustomTooltipDialog;
    .locals 4

    .prologue
    .line 54
    new-instance v1, Lgbis/gbandroid/views/CustomTooltipDialog;

    iget-object v0, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->a:Landroid/content/Context;

    const v2, 0x7f0a0015

    invoke-direct {v1, v0, v2}, Lgbis/gbandroid/views/CustomTooltipDialog;-><init>(Landroid/content/Context;I)V

    .line 55
    iget-object v0, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 56
    const v2, 0x7f030023

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 58
    iget-object v0, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 59
    const v0, 0x7f070095

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    :cond_0
    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomTooltipDialog;->setContentView(Landroid/view/View;)V

    .line 61
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomTooltipDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 62
    const v2, 0x10a0001

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 63
    const/high16 v2, 0x3e80

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 64
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomTooltipDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/high16 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 65
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomTooltipDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 66
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomTooltipDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 67
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomTooltipDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 68
    return-object v1
.end method

.method public setMessage(I)Lgbis/gbandroid/views/CustomTooltipDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->b:Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lgbis/gbandroid/views/CustomTooltipDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lgbis/gbandroid/views/CustomTooltipDialog$Builder;->b:Ljava/lang/String;

    .line 40
    return-object p0
.end method
