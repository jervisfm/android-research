.class public Lgbis/gbandroid/views/FilterButton;
.super Landroid/widget/RelativeLayout;
.source "GBFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030028

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/views/FilterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/views/FilterButton;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f0700b6

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/FilterButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lgbis/gbandroid/views/FilterButton;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 33
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030028

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 34
    sget-object v0, Lgbis/gbandroid/R$styleable;->FilterButton:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/views/FilterButton;->a:Ljava/lang/String;

    .line 36
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/views/FilterButton;->b:Landroid/graphics/drawable/Drawable;

    .line 37
    invoke-direct {p0}, Lgbis/gbandroid/views/FilterButton;->a()V

    .line 38
    invoke-direct {p0}, Lgbis/gbandroid/views/FilterButton;->b()V

    .line 39
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 40
    invoke-virtual {p0, v2}, Lgbis/gbandroid/views/FilterButton;->setClickable(Z)V

    .line 41
    invoke-virtual {p0, v2}, Lgbis/gbandroid/views/FilterButton;->setFocusable(Z)V

    .line 42
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lgbis/gbandroid/views/FilterButton;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 50
    const v0, 0x7f0700b5

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/FilterButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/views/FilterButton;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgbis/gbandroid/views/FilterButton;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lgbis/gbandroid/views/FilterButton;->a:Ljava/lang/String;

    return-object v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lgbis/gbandroid/views/FilterButton;->b:Landroid/graphics/drawable/Drawable;

    .line 64
    invoke-direct {p0}, Lgbis/gbandroid/views/FilterButton;->b()V

    .line 65
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lgbis/gbandroid/views/FilterButton;->a:Ljava/lang/String;

    .line 55
    invoke-direct {p0}, Lgbis/gbandroid/views/FilterButton;->a()V

    .line 56
    return-void
.end method
