.class public Lgbis/gbandroid/views/MainButton;
.super Ljava/lang/Object;
.source "GBFile"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/views/MainButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;Z)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-direct/range {p0 .. p5}, Lgbis/gbandroid/views/MainButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;Z)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;IZ)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lgbis/gbandroid/views/MainButton;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;Z)V

    .line 18
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lgbis/gbandroid/views/MainButton;->a:Landroid/graphics/drawable/Drawable;

    .line 26
    iput-object p2, p0, Lgbis/gbandroid/views/MainButton;->b:Ljava/lang/String;

    .line 27
    iput p3, p0, Lgbis/gbandroid/views/MainButton;->c:I

    .line 28
    iput-object p4, p0, Lgbis/gbandroid/views/MainButton;->d:Landroid/graphics/drawable/Drawable;

    .line 29
    iput-boolean p5, p0, Lgbis/gbandroid/views/MainButton;->e:Z

    .line 30
    return-void
.end method


# virtual methods
.method public getBadge()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgbis/gbandroid/views/MainButton;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getButtonId()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lgbis/gbandroid/views/MainButton;->c:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lgbis/gbandroid/views/MainButton;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lgbis/gbandroid/views/MainButton;->b:Ljava/lang/String;

    return-object v0
.end method

.method public isBadgeDisplayed()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lgbis/gbandroid/views/MainButton;->e:Z

    return v0
.end method

.method public setBadge(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lgbis/gbandroid/views/MainButton;->d:Landroid/graphics/drawable/Drawable;

    .line 62
    return-void
.end method

.method public setBadgeDisplayed(Z)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, Lgbis/gbandroid/views/MainButton;->e:Z

    .line 70
    return-void
.end method

.method public setButtonId(I)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput p1, p0, Lgbis/gbandroid/views/MainButton;->c:I

    .line 50
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lgbis/gbandroid/views/MainButton;->a:Landroid/graphics/drawable/Drawable;

    .line 34
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    iput-object p1, p0, Lgbis/gbandroid/views/MainButton;->b:Ljava/lang/String;

    .line 42
    return-void
.end method
