.class public Lgbis/gbandroid/views/MapAdOverlay;
.super Lgbis/gbandroid/views/MyItemizedOverlay;
.source "GBFile"


# instance fields
.field private a:Lgbis/gbandroid/activities/map/MapStationsBase;

.field private b:Lgbis/gbandroid/entities/ChoiceHotels;

.field private c:Landroid/app/Dialog;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-static {p1}, Lgbis/gbandroid/views/MapAdOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lgbis/gbandroid/views/MyItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lgbis/gbandroid/views/MapAdOverlay;->a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;ZLcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-static {p1}, Lgbis/gbandroid/views/MapAdOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p5}, Lgbis/gbandroid/views/MyItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/views/MapAdOverlay;->a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V

    .line 45
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/views/MapAdOverlay;)Lgbis/gbandroid/entities/ChoiceHotels;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    return-object v0
.end method

.method private a(DD)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const-wide v6, 0x412e848000000000L

    .line 135
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    check-cast v0, Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    .line 136
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    .line 138
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://maps.google.com/maps?saddr="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    int-to-double v4, v0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 139
    const-string v3, "&daddr="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 137
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 140
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->startActivity(Landroid/content/Intent;)V

    .line 143
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    check-cast v0, Lgbis/gbandroid/activities/map/MapStations;

    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStations;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ChoiceHotels;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lgbis/gbandroid/views/MapAdOverlay;->d:Landroid/graphics/drawable/Drawable;

    .line 49
    iput-object p3, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    .line 50
    iput-object p2, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    .line 51
    iput-boolean p4, p0, Lgbis/gbandroid/views/MapAdOverlay;->e:Z

    .line 52
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 108
    const-string v0, ""

    .line 109
    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ChoiceHotels;->getAddress2()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 110
    const-string v0, ". "

    move-object v1, v0

    .line 111
    :goto_0
    const v0, 0x7f070113

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ChoiceHotels;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getAddress2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    const v0, 0x7f070114

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ChoiceHotels;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ChoiceHotels;->getState()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ChoiceHotels;->getState()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ChoiceHotels;->getState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    const v0, 0x7f070116

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 116
    const v1, 0x7f070115

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 117
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<a href=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ChoiceHotels;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ChoiceHotels;->getClickText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</a>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ChoiceHotels;->getPhone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    new-instance v0, Lgbis/gbandroid/views/h;

    invoke-direct {v0, p0}, Lgbis/gbandroid/views/h;-><init>(Lgbis/gbandroid/views/MapAdOverlay;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    return-void

    .line 113
    :cond_0
    const-string v1, ""

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto/16 :goto_0
.end method

.method static synthetic a(Lgbis/gbandroid/views/MapAdOverlay;DD)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/views/MapAdOverlay;->a(DD)V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/views/MapAdOverlay;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/MapAdOverlay;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 148
    iget-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v1, v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->startActivity(Landroid/content/Intent;)V

    .line 149
    return-void
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-virtual {p0}, Lgbis/gbandroid/views/MapAdOverlay;->populate()V

    .line 132
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .parameter

    .prologue
    .line 153
    instance-of v0, p1, Lgbis/gbandroid/views/MapAdOverlay;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ChoiceHotels;->getBrandId()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lgbis/gbandroid/views/MapAdOverlay;

    iget-object v1, p1, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ChoiceHotels;->getBrandId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x1

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultMarker()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ChoiceHotels;->getBrandId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method protected onTap(I)Z
    .locals 6
    .parameter

    .prologue
    .line 60
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    .line 63
    :cond_0
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->c:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 65
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 66
    const v1, 0x7f03004b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 67
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    iget-object v3, p0, Lgbis/gbandroid/views/MapAdOverlay;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v4, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v5, 0x7f090252

    invoke-virtual {v4, v5}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 69
    iget-object v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 70
    iget-object v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->b:Lgbis/gbandroid/entities/ChoiceHotels;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ChoiceHotels;->getAdId()I

    move-result v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Ads"

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 71
    iget-object v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v3, 0x7f090189

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgbis/gbandroid/views/g;

    invoke-direct {v3, p0}, Lgbis/gbandroid/views/g;-><init>(Lgbis/gbandroid/views/MapAdOverlay;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 76
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->createCloseOnTouchOutsideDialog()Lgbis/gbandroid/views/CustomCloseOnTouchDialog;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/views/MapAdOverlay;->c:Landroid/app/Dialog;

    .line 77
    invoke-direct {p0, v0}, Lgbis/gbandroid/views/MapAdOverlay;->a(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 80
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 85
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/views/MyItemizedOverlay;->onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 94
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    .line 104
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/views/MyItemizedOverlay;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z

    move-result v0

    return v0

    .line 99
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lgbis/gbandroid/utils/FeaturesUtils;->getMotionEventPointCount(Landroid/view/MotionEvent;)I

    move-result v0

    if-le v0, v2, :cond_0

    .line 101
    iput-boolean v2, p0, Lgbis/gbandroid/views/MapAdOverlay;->f:Z

    goto :goto_0
.end method
