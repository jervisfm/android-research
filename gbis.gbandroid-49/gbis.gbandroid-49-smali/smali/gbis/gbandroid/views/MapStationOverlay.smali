.class public Lgbis/gbandroid/views/MapStationOverlay;
.super Lgbis/gbandroid/views/MyItemizedOverlay;
.source "GBFile"


# instance fields
.field private a:Lgbis/gbandroid/activities/map/MapStationsBase;

.field private b:Landroid/content/SharedPreferences;

.field private c:Lgbis/gbandroid/entities/ListMessage;

.field private d:Ljava/lang/String;

.field private e:Landroid/app/Dialog;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-static {p1}, Lgbis/gbandroid/views/MapStationOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lgbis/gbandroid/views/MyItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lgbis/gbandroid/views/MapStationOverlay;->a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;ZLcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-static {p1}, Lgbis/gbandroid/views/MapStationOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0, p5}, Lgbis/gbandroid/views/MyItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    .line 55
    invoke-direct {p0, p1, p2, p3, p4}, Lgbis/gbandroid/views/MapStationOverlay;->a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V

    .line 56
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->b:Landroid/content/SharedPreferences;

    const-string v1, "fuelPreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    .line 159
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Lgbis/gbandroid/entities/ListMessage;Lgbis/gbandroid/activities/map/MapStationsBase;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lgbis/gbandroid/views/MapStationOverlay;->f:Landroid/graphics/drawable/Drawable;

    .line 60
    iput-object p3, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    .line 61
    iput-object p2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    .line 62
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->b:Landroid/content/SharedPreferences;

    .line 63
    invoke-direct {p0}, Lgbis/gbandroid/views/MapStationOverlay;->a()V

    .line 64
    iput-boolean p4, p0, Lgbis/gbandroid/views/MapStationOverlay;->g:Z

    .line 65
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f07011a

    const v7, 0x7f070119

    const v6, 0x7f070117

    const v4, 0x7f070118

    const/16 v5, 0x8

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 127
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lgbis/gbandroid/views/MapStationOverlay;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/utils/DateUtils;->toDateFormat(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 136
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Updated: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListMessage;->getTimeOffset()I

    move-result v3

    invoke-static {v1, v3}, Lgbis/gbandroid/utils/DateUtils;->compareDateToNow(Ljava/util/Date;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->isNotIntersection()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    const-string v0, "near"

    move-object v1, v0

    .line 147
    :goto_2
    const v0, 0x7f070113

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListMessage;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getCrossStreet()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    const v0, 0x7f070114

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v3}, Lgbis/gbandroid/entities/ListMessage;->getState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    return-void

    .line 132
    :cond_0
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 138
    :cond_1
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 146
    :cond_2
    const-string v0, "&"

    move-object v1, v0

    goto/16 :goto_2

    .line 149
    :cond_3
    const-string v1, ""

    goto :goto_3
.end method

.method static synthetic a(Lgbis/gbandroid/views/MapStationOverlay;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    invoke-direct {p0}, Lgbis/gbandroid/views/MapStationOverlay;->c()V

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901c9

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901cd

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    .line 165
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901ca

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901ce

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901cb

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901cf

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 171
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901cc

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v2, 0x7f0901d0

    invoke-virtual {v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 174
    :cond_3
    const-string v0, ""

    goto/16 :goto_0
.end method

.method static synthetic b(Lgbis/gbandroid/views/MapStationOverlay;)V
    .locals 0
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lgbis/gbandroid/views/MapStationOverlay;->d()V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 178
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const-class v2, Lgbis/gbandroid/activities/station/StationDetails;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    const-string v0, "station"

    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 180
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    check-cast v0, Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->unregisterPreferencesListener()V

    .line 181
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/activities/map/MapStationsBase;->startActivityForResult(Landroid/content/Intent;I)V

    .line 182
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 183
    return-void
.end method

.method private d()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const-wide v4, 0x412e848000000000L

    const-wide/16 v1, 0x0

    const v7, 0x7f060001

    .line 187
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    check-cast v0, Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->getMyLocation()Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    .line 188
    if-nez v0, :cond_1

    move-wide v9, v1

    move-wide v11, v1

    move-wide v2, v11

    move-wide v0, v9

    .line 195
    :goto_0
    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const-class v6, Lgbis/gbandroid/activities/report/ReportPrices;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 196
    const-string v5, "station"

    iget-object v6, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 197
    const-string v5, "my latitude"

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 198
    const-string v2, "my longitude"

    invoke-virtual {v4, v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 199
    const-string v0, "time offset"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getTimeOffset()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    const-string v0, "fuel regular"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 202
    const-string v0, "time spotted regular"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    :cond_0
    :goto_1
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    check-cast v0, Lgbis/gbandroid/activities/map/MapStations;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStations;->unregisterPreferencesListener()V

    .line 217
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v0, v4, v8}, Lgbis/gbandroid/activities/map/MapStationsBase;->startActivityForResult(Landroid/content/Intent;I)V

    .line 218
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 219
    return-void

    .line 192
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    int-to-double v1, v1

    div-double v2, v1, v4

    .line 193
    invoke-virtual {v0}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    goto :goto_0

    .line 204
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    const-string v0, "fuel midgrade"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 206
    const-string v0, "time spotted midgrade"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 208
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 209
    const-string v0, "fuel premium"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 210
    const-string v0, "time spotted premium"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 212
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "fuel diesel"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getPrice()D

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 214
    const-string v0, "time spotted diesel"

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getTimeSpotted()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-virtual {p0}, Lgbis/gbandroid/views/MapStationOverlay;->populate()V

    .line 155
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .parameter

    .prologue
    .line 223
    instance-of v0, p1, Lgbis/gbandroid/views/MapStationOverlay;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    check-cast p1, Lgbis/gbandroid/views/MapStationOverlay;

    iget-object v1, p1, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 225
    const/4 v0, 0x1

    .line 226
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultMarker()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->f:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListMessage;->getStationId()I

    move-result v0

    return v0
.end method

.method protected onTap(I)Z
    .locals 6
    .parameter

    .prologue
    .line 73
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    .line 76
    :cond_0
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 77
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    iget-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->showPinFront(Lgbis/gbandroid/entities/ListMessage;)V

    .line 78
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lgbis/gbandroid/activities/map/MapStationsBase;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 79
    const v1, 0x7f03004c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 80
    new-instance v1, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    iget-object v3, p0, Lgbis/gbandroid/views/MapStationOverlay;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    iget-object v4, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v5, 0x7f090252

    invoke-virtual {v4, v5}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 82
    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 83
    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->c:Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getGasBrandId()I

    move-result v2

    const-string v3, "/Android/data/gbis.gbandroid/cache/Brands"

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setStationLogo(ILjava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 84
    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v3, 0x7f09017b

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgbis/gbandroid/views/i;

    invoke-direct {v3, p0}, Lgbis/gbandroid/views/i;-><init>(Lgbis/gbandroid/views/MapStationOverlay;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 89
    iget-object v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->a:Lgbis/gbandroid/activities/map/MapStationsBase;

    const v3, 0x7f09017c

    invoke-virtual {v2, v3}, Lgbis/gbandroid/activities/map/MapStationsBase;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgbis/gbandroid/views/j;

    invoke-direct {v3, p0}, Lgbis/gbandroid/views/j;-><init>(Lgbis/gbandroid/views/MapStationOverlay;)V

    invoke-virtual {v1, v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 94
    invoke-virtual {v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->createCloseOnTouchOutsideDialog()Lgbis/gbandroid/views/CustomCloseOnTouchDialog;

    move-result-object v1

    iput-object v1, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    .line 95
    invoke-direct {p0, v0}, Lgbis/gbandroid/views/MapStationOverlay;->a(Landroid/view/View;)V

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 98
    :cond_2
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 103
    iget-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    if-eqz v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 106
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/views/MyItemizedOverlay;->onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 112
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    .line 113
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    .line 122
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/views/MyItemizedOverlay;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z

    move-result v0

    return v0

    .line 117
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lgbis/gbandroid/utils/FeaturesUtils;->getMotionEventPointCount(Landroid/view/MotionEvent;)I

    move-result v0

    if-le v0, v2, :cond_0

    .line 119
    iput-boolean v2, p0, Lgbis/gbandroid/views/MapStationOverlay;->h:Z

    goto :goto_0
.end method
