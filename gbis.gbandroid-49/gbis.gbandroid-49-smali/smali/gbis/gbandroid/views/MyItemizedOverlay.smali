.class public Lgbis/gbandroid/views/MyItemizedOverlay;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/maps/ItemizedOverlay",
        "<",
        "Lcom/google/android/maps/OverlayItem;",
        ">;"
    }
.end annotation


# instance fields
.field protected mOverlays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/maps/OverlayItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-static {p1}, Lgbis/gbandroid/views/MyItemizedOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/MyItemizedOverlay;->mOverlays:Ljava/util/ArrayList;

    .line 18
    iput-object p2, p0, Lgbis/gbandroid/views/MyItemizedOverlay;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    .line 19
    return-void
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lgbis/gbandroid/views/MyItemizedOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    invoke-virtual {p0}, Lgbis/gbandroid/views/MyItemizedOverlay;->populate()V

    .line 23
    return-void
.end method

.method protected createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lgbis/gbandroid/views/MyItemizedOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-super {p0, p1, p2, v0}, Lcom/google/android/maps/ItemizedOverlay;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V

    .line 35
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lgbis/gbandroid/views/MyItemizedOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
