.class public Lgbis/gbandroid/views/StationsMapView;
.super Lgbis/gbandroid/views/CustomMapView;
.source "GBFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Lgbis/gbandroid/activities/map/MapStationsBase;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/views/CustomMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/views/CustomMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lgbis/gbandroid/views/CustomMapView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 24
    invoke-super {p0, p1}, Lgbis/gbandroid/views/CustomMapView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 25
    iget v0, p0, Lgbis/gbandroid/views/StationsMapView;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, Lgbis/gbandroid/views/StationsMapView;->b:I

    if-eqz v0, :cond_1

    .line 26
    invoke-virtual {p0}, Lgbis/gbandroid/views/StationsMapView;->getLatitudeSpan()I

    move-result v0

    iget v1, p0, Lgbis/gbandroid/views/StationsMapView;->a:I

    div-int/2addr v0, v1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lgbis/gbandroid/views/StationsMapView;->getLongitudeSpan()I

    move-result v0

    iget v1, p0, Lgbis/gbandroid/views/StationsMapView;->b:I

    div-int/2addr v0, v1

    if-eq v0, v2, :cond_2

    .line 27
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/StationsMapView;->c:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->loadMapPins()V

    .line 28
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/views/StationsMapView;->getLatitudeSpan()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/StationsMapView;->a:I

    .line 33
    invoke-virtual {p0}, Lgbis/gbandroid/views/StationsMapView;->getLongitudeSpan()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/StationsMapView;->b:I

    .line 35
    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/views/StationsMapView;->c:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->getMapDistance()D

    move-result-wide v0

    const-wide/high16 v2, 0x4079

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 39
    iget-object v0, p0, Lgbis/gbandroid/views/StationsMapView;->c:Lgbis/gbandroid/activities/map/MapStationsBase;

    invoke-virtual {v0}, Lgbis/gbandroid/activities/map/MapStationsBase;->loadMapPins()V

    .line 41
    :cond_0
    invoke-super {p0, p1}, Lgbis/gbandroid/views/CustomMapView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setControl(Lgbis/gbandroid/activities/map/MapStationsBase;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lgbis/gbandroid/views/StationsMapView;->c:Lgbis/gbandroid/activities/map/MapStationsBase;

    .line 46
    return-void
.end method
