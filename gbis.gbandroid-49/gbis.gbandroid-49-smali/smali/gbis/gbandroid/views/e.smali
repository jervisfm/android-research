.class final Lgbis/gbandroid/views/e;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field a:Z

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field final synthetic d:Lgbis/gbandroid/views/CustomEditTextForPrices;


# direct methods
.method constructor <init>(Lgbis/gbandroid/views/CustomEditTextForPrices;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/e;->a:Z

    .line 153
    iput-object v1, p0, Lgbis/gbandroid/views/e;->b:Ljava/lang/String;

    .line 154
    iput-object v1, p0, Lgbis/gbandroid/views/e;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-static {v0}, Lgbis/gbandroid/views/CustomEditTextForPrices;->a(Lgbis/gbandroid/views/CustomEditTextForPrices;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v0, v0, Lgbis/gbandroid/views/CustomEditTextForPrices;->onTextChangedListener:Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-object v0, v0, Lgbis/gbandroid/views/CustomEditTextForPrices;->onTextChangedListener:Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;

    .line 162
    iget-object v1, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    iget-boolean v2, p0, Lgbis/gbandroid/views/e;->a:Z

    iget-object v3, p0, Lgbis/gbandroid/views/e;->c:Ljava/lang/String;

    iget-object v4, p0, Lgbis/gbandroid/views/e;->b:Ljava/lang/String;

    iget-object v5, p0, Lgbis/gbandroid/views/e;->d:Lgbis/gbandroid/views/CustomEditTextForPrices;

    invoke-virtual {v5}, Lgbis/gbandroid/views/CustomEditTextForPrices;->getSelectionStart()I

    move-result v5

    .line 161
    invoke-interface/range {v0 .. v5}, Lgbis/gbandroid/views/CustomEditTextForPrices$OnTextChangedListener;->onTextChanged(Lgbis/gbandroid/views/CustomEditTextForPrices;ZLjava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    if-lez p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgbis/gbandroid/views/e;->a:Z

    .line 169
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/e;->b:Ljava/lang/String;

    .line 170
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/e;->c:Ljava/lang/String;

    .line 175
    return-void
.end method
