.class public Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "GBFile"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 27
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 28
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->getChildCount()I

    move-result v2

    .line 29
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 35
    return-void

    .line 30
    :cond_0
    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 31
    instance-of v3, v0, Landroid/widget/CheckBox;

    if-eqz v3, :cond_1

    .line 32
    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    .line 29
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 46
    :cond_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CheckableRelativeLayout;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 52
    :cond_0
    return-void
.end method
