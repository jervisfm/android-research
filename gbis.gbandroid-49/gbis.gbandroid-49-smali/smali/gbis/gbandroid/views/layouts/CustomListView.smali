.class public Lgbis/gbandroid/views/layouts/CustomListView;
.super Landroid/widget/ListView;
.source "GBFile"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/views/layouts/CustomListView$a;,
        Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;
    }
.end annotation


# instance fields
.field private a:Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;

.field private b:Landroid/widget/AbsListView$OnScrollListener;

.field private c:Landroid/view/LayoutInflater;

.field private d:Landroid/widget/LinearLayout;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ProgressBar;

.field private h:I

.field private i:I

.field private j:Landroid/view/animation/RotateAnimation;

.field private k:Landroid/view/animation/RotateAnimation;

.field private l:I

.field private m:I

.field private n:I

.field private o:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/layouts/CustomListView;->a(Landroid/content/Context;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/layouts/CustomListView;->a(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/layouts/CustomListView;->a(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/views/layouts/CustomListView;)I
    .locals 1
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    return v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 245
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    .line 246
    iget-object v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    .line 247
    iget v2, p0, Lgbis/gbandroid/views/layouts/CustomListView;->m:I

    .line 248
    iget-object v3, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v3

    .line 249
    iget-object v4, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v4

    .line 245
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 250
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 13
    .parameter

    .prologue
    const/4 v12, 0x0

    const/4 v1, 0x0

    const/high16 v2, -0x3ccc

    const/high16 v4, 0x3f00

    const/4 v3, 0x1

    .line 78
    new-instance v0, Landroid/view/animation/RotateAnimation;

    move v5, v3

    move v6, v4

    .line 80
    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 78
    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->j:Landroid/view/animation/RotateAnimation;

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->j:Landroid/view/animation/RotateAnimation;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 82
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->j:Landroid/view/animation/RotateAnimation;

    const-wide/16 v5, 0xfa

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 83
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->j:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 84
    new-instance v5, Landroid/view/animation/RotateAnimation;

    move v6, v2

    move v7, v1

    move v8, v3

    move v9, v4

    move v10, v3

    move v11, v4

    .line 86
    invoke-direct/range {v5 .. v11}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 84
    iput-object v5, p0, Lgbis/gbandroid/views/layouts/CustomListView;->k:Landroid/view/animation/RotateAnimation;

    .line 87
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->k:Landroid/view/animation/RotateAnimation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 88
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->k:Landroid/view/animation/RotateAnimation;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 89
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->k:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 91
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->c:Landroid/view/LayoutInflater;

    .line 93
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f03003a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    .line 95
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f0700d1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->e:Landroid/widget/TextView;

    .line 96
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f0700d2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    .line 97
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f0700d0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->g:Landroid/widget/ProgressBar;

    .line 99
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 100
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->m:I

    .line 102
    iput v3, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    .line 104
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v12}, Lgbis/gbandroid/views/layouts/CustomListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 105
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lgbis/gbandroid/views/layouts/CustomListView$a;

    invoke-direct {v1, p0, v12}, Lgbis/gbandroid/views/layouts/CustomListView$a;-><init>(Lgbis/gbandroid/views/layouts/CustomListView;B)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-super {p0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 109
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lgbis/gbandroid/views/layouts/CustomListView;->a(Landroid/view/View;)V

    .line 110
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    .line 112
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->o:F

    .line 113
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 10
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    .line 190
    :try_start_0
    const-class v0, Landroid/view/MotionEvent;

    const-string v3, "getPointerCount"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 191
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    move v1, v0

    :goto_0
    move v4, v2

    .line 202
    :goto_1
    if-lt v4, v5, :cond_0

    .line 239
    return-void

    .line 194
    :catch_0
    move-exception v0

    throw v0

    .line 196
    :catch_1
    move-exception v0

    .line 197
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "unexpected "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :catch_2
    move-exception v0

    .line 199
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "unexpected "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move v3, v2

    .line 203
    :goto_2
    if-lt v3, v1, :cond_1

    .line 202
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 204
    :cond_1
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    const/4 v6, 0x3

    if-ne v0, v6, :cond_3

    .line 205
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->isVerticalFadingEdgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {p0, v2}, Lgbis/gbandroid/views/layouts/CustomListView;->setVerticalScrollBarEnabled(Z)V

    .line 212
    :cond_2
    :try_start_1
    const-class v0, Landroid/view/MotionEvent;

    .line 213
    const-string v6, "getHistoricalY"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    .line 212
    invoke-virtual {v0, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 214
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_6

    move-result v0

    .line 228
    :goto_3
    iget v6, p0, Lgbis/gbandroid/views/layouts/CustomListView;->n:I

    sub-int/2addr v0, v6

    .line 229
    iget v6, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    .line 228
    sub-int/2addr v0, v6

    int-to-double v6, v0

    .line 229
    const-wide v8, 0x3ffb333333333333L

    .line 228
    div-double/2addr v6, v8

    double-to-int v0, v6

    .line 231
    iget-object v6, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    .line 232
    iget-object v7, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v7

    .line 234
    iget-object v8, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v8

    .line 235
    iget-object v9, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v9

    .line 231
    invoke-virtual {v6, v7, v0, v8, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 203
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 215
    :catch_3
    move-exception v0

    .line 217
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_3

    .line 218
    :catch_4
    move-exception v0

    throw v0

    .line 220
    :catch_5
    move-exception v0

    .line 221
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unexpected "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v0, v2

    goto :goto_3

    .line 222
    :catch_6
    move-exception v0

    .line 223
    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unexpected "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v0, v2

    goto :goto_3

    .line 192
    :catch_7
    move-exception v0

    goto/16 :goto_0
.end method

.method private static a(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 275
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 276
    if-nez v0, :cond_0

    .line 277
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .line 278
    const/4 v1, -0x1

    .line 279
    const/4 v2, -0x2

    .line 277
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 283
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 282
    invoke-static {v3, v3, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 284
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 286
    if-lez v0, :cond_1

    .line 287
    const/high16 v2, 0x4000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 291
    :goto_0
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->measure(II)V

    .line 292
    return-void

    .line 289
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x1

    .line 256
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v1, :cond_0

    .line 257
    iput v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    .line 259
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->a()V

    .line 262
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->e:Landroid/widget/TextView;

    const v1, 0x7f090030

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 264
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    const v1, 0x7f020060

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 266
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 268
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 272
    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Landroid/widget/ListView;->onAttachedToWindow()V

    .line 119
    return-void
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->a:Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->a:Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;

    invoke-interface {v0}, Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;->onRefresh()V

    .line 377
    :cond_0
    return-void
.end method

.method public onRefreshComplete()V
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->b()V

    .line 394
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v0

    if-lez v0, :cond_0

    .line 395
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->invalidateViews()V

    .line 396
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/CustomListView;->setSelection(I)V

    .line 398
    :cond_0
    return-void
.end method

.method public onRefreshComplete(Ljava/lang/CharSequence;)V
    .locals 0
    .parameter

    .prologue
    .line 384
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->onRefreshComplete()V

    .line 385
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 299
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->h:I

    if-ne v0, v4, :cond_6

    .line 300
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v1, :cond_6

    .line 301
    if-nez p2, :cond_5

    .line 302
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    int-to-float v1, v1

    const/high16 v2, 0x4248

    iget v3, p0, Lgbis/gbandroid/views/layouts/CustomListView;->o:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 304
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    if-ltz v0, :cond_3

    .line 305
    :cond_0
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v6, :cond_3

    .line 306
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->e:Landroid/widget/TextView;

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 307
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 308
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->j:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 309
    iput v6, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    .line 329
    :cond_1
    :goto_0
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 332
    :cond_2
    return-void

    .line 310
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    int-to-float v1, v1

    const/high16 v2, 0x41c8

    iget v3, p0, Lgbis/gbandroid/views/layouts/CustomListView;->o:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 311
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v5, :cond_1

    .line 312
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->e:Landroid/widget/TextView;

    const v1, 0x7f090031

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 313
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v4, :cond_4

    .line 314
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 315
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    iget-object v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->k:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 317
    :cond_4
    iput v5, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    goto :goto_0

    .line 320
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 321
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->b()V

    goto :goto_0

    .line 323
    :cond_6
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->h:I

    if-ne v0, v5, :cond_1

    .line 324
    if-nez p2, :cond_1

    .line 325
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v1, :cond_1

    .line 326
    invoke-virtual {p0, v4}, Lgbis/gbandroid/views/layouts/CustomListView;->setSelection(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 336
    iput p2, p0, Lgbis/gbandroid/views/layouts/CustomListView;->h:I

    .line 338
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 341
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 150
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    .line 152
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 180
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 154
    :pswitch_0
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->isVerticalScrollBarEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    invoke-virtual {p0, v2}, Lgbis/gbandroid/views/layouts/CustomListView;->setVerticalScrollBarEnabled(Z)V

    .line 157
    :cond_1
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    if-eq v0, v3, :cond_0

    .line 158
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v0

    iget v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    if-gt v0, v1, :cond_2

    .line 159
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    if-ltz v0, :cond_3

    .line 160
    :cond_2
    iget v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 162
    iput v3, p0, Lgbis/gbandroid/views/layouts/CustomListView;->i:I

    .line 163
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->prepareForRefresh()V

    .line 164
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->onRefresh()V

    goto :goto_0

    .line 165
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v0

    iget v1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->l:I

    if-lt v0, v1, :cond_4

    .line 166
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    if-gez v0, :cond_0

    .line 168
    :cond_4
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/CustomListView;->b()V

    .line 169
    invoke-virtual {p0, v2}, Lgbis/gbandroid/views/layouts/CustomListView;->setSelection(I)V

    goto :goto_0

    .line 174
    :pswitch_1
    iput v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->n:I

    goto :goto_0

    .line 177
    :pswitch_2
    invoke-direct {p0, p1}, Lgbis/gbandroid/views/layouts/CustomListView;->a(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public prepareForRefresh()V
    .locals 9

    .prologue
    const/high16 v1, 0x3f80

    .line 357
    iget-object v8, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    new-instance v0, Lgbis/gbandroid/animations/MyScaleAnimation;

    const/4 v4, 0x0

    const/16 v5, 0x190

    iget-object v6, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v7}, Lgbis/gbandroid/animations/MyScaleAnimation;-><init>(FFFFILandroid/view/View;Z)V

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 358
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomListView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/views/layouts/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/views/layouts/a;-><init>(Lgbis/gbandroid/views/layouts/CustomListView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 371
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .parameter

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/CustomListView;->setSelection(I)V

    .line 126
    return-void
.end method

.method public setOnRefreshListener(Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->a:Lgbis/gbandroid/views/layouts/CustomListView$OnRefreshListener;

    .line 146
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .parameter

    .prologue
    .line 136
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/CustomListView;->b:Landroid/widget/AbsListView$OnScrollListener;

    .line 137
    return-void
.end method
