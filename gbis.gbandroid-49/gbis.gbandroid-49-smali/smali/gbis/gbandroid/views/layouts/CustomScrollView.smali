.class public Lgbis/gbandroid/views/layouts/CustomScrollView;
.super Landroid/widget/ScrollView;
.source "GBFile"


# instance fields
.field private a:Lgbis/gbandroid/listeners/ScrollViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    .line 14
    return-void
.end method


# virtual methods
.method protected onScrollChanged(IIII)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 33
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lgbis/gbandroid/listeners/ScrollViewListener;->onScrollChanged(Lgbis/gbandroid/views/layouts/CustomScrollView;IIII)V

    .line 36
    :cond_0
    return-void
.end method

.method public setOnScrollViewListener(Lgbis/gbandroid/listeners/ScrollViewListener;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/CustomScrollView;->a:Lgbis/gbandroid/listeners/ScrollViewListener;

    .line 28
    return-void
.end method
