.class public Lgbis/gbandroid/views/layouts/DashboardLayout;
.super Landroid/view/ViewGroup;
.source "GBFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    iput v1, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    .line 33
    iput v1, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    .line 33
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    .line 33
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    .line 45
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    sub-int v8, p4, p2

    .line 78
    sub-int v9, p5, p3

    .line 80
    invoke-virtual/range {p0 .. p0}, Lgbis/gbandroid/views/layouts/DashboardLayout;->getChildCount()I

    move-result v10

    .line 83
    const/4 v2, 0x0

    .line 84
    const/4 v1, 0x0

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    :goto_0
    if-lt v2, v10, :cond_1

    .line 92
    if-nez v1, :cond_3

    .line 170
    :cond_0
    return-void

    .line 85
    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lgbis/gbandroid/views/layouts/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_2

    .line 87
    add-int/lit8 v1, v1, 0x1

    .line 84
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 98
    :cond_3
    const v3, 0x7fffffff

    .line 105
    const/4 v2, 0x1

    .line 109
    :goto_1
    add-int/lit8 v4, v1, -0x1

    div-int/2addr v4, v2

    add-int/lit8 v5, v4, 0x1

    .line 111
    move-object/from16 v0, p0

    iget v4, v0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    mul-int/2addr v4, v2

    sub-int v4, v8, v4

    add-int/lit8 v6, v2, 0x1

    div-int v7, v4, v6

    .line 112
    move-object/from16 v0, p0

    iget v4, v0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    mul-int/2addr v4, v5

    sub-int v4, v9, v4

    add-int/lit8 v6, v5, 0x1

    div-int v6, v4, v6

    .line 114
    sub-int v4, v6, v7

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 115
    mul-int v11, v5, v2

    if-eq v11, v1, :cond_4

    .line 116
    mul-int/lit8 v4, v4, 0xa

    .line 119
    :cond_4
    if-ge v4, v3, :cond_5

    .line 125
    const/4 v3, 0x1

    if-ne v5, v3, :cond_6

    move v1, v5

    move v3, v6

    move v4, v7

    .line 143
    :goto_2
    const/4 v5, 0x0

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 144
    const/4 v4, 0x0

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v11

    .line 147
    add-int/lit8 v3, v2, 0x1

    mul-int/2addr v3, v7

    sub-int v3, v8, v3

    div-int v8, v3, v2

    .line 148
    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v11

    sub-int v3, v9, v3

    div-int v9, v3, v1

    .line 152
    const/4 v4, 0x0

    .line 153
    const/4 v3, 0x0

    move v6, v3

    :goto_3
    if-ge v6, v10, :cond_0

    .line 154
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lgbis/gbandroid/views/layouts/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    .line 155
    invoke-virtual {v12}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v5, 0x8

    if-eq v3, v5, :cond_9

    .line 156
    div-int v13, v4, v2

    .line 160
    rem-int v3, v4, v2

    .line 162
    add-int/lit8 v5, v3, 0x1

    mul-int/2addr v5, v7

    add-int v5, v5, p2

    mul-int v14, v8, v3

    add-int/2addr v14, v5

    .line 163
    add-int/lit8 v5, v13, 0x1

    mul-int/2addr v5, v11

    add-int v5, v5, p3

    mul-int v15, v9, v13

    add-int/2addr v15, v5

    .line 166
    if-nez v7, :cond_7

    add-int/lit8 v5, v2, -0x1

    if-ne v3, v5, :cond_7

    move/from16 v5, p4

    .line 167
    :goto_4
    if-nez v11, :cond_8

    add-int/lit8 v3, v1, -0x1

    if-ne v13, v3, :cond_8

    move/from16 v3, p5

    .line 165
    :goto_5
    invoke-virtual {v12, v14, v15, v5, v3}, Landroid/view/View;->layout(IIII)V

    .line 168
    add-int/lit8 v3, v4, 0x1

    .line 153
    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    goto :goto_3

    .line 130
    :cond_5
    add-int/lit8 v2, v2, -0x1

    .line 131
    add-int/lit8 v1, v1, -0x1

    div-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    .line 132
    move-object/from16 v0, p0

    iget v3, v0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    mul-int/2addr v3, v2

    sub-int v3, v8, v3

    add-int/lit8 v4, v2, 0x1

    div-int v4, v3, v4

    .line 133
    move-object/from16 v0, p0

    iget v3, v0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    mul-int/2addr v3, v1

    sub-int v3, v9, v3

    add-int/lit8 v5, v1, 0x1

    div-int/2addr v3, v5

    goto :goto_2

    .line 137
    :cond_6
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 108
    goto/16 :goto_1

    .line 166
    :cond_7
    add-int v3, v14, v8

    move v5, v3

    goto :goto_4

    .line 167
    :cond_8
    add-int v3, v15, v9

    goto :goto_5

    :cond_9
    move v3, v4

    goto :goto_6
.end method

.method protected onMeasure(II)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/high16 v3, -0x8000

    const/4 v0, 0x0

    .line 49
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    .line 50
    iput v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    .line 53
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 52
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 55
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 54
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 57
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DashboardLayout;->getChildCount()I

    move-result v3

    .line 58
    :goto_0
    if-lt v0, v3, :cond_0

    .line 71
    iget v0, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    invoke-static {v0, p1}, Lgbis/gbandroid/views/layouts/DashboardLayout;->resolveSize(II)I

    move-result v0

    .line 72
    iget v1, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    invoke-static {v1, p2}, Lgbis/gbandroid/views/layouts/DashboardLayout;->resolveSize(II)I

    move-result v1

    .line 70
    invoke-virtual {p0, v0, v1}, Lgbis/gbandroid/views/layouts/DashboardLayout;->setMeasuredDimension(II)V

    .line 73
    return-void

    .line 59
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DashboardLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 60
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_1

    .line 61
    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    .line 66
    iget v5, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->a:I

    .line 67
    iget v5, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lgbis/gbandroid/views/layouts/DashboardLayout;->b:I

    .line 58
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
