.class public Lgbis/gbandroid/views/layouts/DnDGridView;
.super Landroid/widget/GridView;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;,
        Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;,
        Lgbis/gbandroid/views/layouts/DnDGridView$RemoveListener;
    }
.end annotation


# instance fields
.field private final a:I

.field private b:Landroid/view/WindowManager;

.field private c:Landroid/view/WindowManager$LayoutParams;

.field private d:Landroid/content/res/Resources;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/graphics/Rect;

.field private g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

.field private h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Landroid/content/Context;

.field private s:Landroid/graphics/Bitmap;

.field private t:I

.field private u:I

.field private v:Z

.field private w:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 63
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->d:Landroid/content/res/Resources;

    .line 64
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->a:I

    .line 65
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 56
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->d:Landroid/content/res/Resources;

    .line 57
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->a:I

    .line 58
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 49
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->d:Landroid/content/res/Resources;

    .line 50
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->a:I

    .line 51
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    .line 52
    return-void
.end method

.method private a(II)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 165
    iget-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 166
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildCount()I

    move-result v0

    .line 167
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-gez v0, :cond_0

    .line 174
    const/4 v0, -0x1

    :goto_1
    return v0

    .line 168
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 169
    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 170
    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 171
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getFirstVisiblePosition()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    .line 167
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 200
    move v0, v1

    .line 201
    :goto_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 202
    if-nez v2, :cond_0

    .line 203
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->layoutChildren()V

    .line 210
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 211
    if-eqz v2, :cond_1

    .line 212
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 215
    iget v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->t:I

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 216
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_1
    return-void
.end method

.method private a(I)V
    .locals 1
    .parameter

    .prologue
    .line 193
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    div-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 194
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->p:I

    .line 195
    :cond_0
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    if-gt p1, v0, :cond_1

    .line 196
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->q:I

    .line 197
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;II)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x2

    .line 273
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    .line 274
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 275
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->k:I

    sub-int v1, p3, v1

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->m:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 276
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->j:I

    sub-int v1, p2, v1

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->l:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 278
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 279
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 280
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x198

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 282
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 283
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 285
    new-instance v1, Landroid/widget/ImageView;

    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 286
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->d:Landroid/content/res/Resources;

    const/high16 v2, 0x7f08

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 287
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 288
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 289
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->s:Landroid/graphics/Bitmap;

    .line 291
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->b:Landroid/view/WindowManager;

    .line 292
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->b:Landroid/view/WindowManager;

    iget-object v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 293
    iput-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    .line 294
    return-void
.end method

.method private b(II)I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 179
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->w:I

    if-nez v0, :cond_0

    .line 180
    iput p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->w:I

    .line 181
    :cond_0
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->w:I

    if-ge v0, p1, :cond_2

    .line 182
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->u:I

    div-int/lit8 v0, v0, 0x8

    sub-int v0, p1, v0

    .line 185
    :goto_0
    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 186
    const/16 v0, 0xf

    .line 187
    :cond_1
    invoke-direct {p0, v0, p2}, Lgbis/gbandroid/views/layouts/DnDGridView;->a(II)I

    move-result v0

    .line 188
    iput p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->w:I

    .line 189
    return v0

    .line 184
    :cond_2
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->u:I

    div-int/lit8 v0, v0, 0x8

    add-int/2addr v0, p1

    goto :goto_0
.end method

.method private b()V
    .locals 9

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 222
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    .line 223
    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    iget v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->i:I

    if-le v1, v3, :cond_0

    .line 224
    add-int/lit8 v0, v0, 0x1

    .line 225
    :cond_0
    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->i:I

    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getFirstVisiblePosition()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    move v1, v2

    .line 227
    :goto_0
    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 228
    if-eqz v7, :cond_3

    .line 229
    iget v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->t:I

    .line 232
    invoke-virtual {v7, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 233
    iget v5, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    iget v8, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->i:I

    if-ne v5, v8, :cond_1

    move v5, v3

    move v3, v4

    .line 242
    :goto_1
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 243
    iput v5, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 244
    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 245
    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v5, v3

    move v3, v4

    .line 237
    goto :goto_1

    .line 239
    :cond_2
    if-ne v1, v0, :cond_4

    .line 240
    iget v5, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v5, v8, :cond_4

    .line 241
    iget v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->t:I

    move v5, v3

    move v3, v2

    goto :goto_1

    .line 247
    :cond_3
    return-void

    :cond_4
    move v5, v3

    move v3, v2

    goto :goto_1
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 303
    iget-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    if-eqz v0, :cond_1

    .line 304
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 305
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 307
    iget-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 308
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 309
    iput-object v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    .line 311
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->s:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->s:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 313
    iput-object v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->s:Landroid/graphics/Bitmap;

    .line 316
    :cond_1
    return-void
.end method

.method private c(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->j:I

    sub-int v1, p2, v1

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->l:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 298
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->k:I

    sub-int v1, p1, v1

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->m:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 299
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    iget-object v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->c:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 300
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 70
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->t:I

    if-eqz v0, :cond_0

    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->u:I

    if-nez v0, :cond_1

    .line 71
    :cond_0
    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->t:I

    .line 72
    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->u:I

    .line 74
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    if-nez v0, :cond_2

    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    if-eqz v0, :cond_3

    .line 75
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_3
    :goto_0
    :pswitch_0
    iget-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    if-eqz v0, :cond_4

    .line 105
    iget-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 107
    :goto_1
    return v0

    .line 78
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 79
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 80
    invoke-virtual {p0, v1, v2}, Lgbis/gbandroid/views/layouts/DnDGridView;->pointToPosition(II)I

    move-result v3

    .line 81
    const/4 v0, -0x1

    if-eq v3, v0, :cond_3

    .line 82
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 83
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    sub-int v4, v2, v4

    iput v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->j:I

    .line 84
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v4

    sub-int v4, v1, v4

    iput v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->k:I

    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v4, v2

    iput v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->l:I

    .line 86
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v4, v1

    iput v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->m:I

    .line 87
    const v4, 0x7f070100

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 88
    iget-object v5, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 89
    invoke-virtual {v4, v5}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 90
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setDrawingCacheEnabled(Z)V

    .line 91
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    invoke-direct {p0, v0, v2, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->a(Landroid/graphics/Bitmap;II)V

    .line 93
    iput v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    .line 94
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->i:I

    .line 95
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getHeight()I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    .line 96
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->a:I

    .line 97
    sub-int v3, v2, v0

    iget v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    div-int/lit8 v4, v4, 0x3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->p:I

    .line 98
    add-int/2addr v0, v2

    iget v3, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->q:I

    .line 99
    invoke-direct {p0, v1, v2}, Lgbis/gbandroid/views/layouts/DnDGridView;->c(II)V

    goto/16 :goto_0

    .line 107
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/GridView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-boolean v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    if-nez v0, :cond_0

    .line 113
    invoke-super {p0, p1}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 161
    :goto_0
    return v0

    .line 114
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    if-nez v0, :cond_1

    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    if-eqz v0, :cond_b

    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 116
    packed-switch v0, :pswitch_data_0

    .line 159
    :cond_2
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 119
    :pswitch_0
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->f:Landroid/graphics/Rect;

    .line 120
    iget-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 121
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->c()V

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    if-eqz v0, :cond_3

    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    if-ltz v0, :cond_3

    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    iget v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->i:I

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    invoke-interface {v0, v1, v2}, Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;->drop(II)V

    .line 124
    :cond_3
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->a()V

    goto :goto_1

    .line 128
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 129
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 130
    invoke-direct {p0, v2, v3}, Lgbis/gbandroid/views/layouts/DnDGridView;->c(II)V

    .line 131
    invoke-direct {p0, v2, v3}, Lgbis/gbandroid/views/layouts/DnDGridView;->b(II)I

    move-result v2

    .line 132
    if-ltz v2, :cond_2

    .line 133
    if-eqz v0, :cond_4

    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    if-eq v2, v0, :cond_6

    .line 134
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    if-eqz v0, :cond_5

    .line 135
    iget-object v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    iget v4, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    invoke-interface {v0, v4, v2}, Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;->drag(II)V

    .line 136
    :cond_5
    iput v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->n:I

    .line 137
    invoke-direct {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->b()V

    .line 140
    :cond_6
    invoke-direct {p0, v3}, Lgbis/gbandroid/views/layouts/DnDGridView;->a(I)V

    .line 141
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->q:I

    if-le v3, v0, :cond_9

    .line 142
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    iget v2, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->q:I

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    if-le v3, v0, :cond_8

    const/16 v0, 0x10

    .line 146
    :goto_2
    if-eqz v0, :cond_2

    .line 147
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->pointToPosition(II)I

    move-result v0

    .line 148
    const/4 v2, -0x1

    if-ne v0, v2, :cond_7

    .line 149
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->o:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x40

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->pointToPosition(II)I

    move-result v0

    .line 150
    :cond_7
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 151
    if-eqz v1, :cond_2

    .line 153
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->setSelection(I)V

    goto/16 :goto_1

    .line 142
    :cond_8
    const/4 v0, 0x4

    goto :goto_2

    .line 144
    :cond_9
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->p:I

    if-ge v3, v0, :cond_c

    .line 145
    iget v0, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->p:I

    div-int/lit8 v0, v0, 0x2

    if-ge v3, v0, :cond_a

    const/16 v0, -0x10

    goto :goto_2

    :cond_a
    const/4 v0, -0x4

    goto :goto_2

    :cond_b
    move v0, v1

    .line 161
    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_2

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDragListener(Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;)V
    .locals 0
    .parameter

    .prologue
    .line 319
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->g:Lgbis/gbandroid/views/layouts/DnDGridView$DragListener;

    .line 320
    return-void
.end method

.method public setDropListener(Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;)V
    .locals 0
    .parameter

    .prologue
    .line 323
    iput-object p1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->h:Lgbis/gbandroid/views/layouts/DnDGridView$DropListner;

    .line 324
    return-void
.end method

.method public startDrag(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 250
    const/4 v1, 0x1

    iput-boolean v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->v:Z

    .line 251
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    .line 252
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 253
    iget-object v1, p0, Lgbis/gbandroid/views/layouts/DnDGridView;->r:Landroid/content/Context;

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 254
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildCount()I

    move-result v2

    .line 255
    :goto_0
    if-lt v0, v2, :cond_0

    .line 260
    return-void

    .line 256
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 257
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public stopDrag()V
    .locals 3

    .prologue
    .line 263
    invoke-virtual {p0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildCount()I

    move-result v1

    .line 264
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    .line 268
    return-void

    .line 265
    :cond_0
    invoke-virtual {p0, v0}, Lgbis/gbandroid/views/layouts/DnDGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 266
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
