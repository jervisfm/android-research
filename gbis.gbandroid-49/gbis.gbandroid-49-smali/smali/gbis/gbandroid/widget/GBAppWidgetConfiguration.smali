.class public Lgbis/gbandroid/widget/GBAppWidgetConfiguration;
.super Lgbis/gbandroid/activities/base/GBActivity;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:I

.field private b:Landroid/widget/RelativeLayout;

.field private c:Landroid/widget/RelativeLayout;

.field private d:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivity;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    .line 15
    return-void
.end method

.method static a(Landroid/content/Context;I)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prefix_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 90
    return v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f070166

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->b:Landroid/widget/RelativeLayout;

    .line 45
    const v0, 0x7f070164

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->c:Landroid/widget/RelativeLayout;

    .line 46
    const v0, 0x7f070168

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->d:Landroid/widget/RelativeLayout;

    .line 47
    iget-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->b:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void
.end method

.method private static a(Landroid/content/Context;II)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prefix_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 97
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 98
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->b:Landroid/widget/RelativeLayout;

    if-ne p1, v1, :cond_1

    .line 57
    const v0, 0x17089

    .line 63
    :cond_0
    :goto_0
    iget v1, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    invoke-static {p0, v1, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a(Landroid/content/Context;II)V

    .line 79
    iget v1, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    invoke-static {p0, v0, v1}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->a(Landroid/content/Context;II)V

    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    const-string v1, "appWidgetId"

    iget v2, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 83
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->setResult(ILandroid/content/Intent;)V

    .line 84
    invoke-virtual {p0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->finish()V

    .line 85
    return-void

    .line 58
    :cond_1
    iget-object v1, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->c:Landroid/widget/RelativeLayout;

    if-ne p1, v1, :cond_2

    .line 59
    const/16 v0, 0x5e95

    goto :goto_0

    .line 60
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->d:Landroid/widget/RelativeLayout;

    if-ne p1, v1, :cond_0

    .line 61
    const/16 v0, 0x5e96

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 25
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f030053

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->setContentView(I)V

    .line 27
    invoke-direct {p0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a()V

    .line 30
    invoke-virtual {p0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 34
    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    .line 33
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    .line 38
    :cond_0
    iget v0, p0, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->a:I

    if-nez v0, :cond_1

    .line 39
    invoke-virtual {p0}, Lgbis/gbandroid/widget/GBAppWidgetConfiguration;->finish()V

    .line 41
    :cond_1
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    const-string v0, ""

    return-object v0
.end method
