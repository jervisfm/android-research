.class public Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/widget/GBAppWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StationTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/appwidget/AppWidgetManager;

.field private d:I

.field private e:Lgbis/gbandroid/entities/StationMessage;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/appwidget/AppWidgetManager;I)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 169
    invoke-static {}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->a()[I

    move-result-object v0

    invoke-static {}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->b()I

    move-result v1

    aget v0, v0, v1

    iput v0, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->a:I

    .line 170
    iput-object p1, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->b:Landroid/content/Context;

    .line 171
    iput-object p3, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->c:Landroid/appwidget/AppWidgetManager;

    .line 172
    iput p4, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->d:I

    .line 173
    invoke-static {}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->a(I)V

    .line 174
    invoke-static {}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->b()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 175
    const/4 v0, 0x0

    invoke-static {v0}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->a(I)V

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 201
    :try_start_0
    invoke-virtual {p0}, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->queryWebService()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 205
    :goto_0
    return-object v0

    .line 203
    :catch_0
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 205
    :catch_1
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .parameter

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 181
    const-string v0, "echale"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "aca "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stationId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->e:Lgbis/gbandroid/entities/StationMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/StationMessage;->getStationId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->c:Landroid/appwidget/AppWidgetManager;

    iget v2, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->d:I

    iget-object v3, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->e:Lgbis/gbandroid/entities/StationMessage;

    invoke-static {v0, v1, v2, v3}, Lgbis/gbandroid/widget/GBAppWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILgbis/gbandroid/entities/StationMessage;)V

    .line 191
    :goto_0
    return-void

    .line 185
    :cond_0
    new-instance v0, Landroid/widget/Toast;

    iget-object v1, p0, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 186
    const/16 v1, 0x31

    const/4 v2, 0x0

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 187
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 188
    const-string v1, "error"

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 189
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/GBAppWidgetProvider$StationTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected queryWebService()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method
