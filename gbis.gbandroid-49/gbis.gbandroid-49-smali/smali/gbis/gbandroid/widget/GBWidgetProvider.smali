.class public Lgbis/gbandroid/widget/GBWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "GBFile"


# static fields
.field public static final WIDGET_ID:Ljava/lang/String; = "appWidgetId"

.field protected static prefs:Landroid/content/SharedPreferences;

.field protected static res:Landroid/content/res/Resources;


# instance fields
.field protected adBanner:Lcom/google/ads/AdView;

.field protected adsVisible:Z

.field protected responseObject:Lgbis/gbandroid/entities/ResponseMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgbis/gbandroid/entities/ResponseMessage",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method protected static addPlace(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    .line 110
    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/DBHelper;->addSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v1

    .line 111
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 112
    return v1
.end method

.method protected static closeDB(Lgbis/gbandroid/utils/DBHelper;)V
    .locals 0
    .parameter

    .prologue
    .line 178
    if-eqz p0, :cond_0

    .line 179
    invoke-virtual {p0}, Lgbis/gbandroid/utils/DBHelper;->closeDB()V

    .line 180
    :cond_0
    return-void
.end method

.method protected static deleteAllPlaces(Landroid/content/Context;)Z
    .locals 2
    .parameter

    .prologue
    .line 123
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->deleteSearchesAllRows()Z

    move-result v1

    .line 125
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 126
    return v1
.end method

.method protected static deletePlace(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 116
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 117
    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->deleteSearchesRow(Ljava/lang/String;)Z

    move-result v1

    .line 118
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 119
    return v1
.end method

.method protected static getAllPlaces(Landroid/content/Context;I)Landroid/database/Cursor;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 165
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 166
    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getAllSearchesRows(I)Landroid/database/Cursor;

    move-result-object v1

    .line 167
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 168
    return-object v1
.end method

.method protected static getPlace(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 152
    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getSearchesRow(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 154
    return-object v0
.end method

.method protected static getPlaceFromCityZip(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 158
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 159
    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->getSearchesRowFromCityZip(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 160
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 161
    return-object v1
.end method

.method protected static incrementFrequencyOfPlace(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 145
    invoke-virtual {v0, p1}, Lgbis/gbandroid/utils/DBHelper;->incrementSearchesFrequency(Ljava/lang/String;)Z

    move-result v1

    .line 146
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 147
    return v1
.end method

.method protected static openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;
    .locals 1
    .parameter

    .prologue
    .line 172
    new-instance v0, Lgbis/gbandroid/utils/DBHelper;

    invoke-direct {v0, p0}, Lgbis/gbandroid/utils/DBHelper;-><init>(Landroid/content/Context;)V

    .line 173
    invoke-virtual {v0}, Lgbis/gbandroid/utils/DBHelper;->openDB()Z

    .line 174
    return-object v0
.end method

.method protected static updatePlace(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;DD)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 130
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    .line 131
    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/utils/DBHelper;->updateSearchesRow(Ljava/lang/String;Ljava/lang/String;DD)Z

    move-result v1

    .line 132
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 133
    return v1
.end method

.method protected static updatePlaceName(Landroid/content/Context;JLjava/lang/String;)Z
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    invoke-static {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;->openDB(Landroid/content/Context;)Lgbis/gbandroid/utils/DBHelper;

    move-result-object v0

    .line 138
    invoke-virtual {v0, p1, p2, p3}, Lgbis/gbandroid/utils/DBHelper;->updateSearchesName(JLjava/lang/String;)Z

    move-result v1

    .line 139
    invoke-static {v0}, Lgbis/gbandroid/widget/GBWidgetProvider;->closeDB(Lgbis/gbandroid/utils/DBHelper;)V

    .line 140
    return v1
.end method


# virtual methods
.method protected checkConnection(Landroid/content/Context;)Z
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-static {p1}, Lgbis/gbandroid/utils/ConnectionUtils;->isConnectedToAny(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected getActivity()Lgbis/gbandroid/widget/GBWidgetProvider;
    .locals 0

    .prologue
    .line 55
    return-object p0
.end method

.method protected getHost(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 105
    sget-object v0, Lgbis/gbandroid/widget/GBWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "host"

    const v2, 0x7f0901dd

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initialize(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 39
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lgbis/gbandroid/widget/GBWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lgbis/gbandroid/widget/GBWidgetProvider;->res:Landroid/content/res/Resources;

    .line 42
    return-void
.end method

.method protected launchSettings(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/settings/Settings;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 71
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 72
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 47
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/services/GPSService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 48
    return-void
.end method

.method protected setMessage(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 184
    return-void
.end method

.method protected showAddStation(Landroid/content/Context;Landroid/location/Location;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 100
    new-instance v0, Lgbis/gbandroid/content/GBIntent;

    const-class v1, Lgbis/gbandroid/activities/station/AddStationMap;

    invoke-direct {v0, p1, v1, p2}, Lgbis/gbandroid/content/GBIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;Landroid/location/Location;)V

    .line 101
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 102
    return-void
.end method

.method protected showCityZipSearch(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 59
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/search/SearchBar;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void
.end method

.method protected showFavourites(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 75
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/favorites/Favourites;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 76
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 77
    return-void
.end method

.method protected showHome(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 64
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/search/MainScreen;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const/high16 v1, 0x3400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 66
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 67
    return-void
.end method

.method protected showLogin(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/members/MemberLogin;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method protected showPlaces(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method

.method protected showRegistration(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/members/MemberRegistration;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 92
    return-void
.end method

.method protected showShare(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/settings/ShareApp;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 97
    return-void
.end method
