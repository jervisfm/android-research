.class final Lgbis/gbandroid/widget/ListWidgetConfiguration$a;
.super Lgbis/gbandroid/utils/CustomAsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/widget/ListWidgetConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lgbis/gbandroid/widget/ListWidgetConfiguration;


# direct methods
.method public constructor <init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;Lgbis/gbandroid/activities/base/GBPreferenceActivity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 216
    iput-object p1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    .line 217
    invoke-direct {p0, p2}, Lgbis/gbandroid/utils/CustomAsyncTask;-><init>(Landroid/content/Context;)V

    .line 218
    return-void
.end method


# virtual methods
.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0}, Lgbis/gbandroid/utils/CustomAsyncTask;->onCancelled()V

    .line 247
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-virtual {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->finish()V

    .line 248
    return-void
.end method

.method protected final onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .parameter

    .prologue
    .line 222
    invoke-super {p0, p1}, Lgbis/gbandroid/utils/CustomAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 224
    :try_start_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 228
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-static {v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v1

    .line 230
    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    const-string v3, "widgetFavoritesPreference"

    iget-object v4, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    const v5, 0x7f090113

    invoke-virtual {v4, v5}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v1, v3, v4}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a(Lgbis/gbandroid/widget/ListWidgetConfiguration;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_1
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    const v2, 0x7f09006b

    invoke-virtual {v1, v2}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showMessage(Ljava/lang/String;)V

    goto :goto_1

    .line 235
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-virtual {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showMessage()V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final queryWebService()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c(Lgbis/gbandroid/widget/ListWidgetConfiguration;)V

    .line 241
    const/4 v0, 0x1

    return v0
.end method
