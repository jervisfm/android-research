.class public Lgbis/gbandroid/widget/ListWidgetConfiguration;
.super Lgbis/gbandroid/widget/WidgetConfiguration;
.source "GBFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Lgbis/gbandroid/listeners/WidgetSettingsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/widget/ListWidgetConfiguration$a;
    }
.end annotation


# static fields
.field public static final DISTANCE_PREFERENCE:Ljava/lang/String; = "widgetDistancePreference"

.field public static final FAVORITES_PREFERENCE:Ljava/lang/String; = "widgetFavoritesPreference"

.field public static final FUEL_PREFERENCE:Ljava/lang/String; = "widgetFuelPreference"

.field public static final LOCATION_TITLE_PREFERENCE:Ljava/lang/String; = "widgetLocationTitle"

.field public static final MYLOCATION_PREFERENCE:Ljava/lang/String; = "widgetMyLocationPreference"

.field public static final OTHERLOCATION_PREFERENCE:Ljava/lang/String; = "widgetOtherLocationPreference"

.field public static final RADIUS_PREFERENCE:Ljava/lang/String; = "widgetRadiusPreference"

.field public static final REFRESH_PREFERENCE:Ljava/lang/String; = "widgetRefreshPreference"

.field public static final SAVEDSEARCHES_PREFERENCE:Ljava/lang/String; = "widgetSavedSearchesPreference"

.field public static final SORT_PREFERENCE:Ljava/lang/String; = "widgetSortPreference"


# instance fields
.field private a:Landroid/preference/Preference;

.field private b:Landroid/preference/Preference;

.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/Preference;

.field private e:Landroid/preference/Preference;

.field private f:I

.field private g:Landroid/preference/Preference;

.field private h:Landroid/preference/Preference;

.field private i:Landroid/preference/Preference;

.field private j:Landroid/preference/PreferenceCategory;

.field private k:Landroid/preference/Preference;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavListMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->progress:Landroid/app/Dialog;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 106
    const-string v0, "widgetFuelPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a:Landroid/preference/Preference;

    .line 107
    const-string v0, "widgetSortPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b:Landroid/preference/Preference;

    .line 108
    const-string v0, "widgetDistancePreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->e:Landroid/preference/Preference;

    .line 109
    const-string v0, "widgetRadiusPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c:Landroid/preference/Preference;

    .line 110
    const-string v0, "widgetRefreshPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->d:Landroid/preference/Preference;

    .line 111
    const-string v0, "widgetMyLocationPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->g:Landroid/preference/Preference;

    .line 112
    const-string v0, "widgetSavedSearchesPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->h:Landroid/preference/Preference;

    .line 113
    const-string v0, "widgetFavoritesPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->k:Landroid/preference/Preference;

    .line 114
    const-string v0, "widgetOtherLocationPreference"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->i:Landroid/preference/Preference;

    .line 115
    const-string v0, "widgetLocationTitle"

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->j:Landroid/preference/PreferenceCategory;

    .line 117
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 118
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->e:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 120
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 121
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->d:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 122
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->g:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 123
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->h:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 124
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->k:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 125
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->i:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 126
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->k:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 128
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/SeekBar;Landroid/widget/TextView;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "widgetDistancePreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const-string v0, "miles"

    .line 158
    :goto_0
    const-string v1, "miles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    const/16 v2, 0xf

    .line 160
    const/high16 v1, 0x3f80

    .line 167
    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 168
    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "widgetRadiusPreference"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/lit8 v2, v2, -0x5

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    new-instance v2, Lgbis/gbandroid/widget/b;

    invoke-direct {v2, p0, v1, p2, v0}, Lgbis/gbandroid/widget/b;-><init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;FLandroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 183
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "widgetDistancePreference"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :cond_1
    const/16 v2, 0x19

    .line 164
    const v1, 0x3fcdfeda

    goto :goto_1
.end method

.method static synthetic a(Lgbis/gbandroid/widget/ListWidgetConfiguration;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59
    iput p1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->f:I

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/widget/ListWidgetConfiguration;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ljava/util/List;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 199
    invoke-static {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lgbis/gbandroid/widget/ListWidgetConfiguration;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->m:Ljava/util/List;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 131
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v2, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 132
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03001c

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 133
    const v0, 0x7f07006a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    .line 134
    const v1, 0x7f07006b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 135
    invoke-direct {p0, v0, v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a(Landroid/widget/SeekBar;Landroid/widget/TextView;)V

    .line 136
    const v0, 0x7f09010d

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 137
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 138
    const v0, 0x7f090198

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/widget/a;

    invoke-direct {v1, p0}, Lgbis/gbandroid/widget/a;-><init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;)V

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 146
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 148
    return-void
.end method

.method static synthetic b(Ljava/util/List;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 207
    invoke-static {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->d(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 187
    new-instance v0, Lgbis/gbandroid/widget/c;

    invoke-direct {v0, p0}, Lgbis/gbandroid/widget/c;-><init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;)V

    invoke-virtual {v0}, Lgbis/gbandroid/widget/c;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 188
    new-instance v1, Lgbis/gbandroid/queries/FavListQuery;

    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v3, Landroid/location/Location;

    const-string v4, "NoLocation"

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 189
    invoke-virtual {v1}, Lgbis/gbandroid/queries/FavListQuery;->getResponseObject()Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    .line 190
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mResponseObject:Lgbis/gbandroid/entities/ResponseMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->m:Ljava/util/List;

    .line 191
    return-void
.end method

.method static synthetic c(Lgbis/gbandroid/widget/ListWidgetConfiguration;)V
    .locals 0
    .parameter

    .prologue
    .line 186
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c()V

    return-void
.end method

.method private static c(Ljava/util/List;)[Ljava/lang/String;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavListMessage;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 200
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 201
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 202
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 204
    return-object v2

    .line 203
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic d(Lgbis/gbandroid/widget/ListWidgetConfiguration;)I
    .locals 1
    .parameter

    .prologue
    .line 59
    iget v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->f:I

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;

    invoke-direct {v0, p0, p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration$a;-><init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;Lgbis/gbandroid/activities/base/GBPreferenceActivity;)V

    .line 195
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/utils/CustomAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 196
    const v1, 0x7f090092

    invoke-virtual {p0, v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->loadDialog(Ljava/lang/String;Lgbis/gbandroid/utils/CustomAsyncTask;)V

    .line 197
    return-void
.end method

.method private static d(Ljava/util/List;)[Ljava/lang/String;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavListMessage;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 208
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 209
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 210
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 212
    return-object v2

    .line 211
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 210
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected locationChosen(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->j:Landroid/preference/PreferenceCategory;

    new-instance v1, Ljava/lang/StringBuilder;

    const v2, 0x7f090104

    invoke-virtual {p0, v2}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 295
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 70
    invoke-super {p0, p1}, Lgbis/gbandroid/widget/WidgetConfiguration;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const v0, 0x7f050003

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->addPreferencesFromResource(I)V

    .line 72
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const-string v1, "member_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->l:Ljava/lang/String;

    .line 73
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a()V

    .line 74
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationDisplayed_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->locationChosen(Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->onDestroy()V

    .line 87
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 88
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 92
    packed-switch p1, :pswitch_data_0

    .line 101
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/widget/WidgetConfiguration;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 94
    :pswitch_0
    iget v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mAppWidgetId:I

    invoke-static {p0, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;I)V

    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 96
    const-string v1, "appWidgetId"

    iget v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->setResult(ILandroid/content/Intent;)V

    .line 98
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->finish()V

    .line 99
    const/4 v0, 0x1

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 254
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const/high16 v1, 0x7f06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 256
    const-string v2, "widgetFuelPreference"

    const v3, 0x7f09010e

    invoke-virtual {p0, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 257
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b:Landroid/preference/Preference;

    if-ne p1, v0, :cond_1

    .line 258
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v1, 0x7f060006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v2, 0x7f060007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 260
    const-string v2, "widgetSortPreference"

    const v3, 0x7f090110

    invoke-virtual {p0, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->e:Landroid/preference/Preference;

    if-ne p1, v0, :cond_2

    .line 262
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v1, 0x7f060002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 264
    const-string v2, "widgetDistancePreference"

    const v3, 0x7f09010f

    invoke-virtual {p0, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 265
    :cond_2
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->c:Landroid/preference/Preference;

    if-ne p1, v0, :cond_3

    .line 266
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->b()V

    goto :goto_0

    .line 267
    :cond_3
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->d:Landroid/preference/Preference;

    if-ne p1, v0, :cond_4

    .line 268
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v1, 0x7f06000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 269
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->res:Landroid/content/res/Resources;

    const v2, 0x7f06000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 270
    const-string v2, "widgetRefreshPreference"

    const v3, 0x7f090112

    invoke-virtual {p0, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showListDialog([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 271
    :cond_4
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->g:Landroid/preference/Preference;

    if-ne p1, v0, :cond_5

    .line 272
    const v0, 0x7f090107

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->locationChosen(Ljava/lang/String;)V

    .line 273
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/services/GPSService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 275
    const-wide v0, 0x4060400000000000L

    const-wide v2, -0x3fa9800000000000L

    invoke-virtual {p0, v0, v1, v2, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->storeLocation(DD)V

    goto/16 :goto_0

    .line 276
    :cond_5
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->h:Landroid/preference/Preference;

    if-ne p1, v0, :cond_6

    .line 277
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showPlaces()V

    goto/16 :goto_0

    .line 278
    :cond_6
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->i:Landroid/preference/Preference;

    if-ne p1, v0, :cond_7

    .line 279
    invoke-virtual {p0, p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->showOtherLocation(Lgbis/gbandroid/listeners/WidgetSettingsListener;)V

    goto/16 :goto_0

    .line 280
    :cond_7
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->k:Landroid/preference/Preference;

    if-ne p1, v0, :cond_8

    .line 281
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->d()V

    goto/16 :goto_0

    .line 283
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->onResume()V

    .line 81
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 82
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 299
    const-string v1, "widgetFavoritesPreference"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    const-string v1, ""

    .line 301
    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const-string v3, "widgetFavoritesPreference"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 302
    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    .line 303
    :goto_0
    if-lt v2, v4, :cond_1

    move-object v0, v1

    .line 308
    :goto_1
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    invoke-virtual {p0, v0, v3}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->storeLocation(Ljava/lang/String;I)V

    .line 310
    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->locationChosen(Ljava/lang/String;)V

    .line 313
    :cond_0
    return-void

    .line 304
    :cond_1
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListId()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 305
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavListMessage;

    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavListMessage;->getListName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 303
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public otherLocationEntered(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->storeLocation(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->locationChosen(Ljava/lang/String;)V

    .line 291
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    const-string v0, ""

    return-object v0
.end method
