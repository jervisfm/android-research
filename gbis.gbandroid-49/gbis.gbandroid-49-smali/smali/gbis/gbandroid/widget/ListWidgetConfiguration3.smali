.class public Lgbis/gbandroid/widget/ListWidgetConfiguration3;
.super Lgbis/gbandroid/activities/base/GBActivitySearch;
.source "GBFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/widget/ListWidgetConfiguration3$UpdateService;
    }
.end annotation


# instance fields
.field b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->b:I

    .line 22
    return-void
.end method

.method public static makeControlPendingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 160
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/services/GPSService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "gblistwidget://widget/id/#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 166
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 167
    const/4 v1, 0x0

    const/high16 v2, 0x800

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static updateList(Landroid/content/Context;II)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 149
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/widget/ListWidgetConfiguration3$UpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    const-string v1, "smId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 151
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 152
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 153
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 154
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 155
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 157
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 125
    iget v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->b:I

    invoke-static {p0, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;I)V

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    const-string v1, "appWidgetId"

    iget v2, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 129
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->setResult(ILandroid/content/Intent;)V

    .line 130
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->finish()V

    .line 131
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 29
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "oncreate"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->setContentView(I)V

    .line 32
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->populateButtons()V

    .line 35
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 37
    if-eqz v0, :cond_0

    .line 39
    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->b:I

    .line 43
    :cond_0
    iget v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->b:I

    if-nez v0, :cond_1

    .line 44
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->finish()V

    .line 46
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onDestroy()V

    .line 51
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "ondestoy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 80
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onNewIntent(Landroid/content/Intent;)V

    .line 81
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "onnewintent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onPause()V

    .line 75
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "onpause"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onRestart()V

    .line 69
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "onrestart"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onResume()V

    .line 63
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "onresume"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->onStop()V

    .line 57
    const-string v0, "Chaaaaaaacnlas1"

    const-string v1, "onstop"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void
.end method

.method protected populateButtons()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lgbis/gbandroid/activities/base/GBActivitySearch;->populateButtons()V

    .line 86
    const v0, 0x7f070106

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->buttonNearMe:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetConfiguration3;->buttonNearMe:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    return-void
.end method

.method protected setAdsAfterGPSServiceConnected()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method protected setAnalyticsPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    const-string v0, ""

    return-object v0
.end method
