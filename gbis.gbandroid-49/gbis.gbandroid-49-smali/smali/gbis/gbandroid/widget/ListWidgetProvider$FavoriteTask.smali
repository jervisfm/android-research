.class public Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/widget/ListWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FavoriteTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Landroid/content/Context;

.field private c:Landroid/appwidget/AppWidgetManager;

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 407
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 408
    iput-object p1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->b:Landroid/content/Context;

    .line 409
    iput-object p2, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->c:Landroid/appwidget/AppWidgetManager;

    .line 410
    iput p3, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->d:I

    .line 411
    iput p4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->a:I

    .line 412
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 443
    :try_start_0
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->queryWebService()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 447
    :goto_0
    return-object v0

    .line 445
    :catch_0
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 447
    :catch_1
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 416
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 417
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 418
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 420
    :goto_0
    if-lt v1, v4, :cond_0

    .line 423
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->c:Landroid/appwidget/AppWidgetManager;

    iget v4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->d:I

    invoke-static {v0, v1, v4, v3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V

    .line 433
    :goto_1
    return-void

    .line 421
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 420
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 430
    :cond_1
    const-string v0, "GBAPP"

    const-string v1, "error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->c:Landroid/appwidget/AppWidgetManager;

    iget v4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->d:I

    invoke-static {v0, v1, v4, v3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected queryWebService()Z
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->b:Landroid/content/Context;

    iget v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->a:I

    invoke-static {v0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->e:Ljava/util/List;

    .line 437
    const/4 v0, 0x1

    return v0
.end method
