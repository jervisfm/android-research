.class public Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;
.super Landroid/os/AsyncTask;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/widget/ListWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Landroid/appwidget/AppWidgetManager;

.field private d:I

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/maps/GeoPoint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 331
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 332
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 333
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILcom/google/android/maps/GeoPoint;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 340
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 341
    iput-object p4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    .line 342
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 343
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 335
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 336
    iput-object p4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a:Ljava/lang/String;

    .line 337
    invoke-direct {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 338
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 347
    iput-object p1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->b:Landroid/content/Context;

    .line 348
    iput-object p2, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->c:Landroid/appwidget/AppWidgetManager;

    .line 349
    iput p3, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->d:I

    .line 353
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 392
    :try_start_0
    invoke-virtual {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->queryWebService()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lgbis/gbandroid/exceptions/CustomConnectionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 396
    :goto_0
    return-object v0

    .line 394
    :catch_0
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 396
    :catch_1
    move-exception v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 357
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 358
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 359
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 361
    :goto_0
    if-lt v1, v4, :cond_0

    .line 364
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->c:Landroid/appwidget/AppWidgetManager;

    iget v4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->d:I

    invoke-static {v0, v1, v4, v3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V

    .line 374
    :goto_1
    return-void

    .line 362
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/Station;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 371
    :cond_1
    const-string v0, "GBAPP"

    const-string v1, "error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->c:Landroid/appwidget/AppWidgetManager;

    iget v4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->d:I

    invoke-static {v0, v1, v4, v3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected queryWebService()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 377
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    if-nez v1, :cond_0

    .line 378
    new-instance v1, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v1, v0, v0}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iput-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    .line 379
    :cond_0
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 380
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    invoke-static {v0, v1, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->e:Ljava/util/List;

    .line 386
    :goto_0
    const/4 v0, 0x1

    :cond_1
    return v0

    .line 382
    :cond_2
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v1

    if-eqz v1, :cond_1

    .line 383
    iget-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->b:Landroid/content/Context;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    iget-object v4, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->f:Lcom/google/android/maps/GeoPoint;

    invoke-static {v0, v1, v2, v3, v4}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->e:Ljava/util/List;

    goto :goto_0
.end method
