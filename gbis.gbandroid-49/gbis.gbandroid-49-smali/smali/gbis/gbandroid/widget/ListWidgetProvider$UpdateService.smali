.class public Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;
.super Landroid/app/Service;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgbis/gbandroid/widget/ListWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateService"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService$a;
    }
.end annotation


# static fields
.field private static b:Lgbis/gbandroid/services/GPSService;

.field private static c:I

.field private static d:Landroid/content/SharedPreferences;


# instance fields
.field final a:Landroid/os/Handler;

.field private e:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 452
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 535
    new-instance v0, Lgbis/gbandroid/widget/f;

    invoke-direct {v0, p0}, Lgbis/gbandroid/widget/f;-><init>(Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;)V

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->e:Landroid/content/ServiceConnection;

    .line 566
    new-instance v0, Lgbis/gbandroid/widget/g;

    invoke-direct {v0, p0}, Lgbis/gbandroid/widget/g;-><init>(Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;)V

    iput-object v0, p0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->a:Landroid/os/Handler;

    .line 452
    return-void
.end method

.method static synthetic a()Lgbis/gbandroid/services/GPSService;
    .locals 1

    .prologue
    .line 453
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    return-object v0
.end method

.method static synthetic a(Lgbis/gbandroid/services/GPSService;)V
    .locals 0
    .parameter

    .prologue
    .line 453
    sput-object p0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;)V
    .locals 0
    .parameter

    .prologue
    .line 515
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 506
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/services/GPSService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 507
    sget-object v1, Lgbis/gbandroid/services/GPSService;->LOCATION_UPDATE_TIME:Ljava/lang/String;

    sget-object v2, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d:Landroid/content/SharedPreferences;

    const-string v3, "widgetRefreshPreference"

    const v4, 0x36ee80

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 508
    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->e:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    :goto_0
    return-void

    .line 509
    :catch_0
    move-exception v0

    .line 510
    const-string v1, "ERRORSISIMO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "asi es papa "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const-wide v6, 0x412e848000000000L

    .line 516
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    sget-object v1, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d:Landroid/content/SharedPreferences;

    const-string v2, "widgetRefreshPreference"

    const v3, 0x36ee80

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lgbis/gbandroid/services/GPSService;->startLocationListener(J)V

    .line 517
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 518
    const-string v0, "WIDGETLISTGPS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "retrieving gps "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    invoke-static {}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->getSelfLocation()Landroid/location/Location;

    move-result-object v0

    .line 520
    if-nez v0, :cond_1

    .line 521
    const-string v0, "WIDGETLISTGPS"

    const-string v1, "segundo"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    invoke-static {}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    move-object v1, v0

    .line 525
    :goto_0
    if-eqz v1, :cond_0

    .line 526
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    mul-double/2addr v3, v6

    double-to-int v3, v3

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v1, v4

    invoke-direct {v0, v3, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 529
    :goto_1
    const-string v1, "WIDGETLISTGPS"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "gps2 "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    sget v1, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-static {p0, v2, v1, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->startNearMeThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILcom/google/android/maps/GeoPoint;)V

    .line 531
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0}, Lgbis/gbandroid/services/GPSService;->stopLocationListener()V

    .line 532
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d()V

    .line 533
    return-void

    .line 528
    :cond_0
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v0, v5, v5}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 576
    new-instance v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService$a;

    iget-object v1, p0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->a:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService$a;-><init>(Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;Landroid/os/Handler;)V

    .line 577
    invoke-virtual {v0}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService$a;->start()V

    .line 578
    return-void
.end method

.method protected static getLastKnownLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 560
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    if-eqz v0, :cond_0

    .line 561
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0}, Lgbis/gbandroid/services/GPSService;->getLastKnownLocation()Landroid/location/Location;

    move-result-object v0

    .line 563
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static getSelfLocation()Landroid/location/Location;
    .locals 3

    .prologue
    .line 549
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    if-eqz v0, :cond_0

    .line 550
    const-string v0, "GPSBinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "este es el mservice "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :goto_0
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    if-eqz v0, :cond_1

    .line 554
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    invoke-virtual {v0}, Lgbis/gbandroid/services/GPSService;->getSelfLocation()Landroid/location/Location;

    move-result-object v0

    .line 556
    :goto_1
    return-object v0

    .line 552
    :cond_0
    const-string v0, "GPSBinder"

    const-string v1, "mservice es null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 556
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter

    .prologue
    .line 501
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 458
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 459
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d:Landroid/content/SharedPreferences;

    .line 460
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b()V

    .line 461
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 464
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 465
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    .line 466
    const-string v0, "position"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 467
    const-string v1, "list"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 468
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 469
    const-string v3, "WIDGET"

    new-instance v4, Ljava/lang/StringBuilder;

    sget v5, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    if-nez v1, :cond_3

    .line 471
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "LocationDisplayed_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 472
    sget-object v1, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->d:Landroid/content/SharedPreferences;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "LocationFavorite_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 473
    if-nez v1, :cond_2

    .line 474
    sget-object v1, Lgbis/gbandroid/widget/ListWidgetProvider;->res:Landroid/content/res/Resources;

    const v3, 0x7f090107

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->b:Lgbis/gbandroid/services/GPSService;

    if-eqz v0, :cond_0

    .line 476
    invoke-direct {p0}, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c()V

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    sget v1, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-static {p0, v2, v1, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    goto :goto_0

    .line 493
    :cond_2
    sget v0, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-static {p0, v2, v0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider;->startFavThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    goto :goto_0

    .line 496
    :cond_3
    sget v3, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;->c:I

    invoke-static {p0, v2, v3, v1, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I[BI)V

    goto :goto_0
.end method
