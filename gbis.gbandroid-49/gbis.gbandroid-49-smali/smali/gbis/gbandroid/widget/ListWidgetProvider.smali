.class public Lgbis/gbandroid/widget/ListWidgetProvider;
.super Lgbis/gbandroid/widget/GBWidgetProvider;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/widget/ListWidgetProvider$ChangeStationService;,
        Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;,
        Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;,
        Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lgbis/gbandroid/widget/GBWidgetProvider;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;II)Landroid/app/PendingIntent;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x1000

    .line 298
    if-nez p2, :cond_0

    .line 299
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/widget/ListWidgetConfiguration;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 300
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 301
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 305
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 306
    :goto_1
    return-object v0

    .line 302
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 303
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/InitScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 304
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;IILjava/util/List;I)Landroid/app/PendingIntent;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/Station;",
            ">;I)",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/widget/ListWidgetProvider$ChangeStationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 311
    const-string v1, "list"

    invoke-static {p3}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Ljava/util/List;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 312
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 313
    const-string v1, "GasBuddyAndroid://widget/id/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 314
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 315
    const/4 v1, 0x1

    if-ne p4, v1, :cond_1

    .line 316
    add-int/lit8 p1, p1, 0x1

    .line 319
    :cond_0
    :goto_0
    const-string v1, "position"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 320
    const/4 v1, 0x0

    const/high16 v2, 0x800

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 321
    return-object v0

    .line 317
    :cond_1
    const/4 v1, -0x1

    if-ne p4, v1, :cond_0

    .line 318
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-static {p0, p1, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    invoke-static {p0, p1, p2, p3, p4}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;I)V
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 99
    invoke-static {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider;->d(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 100
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 101
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 102
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sget-object v4, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    const-string v5, "widgetRefreshPreference"

    const v7, 0x36ee80

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 103
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-static {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    return-void
.end method

.method static a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/appwidget/AppWidgetManager;",
            "I",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/Station;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 224
    new-instance v5, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030059

    invoke-direct {v5, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 225
    const v1, 0x7f070178

    const/4 v2, 0x0

    invoke-static {p0, p2, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 226
    const v1, 0x7f070176

    const/4 v2, 0x1

    invoke-static {p0, p2, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;II)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 227
    const v1, 0x7f070177

    invoke-static {p0, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->d(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 228
    if-eqz p3, :cond_8

    .line 229
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ne p4, v1, :cond_a

    .line 230
    const/4 v1, 0x0

    .line 231
    :goto_0
    const/4 v2, -0x1

    if-ne v1, v2, :cond_9

    .line 232
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    .line 233
    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    const-string v4, ""

    .line 235
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgbis/gbandroid/entities/Station;

    .line 236
    const v2, 0x7f070182

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 237
    sget-object v2, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "widgetFuelPreference"

    const-string v7, ""

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    invoke-virtual {v1, p0, v2}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_2

    .line 239
    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v6

    const-string v7, "USA"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 240
    const v6, 0x7f07017a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, v2}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v8

    const/4 v10, 0x2

    invoke-static {v8, v9, v10}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 241
    const v6, 0x7f07017b

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 246
    :goto_2
    const v6, 0x7f07017c

    invoke-virtual {v1, p0, v2}, Lgbis/gbandroid/entities/Station;->getTimeSpottedConverted(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 247
    const v2, 0x7f07017c

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    :goto_3
    const v2, 0x7f070183

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 254
    const v6, 0x7f070184

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 255
    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getState()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getState()Ljava/lang/String;

    move-result-object v2

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v8, ", "

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getState()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-virtual {v5, v6, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 256
    instance-of v2, v1, Lgbis/gbandroid/entities/ListMessage;

    if-eqz v2, :cond_7

    move-object v2, v1

    check-cast v2, Lgbis/gbandroid/entities/ListMessage;

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmpl-double v2, v6, v8

    if-eqz v2, :cond_7

    .line 258
    :try_start_0
    sget-object v2, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "widgetDistancePreference"

    const-string v7, ""

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 260
    const-string v6, "miles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 261
    new-instance v6, Ljava/lang/StringBuilder;

    move-object v0, v1

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    move-object v2, v0

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v7

    invoke-static {v7, v8}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 272
    :goto_5
    const v4, 0x7f07017d

    invoke-virtual {v5, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 274
    :try_start_1
    const-class v2, Lgbis/gbandroid/R$drawable;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "logo_"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getStationName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "-"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "\'"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 275
    const v2, 0x7f070181

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v5, v2, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 276
    const v1, 0x7f070181

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 280
    :goto_6
    const v1, 0x7f07017f

    const/4 v2, 0x1

    invoke-static {p0, v3, p2, p3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;IILjava/util/List;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 281
    const v1, 0x7f070180

    const/4 v2, -0x1

    invoke-static {p0, v3, p2, p3, v2}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;IILjava/util/List;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 287
    :cond_0
    :goto_7
    invoke-virtual {p1, p2, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 288
    return-void

    .line 243
    :cond_1
    const v6, 0x7f07017a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, v2}, Lgbis/gbandroid/entities/Station;->getPrice(Landroid/content/Context;Ljava/lang/String;)D

    move-result-wide v8

    const/4 v10, 0x1

    invoke-static {v8, v9, v10}, Lgbis/gbandroid/utils/VerifyFields;->doubleToScale(DI)Ljava/math/BigDecimal;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 244
    const v6, 0x7f07017b

    const/16 v7, 0x8

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 249
    :cond_2
    const v2, 0x7f07017a

    const-string v6, "--"

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 250
    const v2, 0x7f07017b

    const/16 v6, 0x8

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    const v2, 0x7f07017c

    const/4 v6, 0x4

    invoke-virtual {v5, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 255
    :cond_3
    const-string v2, ""

    goto/16 :goto_4

    .line 263
    :cond_4
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    move-object v0, v1

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    move-object v2, v0

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v7

    const-wide v9, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v7, v9

    invoke-static {v7, v8}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 265
    :cond_5
    invoke-virtual {v1}, Lgbis/gbandroid/entities/Station;->getCountry()Ljava/lang/String;

    move-result-object v2

    const-string v6, "USA"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 266
    new-instance v6, Ljava/lang/StringBuilder;

    move-object v0, v1

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    move-object v2, v0

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v7

    invoke-static {v7, v8}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " mi"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 268
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    move-object v0, v1

    check-cast v0, Lgbis/gbandroid/entities/ListMessage;

    move-object v2, v0

    invoke-virtual {v2}, Lgbis/gbandroid/entities/ListMessage;->getDistance()D

    move-result-wide v7

    const-wide v9, 0x3ff9bfdf7e8038a0L

    mul-double/2addr v7, v9

    invoke-static {v7, v8}, Lgbis/gbandroid/utils/VerifyFields;->roundTwoDecimals(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " km"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    goto/16 :goto_5

    .line 269
    :catch_0
    move-exception v2

    move-object v2, v4

    goto/16 :goto_5

    .line 271
    :cond_7
    const-string v2, ""

    goto/16 :goto_5

    .line 278
    :catch_1
    move-exception v1

    const v1, 0x7f070181

    const/4 v2, 0x4

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6

    .line 284
    :cond_8
    const v1, 0x7f070182

    const-string v2, "ERROR"

    invoke-virtual {v5, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 285
    const-string v1, "Error "

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    :cond_9
    move v3, v1

    goto/16 :goto_1

    :cond_a
    move v1, p4

    goto/16 :goto_0
.end method

.method static a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I[BI)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 214
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 215
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 216
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V

    .line 217
    invoke-static {p0, p1, p2, v0, p4}, Lgbis/gbandroid/widget/ListWidgetProvider;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/util/List;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)[B
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/Station;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 107
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 108
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 109
    invoke-interface {v1, p0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    .line 110
    invoke-interface {v1}, Ljava/io/ObjectOutput;->close()V

    .line 111
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/content/Context;I)Ljava/util/List;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-static {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider;->c(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;
    .locals 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/maps/GeoPoint;",
            ")",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 185
    const-string v0, ""

    const-string v1, ""

    invoke-static {p0, v0, v1, p1, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 187
    :catch_0
    move-exception v0

    invoke-static {p1}, Lgbis/gbandroid/utils/VerifyFields;->checkPostalCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    const-string v1, ""

    const-string v2, ""

    invoke-static {p0, v1, v2, v0, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 191
    :cond_0
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 192
    const/4 v0, -0x1

    if-eq v1, v0, :cond_3

    .line 193
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x3

    if-le v2, v3, :cond_1

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object p1, v0

    move-object v0, v1

    .line 204
    :goto_1
    const-string v1, ""

    invoke-static {p0, p1, v0, v1, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 198
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v3, v1, 0x2

    if-le v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 199
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object p1, v0

    move-object v0, v1

    goto :goto_1

    .line 202
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 206
    :cond_3
    const-string v0, ""

    const-string v1, ""

    invoke-static {p0, p1, v0, v1, p2}, Lgbis/gbandroid/widget/ListWidgetProvider;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Ljava/util/List;
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/maps/GeoPoint;",
            ")",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/ListMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide v4, 0x412e848000000000L

    .line 118
    new-instance v0, Lgbis/gbandroid/widget/d;

    invoke-direct {v0}, Lgbis/gbandroid/widget/d;-><init>()V

    invoke-virtual {v0}, Lgbis/gbandroid/widget/d;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 119
    new-instance v1, Landroid/location/Location;

    const-string v2, "Widget"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p4}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 121
    invoke-virtual {p4}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 122
    new-instance v2, Lgbis/gbandroid/queries/ListQuery;

    sget-object v3, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    invoke-direct {v2, p0, v3, v0, v1}, Lgbis/gbandroid/queries/ListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 123
    invoke-virtual {v2, p1, p2, p3, p4}, Lgbis/gbandroid/queries/ListQuery;->getResponseObject(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/maps/GeoPoint;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/ListResults;

    .line 125
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ListResults;->getListMessages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 152
    invoke-static {p0, p3}, Lgbis/gbandroid/widget/ListWidgetProvider;->getPlace(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 153
    if-nez v0, :cond_0

    .line 154
    invoke-static {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider;->startCityZipThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    .line 163
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_1

    .line 157
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 158
    invoke-static {p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider;->startCityZipThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 161
    invoke-static {p0, p1, p2, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->searchUsingPlaces(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;I)Ljava/util/List;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/FavStationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Lgbis/gbandroid/widget/e;

    invoke-direct {v0}, Lgbis/gbandroid/widget/e;-><init>()V

    invoke-virtual {v0}, Lgbis/gbandroid/widget/e;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 130
    new-instance v1, Lgbis/gbandroid/queries/FavStationListQuery;

    sget-object v2, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    new-instance v3, Landroid/location/Location;

    const-string v4, "NoLocation"

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/FavStationListQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 131
    const/4 v0, 0x1

    const/16 v2, 0x64

    invoke-virtual {v1, p1, v0, v2}, Lgbis/gbandroid/queries/FavStationListQuery;->getResponseObject(III)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbis/gbandroid/entities/FavStationResults;

    .line 133
    invoke-virtual {v0}, Lgbis/gbandroid/entities/FavStationResults;->getFavStationMessage()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 291
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/widget/ListWidgetProvider$UpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    const/4 v1, 0x0

    const/high16 v2, 0x800

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method protected static searchUsingPlaces(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/database/Cursor;)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide v9, 0x412e848000000000L

    const-wide/16 v7, 0x0

    .line 166
    const-string v0, "table_column_latitude"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 167
    const-string v2, "table_column_longitude"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 168
    const-string v4, "Places"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    cmpl-double v4, v0, v7

    if-nez v4, :cond_0

    cmpl-double v4, v2, v7

    if-nez v4, :cond_0

    .line 170
    const-string v0, "table_column_city_zip"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    const-string v1, "Places2"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-static {p0, p1, p2, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->startCityZipThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    .line 178
    :goto_0
    const-string v0, "table_column_name"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lgbis/gbandroid/widget/ListWidgetProvider;->incrementFrequencyOfPlace(Landroid/content/Context;Ljava/lang/String;)Z

    .line 179
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 180
    return-void

    .line 174
    :cond_0
    const-string v4, "Places3"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    new-instance v4, Lcom/google/android/maps/GeoPoint;

    mul-double/2addr v0, v9

    double-to-int v0, v0

    mul-double v1, v2, v9

    double-to-int v1, v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 176
    invoke-static {p0, p1, p2, v4}, Lgbis/gbandroid/widget/ListWidgetProvider;->startNearMeThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILcom/google/android/maps/GeoPoint;)V

    goto :goto_0
.end method

.method protected static startCityZipThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    new-instance v0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;-><init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILjava/lang/String;)V

    .line 138
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 139
    return-void
.end method

.method protected static startFavThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    new-instance v0, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;-><init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 148
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider$FavoriteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 149
    return-void
.end method

.method protected static startNearMeThread(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILcom/google/android/maps/GeoPoint;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    new-instance v0, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;

    invoke-direct {v0, p0, p1, p2, p3}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;-><init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILcom/google/android/maps/GeoPoint;)V

    .line 143
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/ListWidgetProvider$ListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 144
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/widget/GBWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 93
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    invoke-super {p0, p1}, Lgbis/gbandroid/widget/GBWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 88
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    invoke-super {p0, p1}, Lgbis/gbandroid/widget/GBWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lgbis/gbandroid/widget/GBWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 78
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/widget/GBWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 71
    sget-object v0, Lgbis/gbandroid/widget/ListWidgetProvider;->prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 72
    invoke-virtual {p0, p1}, Lgbis/gbandroid/widget/ListWidgetProvider;->initialize(Landroid/content/Context;)V

    .line 73
    :cond_0
    return-void
.end method
