.class public abstract Lgbis/gbandroid/widget/WidgetConfiguration;
.super Lgbis/gbandroid/activities/base/GBPreferenceActivity;
.source "GBFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgbis/gbandroid/widget/WidgetConfiguration$a;
    }
.end annotation


# static fields
.field public static final LOCATION_CITYZIP:Ljava/lang/String; = "LocationCityZip_"

.field public static final LOCATION_DISPLAYED:Ljava/lang/String; = "LocationDisplayed_"

.field public static final LOCATION_FAVORITE:Ljava/lang/String; = "LocationFavorite_"

.field public static final LOCATION_LATITUDE:Ljava/lang/String; = "LocationLatitude_"

.field public static final LOCATION_LONGITUDE:Ljava/lang/String; = "LocationLongitude_"

.field protected static final PLACES:I = 0x1


# instance fields
.field private a:Landroid/widget/AutoCompleteTextView;

.field protected mAppWidgetId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lgbis/gbandroid/widget/WidgetConfiguration;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method private a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lgbis/gbandroid/entities/AutoCompMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v0, Lgbis/gbandroid/widget/i;

    invoke-direct {v0, p0}, Lgbis/gbandroid/widget/i;-><init>(Lgbis/gbandroid/widget/WidgetConfiguration;)V

    invoke-virtual {v0}, Lgbis/gbandroid/widget/i;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 144
    new-instance v1, Lgbis/gbandroid/queries/AutoCompleteCityQuery;

    iget-object v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/reflect/Type;Landroid/location/Location;)V

    .line 145
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgbis/gbandroid/queries/AutoCompleteCityQuery;->getResponseObject(Ljava/lang/String;)Lgbis/gbandroid/entities/ResponseMessage;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lgbis/gbandroid/entities/ResponseMessage;->getPayload()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 147
    return-object v0
.end method

.method static synthetic b(Lgbis/gbandroid/widget/WidgetConfiguration;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 152
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lgbis/gbandroid/widget/j;

    invoke-direct {v1, p0}, Lgbis/gbandroid/widget/j;-><init>(Lgbis/gbandroid/widget/WidgetConfiguration;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 174
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f030052

    invoke-direct {v1, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 175
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    return-void
.end method


# virtual methods
.method protected abstract locationChosen(Ljava/lang/String;)V
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 65
    packed-switch p1, :pswitch_data_0

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 67
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 68
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 69
    if-eqz v0, :cond_0

    .line 70
    const-string v1, "place name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    const-string v2, "city"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 72
    if-nez v2, :cond_1

    .line 73
    const-string v2, ""

    .line 74
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "center latitude"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 75
    const-string v4, "center longitude"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 74
    invoke-virtual {p0, v3}, Lgbis/gbandroid/widget/WidgetConfiguration;->showMessage(Ljava/lang/String;)V

    .line 76
    const-string v3, "center latitude"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    .line 77
    const-string v5, "center longitude"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v5

    move-object v0, p0

    .line 76
    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/widget/WidgetConfiguration;->storeLocation(Ljava/lang/String;Ljava/lang/String;DD)V

    .line 78
    invoke-virtual {p0, v1}, Lgbis/gbandroid/widget/WidgetConfiguration;->locationChosen(Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-super {p0, p1}, Lgbis/gbandroid/activities/base/GBPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    .line 55
    :cond_0
    iget v1, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    if-nez v1, :cond_1

    .line 56
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    .line 57
    iget v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    if-nez v0, :cond_1

    .line 58
    invoke-virtual {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->finish()V

    .line 60
    :cond_1
    return-void
.end method

.method protected showOtherLocation(Lgbis/gbandroid/listeners/WidgetSettingsListener;)V
    .locals 4
    .parameter

    .prologue
    .line 118
    new-instance v2, Lgbis/gbandroid/views/CustomDialog$Builder;

    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mTracker:Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;

    invoke-direct {v2, p0, v0}, Lgbis/gbandroid/views/CustomDialog$Builder;-><init>(Landroid/content/Context;Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;)V

    .line 119
    iget-object v0, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030011

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 120
    const v0, 0x7f07003e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 121
    const v1, 0x7f07003f

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->a:Landroid/widget/AutoCompleteTextView;

    .line 122
    const v1, 0x7f090104

    invoke-virtual {p0, v1}, Lgbis/gbandroid/widget/WidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setTitle(Ljava/lang/String;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 123
    invoke-virtual {v2, v3}, Lgbis/gbandroid/views/CustomDialog$Builder;->setContentView(Landroid/view/View;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 124
    const v1, 0x7f090102

    invoke-virtual {p0, v1}, Lgbis/gbandroid/widget/WidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    invoke-direct {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->b()V

    .line 126
    const v0, 0x7f090198

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgbis/gbandroid/widget/h;

    invoke-direct {v1, p0, p1}, Lgbis/gbandroid/widget/h;-><init>(Lgbis/gbandroid/widget/WidgetConfiguration;Lgbis/gbandroid/listeners/WidgetSettingsListener;)V

    invoke-virtual {v2, v0, v1}, Lgbis/gbandroid/views/CustomDialog$Builder;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lgbis/gbandroid/views/CustomDialog$Builder;

    .line 137
    invoke-virtual {v2}, Lgbis/gbandroid/views/CustomDialog$Builder;->create()Lgbis/gbandroid/views/CustomDialog;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 139
    return-void
.end method

.method protected showPlaces()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lgbis/gbandroid/activities/favorites/PlacesList;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    const-string v1, "WidgetCall"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    invoke-virtual {p0, v0, v2}, Lgbis/gbandroid/widget/WidgetConfiguration;->startActivityForResult(Landroid/content/Intent;I)V

    .line 91
    return-void
.end method

.method protected storeLocation(DD)V
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 98
    const v0, 0x7f090107

    invoke-virtual {p0, v0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    move-object v0, p0

    move-wide v3, p1

    move-wide v5, p3

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/widget/WidgetConfiguration;->storeLocation(Ljava/lang/String;Ljava/lang/String;DD)V

    .line 99
    return-void
.end method

.method protected storeLocation(Ljava/lang/String;)V
    .locals 7
    .parameter

    .prologue
    const-wide/16 v3, 0x0

    .line 94
    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move-wide v5, v3

    invoke-virtual/range {v0 .. v6}, Lgbis/gbandroid/widget/WidgetConfiguration;->storeLocation(Ljava/lang/String;Ljava/lang/String;DD)V

    .line 95
    return-void
.end method

.method protected storeLocation(Ljava/lang/String;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 111
    invoke-virtual {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationDisplayed_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationFavorite_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 115
    return-void
.end method

.method protected storeLocation(Ljava/lang/String;Ljava/lang/String;DD)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide v4, 0x412e848000000000L

    .line 102
    invoke-virtual {p0}, Lgbis/gbandroid/widget/WidgetConfiguration;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationDisplayed_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationCityZip_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationLatitude_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    mul-double v2, p3, v4

    double-to-int v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationLongitude_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lgbis/gbandroid/widget/WidgetConfiguration;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    mul-double v2, p5, v4

    double-to-int v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 107
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 108
    return-void
.end method
