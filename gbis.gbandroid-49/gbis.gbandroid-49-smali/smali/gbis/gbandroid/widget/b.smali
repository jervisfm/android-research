.class final Lgbis/gbandroid/widget/b;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

.field private final synthetic b:I

.field private final synthetic c:F

.field private final synthetic d:Landroid/widget/TextView;

.field private final synthetic e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lgbis/gbandroid/widget/ListWidgetConfiguration;FLandroid/widget/TextView;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/widget/b;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    const/4 v0, 0x5

    iput v0, p0, Lgbis/gbandroid/widget/b;->b:I

    iput p2, p0, Lgbis/gbandroid/widget/b;->c:F

    iput-object p3, p0, Lgbis/gbandroid/widget/b;->d:Landroid/widget/TextView;

    iput-object p4, p0, Lgbis/gbandroid/widget/b;->e:Ljava/lang/String;

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lgbis/gbandroid/widget/b;->a:Lgbis/gbandroid/widget/ListWidgetConfiguration;

    iget v1, p0, Lgbis/gbandroid/widget/b;->b:I

    add-int/2addr v1, p2

    int-to-float v1, v1

    iget v2, p0, Lgbis/gbandroid/widget/b;->c:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Lgbis/gbandroid/widget/ListWidgetConfiguration;->a(Lgbis/gbandroid/widget/ListWidgetConfiguration;I)V

    .line 174
    iget-object v0, p0, Lgbis/gbandroid/widget/b;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lgbis/gbandroid/widget/b;->b:I

    add-int/2addr v2, p2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lgbis/gbandroid/widget/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .parameter

    .prologue
    .line 178
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .parameter

    .prologue
    .line 181
    return-void
.end method
