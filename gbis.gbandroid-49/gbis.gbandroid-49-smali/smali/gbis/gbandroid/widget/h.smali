.class final Lgbis/gbandroid/widget/h;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lgbis/gbandroid/widget/WidgetConfiguration;

.field private final synthetic b:Lgbis/gbandroid/listeners/WidgetSettingsListener;


# direct methods
.method constructor <init>(Lgbis/gbandroid/widget/WidgetConfiguration;Lgbis/gbandroid/listeners/WidgetSettingsListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/widget/h;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    iput-object p2, p0, Lgbis/gbandroid/widget/h;->b:Lgbis/gbandroid/listeners/WidgetSettingsListener;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lgbis/gbandroid/widget/h;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/WidgetConfiguration;->a(Lgbis/gbandroid/widget/WidgetConfiguration;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    iget-object v1, p0, Lgbis/gbandroid/widget/h;->b:Lgbis/gbandroid/listeners/WidgetSettingsListener;

    invoke-interface {v1, v0}, Lgbis/gbandroid/listeners/WidgetSettingsListener;->otherLocationEntered(Ljava/lang/String;)V

    .line 132
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 135
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lgbis/gbandroid/widget/h;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    iget-object v1, p0, Lgbis/gbandroid/widget/h;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    const v2, 0x7f09001d

    invoke-virtual {v1, v2}, Lgbis/gbandroid/widget/WidgetConfiguration;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/WidgetConfiguration;->showMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
