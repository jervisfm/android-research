.class final Lgbis/gbandroid/widget/j;
.super Ljava/lang/Object;
.source "GBFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgbis/gbandroid/widget/WidgetConfiguration;


# direct methods
.method constructor <init>(Lgbis/gbandroid/widget/WidgetConfiguration;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lgbis/gbandroid/widget/j;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter

    .prologue
    .line 155
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 162
    const-string v0, ".*\\p{Digit}.*"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 165
    new-instance v0, Lgbis/gbandroid/widget/WidgetConfiguration$a;

    iget-object v1, p0, Lgbis/gbandroid/widget/j;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    iget-object v2, p0, Lgbis/gbandroid/widget/j;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    invoke-direct {v0, v1, v2}, Lgbis/gbandroid/widget/WidgetConfiguration$a;-><init>(Lgbis/gbandroid/widget/WidgetConfiguration;Lgbis/gbandroid/activities/base/GBPreferenceActivity;)V

    .line 166
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lgbis/gbandroid/widget/WidgetConfiguration$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 169
    iget-object v0, p0, Lgbis/gbandroid/widget/j;->a:Lgbis/gbandroid/widget/WidgetConfiguration;

    invoke-static {v0}, Lgbis/gbandroid/widget/WidgetConfiguration;->a(Lgbis/gbandroid/widget/WidgetConfiguration;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    goto :goto_0
.end method
