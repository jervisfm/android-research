Note
=====
Original APK contained third-party Apache Commons Code that caused decompilation to fail. I removed it and it decompiled OK. This simplified version is in the classes_dex2jar_simplified.src.zip file and in the "logos.quiz.companies.game-java-simplified" folder. 