.class public abstract Lcom/flurry/android/AdNetworkView;
.super Lcom/flurry/android/p;
.source "SourceFile"


# static fields
.field public static sAdNetworkApiKey:Ljava/lang/String;


# instance fields
.field public fAdCreative:Lcom/flurry/android/AdCreative;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/flurry/android/AdCreative;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/flurry/android/p;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p2, p0, Lcom/flurry/android/AdNetworkView;->fAdCreative:Lcom/flurry/android/AdCreative;

    .line 22
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/flurry/android/p;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V

    .line 15
    iput-object p4, p0, Lcom/flurry/android/AdNetworkView;->fAdCreative:Lcom/flurry/android/AdCreative;

    .line 16
    return-void
.end method


# virtual methods
.method public onAdClicked(Ljava/util/Map;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    const-string v0, "clicked"

    const/4 v1, 0x1

    invoke-super {p0, v0, v1, p1}, Lcom/flurry/android/p;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 42
    return-void
.end method

.method public onAdClosed(Ljava/util/Map;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    const-string v0, "adClosed"

    const/4 v1, 0x1

    invoke-super {p0, v0, v1, p1}, Lcom/flurry/android/p;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 47
    return-void
.end method

.method public onAdFilled(Ljava/util/Map;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    const-string v0, "filled"

    const/4 v1, 0x1

    invoke-super {p0, v0, v1, p1}, Lcom/flurry/android/p;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 27
    return-void
.end method

.method public onAdShown(Ljava/util/Map;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    const-string v0, "rendered"

    const/4 v1, 0x1

    invoke-super {p0, v0, v1, p1}, Lcom/flurry/android/p;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 37
    return-void
.end method

.method public onAdUnFilled(Ljava/util/Map;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    const-string v0, "unfilled"

    const/4 v1, 0x1

    invoke-super {p0, v0, v1, p1}, Lcom/flurry/android/p;->a(Ljava/lang/String;ZLjava/util/Map;)V

    .line 32
    return-void
.end method
