.class public final Lcom/flurry/android/FlurryFullscreenTakeoverActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/flurry/android/ak;

.field private c:Lcom/flurry/android/ax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 65
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 22
    const v0, 0x1030007

    invoke-virtual {p0, v0}, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->setTheme(I)V

    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 27
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 28
    if-nez v0, :cond_0

    .line 30
    invoke-static {}, Lcom/flurry/android/FlurryAgent;->a()Lcom/flurry/android/bf;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/flurry/android/ak;

    invoke-virtual {v0}, Lcom/flurry/android/bf;->n()Lcom/flurry/android/bc;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flurry/android/bf;->o()Lcom/flurry/android/AdUnit;

    move-result-object v3

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/flurry/android/ak;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;)V

    iput-object v1, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->b:Lcom/flurry/android/ak;

    .line 32
    iget-object v0, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->b:Lcom/flurry/android/ak;

    invoke-virtual {v0, p0}, Lcom/flurry/android/ak;->initLayout(Landroid/content/Context;)V

    .line 33
    iget-object v0, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->b:Lcom/flurry/android/ak;

    invoke-virtual {p0, v0}, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->setContentView(Landroid/view/View;)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    new-instance v1, Lcom/flurry/android/ax;

    invoke-direct {v1, p0, v0}, Lcom/flurry/android/ax;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->c:Lcom/flurry/android/ax;

    .line 38
    iget-object v0, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->c:Lcom/flurry/android/ax;

    invoke-virtual {p0, v0}, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->setContentView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final onDestroy()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 89
    return-void
.end method

.method protected final onPause()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 83
    return-void
.end method

.method protected final onRestart()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 71
    return-void
.end method

.method protected final onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 77
    return-void
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 47
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 54
    iget-object v0, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->b:Lcom/flurry/android/ak;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;->b:Lcom/flurry/android/ak;

    invoke-virtual {v0}, Lcom/flurry/android/ak;->a()V

    .line 58
    :cond_0
    return-void
.end method
