.class public Lcom/flurry/android/GetAdAsyncTask;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/flurry/android/AdUnit;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Landroid/view/ViewGroup;

.field private e:Lcom/flurry/android/be;

.field private f:Lcom/flurry/android/bf;

.field private g:Lcom/flurry/android/u;

.field private h:Lcom/flurry/android/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/ViewGroup;Lcom/flurry/android/bf;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 12
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->a:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    .line 30
    iget-object v0, p4, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    iput-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    .line 31
    iput-object p4, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    .line 32
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/flurry/android/AdUnit;
    .locals 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 70
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(Landroid/content/Context;I)I

    move-result v2

    .line 73
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(Landroid/content/Context;I)I

    move-result v3

    .line 76
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-object v0

    .line 83
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Making ad request:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",refresh:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZJ)V

    .line 87
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    .line 88
    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 10
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/flurry/android/GetAdAsyncTask;->doInBackground([Ljava/lang/Void;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/flurry/android/AdUnit;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 106
    if-nez p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Lcom/flurry/android/AdUnit;)Lcom/flurry/android/r;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v1}, Lcom/flurry/android/r;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 115
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0}, Lcom/flurry/android/u;->removeAllViews()V

    .line 117
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    invoke-virtual {v0}, Lcom/flurry/android/bf;->e()Lcom/flurry/android/IListener;

    move-result-object v2

    .line 118
    invoke-virtual {v1}, Lcom/flurry/android/r;->a()Lcom/flurry/android/AdUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "takeover"

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    .line 119
    :goto_1
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/flurry/android/IListener;->shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0}, Lcom/flurry/android/u;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_3

    .line 123
    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v0

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0, v2, v3}, Lcom/flurry/android/p;->a(Landroid/view/ViewGroup;Lcom/flurry/android/u;)V

    .line 125
    :cond_3
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/u;->addView(Landroid/view/View;)V

    .line 126
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0, v4}, Lcom/flurry/android/u;->a(I)V

    .line 129
    :cond_4
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 131
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    if-eqz v0, :cond_5

    .line 133
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    invoke-virtual {v0, v1, v2}, Lcom/flurry/android/be;->a(Landroid/content/Context;Lcom/flurry/android/u;)V

    .line 134
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 136
    :cond_5
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 139
    :cond_6
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0}, Lcom/flurry/android/u;->a()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0}, Lcom/flurry/android/u;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0, v1}, Lcom/flurry/android/be;->a(Lcom/flurry/android/u;)V

    goto/16 :goto_0

    .line 118
    :cond_7
    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_BANNER:Lcom/flurry/android/FlurryAdType;

    goto :goto_1

    .line 144
    :cond_8
    invoke-virtual {v1}, Lcom/flurry/android/r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    invoke-virtual {v0}, Lcom/flurry/android/bf;->e()Lcom/flurry/android/IListener;

    move-result-object v2

    .line 147
    invoke-virtual {v1}, Lcom/flurry/android/r;->a()Lcom/flurry/android/AdUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    sget-object v0, Lcom/flurry/android/FlurryAdType;->VIDEO_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    .line 148
    :goto_2
    if-eqz v2, :cond_9

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lcom/flurry/android/IListener;->shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    :cond_9
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 151
    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    const-class v2, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 152
    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 147
    :cond_a
    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 10
    check-cast p1, Lcom/flurry/android/AdUnit;

    invoke-virtual {p0, p1}, Lcom/flurry/android/GetAdAsyncTask;->onPostExecute(Lcom/flurry/android/AdUnit;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 41
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/flurry/android/be;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)Lcom/flurry/android/u;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    .line 45
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->f:Lcom/flurry/android/bf;

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/flurry/android/be;->a(Lcom/flurry/android/bf;Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)Lcom/flurry/android/u;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    .line 48
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->g:Lcom/flurry/android/u;

    invoke-virtual {v0, v1, v2}, Lcom/flurry/android/be;->b(Landroid/content/Context;Lcom/flurry/android/u;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/flurry/android/be;->a(Landroid/view/ViewGroup;)Lcom/flurry/android/u;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    .line 55
    iget-object v0, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v1, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->d:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/flurry/android/be;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/u;

    .line 58
    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->h:Lcom/flurry/android/u;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    iget-object v2, p0, Lcom/flurry/android/GetAdAsyncTask;->e:Lcom/flurry/android/be;

    iget-object v3, p0, Lcom/flurry/android/GetAdAsyncTask;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/flurry/android/be;->a(Landroid/content/Context;Lcom/flurry/android/u;)V

    .line 61
    invoke-virtual {v0}, Lcom/flurry/android/u;->c()Landroid/view/ViewGroup;

    move-result-object v2

    .line 62
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 65
    :cond_2
    return-void
.end method
