.class public interface abstract Lcom/flurry/android/IListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAdClosed(Ljava/lang/String;)V
.end method

.method public abstract onApplicationExit(Ljava/lang/String;)V
.end method

.method public abstract onRenderFailed(Ljava/lang/String;)V
.end method

.method public abstract onReward(Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z
.end method
