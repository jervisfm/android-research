.class final Lcom/flurry/android/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/inmobi/androidsdk/IMAdListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/ab;


# direct methods
.method constructor <init>(Lcom/flurry/android/ab;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/flurry/android/ac;->a:Lcom/flurry/android/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdRequestCompleted(Lcom/inmobi/androidsdk/IMAdView;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Lcom/flurry/android/ac;->a:Lcom/flurry/android/ab;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdFilled(Ljava/util/Map;)V

    .line 122
    iget-object v0, p0, Lcom/flurry/android/ac;->a:Lcom/flurry/android/ab;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdShown(Ljava/util/Map;)V

    .line 123
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi imAdView ad request completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    return-void
.end method

.method public final onAdRequestFailed(Lcom/inmobi/androidsdk/IMAdView;Lcom/inmobi/androidsdk/IMAdRequest$ErrorCode;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/flurry/android/ac;->a:Lcom/flurry/android/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdUnFilled(Ljava/util/Map;)V

    .line 115
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi imAdView ad request failed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    return-void
.end method

.method public final onDismissAdScreen(Lcom/inmobi/androidsdk/IMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 108
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi imAdView dismiss ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method public final onShowAdScreen(Lcom/inmobi/androidsdk/IMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/flurry/android/ac;->a:Lcom/flurry/android/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdClicked(Ljava/util/Map;)V

    .line 102
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi imAdView ad shown."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    return-void
.end method
