.class final Lcom/flurry/android/af;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private synthetic d:Lcom/flurry/android/bf;


# direct methods
.method public constructor <init>(Lcom/flurry/android/bf;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1872
    iput-object p1, p0, Lcom/flurry/android/af;->d:Lcom/flurry/android/bf;

    .line 1873
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1866
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/af;->a:Ljava/lang/String;

    .line 1874
    iput-object p2, p0, Lcom/flurry/android/af;->b:Landroid/content/Context;

    .line 1875
    iput-object p3, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    .line 1876
    return-void
.end method

.method private varargs a()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1882
    const/4 v0, 0x0

    move v1, v2

    .line 1886
    :goto_0
    const/4 v3, 0x5

    if-ge v1, v3, :cond_0

    .line 1890
    iget-object v3, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1892
    iget-object v3, p0, Lcom/flurry/android/af;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/flurry/android/bf;->c(Landroid/content/Context;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1895
    iget-object v3, p0, Lcom/flurry/android/af;->d:Lcom/flurry/android/bf;

    invoke-static {v3}, Lcom/flurry/android/bf;->c(Lcom/flurry/android/bf;)Lcom/flurry/android/ay;

    move-result-object v3

    iget-object v4, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    const/16 v5, 0x2710

    const/16 v6, 0x3a98

    invoke-static {v3, v4, v5, v6, v2}, Lcom/flurry/android/z;->a(Lcom/flurry/android/ay;Ljava/lang/String;IIZ)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 1896
    if-eqz v3, :cond_2

    .line 1898
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    .line 1899
    const/16 v5, 0xc8

    if-ne v4, v5, :cond_1

    .line 1901
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Redirect URL found for: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1902
    iget-object v0, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    .line 1946
    :cond_0
    :goto_1
    return-object v0

    .line 1905
    :cond_1
    const/16 v5, 0x12c

    if-lt v4, v5, :cond_3

    const/16 v5, 0x190

    if-ge v4, v5, :cond_3

    .line 1907
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NumRedirects: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1908
    const-string v4, "Location"

    invoke-interface {v3, v4}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1910
    const-string v4, "Location"

    invoke-interface {v3, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    .line 1944
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 1915
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad Response status code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1

    .line 1924
    :cond_4
    const-wide/16 v3, 0x64

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1926
    :catch_0
    move-exception v3

    .line 1928
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    goto :goto_2

    .line 1935
    :cond_5
    iget-object v1, p0, Lcom/flurry/android/af;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1, v2, v3}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1937
    iget-object v0, p0, Lcom/flurry/android/af;->c:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1864
    invoke-direct {p0}, Lcom/flurry/android/af;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1864
    return-void
.end method
