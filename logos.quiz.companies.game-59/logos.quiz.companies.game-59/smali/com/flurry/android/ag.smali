.class final Lcom/flurry/android/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/android/MMAdView$MMAdListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/ah;


# direct methods
.method constructor <init>(Lcom/flurry/android/ah;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final MMAdCachingCompleted(Lcom/millennialmedia/android/MMAdView;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 126
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView caching completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void
.end method

.method public final MMAdClickedToNewBrowser(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdClicked(Ljava/util/Map;)V

    .line 101
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView clicked to new browser."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public final MMAdClickedToOverlay(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdClicked(Ljava/util/Map;)V

    .line 108
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView clicked to overlay."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method public final MMAdFailed(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdUnFilled(Ljava/util/Map;)V

    .line 86
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView failed to load ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method public final MMAdOverlayLaunched(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 114
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView overlay launched."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void
.end method

.method public final MMAdRequestIsCaching(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 120
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView request is caching."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    return-void
.end method

.method public final MMAdReturned(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdFilled(Ljava/util/Map;)V

    .line 93
    iget-object v0, p0, Lcom/flurry/android/ag;->a:Lcom/flurry/android/ah;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdShown(Ljava/util/Map;)V

    .line 94
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial MMAdView returned ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-void
.end method
