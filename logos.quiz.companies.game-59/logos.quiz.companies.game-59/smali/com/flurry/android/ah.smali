.class final Lcom/flurry/android/ah;
.super Lcom/flurry/android/AdNetworkView;
.source "SourceFile"


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flurry/android/AdNetworkView;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 24
    const/4 v0, 0x0

    .line 26
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 30
    :goto_0
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 31
    const-string v1, "com.flurry.millennial.MYAPID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    sput-object v0, Lcom/flurry/android/ah;->sAdNetworkApiKey:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 34
    const-string v0, "FlurryAgent"

    const-string v1, "com.flurry.millennial.MYAPID not set in manifest"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/flurry/android/ah;->setFocusable(Z)V

    .line 37
    return-void

    .line 28
    :catch_0
    move-exception v1

    const-string v1, "FlurryAgent"

    const-string v2, "Cannot find manifest for app"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final initLayout(Landroid/content/Context;)V
    .locals 7
    .parameter

    .prologue
    const/4 v5, 0x0

    const v6, 0x711e41a1

    .line 211
    iget-object v0, p0, Lcom/flurry/android/ah;->fAdCreative:Lcom/flurry/android/AdCreative;

    invoke-virtual {v0}, Lcom/flurry/android/AdCreative;->getFormat()Ljava/lang/String;

    move-result-object v0

    const-string v1, "takeover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    new-instance v0, Lcom/millennialmedia/android/MMAdView;

    move-object v1, p1

    check-cast v1, Landroid/app/Activity;

    sget-object v2, Lcom/flurry/android/ah;->sAdNetworkApiKey:Ljava/lang/String;

    const-string v3, "MMFullScreenAdTransition"

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/android/MMAdView;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;)V

    invoke-virtual {v0, v6}, Lcom/millennialmedia/android/MMAdView;->setId(I)V

    new-instance v1, Lcom/flurry/android/at;

    invoke-direct {v1, p0}, Lcom/flurry/android/at;-><init>(Lcom/flurry/android/ah;)V

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MMAdView;->setListener(Lcom/millennialmedia/android/MMAdView$MMAdListener;)V

    invoke-virtual {v0}, Lcom/millennialmedia/android/MMAdView;->fetch()V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/ah;->fAdCreative:Lcom/flurry/android/AdCreative;

    invoke-virtual {v0}, Lcom/flurry/android/AdCreative;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/flurry/android/ah;->fAdCreative:Lcom/flurry/android/AdCreative;

    invoke-virtual {v1}, Lcom/flurry/android/AdCreative;->getWidth()I

    move-result v1

    const/16 v2, 0x140

    if-lt v1, v2, :cond_1

    const/16 v2, 0x32

    if-lt v0, v2, :cond_1

    const-string v0, "FlurryAgent"

    const-string v1, "Determined Millennial AdSize as MMBannerAdBottom"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "MMBannerAdBottom"

    :goto_1
    if-eqz v5, :cond_3

    new-instance v0, Lcom/millennialmedia/android/MMAdView;

    check-cast p1, Landroid/app/Activity;

    sget-object v1, Lcom/flurry/android/ah;->sAdNetworkApiKey:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v5, v2}, Lcom/millennialmedia/android/MMAdView;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v6}, Lcom/millennialmedia/android/MMAdView;->setId(I)V

    new-instance v1, Lcom/flurry/android/ag;

    invoke-direct {v1, p0}, Lcom/flurry/android/ag;-><init>(Lcom/flurry/android/ah;)V

    invoke-virtual {v0, v1}, Lcom/millennialmedia/android/MMAdView;->setListener(Lcom/millennialmedia/android/MMAdView$MMAdListener;)V

    invoke-virtual {p0, v0}, Lcom/flurry/android/ah;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x12c

    if-lt v1, v2, :cond_2

    const/16 v1, 0xfa

    if-lt v0, v1, :cond_2

    const-string v0, "FlurryAgent"

    const-string v1, "Determined Millennial AdSize as MMBannerAdRectangle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "MMBannerAdRectangle"

    goto :goto_1

    :cond_2
    const-string v0, "FlurryAgent"

    const-string v1, "Could not find Millennial AdSize that matches size"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v0, "FlurryAgent"

    const-string v1, "**********Could not load Millennial Ad"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
