.class final Lcom/flurry/android/ak;
.super Lcom/flurry/android/p;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Lcom/flurry/android/aj;


# instance fields
.field private final e:Ljava/lang/String;

.field private f:Landroid/app/ProgressDialog;

.field private g:Lcom/flurry/android/au;

.field private h:Landroid/webkit/WebView;

.field private i:I

.field private j:Lcom/flurry/android/AdUnit;

.field private k:I

.field private l:Lcom/flurry/android/bc;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/AdFrame;",
            ">;"
        }
    .end annotation
.end field

.field private n:Z

.field private o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/flurry/android/AdUnit;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/flurry/android/bc;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/content/Context;

.field private r:Z

.field private s:Lcom/flurry/android/bf;

.field private t:Lcom/flurry/android/x;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 154
    invoke-direct {p0, p1, p2, p3}, Lcom/flurry/android/p;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V

    .line 34
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/flurry/android/ak;->e:Ljava/lang/String;

    .line 155
    iput-object p1, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    .line 157
    iput-object p4, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    .line 158
    iput v1, p0, Lcom/flurry/android/ak;->k:I

    .line 159
    iput-object p3, p0, Lcom/flurry/android/ak;->l:Lcom/flurry/android/bc;

    .line 160
    iget-object v2, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v2}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    .line 162
    iput-boolean v1, p0, Lcom/flurry/android/ak;->r:Z

    .line 163
    iget-object v2, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v2}, Lcom/flurry/android/AdUnit;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/flurry/android/ak;->n:Z

    .line 165
    iget-boolean v0, p0, Lcom/flurry/android/ak;->n:Z

    if-eqz v0, :cond_0

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/ak;->o:Ljava/util/Map;

    .line 169
    iget-object v0, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    invoke-virtual {p3}, Lcom/flurry/android/bc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v2, p0, Lcom/flurry/android/ak;->o:Ljava/util/Map;

    invoke-virtual {p4}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    :cond_0
    iput-object p2, p0, Lcom/flurry/android/ak;->s:Lcom/flurry/android/bf;

    .line 174
    iget-object v0, p0, Lcom/flurry/android/ak;->s:Lcom/flurry/android/bf;

    iget-object v0, v0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    iput-object v0, p0, Lcom/flurry/android/ak;->t:Lcom/flurry/android/x;

    .line 175
    iget-object v0, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    iput-object v0, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    .line 176
    iget-object v0, p0, Lcom/flurry/android/ak;->l:Lcom/flurry/android/bc;

    iput-object v0, p0, Lcom/flurry/android/p;->b:Lcom/flurry/android/bc;

    .line 177
    return-void

    :cond_1
    move v0, v1

    .line 163
    goto :goto_0
.end method

.method static synthetic a(Lcom/flurry/android/ak;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/AdUnit;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 592
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 593
    const-string v0, "\'{\"adComponents\":["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 595
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdUnit;

    .line 599
    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 605
    :cond_1
    const-string v0, "]}\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(II)Ljava/util/List;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/AdUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611
    iget-object v0, p0, Lcom/flurry/android/ak;->t:Lcom/flurry/android/x;

    iget-object v1, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v1}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/flurry/android/x;->a(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v2

    .line 612
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdUnit;

    .line 614
    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 616
    iget-object v4, p0, Lcom/flurry/android/ak;->o:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flurry/android/AdFrame;

    invoke-virtual {v1}, Lcom/flurry/android/AdFrame;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 619
    :cond_1
    return-object v2
.end method

.method static synthetic a(Lcom/flurry/android/ak;Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 32
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    if-eqz p1, :cond_1

    const-string v0, "&"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v6, v5, v1

    const-string v7, "event"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    aget-object v6, v5, v1

    const/4 v7, 0x1

    aget-object v5, v5, v7

    invoke-interface {v2, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method static synthetic a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 512
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fireEvent(event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 513
    iget-object v6, p0, Lcom/flurry/android/ak;->a:Lcom/flurry/android/bf;

    new-instance v0, Lcom/flurry/android/bb;

    iget-object v2, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    iget-object v3, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    iget-object v4, p0, Lcom/flurry/android/ak;->l:Lcom/flurry/android/bc;

    iget v5, p0, Lcom/flurry/android/ak;->k:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/bb;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    invoke-virtual {v6, v0, p0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bb;Lcom/flurry/android/aj;)V

    .line 514
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/flurry/android/AdUnit;",
            "Lcom/flurry/android/bc;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 518
    iget-object v6, p0, Lcom/flurry/android/ak;->a:Lcom/flurry/android/bf;

    new-instance v0, Lcom/flurry/android/bb;

    iget-object v2, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/bb;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    invoke-virtual {v6, v0, p0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bb;Lcom/flurry/android/aj;)V

    .line 519
    return-void
.end method

.method static synthetic b(Lcom/flurry/android/ak;Ljava/lang/String;)Lcom/flurry/android/AdUnit;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->o:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/flurry/android/ak;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdUnit;

    goto :goto_0
.end method

.method static synthetic b(Lcom/flurry/android/ak;)Z
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/flurry/android/ak;->n:Z

    return v0
.end method

.method static synthetic c(Lcom/flurry/android/ak;)Lcom/flurry/android/AdUnit;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    return-object v0
.end method

.method static synthetic c(Lcom/flurry/android/ak;Ljava/lang/String;)Lcom/flurry/android/bc;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/bc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/flurry/android/ak;->s:Lcom/flurry/android/bf;

    invoke-static {v0, p1}, Lcom/flurry/android/z;->a(Lcom/flurry/android/bf;Ljava/lang/String;)Lcom/flurry/android/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic d(Lcom/flurry/android/ak;)Lcom/flurry/android/bc;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->l:Lcom/flurry/android/bc;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/flurry/android/ak;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "takeover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 197
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flurry/android/ak;->r:Z

    .line 199
    const-string v0, "adClosed"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "caught class cast exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/flurry/android/ak;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_0

    .line 211
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    .line 215
    :try_start_1
    check-cast v0, Landroid/view/ViewGroup;

    .line 216
    iget-object v1, p0, Lcom/flurry/android/ak;->s:Lcom/flurry/android/bf;

    iget-object v2, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    iget-object v3, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v3}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Landroid/view/ViewGroup;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 219
    :catch_1
    move-exception v0

    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to remove view from holder: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method private e()I
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/flurry/android/ak;)I
    .locals 1
    .parameter

    .prologue
    .line 32
    iget v0, p0, Lcom/flurry/android/ak;->k:I

    return v0
.end method

.method static synthetic f(Lcom/flurry/android/ak;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->b()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/flurry/android/AdFrame;
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 499
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0}, Lcom/flurry/android/au;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0}, Lcom/flurry/android/au;->stopPlayback()V

    .line 508
    :cond_1
    return-void
.end method

.method public final a(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 312
    iget-object v0, p1, Lcom/flurry/android/v;->a:Ljava/lang/String;

    .line 313
    iget-object v3, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    .line 314
    iget-object v4, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    .line 316
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "performAction("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "...)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 318
    iget-boolean v6, p0, Lcom/flurry/android/ak;->r:Z

    if-eqz v6, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    const-string v6, "nextAdUnit"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 327
    :try_start_0
    invoke-virtual {p0}, Lcom/flurry/android/ak;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/u;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    if-eqz v0, :cond_0

    .line 333
    :try_start_1
    iget-object v1, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v2, "delay"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    .line 337
    :goto_1
    mul-int/lit16 v1, v5, 0x3e8

    :try_start_2
    invoke-virtual {v0, v1}, Lcom/flurry/android/u;->a(I)V

    .line 340
    iget-object v1, p0, Lcom/flurry/android/ak;->a:Lcom/flurry/android/bf;

    iget-object v1, v1, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v1, v0}, Lcom/flurry/android/be;->a(Lcom/flurry/android/u;)V
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 343
    :catch_0
    move-exception v0

    .line 345
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->toString()Ljava/lang/String;

    .line 346
    invoke-direct {p0}, Lcom/flurry/android/ak;->d()V

    goto :goto_0

    .line 349
    :cond_2
    const-string v6, "nextFrame"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 351
    iget v0, p0, Lcom/flurry/android/ak;->k:I

    add-int/lit8 v1, v0, 0x1

    .line 352
    const-string v0, "offset"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 353
    if-eqz v0, :cond_5

    .line 355
    const-string v2, "next"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 357
    iget v0, p0, Lcom/flurry/android/ak;->k:I

    add-int/lit8 v0, v0, 0x1

    .line 375
    :goto_2
    iget v1, p0, Lcom/flurry/android/ak;->k:I

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iput v0, p0, Lcom/flurry/android/ak;->k:I

    iget-object v0, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/flurry/android/ak;->initLayout(Landroid/content/Context;)V

    goto :goto_0

    .line 359
    :cond_3
    const-string v2, "current"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 361
    iget v0, p0, Lcom/flurry/android/ak;->k:I

    goto :goto_2

    .line 367
    :cond_4
    :try_start_3
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v0

    goto :goto_2

    .line 369
    :catch_1
    move-exception v0

    .line 371
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "caught: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_5
    move v0, v1

    goto :goto_2

    .line 377
    :cond_6
    const-string v6, "closeAd"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 379
    invoke-direct {p0}, Lcom/flurry/android/ak;->d()V

    goto/16 :goto_0

    .line 381
    :cond_7
    const-string v6, "notifyUser"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 383
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 384
    const-string v0, "message"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "message"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 385
    :goto_3
    const-string v0, "confirmDisplay"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "confirmDisplay"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 386
    :goto_4
    const-string v0, "cancelDisplay"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "cancelDisplay"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 388
    :goto_5
    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v4, Lcom/flurry/android/ap;

    invoke-direct {v4, p0, v3}, Lcom/flurry/android/ap;-><init>(Lcom/flurry/android/ak;Lcom/flurry/android/bb;)V

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/flurry/android/am;

    invoke-direct {v2, p0, v3}, Lcom/flurry/android/am;-><init>(Lcom/flurry/android/ak;Lcom/flurry/android/bb;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 410
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 411
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 384
    :cond_8
    const-string v0, "Are you sure?"

    move-object v1, v0

    goto :goto_3

    .line 385
    :cond_9
    const-string v0, "Yes"

    move-object v2, v0

    goto :goto_4

    .line 386
    :cond_a
    const-string v0, "No"

    goto :goto_5

    .line 413
    :cond_b
    const-string v6, "logEvent"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 415
    const-string v0, "__sendToServer"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "__sendToServer"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    move v5, v1

    .line 416
    :cond_c
    const-string v0, "__sendToServer"

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v0, v3, Lcom/flurry/android/bb;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v5, v4}, Lcom/flurry/android/ak;->a(Ljava/lang/String;ZLjava/util/Map;)V

    goto/16 :goto_0

    .line 421
    :cond_d
    const-string v3, "loadAdComponents"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 424
    const-string v0, "min"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "max"

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 428
    :try_start_4
    const-string v0, "min"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 429
    const-string v0, "max"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_2

    move-result v0

    move v1, v3

    .line 438
    :goto_6
    iget-object v2, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v2}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/flurry/android/ak;->a(II)Ljava/util/List;

    move-result-object v0

    .line 439
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_f

    .line 441
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "converting these units to JSON content:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 442
    iget-object v1, p0, Lcom/flurry/android/ak;->j:Lcom/flurry/android/AdUnit;

    invoke-virtual {v1}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    invoke-static {v0}, Lcom/flurry/android/ak;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 443
    iget-object v2, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "javascript:(function() {var multiadwraps=document.getElementsByClassName(\'multiAdWrap\');if(multiadwraps.length>0){var template=document.getElementsByClassName(\'multiAdWrap\')[0];var compiled=Hogan.compile(template.innerHTML);template.innerHTML=\'\';template.innerHTML=compiled.render(JSON.parse("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "));}})();"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 446
    iget-object v1, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    const-string v2, "javascript:flurryadapter.callComplete();"

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 447
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/flurry/android/AdUnit;

    .line 449
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 450
    const-string v1, "guid"

    invoke-virtual {v3}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    const-string v1, "rendered"

    iget-object v4, p0, Lcom/flurry/android/ak;->p:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/flurry/android/bc;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    goto :goto_7

    .line 433
    :catch_2
    move-exception v0

    move v0, v2

    .line 434
    goto/16 :goto_6

    .line 454
    :cond_e
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {p0, v0}, Lcom/flurry/android/ak;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 458
    :cond_f
    const-string v0, "renderFailed"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 463
    :cond_10
    iget-object v0, p0, Lcom/flurry/android/ak;->a:Lcom/flurry/android/bf;

    invoke-virtual {v0, p1, p2}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V

    goto/16 :goto_0

    :catch_3
    move-exception v1

    goto/16 :goto_1

    :cond_11
    move v0, v2

    goto/16 :goto_6
.end method

.method final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/flurry/android/ak;->m:Ljava/util/List;

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final initLayout(Landroid/content/Context;)V
    .locals 6
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initLayout, current frame index: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/flurry/android/ak;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", binding: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", display: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/flurry/android/ak;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", content: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flurry/android/ak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", format: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/flurry/android/ak;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    invoke-virtual {p0}, Lcom/flurry/android/ak;->removeAllViews()V

    .line 240
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v1

    :try_start_0
    iget-object v0, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v3

    iput v3, p0, Lcom/flurry/android/ak;->i:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    iget v1, p0, Lcom/flurry/android/ak;->i:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_1
    invoke-virtual {p0, v4}, Lcom/flurry/android/ak;->setFocusable(Z)V

    .line 242
    invoke-virtual {p0, v4}, Lcom/flurry/android/ak;->setFocusableInTouchMode(Z)V

    .line 244
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 305
    const-string v0, "renderFailed"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 307
    :goto_2
    return-void

    .line 240
    :pswitch_1
    const/4 v1, 0x4

    goto :goto_0

    :pswitch_2
    move v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->toString()Ljava/lang/String;

    goto :goto_1

    .line 247
    :pswitch_3
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    if-nez v0, :cond_0

    .line 249
    new-instance v0, Lcom/flurry/android/au;

    invoke-direct {v0, p1}, Lcom/flurry/android/au;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    .line 250
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0, p0}, Lcom/flurry/android/au;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 251
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0, p0}, Lcom/flurry/android/au;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 252
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0, p0}, Lcom/flurry/android/au;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 253
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    new-instance v1, Landroid/widget/MediaController;

    invoke-direct {v1, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/flurry/android/au;->setMediaController(Landroid/widget/MediaController;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-direct {p0}, Lcom/flurry/android/ak;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/au;->setVideoURI(Landroid/net/Uri;)V

    .line 257
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {p0, v0}, Lcom/flurry/android/ak;->addView(Landroid/view/View;)V

    .line 259
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    .line 260
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 261
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 263
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_2

    .line 270
    :pswitch_4
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    .line 272
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    .line 273
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    new-instance v1, Lcom/flurry/android/ar;

    invoke-direct {v1, p0}, Lcom/flurry/android/ar;-><init>(Lcom/flurry/android/ak;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 274
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 275
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 276
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 277
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 280
    :cond_1
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 282
    invoke-direct {p0}, Lcom/flurry/android/ak;->f()Ljava/lang/String;

    move-result-object v0

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calling WebView.loadUrl "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 284
    iget-object v1, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 292
    :cond_2
    :goto_3
    invoke-direct {p0}, Lcom/flurry/android/ak;->g()Lcom/flurry/android/AdFrame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 293
    iget-object v1, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/flurry/android/z;->b(Landroid/content/Context;I)I

    move-result v0

    .line 294
    invoke-direct {p0}, Lcom/flurry/android/ak;->g()Lcom/flurry/android/AdFrame;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/AdSpaceLayout;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 295
    iget-object v2, p0, Lcom/flurry/android/ak;->q:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/flurry/android/z;->b(Landroid/content/Context;I)I

    move-result v1

    .line 297
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 298
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 286
    :cond_3
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "calling WebView.loadData: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/flurry/android/ak;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lcom/flurry/android/ak;->h:Landroid/webkit/WebView;

    const-string v1, "base://url/"

    invoke-direct {p0}, Lcom/flurry/android/ak;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    const-string v5, "base://url/"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 240
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 244
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .parameter

    .prologue
    .line 470
    const-string v0, "videoCompleted"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 472
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 490
    const-string v0, "renderFailed"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 492
    const/4 v0, 0x1

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onKeyDown "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 579
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 581
    const-string v0, "adWillClose"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 582
    const/4 v0, 0x1

    .line 586
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flurry/android/p;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .parameter

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/flurry/android/ak;->e()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 480
    iget-object v0, p0, Lcom/flurry/android/ak;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 481
    iget-object v0, p0, Lcom/flurry/android/ak;->g:Lcom/flurry/android/au;

    invoke-virtual {v0}, Lcom/flurry/android/au;->start()V

    .line 482
    const-string v0, "rendered"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 483
    const-string v0, "videoStarted"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/ak;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 485
    :cond_0
    return-void
.end method
