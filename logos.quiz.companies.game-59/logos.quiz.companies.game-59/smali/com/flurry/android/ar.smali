.class final Lcom/flurry/android/ar;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field private synthetic a:Lcom/flurry/android/ak;


# direct methods
.method synthetic constructor <init>(Lcom/flurry/android/ak;)V
    .locals 1
    .parameter

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/flurry/android/ar;-><init>(Lcom/flurry/android/ak;B)V

    return-void
.end method

.method private constructor <init>(Lcom/flurry/android/ak;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageFinished(url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v0}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:var Hogan={};(function(a,b){function i(a){return String(a===null||a===undefined?\"\":a)}function j(a){return a=i(a),h.test(a)?a.replace(c,\"&amp;\").replace(d,\"&lt;\").replace(e,\"&gt;\").replace(f,\"&#39;\").replace(g,\"&quot;\"):a}a.Template=function(a,c,d,e){this.r=a||this.r,this.c=d,this.options=e,this.text=c||\"\",this.buf=b?[]:\"\"},a.Template.prototype={r:function(a,b,c){return\"\"},v:j,t:i,render:function(b,c,d){return this.ri([b],c||{},d)},ri:function(a,b,c){return this.r(a,b,c)},rp:function(a,b,c,d){var e=c[a];return e?(this.c&&typeof e==\"string\"&&(e=this.c.compile(e,this.options)),e.ri(b,c,d)):\"\"},rs:function(a,b,c){var d=a[a.length-1];if(!k(d)){c(a,b,this);return}for(var e=0;e<d.length;e++)a.push(d[e]),c(a,b,this),a.pop()},s:function(a,b,c,d,e,f,g){var h;return k(a)&&a.length===0?!1:(typeof a==\"function\"&&(a=this.ls(a,b,c,d,e,f,g)),h=a===\"\"||!!a,!d&&h&&b&&b.push(typeof a==\"object\"?a:b[b.length-1]),h)},d:function(a,b,c,d){var e=a.split(\".\"),f=this.f(e[0],b,c,d),g=null;if(a===\".\"&&k(b[b.length-2]))return b[b.length-1];for(var h=1;h<e.length;h++)f&&typeof f==\"object\"&&e[h]in f?(g=f,f=f[e[h]]):f=\"\";return d&&!f?!1:(!d&&typeof f==\"function\"&&(b.push(g),f=this.lv(f,b,c),b.pop()),f)},f:function(a,b,c,d){var e=!1,f=null,g=!1;for(var h=b.length-1;h>=0;h--){f=b[h];if(f&&typeof f==\"object\"&&a in f){e=f[a],g=!0;break}}return g?(!d&&typeof e==\"function\"&&(e=this.lv(e,b,c)),e):d?!1:\"\"},ho:function(a,b,c,d,e){var f=this.c,g=this.options;g.delimiters=e;var h=a.call(b,d,function(a){return f.compile(a,g).render(b,c)});return this.b(f.compile(h.toString(),g).render(b,c)),!1},b:b?function(a){this.buf.push(a)}:function(a){this.buf+=a},fl:b?function(){var a=this.buf.join(\"\");return this.buf=[],a}:function(){var a=this.buf;return this.buf=\"\",a},ls:function(a,b,c,d,e,f,g){var h=b[b.length-1],i=null;if(!d&&this.c&&a.length>0)return this.ho(a,h,c,this.text.substring(e,f),g);i=a.call(h);if(typeof i==\"function\"){if(d)return!0;if(this.c)return this.ho(i,h,c,this.text.substring(e,f),g)}return i},lv:function(a,b,c){var d=b[b.length-1],e=a.call(d);return typeof e==\"function\"&&(e=e.call(d)),e=i(e),this.c&&~e.indexOf(\"{{\")?this.c.compile(e,this.options).render(d,c):e}};var c=/&/g,d=/</g,e=/>/g,f=/\\\'/g,g=/\\\"/g,h=/[&<>\\\"\\\']/,k=Array.isArray||function(a){return Object.prototype.toString.call(a)===\"[object Array]\"}})(typeof exports!=\"undefined\"?exports:Hogan),function(a){function h(a){a.n.substr(a.n.length-1)===\"}\"&&(a.n=a.n.substring(0,a.n.length-1))}function i(a){return a.trim?a.trim():a.replace(/^\\s*|\\s*$/g,\"\")}function j(a,b,c){if(b.charAt(c)!=a.charAt(0))return!1;for(var d=1,e=a.length;d<e;d++)if(b.charAt(c+d)!=a.charAt(d))return!1;return!0}function k(a,b,c,d){var e=[],f=null,g=null;while(a.length>0){g=a.shift();if(g.tag==\"#\"||g.tag==\"^\"||l(g,d))c.push(g),g.nodes=k(a,g.tag,c,d),e.push(g);else{if(g.tag==\"/\"){if(c.length===0)throw new Error(\"Closing tag without opener: /\"+g.n);f=c.pop();if(g.n!=f.n&&!m(g.n,f.n,d))throw new Error(\"Nesting error: \"+f.n+\" vs. \"+g.n);return f.end=g.i,e}e.push(g)}}if(c.length>0)throw new Error(\"missing closing tag: \"+c.pop().n);return e}function l(a,b){for(var c=0,d=b.length;c<d;c++)if(b[c].o==a.n)return a.tag=\"#\",!0}function m(a,b,c){for(var d=0,e=c.length;d<e;d++)if(c[d].c==a&&c[d].o==b)return!0}function n(a){return\'var _=this;_.b(i=i||\"\");\'+q(a)+\"return _.fl();\"}function o(a){return a.replace(f,\"\\\\\\\\\").replace(c,\'\\\\\"\').replace(d,\"\\\\n\").replace(e,\"\\\\r\")}function p(a){return~a.indexOf(\".\")?\"d\":\"f\"}function q(a){var b=\"\";for(var c=0,d=a.length;c<d;c++){var e=a[c].tag;e==\"#\"?b+=r(a[c].nodes,a[c].n,p(a[c].n),a[c].i,a[c].end,a[c].otag+\" \"+a[c].ctag):e==\"^\"?b+=s(a[c].nodes,a[c].n,p(a[c].n)):e==\"<\"||e==\">\"?b+=t(a[c]):e==\"{\"||e==\"&\"?b+=u(a[c].n,p(a[c].n)):e==\"\\n\"?b+=w(\'\"\\\\n\"\'+(a.length-1==c?\"\":\" + i\")):e==\"_v\"?b+=v(a[c].n,p(a[c].n)):e===undefined&&(b+=w(\'\"\'+o(a[c])+\'\"\'))}return b}function r(a,b,c,d,e,f){return\"if(_.s(_.\"+c+\'(\"\'+o(b)+\'\",c,p,1),\'+\"c,p,0,\"+d+\",\"+e+\',\"\'+f+\'\")){\'+\"_.rs(c,p,\"+\"function(c,p,_){\"+q(a)+\"});c.pop();}\"}function s(a,b,c){return\"if(!_.s(_.\"+c+\'(\"\'+o(b)+\'\",c,p,1),c,p,1,0,0,\"\")){\'+q(a)+\"};\"}function t(a){return\'_.b(_.rp(\"\'+o(a.n)+\'\",c,p,\"\'+(a.indent||\"\")+\'\"));\'}function u(a,b){return\"_.b(_.t(_.\"+b+\'(\"\'+o(a)+\'\",c,p,0)));\'}function v(a,b){return\"_.b(_.v(_.\"+b+\'(\"\'+o(a)+\'\",c,p,0)));\'}function w(a){return\"_.b(\"+a+\");\"}var b=/\\S/,c=/\\\"/g,d=/\\n/g,e=/\\r/g,f=/\\\\/g,g={\"#\":1,\"^\":2,\"/\":3,\"!\":4,\">\":5,\"<\":6,\"=\":7,_v:8,\"{\":9,\"&\":10};a.scan=function(c,d){function w(){p.length>0&&(q.push(new String(p)),p=\"\")}function x(){var a=!0;for(var c=t;c<q.length;c++){a=q[c].tag&&g[q[c].tag]<g._v||!q[c].tag&&q[c].match(b)===null;if(!a)return!1}return a}function y(a,b){w();if(a&&x())for(var c=t,d;c<q.length;c++)q[c].tag||((d=q[c+1])&&d.tag==\">\"&&(d.indent=q[c].toString()),q.splice(c,1));else b||q.push({tag:\"\\n\"});r=!1,t=q.length}function z(a,b){var c=\"=\"+v,d=a.indexOf(c,b),e=i(a.substring(a.indexOf(\"=\",b)+1,d)).split(\" \");return u=e[0],v=e[1],d+c.length-1}var e=c.length,f=0,k=1,l=2,m=f,n=null,o=null,p=\"\",q=[],r=!1,s=0,t=0,u=\"{{\",v=\"}}\";d&&(d=d.split(\" \"),u=d[0],v=d[1]);for(s=0;s<e;s++)m==f?j(u,c,s)?(--s,w(),m=k):c.charAt(s)==\"\\n\"?y(r):p+=c.charAt(s):m==k?(s+=u.length-1,o=g[c.charAt(s+1)],n=o?c.charAt(s+1):\"_v\",n==\"=\"?(s=z(c,s),m=f):(o&&s++,m=l),r=s):j(v,c,s)?(q.push({tag:n,n:i(p),otag:u,ctag:v,i:n==\"/\"?r-v.length:s+u.length}),p=\"\",s+=v.length-1,m=f,n==\"{\"&&(v==\"}}\"?s++:h(q[q.length-1]))):p+=c.charAt(s);return y(r,!0),q},a.generate=function(b,c,d){return d.asString?\"function(c,p,i){\"+b+\";}\":new a.Template(new Function(\"c\",\"p\",\"i\",b),c,a,d)},a.parse=function(a,b,c){return c=c||{},k(a,\"\",[],c.sectionTags||[])},a.cache={},a.compile=function(a,b){b=b||{};var c=a+\"||\"+!!b.asString,d=this.cache[c];return d?d:(d=this.generate(n(this.parse(this.scan(a,b.delimiters),a,b)),a,b),this.cache[c]=d)}}(typeof exports!=\"undefined\"?exports:Hogan)"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v0}, Lcom/flurry/android/ak;->b(Lcom/flurry/android/ak;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v0}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:(function() {       var flurryadapter = window.flurryadapter = {};       flurryadapter.flurryCallQueue = [ ];       flurryadapter.flurryCallInProgress = false;       flurryadapter.callComplete = function( cmd ) {          if ( this.flurryCallQueue.length == 0 ) {             this.flurryCallInProgress = false;             return;          }           var adapterCall = this.flurryCallQueue.pop();           this.executeNativeCall(adapterCall);          return \"OK\";       };       flurryadapter.executeCall = function( command ) {          var adapterCall = \"flurry://flurrycall?event=\" + command;          var value;          for ( var i = 1; i < arguments.length; i += 2 ) {             value = arguments[i + 1];             if ( value == null ) {                continue;             }             adapterCall += \"&\" + arguments[i] + \"=\" + escape( value );          }          if ( this.flurryCallInProgress ) {             this.flurryCallQueue.push( adapterCall );          }          else {            this.executeNativeCall(adapterCall);          }       };     flurryadapter.executeNativeCall = function( adapterCall ) {         if ( adapterCall.length == 0 ) {            return;         }         this.flurryCallInProgress = true;         window.location = adapterCall;     };    window.flurryAdapterAvailable = true;    if (typeof FlurryAdapterReady == \'function\') {        FlurryAdapterReady();    } })();"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v0}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;)Landroid/webkit/WebView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:var content=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-virtual {v2}, Lcom/flurry/android/ak;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\';var compiled=Hogan.compile(document.body.innerHTML);var rendered=compiled.render(JSON.parse(content));document.body.innerHTML=rendered;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    const-string v1, "rendered"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v3}, Lcom/flurry/android/ak;->c(Lcom/flurry/android/ak;)Lcom/flurry/android/AdUnit;

    move-result-object v3

    iget-object v4, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v4}, Lcom/flurry/android/ak;->d(Lcom/flurry/android/ak;)Lcom/flurry/android/bc;

    move-result-object v4

    iget-object v5, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v5}, Lcom/flurry/android/ak;->e(Lcom/flurry/android/ak;)I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    .line 92
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-virtual {v0}, Lcom/flurry/android/ak;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    iget-object v1, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v1}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/ak;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdUnityWebViewClient.onReceivedError:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 147
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    const-string v1, "renderFailed"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;)V

    .line 148
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "shouldOverrideUrlLoading(url:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "flurry"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108
    const-string v1, "event"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "found flurry:// url, event="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    if-eqz v1, :cond_0

    .line 112
    iget-object v2, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 113
    const-string v0, "guid"

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    iget-object v3, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    const-string v0, "guid"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/flurry/android/ak;->b(Lcom/flurry/android/ak;Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v3

    .line 116
    iget-object v4, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    const-string v0, "guid"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/flurry/android/ak;->c(Lcom/flurry/android/ak;Ljava/lang/String;)Lcom/flurry/android/bc;

    move-result-object v4

    .line 117
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static/range {v0 .. v5}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    :cond_0
    :goto_0
    move v5, v6

    .line 138
    :cond_1
    :goto_1
    return v5

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    iget-object v3, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v3}, Lcom/flurry/android/ak;->c(Lcom/flurry/android/ak;)Lcom/flurry/android/AdUnit;

    move-result-object v3

    iget-object v4, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v4}, Lcom/flurry/android/ak;->d(Lcom/flurry/android/ak;)Lcom/flurry/android/bc;

    move-result-object v4

    iget-object v5, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v5}, Lcom/flurry/android/ak;->e(Lcom/flurry/android/ak;)I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/flurry/android/ak;->a(Lcom/flurry/android/ak;Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    goto :goto_0

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-virtual {v0}, Lcom/flurry/android/ak;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "takeover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v1}, Lcom/flurry/android/ak;->f(Lcom/flurry/android/ak;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 136
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    iget-object v1, p0, Lcom/flurry/android/ar;->a:Lcom/flurry/android/ak;

    invoke-static {v1}, Lcom/flurry/android/ak;->f(Lcom/flurry/android/ak;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v5, v6

    .line 138
    goto :goto_1
.end method
