.class final Lcom/flurry/android/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/android/MMAdView$MMAdListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/ah;


# direct methods
.method constructor <init>(Lcom/flurry/android/ah;)V
    .locals 0
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final MMAdCachingCompleted(Lcom/millennialmedia/android/MMAdView;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 195
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial caching completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return-void
.end method

.method public final MMAdClickedToNewBrowser(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdClicked(Ljava/util/Map;)V

    .line 170
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial clicked to new browser."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-void
.end method

.method public final MMAdClickedToOverlay(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 176
    iget-object v0, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdClicked(Ljava/util/Map;)V

    .line 177
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial clicked to overlay."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    return-void
.end method

.method public final MMAdFailed(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdUnFilled(Ljava/util/Map;)V

    .line 154
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial failed to load ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void
.end method

.method public final MMAdOverlayLaunched(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 183
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial overlay launched."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-void
.end method

.method public final MMAdRequestIsCaching(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    .line 189
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial Interstitial request is caching."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    return-void
.end method

.method public final MMAdReturned(Lcom/millennialmedia/android/MMAdView;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 160
    invoke-virtual {p1}, Lcom/millennialmedia/android/MMAdView;->display()Z

    .line 161
    iget-object v0, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdFilled(Ljava/util/Map;)V

    .line 162
    iget-object v0, p0, Lcom/flurry/android/at;->a:Lcom/flurry/android/ah;

    invoke-virtual {v0, v1}, Lcom/flurry/android/ah;->onAdShown(Ljava/util/Map;)V

    .line 163
    const-string v0, "FlurryAgent"

    const-string v1, "Millennial In returned ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    return-void
.end method
