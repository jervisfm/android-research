.class final Lcom/flurry/android/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ads/AdListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/y;


# direct methods
.method constructor <init>(Lcom/flurry/android/y;)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lcom/flurry/android/az;->a:Lcom/flurry/android/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 113
    const-string v0, "FlurryAgent"

    const-string v1, "Admob AdView dismissed from screen."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void
.end method

.method public final onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/flurry/android/az;->a:Lcom/flurry/android/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdUnFilled(Ljava/util/Map;)V

    .line 101
    const-string v0, "FlurryAgent"

    const-string v1, "Admob AdView failed to receive ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    return-void
.end method

.method public final onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/flurry/android/az;->a:Lcom/flurry/android/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdClicked(Ljava/util/Map;)V

    .line 120
    const-string v0, "FlurryAgent"

    const-string v1, "Admob AdView leave application."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    return-void
.end method

.method public final onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 107
    const-string v0, "FlurryAgent"

    const-string v1, "Admob AdView present on screen."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    return-void
.end method

.method public final onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/flurry/android/az;->a:Lcom/flurry/android/y;

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdFilled(Ljava/util/Map;)V

    .line 93
    iget-object v0, p0, Lcom/flurry/android/az;->a:Lcom/flurry/android/y;

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdShown(Ljava/util/Map;)V

    .line 94
    const-string v0, "FlurryAgent"

    const-string v1, "Admob AdView received ad."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-void
.end method
