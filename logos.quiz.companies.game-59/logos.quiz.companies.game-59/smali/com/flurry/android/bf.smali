.class final Lcom/flurry/android/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/flurry/android/aj;


# static fields
.field static a:Ljava/lang/String;

.field static b:I

.field private static e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Lorg/apache/avro/io/DecoderFactory;

.field private static g:Lcom/flurry/android/w;


# instance fields
.field private A:Ljava/lang/String;

.field private B:J

.field private C:J

.field private D:J

.field private E:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ljava/lang/String;

.field private G:Landroid/os/Handler;

.field private H:Lcom/flurry/android/ICustomAdNetworkHandler;

.field private I:Lcom/flurry/android/IListener;

.field private volatile J:Z

.field private volatile K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/bc;",
            ">;"
        }
    .end annotation
.end field

.field private volatile L:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/flurry/android/bc;",
            ">;"
        }
    .end annotation
.end field

.field private volatile M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/bc;",
            ">;"
        }
    .end annotation
.end field

.field private volatile N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/flurry/android/x;

.field d:Lcom/flurry/android/be;

.field private h:Lcom/flurry/android/ay;

.field private i:Lcom/flurry/android/s;

.field private j:Landroid/view/Display;

.field private k:Z

.field private l:Ljava/io/File;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private volatile s:Ljava/lang/String;

.field private volatile t:Ljava/lang/String;

.field private volatile u:F

.field private volatile v:F

.field private volatile w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile x:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/flurry/android/TestAds;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcom/flurry/android/bc;

.field private z:Lcom/flurry/android/AdUnit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-string v0, "FlurryAgent"

    sput-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    .line 110
    const/4 v0, 0x5

    sput v0, Lcom/flurry/android/bf;->b:I

    return-void
.end method

.method constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-boolean v2, p0, Lcom/flurry/android/bf;->k:Z

    .line 89
    iput-object v0, p0, Lcom/flurry/android/bf;->l:Ljava/io/File;

    .line 94
    iput-boolean v3, p0, Lcom/flurry/android/bf;->q:Z

    .line 95
    iput-boolean v2, p0, Lcom/flurry/android/bf;->r:Z

    .line 98
    iput-object v0, p0, Lcom/flurry/android/bf;->s:Ljava/lang/String;

    .line 99
    iput-object v0, p0, Lcom/flurry/android/bf;->t:Ljava/lang/String;

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    .line 164
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->L:Ljava/util/Map;

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->M:Ljava/util/List;

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->N:Ljava/util/List;

    .line 169
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FlurryAdThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 171
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/flurry/android/bf;->G:Landroid/os/Handler;

    .line 173
    new-instance v0, Lcom/flurry/android/x;

    invoke-direct {v0}, Lcom/flurry/android/x;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    .line 174
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/flurry/android/bf;->e:Ljava/util/List;

    .line 176
    new-instance v0, Lorg/apache/avro/io/DecoderFactory;

    invoke-direct {v0}, Lorg/apache/avro/io/DecoderFactory;-><init>()V

    sput-object v0, Lcom/flurry/android/bf;->f:Lorg/apache/avro/io/DecoderFactory;

    .line 177
    new-instance v0, Lcom/flurry/android/w;

    invoke-direct {v0, p0}, Lcom/flurry/android/w;-><init>(Lcom/flurry/android/bf;)V

    sput-object v0, Lcom/flurry/android/bf;->g:Lcom/flurry/android/w;

    .line 178
    new-instance v0, Lcom/flurry/android/s;

    invoke-direct {v0, p0}, Lcom/flurry/android/s;-><init>(Lcom/flurry/android/bf;)V

    iput-object v0, p0, Lcom/flurry/android/bf;->i:Lcom/flurry/android/s;

    .line 179
    return-void
.end method

.method static a([B)I
    .locals 1
    .parameter

    .prologue
    .line 1362
    new-instance v0, Lcom/flurry/android/CrcMessageDigest;

    invoke-direct {v0}, Lcom/flurry/android/CrcMessageDigest;-><init>()V

    .line 1363
    invoke-virtual {v0, p0}, Lcom/flurry/android/CrcMessageDigest;->update([B)V

    .line 1364
    invoke-virtual {v0}, Lcom/flurry/android/CrcMessageDigest;->getChecksum()I

    move-result v0

    return v0
.end method

.method private declared-synchronized a(Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/ao;
    .locals 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/flurry/android/ao;"
        }
    .end annotation

    .prologue
    .line 1538
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flurry/android/ao;

    invoke-virtual {p0}, Lcom/flurry/android/bf;->d()J

    move-result-wide v3

    move-object v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/ao;-><init>(Ljava/lang/String;ZJLjava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/flurry/android/bf;)Ljava/io/File;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/flurry/android/bf;->l:Ljava/io/File;

    return-object v0
.end method

.method private a(Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;Lcom/flurry/android/v;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 855
    const-string v0, ".*?(%\\{\\w+\\}).*?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 856
    invoke-virtual {v1, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 857
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 858
    iget-object v2, p0, Lcom/flurry/android/bf;->i:Lcom/flurry/android/s;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, p2, p4, v0}, Lcom/flurry/android/s;->a(Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 859
    invoke-virtual {v1, p4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    goto :goto_0

    .line 861
    :cond_0
    return-object p4
.end method

.method private static a([BLjava/lang/Class;)Lorg/apache/avro/specific/SpecificRecordBase;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Lorg/apache/avro/specific/SpecificRecordBase;",
            ">([B",
            "Ljava/lang/Class",
            "<TA;>;)TA;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1346
    .line 1347
    sget-object v0, Lcom/flurry/android/bf;->f:Lorg/apache/avro/io/DecoderFactory;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v2, v1}, Lorg/apache/avro/io/DecoderFactory;->binaryDecoder(Ljava/io/InputStream;Lorg/apache/avro/io/BinaryDecoder;)Lorg/apache/avro/io/BinaryDecoder;

    move-result-object v0

    .line 1350
    :try_start_0
    new-instance v2, Lorg/apache/avro/specific/SpecificDatumReader;

    invoke-direct {v2, p1}, Lorg/apache/avro/specific/SpecificDatumReader;-><init>(Ljava/lang/Class;)V

    .line 1351
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lorg/apache/avro/specific/SpecificDatumReader;->read(Ljava/lang/Object;Lorg/apache/avro/io/Decoder;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/avro/specific/SpecificRecordBase;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    :goto_0
    return-object v0

    .line 1353
    :catch_0
    move-exception v0

    .line 1355
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ClassCastException in parseAvroBinary:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v1

    .line 1356
    goto :goto_0
.end method

.method private declared-synchronized a(Lcom/flurry/android/SdkLogResponse;)V
    .locals 2
    .parameter

    .prologue
    .line 878
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/flurry/android/SdkLogResponse;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 880
    iget-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    iget-object v1, p0, Lcom/flurry/android/bf;->M:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 889
    :cond_0
    monitor-exit p0

    return-void

    .line 884
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/flurry/android/SdkLogResponse;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 886
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 878
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/util/List;Ljava/io/DataOutputStream;)V
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/bc;",
            ">;",
            "Ljava/io/DataOutputStream;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1508
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 1509
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1512
    :try_start_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/bc;

    invoke-virtual {v0, p2}, Lcom/flurry/android/bc;->a(Ljava/io/DataOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1509
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1514
    :catch_0
    move-exception v0

    :try_start_2
    sget-object v3, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unable to convert adLog to byte[]: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/bc;

    invoke-virtual {v0}, Lcom/flurry/android/bc;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1508
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1517
    :cond_0
    monitor-exit p0

    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 510
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 511
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 512
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 514
    const/high16 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 516
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a([BLjava/lang/String;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1296
    if-nez p2, :cond_0

    move v0, v1

    .line 1319
    :goto_0
    return v0

    .line 1302
    :cond_0
    const-string v0, "/v3/getAds.do"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/flurry/android/bf;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/flurry/android/bf;->s:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1311
    :goto_2
    new-instance v2, Lcom/flurry/android/h;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/flurry/android/h;-><init>(Lcom/flurry/android/bf;[BLjava/lang/String;Ljava/lang/String;)V

    .line 1317
    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/flurry/android/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1319
    const/4 v0, 0x1

    goto :goto_0

    .line 1304
    :cond_1
    invoke-static {}, Lcom/flurry/android/FlurryAgent;->getUseHttps()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "https://ads.flurry.com"

    goto :goto_1

    :cond_2
    const-string v0, "http://ads.flurry.com"

    goto :goto_1

    .line 1308
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/flurry/android/bf;->t:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/flurry/android/bf;->t:Ljava/lang/String;

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-static {}, Lcom/flurry/android/FlurryAgent;->getUseHttps()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "https://adlog.flurry.com"

    goto :goto_3

    :cond_5
    const-string v0, "http://adlog.flurry.com"

    goto :goto_3
.end method

.method static a(Ljava/io/InputStream;)[B
    .locals 4
    .parameter

    .prologue
    .line 1324
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1325
    const/16 v1, 0x80

    new-array v1, v1, [B

    .line 1327
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1329
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 1331
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;IIZ)[B
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 1230
    invoke-direct {p0}, Lcom/flurry/android/bf;->r()Ljava/util/List;

    move-result-object v0

    .line 1233
    invoke-static {}, Lcom/flurry/android/AdRequest;->a()Lcom/flurry/android/AdRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/flurry/android/bf;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/flurry/android/AdRequest$Builder;->setApiKey(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/flurry/android/AdRequest$Builder;->setAdSpaceName(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v1

    sget-object v2, Lcom/flurry/android/bf;->e:Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/flurry/android/AdRequest$Builder;->setBindings(Ljava/util/List;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/flurry/android/AdRequest$Builder;->setAdReportedIds(Ljava/util/List;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    invoke-static {}, Lcom/flurry/android/Location;->a()Lcom/flurry/android/Location$Builder;

    move-result-object v1

    iget v2, p0, Lcom/flurry/android/bf;->u:F

    invoke-virtual {v1, v2}, Lcom/flurry/android/Location$Builder;->setLat(F)Lcom/flurry/android/Location$Builder;

    move-result-object v1

    iget v2, p0, Lcom/flurry/android/bf;->v:F

    invoke-virtual {v1, v2}, Lcom/flurry/android/Location$Builder;->setLon(F)Lcom/flurry/android/Location$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/Location$Builder;->build()Lcom/flurry/android/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setLocation(Lcom/flurry/android/Location;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/flurry/android/AdRequest$Builder;->setTestDevice(Z)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    invoke-static {}, Lcom/flurry/android/FlurryAgent;->getAgentVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setAgentVersion(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/flurry/android/bf;->B:J

    invoke-virtual {v0, v1, v2}, Lcom/flurry/android/AdRequest$Builder;->setSessionId(J)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    invoke-static {}, Lcom/flurry/android/AdViewContainer;->a()Lcom/flurry/android/AdViewContainer$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/flurry/android/bf;->j:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/flurry/android/AdViewContainer$Builder;->setScreenHeight(I)Lcom/flurry/android/AdViewContainer$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/flurry/android/bf;->j:Landroid/view/Display;

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/flurry/android/AdViewContainer$Builder;->setScreenWidth(I)Lcom/flurry/android/AdViewContainer$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/flurry/android/AdViewContainer$Builder;->setViewHeight(I)Lcom/flurry/android/AdViewContainer$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/flurry/android/AdViewContainer$Builder;->setViewWidth(I)Lcom/flurry/android/AdViewContainer$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/AdViewContainer$Builder;->build()Lcom/flurry/android/AdViewContainer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setAdViewContainer(Lcom/flurry/android/AdViewContainer;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/bf;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setLocale(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/bf;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setTimezone(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/bf;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setOsVersion(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/bf;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdRequest$Builder;->setDevicePlatform(Ljava/lang/CharSequence;)Lcom/flurry/android/AdRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdRequest$Builder;->build()Lcom/flurry/android/AdRequest;

    move-result-object v1

    .line 1255
    if-eqz p4, :cond_0

    .line 1257
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/flurry/android/AdRequest;->a(Ljava/lang/Boolean;)V

    .line 1264
    :goto_0
    iget-object v0, p0, Lcom/flurry/android/bf;->O:Ljava/util/Map;

    if-eqz v0, :cond_2

    .line 1266
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/flurry/android/bf;->O:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1261
    :cond_0
    invoke-virtual {v1, p1}, Lcom/flurry/android/AdRequest;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1266
    :cond_1
    invoke-virtual {v1, v2}, Lcom/flurry/android/AdRequest;->a(Ljava/util/Map;)V

    .line 1269
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/bf;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1271
    iget-object v0, p0, Lcom/flurry/android/bf;->x:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/TestAds;

    invoke-virtual {v1, v0}, Lcom/flurry/android/AdRequest;->a(Lcom/flurry/android/TestAds;)V

    .line 1274
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got ad request  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1277
    new-instance v0, Lorg/apache/avro/specific/SpecificDatumWriter;

    const-class v2, Lcom/flurry/android/AdRequest;

    invoke-direct {v0, v2}, Lorg/apache/avro/specific/SpecificDatumWriter;-><init>(Ljava/lang/Class;)V

    .line 1278
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1279
    invoke-static {}, Lorg/apache/avro/io/EncoderFactory;->get()Lorg/apache/avro/io/EncoderFactory;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lorg/apache/avro/io/EncoderFactory;->directBinaryEncoder(Ljava/io/OutputStream;Lorg/apache/avro/io/BinaryEncoder;)Lorg/apache/avro/io/BinaryEncoder;

    move-result-object v3

    .line 1282
    :try_start_0
    invoke-virtual {v0, v1, v3}, Lorg/apache/avro/specific/SpecificDatumWriter;->write(Ljava/lang/Object;Lorg/apache/avro/io/Encoder;)V

    .line 1283
    invoke-virtual {v3}, Lorg/apache/avro/io/BinaryEncoder;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1289
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 1290
    :goto_2
    return-object v0

    .line 1284
    :catch_0
    move-exception v0

    .line 1285
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    .line 1286
    new-array v0, v5, [B

    goto :goto_2
.end method

.method static synthetic b(Lcom/flurry/android/bf;)Z
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/flurry/android/bf;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/flurry/android/bf;)Lcom/flurry/android/ay;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/flurry/android/bf;->h:Lcom/flurry/android/ay;

    return-object v0
.end method

.method static synthetic c(Landroid/content/Context;)Z
    .locals 1
    .parameter

    .prologue
    .line 61
    invoke-static {p0}, Lcom/flurry/android/bf;->d(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 3
    .parameter

    .prologue
    .line 250
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 254
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 258
    :goto_0
    if-nez v0, :cond_1

    .line 259
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v2, "******No connectivity found."

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_1
    return v0

    .line 256
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/flurry/android/bf;->J:Z

    if-nez v0, :cond_0

    .line 385
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "Platform Module is not initialized"

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/bf;->F:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 389
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "Cannot identify UDID."

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_1
    iget-boolean v0, p0, Lcom/flurry/android/bf;->J:Z

    return v0
.end method

.method private q()[B
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1128
    invoke-direct {p0}, Lcom/flurry/android/bf;->r()Ljava/util/List;

    move-result-object v2

    .line 1130
    sget-object v3, Lcom/flurry/android/bf;->g:Lcom/flurry/android/w;

    iget-object v4, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/bc;

    new-instance v7, Lcom/flurry/android/SdkAdLog;

    invoke-direct {v7}, Lcom/flurry/android/SdkAdLog;-><init>()V

    invoke-virtual {v0}, Lcom/flurry/android/bc;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/flurry/android/SdkAdLog;->a(Ljava/lang/Long;)V

    invoke-virtual {v0}, Lcom/flurry/android/bc;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/flurry/android/SdkAdLog;->a(Ljava/lang/CharSequence;)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/flurry/android/bc;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/ao;

    invoke-virtual {v0}, Lcom/flurry/android/ao;->b()Z

    move-result v10

    if-eqz v10, :cond_0

    new-instance v10, Lcom/flurry/android/SdkAdEvent;

    invoke-direct {v10}, Lcom/flurry/android/SdkAdEvent;-><init>()V

    invoke-virtual {v0}, Lcom/flurry/android/ao;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/flurry/android/SdkAdEvent;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/flurry/android/ao;->c()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/flurry/android/SdkAdEvent;->a(Ljava/lang/Long;)V

    invoke-virtual {v0}, Lcom/flurry/android/ao;->d()Ljava/util/Map;

    move-result-object v0

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v11, v13, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    invoke-virtual {v10, v11}, Lcom/flurry/android/SdkAdEvent;->a(Ljava/util/Map;)V

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v7, v8}, Lcom/flurry/android/SdkAdLog;->a(Ljava/util/List;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, v3, Lcom/flurry/android/w;->a:Lcom/flurry/android/bf;

    iput-object v4, v0, Lcom/flurry/android/bf;->M:Ljava/util/List;

    .line 1131
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 1162
    :goto_3
    return-object v0

    .line 1138
    :cond_4
    invoke-static {}, Lcom/flurry/android/SdkLogRequest;->a()Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/flurry/android/bf;->A:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/flurry/android/SdkLogRequest$Builder;->setApiKey(Ljava/lang/CharSequence;)Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/flurry/android/SdkLogRequest$Builder;->setAdReportedIds(Ljava/util/List;)Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/flurry/android/SdkLogRequest$Builder;->setSdkAdLogs(Ljava/util/List;)Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/flurry/android/SdkLogRequest$Builder;->setTestDevice(Z)Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/flurry/android/SdkLogRequest$Builder;->setAgentTimestamp(J)Lcom/flurry/android/SdkLogRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/SdkLogRequest$Builder;->build()Lcom/flurry/android/SdkLogRequest;

    move-result-object v0

    .line 1146
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got ad log request:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/flurry/android/SdkLogRequest;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1149
    new-instance v2, Lorg/apache/avro/specific/SpecificDatumWriter;

    const-class v3, Lcom/flurry/android/SdkLogRequest;

    invoke-direct {v2, v3}, Lorg/apache/avro/specific/SpecificDatumWriter;-><init>(Ljava/lang/Class;)V

    .line 1150
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1151
    invoke-static {}, Lorg/apache/avro/io/EncoderFactory;->get()Lorg/apache/avro/io/EncoderFactory;

    move-result-object v4

    invoke-virtual {v4, v3, v1}, Lorg/apache/avro/io/EncoderFactory;->directBinaryEncoder(Ljava/io/OutputStream;Lorg/apache/avro/io/BinaryEncoder;)Lorg/apache/avro/io/BinaryEncoder;

    move-result-object v4

    .line 1154
    :try_start_0
    invoke-virtual {v2, v0, v4}, Lorg/apache/avro/specific/SpecificDatumWriter;->write(Ljava/lang/Object;Lorg/apache/avro/io/Encoder;)V

    .line 1155
    invoke-virtual {v4}, Lorg/apache/avro/io/BinaryEncoder;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1161
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_3

    .line 1156
    :catch_0
    move-exception v0

    .line 1157
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-object v0, v1

    .line 1158
    goto :goto_3
.end method

.method private r()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/android/AdReportedId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1214
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1215
    iget-object v0, p0, Lcom/flurry/android/bf;->F:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1216
    invoke-static {}, Lcom/flurry/android/AdReportedId;->a()Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/flurry/android/AdReportedId$Builder;->setId(Ljava/nio/ByteBuffer;)Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/AdReportedId$Builder;->setType(I)Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdReportedId$Builder;->build()Lcom/flurry/android/AdReportedId;

    move-result-object v0

    .line 1217
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1218
    iget-object v0, p0, Lcom/flurry/android/bf;->E:Ljava/util/Map;

    .line 1219
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1221
    invoke-static {}, Lcom/flurry/android/AdReportedId;->a()Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v1}, Lcom/flurry/android/AdReportedId$Builder;->setId(Ljava/nio/ByteBuffer;)Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/flurry/android/AdReportedId$Builder;->setType(I)Lcom/flurry/android/AdReportedId$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdReportedId$Builder;->build()Lcom/flurry/android/AdReportedId;

    move-result-object v0

    .line 1222
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1224
    :cond_0
    return-object v2
.end method


# virtual methods
.method final declared-synchronized a(Lcom/flurry/android/bc;Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/bc;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flurry/android/bc;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/flurry/android/bc;"
        }
    .end annotation

    .prologue
    .line 1521
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2, p3, p4}, Lcom/flurry/android/bf;->a(Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/ao;

    move-result-object v0

    .line 1522
    invoke-virtual {p1, v0}, Lcom/flurry/android/bc;->a(Lcom/flurry/android/ao;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1523
    monitor-exit p0

    return-object p1

    .line 1521
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/bc;
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/flurry/android/bc;"
        }
    .end annotation

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/flurry/android/bf;->L:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/bc;

    .line 1529
    if-nez v0, :cond_0

    .line 1531
    invoke-static {p0, p1}, Lcom/flurry/android/z;->a(Lcom/flurry/android/bf;Ljava/lang/String;)Lcom/flurry/android/bc;

    move-result-object v0

    .line 1533
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1, p4}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bc;Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/bc;

    move-result-object v0

    return-object v0
.end method

.method final a(Landroid/content/Context;Lcom/flurry/android/AdUnit;)Lcom/flurry/android/r;
    .locals 13
    .parameter
    .parameter

    .prologue
    .line 1717
    const/4 v7, 0x0

    .line 1718
    const/4 v6, 0x0

    .line 1728
    invoke-virtual {p2}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    .line 1729
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1731
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    .line 1732
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1733
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1734
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1735
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1736
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "requested"

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {p0, v1, v5, v9, v10}, Lcom/flurry/android/bf;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/bc;

    move-result-object v9

    .line 1743
    iget-object v1, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    invoke-virtual {p2}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v11

    invoke-virtual {v11}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    if-ne v1, v12, :cond_1

    sget-object v1, Lcom/flurry/android/FlurryAdType;->VIDEO_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    :goto_0
    invoke-interface {v5, v10, v1}, Lcom/flurry/android/IListener;->shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1745
    new-instance v0, Lcom/flurry/android/r;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/flurry/android/r;-><init>(Lcom/flurry/android/p;ZZLcom/flurry/android/AdUnit;)V

    .line 1841
    :goto_1
    return-object v0

    .line 1740
    :cond_0
    new-instance v0, Lcom/flurry/android/r;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/flurry/android/r;-><init>(Lcom/flurry/android/p;ZZLcom/flurry/android/AdUnit;)V

    goto :goto_1

    .line 1743
    :cond_1
    const-string v1, "takeover"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/flurry/android/FlurryAdType;->WEB_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/flurry/android/FlurryAdType;->WEB_BANNER:Lcom/flurry/android/FlurryAdType;

    goto :goto_0

    .line 1748
    :cond_3
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Processing ad request for binding: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", networkType: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, ", format: "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    invoke-virtual {p2}, Lcom/flurry/android/AdUnit;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x1

    if-eq v1, v5, :cond_4

    const/4 v1, 0x2

    if-eq v2, v1, :cond_4

    const/4 v1, 0x1

    if-eq v2, v1, :cond_4

    const/4 v1, 0x3

    if-ne v2, v1, :cond_6

    .line 1752
    :cond_4
    const-string v0, "takeover"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1754
    iput-object v9, p0, Lcom/flurry/android/bf;->y:Lcom/flurry/android/bc;

    .line 1755
    iput-object p2, p0, Lcom/flurry/android/bf;->z:Lcom/flurry/android/AdUnit;

    .line 1756
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "opening takeover activity, display: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", content: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1757
    const/4 v0, 0x1

    move-object v1, v7

    .line 1841
    :goto_2
    new-instance v2, Lcom/flurry/android/r;

    invoke-static {v8}, Lcom/flurry/android/z;->e(Ljava/lang/String;)Ljava/util/Map;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v0, v3, p2}, Lcom/flurry/android/r;-><init>(Lcom/flurry/android/p;ZZLcom/flurry/android/AdUnit;)V

    move-object v0, v2

    goto/16 :goto_1

    .line 1761
    :cond_5
    new-instance v0, Lcom/flurry/android/ak;

    invoke-direct {v0, p1, p0, v9, p2}, Lcom/flurry/android/ak;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;)V

    .line 1762
    invoke-virtual {v0, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    move-object v1, v0

    move v0, v6

    goto :goto_2

    .line 1765
    :cond_6
    const/4 v1, 0x4

    if-ne v2, v1, :cond_e

    .line 1768
    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    .line 1769
    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->c()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->e()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/flurry/android/AdCreative;

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/AdCreative;-><init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1771
    const-string v1, "Admob"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1773
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieving BannerView for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    new-instance v1, Lcom/flurry/android/y;

    invoke-direct {v1, p1, p0, v9, v0}, Lcom/flurry/android/y;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 1775
    invoke-virtual {v1, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1776
    const/4 v0, 0x0

    iput v0, v1, Lcom/flurry/android/p;->d:I

    .line 1777
    iput-object p2, v1, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move v0, v6

    goto :goto_2

    .line 1779
    :cond_7
    const-string v1, "Millennial Media"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1781
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieving BannerView for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1782
    new-instance v1, Lcom/flurry/android/ah;

    invoke-direct {v1, p1, p0, v9, v0}, Lcom/flurry/android/ah;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 1783
    invoke-virtual {v1, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1784
    const/4 v0, 0x0

    iput v0, v1, Lcom/flurry/android/p;->d:I

    .line 1785
    iput-object p2, v1, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move v0, v6

    goto/16 :goto_2

    .line 1787
    :cond_8
    const-string v1, "InMobi"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1789
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieving BannerView for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1790
    new-instance v1, Lcom/flurry/android/ab;

    invoke-direct {v1, p1, p0, v9, v0}, Lcom/flurry/android/ab;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 1791
    invoke-virtual {v1, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, v1, Lcom/flurry/android/p;->d:I

    .line 1793
    iput-object p2, v1, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move v0, v6

    goto/16 :goto_2

    .line 1795
    :cond_9
    const-string v1, "Mobclix"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1797
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieving BannerView for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1798
    new-instance v1, Lcom/flurry/android/ad;

    invoke-direct {v1, p1, p0, v9, v0}, Lcom/flurry/android/ad;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 1799
    invoke-virtual {v1, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1800
    const/4 v0, 0x0

    iput v0, v1, Lcom/flurry/android/p;->d:I

    .line 1801
    iput-object p2, v1, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move v0, v6

    goto/16 :goto_2

    .line 1803
    :cond_a
    const-string v1, "Jumptap"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1805
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieving BannerView for:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1806
    new-instance v1, Lcom/flurry/android/g;

    invoke-direct {v1, p1, p0, v9, v0}, Lcom/flurry/android/g;-><init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;Lcom/flurry/android/AdCreative;)V

    .line 1807
    invoke-virtual {v1, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1808
    const/4 v0, 0x0

    iput v0, v1, Lcom/flurry/android/p;->d:I

    .line 1809
    iput-object p2, v1, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move v0, v6

    goto/16 :goto_2

    .line 1813
    :cond_b
    iget-object v1, p0, Lcom/flurry/android/bf;->H:Lcom/flurry/android/ICustomAdNetworkHandler;

    .line 1814
    if-eqz v1, :cond_d

    .line 1816
    invoke-interface {v1, p1, v0, v8}, Lcom/flurry/android/ICustomAdNetworkHandler;->getAdFromNetwork(Landroid/content/Context;Lcom/flurry/android/AdCreative;Ljava/lang/String;)Lcom/flurry/android/AdNetworkView;

    move-result-object v0

    .line 1817
    if-eqz v0, :cond_c

    .line 1819
    iput-object p0, v0, Lcom/flurry/android/p;->a:Lcom/flurry/android/bf;

    .line 1820
    iput-object v9, v0, Lcom/flurry/android/p;->b:Lcom/flurry/android/bc;

    .line 1821
    invoke-virtual {v0, p1}, Lcom/flurry/android/p;->initLayout(Landroid/content/Context;)V

    .line 1822
    const/4 v1, 0x0

    iput v1, v0, Lcom/flurry/android/p;->d:I

    .line 1823
    iput-object p2, v0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    move-object v1, v0

    move v0, v6

    goto/16 :goto_2

    .line 1827
    :cond_c
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v2, "CustomAdNetworkHandler returned null banner view"

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    move v0, v6

    goto/16 :goto_2

    .line 1832
    :cond_d
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "No CustomAdNetworkHandler set"

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v6

    move-object v1, v7

    .line 1835
    goto/16 :goto_2

    .line 1838
    :cond_e
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Do not support binding: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v6

    move-object v1, v7

    goto/16 :goto_2
.end method

.method final a(Landroid/content/Context;Ljava/lang/String;IIZJ)Lcom/flurry/android/r;
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 942
    new-instance v1, Lcom/flurry/android/r;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/flurry/android/r;-><init>(Lcom/flurry/android/p;ZZLcom/flurry/android/AdUnit;)V

    .line 943
    invoke-direct {p0}, Lcom/flurry/android/bf;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 998
    :cond_0
    :goto_0
    return-object v1

    .line 948
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 949
    const/4 v0, 0x0

    move-object v7, v1

    .line 951
    :goto_1
    add-long v1, v8, p6

    invoke-static {v1, v2}, Lcom/flurry/android/z;->a(J)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 953
    invoke-virtual {p0, p2}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v1

    .line 954
    if-eqz v1, :cond_2

    .line 956
    invoke-virtual {p0, p1, v1}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Lcom/flurry/android/AdUnit;)Lcom/flurry/android/r;

    move-result-object v7

    .line 961
    iget-object v1, p0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    invoke-virtual {v1, p2}, Lcom/flurry/android/x;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    if-nez v0, :cond_5

    .line 963
    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    invoke-virtual/range {v0 .. v6}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZJ)V

    .line 966
    const/4 v0, 0x1

    move-object v1, v7

    .line 988
    :goto_2
    invoke-virtual {v1}, Lcom/flurry/android/r;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 990
    const-wide/16 v2, 0xa

    :try_start_0
    div-long v2, p6, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v7, v1

    .line 999
    goto :goto_1

    .line 972
    :cond_2
    if-nez v0, :cond_3

    .line 977
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-wide/from16 v5, p6

    invoke-virtual/range {v0 .. v6}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZJ)V

    .line 978
    const/4 v0, 0x1

    .line 982
    :cond_3
    invoke-virtual {p0, p2}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v1

    .line 983
    if-eqz v1, :cond_4

    .line 985
    invoke-virtual {p0, p1, v1}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Lcom/flurry/android/AdUnit;)Lcom/flurry/android/r;

    move-result-object v1

    goto :goto_2

    .line 997
    :catch_0
    move-exception v0

    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v2, "Thread with makeAdRequest interrupted."

    invoke-static {v0, v2}, Lcom/flurry/android/bd;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move-object v1, v7

    goto :goto_2

    :cond_5
    move-object v1, v7

    goto :goto_2

    :cond_6
    move-object v1, v7

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/be;->a(Landroid/content/Context;)V

    .line 241
    return-void
.end method

.method final a(FF)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 578
    iput p1, p0, Lcom/flurry/android/bf;->u:F

    .line 579
    iput p2, p0, Lcom/flurry/android/bf;->v:F

    .line 580
    return-void
.end method

.method final a(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 234
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p1}, Lcom/flurry/android/be;->a(Landroid/content/Context;)V

    .line 235
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0}, Lcom/flurry/android/be;->b()V

    .line 236
    return-void
.end method

.method final a(Landroid/content/Context;JJ)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    iput-wide p2, p0, Lcom/flurry/android/bf;->B:J

    .line 225
    iput-wide p4, p0, Lcom/flurry/android/bf;->C:J

    .line 226
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flurry/android/bf;->D:J

    .line 227
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p1}, Lcom/flurry/android/be;->a(Landroid/content/Context;)V

    .line 228
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0}, Lcom/flurry/android/be;->a()V

    .line 229
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flurry/android/bf;->r:Z

    .line 230
    return-void
.end method

.method final a(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 482
    invoke-direct {p0}, Lcom/flurry/android/bf;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p2}, Lcom/flurry/android/be;->a(Landroid/view/ViewGroup;)Lcom/flurry/android/u;

    move-result-object v0

    .line 488
    if-eqz v0, :cond_0

    .line 490
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 491
    iget-object v1, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v1, p1, v0}, Lcom/flurry/android/be;->a(Landroid/content/Context;Lcom/flurry/android/u;)V

    goto :goto_0
.end method

.method final a(Landroid/content/Context;Lcom/flurry/android/o;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/flurry/android/bf;->J:Z

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p2, Lcom/flurry/android/o;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/flurry/android/bf;->A:Ljava/lang/String;

    .line 187
    new-instance v0, Lcom/flurry/android/be;

    invoke-direct {v0, p1}, Lcom/flurry/android/be;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flurry/android/bf;->J:Z

    .line 196
    :cond_0
    iget-object v0, p2, Lcom/flurry/android/o;->b:Lcom/flurry/android/ay;

    iput-object v0, p0, Lcom/flurry/android/bf;->h:Lcom/flurry/android/ay;

    .line 197
    iget-object v0, p2, Lcom/flurry/android/o;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/flurry/android/bf;->m:Ljava/lang/String;

    .line 199
    iget-object v0, p2, Lcom/flurry/android/o;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/flurry/android/bf;->n:Ljava/lang/String;

    .line 202
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    iput-object v0, p0, Lcom/flurry/android/bf;->o:Ljava/lang/String;

    .line 203
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/flurry/android/bf;->p:Ljava/lang/String;

    .line 206
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 207
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/bf;->j:Landroid/view/Display;

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".flurryadlog."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/bf;->A:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/bf;->l:Ljava/io/File;

    .line 213
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "market://details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1, v0, v1}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/flurry/android/bf;->q:Z

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->w:Ljava/util/Map;

    .line 219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/android/bf;->x:Ljava/util/Map;

    .line 220
    return-void
.end method

.method final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 637
    const-string v0, "market://details?id="

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 639
    const-string v0, "market://details?id="

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 640
    iget-boolean v1, p0, Lcom/flurry/android/bf;->q:Z

    if-eqz v1, :cond_0

    .line 644
    :try_start_0
    invoke-static {p1, p2}, Lcom/flurry/android/z;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 661
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    .line 648
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot launch Marketplace url "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/flurry/android/bd;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 653
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://market.android.com/details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 654
    invoke-static {p1, v0}, Lcom/flurry/android/z;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 659
    :cond_1
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected android market url scheme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final a(Lcom/flurry/android/ICustomAdNetworkHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 274
    if-eqz p1, :cond_0

    .line 276
    iput-object p1, p0, Lcom/flurry/android/bf;->H:Lcom/flurry/android/ICustomAdNetworkHandler;

    .line 278
    :cond_0
    return-void
.end method

.method final a(Lcom/flurry/android/IListener;)V
    .locals 0
    .parameter

    .prologue
    .line 321
    if-eqz p1, :cond_0

    .line 323
    iput-object p1, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    .line 325
    :cond_0
    return-void
.end method

.method public final a(Lcom/flurry/android/bb;Lcom/flurry/android/aj;)V
    .locals 12
    .parameter
    .parameter

    .prologue
    .line 1611
    iget-object v0, p1, Lcom/flurry/android/bb;->c:Lcom/flurry/android/AdUnit;

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    iget v1, p1, Lcom/flurry/android/bb;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->e()Ljava/util/List;

    move-result-object v0

    iget-object v3, p1, Lcom/flurry/android/bb;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/Callback;

    invoke-virtual {v0}, Lcom/flurry/android/Callback;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/flurry/android/Callback;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x3f

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    const/4 v0, -0x1

    if-eq v7, v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v1, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v7, "&"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v8, :cond_3

    aget-object v9, v7, v1

    const/16 v10, 0x3d

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    const/4 v11, 0x0

    invoke-virtual {v9, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v11, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    :cond_3
    new-instance v1, Lcom/flurry/android/v;

    invoke-direct {v1, v0, v6, p1}, Lcom/flurry/android/v;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/bb;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1614
    :cond_4
    iget-object v0, p1, Lcom/flurry/android/bb;->a:Ljava/lang/String;

    const-string v1, "adWillClose"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1616
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    if-eqz v0, :cond_5

    .line 1618
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    iget-object v1, p1, Lcom/flurry/android/bb;->c:Lcom/flurry/android/AdUnit;

    invoke-virtual {v1}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flurry/android/IListener;->onAdClosed(Ljava/lang/String;)V

    .line 1620
    :cond_5
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1621
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "closeAd"

    aput-object v4, v0, v1

    const/4 v1, 0x1

    const-string v4, "processRedirect"

    aput-object v4, v0, v1

    const/4 v1, 0x2

    const-string v4, "nextFrame"

    aput-object v4, v0, v1

    const/4 v1, 0x3

    const-string v4, "nextAdUnit"

    aput-object v4, v0, v1

    const/4 v1, 0x4

    const-string v4, "notifyUser"

    aput-object v4, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1622
    const/4 v1, 0x0

    .line 1623
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/v;

    .line 1625
    iget-object v0, v0, Lcom/flurry/android/v;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1627
    const/4 v0, 0x1

    .line 1631
    :goto_2
    if-nez v0, :cond_7

    .line 1633
    new-instance v0, Lcom/flurry/android/v;

    const-string v1, "closeAd"

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v0, v1, v3, p1}, Lcom/flurry/android/v;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/bb;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1637
    :cond_7
    iget-object v0, p1, Lcom/flurry/android/bb;->a:Ljava/lang/String;

    const-string v1, "renderFailed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1639
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    if-eqz v0, :cond_8

    .line 1641
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    iget-object v1, p1, Lcom/flurry/android/bb;->c:Lcom/flurry/android/AdUnit;

    invoke-virtual {v1}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flurry/android/IListener;->onRenderFailed(Ljava/lang/String;)V

    .line 1647
    :cond_8
    const/4 v1, 0x0

    .line 1649
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/v;

    .line 1651
    iget-object v3, v0, Lcom/flurry/android/v;->a:Ljava/lang/String;

    const-string v4, "logEvent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1654
    iget-object v1, v0, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v3, "__sendToServer"

    const-string v4, "true"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1656
    :cond_9
    invoke-interface {p2, v0, p0}, Lcom/flurry/android/aj;->a(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V

    goto :goto_3

    .line 1659
    :cond_a
    if-nez v1, :cond_b

    .line 1661
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1662
    const-string v1, "__sendToServer"

    const-string v2, "false"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1663
    new-instance v1, Lcom/flurry/android/v;

    const-string v2, "logEvent"

    invoke-direct {v1, v2, v0, p1}, Lcom/flurry/android/v;-><init>(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/bb;)V

    .line 1664
    invoke-interface {p2, v1, p0}, Lcom/flurry/android/aj;->a(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V

    .line 1666
    :cond_b
    return-void

    :cond_c
    move v0, v1

    goto :goto_2
.end method

.method final declared-synchronized a(Lcom/flurry/android/bc;)V
    .locals 2
    .parameter

    .prologue
    .line 396
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x7fff

    if-ge v0, v1, :cond_0

    .line 398
    iget-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    iget-object v0, p0, Lcom/flurry/android/bf;->L:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/flurry/android/bc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :cond_0
    monitor-exit p0

    return-void

    .line 396
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 1671
    iget-object v0, p1, Lcom/flurry/android/v;->a:Ljava/lang/String;

    .line 1672
    iget-object v1, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    iget-object v2, v1, Lcom/flurry/android/bb;->b:Landroid/content/Context;

    .line 1673
    iget-object v1, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    iget-object v4, v1, Lcom/flurry/android/bb;->d:Lcom/flurry/android/bc;

    .line 1674
    iget-object v1, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    iget-object v3, v1, Lcom/flurry/android/bb;->c:Lcom/flurry/android/AdUnit;

    .line 1676
    const-string v1, "processRedirect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1678
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1680
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    if-eqz v0, :cond_0

    .line 1682
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    invoke-virtual {v3}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flurry/android/IListener;->onApplicationExit(Ljava/lang/String;)V

    .line 1684
    :cond_0
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/flurry/android/z;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4, v3, p1, v0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;Lcom/flurry/android/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Lcom/flurry/android/af;

    invoke-direct {v0, p0, v2, v3}, Lcom/flurry/android/af;-><init>(Lcom/flurry/android/bf;Landroid/content/Context;Ljava/lang/String;)V

    const-string v1, ""

    const/4 v4, 0x0

    :try_start_0
    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v4}, Lcom/flurry/android/af;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/flurry/android/bf;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/flurry/android/bf;->G:Landroid/os/Handler;

    new-instance v3, Lcom/flurry/android/c;

    const/4 v4, 0x1

    invoke-direct {v3, p0, v0, v2, v4}, Lcom/flurry/android/c;-><init>(Lcom/flurry/android/bf;Ljava/lang/String;Landroid/content/Context;Z)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1712
    :cond_1
    :goto_1
    return-void

    .line 1684
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Redirect URL could not be found for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1687
    :cond_3
    const-string v1, "verifyUrl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1689
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1691
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v4, v3, p1, v0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;Lcom/flurry/android/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1692
    const-string v1, "android.intent.action.VIEW"

    invoke-static {v2, v0, v1}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v1, "urlVerified"

    .line 1693
    :goto_2
    new-instance v0, Lcom/flurry/android/bb;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    iget-object v2, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    iget-object v2, v2, Lcom/flurry/android/bb;->b:Landroid/content/Context;

    iget-object v5, p1, Lcom/flurry/android/v;->c:Lcom/flurry/android/bb;

    iget v5, v5, Lcom/flurry/android/bb;->e:I

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/bb;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    invoke-virtual {p2, v0, p0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bb;Lcom/flurry/android/aj;)V

    goto :goto_1

    .line 1692
    :cond_4
    const-string v1, "urlNotVerified"

    goto :goto_2

    .line 1697
    :cond_5
    const-string v1, "sendUrlAsync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1699
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1701
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    if-eqz v0, :cond_6

    .line 1703
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    invoke-virtual {v3}, Lcom/flurry/android/AdUnit;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/flurry/android/IListener;->onApplicationExit(Ljava/lang/String;)V

    .line 1705
    :cond_6
    iget-object v0, p1, Lcom/flurry/android/v;->b:Ljava/util/Map;

    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v4, v3, p1, v0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bc;Lcom/flurry/android/AdUnit;Lcom/flurry/android/v;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/flurry/android/d;

    invoke-direct {v1, p0, v2, v0}, Lcom/flurry/android/d;-><init>(Lcom/flurry/android/bf;Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/flurry/android/bf;->G:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 1708
    :cond_7
    const-string v1, "sendAdLogs"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1710
    invoke-virtual {p0}, Lcom/flurry/android/bf;->k()V

    goto/16 :goto_1
.end method

.method final declared-synchronized a(Ljava/io/DataInputStream;)V
    .locals 2
    .parameter

    .prologue
    .line 1494
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 1495
    if-eqz v0, :cond_0

    .line 1497
    iget-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    new-instance v1, Lcom/flurry/android/bc;

    invoke-direct {v1, p1}, Lcom/flurry/android/bc;-><init>(Ljava/io/DataInput;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1502
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/flurry/android/bf;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1503
    monitor-exit p0

    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 306
    iput-object p1, p0, Lcom/flurry/android/bf;->F:Ljava/lang/String;

    .line 307
    return-void
.end method

.method final a(Ljava/lang/String;IIZJ)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/flurry/android/bf;->N:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    if-nez p1, :cond_1

    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "ad space name should not be null"

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    :cond_0
    :goto_0
    return-void

    .line 1066
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/flurry/android/i;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/i;-><init>(Lcom/flurry/android/bf;Ljava/lang/String;IIZ)V

    iget-object v1, p0, Lcom/flurry/android/bf;->G:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    const-string v5, "/v3/getAds.do"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZLjava/lang/String;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;IIZLjava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/flurry/android/bf;->N:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1198
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZ)[B

    move-result-object v0

    .line 1200
    if-eqz v0, :cond_0

    .line 1202
    invoke-direct {p0, v0, p5}, Lcom/flurry/android/bf;->a([BLjava/lang/String;)Z

    .line 1206
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/bf;->N:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1209
    return-void
.end method

.method final a(Ljava/lang/String;Lcom/flurry/android/FlurryAdSize;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 585
    if-eqz p2, :cond_0

    .line 587
    iget-object v0, p0, Lcom/flurry/android/bf;->x:Ljava/util/Map;

    invoke-static {}, Lcom/flurry/android/TestAds;->a()Lcom/flurry/android/TestAds$Builder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/flurry/android/FlurryAdSize;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/flurry/android/TestAds$Builder;->setAdspacePlacement(I)Lcom/flurry/android/TestAds$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/android/TestAds$Builder;->build()Lcom/flurry/android/TestAds;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 589
    :cond_0
    return-void
.end method

.method final a(Ljava/util/Map;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/nio/ByteBuffer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 311
    iput-object p1, p0, Lcom/flurry/android/bf;->E:Ljava/util/Map;

    .line 312
    return-void
.end method

.method final a(Landroid/content/Context;Ljava/lang/String;J)Z
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 913
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 914
    :cond_0
    add-long v0, v8, p3

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 916
    invoke-virtual {p0, p2}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    .line 917
    if-eqz v0, :cond_2

    move v4, v7

    .line 936
    :cond_1
    :goto_0
    return v4

    .line 924
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 925
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 926
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object v0, p0

    move-object v1, p2

    move-wide v5, p3

    .line 927
    invoke-virtual/range {v0 .. v6}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZJ)V

    .line 929
    invoke-virtual {p0, p2}, Lcom/flurry/android/bf;->d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    .line 930
    if-eqz v0, :cond_0

    move v4, v7

    .line 932
    goto :goto_0
.end method

.method final a(Landroid/content/Context;Ljava/lang/String;Landroid/view/ViewGroup;J)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 406
    .line 407
    invoke-direct {p0}, Lcom/flurry/android/bf;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    :goto_0
    return v2

    .line 412
    :cond_0
    const-wide/16 v0, 0x1

    cmp-long v0, p4, v0

    if-gez v0, :cond_2

    .line 414
    iget-object v0, p0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    invoke-virtual {v0, p2}, Lcom/flurry/android/x;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-static {p1}, Lcom/flurry/android/bf;->d(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    new-instance v1, Lcom/flurry/android/GetAdAsyncTask;

    invoke-direct {v1, p1, p2, p3, p0}, Lcom/flurry/android/GetAdAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/view/ViewGroup;Lcom/flurry/android/bf;)V

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/flurry/android/GetAdAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    move v2, v0

    goto :goto_0

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p3}, Lcom/flurry/android/be;->a(Landroid/view/ViewGroup;)Lcom/flurry/android/u;

    move-result-object v4

    .line 421
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p1, p3, p2}, Lcom/flurry/android/be;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)Lcom/flurry/android/u;

    move-result-object v0

    .line 422
    if-nez v0, :cond_3

    .line 424
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p0, p1, p3, p2}, Lcom/flurry/android/be;->a(Lcom/flurry/android/bf;Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)Lcom/flurry/android/u;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v1, p1, v0}, Lcom/flurry/android/be;->b(Landroid/content/Context;Lcom/flurry/android/u;)V

    :cond_3
    move-object v1, v0

    .line 429
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, p1, p3, p2}, Lcom/flurry/android/be;->a(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 430
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/u;

    .line 432
    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 434
    iget-object v6, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v6, p1, v0}, Lcom/flurry/android/be;->a(Landroid/content/Context;Lcom/flurry/android/u;)V

    .line 435
    invoke-virtual {v0}, Lcom/flurry/android/u;->c()Landroid/view/ViewGroup;

    move-result-object v6

    .line 436
    invoke-virtual {v6, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 441
    :cond_5
    iget-object v0, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v0, v1, p4, p5}, Lcom/flurry/android/be;->a(Lcom/flurry/android/u;J)Lcom/flurry/android/as;

    move-result-object v0

    .line 442
    invoke-virtual {v0}, Lcom/flurry/android/as;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 444
    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 446
    if-eqz v4, :cond_6

    .line 448
    iget-object v5, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v5, p1, v4}, Lcom/flurry/android/be;->a(Landroid/content/Context;Lcom/flurry/android/u;)V

    .line 449
    invoke-virtual {p3, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 451
    :cond_6
    invoke-virtual {p3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 453
    :cond_7
    invoke-virtual {v1}, Lcom/flurry/android/u;->a()I

    move-result v4

    if-lez v4, :cond_8

    invoke-virtual {v1}, Lcom/flurry/android/u;->b()Z

    move-result v4

    if-nez v4, :cond_8

    .line 455
    iget-object v4, p0, Lcom/flurry/android/bf;->d:Lcom/flurry/android/be;

    invoke-virtual {v4, v1}, Lcom/flurry/android/be;->a(Lcom/flurry/android/u;)V

    .line 458
    :cond_8
    invoke-virtual {v0}, Lcom/flurry/android/as;->a()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Lcom/flurry/android/as;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v0, v3

    .line 460
    :goto_2
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "******Ad is being returned: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/android/bd;->a(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v0

    .line 461
    goto/16 :goto_0

    :cond_a
    move v0, v2

    .line 458
    goto :goto_2
.end method

.method final b()J
    .locals 2

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/flurry/android/bf;->B:J

    return-wide v0
.end method

.method final b(Landroid/content/Context;)V
    .locals 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 894
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "Init\'ing ads."

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    iget-boolean v0, p0, Lcom/flurry/android/bf;->r:Z

    if-nez v0, :cond_0

    .line 898
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 899
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 900
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 901
    const-string v1, ""

    const-wide/16 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/flurry/android/bf;->a(Ljava/lang/String;IIZJ)V

    .line 902
    iput-boolean v4, p0, Lcom/flurry/android/bf;->r:Z

    .line 908
    :goto_0
    return-void

    .line 906
    :cond_0
    sget-object v0, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v1, "Ads already init\'ed, will not init them again this session"

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 528
    iput-object p1, p0, Lcom/flurry/android/bf;->s:Ljava/lang/String;

    .line 529
    return-void
.end method

.method final b(Ljava/util/Map;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    if-eqz p1, :cond_0

    .line 346
    iput-object p1, p0, Lcom/flurry/android/bf;->O:Ljava/util/Map;

    .line 348
    :cond_0
    return-void
.end method

.method final b([B)V
    .locals 3
    .parameter

    .prologue
    .line 1370
    const-class v0, Lcom/flurry/android/SdkLogResponse;

    invoke-static {p1, v0}, Lcom/flurry/android/bf;->a([BLjava/lang/Class;)Lorg/apache/avro/specific/SpecificRecordBase;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/SdkLogResponse;

    .line 1371
    if-eqz v0, :cond_0

    .line 1373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got an AdLogResponse:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1375
    invoke-direct {p0, v0}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/SdkLogResponse;)V

    .line 1377
    :cond_0
    return-void
.end method

.method final b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 820
    move v2, v1

    .line 822
    :goto_0
    const/4 v3, 0x5

    if-ge v2, v3, :cond_2

    .line 825
    invoke-static {p1}, Lcom/flurry/android/bf;->d(Landroid/content/Context;)Z

    move-result v3

    if-ne v3, v0, :cond_0

    .line 827
    iget-object v3, p0, Lcom/flurry/android/bf;->h:Lcom/flurry/android/ay;

    const/16 v4, 0x2710

    const/16 v5, 0x3a98

    invoke-static {v3, p2, v4, v5, v0}, Lcom/flurry/android/z;->a(Lcom/flurry/android/ay;Ljava/lang/String;IIZ)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 828
    if-eqz v3, :cond_1

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 830
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL hit succeeded for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 849
    :goto_1
    return v0

    .line 839
    :cond_0
    const-wide/16 v3, 0x64

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 847
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 841
    :catch_0
    move-exception v3

    .line 843
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/flurry/android/bf;->A:Ljava/lang/String;

    return-object v0
.end method

.method final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 534
    iput-object p1, p0, Lcom/flurry/android/bf;->t:Ljava/lang/String;

    .line 535
    return-void
.end method

.method final c(Ljava/util/Map;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 613
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 615
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 617
    iget-object v2, p0, Lcom/flurry/android/bf;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 620
    :cond_0
    return-void
.end method

.method final c([B)V
    .locals 3
    .parameter

    .prologue
    .line 1382
    const-class v0, Lcom/flurry/android/AdResponse;

    invoke-static {p1, v0}, Lcom/flurry/android/bf;->a([BLjava/lang/Class;)Lorg/apache/avro/specific/SpecificRecordBase;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdResponse;

    .line 1383
    if-eqz v0, :cond_0

    .line 1385
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got an AdResponse:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1387
    invoke-direct {p0}, Lcom/flurry/android/bf;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    invoke-virtual {v1, v0}, Lcom/flurry/android/x;->a(Lcom/flurry/android/AdResponse;)V

    .line 1389
    :cond_0
    return-void
.end method

.method final declared-synchronized d()J
    .locals 4

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/flurry/android/bf;->C:J

    sub-long/2addr v0, v2

    .line 289
    iget-wide v2, p0, Lcom/flurry/android/bf;->D:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    :goto_0
    iput-wide v0, p0, Lcom/flurry/android/bf;->D:J

    .line 291
    iget-wide v0, p0, Lcom/flurry/android/bf;->D:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    .line 289
    :cond_0
    :try_start_1
    iget-wide v0, p0, Lcom/flurry/android/bf;->D:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/flurry/android/bf;->D:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final d(Ljava/lang/String;)Lcom/flurry/android/AdUnit;
    .locals 1
    .parameter

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/flurry/android/bf;->c:Lcom/flurry/android/x;

    invoke-virtual {v0, p1}, Lcom/flurry/android/x;->a(Ljava/lang/String;)Lcom/flurry/android/AdUnit;

    move-result-object v0

    .line 1074
    return-object v0
.end method

.method final e()Lcom/flurry/android/IListener;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/flurry/android/bf;->I:Lcom/flurry/android/IListener;

    return-object v0
.end method

.method final e(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 1113
    invoke-direct {p0}, Lcom/flurry/android/bf;->q()[B

    move-result-object v0

    .line 1115
    if-eqz v0, :cond_0

    .line 1117
    invoke-direct {p0, v0, p1}, Lcom/flurry/android/bf;->a([BLjava/lang/String;)Z

    .line 1121
    :cond_0
    return-void
.end method

.method final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/flurry/android/bf;->F:Ljava/lang/String;

    return-object v0
.end method

.method final g()V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flurry/android/bf;->O:Ljava/util/Map;

    .line 353
    return-void
.end method

.method final h()Z
    .locals 1

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/flurry/android/bf;->J:Z

    return v0
.end method

.method final i()V
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/flurry/android/bf;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 626
    return-void
.end method

.method final j()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 631
    iget-object v0, p0, Lcom/flurry/android/bf;->w:Ljava/util/Map;

    return-object v0
.end method

.method final declared-synchronized k()V
    .locals 2

    .prologue
    .line 1098
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flurry/android/f;

    invoke-direct {v0, p0}, Lcom/flurry/android/f;-><init>(Lcom/flurry/android/bf;)V

    .line 1107
    iget-object v1, p0, Lcom/flurry/android/bf;->G:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1108
    monitor-exit p0

    return-void

    .line 1098
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized l()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1394
    monitor-enter p0

    .line 1398
    :try_start_0
    iget-object v0, p0, Lcom/flurry/android/bf;->l:Ljava/io/File;

    invoke-static {v0}, Lcom/flurry/android/ae;->a(Ljava/io/File;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1399
    if-nez v0, :cond_0

    .line 1416
    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Lcom/flurry/android/z;->a(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1417
    :goto_0
    monitor-exit p0

    return-void

    .line 1404
    :cond_0
    :try_start_2
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/flurry/android/bf;->l:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1405
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1406
    const v0, 0xb5fa

    :try_start_3
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1407
    iget-object v0, p0, Lcom/flurry/android/bf;->K:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/flurry/android/bf;->a(Ljava/util/List;Ljava/io/DataOutputStream;)V

    .line 1408
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 1416
    :try_start_4
    invoke-static {v1}, Lcom/flurry/android/z;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1410
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1412
    :goto_1
    :try_start_5
    sget-object v2, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v2, v3, v0}, Lcom/flurry/android/bd;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1416
    :try_start_6
    invoke-static {v1}, Lcom/flurry/android/z;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/flurry/android/z;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_2

    .line 1410
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method final declared-synchronized m()V
    .locals 2

    .prologue
    .line 1423
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/flurry/android/k;

    invoke-direct {v0, p0}, Lcom/flurry/android/k;-><init>(Lcom/flurry/android/bf;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/flurry/android/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1487
    monitor-exit p0

    return-void

    .line 1423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final n()Lcom/flurry/android/bc;
    .locals 1

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/flurry/android/bf;->y:Lcom/flurry/android/bc;

    return-object v0
.end method

.method final o()Lcom/flurry/android/AdUnit;
    .locals 1

    .prologue
    .line 1558
    iget-object v0, p0, Lcom/flurry/android/bf;->z:Lcom/flurry/android/AdUnit;

    return-object v0
.end method
