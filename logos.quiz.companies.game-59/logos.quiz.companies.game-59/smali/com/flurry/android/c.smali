.class final Lcom/flurry/android/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:Landroid/content/Context;

.field private synthetic c:Z

.field private synthetic d:Lcom/flurry/android/bf;


# direct methods
.method constructor <init>(Lcom/flurry/android/bf;Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 672
    iput-object p1, p0, Lcom/flurry/android/c;->d:Lcom/flurry/android/bf;

    iput-object p2, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/flurry/android/c;->b:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flurry/android/c;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 677
    iget-object v0, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 679
    iget-object v0, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    const-string v1, "market://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/flurry/android/c;->d:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 703
    :goto_0
    return-void

    .line 685
    :cond_0
    iget-boolean v0, p0, Lcom/flurry/android/c;->c:Z

    if-eqz v0, :cond_1

    .line 687
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/flurry/android/c;->b:Landroid/content/Context;

    const-class v2, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 688
    const-string v1, "url"

    iget-object v2, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 689
    iget-object v1, p0, Lcom/flurry/android/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 693
    :cond_1
    iget-object v0, p0, Lcom/flurry/android/c;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 700
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to launch intent for: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/flurry/android/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 701
    sget-object v1, Lcom/flurry/android/bf;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/flurry/android/bd;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
