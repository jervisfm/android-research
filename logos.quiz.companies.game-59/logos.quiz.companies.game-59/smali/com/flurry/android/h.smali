.class final Lcom/flurry/android/h;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:[B

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private synthetic e:Lcom/flurry/android/bf;


# direct methods
.method public constructor <init>(Lcom/flurry/android/bf;[BLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1966
    iput-object p1, p0, Lcom/flurry/android/h;->e:Lcom/flurry/android/bf;

    .line 1967
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1960
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/flurry/android/h;->a:Ljava/lang/String;

    .line 1961
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flurry/android/h;->b:[B

    .line 1962
    const-string v0, ""

    iput-object v0, p0, Lcom/flurry/android/h;->c:Ljava/lang/String;

    .line 1963
    const-string v0, ""

    iput-object v0, p0, Lcom/flurry/android/h;->d:Ljava/lang/String;

    .line 1968
    iput-object p2, p0, Lcom/flurry/android/h;->b:[B

    .line 1969
    iput-object p3, p0, Lcom/flurry/android/h;->c:Ljava/lang/String;

    .line 1970
    iput-object p4, p0, Lcom/flurry/android/h;->d:Ljava/lang/String;

    .line 1971
    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 7

    .prologue
    .line 1976
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    iget-object v1, p0, Lcom/flurry/android/h;->b:[B

    invoke-direct {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 1979
    const-string v1, "avro/binary"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 1980
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    iget-object v2, p0, Lcom/flurry/android/h;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1981
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1982
    const-string v2, "accept"

    const-string v3, "avro/binary"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1983
    const-string v2, "FM-Checksum"

    iget-object v3, p0, Lcom/flurry/android/h;->b:[B

    invoke-static {v3}, Lcom/flurry/android/bf;->a([B)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1984
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 1985
    const/16 v3, 0x2710

    invoke-static {v2, v3}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1986
    const/16 v3, 0x3a98

    invoke-static {v2, v3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1987
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpPost;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    const-string v4, "http.protocol.expect-continue"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 1989
    iget-object v3, p0, Lcom/flurry/android/h;->e:Lcom/flurry/android/bf;

    invoke-static {v3}, Lcom/flurry/android/bf;->c(Lcom/flurry/android/bf;)Lcom/flurry/android/ay;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/flurry/android/ay;->a(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    .line 1993
    :try_start_0
    invoke-interface {v2, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 1994
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 1995
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_3

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 1997
    iget-object v2, p0, Lcom/flurry/android/h;->a:Ljava/lang/String;

    const-string v3, "Request successful"

    invoke-static {v2, v3}, Lcom/flurry/android/bd;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1998
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/flurry/android/bf;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 2000
    invoke-virtual {v0}, Lorg/apache/http/entity/ByteArrayEntity;->consumeContent()V

    .line 2001
    invoke-static {v2}, Lcom/flurry/android/bf;->a([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2002
    const-string v3, "FM-Checksum"

    invoke-interface {v1, v3}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "FM-Checksum"

    invoke-interface {v1, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2004
    :cond_0
    iget-object v0, p0, Lcom/flurry/android/h;->d:Ljava/lang/String;

    const-string v1, "/v3/getAds.do"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2006
    iget-object v0, p0, Lcom/flurry/android/h;->e:Lcom/flurry/android/bf;

    invoke-virtual {v0, v2}, Lcom/flurry/android/bf;->c([B)V

    .line 2027
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2010
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/h;->e:Lcom/flurry/android/bf;

    invoke-virtual {v0, v2}, Lcom/flurry/android/bf;->b([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2023
    :catch_0
    move-exception v0

    .line 2025
    iget-object v1, p0, Lcom/flurry/android/h;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/flurry/android/bd;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2020
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/flurry/android/h;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Report failed. HTTP response: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/android/bd;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1958
    invoke-direct {p0}, Lcom/flurry/android/h;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 1958
    return-void
.end method
