.class final Lcom/flurry/android/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ads/AdListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/y;


# direct methods
.method constructor <init>(Lcom/flurry/android/y;)V
    .locals 0
    .parameter

    .prologue
    .line 153
    iput-object p1, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 179
    const-string v0, "FlurryAgent"

    const-string v1, "Admob Interstitial dismissed from screen."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void
.end method

.method public final onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 166
    iget-object v0, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdUnFilled(Ljava/util/Map;)V

    .line 167
    const-string v0, "FlurryAgent"

    const-string v1, "Admob Interstitial failed to receive takeover."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    return-void
.end method

.method public final onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/y;->onAdClicked(Ljava/util/Map;)V

    .line 186
    const-string v0, "FlurryAgent"

    const-string v1, "Admob Interstitial leave application."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    return-void
.end method

.method public final onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter

    .prologue
    .line 173
    const-string v0, "FlurryAgent"

    const-string v1, "Admob Interstitial present on screen."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method

.method public final onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 157
    iget-object v0, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    invoke-virtual {v0, v2}, Lcom/flurry/android/y;->onAdFilled(Ljava/util/Map;)V

    .line 158
    const-string v0, "FlurryAgent"

    const-string v1, "Admob Interstitial received takeover."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    invoke-virtual {v0, v2}, Lcom/flurry/android/y;->onAdShown(Ljava/util/Map;)V

    .line 160
    iget-object v0, p0, Lcom/flurry/android/j;->a:Lcom/flurry/android/y;

    invoke-static {v0}, Lcom/flurry/android/y;->a(Lcom/flurry/android/y;)Lcom/google/ads/InterstitialAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ads/InterstitialAd;->show()V

    .line 161
    return-void
.end method
