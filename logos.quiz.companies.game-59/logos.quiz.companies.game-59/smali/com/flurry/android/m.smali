.class final Lcom/flurry/android/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/inmobi/androidsdk/IMAdInterstitialListener;


# instance fields
.field private synthetic a:Lcom/flurry/android/ab;


# direct methods
.method constructor <init>(Lcom/flurry/android/ab;)V
    .locals 0
    .parameter

    .prologue
    .line 159
    iput-object p1, p0, Lcom/flurry/android/m;->a:Lcom/flurry/android/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAdRequestFailed(Lcom/inmobi/androidsdk/IMAdInterstitial;Lcom/inmobi/androidsdk/IMAdRequest$ErrorCode;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/flurry/android/m;->a:Lcom/flurry/android/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdUnFilled(Ljava/util/Map;)V

    .line 165
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi imAdView ad request failed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    return-void
.end method

.method public final onAdRequestLoaded(Lcom/inmobi/androidsdk/IMAdInterstitial;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/flurry/android/m;->a:Lcom/flurry/android/ab;

    invoke-virtual {v0, v2}, Lcom/flurry/android/ab;->onAdFilled(Ljava/util/Map;)V

    .line 172
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi Interstitial ad request completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    sget-object v0, Lcom/inmobi/androidsdk/IMAdInterstitial$State;->READY:Lcom/inmobi/androidsdk/IMAdInterstitial$State;

    invoke-virtual {p1}, Lcom/inmobi/androidsdk/IMAdInterstitial;->getState()Lcom/inmobi/androidsdk/IMAdInterstitial$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/inmobi/androidsdk/IMAdInterstitial$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/flurry/android/m;->a:Lcom/flurry/android/ab;

    invoke-virtual {v0, v2}, Lcom/flurry/android/ab;->onAdShown(Ljava/util/Map;)V

    .line 175
    invoke-virtual {p1}, Lcom/inmobi/androidsdk/IMAdInterstitial;->show()V

    .line 177
    :cond_0
    return-void
.end method

.method public final onDismissAdScreen(Lcom/inmobi/androidsdk/IMAdInterstitial;)V
    .locals 2
    .parameter

    .prologue
    .line 182
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi Interstitial ad dismissed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void
.end method

.method public final onShowAdScreen(Lcom/inmobi/androidsdk/IMAdInterstitial;)V
    .locals 2
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lcom/flurry/android/m;->a:Lcom/flurry/android/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flurry/android/ab;->onAdClicked(Ljava/util/Map;)V

    .line 189
    const-string v0, "FlurryAgent"

    const-string v1, "InMobi Interstitial ad shown."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    return-void
.end method
