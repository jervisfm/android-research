.class abstract Lcom/flurry/android/p;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field a:Lcom/flurry/android/bf;

.field b:Lcom/flurry/android/bc;

.field c:Lcom/flurry/android/AdUnit;

.field d:I

.field private e:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-direct {p0, p1, v0, v0}, Lcom/flurry/android/p;->a(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V

    .line 31
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/flurry/android/p;->a(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V

    .line 25
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/flurry/android/bf;Lcom/flurry/android/bc;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/flurry/android/p;->e:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/flurry/android/p;->a:Lcom/flurry/android/bf;

    .line 37
    iput-object p3, p0, Lcom/flurry/android/p;->b:Lcom/flurry/android/bc;

    .line 38
    return-void
.end method


# virtual methods
.method final a(Landroid/view/ViewGroup;Lcom/flurry/android/u;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 93
    iget-object v0, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v6, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    instance-of v0, p1, Landroid/widget/RelativeLayout;

    if-nez v0, :cond_2

    .line 100
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 102
    invoke-virtual {p2, v0}, Lcom/flurry/android/u;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    .line 107
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/flurry/android/p;->e:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/flurry/android/z;->b(Landroid/content/Context;I)I

    move-result v2

    iget-object v3, p0, Lcom/flurry/android/p;->e:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v3, v4}, Lcom/flurry/android/z;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 108
    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->e()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 110
    array-length v2, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 113
    aget-object v2, v0, v5

    const-string v3, "b"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 115
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 127
    :cond_3
    :goto_1
    aget-object v2, v0, v6

    const-string v3, "c"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 129
    const/16 v0, 0xe

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 141
    :cond_4
    :goto_2
    invoke-virtual {p2, v1}, Lcom/flurry/android/u;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 117
    :cond_5
    aget-object v2, v0, v5

    const-string v3, "t"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 119
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_1

    .line 121
    :cond_6
    aget-object v2, v0, v5

    const-string v3, "m"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 123
    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_1

    .line 131
    :cond_7
    aget-object v2, v0, v6

    const-string v3, "l"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 133
    const/16 v0, 0x9

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_2

    .line 135
    :cond_8
    aget-object v0, v0, v6

    const-string v2, "r"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 137
    const/16 v0, 0xb

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_2
.end method

.method final a(Ljava/lang/String;ZLjava/util/Map;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AppSpotBannerView.onEvent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    iget-object v0, p0, Lcom/flurry/android/p;->a:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/p;->b:Lcom/flurry/android/bc;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bc;Ljava/lang/String;ZLjava/util/Map;)Lcom/flurry/android/bc;

    .line 74
    iget-object v0, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    if-eqz v0, :cond_0

    .line 76
    iget-object v6, p0, Lcom/flurry/android/p;->a:Lcom/flurry/android/bf;

    new-instance v0, Lcom/flurry/android/bb;

    iget-object v2, p0, Lcom/flurry/android/p;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/flurry/android/p;->c:Lcom/flurry/android/AdUnit;

    iget-object v4, p0, Lcom/flurry/android/p;->b:Lcom/flurry/android/bc;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/flurry/android/bb;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V

    iget-object v1, p0, Lcom/flurry/android/p;->a:Lcom/flurry/android/bf;

    invoke-virtual {v6, v0, v1}, Lcom/flurry/android/bf;->a(Lcom/flurry/android/bb;Lcom/flurry/android/aj;)V

    .line 80
    :cond_0
    return-void
.end method

.method public abstract initLayout(Landroid/content/Context;)V
.end method
