.class final Lcom/flurry/android/u;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/flurry/android/bf;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Landroid/view/ViewGroup;

.field private e:I

.field private f:Z


# direct methods
.method constructor <init>(Lcom/flurry/android/bf;Landroid/content/Context;Ljava/lang/String;Landroid/view/ViewGroup;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p1, p0, Lcom/flurry/android/u;->a:Lcom/flurry/android/bf;

    .line 28
    iput-object p2, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    .line 29
    iput-object p3, p0, Lcom/flurry/android/u;->c:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lcom/flurry/android/u;->d:Landroid/view/ViewGroup;

    .line 31
    iput v0, p0, Lcom/flurry/android/u;->e:I

    .line 32
    iput-boolean v0, p0, Lcom/flurry/android/u;->f:Z

    .line 34
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/flurry/android/u;->e:I

    return v0
.end method

.method final a(J)Lcom/flurry/android/as;
    .locals 10
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 39
    .line 42
    iget-object v0, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/flurry/android/u;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(Landroid/content/Context;I)I

    move-result v3

    iget-object v0, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/flurry/android/u;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/flurry/android/z;->a(Landroid/content/Context;I)I

    move-result v4

    iget-object v0, p0, Lcom/flurry/android/u;->a:Lcom/flurry/android/bf;

    iget-object v1, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/flurry/android/u;->c:Ljava/lang/String;

    move-wide v6, p1

    invoke-virtual/range {v0 .. v7}, Lcom/flurry/android/bf;->a(Landroid/content/Context;Ljava/lang/String;IIZJ)Lcom/flurry/android/r;

    move-result-object v1

    .line 43
    iget-object v0, p0, Lcom/flurry/android/u;->a:Lcom/flurry/android/bf;

    invoke-virtual {v0}, Lcom/flurry/android/bf;->e()Lcom/flurry/android/IListener;

    move-result-object v2

    .line 44
    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v1}, Lcom/flurry/android/r;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 46
    invoke-virtual {p0}, Lcom/flurry/android/u;->removeAllViews()V

    .line 47
    invoke-virtual {v1}, Lcom/flurry/android/r;->a()Lcom/flurry/android/AdUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->d()Lcom/flurry/android/AdSpaceLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdSpaceLayout;->d()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "takeover"

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    .line 48
    :goto_0
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/flurry/android/u;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/flurry/android/IListener;->shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/flurry/android/u;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 52
    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v0

    iget-object v2, p0, Lcom/flurry/android/u;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, p0}, Lcom/flurry/android/p;->a(Landroid/view/ViewGroup;Lcom/flurry/android/u;)V

    .line 54
    :cond_1
    invoke-virtual {v1}, Lcom/flurry/android/r;->b()Lcom/flurry/android/p;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/flurry/android/u;->addView(Landroid/view/View;)V

    .line 55
    iput v5, p0, Lcom/flurry/android/u;->e:I

    .line 70
    :cond_2
    :goto_1
    new-instance v0, Lcom/flurry/android/as;

    invoke-direct {v0, v8, v5}, Lcom/flurry/android/as;-><init>(ZZ)V

    return-object v0

    .line 47
    :cond_3
    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_BANNER:Lcom/flurry/android/FlurryAdType;

    goto :goto_0

    .line 59
    :cond_4
    invoke-virtual {v1}, Lcom/flurry/android/r;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 61
    invoke-virtual {v1}, Lcom/flurry/android/r;->a()Lcom/flurry/android/AdUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/android/AdUnit;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/android/AdFrame;

    invoke-virtual {v0}, Lcom/flurry/android/AdFrame;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/flurry/android/FlurryAdType;->VIDEO_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    .line 62
    :goto_2
    if-eqz v2, :cond_5

    iget-object v1, p0, Lcom/flurry/android/u;->c:Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lcom/flurry/android/IListener;->shouldDisplayAd(Ljava/lang/String;Lcom/flurry/android/FlurryAdType;)Z

    .line 64
    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    iget-object v1, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    const-class v2, Lcom/flurry/android/FlurryFullscreenTakeoverActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 66
    iget-object v1, p0, Lcom/flurry/android/u;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v9, v8

    move v8, v5

    move v5, v9

    .line 68
    goto :goto_1

    .line 61
    :cond_6
    sget-object v0, Lcom/flurry/android/FlurryAdType;->WEB_TAKEOVER:Lcom/flurry/android/FlurryAdType;

    goto :goto_2

    :cond_7
    move v8, v5

    goto :goto_1
.end method

.method final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput p1, p0, Lcom/flurry/android/u;->e:I

    .line 76
    return-void
.end method

.method final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/flurry/android/u;->f:Z

    .line 86
    return-void
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/flurry/android/u;->f:Z

    return v0
.end method

.method final c()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/flurry/android/u;->d:Landroid/view/ViewGroup;

    return-object v0
.end method

.method final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/flurry/android/u;->c:Ljava/lang/String;

    return-object v0
.end method
