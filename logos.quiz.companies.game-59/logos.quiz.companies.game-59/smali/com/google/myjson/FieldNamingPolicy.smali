.class public final enum Lcom/google/myjson/FieldNamingPolicy;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/myjson/FieldNamingPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum LOWER_CASE_WITH_DASHES:Lcom/google/myjson/FieldNamingPolicy;

.field public static final enum LOWER_CASE_WITH_UNDERSCORES:Lcom/google/myjson/FieldNamingPolicy;

.field public static final enum UPPER_CAMEL_CASE:Lcom/google/myjson/FieldNamingPolicy;

.field public static final enum UPPER_CAMEL_CASE_WITH_SPACES:Lcom/google/myjson/FieldNamingPolicy;

.field private static final synthetic b:[Lcom/google/myjson/FieldNamingPolicy;


# instance fields
.field private final a:Lcom/google/myjson/l;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/myjson/FieldNamingPolicy;

    const-string v1, "UPPER_CAMEL_CASE"

    new-instance v2, Lcom/google/myjson/ModifyFirstLetterNamingPolicy;

    sget-object v3, Lcom/google/myjson/ModifyFirstLetterNamingPolicy$LetterModifier;->UPPER:Lcom/google/myjson/ModifyFirstLetterNamingPolicy$LetterModifier;

    invoke-direct {v2, v3}, Lcom/google/myjson/ModifyFirstLetterNamingPolicy;-><init>(Lcom/google/myjson/ModifyFirstLetterNamingPolicy$LetterModifier;)V

    invoke-direct {v0, v1, v4, v2}, Lcom/google/myjson/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/l;)V

    sput-object v0, Lcom/google/myjson/FieldNamingPolicy;->UPPER_CAMEL_CASE:Lcom/google/myjson/FieldNamingPolicy;

    new-instance v0, Lcom/google/myjson/FieldNamingPolicy;

    const-string v1, "UPPER_CAMEL_CASE_WITH_SPACES"

    new-instance v2, Lcom/google/myjson/h;

    const-string v3, " "

    invoke-direct {v2, v3}, Lcom/google/myjson/h;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v5, v2}, Lcom/google/myjson/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/l;)V

    sput-object v0, Lcom/google/myjson/FieldNamingPolicy;->UPPER_CAMEL_CASE_WITH_SPACES:Lcom/google/myjson/FieldNamingPolicy;

    new-instance v0, Lcom/google/myjson/FieldNamingPolicy;

    const-string v1, "LOWER_CASE_WITH_UNDERSCORES"

    new-instance v2, Lcom/google/myjson/b;

    const-string v3, "_"

    invoke-direct {v2, v3}, Lcom/google/myjson/b;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v6, v2}, Lcom/google/myjson/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/l;)V

    sput-object v0, Lcom/google/myjson/FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/google/myjson/FieldNamingPolicy;

    new-instance v0, Lcom/google/myjson/FieldNamingPolicy;

    const-string v1, "LOWER_CASE_WITH_DASHES"

    new-instance v2, Lcom/google/myjson/b;

    const-string v3, "-"

    invoke-direct {v2, v3}, Lcom/google/myjson/b;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v7, v2}, Lcom/google/myjson/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/l;)V

    sput-object v0, Lcom/google/myjson/FieldNamingPolicy;->LOWER_CASE_WITH_DASHES:Lcom/google/myjson/FieldNamingPolicy;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/myjson/FieldNamingPolicy;

    sget-object v1, Lcom/google/myjson/FieldNamingPolicy;->UPPER_CAMEL_CASE:Lcom/google/myjson/FieldNamingPolicy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/myjson/FieldNamingPolicy;->UPPER_CAMEL_CASE_WITH_SPACES:Lcom/google/myjson/FieldNamingPolicy;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/myjson/FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/google/myjson/FieldNamingPolicy;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/myjson/FieldNamingPolicy;->LOWER_CASE_WITH_DASHES:Lcom/google/myjson/FieldNamingPolicy;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/myjson/FieldNamingPolicy;->b:[Lcom/google/myjson/FieldNamingPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/myjson/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/l;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/myjson/FieldNamingPolicy;->a:Lcom/google/myjson/l;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/myjson/FieldNamingPolicy;
    .locals 1

    const-class v0, Lcom/google/myjson/FieldNamingPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/FieldNamingPolicy;

    return-object v0
.end method

.method public static final values()[Lcom/google/myjson/FieldNamingPolicy;
    .locals 1

    sget-object v0, Lcom/google/myjson/FieldNamingPolicy;->b:[Lcom/google/myjson/FieldNamingPolicy;

    invoke-virtual {v0}, [Lcom/google/myjson/FieldNamingPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/myjson/FieldNamingPolicy;

    return-object v0
.end method


# virtual methods
.method a()Lcom/google/myjson/l;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/FieldNamingPolicy;->a:Lcom/google/myjson/l;

    return-object v0
.end method
