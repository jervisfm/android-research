.class Lcom/google/myjson/Gson$4;
.super Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/myjson/Gson;-><init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/l;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZLcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/myjson/LongSerializationPolicy;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/myjson/l;

.field final synthetic b:Lcom/google/myjson/Gson;


# direct methods
.method constructor <init>(Lcom/google/myjson/Gson;Lcom/google/myjson/internal/ConstructorConstructor;Lcom/google/myjson/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/myjson/Gson$4;->b:Lcom/google/myjson/Gson;

    iput-object p3, p0, Lcom/google/myjson/Gson$4;->a:Lcom/google/myjson/l;

    invoke-direct {p0, p2}, Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    return-void
.end method


# virtual methods
.method public deserializeField(Ljava/lang/Class;Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/reflect/Type;",
            ")Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/Gson$4;->b:Lcom/google/myjson/Gson;

    invoke-static {v0}, Lcom/google/myjson/Gson;->b(Lcom/google/myjson/Gson;)Lcom/google/myjson/ExclusionStrategy;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipClass(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/myjson/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/myjson/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipField(Lcom/google/myjson/FieldAttributes;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFieldName(Ljava/lang/Class;Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/Gson$4;->a:Lcom/google/myjson/l;

    new-instance v1, Lcom/google/myjson/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/myjson/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/myjson/l;->translateName(Lcom/google/myjson/FieldAttributes;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public serializeField(Ljava/lang/Class;Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/reflect/Type;",
            ")Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/Gson$4;->b:Lcom/google/myjson/Gson;

    invoke-static {v0}, Lcom/google/myjson/Gson;->a(Lcom/google/myjson/Gson;)Lcom/google/myjson/ExclusionStrategy;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipClass(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/myjson/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/myjson/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipField(Lcom/google/myjson/FieldAttributes;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
