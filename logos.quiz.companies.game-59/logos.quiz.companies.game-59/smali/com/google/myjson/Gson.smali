.class public final Lcom/google/myjson/Gson;
.super Ljava/lang/Object;


# static fields
.field static final a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

.field static final b:Lcom/google/myjson/q;

.field static final c:Lcom/google/myjson/g;

.field static final d:Lcom/google/myjson/n;

.field static final e:Lcom/google/myjson/l;

.field private static final f:Lcom/google/myjson/ExclusionStrategy;


# instance fields
.field private final g:Lcom/google/myjson/ExclusionStrategy;

.field private final h:Lcom/google/myjson/ExclusionStrategy;

.field private final i:Lcom/google/myjson/internal/ConstructorConstructor;

.field private final j:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Lcom/google/myjson/internal/bind/MiniGson;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;-><init>()V

    invoke-virtual {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->makeUnmodifiable()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/Gson;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v0, Lcom/google/myjson/q;

    invoke-direct {v0}, Lcom/google/myjson/q;-><init>()V

    sput-object v0, Lcom/google/myjson/Gson;->b:Lcom/google/myjson/q;

    new-instance v0, Lcom/google/myjson/g;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/myjson/g;-><init>(Z)V

    sput-object v0, Lcom/google/myjson/Gson;->c:Lcom/google/myjson/g;

    new-instance v0, Lcom/google/myjson/n;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/google/myjson/n;-><init>([I)V

    sput-object v0, Lcom/google/myjson/Gson;->d:Lcom/google/myjson/n;

    new-instance v0, Lcom/google/myjson/f;

    new-instance v1, Lcom/google/myjson/k;

    invoke-direct {v1}, Lcom/google/myjson/k;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/myjson/f;-><init>(Lcom/google/myjson/l;)V

    sput-object v0, Lcom/google/myjson/Gson;->e:Lcom/google/myjson/l;

    invoke-static {}, Lcom/google/myjson/Gson;->a()Lcom/google/myjson/ExclusionStrategy;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/Gson;->f:Lcom/google/myjson/ExclusionStrategy;

    return-void

    :array_0
    .array-data 0x4
        0x80t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 15

    const/4 v5, 0x0

    sget-object v1, Lcom/google/myjson/Gson;->f:Lcom/google/myjson/ExclusionStrategy;

    sget-object v2, Lcom/google/myjson/Gson;->f:Lcom/google/myjson/ExclusionStrategy;

    sget-object v3, Lcom/google/myjson/Gson;->e:Lcom/google/myjson/l;

    sget-object v4, Lcom/google/myjson/Gson;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    sget-object v6, Lcom/google/myjson/Gson;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    sget-object v7, Lcom/google/myjson/Gson;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    const/4 v10, 0x1

    sget-object v13, Lcom/google/myjson/LongSerializationPolicy;->DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v14

    move-object v0, p0

    move v8, v5

    move v9, v5

    move v11, v5

    move v12, v5

    invoke-direct/range {v0 .. v14}, Lcom/google/myjson/Gson;-><init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/l;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZLcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/myjson/LongSerializationPolicy;Ljava/util/List;)V

    return-void
.end method

.method constructor <init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/l;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZLcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/myjson/LongSerializationPolicy;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/ExclusionStrategy;",
            "Lcom/google/myjson/ExclusionStrategy;",
            "Lcom/google/myjson/l;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/InstanceCreator",
            "<*>;>;Z",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;ZZZZZ",
            "Lcom/google/myjson/LongSerializationPolicy;",
            "Ljava/util/List",
            "<",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/myjson/Gson;->g:Lcom/google/myjson/ExclusionStrategy;

    iput-object p2, p0, Lcom/google/myjson/Gson;->h:Lcom/google/myjson/ExclusionStrategy;

    new-instance v1, Lcom/google/myjson/internal/ConstructorConstructor;

    invoke-direct {v1, p4}, Lcom/google/myjson/internal/ConstructorConstructor;-><init>(Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V

    iput-object v1, p0, Lcom/google/myjson/Gson;->i:Lcom/google/myjson/internal/ConstructorConstructor;

    iput-boolean p5, p0, Lcom/google/myjson/Gson;->l:Z

    iput-object p6, p0, Lcom/google/myjson/Gson;->j:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    iput-object p7, p0, Lcom/google/myjson/Gson;->k:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    iput-boolean p9, p0, Lcom/google/myjson/Gson;->n:Z

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/myjson/Gson;->m:Z

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/myjson/Gson;->o:Z

    new-instance v2, Lcom/google/myjson/Gson$4;

    iget-object v1, p0, Lcom/google/myjson/Gson;->i:Lcom/google/myjson/internal/ConstructorConstructor;

    invoke-direct {v2, p0, v1, p3}, Lcom/google/myjson/Gson$4;-><init>(Lcom/google/myjson/Gson;Lcom/google/myjson/internal/ConstructorConstructor;Lcom/google/myjson/l;)V

    new-instance v1, Lcom/google/myjson/internal/bind/MiniGson$Builder;

    invoke-direct {v1}, Lcom/google/myjson/internal/bind/MiniGson$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->withoutDefaultFactories()Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->INTEGER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->BOOLEAN_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->BYTE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->SHORT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Long;

    move-object/from16 v0, p13

    invoke-direct {p0, v0}, Lcom/google/myjson/Gson;->a(Lcom/google/myjson/LongSerializationPolicy;)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Double;

    move/from16 v0, p12

    invoke-direct {p0, v0}, Lcom/google/myjson/Gson;->a(Z)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Float;

    move/from16 v0, p12

    invoke-direct {p0, v0}, Lcom/google/myjson/Gson;->b(Z)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v3, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;

    invoke-direct {v3, p2, p1}, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;-><init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;)V

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->NUMBER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->CHARACTER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUILDER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUFFER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    const-class v3, Ljava/math/BigDecimal;

    new-instance v4, Lcom/google/myjson/internal/bind/BigDecimalTypeAdapter;

    invoke-direct {v4}, Lcom/google/myjson/internal/bind/BigDecimalTypeAdapter;-><init>()V

    invoke-virtual {v1, v3, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->typeAdapter(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    const-class v3, Ljava/math/BigInteger;

    new-instance v4, Lcom/google/myjson/internal/bind/BigIntegerTypeAdapter;

    invoke-direct {v4}, Lcom/google/myjson/internal/bind/BigIntegerTypeAdapter;-><init>()V

    invoke-virtual {v1, v3, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->typeAdapter(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/TypeAdapters;->JSON_ELEMENT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/myjson/internal/bind/ObjectTypeAdapter;->FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v3

    invoke-interface/range {p14 .. p14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v3, v1}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/myjson/j;

    invoke-direct {v1, p0, p6, p7}, Lcom/google/myjson/j;-><init>(Lcom/google/myjson/Gson;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V

    invoke-virtual {v3, v1}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v4, Lcom/google/myjson/internal/bind/CollectionTypeAdapterFactory;

    iget-object v5, p0, Lcom/google/myjson/Gson;->i:Lcom/google/myjson/internal/ConstructorConstructor;

    invoke-direct {v4, v5}, Lcom/google/myjson/internal/bind/CollectionTypeAdapterFactory;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->URL_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->URI_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->UUID_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->LOCALE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->INET_ADDRESS_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->BIT_SET_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/DateTypeAdapter;->FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->CALENDAR_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TimeTypeAdapter;->FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/SqlDateTypeAdapter;->FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->TIMESTAMP_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v4, Lcom/google/myjson/internal/bind/MapTypeAdapterFactory;

    iget-object v5, p0, Lcom/google/myjson/Gson;->i:Lcom/google/myjson/internal/ConstructorConstructor;

    invoke-direct {v4, v5, p8}, Lcom/google/myjson/internal/bind/MapTypeAdapterFactory;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;Z)V

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/ArrayTypeAdapter;->FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/myjson/internal/bind/TypeAdapters;->ENUM_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->factory(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;)Lcom/google/myjson/internal/bind/MiniGson$Builder;

    invoke-virtual {v3}, Lcom/google/myjson/internal/bind/MiniGson$Builder;->build()Lcom/google/myjson/internal/bind/MiniGson;

    move-result-object v1

    iput-object v1, p0, Lcom/google/myjson/Gson;->p:Lcom/google/myjson/internal/bind/MiniGson;

    return-void
.end method

.method private static a()Lcom/google/myjson/ExclusionStrategy;
    .locals 2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sget-object v1, Lcom/google/myjson/Gson;->b:Lcom/google/myjson/q;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/myjson/Gson;->c:Lcom/google/myjson/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/myjson/Gson;->d:Lcom/google/myjson/n;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/myjson/t;

    invoke-direct {v1, v0}, Lcom/google/myjson/t;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/myjson/Gson;)Lcom/google/myjson/ExclusionStrategy;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/Gson;->h:Lcom/google/myjson/ExclusionStrategy;

    return-object v0
.end method

.method private a(Lcom/google/myjson/LongSerializationPolicy;)Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/LongSerializationPolicy;",
            ")",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/LongSerializationPolicy;->DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->LONG:Lcom/google/myjson/internal/bind/TypeAdapter;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/Gson$1;

    invoke-direct {v0, p0}, Lcom/google/myjson/Gson$1;-><init>(Lcom/google/myjson/Gson;)V

    goto :goto_0
.end method

.method private a(Z)Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->DOUBLE:Lcom/google/myjson/internal/bind/TypeAdapter;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/Gson$3;

    invoke-direct {v0, p0}, Lcom/google/myjson/Gson$3;-><init>(Lcom/google/myjson/Gson;)V

    goto :goto_0
.end method

.method private a(Ljava/io/Writer;)Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/myjson/Gson;->n:Z

    if-eqz v0, :cond_0

    const-string v0, ")]}\'\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/google/myjson/stream/JsonWriter;

    invoke-direct {v0, p1}, Lcom/google/myjson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    iget-boolean v1, p0, Lcom/google/myjson/Gson;->o:Z

    if-eqz v1, :cond_1

    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/myjson/stream/JsonWriter;->setIndent(Ljava/lang/String;)V

    :cond_1
    iget-boolean v1, p0, Lcom/google/myjson/Gson;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    return-object v0
.end method

.method private a(D)V
    .locals 3

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid double value as per JSON specification. To override this"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " behavior, use GsonBuilder.serializeSpecialDoubleValues() method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/myjson/Gson;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/myjson/Gson;->a(D)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Lcom/google/myjson/stream/JsonReader;)V
    .locals 2

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->END_DOCUMENT:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/myjson/JsonIOException;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/google/myjson/JsonIOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/myjson/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/myjson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/myjson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/myjson/Gson;)Lcom/google/myjson/ExclusionStrategy;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/Gson;->g:Lcom/google/myjson/ExclusionStrategy;

    return-object v0
.end method

.method private b(Z)Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->FLOAT:Lcom/google/myjson/internal/bind/TypeAdapter;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/Gson$2;

    invoke-direct {v0, p0}, Lcom/google/myjson/Gson$2;-><init>(Lcom/google/myjson/Gson;)V

    goto :goto_0
.end method


# virtual methods
.method public fromJson(Lcom/google/myjson/JsonElement;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/JsonElement;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/myjson/Gson;->fromJson(Lcom/google/myjson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2}, Lcom/google/myjson/internal/Primitives;->wrap(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fromJson(Lcom/google/myjson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/JsonElement;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/internal/bind/JsonElementReader;

    invoke-direct {v0, p1}, Lcom/google/myjson/internal/bind/JsonElementReader;-><init>(Lcom/google/myjson/JsonElement;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/myjson/Gson;->fromJson(Lcom/google/myjson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public fromJson(Lcom/google/myjson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/stream/JsonReader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;,
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonReader;->isLenient()Z

    move-result v2

    invoke-virtual {p1, v1}, Lcom/google/myjson/stream/JsonReader;->setLenient(Z)V

    :try_start_0
    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/myjson/Gson;->p:Lcom/google/myjson/internal/bind/MiniGson;

    invoke-static {p2}, Lcom/google/myjson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/myjson/reflect/TypeToken;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/myjson/internal/bind/MiniGson;->getAdapter(Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/myjson/internal/bind/TypeAdapter;->read(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    invoke-virtual {p1, v2}, Lcom/google/myjson/stream/JsonReader;->setLenient(Z)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v2}, Lcom/google/myjson/stream/JsonReader;->setLenient(Z)V

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/myjson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v2}, Lcom/google/myjson/stream/JsonReader;->setLenient(Z)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/google/myjson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/myjson/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonSyntaxException;,
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/stream/JsonReader;

    invoke-direct {v0, p1}, Lcom/google/myjson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/myjson/Gson;->fromJson(Lcom/google/myjson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/myjson/Gson;->a(Ljava/lang/Object;Lcom/google/myjson/stream/JsonReader;)V

    invoke-static {p2}, Lcom/google/myjson/internal/Primitives;->wrap(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/Reader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;,
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/stream/JsonReader;

    invoke-direct {v0, p1}, Lcom/google/myjson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/myjson/Gson;->fromJson(Lcom/google/myjson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/myjson/Gson;->a(Ljava/lang/Object;Lcom/google/myjson/stream/JsonReader;)V

    return-object v1
.end method

.method public fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/google/myjson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2}, Lcom/google/myjson/internal/Primitives;->wrap(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonSyntaxException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/myjson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public toJson(Lcom/google/myjson/JsonElement;)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/google/myjson/Gson;->toJson(Lcom/google/myjson/JsonElement;Ljava/lang/Appendable;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJson(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/myjson/JsonNull;->INSTANCE:Lcom/google/myjson/JsonNull;

    invoke-virtual {p0, v0}, Lcom/google/myjson/Gson;->toJson(Lcom/google/myjson/JsonElement;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJson(Lcom/google/myjson/JsonElement;Lcom/google/myjson/stream/JsonWriter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/myjson/stream/JsonWriter;->isLenient()Z

    move-result v1

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p2}, Lcom/google/myjson/stream/JsonWriter;->isHtmlSafe()Z

    move-result v2

    iget-boolean v0, p0, Lcom/google/myjson/Gson;->m:Z

    invoke-virtual {p2, v0}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p2}, Lcom/google/myjson/stream/JsonWriter;->getSerializeNulls()Z

    move-result v3

    iget-boolean v0, p0, Lcom/google/myjson/Gson;->l:Z

    invoke-virtual {p2, v0}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    :try_start_0
    invoke-static {p1, p2}, Lcom/google/myjson/internal/Streams;->write(Lcom/google/myjson/JsonElement;Lcom/google/myjson/stream/JsonWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p2, v1}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p2, v2}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p2, v3}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v4, Lcom/google/myjson/JsonIOException;

    invoke-direct {v4, v0}, Lcom/google/myjson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2, v1}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p2, v2}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p2, v3}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    throw v0
.end method

.method public toJson(Lcom/google/myjson/JsonElement;Ljava/lang/Appendable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    :try_start_0
    invoke-static {p2}, Lcom/google/myjson/internal/Streams;->writerForAppendable(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/myjson/Gson;->a(Ljava/io/Writer;)Lcom/google/myjson/stream/JsonWriter;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/myjson/Gson;->toJson(Lcom/google/myjson/JsonElement;Lcom/google/myjson/stream/JsonWriter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toJson(Ljava/lang/Object;Ljava/lang/Appendable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/myjson/JsonNull;->INSTANCE:Lcom/google/myjson/JsonNull;

    invoke-virtual {p0, v0, p2}, Lcom/google/myjson/Gson;->toJson(Lcom/google/myjson/JsonElement;Ljava/lang/Appendable;)V

    goto :goto_0
.end method

.method public toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/myjson/stream/JsonWriter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/Gson;->p:Lcom/google/myjson/internal/bind/MiniGson;

    invoke-static {p2}, Lcom/google/myjson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/myjson/reflect/TypeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/myjson/internal/bind/MiniGson;->getAdapter(Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/myjson/stream/JsonWriter;->isLenient()Z

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {p3, v2}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p3}, Lcom/google/myjson/stream/JsonWriter;->isHtmlSafe()Z

    move-result v2

    iget-boolean v3, p0, Lcom/google/myjson/Gson;->m:Z

    invoke-virtual {p3, v3}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p3}, Lcom/google/myjson/stream/JsonWriter;->getSerializeNulls()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/myjson/Gson;->l:Z

    invoke-virtual {p3, v4}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    :try_start_0
    invoke-virtual {v0, p3, p1}, Lcom/google/myjson/internal/bind/TypeAdapter;->write(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p3, v1}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p3, v2}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p3, v3}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v4, Lcom/google/myjson/JsonIOException;

    invoke-direct {v4, v0}, Lcom/google/myjson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p3, v1}, Lcom/google/myjson/stream/JsonWriter;->setLenient(Z)V

    invoke-virtual {p3, v2}, Lcom/google/myjson/stream/JsonWriter;->setHtmlSafe(Z)V

    invoke-virtual {p3, v3}, Lcom/google/myjson/stream/JsonWriter;->setSerializeNulls(Z)V

    throw v0
.end method

.method public toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/myjson/JsonIOException;
        }
    .end annotation

    :try_start_0
    invoke-static {p3}, Lcom/google/myjson/internal/Streams;->writerForAppendable(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/myjson/Gson;->a(Ljava/io/Writer;)Lcom/google/myjson/stream/JsonWriter;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/myjson/stream/JsonWriter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/myjson/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/myjson/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public toJsonTree(Ljava/lang/Object;)Lcom/google/myjson/JsonElement;
    .locals 1

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/myjson/JsonNull;->INSTANCE:Lcom/google/myjson/JsonNull;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/myjson/Gson;->toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/myjson/JsonElement;

    move-result-object v0

    goto :goto_0
.end method

.method public toJsonTree(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/google/myjson/JsonElement;
    .locals 1

    new-instance v0, Lcom/google/myjson/internal/bind/JsonElementWriter;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/myjson/stream/JsonWriter;)V

    invoke-virtual {v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->get()Lcom/google/myjson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "serializeNulls:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/myjson/Gson;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/myjson/Gson;->j:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",deserializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/myjson/Gson;->k:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",instanceCreators:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/myjson/Gson;->i:Lcom/google/myjson/internal/ConstructorConstructor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
