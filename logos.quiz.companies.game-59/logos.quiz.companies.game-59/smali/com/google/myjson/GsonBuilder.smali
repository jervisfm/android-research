.class public final Lcom/google/myjson/GsonBuilder;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/google/myjson/p;

.field private static final b:Lcom/google/myjson/i;

.field private static final c:Lcom/google/myjson/m;


# instance fields
.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/myjson/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/myjson/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private f:D

.field private g:Lcom/google/myjson/n;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/myjson/LongSerializationPolicy;

.field private k:Lcom/google/myjson/l;

.field private final l:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/myjson/p;

    invoke-direct {v0}, Lcom/google/myjson/p;-><init>()V

    sput-object v0, Lcom/google/myjson/GsonBuilder;->a:Lcom/google/myjson/p;

    new-instance v0, Lcom/google/myjson/i;

    invoke-direct {v0}, Lcom/google/myjson/i;-><init>()V

    sput-object v0, Lcom/google/myjson/GsonBuilder;->b:Lcom/google/myjson/i;

    new-instance v0, Lcom/google/myjson/m;

    invoke-direct {v0}, Lcom/google/myjson/m;-><init>()V

    sput-object v0, Lcom/google/myjson/GsonBuilder;->c:Lcom/google/myjson/m;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->o:Ljava/util/List;

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->t:Z

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/myjson/Gson;->b:Lcom/google/myjson/q;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/myjson/Gson;->c:Lcom/google/myjson/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/myjson/Gson;->b:Lcom/google/myjson/q;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/myjson/Gson;->c:Lcom/google/myjson/g;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-wide/high16 v0, -0x4010

    iput-wide v0, p0, Lcom/google/myjson/GsonBuilder;->f:D

    iput-boolean v3, p0, Lcom/google/myjson/GsonBuilder;->h:Z

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->w:Z

    iput-boolean v3, p0, Lcom/google/myjson/GsonBuilder;->v:Z

    sget-object v0, Lcom/google/myjson/Gson;->d:Lcom/google/myjson/n;

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->g:Lcom/google/myjson/n;

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->i:Z

    sget-object v0, Lcom/google/myjson/LongSerializationPolicy;->DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->j:Lcom/google/myjson/LongSerializationPolicy;

    sget-object v0, Lcom/google/myjson/Gson;->e:Lcom/google/myjson/l;

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->k:Lcom/google/myjson/l;

    new-instance v0, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->l:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v0, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v0, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->p:Z

    iput v4, p0, Lcom/google/myjson/GsonBuilder;->r:I

    iput v4, p0, Lcom/google/myjson/GsonBuilder;->s:I

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->u:Z

    iput-boolean v2, p0, Lcom/google/myjson/GsonBuilder;->x:Z

    return-void
.end method

.method private a(Ljava/lang/Class;Lcom/google/myjson/InstanceCreator;Z)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/myjson/InstanceCreator",
            "<+TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->l:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->registerForTypeHierarchy(Ljava/lang/Class;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/Class;Lcom/google/myjson/JsonDeserializer;Z)Lcom/google/myjson/GsonBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/myjson/JsonDeserializer",
            "<TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v1, Lcom/google/myjson/u;

    invoke-direct {v1, p2}, Lcom/google/myjson/u;-><init>(Lcom/google/myjson/JsonDeserializer;)V

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->registerForTypeHierarchy(Ljava/lang/Class;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/Class;Lcom/google/myjson/JsonSerializer;Z)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/myjson/JsonSerializer",
            "<TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->registerForTypeHierarchy(Ljava/lang/Class;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/Class;Ljava/lang/Object;Z)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            "Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    instance-of v0, p2, Lcom/google/myjson/JsonSerializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/myjson/JsonDeserializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/myjson/InstanceCreator;

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/myjson/internal/$Gson$Preconditions;->checkArgument(Z)V

    instance-of v0, p2, Lcom/google/myjson/InstanceCreator;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/google/myjson/InstanceCreator;

    invoke-direct {p0, p1, v0, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/InstanceCreator;Z)Lcom/google/myjson/GsonBuilder;

    :cond_1
    instance-of v0, p2, Lcom/google/myjson/JsonSerializer;

    if-eqz v0, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/google/myjson/JsonSerializer;

    invoke-direct {p0, p1, v0, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/JsonSerializer;Z)Lcom/google/myjson/GsonBuilder;

    :cond_2
    instance-of v0, p2, Lcom/google/myjson/JsonDeserializer;

    if-eqz v0, :cond_3

    check-cast p2, Lcom/google/myjson/JsonDeserializer;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/JsonDeserializer;Z)Lcom/google/myjson/GsonBuilder;

    :cond_3
    return-object p0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/myjson/InstanceCreator;Z)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/myjson/InstanceCreator",
            "<+TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->l:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->register(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/myjson/JsonDeserializer;Z)Lcom/google/myjson/GsonBuilder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/myjson/JsonDeserializer",
            "<TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v1, Lcom/google/myjson/u;

    invoke-direct {v1, p2}, Lcom/google/myjson/u;-><init>(Lcom/google/myjson/JsonDeserializer;)V

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->register(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Lcom/google/myjson/JsonSerializer;Z)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/myjson/JsonSerializer",
            "<TT;>;Z)",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->register(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)V

    return-object p0
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)Lcom/google/myjson/GsonBuilder;
    .locals 3

    instance-of v0, p2, Lcom/google/myjson/JsonSerializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/myjson/JsonDeserializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/myjson/InstanceCreator;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/myjson/internal/$Gson$Preconditions;->checkArgument(Z)V

    invoke-static {p1}, Lcom/google/myjson/internal/Primitives;->isPrimitive(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/myjson/internal/Primitives;->isWrapperType(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot register type adapters for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    instance-of v0, p2, Lcom/google/myjson/InstanceCreator;

    if-eqz v0, :cond_4

    move-object v0, p2

    check-cast v0, Lcom/google/myjson/InstanceCreator;

    invoke-direct {p0, p1, v0, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/myjson/InstanceCreator;Z)Lcom/google/myjson/GsonBuilder;

    :cond_4
    instance-of v0, p2, Lcom/google/myjson/JsonSerializer;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Lcom/google/myjson/JsonSerializer;

    invoke-direct {p0, p1, v0, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/myjson/JsonSerializer;Z)Lcom/google/myjson/GsonBuilder;

    :cond_5
    instance-of v0, p2, Lcom/google/myjson/JsonDeserializer;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Lcom/google/myjson/JsonDeserializer;

    invoke-direct {p0, p1, v0, p3}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/reflect/Type;Lcom/google/myjson/JsonDeserializer;Z)Lcom/google/myjson/GsonBuilder;

    :cond_6
    instance-of v0, p2, Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->o:Ljava/util/List;

    check-cast p2, Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-object p0
.end method

.method private static a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1, p0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->hasSpecificHandlerFor(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, p0, p2, v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->register(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/String;IILcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;)V"
        }
    .end annotation

    const/4 v3, 0x2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    const-string v1, ""

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Lcom/google/myjson/o$a;

    invoke-direct {v0, p0}, Lcom/google/myjson/o$a;-><init>(Ljava/lang/String;)V

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    const-class v1, Ljava/util/Date;

    invoke-static {v1, p3, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/util/Date;

    invoke-static {v1, p4, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, p3, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, p4, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Date;

    invoke-static {v1, p3, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Date;

    invoke-static {v1, p4, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    if-eq p1, v3, :cond_0

    if-eq p2, v3, :cond_0

    new-instance v0, Lcom/google/myjson/o$a;

    invoke-direct {v0, p1, p2}, Lcom/google/myjson/o$a;-><init>(II)V

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/google/myjson/l;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    new-instance v0, Lcom/google/myjson/f;

    invoke-direct {v0, p1}, Lcom/google/myjson/f;-><init>(Lcom/google/myjson/l;)V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->k:Lcom/google/myjson/l;

    return-object p0
.end method

.method public addDeserializationExclusionStrategy(Lcom/google/myjson/ExclusionStrategy;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSerializationExclusionStrategy(Lcom/google/myjson/ExclusionStrategy;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public create()Lcom/google/myjson/Gson;
    .locals 15

    new-instance v2, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    new-instance v3, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    invoke-direct {v3, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->g:Lcom/google/myjson/n;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->g:Lcom/google/myjson/n;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->h:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/myjson/GsonBuilder;->a:Lcom/google/myjson/p;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/myjson/GsonBuilder;->a:Lcom/google/myjson/p;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-wide v0, p0, Lcom/google/myjson/GsonBuilder;->f:D

    const-wide/high16 v4, -0x4010

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/myjson/v;

    iget-wide v4, p0, Lcom/google/myjson/GsonBuilder;->f:D

    invoke-direct {v0, v4, v5}, Lcom/google/myjson/v;-><init>(D)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->i:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/myjson/GsonBuilder;->b:Lcom/google/myjson/i;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/myjson/GsonBuilder;->c:Lcom/google/myjson/m;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/myjson/GsonBuilder;->q:Ljava/lang/String;

    iget v1, p0, Lcom/google/myjson/GsonBuilder;->r:I

    iget v4, p0, Lcom/google/myjson/GsonBuilder;->s:I

    iget-object v5, p0, Lcom/google/myjson/GsonBuilder;->m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    iget-object v6, p0, Lcom/google/myjson/GsonBuilder;->n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-static {v0, v1, v4, v5, v6}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/String;IILcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V

    new-instance v0, Lcom/google/myjson/Gson;

    new-instance v1, Lcom/google/myjson/t;

    invoke-direct {v1, v2}, Lcom/google/myjson/t;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/myjson/t;

    invoke-direct {v2, v3}, Lcom/google/myjson/t;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/google/myjson/GsonBuilder;->k:Lcom/google/myjson/l;

    iget-object v4, p0, Lcom/google/myjson/GsonBuilder;->l:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v4}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->copyOf()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->makeUnmodifiable()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/myjson/GsonBuilder;->p:Z

    iget-object v6, p0, Lcom/google/myjson/GsonBuilder;->m:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v6}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->copyOf()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->makeUnmodifiable()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v6

    iget-object v7, p0, Lcom/google/myjson/GsonBuilder;->n:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v7}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->copyOf()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->makeUnmodifiable()Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/myjson/GsonBuilder;->t:Z

    iget-boolean v9, p0, Lcom/google/myjson/GsonBuilder;->x:Z

    iget-boolean v10, p0, Lcom/google/myjson/GsonBuilder;->v:Z

    iget-boolean v11, p0, Lcom/google/myjson/GsonBuilder;->w:Z

    iget-boolean v12, p0, Lcom/google/myjson/GsonBuilder;->u:Z

    iget-object v13, p0, Lcom/google/myjson/GsonBuilder;->j:Lcom/google/myjson/LongSerializationPolicy;

    iget-object v14, p0, Lcom/google/myjson/GsonBuilder;->o:Ljava/util/List;

    invoke-direct/range {v0 .. v14}, Lcom/google/myjson/Gson;-><init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/l;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZLcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/myjson/LongSerializationPolicy;Ljava/util/List;)V

    return-object v0
.end method

.method public disableHtmlEscaping()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->v:Z

    return-object p0
.end method

.method public disableInnerClassSerialization()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->h:Z

    return-object p0
.end method

.method public enableComplexMapKeySerialization()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->t:Z

    return-object p0
.end method

.method public varargs excludeFieldsWithModifiers([I)Lcom/google/myjson/GsonBuilder;
    .locals 1

    new-instance v0, Lcom/google/myjson/n;

    invoke-direct {v0, p1}, Lcom/google/myjson/n;-><init>([I)V

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->g:Lcom/google/myjson/n;

    return-object p0
.end method

.method public excludeFieldsWithoutExposeAnnotation()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->i:Z

    return-object p0
.end method

.method public generateNonExecutableJson()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->x:Z

    return-object p0
.end method

.method public registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;Z)Lcom/google/myjson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public registerTypeHierarchyAdapter(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/myjson/GsonBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/myjson/GsonBuilder;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/myjson/GsonBuilder;->a(Ljava/lang/Class;Ljava/lang/Object;Z)Lcom/google/myjson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public serializeNulls()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->p:Z

    return-object p0
.end method

.method public serializeSpecialFloatingPointValues()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->u:Z

    return-object p0
.end method

.method public setDateFormat(I)Lcom/google/myjson/GsonBuilder;
    .locals 1

    iput p1, p0, Lcom/google/myjson/GsonBuilder;->r:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->q:Ljava/lang/String;

    return-object p0
.end method

.method public setDateFormat(II)Lcom/google/myjson/GsonBuilder;
    .locals 1

    iput p1, p0, Lcom/google/myjson/GsonBuilder;->r:I

    iput p2, p0, Lcom/google/myjson/GsonBuilder;->s:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/myjson/GsonBuilder;->q:Ljava/lang/String;

    return-object p0
.end method

.method public setDateFormat(Ljava/lang/String;)Lcom/google/myjson/GsonBuilder;
    .locals 0

    iput-object p1, p0, Lcom/google/myjson/GsonBuilder;->q:Ljava/lang/String;

    return-object p0
.end method

.method public varargs setExclusionStrategies([Lcom/google/myjson/ExclusionStrategy;)Lcom/google/myjson/GsonBuilder;
    .locals 2

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/myjson/GsonBuilder;->d:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/myjson/GsonBuilder;->e:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public setFieldNamingPolicy(Lcom/google/myjson/FieldNamingPolicy;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    invoke-virtual {p1}, Lcom/google/myjson/FieldNamingPolicy;->a()Lcom/google/myjson/l;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/myjson/GsonBuilder;->a(Lcom/google/myjson/l;)Lcom/google/myjson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setFieldNamingStrategy(Lcom/google/myjson/FieldNamingStrategy;)Lcom/google/myjson/GsonBuilder;
    .locals 1

    new-instance v0, Lcom/google/myjson/w;

    invoke-direct {v0, p1}, Lcom/google/myjson/w;-><init>(Lcom/google/myjson/FieldNamingStrategy;)V

    invoke-virtual {p0, v0}, Lcom/google/myjson/GsonBuilder;->a(Lcom/google/myjson/l;)Lcom/google/myjson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setLongSerializationPolicy(Lcom/google/myjson/LongSerializationPolicy;)Lcom/google/myjson/GsonBuilder;
    .locals 0

    iput-object p1, p0, Lcom/google/myjson/GsonBuilder;->j:Lcom/google/myjson/LongSerializationPolicy;

    return-object p0
.end method

.method public setPrettyPrinting()Lcom/google/myjson/GsonBuilder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/GsonBuilder;->w:Z

    return-object p0
.end method

.method public setVersion(D)Lcom/google/myjson/GsonBuilder;
    .locals 0

    iput-wide p1, p0, Lcom/google/myjson/GsonBuilder;->f:D

    return-object p0
.end method
