.class public final enum Lcom/google/myjson/LongSerializationPolicy;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/myjson/LongSerializationPolicy$1;,
        Lcom/google/myjson/LongSerializationPolicy$b;,
        Lcom/google/myjson/LongSerializationPolicy$c;,
        Lcom/google/myjson/LongSerializationPolicy$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/myjson/LongSerializationPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

.field public static final enum STRING:Lcom/google/myjson/LongSerializationPolicy;

.field private static final synthetic b:[Lcom/google/myjson/LongSerializationPolicy;


# instance fields
.field private final a:Lcom/google/myjson/LongSerializationPolicy$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/myjson/LongSerializationPolicy;

    const-string v1, "DEFAULT"

    new-instance v2, Lcom/google/myjson/LongSerializationPolicy$c;

    invoke-direct {v2, v5}, Lcom/google/myjson/LongSerializationPolicy$c;-><init>(Lcom/google/myjson/LongSerializationPolicy$1;)V

    invoke-direct {v0, v1, v3, v2}, Lcom/google/myjson/LongSerializationPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/LongSerializationPolicy$a;)V

    sput-object v0, Lcom/google/myjson/LongSerializationPolicy;->DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

    new-instance v0, Lcom/google/myjson/LongSerializationPolicy;

    const-string v1, "STRING"

    new-instance v2, Lcom/google/myjson/LongSerializationPolicy$b;

    invoke-direct {v2, v5}, Lcom/google/myjson/LongSerializationPolicy$b;-><init>(Lcom/google/myjson/LongSerializationPolicy$1;)V

    invoke-direct {v0, v1, v4, v2}, Lcom/google/myjson/LongSerializationPolicy;-><init>(Ljava/lang/String;ILcom/google/myjson/LongSerializationPolicy$a;)V

    sput-object v0, Lcom/google/myjson/LongSerializationPolicy;->STRING:Lcom/google/myjson/LongSerializationPolicy;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/myjson/LongSerializationPolicy;

    sget-object v1, Lcom/google/myjson/LongSerializationPolicy;->DEFAULT:Lcom/google/myjson/LongSerializationPolicy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/myjson/LongSerializationPolicy;->STRING:Lcom/google/myjson/LongSerializationPolicy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/myjson/LongSerializationPolicy;->b:[Lcom/google/myjson/LongSerializationPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/myjson/LongSerializationPolicy$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/LongSerializationPolicy$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/myjson/LongSerializationPolicy;->a:Lcom/google/myjson/LongSerializationPolicy$a;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/myjson/LongSerializationPolicy;
    .locals 1

    const-class v0, Lcom/google/myjson/LongSerializationPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/LongSerializationPolicy;

    return-object v0
.end method

.method public static final values()[Lcom/google/myjson/LongSerializationPolicy;
    .locals 1

    sget-object v0, Lcom/google/myjson/LongSerializationPolicy;->b:[Lcom/google/myjson/LongSerializationPolicy;

    invoke-virtual {v0}, [Lcom/google/myjson/LongSerializationPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/myjson/LongSerializationPolicy;

    return-object v0
.end method


# virtual methods
.method public serialize(Ljava/lang/Long;)Lcom/google/myjson/JsonElement;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/LongSerializationPolicy;->a:Lcom/google/myjson/LongSerializationPolicy$a;

    invoke-interface {v0, p1}, Lcom/google/myjson/LongSerializationPolicy$a;->serialize(Ljava/lang/Long;)Lcom/google/myjson/JsonElement;

    move-result-object v0

    return-object v0
.end method
