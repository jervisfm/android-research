.class final Lcom/google/myjson/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/myjson/l;


# instance fields
.field private final a:Lcom/google/myjson/l;


# direct methods
.method constructor <init>(Lcom/google/myjson/l;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/myjson/f;->a:Lcom/google/myjson/l;

    return-void
.end method


# virtual methods
.method public translateName(Lcom/google/myjson/FieldAttributes;)Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/google/myjson/annotations/SerializedName;

    invoke-virtual {p1, v0}, Lcom/google/myjson/FieldAttributes;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/annotations/SerializedName;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/f;->a:Lcom/google/myjson/l;

    invoke-interface {v0, p1}, Lcom/google/myjson/l;->translateName(Lcom/google/myjson/FieldAttributes;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/myjson/annotations/SerializedName;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
