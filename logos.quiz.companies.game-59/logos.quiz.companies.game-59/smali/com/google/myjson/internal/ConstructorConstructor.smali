.class public final Lcom/google/myjson/internal/ConstructorConstructor;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/ConstructorConstructor;-><init>(Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/InstanceCreator",
            "<*>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/myjson/internal/ConstructorConstructor;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    return-void
.end method

.method private a(Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/myjson/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    :cond_0
    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$2;

    invoke-direct {v0, p0, v1}, Lcom/google/myjson/internal/ConstructorConstructor$2;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/myjson/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$7;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/myjson/internal/ConstructorConstructor$7;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;Ljava/lang/Class;Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private b(Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/myjson/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-class v0, Ljava/util/SortedSet;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$3;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/ConstructorConstructor$3;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Ljava/util/Set;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$4;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/ConstructorConstructor$4;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    goto :goto_0

    :cond_1
    const-class v0, Ljava/util/Queue;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$6;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/ConstructorConstructor$6;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$5;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/ConstructorConstructor$5;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    goto :goto_0

    :cond_3
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/myjson/internal/ConstructorConstructor$8;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/ConstructorConstructor$8;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getConstructor(Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/ObjectConstructor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/myjson/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/myjson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/myjson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v1

    iget-object v0, p0, Lcom/google/myjson/internal/ConstructorConstructor;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->getHandlerFor(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/InstanceCreator;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/myjson/internal/ConstructorConstructor$1;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/myjson/internal/ConstructorConstructor$1;-><init>(Lcom/google/myjson/internal/ConstructorConstructor;Lcom/google/myjson/InstanceCreator;Ljava/lang/reflect/Type;)V

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/myjson/internal/ConstructorConstructor;->a(Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/myjson/internal/ConstructorConstructor;->b(Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v2, v1}, Lcom/google/myjson/internal/ConstructorConstructor;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/myjson/internal/ObjectConstructor;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/internal/ConstructorConstructor;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
