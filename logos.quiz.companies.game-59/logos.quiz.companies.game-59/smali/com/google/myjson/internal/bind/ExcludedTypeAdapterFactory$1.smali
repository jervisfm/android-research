.class Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;
.super Lcom/google/myjson/internal/bind/TypeAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;->create(Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/myjson/internal/bind/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lcom/google/myjson/internal/bind/MiniGson;

.field final synthetic d:Lcom/google/myjson/reflect/TypeToken;

.field final synthetic e:Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;

.field private f:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;ZZLcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->e:Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;

    iput-boolean p2, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->a:Z

    iput-boolean p3, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->b:Z

    iput-object p4, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->c:Lcom/google/myjson/internal/bind/MiniGson;

    iput-object p5, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->d:Lcom/google/myjson/reflect/TypeToken;

    invoke-direct {p0}, Lcom/google/myjson/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private a()Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->f:Lcom/google/myjson/internal/bind/TypeAdapter;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->c:Lcom/google/myjson/internal/bind/MiniGson;

    iget-object v1, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->e:Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;

    iget-object v2, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->d:Lcom/google/myjson/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/myjson/internal/bind/MiniGson;->getNextAdapter(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->f:Lcom/google/myjson/internal/bind/TypeAdapter;

    goto :goto_0
.end method


# virtual methods
.method public read(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonReader;->skipValue()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->a()Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/myjson/internal/bind/TypeAdapter;->read(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public write(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonWriter;->nullValue()Lcom/google/myjson/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;->a()Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/myjson/internal/bind/TypeAdapter;->write(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_0
.end method
