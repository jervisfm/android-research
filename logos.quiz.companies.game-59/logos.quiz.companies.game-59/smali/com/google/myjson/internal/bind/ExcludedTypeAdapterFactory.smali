.class public final Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/myjson/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final a:Lcom/google/myjson/ExclusionStrategy;

.field private final b:Lcom/google/myjson/ExclusionStrategy;


# direct methods
.method public constructor <init>(Lcom/google/myjson/ExclusionStrategy;Lcom/google/myjson/ExclusionStrategy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;->a:Lcom/google/myjson/ExclusionStrategy;

    iput-object p2, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;->b:Lcom/google/myjson/ExclusionStrategy;

    return-void
.end method


# virtual methods
.method public create(Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/internal/bind/MiniGson;",
            "Lcom/google/myjson/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/myjson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;->a:Lcom/google/myjson/ExclusionStrategy;

    invoke-interface {v1, v0}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipClass(Ljava/lang/Class;)Z

    move-result v3

    iget-object v1, p0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;->b:Lcom/google/myjson/ExclusionStrategy;

    invoke-interface {v1, v0}, Lcom/google/myjson/ExclusionStrategy;->shouldSkipClass(Ljava/lang/Class;)Z

    move-result v2

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory$1;-><init>(Lcom/google/myjson/internal/bind/ExcludedTypeAdapterFactory;ZZLcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)V

    goto :goto_0
.end method
