.class public final Lcom/google/myjson/internal/bind/JsonElementWriter;
.super Lcom/google/myjson/stream/JsonWriter;


# static fields
.field private static final a:Ljava/io/Writer;

.field private static final b:Lcom/google/myjson/JsonPrimitive;


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/myjson/JsonElement;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Lcom/google/myjson/JsonElement;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/myjson/internal/bind/h;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/h;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/JsonElementWriter;->a:Ljava/io/Writer;

    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/myjson/internal/bind/JsonElementWriter;->b:Lcom/google/myjson/JsonPrimitive;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/google/myjson/internal/bind/JsonElementWriter;->a:Ljava/io/Writer;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    sget-object v0, Lcom/google/myjson/JsonNull;->INSTANCE:Lcom/google/myjson/JsonNull;

    iput-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->e:Lcom/google/myjson/JsonElement;

    return-void
.end method

.method private a()Lcom/google/myjson/JsonElement;
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/JsonElement;

    return-object v0
.end method

.method private a(Lcom/google/myjson/JsonElement;)V
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/myjson/JsonElement;->isJsonNull()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->getSerializeNulls()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a()Lcom/google/myjson/JsonElement;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/JsonObject;

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/myjson/JsonObject;->add(Ljava/lang/String;Lcom/google/myjson/JsonElement;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->e:Lcom/google/myjson/JsonElement;

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a()Lcom/google/myjson/JsonElement;

    move-result-object v0

    instance-of v1, v0, Lcom/google/myjson/JsonArray;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/google/myjson/JsonArray;

    invoke-virtual {v0, p1}, Lcom/google/myjson/JsonArray;->add(Lcom/google/myjson/JsonElement;)V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method


# virtual methods
.method public beginArray()Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/JsonArray;

    invoke-direct {v0}, Lcom/google/myjson/JsonArray;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public beginObject()Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/JsonObject;

    invoke-direct {v0}, Lcom/google/myjson/JsonObject;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    sget-object v1, Lcom/google/myjson/internal/bind/JsonElementWriter;->b:Lcom/google/myjson/JsonPrimitive;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public endArray()Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a()Lcom/google/myjson/JsonElement;

    move-result-object v0

    instance-of v0, v0, Lcom/google/myjson/JsonArray;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public endObject()Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a()Lcom/google/myjson/JsonElement;

    move-result-object v0

    instance-of v0, v0, Lcom/google/myjson/JsonObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public get()Lcom/google/myjson/JsonElement;
    .locals 3

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected one JSON element but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->e:Lcom/google/myjson/JsonElement;

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/google/myjson/stream/JsonWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a()Lcom/google/myjson/JsonElement;

    move-result-object v0

    instance-of v0, v0, Lcom/google/myjson/JsonObject;

    if-eqz v0, :cond_2

    iput-object p1, p0, Lcom/google/myjson/internal/bind/JsonElementWriter;->d:Ljava/lang/String;

    return-object p0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public nullValue()Lcom/google/myjson/stream/JsonWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/JsonNull;->INSTANCE:Lcom/google/myjson/JsonNull;

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    return-object p0
.end method

.method public value(D)Lcom/google/myjson/stream/JsonWriter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->isLenient()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    return-object p0
.end method

.method public value(J)Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    return-object p0
.end method

.method public value(Ljava/lang/Number;)Lcom/google/myjson/stream/JsonWriter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->nullValue()Lcom/google/myjson/stream/JsonWriter;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->isLenient()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    invoke-direct {v0, p1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    goto :goto_0
.end method

.method public value(Ljava/lang/String;)Lcom/google/myjson/stream/JsonWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->nullValue()Lcom/google/myjson/stream/JsonWriter;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    invoke-direct {v0, p1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    goto :goto_0
.end method

.method public value(Z)Lcom/google/myjson/stream/JsonWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/JsonPrimitive;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/myjson/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lcom/google/myjson/internal/bind/JsonElementWriter;->a(Lcom/google/myjson/JsonElement;)V

    return-object p0
.end method
