.class abstract Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "a"
.end annotation


# instance fields
.field final g:Ljava/lang/String;

.field final h:Z

.field final i:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory$a;->g:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory$a;->h:Z

    iput-boolean p3, p0, Lcom/google/myjson/internal/bind/ReflectiveTypeAdapterFactory$a;->i:Z

    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/myjson/stream/JsonReader;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation
.end method

.method abstract a(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation
.end method
