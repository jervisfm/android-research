.class public final Lcom/google/myjson/internal/bind/TypeAdapters;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/myjson/internal/bind/TypeAdapters$7;,
        Lcom/google/myjson/internal/bind/TypeAdapters$a;
    }
.end annotation


# static fields
.field public static final BIT_SET:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final BIT_SET_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final BOOLEAN:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final BOOLEAN_AS_STRING:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final BOOLEAN_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final BYTE:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final BYTE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final CALENDAR:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final CALENDAR_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final CHARACTER:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final CHARACTER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final DOUBLE:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final DOUBLE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final ENUM_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final FLOAT:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final FLOAT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final INET_ADDRESS:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final INET_ADDRESS_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final INTEGER:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final INTEGER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final JSON_ELEMENT:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Lcom/google/myjson/JsonElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final JSON_ELEMENT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final LOCALE:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOCALE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final LONG:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final LONG_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final NUMBER:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final NUMBER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final SHORT:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final SHORT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final STRING:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_BUFFER:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_BUFFER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final STRING_BUILDER:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final STRING_BUILDER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final STRING_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final TIMESTAMP_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final URI:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final URI_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final URL:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final URL_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

.field public static final UUID:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final UUID_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/myjson/internal/bind/y;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/y;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BIT_SET:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->BIT_SET:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BIT_SET_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/x;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/x;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BOOLEAN:Lcom/google/myjson/internal/bind/TypeAdapter;

    new-instance v0, Lcom/google/myjson/internal/bind/z;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/z;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BOOLEAN_AS_STRING:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->BOOLEAN:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BOOLEAN_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/aa;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/aa;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BYTE:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->BYTE:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->BYTE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/ab;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/ab;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->SHORT:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->SHORT:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->SHORT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/ac;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/ac;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->INTEGER:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->INTEGER:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->INTEGER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/ad;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/ad;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->LONG:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Long;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->LONG:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->LONG_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/ae;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/ae;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->FLOAT:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Float;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->FLOAT:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->FLOAT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/af;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/af;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->DOUBLE:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Double;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->DOUBLE:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->DOUBLE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/p;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/p;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->NUMBER:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->NUMBER:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->NUMBER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/o;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/o;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->CHARACTER:Lcom/google/myjson/internal/bind/TypeAdapter;

    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->CHARACTER:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->CHARACTER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/t;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/t;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/s;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/s;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUILDER:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUILDER:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUILDER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/r;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/r;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUFFER:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUFFER:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->STRING_BUFFER_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/q;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/q;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->URL:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->URL:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->URL_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/w;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/w;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->URI:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->URI:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->URI_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/v;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/v;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->INET_ADDRESS:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->INET_ADDRESS:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newTypeHierarchyFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->INET_ADDRESS_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/u;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/u;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->UUID:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->UUID:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->UUID_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/n;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/n;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->TIMESTAMP_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/k;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/k;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->CALENDAR:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/myjson/internal/bind/TypeAdapters;->CALENDAR:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactoryForMultipleTypes(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->CALENDAR_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/m;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/m;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->LOCALE:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->LOCALE:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->LOCALE_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    new-instance v0, Lcom/google/myjson/internal/bind/l;

    invoke-direct {v0}, Lcom/google/myjson/internal/bind/l;-><init>()V

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->JSON_ELEMENT:Lcom/google/myjson/internal/bind/TypeAdapter;

    const-class v0, Lcom/google/myjson/JsonElement;

    sget-object v1, Lcom/google/myjson/internal/bind/TypeAdapters;->JSON_ELEMENT:Lcom/google/myjson/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/myjson/internal/bind/TypeAdapters;->newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->JSON_ELEMENT_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    const-class v0, Ljava/lang/Enum;

    invoke-static {v0}, Lcom/google/myjson/internal/bind/TypeAdapters;->newEnumTypeHierarchyFactory(Ljava/lang/Class;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/myjson/internal/bind/TypeAdapters;->ENUM_FACTORY:Lcom/google/myjson/internal/bind/TypeAdapter$Factory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newEnumTypeHierarchyFactory(Ljava/lang/Class;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$2;

    invoke-direct {v0, p0}, Lcom/google/myjson/internal/bind/TypeAdapters$2;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static newFactory(Lcom/google/myjson/reflect/TypeToken;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/reflect/TypeToken",
            "<TTT;>;",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$1;

    invoke-direct {v0, p0, p1}, Lcom/google/myjson/internal/bind/TypeAdapters$1;-><init>(Lcom/google/myjson/reflect/TypeToken;Lcom/google/myjson/internal/bind/TypeAdapter;)V

    return-object v0
.end method

.method public static newFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$4;

    invoke-direct {v0, p0, p1}, Lcom/google/myjson/internal/bind/TypeAdapters$4;-><init>(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)V

    return-object v0
.end method

.method public static newFactory(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<-TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/myjson/internal/bind/TypeAdapters$3;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)V

    return-object v0
.end method

.method public static newFactoryForMultipleTypes(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<+TTT;>;",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<-TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/myjson/internal/bind/TypeAdapters$6;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)V

    return-object v0
.end method

.method public static newTypeHierarchyFactory(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)Lcom/google/myjson/internal/bind/TypeAdapter$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TTT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/internal/bind/TypeAdapters$5;

    invoke-direct {v0, p0, p1}, Lcom/google/myjson/internal/bind/TypeAdapters$5;-><init>(Ljava/lang/Class;Lcom/google/myjson/internal/bind/TypeAdapter;)V

    return-object v0
.end method
