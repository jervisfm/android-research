.class Lcom/google/myjson/j$2;
.super Lcom/google/myjson/internal/bind/TypeAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/myjson/j;->create(Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/myjson/internal/bind/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/myjson/JsonDeserializer;

.field final synthetic b:Ljava/lang/reflect/Type;

.field final synthetic c:Lcom/google/myjson/JsonSerializer;

.field final synthetic d:Lcom/google/myjson/internal/bind/MiniGson;

.field final synthetic e:Lcom/google/myjson/reflect/TypeToken;

.field final synthetic f:Lcom/google/myjson/j;

.field private g:Lcom/google/myjson/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/myjson/j;Lcom/google/myjson/JsonDeserializer;Ljava/lang/reflect/Type;Lcom/google/myjson/JsonSerializer;Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/myjson/j$2;->f:Lcom/google/myjson/j;

    iput-object p2, p0, Lcom/google/myjson/j$2;->a:Lcom/google/myjson/JsonDeserializer;

    iput-object p3, p0, Lcom/google/myjson/j$2;->b:Ljava/lang/reflect/Type;

    iput-object p4, p0, Lcom/google/myjson/j$2;->c:Lcom/google/myjson/JsonSerializer;

    iput-object p5, p0, Lcom/google/myjson/j$2;->d:Lcom/google/myjson/internal/bind/MiniGson;

    iput-object p6, p0, Lcom/google/myjson/j$2;->e:Lcom/google/myjson/reflect/TypeToken;

    invoke-direct {p0}, Lcom/google/myjson/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private a()Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/j$2;->g:Lcom/google/myjson/internal/bind/TypeAdapter;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/j$2;->d:Lcom/google/myjson/internal/bind/MiniGson;

    iget-object v1, p0, Lcom/google/myjson/j$2;->f:Lcom/google/myjson/j;

    iget-object v2, p0, Lcom/google/myjson/j$2;->e:Lcom/google/myjson/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/myjson/internal/bind/MiniGson;->getNextAdapter(Lcom/google/myjson/internal/bind/TypeAdapter$Factory;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/myjson/j$2;->g:Lcom/google/myjson/internal/bind/TypeAdapter;

    goto :goto_0
.end method


# virtual methods
.method public read(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/j$2;->a:Lcom/google/myjson/JsonDeserializer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/myjson/j$2;->a()Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/myjson/internal/bind/TypeAdapter;->read(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/google/myjson/internal/Streams;->parse(Lcom/google/myjson/stream/JsonReader;)Lcom/google/myjson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/myjson/JsonElement;->isJsonNull()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/myjson/j$2;->a:Lcom/google/myjson/JsonDeserializer;

    iget-object v2, p0, Lcom/google/myjson/j$2;->b:Ljava/lang/reflect/Type;

    iget-object v3, p0, Lcom/google/myjson/j$2;->f:Lcom/google/myjson/j;

    invoke-static {v3}, Lcom/google/myjson/j;->a(Lcom/google/myjson/j;)Lcom/google/myjson/JsonDeserializationContext;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/myjson/JsonDeserializer;->deserialize(Lcom/google/myjson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/myjson/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public write(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/j$2;->c:Lcom/google/myjson/JsonSerializer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/myjson/j$2;->a()Lcom/google/myjson/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/myjson/internal/bind/TypeAdapter;->write(Lcom/google/myjson/stream/JsonWriter;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p1}, Lcom/google/myjson/stream/JsonWriter;->nullValue()Lcom/google/myjson/stream/JsonWriter;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/j$2;->c:Lcom/google/myjson/JsonSerializer;

    iget-object v1, p0, Lcom/google/myjson/j$2;->b:Ljava/lang/reflect/Type;

    iget-object v2, p0, Lcom/google/myjson/j$2;->f:Lcom/google/myjson/j;

    invoke-static {v2}, Lcom/google/myjson/j;->b(Lcom/google/myjson/j;)Lcom/google/myjson/JsonSerializationContext;

    move-result-object v2

    invoke-interface {v0, p2, v1, v2}, Lcom/google/myjson/JsonSerializer;->serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/myjson/JsonSerializationContext;)Lcom/google/myjson/JsonElement;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/myjson/internal/Streams;->write(Lcom/google/myjson/JsonElement;Lcom/google/myjson/stream/JsonWriter;)V

    goto :goto_0
.end method
