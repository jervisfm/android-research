.class final Lcom/google/myjson/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/myjson/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/myjson/JsonDeserializationContext;

.field private final d:Lcom/google/myjson/JsonSerializationContext;


# direct methods
.method public constructor <init>(Lcom/google/myjson/Gson;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/myjson/Gson;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonSerializer",
            "<*>;>;",
            "Lcom/google/myjson/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/myjson/JsonDeserializer",
            "<*>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/myjson/j;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    iput-object p3, p0, Lcom/google/myjson/j;->b:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    new-instance v0, Lcom/google/myjson/j$3;

    invoke-direct {v0, p0, p1}, Lcom/google/myjson/j$3;-><init>(Lcom/google/myjson/j;Lcom/google/myjson/Gson;)V

    iput-object v0, p0, Lcom/google/myjson/j;->c:Lcom/google/myjson/JsonDeserializationContext;

    new-instance v0, Lcom/google/myjson/j$1;

    invoke-direct {v0, p0, p1}, Lcom/google/myjson/j$1;-><init>(Lcom/google/myjson/j;Lcom/google/myjson/Gson;)V

    iput-object v0, p0, Lcom/google/myjson/j;->d:Lcom/google/myjson/JsonSerializationContext;

    return-void
.end method

.method static synthetic a(Lcom/google/myjson/j;)Lcom/google/myjson/JsonDeserializationContext;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/j;->c:Lcom/google/myjson/JsonDeserializationContext;

    return-object v0
.end method

.method static synthetic b(Lcom/google/myjson/j;)Lcom/google/myjson/JsonSerializationContext;
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/j;->d:Lcom/google/myjson/JsonSerializationContext;

    return-object v0
.end method


# virtual methods
.method public create(Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/myjson/internal/bind/MiniGson;",
            "Lcom/google/myjson/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/myjson/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/myjson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    iget-object v0, p0, Lcom/google/myjson/j;->a:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v3, v1}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->getHandlerFor(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/myjson/JsonSerializer;

    iget-object v0, p0, Lcom/google/myjson/j;->b:Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v3, v1}, Lcom/google/myjson/internal/ParameterizedTypeHandlerMap;->getHandlerFor(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/myjson/JsonDeserializer;

    if-nez v4, :cond_0

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/myjson/j$2;

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/myjson/j$2;-><init>(Lcom/google/myjson/j;Lcom/google/myjson/JsonDeserializer;Ljava/lang/reflect/Type;Lcom/google/myjson/JsonSerializer;Lcom/google/myjson/internal/bind/MiniGson;Lcom/google/myjson/reflect/TypeToken;)V

    goto :goto_0
.end method
