.class public Lcom/google/myjson/stream/JsonReader;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/myjson/stream/JsonReader$1;
    }
.end annotation


# static fields
.field private static final NON_EXECUTE_PREFIX:[C


# instance fields
.field private final buffer:[C

.field private bufferStartColumn:I

.field private bufferStartLine:I

.field private hasToken:Z

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private name:Ljava/lang/String;

.field private pos:I

.field private skipping:Z

.field private final stack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/myjson/stream/JsonScope;",
            ">;"
        }
    .end annotation
.end field

.field private final stringPool:Lcom/google/myjson/stream/StringPool;

.field private token:Lcom/google/myjson/stream/JsonToken;

.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ")]}\'\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/myjson/stream/StringPool;

    invoke-direct {v0}, Lcom/google/myjson/stream/StringPool;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stringPool:Lcom/google/myjson/stream/StringPool;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iput v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iput v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartLine:I

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    sget-object v0, Lcom/google/myjson/stream/JsonScope;->EMPTY_DOCUMENT:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->push(Lcom/google/myjson/stream/JsonScope;)V

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/myjson/stream/JsonReader;->in:Ljava/io/Reader;

    return-void
.end method

.method private advance()Lcom/google/myjson/stream/JsonToken;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    iput-object v2, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    iput-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/myjson/stream/JsonReader;->name:Ljava/lang/String;

    return-object v0
.end method

.method private checkLenient()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    if-nez v0, :cond_0

    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    return-void
.end method

.method private consumeNonExecutePrefix()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sget-object v1, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-le v0, v1, :cond_1

    sget-object v0, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/2addr v2, v0

    aget-char v1, v1, v2

    sget-object v2, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    aget-char v2, v2, v0

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sget-object v1, Lcom/google/myjson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    goto :goto_0
.end method

.method private decodeLiteral()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->NULL:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/google/myjson/stream/JsonToken;->BOOLEAN:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->NUMBER:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->STRING:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0
.end method

.method private expect(Lcom/google/myjson/stream/JsonToken;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-void
.end method

.method private fillBuffer(I)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    aget-char v3, v3, v0

    const/16 v4, 0xa

    if-ne v3, v4, :cond_0

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartLine:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartLine:I

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    if-eq v0, v3, :cond_5

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget-object v4, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    invoke-static {v0, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_2
    iput v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    :cond_2
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->in:Ljava/io/Reader;

    iget-object v3, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget-object v5, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    array-length v5, v5

    iget v6, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    sub-int/2addr v5, v6

    invoke-virtual {v0, v3, v4, v5}, Ljava/io/Reader;->read([CII)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartLine:I

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    aget-char v0, v0, v1

    const v3, 0xfeff

    if-ne v0, v3, :cond_3

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    :cond_3
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-lt v0, p1, :cond_2

    move v1, v2

    :cond_4
    return v1

    :cond_5
    iput v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    goto :goto_2
.end method

.method private getColumnNumber()I
    .locals 4

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartColumn:I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    aget-char v2, v2, v0

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return v1
.end method

.method private getLineNumber()I
    .locals 4

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->bufferStartLine:I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    aget-char v2, v2, v0

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private getSnippet()Ljava/lang/CharSequence;
    .locals 5

    const/16 v4, 0x14

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    return-object v0
.end method

.method private nextInArray(Z)Lcom/google/myjson/stream/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/myjson/stream/JsonScope;->NONEMPTY_ARRAY:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->replaceTop(Lcom/google/myjson/stream/JsonScope;)V

    :goto_0
    :sswitch_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextValue()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->pop()Lcom/google/myjson/stream/JsonScope;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_ARRAY:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_1

    :sswitch_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    goto :goto_0

    :sswitch_3
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->pop()Lcom/google/myjson/stream/JsonScope;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_ARRAY:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_1

    :cond_1
    :sswitch_4
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    const-string v0, "null"

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->NULL:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_4
        0x3b -> :sswitch_4
        0x5d -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_2
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private nextInObject(Z)Lcom/google/myjson/stream/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    :sswitch_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextLiteral()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->pop()Lcom/google/myjson/stream/JsonScope;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_OBJECT:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    const-string v0, "Unterminated object"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->pop()Lcom/google/myjson/stream/JsonScope;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_OBJECT:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    :sswitch_3
    int-to-char v0, v0

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->name:Ljava/lang/String;

    :cond_1
    sget-object v0, Lcom/google/myjson/stream/JsonScope;->DANGLING_NAME:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->replaceTop(Lcom/google/myjson/stream/JsonScope;)V

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->NAME:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private nextLiteral()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :cond_0
    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    :goto_0
    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-ge v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :goto_1
    :sswitch_0
    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget-boolean v2, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    if-eqz v2, :cond_1

    const-string v0, "skipped!"

    :goto_2
    return-object v0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    goto :goto_1

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stringPool:Lcom/google/myjson/stream/StringPool;

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/myjson/stream/StringPool;->get([CII)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    invoke-virtual {v0, v2, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_4
    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    invoke-virtual {v0, v2, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x23 -> :sswitch_1
        0x2c -> :sswitch_0
        0x2f -> :sswitch_1
        0x3a -> :sswitch_0
        0x3b -> :sswitch_1
        0x3d -> :sswitch_1
        0x5b -> :sswitch_0
        0x5c -> :sswitch_1
        0x5d -> :sswitch_0
        0x7b -> :sswitch_0
        0x7d -> :sswitch_0
    .end sparse-switch
.end method

.method private nextNonWhitespace()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    :goto_0
    :sswitch_0
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_0

    invoke-direct {p0, v3}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    :cond_1
    :goto_1
    return v0

    :sswitch_1
    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-ne v1, v2, :cond_2

    invoke-direct {p0, v3}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v1, v1, v2

    sparse-switch v1, :sswitch_data_1

    goto :goto_1

    :sswitch_2
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    const-string v0, "*/"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->skipTo(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Unterminated comment"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->skipToEndOfLine()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->skipToEndOfLine()V

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/io/EOFException;

    const-string v1, "End of input"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x23 -> :sswitch_4
        0x2f -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2a -> :sswitch_2
        0x2f -> :sswitch_3
    .end sparse-switch
.end method

.method private nextString(C)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :cond_0
    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    :goto_0
    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-ge v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v2, v2, v3

    if-ne v2, p1, :cond_3

    iget-boolean v2, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    if-eqz v2, :cond_1

    const-string v0, "skipped!"

    :goto_1
    return-object v0

    :cond_1
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stringPool:Lcom/google/myjson/stream/StringPool;

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/myjson/stream/StringPool;->get([CII)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/16 v3, 0x5c

    if-ne v2, v3, :cond_7

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_4
    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->readEscapeCharacter()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    move v5, v1

    move-object v1, v0

    move v0, v5

    :goto_2
    move v5, v0

    move-object v0, v1

    move v1, v5

    goto :goto_0

    :cond_5
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_6
    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    sub-int/2addr v3, v1

    invoke-virtual {v0, v2, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_7
    move v5, v1

    move-object v1, v0

    move v0, v5

    goto :goto_2
.end method

.method private nextValue()Lcom/google/myjson/stream/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->readLiteral()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/google/myjson/stream/JsonScope;->EMPTY_OBJECT:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->push(Lcom/google/myjson/stream/JsonScope;)V

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->BEGIN_OBJECT:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/myjson/stream/JsonScope;->EMPTY_ARRAY:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->push(Lcom/google/myjson/stream/JsonScope;)V

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    :sswitch_3
    int-to-char v0, v0

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->STRING:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
        0x5b -> :sswitch_1
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private objectValue()Lcom/google/myjson/stream/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextNonWhitespace()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, "Expected \':\'"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->checkLenient()V

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    :cond_1
    :pswitch_2
    sget-object v0, Lcom/google/myjson/stream/JsonScope;->NONEMPTY_OBJECT:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->replaceTop(Lcom/google/myjson/stream/JsonScope;)V

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextValue()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x3a
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private peekStack()Lcom/google/myjson/stream/JsonScope;
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/stream/JsonScope;

    return-object v0
.end method

.method private pop()Lcom/google/myjson/stream/JsonScope;
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/stream/JsonScope;

    return-object v0
.end method

.method private push(Lcom/google/myjson/stream/JsonScope;)V
    .locals 1

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private quickPeek()Lcom/google/myjson/stream/JsonToken;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/myjson/stream/JsonReader$1;->$SwitchMap$com$google$gson$stream$JsonScope:[I

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->peekStack()Lcom/google/myjson/stream/JsonScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/myjson/stream/JsonScope;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->consumeNonExecutePrefix()V

    :cond_2
    sget-object v0, Lcom/google/myjson/stream/JsonScope;->NONEMPTY_DOCUMENT:Lcom/google/myjson/stream/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->replaceTop(Lcom/google/myjson/stream/JsonScope;)V

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextValue()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->BEGIN_OBJECT:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    const-string v1, "Expected JSON document to start with \'[\' or \'{\'"

    invoke-direct {p0, v1}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2}, Lcom/google/myjson/stream/JsonReader;->nextInArray(Z)Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v3}, Lcom/google/myjson/stream/JsonReader;->nextInArray(Z)Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v2}, Lcom/google/myjson/stream/JsonReader;->nextInObject(Z)Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->objectValue()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v3}, Lcom/google/myjson/stream/JsonReader;->nextInObject(Z)Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    :try_start_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextValue()Lcom/google/myjson/stream/JsonToken;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    if-nez v1, :cond_0

    const-string v0, "Expected EOF"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_DOCUMENT:Lcom/google/myjson/stream/JsonToken;

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    goto :goto_0

    :pswitch_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private readEscapeCharacter()C
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-le v0, v1, :cond_1

    invoke-direct {p0, v3}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stringPool:Lcom/google/myjson/stream/StringPool;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/myjson/stream/StringPool;->get([CII)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    int-to-char v0, v0

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x9

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x8

    goto :goto_0

    :sswitch_3
    const/16 v0, 0xa

    goto :goto_0

    :sswitch_4
    const/16 v0, 0xd

    goto :goto_0

    :sswitch_5
    const/16 v0, 0xc

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_2
        0x66 -> :sswitch_5
        0x6e -> :sswitch_3
        0x72 -> :sswitch_4
        0x74 -> :sswitch_1
        0x75 -> :sswitch_0
    .end sparse-switch
.end method

.method private readLiteral()Lcom/google/myjson/stream/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->nextLiteral()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "Expected literal value"

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    return-object v0
.end method

.method private replaceTop(Lcom/google/myjson/stream/JsonScope;)V
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private skipTo(Ljava/lang/String;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    :goto_0
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-le v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v2, v3, :cond_1

    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method

.method private skipToEndOfLine()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    iget v0, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/myjson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    :cond_2
    return-void
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/myjson/stream/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at line "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->getLineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->getColumnNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/myjson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public beginArray()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/myjson/stream/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->expect(Lcom/google/myjson/stream/JsonToken;)V

    return-void
.end method

.method public beginObject()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->BEGIN_OBJECT:Lcom/google/myjson/stream/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->expect(Lcom/google/myjson/stream/JsonToken;)V

    return-void
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->hasToken:Z

    iput-object v1, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->stack:Ljava/util/List;

    sget-object v1, Lcom/google/myjson/stream/JsonScope;->CLOSED:Lcom/google/myjson/stream/JsonScope;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    return-void
.end method

.method public endArray()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_ARRAY:Lcom/google/myjson/stream/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->expect(Lcom/google/myjson/stream/JsonToken;)V

    return-void
.end method

.method public endObject()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/myjson/stream/JsonToken;->END_OBJECT:Lcom/google/myjson/stream/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/myjson/stream/JsonReader;->expect(Lcom/google/myjson/stream/JsonToken;)V

    return-void
.end method

.method public hasNext()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->END_OBJECT:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->END_ARRAY:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLenient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    return v0
.end method

.method public nextBoolean()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->STRING:Lcom/google/myjson/stream/JsonToken;

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a boolean: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextDouble()D
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids octal prefixes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v2, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    if-nez v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-wide v0
.end method

.method public nextInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_1
    int-to-long v1, v0

    const-wide/16 v3, 0x1

    cmp-long v1, v1, v3

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids octal prefixes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    double-to-int v0, v1

    int-to-double v3, v0

    cmpl-double v1, v3, v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/NumberFormatException;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return v0
.end method

.method public nextLong()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_1
    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids octal prefixes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v0, v2

    long-to-double v4, v0

    cmpl-double v2, v4, v2

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/NumberFormatException;

    iget-object v1, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-wide v0
.end method

.method public nextName()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->NAME:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->name:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-object v0
.end method

.method public nextNull()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->STRING:Lcom/google/myjson/stream/JsonToken;

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a null: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-void
.end method

.method public nextString()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->STRING:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    sget-object v1, Lcom/google/myjson/stream/JsonToken;->NUMBER:Lcom/google/myjson/stream/JsonToken;

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/myjson/stream/JsonReader;->peek()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->value:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    return-object v0
.end method

.method public peek()Lcom/google/myjson/stream/JsonToken;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->quickPeek()Lcom/google/myjson/stream/JsonToken;

    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->decodeLiteral()V

    :cond_0
    iget-object v0, p0, Lcom/google/myjson/stream/JsonReader;->token:Lcom/google/myjson/stream/JsonToken;

    return-object v0
.end method

.method public final setLenient(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/myjson/stream/JsonReader;->lenient:Z

    return-void
.end method

.method public skipValue()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    move v0, v1

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->advance()Lcom/google/myjson/stream/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/myjson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/myjson/stream/JsonToken;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/google/myjson/stream/JsonToken;->BEGIN_OBJECT:Lcom/google/myjson/stream/JsonToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v2, v3, :cond_3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    return-void

    :cond_3
    :try_start_1
    sget-object v3, Lcom/google/myjson/stream/JsonToken;->END_ARRAY:Lcom/google/myjson/stream/JsonToken;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/google/myjson/stream/JsonToken;->END_OBJECT:Lcom/google/myjson/stream/JsonToken;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v2, v3, :cond_2

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Lcom/google/myjson/stream/JsonReader;->skipping:Z

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " near "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/myjson/stream/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
