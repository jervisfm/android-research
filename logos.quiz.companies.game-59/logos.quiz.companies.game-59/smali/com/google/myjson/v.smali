.class final Lcom/google/myjson/v;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/myjson/ExclusionStrategy;


# instance fields
.field private final a:D


# direct methods
.method constructor <init>(D)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/myjson/internal/$Gson$Preconditions;->checkArgument(Z)V

    iput-wide p1, p0, Lcom/google/myjson/v;->a:D

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/myjson/annotations/Since;)Z
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/myjson/annotations/Since;->value()D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/myjson/v;->a:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/myjson/annotations/Since;Lcom/google/myjson/annotations/Until;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/myjson/v;->a(Lcom/google/myjson/annotations/Since;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/myjson/v;->a(Lcom/google/myjson/annotations/Until;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/myjson/annotations/Until;)Z
    .locals 4

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/myjson/annotations/Until;->value()D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/myjson/v;->a:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public shouldSkipClass(Ljava/lang/Class;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    const-class v0, Lcom/google/myjson/annotations/Since;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/annotations/Since;

    const-class v1, Lcom/google/myjson/annotations/Until;

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/myjson/annotations/Until;

    invoke-direct {p0, v0, v1}, Lcom/google/myjson/v;->a(Lcom/google/myjson/annotations/Since;Lcom/google/myjson/annotations/Until;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldSkipField(Lcom/google/myjson/FieldAttributes;)Z
    .locals 2

    const-class v0, Lcom/google/myjson/annotations/Since;

    invoke-virtual {p1, v0}, Lcom/google/myjson/FieldAttributes;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/annotations/Since;

    const-class v1, Lcom/google/myjson/annotations/Until;

    invoke-virtual {p1, v1}, Lcom/google/myjson/FieldAttributes;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/myjson/annotations/Until;

    invoke-direct {p0, v0, v1}, Lcom/google/myjson/v;->a(Lcom/google/myjson/annotations/Since;Lcom/google/myjson/annotations/Until;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
