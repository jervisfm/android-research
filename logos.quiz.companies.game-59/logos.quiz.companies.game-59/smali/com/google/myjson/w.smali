.class final Lcom/google/myjson/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/myjson/l;


# instance fields
.field private final a:Lcom/google/myjson/FieldNamingStrategy;


# direct methods
.method constructor <init>(Lcom/google/myjson/FieldNamingStrategy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/myjson/internal/$Gson$Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/myjson/FieldNamingStrategy;

    iput-object v0, p0, Lcom/google/myjson/w;->a:Lcom/google/myjson/FieldNamingStrategy;

    return-void
.end method


# virtual methods
.method public translateName(Lcom/google/myjson/FieldAttributes;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/myjson/w;->a:Lcom/google/myjson/FieldNamingStrategy;

    invoke-virtual {p1}, Lcom/google/myjson/FieldAttributes;->b()Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/myjson/FieldNamingStrategy;->translateName(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
