.class Lcom/swarmconnect/APICall;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/APICall$APICallback;,
        Lcom/swarmconnect/APICall$APIRequest;,
        Lcom/swarmconnect/APICall$BufferedRequestList;
    }
.end annotation


# static fields
.field public static final RESPONSE_ERROR:I = 0x0

.field public static final RESPONSE_LOGOUT:I = -0x1

.field private static final a:Lcom/google/myjson/Gson;

.field private static c:Z

.field private static d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/APICall;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public appId:Ljava/lang/String;

.field private transient b:Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;

.field public transient cb:Lcom/swarmconnect/APICall$APICallback;

.field public transient statusCode:I

.field public transient statusMessage:Ljava/lang/String;

.field public transient url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v2, 0x12c

    new-instance v0, Lcom/google/myjson/Gson;

    invoke-direct {v0}, Lcom/google/myjson/Gson;-><init>()V

    sput-object v0, Lcom/swarmconnect/APICall;->a:Lcom/google/myjson/Gson;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/APICall;->c:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/swarmconnect/APICall;->d:Ljava/util/HashSet;

    sget-object v0, Lcom/swarmconnect/APICall;->d:Ljava/util/HashSet;

    const-class v1, Lcom/swarmconnect/as;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/APICall;->d:Ljava/util/HashSet;

    const-class v1, Lcom/swarmconnect/ay;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-class v0, Lcom/swarmconnect/ab;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/p;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/l;

    const/16 v1, 0xe10

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/GetDeviceAccountsAPI;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/GetFriendsAPI;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/bp;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/b;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/cf;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/a;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/bo;

    invoke-static {v0, v2}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;I)V

    const-class v0, Lcom/swarmconnect/o;

    const-class v1, Lcom/swarmconnect/ab;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bu;

    const-class v1, Lcom/swarmconnect/ab;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/y;

    const-class v1, Lcom/swarmconnect/p;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/ce;

    const-class v1, Lcom/swarmconnect/GetDeviceAccountsAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/w;

    const-class v1, Lcom/swarmconnect/GetDeviceAccountsAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/br;

    const-class v1, Lcom/swarmconnect/GetFriendsAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bt;

    const-class v1, Lcom/swarmconnect/GetFriendsAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bd;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bd;

    const-class v1, Lcom/swarmconnect/bo;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/aw;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/cj;

    const-class v1, Lcom/swarmconnect/bp;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bu;

    const-class v1, Lcom/swarmconnect/a;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/cn;

    const-class v1, Lcom/swarmconnect/a;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/o;

    const-class v1, Lcom/swarmconnect/a;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/ce;

    const-class v1, Lcom/swarmconnect/bo;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/ce;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/w;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/w;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    invoke-static {v0, v1}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/swarmconnect/ap;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ap;-><init>(Lcom/swarmconnect/APICall;)V

    iput-object v0, p0, Lcom/swarmconnect/APICall;->b:Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;

    return-void
.end method

.method public constructor <init>(Lcom/swarmconnect/APICall$APICallback;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/swarmconnect/ap;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ap;-><init>(Lcom/swarmconnect/APICall;)V

    iput-object v0, p0, Lcom/swarmconnect/APICall;->b:Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;

    iput-object p1, p0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/APICall;Ljava/lang/String;)Lcom/swarmconnect/APICall;
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/APICall;->b(Ljava/lang/String;)Lcom/swarmconnect/APICall;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/APICall;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/APICall;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/APICall;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/swarmconnect/APICall;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Z)V
    .locals 0

    sput-boolean p0, Lcom/swarmconnect/APICall;->c:Z

    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/swarmconnect/APICall;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/google/myjson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/myjson/GsonBuilder;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x8

    aput v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/myjson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/myjson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/myjson/GsonBuilder;->create()Lcom/google/myjson/Gson;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/myjson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/APICall;

    iget v2, v0, Lcom/swarmconnect/APICall;->statusCode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    invoke-static {}, Lcom/swarmconnect/Swarm;->logOut()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v1, v0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pending_apis_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v0, Lcom/google/myjson/Gson;

    invoke-direct {v0}, Lcom/google/myjson/Gson;-><init>()V

    const-class v3, Lcom/swarmconnect/APICall$BufferedRequestList;

    invoke-virtual {v0, v2, v3}, Lcom/google/myjson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/APICall$BufferedRequestList;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    if-nez v2, :cond_2

    :cond_1
    new-instance v0, Lcom/swarmconnect/APICall$BufferedRequestList;

    invoke-direct {v0}, Lcom/swarmconnect/APICall$BufferedRequestList;-><init>()V

    :cond_2
    iget-object v2, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    new-instance v3, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;

    invoke-direct {v3, p0, p1}, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pending_apis_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/myjson/Gson;

    invoke-direct {v3}, Lcom/google/myjson/Gson;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_3
    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    new-instance v2, Ljava/util/zip/Deflater;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/util/zip/Deflater;-><init>(IZ)V

    invoke-direct {v1, v0, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;)V

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/zip/DeflaterOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/utils/Base64;->encode([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static c()Z
    .locals 5

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/APICall;->c:Z

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "pending_apis_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/myjson/Gson;

    invoke-direct {v1}, Lcom/google/myjson/Gson;-><init>()V

    const-class v3, Lcom/swarmconnect/APICall$BufferedRequestList;

    invoke-virtual {v1, v0, v3}, Lcom/google/myjson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/APICall$BufferedRequestList;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    iget-object v1, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_0

    sget-boolean v1, Lcom/swarmconnect/APICall;->c:Z

    if-nez v1, :cond_1

    :cond_0
    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pending_apis_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/myjson/Gson;

    invoke-direct {v3}, Lcom/google/myjson/Gson;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-boolean v0, Lcom/swarmconnect/APICall;->c:Z

    :goto_1
    return v0

    :cond_1
    iget-object v1, v0, Lcom/swarmconnect/APICall$BufferedRequestList;->requests:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;

    new-instance v3, Lcom/swarmconnect/APICall$APIRequest;

    invoke-direct {v3}, Lcom/swarmconnect/APICall$APIRequest;-><init>()V

    sget-object v4, Lcom/swarmconnect/Swarm;->i:Ljava/lang/String;

    iput-object v4, v3, Lcom/swarmconnect/APICall$APIRequest;->a:Ljava/lang/String;

    iput v2, v3, Lcom/swarmconnect/APICall$APIRequest;->u:I

    iget-object v4, v1, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;->module:Ljava/lang/String;

    iput-object v4, v3, Lcom/swarmconnect/APICall$APIRequest;->m:Ljava/lang/String;

    iget-object v4, v1, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;->payload:Ljava/lang/String;

    iput-object v4, v3, Lcom/swarmconnect/APICall$APIRequest;->d:Ljava/lang/String;

    sget-object v4, Lcom/swarmconnect/APICall;->a:Lcom/google/myjson/Gson;

    invoke-virtual {v4, v3}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/swarmconnect/APICall;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v1, v1, Lcom/swarmconnect/APICall$BufferedRequestList$BufferedRequest;->module:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "v="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "&req="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/APICall;->d(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "http://api.swarmconnect.com/api.php?v=5&req="

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "&t="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/swarmconnect/APICall$1;

    invoke-direct {v3, v0}, Lcom/swarmconnect/APICall$1;-><init>(Lcom/swarmconnect/APICall$BufferedRequestList;)V

    invoke-static {v1, v3}, Lcom/swarmconnect/utils/AsyncHttp;->getBlocking(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic d()Ljava/util/HashSet;
    .locals 1

    sget-object v0, Lcom/swarmconnect/APICall;->d:Ljava/util/HashSet;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 1

    new-instance v0, Lcom/google/myjson/Gson;

    invoke-direct {v0}, Lcom/google/myjson/Gson;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()I
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v0, v0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    goto :goto_0
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->i:Ljava/lang/String;

    return-object v0
.end method

.method public run()V
    .locals 3

    :try_start_0
    sget-object v0, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/swarmconnect/APICall;->appId:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/APICall$APIRequest;

    invoke-direct {v0}, Lcom/swarmconnect/APICall$APIRequest;-><init>()V

    invoke-virtual {p0}, Lcom/swarmconnect/APICall;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/APICall$APIRequest;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/swarmconnect/APICall;->a()I

    move-result v1

    iput v1, v0, Lcom/swarmconnect/APICall$APIRequest;->u:I

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bx;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/APICall$APIRequest;->m:Ljava/lang/String;

    sget v1, Lcom/swarmconnect/Swarm;->g:I

    iput v1, v0, Lcom/swarmconnect/APICall$APIRequest;->av:I

    invoke-direct {p0}, Lcom/swarmconnect/APICall;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/APICall$APIRequest;->d:Ljava/lang/String;

    sget-object v1, Lcom/swarmconnect/APICall;->a:Lcom/google/myjson/Gson;

    invoke-virtual {v1, v0}, Lcom/google/myjson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/APICall;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api.swarmconnect.com/api.php?v=5&req="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/APICall;->url:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bx;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/swarmconnect/APICall;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/swarmconnect/APICall;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/APICall;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/APICall;->d(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/swarmconnect/aq;->a(Lcom/swarmconnect/APICall;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-direct {p0, v0}, Lcom/swarmconnect/APICall;->b(Ljava/lang/String;)Lcom/swarmconnect/APICall;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/swarmconnect/APICall$APICallback;->gotAPI(Lcom/swarmconnect/APICall;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/aq;->a(Ljava/lang/Class;)V

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swarmconnect/APICall;->url:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/APICall;->b:Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;

    invoke-static {v0, v1}, Lcom/swarmconnect/utils/AsyncHttp;->getURL(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->a()V

    invoke-static {}, Lcom/swarmconnect/aq;->a()V

    goto :goto_0
.end method
