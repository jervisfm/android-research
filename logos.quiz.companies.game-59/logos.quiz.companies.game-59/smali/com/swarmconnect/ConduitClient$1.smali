.class Lcom/swarmconnect/ConduitClient$1;
.super Lcom/swarmconnect/AsyncHttp$AsyncCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/ConduitClient;->getHost()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ConduitClient;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ConduitClient;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    invoke-direct {p0}, Lcom/swarmconnect/AsyncHttp$AsyncCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotURL(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getHost(): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;J)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    invoke-static {v0, p1}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;Z)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    invoke-virtual {v0}, Lcom/swarmconnect/ConduitClient;->connect()V

    return-void
.end method

.method public requestFailed(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;Z)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$1;->a:Lcom/swarmconnect/ConduitClient;

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Lcom/swarmconnect/ConduitClient;)V

    return-void
.end method
