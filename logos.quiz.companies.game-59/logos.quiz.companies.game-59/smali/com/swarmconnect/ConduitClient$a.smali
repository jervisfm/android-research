.class Lcom/swarmconnect/ConduitClient$a;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/ConduitClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/ConduitClient$a$a;
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ConduitClient;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/ConduitClient;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ConduitClient$a;->a:Lcom/swarmconnect/ConduitClient;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/ConduitClient;Lcom/swarmconnect/ConduitClient$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/ConduitClient$a;-><init>(Lcom/swarmconnect/ConduitClient;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_1

    new-instance v1, Lcom/swarmconnect/ConduitClient$a$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/swarmconnect/ConduitClient$a$a;-><init>(Lcom/swarmconnect/ConduitClient$a;Lcom/swarmconnect/ConduitClient$a$a;)V

    invoke-virtual {v1}, Lcom/swarmconnect/ConduitClient$a$a;->isInitialSticky()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v1, "noConnectivity"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$a;->a:Lcom/swarmconnect/ConduitClient;

    invoke-virtual {v0}, Lcom/swarmconnect/ConduitClient;->c()V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$a;->a:Lcom/swarmconnect/ConduitClient;

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;)Lcom/swarmconnect/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient$a;->a:Lcom/swarmconnect/ConduitClient;

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->a(Lcom/swarmconnect/ConduitClient;)Lcom/swarmconnect/ac;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "connectivity changed"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ac;->a(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
