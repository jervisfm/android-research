.class public Lcom/swarmconnect/ConduitClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/swarmconnect/ad;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/ConduitClient$a;
    }
.end annotation


# static fields
.field private static k:Lcom/swarmconnect/ConduitClient;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/Timer;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:Z

.field private h:Z

.field private i:Lcom/swarmconnect/ac;

.field private j:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/swarmconnect/ConduitClient;->b:Ljava/lang/String;

    const/16 v0, 0x1388

    iput v0, p0, Lcom/swarmconnect/ConduitClient;->c:I

    iput-object v1, p0, Lcom/swarmconnect/ConduitClient;->e:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/swarmconnect/ConduitClient;->f:J

    iput-boolean v2, p0, Lcom/swarmconnect/ConduitClient;->g:Z

    iput-boolean v2, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    return-void
.end method

.method protected static a()Lcom/swarmconnect/ConduitClient;
    .locals 1

    sget-object v0, Lcom/swarmconnect/ConduitClient;->k:Lcom/swarmconnect/ConduitClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/swarmconnect/ConduitClient;

    invoke-direct {v0}, Lcom/swarmconnect/ConduitClient;-><init>()V

    sput-object v0, Lcom/swarmconnect/ConduitClient;->k:Lcom/swarmconnect/ConduitClient;

    :cond_0
    sget-object v0, Lcom/swarmconnect/ConduitClient;->k:Lcom/swarmconnect/ConduitClient;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/ConduitClient;)Lcom/swarmconnect/ac;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/ConduitClient;J)V
    .locals 0

    iput-wide p1, p0, Lcom/swarmconnect/ConduitClient;->f:J

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ConduitClient;Lcom/swarmconnect/ac;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ConduitClient;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ConduitClient;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ConduitClient;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/swarmconnect/ConduitClient;->g:Z

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/ConduitClient;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ConduitClient;->d()V

    return-void
.end method

.method private static b(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/NotificationService;->a(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized d()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->d:Ljava/util/Timer;

    new-instance v1, Lcom/swarmconnect/ConduitClient$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ConduitClient$2;-><init>(Lcom/swarmconnect/ConduitClient;)V

    iget v2, p0, Lcom/swarmconnect/ConduitClient;->c:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const v0, 0x1499700

    iget v1, p0, Lcom/swarmconnect/ConduitClient;->c:I

    mul-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/swarmconnect/ConduitClient;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    iput-object p1, p0, Lcom/swarmconnect/ConduitClient;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/swarmconnect/SwarmIO;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/swarmconnect/ConduitClient$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/ConduitClient$a;-><init>(Lcom/swarmconnect/ConduitClient;Lcom/swarmconnect/ConduitClient$a;)V

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/swarmconnect/ConduitClient;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized b()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->j:Landroid/content/BroadcastReceiver;

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "ConduitClient.onDestroy"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ac;->a(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected c()V
    .locals 1

    const/16 v0, 0x1388

    iput v0, p0, Lcom/swarmconnect/ConduitClient;->c:I

    return-void
.end method

.method public connect()V
    .locals 4

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConduitClient.connect(), conn: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/swarmconnect/ConduitClient;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->e:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/swarmconnect/ConduitClient;->getHost()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/swarmconnect/ac;

    iget-object v1, p0, Lcom/swarmconnect/ConduitClient;->e:Ljava/lang/String;

    const/16 v2, 0x23fa

    invoke-direct {v0, v1, v2, p0}, Lcom/swarmconnect/ac;-><init>(Ljava/lang/String;ILcom/swarmconnect/ad;)V

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    new-instance v0, Lcom/swarmconnect/ConduitClient$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ConduitClient$3;-><init>(Lcom/swarmconnect/ConduitClient;)V

    invoke-virtual {v0}, Lcom/swarmconnect/ConduitClient$3;->start()V

    goto :goto_0
.end method

.method public getHost()V
    .locals 2

    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->h:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "ConduitClient.getHost()..."

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/swarmconnect/ConduitClient;->g:Z

    const-string v0, "http://nadmin.swarm.io"

    new-instance v1, Lcom/swarmconnect/ConduitClient$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ConduitClient$1;-><init>(Lcom/swarmconnect/ConduitClient;)V

    invoke-static {v0, v1}, Lcom/swarmconnect/AsyncHttp;->a(Ljava/lang/String;Lcom/swarmconnect/AsyncHttp$AsyncCB;)V

    goto :goto_0
.end method

.method public onConnected(Lcom/swarmconnect/ac;)V
    .locals 1

    const-string v0, "ConduitClient.onConnected"

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/NotificationService;->a()V

    invoke-virtual {p0}, Lcom/swarmconnect/ConduitClient;->c()V

    return-void
.end method

.method public onConnectionLost(Lcom/swarmconnect/ac;Ljava/lang/Throwable;)V
    .locals 2

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NotifcationService.onConnectionLost: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", conn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    :cond_0
    invoke-direct {p0}, Lcom/swarmconnect/ConduitClient;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPacketReceived(I[B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onPacketReceived: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ConduitClient;->b(Ljava/lang/String;)V

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    new-instance v1, Lcom/swarmconnect/packets/PacketAuth;

    iget-object v2, p0, Lcom/swarmconnect/ConduitClient;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/swarmconnect/packets/PacketAuth;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/swarmconnect/packets/PacketAuth;->getPayload()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ac;->a([B)Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/ConduitClient;->i:Lcom/swarmconnect/ac;

    new-instance v1, Lcom/swarmconnect/packets/PacketPong;

    invoke-direct {v1}, Lcom/swarmconnect/packets/PacketPong;-><init>()V

    invoke-virtual {v1}, Lcom/swarmconnect/packets/PacketPong;->getPayload()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ac;->a([B)Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    invoke-static {p2, p1}, Lcom/swarmconnect/packets/Packet;->build([BI)Lcom/swarmconnect/packets/Packet;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/packets/PacketNotification;

    iget-object v0, v0, Lcom/swarmconnect/packets/PacketNotification;->clientId:Ljava/lang/String;

    iget-object v1, p0, Lcom/swarmconnect/ConduitClient;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/NotificationService;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
