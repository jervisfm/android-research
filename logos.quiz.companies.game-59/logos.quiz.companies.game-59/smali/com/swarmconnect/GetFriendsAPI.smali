.class Lcom/swarmconnect/GetFriendsAPI;
.super Lcom/swarmconnect/APICall;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/GetFriendsAPI$FriendStatus;
    }
.end annotation


# instance fields
.field public transient friends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;"
        }
    .end annotation
.end field

.field public transient incomingRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/APICall;-><init>()V

    sget-object v0, Lcom/swarmconnect/GetFriendsAPI$FriendStatus;->ALL:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;

    iput-object v0, p0, Lcom/swarmconnect/GetFriendsAPI;->status:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;

    return-void
.end method
