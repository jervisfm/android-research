.class public Lcom/swarmconnect/NotificationChallenge;
.super Lcom/swarmconnect/SwarmNotification;


# instance fields
.field public challenge:Lcom/swarmconnect/SwarmChallenge;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->CHALLENGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationChallenge;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    new-instance v0, Lcom/swarmconnect/aa;

    invoke-direct {v0}, Lcom/swarmconnect/aa;-><init>()V

    iget v1, p0, Lcom/swarmconnect/NotificationChallenge;->data:I

    iput v1, v0, Lcom/swarmconnect/aa;->challengeId:I

    new-instance v1, Lcom/swarmconnect/NotificationChallenge$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/NotificationChallenge$1;-><init>(Lcom/swarmconnect/NotificationChallenge;)V

    iput-object v1, v0, Lcom/swarmconnect/aa;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/aa;->run()V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_trophy"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "Challenge text"

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string v0, "Challenge"

    return-object v0
.end method
