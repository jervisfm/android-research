.class public Lcom/swarmconnect/NotificationFriend;
.super Lcom/swarmconnect/SwarmNotification;


# instance fields
.field public accepted:Z

.field public friend:Lcom/swarmconnect/SwarmUser;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationFriend;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    const-class v0, Lcom/swarmconnect/GetFriendsAPI;

    invoke-static {v0}, Lcom/swarmconnect/aq;->invalidate(Ljava/lang/Class;)V

    new-instance v0, Lcom/swarmconnect/bt;

    invoke-direct {v0}, Lcom/swarmconnect/bt;-><init>()V

    iget v1, p0, Lcom/swarmconnect/NotificationFriend;->data:I

    iput v1, v0, Lcom/swarmconnect/bt;->friendId:I

    new-instance v1, Lcom/swarmconnect/NotificationFriend$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/NotificationFriend$1;-><init>(Lcom/swarmconnect/NotificationFriend;)V

    iput-object v1, v0, Lcom/swarmconnect/bt;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bt;->run()V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "@drawable/swarm_friends"

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/swarmconnect/NotificationFriend;->accepted:Z

    if-eqz v0, :cond_0

    const-string v0, "Accepted your friend request!"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Wants to be your friend!"

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/NotificationFriend;->friend:Lcom/swarmconnect/SwarmUser;

    iget-object v0, v0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    return-object v0
.end method
