.class public Lcom/swarmconnect/NotificationGotCoins;
.super Lcom/swarmconnect/SwarmNotification;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationGotCoins;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    const-class v0, Lcom/swarmconnect/bo;

    invoke-static {v0}, Lcom/swarmconnect/aq;->invalidate(Ljava/lang/Class;)V

    new-instance v0, Lcom/swarmconnect/bo;

    invoke-direct {v0}, Lcom/swarmconnect/bo;-><init>()V

    new-instance v1, Lcom/swarmconnect/NotificationGotCoins$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/NotificationGotCoins$1;-><init>(Lcom/swarmconnect/NotificationGotCoins;)V

    iput-object v1, v0, Lcom/swarmconnect/bo;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bo;->run()V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_coin"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/swarmconnect/NotificationGotCoins;->data:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " coins added to your account."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string v0, "You earned coins!"

    return-object v0
.end method
