.class Lcom/swarmconnect/NotificationMessage$1;
.super Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/NotificationMessage;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/NotificationMessage;


# direct methods
.method constructor <init>(Lcom/swarmconnect/NotificationMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/NotificationMessage$1;->a:Lcom/swarmconnect/NotificationMessage;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotMessages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmMessage;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-gez v1, :cond_0

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/swarmconnect/NotificationMessage$1;->a:Lcom/swarmconnect/NotificationMessage;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessage;

    iput-object v0, v2, Lcom/swarmconnect/NotificationMessage;->message:Lcom/swarmconnect/SwarmMessage;

    iget-object v0, p0, Lcom/swarmconnect/NotificationMessage$1;->a:Lcom/swarmconnect/NotificationMessage;

    iget-object v0, v0, Lcom/swarmconnect/NotificationMessage;->message:Lcom/swarmconnect/SwarmMessage;

    iget-object v0, v0, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/NotificationMessage$1;->a:Lcom/swarmconnect/NotificationMessage;

    iget-object v0, v0, Lcom/swarmconnect/NotificationMessage;->message:Lcom/swarmconnect/SwarmMessage;

    iget-object v0, v0, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    iget v0, v0, Lcom/swarmconnect/SwarmUser;->userId:I

    sget-object v2, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v2, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/NotificationMessage$1;->a:Lcom/swarmconnect/NotificationMessage;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmNotification;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method
