.class public Lcom/swarmconnect/NotificationMessage;
.super Lcom/swarmconnect/SwarmNotification;


# instance fields
.field public message:Lcom/swarmconnect/SwarmMessage;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationMessage;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    const-class v0, Lcom/swarmconnect/aj;

    invoke-static {v0}, Lcom/swarmconnect/aq;->invalidate(Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bq;

    invoke-static {v0}, Lcom/swarmconnect/aq;->invalidate(Ljava/lang/Class;)V

    const-class v0, Lcom/swarmconnect/bh;

    invoke-static {v0}, Lcom/swarmconnect/aq;->invalidate(Ljava/lang/Class;)V

    new-instance v0, Lcom/swarmconnect/SwarmMessageThread;

    invoke-direct {v0}, Lcom/swarmconnect/SwarmMessageThread;-><init>()V

    iget v1, p0, Lcom/swarmconnect/NotificationMessage;->data:I

    iput v1, v0, Lcom/swarmconnect/SwarmMessageThread;->id:I

    new-instance v1, Lcom/swarmconnect/NotificationMessage$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/NotificationMessage$1;-><init>(Lcom/swarmconnect/NotificationMessage;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmMessageThread;->getMessages(Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_messages"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/NotificationMessage;->message:Lcom/swarmconnect/SwarmMessage;

    iget-object v0, v0, Lcom/swarmconnect/SwarmMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Message from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swarmconnect/NotificationMessage;->message:Lcom/swarmconnect/SwarmMessage;

    iget-object v1, v1, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    iget-object v1, v1, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
