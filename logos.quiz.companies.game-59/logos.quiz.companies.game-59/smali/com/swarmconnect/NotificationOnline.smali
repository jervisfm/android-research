.class public Lcom/swarmconnect/NotificationOnline;
.super Lcom/swarmconnect/SwarmNotification;


# instance fields
.field public app:Lcom/swarmconnect/SwarmApplication;

.field public online:Z

.field public user:Lcom/swarmconnect/SwarmUser;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationOnline;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    new-instance v0, Lcom/swarmconnect/z;

    invoke-direct {v0}, Lcom/swarmconnect/z;-><init>()V

    iget v1, p0, Lcom/swarmconnect/NotificationOnline;->data:I

    iput v1, v0, Lcom/swarmconnect/z;->id:I

    new-instance v1, Lcom/swarmconnect/NotificationOnline$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/NotificationOnline$1;-><init>(Lcom/swarmconnect/NotificationOnline;)V

    iput-object v1, v0, Lcom/swarmconnect/z;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/z;->run()V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, Lcom/swarmconnect/NotificationOnline;->online:Z

    if-eqz v0, :cond_0

    const-string v0, "@drawable/swarm_friend_online"

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    const-string v0, "@drawable/swarm_friend_offline"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Lcom/swarmconnect/NotificationOnline;->online:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Playing "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swarmconnect/NotificationOnline;->app:Lcom/swarmconnect/SwarmApplication;

    iget-object v1, v1, Lcom/swarmconnect/SwarmApplication;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    iget-boolean v0, p0, Lcom/swarmconnect/NotificationOnline;->online:Z

    if-eqz v0, :cond_0

    const-string v0, " came online"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/swarmconnect/NotificationOnline;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v2, v2, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, " went offline"

    goto :goto_0
.end method
