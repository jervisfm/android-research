.class public Lcom/swarmconnect/NotificationPurchase;
.super Lcom/swarmconnect/SwarmNotification;


# instance fields
.field public listing:Lcom/swarmconnect/SwarmStoreListing;


# direct methods
.method protected constructor <init>(Lcom/swarmconnect/SwarmStoreListing;)V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/SwarmNotification;-><init>()V

    iput-object p1, p0, Lcom/swarmconnect/NotificationPurchase;->listing:Lcom/swarmconnect/SwarmStoreListing;

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput-object v0, p0, Lcom/swarmconnect/NotificationPurchase;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method


# virtual methods
.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_coin"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/NotificationPurchase;->listing:Lcom/swarmconnect/SwarmStoreListing;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreListing;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string v0, "Item Purchased!"

    return-object v0
.end method
