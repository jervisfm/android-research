.class public Lcom/swarmconnect/NotificationService;
.super Landroid/app/Service;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/NotificationService$LocalBinder;
    }
.end annotation


# static fields
.field private static a:I

.field private static b:Landroid/os/Handler;

.field private static c:Landroid/content/Context;


# instance fields
.field private d:Lcom/swarmconnect/NotificationService$LocalBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/swarmconnect/NotificationService;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/swarmconnect/NotificationService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/swarmconnect/NotificationService$LocalBinder;-><init>(Lcom/swarmconnect/NotificationService;)V

    iput-object v0, p0, Lcom/swarmconnect/NotificationService;->d:Lcom/swarmconnect/NotificationService$LocalBinder;

    return-void
.end method

.method protected static a()V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/swarmconnect/NotificationService;->b:Landroid/os/Handler;

    new-instance v1, Lcom/swarmconnect/NotificationService$1;

    invoke-direct {v1}, Lcom/swarmconnect/NotificationService$1;-><init>()V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected static a(I)V
    .locals 0

    sput p0, Lcom/swarmconnect/NotificationService;->a:I

    return-void
.end method

.method protected static a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method static synthetic b()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/swarmconnect/NotificationService;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/NotificationService;->d:Lcom/swarmconnect/NotificationService$LocalBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/swarmconnect/NotificationService;->b:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/swarmconnect/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/NotificationService;->c:Landroid/content/Context;

    invoke-static {}, Lcom/swarmconnect/ConduitClient;->a()Lcom/swarmconnect/ConduitClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/swarmconnect/ConduitClient;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-static {}, Lcom/swarmconnect/ConduitClient;->a()Lcom/swarmconnect/ConduitClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swarmconnect/ConduitClient;->b()V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    invoke-static {}, Lcom/swarmconnect/ConduitClient;->a()Lcom/swarmconnect/ConduitClient;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/swarmconnect/ConduitClient;->a(Landroid/content/Context;)V

    :cond_0
    sget v0, Lcom/swarmconnect/NotificationService;->a:I

    return v0
.end method
