.class public final Lcom/swarmconnect/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final icon:I = 0x7f020000

.field public static final swarm_add_friend:I = 0x7f020001

.field public static final swarm_background:I = 0x7f020002

.field public static final swarm_bubble_arrow:I = 0x7f020003

.field public static final swarm_bubble_arrow_left:I = 0x7f020004

.field public static final swarm_checkmark:I = 0x7f020005

.field public static final swarm_close:I = 0x7f020006

.field public static final swarm_coin:I = 0x7f020007

.field public static final swarm_coin_small:I = 0x7f020008

.field public static final swarm_facebook_icon:I = 0x7f020009

.field public static final swarm_friend:I = 0x7f02000a

.field public static final swarm_friend_offline:I = 0x7f02000b

.field public static final swarm_friend_online:I = 0x7f02000c

.field public static final swarm_friends:I = 0x7f02000d

.field public static final swarm_friends40:I = 0x7f02000e

.field public static final swarm_games:I = 0x7f02000f

.field public static final swarm_games40:I = 0x7f020010

.field public static final swarm_head:I = 0x7f020011

.field public static final swarm_home:I = 0x7f020012

.field public static final swarm_icon:I = 0x7f020013

.field public static final swarm_leaderboards:I = 0x7f020014

.field public static final swarm_leaderboards40:I = 0x7f020015

.field public static final swarm_logo:I = 0x7f020016

.field public static final swarm_mail:I = 0x7f020017

.field public static final swarm_mail_grey:I = 0x7f020018

.field public static final swarm_messages:I = 0x7f020019

.field public static final swarm_messages40:I = 0x7f02001a

.field public static final swarm_quit:I = 0x7f02001b

.field public static final swarm_reply:I = 0x7f02001c

.field public static final swarm_settings:I = 0x7f02001d

.field public static final swarm_settings40:I = 0x7f02001e

.field public static final swarm_store:I = 0x7f02001f

.field public static final swarm_store40:I = 0x7f020020

.field public static final swarm_trophy:I = 0x7f020021

.field public static final swarm_trophy40:I = 0x7f020022

.field public static final swarm_trophy_gold:I = 0x7f020023

.field public static final swarm_trophy_grey:I = 0x7f020024

.field public static final swarm_x:I = 0x7f020025


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
