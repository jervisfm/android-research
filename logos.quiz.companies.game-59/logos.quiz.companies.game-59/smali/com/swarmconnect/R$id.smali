.class public final Lcom/swarmconnect/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept:I = 0x7f04001e

.field public static final achievement_icon:I = 0x7f040000

.field public static final add:I = 0x7f040019

.field public static final add_friend_username:I = 0x7f040018

.field public static final background:I = 0x7f040007

.field public static final benefits:I = 0x7f040013

.field public static final cancel:I = 0x7f04004a

.field public static final change_pass:I = 0x7f040055

.field public static final close:I = 0x7f040027

.field public static final coins:I = 0x7f040036

.field public static final coins_icon:I = 0x7f040044

.field public static final confirm:I = 0x7f04000b

.field public static final confirm_error:I = 0x7f04000a

.field public static final confirm_password:I = 0x7f040052

.field public static final create:I = 0x7f04000e

.field public static final current_password:I = 0x7f040050

.field public static final date:I = 0x7f04005e

.field public static final deny:I = 0x7f04001f

.field public static final desc:I = 0x7f040047

.field public static final description:I = 0x7f040003

.field public static final email:I = 0x7f04000d

.field public static final email_error:I = 0x7f04000c

.field public static final empty_list:I = 0x7f040015

.field public static final existing:I = 0x7f04004b

.field public static final external_provider:I = 0x7f040040

.field public static final extra:I = 0x7f040010

.field public static final facebook_login:I = 0x7f040033

.field public static final footer:I = 0x7f040017

.field public static final friended_section:I = 0x7f04001c

.field public static final get_coins:I = 0x7f040058

.field public static final guest:I = 0x7f04004c

.field public static final header:I = 0x7f040023

.field public static final home:I = 0x7f040025

.field public static final icon:I = 0x7f04000f

.field public static final image:I = 0x7f040046

.field public static final list:I = 0x7f040016

.field public static final login:I = 0x7f04003e

.field public static final logo:I = 0x7f040024

.field public static final logout:I = 0x7f040057

.field public static final lost_password:I = 0x7f04003d

.field public static final mail:I = 0x7f040035

.field public static final message:I = 0x7f040034

.field public static final messages:I = 0x7f040026

.field public static final more:I = 0x7f040039

.field public static final name:I = 0x7f040005

.field public static final new_password:I = 0x7f040051

.field public static final notifications:I = 0x7f040056

.field public static final num_items:I = 0x7f04005a

.field public static final offline_error:I = 0x7f040031

.field public static final offline_error_underline:I = 0x7f040032

.field public static final online_text:I = 0x7f04001b

.field public static final password:I = 0x7f040009

.field public static final password_box:I = 0x7f040054

.field public static final password_error:I = 0x7f040008

.field public static final paypal:I = 0x7f040022

.field public static final pic:I = 0x7f040004

.field public static final points:I = 0x7f040001

.field public static final popup_bottom_half:I = 0x7f040048

.field public static final popup_header:I = 0x7f04003a

.field public static final popup_top_half:I = 0x7f040045

.field public static final price:I = 0x7f040059

.field public static final profile_pic:I = 0x7f04001a

.field public static final progressbar:I = 0x7f04003b

.field public static final publisher:I = 0x7f040006

.field public static final purchase:I = 0x7f040049

.field public static final rank:I = 0x7f040037

.field public static final reply:I = 0x7f04005b

.field public static final row:I = 0x7f04003f

.field public static final score:I = 0x7f040038

.field public static final select_text:I = 0x7f04003c

.field public static final send:I = 0x7f04005c

.field public static final set_pass:I = 0x7f040053

.field public static final set_password_box:I = 0x7f04004f

.field public static final sheader:I = 0x7f040028

.field public static final sheader_btn1:I = 0x7f04002b

.field public static final sheader_btn1_dropdown:I = 0x7f04002d

.field public static final sheader_btn1_text:I = 0x7f04002c

.field public static final sheader_btn2:I = 0x7f04002e

.field public static final sheader_btn2_dropdown:I = 0x7f040030

.field public static final sheader_btn2_text:I = 0x7f04002f

.field public static final sheader_icon:I = 0x7f040029

.field public static final sheader_title:I = 0x7f04002a

.field public static final speech_bubble:I = 0x7f04005d

.field public static final submit:I = 0x7f040041

.field public static final success:I = 0x7f040042

.field public static final swarm_purchase_popup:I = 0x7f040043

.field public static final tapjoy:I = 0x7f040020

.field public static final terms:I = 0x7f040014

.field public static final title:I = 0x7f040002

.field public static final unfriended_section:I = 0x7f04001d

.field public static final upgrade:I = 0x7f04004e

.field public static final upgrade_box:I = 0x7f04004d

.field public static final username:I = 0x7f040012

.field public static final username_error:I = 0x7f040011

.field public static final webview:I = 0x7f04005f

.field public static final zong:I = 0x7f040021


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
