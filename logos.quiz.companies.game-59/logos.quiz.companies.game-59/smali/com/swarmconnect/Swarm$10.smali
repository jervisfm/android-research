.class Lcom/swarmconnect/Swarm$10;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/Swarm;->a(Landroid/content/Context;Lcom/facebook/android/Facebook;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/Swarm$10;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    check-cast p1, Lcom/swarmconnect/w;

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    iget v0, v0, Lcom/swarmconnect/SwarmUser;->userId:I

    if-lez v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v1, p1, Lcom/swarmconnect/w;->auth:Ljava/lang/String;

    iget v2, p1, Lcom/swarmconnect/w;->newMessages:I

    invoke-static {v0, v1, v2}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p1, Lcom/swarmconnect/w;->needsUsername:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/swarmconnect/w;->username:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/swarmconnect/w;->username:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p1, Lcom/swarmconnect/w;->username:Ljava/lang/String;

    sput-object v0, Lcom/swarmconnect/ar;->prefillUsername:Ljava/lang/String;

    :cond_2
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/swarmconnect/Swarm$10;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/Swarm$10;->a:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/swarmconnect/w;->statusMessage:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " ... error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/swarmconnect/w;->statusCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 3

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    iget-object v0, p0, Lcom/swarmconnect/Swarm$10;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/Swarm$10;->a:Landroid/content/Context;

    const-string v1, "Facebook login failed."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    const-string v0, "FacebookLoginAPI request failed"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    return-void
.end method
