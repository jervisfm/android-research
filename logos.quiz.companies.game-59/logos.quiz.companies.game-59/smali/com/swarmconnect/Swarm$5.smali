.class Lcom/swarmconnect/Swarm$5;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/Swarm;->autoLogin(Lcom/swarmconnect/Swarm$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Lcom/swarmconnect/Swarm$a;


# direct methods
.method constructor <init>(Lcom/swarmconnect/Swarm$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/Swarm$5;->a:Lcom/swarmconnect/Swarm$a;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    iget v0, p1, Lcom/swarmconnect/APICall;->statusCode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/Swarm$5;->a:Lcom/swarmconnect/Swarm$a;

    invoke-virtual {v0}, Lcom/swarmconnect/Swarm$a;->loginFailed()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->e()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/Swarm$5;->a:Lcom/swarmconnect/Swarm$a;

    invoke-virtual {v0}, Lcom/swarmconnect/Swarm$a;->loginSuccess()V

    check-cast p1, Lcom/swarmconnect/ce;

    iget-object v0, p1, Lcom/swarmconnect/ce;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v1, p1, Lcom/swarmconnect/ce;->auth:Ljava/lang/String;

    iget v2, p1, Lcom/swarmconnect/ce;->newMessages:I

    invoke-static {v0, v1, v2}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/Swarm$5;->a:Lcom/swarmconnect/Swarm$a;

    invoke-virtual {v0}, Lcom/swarmconnect/Swarm$a;->loginFailed()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->a()V

    return-void
.end method
