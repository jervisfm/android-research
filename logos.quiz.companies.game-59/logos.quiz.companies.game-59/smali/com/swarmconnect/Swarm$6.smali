.class Lcom/swarmconnect/Swarm$6;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmNotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Lcom/swarmconnect/SwarmNotification;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmNotification;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/Swarm$6;->a:Lcom/swarmconnect/SwarmNotification;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/swarmconnect/Swarm$6;->a:Lcom/swarmconnect/SwarmNotification;

    instance-of v0, v0, Lcom/swarmconnect/NotificationMessage;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v0, v0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iput v1, v0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    :cond_0
    const/4 v2, 0x0

    invoke-static {}, Lcom/swarmconnect/Swarm;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/Swarm$6;->a:Lcom/swarmconnect/SwarmNotification;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->b(Lcom/swarmconnect/SwarmNotification;)V

    :cond_2
    return-void

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/delegates/SwarmNotificationDelegate;

    iget-object v4, p0, Lcom/swarmconnect/Swarm$6;->a:Lcom/swarmconnect/SwarmNotification;

    invoke-interface {v0, v4}, Lcom/swarmconnect/delegates/SwarmNotificationDelegate;->gotNotification(Lcom/swarmconnect/SwarmNotification;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0
.end method
