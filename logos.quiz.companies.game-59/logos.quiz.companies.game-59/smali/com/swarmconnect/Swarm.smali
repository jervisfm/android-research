.class public Lcom/swarmconnect/Swarm;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/Swarm$a;
    }
.end annotation


# static fields
.field protected static a:Landroid/content/Context;

.field protected static b:Landroid/app/Activity;

.field protected static c:I

.field protected static d:Ljava/lang/String;

.field protected static e:Ljava/lang/String;

.field protected static f:Ljava/lang/String;

.field protected static g:I

.field protected static h:Z

.field protected static i:Ljava/lang/String;

.field protected static j:Landroid/content/SharedPreferences;

.field private static k:Lcom/swarmconnect/SwarmConnectivityReceiver;

.field private static l:Lcom/swarmconnect/SwarmPushListener;

.field private static m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/swarmconnect/delegates/SwarmNotificationDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private static n:Z

.field private static o:Landroid/app/Dialog;

.field public static user:Lcom/swarmconnect/SwarmActiveUser;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/swarmconnect/Swarm;->m:Ljava/util/ArrayList;

    sput v1, Lcom/swarmconnect/Swarm;->c:I

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/Swarm;->f:Ljava/lang/String;

    sput v1, Lcom/swarmconnect/Swarm;->g:I

    sput-boolean v1, Lcom/swarmconnect/Swarm;->h:Z

    sput-object v2, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/Swarm;->i:Ljava/lang/String;

    sput-object v2, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/Swarm;->n:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a()V
    .locals 5

    const/4 v2, 0x0

    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_1

    const-string v0, "Swarm going into Offline Mode"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    sput-boolean v2, Lcom/swarmconnect/Swarm;->n:Z

    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    check-cast v0, Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->refresh()V

    :cond_0
    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-nez v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v1, "swarmId"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v2, "swarmUsername"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v3, "swarmAuth"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-boolean v0, Lcom/swarmconnect/Swarm;->n:Z

    if-nez v0, :cond_2

    if-lez v1, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/swarmconnect/SwarmUser;

    invoke-direct {v0, v1}, Lcom/swarmconnect/SwarmUser;-><init>(I)V

    iput-object v2, v0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    sput-object v3, Lcom/swarmconnect/Swarm;->i:Ljava/lang/String;

    :goto_0
    new-instance v1, Lcom/swarmconnect/SwarmActiveUser;

    invoke-direct {v1, v0}, Lcom/swarmconnect/SwarmActiveUser;-><init>(Lcom/swarmconnect/SwarmUser;)V

    sput-object v1, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-static {v0}, Lcom/swarmconnect/SwarmLoginManager;->a(Lcom/swarmconnect/SwarmActiveUser;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/swarmconnect/SwarmUser;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/swarmconnect/SwarmUser;-><init>(I)V

    const-string v1, "OfflineGuest"

    iput-object v1, v0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;)V
    .locals 5

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    const-string v0, "SwarmPrefs"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v1, "swarmId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v2, "swarmAuth"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v3, "swarmAppAuth"

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-lez v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    sput-object v2, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    new-instance v2, Lcom/swarmconnect/ce;

    invoke-direct {v2}, Lcom/swarmconnect/ce;-><init>()V

    sget-object v3, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    iput-object v3, v2, Lcom/swarmconnect/ce;->device:Ljava/lang/String;

    iput v0, v2, Lcom/swarmconnect/ce;->userId:I

    iput-object v1, v2, Lcom/swarmconnect/ce;->auth:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/Swarm$11;

    invoke-direct {v0}, Lcom/swarmconnect/Swarm$11;-><init>()V

    iput-object v0, v2, Lcom/swarmconnect/ce;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v2}, Lcom/swarmconnect/ce;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected static a(Landroid/content/Context;Lcom/facebook/android/Facebook;)V
    .locals 2

    invoke-static {}, Lcom/swarmconnect/Swarm;->c()V

    new-instance v0, Lcom/swarmconnect/w;

    invoke-direct {v0}, Lcom/swarmconnect/w;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/w;->authKey:Ljava/lang/String;

    invoke-static {p0}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/w;->device:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/Swarm$10;

    invoke-direct {v1, p0}, Lcom/swarmconnect/Swarm$10;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/swarmconnect/w;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/w;->run()V

    return-void
.end method

.method protected static a(Lcom/swarmconnect/SwarmNotification;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Swarm got notification: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$6;

    invoke-direct {v1, p0}, Lcom/swarmconnect/Swarm$6;-><init>(Lcom/swarmconnect/SwarmNotification;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected static a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;I)V

    return-void
.end method

.method protected static a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;I)V
    .locals 3

    new-instance v0, Lcom/swarmconnect/SwarmActiveUser;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmActiveUser;-><init>(Lcom/swarmconnect/SwarmUser;)V

    sput-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iput p2, v0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    sput-object p1, Lcom/swarmconnect/Swarm;->i:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v1, "SwarmPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v0, v0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "swarmAppAuth"

    sget-object v2, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmUsername"

    sget-object v2, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget-object v2, v2, Lcom/swarmconnect/SwarmActiveUser;->username:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmAuth"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmId"

    sget-object v2, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v2, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    invoke-static {}, Lcom/swarmconnect/APICall;->c()Z

    invoke-static {}, Lcom/swarmconnect/Swarm;->g()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-static {v0}, Lcom/swarmconnect/SwarmLoginManager;->a(Lcom/swarmconnect/SwarmActiveUser;)V

    invoke-static {}, Lcom/swarmconnect/u;->a()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swarm-user-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v1, v1, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/SwarmIO;->addGroup(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/SwarmPushListener;->a()V

    return-void
.end method

.method protected static a(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p0, :cond_1

    const-string p0, "Loading..."

    :cond_1
    new-instance v0, Landroid/app/Dialog;

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    const-string v1, "@layout/swarm_loading_popup"

    sget-object v2, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(I)V

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    const-string v1, "@id/popup_header"

    sget-object v2, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->loadingPopupBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    const-string v1, "@id/message"

    sget-object v2, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public static addLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/SwarmLoginManager;->addLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    return-void
.end method

.method public static addNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static autoLogin(Lcom/swarmconnect/Swarm$a;)V
    .locals 4

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/swarmconnect/Swarm$a;->loginSuccess()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/swarmconnect/Swarm$a;->loginFailed()V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v1, "swarmId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v2, "swarmAuth"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-lez v0, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    const-string v2, "Logging In..."

    invoke-static {v2}, Lcom/swarmconnect/Swarm;->a(Ljava/lang/String;)V

    new-instance v2, Lcom/swarmconnect/ce;

    invoke-direct {v2}, Lcom/swarmconnect/ce;-><init>()V

    sget-object v3, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    iput-object v3, v2, Lcom/swarmconnect/ce;->device:Ljava/lang/String;

    iput v0, v2, Lcom/swarmconnect/ce;->userId:I

    iput-object v1, v2, Lcom/swarmconnect/ce;->auth:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/Swarm$5;

    invoke-direct {v0, p0}, Lcom/swarmconnect/Swarm$5;-><init>(Lcom/swarmconnect/Swarm$a;)V

    iput-object v0, v2, Lcom/swarmconnect/ce;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v2}, Lcom/swarmconnect/ce;->run()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->getSession()Lcom/facebook/android/Facebook;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/swarmconnect/Swarm;->a(Landroid/content/Context;Lcom/facebook/android/Facebook;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/swarmconnect/Swarm$a;->loginFailed()V

    goto :goto_0
.end method

.method protected static b()V
    .locals 2

    sget-boolean v0, Lcom/swarmconnect/Swarm;->n:Z

    if-nez v0, :cond_1

    const-string v0, "Swarm going into Online Mode"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/Swarm;->n:Z

    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/swarmconnect/Swarm;->g()V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$4;

    invoke-direct {v1}, Lcom/swarmconnect/Swarm$4;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Landroid/content/Context;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmActiveUser;->isOfflineGuest()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/swarmconnect/APICall;->c()Z

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$8;

    invoke-direct {v1}, Lcom/swarmconnect/Swarm$8;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected static b(Lcom/swarmconnect/SwarmNotification;)V
    .locals 2

    instance-of v0, p0, Lcom/swarmconnect/NotificationPurchase;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget-object v0, v0, Lcom/swarmconnect/SwarmActiveUser;->a:Lcom/swarmconnect/SwarmUserInventory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget-object v1, v0, Lcom/swarmconnect/SwarmActiveUser;->a:Lcom/swarmconnect/SwarmUserInventory;

    move-object v0, p0

    check-cast v0, Lcom/swarmconnect/NotificationPurchase;

    iget-object v0, v0, Lcom/swarmconnect/NotificationPurchase;->listing:Lcom/swarmconnect/SwarmStoreListing;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmUserInventory;->a(Lcom/swarmconnect/SwarmStoreListing;)V

    :cond_0
    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/Swarm$2;-><init>(Lcom/swarmconnect/SwarmNotification;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method protected static b(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$7;

    invoke-direct {v1, p0}, Lcom/swarmconnect/Swarm$7;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected static c()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected static c(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method protected static d()V
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/Swarm;->o:Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic e()V
    .locals 0

    invoke-static {}, Lcom/swarmconnect/Swarm;->h()V

    return-void
.end method

.method public static enableAlternativeMarketCompatability()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/u;->b:Z

    return-void
.end method

.method static synthetic f()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static g()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/swarmconnect/SwarmLeaderboard;->getLeaderboardsList(Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;)V

    invoke-static {v1}, Lcom/swarmconnect/SwarmStore;->getStore(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmActiveUser;->isOfflineGuest()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1}, Lcom/swarmconnect/SwarmAchievement;->getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getInventory(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getCoins(Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V

    :cond_0
    return-void
.end method

.method public static getLastLogin()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v2, "SwarmPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "swarmUsername"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static h()V
    .locals 3

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v1, "SwarmPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "swarmAppAuth"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmUsername"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmAuth"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "swarmId"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method public static init(Landroid/app/Activity;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/swarmconnect/Swarm;->init(Landroid/app/Activity;ILjava/lang/String;Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    return-void
.end method

.method public static init(Landroid/app/Activity;ILjava/lang/String;Lcom/swarmconnect/delegates/SwarmLoginListener;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_0
    sput p1, Lcom/swarmconnect/Swarm;->c:I

    sput-object p2, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    invoke-static {p0}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/Swarm;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->a:Landroid/content/Context;

    :try_start_0
    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v3, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v3, Lcom/swarmconnect/Swarm;->g:I

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/swarmconnect/Swarm;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    new-instance v0, Lcom/swarmconnect/SwarmConnectivityReceiver;

    invoke-direct {v0}, Lcom/swarmconnect/SwarmConnectivityReceiver;-><init>()V

    sput-object v0, Lcom/swarmconnect/Swarm;->k:Lcom/swarmconnect/SwarmConnectivityReceiver;

    sget-object v0, Lcom/swarmconnect/Swarm;->a:Landroid/content/Context;

    sget-object v3, Lcom/swarmconnect/Swarm;->k:Lcom/swarmconnect/SwarmConnectivityReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/swarmconnect/SwarmPushListener;

    invoke-direct {v0}, Lcom/swarmconnect/SwarmPushListener;-><init>()V

    sput-object v0, Lcom/swarmconnect/Swarm;->l:Lcom/swarmconnect/SwarmPushListener;

    sget-object v0, Lcom/swarmconnect/Swarm;->a:Landroid/content/Context;

    sget-object v3, Lcom/swarmconnect/Swarm;->l:Lcom/swarmconnect/SwarmPushListener;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.swarmconnect.CUSTOM_PUSH_RECEIVED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "android.permission.RECEIVE_BOOT_COMPLETED"

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/swarmconnect/Swarm;->h:Z

    sget-boolean v0, Lcom/swarmconnect/Swarm;->h:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/swarmconnect/SwarmIO;->setStartMode(I)V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v3, "1ghy789ghXC18d1yA98y45olaC"

    invoke-static {v0, v1, v3}, Lcom/swarmconnect/SwarmIO;->init(Landroid/content/Context;ILjava/lang/String;)V

    const-class v0, Lcom/swarmconnect/SwarmPushListener;

    invoke-static {v0}, Lcom/swarmconnect/SwarmIO;->setPushReciever(Ljava/lang/Class;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swarm-app-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/swarmconnect/Swarm;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-v"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/swarmconnect/Swarm;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/SwarmIO;->addGroup(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/SwarmIO;->getGroups()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    array-length v0, v3

    if-lez v0, :cond_2

    array-length v4, v3

    move v0, v2

    :goto_3
    if-lt v0, v4, :cond_7

    :cond_2
    invoke-static {p0}, Lcom/swarmconnect/Swarm;->setActive(Landroid/app/Activity;)V

    invoke-static {p3}, Lcom/swarmconnect/SwarmLoginManager;->addLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    invoke-static {}, Lcom/swarmconnect/Swarm;->g()V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v1, "SwarmPrefs"

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    instance-of v0, p0, Lcom/swarmconnect/SwarmMainActivity;

    if-nez v0, :cond_3

    invoke-static {}, Lcom/swarmconnect/SwarmLoginManager;->b()V

    new-instance v0, Lcom/swarmconnect/Swarm$9;

    invoke-direct {v0}, Lcom/swarmconnect/Swarm$9;-><init>()V

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->autoLogin(Lcom/swarmconnect/Swarm$a;)V

    :cond_3
    new-instance v0, Lcom/swarmconnect/cp;

    invoke-direct {v0}, Lcom/swarmconnect/cp;-><init>()V

    sget-object v1, Lcom/swarmconnect/Swarm;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/swarmconnect/cp;->versionName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/swarmconnect/cp;->run()V

    :cond_4
    :goto_4
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    const/4 v0, 0x2

    goto :goto_2

    :cond_7
    aget-object v5, v3, v0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "swarm-app-"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v7, Lcom/swarmconnect/Swarm;->c:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-v"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-static {v5}, Lcom/swarmconnect/SwarmIO;->removeGroup(Ljava/lang/String;)V

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v0, v0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    invoke-static {}, Lcom/swarmconnect/Swarm;->logOut()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->showLogin()V

    goto :goto_4
.end method

.method public static isEnabled()Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v2, "SwarmPrefs"

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sput-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    :cond_0
    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/swarmconnect/Swarm;->j:Landroid/content/SharedPreferences;

    const-string v2, "swarmId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static isInitialized()Z
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLoggedIn()Z
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOnline()Z
    .locals 1

    sget-boolean v0, Lcom/swarmconnect/Swarm;->n:Z

    return v0
.end method

.method public static isSwarmEnabled()Z
    .locals 1

    invoke-static {}, Lcom/swarmconnect/Swarm;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public static logOut()V
    .locals 2

    invoke-static {}, Lcom/swarmconnect/Swarm;->h()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swarm-user-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v1, v1, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/SwarmIO;->removeGroup(Ljava/lang/String;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-static {}, Lcom/swarmconnect/SwarmLoginManager;->c()V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    instance-of v0, v0, Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    :cond_0
    return-void
.end method

.method public static removeLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/SwarmLoginManager;->removeLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    return-void
.end method

.method public static removeNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/Swarm;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static setActive(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eq p0, v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    :cond_0
    sput-object p0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    :cond_2
    sget v0, Lcom/swarmconnect/ui/UiConf;->density:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/swarmconnect/ui/UiConf;->density:F

    :cond_3
    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/swarmconnect/SwarmIO;->startService()V

    :cond_4
    return-void
.end method

.method public static setAllowGuests(Z)V
    .locals 0

    sput-boolean p0, Lcom/swarmconnect/cb;->allowGuestAccounts:Z

    return-void
.end method

.method public static setInactive(Landroid/app/Activity;)V
    .locals 4

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-ne v0, p0, :cond_0

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    sget-boolean v0, Lcom/swarmconnect/Swarm;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/swarmconnect/Swarm$3;

    invoke-direct {v1}, Lcom/swarmconnect/Swarm$3;-><init>()V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    return-void
.end method

.method public static show(I)V
    .locals 2

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    new-instance v1, Lcom/swarmconnect/Swarm$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/Swarm$1;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public static showAchievements()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method

.method public static showDashboard()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method

.method public static showGetCoins()V
    .locals 1

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method

.method public static showLeaderboards()V
    .locals 1

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method

.method public static showLogin()V
    .locals 1

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    :cond_0
    return-void
.end method

.method public static showStore()V
    .locals 1

    const/16 v0, 0xc

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method
