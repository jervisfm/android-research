.class Lcom/swarmconnect/SwarmAchievement$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmAchievement;->unlock(Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmAchievement;

.field private final synthetic b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmAchievement;Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmAchievement$1;->a:Lcom/swarmconnect/SwarmAchievement;

    iput-object p2, p0, Lcom/swarmconnect/SwarmAchievement$1;->b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    check-cast p1, Lcom/swarmconnect/y;

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;

    iget-boolean v1, p1, Lcom/swarmconnect/y;->unlocked:Z

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;->achievementUnlocked(Z)V

    :cond_0
    iget-boolean v0, p1, Lcom/swarmconnect/y;->unlocked:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swarmconnect/NotificationAchievement;

    iget-object v1, p0, Lcom/swarmconnect/SwarmAchievement$1;->a:Lcom/swarmconnect/SwarmAchievement;

    invoke-direct {v0, v1}, Lcom/swarmconnect/NotificationAchievement;-><init>(Lcom/swarmconnect/SwarmAchievement;)V

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmNotification;)V

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->a:Lcom/swarmconnect/SwarmAchievement;

    iget-boolean v1, p1, Lcom/swarmconnect/y;->unlocked:Z

    iput-boolean v1, v0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    return-void
.end method

.method public requestFailed()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->b:Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;->achievementUnlocked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$1;->a:Lcom/swarmconnect/SwarmAchievement;

    iput-boolean v1, v0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    return-void
.end method
