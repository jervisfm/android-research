.class Lcom/swarmconnect/SwarmAchievement$2;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmAchievement;->getAchievementsMap(Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmAchievement$2;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 4

    check-cast p1, Lcom/swarmconnect/p;

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$2;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p1, Lcom/swarmconnect/p;->achievements:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/p;->achievements:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$2;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;->gotMap(Ljava/util/Map;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmAchievement;

    iget v3, v0, Lcom/swarmconnect/SwarmAchievement;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$2;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$2;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;->gotMap(Ljava/util/Map;)V

    :cond_0
    return-void
.end method
