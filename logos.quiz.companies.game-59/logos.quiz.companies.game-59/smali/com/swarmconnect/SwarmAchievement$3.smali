.class Lcom/swarmconnect/SwarmAchievement$3;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmAchievement;->getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmAchievement$3;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    check-cast p1, Lcom/swarmconnect/p;

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$3;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$3;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;

    iget-object v1, p1, Lcom/swarmconnect/p;->achievements:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;->gotList(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public requestFailed()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$3;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmAchievement$3;->a:Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;->gotList(Ljava/util/List;)V

    :cond_0
    return-void
.end method
