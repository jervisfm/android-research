.class public Lcom/swarmconnect/SwarmAchievement;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;,
        Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;,
        Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;
    }
.end annotation


# instance fields
.field protected a:J

.field public description:Ljava/lang/String;

.field public hidden:Z

.field public id:I

.field public orderId:I

.field public points:I

.field public title:Ljava/lang/String;

.field public unlocked:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/p;

    invoke-direct {v0}, Lcom/swarmconnect/p;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmAchievement$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmAchievement$3;-><init>(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V

    iput-object v1, v0, Lcom/swarmconnect/p;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/p;->run()V

    return-void
.end method

.method public static getAchievementsMap(Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/p;

    invoke-direct {v0}, Lcom/swarmconnect/p;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmAchievement$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmAchievement$2;-><init>(Lcom/swarmconnect/SwarmAchievement$GotAchievementsMapCB;)V

    iput-object v1, v0, Lcom/swarmconnect/p;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/p;->run()V

    return-void
.end method


# virtual methods
.method public getUnlockDate()Ljava/util/Date;
    .locals 5

    iget-boolean v0, p0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/swarmconnect/SwarmAchievement;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lcom/swarmconnect/SwarmAchievement;->a:J

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public unlock()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmAchievement;->unlock(Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;)V

    return-void
.end method

.method public unlock(Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;)V
    .locals 2

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    new-instance v0, Lcom/swarmconnect/y;

    invoke-direct {v0}, Lcom/swarmconnect/y;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmAchievement;->id:I

    iput v1, v0, Lcom/swarmconnect/y;->achievementId:I

    new-instance v1, Lcom/swarmconnect/SwarmAchievement$1;

    invoke-direct {v1, p0, p1}, Lcom/swarmconnect/SwarmAchievement$1;-><init>(Lcom/swarmconnect/SwarmAchievement;Lcom/swarmconnect/SwarmAchievement$AchievementUnlockedCB;)V

    iput-object v1, v0, Lcom/swarmconnect/y;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/y;->run()V

    :cond_0
    return-void
.end method
