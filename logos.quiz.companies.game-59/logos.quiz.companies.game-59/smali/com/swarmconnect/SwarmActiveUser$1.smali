.class Lcom/swarmconnect/SwarmActiveUser$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmActiveUser;->getCloudData(Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmActiveUser;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmActiveUser;Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iput-object p2, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 7

    check-cast p1, Lcom/swarmconnect/co;

    iget-object v0, p1, Lcom/swarmconnect/co;->data:Ljava/lang/String;

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v2, "SwarmPrefs"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-wide v2, p1, Lcom/swarmconnect/co;->timestamp:J

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "clouddata_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iget v5, v5, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_ts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    invoke-interface {v1, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clouddata_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iget v3, v3, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/swarmconnect/co;->data:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clouddata_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iget v3, v3, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_ts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;->gotData(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "clouddata_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v2, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v1, "SwarmPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clouddata_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/SwarmActiveUser$1;->a:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v2, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/SwarmActiveUser$1;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;->gotData(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$1;->c:Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;

    invoke-virtual {v0, v3}, Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;->gotData(Ljava/lang/String;)V

    goto :goto_0
.end method
