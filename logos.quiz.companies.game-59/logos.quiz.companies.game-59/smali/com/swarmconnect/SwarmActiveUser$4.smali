.class Lcom/swarmconnect/SwarmActiveUser$4;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmActiveUser;->a(Lcom/swarmconnect/GetFriendsAPI$FriendStatus;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmActiveUser;

.field private final synthetic b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmActiveUser;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmActiveUser$4;->a:Lcom/swarmconnect/SwarmActiveUser;

    iput-object p2, p0, Lcom/swarmconnect/SwarmActiveUser$4;->b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    check-cast p1, Lcom/swarmconnect/GetFriendsAPI;

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$4;->b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/swarmconnect/GetFriendsAPI;->friends:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/swarmconnect/GetFriendsAPI;->friends:Ljava/util/List;

    :cond_0
    iget-object v0, p1, Lcom/swarmconnect/GetFriendsAPI;->incomingRequests:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/swarmconnect/GetFriendsAPI;->incomingRequests:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$4;->b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;

    iget-object v1, p1, Lcom/swarmconnect/GetFriendsAPI;->friends:Ljava/util/List;

    iget-object v2, p1, Lcom/swarmconnect/GetFriendsAPI;->incomingRequests:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;->gotFriends(Ljava/util/List;Ljava/util/List;)V

    :cond_2
    return-void
.end method

.method public requestFailed()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$4;->b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser$4;->b:Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;

    invoke-virtual {v0, v1, v1}, Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;->gotFriends(Ljava/util/List;Ljava/util/List;)V

    :cond_0
    return-void
.end method
