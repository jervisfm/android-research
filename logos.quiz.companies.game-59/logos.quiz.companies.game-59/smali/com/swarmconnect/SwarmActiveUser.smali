.class public Lcom/swarmconnect/SwarmActiveUser;
.super Lcom/swarmconnect/SwarmUser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmActiveUser$FriendRequestCB;,
        Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;,
        Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;,
        Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;,
        Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;,
        Lcom/swarmconnect/SwarmActiveUser$SaveCloudDataCB;
    }
.end annotation


# instance fields
.field protected a:Lcom/swarmconnect/SwarmUserInventory;

.field public numNewMessages:I


# direct methods
.method public constructor <init>(Lcom/swarmconnect/SwarmUser;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/SwarmUser;-><init>(Lcom/swarmconnect/SwarmUser;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    return-void
.end method


# virtual methods
.method protected a(IZLcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V
    .locals 2

    new-instance v1, Lcom/swarmconnect/br;

    invoke-direct {v1}, Lcom/swarmconnect/br;-><init>()V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Lcom/swarmconnect/br;->action:I

    iput p1, v1, Lcom/swarmconnect/br;->friendId:I

    new-instance v0, Lcom/swarmconnect/SwarmActiveUser$3;

    invoke-direct {v0, p0, p3}, Lcom/swarmconnect/SwarmActiveUser$3;-><init>(Lcom/swarmconnect/SwarmActiveUser;Lcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    iput-object v0, v1, Lcom/swarmconnect/br;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1}, Lcom/swarmconnect/br;->run()V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method protected a(Lcom/swarmconnect/GetFriendsAPI$FriendStatus;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/GetFriendsAPI;

    invoke-direct {v0}, Lcom/swarmconnect/GetFriendsAPI;-><init>()V

    iput-object p1, v0, Lcom/swarmconnect/GetFriendsAPI;->status:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;

    new-instance v1, Lcom/swarmconnect/SwarmActiveUser$4;

    invoke-direct {v1, p0, p2}, Lcom/swarmconnect/SwarmActiveUser$4;-><init>(Lcom/swarmconnect/SwarmActiveUser;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V

    iput-object v1, v0, Lcom/swarmconnect/GetFriendsAPI;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/GetFriendsAPI;->run()V

    return-void
.end method

.method public acceptFriendRequest(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/swarmconnect/SwarmActiveUser;->addFriend(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    return-void
.end method

.method public addFriend(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/swarmconnect/SwarmActiveUser;->a(IZLcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    return-void
.end method

.method public deleteFriend(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/swarmconnect/SwarmActiveUser;->a(IZLcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    return-void
.end method

.method public getCloudData(Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/swarmconnect/co;

    invoke-direct {v0}, Lcom/swarmconnect/co;-><init>()V

    iput-object p1, v0, Lcom/swarmconnect/co;->key:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmActiveUser$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/swarmconnect/SwarmActiveUser$1;-><init>(Lcom/swarmconnect/SwarmActiveUser;Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$GotCloudDataCB;)V

    iput-object v1, v0, Lcom/swarmconnect/co;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/co;->run()V

    goto :goto_0
.end method

.method public getCoins(Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/bo;

    invoke-direct {v0}, Lcom/swarmconnect/bo;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmActiveUser$2;

    invoke-direct {v1, p0, p1}, Lcom/swarmconnect/SwarmActiveUser$2;-><init>(Lcom/swarmconnect/SwarmActiveUser;Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V

    iput-object v1, v0, Lcom/swarmconnect/bo;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bo;->run()V

    return-void
.end method

.method public getFriendRequests(Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/GetFriendsAPI$FriendStatus;->PENDING:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;

    invoke-virtual {p0, v0, p1}, Lcom/swarmconnect/SwarmActiveUser;->a(Lcom/swarmconnect/GetFriendsAPI$FriendStatus;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V

    return-void
.end method

.method public getFriends(Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/GetFriendsAPI$FriendStatus;->ALL:Lcom/swarmconnect/GetFriendsAPI$FriendStatus;

    invoke-virtual {p0, v0, p1}, Lcom/swarmconnect/SwarmActiveUser;->a(Lcom/swarmconnect/GetFriendsAPI$FriendStatus;Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V

    return-void
.end method

.method public getInventory(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->a:Lcom/swarmconnect/SwarmUserInventory;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/swarmconnect/SwarmUserInventory;->a(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)Lcom/swarmconnect/SwarmUserInventory;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->a:Lcom/swarmconnect/SwarmUserInventory;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->a:Lcom/swarmconnect/SwarmUserInventory;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/SwarmUserInventory;->refresh(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V

    goto :goto_0
.end method

.method public isGuestAccount()Z
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->username:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->username:Ljava/lang/String;

    const-string v1, "guest-[0-9a-f]+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmActiveUser;->username:Ljava/lang/String;

    const-string v1, "OfflineGuest"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOfflineGuest()Z
    .locals 2

    iget v0, p0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveCloudData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/swarmconnect/SwarmActiveUser;->saveCloudData(Ljava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$SaveCloudDataCB;)V

    return-void
.end method

.method public saveCloudData(Ljava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$SaveCloudDataCB;)V
    .locals 6

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/swarmconnect/as;

    invoke-direct {v0}, Lcom/swarmconnect/as;-><init>()V

    iput-object p1, v0, Lcom/swarmconnect/as;->key:Ljava/lang/String;

    iput-object p2, v0, Lcom/swarmconnect/as;->data:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmActiveUser$5;

    invoke-direct {v1, p0, p3}, Lcom/swarmconnect/SwarmActiveUser$5;-><init>(Lcom/swarmconnect/SwarmActiveUser;Lcom/swarmconnect/SwarmActiveUser$SaveCloudDataCB;)V

    iput-object v1, v0, Lcom/swarmconnect/as;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/as;->run()V

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-string v1, "SwarmPrefs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clouddata_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clouddata_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public upgradeGuest()V
    .locals 1

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmActiveUser;->isGuestAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->show(I)V

    :cond_0
    return-void
.end method
