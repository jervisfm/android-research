.class public final enum Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/SwarmChallenge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChallengeStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACCEPTED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

.field public static final enum COMPLETED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

.field public static final enum DECLINED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

.field public static final enum PENDING:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

.field private static final synthetic a:[Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->PENDING:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    new-instance v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v3}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->ACCEPTED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    new-instance v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    const-string v1, "DECLINED"

    invoke-direct {v0, v1, v4}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->DECLINED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    new-instance v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->COMPLETED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    sget-object v1, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->PENDING:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->ACCEPTED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->DECLINED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->COMPLETED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->a:[Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;
    .locals 1

    const-class v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    return-object v0
.end method

.method public static values()[Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->a:[Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
