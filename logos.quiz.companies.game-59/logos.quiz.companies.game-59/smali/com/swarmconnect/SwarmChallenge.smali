.class Lcom/swarmconnect/SwarmChallenge;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;,
        Lcom/swarmconnect/SwarmChallenge$ChallengeType;,
        Lcom/swarmconnect/SwarmChallenge$ModChallengeCB;,
        Lcom/swarmconnect/SwarmChallenge$SentChallengeCB;
    }
.end annotation


# instance fields
.field public app:Lcom/swarmconnect/SwarmApplication;

.field public data:Ljava/lang/String;

.field public id:I

.field public onlineOnly:Z

.field public status:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

.field public type:Lcom/swarmconnect/SwarmChallenge$ChallengeType;

.field public user:Lcom/swarmconnect/SwarmUser;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->PENDING:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    iput-object v0, p0, Lcom/swarmconnect/SwarmChallenge;->status:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    sget-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeType;->CUSTOM:Lcom/swarmconnect/SwarmChallenge$ChallengeType;

    iput-object v0, p0, Lcom/swarmconnect/SwarmChallenge;->type:Lcom/swarmconnect/SwarmChallenge$ChallengeType;

    return-void
.end method

.method private a(ZLcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V
    .locals 2

    new-instance v1, Lcom/swarmconnect/h;

    invoke-direct {v1}, Lcom/swarmconnect/h;-><init>()V

    iget v0, p0, Lcom/swarmconnect/SwarmChallenge;->id:I

    iput v0, v1, Lcom/swarmconnect/h;->challengeId:I

    if-eqz p1, :cond_0

    sget-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->ACCEPTED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Lcom/swarmconnect/h;->status:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/SwarmChallenge$1;

    invoke-direct {v0, p0, p2}, Lcom/swarmconnect/SwarmChallenge$1;-><init>(Lcom/swarmconnect/SwarmChallenge;Lcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V

    iput-object v0, v1, Lcom/swarmconnect/h;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1}, Lcom/swarmconnect/h;->run()V

    return-void

    :cond_0
    sget-object v0, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->DECLINED:Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmChallenge$ChallengeStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static sendChallenge(ILjava/lang/String;ZILcom/swarmconnect/SwarmChallenge$SentChallengeCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/an;

    invoke-direct {v0}, Lcom/swarmconnect/an;-><init>()V

    iput p0, v0, Lcom/swarmconnect/an;->inviteId:I

    iput-object p1, v0, Lcom/swarmconnect/an;->data:Ljava/lang/String;

    iput-boolean p2, v0, Lcom/swarmconnect/an;->onlineOnly:Z

    iput p3, v0, Lcom/swarmconnect/an;->expiration:I

    new-instance v1, Lcom/swarmconnect/SwarmChallenge$2;

    invoke-direct {v1, p4}, Lcom/swarmconnect/SwarmChallenge$2;-><init>(Lcom/swarmconnect/SwarmChallenge$SentChallengeCB;)V

    iput-object v1, v0, Lcom/swarmconnect/an;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/an;->run()V

    return-void
.end method


# virtual methods
.method public acceptChallenge(Lcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/swarmconnect/SwarmChallenge;->a(ZLcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V

    return-void
.end method

.method public declineChallenge(Lcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/swarmconnect/SwarmChallenge;->a(ZLcom/swarmconnect/SwarmChallenge$ModChallengeCB;)V

    return-void
.end method
