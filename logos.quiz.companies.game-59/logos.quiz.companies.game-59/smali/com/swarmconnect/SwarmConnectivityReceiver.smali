.class public Lcom/swarmconnect/SwarmConnectivityReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmConnectivityReceiver$a;
    }
.end annotation


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/SwarmConnectivityReceiver;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    sget-boolean v0, Lcom/swarmconnect/SwarmConnectivityReceiver;->a:Z

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/swarmconnect/SwarmConnectivityReceiver$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/SwarmConnectivityReceiver$a;-><init>(Lcom/swarmconnect/SwarmConnectivityReceiver;Lcom/swarmconnect/SwarmConnectivityReceiver$a;)V

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmConnectivityReceiver$a;->isInitialSticky()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "noConnectivity"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    sput-boolean v2, Lcom/swarmconnect/SwarmConnectivityReceiver;->a:Z

    invoke-static {}, Lcom/swarmconnect/Swarm;->a()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/SwarmConnectivityReceiver;->a:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/SwarmConnectivityReceiver$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmConnectivityReceiver$1;-><init>(Lcom/swarmconnect/SwarmConnectivityReceiver;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
