.class Lcom/swarmconnect/SwarmDashboardScreen$4;
.super Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmDashboardScreen;->reload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmDashboardScreen;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmDashboardScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmDashboardScreen$4;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotStore(Lcom/swarmconnect/SwarmStore;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$4;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v0}, Lcom/swarmconnect/SwarmDashboardScreen;->f(Lcom/swarmconnect/SwarmDashboardScreen;)V

    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v2, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$4;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->c(Lcom/swarmconnect/SwarmDashboardScreen;Z)V

    goto :goto_0
.end method
