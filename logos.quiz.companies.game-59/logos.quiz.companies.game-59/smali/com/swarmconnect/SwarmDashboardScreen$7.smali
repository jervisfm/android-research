.class Lcom/swarmconnect/SwarmDashboardScreen$7;
.super Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmDashboardScreen;->reload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmDashboardScreen;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmDashboardScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmDashboardScreen$7;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmAchievement;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$7;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->c(Lcom/swarmconnect/SwarmDashboardScreen;I)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$7;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v0}, Lcom/swarmconnect/SwarmDashboardScreen;->e(Lcom/swarmconnect/SwarmDashboardScreen;)Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->notifyDataSetInvalidated()V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmAchievement;

    iget-boolean v2, v0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$7;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v0}, Lcom/swarmconnect/SwarmDashboardScreen;->c(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->b(Lcom/swarmconnect/SwarmDashboardScreen;I)V

    goto :goto_0

    :cond_3
    iget-boolean v0, v0, Lcom/swarmconnect/SwarmAchievement;->hidden:Z

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method
