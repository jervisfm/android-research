.class public Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/SwarmDashboardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DashboardAdapter"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmDashboardScreen;


# direct methods
.method public constructor <init>(Lcom/swarmconnect/SwarmDashboardScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    invoke-static {}, Lcom/swarmconnect/SwarmDashboardScreen;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/swarmconnect/SwarmDashboardScreen;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmDashboardScreen;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v2, "@layout/swarm_dashboard_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    if-eqz v0, :cond_0

    rem-int/lit8 v1, p1, 0x2

    if-nez v1, :cond_1

    const v1, 0xffffff

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v3, "@id/name"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v3, "@id/icon"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v3, v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;->iconResId:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v3, "@id/extra"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget v1, v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;->screenType:I

    const/4 v3, 0x5

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v1}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v1

    if-lez v1, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v1, "@id/extra"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v3}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " new messages"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    const v1, -0x77000001

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;->screenType:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v1}, Lcom/swarmconnect/SwarmDashboardScreen;->b(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v1

    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v1, "@id/extra"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v3}, Lcom/swarmconnect/SwarmDashboardScreen;->c(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " / "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v3}, Lcom/swarmconnect/SwarmDashboardScreen;->b(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " completed"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget v0, v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;->screenType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v0}, Lcom/swarmconnect/SwarmDashboardScreen;->d(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v1, "@id/extra"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-static {v3}, Lcom/swarmconnect/SwarmDashboardScreen;->d(Lcom/swarmconnect/SwarmDashboardScreen;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " friend requests"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->a:Lcom/swarmconnect/SwarmDashboardScreen;

    const-string v1, "@id/extra"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
