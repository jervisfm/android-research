.class Lcom/swarmconnect/SwarmDashboardScreen;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;,
        Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;
    }
.end annotation


# static fields
.field private static t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private l:Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Z

.field private s:Z

.field private u:I

.field private v:Lcom/swarmconnect/delegates/SwarmLoginListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    iput-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->l:Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    iput v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->m:I

    iput v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->n:I

    iput v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->o:I

    iput v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->p:I

    iput-boolean v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->q:Z

    iput-boolean v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->r:Z

    iput-boolean v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->s:Z

    iput v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->u:I

    new-instance v0, Lcom/swarmconnect/ai;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ai;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    iput-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->v:Lcom/swarmconnect/delegates/SwarmLoginListener;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/SwarmDashboardScreen;)I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->m:I

    return v0
.end method

.method static synthetic a(Lcom/swarmconnect/SwarmDashboardScreen;I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->m:I

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/SwarmDashboardScreen;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->q:Z

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/SwarmDashboardScreen;)I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->o:I

    return v0
.end method

.method static synthetic b(Lcom/swarmconnect/SwarmDashboardScreen;I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->n:I

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/SwarmDashboardScreen;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->r:Z

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/SwarmDashboardScreen;)I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->n:I

    return v0
.end method

.method static synthetic c(Lcom/swarmconnect/SwarmDashboardScreen;I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->o:I

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/SwarmDashboardScreen;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->s:Z

    return-void
.end method

.method static synthetic d(Lcom/swarmconnect/SwarmDashboardScreen;)I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->p:I

    return v0
.end method

.method static synthetic d(Lcom/swarmconnect/SwarmDashboardScreen;I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->p:I

    return-void
.end method

.method static synthetic e(Lcom/swarmconnect/SwarmDashboardScreen;)Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->l:Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    return-object v0
.end method

.method static synthetic e()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 6

    const/4 v5, 0x1

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->u:I

    iget v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->u:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->s:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_store40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Store"

    const/16 v4, 0xc

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v5, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    iget-boolean v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->r:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_trophy40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Achievements"

    invoke-direct {v1, v2, v3, v5}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v5, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_1
    iget-boolean v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->q:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_leaderboards40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Leaderboards"

    const/16 v4, 0x8

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v5, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->l:Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmDashboardScreen;->c()V

    :cond_3
    return-void
.end method

.method static synthetic f(Lcom/swarmconnect/SwarmDashboardScreen;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/SwarmDashboardScreen;->f()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "@layout/swarm_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmDashboardScreen;->b(I)V

    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.swarmconnect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->c:Lcom/swarmconnect/SwarmMainActivity;

    const/4 v1, 0x2

    const-string v2, "1279has9f7y1972ya"

    iget-object v3, p0, Lcom/swarmconnect/SwarmDashboardScreen;->v:Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-static {v0, v1, v2, v3}, Lcom/swarmconnect/Swarm;->init(Landroid/app/Activity;ILjava/lang/String;Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    :cond_0
    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmDashboardScreen;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/SwarmDashboardScreen;->l:Lcom/swarmconnect/SwarmDashboardScreen$DashboardAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$5;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmDashboardScreen$5;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_0
    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/swarmconnect/SwarmDashboardScreen;->finish()V

    goto :goto_0
.end method

.method protected reload()V
    .locals 5

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmActiveUser;->isOfflineGuest()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$6;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$6;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmMessageThread;->getNumNewMessages(Lcom/swarmconnect/SwarmMessageThread$GotNumMessages;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/SwarmDashboardScreen;->n:I

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$7;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$7;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmAchievement;->getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmDashboardScreen$1;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getFriendRequests(Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V

    :cond_0
    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/swarmconnect/u;->b:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_games40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Get More Games!"

    const/16 v4, 0x11

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_messages40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Messages"

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_friends40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Friends"

    const/4 v4, 0x3

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/SwarmDashboardScreen;->t:Ljava/util/ArrayList;

    new-instance v1, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;

    const-string v2, "@drawable/swarm_settings40"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/SwarmDashboardScreen;->a(Ljava/lang/String;)I

    move-result v2

    const-string v3, "Settings"

    const/16 v4, 0x10

    invoke-direct {v1, v2, v3, v4}, Lcom/swarmconnect/SwarmDashboardScreen$DashboardItem;-><init>(ILjava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmActiveUser;->isOfflineGuest()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmDashboardScreen;->b()V

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$2;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmLeaderboard;->getLeaderboardsList(Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;)V

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$3;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmAchievement;->getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V

    new-instance v0, Lcom/swarmconnect/SwarmDashboardScreen$4;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmDashboardScreen$4;-><init>(Lcom/swarmconnect/SwarmDashboardScreen;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmStore;->getStore(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V

    :cond_2
    return-void
.end method
