.class Lcom/swarmconnect/SwarmFacebook$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/facebook/android/Facebook$DialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmFacebook;->login(Landroid/app/Activity;[Ljava/lang/String;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:Landroid/app/Activity;

.field private final synthetic b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmFacebook$1;->a:Landroid/app/Activity;

    iput-object p2, p0, Lcom/swarmconnect/SwarmFacebook$1;->b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmFacebook$1;->b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    const-string v1, "Login Canceled"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginError(Ljava/lang/String;)V

    return-void
.end method

.method public onComplete(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmFacebook$1;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/swarmconnect/SwarmFacebook;->save(Landroid/content/Context;)Z

    iget-object v0, p0, Lcom/swarmconnect/SwarmFacebook$1;->b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->a()Lcom/facebook/android/Facebook;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginSuccess(Lcom/facebook/android/Facebook;)V

    return-void
.end method

.method public onError(Lcom/facebook/android/DialogError;)V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/SwarmFacebook$1;->b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FB Login Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginError(Ljava/lang/String;)V

    return-void
.end method

.method public onFacebookError(Lcom/facebook/android/FacebookError;)V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/SwarmFacebook$1;->b:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FB Login FacebookError: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginError(Ljava/lang/String;)V

    return-void
.end method
