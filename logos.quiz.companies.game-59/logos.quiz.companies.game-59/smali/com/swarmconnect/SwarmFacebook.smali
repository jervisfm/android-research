.class public Lcom/swarmconnect/SwarmFacebook;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/facebook/android/Facebook;

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/SwarmFacebook;->a:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/swarmconnect/SwarmFacebook;->c:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Lcom/facebook/android/Facebook;
    .locals 1

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    return-object v0
.end method

.method public static authorizeCallback(IILandroid/content/Intent;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v0, p0, p1, p2}, Lcom/facebook/android/Facebook;->authorizeCallback(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private static b()Z
    .locals 2

    const-string v0, "289484921072856"

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static clear(Landroid/content/Context;)V
    .locals 2

    const-string v0, "facebook-session"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static enableSingleSignOn()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/SwarmFacebook;->c:Z

    return-void
.end method

.method public static getSession()Lcom/facebook/android/Facebook;
    .locals 1

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-nez v0, :cond_0

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sput-object p1, Lcom/swarmconnect/SwarmFacebook;->a:Ljava/lang/String;

    new-instance v0, Lcom/facebook/android/Facebook;

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/android/Facebook;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-static {p0}, Lcom/swarmconnect/SwarmFacebook;->restore(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public static isValid()Z
    .locals 1

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Facebook: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    return-void
.end method

.method public static login(Landroid/app/Activity;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/swarmconnect/SwarmFacebook;->login(Landroid/app/Activity;[Ljava/lang/String;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V

    return-void
.end method

.method public static login(Landroid/app/Activity;[Ljava/lang/String;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "call to login... "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " && "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    instance-of v0, p0, Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/SwarmFacebook;->l(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p0, Lcom/swarmconnect/SwarmMainActivity;

    if-nez v0, :cond_1

    const-string v0, "Invalid Application Context"

    invoke-virtual {p2, v0}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginError(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-eqz v0, :cond_2

    if-nez p2, :cond_3

    :cond_2
    const-string v0, "Missing parameters"

    invoke-virtual {p2, v0}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginError(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {p2, v0}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;->loginSuccess(Lcom/facebook/android/Facebook;)V

    goto :goto_1

    :cond_4
    if-nez p1, :cond_5

    new-array p1, v2, [Ljava/lang/String;

    const-string v0, "email"

    aput-object v0, p1, v1

    :cond_5
    array-length v3, p1

    move v0, v1

    :goto_2
    if-lt v0, v3, :cond_8

    move v2, v1

    :cond_6
    if-nez v2, :cond_7

    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    array-length v2, p1

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, p1

    const-string v2, "email"

    aput-object v2, v0, v1

    move-object p1, v0

    :cond_7
    new-instance v0, Lcom/swarmconnect/SwarmFacebook$1;

    invoke-direct {v0, p0, p2}, Lcom/swarmconnect/SwarmFacebook$1;-><init>(Landroid/app/Activity;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V

    sget-boolean v1, Lcom/swarmconnect/SwarmFacebook;->c:Z

    if-eqz v1, :cond_9

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v1, p0, p1, v0}, Lcom/facebook/android/Facebook;->authorize(Landroid/app/Activity;[Ljava/lang/String;Lcom/facebook/android/Facebook$DialogListener;)V

    goto :goto_1

    :cond_8
    aget-object v4, p1, v0

    const-string v5, "email"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    const/4 v2, -0x1

    invoke-virtual {v1, p0, p1, v2, v0}, Lcom/facebook/android/Facebook;->authorize(Landroid/app/Activity;[Ljava/lang/String;ILcom/facebook/android/Facebook$DialogListener;)V

    goto :goto_1
.end method

.method public static restore(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "facebook-session"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    const-string v2, "access_token"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/android/Facebook;->setAccessToken(Ljava/lang/String;)V

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    const-string v2, "expires_in"

    const-wide/16 v3, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/android/Facebook;->setAccessExpires(J)V

    sget-object v0, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    goto :goto_0
.end method

.method public static save(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v1, "facebook-session"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "access_token"

    sget-object v2, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "expires_in"

    sget-object v2, Lcom/swarmconnect/SwarmFacebook;->b:Lcom/facebook/android/Facebook;

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->getAccessExpires()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0
.end method
