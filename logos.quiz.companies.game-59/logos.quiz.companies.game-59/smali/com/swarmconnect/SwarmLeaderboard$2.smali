.class Lcom/swarmconnect/SwarmLeaderboard$2;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmLeaderboard;

.field private final synthetic b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmLeaderboard;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->a:Lcom/swarmconnect/SwarmLeaderboard;

    iput-object p2, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    check-cast p1, Lcom/swarmconnect/bp;

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;

    iget v1, p1, Lcom/swarmconnect/bp;->pageNum:I

    iget-object v2, p1, Lcom/swarmconnect/bp;->scores:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;->gotScores(ILjava/util/List;)V

    :cond_0
    return-void
.end method

.method public requestFailed()V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$2;->b:Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;->gotScores(ILjava/util/List;)V

    :cond_0
    return-void
.end method
