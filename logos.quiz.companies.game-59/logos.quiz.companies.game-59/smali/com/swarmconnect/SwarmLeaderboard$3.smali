.class Lcom/swarmconnect/SwarmLeaderboard$3;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmLeaderboard;->submitScore(FLjava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmLeaderboard;

.field private final synthetic b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

.field private final synthetic c:F


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmLeaderboard;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;F)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->a:Lcom/swarmconnect/SwarmLeaderboard;

    iput-object p2, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

    iput p3, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->c:F

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    check-cast p1, Lcom/swarmconnect/cj;

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

    iget v1, p1, Lcom/swarmconnect/cj;->rank:I

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;->scoreSubmitted(I)V

    :cond_0
    new-instance v0, Lcom/swarmconnect/SwarmLeaderboardScore;

    invoke-direct {v0}, Lcom/swarmconnect/SwarmLeaderboardScore;-><init>()V

    sget-object v1, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iput-object v1, v0, Lcom/swarmconnect/SwarmLeaderboardScore;->user:Lcom/swarmconnect/SwarmUser;

    iget v1, p1, Lcom/swarmconnect/cj;->rank:I

    iput v1, v0, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    iget v1, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->c:F

    iput v1, v0, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    new-instance v1, Lcom/swarmconnect/NotificationLeaderboard;

    invoke-direct {v1, v0}, Lcom/swarmconnect/NotificationLeaderboard;-><init>(Lcom/swarmconnect/SwarmLeaderboardScore;)V

    invoke-static {v1}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmNotification;)V

    return-void
.end method

.method public requestFailed()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$3;->b:Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;->scoreSubmitted(I)V

    :cond_0
    return-void
.end method
