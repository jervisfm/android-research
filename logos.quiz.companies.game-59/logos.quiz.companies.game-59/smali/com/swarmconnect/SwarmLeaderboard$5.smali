.class Lcom/swarmconnect/SwarmLeaderboard$5;
.super Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmLeaderboard;->getLeaderboardById(ILcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:I

.field private final synthetic b:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;


# direct methods
.method constructor <init>(ILcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmLeaderboard$5;->a:I

    iput-object p2, p0, Lcom/swarmconnect/SwarmLeaderboard$5;->b:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotList(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboard;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmLeaderboard$5;->b:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;->gotLeaderboard(Lcom/swarmconnect/SwarmLeaderboard;)V

    :goto_0
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmLeaderboard;

    iget v2, v0, Lcom/swarmconnect/SwarmLeaderboard;->id:I

    iget v3, p0, Lcom/swarmconnect/SwarmLeaderboard$5;->a:I

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/SwarmLeaderboard$5;->b:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;->gotLeaderboard(Lcom/swarmconnect/SwarmLeaderboard;)V

    goto :goto_0
.end method
