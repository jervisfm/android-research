.class public Lcom/swarmconnect/SwarmLeaderboard;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;,
        Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;,
        Lcom/swarmconnect/SwarmLeaderboard$GotScoreCB;,
        Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;,
        Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;,
        Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;
    }
.end annotation


# static fields
.field public static final LEADERBOARD_DATE_ALL_TIME:Ljava/lang/String; = "all"

.field public static final LEADERBOARD_DATE_MONTH:Ljava/lang/String; = "month"

.field public static final LEADERBOARD_DATE_TODAY:Ljava/lang/String; = "today"

.field public static final LEADERBOARD_DATE_WEEK:Ljava/lang/String; = "week"

.field public static final LEADERBOARD_SCOPE_ALL:Ljava/lang/String; = "all"

.field public static final LEADERBOARD_SCOPE_FRIENDS:Ljava/lang/String; = "friends"

.field public static final LEADERBOARD_SCOPE_SELF:Ljava/lang/String; = "self"


# instance fields
.field public format:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

.field public id:I

.field public max_version:I

.field public min_version:I

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLeaderboardById(ILcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/swarmconnect/SwarmLeaderboard$5;

    invoke-direct {v0, p0, p1}, Lcom/swarmconnect/SwarmLeaderboard$5;-><init>(ILcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmLeaderboard;->getLeaderboardsList(Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;)V

    goto :goto_0
.end method

.method public static getLeaderboardsList(Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/b;

    invoke-direct {v0}, Lcom/swarmconnect/b;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmLeaderboard$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmLeaderboard$4;-><init>(Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardsListCB;)V

    iput-object v1, v0, Lcom/swarmconnect/b;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/b;->run()V

    return-void
.end method


# virtual methods
.method public getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/bp;

    invoke-direct {v0}, Lcom/swarmconnect/bp;-><init>()V

    iput p1, v0, Lcom/swarmconnect/bp;->pageNum:I

    iget v1, p0, Lcom/swarmconnect/SwarmLeaderboard;->id:I

    iput v1, v0, Lcom/swarmconnect/bp;->leaderboardId:I

    iput-object p2, v0, Lcom/swarmconnect/bp;->time:Ljava/lang/String;

    iput-object p3, v0, Lcom/swarmconnect/bp;->scope:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmLeaderboard$2;

    invoke-direct {v1, p0, p4}, Lcom/swarmconnect/SwarmLeaderboard$2;-><init>(Lcom/swarmconnect/SwarmLeaderboard;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    iput-object v1, v0, Lcom/swarmconnect/bp;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bp;->run()V

    return-void
.end method

.method public getPageOfScoresForCurrentUser(Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V
    .locals 2

    const/4 v0, -0x1

    const-string v1, "all"

    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    return-void
.end method

.method public getScoreForUser(ILcom/swarmconnect/SwarmLeaderboard$GotScoreCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/cq;

    invoke-direct {v0}, Lcom/swarmconnect/cq;-><init>()V

    iput p1, v0, Lcom/swarmconnect/cq;->scoreUserId:I

    iget v1, p0, Lcom/swarmconnect/SwarmLeaderboard;->id:I

    iput v1, v0, Lcom/swarmconnect/cq;->leaderboardId:I

    new-instance v1, Lcom/swarmconnect/SwarmLeaderboard$1;

    invoke-direct {v1, p0, p2}, Lcom/swarmconnect/SwarmLeaderboard$1;-><init>(Lcom/swarmconnect/SwarmLeaderboard;Lcom/swarmconnect/SwarmLeaderboard$GotScoreCB;)V

    iput-object v1, v0, Lcom/swarmconnect/cq;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/cq;->run()V

    return-void
.end method

.method public getScoreForUser(Lcom/swarmconnect/SwarmUser;Lcom/swarmconnect/SwarmLeaderboard$GotScoreCB;)V
    .locals 1

    if-eqz p1, :cond_1

    iget v0, p1, Lcom/swarmconnect/SwarmUser;->userId:I

    invoke-virtual {p0, v0, p2}, Lcom/swarmconnect/SwarmLeaderboard;->getScoreForUser(ILcom/swarmconnect/SwarmLeaderboard$GotScoreCB;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/swarmconnect/SwarmLeaderboard$GotScoreCB;->gotScore(Lcom/swarmconnect/SwarmLeaderboardScore;)V

    goto :goto_0
.end method

.method public getTopScores(Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "all"

    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    return-void
.end method

.method public showLeaderboard()V
    .locals 3

    sget-object v0, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    const-class v2, Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "screenType"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "leaderboard"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    sget-object v1, Lcom/swarmconnect/Swarm;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public submitScore(F)V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/swarmconnect/SwarmLeaderboard;->submitScore(FLjava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;)V

    return-void
.end method

.method public submitScore(FLjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/swarmconnect/SwarmLeaderboard;->submitScore(FLjava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;)V

    return-void
.end method

.method public submitScore(FLjava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;)V
    .locals 2

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/swarmconnect/cj;

    invoke-direct {v0}, Lcom/swarmconnect/cj;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmLeaderboard;->id:I

    iput v1, v0, Lcom/swarmconnect/cj;->leaderboardId:I

    iput p1, v0, Lcom/swarmconnect/cj;->score:F

    iput-object p2, v0, Lcom/swarmconnect/cj;->data:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmLeaderboard$3;

    invoke-direct {v1, p0, p3, p1}, Lcom/swarmconnect/SwarmLeaderboard$3;-><init>(Lcom/swarmconnect/SwarmLeaderboard;Lcom/swarmconnect/SwarmLeaderboard$SubmitScoreCB;F)V

    iput-object v1, v0, Lcom/swarmconnect/cj;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/cj;->run()V

    goto :goto_0
.end method
