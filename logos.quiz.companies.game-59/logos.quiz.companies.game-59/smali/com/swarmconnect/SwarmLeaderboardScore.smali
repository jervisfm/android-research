.class public Lcom/swarmconnect/SwarmLeaderboardScore;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmLeaderboardScore$GotDataCB;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/swarmconnect/SwarmLeaderboardScore;",
        ">;"
    }
.end annotation


# instance fields
.field public id:I

.field public rank:I

.field public score:F

.field public timestamp:I

.field public user:Lcom/swarmconnect/SwarmUser;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/swarmconnect/SwarmLeaderboardScore;)I
    .locals 2

    iget v0, p0, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    iget v1, p1, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    iget v1, p1, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/swarmconnect/SwarmLeaderboardScore;

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmLeaderboardScore;->compareTo(Lcom/swarmconnect/SwarmLeaderboardScore;)I

    move-result v0

    return v0
.end method

.method public getData(Lcom/swarmconnect/SwarmLeaderboardScore$GotDataCB;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/swarmconnect/bs;

    invoke-direct {v0}, Lcom/swarmconnect/bs;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmLeaderboardScore;->id:I

    iput v1, v0, Lcom/swarmconnect/bs;->scoreId:I

    iget-object v1, p0, Lcom/swarmconnect/SwarmLeaderboardScore;->user:Lcom/swarmconnect/SwarmUser;

    iget v1, v1, Lcom/swarmconnect/SwarmUser;->userId:I

    iput v1, v0, Lcom/swarmconnect/bs;->scoreUserId:I

    new-instance v1, Lcom/swarmconnect/SwarmLeaderboardScore$1;

    invoke-direct {v1, p0, p1}, Lcom/swarmconnect/SwarmLeaderboardScore$1;-><init>(Lcom/swarmconnect/SwarmLeaderboardScore;Lcom/swarmconnect/SwarmLeaderboardScore$GotDataCB;)V

    iput-object v1, v0, Lcom/swarmconnect/bs;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bs;->run()V

    goto :goto_0
.end method
