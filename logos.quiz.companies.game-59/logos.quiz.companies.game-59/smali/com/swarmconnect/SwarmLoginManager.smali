.class public Lcom/swarmconnect/SwarmLoginManager;
.super Ljava/lang/Object;


# static fields
.field protected static a:I

.field private static b:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/swarmconnect/delegates/SwarmLoginListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    sput-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a()V
    .locals 2

    sget v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    if-lez v0, :cond_0

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    const-string v0, "login canceled"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-interface {v0}, Lcom/swarmconnect/delegates/SwarmLoginListener;->loginCanceled()V

    goto :goto_0
.end method

.method protected static a(Lcom/swarmconnect/SwarmActiveUser;)V
    .locals 2

    const/4 v1, 0x2

    sget v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    if-ge v0, v1, :cond_0

    const-string v0, "Logged in"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    sput v1, Lcom/swarmconnect/SwarmLoginManager;->a:I

    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-interface {v0, p0}, Lcom/swarmconnect/delegates/SwarmLoginListener;->userLoggedIn(Lcom/swarmconnect/SwarmActiveUser;)V

    goto :goto_0
.end method

.method public static addLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V
    .locals 2

    if-eqz p0, :cond_0

    sget-object v1, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected static b()V
    .locals 2

    sget v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    if-nez v0, :cond_0

    const-string v0, "login started"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    sput v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-interface {v0}, Lcom/swarmconnect/delegates/SwarmLoginListener;->loginStarted()V

    goto :goto_0
.end method

.method protected static c()V
    .locals 2

    sget v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    if-lez v0, :cond_0

    const-string v0, "Logged out"

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/SwarmLoginManager;->a:I

    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-interface {v0}, Lcom/swarmconnect/delegates/SwarmLoginListener;->userLoggedOut()V

    goto :goto_0
.end method

.method public static removeLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V
    .locals 2

    if-eqz p0, :cond_0

    sget-object v1, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/swarmconnect/SwarmLoginManager;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
