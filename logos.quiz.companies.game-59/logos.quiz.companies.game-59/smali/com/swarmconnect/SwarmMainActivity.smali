.class public Lcom/swarmconnect/SwarmMainActivity;
.super Lcom/swarmconnect/SwarmActivity;


# static fields
.field public static final SCREEN_ACHIEVEMENTS:I = 0x1

.field public static final SCREEN_CREATE_ACCOUNT:I = 0x2

.field public static final SCREEN_DASHBOARD:I = 0x0

.field public static final SCREEN_EXTERNAL_USERNAME:I = 0x12

.field public static final SCREEN_FORTUMO:I = 0x16

.field public static final SCREEN_FRIENDS:I = 0x3

.field public static final SCREEN_GAMES:I = 0x11

.field public static final SCREEN_GET_COINS:I = 0x4

.field public static final SCREEN_INBOX:I = 0x5

.field public static final SCREEN_INVENTORY:I = 0x6

.field public static final SCREEN_LEADERBOARD:I = 0x7

.field public static final SCREEN_LEADERBOARDS:I = 0x8

.field public static final SCREEN_LOGIN:I = 0x9

.field public static final SCREEN_LOST_PASSWORD:I = 0xa

.field public static final SCREEN_MESSAGE_THREAD:I = 0xe

.field public static final SCREEN_PAYPAL:I = 0x14

.field public static final SCREEN_SELECT_USERNAME:I = 0xb

.field public static final SCREEN_SETTINGS:I = 0x10

.field public static final SCREEN_STORE:I = 0xc

.field public static final SCREEN_STORE_CATEGORY:I = 0xd

.field public static final SCREEN_TAPJOY:I = 0x13

.field public static final SCREEN_TRAY_NOTIFICATION:I = -0x3e8

.field public static final SCREEN_UPGRADE_GUEST:I = 0xf

.field public static final SCREEN_ZONG:I = 0x15

.field public static screenClasses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/u;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field private b:Lcom/swarmconnect/u;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/SwarmDashboardScreen;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ao;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/bi;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/q;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ck;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/g;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ah;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/au;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/k;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/bz;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/d;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/cb;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/bf;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/m;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/t;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ak;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/i;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/s;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ar;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x13

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/bm;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/cd;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/bw;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Lcom/swarmconnect/ca;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/SwarmMainActivity;)Lcom/swarmconnect/u;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/swarmconnect/u;->a(IILandroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/u;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "screenType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/swarmconnect/SwarmMainActivity;->a:I

    :cond_0
    iget v0, p0, Lcom/swarmconnect/SwarmMainActivity;->a:I

    const/16 v1, -0x3e8

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmMainActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/swarmconnect/SwarmMainActivity;->screenClasses:Ljava/util/HashMap;

    iget v1, p0, Lcom/swarmconnect/SwarmMainActivity;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_2

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/u;

    iput-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    iput-object p0, v0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/swarmconnect/SwarmMainActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onDestroy()V

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->onDestroy()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0, p1, p2}, Lcom/swarmconnect/u;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/swarmconnect/SwarmActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onPause()V

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->onPause()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onResume()V

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->onResume()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->onStop()V

    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/SwarmMainActivity;->b:Lcom/swarmconnect/u;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swarmconnect/SwarmMainActivity$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/SwarmMainActivity$1;-><init>(Lcom/swarmconnect/SwarmMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public show(I)V
    .locals 0

    invoke-static {p1}, Lcom/swarmconnect/Swarm;->show(I)V

    return-void
.end method
