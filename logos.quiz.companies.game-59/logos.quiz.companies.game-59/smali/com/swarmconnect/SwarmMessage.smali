.class public Lcom/swarmconnect/SwarmMessage;
.super Ljava/lang/Object;


# instance fields
.field public from:Lcom/swarmconnect/SwarmUser;

.field public id:I

.field public message:Ljava/lang/String;

.field public timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendMessage(ILjava/lang/String;Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/ay;

    invoke-direct {v0}, Lcom/swarmconnect/ay;-><init>()V

    iput p0, v0, Lcom/swarmconnect/ay;->toUserId:I

    iput-object p1, v0, Lcom/swarmconnect/ay;->message:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmMessage$1;

    invoke-direct {v1, p2}, Lcom/swarmconnect/SwarmMessage$1;-><init>(Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V

    iput-object v1, v0, Lcom/swarmconnect/ay;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/ay;->run()V

    return-void
.end method


# virtual methods
.method public getSentDate()Ljava/util/Date;
    .locals 5

    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lcom/swarmconnect/SwarmMessage;->timestamp:J

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method
