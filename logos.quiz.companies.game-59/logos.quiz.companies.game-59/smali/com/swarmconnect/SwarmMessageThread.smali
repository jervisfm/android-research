.class public Lcom/swarmconnect/SwarmMessageThread;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;,
        Lcom/swarmconnect/SwarmMessageThread$GotNumMessages;,
        Lcom/swarmconnect/SwarmMessageThread$GotThreadsCB;,
        Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x3a5e22f1eadaf767L


# instance fields
.field public id:I

.field public lastMessage:Ljava/lang/String;

.field public otherUser:Lcom/swarmconnect/SwarmUser;

.field public viewed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAllThreads(Lcom/swarmconnect/SwarmMessageThread$GotThreadsCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/bq;

    invoke-direct {v0}, Lcom/swarmconnect/bq;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmMessageThread$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmMessageThread$3;-><init>(Lcom/swarmconnect/SwarmMessageThread$GotThreadsCB;)V

    iput-object v1, v0, Lcom/swarmconnect/bq;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bq;->run()V

    return-void
.end method

.method public static getMessages(IILcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/aj;

    invoke-direct {v0}, Lcom/swarmconnect/aj;-><init>()V

    iput p0, v0, Lcom/swarmconnect/aj;->threadId:I

    iput p1, v0, Lcom/swarmconnect/aj;->otherId:I

    new-instance v1, Lcom/swarmconnect/SwarmMessageThread$2;

    invoke-direct {v1, p2}, Lcom/swarmconnect/SwarmMessageThread$2;-><init>(Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    iput-object v1, v0, Lcom/swarmconnect/aj;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/aj;->run()V

    return-void
.end method

.method public static getMessages(ILcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/swarmconnect/SwarmMessageThread;->getMessages(IILcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    return-void
.end method

.method public static getNumNewMessages(Lcom/swarmconnect/SwarmMessageThread$GotNumMessages;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/bh;

    invoke-direct {v0}, Lcom/swarmconnect/bh;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmMessageThread$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmMessageThread$1;-><init>(Lcom/swarmconnect/SwarmMessageThread$GotNumMessages;)V

    iput-object v1, v0, Lcom/swarmconnect/bh;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bh;->run()V

    return-void
.end method


# virtual methods
.method public getMessages(Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V
    .locals 2

    iget v0, p0, Lcom/swarmconnect/SwarmMessageThread;->id:I

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/swarmconnect/SwarmMessageThread;->getMessages(IILcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    return-void
.end method

.method public markRead()V
    .locals 2

    iget-boolean v0, p0, Lcom/swarmconnect/SwarmMessageThread;->viewed:Z

    if-nez v0, :cond_1

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v1, v0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/swarmconnect/SwarmActiveUser;->numNewMessages:I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/swarmconnect/SwarmMessageThread;->viewed:Z

    new-instance v0, Lcom/swarmconnect/n;

    invoke-direct {v0}, Lcom/swarmconnect/n;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmMessageThread;->id:I

    iput v1, v0, Lcom/swarmconnect/n;->threadId:I

    invoke-virtual {v0}, Lcom/swarmconnect/n;->run()V

    :cond_1
    return-void
.end method

.method public reply(Ljava/lang/String;Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V
    .locals 1

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmMessageThread;->markRead()V

    iget-object v0, p0, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    iget v0, v0, Lcom/swarmconnect/SwarmUser;->userId:I

    invoke-static {v0, p1, p2}, Lcom/swarmconnect/SwarmMessage;->sendMessage(ILjava/lang/String;Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V

    return-void
.end method
