.class public final enum Lcom/swarmconnect/SwarmNotification$NotificationType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/SwarmNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/swarmconnect/SwarmNotification$NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACHIEVEMENT:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum CHALLENGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum LEADERBOARD:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field public static final enum PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

.field private static final synthetic a:[Lcom/swarmconnect/SwarmNotification$NotificationType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "FRIEND"

    invoke-direct {v0, v1, v3}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v4}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "ONLINE"

    invoke-direct {v0, v1, v5}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "ACHIEVEMENT"

    invoke-direct {v0, v1, v6}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->ACHIEVEMENT:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "CHALLENGE"

    invoke-direct {v0, v1, v7}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->CHALLENGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "PURCHASE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "GOT_COINS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    new-instance v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-string v1, "LEADERBOARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/SwarmNotification$NotificationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->LEADERBOARD:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->ACHIEVEMENT:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->CHALLENGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/swarmconnect/SwarmNotification$NotificationType;->LEADERBOARD:Lcom/swarmconnect/SwarmNotification$NotificationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->a:[Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swarmconnect/SwarmNotification$NotificationType;
    .locals 1

    const-class v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmNotification$NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/swarmconnect/SwarmNotification$NotificationType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/swarmconnect/SwarmNotification$NotificationType;->a:[Lcom/swarmconnect/SwarmNotification$NotificationType;

    array-length v1, v0

    new-array v2, v1, [Lcom/swarmconnect/SwarmNotification$NotificationType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
