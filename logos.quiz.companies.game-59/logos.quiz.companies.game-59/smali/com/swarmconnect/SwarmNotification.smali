.class public Lcom/swarmconnect/SwarmNotification;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmNotification$NotificationType;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/swarmconnect/SwarmNotification$NotificationType;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/SwarmNotification;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public data:I

.field public id:I

.field public type:Lcom/swarmconnect/SwarmNotification$NotificationType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationFriend;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationMessage;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationOnline;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->ACHIEVEMENT:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationAchievement;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->CHALLENGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationChallenge;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationPurchase;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationGotCoins;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->LEADERBOARD:Lcom/swarmconnect/SwarmNotification$NotificationType;

    const-class v2, Lcom/swarmconnect/NotificationLeaderboard;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static b(ILcom/swarmconnect/SwarmNotification$NotificationType;I)Lcom/swarmconnect/SwarmNotification;
    .locals 3

    const/4 v1, 0x0

    sget-object v0, Lcom/swarmconnect/SwarmNotification;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmNotification;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v0, p0, p1, p2}, Lcom/swarmconnect/SwarmNotification;->a(ILcom/swarmconnect/SwarmNotification$NotificationType;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_1

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/view/View;
    .locals 8

    :try_start_0
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    const/4 v1, 0x2

    const v2, -0xd58349

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    const/high16 v1, 0x4080

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    const/16 v1, 0x8

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x2

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x3

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x4

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x5

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x6

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const/4 v2, 0x7

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v3, v1, v4, v5}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const/high16 v3, -0x5600

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    const/high16 v3, 0x4060

    const/high16 v4, 0x4040

    const/high16 v5, 0x4040

    const/high16 v6, -0x5600

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v3, -0x2

    invoke-direct {v2, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v0, 0x4100

    invoke-static {v0}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v0

    float-to-int v0, v0

    const/high16 v3, 0x4100

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x4100

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v5, 0x4100

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/high16 v1, 0x4120

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    float-to-int v1, v1

    const/high16 v2, 0x4120

    invoke-static {v2}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    float-to-int v2, v2

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x4120

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    const/high16 v2, 0x40a0

    invoke-static {v2}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    float-to-int v2, v2

    const/high16 v3, 0x40a0

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x40a0

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v5, 0x40a0

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/high16 v5, 0x4160

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmNotification;->getLogoIconId(Landroid/content/Context;)I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/high16 v2, 0x4120

    invoke-static {v2}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    float-to-int v2, v2

    const/high16 v3, 0x3f80

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x4120

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v5, 0x4120

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x116c533

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setId(I)V

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmNotification;->getIconId(Landroid/content/Context;)I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v3, 0x4120

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/high16 v5, 0x40a0

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v4, 0x187e67d

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setId(I)V

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmNotification;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x2

    const/high16 v7, 0x3f80

    invoke-direct {v4, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v4, 0x1a5811

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setId(I)V

    const v4, -0x222223

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmNotification;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 0x4
        0x33t 0x33t 0x33t 0xfft
        0x55t 0x55t 0x55t 0xfft
    .end array-data
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(ILcom/swarmconnect/SwarmNotification$NotificationType;I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmNotification;->id:I

    iput-object p2, p0, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iput p3, p0, Lcom/swarmconnect/SwarmNotification;->data:I

    invoke-virtual {p0}, Lcom/swarmconnect/SwarmNotification;->a()V

    return-void
.end method

.method public getIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_icon"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLogoIconId(Landroid/content/Context;)I
    .locals 4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "@drawable/swarm_logo"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmNotification;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
