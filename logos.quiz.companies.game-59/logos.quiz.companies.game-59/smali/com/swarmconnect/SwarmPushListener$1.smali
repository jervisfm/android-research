.class Lcom/swarmconnect/SwarmPushListener$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmPushListener;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 4

    check-cast p1, Lcom/swarmconnect/ci;

    iget-object v0, p1, Lcom/swarmconnect/ci;->notifications:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/ci;->notifications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/ci;->notifications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmNotification;

    iget v2, v0, Lcom/swarmconnect/SwarmNotification;->id:I

    iget-object v3, v0, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    iget v0, v0, Lcom/swarmconnect/SwarmNotification;->data:I

    invoke-static {v2, v3, v0}, Lcom/swarmconnect/SwarmNotification;->b(ILcom/swarmconnect/SwarmNotification$NotificationType;I)Lcom/swarmconnect/SwarmNotification;

    goto :goto_0
.end method
