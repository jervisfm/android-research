.class Lcom/swarmconnect/SwarmStore$2;
.super Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmStore;->getListingById(ILcom/swarmconnect/SwarmStore$GotStoreListingCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic a:I

.field private final synthetic b:Lcom/swarmconnect/SwarmStore$GotStoreListingCB;


# direct methods
.method constructor <init>(ILcom/swarmconnect/SwarmStore$GotStoreListingCB;)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/SwarmStore$2;->a:I

    iput-object p2, p0, Lcom/swarmconnect/SwarmStore$2;->b:Lcom/swarmconnect/SwarmStore$GotStoreListingCB;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotStore(Lcom/swarmconnect/SwarmStore;)V
    .locals 5

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/SwarmStore$2;->b:Lcom/swarmconnect/SwarmStore$GotStoreListingCB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmStore$GotStoreListingCB;->gotStoreListing(Lcom/swarmconnect/SwarmStoreListing;)V

    :goto_0
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreCategory;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreListing;

    if-eqz v0, :cond_3

    iget v3, v0, Lcom/swarmconnect/SwarmStoreListing;->id:I

    iget v4, p0, Lcom/swarmconnect/SwarmStore$2;->a:I

    if-ne v3, v4, :cond_3

    iget-object v1, p0, Lcom/swarmconnect/SwarmStore$2;->b:Lcom/swarmconnect/SwarmStore$GotStoreListingCB;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmStore$GotStoreListingCB;->gotStoreListing(Lcom/swarmconnect/SwarmStoreListing;)V

    goto :goto_0
.end method
