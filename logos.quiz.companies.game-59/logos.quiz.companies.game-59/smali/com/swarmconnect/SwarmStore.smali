.class public Lcom/swarmconnect/SwarmStore;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmStore$GotStoreListingCB;,
        Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;
    }
.end annotation


# instance fields
.field public categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmStoreCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    return-void
.end method

.method public static getListingById(ILcom/swarmconnect/SwarmStore$GotStoreListingCB;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/swarmconnect/SwarmStore$2;

    invoke-direct {v0, p0, p1}, Lcom/swarmconnect/SwarmStore$2;-><init>(ILcom/swarmconnect/SwarmStore$GotStoreListingCB;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmStore;->getStore(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V

    goto :goto_0
.end method

.method public static getStore(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/cf;

    invoke-direct {v0}, Lcom/swarmconnect/cf;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmStore$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/SwarmStore$1;-><init>(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V

    iput-object v1, v0, Lcom/swarmconnect/cf;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/cf;->run()V

    return-void
.end method
