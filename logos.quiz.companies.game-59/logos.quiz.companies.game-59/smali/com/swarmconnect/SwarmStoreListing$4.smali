.class Lcom/swarmconnect/SwarmStoreListing$4;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmStoreListing;->a(Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmStoreListing;

.field private final synthetic b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmStoreListing;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmStoreListing$4;->a:Lcom/swarmconnect/SwarmStoreListing;

    iput-object p2, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    check-cast p1, Lcom/swarmconnect/bd;

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/swarmconnect/bd;->purchaseStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/swarmconnect/NotificationPurchase;

    iget-object v1, p0, Lcom/swarmconnect/SwarmStoreListing$4;->a:Lcom/swarmconnect/SwarmStoreListing;

    invoke-direct {v0, v1}, Lcom/swarmconnect/NotificationPurchase;-><init>(Lcom/swarmconnect/SwarmStoreListing;)V

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmNotification;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;->purchaseSuccess()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    iget v1, p1, Lcom/swarmconnect/bd;->purchaseStatus:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    iget v1, p1, Lcom/swarmconnect/bd;->purchaseStatus:I

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;->purchaseFailed(I)V

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 3

    const/16 v2, -0x3ed

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$4;->b:Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;->purchaseFailed(I)V

    :cond_0
    return-void
.end method
