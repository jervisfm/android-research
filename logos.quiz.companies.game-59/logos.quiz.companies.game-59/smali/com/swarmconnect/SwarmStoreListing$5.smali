.class Lcom/swarmconnect/SwarmStoreListing$5;
.super Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmStoreListing;->purchase(Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmStoreListing;

.field private final synthetic b:Landroid/widget/TextView;

.field private final synthetic c:Landroid/widget/TextView;

.field private final synthetic d:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmStoreListing;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmStoreListing$5;->a:Lcom/swarmconnect/SwarmStoreListing;

    iput-object p2, p0, Lcom/swarmconnect/SwarmStoreListing$5;->b:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/swarmconnect/SwarmStoreListing$5;->c:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/swarmconnect/SwarmStoreListing$5;->d:Landroid/app/Dialog;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotCoins(I)V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$5;->a:Lcom/swarmconnect/SwarmStoreListing;

    iget v0, v0, Lcom/swarmconnect/SwarmStoreListing;->price:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$5;->b:Landroid/widget/TextView;

    const-string v1, "You need more coins to purchase this. Tap below to get more coins!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$5;->c:Landroid/widget/TextView;

    const-string v1, "Get Coins"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing$5;->c:Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/SwarmStoreListing$5$1;

    iget-object v2, p0, Lcom/swarmconnect/SwarmStoreListing$5;->d:Landroid/app/Dialog;

    invoke-direct {v1, p0, v2}, Lcom/swarmconnect/SwarmStoreListing$5$1;-><init>(Lcom/swarmconnect/SwarmStoreListing$5;Landroid/app/Dialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
