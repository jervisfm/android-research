.class public Lcom/swarmconnect/SwarmStoreListing;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;
    }
.end annotation


# static fields
.field public static final PURCHASE_CANCELED:I = -0x3e8

.field public static final PURCHASE_FAILED_ALREADY_PURCHASED:I = -0x3ee

.field public static final PURCHASE_FAILED_INVALID_ITEM:I = -0x3eb

.field public static final PURCHASE_FAILED_INVALID_STORE:I = -0x3ea

.field public static final PURCHASE_FAILED_NO_COINS:I = -0x3e9

.field public static final PURCHASE_FAILED_NO_INTERNET:I = -0x3ed

.field public static final PURCHASE_FAILED_OTHER:I = -0x3ec

.field public static final PURCHASE_SUCCESS:I = 0x1


# instance fields
.field public id:I

.field public imageUrl:Ljava/lang/String;

.field public item:Lcom/swarmconnect/SwarmStoreItem;

.field public orderId:I

.field public price:I

.field public quantity:I

.field public title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Canceled"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3e9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed: Not enough coins"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3ea

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed: Invalid Swarm store"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3eb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed: Invalid item"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3ec

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3ed

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed: Network unavailable"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/ax;->a:Ljava/util/HashMap;

    const/16 v1, -0x3ee

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Purchase Failed: You already own that item"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Purchasing "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swarmconnect/SwarmStoreListing;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/swarmconnect/bd;

    invoke-direct {v0}, Lcom/swarmconnect/bd;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmStoreListing;->id:I

    iput v1, v0, Lcom/swarmconnect/bd;->listingId:I

    new-instance v1, Lcom/swarmconnect/SwarmStoreListing$4;

    invoke-direct {v1, p0, p2}, Lcom/swarmconnect/SwarmStoreListing$4;-><init>(Lcom/swarmconnect/SwarmStoreListing;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V

    iput-object v1, v0, Lcom/swarmconnect/bd;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/bd;->run()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/SwarmStoreListing;Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/swarmconnect/SwarmStoreListing;->a(Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V

    return-void
.end method


# virtual methods
.method public purchase(Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V
    .locals 5

    const v4, -0xcccccd

    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    const/16 v0, -0x3ed

    invoke-virtual {p2, v0}, Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;->purchaseFailed(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    const-string v0, "@layout/swarm_purchase_popup"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(I)V

    new-instance v0, Lcom/swarmconnect/SwarmStoreListing$3;

    invoke-direct {v0, p0, p2}, Lcom/swarmconnect/SwarmStoreListing$3;-><init>(Lcom/swarmconnect/SwarmStoreListing;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;)V

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    const-string v0, "@id/purchase"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/SwarmStoreListing$2;

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/swarmconnect/SwarmStoreListing$2;-><init>(Lcom/swarmconnect/SwarmStoreListing;Landroid/content/Context;Lcom/swarmconnect/SwarmStoreListing$ItemPurchaseCB;Landroid/app/Dialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/cancel"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/SwarmStoreListing$1;

    invoke-direct {v1, p0, v2}, Lcom/swarmconnect/SwarmStoreListing$1;-><init>(Lcom/swarmconnect/SwarmStoreListing;Landroid/app/Dialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/title"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/swarmconnect/SwarmStoreListing;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "@id/desc"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Purchase for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/swarmconnect/SwarmStoreListing;->price:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " coins"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "@id/coins"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/swarmconnect/SwarmStoreListing;->price:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/SwarmStoreListing;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "@id/image"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/ui/AsyncImageView;

    iget-object v1, p0, Lcom/swarmconnect/SwarmStoreListing;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ui/AsyncImageView;->getUrl(Ljava/lang/String;)V

    :cond_2
    const-string v0, "@id/purchase"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->purchasePopupBuyButton()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/cancel"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->purchasePopupCancelButton()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/swarm_purchase_popup"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->loadingPopupBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/popup_header"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-static {v4, v4}, Lcom/swarmconnect/ui/UiConf;->customGradient(II)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/popup_top_half"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const v1, -0x777778

    const v3, -0x555556

    invoke-static {v1, v3}, Lcom/swarmconnect/ui/UiConf;->customGradient(II)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/popup_bottom_half"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v4, v4}, Lcom/swarmconnect/ui/UiConf;->customGradient(II)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/desc"

    invoke-static {v0, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "@id/purchase"

    invoke-static {v1, p1}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v3, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v4, Lcom/swarmconnect/SwarmStoreListing$5;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/swarmconnect/SwarmStoreListing$5;-><init>(Lcom/swarmconnect/SwarmStoreListing;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/app/Dialog;)V

    invoke-virtual {v3, v4}, Lcom/swarmconnect/SwarmActiveUser;->getCoins(Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0
.end method
