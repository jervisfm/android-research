.class public Lcom/swarmconnect/SwarmUser;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/SwarmUser$GotUserCB;,
        Lcom/swarmconnect/SwarmUser$GotUserStatusCB;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x44a304a6457907e2L


# instance fields
.field public points:I

.field public userId:I

.field public username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/swarmconnect/SwarmUser;->userId:I

    return-void
.end method

.method protected constructor <init>(Lcom/swarmconnect/SwarmUser;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Lcom/swarmconnect/SwarmUser;->userId:I

    iput v0, p0, Lcom/swarmconnect/SwarmUser;->userId:I

    iget-object v0, p1, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    iget v0, p1, Lcom/swarmconnect/SwarmUser;->points:I

    iput v0, p0, Lcom/swarmconnect/SwarmUser;->points:I

    return-void
.end method

.method protected static a(ILjava/lang/String;Lcom/swarmconnect/SwarmUser$GotUserCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/a;

    invoke-direct {v0}, Lcom/swarmconnect/a;-><init>()V

    iput p0, v0, Lcom/swarmconnect/a;->searchId:I

    iput-object p1, v0, Lcom/swarmconnect/a;->searchUsername:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/SwarmUser$2;

    invoke-direct {v1, p2}, Lcom/swarmconnect/SwarmUser$2;-><init>(Lcom/swarmconnect/SwarmUser$GotUserCB;)V

    iput-object v1, v0, Lcom/swarmconnect/a;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/a;->run()V

    return-void
.end method

.method public static getUser(ILcom/swarmconnect/SwarmUser$GotUserCB;)V
    .locals 1

    const-string v0, ""

    invoke-static {p0, v0, p1}, Lcom/swarmconnect/SwarmUser;->a(ILjava/lang/String;Lcom/swarmconnect/SwarmUser$GotUserCB;)V

    return-void
.end method

.method public static getUser(Ljava/lang/String;Lcom/swarmconnect/SwarmUser$GotUserCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/swarmconnect/SwarmUser;->a(ILjava/lang/String;Lcom/swarmconnect/SwarmUser$GotUserCB;)V

    return-void
.end method


# virtual methods
.method public getOnlineStatus(Lcom/swarmconnect/SwarmUser$GotUserStatusCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/z;

    invoke-direct {v0}, Lcom/swarmconnect/z;-><init>()V

    iget v1, p0, Lcom/swarmconnect/SwarmUser;->userId:I

    iput v1, v0, Lcom/swarmconnect/z;->id:I

    new-instance v1, Lcom/swarmconnect/SwarmUser$1;

    invoke-direct {v1, p0, p1}, Lcom/swarmconnect/SwarmUser$1;-><init>(Lcom/swarmconnect/SwarmUser;Lcom/swarmconnect/SwarmUser$GotUserStatusCB;)V

    iput-object v1, v0, Lcom/swarmconnect/z;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/z;->run()V

    return-void
.end method
