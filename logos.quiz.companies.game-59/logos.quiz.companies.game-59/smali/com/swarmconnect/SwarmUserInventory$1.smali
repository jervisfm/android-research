.class Lcom/swarmconnect/SwarmUserInventory$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/SwarmUserInventory;->refresh(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/SwarmUserInventory;

.field private final synthetic b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;


# direct methods
.method constructor <init>(Lcom/swarmconnect/SwarmUserInventory;Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/SwarmUserInventory$1;->a:Lcom/swarmconnect/SwarmUserInventory;

    iput-object p2, p0, Lcom/swarmconnect/SwarmUserInventory$1;->b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    check-cast p1, Lcom/swarmconnect/GetInventoryAPI;

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory$1;->b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory$1;->a:Lcom/swarmconnect/SwarmUserInventory;

    iget-object v1, p1, Lcom/swarmconnect/GetInventoryAPI;->inventory:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmUserInventory;->a(Ljava/util/List;)Lcom/swarmconnect/SwarmUserInventory;

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory$1;->b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;

    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory$1;->a:Lcom/swarmconnect/SwarmUserInventory;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;->gotInventory(Lcom/swarmconnect/SwarmUserInventory;)V

    :cond_0
    return-void
.end method

.method public requestFailed()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory$1;->b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory$1;->b:Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;->gotInventory(Lcom/swarmconnect/SwarmUserInventory;)V

    :cond_0
    return-void
.end method
