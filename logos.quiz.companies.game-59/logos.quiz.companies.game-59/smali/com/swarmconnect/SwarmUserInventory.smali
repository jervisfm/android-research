.class public Lcom/swarmconnect/SwarmUserInventory;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/swarmconnect/SwarmStoreItem;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    return-void
.end method

.method protected static a(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)Lcom/swarmconnect/SwarmUserInventory;
    .locals 1

    new-instance v0, Lcom/swarmconnect/SwarmUserInventory;

    invoke-direct {v0}, Lcom/swarmconnect/SwarmUserInventory;-><init>()V

    invoke-virtual {v0, p0}, Lcom/swarmconnect/SwarmUserInventory;->refresh(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/util/List;)Lcom/swarmconnect/SwarmUserInventory;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;",
            ">;)",
            "Lcom/swarmconnect/SwarmUserInventory;"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-object p0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;

    iget-object v2, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget v3, v3, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, v0, Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;->item:Lcom/swarmconnect/SwarmStoreItem;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget v3, v3, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v0, v0, Lcom/swarmconnect/GetInventoryAPI$SwarmInventoryItemTransport;->quantity:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected a(Lcom/swarmconnect/SwarmStoreListing;)V
    .locals 4

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmUserInventory;->getItemQuantity(Lcom/swarmconnect/SwarmStoreItem;)I

    move-result v0

    iget v1, p1, Lcom/swarmconnect/SwarmStoreListing;->quantity:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget v2, v2, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget v2, v2, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget v2, v2, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public consumeItem(I)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreItem;

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmUserInventory;->consumeItem(Lcom/swarmconnect/SwarmStoreItem;)V

    return-void
.end method

.method public consumeItem(Lcom/swarmconnect/SwarmStoreItem;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p1, Lcom/swarmconnect/SwarmStoreItem;->consumable:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/swarmconnect/SwarmUserInventory;->getItemQuantity(Lcom/swarmconnect/SwarmStoreItem;)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/swarmconnect/aw;

    invoke-direct {v1}, Lcom/swarmconnect/aw;-><init>()V

    iget v2, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    iput v2, v1, Lcom/swarmconnect/aw;->itemId:I

    invoke-virtual {v1}, Lcom/swarmconnect/aw;->run()V

    add-int/lit8 v0, v0, -0x1

    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    iget v2, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    iget v1, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    iget v1, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public containsItem(I)Z
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsItem(Lcom/swarmconnect/SwarmStoreItem;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmUserInventory;->containsItem(I)Z

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/SwarmStoreItem;
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreItem;

    return-object v0
.end method

.method public getItemList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmStoreItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/swarmconnect/SwarmUserInventory;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public getItemQuantity(I)I
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/SwarmUserInventory;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemQuantity(Lcom/swarmconnect/SwarmStoreItem;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/swarmconnect/SwarmStoreItem;->id:I

    invoke-virtual {p0, v0}, Lcom/swarmconnect/SwarmUserInventory;->getItemQuantity(I)I

    move-result v0

    goto :goto_0
.end method

.method public refresh(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V
    .locals 2

    new-instance v0, Lcom/swarmconnect/GetInventoryAPI;

    invoke-direct {v0}, Lcom/swarmconnect/GetInventoryAPI;-><init>()V

    new-instance v1, Lcom/swarmconnect/SwarmUserInventory$1;

    invoke-direct {v1, p0, p1}, Lcom/swarmconnect/SwarmUserInventory$1;-><init>(Lcom/swarmconnect/SwarmUserInventory;Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V

    iput-object v1, v0, Lcom/swarmconnect/GetInventoryAPI;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/GetInventoryAPI;->run()V

    return-void
.end method
