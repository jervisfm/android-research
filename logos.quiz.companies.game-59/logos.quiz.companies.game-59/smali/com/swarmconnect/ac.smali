.class Lcom/swarmconnect/ac;
.super Ljava/lang/Object;


# instance fields
.field protected a:Z

.field private b:Lcom/swarmconnect/ad;

.field private c:Ljava/lang/String;

.field private d:I

.field private final e:Ljava/net/Socket;

.field private f:Ljava/util/Timer;

.field private g:Ljava/lang/Object;

.field private h:Z

.field private i:Ljava/lang/Throwable;

.field private j:Lcom/swarmconnect/ae;

.field private k:Ljava/lang/Thread;


# direct methods
.method protected constructor <init>(Ljava/lang/String;ILcom/swarmconnect/ad;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/swarmconnect/ac;->a:Z

    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/ac;->g:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/swarmconnect/ac;->h:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ac;->i:Ljava/lang/Throwable;

    iput-object p1, p0, Lcom/swarmconnect/ac;->c:Ljava/lang/String;

    iput p2, p0, Lcom/swarmconnect/ac;->d:I

    iput-object p3, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/swarmconnect/ac;->c:Ljava/lang/String;

    iget v3, p0, Lcom/swarmconnect/ac;->d:I

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v0, Lcom/swarmconnect/ae;

    iget-object v1, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/ae;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/swarmconnect/ac;->j:Lcom/swarmconnect/ae;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/ac$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ac$2;-><init>(Lcom/swarmconnect/ac;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/swarmconnect/ac;->k:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/swarmconnect/ac;->k:Ljava/lang/Thread;

    invoke-virtual {v0, v4}, Ljava/lang/Thread;->setDaemon(Z)V

    iget-object v0, p0, Lcom/swarmconnect/ac;->k:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    if-eqz v0, :cond_1

    iput-boolean v4, p0, Lcom/swarmconnect/ac;->a:Z

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v0, v4}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    const v1, 0x927c0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v0, v4}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    iget-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    iget-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    new-instance v1, Lcom/swarmconnect/ac$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ac$1;-><init>(Lcom/swarmconnect/ac;)V

    const-wide/32 v2, 0x6ddd0

    const-wide/32 v4, 0xdbba0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    invoke-interface {v0, p0}, Lcom/swarmconnect/ad;->onConnected(Lcom/swarmconnect/ac;)V

    :cond_1
    return-void
.end method

.method protected a(Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/swarmconnect/ac;->i:Ljava/lang/Throwable;

    :try_start_0
    iget-object v0, p0, Lcom/swarmconnect/ac;->e:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iget-object v1, p0, Lcom/swarmconnect/ac;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/swarmconnect/ac;->h:Z

    iget-object v0, p0, Lcom/swarmconnect/ac;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ac;->f:Ljava/util/Timer;

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    iget-object v1, p0, Lcom/swarmconnect/ac;->i:Ljava/lang/Throwable;

    invoke-interface {v0, p0, v1}, Lcom/swarmconnect/ad;->onConnectionLost(Lcom/swarmconnect/ac;Ljava/lang/Throwable;)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/swarmconnect/ac;->a:Z

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method protected a([B)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/ac;->b([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in ConnectionManager.sendPacket: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swarmconnect/ac;->a(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/swarmconnect/ac;->j:Lcom/swarmconnect/ae;

    invoke-virtual {v0}, Lcom/swarmconnect/ae;->a()[B

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Peer sent DISCONNECT"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    aget-byte v1, v0, v3

    and-int/lit16 v1, v1, 0xff

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server sent Disconnect Packet"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/ac;->b:Lcom/swarmconnect/ad;

    aget-byte v2, v0, v3

    and-int/lit16 v2, v2, 0xff

    invoke-interface {v1, v2, v0}, Lcom/swarmconnect/ad;->onPacketReceived(I[B)V

    goto :goto_0
.end method

.method protected b([B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/swarmconnect/ac;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/swarmconnect/ac;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ac;->j:Lcom/swarmconnect/ae;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v2, "sendMessage() on a closed connection"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/ac;->i:Ljava/lang/Throwable;

    invoke-virtual {v0, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/swarmconnect/ac;->j:Lcom/swarmconnect/ae;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/ae;->a([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ac;->a(Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
