.class Lcom/swarmconnect/ag$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/ag;->getDisplayAd(Lcom/swarmconnect/by;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ag;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ag$1;->a:Lcom/swarmconnect/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&publisher_user_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&size="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/swarmconnect/ag;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/swarmconnect/ag;->b()Lcom/swarmconnect/cm;

    move-result-object v1

    const-string v2, "https://ws.tapjoyads.com/display_ad?"

    invoke-virtual {v1, v2, v0}, Lcom/swarmconnect/cm;->connectToURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-static {}, Lcom/swarmconnect/ag;->c()Lcom/swarmconnect/by;

    move-result-object v0

    const-string v1, "Network error."

    invoke-interface {v0, v1}, Lcom/swarmconnect/by;->getDisplayAdResponseFailed(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/swarmconnect/ag$1;->a:Lcom/swarmconnect/ag;

    invoke-static {v1, v0}, Lcom/swarmconnect/ag;->a(Lcom/swarmconnect/ag;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/swarmconnect/ag;->c()Lcom/swarmconnect/by;

    move-result-object v0

    const-string v1, "No ad to display."

    invoke-interface {v0, v1}, Lcom/swarmconnect/by;->getDisplayAdResponseFailed(Ljava/lang/String;)V

    goto :goto_0
.end method
