.class Lcom/swarmconnect/ah;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/ah$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/ah$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmStoreItem;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/ah$a;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/ah$a;-><init>(Lcom/swarmconnect/ah;Lcom/swarmconnect/ah$a;)V

    iput-object v0, p0, Lcom/swarmconnect/ah;->l:Lcom/swarmconnect/ah$a;

    iput-object v1, p0, Lcom/swarmconnect/ah;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ah;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ah;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/ah;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ah;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/ah;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ah;->e()V

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/ah;)Lcom/swarmconnect/ah$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ah;->l:Lcom/swarmconnect/ah$a;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/ah;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ah;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/ah;->b()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/ah$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ah$3;-><init>(Lcom/swarmconnect/ah;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getInventory(Lcom/swarmconnect/SwarmActiveUser$GotInventoryCB;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/ah$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ah$4;-><init>(Lcom/swarmconnect/ah;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getCoins(Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_inventory"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->b(I)V

    const-string v0, "@id/coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/ah;->n:Landroid/widget/TextView;

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/ah;->l:Lcom/swarmconnect/ah$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/ah$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ah$1;-><init>(Lcom/swarmconnect/ah;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/ah$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ah$2;-><init>(Lcom/swarmconnect/ah;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ah;->a(Ljava/lang/Runnable;)V

    return-void
.end method
