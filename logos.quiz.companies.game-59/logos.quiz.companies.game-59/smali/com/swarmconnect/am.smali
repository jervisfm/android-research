.class Lcom/swarmconnect/am;
.super Ljava/lang/Object;


# static fields
.field private static c:Lcom/swarmconnect/x;

.field private static d:Lcom/swarmconnect/cm;

.field public static featuredAppURLParams:Ljava/lang/String;


# instance fields
.field final a:Ljava/lang/String;

.field private b:Lcom/swarmconnect/al;

.field private e:Landroid/content/Context;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/am;->d:Lcom/swarmconnect/cm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const/4 v0, 0x5

    iput v0, p0, Lcom/swarmconnect/am;->f:I

    const-string v0, "Featured App"

    iput-object v0, p0, Lcom/swarmconnect/am;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/swarmconnect/am;->e:Landroid/content/Context;

    new-instance v0, Lcom/swarmconnect/cm;

    invoke-direct {v0}, Lcom/swarmconnect/cm;-><init>()V

    sput-object v0, Lcom/swarmconnect/am;->d:Lcom/swarmconnect/cm;

    return-void
.end method

.method static synthetic a()Lcom/swarmconnect/cm;
    .locals 1

    sget-object v0, Lcom/swarmconnect/am;->d:Lcom/swarmconnect/cm;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/am;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/am;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    :try_start_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    const-string v4, "UTF-8"

    invoke-virtual {p1, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "Cost"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->cost:Ljava/lang/String;

    const-string v3, "Amount"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-static {v3}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v4, Lcom/swarmconnect/al;->amount:I

    :cond_0
    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "Description"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->description:Ljava/lang/String;

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "IconURL"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->iconURL:Ljava/lang/String;

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "Name"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "RedirectURL"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->redirectURL:Ljava/lang/String;

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "StoreID"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    invoke-static {v4}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/swarmconnect/al;->storeID:Ljava/lang/String;

    iget-object v3, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    const-string v4, "FullScreenAdURL"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/swarmconnect/al;->fullScreenAdURL:Ljava/lang/String;

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cost: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->cost:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "amount: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget v4, v4, Lcom/swarmconnect/al;->amount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "description: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "iconURL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->iconURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "name: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "redirectURL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->redirectURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "storeID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->storeID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fullScreenAdURL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v4, v4, Lcom/swarmconnect/al;->fullScreenAdURL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v0, v0, Lcom/swarmconnect/al;->fullScreenAdURL:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v0, v0, Lcom/swarmconnect/al;->fullScreenAdURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v1, v1, Lcom/swarmconnect/al;->storeID:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/swarmconnect/am;->b(Ljava/lang/String;)I

    move-result v1

    iget v2, p0, Lcom/swarmconnect/am;->f:I

    if-ge v1, v2, :cond_4

    sget-object v1, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    iget-object v2, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    invoke-interface {v1, v2}, Lcom/swarmconnect/x;->getFeaturedAppResponse(Lcom/swarmconnect/al;)V

    invoke-static {}, Lcom/swarmconnect/bj;->getAppID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v2, v2, Lcom/swarmconnect/al;->storeID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v1, v1, Lcom/swarmconnect/al;->storeID:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/swarmconnect/am;->c(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return v0

    :cond_3
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Featured App"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing XML: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    const-string v2, "Featured App to display has exceeded display count"

    invoke-interface {v1, v2}, Lcom/swarmconnect/x;->getFeaturedAppResponseFailed(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    const-string v2, "Failed to parse XML file from response"

    invoke-interface {v0, v2}, Lcom/swarmconnect/x;->getFeaturedAppResponseFailed(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)I
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/am;->e:Landroid/content/Context;

    const-string v1, "TapjoyFeaturedAppPrefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "Featured App"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getDisplayCount: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", storeID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method static synthetic b()Lcom/swarmconnect/x;
    .locals 1

    sget-object v0, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/am;->e:Landroid/content/Context;

    const-string v1, "TapjoyFeaturedAppPrefs"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const-string v2, "Featured App"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "incrementDisplayCount: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", storeID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public getFeaturedApp(Lcom/swarmconnect/x;)V
    .locals 2

    const-string v0, "Featured App"

    const-string v1, "Getting Featured App"

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    sput-object p1, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    new-instance v0, Lcom/swarmconnect/al;

    invoke-direct {v0}, Lcom/swarmconnect/al;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/am;->featuredAppURLParams:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/am;->featuredAppURLParams:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&publisher_user_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/am;->featuredAppURLParams:Ljava/lang/String;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/am$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/am$2;-><init>(Lcom/swarmconnect/am;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getFeaturedApp(Ljava/lang/String;Lcom/swarmconnect/x;)V
    .locals 3

    const-string v0, "Featured App"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Getting Featured App userID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", currencyID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    sput-object p2, Lcom/swarmconnect/am;->c:Lcom/swarmconnect/x;

    new-instance v0, Lcom/swarmconnect/al;

    invoke-direct {v0}, Lcom/swarmconnect/al;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&publisher_user_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&currency_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/am$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/am$1;-><init>(Lcom/swarmconnect/am;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getFeaturedAppObject()Lcom/swarmconnect/al;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    return-object v0
.end method

.method public setDisplayCount(I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/am;->f:I

    return-void
.end method

.method public showFeaturedAppFullScreenAd()V
    .locals 4

    const-string v0, ""

    iget-object v1, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/am;->b:Lcom/swarmconnect/al;

    iget-object v0, v0, Lcom/swarmconnect/al;->fullScreenAdURL:Ljava/lang/String;

    :cond_0
    const-string v1, "Featured App"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Displaying Full Screen AD with URL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/swarmconnect/am;->e:Landroid/content/Context;

    const-class v3, Lcom/swarmconnect/bb;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "USER_ID"

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "URL_PARAMS"

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "FULLSCREEN_AD_URL"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "CLIENT_PACKAGE"

    invoke-static {}, Lcom/swarmconnect/bj;->getClientPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/swarmconnect/am;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void
.end method
