.class Lcom/swarmconnect/ao$2;
.super Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/ao;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ao;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ao;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ao$2;->a:Lcom/swarmconnect/ao;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmAchievement;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/ao$2;->a:Lcom/swarmconnect/ao;

    invoke-virtual {v0}, Lcom/swarmconnect/ao;->c()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ao$2;->a:Lcom/swarmconnect/ao;

    invoke-static {v0, p1}, Lcom/swarmconnect/ao;->a(Lcom/swarmconnect/ao;Ljava/util/List;)V

    iget-object v0, p0, Lcom/swarmconnect/ao$2;->a:Lcom/swarmconnect/ao;

    invoke-static {v0}, Lcom/swarmconnect/ao;->a(Lcom/swarmconnect/ao;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/ao$2;->a:Lcom/swarmconnect/ao;

    invoke-static {v0}, Lcom/swarmconnect/ao;->c(Lcom/swarmconnect/ao;)Lcom/swarmconnect/ao$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/ao$a;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmAchievement;

    iget-boolean v2, v0, Lcom/swarmconnect/SwarmAchievement;->hidden:Z

    if-eqz v2, :cond_0

    iget-boolean v0, v0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method
