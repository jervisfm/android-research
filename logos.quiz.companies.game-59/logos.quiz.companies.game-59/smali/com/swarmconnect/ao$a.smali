.class Lcom/swarmconnect/ao$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/ao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ao;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/ao;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/ao;Lcom/swarmconnect/ao$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/ao$a;-><init>(Lcom/swarmconnect/ao;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    invoke-static {v0}, Lcom/swarmconnect/ao;->a(Lcom/swarmconnect/ao;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    invoke-static {v0}, Lcom/swarmconnect/ao;->a(Lcom/swarmconnect/ao;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    invoke-static {v0}, Lcom/swarmconnect/ao;->a(Lcom/swarmconnect/ao;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    invoke-virtual {v0}, Lcom/swarmconnect/ao;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@layout/swarm_achievement_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/ao$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmAchievement;

    if-eqz v0, :cond_1

    rem-int/lit8 v1, p1, 0x2

    if-nez v1, :cond_2

    const v1, 0xffffff

    :goto_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@id/title"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcom/swarmconnect/SwarmAchievement;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@id/description"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcom/swarmconnect/SwarmAchievement;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@id/points"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v0, Lcom/swarmconnect/SwarmAchievement;->points:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@id/points"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, -0xddddde

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-boolean v0, v0, Lcom/swarmconnect/SwarmAchievement;->unlocked:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v1, "@id/achievement_icon"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@drawable/swarm_trophy_gold"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_1
    return-object p2

    :cond_2
    const v1, -0x77000001

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v1, "@id/achievement_icon"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/swarmconnect/ao$a;->a:Lcom/swarmconnect/ao;

    const-string v2, "@drawable/swarm_trophy_grey"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
