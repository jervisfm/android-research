.class Lcom/swarmconnect/ao;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/ao$a;
    }
.end annotation


# instance fields
.field private l:Landroid/widget/ListView;

.field private m:Lcom/swarmconnect/ao$a;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmAchievement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/ao$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/ao$a;-><init>(Lcom/swarmconnect/ao;Lcom/swarmconnect/ao$a;)V

    iput-object v0, p0, Lcom/swarmconnect/ao;->m:Lcom/swarmconnect/ao$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/ao;->n:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ao;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ao;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/ao;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ao;->n:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/ao;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ao;->e()V

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/ao;)Lcom/swarmconnect/ao$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ao;->m:Lcom/swarmconnect/ao$a;

    return-object v0
.end method

.method private e()V
    .locals 1

    invoke-virtual {p0}, Lcom/swarmconnect/ao;->b()V

    new-instance v0, Lcom/swarmconnect/ao$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ao$2;-><init>(Lcom/swarmconnect/ao;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmAchievement;->getAchievementsList(Lcom/swarmconnect/SwarmAchievement$GotAchievementsListCB;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->b(I)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/swarmconnect/ao;->l:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/swarmconnect/ao;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/ao;->m:Lcom/swarmconnect/ao$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_trophy"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Achievements"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/ao;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/ao$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ao$1;-><init>(Lcom/swarmconnect/ao;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ao;->a(Ljava/lang/Runnable;)V

    return-void
.end method
