.class Lcom/swarmconnect/ap;
.super Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/APICall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/APICall;


# direct methods
.method constructor <init>(Lcom/swarmconnect/APICall;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-direct {p0}, Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotURL(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Response: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bx;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/APICall;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-static {v0, p1}, Lcom/swarmconnect/aq;->a(Lcom/swarmconnect/APICall;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    iget-object v0, v0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-static {v0, p1}, Lcom/swarmconnect/APICall;->a(Lcom/swarmconnect/APICall;Ljava/lang/String;)Lcom/swarmconnect/APICall;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    iget-object v1, v1, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/APICall$APICallback;->gotAPI(Lcom/swarmconnect/APICall;)V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/swarmconnect/Swarm;->b()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    iget-object v0, v0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/APICall$APICallback;->requestFailed()V

    goto :goto_0
.end method

.method public requestFailed(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    iget-object v0, v0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    iget-object v0, v0, Lcom/swarmconnect/APICall;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/APICall$APICallback;->requestFailed()V

    :cond_0
    invoke-static {}, Lcom/swarmconnect/APICall;->d()Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/ap;->a:Lcom/swarmconnect/APICall;

    invoke-static {v1}, Lcom/swarmconnect/APICall;->a(Lcom/swarmconnect/APICall;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swarmconnect/APICall;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->a()V

    return-void
.end method
