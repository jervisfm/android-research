.class Lcom/swarmconnect/aq$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/aq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field public api:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/APICall;",
            ">;"
        }
    .end annotation
.end field

.field public expiration:J

.field public response:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/APICall;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swarmconnect/aq$a;->api:Ljava/lang/Class;

    iput-object p2, p0, Lcom/swarmconnect/aq$a;->url:Ljava/lang/String;

    iput-object p3, p0, Lcom/swarmconnect/aq$a;->response:Ljava/lang/String;

    iput-wide p4, p0, Lcom/swarmconnect/aq$a;->expiration:J

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 4

    instance-of v0, p1, Lcom/swarmconnect/aq$a;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ClassCastException;

    const-string v1, "CachedResponse expected."

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-wide v0, p0, Lcom/swarmconnect/aq$a;->expiration:J

    check-cast p1, Lcom/swarmconnect/aq$a;

    iget-wide v2, p1, Lcom/swarmconnect/aq$a;->expiration:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
