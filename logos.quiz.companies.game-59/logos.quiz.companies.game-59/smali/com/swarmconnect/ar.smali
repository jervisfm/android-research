.class Lcom/swarmconnect/ar;
.super Lcom/swarmconnect/ba;


# static fields
.field public static prefillUsername:Ljava/lang/String;


# instance fields
.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/EditText;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/ar;->prefillUsername:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/ba;-><init>()V

    new-instance v0, Lcom/swarmconnect/bg;

    invoke-direct {v0, p0}, Lcom/swarmconnect/bg;-><init>(Lcom/swarmconnect/ar;)V

    iput-object v0, p0, Lcom/swarmconnect/ar;->p:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ar;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/ar;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ar;->n:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/ar;)Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ar;->p:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    return-object v0
.end method


# virtual methods
.method public createAccount()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ar;->o:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/ar;->o:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/ar;->n:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/swarmconnect/ar;->b()V

    new-instance v1, Lcom/swarmconnect/ab;

    invoke-direct {v1}, Lcom/swarmconnect/ab;-><init>()V

    iput-object v0, v1, Lcom/swarmconnect/ab;->username:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/ar$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ar$3;-><init>(Lcom/swarmconnect/ar;)V

    iput-object v0, v1, Lcom/swarmconnect/ab;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1}, Lcom/swarmconnect/ab;->run()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_external_username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->b(I)V

    const-string v0, "@id/terms"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/ar$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ar$2;-><init>(Lcom/swarmconnect/ar;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/ar;->n:Landroid/widget/EditText;

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/ar;->m:Landroid/widget/Button;

    iget-object v0, p0, Lcom/swarmconnect/ar;->m:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/ar;->m:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/ar$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ar$1;-><init>(Lcom/swarmconnect/ar;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/username_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ar;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/ar;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/ar;->o:Landroid/widget/TextView;

    const v1, -0x70e5d3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-super {p0, p1}, Lcom/swarmconnect/ba;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/swarmconnect/ba;->onResume()V

    invoke-static {}, Lcom/swarmconnect/SwarmFacebook;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Facebook is valid!"

    invoke-static {v0}, Lcom/swarmconnect/ar;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/swarmconnect/ar;->prefillUsername:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ar;->n:Landroid/widget/EditText;

    sget-object v1, Lcom/swarmconnect/ar;->prefillUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/ar;->prefillUsername:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/swarmconnect/ar;->finish()V

    goto :goto_0
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
