.class Lcom/swarmconnect/at;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/swarmconnect/delegates/SwarmNotificationDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public gotNotification(Lcom/swarmconnect/SwarmNotification;)Z
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/SwarmDashboardScreen;

    if-nez v0, :cond_b

    :cond_2
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->FRIEND:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/q;

    if-nez v0, :cond_b

    :cond_3
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->ONLINE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/q;

    if-nez v0, :cond_b

    :cond_4
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/g;

    if-nez v0, :cond_b

    :cond_5
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/t;

    if-nez v0, :cond_b

    :cond_6
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->MESSAGE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_7

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/SwarmDashboardScreen;

    if-nez v0, :cond_b

    :cond_7
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_8

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/bf;

    if-nez v0, :cond_b

    :cond_8
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->GOT_COINS:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_9

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/m;

    if-nez v0, :cond_b

    :cond_9
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_a

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/bf;

    if-nez v0, :cond_b

    :cond_a
    iget-object v0, p1, Lcom/swarmconnect/SwarmNotification;->type:Lcom/swarmconnect/SwarmNotification$NotificationType;

    sget-object v1, Lcom/swarmconnect/SwarmNotification$NotificationType;->PURCHASE:Lcom/swarmconnect/SwarmNotification$NotificationType;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    instance-of v0, v0, Lcom/swarmconnect/m;

    if-eqz v0, :cond_0

    :cond_b
    sget-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->refresh()V

    goto :goto_0
.end method
