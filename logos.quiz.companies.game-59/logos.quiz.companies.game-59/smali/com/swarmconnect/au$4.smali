.class Lcom/swarmconnect/au$4;
.super Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/au;->b(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/au;


# direct methods
.method constructor <init>(Lcom/swarmconnect/au;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotScores(ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;)V"
        }
    .end annotation

    const/16 v1, 0x8

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-virtual {v2}, Lcom/swarmconnect/au;->c()V

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->a(Lcom/swarmconnect/au;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->a(Lcom/swarmconnect/au;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->e(Lcom/swarmconnect/au;)Lcom/swarmconnect/utils/SeparatedListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swarmconnect/utils/SeparatedListAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->a(Lcom/swarmconnect/au;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->g(Lcom/swarmconnect/au;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->h(Lcom/swarmconnect/au;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->i(Lcom/swarmconnect/au;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v3}, Lcom/swarmconnect/au;->a(Lcom/swarmconnect/au;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    rem-int/lit8 v3, v3, 0x19

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->g(Lcom/swarmconnect/au;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v1}, Lcom/swarmconnect/au;->h(Lcom/swarmconnect/au;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/au$4;->a:Lcom/swarmconnect/au;

    invoke-static {v0}, Lcom/swarmconnect/au;->h(Lcom/swarmconnect/au;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "No scores"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
