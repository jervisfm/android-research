.class Lcom/swarmconnect/au$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/au;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/au;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swarmconnect/au;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/SwarmLeaderboardScore;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmLeaderboardScore;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/au$a;->getItem(I)Lcom/swarmconnect/SwarmLeaderboardScore;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-virtual {v0}, Lcom/swarmconnect/au;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@layout/swarm_leaderboard_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/au$a;->getItem(I)Lcom/swarmconnect/SwarmLeaderboardScore;

    move-result-object v1

    if-eqz v1, :cond_2

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_3

    const v0, 0xffffff

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->user:Lcom/swarmconnect/SwarmUser;

    iget v0, v0, Lcom/swarmconnect/SwarmUser;->userId:I

    sget-object v2, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v2, v2, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/au$a;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-static {v2}, Lcom/swarmconnect/au;->a(Lcom/swarmconnect/au;)Ljava/util/List;

    move-result-object v2

    if-ne v0, v2, :cond_1

    const v0, 0x551988d7

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@id/rank"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->rank:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@id/name"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v2, v2, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-static {v0}, Lcom/swarmconnect/au;->b(Lcom/swarmconnect/au;)Lcom/swarmconnect/SwarmLeaderboard;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmLeaderboard;->format:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    sget-object v2, Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;->INTEGER:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@id/score"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    float-to-int v1, v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-object p2

    :cond_3
    const v0, -0x77000001

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-static {v0}, Lcom/swarmconnect/au;->b(Lcom/swarmconnect/au;)Lcom/swarmconnect/SwarmLeaderboard;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmLeaderboard;->format:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    sget-object v2, Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;->TIME:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    if-ne v0, v2, :cond_5

    iget v0, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    float-to-int v0, v0

    div-int/lit16 v0, v0, 0xe10

    iget v2, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    float-to-int v2, v2

    rem-int/lit16 v2, v2, 0xe10

    div-int/lit8 v2, v2, 0x3c

    iget v1, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    const/high16 v3, 0x4270

    rem-float/2addr v1, v3

    new-instance v3, Ljava/text/DecimalFormat;

    invoke-direct {v3}, Ljava/text/DecimalFormat;-><init>()V

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/text/DecimalFormat;->setMinimumIntegerDigits(I)V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    float-to-double v1, v1

    invoke-virtual {v3, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@id/score"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    invoke-static {v0}, Lcom/swarmconnect/au;->b(Lcom/swarmconnect/au;)Lcom/swarmconnect/SwarmLeaderboard;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmLeaderboard;->format:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    sget-object v2, Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;->FLOAT:Lcom/swarmconnect/SwarmLeaderboard$LeaderboardFormat;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/au$a;->a:Lcom/swarmconnect/au;

    const-string v2, "@id/score"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v1, Lcom/swarmconnect/SwarmLeaderboardScore;->score:F

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
