.class Lcom/swarmconnect/au;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/au$a;
    }
.end annotation


# static fields
.field private static l:Ljava/lang/String;


# instance fields
.field private m:Lcom/swarmconnect/SwarmLeaderboard;

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboardScore;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/widget/ListView;

.field private r:Lcom/swarmconnect/utils/SeparatedListAdapter;

.field private s:Lcom/swarmconnect/au$a;

.field private t:Lcom/swarmconnect/au$a;

.field private u:Lcom/swarmconnect/au$a;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/au;->n:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/au;->o:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/au;->p:Ljava/util/List;

    new-instance v0, Lcom/swarmconnect/au$a;

    iget-object v1, p0, Lcom/swarmconnect/au;->n:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/au$a;-><init>(Lcom/swarmconnect/au;Ljava/util/List;)V

    iput-object v0, p0, Lcom/swarmconnect/au;->s:Lcom/swarmconnect/au$a;

    new-instance v0, Lcom/swarmconnect/au$a;

    iget-object v1, p0, Lcom/swarmconnect/au;->o:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/au$a;-><init>(Lcom/swarmconnect/au;Ljava/util/List;)V

    iput-object v0, p0, Lcom/swarmconnect/au;->t:Lcom/swarmconnect/au$a;

    new-instance v0, Lcom/swarmconnect/au$a;

    iget-object v1, p0, Lcom/swarmconnect/au;->p:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/au$a;-><init>(Lcom/swarmconnect/au;Ljava/util/List;)V

    iput-object v0, p0, Lcom/swarmconnect/au;->u:Lcom/swarmconnect/au$a;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/au;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/au;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/swarmconnect/au;->b(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/au;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/au;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/au;)Lcom/swarmconnect/SwarmLeaderboard;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    return-object v0
.end method

.method private b(ILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/swarmconnect/au;->v:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/swarmconnect/au;->b()V

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    const-string v1, "self"

    new-instance v2, Lcom/swarmconnect/au$3;

    invoke-direct {v2, p0}, Lcom/swarmconnect/au$3;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v0, v3, p2, v1, v2}, Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    const-string v1, "friends"

    new-instance v2, Lcom/swarmconnect/au$5;

    invoke-direct {v2, p0}, Lcom/swarmconnect/au$5;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v0, v3, p2, v1, v2}, Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    const-string v1, "all"

    new-instance v2, Lcom/swarmconnect/au$4;

    invoke-direct {v2, p0}, Lcom/swarmconnect/au$4;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/swarmconnect/SwarmLeaderboard;->getPageOfScores(ILjava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmLeaderboard$GotScoresCB;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/au;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/au;->g()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    if-eq v0, p1, :cond_1

    sput-object p1, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/au;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/swarmconnect/au;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/swarmconnect/au;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    invoke-virtual {v0}, Lcom/swarmconnect/utils/SeparatedListAdapter;->notifyDataSetChanged()V

    sget-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    new-instance v3, Lcom/swarmconnect/au$2;

    invoke-direct {v3, p0}, Lcom/swarmconnect/au$2;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/swarmconnect/au;->a(Ljava/lang/String;ZLjava/lang/Runnable;)V

    sget-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/swarmconnect/au;->b(ILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method static synthetic d(Lcom/swarmconnect/au;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/swarmconnect/au;)Lcom/swarmconnect/utils/SeparatedListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/swarmconnect/au;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->o:Ljava/util/List;

    return-object v0
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/swarmconnect/au;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->q:Landroid/widget/ListView;

    return-object v0
.end method

.method private g()V
    .locals 14

    invoke-direct {p0}, Lcom/swarmconnect/au;->f()V

    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const v1, -0xcccccd

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/high16 v2, 0x4120

    invoke-static {v2}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    float-to-int v2, v2

    const/high16 v3, 0x40a0

    invoke-static {v3}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    float-to-int v3, v3

    const/high16 v4, 0x4120

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v5, 0x40a0

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const v2, -0xddddde

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const-string v3, "@drawable/swarm_head"

    invoke-virtual {p0, v3}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x4188

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/high16 v5, 0x4190

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v4, "Show Scores"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, -0x333334

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v4, 0x3f80

    const/high16 v5, 0x3f80

    const/high16 v6, 0x3f80

    const/high16 v7, -0x100

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    const/high16 v4, 0x4170

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextSize(F)V

    const/high16 v4, 0x40a0

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v6, 0x4348

    invoke-static {v6}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v6

    float-to-int v6, v6

    const/4 v7, 0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v5, -0xaaaaab

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v6, 0x4120

    invoke-static {v6}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v6

    float-to-int v6, v6

    const/high16 v7, 0x4120

    invoke-static {v7}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v7

    float-to-int v7, v7

    const/high16 v8, 0x4120

    invoke-static {v8}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v8

    float-to-int v8, v8

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    new-instance v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v6, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v7, "Today"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->contextualGreyBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v7, 0x4188

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextSize(F)V

    const/high16 v7, 0x3f80

    const/high16 v8, 0x3f80

    const/high16 v9, 0x3f80

    const/high16 v10, -0x100

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    const/high16 v7, 0x41a0

    invoke-static {v7}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v7

    float-to-int v7, v7

    const/high16 v8, 0x4140

    invoke-static {v8}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v8

    float-to-int v8, v8

    const/high16 v9, 0x41a0

    invoke-static {v9}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v9

    float-to-int v9, v9

    const/high16 v10, 0x4140

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v7, Lcom/swarmconnect/au$7;

    invoke-direct {v7, p0}, Lcom/swarmconnect/au$7;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Landroid/widget/TextView;

    iget-object v8, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v8, "This Week"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v8, 0x11

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->contextualGreyBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v8, 0x4188

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextSize(F)V

    const/high16 v8, 0x3f80

    const/high16 v9, 0x3f80

    const/high16 v10, 0x3f80

    const/high16 v11, -0x100

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    const/high16 v8, 0x41a0

    invoke-static {v8}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v8

    float-to-int v8, v8

    const/high16 v9, 0x4140

    invoke-static {v9}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v9

    float-to-int v9, v9

    const/high16 v10, 0x41a0

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    const/high16 v11, 0x4140

    invoke-static {v11}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v8, Lcom/swarmconnect/au$6;

    invoke-direct {v8, p0}, Lcom/swarmconnect/au$6;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v8, Landroid/widget/TextView;

    iget-object v9, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v8, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v9, "This Month"

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v9, 0x11

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->contextualGreyBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v9, 0x4188

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextSize(F)V

    const/high16 v9, 0x3f80

    const/high16 v10, 0x3f80

    const/high16 v11, 0x3f80

    const/high16 v12, -0x100

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    const/high16 v9, 0x41a0

    invoke-static {v9}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v9

    float-to-int v9, v9

    const/high16 v10, 0x4140

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    const/high16 v11, 0x41a0

    invoke-static {v11}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v11

    float-to-int v11, v11

    const/high16 v12, 0x4140

    invoke-static {v12}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v12

    float-to-int v12, v12

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Lcom/swarmconnect/au$9;

    invoke-direct {v5, p0}, Lcom/swarmconnect/au$9;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v8, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x2

    invoke-direct {v5, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v9, 0x4120

    invoke-static {v9}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v9

    float-to-int v9, v9

    const/high16 v10, 0x4120

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    const/high16 v11, 0x4120

    invoke-static {v11}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v11

    float-to-int v11, v11

    const/high16 v12, 0x4120

    invoke-static {v12}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v12

    float-to-int v12, v12

    invoke-virtual {v5, v9, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v10, 0x4120

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    const/high16 v11, 0x4120

    invoke-static {v11}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v11

    float-to-int v11, v11

    const/high16 v12, 0x4120

    invoke-static {v12}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v12

    float-to-int v12, v12

    const/high16 v13, 0x4120

    invoke-static {v13}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v13

    float-to-int v13, v13

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    new-instance v9, Landroid/widget/TextView;

    iget-object v10, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v9, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v10, "All Time"

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v10, 0x11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setGravity(I)V

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->contextualGreyBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v10, -0x1

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v10, 0x4188

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextSize(F)V

    const/high16 v10, 0x3f80

    const/high16 v11, 0x3f80

    const/high16 v12, 0x3f80

    const/high16 v13, -0x100

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    const/high16 v10, 0x41a0

    invoke-static {v10}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v10

    float-to-int v10, v10

    const/high16 v11, 0x4140

    invoke-static {v11}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v11

    float-to-int v11, v11

    const/high16 v12, 0x41a0

    invoke-static {v12}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v12

    float-to-int v12, v12

    const/high16 v13, 0x4140

    invoke-static {v13}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v13

    float-to-int v13, v13

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Lcom/swarmconnect/au$8;

    invoke-direct {v5, p0}, Lcom/swarmconnect/au$8;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method static synthetic h(Lcom/swarmconnect/au;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->w:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic i(Lcom/swarmconnect/au;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->v:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/swarmconnect/au;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/au;->x:Landroid/app/Dialog;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "@layout/swarm_leaderboard_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->b(I)V

    iget-object v0, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "leaderboard"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmLeaderboard;

    iput-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "@id/empty_list"

    invoke-virtual {p0, v1}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/au;->w:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "@layout/swarm_load_more"

    invoke-virtual {p0, v1}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const-string v0, "@id/more"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/au;->v:Landroid/view/View;

    new-instance v0, Lcom/swarmconnect/au$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/au$1;-><init>(Lcom/swarmconnect/au;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/swarmconnect/utils/SeparatedListAdapter;

    iget-object v2, p0, Lcom/swarmconnect/au;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v3, "@layout/swarm_list_section_header"

    invoke-virtual {p0, v3}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/swarmconnect/utils/SeparatedListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    iget-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    const-string v2, "My Scores"

    iget-object v3, p0, Lcom/swarmconnect/au;->s:Lcom/swarmconnect/au$a;

    invoke-virtual {v0, v2, v3}, Lcom/swarmconnect/utils/SeparatedListAdapter;->addSection(Ljava/lang/String;Landroid/widget/BaseAdapter;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    const-string v2, "Friend Scores"

    iget-object v3, p0, Lcom/swarmconnect/au;->t:Lcom/swarmconnect/au$a;

    invoke-virtual {v0, v2, v3}, Lcom/swarmconnect/utils/SeparatedListAdapter;->addSection(Ljava/lang/String;Landroid/widget/BaseAdapter;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    const-string v2, "Top Scores"

    iget-object v3, p0, Lcom/swarmconnect/au;->u:Lcom/swarmconnect/au$a;

    invoke-virtual {v0, v2, v3}, Lcom/swarmconnect/utils/SeparatedListAdapter;->addSection(Ljava/lang/String;Landroid/widget/BaseAdapter;)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/swarmconnect/au;->q:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/swarmconnect/au;->q:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/swarmconnect/au;->q:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/au;->r:Lcom/swarmconnect/utils/SeparatedListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/au;->l:Ljava/lang/String;

    const-string v0, "week"

    invoke-direct {p0, v0}, Lcom/swarmconnect/au;->c(Ljava/lang/String;)V

    const-string v0, "@drawable/swarm_leaderboards"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/au;->a(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    if-nez v0, :cond_1

    const-string v0, "Leaderboard"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/swarmconnect/au;->a(ILjava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/au;->m:Lcom/swarmconnect/SwarmLeaderboard;

    iget-object v0, v0, Lcom/swarmconnect/SwarmLeaderboard;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/swarmconnect/u;->onPause()V

    invoke-direct {p0}, Lcom/swarmconnect/au;->f()V

    return-void
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
