.class Lcom/swarmconnect/az;
.super Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/ba;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ba;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ba;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/az;->a:Lcom/swarmconnect/ba;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;-><init>()V

    return-void
.end method


# virtual methods
.method public loginError(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FB Callback error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ba;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/az;->a:Lcom/swarmconnect/ba;

    iget-object v0, v0, Lcom/swarmconnect/ba;->c:Lcom/swarmconnect/SwarmMainActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Facebook Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public loginSuccess(Lcom/facebook/android/Facebook;)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/az;->a:Lcom/swarmconnect/ba;

    iget-object v0, v0, Lcom/swarmconnect/ba;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-static {v0, p1}, Lcom/swarmconnect/Swarm;->a(Landroid/content/Context;Lcom/facebook/android/Facebook;)V

    return-void
.end method
