.class abstract Lcom/swarmconnect/ba;
.super Lcom/swarmconnect/u;


# static fields
.field private static m:Z


# instance fields
.field protected l:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/swarmconnect/ba;->m:Z

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/az;

    invoke-direct {v0, p0}, Lcom/swarmconnect/az;-><init>(Lcom/swarmconnect/ba;)V

    iput-object v0, p0, Lcom/swarmconnect/ba;->l:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    return-void
.end method


# virtual methods
.method protected a(IILandroid/content/Intent;)V
    .locals 0

    invoke-static {p1, p2, p3}, Lcom/swarmconnect/SwarmFacebook;->authorizeCallback(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/swarmconnect/ba;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "289484921072856"

    invoke-static {v0, v1}, Lcom/swarmconnect/SwarmFacebook;->initialize(Landroid/content/Context;Ljava/lang/String;)V

    const-string v0, "@id/facebook_login"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ba;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ba;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->facebookLoginButton()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Lcom/swarmconnect/ba$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ba$1;-><init>(Lcom/swarmconnect/ba;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    instance-of v1, p0, Lcom/swarmconnect/ar;

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/u;->onDestroy()V

    sget-boolean v0, Lcom/swarmconnect/ba;->m:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/ba;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/swarmconnect/Swarm;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/swarmconnect/SwarmLoginManager;->a()V

    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/swarmconnect/ba;->m:Z

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/u;->onResume()V

    sget-boolean v0, Lcom/swarmconnect/ba;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/ba;->m:Z

    invoke-static {}, Lcom/swarmconnect/SwarmLoginManager;->b()V

    :cond_0
    return-void
.end method
