.class Lcom/swarmconnect/be;
.super Ljava/lang/Object;


# static fields
.field public static final TAPJOY_OFFERS:Ljava/lang/String; = "TapjoyOffers"

.field public static final TAPJOY_POINTS:Ljava/lang/String; = "TapjoyPoints"

.field private static d:Lcom/swarmconnect/bc;

.field private static e:Lcom/swarmconnect/f;

.field private static f:Lcom/swarmconnect/cl;

.field private static g:Lcom/swarmconnect/cr;


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/be;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/be;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/swarmconnect/be;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/swarmconnect/be;->i:Ljava/lang/String;

    iput-object p1, p0, Lcom/swarmconnect/be;->c:Landroid/content/Context;

    return-void
.end method

.method static synthetic a()Lcom/swarmconnect/bc;
    .locals 1

    sget-object v0, Lcom/swarmconnect/be;->d:Lcom/swarmconnect/bc;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/be;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/be;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 5

    invoke-static {p1}, Lcom/swarmconnect/av;->buildDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "Success"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "TapPoints"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "CurrencyName"

    invoke-interface {v0, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {}, Lcom/swarmconnect/bj;->getLocalTapPointsTotal()I

    move-result v3

    sget-object v4, Lcom/swarmconnect/be;->g:Lcom/swarmconnect/cr;

    if-eqz v4, :cond_0

    const/16 v4, -0x270f

    if-eq v3, v4, :cond_0

    if-le v2, v3, :cond_0

    sget-object v4, Lcom/swarmconnect/be;->g:Lcom/swarmconnect/cr;

    sub-int/2addr v2, v3

    invoke-interface {v4, v2}, Lcom/swarmconnect/cr;->earnedTapPoints(I)V

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/swarmconnect/bj;->saveTapPointsTotal(I)V

    sget-object v2, Lcom/swarmconnect/be;->d:Lcom/swarmconnect/bc;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v0, v1}, Lcom/swarmconnect/bc;->getUpdatePoints(Ljava/lang/String;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing tags."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing <Success> tag."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic b()Lcom/swarmconnect/f;
    .locals 1

    sget-object v0, Lcom/swarmconnect/be;->e:Lcom/swarmconnect/f;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/be;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/be;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, ""

    invoke-static {p1}, Lcom/swarmconnect/av;->buildDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "Success"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-static {v2}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "TapPoints"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-static {v2}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CurrencyName"

    invoke-interface {v1, v3}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/swarmconnect/bj;->saveTapPointsTotal(I)V

    sget-object v3, Lcom/swarmconnect/be;->e:Lcom/swarmconnect/f;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v1, v2}, Lcom/swarmconnect/f;->getSpendPointsResponse(Ljava/lang/String;I)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing tags."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "Message"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "TapjoyPoints"

    invoke-static {v2, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/swarmconnect/be;->e:Lcom/swarmconnect/f;

    invoke-interface {v2, v1}, Lcom/swarmconnect/f;->getSpendPointsResponseFailed(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing <Success> tag."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic c()Lcom/swarmconnect/cl;
    .locals 1

    sget-object v0, Lcom/swarmconnect/be;->f:Lcom/swarmconnect/cl;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/be;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/swarmconnect/be;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x1

    const-string v1, ""

    invoke-static {p1}, Lcom/swarmconnect/av;->buildDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "Success"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-static {v2}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "TapPoints"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    invoke-static {v2}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CurrencyName"

    invoke-interface {v1, v3}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Lcom/swarmconnect/bj;->saveTapPointsTotal(I)V

    sget-object v3, Lcom/swarmconnect/be;->f:Lcom/swarmconnect/cl;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v1, v2}, Lcom/swarmconnect/cl;->getAwardPointsResponse(Ljava/lang/String;I)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing tags."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "Message"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/av;->getNodeTrimValue(Lorg/w3c/dom/NodeList;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "TapjoyPoints"

    invoke-static {v2, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/swarmconnect/be;->f:Lcom/swarmconnect/cl;

    invoke-interface {v2, v1}, Lcom/swarmconnect/cl;->getAwardPointsResponseFailed(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "TapjoyPoints"

    const-string v1, "Invalid XML: Missing <Success> tag."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public awardTapPoints(ILcom/swarmconnect/cl;)V
    .locals 2

    if-gez p1, :cond_0

    const-string v0, "TapjoyPoints"

    const-string v1, "spendTapPoints error: amount must be a positive number"

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/swarmconnect/be;->b:I

    sput-object p2, Lcom/swarmconnect/be;->f:Lcom/swarmconnect/cl;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/be$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/be$1;-><init>(Lcom/swarmconnect/be;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public getTapPoints(Lcom/swarmconnect/bc;)V
    .locals 2

    sput-object p1, Lcom/swarmconnect/be;->d:Lcom/swarmconnect/bc;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/be$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/be$2;-><init>(Lcom/swarmconnect/be;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setEarnedPointsNotifier(Lcom/swarmconnect/cr;)V
    .locals 0

    sput-object p1, Lcom/swarmconnect/be;->g:Lcom/swarmconnect/cr;

    return-void
.end method

.method public showOffers()V
    .locals 3

    const-string v0, "TapjoyOffers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Showing offers with userID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/swarmconnect/be;->c:Landroid/content/Context;

    const-class v2, Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "screenType"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "USER_ID"

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "URL_PARAMS"

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "CLIENT_PACKAGE"

    invoke-static {}, Lcom/swarmconnect/bj;->getClientPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/swarmconnect/be;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public showOffersWithCurrencyID(Ljava/lang/String;Z)V
    .locals 4

    const-string v0, "TapjoyOffers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Showing offers with currencyID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", selector: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (userID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swarmconnect/be;->h:Ljava/lang/String;

    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    iput-object v0, p0, Lcom/swarmconnect/be;->i:Ljava/lang/String;

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&currency_id="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/be;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&currency_selector="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/be;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/swarmconnect/be;->c:Landroid/content/Context;

    const-class v3, Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "screenType"

    const/16 v3, 0x13

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "USER_ID"

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "URL_PARAMS"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "CLIENT_PACKAGE"

    invoke-static {}, Lcom/swarmconnect/bj;->getClientPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/swarmconnect/be;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public spendTapPoints(ILcom/swarmconnect/f;)V
    .locals 2

    if-gez p1, :cond_0

    const-string v0, "TapjoyPoints"

    const-string v1, "spendTapPoints error: amount must be a positive number"

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/be;->a:Ljava/lang/String;

    sput-object p2, Lcom/swarmconnect/be;->e:Lcom/swarmconnect/f;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/be$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/be$3;-><init>(Lcom/swarmconnect/be;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
