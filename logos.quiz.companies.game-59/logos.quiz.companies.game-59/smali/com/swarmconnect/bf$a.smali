.class Lcom/swarmconnect/bf$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bf;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/bf;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/bf;Lcom/swarmconnect/bf$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/bf$a;-><init>(Lcom/swarmconnect/bf;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/SwarmStoreCategory;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-static {v0}, Lcom/swarmconnect/bf;->a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/SwarmStore;->categories:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreCategory;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/bf$a;->getItem(I)Lcom/swarmconnect/SwarmStoreCategory;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    invoke-virtual {v0}, Lcom/swarmconnect/bf;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    const-string v2, "@layout/swarm_store_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/bf$a;->getItem(I)Lcom/swarmconnect/SwarmStoreCategory;

    move-result-object v1

    if-eqz v1, :cond_1

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    const v0, 0xffffff

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    const-string v2, "@id/name"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmStoreCategory;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/bf$a;->a:Lcom/swarmconnect/bf;

    const-string v2, "@id/num_items"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2

    :cond_2
    const v0, -0x77000001

    goto :goto_0
.end method
