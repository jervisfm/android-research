.class Lcom/swarmconnect/bf;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/bf$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/bf$a;

.field private m:Lcom/swarmconnect/SwarmStore;

.field private n:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/bf$a;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/bf$a;-><init>(Lcom/swarmconnect/bf;Lcom/swarmconnect/bf$a;)V

    iput-object v0, p0, Lcom/swarmconnect/bf;->l:Lcom/swarmconnect/bf$a;

    iput-object v1, p0, Lcom/swarmconnect/bf;->m:Lcom/swarmconnect/SwarmStore;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/bf;)Lcom/swarmconnect/SwarmStore;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bf;->m:Lcom/swarmconnect/SwarmStore;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/bf;Lcom/swarmconnect/SwarmStore;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bf;->m:Lcom/swarmconnect/SwarmStore;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/bf;)Lcom/swarmconnect/bf$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bf;->l:Lcom/swarmconnect/bf$a;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/bf;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/bf;->e()V

    return-void
.end method

.method static synthetic d(Lcom/swarmconnect/bf;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bf;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/bf;->b()V

    new-instance v0, Lcom/swarmconnect/bf$5;

    invoke-direct {v0, p0}, Lcom/swarmconnect/bf$5;-><init>(Lcom/swarmconnect/bf;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmStore;->getStore(Lcom/swarmconnect/SwarmStore$GotSwarmStoreCB;)V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/bf$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bf$4;-><init>(Lcom/swarmconnect/bf;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getCoins(Lcom/swarmconnect/SwarmActiveUser$GotUserCoinsCB;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_store"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->b(I)V

    const-string v0, "@id/coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/bf;->n:Landroid/widget/TextView;

    const-string v0, "@id/get_coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/get_coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/bf$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bf$1;-><init>(Lcom/swarmconnect/bf;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/bf;->l:Lcom/swarmconnect/bf$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/bf$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bf$3;-><init>(Lcom/swarmconnect/bf;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_store"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Store"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/bf;->a(ILjava/lang/String;)V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/bf$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/bf$2;-><init>(Lcom/swarmconnect/bf;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bf;->a(Ljava/lang/Runnable;)V

    return-void
.end method
