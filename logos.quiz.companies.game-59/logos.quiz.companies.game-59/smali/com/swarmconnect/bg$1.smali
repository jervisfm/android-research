.class Lcom/swarmconnect/bg$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/bg;->loginSuccess(Lcom/facebook/android/Facebook;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bg;


# direct methods
.method constructor <init>(Lcom/swarmconnect/bg;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/ar;->c()V

    check-cast p1, Lcom/swarmconnect/w;

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    iget v0, v0, Lcom/swarmconnect/SwarmUser;->userId:I

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/w;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v1, p1, Lcom/swarmconnect/w;->auth:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;I)V

    invoke-static {}, Lcom/swarmconnect/ar;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/swarmconnect/w;->usernameError:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/w;->usernameError:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p1, Lcom/swarmconnect/w;->usernameError:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p1, Lcom/swarmconnect/w;->statusMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/ar;->c()V

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/ar;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "Account Creation Failed"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/swarmconnect/bg$1;->a:Lcom/swarmconnect/bg;

    invoke-static {v0}, Lcom/swarmconnect/bg;->a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/ar;->finish()V

    return-void
.end method
