.class Lcom/swarmconnect/bg;
.super Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/ar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ar;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ar;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/bg;)Lcom/swarmconnect/ar;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    return-object v0
.end method


# virtual methods
.method public loginError(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    iget-object v0, v0, Lcom/swarmconnect/ar;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "Facebook Login Failed"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-virtual {v0}, Lcom/swarmconnect/ar;->finish()V

    return-void
.end method

.method public loginSuccess(Lcom/facebook/android/Facebook;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-virtual {v0}, Lcom/swarmconnect/ar;->b()V

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-static {v0}, Lcom/swarmconnect/ar;->a(Lcom/swarmconnect/ar;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/swarmconnect/w;

    invoke-direct {v0}, Lcom/swarmconnect/w;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/w;->authKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    iget-object v1, v1, Lcom/swarmconnect/ar;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-static {v1}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/w;->device:Ljava/lang/String;

    iget-object v1, p0, Lcom/swarmconnect/bg;->a:Lcom/swarmconnect/ar;

    invoke-static {v1}, Lcom/swarmconnect/ar;->b(Lcom/swarmconnect/ar;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/w;->username:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/bg$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bg$1;-><init>(Lcom/swarmconnect/bg;)V

    iput-object v1, v0, Lcom/swarmconnect/w;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/w;->run()V

    return-void
.end method
