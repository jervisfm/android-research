.class Lcom/swarmconnect/bi$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/bi$1;->gotAPI(Lcom/swarmconnect/APICall;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bi$1;

.field private final synthetic b:Lcom/swarmconnect/APICall;


# direct methods
.method constructor <init>(Lcom/swarmconnect/bi$1;Lcom/swarmconnect/APICall;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    iput-object p2, p0, Lcom/swarmconnect/bi$1$1;->b:Lcom/swarmconnect/APICall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v0}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/bi;->c()V

    iget-object v0, p0, Lcom/swarmconnect/bi$1$1;->b:Lcom/swarmconnect/APICall;

    check-cast v0, Lcom/swarmconnect/bu;

    iget-object v1, v0, Lcom/swarmconnect/bu;->user:Lcom/swarmconnect/SwarmUser;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/swarmconnect/bu;->user:Lcom/swarmconnect/SwarmUser;

    iget-object v0, v0, Lcom/swarmconnect/bu;->auth:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/swarmconnect/Swarm;->a(Lcom/swarmconnect/SwarmUser;Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/bi;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, Lcom/swarmconnect/bu;->usernameError:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Username: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/swarmconnect/bu;->usernameError:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v0}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/swarmconnect/bi;->show(I)V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/swarmconnect/bu;->passwordError:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->b(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->b(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, v0, Lcom/swarmconnect/bu;->passwordError:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, v0, Lcom/swarmconnect/bu;->confirmError:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->c(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->c(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, v0, Lcom/swarmconnect/bu;->confirmError:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v1, v0, Lcom/swarmconnect/bu;->emailError:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->d(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->d(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, v0, Lcom/swarmconnect/bu;->emailError:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->b(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->b(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->c(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v1}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/bi;->c(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v0}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/bi;->d(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bi$1$1;->a:Lcom/swarmconnect/bi$1;

    invoke-static {v0}, Lcom/swarmconnect/bi$1;->a(Lcom/swarmconnect/bi$1;)Lcom/swarmconnect/bi;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/bi;->d(Lcom/swarmconnect/bi;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
