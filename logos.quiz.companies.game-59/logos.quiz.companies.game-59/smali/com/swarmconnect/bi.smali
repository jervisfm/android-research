.class Lcom/swarmconnect/bi;
.super Lcom/swarmconnect/ba;


# static fields
.field public static username:Ljava/lang/String;


# instance fields
.field private m:Landroid/widget/EditText;

.field private n:Landroid/widget/EditText;

.field private o:Landroid/widget/EditText;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ba;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/bi;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/bi;->f()V

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/bi;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bi;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/bi;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bi;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/bi;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bi;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/swarmconnect/bi;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bi;->p:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/bi;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bi;->q:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/bi;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bi;->r:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private f()V
    .locals 5

    invoke-direct {p0}, Lcom/swarmconnect/bi;->e()V

    iget-object v0, p0, Lcom/swarmconnect/bi;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bi;->n:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/bi;->o:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/swarmconnect/bi;->b()V

    new-instance v3, Lcom/swarmconnect/bu;

    invoke-direct {v3}, Lcom/swarmconnect/bu;-><init>()V

    sget-object v4, Lcom/swarmconnect/bi;->username:Ljava/lang/String;

    iput-object v4, v3, Lcom/swarmconnect/bu;->username:Ljava/lang/String;

    iput-object v0, v3, Lcom/swarmconnect/bu;->password:Ljava/lang/String;

    iput-object v1, v3, Lcom/swarmconnect/bu;->confirm:Ljava/lang/String;

    iput-object v2, v3, Lcom/swarmconnect/bu;->email:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/bi$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/bi$1;-><init>(Lcom/swarmconnect/bi;)V

    iput-object v0, v3, Lcom/swarmconnect/bu;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v3}, Lcom/swarmconnect/bu;->run()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const v1, -0x70e5d3

    const-string v0, "@layout/swarm_create_account"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->b(I)V

    const-string v0, "@id/password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/bi;->m:Landroid/widget/EditText;

    const-string v0, "@id/confirm"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/bi;->n:Landroid/widget/EditText;

    const-string v0, "@id/email"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/bi;->o:Landroid/widget/EditText;

    const-string v0, "@id/password_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/bi;->p:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/bi;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "@id/confirm_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/bi;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/bi;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "@id/email_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/bi;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/bi;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->blueButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bi;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/bi$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bi$2;-><init>(Lcom/swarmconnect/bi;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/ba;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
