.class Lcom/swarmconnect/bj$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/bj;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bj;


# direct methods
.method constructor <init>(Lcom/swarmconnect/bj;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bj$2;->a:Lcom/swarmconnect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const-string v0, "TapjoyConnect"

    const-string v1, "starting connect call..."

    invoke-static {v0, v1}, Lcom/swarmconnect/bv;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/swarmconnect/bj;->getURLParams()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bj$2;->a:Lcom/swarmconnect/bj;

    invoke-static {v1}, Lcom/swarmconnect/bj;->b(Lcom/swarmconnect/bj;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bj$2;->a:Lcom/swarmconnect/bj;

    invoke-static {v1}, Lcom/swarmconnect/bj;->b(Lcom/swarmconnect/bj;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {}, Lcom/swarmconnect/bj;->c()Lcom/swarmconnect/cm;

    move-result-object v1

    const-string v2, "https://ws.tapjoyads.com/connect?"

    invoke-virtual {v1, v2, v0}, Lcom/swarmconnect/cm;->connectToURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/bj$2;->a:Lcom/swarmconnect/bj;

    invoke-static {v1, v0}, Lcom/swarmconnect/bj;->a(Lcom/swarmconnect/bj;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method
