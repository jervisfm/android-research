.class final Lcom/swarmconnect/bk;
.super Ljava/lang/Object;


# static fields
.field public static final TAPJOY_CONNECT:Ljava/lang/String; = "TapjoyConnect"

.field private static a:Lcom/swarmconnect/bk;

.field private static b:Lcom/swarmconnect/be;

.field private static c:Lcom/swarmconnect/am;

.field private static d:Lcom/swarmconnect/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/bk;->a:Lcom/swarmconnect/bk;

    sput-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    sput-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    sput-object v0, Lcom/swarmconnect/bk;->d:Lcom/swarmconnect/ag;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2, p3}, Lcom/swarmconnect/bj;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getTapjoyConnectInstance()Lcom/swarmconnect/bk;
    .locals 2

    sget-object v0, Lcom/swarmconnect/bk;->a:Lcom/swarmconnect/bk;

    if-nez v0, :cond_0

    const-string v0, "TapjoyConnect"

    const-string v1, "----------------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TapjoyConnect"

    const-string v1, "ERROR -- call requestTapjoyConnect before any other Tapjoy methods"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TapjoyConnect"

    const-string v1, "----------------------------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/swarmconnect/bk;->a:Lcom/swarmconnect/bk;

    return-object v0
.end method

.method public static requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/swarmconnect/bk;

    invoke-direct {v0, p0, p1, p2}, Lcom/swarmconnect/bk;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/swarmconnect/bk;->a:Lcom/swarmconnect/bk;

    new-instance v0, Lcom/swarmconnect/be;

    invoke-direct {v0, p0}, Lcom/swarmconnect/be;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    new-instance v0, Lcom/swarmconnect/am;

    invoke-direct {v0, p0}, Lcom/swarmconnect/am;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    new-instance v0, Lcom/swarmconnect/ag;

    invoke-direct {v0, p0}, Lcom/swarmconnect/ag;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/swarmconnect/bk;->d:Lcom/swarmconnect/ag;

    return-void
.end method


# virtual methods
.method public actionComplete(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getInstance()Lcom/swarmconnect/bj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swarmconnect/bj;->actionComplete(Ljava/lang/String;)V

    return-void
.end method

.method public awardTapPoints(ILcom/swarmconnect/cl;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0, p1, p2}, Lcom/swarmconnect/be;->awardTapPoints(ILcom/swarmconnect/cl;)V

    return-void
.end method

.method public enablePaidAppWithActionID(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getInstance()Lcom/swarmconnect/bj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swarmconnect/bj;->enablePaidAppWithActionID(Ljava/lang/String;)V

    return-void
.end method

.method public getAppID()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getAppID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrencyMultiplier()F
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getInstance()Lcom/swarmconnect/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/bj;->getCurrencyMultiplier()F

    move-result v0

    return v0
.end method

.method public getDisplayAd(Lcom/swarmconnect/by;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->d:Lcom/swarmconnect/ag;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/ag;->getDisplayAd(Lcom/swarmconnect/by;)V

    return-void
.end method

.method public getFeaturedApp(Lcom/swarmconnect/x;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/am;->getFeaturedApp(Lcom/swarmconnect/x;)V

    return-void
.end method

.method public getFeaturedAppWithCurrencyID(Ljava/lang/String;Lcom/swarmconnect/x;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    invoke-virtual {v0, p1, p2}, Lcom/swarmconnect/am;->getFeaturedApp(Ljava/lang/String;Lcom/swarmconnect/x;)V

    return-void
.end method

.method public getTapPoints(Lcom/swarmconnect/bc;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/be;->getTapPoints(Lcom/swarmconnect/bc;)V

    return-void
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getUserID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBannerAdSize(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->d:Lcom/swarmconnect/ag;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/ag;->setBannerAdSize(Ljava/lang/String;)V

    return-void
.end method

.method public setCurrencyMultiplier(F)V
    .locals 1

    invoke-static {}, Lcom/swarmconnect/bj;->getInstance()Lcom/swarmconnect/bj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swarmconnect/bj;->setCurrencyMultiplier(F)V

    return-void
.end method

.method public setEarnedPointsNotifier(Lcom/swarmconnect/cr;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/be;->setEarnedPointsNotifier(Lcom/swarmconnect/cr;)V

    return-void
.end method

.method public setFeaturedAppDisplayCount(I)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/am;->setDisplayCount(I)V

    return-void
.end method

.method public setUserID(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/swarmconnect/bj;->setUserID(Ljava/lang/String;)V

    return-void
.end method

.method public showFeaturedAppFullScreenAd()V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->c:Lcom/swarmconnect/am;

    invoke-virtual {v0}, Lcom/swarmconnect/am;->showFeaturedAppFullScreenAd()V

    return-void
.end method

.method public showOffers()V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0}, Lcom/swarmconnect/be;->showOffers()V

    return-void
.end method

.method public showOffersWithCurrencyID(Ljava/lang/String;Z)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0, p1, p2}, Lcom/swarmconnect/be;->showOffersWithCurrencyID(Ljava/lang/String;Z)V

    return-void
.end method

.method public spendTapPoints(ILcom/swarmconnect/f;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/bk;->b:Lcom/swarmconnect/be;

    invoke-virtual {v0, p1, p2}, Lcom/swarmconnect/be;->spendTapPoints(ILcom/swarmconnect/f;)V

    return-void
.end method
