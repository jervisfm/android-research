.class Lcom/swarmconnect/bx;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/APICall;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cj;

    const-string v2, "AddLeaderboardScoreAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cp;

    const-string v2, "AddVersionInfoAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cc;

    const-string v2, "ChangePasswordAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/ab;

    const-string v2, "CheckUsernameAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/aw;

    const-string v2, "ConsumeItemAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bu;

    const-string v2, "CreateAccountAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cn;

    const-string v2, "CreateGuestAccountAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bn;

    const-string v2, "DelMessageThreadAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/w;

    const-string v2, "FacebookLoginAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/br;

    const-string v2, "FriendRequestAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/p;

    const-string v2, "GetAchievementsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/l;

    const-string v2, "GetAppsListAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/aa;

    const-string v2, "GetChallengeAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/GetDeviceAccountsAPI;

    const-string v2, "GetDeviceAccountsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bt;

    const-string v2, "GetFriendNotificationAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/GetFriendsAPI;

    const-string v2, "GetFriendsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/GetInventoryAPI;

    const-string v2, "GetInventoryAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bp;

    const-string v2, "GetLeaderboardAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/b;

    const-string v2, "GetLeaderboardsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cq;

    const-string v2, "GetLeaderboardScoreAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bs;

    const-string v2, "GetLeaderboardScoreDataAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/aj;

    const-string v2, "GetMessagesAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bq;

    const-string v2, "GetMessageThreadsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bh;

    const-string v2, "GetNumNewMessagesAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bl;

    const-string v2, "GetPushDataAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/cf;

    const-string v2, "GetStoreAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bo;

    const-string v2, "GetUserCoinsAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/co;

    const-string v2, "GetUserDataAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/z;

    const-string v2, "GetUserStatusAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/ce;

    const-string v2, "LoginAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/n;

    const-string v2, "MessageThreadViewedAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/h;

    const-string v2, "ModChallengeAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/ci;

    const-string v2, "NotificationCheckAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/bd;

    const-string v2, "PurchaseItemAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/v;

    const-string v2, "ResetPasswordAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/an;

    const-string v2, "SendChallengeAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/ay;

    const-string v2, "SendMessageAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/as;

    const-string v2, "SetUserDataAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/y;

    const-string v2, "UnlockAchievementAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/c;

    const-string v2, "UpdateStatusAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/o;

    const-string v2, "UpgradeGuestAccountAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    const-class v1, Lcom/swarmconnect/a;

    const-string v2, "UserSearchAPI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/swarmconnect/APICall;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    sget-object v0, Lcom/swarmconnect/bx;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
