.class Lcom/swarmconnect/bz$5;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/bz;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bz;


# direct methods
.method constructor <init>(Lcom/swarmconnect/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 2

    check-cast p1, Lcom/swarmconnect/GetDeviceAccountsAPI;

    iget-object v0, p1, Lcom/swarmconnect/GetDeviceAccountsAPI;->users:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/swarmconnect/GetDeviceAccountsAPI;->users:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p1, Lcom/swarmconnect/GetDeviceAccountsAPI;->users:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->c(Lcom/swarmconnect/bz;)Lcom/swarmconnect/bz$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/bz$a;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->b(Lcom/swarmconnect/bz;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmUser;

    iget-object v1, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-static {v1}, Lcom/swarmconnect/bz;->b(Lcom/swarmconnect/bz;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v0, v0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-virtual {v0}, Lcom/swarmconnect/bz;->c()V

    return-void
.end method

.method public requestFailed()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz$5;->a:Lcom/swarmconnect/bz;

    invoke-virtual {v0}, Lcom/swarmconnect/bz;->c()V

    return-void
.end method
