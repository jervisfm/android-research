.class Lcom/swarmconnect/bz$6;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/bz;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bz;


# direct methods
.method constructor <init>(Lcom/swarmconnect/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->c(Lcom/swarmconnect/bz;)Lcom/swarmconnect/bz$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/swarmconnect/bz$a;->getItem(I)Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->provider:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->provider:Ljava/lang/String;

    const-string v2, "facebook"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    iget-object v0, v0, Lcom/swarmconnect/bz;->c:Lcom/swarmconnect/SwarmMainActivity;

    iget-object v1, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    iget-object v1, v1, Lcom/swarmconnect/bz;->l:Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;

    invoke-static {v0, v1}, Lcom/swarmconnect/SwarmFacebook;->login(Landroid/app/Activity;Lcom/swarmconnect/SwarmFacebook$FacebookLoginCB;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    invoke-static {v1}, Lcom/swarmconnect/bz;->b(Lcom/swarmconnect/bz;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v0, v0, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/bz$6;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->e(Lcom/swarmconnect/bz;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method
