.class Lcom/swarmconnect/bz$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/bz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/bz;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/bz;Lcom/swarmconnect/bz$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/bz$a;-><init>(Lcom/swarmconnect/bz;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/bz$a;->getItem(I)Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-virtual {v0}, Lcom/swarmconnect/bz;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v2, "@layout/swarm_login_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    invoke-static {v0}, Lcom/swarmconnect/bz;->b(Lcom/swarmconnect/bz;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/swarmconnect/bz$a;->getItem(I)Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v1, "@id/row"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v4, "@id/username"

    invoke-virtual {v1, v4}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v4, v3, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v4, "@id/points"

    invoke-virtual {v1, v4}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v3, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->points:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v3, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->provider:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v3, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->provider:Ljava/lang/String;

    const-string v4, "facebook"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v4, "@id/external_provider"

    invoke-virtual {v1, v4}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v5, "@drawable/swarm_facebook_icon"

    invoke-virtual {v4, v5}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v4, "@id/external_provider"

    invoke-virtual {v1, v4}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    if-eqz v2, :cond_1

    iget-object v1, v3, Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-object p2

    :cond_2
    iget-object v1, p0, Lcom/swarmconnect/bz$a;->a:Lcom/swarmconnect/bz;

    const-string v4, "@id/external_provider"

    invoke-virtual {v1, v4}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
