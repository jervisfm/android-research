.class Lcom/swarmconnect/bz;
.super Lcom/swarmconnect/ba;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/bz$a;
    }
.end annotation


# static fields
.field public static loginUsername:Ljava/lang/String;


# instance fields
.field private m:Landroid/widget/ListView;

.field private n:Lcom/swarmconnect/bz$a;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/EditText;

.field private q:Landroid/widget/EditText;

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/swarmconnect/GetDeviceAccountsAPI$ExternalSwarmUser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/ba;-><init>()V

    new-instance v0, Lcom/swarmconnect/bz$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/bz$a;-><init>(Lcom/swarmconnect/bz;Lcom/swarmconnect/bz$a;)V

    iput-object v0, p0, Lcom/swarmconnect/bz;->n:Lcom/swarmconnect/bz$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/bz;->r:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/bz;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/bz;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/bz;)Lcom/swarmconnect/bz$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz;->n:Lcom/swarmconnect/bz$a;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/bz;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/bz;->e()V

    return-void
.end method

.method static synthetic e(Lcom/swarmconnect/bz;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz;->q:Landroid/widget/EditText;

    return-object v0
.end method

.method private e()V
    .locals 4

    invoke-virtual {p0}, Lcom/swarmconnect/bz;->b()V

    iget-object v0, p0, Lcom/swarmconnect/bz;->o:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/bz;->o:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/bz;->q:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/swarmconnect/ce;

    invoke-direct {v2}, Lcom/swarmconnect/ce;-><init>()V

    sget-object v3, Lcom/swarmconnect/Swarm;->e:Ljava/lang/String;

    iput-object v3, v2, Lcom/swarmconnect/ce;->device:Ljava/lang/String;

    iput-object v0, v2, Lcom/swarmconnect/ce;->username:Ljava/lang/String;

    iput-object v1, v2, Lcom/swarmconnect/ce;->password:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/bz$4;

    invoke-direct {v0, p0}, Lcom/swarmconnect/bz$4;-><init>(Lcom/swarmconnect/bz;)V

    iput-object v0, v2, Lcom/swarmconnect/ce;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v2}, Lcom/swarmconnect/ce;->run()V

    return-void
.end method

.method static synthetic f(Lcom/swarmconnect/bz;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/bz;->o:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_login"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->b(I)V

    const-string v0, "@id/username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    new-instance v1, Lcom/swarmconnect/bz$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$3;-><init>(Lcom/swarmconnect/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const-string v0, "@id/password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/bz;->q:Landroid/widget/EditText;

    const-string v0, "@id/username_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/bz;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/bz;->o:Landroid/widget/TextView;

    const v1, -0x70e5d3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "@id/login"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->blueButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/login"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/bz$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$2;-><init>(Lcom/swarmconnect/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/bz$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$1;-><init>(Lcom/swarmconnect/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/lost_password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/bz$7;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$7;-><init>(Lcom/swarmconnect/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/bz;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/swarmconnect/bz;->m:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/swarmconnect/bz;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/bz;->n:Lcom/swarmconnect/bz$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/swarmconnect/bz;->m:Landroid/widget/ListView;

    new-instance v1, Lcom/swarmconnect/bz$6;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$6;-><init>(Lcom/swarmconnect/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/ba;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/swarmconnect/ba;->onResume()V

    invoke-virtual {p0}, Lcom/swarmconnect/bz;->b()V

    new-instance v0, Lcom/swarmconnect/GetDeviceAccountsAPI;

    invoke-direct {v0}, Lcom/swarmconnect/GetDeviceAccountsAPI;-><init>()V

    iget-object v1, p0, Lcom/swarmconnect/bz;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-static {v1}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI;->device:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/bz$5;

    invoke-direct {v1, p0}, Lcom/swarmconnect/bz$5;-><init>(Lcom/swarmconnect/bz;)V

    iput-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/GetDeviceAccountsAPI;->run()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->getLastLogin()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/swarmconnect/bz;->loginUsername:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/bz;->loginUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/bz;->p:Landroid/widget/EditText;

    sget-object v1, Lcom/swarmconnect/bz;->loginUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    sput-object v0, Lcom/swarmconnect/bz;->loginUsername:Ljava/lang/String;

    iget-object v0, p0, Lcom/swarmconnect/bz;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
