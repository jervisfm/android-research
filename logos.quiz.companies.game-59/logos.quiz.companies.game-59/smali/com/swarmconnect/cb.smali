.class Lcom/swarmconnect/cb;
.super Lcom/swarmconnect/ba;


# static fields
.field public static allowGuestAccounts:Z

.field public static checkExistingAccounts:Z


# instance fields
.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/EditText;

.field private o:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/swarmconnect/cb;->allowGuestAccounts:Z

    sput-boolean v0, Lcom/swarmconnect/cb;->checkExistingAccounts:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ba;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/cb;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/cb;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/cb;->e()V

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/cb;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/cb;->b()V

    new-instance v0, Lcom/swarmconnect/GetDeviceAccountsAPI;

    invoke-direct {v0}, Lcom/swarmconnect/GetDeviceAccountsAPI;-><init>()V

    iget-object v1, p0, Lcom/swarmconnect/cb;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-static {v1}, Lcom/swarmconnect/utils/DeviceUtils;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI;->device:Ljava/lang/String;

    new-instance v1, Lcom/swarmconnect/cb$8;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$8;-><init>(Lcom/swarmconnect/cb;)V

    iput-object v1, v0, Lcom/swarmconnect/GetDeviceAccountsAPI;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/GetDeviceAccountsAPI;->run()V

    return-void
.end method


# virtual methods
.method public createAccount()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/swarmconnect/cb;->b()V

    new-instance v1, Lcom/swarmconnect/ab;

    invoke-direct {v1}, Lcom/swarmconnect/ab;-><init>()V

    iput-object v0, v1, Lcom/swarmconnect/ab;->username:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/cb$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/cb$2;-><init>(Lcom/swarmconnect/cb;)V

    iput-object v0, v1, Lcom/swarmconnect/ab;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1}, Lcom/swarmconnect/ab;->run()V

    return-void
.end method

.method public createGuestAccount()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/swarmconnect/cb;->b()V

    new-instance v0, Lcom/swarmconnect/cn;

    invoke-direct {v0}, Lcom/swarmconnect/cn;-><init>()V

    new-instance v1, Lcom/swarmconnect/cb$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$1;-><init>(Lcom/swarmconnect/cb;)V

    iput-object v1, v0, Lcom/swarmconnect/cn;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/cn;->run()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const-string v0, ""

    :try_start_0
    iget-object v1, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/swarmconnect/cb;->onCreate(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_select_username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->b(I)V

    const-string v0, "@id/terms"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/cb$5;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$5;-><init>(Lcom/swarmconnect/cb;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/cb;->n:Landroid/widget/EditText;

    const-string v0, "@id/create"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/cb;->m:Landroid/widget/Button;

    iget-object v0, p0, Lcom/swarmconnect/cb;->m:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/cb;->m:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/cb$6;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$6;-><init>(Lcom/swarmconnect/cb;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/existing"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/existing"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/cb$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$3;-><init>(Lcom/swarmconnect/cb;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v0, Lcom/swarmconnect/cb;->allowGuestAccounts:Z

    if-eqz v0, :cond_0

    const-string v0, "@id/guest"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->blueButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/guest"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/cb$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/cb$4;-><init>(Lcom/swarmconnect/cb;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const-string v0, "@id/username_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/cb;->o:Landroid/widget/TextView;

    const v1, -0x70e5d3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-super {p0, p1}, Lcom/swarmconnect/ba;->onCreate(Landroid/os/Bundle;)V

    return-void

    :cond_0
    const-string v0, "@id/guest"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/cb;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/swarmconnect/ba;->onResume()V

    new-instance v0, Lcom/swarmconnect/cb$7;

    invoke-direct {v0, p0}, Lcom/swarmconnect/cb$7;-><init>(Lcom/swarmconnect/cb;)V

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->autoLogin(Lcom/swarmconnect/Swarm$a;)V

    return-void
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
