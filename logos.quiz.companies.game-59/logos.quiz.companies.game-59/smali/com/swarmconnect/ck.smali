.class Lcom/swarmconnect/ck;
.super Lcom/swarmconnect/u;


# instance fields
.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/16 v2, 0x8

    const-string v0, "@layout/swarm_get_coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->b(I)V

    const-string v0, "@id/paypal"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/ck;->l:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/swarmconnect/ck;->l:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->coinsProviderButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/tapjoy"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/ck;->m:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/swarmconnect/ck;->m:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->coinsProviderButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/zong"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/ck;->n:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/swarmconnect/ck;->n:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->coinsProviderButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/ck;->l:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/swarmconnect/ck$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ck$2;-><init>(Lcom/swarmconnect/ck;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/swarmconnect/ck;->m:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/swarmconnect/ck$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/ck$1;-><init>(Lcom/swarmconnect/ck;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v0, Lcom/swarmconnect/u;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ck;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/ck;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_coin_small"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ck;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Select a coin provider"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/ck;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
