.class Lcom/swarmconnect/d$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/d;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/d;


# direct methods
.method constructor <init>(Lcom/swarmconnect/d;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    invoke-static {v0}, Lcom/swarmconnect/d;->a(Lcom/swarmconnect/d;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    invoke-static {v0}, Lcom/swarmconnect/d;->a(Lcom/swarmconnect/d;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/utils/Text;->isEmailValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    invoke-static {v0}, Lcom/swarmconnect/d;->b(Lcom/swarmconnect/d;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    iget-object v0, v0, Lcom/swarmconnect/d;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "Please enter a valid email address"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/d$2;->a:Lcom/swarmconnect/d;

    iget-object v0, v0, Lcom/swarmconnect/d;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "Please enter your email address"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
