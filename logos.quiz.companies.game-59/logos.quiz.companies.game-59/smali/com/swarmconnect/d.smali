.class Lcom/swarmconnect/d;
.super Lcom/swarmconnect/ba;


# instance fields
.field private m:Landroid/widget/EditText;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/ba;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/d;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/d;->m:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/d;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/d;->e()V

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/d;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/d;->n:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/d;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/d;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/d;->b()V

    iget-object v0, p0, Lcom/swarmconnect/d;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/swarmconnect/v;

    invoke-direct {v1}, Lcom/swarmconnect/v;-><init>()V

    iput-object v0, v1, Lcom/swarmconnect/v;->email:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/d$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/d$1;-><init>(Lcom/swarmconnect/d;)V

    iput-object v0, v1, Lcom/swarmconnect/v;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v1}, Lcom/swarmconnect/v;->run()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_lost_password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->b(I)V

    const-string v0, "@id/email"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/d;->m:Landroid/widget/EditText;

    const-string v0, "@id/success"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/d;->o:Landroid/widget/TextView;

    const-string v0, "@id/submit"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/d;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/d;->n:Landroid/widget/Button;

    iget-object v0, p0, Lcom/swarmconnect/d;->n:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->blueButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/d;->n:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/d$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/d$2;-><init>(Lcom/swarmconnect/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/ba;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/swarmconnect/ba;->onResume()V

    iget-object v0, p0, Lcom/swarmconnect/d;->n:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/d;->o:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
