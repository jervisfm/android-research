.class Lcom/swarmconnect/g$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/g;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/g;


# direct methods
.method constructor <init>(Lcom/swarmconnect/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/g$1;)Lcom/swarmconnect/g;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    return-object v0
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->b(Lcom/swarmconnect/g;)Lcom/swarmconnect/g$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/swarmconnect/g$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessageThread;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    invoke-static {v1}, Lcom/swarmconnect/g;->c(Lcom/swarmconnect/g;)V

    iget-object v1, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    iget-object v3, v3, Lcom/swarmconnect/g;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v3, "Delete Thread?"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Delete messages from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    iget-object v4, v4, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Delete"

    new-instance v4, Lcom/swarmconnect/g$1$1;

    invoke-direct {v4, p0, v0}, Lcom/swarmconnect/g$1$1;-><init>(Lcom/swarmconnect/g$1;Lcom/swarmconnect/SwarmMessageThread;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "Cancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/swarmconnect/g;->a(Lcom/swarmconnect/g;Landroid/app/AlertDialog;)V

    iget-object v0, p0, Lcom/swarmconnect/g$1;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->d(Lcom/swarmconnect/g;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
