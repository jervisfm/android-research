.class Lcom/swarmconnect/g$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/g;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/g;


# direct methods
.method constructor <init>(Lcom/swarmconnect/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/g$4;->a:Lcom/swarmconnect/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/g$4;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->b(Lcom/swarmconnect/g;)Lcom/swarmconnect/g$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/swarmconnect/g$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessageThread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMessageThread;->markRead()V

    iget-object v1, p0, Lcom/swarmconnect/g$4;->a:Lcom/swarmconnect/g;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/swarmconnect/g;->c(I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "thread"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/swarmconnect/g$4;->a:Lcom/swarmconnect/g;

    iget-object v0, v0, Lcom/swarmconnect/g;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmMainActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
