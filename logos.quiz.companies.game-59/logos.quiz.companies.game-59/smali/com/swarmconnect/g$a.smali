.class Lcom/swarmconnect/g$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/g;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/g;Lcom/swarmconnect/g$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/g$a;-><init>(Lcom/swarmconnect/g;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->a(Lcom/swarmconnect/g;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->a(Lcom/swarmconnect/g;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    if-ltz p1, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->a(Lcom/swarmconnect/g;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-static {v0}, Lcom/swarmconnect/g;->a(Lcom/swarmconnect/g;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    invoke-virtual {v0}, Lcom/swarmconnect/g;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v2, "@layout/swarm_inbox_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    const v0, 0xffffff

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0, p1}, Lcom/swarmconnect/g$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessageThread;

    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lcom/swarmconnect/SwarmMessageThread;->viewed:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v2, "@id/mail"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v3, "@drawable/swarm_mail_grey"

    invoke-virtual {v2, v3}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget-object v2, v0, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    iget-object v1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v3, "@id/username"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v2, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v2, "@id/message"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v0, v0, Lcom/swarmconnect/SwarmMessageThread;->lastMessage:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2

    :cond_2
    const v0, -0x77000001

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v2, "@id/mail"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/swarmconnect/g$a;->a:Lcom/swarmconnect/g;

    const-string v3, "@drawable/swarm_mail"

    invoke-virtual {v2, v3}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
