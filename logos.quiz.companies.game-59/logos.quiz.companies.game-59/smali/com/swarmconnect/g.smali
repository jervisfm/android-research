.class Lcom/swarmconnect/g;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/g$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/g$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmMessageThread;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/widget/TextView;

.field private o:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/g$a;-><init>(Lcom/swarmconnect/g;Lcom/swarmconnect/g$a;)V

    iput-object v0, p0, Lcom/swarmconnect/g;->l:Lcom/swarmconnect/g$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/g;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/g;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/g;Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/g;->o:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/g;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/g;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/g;)Lcom/swarmconnect/g$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g;->l:Lcom/swarmconnect/g$a;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/g;->e()V

    return-void
.end method

.method static synthetic d(Lcom/swarmconnect/g;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g;->o:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic e(Lcom/swarmconnect/g;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/g;->o:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/g;->o:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/g;->o:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method


# virtual methods
.method public getThreads()V
    .locals 1

    invoke-virtual {p0}, Lcom/swarmconnect/g;->b()V

    new-instance v0, Lcom/swarmconnect/g$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/g$3;-><init>(Lcom/swarmconnect/g;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmMessageThread;->getAllThreads(Lcom/swarmconnect/SwarmMessageThread$GotThreadsCB;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->b(I)V

    const-string v0, "@id/empty_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/g;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/swarmconnect/g;->n:Landroid/widget/TextView;

    const-string v1, "\n\n\nYou don\'t have any Swarm messages yet!  Make some friends and start chatting."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/g;->l:Lcom/swarmconnect/g$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/g$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/g$4;-><init>(Lcom/swarmconnect/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v1, Lcom/swarmconnect/g$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/g$1;-><init>(Lcom/swarmconnect/g;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_messages"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Message Inbox"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/g;->a(ILjava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/swarmconnect/u;->onPause()V

    invoke-direct {p0}, Lcom/swarmconnect/g;->e()V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/g$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/g$2;-><init>(Lcom/swarmconnect/g;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/g;->a(Ljava/lang/Runnable;)V

    return-void
.end method
