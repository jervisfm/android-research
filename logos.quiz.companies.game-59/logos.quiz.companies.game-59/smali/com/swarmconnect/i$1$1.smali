.class Lcom/swarmconnect/i$1$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/i$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/i$1;


# direct methods
.method constructor <init>(Lcom/swarmconnect/i$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p1, Lcom/swarmconnect/cc;

    iget-boolean v0, p1, Lcom/swarmconnect/cc;->ok:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    iget-object v0, v0, Lcom/swarmconnect/i;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-string v1, "Password changed"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/i;->a(Lcom/swarmconnect/i;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/i;->b(Lcom/swarmconnect/i;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/i;->c()V

    return-void

    :cond_0
    iget-object v0, p1, Lcom/swarmconnect/cc;->error:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/swarmconnect/cc;->error:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/i;->f(Lcom/swarmconnect/i;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p1, Lcom/swarmconnect/cc;->error:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/i;->f(Lcom/swarmconnect/i;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Error saving new password."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public requestFailed()V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/i;->f(Lcom/swarmconnect/i;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Error saving new password."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/i$1$1;->a:Lcom/swarmconnect/i$1;

    invoke-static {v0}, Lcom/swarmconnect/i$1;->a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/i;->c()V

    return-void
.end method
