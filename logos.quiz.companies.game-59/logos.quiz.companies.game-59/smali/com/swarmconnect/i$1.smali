.class Lcom/swarmconnect/i$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/i;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/i;


# direct methods
.method constructor <init>(Lcom/swarmconnect/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/i$1;)Lcom/swarmconnect/i;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-static {v0}, Lcom/swarmconnect/i;->c(Lcom/swarmconnect/i;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-static {v1}, Lcom/swarmconnect/i;->d(Lcom/swarmconnect/i;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-static {v2}, Lcom/swarmconnect/i;->e(Lcom/swarmconnect/i;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-virtual {v2}, Lcom/swarmconnect/i;->b()V

    new-instance v2, Lcom/swarmconnect/cc;

    invoke-direct {v2}, Lcom/swarmconnect/cc;-><init>()V

    iput-object v0, v2, Lcom/swarmconnect/cc;->currentPass:Ljava/lang/String;

    iput-object v1, v2, Lcom/swarmconnect/cc;->newPass:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/i$1$1;

    invoke-direct {v0, p0}, Lcom/swarmconnect/i$1$1;-><init>(Lcom/swarmconnect/i$1;)V

    iput-object v0, v2, Lcom/swarmconnect/cc;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v2}, Lcom/swarmconnect/cc;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-static {v0}, Lcom/swarmconnect/i;->f(Lcom/swarmconnect/i;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "New passwords do not match."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/i$1;->a:Lcom/swarmconnect/i;

    invoke-static {v0}, Lcom/swarmconnect/i;->f(Lcom/swarmconnect/i;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "All fields are required."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
