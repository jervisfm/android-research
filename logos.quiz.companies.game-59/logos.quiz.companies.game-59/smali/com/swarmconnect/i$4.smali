.class Lcom/swarmconnect/i$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/i;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/i;


# direct methods
.method constructor <init>(Lcom/swarmconnect/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/i$4;->a:Lcom/swarmconnect/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/swarmconnect/SwarmIO;->enablePush()V

    :goto_0
    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    const-string v1, "pushEnabled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/swarmconnect/SwarmActiveUser;->saveCloudData(Ljava/lang/String;Ljava/lang/String;Lcom/swarmconnect/SwarmActiveUser$SaveCloudDataCB;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/swarmconnect/SwarmIO;->disablePush()V

    goto :goto_0
.end method
