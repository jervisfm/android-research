.class Lcom/swarmconnect/i;
.super Lcom/swarmconnect/u;


# instance fields
.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/LinearLayout;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/CheckBox;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/EditText;

.field private v:Landroid/widget/EditText;

.field private w:Landroid/widget/EditText;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/i;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->o:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/i;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->n:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/i;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->u:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/i;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->v:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lcom/swarmconnect/i;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->w:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic f(Lcom/swarmconnect/i;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/i;->t:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_settings"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->b(I)V

    const-string v0, "@id/username"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/i;->l:Landroid/widget/TextView;

    const-string v0, "@id/upgrade_box"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/i;->m:Landroid/widget/LinearLayout;

    const-string v0, "@id/password_box"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/i;->n:Landroid/widget/LinearLayout;

    const-string v0, "@id/set_password_box"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/swarmconnect/i;->o:Landroid/widget/LinearLayout;

    const-string v0, "@id/upgrade"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/i;->p:Landroid/widget/Button;

    const-string v0, "@id/change_pass"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/i;->q:Landroid/widget/Button;

    const-string v0, "@id/set_pass"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/swarmconnect/i;->r:Landroid/widget/Button;

    const-string v0, "@id/password_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/i;->t:Landroid/widget/TextView;

    const-string v0, "@id/current_password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/i;->u:Landroid/widget/EditText;

    const-string v0, "@id/new_password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/i;->v:Landroid/widget/EditText;

    const-string v0, "@id/confirm_password"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/swarmconnect/i;->w:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/swarmconnect/i;->t:Landroid/widget/TextView;

    const v1, -0x70e5d3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "@id/notifications"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/swarmconnect/i;->s:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/swarmconnect/i;->s:Landroid/widget/CheckBox;

    new-instance v1, Lcom/swarmconnect/i$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/i$4;-><init>(Lcom/swarmconnect/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->p:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->p:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/i$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/i$3;-><init>(Lcom/swarmconnect/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->q:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->q:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/i$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/i$2;-><init>(Lcom/swarmconnect/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->r:Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/swarmconnect/i;->r:Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/i$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/i$1;-><init>(Lcom/swarmconnect/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/logout"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/logout"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/swarmconnect/i$5;

    invoke-direct {v1, p0}, Lcom/swarmconnect/i$5;-><init>(Lcom/swarmconnect/i;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_settings"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/i;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Settings"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/i;->a(ILjava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-super {p0}, Lcom/swarmconnect/u;->onResume()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/i;->l:Landroid/widget/TextView;

    sget-object v3, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget-object v3, v3, Lcom/swarmconnect/SwarmActiveUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/swarmconnect/i;->m:Landroid/widget/LinearLayout;

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmActiveUser;->isGuestAccount()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/i;->n:Landroid/widget/LinearLayout;

    sget-object v3, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    invoke-virtual {v3}, Lcom/swarmconnect/SwarmActiveUser;->isGuestAccount()Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/i;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/i;->s:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/swarmconnect/i;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-static {v1}, Lcom/swarmconnect/SwarmIO;->getPushEnabled(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method protected reload()V
    .locals 0

    return-void
.end method
