.class Lcom/swarmconnect/k$1;
.super Lcom/swarmconnect/APICall$APICallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/k;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/k;


# direct methods
.method constructor <init>(Lcom/swarmconnect/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-direct {p0}, Lcom/swarmconnect/APICall$APICallback;-><init>()V

    return-void
.end method


# virtual methods
.method public gotAPI(Lcom/swarmconnect/APICall;)V
    .locals 3

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-virtual {v0}, Lcom/swarmconnect/k;->c()V

    check-cast p1, Lcom/swarmconnect/b;

    iget-object v0, p1, Lcom/swarmconnect/b;->leaderboards:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    iget-object v1, p1, Lcom/swarmconnect/b;->leaderboards:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/swarmconnect/k;->a(Lcom/swarmconnect/k;Ljava/util/List;)V

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-static {v0}, Lcom/swarmconnect/k;->b(Lcom/swarmconnect/k;)Lcom/swarmconnect/k$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/k$a;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-virtual {v0}, Lcom/swarmconnect/k;->c()V

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-static {v0}, Lcom/swarmconnect/k;->a(Lcom/swarmconnect/k;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-static {v0}, Lcom/swarmconnect/k;->a(Lcom/swarmconnect/k;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-static {v0}, Lcom/swarmconnect/k;->a(Lcom/swarmconnect/k;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmLeaderboard;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/k;->a(Lcom/swarmconnect/SwarmLeaderboard;)V

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-virtual {v0}, Lcom/swarmconnect/k;->finish()V

    :cond_0
    return-void
.end method

.method public requestFailed()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/k$1;->a:Lcom/swarmconnect/k;

    invoke-virtual {v0}, Lcom/swarmconnect/k;->c()V

    return-void
.end method
