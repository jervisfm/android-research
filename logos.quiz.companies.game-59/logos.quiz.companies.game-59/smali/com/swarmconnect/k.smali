.class Lcom/swarmconnect/k;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/k$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/k$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmLeaderboard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/k$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/k$a;-><init>(Lcom/swarmconnect/k;Lcom/swarmconnect/k$a;)V

    iput-object v0, p0, Lcom/swarmconnect/k;->l:Lcom/swarmconnect/k$a;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/k;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/k;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/k;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/k;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/k;)Lcom/swarmconnect/k$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/k;->l:Lcom/swarmconnect/k$a;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/k;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/k;->b()V

    new-instance v0, Lcom/swarmconnect/b;

    invoke-direct {v0}, Lcom/swarmconnect/b;-><init>()V

    new-instance v1, Lcom/swarmconnect/k$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/k$1;-><init>(Lcom/swarmconnect/k;)V

    iput-object v1, v0, Lcom/swarmconnect/b;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/b;->run()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/swarmconnect/SwarmLeaderboard;)V
    .locals 2

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->c(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "leaderboard"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/swarmconnect/k;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmMainActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->b(I)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/k;->l:Lcom/swarmconnect/k$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/k$2;

    invoke-direct {v1, p0}, Lcom/swarmconnect/k$2;-><init>(Lcom/swarmconnect/k;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_leaderboards"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Leaderboards"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/k;->a(ILjava/lang/String;)V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/k$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/k$3;-><init>(Lcom/swarmconnect/k;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/k;->a(Ljava/lang/Runnable;)V

    return-void
.end method
