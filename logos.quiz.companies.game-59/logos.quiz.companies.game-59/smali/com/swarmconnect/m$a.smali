.class Lcom/swarmconnect/m$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/m;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/m;Lcom/swarmconnect/m$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/m$a;-><init>(Lcom/swarmconnect/m;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/SwarmStoreListing;
    .locals 1

    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreCategory;->listings:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmStoreListing;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/m$a;->getItem(I)Lcom/swarmconnect/SwarmStoreListing;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    invoke-virtual {v0}, Lcom/swarmconnect/m;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@layout/swarm_store_category_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/m$a;->getItem(I)Lcom/swarmconnect/SwarmStoreListing;

    move-result-object v1

    if-eqz v1, :cond_1

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    const v0, 0xffffff

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/pic"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/ui/AsyncImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/ui/AsyncImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/pic"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/ui/AsyncImageView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmStoreListing;->imageUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/ui/AsyncImageView;->getUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/name"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmStoreListing;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreItem;->description:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget-object v0, v0, Lcom/swarmconnect/SwarmStoreItem;->description:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/description"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/description"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget-object v2, v2, Lcom/swarmconnect/SwarmStoreItem;->description:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    invoke-static {v0}, Lcom/swarmconnect/m;->a(Lcom/swarmconnect/m;)Lcom/swarmconnect/SwarmUserInventory;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    invoke-static {v0}, Lcom/swarmconnect/m;->a(Lcom/swarmconnect/m;)Lcom/swarmconnect/SwarmUserInventory;

    move-result-object v0

    iget-object v2, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/SwarmUserInventory;->containsItem(Lcom/swarmconnect/SwarmStoreItem;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/swarmconnect/SwarmStoreListing;->item:Lcom/swarmconnect/SwarmStoreItem;

    iget-boolean v0, v0, Lcom/swarmconnect/SwarmStoreItem;->consumable:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v1, "@id/coins"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v1, "@id/price"

    invoke-virtual {v0, v1}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_2
    return-object p2

    :cond_2
    const v0, -0x77000001

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/description"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/coins"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/m$a;->a:Lcom/swarmconnect/m;

    const-string v2, "@id/price"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v1, Lcom/swarmconnect/SwarmStoreListing;->price:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
