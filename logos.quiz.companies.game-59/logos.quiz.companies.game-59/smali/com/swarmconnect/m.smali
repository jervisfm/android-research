.class Lcom/swarmconnect/m;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/m$a;
    }
.end annotation


# static fields
.field public static category:Lcom/swarmconnect/SwarmStoreCategory;


# instance fields
.field private l:Lcom/swarmconnect/m$a;

.field private m:Lcom/swarmconnect/SwarmUserInventory;

.field private n:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/m;->category:Lcom/swarmconnect/SwarmStoreCategory;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/m$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/m$a;-><init>(Lcom/swarmconnect/m;Lcom/swarmconnect/m$a;)V

    iput-object v0, p0, Lcom/swarmconnect/m;->l:Lcom/swarmconnect/m$a;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/m;)Lcom/swarmconnect/SwarmUserInventory;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/m;->m:Lcom/swarmconnect/SwarmUserInventory;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/m;Lcom/swarmconnect/SwarmUserInventory;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/m;->m:Lcom/swarmconnect/SwarmUserInventory;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/m;)Lcom/swarmconnect/m$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/m;->l:Lcom/swarmconnect/m$a;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/m;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/m;->n:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_store"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->b(I)V

    const-string v0, "@id/coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/m;->n:Landroid/widget/TextView;

    const-string v0, "@id/get_coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/get_coins"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Lcom/swarmconnect/m$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/m$1;-><init>(Lcom/swarmconnect/m;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/m;->l:Lcom/swarmconnect/m$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/m$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/m$3;-><init>(Lcom/swarmconnect/m;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_store"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Store"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/m;->a(ILjava/lang/String;)V

    return-void
.end method

.method protected reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/m$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/m$2;-><init>(Lcom/swarmconnect/m;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/m;->a(Ljava/lang/Runnable;)V

    return-void
.end method
