.class public Lcom/swarmconnect/network/Connection;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/io/InputStream;

.field private b:Ljava/io/OutputStream;

.field private c:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/network/Connection;->a:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/swarmconnect/network/Connection;->c:[B

    iput-object p1, p0, Lcom/swarmconnect/network/Connection;->a:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public receiveMessage()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v5, -0x1

    move v0, v1

    :goto_0
    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/Connection;->c:[B

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/swarmconnect/network/Connection;->c:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    const/16 v1, 0x4000

    if-le v0, v1, :cond_2

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Packet too large: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v2, p0, Lcom/swarmconnect/network/Connection;->a:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/swarmconnect/network/Connection;->c:[B

    rsub-int/lit8 v4, v0, 0x2

    invoke-virtual {v2, v3, v0, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-ne v2, v5, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "End of Stream (in header)"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    add-int/2addr v0, v2

    goto :goto_0

    :cond_2
    new-array v1, v0, [B

    :goto_1
    if-gtz v0, :cond_3

    return-object v1

    :cond_3
    iget-object v2, p0, Lcom/swarmconnect/network/Connection;->a:Ljava/io/InputStream;

    array-length v3, v1

    sub-int/2addr v3, v0

    invoke-virtual {v2, v1, v3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-ne v2, v5, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "End of Stream (in packet)"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    sub-int/2addr v0, v2

    goto :goto_1
.end method

.method public declared-synchronized sendMessage([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    array-length v1, p1

    shr-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    iget-object v0, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    array-length v1, p1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    iget-object v0, p0, Lcom/swarmconnect/network/Connection;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
