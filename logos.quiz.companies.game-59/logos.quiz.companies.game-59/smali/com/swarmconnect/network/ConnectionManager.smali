.class public Lcom/swarmconnect/network/ConnectionManager;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/swarmconnect/network/NetworkHandler;

.field private b:Ljava/lang/String;

.field private c:I

.field public connected:Z

.field private final d:Ljava/net/Socket;

.field private e:Ljava/lang/Object;

.field private f:Z

.field private g:Ljava/lang/Throwable;

.field private h:Lcom/swarmconnect/network/Connection;

.field private i:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcom/swarmconnect/network/NetworkHandler;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/swarmconnect/network/ConnectionManager;->connected:Z

    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->e:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/swarmconnect/network/ConnectionManager;->f:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->g:Ljava/lang/Throwable;

    iput-object p1, p0, Lcom/swarmconnect/network/ConnectionManager;->b:Ljava/lang/String;

    iput p2, p0, Lcom/swarmconnect/network/ConnectionManager;->c:I

    iput-object p3, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    return-void
.end method

.method private a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/swarmconnect/network/ConnectionManager;->b:Ljava/lang/String;

    iget v3, p0, Lcom/swarmconnect/network/ConnectionManager;->c:I

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, p1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    return-void
.end method


# virtual methods
.method public close(Ljava/lang/Throwable;)V
    .locals 2

    iput-object p1, p0, Lcom/swarmconnect/network/ConnectionManager;->g:Ljava/lang/Throwable;

    :try_start_0
    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->e:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/swarmconnect/network/ConnectionManager;->f:Z

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-enter p0

    :try_start_2
    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->g:Ljava/lang/Throwable;

    invoke-interface {v0, p0, v1}, Lcom/swarmconnect/network/NetworkHandler;->onConnectionLost(Lcom/swarmconnect/network/ConnectionManager;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/swarmconnect/network/ConnectionManager;->connected:Z

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getCloseReason()Ljava/lang/Throwable;
    .locals 2

    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->g:Ljava/lang/Throwable;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public initialize(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Lcom/swarmconnect/network/ConnectionManager;->a(I)V

    new-instance v0, Lcom/swarmconnect/network/Connection;

    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/swarmconnect/network/Connection;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->h:Lcom/swarmconnect/network/Connection;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/swarmconnect/network/ConnectionManager$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/network/ConnectionManager$1;-><init>(Lcom/swarmconnect/network/ConnectionManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->i:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->i:Ljava/lang/Thread;

    invoke-virtual {v0, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->i:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/swarmconnect/network/ConnectionManager;->connected:Z

    invoke-virtual {p0, v3}, Lcom/swarmconnect/network/ConnectionManager;->setKeepAlive(Z)V

    const/16 v0, 0x3a98

    invoke-virtual {p0, v0}, Lcom/swarmconnect/network/ConnectionManager;->setSoTimeout(I)V

    invoke-virtual {p0, v3}, Lcom/swarmconnect/network/ConnectionManager;->setTcpNoDelay(Z)V

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    invoke-interface {v0, p0}, Lcom/swarmconnect/network/NetworkHandler;->onConnected(Lcom/swarmconnect/network/ConnectionManager;)V

    :cond_0
    return-void
.end method

.method public receiveLoop()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->h:Lcom/swarmconnect/network/Connection;

    invoke-virtual {v0}, Lcom/swarmconnect/network/Connection;->receiveMessage()[B

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Peer sent DISCONNECT"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    aget-byte v1, v0, v3

    and-int/lit16 v1, v1, 0xff

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server sent Disconnect Packet"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->a:Lcom/swarmconnect/network/NetworkHandler;

    aget-byte v2, v0, v3

    and-int/lit16 v2, v2, 0xff

    invoke-interface {v1, v2, v0}, Lcom/swarmconnect/network/NetworkHandler;->onPacketReceived(I[B)V

    goto :goto_0
.end method

.method public sendBytes([B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/swarmconnect/network/ConnectionManager;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/swarmconnect/network/ConnectionManager;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->h:Lcom/swarmconnect/network/Connection;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v2, "sendMessage() on a closed connection"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/network/ConnectionManager;->g:Ljava/lang/Throwable;

    invoke-virtual {v0, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->h:Lcom/swarmconnect/network/Connection;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/network/Connection;->sendMessage([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/network/ConnectionManager;->close(Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public sendPacket([B)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/network/ConnectionManager;->sendBytes([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error in ConnectionManager.sendPacket: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/ConnectionManager;->close(Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setKeepAlive(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setKeepAlive(Z)V

    return-void
.end method

.method public setSoTimeout(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    return-void
.end method

.method public setTcpNoDelay(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/network/ConnectionManager;->d:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    return-void
.end method
