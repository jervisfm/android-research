.class public interface abstract Lcom/swarmconnect/network/NetworkHandler;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onConnected(Lcom/swarmconnect/network/ConnectionManager;)V
.end method

.method public abstract onConnectionLost(Lcom/swarmconnect/network/ConnectionManager;Ljava/lang/Throwable;)V
.end method

.method public abstract onPacketReceived(I[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
