.class public Lcom/swarmconnect/network/TypesWriter;
.super Ljava/lang/Object;


# instance fields
.field a:[B

.field b:I

.field c:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/swarmconnect/network/TypesWriter;->c:Z

    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iput v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/swarmconnect/network/TypesWriter;->c:Z

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iput v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/swarmconnect/network/TypesWriter;->c:Z

    iput-object p1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/swarmconnect/network/TypesWriter;->c:Z

    return-void
.end method

.method private a(I)V
    .locals 4

    const/4 v3, 0x0

    new-array v0, p1, [B

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget-object v2, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    return-void
.end method


# virtual methods
.method public getBytes([B)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public getBytes()[B
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/swarmconnect/network/TypesWriter;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public length()I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return v0
.end method

.method public writeBoolean(Z)V
    .locals 3

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v0, v0

    add-int/lit16 v0, v0, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    aput-byte v0, v1, v2

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeBoolean(ZI)V
    .locals 2

    add-int/lit8 v0, p2, 0x1

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v0, v0

    add-int/lit16 v0, v0, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    aput-byte v0, v1, p2

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeByte(I)V
    .locals 1

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-virtual {p0, p1, v0}, Lcom/swarmconnect/network/TypesWriter;->writeByte(II)V

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public writeByte(II)V
    .locals 2

    add-int/lit8 v0, p2, 0x1

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    add-int/lit16 v0, p2, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    int-to-byte v1, p1

    aput-byte v1, v0, p2

    return-void
.end method

.method public writeBytes([B)V
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeBytes([BII)V

    return-void
.end method

.method public writeBytes([BII)V
    .locals 2

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/2addr v0, p3

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v0, v0

    add-int/2addr v0, p3

    add-int/lit16 v0, v0, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public writeFloat(F)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(II)V

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public writeFloat(FI)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(II)V

    return-void
.end method

.method public writeIntArray([I)V
    .locals 2

    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    aget v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeMPInt(Ljava/math/BigInteger;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    aget-byte v1, v0, v3

    if-nez v1, :cond_0

    invoke-virtual {p0, v3}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    :goto_0
    return-void

    :cond_0
    array-length v1, v0

    invoke-virtual {p0, v0, v3, v1}, Lcom/swarmconnect/network/TypesWriter;->writeString([BII)V

    goto :goto_0
.end method

.method public writeNameList([Ljava/lang/String;)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    if-lez v0, :cond_1

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 3

    :try_start_0
    const-string v0, "ISO-8859-1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/swarmconnect/network/TypesWriter;->writeBytes([BII)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0
.end method

.method public writeString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    :goto_0
    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lcom/swarmconnect/network/TypesWriter;->writeBytes([BII)V

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public writeString([BII)V
    .locals 0

    invoke-virtual {p0, p3}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/swarmconnect/network/TypesWriter;->writeBytes([BII)V

    return-void
.end method

.method public writeStringArray([Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    array-length v1, p1

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    :goto_1
    return-void

    :cond_0
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/swarmconnect/network/TypesWriter;->writeString(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(I)V

    goto :goto_1
.end method

.method public writeUINT32(I)V
    .locals 1

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    invoke-virtual {p0, p1, v0}, Lcom/swarmconnect/network/TypesWriter;->writeUINT32(II)V

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    return-void
.end method

.method public writeUINT32(II)V
    .locals 4

    add-int/lit8 v0, p2, 0x4

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    add-int/lit16 v0, p2, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    add-int/lit8 v1, p2, 0x1

    shr-int/lit8 v2, p1, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, p2

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    add-int/lit8 v2, v1, 0x1

    shr-int/lit8 v3, p1, 0x10

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    add-int/lit8 v1, v2, 0x1

    shr-int/lit8 v3, p1, 0x8

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    add-int/lit8 v2, v1, 0x1

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    return-void
.end method

.method public writeUINT64(J)V
    .locals 4

    iget v0, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    array-length v0, v0

    add-int/lit16 v0, v0, 0x100

    invoke-direct {p0, v0}, Lcom/swarmconnect/network/TypesWriter;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x38

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x30

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x28

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x20

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x18

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x10

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    const/16 v2, 0x8

    shr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/swarmconnect/network/TypesWriter;->a:[B

    iget v1, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/network/TypesWriter;->b:I

    long-to-int v2, p1

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-void
.end method
