.class public Lcom/swarmconnect/packets/PacketNotification;
.super Lcom/swarmconnect/packets/Packet;


# static fields
.field private static a:I


# instance fields
.field public clientId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    sput v0, Lcom/swarmconnect/packets/PacketNotification;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/packets/Packet;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/packets/Packet;-><init>()V

    iput-object p1, p0, Lcom/swarmconnect/packets/PacketNotification;->clientId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public fromPayload([BI)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/swarmconnect/packets/TypesReader;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/swarmconnect/packets/TypesReader;-><init>([BII)V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/TypesReader;->readByte()I

    move-result v1

    sget v2, Lcom/swarmconnect/packets/PacketNotification;->a:I

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "This is not a "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/swarmconnect/packets/Packets;->reverseNames:[Ljava/lang/String;

    sget v4, Lcom/swarmconnect/packets/PacketNotification;->a:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Packet! ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/swarmconnect/packets/TypesReader;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/packets/PacketNotification;->clientId:Ljava/lang/String;

    return-void
.end method

.method public getPayload()[B
    .locals 2

    new-instance v0, Lcom/swarmconnect/packets/TypesWriter;

    invoke-direct {v0}, Lcom/swarmconnect/packets/TypesWriter;-><init>()V

    sget v1, Lcom/swarmconnect/packets/PacketNotification;->a:I

    invoke-virtual {v0, v1}, Lcom/swarmconnect/packets/TypesWriter;->writeByte(I)V

    iget-object v1, p0, Lcom/swarmconnect/packets/PacketNotification;->clientId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/packets/TypesWriter;->writeString(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/TypesWriter;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
