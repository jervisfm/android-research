.class public Lcom/swarmconnect/packets/PacketPing;
.super Lcom/swarmconnect/packets/Packet;


# static fields
.field private static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    sput v0, Lcom/swarmconnect/packets/PacketPing;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/packets/Packet;-><init>()V

    return-void
.end method


# virtual methods
.method public fromPayload([BI)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/swarmconnect/packets/TypesReader;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/swarmconnect/packets/TypesReader;-><init>([BII)V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/TypesReader;->readByte()I

    move-result v0

    sget v1, Lcom/swarmconnect/packets/PacketPing;->a:I

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "This is not a "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/swarmconnect/packets/Packets;->reverseNames:[Ljava/lang/String;

    sget v4, Lcom/swarmconnect/packets/PacketPing;->a:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Packet! ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method public getPayload()[B
    .locals 2

    new-instance v0, Lcom/swarmconnect/packets/TypesWriter;

    invoke-direct {v0}, Lcom/swarmconnect/packets/TypesWriter;-><init>()V

    sget v1, Lcom/swarmconnect/packets/PacketPing;->a:I

    invoke-virtual {v0, v1}, Lcom/swarmconnect/packets/TypesWriter;->writeByte(I)V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/TypesWriter;->getBytes()[B

    move-result-object v0

    return-object v0
.end method
