.class public Lcom/swarmconnect/packets/Packets;
.super Ljava/lang/Object;


# static fields
.field public static final AUTH:I = 0x3

.field public static final DISCONNECT:I = 0x1

.field public static final NOTIFICATION:I = 0x6

.field public static final PING:I = 0x4

.field public static final PONG:I = 0x5

.field public static final VERSION:I = 0x2

.field public static final pingPayload:[B

.field public static final pongPayload:[B

.field public static final reverseNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/swarmconnect/packets/PacketPong;

    invoke-direct {v0}, Lcom/swarmconnect/packets/PacketPong;-><init>()V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/PacketPong;->getPayload()[B

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/packets/Packets;->pongPayload:[B

    new-instance v0, Lcom/swarmconnect/packets/PacketPing;

    invoke-direct {v0}, Lcom/swarmconnect/packets/PacketPing;-><init>()V

    invoke-virtual {v0}, Lcom/swarmconnect/packets/PacketPing;->getPayload()[B

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/packets/Packets;->pingPayload:[B

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UNDEFINED"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Disconnect"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Version"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Auth"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Ping"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Pong"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Notification"

    aput-object v2, v0, v1

    sput-object v0, Lcom/swarmconnect/packets/Packets;->reverseNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
