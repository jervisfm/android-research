.class public Lcom/swarmconnect/packets/TypesReader;
.super Ljava/lang/Object;


# instance fields
.field a:[B

.field b:I

.field c:I


# direct methods
.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iput-object p1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    array-length v0, p1

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iput-object p1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iput p2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    array-length v0, p1

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    array-length v1, p1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal offset."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iput-object p1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iput p2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int v0, p2, p3

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    array-length v1, p1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal offset."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    array-length v1, p1

    if-le v0, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal length."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method


# virtual methods
.method public readBoolean()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v0, v0, v1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readByte()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-lt v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public readByteString()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v0

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v1, v0

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed SSH byte string."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-array v1, v0, [B

    iget-object v2, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    return-object v1
.end method

.method public readBytes([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, p3

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    return-void
.end method

.method public readBytes(I)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-array v0, p1, [B

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    return-object v0
.end method

.method public readFloat()F
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public readIntArray()[I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v2

    const/4 v0, 0x1

    if-ge v2, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-array v0, v2, [I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v3

    aput v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public readMPINT()Ljava/math/BigInteger;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readByteString()[B

    move-result-object v1

    array-length v0, v1

    if-nez v0, :cond_0

    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>([B)V

    goto :goto_0
.end method

.method public readString()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v0

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v1, v0

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed SSH string."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    const-string v4, "ISO-8859-1"

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    return-object v1
.end method

.method public readString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v1

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed SSH string."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    invoke-direct {v0, v2, v3, v1}, Ljava/lang/String;-><init>([BII)V

    :goto_0
    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    invoke-direct {v0, v2, v3, v1, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    goto :goto_0
.end method

.method public readStringArray()[Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readUINT32()I

    move-result v2

    if-lez v2, :cond_1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/swarmconnect/packets/TypesReader;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public readUINT32()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public readUINT64()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v0, v0, 0x8

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Packet too short."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v2, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-long v0, v0

    iget-object v2, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v3, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v4, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v4, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    iget-object v3, p0, Lcom/swarmconnect/packets/TypesReader;->a:[B

    iget v4, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    int-to-long v2, v2

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public remain()I
    .locals 2

    iget v0, p0, Lcom/swarmconnect/packets/TypesReader;->c:I

    iget v1, p0, Lcom/swarmconnect/packets/TypesReader;->b:I

    sub-int/2addr v0, v1

    return v0
.end method
