.class Lcom/swarmconnect/q$4;
.super Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/q;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/q;


# direct methods
.method constructor <init>(Lcom/swarmconnect/q;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotFriends(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->g(Lcom/swarmconnect/q;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-static {v0, p1}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;Ljava/util/List;)V

    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-static {v0, p2}, Lcom/swarmconnect/q;->b(Lcom/swarmconnect/q;Ljava/util/List;)V

    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->h(Lcom/swarmconnect/q;)Lcom/swarmconnect/q$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/q$a;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-virtual {v0}, Lcom/swarmconnect/q;->c()V

    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-virtual {v0}, Lcom/swarmconnect/q;->getOnlineStatus()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/q$4;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->g(Lcom/swarmconnect/q;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
