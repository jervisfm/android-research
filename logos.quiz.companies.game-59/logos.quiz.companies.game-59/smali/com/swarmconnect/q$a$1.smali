.class Lcom/swarmconnect/q$a$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/q$a;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/q$a;

.field private final synthetic b:Lcom/swarmconnect/SwarmUser;


# direct methods
.method constructor <init>(Lcom/swarmconnect/q$a;Lcom/swarmconnect/SwarmUser;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    iput-object p2, p0, Lcom/swarmconnect/q$a$1;->b:Lcom/swarmconnect/SwarmUser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/q$a$1;)Lcom/swarmconnect/q$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    return-object v0
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    invoke-static {v0}, Lcom/swarmconnect/q$a;->a(Lcom/swarmconnect/q$a;)Lcom/swarmconnect/q;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/q;->d(Lcom/swarmconnect/q;)V

    iget-object v0, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    invoke-static {v0}, Lcom/swarmconnect/q$a;->a(Lcom/swarmconnect/q$a;)Lcom/swarmconnect/q;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    invoke-static {v2}, Lcom/swarmconnect/q$a;->a(Lcom/swarmconnect/q$a;)Lcom/swarmconnect/q;

    move-result-object v2

    iget-object v2, v2, Lcom/swarmconnect/q;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "Delete Friend"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Do you want to remove "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/swarmconnect/q$a$1;->b:Lcom/swarmconnect/SwarmUser;

    iget-object v3, v3, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from your friends list?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Remove"

    new-instance v3, Lcom/swarmconnect/q$a$1$2;

    iget-object v4, p0, Lcom/swarmconnect/q$a$1;->b:Lcom/swarmconnect/SwarmUser;

    invoke-direct {v3, p0, v4}, Lcom/swarmconnect/q$a$1$2;-><init>(Lcom/swarmconnect/q$a$1;Lcom/swarmconnect/SwarmUser;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Cancel"

    new-instance v3, Lcom/swarmconnect/q$a$1$1;

    invoke-direct {v3, p0}, Lcom/swarmconnect/q$a$1$1;-><init>(Lcom/swarmconnect/q$a$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;Landroid/app/AlertDialog;)V

    iget-object v0, p0, Lcom/swarmconnect/q$a$1;->a:Lcom/swarmconnect/q$a;

    invoke-static {v0}, Lcom/swarmconnect/q$a;->a(Lcom/swarmconnect/q$a;)Lcom/swarmconnect/q;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/q;->e(Lcom/swarmconnect/q;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return v5
.end method
