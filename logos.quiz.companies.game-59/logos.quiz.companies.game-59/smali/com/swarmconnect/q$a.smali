.class Lcom/swarmconnect/q$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/q;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/q;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/q;Lcom/swarmconnect/q$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/q$a;-><init>(Lcom/swarmconnect/q;)V

    return-void
.end method

.method private a()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->b(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->b(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/swarmconnect/q$a;)Lcom/swarmconnect/q;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    return-object v0
.end method

.method private a(I)Z
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/q$a;->a()I

    move-result v0

    invoke-direct {p0}, Lcom/swarmconnect/q$a;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/q$a;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v0}, Lcom/swarmconnect/q;->a(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-direct {p0}, Lcom/swarmconnect/q$a;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v1}, Lcom/swarmconnect/q;->b(Lcom/swarmconnect/q;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-virtual {v0}, Lcom/swarmconnect/q;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@layout/swarm_friends_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/q$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_1

    rem-int/lit8 v1, p1, 0x2

    if-nez v1, :cond_2

    const v1, 0xffffff

    :goto_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/username"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/points"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v0, Lcom/swarmconnect/SwarmUser;->points:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/points"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, -0xe67729

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    invoke-static {v1}, Lcom/swarmconnect/q;->c(Lcom/swarmconnect/q;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swarmconnect/SwarmApplication;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v3, "@id/online_text"

    invoke-virtual {v2, v3}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Online: Playing "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/swarmconnect/SwarmApplication;->name:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/profile_pic"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v3, "@drawable/swarm_friend_online"

    invoke-virtual {v2, v3}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    invoke-direct {p0, p1}, Lcom/swarmconnect/q$a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p2, v5}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/online_text"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "Wants to be your friend!"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/friended_section"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/unfriended_section"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/accept"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->footerButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/accept"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/swarmconnect/q$a$2;

    invoke-direct {v2, p0, v0}, Lcom/swarmconnect/q$a$2;-><init>(Lcom/swarmconnect/q$a;Lcom/swarmconnect/SwarmUser;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/deny"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->footerButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/deny"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/swarmconnect/q$a$3;

    invoke-direct {v2, p0, v0}, Lcom/swarmconnect/q$a$3;-><init>(Lcom/swarmconnect/q$a;Lcom/swarmconnect/SwarmUser;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_2
    return-object p2

    :cond_2
    const v1, -0x77000001

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/online_text"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "Offline"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/profile_pic"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v3, "@drawable/swarm_friend_offline"

    invoke-virtual {v2, v3}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/friended_section"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/q$a;->a:Lcom/swarmconnect/q;

    const-string v2, "@id/unfriended_section"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v1, Lcom/swarmconnect/q$a$4;

    invoke-direct {v1, p0, v0}, Lcom/swarmconnect/q$a$4;-><init>(Lcom/swarmconnect/q$a;Lcom/swarmconnect/SwarmUser;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/swarmconnect/q$a$1;

    invoke-direct {v1, p0, v0}, Lcom/swarmconnect/q$a$1;-><init>(Lcom/swarmconnect/q$a;Lcom/swarmconnect/SwarmUser;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_2
.end method
