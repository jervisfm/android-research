.class Lcom/swarmconnect/q;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/q$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/q$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/widget/TextView;

.field private p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/swarmconnect/SwarmUser;",
            "Lcom/swarmconnect/SwarmApplication;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/q$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/q$a;-><init>(Lcom/swarmconnect/q;Lcom/swarmconnect/q$a;)V

    iput-object v0, p0, Lcom/swarmconnect/q;->l:Lcom/swarmconnect/q$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/q;->m:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/q;->n:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/q;->p:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/q;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->n:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/q;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/q;->e(I)V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/q;Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q;->q:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/q;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/q;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/q;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/q;->d(I)V

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/q;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/q;->n:Ljava/util/List;

    return-void
.end method

.method static synthetic c(Lcom/swarmconnect/q;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->p:Ljava/util/HashMap;

    return-object v0
.end method

.method private d(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/q;->b()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/q$6;

    invoke-direct {v1, p0}, Lcom/swarmconnect/q$6;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {v0, p1, v1}, Lcom/swarmconnect/SwarmActiveUser;->deleteFriend(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    return-void
.end method

.method static synthetic d(Lcom/swarmconnect/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/q;->e()V

    return-void
.end method

.method static synthetic e(Lcom/swarmconnect/q;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->q:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->q:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/q;->q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/q;->q:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/q;->b()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/q$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/q$3;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {v0, p1, v1}, Lcom/swarmconnect/SwarmActiveUser;->addFriend(ILcom/swarmconnect/SwarmActiveUser$FriendRequestCB;)V

    return-void
.end method

.method private f()V
    .locals 2

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/swarmconnect/q;->b()V

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    new-instance v1, Lcom/swarmconnect/q$4;

    invoke-direct {v1, p0}, Lcom/swarmconnect/q$4;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmActiveUser;->getFriends(Lcom/swarmconnect/SwarmActiveUser$GotFriendsCB;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/swarmconnect/q;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/q;->f()V

    return-void
.end method

.method static synthetic g(Lcom/swarmconnect/q;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lcom/swarmconnect/q;)Lcom/swarmconnect/q$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/q;->l:Lcom/swarmconnect/q$a;

    return-object v0
.end method


# virtual methods
.method public getOnlineStatus()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/q$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/q$2;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_friends"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->b(I)V

    const-string v0, "@id/empty_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/q;->o:Landroid/widget/TextView;

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/q;->l:Lcom/swarmconnect/q$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const-string v0, "@id/add"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->footerButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/add"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v1, Lcom/swarmconnect/q$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/q$1;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_friends"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Friends"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/q;->a(ILjava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/swarmconnect/u;->onPause()V

    invoke-direct {p0}, Lcom/swarmconnect/q;->e()V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/q$5;

    invoke-direct {v0, p0}, Lcom/swarmconnect/q$5;-><init>(Lcom/swarmconnect/q;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/q;->a(Ljava/lang/Runnable;)V

    return-void
.end method
