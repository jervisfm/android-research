.class Lcom/swarmconnect/s$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/s;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/s;


# direct methods
.method constructor <init>(Lcom/swarmconnect/s;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/s$1;->a:Lcom/swarmconnect/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/swarmconnect/s$1;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->b(Lcom/swarmconnect/s;)Lcom/swarmconnect/s$a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/swarmconnect/s$a;->getItem(I)Lcom/swarmconnect/SwarmApplication;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "market://details?id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/swarmconnect/SwarmApplication;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/swarmconnect/s$1;->a:Lcom/swarmconnect/s;

    iget-object v0, v0, Lcom/swarmconnect/s;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, v1}, Lcom/swarmconnect/SwarmMainActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
