.class Lcom/swarmconnect/s$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/s;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/s;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/s;Lcom/swarmconnect/s$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/s$a;-><init>(Lcom/swarmconnect/s;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->a(Lcom/swarmconnect/s;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->a(Lcom/swarmconnect/s;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/swarmconnect/SwarmApplication;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->a(Lcom/swarmconnect/s;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->a(Lcom/swarmconnect/s;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-static {v0}, Lcom/swarmconnect/s;->a(Lcom/swarmconnect/s;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmApplication;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/swarmconnect/s$a;->getItem(I)Lcom/swarmconnect/SwarmApplication;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    invoke-virtual {v0}, Lcom/swarmconnect/s;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    const-string v2, "@layout/swarm_app_row"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/swarmconnect/s$a;->getItem(I)Lcom/swarmconnect/SwarmApplication;

    move-result-object v1

    if-eqz v1, :cond_1

    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    const v0, 0xffffff

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    const-string v2, "@id/pic"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/ui/AsyncImageView;

    iget-object v2, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    iget-object v2, v2, Lcom/swarmconnect/s;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/ui/AsyncImageView;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    const-string v2, "@id/pic"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/ui/AsyncImageView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmApplication;->iconUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/swarmconnect/ui/AsyncImageView;->getUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    const-string v2, "@id/name"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/swarmconnect/SwarmApplication;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/swarmconnect/s$a;->a:Lcom/swarmconnect/s;

    const-string v2, "@id/publisher"

    invoke-virtual {v0, v2}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, v1, Lcom/swarmconnect/SwarmApplication;->publisherName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2

    :cond_2
    const v0, -0x77000001

    goto :goto_0
.end method
