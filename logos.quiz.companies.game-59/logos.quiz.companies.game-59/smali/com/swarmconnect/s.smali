.class Lcom/swarmconnect/s;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/s$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/s$a;

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmApplication;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    new-instance v0, Lcom/swarmconnect/s$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/s$a;-><init>(Lcom/swarmconnect/s;Lcom/swarmconnect/s$a;)V

    iput-object v0, p0, Lcom/swarmconnect/s;->l:Lcom/swarmconnect/s$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/s;->m:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/s;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/s;->m:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/swarmconnect/s;)Lcom/swarmconnect/s$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/s;->l:Lcom/swarmconnect/s$a;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/swarmconnect/s;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/swarmconnect/s;->b()V

    new-instance v0, Lcom/swarmconnect/l;

    invoke-direct {v0}, Lcom/swarmconnect/l;-><init>()V

    new-instance v1, Lcom/swarmconnect/s$3;

    invoke-direct {v1, p0}, Lcom/swarmconnect/s$3;-><init>(Lcom/swarmconnect/s;)V

    iput-object v1, v0, Lcom/swarmconnect/l;->cb:Lcom/swarmconnect/APICall$APICallback;

    invoke-virtual {v0}, Lcom/swarmconnect/l;->run()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "@layout/swarm_list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->b(I)V

    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/s;->l:Lcom/swarmconnect/s$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/swarmconnect/s$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/s$1;-><init>(Lcom/swarmconnect/s;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_games"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Get More Games"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/s;->a(ILjava/lang/String;)V

    return-void
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/s$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/s$2;-><init>(Lcom/swarmconnect/s;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/s;->a(Ljava/lang/Runnable;)V

    return-void
.end method
