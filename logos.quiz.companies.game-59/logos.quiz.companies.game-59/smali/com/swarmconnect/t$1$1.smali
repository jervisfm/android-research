.class Lcom/swarmconnect/t$1$1;
.super Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/t$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/t$1;


# direct methods
.method constructor <init>(Lcom/swarmconnect/t$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-direct {p0}, Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;-><init>()V

    return-void
.end method


# virtual methods
.method public sendFailed()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/t;->c()V

    const-string v0, "Unable to send message."

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->b(Ljava/lang/String;)V

    return-void
.end method

.method public sentMessage(ZI)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    new-instance v1, Lcom/swarmconnect/SwarmMessageThread;

    invoke-direct {v1}, Lcom/swarmconnect/SwarmMessageThread;-><init>()V

    invoke-static {v0, v1}, Lcom/swarmconnect/t;->a(Lcom/swarmconnect/t;Lcom/swarmconnect/SwarmMessageThread;)V

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v0

    iput p2, v0, Lcom/swarmconnect/SwarmMessageThread;->id:I

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v1}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v1

    invoke-static {v1}, Lcom/swarmconnect/t;->c(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmUser;

    move-result-object v1

    iput-object v1, v0, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-static {v0}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/swarmconnect/SwarmMessageThread;->viewed:Z

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/t;->reload()V

    iget-object v0, p0, Lcom/swarmconnect/t$1$1;->a:Lcom/swarmconnect/t$1;

    invoke-static {v0}, Lcom/swarmconnect/t$1;->a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swarmconnect/t;->c()V

    return-void
.end method
