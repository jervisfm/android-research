.class Lcom/swarmconnect/t$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/t;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/t;


# direct methods
.method constructor <init>(Lcom/swarmconnect/t;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/t$1;)Lcom/swarmconnect/t;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    iget-object v1, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    const-string v2, "@id/reply"

    invoke-virtual {v1, v2}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/swarmconnect/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-virtual {v2}, Lcom/swarmconnect/t;->b()V

    new-instance v2, Lcom/swarmconnect/t$1$1;

    invoke-direct {v2, p0}, Lcom/swarmconnect/t$1$1;-><init>(Lcom/swarmconnect/t$1;)V

    iget-object v3, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-static {v3}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-static {v3}, Lcom/swarmconnect/t;->b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/swarmconnect/SwarmMessageThread;->reply(Ljava/lang/String;Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V

    :cond_0
    :goto_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-static {v3}, Lcom/swarmconnect/t;->c(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmUser;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/swarmconnect/t$1;->a:Lcom/swarmconnect/t;

    invoke-static {v3}, Lcom/swarmconnect/t;->c(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmUser;

    move-result-object v3

    iget v3, v3, Lcom/swarmconnect/SwarmUser;->userId:I

    invoke-static {v3, v1, v2}, Lcom/swarmconnect/SwarmMessage;->sendMessage(ILjava/lang/String;Lcom/swarmconnect/SwarmMessageThread$SentMessageCB;)V

    goto :goto_0
.end method
