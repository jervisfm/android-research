.class Lcom/swarmconnect/t$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/t;


# direct methods
.method private constructor <init>(Lcom/swarmconnect/t;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/swarmconnect/t;Lcom/swarmconnect/t$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/swarmconnect/t$a;-><init>(Lcom/swarmconnect/t;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-static {v0}, Lcom/swarmconnect/t;->a(Lcom/swarmconnect/t;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-static {v0}, Lcom/swarmconnect/t;->a(Lcom/swarmconnect/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-static {v0}, Lcom/swarmconnect/t;->a(Lcom/swarmconnect/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-static {v0}, Lcom/swarmconnect/t;->a(Lcom/swarmconnect/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lcom/swarmconnect/t$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessage;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    iget v1, v1, Lcom/swarmconnect/SwarmUser;->userId:I

    sget-object v3, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    iget v3, v3, Lcom/swarmconnect/SwarmActiveUser;->userId:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-virtual {v1}, Lcom/swarmconnect/t;->d()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    const-string v4, "@layout/swarm_thread_row_self"

    invoke-virtual {v3, v4}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v1, v3, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    :goto_0
    iget-object v1, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    const-string v3, "@id/speech_bubble"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->messageBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    const-string v3, "@id/message"

    invoke-virtual {v1, v3}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, v0, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    iget-object v4, v4, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/swarmconnect/SwarmMessage;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/swarmconnect/SwarmMessage;->from:Lcom/swarmconnect/SwarmUser;

    iget-object v5, v5, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    const/16 v6, 0x21

    invoke-interface {v1, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMessage;->getSentDate()Ljava/util/Date;

    move-result-object v1

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v0, "MMM dd, h:mma"

    invoke-direct {v3, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    const-string v4, "@id/date"

    invoke-virtual {v0, v4}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-object v2

    :cond_1
    iget-object v1, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    invoke-virtual {v1}, Lcom/swarmconnect/t;->d()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/swarmconnect/t$a;->a:Lcom/swarmconnect/t;

    const-string v4, "@layout/swarm_thread_row_other"

    invoke-virtual {v3, v4}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v1, v3, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_0
.end method
