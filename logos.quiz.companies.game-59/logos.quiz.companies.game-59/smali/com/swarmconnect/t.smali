.class Lcom/swarmconnect/t;
.super Lcom/swarmconnect/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/t$a;
    }
.end annotation


# instance fields
.field private l:Lcom/swarmconnect/SwarmMessageThread;

.field private m:Lcom/swarmconnect/SwarmUser;

.field private n:Lcom/swarmconnect/t$a;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/swarmconnect/SwarmMessage;",
            ">;"
        }
    .end annotation
.end field

.field private p:Landroid/widget/ListView;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/swarmconnect/u;-><init>()V

    iput-object v1, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    iput-object v1, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    new-instance v0, Lcom/swarmconnect/t$a;

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/t$a;-><init>(Lcom/swarmconnect/t;Lcom/swarmconnect/t$a;)V

    iput-object v0, p0, Lcom/swarmconnect/t;->n:Lcom/swarmconnect/t$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/swarmconnect/t;->o:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/t;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t;->o:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/swarmconnect/t;Lcom/swarmconnect/SwarmMessageThread;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/t;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/t;->o:Ljava/util/List;

    return-void
.end method

.method static synthetic b(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmMessageThread;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    return-object v0
.end method

.method static synthetic c(Lcom/swarmconnect/t;)Lcom/swarmconnect/SwarmUser;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    return-object v0
.end method

.method static synthetic d(Lcom/swarmconnect/t;)Lcom/swarmconnect/t$a;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t;->n:Lcom/swarmconnect/t$a;

    return-object v0
.end method

.method static synthetic e(Lcom/swarmconnect/t;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/t;->p:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public getMessages()V
    .locals 3

    new-instance v0, Lcom/swarmconnect/t$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/t$2;-><init>(Lcom/swarmconnect/t;)V

    iget-object v1, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/swarmconnect/t;->b()V

    iget-object v1, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/SwarmMessageThread;->getMessages(Lcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/swarmconnect/t;->b()V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    iget v2, v2, Lcom/swarmconnect/SwarmUser;->userId:I

    invoke-static {v1, v2, v0}, Lcom/swarmconnect/SwarmMessageThread;->getMessages(IILcom/swarmconnect/SwarmMessageThread$GotMessagesCB;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "@layout/swarm_thread"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->b(I)V

    iget-object v0, p0, Lcom/swarmconnect/t;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "thread"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmMessageThread;

    iput-object v0, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    const-string v0, "otherUser"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/SwarmUser;

    iput-object v0, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    :cond_0
    const-string v0, "@id/list"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/swarmconnect/t;->p:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/swarmconnect/t;->p:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/swarmconnect/t;->n:Lcom/swarmconnect/t$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const-string v0, "@id/send"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->footerButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "@id/send"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v1, Lcom/swarmconnect/t$1;

    invoke-direct {v1, p0}, Lcom/swarmconnect/t$1;-><init>(Lcom/swarmconnect/t;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1}, Lcom/swarmconnect/u;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "@drawable/swarm_messages"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    const-string v1, "Messages"

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/t;->a(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_2

    const-string v0, "@drawable/swarm_messages"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conversation with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/t;->m:Lcom/swarmconnect/SwarmUser;

    iget-object v2, v2, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/t;->a(ILjava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    iget-object v0, v0, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    if-eqz v0, :cond_1

    const-string v0, "@drawable/swarm_messages"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conversation with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/swarmconnect/t;->l:Lcom/swarmconnect/SwarmMessageThread;

    iget-object v2, v2, Lcom/swarmconnect/SwarmMessageThread;->otherUser:Lcom/swarmconnect/SwarmUser;

    iget-object v2, v2, Lcom/swarmconnect/SwarmUser;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/swarmconnect/t;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public reload()V
    .locals 1

    new-instance v0, Lcom/swarmconnect/t$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/t$3;-><init>(Lcom/swarmconnect/t;)V

    invoke-virtual {p0, v0}, Lcom/swarmconnect/t;->a(Ljava/lang/Runnable;)V

    return-void
.end method
