.class abstract Lcom/swarmconnect/u;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String; = "Swarm"

.field protected static a:Lcom/swarmconnect/u;

.field protected static b:Z

.field protected static final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/swarmconnect/u;",
            ">;"
        }
    .end annotation
.end field

.field protected static final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/swarmconnect/u;",
            ">;"
        }
    .end annotation
.end field

.field protected static k:Lcom/swarmconnect/delegates/SwarmNotificationDelegate;


# instance fields
.field protected c:Lcom/swarmconnect/SwarmMainActivity;

.field protected d:Landroid/view/View;

.field protected e:Landroid/view/View;

.field protected f:Landroid/view/View;

.field protected g:Landroid/view/View;

.field protected h:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/View;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/swarmconnect/u;->b:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/swarmconnect/u;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/swarmconnect/u;->j:Ljava/util/ArrayList;

    new-instance v0, Lcom/swarmconnect/at;

    invoke-direct {v0}, Lcom/swarmconnect/at;-><init>()V

    sput-object v0, Lcom/swarmconnect/u;->k:Lcom/swarmconnect/delegates/SwarmNotificationDelegate;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a()V
    .locals 2

    sget-object v0, Lcom/swarmconnect/u;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->finish()V

    goto :goto_0
.end method

.method protected static b(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/swarmconnect/Swarm;->c(Ljava/lang/String;)V

    return-void
.end method

.method private e()Z
    .locals 1

    instance-of v0, p0, Lcom/swarmconnect/ba;

    return v0
.end method

.method public static getResource(Ljava/lang/String;Landroid/content/Context;)I
    .locals 3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/swarmconnect/u;->getResource(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    goto :goto_0
.end method

.method protected a(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/SwarmMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(IILandroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method protected a(ILjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/SwarmMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;ZLjava/lang/Runnable;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->subHeaderButtonBackground()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/swarmconnect/u;->s:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/swarmconnect/u;->t:Landroid/view/View;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    new-instance v1, Lcom/swarmconnect/u$1;

    invoke-direct {v1, p0, p3}, Lcom/swarmconnect/u$1;-><init>(Lcom/swarmconnect/u;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected b()V
    .locals 0

    invoke-virtual {p0}, Lcom/swarmconnect/u;->c()V

    invoke-static {}, Lcom/swarmconnect/Swarm;->c()V

    return-void
.end method

.method protected b(I)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/SwarmMainActivity;->setContentView(I)V

    :cond_0
    return-void
.end method

.method protected c(I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    const-class v2, Lcom/swarmconnect/SwarmMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "screenType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0
.end method

.method protected c()V
    .locals 0

    invoke-static {}, Lcom/swarmconnect/Swarm;->d()V

    return-void
.end method

.method public clearStack()V
    .locals 2

    sget-object v0, Lcom/swarmconnect/u;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/u;

    invoke-virtual {v0}, Lcom/swarmconnect/u;->finish()V

    goto :goto_0
.end method

.method protected d()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0}, Lcom/swarmconnect/SwarmMainActivity;->finish()V

    sget-object v0, Lcom/swarmconnect/u;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/16 v3, 0x8

    const-string v0, "@id/background"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/swarmconnect/u;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->d:Landroid/view/View;

    const-string v1, "@drawable/swarm_background"

    invoke-virtual {p0, v1}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    :cond_0
    const-string v0, "@id/header"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/swarmconnect/u;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/u;->e:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->headerBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    const-string v0, "@id/footer"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/swarmconnect/u;->f:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/swarmconnect/u;->f:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->footerBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    const-string v0, "@id/offline_error"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->g:Landroid/view/View;

    const-string v0, "@id/offline_error_underline"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/swarmconnect/u;->g:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/u;->g:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->headerOfflineErrorBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    const-string v0, "@id/sheader"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->l:Landroid/view/View;

    const-string v0, "@id/sheader_icon"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/swarmconnect/u;->m:Landroid/widget/ImageView;

    const-string v0, "@id/sheader_title"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/u;->n:Landroid/widget/TextView;

    const-string v0, "@id/sheader_btn1"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->o:Landroid/view/View;

    const-string v0, "@id/sheader_btn1_text"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/u;->p:Landroid/widget/TextView;

    const-string v0, "@id/sheader_btn1_dropdown"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/u;->q:Landroid/view/View;

    const-string v0, "@id/sheader_btn2"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    const-string v0, "@id/sheader_btn2_text"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/u;->s:Landroid/widget/TextView;

    const-string v0, "@id/sheader_btn1_dropdown"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/swarmconnect/u;->t:Landroid/view/View;

    iget-object v0, p0, Lcom/swarmconnect/u;->l:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/swarmconnect/u;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->r:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    :try_start_0
    const-string v0, "@id/home"

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const-string v1, "@id/messages"

    invoke-virtual {p0, v1}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const-string v2, "@id/close"

    invoke-virtual {p0, v2}, Lcom/swarmconnect/u;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/swarmconnect/u;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    instance-of v3, p0, Lcom/swarmconnect/SwarmDashboardScreen;

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->headerButtonPressedBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    instance-of v0, p0, Lcom/swarmconnect/t;

    if-nez v0, :cond_5

    instance-of v0, p0, Lcom/swarmconnect/g;

    if-eqz v0, :cond_7

    :cond_5
    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->headerButtonPressedBackground()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    new-instance v0, Lcom/swarmconnect/u$3;

    invoke-direct {v0, p0}, Lcom/swarmconnect/u$3;-><init>(Lcom/swarmconnect/u;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    return-void

    :cond_6
    new-instance v3, Lcom/swarmconnect/u$5;

    invoke-direct {v3, p0}, Lcom/swarmconnect/u$5;-><init>(Lcom/swarmconnect/u;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_7
    new-instance v0, Lcom/swarmconnect/u$4;

    invoke-direct {v0, p0}, Lcom/swarmconnect/u$4;-><init>(Lcom/swarmconnect/u;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    sget-object v0, Lcom/swarmconnect/u;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/u;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    sget-object v0, Lcom/swarmconnect/u;->k:Lcom/swarmconnect/delegates/SwarmNotificationDelegate;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->removeNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    sput-object p0, Lcom/swarmconnect/u;->a:Lcom/swarmconnect/u;

    sget-object v0, Lcom/swarmconnect/u;->k:Lcom/swarmconnect/delegates/SwarmNotificationDelegate;

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->addNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V

    invoke-direct {p0}, Lcom/swarmconnect/u;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/u;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/swarmconnect/u;->refresh()V

    return-void

    :cond_1
    sget-object v0, Lcom/swarmconnect/u;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/swarmconnect/Swarm;->user:Lcom/swarmconnect/SwarmActiveUser;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/swarmconnect/u;->clearStack()V

    new-instance v0, Lcom/swarmconnect/u$2;

    invoke-direct {v0, p0}, Lcom/swarmconnect/u$2;-><init>(Lcom/swarmconnect/u;)V

    invoke-static {v0}, Lcom/swarmconnect/SwarmLoginManager;->addLoginListener(Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    invoke-static {}, Lcom/swarmconnect/Swarm;->showLogin()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 4

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/swarmconnect/u;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/swarmconnect/u;->g:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/u;->h:Landroid/view/View;

    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/swarmconnect/u;->reload()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method protected abstract reload()V
.end method

.method public setParent(Lcom/swarmconnect/SwarmMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    return-void
.end method

.method public show(I)V
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/u;->c:Lcom/swarmconnect/SwarmMainActivity;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/SwarmMainActivity;->show(I)V

    :cond_0
    return-void
.end method
