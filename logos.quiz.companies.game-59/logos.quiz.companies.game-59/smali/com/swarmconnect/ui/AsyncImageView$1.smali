.class Lcom/swarmconnect/ui/AsyncImageView$1;
.super Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swarmconnect/ui/AsyncImageView;->getUrl(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swarmconnect/ui/AsyncImageView;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swarmconnect/ui/AsyncImageView;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    iput-object p2, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;-><init>()V

    return-void
.end method


# virtual methods
.method public gotImage(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    iget-object v1, v1, Lcom/swarmconnect/ui/AsyncImageView;->url:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    invoke-virtual {v0, p1}, Lcom/swarmconnect/ui/AsyncImageView;->gotImage(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public requestFailed(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    invoke-static {v0}, Lcom/swarmconnect/ui/AsyncImageView;->a(Lcom/swarmconnect/ui/AsyncImageView;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    invoke-static {v0}, Lcom/swarmconnect/ui/AsyncImageView;->b(Lcom/swarmconnect/ui/AsyncImageView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    invoke-static {v1}, Lcom/swarmconnect/ui/AsyncImageView;->a(Lcom/swarmconnect/ui/AsyncImageView;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/swarmconnect/ui/AsyncImageView$1;->a:Lcom/swarmconnect/ui/AsyncImageView;

    invoke-virtual {v1, v0}, Lcom/swarmconnect/ui/AsyncImageView;->gotImage(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
