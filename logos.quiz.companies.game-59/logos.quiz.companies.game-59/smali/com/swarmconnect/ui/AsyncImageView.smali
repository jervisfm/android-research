.class public Lcom/swarmconnect/ui/AsyncImageView;
.super Landroid/widget/LinearLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/ui/AsyncImageView$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ProgressBar;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/ImageView$ScaleType;

.field private d:I

.field public extra:Ljava/lang/String;

.field public imageView:Landroid/widget/ImageView;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    invoke-virtual {p0, p1}, Lcom/swarmconnect/ui/AsyncImageView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    invoke-virtual {p0, p1}, Lcom/swarmconnect/ui/AsyncImageView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    invoke-virtual {p0, p1}, Lcom/swarmconnect/ui/AsyncImageView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    const/4 v0, 0x0

    iput v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    invoke-virtual {p0, p1}, Lcom/swarmconnect/ui/AsyncImageView;->init(Landroid/content/Context;)V

    invoke-virtual {p0, p2}, Lcom/swarmconnect/ui/AsyncImageView;->getUrl(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/swarmconnect/ui/AsyncImageView;)I
    .locals 1

    iget v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    return v0
.end method

.method static synthetic b(Lcom/swarmconnect/ui/AsyncImageView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getUrl(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/swarmconnect/ui/AsyncImageView;->url:Ljava/lang/String;

    new-instance v0, Lcom/swarmconnect/ui/AsyncImageView$1;

    invoke-direct {v0, p0, p1}, Lcom/swarmconnect/ui/AsyncImageView$1;-><init>(Lcom/swarmconnect/ui/AsyncImageView;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/swarmconnect/utils/AsyncImage;->getImage(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;)V

    return-void
.end method

.method public gotImage(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_2

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    new-instance v0, Lcom/swarmconnect/ui/AsyncImageView$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/swarmconnect/ui/AsyncImageView$a;-><init>(Lcom/swarmconnect/ui/AsyncImageView;Lcom/swarmconnect/ui/AsyncImageView$a;)V

    const/16 v1, 0xa0

    invoke-virtual {v0, p1, v1}, Lcom/swarmconnect/ui/AsyncImageView$a;->a(Landroid/graphics/Bitmap;I)V

    :cond_0
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/swarmconnect/ui/AsyncImageView;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :goto_0
    invoke-virtual {p0}, Lcom/swarmconnect/ui/AsyncImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/swarmconnect/ui/AsyncImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    invoke-virtual {p0}, Lcom/swarmconnect/ui/AsyncImageView;->removeAllViews()V

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ui/AsyncImageView;->addView(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 3

    const/4 v2, -0x2

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ui/AsyncImageView;->setGravity(I)V

    iput-object p1, p0, Lcom/swarmconnect/ui/AsyncImageView;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/swarmconnect/ui/AsyncImageView;->removeAllViews()V

    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->a:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->a:Landroid/widget/ProgressBar;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->a:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/swarmconnect/ui/AsyncImageView;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setFailureImage(I)V
    .locals 0

    iput p1, p0, Lcom/swarmconnect/ui/AsyncImageView;->d:I

    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 1

    iput-object p1, p0, Lcom/swarmconnect/ui/AsyncImageView;->c:Landroid/widget/ImageView$ScaleType;

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swarmconnect/ui/AsyncImageView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_0
    return-void
.end method
