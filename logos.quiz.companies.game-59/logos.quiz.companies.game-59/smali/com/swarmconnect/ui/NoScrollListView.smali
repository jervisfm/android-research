.class public Lcom/swarmconnect/ui/NoScrollListView;
.super Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    const/4 v0, 0x0

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, p1, v1}, Landroid/widget/ListView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getVerticalFadingEdgeLength()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getCount()I

    move-result v3

    mul-int/2addr v1, v3

    add-int/2addr v2, v1

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getChildCount()I

    move-result v3

    if-lt v0, v3, :cond_0

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getListPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/swarmconnect/ui/NoScrollListView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/swarmconnect/ui/NoScrollListView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/swarmconnect/ui/NoScrollListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v1, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method
