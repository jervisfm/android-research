.class public Lcom/swarmconnect/ui/UiConf;
.super Ljava/lang/Object;


# static fields
.field public static final CORNER_RADIUS:I = 0x3

.field public static final altRowColor:I = -0x77000001

.field public static final blackTextColor:I = -0xddddde

.field public static final blueTextColor:I = -0xe67729

.field public static density:F = 0.0f

.field public static final errorTextColor:I = -0x70e5d3

.field public static final greyTextColor:I = -0x99999a


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/ui/UiConf;->density:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final blueButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 8

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x7800

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v0, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v1, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v2, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xf6t 0xabt 0x41t 0xfft
        0xd4t 0x85t 0x15t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0xfft 0xbbt 0x61t 0xfft
        0xe9t 0xa5t 0x35t 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0xd0t 0x8bt 0x21t 0xfft
        0xb4t 0x65t 0x5t 0xfft
    .end array-data
.end method

.method public static final buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;
    .locals 3

    const/4 v2, 0x2

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p0, :cond_0

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1, p0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p1, :cond_1

    new-array v1, v2, [I

    fill-array-data v1, :array_1

    invoke-virtual {v0, v1, p1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p2, :cond_2

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    invoke-virtual {v0, v1, p2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xa7t 0x0t 0x1t 0x1t
        0x64t 0xfft 0xfet 0xfet
    .end array-data

    :array_1
    .array-data 0x4
        0x59t 0xfft 0xfet 0xfet
        0x9ct 0x0t 0x1t 0x1t
    .end array-data

    :array_2
    .array-data 0x4
        0x59t 0xfft 0xfet 0xfet
        0x64t 0xfft 0xfet 0xfet
    .end array-data
.end method

.method public static final coinsProviderButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/high16 v4, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const v1, -0x424243

    invoke-virtual {v0, v5, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v3, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const v2, -0xe67729

    invoke-virtual {v1, v5, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 0x4
        0x33t 0x33t 0x33t 0xfft
        0x44t 0x44t 0x44t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0x55t 0x55t 0x55t 0xfft
        0x66t 0x66t 0x66t 0xfft
    .end array-data
.end method

.method public static final contextualGreyBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/high16 v1, 0x4040

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0x55t 0x55t 0x55t 0xfft
        0x55t 0x55t 0x55t 0xfft
    .end array-data
.end method

.method public static final customGradient(II)Landroid/graphics/drawable/GradientDrawable;
    .locals 4

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p0, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0
.end method

.method public static final customGradient(IIZ)Landroid/graphics/drawable/GradientDrawable;
    .locals 4

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p0, v2, v3

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    if-eqz p2, :cond_0

    const/high16 v1, 0x4040

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    :cond_0
    return-object v0
.end method

.method public static final dips(F)F
    .locals 1

    sget v0, Lcom/swarmconnect/ui/UiConf;->density:F

    mul-float/2addr v0, p0

    return v0
.end method

.method public static final facebookLoginButton()Landroid/graphics/drawable/StateListDrawable;
    .locals 7

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/high16 v5, -0x7800

    const/high16 v4, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v0, v6, v5}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v3, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v1, v6, v5}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v1, v0, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xcct 0xa0t 0x8bt 0xfft
        0x99t 0x5at 0x3ct 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0x79t 0x5dt 0x50t 0xfft
        0x5ct 0x37t 0x25t 0xfft
    .end array-data
.end method

.method public static final footerBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :array_0
    .array-data 0x4
        0xf2t 0xf2t 0xf2t 0xfft
        0xd1t 0xd1t 0xd1t 0xfft
    .end array-data
.end method

.method public static final footerButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/high16 v4, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const v1, -0x424243

    invoke-virtual {v0, v5, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v3, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const v2, -0x666667

    invoke-virtual {v1, v5, v2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v4}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 0x4
        0xf2t 0xf2t 0xf2t 0xfft
        0xd1t 0xd1t 0xd1t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0xd2t 0xd2t 0xd2t 0xfft
        0xb1t 0xb1t 0xb1t 0xfft
    .end array-data
.end method

.method public static final greenButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 8

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x7800

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v0, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v1, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v2, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0x1at 0xc0t 0x49t 0xfft
        0x14t 0x94t 0x38t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0x3ct 0xddt 0x69t 0xfft
        0x2et 0xaat 0x51t 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0x13t 0x81t 0x32t 0xfft
        0xft 0x63t 0x27t 0xfft
    .end array-data
.end method

.method public static final greyButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 8

    const/4 v4, 0x2

    const/4 v7, 0x1

    const/high16 v6, -0x7800

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v0, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v1, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-virtual {v2, v7, v6}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xf5t 0xf5t 0xf5t 0xfft
        0xbdt 0xbdt 0xbdt 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xcat 0xcat 0xcat 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0xbbt 0xbbt 0xbbt 0xfft
        0x8et 0x8et 0x8et 0xfft
    .end array-data
.end method

.method public static final headerBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :array_0
    .array-data 0x4
        0xf6t 0xabt 0x41t 0xfft
        0xd4t 0x85t 0x15t 0xfft
    .end array-data
.end method

.method public static final headerButtonPressedBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/high16 v1, 0x4040

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xb7t 0x7ct 0x2at 0xfft
        0xb7t 0x7ct 0x2at 0xfft
    .end array-data
.end method

.method public static final headerOfflineErrorBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :array_0
    .array-data 0x4
        0x12t 0xe0t 0xf2t 0xfft
        0x10t 0x9at 0xbdt 0xfft
    .end array-data
.end method

.method public static final loadingPopupBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/high16 v1, 0x4040

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0x33t 0x33t 0x33t 0xfft
        0x55t 0x55t 0x55t 0xfft
    .end array-data
.end method

.method public static final messageBackground()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/high16 v1, 0x4100

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0xfft 0xfft 0xfft 0xfft
        0xfft 0xfft 0xfft 0xfft
    .end array-data
.end method

.method public static final purchasePopupBuyButton()Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v4, 0x2

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0x21t 0xbdt 0x51t 0xfft
        0x21t 0x9at 0x41t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0x41t 0xddt 0x71t 0xfft
        0x41t 0xaat 0x61t 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0x1t 0x9dt 0x31t 0xfft
        0x1t 0x7at 0x21t 0xfft
    .end array-data
.end method

.method public static final purchasePopupCancelButton()Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v4, 0x2

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    const/high16 v2, 0x4000

    invoke-static {v2}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    float-to-int v2, v2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    :array_0
    .array-data 0x4
        0x55t 0x55t 0x55t 0xfft
        0x55t 0x55t 0x55t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0xfft 0xeat 0xact 0xfft
        0xfft 0xdet 0x9ct 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0x44t 0x44t 0x44t 0xfft
        0x44t 0x44t 0x44t 0xfft
    .end array-data
.end method

.method public static final purchasePopupLightBlue()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :array_0
    .array-data 0x4
        0x88t 0x88t 0x88t 0xfft
        0x99t 0x99t 0x99t 0xfft
    .end array-data
.end method

.method public static final purchasePopupMedBlue()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    return-object v0

    :array_0
    .array-data 0x4
        0x9ct 0x7dt 0x4ft 0xfft
        0xb6t 0x92t 0x5ct 0xfft
    .end array-data
.end method

.method public static final purchasePopupMedBlueRound()Landroid/graphics/drawable/GradientDrawable;
    .locals 2

    invoke-static {}, Lcom/swarmconnect/ui/UiConf;->purchasePopupMedBlue()Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    const/high16 v1, 0x4040

    invoke-static {v1}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    return-object v0
.end method

.method public static final subHeaderButtonBackground()Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v4, 0x2

    const/high16 v5, 0x4040

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v3, v4, [I

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    invoke-static {v5}, Lcom/swarmconnect/ui/UiConf;->dips(F)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    invoke-static {v2, v1, v0}, Lcom/swarmconnect/ui/UiConf;->buttonStates(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 0x4
        0x44t 0x44t 0x44t 0xfft
        0x55t 0x55t 0x55t 0xfft
    .end array-data

    :array_1
    .array-data 0x4
        0xfft 0xeat 0xact 0xfft
        0xfft 0xdet 0x9ct 0xfft
    .end array-data

    :array_2
    .array-data 0x4
        0x44t 0x44t 0x44t 0xfft
        0x44t 0x44t 0x44t 0xfft
    .end array-data
.end method
