.class public Lcom/swarmconnect/utils/AsyncHttp;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;,
        Lcom/swarmconnect/utils/AsyncHttp$b;,
        Lcom/swarmconnect/utils/AsyncHttp$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBlocking(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/swarmconnect/utils/AsyncHttp;->getBlocking(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;Landroid/os/Handler;)V

    return-void
.end method

.method public static getBlocking(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;Landroid/os/Handler;)V
    .locals 3

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/params/HttpParams;)V

    const/16 v2, 0x2000

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    new-instance v2, Lorg/apache/http/impl/client/BasicResponseHandler;

    invoke-direct {v2}, Lorg/apache/http/impl/client/BasicResponseHandler;-><init>()V

    :try_start_0
    invoke-virtual {v1, v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    new-instance v1, Lcom/swarmconnect/utils/AsyncHttp$a;

    invoke-direct {v1, v0, p1}, Lcom/swarmconnect/utils/AsyncHttp$a;-><init>(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V

    invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, v0}, Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;->gotURL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_2

    new-instance v1, Lcom/swarmconnect/utils/AsyncHttp$b;

    invoke-direct {v1, v0, p1}, Lcom/swarmconnect/utils/AsyncHttp$b;-><init>(Ljava/lang/Exception;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V

    invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v0}, Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;->requestFailed(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static getURL(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/swarmconnect/utils/AsyncHttp;->getURL(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V

    return-void
.end method

.method public static getURL(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;)V
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/swarmconnect/utils/AsyncHttp$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/swarmconnect/utils/AsyncHttp$1;-><init>(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncHttp$AsyncCB;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method
