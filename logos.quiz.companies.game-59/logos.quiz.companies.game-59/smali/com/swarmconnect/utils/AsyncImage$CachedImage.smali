.class public Lcom/swarmconnect/utils/AsyncImage$CachedImage;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swarmconnect/utils/AsyncImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CachedImage"
.end annotation


# instance fields
.field public image:Landroid/graphics/Bitmap;

.field public timestamp:J

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->image:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->url:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->timestamp:J

    return-void
.end method
