.class public Lcom/swarmconnect/utils/AsyncImage;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;,
        Lcom/swarmconnect/utils/AsyncImage$CachedImage;,
        Lcom/swarmconnect/utils/AsyncImage$a;,
        Lcom/swarmconnect/utils/AsyncImage$b;
    }
.end annotation


# static fields
.field public static final CACHE_EXPIRE:J = 0x36ee80L

.field private static a:I

.field private static b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/swarmconnect/utils/AsyncImage$CachedImage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearCachedImages()V
    .locals 1

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    const/4 v0, 0x0

    sput v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    return-void
.end method

.method public static getBlocking(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;Landroid/os/Handler;)V
    .locals 9

    const/4 v3, 0x0

    :try_start_0
    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;

    if-eqz v0, :cond_5

    iget-wide v1, v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->timestamp:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    sub-long/2addr v4, v6

    cmp-long v1, v1, v4

    if-lez v1, :cond_4

    iget-object v0, v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->image:Landroid/graphics/Bitmap;

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-static {p0}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    new-instance v1, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-direct {v1, v0}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v1}, Lorg/apache/http/entity/BufferedHttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    new-instance v2, Lcom/swarmconnect/utils/AsyncImage$CachedImage;

    invoke-direct {v2, v1, p0}, Lcom/swarmconnect/utils/AsyncImage$CachedImage;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v2, v4

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    sput v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    sget v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    int-to-long v4, v0

    const-wide/32 v6, 0x3d0900

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    :cond_0
    :goto_1
    sget v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    int-to-long v4, v0

    const-wide/32 v6, 0x2625a0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    :cond_1
    move-object v0, v1

    :cond_2
    if-eqz p1, :cond_3

    if-eqz p2, :cond_a

    new-instance v1, Lcom/swarmconnect/utils/AsyncImage$b;

    invoke-direct {v1, v0, p1}, Lcom/swarmconnect/utils/AsyncImage$b;-><init>(Landroid/graphics/Bitmap;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;)V

    invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    :goto_2
    return-void

    :cond_4
    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object v0, v3

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    if-eqz v2, :cond_0

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->url:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/swarmconnect/utils/AsyncImage;->a:I

    iget-object v4, v2, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v2, v2, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    mul-int/2addr v2, v4

    mul-int/lit8 v2, v2, 0x4

    sub-int/2addr v0, v2

    sput v0, Lcom/swarmconnect/utils/AsyncImage;->a:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_b

    new-instance v1, Lcom/swarmconnect/utils/AsyncImage$a;

    invoke-direct {v1, v0, p1}, Lcom/swarmconnect/utils/AsyncImage$a;-><init>(Ljava/lang/Exception;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;)V

    invoke-virtual {p2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_8
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v5, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;

    if-eqz v2, :cond_9

    if-eqz v0, :cond_7

    iget-wide v5, v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->timestamp:J

    iget-wide v7, v2, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->timestamp:J

    cmp-long v5, v5, v7

    if-gez v5, :cond_7

    :cond_9
    move-object v2, v0

    goto :goto_3

    :cond_a
    invoke-virtual {p1, v0}, Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;->gotImage(Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :cond_b
    invoke-virtual {p1, v0}, Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;->requestFailed(Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public static getImage(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;)V
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v1, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->timestamp:J

    iget-object v0, v0, Lcom/swarmconnect/utils/AsyncImage$CachedImage;->image:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0}, Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;->gotImage(Landroid/graphics/Bitmap;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/swarmconnect/utils/AsyncImage$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/swarmconnect/utils/AsyncImage$1;-><init>(Ljava/lang/String;Lcom/swarmconnect/utils/AsyncImage$AsyncImageCB;Landroid/os/Handler;)V

    invoke-static {v1}, Lcom/swarmconnect/utils/ThreadPool;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public static removeImage(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/utils/AsyncImage;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
