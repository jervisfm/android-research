.class public Lcom/swarmconnect/utils/DeviceUtils;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swarmconnect/utils/DeviceUtils$SerialGetter;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    :try_start_1
    new-instance v0, Lcom/swarmconnect/utils/DeviceUtils$SerialGetter;

    invoke-direct {v0}, Lcom/swarmconnect/utils/DeviceUtils$SerialGetter;-><init>()V

    invoke-virtual {v0}, Lcom/swarmconnect/utils/DeviceUtils$SerialGetter;->getSerial()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    const-string v1, "unknown"

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    :goto_1
    :try_start_2
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    const-string v1, "0+"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/swarmconnect/utils/DeviceUtils;->a:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :cond_3
    const-string v0, ""

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method
