.class Llogo/quiz/commons/AdserwerCommons$2;
.super Ljava/lang/Object;
.source "AdserwerCommons.java"

# interfaces
.implements Lcom/tapjoy/TapjoyDisplayAdNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/AdserwerCommons;->getTapjoyAd(ZLandroid/widget/RelativeLayout;Llogo/quiz/commons/TapjoyKeys;Landroid/app/Activity;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$adContainer:Landroid/widget/RelativeLayout;

.field private final synthetic val$mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/widget/RelativeLayout;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/AdserwerCommons$2;->val$mHandler:Landroid/os/Handler;

    iput-object p2, p0, Llogo/quiz/commons/AdserwerCommons$2;->val$adContainer:Landroid/widget/RelativeLayout;

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayAdResponse(Landroid/view/View;)V
    .locals 8
    .parameter "adView"

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v1, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 92
    .local v1, ad_width:I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 94
    .local v0, ad_height:I
    const-string v5, "EASY_APP"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "adView dimensions: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v5, p0, Llogo/quiz/commons/AdserwerCommons$2;->val$adContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v2

    .line 99
    .local v2, desired_width:I
    if-le v2, v1, :cond_0

    .line 100
    move v2, v1

    .line 104
    :cond_0
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    mul-int v5, v2, v0

    div-int/2addr v5, v1

    invoke-direct {v3, v2, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 105
    .local v3, layout:Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    new-instance v4, Landroid/os/Message;

    invoke-direct {v4}, Landroid/os/Message;-><init>()V

    .line 109
    .local v4, message:Landroid/os/Message;
    iput-object p1, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 110
    iget-object v5, p0, Llogo/quiz/commons/AdserwerCommons$2;->val$mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 111
    return-void
.end method

.method public getDisplayAdResponseFailed(Ljava/lang/String;)V
    .locals 2
    .parameter "error"

    .prologue
    .line 84
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 85
    .local v0, message:Landroid/os/Message;
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 86
    iget-object v1, p0, Llogo/quiz/commons/AdserwerCommons$2;->val$mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 87
    return-void
.end method
