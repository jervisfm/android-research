.class public abstract Llogo/quiz/commons/AdserwerCommons;
.super Ljava/lang/Object;
.source "AdserwerCommons.java"


# static fields
.field private static final AD_SYSTEM:Ljava/lang/String; = "adSystem"

.field public static final FREE_HINTS_FOR_FACEBOOK_LIKE:I = 0x3

.field public static final FREE_HINTS_FOR_INSTALL_APP:I = 0x5

.field public static final FREE_HINTS_FOR_RATE_APP:I = 0x3

.field public static adId:Ljava/lang/String;

.field public static adUrl:Ljava/lang/String;

.field static final flipAnimator:Llogo/quiz/commons/FlipAnimator;

.field private static myAds:[Llogo/quiz/commons/MyAdCommons;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 35
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    sput-object v0, Llogo/quiz/commons/AdserwerCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 40
    const-string v0, "market://details?id=flag.quiz.world.national.names.learn"

    sput-object v0, Llogo/quiz/commons/AdserwerCommons;->adUrl:Ljava/lang/String;

    .line 41
    const-string v0, "ad3"

    sput-object v0, Llogo/quiz/commons/AdserwerCommons;->adId:Ljava/lang/String;

    .line 42
    const/16 v0, 0x8

    new-array v0, v0, [Llogo/quiz/commons/MyAdCommons;

    const/4 v1, 0x0

    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->music_bands_logo_quiz_promo:I

    const-string v4, "ad7"

    const-string v5, "market://details?id=logo.quiz.music.bands.game"

    const-string v6, "Music Bands Logo Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 43
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->car_logo_promo:I

    const-string v4, "ad1"

    const-string v5, "market://details?id=logo.quiz.car.game"

    const-string v6, "Car Logo Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 44
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->logo_quiz_football_promo:I

    const-string v4, "ad10"

    const-string v5, "market://details?id=logos.quiz.football.soccer.clubs"

    const-string v6, "Football Logo Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 46
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->flag_quiz_promo:I

    const-string v4, "ad2"

    const-string v5, "market://details?id=flag.quiz.world.national.names.learn"

    const-string v6, "Flag Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 47
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->capitals_quiz_promo:I

    const-string v4, "ad3"

    const-string v5, "market://details?id=capitals.quiz.world.national.names.learn"

    const-string v6, "Capitals Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 48
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->scare_promo:I

    const-string v4, "ad4"

    const-string v5, "market://details?id=scare.your.friends.prank.maze.halloween"

    const-string v6, "Scare your friends"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 49
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->classic_logo_quiz_promo:I

    const-string v4, "ad9"

    const-string v5, "market://details?id=logos.quiz.companies.game"

    const-string v6, "Classic Logo Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 50
    new-instance v2, Llogo/quiz/commons/MyAdCommons;

    sget v3, Llogo/quiz/commons/R$drawable;->logo_quiz_by_category_promo:I

    const-string v4, "ad8"

    const-string v5, "market://details?id=logo.quiz.game.category"

    const-string v6, "Ultimate Logo Quiz"

    invoke-direct {v2, v3, v4, v5, v6}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    .line 42
    sput-object v0, Llogo/quiz/commons/AdserwerCommons;->myAds:[Llogo/quiz/commons/MyAdCommons;

    .line 32
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/app/Activity;)V
    .locals 0
    .parameter

    .prologue
    .line 134
    invoke-static {p0}, Llogo/quiz/commons/AdserwerCommons;->initFlipAnimator(Landroid/app/Activity;)V

    return-void
.end method

.method public static getActiveAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .parameter "context"
    .parameter "removeAdID"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/MyAdCommons;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 188
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 189
    .local v7, settings:Landroid/content/SharedPreferences;
    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 191
    .local v3, editor:Landroid/content/SharedPreferences$Editor;
    const-string v8, "firstRun"

    invoke-interface {v7, v8, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 193
    .local v4, firstRun:Z
    const-string v8, "1hint"

    invoke-interface {v7, v8, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 194
    const-string v8, "1hint"

    invoke-interface {v3, v8, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 195
    const-string v8, "Nice! You get new hint!"

    invoke-static {p0, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 198
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v0, activeMyAds:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    const/4 v5, 0x0

    .line 200
    .local v5, i:I
    invoke-static {p0, p1}, Llogo/quiz/commons/AdserwerCommons;->getAllAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 217
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 219
    return-object v0

    .line 200
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llogo/quiz/commons/MyAdCommons;

    .line 201
    .local v1, ad:Llogo/quiz/commons/MyAdCommons;
    invoke-virtual {v1}, Llogo/quiz/commons/MyAdCommons;->getAdUrl()Ljava/lang/String;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    aget-object v6, v9, v12

    .line 202
    .local v6, packageName:Ljava/lang/String;
    invoke-static {v6, p0}, Llogo/quiz/commons/DeviceUtilCommons;->isPackageExists(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v1}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_3

    .line 203
    invoke-virtual {v1}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v3, v9, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 204
    if-eqz v4, :cond_4

    .line 205
    const-string v9, "firstRun"

    invoke-interface {v3, v9, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 212
    :cond_3
    :goto_1
    invoke-virtual {v1}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-nez v9, :cond_1

    .line 213
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    :cond_4
    const-string v9, "allHints"

    invoke-interface {v7, v9, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 208
    .local v2, allHintsCount:I
    const-string v9, "allHints"

    add-int/lit8 v10, v2, 0x5

    invoke-interface {v3, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method

.method public static getAdDelayMillis(Landroid/content/Context;)I
    .locals 1
    .parameter "context"

    .prologue
    .line 233
    const/16 v0, 0xa

    .line 248
    .local v0, adDelayMillis:I
    return v0
.end method

.method public static getAdmob(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/ads/AdView;
    .locals 4
    .parameter "myActivity"
    .parameter "ADMOB_PUB_ID"

    .prologue
    .line 125
    new-instance v0, Lcom/google/ads/AdView;

    sget-object v1, Lcom/google/ads/AdSize;->BANNER:Lcom/google/ads/AdSize;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    .line 126
    .local v0, adView:Lcom/google/ads/AdView;
    check-cast p0, Lcom/google/ads/AdListener;

    .end local p0
    invoke-virtual {v0, p0}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 127
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/ads/AdView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/ads/AdView;->setGravity(I)V

    .line 129
    new-instance v1, Lcom/google/ads/AdRequest;

    invoke-direct {v1}, Lcom/google/ads/AdRequest;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 131
    return-object v0
.end method

.method public static getAllAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .parameter "context"
    .parameter "removeAdID"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/MyAdCommons;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v1, ads:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    sget-object v3, Llogo/quiz/commons/AdserwerCommons;->myAds:[Llogo/quiz/commons/MyAdCommons;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 229
    return-object v1

    .line 224
    :cond_0
    aget-object v0, v3, v2

    .line 225
    .local v0, ad:Llogo/quiz/commons/MyAdCommons;
    invoke-virtual {v0}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 226
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getPromoIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 5
    .parameter "context"

    .prologue
    .line 142
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 143
    .local v1, intent:Landroid/content/Intent;
    sget-object v3, Llogo/quiz/commons/AdserwerCommons;->adUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 144
    const/high16 v3, 0x4000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 147
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 148
    .local v2, settings:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 149
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    sget-object v3, Llogo/quiz/commons/AdserwerCommons;->adId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 150
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 152
    return-object v1
.end method

.method public static getTapjoyAd(ZLandroid/widget/RelativeLayout;Llogo/quiz/commons/TapjoyKeys;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 9
    .parameter "isNonRewarded"
    .parameter "adContainer"
    .parameter "tapjoyKeys"
    .parameter "myActivity"
    .parameter "ADMOB_PUB_ID"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 55
    invoke-virtual {p3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 56
    .local v4, settings:Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v7, "adSystem"

    invoke-interface {v4, v7, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_1

    if-eqz p4, :cond_1

    move v1, v5

    .line 59
    .local v1, isAdmob:Z
    :goto_0
    if-eqz p2, :cond_0

    if-eqz v1, :cond_2

    .line 60
    :cond_0
    invoke-static {p3, p4}, Llogo/quiz/commons/AdserwerCommons;->getAdmob(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/ads/AdView;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 62
    const-string v6, "adSystem"

    invoke-interface {v0, v6, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 121
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 122
    return-void

    .end local v1           #isAdmob:Z
    :cond_1
    move v1, v6

    .line 58
    goto :goto_0

    .line 64
    .restart local v1       #isAdmob:Z
    :cond_2
    new-instance v2, Llogo/quiz/commons/AdserwerCommons$1;

    invoke-direct {v2, p3, p4, p1}, Llogo/quiz/commons/AdserwerCommons$1;-><init>(Landroid/app/Activity;Ljava/lang/String;Landroid/widget/RelativeLayout;)V

    .line 80
    .local v2, mHandler:Landroid/os/Handler;
    invoke-virtual {p3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p2}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v7, v8}, Lcom/tapjoy/TapjoyConnect;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    new-instance v3, Llogo/quiz/commons/AdserwerCommons$2;

    invoke-direct {v3, v2, p1}, Llogo/quiz/commons/AdserwerCommons$2;-><init>(Landroid/os/Handler;Landroid/widget/RelativeLayout;)V

    .line 113
    .local v3, notifier:Lcom/tapjoy/TapjoyDisplayAdNotifier;
    if-eqz p0, :cond_3

    .line 114
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v5

    invoke-virtual {p2}, Llogo/quiz/commons/TapjoyKeys;->getCurrencyId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7, v3}, Lcom/tapjoy/TapjoyConnect;->getDisplayAdWithCurrencyID(Ljava/lang/String;Lcom/tapjoy/TapjoyDisplayAdNotifier;)V

    .line 118
    :goto_2
    const-string v5, "adSystem"

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 116
    :cond_3
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/tapjoy/TapjoyConnect;->getDisplayAd(Lcom/tapjoy/TapjoyDisplayAdNotifier;)V

    goto :goto_2
.end method

.method private static initFlipAnimator(Landroid/app/Activity;)V
    .locals 3
    .parameter "myActivity"

    .prologue
    .line 135
    sget-object v0, Llogo/quiz/commons/AdserwerCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 136
    sget-object v0, Llogo/quiz/commons/AdserwerCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 137
    sget-object v0, Llogo/quiz/commons/AdserwerCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 138
    sget-object v0, Llogo/quiz/commons/AdserwerCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Llogo/quiz/commons/DeviceUtilCommons;->getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FlipAnimator;->setmCenterX(F)V

    .line 139
    return-void
.end method

.method public static setAd(Landroid/widget/ImageView;Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .parameter "myAd"
    .parameter "context"
    .parameter "removeAdID"

    .prologue
    .line 156
    invoke-static {p1}, Llogo/quiz/commons/DeviceUtilCommons;->checkInfo(Landroid/content/Context;)V

    .line 158
    if-eqz p0, :cond_0

    .line 159
    invoke-static {p1, p2}, Llogo/quiz/commons/AdserwerCommons;->getActiveAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 161
    .local v0, activeMyAds:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 162
    .local v1, bmpFactory:Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x0

    iput-boolean v5, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 164
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 165
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    int-to-long v7, v7

    rem-long/2addr v5, v7

    long-to-int v4, v5

    .line 166
    .local v4, randElem:I
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llogo/quiz/commons/MyAdCommons;

    .line 168
    .local v3, randAd:Llogo/quiz/commons/MyAdCommons;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v3}, Llogo/quiz/commons/MyAdCommons;->getImageResource()I

    move-result v6

    invoke-static {v5, v6, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 170
    invoke-virtual {v3}, Llogo/quiz/commons/MyAdCommons;->getAdUrl()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adUrl:Ljava/lang/String;

    .line 171
    invoke-virtual {v3}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adId:Ljava/lang/String;

    .line 185
    .end local v0           #activeMyAds:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    .end local v1           #bmpFactory:Landroid/graphics/BitmapFactory$Options;
    .end local v3           #randAd:Llogo/quiz/commons/MyAdCommons;
    .end local v4           #randElem:I
    :cond_0
    :goto_0
    return-void

    .line 173
    .restart local v0       #activeMyAds:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    .restart local v1       #bmpFactory:Landroid/graphics/BitmapFactory$Options;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Llogo/quiz/commons/R$drawable;->flag_quiz_promo:I

    invoke-static {v5, v6, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 175
    const-string v5, "market://details?id=flag.quiz.world.national.names.learn"

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adUrl:Ljava/lang/String;

    .line 176
    const-string v5, "ad1"

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v2

    .line 179
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Llogo/quiz/commons/R$drawable;->flag_quiz_promo:I

    invoke-static {v5, v6, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 181
    const-string v5, "market://details?id=flag.quiz.world.national.names.learn"

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adUrl:Ljava/lang/String;

    .line 182
    const-string v5, "ad1"

    sput-object v5, Llogo/quiz/commons/AdserwerCommons;->adId:Ljava/lang/String;

    goto :goto_0
.end method
