.class public Llogo/quiz/commons/BrandTO;
.super Ljava/lang/Object;
.source "BrandTO.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public brandPosition:I

.field public category:Ljava/lang/String;

.field public complete:Z

.field public drawable:I

.field public hint1:Ljava/lang/String;

.field public hint2:Ljava/lang/String;

.field public hint3:Ljava/lang/String;

.field public level:I

.field public names:[Ljava/lang/String;

.field public wikipediaLink:Ljava/lang/String;


# direct methods
.method public constructor <init>(II[Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "brandPosition"
    .parameter "drawable"
    .parameter "names"
    .parameter "complete"
    .parameter "level"
    .parameter "category"
    .parameter "hint1"
    .parameter "hint2"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 35
    iput p1, p0, Llogo/quiz/commons/BrandTO;->brandPosition:I

    .line 36
    iput p2, p0, Llogo/quiz/commons/BrandTO;->drawable:I

    .line 37
    iput-object p3, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    .line 38
    iput-boolean p4, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 39
    iput p5, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 40
    iput-object p6, p0, Llogo/quiz/commons/BrandTO;->category:Ljava/lang/String;

    .line 41
    iput-object p7, p0, Llogo/quiz/commons/BrandTO;->hint1:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Llogo/quiz/commons/BrandTO;->hint2:Ljava/lang/String;

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(II[Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "brandPosition"
    .parameter "drawable"
    .parameter "names"
    .parameter "complete"
    .parameter "level"
    .parameter "category"
    .parameter "hint1"
    .parameter "hint2"
    .parameter "wikipediaLink"

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 48
    iput p1, p0, Llogo/quiz/commons/BrandTO;->brandPosition:I

    .line 49
    iput p2, p0, Llogo/quiz/commons/BrandTO;->drawable:I

    .line 50
    iput-object p3, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    .line 51
    iput-boolean p4, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 52
    iput p5, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 53
    iput-object p6, p0, Llogo/quiz/commons/BrandTO;->category:Ljava/lang/String;

    .line 54
    iput-object p7, p0, Llogo/quiz/commons/BrandTO;->hint1:Ljava/lang/String;

    .line 55
    iput-object p8, p0, Llogo/quiz/commons/BrandTO;->hint2:Ljava/lang/String;

    .line 56
    iput-object p9, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(II[Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "brandPosition"
    .parameter "drawable"
    .parameter "names"
    .parameter "complete"
    .parameter "level"
    .parameter "category"
    .parameter "hint1"
    .parameter "hint2"
    .parameter "hint3"
    .parameter "wikipediaLink"

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 12
    const/4 v0, 0x1

    iput v0, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 21
    iput p1, p0, Llogo/quiz/commons/BrandTO;->brandPosition:I

    .line 22
    iput p2, p0, Llogo/quiz/commons/BrandTO;->drawable:I

    .line 23
    iput-object p3, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    .line 24
    iput-boolean p4, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 25
    iput p5, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 26
    iput-object p6, p0, Llogo/quiz/commons/BrandTO;->category:Ljava/lang/String;

    .line 27
    iput-object p7, p0, Llogo/quiz/commons/BrandTO;->hint1:Ljava/lang/String;

    .line 28
    iput-object p8, p0, Llogo/quiz/commons/BrandTO;->hint2:Ljava/lang/String;

    .line 29
    iput-object p9, p0, Llogo/quiz/commons/BrandTO;->hint3:Ljava/lang/String;

    .line 30
    iput-object p10, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getBrandPosition()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Llogo/quiz/commons/BrandTO;->brandPosition:I

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getDrawable()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Llogo/quiz/commons/BrandTO;->drawable:I

    return v0
.end method

.method public getHint1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->hint1:Ljava/lang/String;

    return-object v0
.end method

.method public getHint2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->hint2:Ljava/lang/String;

    return-object v0
.end method

.method public getHint3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->hint3:Ljava/lang/String;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Llogo/quiz/commons/BrandTO;->level:I

    return v0
.end method

.method public getNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    return-object v0
.end method

.method public getWikipediaLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    return-object v0
.end method

.method public getbrandName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    const-string v0, ""

    .line 144
    .local v0, brandName:Ljava/lang/String;
    iget-object v1, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 145
    iget-object v1, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 147
    :cond_0
    return-object v0
.end method

.method public hasWikipediaLink()Z
    .locals 3

    .prologue
    .line 129
    const/4 v0, 0x0

    .line 131
    .local v0, hasWikipediaLink:Z
    iget-object v1, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    const/4 v0, 0x1

    .line 135
    :cond_0
    return v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    return v0
.end method

.method public setBrandPosition(I)V
    .locals 0
    .parameter "brandPosition"

    .prologue
    .line 96
    iput p1, p0, Llogo/quiz/commons/BrandTO;->brandPosition:I

    .line 97
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .parameter "category"

    .prologue
    .line 64
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->category:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setComplete(Z)V
    .locals 0
    .parameter "complete"

    .prologue
    .line 115
    iput-boolean p1, p0, Llogo/quiz/commons/BrandTO;->complete:Z

    .line 116
    return-void
.end method

.method public setDrawable(I)V
    .locals 0
    .parameter "drawable"

    .prologue
    .line 103
    iput p1, p0, Llogo/quiz/commons/BrandTO;->drawable:I

    .line 104
    return-void
.end method

.method public setHint1(Ljava/lang/String;)V
    .locals 0
    .parameter "hint1"

    .prologue
    .line 72
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->hint1:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setHint2(Ljava/lang/String;)V
    .locals 0
    .parameter "hint2"

    .prologue
    .line 80
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->hint2:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setHint3(Ljava/lang/String;)V
    .locals 0
    .parameter "hint3"

    .prologue
    .line 88
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->hint3:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setLevel(I)V
    .locals 0
    .parameter "level"

    .prologue
    .line 121
    iput p1, p0, Llogo/quiz/commons/BrandTO;->level:I

    .line 122
    return-void
.end method

.method public setNames([Ljava/lang/String;)V
    .locals 0
    .parameter "names"

    .prologue
    .line 109
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->names:[Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setWikipediaLink(Ljava/lang/String;)V
    .locals 0
    .parameter "wikipediaLink"

    .prologue
    .line 139
    iput-object p1, p0, Llogo/quiz/commons/BrandTO;->wikipediaLink:Ljava/lang/String;

    .line 140
    return-void
.end method
