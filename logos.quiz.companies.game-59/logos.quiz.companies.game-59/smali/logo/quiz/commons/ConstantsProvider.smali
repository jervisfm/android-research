.class public interface abstract Llogo/quiz/commons/ConstantsProvider;
.super Ljava/lang/Object;
.source "ConstantsProvider.java"


# virtual methods
.method public abstract getAdmobPubId()Ljava/lang/String;
.end method

.method public abstract getAdmobRemoveAdId()Ljava/lang/String;
.end method

.method public abstract getChartboostAppId()Ljava/lang/String;
.end method

.method public abstract getChartboostAppSignature()Ljava/lang/String;
.end method

.method public abstract getFlurryKey()Ljava/lang/String;
.end method

.method public abstract getLeaderboard()Lcom/swarmconnect/SwarmLeaderboard;
.end method

.method public abstract getSwarmconnectAppId()I
.end method

.method public abstract getSwarmconnectAppKey()Ljava/lang/String;
.end method

.method public abstract getSwarmconnectHighScoreId()I
.end method

.method public abstract getTapJoyAppCurrencyId()Ljava/lang/String;
.end method

.method public abstract getTapJoyAppId()Ljava/lang/String;
.end method

.method public abstract getTapJoyAppSecret()Ljava/lang/String;
.end method

.method public abstract getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;
.end method

.method public abstract setLeaderboard(Lcom/swarmconnect/SwarmLeaderboard;)V
.end method
