.class Llogo/quiz/commons/FormActivityCommons$1;
.super Landroid/os/Handler;
.source "FormActivityCommons.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FormActivityCommons;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Llogo/quiz/commons/FormActivityCommons;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Llogo/quiz/commons/FormActivityCommons;Landroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iput-object p2, p0, Llogo/quiz/commons/FormActivityCommons$1;->val$activity:Landroid/app/Activity;

    .line 127
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 130
    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v3}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Llogo/quiz/commons/DeviceUtilCommons;->checkInfo(Landroid/content/Context;)V

    .line 131
    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    sget v4, Llogo/quiz/commons/R$id;->hintsCountForm:I

    invoke-virtual {v3, v4}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v6, p0, Llogo/quiz/commons/FormActivityCommons$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v5, v6}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " hints"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;
    invoke-static {v3}, Llogo/quiz/commons/FormActivityCommons;->access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 134
    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v3}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 137
    .local v2, settings:Landroid/content/SharedPreferences;
    const-string v3, "allHints"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 138
    .local v0, allHintsCount:I
    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$1;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;
    invoke-static {v3}, Llogo/quiz/commons/FormActivityCommons;->access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;

    move-result-object v3

    sget v4, Llogo/quiz/commons/R$id;->hintsCount:I

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 139
    .local v1, hintAllCount:Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "You have "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hint"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    const-string v3, ""

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    .end local v0           #allHintsCount:I
    .end local v1           #hintAllCount:Landroid/widget/TextView;
    .end local v2           #settings:Landroid/content/SharedPreferences;
    :cond_0
    return-void

    .line 139
    .restart local v0       #allHintsCount:I
    .restart local v1       #hintAllCount:Landroid/widget/TextView;
    .restart local v2       #settings:Landroid/content/SharedPreferences;
    :cond_1
    const-string v3, "s"

    goto :goto_0
.end method
