.class Llogo/quiz/commons/FormActivityCommons$5;
.super Ljava/lang/Object;
.source "FormActivityCommons.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FormActivityCommons;->hint(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Llogo/quiz/commons/FormActivityCommons;

.field private final synthetic val$brandTO:Llogo/quiz/commons/BrandTO;


# direct methods
.method constructor <init>(Llogo/quiz/commons/FormActivityCommons;Llogo/quiz/commons/BrandTO;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FormActivityCommons$5;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iput-object p2, p0, Llogo/quiz/commons/FormActivityCommons$5;->val$brandTO:Llogo/quiz/commons/BrandTO;

    .line 465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 468
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 469
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "image/png"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 470
    const/high16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 472
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "android.resource://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$5;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v3}, Llogo/quiz/commons/FormActivityCommons;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Llogo/quiz/commons/FormActivityCommons$5;->val$brandTO:Llogo/quiz/commons/BrandTO;

    invoke-virtual {v3}, Llogo/quiz/commons/BrandTO;->getDrawable()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 473
    .local v0, brandUri:Landroid/net/Uri;
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 476
    const-string v2, "android.intent.extra.SUBJECT"

    const-string v3, "Need help in logo quiz game"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 477
    const-string v2, "android.intent.extra.TEXT"

    const-string v3, "Does anyone know this logo? Logo quiz game http://goo.gl/bekzQ"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 479
    iget-object v2, p0, Llogo/quiz/commons/FormActivityCommons$5;->this$0:Llogo/quiz/commons/FormActivityCommons;

    const-string v3, "Ask on:"

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Llogo/quiz/commons/FormActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 480
    return-void
.end method
