.class Llogo/quiz/commons/FormActivityCommons$6;
.super Ljava/lang/Object;
.source "FormActivityCommons.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FormActivityCommons;->hint(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Llogo/quiz/commons/FormActivityCommons;

.field private final synthetic val$brandTO:Llogo/quiz/commons/BrandTO;

.field private final synthetic val$hintButton:Landroid/widget/Button;

.field private final synthetic val$hintLayout:Landroid/widget/LinearLayout;

.field private final synthetic val$settings:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Llogo/quiz/commons/FormActivityCommons;Landroid/content/SharedPreferences;Llogo/quiz/commons/BrandTO;Landroid/widget/Button;Landroid/widget/LinearLayout;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iput-object p2, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$settings:Landroid/content/SharedPreferences;

    iput-object p3, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$brandTO:Llogo/quiz/commons/BrandTO;

    iput-object p4, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$hintButton:Landroid/widget/Button;

    iput-object p5, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$hintLayout:Landroid/widget/LinearLayout;

    .line 485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .parameter "v"

    .prologue
    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 488
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$settings:Landroid/content/SharedPreferences;

    const-string v10, "allHints"

    invoke-interface {v9, v10, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 489
    .local v0, allHintsCount:I
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$brandTO:Llogo/quiz/commons/BrandTO;

    iget-object v10, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v10}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v11}, Llogo/quiz/commons/FormActivityCommons;->isCategoryHint()Z

    move-result v11

    invoke-static {v9, v10, v11}, Llogo/quiz/commons/HintsUtil;->getAllHintsForBrand(Llogo/quiz/commons/BrandTO;Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    .line 490
    .local v1, allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #calls: Llogo/quiz/commons/FormActivityCommons;->availibleHints(Ljava/util/List;)I
    invoke-static {v9, v1}, Llogo/quiz/commons/FormActivityCommons;->access$1(Llogo/quiz/commons/FormActivityCommons;Ljava/util/List;)I

    move-result v2

    .line 492
    .local v2, availibleHints:I
    if-nez v2, :cond_1

    .line 493
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v9, v9, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "All hints for this brand have been used"

    invoke-static {v9, v10, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 494
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #calls: Llogo/quiz/commons/FormActivityCommons;->wrongSound()V
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$2(Llogo/quiz/commons/FormActivityCommons;)V

    .line 495
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #calls: Llogo/quiz/commons/FormActivityCommons;->vibrateNegative()V
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$3(Llogo/quiz/commons/FormActivityCommons;)V

    .line 496
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v10, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$hintButton:Landroid/widget/Button;

    #calls: Llogo/quiz/commons/FormActivityCommons;->shakeView(Landroid/view/View;)V
    invoke-static {v9, v10}, Llogo/quiz/commons/FormActivityCommons;->access$4(Llogo/quiz/commons/FormActivityCommons;Landroid/view/View;)V

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    if-nez v0, :cond_2

    .line 499
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v9, v9, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "You have no hints."

    invoke-static {v9, v10, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 500
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$5(Llogo/quiz/commons/FormActivityCommons;)Landroid/widget/Button;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 501
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v10, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;
    invoke-static {v10}, Llogo/quiz/commons/FormActivityCommons;->access$5(Llogo/quiz/commons/FormActivityCommons;)Landroid/widget/Button;

    move-result-object v10

    #calls: Llogo/quiz/commons/FormActivityCommons;->shakeView(Landroid/view/View;)V
    invoke-static {v9, v10}, Llogo/quiz/commons/FormActivityCommons;->access$4(Llogo/quiz/commons/FormActivityCommons;Landroid/view/View;)V

    goto :goto_0

    .line 508
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Llogo/quiz/commons/Hint;

    .line 509
    .local v5, hint:Llogo/quiz/commons/Hint;
    invoke-virtual {v5}, Llogo/quiz/commons/Hint;->isUsed()Z

    move-result v10

    if-nez v10, :cond_3

    .line 510
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;

    move-result-object v9

    sget v10, Llogo/quiz/commons/R$id;->hintHelp:I

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 512
    new-instance v8, Landroid/widget/TextView;

    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    invoke-virtual {v9}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 513
    .local v8, hintView:Landroid/widget/TextView;
    const-string v9, "#575757"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 514
    const/high16 v9, 0x4180

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextSize(F)V

    .line 515
    const/16 v9, 0xa

    invoke-virtual {v8, v12, v12, v12, v9}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 516
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "<b>Hint "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #calls: Llogo/quiz/commons/FormActivityCommons;->usedHints(Ljava/util/List;)I
    invoke-static {v10, v1}, Llogo/quiz/commons/FormActivityCommons;->access$6(Llogo/quiz/commons/FormActivityCommons;Ljava/util/List;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".</b> "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Llogo/quiz/commons/Hint;->getHint()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$hintLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 519
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$settings:Landroid/content/SharedPreferences;

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 520
    .local v4, editor:Landroid/content/SharedPreferences$Editor;
    const-string v9, "allHints"

    add-int/lit8 v10, v0, -0x1

    invoke-interface {v4, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 521
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "isUsedHint"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Llogo/quiz/commons/Hint;->getId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Brand"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Llogo/quiz/commons/FormActivityCommons$6;->val$brandTO:Llogo/quiz/commons/BrandTO;

    invoke-virtual {v10}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9, v13}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 522
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 524
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    sget v10, Llogo/quiz/commons/R$id;->hintsCountForm:I

    invoke-virtual {v9, v10}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v12, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    iget-object v12, v12, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    invoke-virtual {v11, v12}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " hints"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;

    move-result-object v9

    sget v10, Llogo/quiz/commons/R$id;->hintsCount:I

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 528
    .local v6, hintAllCount:Landroid/widget/TextView;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "You have "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v10, v0, -0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " hint"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    add-int/lit8 v9, v0, -0x1

    if-ne v9, v13, :cond_7

    const-string v9, ""

    :goto_1
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #getter for: Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;
    invoke-static {v9}, Llogo/quiz/commons/FormActivityCommons;->access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;

    move-result-object v9

    sget v10, Llogo/quiz/commons/R$id;->hintButton:I

    invoke-virtual {v9, v10}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 533
    .local v3, buttonShowHint:Landroid/widget/Button;
    const-string v7, ""

    .line 534
    .local v7, hintNumber:Ljava/lang/String;
    if-le v2, v13, :cond_4

    .line 535
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    sub-int/2addr v10, v2

    add-int/lit8 v10, v10, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 537
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Show Hint "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 538
    add-int/lit8 v9, v2, -0x1

    if-eqz v9, :cond_5

    add-int/lit8 v9, v0, -0x1

    if-nez v9, :cond_6

    .line 539
    :cond_5
    iget-object v9, p0, Llogo/quiz/commons/FormActivityCommons$6;->this$0:Llogo/quiz/commons/FormActivityCommons;

    #calls: Llogo/quiz/commons/FormActivityCommons;->disableButton(Landroid/widget/Button;)V
    invoke-static {v9, v3}, Llogo/quiz/commons/FormActivityCommons;->access$7(Llogo/quiz/commons/FormActivityCommons;Landroid/widget/Button;)V

    .line 541
    :cond_6
    add-int/lit8 v9, v2, -0x1

    if-nez v9, :cond_0

    .line 542
    const-string v9, "All Hints Used"

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 528
    .end local v3           #buttonShowHint:Landroid/widget/Button;
    .end local v7           #hintNumber:Ljava/lang/String;
    :cond_7
    const-string v9, "s"

    goto :goto_1
.end method
