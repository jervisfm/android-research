.class public abstract Llogo/quiz/commons/FormActivityCommons;
.super Lcom/swarmconnect/SwarmActivity;
.source "FormActivityCommons.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llogo/quiz/commons/FormActivityCommons$MyWebViewClient;
    }
.end annotation


# instance fields
.field adHandler:Landroid/os/Handler;

.field adView:Lcom/google/ads/AdView;

.field complete:Z

.field private dialog:Landroid/app/Dialog;

.field final flipAnimator:Llogo/quiz/commons/FlipAnimator;

.field myActivity:Landroid/app/Activity;

.field private tapjoyOfferButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/FormActivityCommons;->complete:Z

    .line 50
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    iput-object v0, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 51
    iput-object v3, p0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    .line 52
    iput-object v3, p0, Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;

    .line 44
    return-void
.end method

.method static synthetic access$0(Llogo/quiz/commons/FormActivityCommons;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1(Llogo/quiz/commons/FormActivityCommons;Ljava/util/List;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 582
    invoke-direct {p0, p1}, Llogo/quiz/commons/FormActivityCommons;->availibleHints(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2(Llogo/quiz/commons/FormActivityCommons;)V
    .locals 0
    .parameter

    .prologue
    .line 575
    invoke-direct {p0}, Llogo/quiz/commons/FormActivityCommons;->wrongSound()V

    return-void
.end method

.method static synthetic access$3(Llogo/quiz/commons/FormActivityCommons;)V
    .locals 0
    .parameter

    .prologue
    .line 566
    invoke-direct {p0}, Llogo/quiz/commons/FormActivityCommons;->vibrateNegative()V

    return-void
.end method

.method static synthetic access$4(Llogo/quiz/commons/FormActivityCommons;Landroid/view/View;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 300
    invoke-direct {p0, p1}, Llogo/quiz/commons/FormActivityCommons;->shakeView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$5(Llogo/quiz/commons/FormActivityCommons;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6(Llogo/quiz/commons/FormActivityCommons;Ljava/util/List;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 592
    invoke-direct {p0, p1}, Llogo/quiz/commons/FormActivityCommons;->usedHints(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method static synthetic access$7(Llogo/quiz/commons/FormActivityCommons;Landroid/widget/Button;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 561
    invoke-direct {p0, p1}, Llogo/quiz/commons/FormActivityCommons;->disableButton(Landroid/widget/Button;)V

    return-void
.end method

.method private availibleHints(Ljava/util/List;)I
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/Hint;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 583
    .local p1, allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    const/4 v0, 0x0

    .line 584
    .local v0, availibleHints:I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 589
    return v0

    .line 584
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llogo/quiz/commons/Hint;

    .line 585
    .local v1, hint:Llogo/quiz/commons/Hint;
    invoke-virtual {v1}, Llogo/quiz/commons/Hint;->isUsed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 586
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private correctSound()V
    .locals 3

    .prologue
    .line 311
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 312
    .local v0, settings:Landroid/content/SharedPreferences;
    const-string v1, "SOUND"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Llogo/quiz/commons/R$raw;->correct:I

    invoke-static {v1, v2}, Llogo/quiz/commons/DeviceUtilCommons;->playSound(Landroid/content/Context;I)V

    .line 315
    :cond_0
    return-void
.end method

.method private disableButton(Landroid/widget/Button;)V
    .locals 0
    .parameter "buttonShowHint"

    .prologue
    .line 564
    return-void
.end method

.method private rotateView(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 306
    sget v1, Llogo/quiz/commons/R$anim;->rotation:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 307
    .local v0, shake:Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 308
    return-void
.end method

.method private shakeView(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 301
    sget v1, Llogo/quiz/commons/R$anim;->shake:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 302
    .local v0, shake:Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 303
    return-void
.end method

.method private usedHints(Ljava/util/List;)I
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/Hint;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 593
    .local p1, allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    const/4 v1, 0x0

    .line 594
    .local v1, usedHints:I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 599
    return v1

    .line 594
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llogo/quiz/commons/Hint;

    .line 595
    .local v0, hint:Llogo/quiz/commons/Hint;
    invoke-virtual {v0}, Llogo/quiz/commons/Hint;->isUsed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 596
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private vibrateNegative()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x96

    const/4 v4, 0x1

    .line 567
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 568
    .local v1, settings:Landroid/content/SharedPreferences;
    const-string v3, "VIBRATE"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 569
    const-string v3, "vibrator"

    invoke-virtual {p0, v3}, Llogo/quiz/commons/FormActivityCommons;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    .line 570
    .local v2, vib:Landroid/os/Vibrator;
    const/4 v3, 0x4

    new-array v0, v3, [J

    aput-wide v6, v0, v4

    const/4 v3, 0x2

    const-wide/16 v4, 0x64

    aput-wide v4, v0, v3

    const/4 v3, 0x3

    aput-wide v6, v0, v3

    .line 571
    .local v0, pattern:[J
    const/4 v3, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 573
    .end local v0           #pattern:[J
    .end local v2           #vib:Landroid/os/Vibrator;
    :cond_0
    return-void
.end method

.method private vibratePositive()V
    .locals 4

    .prologue
    .line 318
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 319
    .local v0, settings:Landroid/content/SharedPreferences;
    const-string v2, "VIBRATE"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Llogo/quiz/commons/FormActivityCommons;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 321
    .local v1, v:Landroid/os/Vibrator;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 323
    .end local v1           #v:Landroid/os/Vibrator;
    :cond_0
    return-void
.end method

.method private wrongSound()V
    .locals 3

    .prologue
    .line 576
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 577
    .local v0, settings:Landroid/content/SharedPreferences;
    const-string v1, "SOUND"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Llogo/quiz/commons/R$raw;->wrong:I

    invoke-static {v1, v2}, Llogo/quiz/commons/DeviceUtilCommons;->playSound(Landroid/content/Context;I)V

    .line 580
    :cond_0
    return-void
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 160
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 161
    .local v0, in:Landroid/content/Intent;
    const-string v2, "isCorrect"

    iget-boolean v3, p0, Llogo/quiz/commons/FormActivityCommons;->complete:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 162
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 163
    .local v1, position:Ljava/lang/Integer;
    iget-boolean v2, p0, Llogo/quiz/commons/FormActivityCommons;->complete:Z

    if-eqz v2, :cond_0

    .line 164
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "position"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .end local v1           #position:Ljava/lang/Integer;
    check-cast v1, Ljava/lang/Integer;

    .line 166
    .restart local v1       #position:Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Llogo/quiz/commons/FormActivityCommons;->setResult(ILandroid/content/Intent;)V

    .line 167
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->finish()V

    .line 168
    return-void
.end method

.method protected abstract checkLevelCompletedLogos(Landroid/app/Activity;III)V
.end method

.method public ckeck(Landroid/view/View;)V
    .locals 38
    .parameter "view"

    .prologue
    .line 171
    sget v33, Llogo/quiz/commons/R$id;->editTextLogo:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/EditText;

    .line 172
    .local v13, editTextLogo:Landroid/widget/EditText;
    invoke-virtual {v13}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v33

    invoke-interface/range {v33 .. v33}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v31

    .line 173
    .local v31, userLogoName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "brandNames"

    invoke-virtual/range {v33 .. v34}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    .line 174
    .local v5, brandNames:[Ljava/lang/String;
    const/16 v18, 0x0

    .line 175
    .local v18, isCorrectAnswer:Z
    array-length v0, v5

    move/from16 v34, v0

    const/16 v33, 0x0

    :goto_0
    move/from16 v0, v33

    move/from16 v1, v34

    if-lt v0, v1, :cond_4

    .line 184
    :goto_1
    invoke-static/range {p0 .. p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v29

    .line 185
    .local v29, settings:Landroid/content/SharedPreferences;
    invoke-interface/range {v29 .. v29}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 187
    .local v14, editor:Landroid/content/SharedPreferences$Editor;
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "brandPosition"

    invoke-virtual/range {v33 .. v34}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 189
    .local v6, brandPosition:Ljava/lang/Integer;
    const-string v33, "guessTries"

    const-string v34, "guessTries"

    const/16 v35, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v34

    add-int/lit8 v34, v34, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 190
    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "perfectGuessBrand"

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v25

    .line 192
    .local v25, perfectGuessBrand:I
    if-eqz v18, :cond_8

    .line 193
    if-nez v25, :cond_0

    .line 194
    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "perfectGuessBrand"

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    const/16 v34, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 195
    const-string v33, "perfectGuess"

    const-string v34, "perfectGuess"

    const/16 v35, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v34

    add-int/lit8 v34, v34, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 201
    :cond_0
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->correctSound()V

    .line 202
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->vibratePositive()V

    .line 204
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v33

    const-string v34, "brandTO"

    invoke-virtual/range {v33 .. v34}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Llogo/quiz/commons/BrandTO;

    .line 206
    .local v7, brandTO:Llogo/quiz/commons/BrandTO;
    const-string v33, "COMPLETE_LOGOS"

    const-string v34, "0,0"

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    const-string v34, ","

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 207
    .local v11, completeLogos:[Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v33

    const-string v34, "1"

    aput-object v34, v11, v33

    .line 208
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .local v24, newCompleteLogosBuff:Ljava/lang/StringBuilder;
    array-length v0, v11

    move/from16 v34, v0

    const/16 v33, 0x0

    :goto_2
    move/from16 v0, v33

    move/from16 v1, v34

    if-lt v0, v1, :cond_6

    .line 213
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 214
    .local v23, newCompleteLogos:Ljava/lang/String;
    const/16 v33, 0x0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v34

    add-int/lit8 v34, v34, -0x1

    move-object/from16 v0, v23

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    .line 216
    const-string v33, "COMPLETE_LOGOS"

    move-object/from16 v0, v33

    move-object/from16 v1, v23

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 217
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v33

    const-string v34, "position"

    invoke-virtual/range {v33 .. v34}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    .line 218
    .local v26, position:Ljava/lang/Integer;
    const-string v33, "complete_position"

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v34

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 219
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 221
    const/16 v33, 0x1

    move/from16 v0, v33

    move-object/from16 v1, p0

    iput-boolean v0, v1, Llogo/quiz/commons/FormActivityCommons;->complete:Z

    .line 223
    const-string v33, "input_method"

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/inputmethod/InputMethodManager;

    .line 224
    .local v17, imm:Landroid/view/inputmethod/InputMethodManager;
    sget v33, Llogo/quiz/commons/R$id;->editTextLogo:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    invoke-virtual/range {v33 .. v33}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v33

    const/16 v34, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 227
    sget v33, Llogo/quiz/commons/R$id;->completeLogoName:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 228
    .local v10, completeLogoName:Landroid/widget/TextView;
    sget v33, Llogo/quiz/commons/R$id;->completeBrandInfos:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 229
    .local v9, completeBrandInfos:Landroid/widget/LinearLayout;
    const/16 v33, 0x0

    move/from16 v0, v33

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 230
    invoke-virtual {v7}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-virtual {v7}, Llogo/quiz/commons/BrandTO;->hasWikipediaLink()Z

    move-result v33

    if-eqz v33, :cond_1

    .line 232
    sget v33, Llogo/quiz/commons/R$id;->readMoreButton:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/ImageButton;

    .line 233
    .local v27, readMoreButton:Landroid/widget/ImageButton;
    const/16 v33, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 241
    .end local v27           #readMoreButton:Landroid/widget/ImageButton;
    :cond_1
    sget v33, Llogo/quiz/commons/R$id;->editLogo:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/LinearLayout;

    const/16 v34, 0x4

    invoke-virtual/range {v33 .. v34}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 242
    sget v33, Llogo/quiz/commons/R$id;->editWinLogo:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/LinearLayout;

    const/16 v34, 0x0

    invoke-virtual/range {v33 .. v34}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 243
    sget v33, Llogo/quiz/commons/R$id;->closeButtonId:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/Button;

    invoke-virtual/range {v33 .. v33}, Landroid/widget/Button;->requestFocus()Z

    .line 244
    sget v33, Llogo/quiz/commons/R$id;->scoreId:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    new-instance v34, Ljava/lang/StringBuilder;

    const-string v35, "Points: +"

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    sget v33, Llogo/quiz/commons/R$id;->hintsCountForm:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    new-instance v34, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, " hints"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    const-string v33, "allHints"

    const/16 v34, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 251
    .local v4, allHintsCount:I
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->isLevelUnlocked()Z

    move-result v19

    .line 252
    .local v19, isLevelUnlocked:Z
    move-object/from16 v0, p0

    iget-object v0, v0, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->getCompletedLogosCount(Landroid/app/Activity;)I

    move-result v8

    .line 253
    .local v8, completeAllLogos:I
    const/16 v33, 0x1

    move/from16 v0, v33

    if-ne v8, v0, :cond_2

    .line 254
    const-string v33, "afterResetApp"

    const/16 v34, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 255
    .local v3, afterResetApp:Z
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getTapJoyPayPerActionCode()Ljava/lang/String;

    move-result-object v30

    .line 256
    .local v30, tapJoyPayPerActionCode:Ljava/lang/String;
    if-eqz v30, :cond_2

    if-nez v3, :cond_2

    .line 257
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/tapjoy/TapjoyConnect;->actionComplete(Ljava/lang/String;)V

    .line 261
    .end local v3           #afterResetApp:Z
    .end local v30           #tapJoyPayPerActionCode:Ljava/lang/String;
    :cond_2
    if-nez v19, :cond_7

    .line 263
    rem-int/lit8 v33, v8, 0x4

    if-nez v33, :cond_3

    .line 264
    const-string v33, "You get new hint!"

    const/16 v34, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/widget/Toast;->show()V

    .line 265
    const-string v33, "allHints"

    add-int/lit8 v34, v4, 0x1

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 266
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 267
    sget v33, Llogo/quiz/commons/R$id;->hintsCountForm:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    new-instance v34, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, " hints"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    .end local v4           #allHintsCount:I
    .end local v7           #brandTO:Llogo/quiz/commons/BrandTO;
    .end local v8           #completeAllLogos:I
    .end local v9           #completeBrandInfos:Landroid/widget/LinearLayout;
    .end local v10           #completeLogoName:Landroid/widget/TextView;
    .end local v11           #completeLogos:[Ljava/lang/String;
    .end local v17           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v19           #isLevelUnlocked:Z
    .end local v23           #newCompleteLogos:Ljava/lang/String;
    .end local v24           #newCompleteLogosBuff:Ljava/lang/StringBuilder;
    .end local v26           #position:Ljava/lang/Integer;
    :cond_3
    :goto_3
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 298
    return-void

    .line 175
    .end local v6           #brandPosition:Ljava/lang/Integer;
    .end local v14           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v25           #perfectGuessBrand:I
    .end local v29           #settings:Landroid/content/SharedPreferences;
    :cond_4
    aget-object v21, v5, v33

    .line 176
    .local v21, name:Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v35

    const-string v36, "[^a-z0-9]+"

    const-string v37, ""

    invoke-virtual/range {v35 .. v37}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 177
    .local v22, nameOnlyLettersAndDigits:Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v35

    const-string v36, "[^a-z0-9]+"

    const-string v37, ""

    invoke-virtual/range {v35 .. v37}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 178
    .local v32, userLogoNameOnlyLettersAndDigits:Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 179
    const/16 v18, 0x1

    .line 180
    goto/16 :goto_1

    .line 175
    :cond_5
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_0

    .line 209
    .end local v21           #name:Ljava/lang/String;
    .end local v22           #nameOnlyLettersAndDigits:Ljava/lang/String;
    .end local v32           #userLogoNameOnlyLettersAndDigits:Ljava/lang/String;
    .restart local v6       #brandPosition:Ljava/lang/Integer;
    .restart local v7       #brandTO:Llogo/quiz/commons/BrandTO;
    .restart local v11       #completeLogos:[Ljava/lang/String;
    .restart local v14       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v24       #newCompleteLogosBuff:Ljava/lang/StringBuilder;
    .restart local v25       #perfectGuessBrand:I
    .restart local v29       #settings:Landroid/content/SharedPreferences;
    :cond_6
    aget-object v20, v11, v33

    .line 210
    .local v20, logo:Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string v35, ","

    move-object/from16 v0, v24

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_2

    .line 270
    .end local v20           #logo:Ljava/lang/String;
    .restart local v4       #allHintsCount:I
    .restart local v8       #completeAllLogos:I
    .restart local v9       #completeBrandInfos:Landroid/widget/LinearLayout;
    .restart local v10       #completeLogoName:Landroid/widget/TextView;
    .restart local v17       #imm:Landroid/view/inputmethod/InputMethodManager;
    .restart local v19       #isLevelUnlocked:Z
    .restart local v23       #newCompleteLogos:Ljava/lang/String;
    .restart local v26       #position:Ljava/lang/Integer;
    :cond_7
    sget v33, Llogo/quiz/commons/R$id;->hintsCountForm:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    new-instance v34, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v35, " hints"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 273
    .end local v4           #allHintsCount:I
    .end local v7           #brandTO:Llogo/quiz/commons/BrandTO;
    .end local v8           #completeAllLogos:I
    .end local v9           #completeBrandInfos:Landroid/widget/LinearLayout;
    .end local v10           #completeLogoName:Landroid/widget/TextView;
    .end local v11           #completeLogos:[Ljava/lang/String;
    .end local v17           #imm:Landroid/view/inputmethod/InputMethodManager;
    .end local v19           #isLevelUnlocked:Z
    .end local v23           #newCompleteLogos:Ljava/lang/String;
    .end local v24           #newCompleteLogosBuff:Ljava/lang/StringBuilder;
    .end local v26           #position:Ljava/lang/Integer;
    :cond_8
    if-nez v25, :cond_9

    .line 274
    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "perfectGuessBrand"

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    const/16 v34, -0x1

    move-object/from16 v0, v33

    move/from16 v1, v34

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 277
    :cond_9
    const/16 v33, 0x0

    aget-object v33, v5, v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v12

    .line 278
    .local v12, correctLogoName:Ljava/lang/String;
    const/16 v28, 0x0

    .line 279
    .local v28, score:I
    const/4 v15, 0x0

    .local v15, i:I
    :goto_4
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v33

    move/from16 v0, v33

    if-lt v15, v0, :cond_a

    .line 286
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v33

    div-int/lit8 v33, v33, 0x2

    move/from16 v0, v28

    move/from16 v1, v33

    if-le v0, v1, :cond_c

    .line 287
    const-string v33, "Almost good. Try again!"

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/widget/Toast;->show()V

    .line 291
    :goto_5
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->wrongSound()V

    .line 292
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->vibrateNegative()V

    .line 293
    sget v33, Llogo/quiz/commons/R$id;->imageBrand:I

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    .line 294
    .local v16, imageBrand:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Llogo/quiz/commons/FormActivityCommons;->shakeView(Landroid/view/View;)V

    goto/16 :goto_3

    .line 280
    .end local v16           #imageBrand:Landroid/widget/ImageView;
    :cond_a
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v33

    move/from16 v0, v33

    if-ge v15, v0, :cond_b

    .line 281
    invoke-virtual {v12, v15}, Ljava/lang/String;->charAt(I)C

    move-result v33

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v34

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_b

    .line 282
    add-int/lit8 v28, v28, 0x1

    .line 279
    :cond_b
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 289
    :cond_c
    const-string v33, "Wrong answer"

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move/from16 v2, v34

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Landroid/widget/Toast;->show()V

    goto :goto_5
.end method

.method public close(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 329
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 330
    .local v0, in:Landroid/content/Intent;
    const-string v2, "isCorrect"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 332
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "position"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 333
    .local v1, position:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Llogo/quiz/commons/FormActivityCommons;->setResult(ILandroid/content/Intent;)V

    .line 334
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->finish()V

    .line 335
    return-void
.end method

.method protected abstract getAvailibleHintsCount(Landroid/app/Activity;)I
.end method

.method protected abstract getCompletedLogosCount(Landroid/app/Activity;)I
.end method

.method protected abstract getConstants()Llogo/quiz/commons/ConstantsProvider;
.end method

.method protected getTapJoyPayPerActionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public hint(Landroid/view/View;)V
    .locals 25
    .parameter "view"

    .prologue
    .line 406
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 408
    .local v6, settings:Landroid/content/SharedPreferences;
    new-instance v4, Landroid/app/Dialog;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    .line 409
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 410
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    const-string v5, "Watch hints"

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 411
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$layout;->logos_hints:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setContentView(I)V

    .line 412
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 414
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "brandTO"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Llogo/quiz/commons/BrandTO;

    .line 417
    .local v7, brandTO:Llogo/quiz/commons/BrandTO;
    const-string v4, "allHints"

    const/4 v5, 0x0

    invoke-interface {v6, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 418
    .local v10, allHintsCount:I
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->hintsCount:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 419
    .local v17, hintAllCount:Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "You have "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " hint"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v4, 0x1

    if-ne v10, v4, :cond_4

    const-string v4, ""

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->isCategoryHint()Z

    move-result v5

    invoke-static {v7, v4, v5}, Llogo/quiz/commons/HintsUtil;->getAllHintsForBrand(Llogo/quiz/commons/BrandTO;Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v11

    .line 423
    .local v11, allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Llogo/quiz/commons/FormActivityCommons;->availibleHints(Ljava/util/List;)I

    move-result v12

    .line 424
    .local v12, availibleHints:I
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->hintButton:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    .line 426
    .local v15, buttonShowHint:Landroid/widget/Button;
    const-string v18, ""

    .line 427
    .local v18, hintNumber:Ljava/lang/String;
    if-lez v12, :cond_0

    .line 428
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, v12

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 430
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Show Hint "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 431
    if-eqz v12, :cond_1

    if-nez v10, :cond_2

    .line 432
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Llogo/quiz/commons/FormActivityCommons;->disableButton(Landroid/widget/Button;)V

    .line 435
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->hintLayout:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 438
    .local v9, hintLayout:Landroid/widget/LinearLayout;
    const/16 v20, 0x0

    .line 439
    .local v20, i:I
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_5

    .line 453
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->backButton:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageButton;

    .line 454
    .local v13, backButton:Landroid/widget/ImageButton;
    new-instance v4, Llogo/quiz/commons/FormActivityCommons$4;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Llogo/quiz/commons/FormActivityCommons$4;-><init>(Llogo/quiz/commons/FormActivityCommons;)V

    invoke-virtual {v13, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->askOnFbButton:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageButton;

    .line 465
    .local v14, backaskOnFbButton:Landroid/widget/ImageButton;
    new-instance v4, Llogo/quiz/commons/FormActivityCommons$5;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v7}, Llogo/quiz/commons/FormActivityCommons$5;-><init>(Llogo/quiz/commons/FormActivityCommons;Llogo/quiz/commons/BrandTO;)V

    invoke-virtual {v14, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 484
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->hintButton:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 485
    .local v8, hintButton:Landroid/widget/Button;
    new-instance v4, Llogo/quiz/commons/FormActivityCommons$6;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Llogo/quiz/commons/FormActivityCommons$6;-><init>(Llogo/quiz/commons/FormActivityCommons;Landroid/content/SharedPreferences;Llogo/quiz/commons/BrandTO;Landroid/widget/Button;Landroid/widget/LinearLayout;)V

    invoke-virtual {v8, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 552
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v5, Llogo/quiz/commons/R$id;->tapjoyOffers:I

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/RelativeLayout;

    .line 553
    .local v21, tapjoyOffers:Landroid/widget/RelativeLayout;
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v4

    invoke-interface {v4}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyOfferButton(Landroid/app/Activity;Llogo/quiz/commons/TapjoyKeys;)Landroid/widget/Button;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;

    .line 554
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->tapjoyOfferButton:Landroid/widget/Button;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 557
    move-object/from16 v0, p0

    iget-object v4, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 559
    return-void

    .line 419
    .end local v8           #hintButton:Landroid/widget/Button;
    .end local v9           #hintLayout:Landroid/widget/LinearLayout;
    .end local v11           #allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    .end local v12           #availibleHints:I
    .end local v13           #backButton:Landroid/widget/ImageButton;
    .end local v14           #backaskOnFbButton:Landroid/widget/ImageButton;
    .end local v15           #buttonShowHint:Landroid/widget/Button;
    .end local v18           #hintNumber:Ljava/lang/String;
    .end local v20           #i:I
    .end local v21           #tapjoyOffers:Landroid/widget/RelativeLayout;
    :cond_4
    const-string v4, "s"

    goto/16 :goto_0

    .line 439
    .restart local v9       #hintLayout:Landroid/widget/LinearLayout;
    .restart local v11       #allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    .restart local v12       #availibleHints:I
    .restart local v15       #buttonShowHint:Landroid/widget/Button;
    .restart local v18       #hintNumber:Ljava/lang/String;
    .restart local v20       #i:I
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Llogo/quiz/commons/Hint;

    .line 440
    .local v16, hint:Llogo/quiz/commons/Hint;
    invoke-virtual/range {v16 .. v16}, Llogo/quiz/commons/Hint;->isUsed()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 441
    add-int/lit8 v20, v20, 0x1

    .line 442
    move-object/from16 v0, p0

    iget-object v5, v0, Llogo/quiz/commons/FormActivityCommons;->dialog:Landroid/app/Dialog;

    sget v22, Llogo/quiz/commons/R$id;->hintHelp:I

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/16 v22, 0x8

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 443
    new-instance v19, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 444
    .local v19, hintView:Landroid/widget/TextView;
    const/high16 v5, 0x4180

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextSize(F)V

    .line 445
    const/4 v5, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0xa

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 446
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v22, "<b>Hint "

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v22, ".</b> "

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Llogo/quiz/commons/Hint;->getHint()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    const-string v5, "#575757"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 448
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1
.end method

.method protected isCategoryHint()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract isLevelUnlocked()Z
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/16 v7, 0x400

    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 71
    invoke-virtual {p0, v9, v9}, Llogo/quiz/commons/FormActivityCommons;->overridePendingTransition(II)V

    .line 72
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0, v10}, Llogo/quiz/commons/FormActivityCommons;->requestWindowFeature(I)Z

    .line 74
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 75
    sget v6, Llogo/quiz/commons/R$layout;->logos_form:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->setContentView(I)V

    .line 79
    iput-object p0, p0, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    .line 81
    iget-object v6, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const-wide/16 v7, 0x1f4

    invoke-virtual {v6, v7, v8}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 82
    iget-object v6, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v6, v10}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 83
    iget-object v6, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v7}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 85
    sget v6, Llogo/quiz/commons/R$id;->myAd2:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v8

    invoke-interface {v8}, Llogo/quiz/commons/ConstantsProvider;->getAdmobRemoveAdId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Llogo/quiz/commons/AdserwerCommons;->setAd(Landroid/widget/ImageView;Landroid/content/Context;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "brandTO"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Llogo/quiz/commons/BrandTO;

    .line 88
    .local v2, brandTO:Llogo/quiz/commons/BrandTO;
    invoke-virtual {v2}, Llogo/quiz/commons/BrandTO;->isComplete()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 89
    sget v6, Llogo/quiz/commons/R$id;->completeLogoName:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 90
    .local v4, completeLogoName:Landroid/widget/TextView;
    sget v6, Llogo/quiz/commons/R$id;->completeBrandInfos:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 91
    .local v3, completeBrandInfos:Landroid/widget/LinearLayout;
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    invoke-virtual {v2}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {v2}, Llogo/quiz/commons/BrandTO;->hasWikipediaLink()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 95
    sget v6, Llogo/quiz/commons/R$id;->readMoreButton:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 96
    .local v5, readMoreButton:Landroid/widget/ImageButton;
    invoke-virtual {v5, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 97
    invoke-direct {p0, v5}, Llogo/quiz/commons/FormActivityCommons;->rotateView(Landroid/view/View;)V

    .line 100
    .end local v5           #readMoreButton:Landroid/widget/ImageButton;
    :cond_0
    sget v6, Llogo/quiz/commons/R$id;->editLogo:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 101
    sget v6, Llogo/quiz/commons/R$id;->editWinLogo:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    sget v6, Llogo/quiz/commons/R$id;->closeButtonId:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->requestFocus()Z

    .line 103
    sget v6, Llogo/quiz/commons/R$id;->scoreId:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Points: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    .end local v3           #completeBrandInfos:Landroid/widget/LinearLayout;
    .end local v4           #completeLogoName:Landroid/widget/TextView;
    :goto_0
    invoke-virtual {p0, p0}, Llogo/quiz/commons/FormActivityCommons;->getAvailibleHintsCount(Landroid/app/Activity;)I

    move-result v1

    .line 111
    .local v1, availibleHintsCount:I
    sget v6, Llogo/quiz/commons/R$id;->hintsCountForm:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " hint"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-ne v1, v10, :cond_2

    const-string v7, ""

    :goto_1
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    sget v6, Llogo/quiz/commons/R$id;->imageBrand:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "brandDrawable"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    iget-object v6, p0, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    sget v7, Llogo/quiz/commons/R$id;->logosListAd2:I

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 116
    .local v0, adContainer:Landroid/widget/RelativeLayout;
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v6

    invoke-interface {v6}, Llogo/quiz/commons/ConstantsProvider;->getAdmobPubId()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Llogo/quiz/commons/AdserwerCommons;->getAdmob(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/ads/AdView;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 118
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v7

    invoke-interface {v7}, Llogo/quiz/commons/ConstantsProvider;->getTapJoyAppId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v8

    invoke-interface {v8}, Llogo/quiz/commons/ConstantsProvider;->getTapJoyAppSecret()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/tapjoy/TapjoyConnect;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void

    .line 106
    .end local v0           #adContainer:Landroid/widget/RelativeLayout;
    .end local v1           #availibleHintsCount:I
    :cond_1
    sget v6, Llogo/quiz/commons/R$id;->editLogo:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 107
    sget v6, Llogo/quiz/commons/R$id;->editWinLogo:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 111
    .restart local v1       #availibleHintsCount:I
    :cond_2
    const-string v7, "s"

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 150
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/tapjoy/TapjoyConnect;->sendShutDownEvent()V

    .line 152
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onDestroy()V

    .line 153
    return-void
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 606
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 612
    return-void
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 618
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 624
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter "arg0"

    .prologue
    .line 628
    sget v1, Llogo/quiz/commons/R$id;->logosListAd2:I

    invoke-virtual {p0, v1}, Llogo/quiz/commons/FormActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 629
    .local v0, layout:Landroid/widget/RelativeLayout;
    iget-object v1, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llogo/quiz/commons/DeviceUtilCommons;->getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Llogo/quiz/commons/FlipAnimator;->setmCenterX(F)V

    .line 630
    iget-object v1, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 631
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 123
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onResume()V

    .line 125
    move-object v0, p0

    .line 126
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v2

    invoke-interface {v2}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v2

    invoke-virtual {v2}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v3

    invoke-interface {v3}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v3

    invoke-virtual {v3}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v3

    .line 127
    new-instance v4, Llogo/quiz/commons/FormActivityCommons$1;

    invoke-direct {v4, p0, v0}, Llogo/quiz/commons/FormActivityCommons$1;-><init>(Llogo/quiz/commons/FormActivityCommons;Landroid/app/Activity;)V

    .line 126
    invoke-static {v1, v2, v3, v4}, Llogo/quiz/commons/FreeHintsActivityCommons;->tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 144
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 156
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 157
    return-void
.end method

.method public promo(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 338
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llogo/quiz/commons/AdserwerCommons;->getPromoIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Llogo/quiz/commons/FormActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 339
    return-void
.end method

.method public readMore(Landroid/view/View;)V
    .locals 12
    .parameter "view"

    .prologue
    const/4 v11, 0x1

    .line 342
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Llogo/quiz/commons/DeviceUtilCommons;->isOnline(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 343
    new-instance v5, Landroid/app/Dialog;

    sget v8, Llogo/quiz/commons/R$style;->Dialog_Fullscreen:I

    invoke-direct {v5, p0, v8}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 344
    .local v5, dialog:Landroid/app/Dialog;
    invoke-virtual {v5, v11}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 345
    sget v8, Llogo/quiz/commons/R$layout;->read_more:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->setContentView(I)V

    .line 346
    invoke-virtual {v5, v11}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 348
    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 350
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "brandTO"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Llogo/quiz/commons/BrandTO;

    .line 354
    .local v4, brandTO:Llogo/quiz/commons/BrandTO;
    iget-object v8, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const-wide/16 v9, 0x1f4

    invoke-virtual {v8, v9, v10}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 355
    iget-object v8, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v8, v11}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 356
    iget-object v8, p0, Llogo/quiz/commons/FormActivityCommons;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    new-instance v9, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v9}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v8, v9}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 358
    sget v8, Llogo/quiz/commons/R$id;->myAdReadMore:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v10

    invoke-interface {v10}, Llogo/quiz/commons/ConstantsProvider;->getAdmobRemoveAdId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Llogo/quiz/commons/AdserwerCommons;->setAd(Landroid/widget/ImageView;Landroid/content/Context;Ljava/lang/String;)V

    .line 359
    sget v8, Llogo/quiz/commons/R$id;->logosReadMoreAd:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 360
    .local v0, adContainer:Landroid/widget/RelativeLayout;
    iget-object v8, p0, Llogo/quiz/commons/FormActivityCommons;->myActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v9

    invoke-interface {v9}, Llogo/quiz/commons/ConstantsProvider;->getAdmobPubId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Llogo/quiz/commons/AdserwerCommons;->getAdmob(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/ads/AdView;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 363
    sget v8, Llogo/quiz/commons/R$id;->brandImageReadMore:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 364
    .local v2, brandImageReadMore:Landroid/widget/ImageView;
    invoke-virtual {v4}, Llogo/quiz/commons/BrandTO;->getDrawable()I

    move-result v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 365
    sget v8, Llogo/quiz/commons/R$id;->brandNameReadMore:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 366
    .local v3, brandNameReadMore:Landroid/widget/TextView;
    invoke-virtual {v4}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    sget v8, Llogo/quiz/commons/R$id;->webBrandInfoReadMore:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/webkit/WebView;

    .line 369
    .local v7, webBrandInfoReadMore:Landroid/webkit/WebView;
    invoke-virtual {v7}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 370
    invoke-virtual {v4}, Llogo/quiz/commons/BrandTO;->getWikipediaLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 371
    new-instance v8, Llogo/quiz/commons/FormActivityCommons$MyWebViewClient;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Llogo/quiz/commons/FormActivityCommons$MyWebViewClient;-><init>(Llogo/quiz/commons/FormActivityCommons;Llogo/quiz/commons/FormActivityCommons$MyWebViewClient;)V

    invoke-virtual {v7, v8}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 374
    sget v8, Llogo/quiz/commons/R$id;->myAdReadMore:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 375
    .local v6, promoButton:Landroid/widget/ImageView;
    new-instance v8, Llogo/quiz/commons/FormActivityCommons$2;

    invoke-direct {v8, p0}, Llogo/quiz/commons/FormActivityCommons$2;-><init>(Llogo/quiz/commons/FormActivityCommons;)V

    invoke-virtual {v6, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    sget v8, Llogo/quiz/commons/R$id;->backButton:I

    invoke-virtual {v5, v8}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 384
    .local v1, backButton:Landroid/widget/ImageButton;
    new-instance v8, Llogo/quiz/commons/FormActivityCommons$3;

    invoke-direct {v8, p0, v5}, Llogo/quiz/commons/FormActivityCommons$3;-><init>(Llogo/quiz/commons/FormActivityCommons;Landroid/app/Dialog;)V

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    .end local v0           #adContainer:Landroid/widget/RelativeLayout;
    .end local v1           #backButton:Landroid/widget/ImageButton;
    .end local v2           #brandImageReadMore:Landroid/widget/ImageView;
    .end local v3           #brandNameReadMore:Landroid/widget/TextView;
    .end local v4           #brandTO:Llogo/quiz/commons/BrandTO;
    .end local v5           #dialog:Landroid/app/Dialog;
    .end local v6           #promoButton:Landroid/widget/ImageView;
    .end local v7           #webBrandInfoReadMore:Landroid/webkit/WebView;
    :goto_0
    return-void

    .line 391
    :cond_0
    invoke-virtual {p0}, Llogo/quiz/commons/FormActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "You have to be online!"

    invoke-static {v8, v9, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
