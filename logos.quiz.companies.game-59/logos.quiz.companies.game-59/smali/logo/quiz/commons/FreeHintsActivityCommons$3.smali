.class Llogo/quiz/commons/FreeHintsActivityCommons$3;
.super Ljava/lang/Object;
.source "FreeHintsActivityCommons.java"

# interfaces
.implements Lcom/tapjoy/TapjoyNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FreeHintsActivityCommons;->tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FreeHintsActivityCommons$3;->val$context:Landroid/content/Context;

    iput-object p2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$3;->val$mHandler:Landroid/os/Handler;

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getUpdatePoints(Ljava/lang/String;I)V
    .locals 8
    .parameter "currencyName"
    .parameter "pointTotal"

    .prologue
    const/4 v6, -0x1

    .line 95
    iget-object v5, p0, Llogo/quiz/commons/FreeHintsActivityCommons$3;->val$context:Landroid/content/Context;

    invoke-static {v5}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 96
    .local v2, settings:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 97
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v5, "tapjoyHints"

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 98
    .local v4, tapjoyHints:I
    if-ne v4, v6, :cond_1

    .line 99
    const-string v5, "tapjoyHints"

    invoke-interface {v1, v5, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 100
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    sub-int v3, p2, v4

    .line 103
    .local v3, tapjoyEarnHints:I
    if-lez v3, :cond_0

    .line 104
    const-string v5, "allHints"

    const/4 v6, 0x0

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 105
    .local v0, allHintsCount:I
    const-string v5, "allHints"

    add-int v6, v0, v3

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 106
    const-string v5, "tapjoyHints"

    invoke-interface {v1, v5, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 107
    const-string v5, "info"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "You get "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " new hints for app install!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 110
    iget-object v5, p0, Llogo/quiz/commons/FreeHintsActivityCommons$3;->val$mHandler:Landroid/os/Handler;

    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public getUpdatePointsFailed(Ljava/lang/String;)V
    .locals 0
    .parameter "error"

    .prologue
    .line 91
    return-void
.end method
