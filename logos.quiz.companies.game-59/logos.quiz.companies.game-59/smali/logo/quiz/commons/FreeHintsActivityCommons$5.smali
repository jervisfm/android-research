.class Llogo/quiz/commons/FreeHintsActivityCommons$5;
.super Ljava/lang/Object;
.source "FreeHintsActivityCommons.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FreeHintsActivityCommons;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Llogo/quiz/commons/FreeHintsActivityCommons;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$settings:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Llogo/quiz/commons/FreeHintsActivityCommons;Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->this$0:Llogo/quiz/commons/FreeHintsActivityCommons;

    iput-object p2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$context:Landroid/content/Context;

    iput-object p3, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$settings:Landroid/content/SharedPreferences;

    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 240
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$context:Landroid/content/Context;

    invoke-static {v2}, Llogo/quiz/commons/DeviceUtilCommons;->isOnline(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$settings:Landroid/content/SharedPreferences;

    const-string v3, "like_on_fb"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 242
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$settings:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 243
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "like_on_fb"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 244
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->val$settings:Landroid/content/SharedPreferences;

    const-string v3, "allHints"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 245
    .local v0, allHintsCount:I
    const-string v2, "allHints"

    add-int/lit8 v3, v0, 0x3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 246
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 249
    .end local v0           #allHintsCount:I
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->this$0:Llogo/quiz/commons/FreeHintsActivityCommons;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Llogo/quiz/commons/FreeHintsActivityCommons;->like(Landroid/view/View;)V

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$5;->this$0:Llogo/quiz/commons/FreeHintsActivityCommons;

    invoke-virtual {v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "You have to be online"

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
