.class Llogo/quiz/commons/FreeHintsActivityCommons$6;
.super Ljava/lang/Object;
.source "FreeHintsActivityCommons.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyOfferButton(Landroid/app/Activity;Llogo/quiz/commons/TapjoyKeys;)Landroid/widget/Button;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$tapjoyKeys:Llogo/quiz/commons/TapjoyKeys;


# direct methods
.method constructor <init>(Landroid/content/Context;Llogo/quiz/commons/TapjoyKeys;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$context:Landroid/content/Context;

    iput-object p2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$tapjoyKeys:Llogo/quiz/commons/TapjoyKeys;

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 293
    iget-object v0, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$context:Landroid/content/Context;

    invoke-static {v0}, Llogo/quiz/commons/DeviceUtilCommons;->isOnline(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$context:Landroid/content/Context;

    iget-object v1, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$tapjoyKeys:Llogo/quiz/commons/TapjoyKeys;

    invoke-virtual {v1}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$tapjoyKeys:Llogo/quiz/commons/TapjoyKeys;

    invoke-virtual {v2}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/tapjoy/TapjoyConnect;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/tapjoy/TapjoyConnect;->showOffers()V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/FreeHintsActivityCommons$6;->val$context:Landroid/content/Context;

    const-string v1, "You have to be online"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
