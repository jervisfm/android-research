.class public abstract Llogo/quiz/commons/FreeHintsActivityCommons;
.super Lcom/swarmconnect/SwarmActivity;
.source "FreeHintsActivityCommons.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# static fields
.field private static isTapjoyEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    sput-boolean v0, Llogo/quiz/commons/FreeHintsActivityCommons;->isTapjoyEnable:Z

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Llogo/quiz/commons/FreeHintsActivityCommons;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    invoke-direct {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->refreshHints()V

    return-void
.end method

.method static synthetic access$1(Llogo/quiz/commons/FreeHintsActivityCommons;Llogo/quiz/commons/MyAdCommons;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 397
    invoke-direct {p0, p1, p2, p3}, Llogo/quiz/commons/FreeHintsActivityCommons;->freeHintsClick(Llogo/quiz/commons/MyAdCommons;IZ)V

    return-void
.end method

.method private freeHintsClick(Llogo/quiz/commons/MyAdCommons;IZ)V
    .locals 7
    .parameter "ad"
    .parameter "hintCountPromo"
    .parameter "isInstallApp"

    .prologue
    const/4 v6, 0x1

    .line 398
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Llogo/quiz/commons/DeviceUtilCommons;->isOnline(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 399
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 400
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p1}, Llogo/quiz/commons/MyAdCommons;->getAdUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 401
    const/high16 v4, 0x4000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 403
    if-nez p3, :cond_0

    .line 404
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 405
    .local v3, settings:Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 406
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 407
    const-string v4, "allHints"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 408
    .local v0, allHintsCount:I
    const-string v4, "allHints"

    add-int v5, v0, p2

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 409
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 412
    .end local v0           #allHintsCount:I
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v3           #settings:Landroid/content/SharedPreferences;
    :cond_0
    invoke-virtual {p0, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 416
    .end local v2           #intent:Landroid/content/Intent;
    :goto_0
    return-void

    .line 414
    :cond_1
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "You have to be online"

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private getHintText(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;
    .locals 10
    .parameter "freeHintsCount"
    .parameter "str"

    .prologue
    const/4 v8, 0x5

    .line 331
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 332
    .local v2, layout:Landroid/widget/LinearLayout;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    .line 333
    const/4 v6, -0x2

    .line 334
    const/high16 v7, 0x3f80

    .line 332
    invoke-direct {v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 335
    .local v3, layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v8, v5}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v5

    .line 336
    const/16 v6, 0xa

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v6, v7}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v6

    .line 337
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v8, v7}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v7

    .line 338
    const/16 v8, 0xf

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v8, v9}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v8

    .line 335
    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 339
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 341
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 342
    .local v4, topText:Landroid/widget/TextView;
    const-string v5, "Get "

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Llogo/quiz/commons/R$style;->hintsTextStyleCommons:I

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 344
    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 346
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    sget v6, Llogo/quiz/commons/R$layout;->hints_button:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 347
    .local v1, hintsButton:Landroid/widget/Button;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " hints"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 348
    new-instance v5, Llogo/quiz/commons/FreeHintsActivityCommons$7;

    invoke-direct {v5, p0}, Llogo/quiz/commons/FreeHintsActivityCommons$7;-><init>(Llogo/quiz/commons/FreeHintsActivityCommons;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 355
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 356
    .local v0, bottomText:Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Llogo/quiz/commons/R$style;->hintsTextStyleCommons:I

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 358
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 360
    return-object v2
.end method

.method public static getTapjoyOfferButton(Landroid/app/Activity;Llogo/quiz/commons/TapjoyKeys;)Landroid/widget/Button;
    .locals 7
    .parameter "activity"
    .parameter "tapjoyKeys"

    .prologue
    const/16 v6, 0x14

    .line 281
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 282
    .local v0, context:Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    sget v4, Llogo/quiz/commons/R$layout;->tapjoy_offers_button:I

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 283
    .local v2, tapjoyOfferBtn:Landroid/widget/Button;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    .line 284
    const/4 v4, -0x2

    .line 285
    const/high16 v5, 0x3f80

    .line 283
    invoke-direct {v1, v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 286
    .local v1, layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {v6, v0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v3

    .line 287
    const/4 v4, 0x0

    invoke-static {v4, v0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v4

    .line 288
    invoke-static {v6, v0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v5

    .line 289
    invoke-static {v6, v0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v6

    .line 286
    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 290
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    new-instance v3, Llogo/quiz/commons/FreeHintsActivityCommons$6;

    invoke-direct {v3, v0, p1}, Llogo/quiz/commons/FreeHintsActivityCommons$6;-><init>(Landroid/content/Context;Llogo/quiz/commons/TapjoyKeys;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 301
    return-object v2
.end method

.method private promoAd(Landroid/widget/LinearLayout;Llogo/quiz/commons/MyAdCommons;IZ)V
    .locals 10
    .parameter "layout"
    .parameter "ad"
    .parameter "hintCountPromo"
    .parameter "isInstallApp"

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 364
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 365
    .local v0, adView:Landroid/widget/ImageView;
    const/16 v7, 0x140

    invoke-static {v7, p0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v4

    .line 366
    .local v4, pixelsW:I
    const/16 v7, 0x32

    invoke-static {v7, p0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v3

    .line 367
    .local v3, pixelsH:I
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v3, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 368
    .local v1, adViewParams:Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 369
    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 376
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 377
    .local v2, bmpFactory:Landroid/graphics/BitmapFactory$Options;
    iput-boolean v8, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 378
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {p2}, Llogo/quiz/commons/MyAdCommons;->getImageResource()I

    move-result v8

    invoke-static {v7, v8, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 381
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 382
    new-instance v7, Llogo/quiz/commons/FreeHintsActivityCommons$8;

    invoke-direct {v7, p0, p2, p3, p4}, Llogo/quiz/commons/FreeHintsActivityCommons$8;-><init>(Llogo/quiz/commons/FreeHintsActivityCommons;Llogo/quiz/commons/MyAdCommons;IZ)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 389
    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 390
    .local v6, separator:Landroid/widget/LinearLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    .line 391
    const/16 v8, 0xf

    invoke-static {v8, p0}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v8

    .line 392
    const/high16 v9, 0x3f80

    .line 390
    invoke-direct {v5, v7, v8, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 393
    .local v5, sepParams:Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 395
    return-void
.end method

.method private refreshHints()V
    .locals 5

    .prologue
    .line 144
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 145
    .local v2, settings:Landroid/content/SharedPreferences;
    const-string v3, "allHints"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 146
    .local v0, allHintsCount:I
    sget v3, Llogo/quiz/commons/R$id;->yourHintsInfo:I

    invoke-virtual {p0, v3}, Llogo/quiz/commons/FreeHintsActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 147
    .local v1, hintAllCount:Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "You have "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hint"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const-string v3, ""

    :goto_0
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    return-void

    .line 147
    :cond_0
    const-string v3, "s"

    goto :goto_0
.end method

.method public static tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "context"
    .parameter "tapjoyAppId"
    .parameter "tapjoySecret"

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Llogo/quiz/commons/FreeHintsActivityCommons;->tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 73
    return-void
.end method

.method public static tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 4
    .parameter "context"
    .parameter "tapjoyAppId"
    .parameter "tapjoySecret"
    .parameter "handler"

    .prologue
    .line 76
    if-nez p3, :cond_0

    .line 77
    new-instance p3, Llogo/quiz/commons/FreeHintsActivityCommons$2;

    .end local p3
    invoke-direct {p3, p0}, Llogo/quiz/commons/FreeHintsActivityCommons$2;-><init>(Landroid/content/Context;)V

    .line 84
    .restart local p3
    :cond_0
    move-object v1, p3

    .line 87
    .local v1, mHandler:Landroid/os/Handler;
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/tapjoy/TapjoyConnect;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v2

    new-instance v3, Llogo/quiz/commons/FreeHintsActivityCommons$3;

    invoke-direct {v3, p0, v1}, Llogo/quiz/commons/FreeHintsActivityCommons$3;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    invoke-virtual {v2, v3}, Lcom/tapjoy/TapjoyConnect;->getTapPoints(Lcom/tapjoy/TapjoyNotifier;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :goto_0
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 117
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x0

    sput-boolean v2, Llogo/quiz/commons/FreeHintsActivityCommons;->isTapjoyEnable:Z

    goto :goto_0
.end method


# virtual methods
.method public abstract back(Landroid/view/View;)V
.end method

.method protected abstract getAdmobPubId()Ljava/lang/String;
.end method

.method protected abstract getAdmobRemoveAdId()Ljava/lang/String;
.end method

.method protected abstract getChartBoostAppId()Ljava/lang/String;
.end method

.method protected abstract getChartBoostSignature()Ljava/lang/String;
.end method

.method protected abstract getFacebookProfileId()Ljava/lang/String;
.end method

.method protected abstract getGooglePlayUrl()Ljava/lang/String;
.end method

.method public abstract getRemoveAdId()Ljava/lang/String;
.end method

.method protected getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    return-object v0
.end method

.method public like(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 305
    const/4 v1, 0x0

    .line 307
    .local v1, intent:Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.facebook.katana"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 308
    new-instance v1, Landroid/content/Intent;

    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fb://profile/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getFacebookProfileId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    .restart local v1       #intent:Landroid/content/Intent;
    :goto_0
    const/high16 v2, 0x4000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 314
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 315
    invoke-virtual {p0, v1}, Llogo/quiz/commons/FreeHintsActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 316
    return-void

    .line 310
    .end local v1           #intent:Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 311
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://www.facebook.com/pages/profile/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getFacebookProfileId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v1       #intent:Landroid/content/Intent;
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/16 v3, 0x400

    const/4 v2, 0x0

    .line 43
    invoke-virtual {p0, v2, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->overridePendingTransition(II)V

    .line 44
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->requestWindowFeature(I)Z

    .line 46
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 47
    sget v2, Llogo/quiz/commons/R$layout;->free_hints:I

    invoke-virtual {p0, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->setContentView(I)V

    .line 54
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 55
    .local v1, settings:Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/chartboost/sdk/ChartBoost;->getSharedChartBoost(Landroid/content/Context;)Lcom/chartboost/sdk/ChartBoost;

    move-result-object v0

    .line 56
    .local v0, _cb:Lcom/chartboost/sdk/ChartBoost;
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getChartBoostAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chartboost/sdk/ChartBoost;->setAppId(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getChartBoostSignature()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chartboost/sdk/ChartBoost;->setAppSignature(Ljava/lang/String;)V

    .line 58
    new-instance v2, Llogo/quiz/commons/FreeHintsActivityCommons$1;

    invoke-direct {v2, p0, v1}, Llogo/quiz/commons/FreeHintsActivityCommons$1;-><init>(Llogo/quiz/commons/FreeHintsActivityCommons;Landroid/content/SharedPreferences;)V

    invoke-virtual {v0, v2}, Lcom/chartboost/sdk/ChartBoost;->setDelegate(Lcom/chartboost/sdk/ChartBoostDelegate;)V

    .line 67
    invoke-virtual {v0}, Lcom/chartboost/sdk/ChartBoost;->install()V

    .line 68
    invoke-virtual {v0}, Lcom/chartboost/sdk/ChartBoost;->showInterstitial()V

    .line 69
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 126
    :try_start_0
    invoke-static {}, Lcom/tapjoy/TapjoyConnect;->getTapjoyConnectInstance()Lcom/tapjoy/TapjoyConnect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/tapjoy/TapjoyConnect;->sendShutDownEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onDestroy()V

    .line 132
    return-void

    .line 127
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 443
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 449
    return-void
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 455
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 461
    return-void
.end method

.method public abstract onReceiveAd(Lcom/google/ads/Ad;)V
.end method

.method protected onResume()V
    .locals 25

    .prologue
    .line 153
    invoke-super/range {p0 .. p0}, Lcom/swarmconnect/SwarmActivity;->onResume()V

    .line 155
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    .line 157
    .local v11, context:Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v21

    if-eqz v21, :cond_0

    .line 158
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v23

    .line 159
    new-instance v24, Llogo/quiz/commons/FreeHintsActivityCommons$4;

    invoke-direct/range {v24 .. v25}, Llogo/quiz/commons/FreeHintsActivityCommons$4;-><init>(Llogo/quiz/commons/FreeHintsActivityCommons;)V

    .line 158
    invoke-static/range {v21 .. v24}, Llogo/quiz/commons/FreeHintsActivityCommons;->tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 170
    :cond_0
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v20

    .line 173
    .local v20, settings:Landroid/content/SharedPreferences;
    invoke-static/range {p0 .. p0}, Lcom/chartboost/sdk/ChartBoost;->getSharedChartBoost(Landroid/content/Context;)Lcom/chartboost/sdk/ChartBoost;

    .line 175
    sget v21, Llogo/quiz/commons/R$id;->freeHintsContainer:I

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Llogo/quiz/commons/FreeHintsActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 176
    .local v13, freeHintsContainer:Landroid/widget/LinearLayout;
    invoke-virtual {v13}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 178
    const/4 v5, 0x0

    .line 181
    .local v5, activeCount:I
    sget-boolean v21, Llogo/quiz/commons/FreeHintsActivityCommons;->isTapjoyEnable:Z

    if-eqz v21, :cond_1

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v21

    if-eqz v21, :cond_1

    .line 182
    const-string v21, "free"

    const-string v22, " by install and run other apps"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->getHintText(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 183
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Llogo/quiz/commons/FreeHintsActivityCommons;->getTapjoyOfferButton(Landroid/app/Activity;Llogo/quiz/commons/TapjoyKeys;)Landroid/widget/Button;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 187
    :cond_1
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getRemoveAdId()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Llogo/quiz/commons/AdserwerCommons;->getActiveAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 188
    .local v4, activeAds:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/MyAdCommons;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_2

    .line 189
    const-string v21, "5"

    const-string v22, " by installing other logo quiz"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->getHintText(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 191
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_3
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_7

    .line 199
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->refreshHints()V

    .line 201
    new-instance v19, Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 202
    .local v19, separator:Landroid/widget/LinearLayout;
    new-instance v18, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v21, -0x1

    .line 203
    const/16 v22, 0xa

    move/from16 v0, v22

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v22

    .line 204
    const/high16 v23, 0x3f80

    .line 202
    move-object/from16 v0, v18

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 205
    .local v18, sepParams:Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 206
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 209
    new-instance v15, Llogo/quiz/commons/MyAdCommons;

    sget v21, Llogo/quiz/commons/R$drawable;->rate_logo_quiz:I

    const-string v22, "ad999"

    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getGooglePlayUrl()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v15, v0, v1, v2}, Llogo/quiz/commons/MyAdCommons;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 210
    .local v15, logoQuizAd:Llogo/quiz/commons/MyAdCommons;
    invoke-virtual {v15}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    if-nez v21, :cond_4

    .line 211
    const-string v21, "3"

    const-string v22, " by rate Logo Quiz"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->getHintText(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 212
    const/16 v21, 0x3

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v13, v15, v1, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->promoAd(Landroid/widget/LinearLayout;Llogo/quiz/commons/MyAdCommons;IZ)V

    .line 213
    add-int/lit8 v5, v5, 0x1

    .line 217
    :cond_4
    new-instance v8, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 218
    .local v8, adView:Landroid/widget/ImageView;
    const/16 v21, 0xa4

    move/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v17

    .line 219
    .local v17, pixelsW:I
    const/16 v21, 0x3e

    move/from16 v0, v21

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Llogo/quiz/commons/DeviceUtilCommons;->dip(ILandroid/content/Context;)I

    move-result v16

    .line 220
    .local v16, pixelsH:I
    new-instance v9, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v21, 0x1

    move/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v21

    invoke-direct {v9, v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 221
    .local v9, adViewParams:Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    const/16 v21, 0x0

    .line 224
    const/16 v22, 0x0

    .line 225
    const/16 v23, 0x0

    .line 226
    const/16 v24, 0x0

    .line 222
    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 227
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 228
    .local v10, bmpFactory:Landroid/graphics/BitmapFactory$Options;
    const/16 v21, 0x0

    move/from16 v0, v21

    iput-boolean v0, v10, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 230
    const-string v21, "like_on_fb"

    const/16 v22, 0x0

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v21

    if-nez v21, :cond_8

    .line 231
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v22, Llogo/quiz/commons/R$drawable;->like_us_on_fb:I

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 232
    add-int/lit8 v5, v5, 0x1

    .line 237
    :goto_1
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 238
    new-instance v21, Llogo/quiz/commons/FreeHintsActivityCommons$5;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v11, v2}, Llogo/quiz/commons/FreeHintsActivityCommons$5;-><init>(Llogo/quiz/commons/FreeHintsActivityCommons;Landroid/content/Context;Landroid/content/SharedPreferences;)V

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    invoke-virtual {v13, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 257
    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v21

    if-nez v21, :cond_5

    .line 258
    new-instance v7, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 259
    .local v7, adTopText2:Landroid/widget/TextView;
    const-string v21, "At this moment there are no free hints."

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    invoke-virtual {v13, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 263
    .end local v7           #adTopText2:Landroid/widget/TextView;
    :cond_5
    const-string v21, "lastActiveAdsCount"

    const/16 v22, -0x1

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 264
    .local v14, lastActiveAdsCount:I
    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    .line 265
    .local v12, editor:Landroid/content/SharedPreferences$Editor;
    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_9

    .line 266
    const-string v21, "lastActiveAdsCount"

    move-object/from16 v0, v21

    invoke-interface {v12, v0, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 275
    :cond_6
    :goto_2
    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 278
    return-void

    .line 191
    .end local v8           #adView:Landroid/widget/ImageView;
    .end local v9           #adViewParams:Landroid/widget/FrameLayout$LayoutParams;
    .end local v10           #bmpFactory:Landroid/graphics/BitmapFactory$Options;
    .end local v12           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v14           #lastActiveAdsCount:I
    .end local v15           #logoQuizAd:Llogo/quiz/commons/MyAdCommons;
    .end local v16           #pixelsH:I
    .end local v17           #pixelsW:I
    .end local v18           #sepParams:Landroid/widget/LinearLayout$LayoutParams;
    .end local v19           #separator:Landroid/widget/LinearLayout;
    :cond_7
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Llogo/quiz/commons/MyAdCommons;

    .line 192
    .local v6, ad:Llogo/quiz/commons/MyAdCommons;
    invoke-virtual {v6}, Llogo/quiz/commons/MyAdCommons;->getAdId()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v22

    if-nez v22, :cond_3

    .line 193
    const/16 v22, 0x5

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v13, v6, v1, v2}, Llogo/quiz/commons/FreeHintsActivityCommons;->promoAd(Landroid/widget/LinearLayout;Llogo/quiz/commons/MyAdCommons;IZ)V

    .line 194
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 234
    .end local v6           #ad:Llogo/quiz/commons/MyAdCommons;
    .restart local v8       #adView:Landroid/widget/ImageView;
    .restart local v9       #adViewParams:Landroid/widget/FrameLayout$LayoutParams;
    .restart local v10       #bmpFactory:Landroid/graphics/BitmapFactory$Options;
    .restart local v15       #logoQuizAd:Llogo/quiz/commons/MyAdCommons;
    .restart local v16       #pixelsH:I
    .restart local v17       #pixelsW:I
    .restart local v18       #sepParams:Landroid/widget/LinearLayout$LayoutParams;
    .restart local v19       #separator:Landroid/widget/LinearLayout;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    sget v22, Llogo/quiz/commons/R$drawable;->like_us_used:I

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-static {v0, v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1

    .line 268
    .restart local v12       #editor:Landroid/content/SharedPreferences$Editor;
    .restart local v14       #lastActiveAdsCount:I
    :cond_9
    if-eq v14, v5, :cond_6

    .line 270
    const-string v21, "Congratulations! You get new hints."

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/widget/Toast;->show()V

    .line 271
    const-string v21, "lastActiveAdsCount"

    move-object/from16 v0, v21

    invoke-interface {v12, v0, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 272
    invoke-direct/range {p0 .. p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->refreshHints()V

    goto :goto_2
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 419
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 420
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 421
    return-void
.end method

.method public promo(Landroid/view/View;)V
    .locals 1
    .parameter "view"

    .prologue
    .line 424
    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llogo/quiz/commons/AdserwerCommons;->getPromoIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Llogo/quiz/commons/FreeHintsActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 435
    return-void
.end method

.method public rate(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 319
    const-string v1, "Rate app"

    invoke-static {v1}, Lcom/flurry/android/FlurryAgent;->onEvent(Ljava/lang/String;)V

    .line 320
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 321
    .local v0, intent:Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "market://details?id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 322
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 323
    invoke-virtual {p0, v0}, Llogo/quiz/commons/FreeHintsActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 324
    return-void
.end method
