.class public abstract Llogo/quiz/commons/HighScoreActivityCommons;
.super Lcom/swarmconnect/SwarmActivity;
.source "HighScoreActivityCommons.java"


# instance fields
.field private mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    .line 22
    new-instance v0, Llogo/quiz/commons/HighScoreActivityCommons$1;

    invoke-direct {v0, p0}, Llogo/quiz/commons/HighScoreActivityCommons$1;-><init>(Llogo/quiz/commons/HighScoreActivityCommons;)V

    iput-object v0, p0, Llogo/quiz/commons/HighScoreActivityCommons;->mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;

    .line 18
    return-void
.end method


# virtual methods
.method protected abstract getConstants()Llogo/quiz/commons/ConstantsProvider;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    const/16 v1, 0x400

    const/4 v0, 0x0

    .line 60
    invoke-virtual {p0, v0, v0}, Llogo/quiz/commons/HighScoreActivityCommons;->overridePendingTransition(II)V

    .line 61
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Llogo/quiz/commons/HighScoreActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v0

    invoke-interface {v0}, Llogo/quiz/commons/ConstantsProvider;->getFlurryKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Llogo/quiz/commons/HighScoreActivityCommons;->requestWindowFeature(I)Z

    .line 64
    invoke-virtual {p0}, Llogo/quiz/commons/HighScoreActivityCommons;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 65
    sget v0, Llogo/quiz/commons/R$layout;->high_score:I

    invoke-virtual {p0, v0}, Llogo/quiz/commons/HighScoreActivityCommons;->setContentView(I)V

    .line 67
    new-instance v0, Llogo/quiz/commons/HighScoreActivityCommons$2;

    invoke-direct {v0, p0}, Llogo/quiz/commons/HighScoreActivityCommons$2;-><init>(Llogo/quiz/commons/HighScoreActivityCommons;)V

    invoke-static {v0}, Lcom/swarmconnect/Swarm;->addNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V

    .line 91
    invoke-virtual {p0}, Llogo/quiz/commons/HighScoreActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v0

    invoke-interface {v0}, Llogo/quiz/commons/ConstantsProvider;->getSwarmconnectAppId()I

    move-result v0

    invoke-virtual {p0}, Llogo/quiz/commons/HighScoreActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v1

    invoke-interface {v1}, Llogo/quiz/commons/ConstantsProvider;->getSwarmconnectAppKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Llogo/quiz/commons/HighScoreActivityCommons;->mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;

    invoke-static {p0, v0, v1, v2}, Lcom/swarmconnect/Swarm;->init(Landroid/app/Activity;ILjava/lang/String;Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    .line 92
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 96
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 97
    return-void
.end method
