.class public Llogo/quiz/commons/Hint;
.super Ljava/lang/Object;
.source "Hint.java"


# instance fields
.field hint:Ljava/lang/String;

.field id:I

.field isUsed:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 0
    .parameter "id"
    .parameter "hint"
    .parameter "isUsed"

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Llogo/quiz/commons/Hint;->id:I

    .line 11
    iput-object p2, p0, Llogo/quiz/commons/Hint;->hint:Ljava/lang/String;

    .line 12
    iput-boolean p3, p0, Llogo/quiz/commons/Hint;->isUsed:Z

    .line 13
    return-void
.end method


# virtual methods
.method public getHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Llogo/quiz/commons/Hint;->hint:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Llogo/quiz/commons/Hint;->id:I

    return v0
.end method

.method public isUsed()Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Llogo/quiz/commons/Hint;->isUsed:Z

    return v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 0
    .parameter "hint"

    .prologue
    .line 27
    iput-object p1, p0, Llogo/quiz/commons/Hint;->hint:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setId(I)V
    .locals 0
    .parameter "id"

    .prologue
    .line 20
    iput p1, p0, Llogo/quiz/commons/Hint;->id:I

    .line 21
    return-void
.end method

.method public setUsed(Z)V
    .locals 0
    .parameter "isUsed"

    .prologue
    .line 33
    iput-boolean p1, p0, Llogo/quiz/commons/Hint;->isUsed:Z

    .line 34
    return-void
.end method
