.class public Llogo/quiz/commons/HintsUtil;
.super Ljava/lang/Object;
.source "HintsUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAllHintsForBrand(Llogo/quiz/commons/BrandTO;Landroid/content/Context;Z)Ljava/util/List;
    .locals 12
    .parameter "brandTO"
    .parameter "context"
    .parameter "isCategoryHint"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llogo/quiz/commons/BrandTO;",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/Hint;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v11, 0x0

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .local v0, hints:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 17
    .local v6, settings:Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getHint1()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 18
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isUsedHint1Brand"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 19
    .local v1, isUsedHint1:Z
    new-instance v7, Llogo/quiz/commons/Hint;

    const/4 v8, 0x1

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getHint1()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9, v1}, Llogo/quiz/commons/Hint;-><init>(ILjava/lang/String;Z)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    .end local v1           #isUsedHint1:Z
    :cond_0
    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getHint2()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 23
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isUsedHint2Brand"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 24
    .local v2, isUsedHint2:Z
    new-instance v7, Llogo/quiz/commons/Hint;

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getHint2()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v10, v8, v2}, Llogo/quiz/commons/Hint;-><init>(ILjava/lang/String;Z)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .end local v2           #isUsedHint2:Z
    :cond_1
    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v10, :cond_2

    .line 28
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isUsedHint3Brand"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 29
    .local v3, isUsedHint3:Z
    new-instance v7, Llogo/quiz/commons/Hint;

    const/4 v8, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Half solution: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Llogo/quiz/commons/HintsUtil;->hideCharsInBrandName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9, v3}, Llogo/quiz/commons/Hint;-><init>(ILjava/lang/String;Z)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    .end local v3           #isUsedHint3:Z
    :cond_2
    if-eqz p2, :cond_3

    .line 33
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isUsedHint4Brand"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 34
    .local v4, isUsedHint4:Z
    new-instance v7, Llogo/quiz/commons/Hint;

    const/4 v8, 0x4

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Brand is from category \'"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getCategory()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9, v4}, Llogo/quiz/commons/Hint;-><init>(ILjava/lang/String;Z)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    .end local v4           #isUsedHint4:Z
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "isUsedHint5Brand"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 38
    .local v5, isUsedHint5:Z
    new-instance v7, Llogo/quiz/commons/Hint;

    const/4 v8, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Full solution: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Llogo/quiz/commons/BrandTO;->getbrandName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9, v5}, Llogo/quiz/commons/Hint;-><init>(ILjava/lang/String;Z)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-object v0
.end method

.method private static hideCharsInBrandName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .parameter "brandName"

    .prologue
    .line 44
    const-string v0, ""

    .line 45
    .local v0, brandNameWithHiddenChars:Ljava/lang/String;
    const/4 v2, 0x0

    .line 46
    .local v2, i:I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 58
    return-object v0

    .line 46
    :cond_0
    aget-char v1, v4, v3

    .line 47
    .local v1, c:C
    rem-int/lit8 v6, v2, 0x2

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v2, v6, :cond_2

    .line 48
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 46
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 50
    :cond_2
    const/16 v6, 0x20

    if-ne v1, v6, :cond_3

    .line 51
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 53
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
