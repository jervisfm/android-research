.class public abstract Llogo/quiz/commons/ImageAdapterCommons;
.super Landroid/widget/BaseAdapter;
.source "ImageAdapterCommons.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;
    }
.end annotation


# instance fields
.field private bmpFactory:Landroid/graphics/BitmapFactory$Options;

.field protected mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "c"

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    iput-object p1, p0, Llogo/quiz/commons/ImageAdapterCommons;->mContext:Landroid/content/Context;

    .line 18
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Llogo/quiz/commons/ImageAdapterCommons;->mInflater:Landroid/view/LayoutInflater;

    .line 19
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Llogo/quiz/commons/ImageAdapterCommons;->bmpFactory:Landroid/graphics/BitmapFactory$Options;

    .line 20
    iget-object v0, p0, Llogo/quiz/commons/ImageAdapterCommons;->bmpFactory:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x2

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 21
    return-void
.end method


# virtual methods
.method protected abstract getActiveLevelBrands()[Llogo/quiz/commons/BrandTO;
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Llogo/quiz/commons/ImageAdapterCommons;->getActiveLevelBrands()[Llogo/quiz/commons/BrandTO;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Llogo/quiz/commons/ImageAdapterCommons;->getItem(I)Llogo/quiz/commons/BrandTO;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Llogo/quiz/commons/BrandTO;
    .locals 3
    .parameter "position"

    .prologue
    .line 30
    const/4 v1, 0x0

    .line 31
    .local v1, item:Llogo/quiz/commons/BrandTO;
    invoke-virtual {p0}, Llogo/quiz/commons/ImageAdapterCommons;->getActiveLevelBrands()[Llogo/quiz/commons/BrandTO;

    move-result-object v0

    .line 33
    .local v0, brands:[Llogo/quiz/commons/BrandTO;
    :try_start_0
    array-length v2, v0

    if-ne v2, p1, :cond_0

    .line 34
    add-int/lit8 v2, p1, -0x1

    aget-object v1, v0, v2

    .line 40
    :goto_0
    return-object v1

    .line 36
    :cond_0
    aget-object v1, v0, p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 44
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 50
    if-nez p2, :cond_1

    .line 51
    iget-object v2, p0, Llogo/quiz/commons/ImageAdapterCommons;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Llogo/quiz/commons/R$layout;->logos_list_item:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 52
    new-instance v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;

    invoke-direct {v1}, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;-><init>()V

    .line 53
    .local v1, holder:Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;
    sget v2, Llogo/quiz/commons/R$id;->list_item:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    .line 54
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 59
    :goto_0
    invoke-virtual {p0}, Llogo/quiz/commons/ImageAdapterCommons;->getActiveLevelBrands()[Llogo/quiz/commons/BrandTO;

    move-result-object v2

    aget-object v0, v2, p1

    .line 61
    .local v0, brandTO:Llogo/quiz/commons/BrandTO;
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 62
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 63
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_complete_1:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 83
    :cond_0
    :goto_1
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Llogo/quiz/commons/ImageAdapterCommons;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getDrawable()I

    move-result v4

    iget-object v5, p0, Llogo/quiz/commons/ImageAdapterCommons;->bmpFactory:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 85
    return-object p2

    .line 56
    .end local v0           #brandTO:Llogo/quiz/commons/BrandTO;
    .end local v1           #holder:Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;

    .restart local v1       #holder:Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;
    goto :goto_0

    .line 64
    .restart local v0       #brandTO:Llogo/quiz/commons/BrandTO;
    :cond_2
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v6, :cond_3

    .line 65
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_complete_2:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 66
    :cond_3
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v7, :cond_4

    .line 67
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_complete_3:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 68
    :cond_4
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v8, :cond_0

    .line 69
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_complete_4:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 72
    :cond_5
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v5, :cond_6

    .line 73
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_1:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 74
    :cond_6
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v6, :cond_7

    .line 75
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_2:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 76
    :cond_7
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v7, :cond_8

    .line 77
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_3:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 78
    :cond_8
    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v2

    if-ne v2, v8, :cond_0

    .line 79
    iget-object v2, v1, Llogo/quiz/commons/ImageAdapterCommons$ViewHolder;->imageView:Landroid/widget/ImageView;

    sget v3, Llogo/quiz/commons/R$drawable;->bg_4:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method
