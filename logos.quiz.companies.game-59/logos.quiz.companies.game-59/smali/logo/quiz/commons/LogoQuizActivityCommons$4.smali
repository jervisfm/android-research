.class Llogo/quiz/commons/LogoQuizActivityCommons$4;
.super Ljava/lang/Object;
.source "LogoQuizActivityCommons.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogo/quiz/commons/LogoQuizActivityCommons;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Llogo/quiz/commons/LogoQuizActivityCommons;


# direct methods
.method constructor <init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 127
    invoke-static {}, Lcom/swarmconnect/Swarm;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-static {}, Lcom/swarmconnect/Swarm;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    iget-object v0, v0, Llogo/quiz/commons/LogoQuizActivityCommons;->myActivity:Landroid/app/Activity;

    iget-object v1, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    invoke-virtual {v1}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v1

    invoke-interface {v1}, Llogo/quiz/commons/ConstantsProvider;->getSwarmconnectAppId()I

    move-result v1

    iget-object v2, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    invoke-virtual {v2}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v2

    invoke-interface {v2}, Llogo/quiz/commons/ConstantsProvider;->getSwarmconnectAppKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    #getter for: Llogo/quiz/commons/LogoQuizActivityCommons;->mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;
    invoke-static {v3}, Llogo/quiz/commons/LogoQuizActivityCommons;->access$0(Llogo/quiz/commons/LogoQuizActivityCommons;)Lcom/swarmconnect/delegates/SwarmLoginListener;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/swarmconnect/Swarm;->init(Landroid/app/Activity;ILjava/lang/String;Lcom/swarmconnect/delegates/SwarmLoginListener;)V

    .line 136
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    invoke-virtual {v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v0

    invoke-interface {v0}, Llogo/quiz/commons/ConstantsProvider;->getSwarmconnectHighScoreId()I

    move-result v0

    iget-object v1, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    iget-object v1, v1, Llogo/quiz/commons/LogoQuizActivityCommons;->cb:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

    invoke-static {v0, v1}, Lcom/swarmconnect/SwarmLeaderboard;->getLeaderboardById(ILcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;)V

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons$4;->this$0:Llogo/quiz/commons/LogoQuizActivityCommons;

    iget-object v0, v0, Llogo/quiz/commons/LogoQuizActivityCommons;->myActivity:Landroid/app/Activity;

    const-string v1, "You have to be online to see high scores"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
