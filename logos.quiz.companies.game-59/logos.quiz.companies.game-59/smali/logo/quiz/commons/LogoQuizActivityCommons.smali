.class public abstract Llogo/quiz/commons/LogoQuizActivityCommons;
.super Lcom/swarmconnect/SwarmActivity;
.source "LogoQuizActivityCommons.java"


# instance fields
.field cb:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

.field myActivity:Landroid/app/Activity;

.field private final mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    .line 38
    new-instance v0, Llogo/quiz/commons/LogoQuizActivityCommons$1;

    invoke-direct {v0, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$1;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    iput-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons;->cb:Lcom/swarmconnect/SwarmLeaderboard$GotLeaderboardCB;

    .line 50
    new-instance v0, Llogo/quiz/commons/LogoQuizActivityCommons$2;

    invoke-direct {v0, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$2;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    iput-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons;->mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;

    .line 32
    return-void
.end method

.method static synthetic access$0(Llogo/quiz/commons/LogoQuizActivityCommons;)Lcom/swarmconnect/delegates/SwarmLoginListener;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Llogo/quiz/commons/LogoQuizActivityCommons;->mySwarmLoginListener:Lcom/swarmconnect/delegates/SwarmLoginListener;

    return-object v0
.end method


# virtual methods
.method protected abstract getCompletedPoints()I
.end method

.method protected abstract getConstants()Llogo/quiz/commons/ConstantsProvider;
.end method

.method public getFreeHints(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 188
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "FreeHintsActivity"

    invoke-static {v2, v3}, Llogo/quiz/commons/DeviceUtilCommons;->getClassByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 189
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0, v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 191
    return-void
.end method

.method protected abstract getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;
.end method

.method public info(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 176
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "OptionsActivity"

    invoke-static {v2, v3}, Llogo/quiz/commons/DeviceUtilCommons;->getClassByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 178
    invoke-virtual {p0, v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 179
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "savedInstanceState"

    .prologue
    const/16 v8, 0x400

    const/4 v7, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    .line 75
    invoke-virtual {p0, v6, v6}, Llogo/quiz/commons/LogoQuizActivityCommons;->overridePendingTransition(II)V

    .line 76
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v4

    invoke-interface {v4}, Llogo/quiz/commons/ConstantsProvider;->getFlurryKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, v7}, Llogo/quiz/commons/LogoQuizActivityCommons;->requestWindowFeature(I)Z

    .line 79
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 80
    sget v4, Llogo/quiz/commons/R$layout;->main:I

    invoke-virtual {p0, v4}, Llogo/quiz/commons/LogoQuizActivityCommons;->setContentView(I)V

    .line 82
    iput-object p0, p0, Llogo/quiz/commons/LogoQuizActivityCommons;->myActivity:Landroid/app/Activity;

    .line 84
    new-instance v4, Llogo/quiz/commons/LogoQuizActivityCommons$3;

    invoke-direct {v4, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$3;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    invoke-static {v4}, Lcom/swarmconnect/Swarm;->addNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V

    .line 108
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;

    move-result-object v4

    invoke-interface {v4, p0}, Llogo/quiz/commons/ScoreUtilProvider;->initLogos(Landroid/app/Activity;)V

    .line 110
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 111
    .local v3, settings:Landroid/content/SharedPreferences;
    const-string v4, "allHints"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 112
    .local v0, allHintsCount:I
    if-ne v0, v5, :cond_0

    .line 113
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 114
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v4, "allHints"

    const/4 v5, 0x3

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 115
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 119
    .end local v1           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;

    move-result-object v4

    invoke-interface {v4}, Llogo/quiz/commons/ScoreUtilProvider;->getNewLogosCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 120
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;

    move-result-object v5

    invoke-interface {v5}, Llogo/quiz/commons/ScoreUtilProvider;->getNewLogosCount()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new logos to guess!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 121
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;

    move-result-object v4

    invoke-interface {v4, v6}, Llogo/quiz/commons/ScoreUtilProvider;->setNewLogosCount(I)V

    .line 124
    :cond_1
    sget v4, Llogo/quiz/commons/R$id;->my_swarm_button:I

    invoke-virtual {p0, v4}, Llogo/quiz/commons/LogoQuizActivityCommons;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 125
    .local v2, myButton:Landroid/widget/Button;
    new-instance v4, Llogo/quiz/commons/LogoQuizActivityCommons$4;

    invoke-direct {v4, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$4;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    :try_start_0
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v5

    invoke-interface {v5}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v5

    invoke-virtual {v5}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v6

    invoke-interface {v6}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v6

    invoke-virtual {v6}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/tapjoy/TapjoyConnect;->requestTapjoyConnect(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 202
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 204
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 205
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 206
    const-string v1, "Exit"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 207
    const-string v1, "Are you sure you want to leave?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 208
    const/high16 v1, 0x104

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 209
    const v1, 0x104000a

    new-instance v2, Llogo/quiz/commons/LogoQuizActivityCommons$6;

    invoke-direct {v2, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$6;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 219
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/swarmconnect/SwarmActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onResume()V

    .line 150
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v0

    invoke-interface {v0}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v1

    invoke-interface {v1}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v1

    invoke-virtual {v1}, Llogo/quiz/commons/TapjoyKeys;->getAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getConstants()Llogo/quiz/commons/ConstantsProvider;

    move-result-object v2

    invoke-interface {v2}, Llogo/quiz/commons/ConstantsProvider;->getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;

    move-result-object v2

    invoke-virtual {v2}, Llogo/quiz/commons/TapjoyKeys;->getSecret()Ljava/lang/String;

    move-result-object v2

    .line 152
    new-instance v3, Llogo/quiz/commons/LogoQuizActivityCommons$5;

    invoke-direct {v3, p0}, Llogo/quiz/commons/LogoQuizActivityCommons$5;-><init>(Llogo/quiz/commons/LogoQuizActivityCommons;)V

    .line 151
    invoke-static {v0, v1, v2, v3}, Llogo/quiz/commons/FreeHintsActivityCommons;->tapjoyPointToHints(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 160
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 165
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 166
    return-void
.end method

.method public options(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 182
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "OptionsActivity"

    invoke-static {v2, v3}, Llogo/quiz/commons/DeviceUtilCommons;->getClassByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 183
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 184
    invoke-virtual {p0, v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 185
    return-void
.end method

.method public play(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 169
    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Llogo/quiz/commons/LevelUtil;->setActiveLevel(Landroid/content/Context;I)V

    .line 170
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "LevelsActivity"

    invoke-static {v2, v3}, Llogo/quiz/commons/DeviceUtilCommons;->getClassByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 172
    invoke-virtual {p0, v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 173
    return-void
.end method

.method public stats(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 194
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "StatisticsActivity"

    invoke-static {v2, v3}, Llogo/quiz/commons/DeviceUtilCommons;->getClassByName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 196
    invoke-virtual {p0, v0}, Llogo/quiz/commons/LogoQuizActivityCommons;->startActivity(Landroid/content/Intent;)V

    .line 197
    return-void
.end method
