.class public Llogo/quiz/commons/MagicTextView$Shadow;
.super Ljava/lang/Object;
.source "MagicTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llogo/quiz/commons/MagicTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Shadow"
.end annotation


# instance fields
.field color:I

.field dx:F

.field dy:F

.field r:F


# direct methods
.method public constructor <init>(FFFI)V
    .locals 0
    .parameter "r"
    .parameter "dx"
    .parameter "dy"
    .parameter "color"

    .prologue
    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 321
    iput p1, p0, Llogo/quiz/commons/MagicTextView$Shadow;->r:F

    .line 322
    iput p2, p0, Llogo/quiz/commons/MagicTextView$Shadow;->dx:F

    .line 323
    iput p3, p0, Llogo/quiz/commons/MagicTextView$Shadow;->dy:F

    .line 324
    iput p4, p0, Llogo/quiz/commons/MagicTextView$Shadow;->color:I

    .line 325
    return-void
.end method
