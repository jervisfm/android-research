.class public Llogo/quiz/commons/MagicTextView;
.super Landroid/widget/TextView;
.source "MagicTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llogo/quiz/commons/MagicTextView$Shadow;
    }
.end annotation


# instance fields
.field private canvasStore:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private foregroundDrawable:Landroid/graphics/drawable/Drawable;

.field private frozen:Z

.field private innerShadows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llogo/quiz/commons/MagicTextView$Shadow;",
            ">;"
        }
    .end annotation
.end field

.field private lockedCompoundPadding:[I

.field private outerShadows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llogo/quiz/commons/MagicTextView$Shadow;",
            ">;"
        }
    .end annotation
.end field

.field private strokeColor:Ljava/lang/Integer;

.field private strokeJoin:Landroid/graphics/Paint$Join;

.field private strokeMiter:F

.field private strokeWidth:F

.field private tempBitmap:Landroid/graphics/Bitmap;

.field private tempCanvas:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter "context"

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Llogo/quiz/commons/MagicTextView;->init(Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    .line 52
    invoke-virtual {p0, p2}, Llogo/quiz/commons/MagicTextView;->init(Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    .line 56
    invoke-virtual {p0, p2}, Llogo/quiz/commons/MagicTextView;->init(Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method private generateTempCanvas()V
    .locals 6

    .prologue
    .line 235
    const-string v2, "%dx%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getHeight()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, key:Ljava/lang/String;
    iget-object v2, p0, Llogo/quiz/commons/MagicTextView;->canvasStore:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 237
    .local v1, stored:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/graphics/Canvas;Landroid/graphics/Bitmap;>;"
    if-eqz v1, :cond_0

    .line 238
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Canvas;

    iput-object v2, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    .line 239
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    iput-object v2, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    .line 246
    :goto_0
    return-void

    .line 241
    :cond_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    iput-object v2, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    .line 242
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    .line 243
    iget-object v2, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    iget-object v3, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 244
    iget-object v2, p0, Llogo/quiz/commons/MagicTextView;->canvasStore:Ljava/util/WeakHashMap;

    new-instance v3, Landroid/util/Pair;

    iget-object v4, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public addInnerShadow(FFFI)V
    .locals 2
    .parameter "r"
    .parameter "dx"
    .parameter "dy"
    .parameter "color"

    .prologue
    .line 139
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    const p1, 0x38d1b717

    .line 140
    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->innerShadows:Ljava/util/ArrayList;

    new-instance v1, Llogo/quiz/commons/MagicTextView$Shadow;

    invoke-direct {v1, p1, p2, p3, p4}, Llogo/quiz/commons/MagicTextView$Shadow;-><init>(FFFI)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    return-void
.end method

.method public addOuterShadow(FFFI)V
    .locals 2
    .parameter "r"
    .parameter "dx"
    .parameter "dy"
    .parameter "color"

    .prologue
    .line 134
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    const p1, 0x38d1b717

    .line 135
    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->outerShadows:Ljava/util/ArrayList;

    new-instance v1, Llogo/quiz/commons/MagicTextView$Shadow;

    invoke-direct {v1, p1, p2, p3, p4}, Llogo/quiz/commons/MagicTextView$Shadow;-><init>(FFFI)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    return-void
.end method

.method public clearInnerShadows()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->innerShadows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 145
    return-void
.end method

.method public clearOuterShadows()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->outerShadows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 149
    return-void
.end method

.method public freeze()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 251
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 252
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCompoundPaddingLeft()I

    move-result v2

    aput v2, v0, v1

    .line 253
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCompoundPaddingRight()I

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x2

    .line 254
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCompoundPaddingTop()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 255
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCompoundPaddingBottom()I

    move-result v2

    aput v2, v0, v1

    .line 251
    iput-object v0, p0, Llogo/quiz/commons/MagicTextView;->lockedCompoundPadding:[I

    .line 257
    iput-boolean v3, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    .line 258
    return-void
.end method

.method public getCompoundPaddingBottom()I
    .locals 2

    .prologue
    .line 312
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getCompoundPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->lockedCompoundPadding:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 297
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->lockedCompoundPadding:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 302
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->lockedCompoundPadding:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getCompoundPaddingTop()I
    .locals 2

    .prologue
    .line 307
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->getCompoundPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->lockedCompoundPadding:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getForeground()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCurrentTextColor()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method

.method public init(Landroid/util/AttributeSet;)V
    .locals 14
    .parameter "attrs"

    .prologue
    .line 60
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Llogo/quiz/commons/MagicTextView;->outerShadows:Ljava/util/ArrayList;

    .line 61
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Llogo/quiz/commons/MagicTextView;->innerShadows:Ljava/util/ArrayList;

    .line 62
    iget-object v9, p0, Llogo/quiz/commons/MagicTextView;->canvasStore:Ljava/util/WeakHashMap;

    if-nez v9, :cond_0

    .line 63
    new-instance v9, Ljava/util/WeakHashMap;

    invoke-direct {v9}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v9, p0, Llogo/quiz/commons/MagicTextView;->canvasStore:Ljava/util/WeakHashMap;

    .line 66
    :cond_0
    if-eqz p1, :cond_6

    .line 67
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getContext()Landroid/content/Context;

    move-result-object v9

    sget-object v10, Llogo/quiz/commons/R$styleable;->MagicTextView:[I

    invoke-virtual {v9, p1, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    .local v0, a:Landroid/content/res/TypedArray;
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 70
    .local v8, typefaceName:Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 71
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    const-string v10, "fonts/%s.ttf"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v7

    .line 72
    .local v7, tf:Landroid/graphics/Typeface;
    invoke-virtual {p0, v7}, Llogo/quiz/commons/MagicTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 75
    .end local v7           #tf:Landroid/graphics/Typeface;
    :cond_1
    const/16 v9, 0x9

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 76
    const/16 v9, 0x9

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 77
    .local v2, foreground:Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_7

    .line 78
    invoke-virtual {p0, v2}, Llogo/quiz/commons/MagicTextView;->setForegroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 84
    .end local v2           #foreground:Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_0
    const/16 v9, 0xa

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 85
    const/16 v9, 0xa

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 86
    .local v1, background:Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_8

    .line 87
    invoke-virtual {p0, v1}, Llogo/quiz/commons/MagicTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 93
    .end local v1           #background:Landroid/graphics/drawable/Drawable;
    :cond_3
    :goto_1
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 94
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v9

    .line 95
    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v10

    .line 96
    const/4 v11, 0x3

    const/4 v12, 0x0

    invoke-virtual {v0, v11, v12}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    .line 97
    const/4 v12, 0x0

    const/high16 v13, -0x100

    invoke-virtual {v0, v12, v13}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v12

    .line 94
    invoke-virtual {p0, v9, v10, v11, v12}, Llogo/quiz/commons/MagicTextView;->addInnerShadow(FFFI)V

    .line 100
    :cond_4
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 101
    const/4 v9, 0x5

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v9

    .line 102
    const/4 v10, 0x6

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v10

    .line 103
    const/4 v11, 0x7

    const/4 v12, 0x0

    invoke-virtual {v0, v11, v12}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    .line 104
    const/4 v12, 0x4

    const/high16 v13, -0x100

    invoke-virtual {v0, v12, v13}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v12

    .line 101
    invoke-virtual {p0, v9, v10, v11, v12}, Llogo/quiz/commons/MagicTextView;->addOuterShadow(FFFI)V

    .line 107
    :cond_5
    const/16 v9, 0xd

    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 108
    const/16 v9, 0xb

    const/high16 v10, 0x3f80

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v6

    .line 109
    .local v6, strokeWidth:F
    const/16 v9, 0xd

    const/high16 v10, -0x100

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 110
    .local v3, strokeColor:I
    const/16 v9, 0xc

    const/high16 v10, 0x4120

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    .line 111
    .local v5, strokeMiter:F
    const/4 v4, 0x0

    .line 112
    .local v4, strokeJoin:Landroid/graphics/Paint$Join;
    const/16 v9, 0xe

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 117
    :goto_2
    invoke-virtual {p0, v6, v3, v4, v5}, Llogo/quiz/commons/MagicTextView;->setStroke(FILandroid/graphics/Paint$Join;F)V

    .line 120
    .end local v0           #a:Landroid/content/res/TypedArray;
    .end local v3           #strokeColor:I
    .end local v4           #strokeJoin:Landroid/graphics/Paint$Join;
    .end local v5           #strokeMiter:F
    .end local v6           #strokeWidth:F
    .end local v8           #typefaceName:Ljava/lang/String;
    :cond_6
    return-void

    .line 80
    .restart local v0       #a:Landroid/content/res/TypedArray;
    .restart local v2       #foreground:Landroid/graphics/drawable/Drawable;
    .restart local v8       #typefaceName:Ljava/lang/String;
    :cond_7
    const/16 v9, 0x9

    const/high16 v10, -0x100

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v9

    invoke-virtual {p0, v9}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 89
    .end local v2           #foreground:Landroid/graphics/drawable/Drawable;
    .restart local v1       #background:Landroid/graphics/drawable/Drawable;
    :cond_8
    const/16 v9, 0xa

    const/high16 v10, -0x100

    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v9

    invoke-virtual {p0, v9}, Llogo/quiz/commons/MagicTextView;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 113
    .end local v1           #background:Landroid/graphics/drawable/Drawable;
    .restart local v3       #strokeColor:I
    .restart local v4       #strokeJoin:Landroid/graphics/Paint$Join;
    .restart local v5       #strokeMiter:F
    .restart local v6       #strokeWidth:F
    :pswitch_0
    sget-object v4, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    goto :goto_2

    .line 114
    :pswitch_1
    sget-object v4, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    goto :goto_2

    .line 115
    :pswitch_2
    sget-object v4, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    goto :goto_2

    .line 112
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->invalidate()V

    .line 283
    :cond_0
    return-void
.end method

.method public invalidate(IIII)V
    .locals 1
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 292
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->invalidate(IIII)V

    .line 293
    :cond_0
    return-void
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 1
    .parameter "rect"

    .prologue
    .line 287
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->invalidate(Landroid/graphics/Rect;)V

    .line 288
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .parameter "canvas"

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 162
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 164
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->freeze()V

    .line 165
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 166
    .local v1, restoreBackground:Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 167
    .local v3, restoreDrawables:[Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getCurrentTextColor()I

    move-result v2

    .line 169
    .local v2, restoreColor:I
    invoke-virtual {p0, v11, v11, v11, v11}, Llogo/quiz/commons/MagicTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 171
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->outerShadows:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    .line 175
    invoke-virtual {p0, v10, v10, v10, v12}, Llogo/quiz/commons/MagicTextView;->setShadowLayer(FFFI)V

    .line 176
    invoke-virtual {p0, v2}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 178
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_0

    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v5, v5, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_0

    .line 179
    invoke-direct {p0}, Llogo/quiz/commons/MagicTextView;->generateTempCanvas()V

    .line 180
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-super {p0, v5}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 181
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 182
    .local v0, paint:Landroid/graphics/Paint;
    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 183
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 184
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 185
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v5, v10, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 186
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v12, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 189
    .end local v0           #paint:Landroid/graphics/Paint;
    :cond_0
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->strokeColor:Ljava/lang/Integer;

    if-eqz v5, :cond_1

    .line 190
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 191
    .local v0, paint:Landroid/text/TextPaint;
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 192
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->strokeJoin:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 193
    iget v5, p0, Llogo/quiz/commons/MagicTextView;->strokeMiter:F

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setStrokeMiter(F)V

    .line 194
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->strokeColor:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v5}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 195
    iget v5, p0, Llogo/quiz/commons/MagicTextView;->strokeWidth:F

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 196
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 197
    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 198
    invoke-virtual {p0, v2}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 200
    .end local v0           #paint:Landroid/text/TextPaint;
    :cond_1
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->innerShadows:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 201
    invoke-direct {p0}, Llogo/quiz/commons/MagicTextView;->generateTempCanvas()V

    .line 202
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 203
    .restart local v0       #paint:Landroid/text/TextPaint;
    iget-object v5, p0, Llogo/quiz/commons/MagicTextView;->innerShadows:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_5

    .line 225
    .end local v0           #paint:Landroid/text/TextPaint;
    :cond_2
    if-eqz v3, :cond_3

    .line 226
    aget-object v5, v3, v12

    const/4 v6, 0x1

    aget-object v6, v3, v6

    const/4 v7, 0x2

    aget-object v7, v3, v7

    const/4 v8, 0x3

    aget-object v8, v3, v8

    invoke-virtual {p0, v5, v6, v7, v8}, Llogo/quiz/commons/MagicTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 228
    :cond_3
    invoke-virtual {p0, v1}, Llogo/quiz/commons/MagicTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 229
    invoke-virtual {p0, v2}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 231
    invoke-virtual {p0}, Llogo/quiz/commons/MagicTextView;->unfreeze()V

    .line 232
    return-void

    .line 171
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llogo/quiz/commons/MagicTextView$Shadow;

    .line 172
    .local v4, shadow:Llogo/quiz/commons/MagicTextView$Shadow;
    iget v6, v4, Llogo/quiz/commons/MagicTextView$Shadow;->r:F

    iget v7, v4, Llogo/quiz/commons/MagicTextView$Shadow;->dx:F

    iget v8, v4, Llogo/quiz/commons/MagicTextView$Shadow;->dy:F

    iget v9, v4, Llogo/quiz/commons/MagicTextView$Shadow;->color:I

    invoke-virtual {p0, v6, v7, v8, v9}, Llogo/quiz/commons/MagicTextView;->setShadowLayer(FFFI)V

    .line 173
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 203
    .end local v4           #shadow:Llogo/quiz/commons/MagicTextView$Shadow;
    .restart local v0       #paint:Landroid/text/TextPaint;
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llogo/quiz/commons/MagicTextView$Shadow;

    .line 204
    .restart local v4       #shadow:Llogo/quiz/commons/MagicTextView$Shadow;
    iget v6, v4, Llogo/quiz/commons/MagicTextView$Shadow;->color:I

    invoke-virtual {p0, v6}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 205
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-super {p0, v6}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 206
    const/high16 v6, -0x100

    invoke-virtual {p0, v6}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 207
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 208
    new-instance v6, Landroid/graphics/BlurMaskFilter;

    iget v7, v4, Llogo/quiz/commons/MagicTextView$Shadow;->r:F

    sget-object v8, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v6, v7, v8}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 210
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    .line 211
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    iget v7, v4, Llogo/quiz/commons/MagicTextView$Shadow;->dx:F

    iget v8, v4, Llogo/quiz/commons/MagicTextView$Shadow;->dy:F

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 212
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-super {p0, v6}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 213
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    .line 214
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v6, v10, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 215
    iget-object v6, p0, Llogo/quiz/commons/MagicTextView;->tempCanvas:Landroid/graphics/Canvas;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v12, v7}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 217
    invoke-virtual {v0, v11}, Landroid/text/TextPaint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 218
    invoke-virtual {v0, v11}, Landroid/text/TextPaint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 219
    invoke-virtual {p0, v2}, Llogo/quiz/commons/MagicTextView;->setTextColor(I)V

    .line 220
    invoke-virtual {p0, v10, v10, v10, v12}, Llogo/quiz/commons/MagicTextView;->setShadowLayer(FFFI)V

    goto/16 :goto_1
.end method

.method public postInvalidate()V
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->postInvalidate()V

    .line 273
    :cond_0
    return-void
.end method

.method public postInvalidate(IIII)V
    .locals 1
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 277
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->postInvalidate(IIII)V

    .line 278
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/TextView;->requestLayout()V

    .line 268
    :cond_0
    return-void
.end method

.method public setForegroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .parameter "d"

    .prologue
    .line 152
    iput-object p1, p0, Llogo/quiz/commons/MagicTextView;->foregroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 153
    return-void
.end method

.method public setStroke(FI)V
    .locals 2
    .parameter "width"
    .parameter "color"

    .prologue
    .line 130
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    const/high16 v1, 0x4120

    invoke-virtual {p0, p1, p2, v0, v1}, Llogo/quiz/commons/MagicTextView;->setStroke(FILandroid/graphics/Paint$Join;F)V

    .line 131
    return-void
.end method

.method public setStroke(FILandroid/graphics/Paint$Join;F)V
    .locals 1
    .parameter "width"
    .parameter "color"
    .parameter "join"
    .parameter "miter"

    .prologue
    .line 123
    iput p1, p0, Llogo/quiz/commons/MagicTextView;->strokeWidth:F

    .line 124
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llogo/quiz/commons/MagicTextView;->strokeColor:Ljava/lang/Integer;

    .line 125
    iput-object p3, p0, Llogo/quiz/commons/MagicTextView;->strokeJoin:Landroid/graphics/Paint$Join;

    .line 126
    iput p4, p0, Llogo/quiz/commons/MagicTextView;->strokeMiter:F

    .line 127
    return-void
.end method

.method public unfreeze()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Llogo/quiz/commons/MagicTextView;->frozen:Z

    .line 262
    return-void
.end method
