.class public Llogo/quiz/commons/MyAdCommons;
.super Ljava/lang/Object;
.source "MyAdCommons.java"


# instance fields
.field private adId:Ljava/lang/String;

.field private adName:Ljava/lang/String;

.field private adUrl:Ljava/lang/String;

.field private imageResource:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "imageResource"
    .parameter "adId"
    .parameter "adUrl"

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Llogo/quiz/commons/MyAdCommons;->imageResource:I

    .line 12
    iput-object p2, p0, Llogo/quiz/commons/MyAdCommons;->adId:Ljava/lang/String;

    .line 13
    iput-object p3, p0, Llogo/quiz/commons/MyAdCommons;->adUrl:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "imageResource"
    .parameter "adId"
    .parameter "adUrl"
    .parameter "adName"

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Llogo/quiz/commons/MyAdCommons;->imageResource:I

    .line 18
    iput-object p2, p0, Llogo/quiz/commons/MyAdCommons;->adId:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Llogo/quiz/commons/MyAdCommons;->adUrl:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Llogo/quiz/commons/MyAdCommons;->adName:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getAdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Llogo/quiz/commons/MyAdCommons;->adId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Llogo/quiz/commons/MyAdCommons;->adName:Ljava/lang/String;

    return-object v0
.end method

.method public getAdUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Llogo/quiz/commons/MyAdCommons;->adUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getImageResource()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Llogo/quiz/commons/MyAdCommons;->imageResource:I

    return v0
.end method

.method public setAdId(Ljava/lang/String;)V
    .locals 0
    .parameter "adId"

    .prologue
    .line 32
    iput-object p1, p0, Llogo/quiz/commons/MyAdCommons;->adId:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public setAdName(Ljava/lang/String;)V
    .locals 0
    .parameter "adName"

    .prologue
    .line 44
    iput-object p1, p0, Llogo/quiz/commons/MyAdCommons;->adName:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setAdUrl(Ljava/lang/String;)V
    .locals 0
    .parameter "adUrl"

    .prologue
    .line 38
    iput-object p1, p0, Llogo/quiz/commons/MyAdCommons;->adUrl:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setImageResource(I)V
    .locals 0
    .parameter "imageResource"

    .prologue
    .line 26
    iput p1, p0, Llogo/quiz/commons/MyAdCommons;->imageResource:I

    .line 27
    return-void
.end method
