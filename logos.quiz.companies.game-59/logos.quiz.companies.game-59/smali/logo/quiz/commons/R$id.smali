.class public final Llogo/quiz/commons/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llogo/quiz/commons/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept:I = 0x7f060073

.field public static final achievement_icon:I = 0x7f060056

.field public static final add:I = 0x7f06006e

.field public static final add_friend_username:I = 0x7f06006d

.field public static final askOnFbButton:I = 0x7f060025

.field public static final backButton:I = 0x7f060023

.field public static final background:I = 0x7f06005d

.field public static final benefits:I = 0x7f060069

.field public static final bevel:I = 0x7f060001

.field public static final brandImageReadMore:I = 0x7f06003c

.field public static final brandNameReadMore:I = 0x7f06003d

.field public static final btnStart:I = 0x7f060033

.field public static final buttonLevel:I = 0x7f06000e

.field public static final cancel:I = 0x7f06009f

.field public static final cap:I = 0x7f060003

.field public static final change_pass:I = 0x7f0600aa

.field public static final close:I = 0x7f060079

.field public static final closeButtonId:I = 0x7f060020

.field public static final coins:I = 0x7f06008b

.field public static final coins_icon:I = 0x7f060099

.field public static final completeBrandInfos:I = 0x7f060018

.field public static final completeLogoName:I = 0x7f060019

.field public static final completedLogosCounter:I = 0x7f060011

.field public static final completedLogosCounterList:I = 0x7f06002d

.field public static final completedLogosPointsLevel:I = 0x7f060010

.field public static final completedLogosPointsList:I = 0x7f06002c

.field public static final confirm:I = 0x7f060061

.field public static final confirm_error:I = 0x7f060060

.field public static final confirm_password:I = 0x7f0600a7

.field public static final create:I = 0x7f060064

.field public static final current_password:I = 0x7f0600a5

.field public static final date:I = 0x7f0600b3

.field public static final deny:I = 0x7f060074

.field public static final desc:I = 0x7f06009c

.field public static final description:I = 0x7f060059

.field public static final editLogo:I = 0x7f06001c

.field public static final editTextLogo:I = 0x7f06001d

.field public static final editWinLogo:I = 0x7f06001e

.field public static final email:I = 0x7f060063

.field public static final email_error:I = 0x7f060062

.field public static final empty_list:I = 0x7f06006b

.field public static final existing:I = 0x7f0600a0

.field public static final external_provider:I = 0x7f060095

.field public static final extra:I = 0x7f060066

.field public static final facebook_login:I = 0x7f060088

.field public static final footer:I = 0x7f060022

.field public static final freeHintsContainer:I = 0x7f060006

.field public static final friended_section:I = 0x7f060071

.field public static final get_coins:I = 0x7f0600ad

.field public static final guest:I = 0x7f0600a1

.field public static final header:I = 0x7f060078

.field public static final hintButton:I = 0x7f060024

.field public static final hintHelp:I = 0x7f060027

.field public static final hintLayout:I = 0x7f060026

.field public static final hintsCount:I = 0x7f060028

.field public static final hintsCountForm:I = 0x7f060015

.field public static final home:I = 0x7f06007a

.field public static final icon:I = 0x7f060065

.field public static final image:I = 0x7f06009b

.field public static final imageBrand:I = 0x7f06001b

.field public static final imageButton1:I = 0x7f060004

.field public static final level:I = 0x7f060009

.field public static final levelLabelList:I = 0x7f06002b

.field public static final levelLayout:I = 0x7f060007

.field public static final levelLogosCount:I = 0x7f06000b

.field public static final levelScore:I = 0x7f06000a

.field public static final levels_list_view:I = 0x7f060012

.field public static final list:I = 0x7f06006c

.field public static final list_item:I = 0x7f060031

.field public static final locklevel:I = 0x7f060008

.field public static final login:I = 0x7f060093

.field public static final logo:I = 0x7f060087

.field public static final logosGridView:I = 0x7f06002e

.field public static final logosLevelAd:I = 0x7f060013

.field public static final logosListAd:I = 0x7f06002f

.field public static final logosListAd2:I = 0x7f060017

.field public static final logosReadMoreAd:I = 0x7f06003f

.field public static final logout:I = 0x7f0600ac

.field public static final lost_password:I = 0x7f060092

.field public static final mail:I = 0x7f06008a

.field public static final mainLayout:I = 0x7f060016

.field public static final message:I = 0x7f060089

.field public static final messages:I = 0x7f06007b

.field public static final miter:I = 0x7f060000

.field public static final more:I = 0x7f06008e

.field public static final moreApps:I = 0x7f06003a

.field public static final myAd1:I = 0x7f060030

.field public static final myAd2:I = 0x7f060021

.field public static final myAdLevel:I = 0x7f060014

.field public static final myAdReadMore:I = 0x7f060040

.field public static final my_swarm_button:I = 0x7f060035

.field public static final name:I = 0x7f06005b

.field public static final navigationBar:I = 0x7f06000f

.field public static final navigationLogosListBar:I = 0x7f06002a

.field public static final new_password:I = 0x7f0600a6

.field public static final notifications:I = 0x7f0600ab

.field public static final num_items:I = 0x7f0600af

.field public static final offline_error:I = 0x7f060085

.field public static final offline_error_underline:I = 0x7f060086

.field public static final online_text:I = 0x7f060070

.field public static final optionsBtn:I = 0x7f060036

.field public static final password:I = 0x7f06005f

.field public static final password_box:I = 0x7f0600a9

.field public static final password_error:I = 0x7f06005e

.field public static final paypal:I = 0x7f060077

.field public static final pic:I = 0x7f06005a

.field public static final points:I = 0x7f060057

.field public static final popup_bottom_half:I = 0x7f06009d

.field public static final popup_header:I = 0x7f06008f

.field public static final popup_top_half:I = 0x7f06009a

.field public static final price:I = 0x7f0600ae

.field public static final profile_pic:I = 0x7f06006f

.field public static final progressBarPercentlevel:I = 0x7f06000d

.field public static final progressBarlevel:I = 0x7f06000c

.field public static final progressbar:I = 0x7f060090

.field public static final publisher:I = 0x7f06005c

.field public static final purchase:I = 0x7f06009e

.field public static final rank:I = 0x7f06008c

.field public static final rateThisApp:I = 0x7f060039

.field public static final readMoreButton:I = 0x7f06001a

.field public static final reply:I = 0x7f0600b0

.field public static final resetApp:I = 0x7f06003b

.field public static final round:I = 0x7f060002

.field public static final row:I = 0x7f060094

.field public static final score:I = 0x7f06008d

.field public static final scoreId:I = 0x7f06001f

.field public static final select_text:I = 0x7f060091

.field public static final send:I = 0x7f0600b1

.field public static final set_pass:I = 0x7f0600a8

.field public static final set_password_box:I = 0x7f0600a4

.field public static final sheader:I = 0x7f06007c

.field public static final sheader_btn1:I = 0x7f06007f

.field public static final sheader_btn1_dropdown:I = 0x7f060081

.field public static final sheader_btn1_text:I = 0x7f060080

.field public static final sheader_btn2:I = 0x7f060082

.field public static final sheader_btn2_dropdown:I = 0x7f060084

.field public static final sheader_btn2_text:I = 0x7f060083

.field public static final sheader_icon:I = 0x7f06007d

.field public static final sheader_title:I = 0x7f06007e

.field public static final soundMute:I = 0x7f060037

.field public static final speech_bubble:I = 0x7f0600b2

.field public static final statisticsAd:I = 0x7f060055

.field public static final statisticsAdContainer:I = 0x7f060054

.field public static final statisticsContainer:I = 0x7f060041

.field public static final statsAllHints:I = 0x7f06004e

.field public static final statsAllHintsPercent:I = 0x7f06004f

.field public static final statsBtn:I = 0x7f060034

.field public static final statsGuessTries:I = 0x7f060046

.field public static final statsGuessTriesPercent:I = 0x7f060047

.field public static final statsGuessedLogos:I = 0x7f060042

.field public static final statsGuessedLogosPercent:I = 0x7f060043

.field public static final statsHints:I = 0x7f060050

.field public static final statsHintsPercent:I = 0x7f060051

.field public static final statsLevelsCompleted:I = 0x7f06004c

.field public static final statsLevelsCompletedPercent:I = 0x7f06004d

.field public static final statsLevelsUnlocked:I = 0x7f06004a

.field public static final statsLevelsUnlockedPercent:I = 0x7f06004b

.field public static final statsPerfectGuessCount:I = 0x7f060044

.field public static final statsPerfectGuessCountPercent:I = 0x7f060045

.field public static final statsScore:I = 0x7f060048

.field public static final statsScorePercent:I = 0x7f060049

.field public static final statsUsedHints:I = 0x7f060052

.field public static final statsUsedHintsPercent:I = 0x7f060053

.field public static final submit:I = 0x7f060096

.field public static final subtitle:I = 0x7f060032

.field public static final success:I = 0x7f060097

.field public static final swarm_purchase_popup:I = 0x7f060098

.field public static final tapjoy:I = 0x7f060075

.field public static final tapjoyOffers:I = 0x7f060029

.field public static final terms:I = 0x7f06006a

.field public static final title:I = 0x7f060058

.field public static final unfriended_section:I = 0x7f060072

.field public static final upgrade:I = 0x7f0600a3

.field public static final upgrade_box:I = 0x7f0600a2

.field public static final username:I = 0x7f060068

.field public static final username_error:I = 0x7f060067

.field public static final vibrate:I = 0x7f060038

.field public static final webBrandInfoReadMore:I = 0x7f06003e

.field public static final webview:I = 0x7f0600b4

.field public static final yourHintsInfo:I = 0x7f060005

.field public static final zong:I = 0x7f060076


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
