.class public final Llogo/quiz/commons/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llogo/quiz/commons/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final free_hints:I = 0x7f030000

.field public static final high_score:I = 0x7f030001

.field public static final hints_button:I = 0x7f030002

.field public static final level1:I = 0x7f030003

.field public static final levels:I = 0x7f030004

.field public static final logos_form:I = 0x7f030005

.field public static final logos_hints:I = 0x7f030006

.field public static final logos_list:I = 0x7f030007

.field public static final logos_list_item:I = 0x7f030008

.field public static final main:I = 0x7f030009

.field public static final options:I = 0x7f03000a

.field public static final read_more:I = 0x7f03000b

.field public static final statistics:I = 0x7f03000c

.field public static final swarm_achievement_row:I = 0x7f03000d

.field public static final swarm_app_row:I = 0x7f03000e

.field public static final swarm_create_account:I = 0x7f03000f

.field public static final swarm_dashboard_row:I = 0x7f030010

.field public static final swarm_external_username:I = 0x7f030011

.field public static final swarm_friends:I = 0x7f030012

.field public static final swarm_friends_row:I = 0x7f030013

.field public static final swarm_get_coins:I = 0x7f030014

.field public static final swarm_header:I = 0x7f030015

.field public static final swarm_header_external:I = 0x7f030016

.field public static final swarm_inbox_row:I = 0x7f030017

.field public static final swarm_inventory:I = 0x7f030018

.field public static final swarm_leaderboard_list:I = 0x7f030019

.field public static final swarm_leaderboard_row:I = 0x7f03001a

.field public static final swarm_list:I = 0x7f03001b

.field public static final swarm_list_section_header:I = 0x7f03001c

.field public static final swarm_load_more:I = 0x7f03001d

.field public static final swarm_loading_popup:I = 0x7f03001e

.field public static final swarm_login:I = 0x7f03001f

.field public static final swarm_login_row:I = 0x7f030020

.field public static final swarm_lost_password:I = 0x7f030021

.field public static final swarm_purchase_popup:I = 0x7f030022

.field public static final swarm_select_username:I = 0x7f030023

.field public static final swarm_settings:I = 0x7f030024

.field public static final swarm_store:I = 0x7f030025

.field public static final swarm_store_category:I = 0x7f030026

.field public static final swarm_store_category_row:I = 0x7f030027

.field public static final swarm_store_row:I = 0x7f030028

.field public static final swarm_thread:I = 0x7f030029

.field public static final swarm_thread_row_other:I = 0x7f03002a

.field public static final swarm_thread_row_self:I = 0x7f03002b

.field public static final swarm_upgrade_guest:I = 0x7f03002c

.field public static final swarm_webview:I = 0x7f03002d

.field public static final tapjoy_offers_button:I = 0x7f03002e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
