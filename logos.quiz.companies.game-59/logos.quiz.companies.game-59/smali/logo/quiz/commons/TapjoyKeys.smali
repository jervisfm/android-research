.class public Llogo/quiz/commons/TapjoyKeys;
.super Ljava/lang/Object;
.source "TapjoyKeys.java"


# instance fields
.field appId:Ljava/lang/String;

.field currencyId:Ljava/lang/String;

.field secret:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "appId"
    .parameter "secret"
    .parameter "currencyId"

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Llogo/quiz/commons/TapjoyKeys;->appId:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Llogo/quiz/commons/TapjoyKeys;->secret:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Llogo/quiz/commons/TapjoyKeys;->currencyId:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Llogo/quiz/commons/TapjoyKeys;->appId:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Llogo/quiz/commons/TapjoyKeys;->currencyId:Ljava/lang/String;

    return-object v0
.end method

.method public getSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Llogo/quiz/commons/TapjoyKeys;->secret:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(Ljava/lang/String;)V
    .locals 0
    .parameter "appId"

    .prologue
    .line 19
    iput-object p1, p0, Llogo/quiz/commons/TapjoyKeys;->appId:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setCurrencyId(Ljava/lang/String;)V
    .locals 0
    .parameter "currencyId"

    .prologue
    .line 31
    iput-object p1, p0, Llogo/quiz/commons/TapjoyKeys;->currencyId:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setSecret(Ljava/lang/String;)V
    .locals 0
    .parameter "secret"

    .prologue
    .line 25
    iput-object p1, p0, Llogo/quiz/commons/TapjoyKeys;->secret:Ljava/lang/String;

    .line 26
    return-void
.end method
