.class public Llogos/quiz/companies/game/Adserwer;
.super Llogo/quiz/commons/AdserwerCommons;
.source "Adserwer.java"


# static fields
.field public static final REMOVE_AD_ID:Ljava/lang/String; = "ad9"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Llogo/quiz/commons/AdserwerCommons;-><init>()V

    return-void
.end method

.method public static getActiveAds(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/MyAdCommons;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    const-string v0, "ad9"

    invoke-static {p0, v0}, Llogos/quiz/companies/game/Adserwer;->getActiveAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getAdmob(Landroid/app/Activity;)Lcom/google/ads/AdView;
    .locals 1
    .parameter "myActivity"

    .prologue
    .line 22
    const-string v0, "a14fc12954d4f22"

    invoke-static {p0, v0}, Llogos/quiz/companies/game/Adserwer;->getAdmob(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/ads/AdView;

    move-result-object v0

    return-object v0
.end method

.method public static getAllAds(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Llogo/quiz/commons/MyAdCommons;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    const-string v0, "ad9"

    invoke-static {p0, v0}, Llogos/quiz/companies/game/Adserwer;->getAllAds(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static setAd(Landroid/widget/ImageView;Landroid/content/Context;)V
    .locals 1
    .parameter "myAd"
    .parameter "context"

    .prologue
    .line 18
    const-string v0, "ad9"

    invoke-static {p0, p1, v0}, Llogos/quiz/companies/game/Adserwer;->setAd(Landroid/widget/ImageView;Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    return-void
.end method
