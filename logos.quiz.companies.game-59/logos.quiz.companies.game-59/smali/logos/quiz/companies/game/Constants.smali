.class public Llogos/quiz/companies/game/Constants;
.super Ljava/lang/Object;
.source "Constants.java"

# interfaces
.implements Llogo/quiz/commons/ConstantsProvider;


# static fields
.field public static final ADMOB_PUB_ID:Ljava/lang/String; = "a14fc12954d4f22"

.field public static final CHARTBOOST_APP_ID:Ljava/lang/String; = "5074bf9016ba47531e000008"

.field public static final CHARTBOOST_APP_SIGNATURE:Ljava/lang/String; = "4031b2b8c86f878685ec205855e53c61c60accfd"

.field public static final FLURRY_KEY:Ljava/lang/String; = "GHNSFPPH779R3ZQTGTFJ"

.field public static final SWARMCONNECT_APP_ID:I = 0x317

.field public static final SWARMCONNECT_APP_KEY:Ljava/lang/String; = "c4828a20bf2176747643856f966ab231"

.field public static final SWARMCONNECT_HIGH_SCORE_ID:I = 0x461

.field public static final TAP_JOY_APP_CURRENCY_ID:Ljava/lang/String; = ""

.field public static final TAP_JOY_APP_ID:Ljava/lang/String; = "8928eece-ecf9-4708-8be3-22f3c3c61e43"

.field public static final TAP_JOY_APP_SECRET:Ljava/lang/String; = "KsC3RCwzFcQw6YRS0i5L"

.field public static leaderboard:Lcom/swarmconnect/SwarmLeaderboard;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdmobPubId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "a14fc12954d4f22"

    return-object v0
.end method

.method public getAdmobRemoveAdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "ad9"

    return-object v0
.end method

.method public getChartboostAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "5074bf9016ba47531e000008"

    return-object v0
.end method

.method public getChartboostAppSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "4031b2b8c86f878685ec205855e53c61c60accfd"

    return-object v0
.end method

.method public getFlurryKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "GHNSFPPH779R3ZQTGTFJ"

    return-object v0
.end method

.method public getLeaderboard()Lcom/swarmconnect/SwarmLeaderboard;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Llogos/quiz/companies/game/Constants;->leaderboard:Lcom/swarmconnect/SwarmLeaderboard;

    return-object v0
.end method

.method public getSwarmconnectAppId()I
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0x317

    return v0
.end method

.method public getSwarmconnectAppKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    const-string v0, "c4828a20bf2176747643856f966ab231"

    return-object v0
.end method

.method public getSwarmconnectHighScoreId()I
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0x461

    return v0
.end method

.method public getTapJoyAppCurrencyId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, ""

    return-object v0
.end method

.method public getTapJoyAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "8928eece-ecf9-4708-8be3-22f3c3c61e43"

    return-object v0
.end method

.method public getTapJoyAppSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const-string v0, "KsC3RCwzFcQw6YRS0i5L"

    return-object v0
.end method

.method public getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;
    .locals 4

    .prologue
    .line 23
    new-instance v0, Llogo/quiz/commons/TapjoyKeys;

    const-string v1, "8928eece-ecf9-4708-8be3-22f3c3c61e43"

    const-string v2, "KsC3RCwzFcQw6YRS0i5L"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Llogo/quiz/commons/TapjoyKeys;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public setLeaderboard(Lcom/swarmconnect/SwarmLeaderboard;)V
    .locals 0
    .parameter "leaderboard"

    .prologue
    .line 83
    sput-object p1, Llogos/quiz/companies/game/Constants;->leaderboard:Lcom/swarmconnect/SwarmLeaderboard;

    .line 84
    return-void
.end method
