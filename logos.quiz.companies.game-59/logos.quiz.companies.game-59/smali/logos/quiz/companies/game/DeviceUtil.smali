.class public Llogos/quiz/companies/game/DeviceUtil;
.super Ljava/lang/Object;
.source "DeviceUtil.java"


# static fields
.field private static final IMAGE_MAX_SIZE:I = 0x46

.field private static final TAG:Ljava/lang/String; = "DeviceUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dip(ILandroid/content/Context;)I
    .locals 3
    .parameter "value"
    .parameter "ctx"

    .prologue
    .line 73
    const/4 v0, 0x1

    int-to-float v1, p0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 4
    .parameter "ctx"

    .prologue
    .line 84
    invoke-static {p0}, Llogos/quiz/companies/game/DeviceUtil;->getDisplayDevice(Landroid/content/Context;)Landroid/view/Display;

    move-result-object v0

    .line 85
    .local v0, display:Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 86
    .local v1, size:Landroid/graphics/Point;
    return-object v1
.end method

.method private static getDisplayDevice(Landroid/content/Context;)Landroid/view/Display;
    .locals 2
    .parameter "ctx"

    .prologue
    .line 95
    const-string v1, "window"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 96
    .local v0, wm:Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    return-object v1
.end method

.method public static isOnline(Landroid/content/Context;)Z
    .locals 2
    .parameter "ctx"

    .prologue
    .line 106
    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 108
    .local v0, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    .line 108
    goto :goto_0
.end method

.method public static isPackageExists(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .parameter "targetPackage"
    .parameter "ctx"

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 115
    .local v2, pm:Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 119
    .local v1, info:Landroid/content/pm/PackageInfo;
    const/4 v3, 0x1

    .end local v1           #info:Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static playSound(Landroid/content/Context;I)V
    .locals 1
    .parameter "applicationContext"
    .parameter "soundResId"

    .prologue
    .line 77
    invoke-static {p0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    .line 78
    .local v0, mp:Landroid/media/MediaPlayer;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 81
    :cond_0
    return-void
.end method
