.class public Llogos/quiz/companies/game/FreeHintsActivity;
.super Llogo/quiz/commons/FreeHintsActivityCommons;
.source "FreeHintsActivity.java"


# instance fields
.field final flipAnimator:Llogo/quiz/commons/FlipAnimator;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    invoke-direct {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;-><init>()V

    .line 13
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    iput-object v0, p0, Llogos/quiz/companies/game/FreeHintsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 12
    return-void
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/FreeHintsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LogosQuizActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/FreeHintsActivity;->startActivity(Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method protected getAdmobPubId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "a14fc12954d4f22"

    return-object v0
.end method

.method protected getAdmobRemoveAdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "ad9"

    return-object v0
.end method

.method protected getChartBoostAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "5074bf9016ba47531e000008"

    return-object v0
.end method

.method protected getChartBoostSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "4031b2b8c86f878685ec205855e53c61c60accfd"

    return-object v0
.end method

.method protected getFacebookProfileId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "519844888042075"

    return-object v0
.end method

.method protected getGooglePlayUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    const-string v0, "market://details?id=logos.quiz.companies.game"

    return-object v0
.end method

.method public getRemoveAdId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const-string v0, "ad9"

    return-object v0
.end method

.method protected getTapjoyKeys()Llogo/quiz/commons/TapjoyKeys;
    .locals 4

    .prologue
    .line 32
    new-instance v0, Llogo/quiz/commons/TapjoyKeys;

    const-string v1, "8928eece-ecf9-4708-8be3-22f3c3c61e43"

    const-string v2, "KsC3RCwzFcQw6YRS0i5L"

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Llogo/quiz/commons/TapjoyKeys;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 17
    invoke-super {p0, p1}, Llogo/quiz/commons/FreeHintsActivityCommons;->onCreate(Landroid/os/Bundle;)V

    .line 18
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 70
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->onResume()V

    .line 23
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Llogo/quiz/commons/FreeHintsActivityCommons;->onStop()V

    .line 28
    return-void
.end method
