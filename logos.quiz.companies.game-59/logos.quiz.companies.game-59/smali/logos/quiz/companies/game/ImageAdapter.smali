.class public Llogos/quiz/companies/game/ImageAdapter;
.super Llogo/quiz/commons/ImageAdapterCommons;
.source "ImageAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "c"

    .prologue
    .line 11
    invoke-direct {p0, p1}, Llogo/quiz/commons/ImageAdapterCommons;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected getActiveLevelBrands()[Llogo/quiz/commons/BrandTO;
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v0

    .line 16
    .local v0, activeLevel:I
    iget-object v1, p0, Llogos/quiz/companies/game/ImageAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Llogos/quiz/companies/game/ScoreUtil;->getBrands(Landroid/content/Context;I)[Llogo/quiz/commons/BrandTO;

    move-result-object v1

    return-object v1
.end method
