.class public Llogos/quiz/companies/game/Level;
.super Ljava/lang/Object;
.source "Level.java"


# instance fields
.field private from:I

.field private id:I

.field private levelBackgroundResource:I

.field private to:I

.field private unlockLimit:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0
    .parameter "id"
    .parameter "from"
    .parameter "to"
    .parameter "unlockLimit"
    .parameter "levelBackgroundResource"

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Llogos/quiz/companies/game/Level;->id:I

    .line 13
    iput p2, p0, Llogos/quiz/companies/game/Level;->from:I

    .line 14
    iput p3, p0, Llogos/quiz/companies/game/Level;->to:I

    .line 15
    iput p4, p0, Llogos/quiz/companies/game/Level;->unlockLimit:I

    .line 16
    iput p5, p0, Llogos/quiz/companies/game/Level;->levelBackgroundResource:I

    .line 17
    return-void
.end method


# virtual methods
.method public getFrom()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Llogos/quiz/companies/game/Level;->from:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Llogos/quiz/companies/game/Level;->id:I

    return v0
.end method

.method public getLevelBackgroundResource()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Llogos/quiz/companies/game/Level;->levelBackgroundResource:I

    return v0
.end method

.method public getLogosCount()I
    .locals 2

    .prologue
    .line 37
    iget v0, p0, Llogos/quiz/companies/game/Level;->to:I

    iget v1, p0, Llogos/quiz/companies/game/Level;->from:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getTo()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Llogos/quiz/companies/game/Level;->to:I

    return v0
.end method

.method public getUnlockLimit()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Llogos/quiz/companies/game/Level;->unlockLimit:I

    return v0
.end method

.method public setFrom(I)V
    .locals 0
    .parameter "from"

    .prologue
    .line 28
    iput p1, p0, Llogos/quiz/companies/game/Level;->from:I

    .line 29
    return-void
.end method

.method public setId(I)V
    .locals 0
    .parameter "id"

    .prologue
    .line 22
    iput p1, p0, Llogos/quiz/companies/game/Level;->id:I

    .line 23
    return-void
.end method

.method public setLevelBackgroundResource(I)V
    .locals 0
    .parameter "levelBackgroundResource"

    .prologue
    .line 49
    iput p1, p0, Llogos/quiz/companies/game/Level;->levelBackgroundResource:I

    .line 50
    return-void
.end method

.method public setTo(I)V
    .locals 0
    .parameter "to"

    .prologue
    .line 34
    iput p1, p0, Llogos/quiz/companies/game/Level;->to:I

    .line 35
    return-void
.end method

.method public setUnlockLimit(I)V
    .locals 0
    .parameter "unlockLimit"

    .prologue
    .line 43
    iput p1, p0, Llogos/quiz/companies/game/Level;->unlockLimit:I

    .line 44
    return-void
.end method
