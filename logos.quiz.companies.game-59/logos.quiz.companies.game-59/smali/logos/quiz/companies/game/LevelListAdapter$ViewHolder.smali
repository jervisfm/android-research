.class Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "LevelListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llogos/quiz/companies/game/LevelListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolder"
.end annotation


# instance fields
.field private buttonLevel:Landroid/widget/Button;

.field private level:Landroid/widget/TextView;

.field private levelLayout:Landroid/view/View;

.field private levelLogosCountTextView:Landroid/widget/TextView;

.field private levelProgressBarlevel:Landroid/widget/ProgressBar;

.field private levelScoreTextView:Landroid/widget/TextView;

.field private locklevel:Landroid/widget/ImageView;

.field private progressBarPercentlevel:Landroid/widget/TextView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getButtonLevel()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->buttonLevel:Landroid/widget/Button;

    return-object v0
.end method

.method public getLevel()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->level:Landroid/widget/TextView;

    return-object v0
.end method

.method public getLevelLayout()Landroid/view/View;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelLayout:Landroid/view/View;

    return-object v0
.end method

.method public getLevelLogosCountTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelLogosCountTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getLevelProgressBarlevel()Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelProgressBarlevel:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public getLevelScoreTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelScoreTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getLocklevel()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->locklevel:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getProgressBarPercentlevel()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->progressBarPercentlevel:Landroid/widget/TextView;

    return-object v0
.end method

.method public setButtonLevel(Landroid/widget/Button;)V
    .locals 0
    .parameter "buttonLevel"

    .prologue
    .line 188
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->buttonLevel:Landroid/widget/Button;

    .line 189
    return-void
.end method

.method public setLevel(Landroid/widget/TextView;)V
    .locals 0
    .parameter "level"

    .prologue
    .line 194
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->level:Landroid/widget/TextView;

    .line 195
    return-void
.end method

.method public setLevelLayout(Landroid/view/View;)V
    .locals 0
    .parameter "levelLayout"

    .prologue
    .line 158
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelLayout:Landroid/view/View;

    .line 159
    return-void
.end method

.method public setLevelLogosCountTextView(Landroid/widget/TextView;)V
    .locals 0
    .parameter "levelLogosCountTextView"

    .prologue
    .line 182
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelLogosCountTextView:Landroid/widget/TextView;

    .line 183
    return-void
.end method

.method public setLevelProgressBarlevel(Landroid/widget/ProgressBar;)V
    .locals 0
    .parameter "levelProgressBarlevel"

    .prologue
    .line 170
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelProgressBarlevel:Landroid/widget/ProgressBar;

    .line 171
    return-void
.end method

.method public setLevelScoreTextView(Landroid/widget/TextView;)V
    .locals 0
    .parameter "levelScoreTextView"

    .prologue
    .line 176
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->levelScoreTextView:Landroid/widget/TextView;

    .line 177
    return-void
.end method

.method public setLocklevel(Landroid/widget/ImageView;)V
    .locals 0
    .parameter "locklevel"

    .prologue
    .line 200
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->locklevel:Landroid/widget/ImageView;

    .line 201
    return-void
.end method

.method public setProgressBarPercentlevel(Landroid/widget/TextView;)V
    .locals 0
    .parameter "progressBarPercentlevel"

    .prologue
    .line 164
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->progressBarPercentlevel:Landroid/widget/TextView;

    .line 165
    return-void
.end method
