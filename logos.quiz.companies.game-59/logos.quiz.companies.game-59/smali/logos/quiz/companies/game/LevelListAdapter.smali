.class public Llogos/quiz/companies/game/LevelListAdapter;
.super Landroid/widget/BaseAdapter;
.source "LevelListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private levels:[Llogos/quiz/companies/game/Level;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Llogos/quiz/companies/game/Level;)V
    .locals 1
    .parameter "context"
    .parameter "levels"

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 30
    iput-object p1, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Llogos/quiz/companies/game/LevelListAdapter;->levels:[Llogos/quiz/companies/game/Level;

    .line 33
    return-void
.end method

.method static synthetic access$0(Llogos/quiz/companies/game/LevelListAdapter;ILandroid/widget/LinearLayout;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Llogos/quiz/companies/game/LevelListAdapter;->playLevel(ILandroid/widget/LinearLayout;)V

    return-void
.end method

.method private playLevel(ILandroid/widget/LinearLayout;)V
    .locals 8
    .parameter "level"
    .parameter "levelRow"

    .prologue
    const/4 v7, 0x1

    .line 116
    if-ne p1, v7, :cond_1

    .line 117
    invoke-direct {p0, p1}, Llogos/quiz/companies/game/LevelListAdapter;->startLevel(I)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v4, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    add-int/lit8 v5, p1, -0x1

    invoke-static {v4, v5}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v0

    .line 120
    .local v0, completedLevelLogosCount:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v4

    add-int/lit8 v5, p1, -0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Llogos/quiz/companies/game/Level;->getUnlockLimit()I

    move-result v1

    .line 121
    .local v1, levelUnlockLimit:I
    if-lt v0, v1, :cond_2

    .line 122
    invoke-direct {p0, p1}, Llogos/quiz/companies/game/LevelListAdapter;->startLevel(I)V

    goto :goto_0

    .line 124
    :cond_2
    iget-object v4, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    const v5, 0x7f040003

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 125
    .local v3, shake:Landroid/view/animation/Animation;
    invoke-virtual {p2, v3}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 127
    iget-object v4, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Need "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " correct answers from level "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, p1, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 129
    iget-object v4, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 130
    .local v2, settings:Landroid/content/SharedPreferences;
    const-string v4, "SOUND"

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 131
    iget-object v4, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    const v5, 0x7f050002

    invoke-static {v4, v5}, Llogos/quiz/companies/game/DeviceUtil;->playSound(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private startLevel(I)V
    .locals 3
    .parameter "level"

    .prologue
    .line 138
    iget-object v1, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Llogo/quiz/commons/LevelUtil;->setActiveLevel(Landroid/content/Context;I)V

    .line 139
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    const-class v2, Llogos/quiz/companies/game/LogosListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 141
    iget-object v1, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 142
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Llogos/quiz/companies/game/LevelListAdapter;->levels:[Llogos/quiz/companies/game/Level;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter "position"

    .prologue
    .line 41
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter "position"

    .prologue
    .line 45
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 50
    if-nez p2, :cond_2

    .line 51
    iget-object v11, p0, Llogos/quiz/companies/game/LevelListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f030003

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 52
    new-instance v4, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;

    invoke-direct {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;-><init>()V

    .line 53
    .local v4, holder:Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;
    const v11, 0x7f060007

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLevelLayout(Landroid/view/View;)V

    .line 54
    const v11, 0x7f06000a

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLevelScoreTextView(Landroid/widget/TextView;)V

    .line 55
    const v11, 0x7f06000b

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLevelLogosCountTextView(Landroid/widget/TextView;)V

    .line 56
    const v11, 0x7f06000c

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ProgressBar;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLevelProgressBarlevel(Landroid/widget/ProgressBar;)V

    .line 57
    const v11, 0x7f06000d

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setProgressBarPercentlevel(Landroid/widget/TextView;)V

    .line 58
    const v11, 0x7f06000e

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setButtonLevel(Landroid/widget/Button;)V

    .line 59
    const v11, 0x7f060009

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLevel(Landroid/widget/TextView;)V

    .line 60
    const v11, 0x7f060008

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    invoke-virtual {v4, v11}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->setLocklevel(Landroid/widget/ImageView;)V

    .line 61
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 66
    :goto_0
    iget-object v11, p0, Llogos/quiz/companies/game/LevelListAdapter;->levels:[Llogos/quiz/companies/game/Level;

    aget-object v5, v11, p1

    .line 68
    .local v5, level:Llogos/quiz/companies/game/Level;
    add-int/lit8 v11, p1, 0x1

    iget-object v12, p0, Llogos/quiz/companies/game/LevelListAdapter;->levels:[Llogos/quiz/companies/game/Level;

    array-length v12, v12

    if-ne v11, v12, :cond_0

    .line 69
    const/16 v11, 0xa

    const/16 v12, 0xa

    const/16 v13, 0xa

    const/16 v14, 0xa

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/view/View;->setPadding(IIII)V

    .line 72
    :cond_0
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelLayout()Landroid/view/View;

    move-result-object v11

    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v12

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    aget-object v12, v12, v13

    invoke-virtual {v12}, Llogos/quiz/companies/game/Level;->getLevelBackgroundResource()I

    move-result v12

    invoke-virtual {v11, v12}, Landroid/view/View;->setBackgroundResource(I)V

    .line 74
    iget-object v11, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v12

    invoke-static {v11, v12}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v2

    .line 75
    .local v2, completedLevelLogosCount:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v11

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    aget-object v11, v11, v12

    invoke-virtual {v11}, Llogos/quiz/companies/game/Level;->getLogosCount()I

    move-result v10

    .line 76
    .local v10, totalLevelLogosCount:I
    iget-object v11, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v12

    invoke-static {v11, v12}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedPoints(Landroid/content/Context;I)I

    move-result v6

    .line 78
    .local v6, levelScore:I
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevel()Landroid/widget/TextView;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Level "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelScoreTextView()Landroid/widget/TextView;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelLogosCountTextView()Landroid/widget/TextView;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelProgressBarlevel()Landroid/widget/ProgressBar;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 84
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelProgressBarlevel()Landroid/widget/ProgressBar;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 86
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->floatValue()F

    move-result v11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->floatValue()F

    move-result v12

    div-float/2addr v11, v12

    const/high16 v12, 0x42c8

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 87
    .local v9, percent:I
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getProgressBarPercentlevel()Landroid/widget/TextView;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "%"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLocklevel()Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_3

    .line 90
    iget-object v11, p0, Llogos/quiz/companies/game/LevelListAdapter;->context:Landroid/content/Context;

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-static {v11, v12}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v1

    .line 91
    .local v1, completedBeforeLevelLogosCount:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v11

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    aget-object v11, v11, v12

    invoke-virtual {v11}, Llogos/quiz/companies/game/Level;->getUnlockLimit()I

    move-result v7

    .line 92
    .local v7, levelUnlockLimit:I
    if-lt v1, v7, :cond_1

    .line 93
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLocklevel()Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    .end local v1           #completedBeforeLevelLogosCount:I
    .end local v7           #levelUnlockLimit:I
    :cond_1
    :goto_1
    move-object/from16 v3, p2

    .line 100
    .local v3, convertViewFinal:Landroid/view/View;
    new-instance v8, Llogos/quiz/companies/game/LevelListAdapter$1;

    invoke-direct {v8, p0, v5, v3}, Llogos/quiz/companies/game/LevelListAdapter$1;-><init>(Llogos/quiz/companies/game/LevelListAdapter;Llogos/quiz/companies/game/Level;Landroid/view/View;)V

    .line 105
    .local v8, onClickListener:Landroid/view/View$OnClickListener;
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getButtonLevel()Landroid/widget/Button;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelLayout()Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevel()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelScoreTextView()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelLogosCountTextView()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLevelProgressBarlevel()Landroid/widget/ProgressBar;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/ProgressBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    return-object p2

    .line 63
    .end local v2           #completedLevelLogosCount:I
    .end local v3           #convertViewFinal:Landroid/view/View;
    .end local v4           #holder:Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;
    .end local v5           #level:Llogos/quiz/companies/game/Level;
    .end local v6           #levelScore:I
    .end local v8           #onClickListener:Landroid/view/View$OnClickListener;
    .end local v9           #percent:I
    .end local v10           #totalLevelLogosCount:I
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;

    .restart local v4       #holder:Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;
    goto/16 :goto_0

    .line 96
    .restart local v2       #completedLevelLogosCount:I
    .restart local v5       #level:Llogos/quiz/companies/game/Level;
    .restart local v6       #levelScore:I
    .restart local v9       #percent:I
    .restart local v10       #totalLevelLogosCount:I
    :cond_3
    invoke-virtual {v4}, Llogos/quiz/companies/game/LevelListAdapter$ViewHolder;->getLocklevel()Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
