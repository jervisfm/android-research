.class public Llogos/quiz/companies/game/LevelsActivity;
.super Lcom/swarmconnect/SwarmActivity;
.source "LevelsActivity.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# instance fields
.field adHandler:Landroid/os/Handler;

.field adView:Lcom/google/ads/AdView;

.field final flipAnimator:Llogo/quiz/commons/FlipAnimator;

.field myActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    .line 33
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    iput-object v0, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 29
    return-void
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LogosQuizActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 85
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/LevelsActivity;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/16 v9, 0x400

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 37
    invoke-virtual {p0, v8, v8}, Llogos/quiz/companies/game/LevelsActivity;->overridePendingTransition(II)V

    .line 38
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const-string v8, "GHNSFPPH779R3ZQTGTFJ"

    invoke-static {p0, v8}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0, v11}, Llogos/quiz/companies/game/LevelsActivity;->requestWindowFeature(I)Z

    .line 41
    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8, v9, v9}, Landroid/view/Window;->setFlags(II)V

    .line 42
    const v8, 0x7f030004

    invoke-virtual {p0, v8}, Llogos/quiz/companies/game/LevelsActivity;->setContentView(I)V

    .line 44
    iput-object p0, p0, Llogos/quiz/companies/game/LevelsActivity;->myActivity:Landroid/app/Activity;

    .line 46
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const-wide/16 v9, 0x1f4

    invoke-virtual {v8, v9, v10}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 47
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v8, v11}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 48
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    new-instance v9, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v9}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v8, v9}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 50
    const v8, 0x7f060014

    invoke-virtual {p0, v8}, Llogos/quiz/companies/game/LevelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v8, v9}, Llogos/quiz/companies/game/Adserwer;->setAd(Landroid/widget/ImageView;Landroid/content/Context;)V

    .line 52
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->myActivity:Landroid/app/Activity;

    const v9, 0x7f060013

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 53
    .local v0, adContainer:Landroid/widget/RelativeLayout;
    invoke-static {p0}, Llogos/quiz/companies/game/Adserwer;->getAdmob(Landroid/app/Activity;)Lcom/google/ads/AdView;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 56
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->myActivity:Landroid/app/Activity;

    invoke-static {v8}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedPoints(Landroid/app/Activity;)I

    move-result v3

    .line 57
    .local v3, completedPoints:I
    const v8, 0x7f060010

    invoke-virtual {p0, v8}, Llogos/quiz/companies/game/LevelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 58
    .local v4, levelHeadTextView:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v8, p0, Llogos/quiz/companies/game/LevelsActivity;->myActivity:Landroid/app/Activity;

    invoke-static {v8}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/app/Activity;)I

    move-result v1

    .line 62
    .local v1, completedLogosCount:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getBrandsCount()I

    move-result v7

    .line 63
    .local v7, totalLogosCount:I
    const v8, 0x7f060011

    invoke-virtual {p0, v8}, Llogos/quiz/companies/game/LevelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 64
    .local v2, completedLogosCounter:Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const v8, 0x7f060012

    invoke-virtual {p0, v8}, Llogos/quiz/companies/game/LevelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 67
    .local v6, levelsList:Landroid/widget/ListView;
    new-instance v5, Llogos/quiz/companies/game/LevelListAdapter;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Llogos/quiz/companies/game/LevelListAdapter;-><init>(Landroid/content/Context;[Llogos/quiz/companies/game/Level;)V

    .line 68
    .local v5, levelListAdapter:Llogos/quiz/companies/game/LevelListAdapter;
    invoke-virtual {v6, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setSelection(I)V

    .line 70
    return-void
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 118
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 124
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 91
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 93
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LogosQuizActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 95
    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/LevelsActivity;->startActivity(Landroid/content/Intent;)V

    .line 96
    const/4 v1, 0x1

    .line 98
    .end local v0           #intent:Landroid/content/Intent;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/swarmconnect/SwarmActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 130
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 136
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter "arg0"

    .prologue
    .line 140
    const v1, 0x7f060013

    invoke-virtual {p0, v1}, Llogos/quiz/companies/game/LevelsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 141
    .local v0, layout:Landroid/widget/RelativeLayout;
    iget-object v1, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llogos/quiz/companies/game/DeviceUtil;->getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Llogo/quiz/commons/FlipAnimator;->setmCenterX(F)V

    .line 142
    iget-object v1, p0, Llogos/quiz/companies/game/LevelsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 143
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onResume()V

    .line 81
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 74
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public promo(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 102
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 103
    .local v1, intent:Landroid/content/Intent;
    sget-object v3, Llogos/quiz/companies/game/Adserwer;->adUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 104
    const/high16 v3, 0x4000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0}, Llogos/quiz/companies/game/LevelsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 107
    .local v2, settings:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 108
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    sget-object v3, Llogos/quiz/companies/game/Adserwer;->adId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 109
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 111
    invoke-virtual {p0, v1}, Llogos/quiz/companies/game/LevelsActivity;->startActivity(Landroid/content/Intent;)V

    .line 112
    return-void
.end method
