.class public Llogos/quiz/companies/game/LogosFormActivity;
.super Llogo/quiz/commons/FormActivityCommons;
.source "LogosFormActivity.java"


# instance fields
.field constants:Llogo/quiz/commons/ConstantsProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Llogo/quiz/commons/FormActivityCommons;-><init>()V

    .line 15
    new-instance v0, Llogos/quiz/companies/game/Constants;

    invoke-direct {v0}, Llogos/quiz/companies/game/Constants;-><init>()V

    iput-object v0, p0, Llogos/quiz/companies/game/LogosFormActivity;->constants:Llogo/quiz/commons/ConstantsProvider;

    .line 14
    return-void
.end method


# virtual methods
.method protected checkLevelCompletedLogos(Landroid/app/Activity;III)V
    .locals 0
    .parameter "activity"
    .parameter "completedLogosCounterForm"
    .parameter "completedLogosPointsForm"
    .parameter "levelLabelForm"

    .prologue
    .line 55
    invoke-static {p1, p2, p3, p4}, Llogos/quiz/companies/game/ScoreUtil;->checkLevelCompletedLogos(Landroid/app/Activity;III)V

    .line 56
    return-void
.end method

.method protected getAvailibleHintsCount(Landroid/app/Activity;)I
    .locals 1
    .parameter "activity"

    .prologue
    .line 48
    invoke-static {p0}, Llogos/quiz/companies/game/ScoreUtil;->getAvailibleHintsCount(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getCompletedLogosCount(Landroid/app/Activity;)I
    .locals 1
    .parameter "activity"

    .prologue
    .line 60
    invoke-static {p0}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/app/Activity;)I

    move-result v0

    return v0
.end method

.method protected getConstants()Llogo/quiz/commons/ConstantsProvider;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Llogos/quiz/companies/game/LogosFormActivity;->constants:Llogo/quiz/commons/ConstantsProvider;

    return-object v0
.end method

.method protected getTapJoyPayPerActionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    const-string v0, "cee84ce5-630f-4e26-ac2f-4ec2cd660b53"

    return-object v0
.end method

.method protected isLevelUnlocked()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 19
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 22
    .local v6, settings:Landroid/content/SharedPreferences;
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v5

    .line 23
    .local v5, levelsInfo:[Llogos/quiz/companies/game/Level;
    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v9

    invoke-static {p0, v9}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v1

    .line 24
    .local v1, completeLevelLogos:I
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unlockedLevel"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Info"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 25
    .local v4, isUnlockedLevelInfo:Z
    const-string v9, "allHints"

    invoke-interface {v6, v9, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 26
    .local v0, allHintsCount:I
    if-eqz v4, :cond_2

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v9

    array-length v10, v5

    if-ge v9, v10, :cond_2

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v9

    aget-object v9, v5, v9

    invoke-virtual {v9}, Llogos/quiz/companies/game/Level;->getUnlockLimit()I

    move-result v9

    if-lt v1, v9, :cond_2

    move v3, v7

    .line 27
    .local v3, isLevelUnlocked:Z
    :goto_0
    if-eqz v3, :cond_1

    .line 28
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Well done! Level "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " unlocked. You get 2 new hints."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 29
    const-string v9, "SOUND"

    invoke-interface {v6, v9, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 30
    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosFormActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v9, 0x7f050001

    invoke-static {v7, v9}, Llogo/quiz/commons/DeviceUtilCommons;->playSound(Landroid/content/Context;I)V

    .line 33
    :cond_0
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 34
    .local v2, editor:Landroid/content/SharedPreferences$Editor;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "unlockedLevel"

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "Info"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 35
    const-string v7, "allHints"

    add-int/lit8 v8, v0, 0x2

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 36
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 38
    .end local v2           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_1
    return v3

    .end local v3           #isLevelUnlocked:Z
    :cond_2
    move v3, v8

    .line 26
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter "savedInstanceState"

    .prologue
    .line 43
    invoke-super {p0, p1}, Llogo/quiz/commons/FormActivityCommons;->onCreate(Landroid/os/Bundle;)V

    .line 44
    return-void
.end method
