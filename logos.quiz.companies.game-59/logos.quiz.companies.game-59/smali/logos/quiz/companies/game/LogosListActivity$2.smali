.class Llogos/quiz/companies/game/LogosListActivity$2;
.super Ljava/lang/Object;
.source "LogosListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Llogos/quiz/companies/game/LogosListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Llogos/quiz/companies/game/LogosListActivity;


# direct methods
.method constructor <init>(Llogos/quiz/companies/game/LogosListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Llogos/quiz/companies/game/LogosListActivity$2;->this$0:Llogos/quiz/companies/game/LogosListActivity;

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llogo/quiz/commons/BrandTO;

    .line 95
    .local v0, brandTO:Llogo/quiz/commons/BrandTO;
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity$2;->this$0:Llogos/quiz/companies/game/LogosListActivity;

    invoke-virtual {v2}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Llogos/quiz/companies/game/LogosFormActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x2400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    const-string v2, "position"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    const-string v2, "brandPosition"

    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getBrandPosition()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    const-string v2, "brandNames"

    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getNames()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v2, "brandDrawable"

    invoke-virtual {v0}, Llogo/quiz/commons/BrandTO;->getDrawable()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 101
    const-string v2, "brandTO"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 102
    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity$2;->this$0:Llogos/quiz/companies/game/LogosListActivity;

    invoke-virtual {v2, v1, p3}, Llogos/quiz/companies/game/LogosListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 103
    return-void
.end method
