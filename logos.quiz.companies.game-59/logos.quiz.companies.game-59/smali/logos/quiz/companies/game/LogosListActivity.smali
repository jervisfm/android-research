.class public Llogos/quiz/companies/game/LogosListActivity;
.super Lcom/swarmconnect/SwarmActivity;
.source "LogosListActivity.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "LogosListActivity"


# instance fields
.field adHandler:Landroid/os/Handler;

.field final flipAnimator:Llogo/quiz/commons/FlipAnimator;

.field myActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    .line 41
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    iput-object v0, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 36
    return-void
.end method

.method private completeLogo(ILandroid/widget/GridView;Z)V
    .locals 8
    .parameter "responsePosition"
    .parameter "gridview"
    .parameter "isComplete"

    .prologue
    .line 136
    invoke-virtual {p2, p1}, Landroid/widget/GridView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llogo/quiz/commons/BrandTO;

    .line 137
    .local v0, brandTO:Llogo/quiz/commons/BrandTO;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0, p3}, Llogo/quiz/commons/BrandTO;->setComplete(Z)V

    .line 140
    :cond_0
    invoke-virtual {p2}, Landroid/widget/GridView;->invalidateViews()V

    .line 141
    const v5, 0x7f06002d

    const v6, 0x7f06002c

    const v7, 0x7f06002b

    invoke-static {p0, v5, v6, v7}, Llogos/quiz/companies/game/ScoreUtil;->checkLevelCompletedLogos(Landroid/app/Activity;III)V

    .line 144
    :try_start_0
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v3

    .line 145
    .local v3, levelsInfo:[Llogos/quiz/companies/game/Level;
    iget-object v5, p0, Llogos/quiz/companies/game/LogosListActivity;->myActivity:Landroid/app/Activity;

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v6

    invoke-static {v5, v6}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v1

    .line 147
    .local v1, completeLogos:I
    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v3, v5

    invoke-virtual {v5}, Llogos/quiz/companies/game/Level;->getLogosCount()I

    move-result v2

    .line 148
    .local v2, levelLogosCount:I
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 149
    .local v4, parameters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "Level"

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const-string v5, "Complete logos"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v5, "Points"

    iget-object v6, p0, Llogos/quiz/companies/game/LogosListActivity;->myActivity:Landroid/app/Activity;

    invoke-static {}, Llogo/quiz/commons/LevelUtil;->getActiveLevel()I

    move-result v7

    invoke-static {v6, v7}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedPoints(Landroid/content/Context;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v5, "Level score"

    invoke-static {v5, v4}, Lcom/flurry/android/FlurryAgent;->onEvent(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v1           #completeLogos:I
    .end local v2           #levelLogosCount:I
    .end local v3           #levelsInfo:[Llogos/quiz/companies/game/Level;
    .end local v4           #parameters:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v5

    goto :goto_0
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LevelsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/LogosListActivity;->startActivity(Landroid/content/Intent;)V

    .line 160
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .parameter "requestPosition"
    .parameter "responsePosition"
    .parameter "data"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 116
    invoke-super {p0, p1, p2, p3}, Lcom/swarmconnect/SwarmActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 117
    if-ltz p2, :cond_1

    .line 118
    const v3, 0x7f06002e

    invoke-virtual {p0, v3}, Llogos/quiz/companies/game/LogosListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 119
    .local v1, gridview:Landroid/widget/GridView;
    if-nez p2, :cond_3

    if-eqz p3, :cond_0

    const-string v3, "isCorrect"

    invoke-virtual {p3, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 120
    :cond_0
    if-eqz p3, :cond_2

    .line 121
    const-string v3, "isCorrect"

    invoke-virtual {p3, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, p2, v1, v3}, Llogos/quiz/companies/game/LogosListActivity;->completeLogo(ILandroid/widget/GridView;Z)V

    .line 133
    .end local v1           #gridview:Landroid/widget/GridView;
    :cond_1
    :goto_0
    return-void

    .line 123
    .restart local v1       #gridview:Landroid/widget/GridView;
    :cond_2
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 124
    .local v2, settings:Landroid/content/SharedPreferences;
    const-string v3, "complete_position"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    .local v0, completePos:I
    if-eq v0, v4, :cond_1

    .line 126
    invoke-direct {p0, v0, v1, v6}, Llogos/quiz/companies/game/LogosListActivity;->completeLogo(ILandroid/widget/GridView;Z)V

    goto :goto_0

    .line 130
    .end local v0           #completePos:I
    .end local v2           #settings:Landroid/content/SharedPreferences;
    :cond_3
    invoke-direct {p0, p2, v1, v6}, Llogos/quiz/companies/game/LogosListActivity;->completeLogo(ILandroid/widget/GridView;Z)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/16 v3, 0x400

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-virtual {p0, v2, v2}, Llogos/quiz/companies/game/LogosListActivity;->overridePendingTransition(II)V

    .line 46
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const-string v2, "GHNSFPPH779R3ZQTGTFJ"

    invoke-static {p0, v2}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0, v5}, Llogos/quiz/companies/game/LogosListActivity;->requestWindowFeature(I)Z

    .line 49
    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 50
    const v2, 0x7f030007

    invoke-virtual {p0, v2}, Llogos/quiz/companies/game/LogosListActivity;->setContentView(I)V

    .line 54
    iput-object p0, p0, Llogos/quiz/companies/game/LogosListActivity;->myActivity:Landroid/app/Activity;

    .line 56
    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v2, v3, v4}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 57
    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v2, v5}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 58
    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 60
    const v2, 0x7f060030

    invoke-virtual {p0, v2}, Llogos/quiz/companies/game/LogosListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Llogos/quiz/companies/game/Adserwer;->setAd(Landroid/widget/ImageView;Landroid/content/Context;)V

    .line 62
    iget-object v2, p0, Llogos/quiz/companies/game/LogosListActivity;->myActivity:Landroid/app/Activity;

    const v3, 0x7f06002f

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 63
    .local v0, adContainer:Landroid/widget/RelativeLayout;
    invoke-static {p0}, Llogos/quiz/companies/game/Adserwer;->getAdmob(Landroid/app/Activity;)Lcom/google/ads/AdView;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 65
    new-instance v2, Llogos/quiz/companies/game/LogosListActivity$1;

    invoke-direct {v2, p0}, Llogos/quiz/companies/game/LogosListActivity$1;-><init>(Llogos/quiz/companies/game/LogosListActivity;)V

    invoke-static {v2}, Lcom/swarmconnect/Swarm;->addNotificationDelegate(Lcom/swarmconnect/delegates/SwarmNotificationDelegate;)V

    .line 89
    const v2, 0x7f06002e

    invoke-virtual {p0, v2}, Llogos/quiz/companies/game/LogosListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 90
    .local v1, gridview:Landroid/widget/GridView;
    new-instance v2, Llogos/quiz/companies/game/ImageAdapter;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Llogos/quiz/companies/game/ImageAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    new-instance v2, Llogos/quiz/companies/game/LogosListActivity$2;

    invoke-direct {v2, p0}, Llogos/quiz/companies/game/LogosListActivity$2;-><init>(Llogos/quiz/companies/game/LogosListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 106
    const v2, 0x7f06002d

    const v3, 0x7f06002c

    const v4, 0x7f06002b

    invoke-static {p0, v2, v3, v4}, Llogos/quiz/companies/game/ScoreUtil;->checkLevelCompletedLogos(Landroid/app/Activity;III)V

    .line 107
    return-void
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 189
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 195
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 164
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 166
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LevelsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/LogosListActivity;->startActivity(Landroid/content/Intent;)V

    .line 167
    const/4 v0, 0x1

    .line 169
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/swarmconnect/SwarmActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 201
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 207
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter "arg0"

    .prologue
    .line 211
    const v1, 0x7f06002f

    invoke-virtual {p0, v1}, Llogos/quiz/companies/game/LogosListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 212
    .local v0, layout:Landroid/widget/RelativeLayout;
    iget-object v1, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llogos/quiz/companies/game/DeviceUtil;->getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Llogo/quiz/commons/FlipAnimator;->setmCenterX(F)V

    .line 213
    iget-object v1, p0, Llogos/quiz/companies/game/LogosListActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 214
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 111
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 112
    return-void
.end method

.method public promo(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 173
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 174
    .local v1, intent:Landroid/content/Intent;
    sget-object v3, Llogos/quiz/companies/game/Adserwer;->adUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 175
    const/high16 v3, 0x4000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 177
    invoke-virtual {p0}, Llogos/quiz/companies/game/LogosListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 178
    .local v2, settings:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 179
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    sget-object v3, Llogos/quiz/companies/game/Adserwer;->adId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 182
    invoke-virtual {p0, v1}, Llogos/quiz/companies/game/LogosListActivity;->startActivity(Landroid/content/Intent;)V

    .line 183
    return-void
.end method
