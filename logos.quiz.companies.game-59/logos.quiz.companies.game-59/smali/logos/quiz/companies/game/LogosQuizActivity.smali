.class public Llogos/quiz/companies/game/LogosQuizActivity;
.super Llogo/quiz/commons/LogoQuizActivityCommons;
.source "LogosQuizActivity.java"


# instance fields
.field constants:Llogo/quiz/commons/ConstantsProvider;

.field scoreUtilProvider:Llogo/quiz/commons/ScoreUtilProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Llogo/quiz/commons/LogoQuizActivityCommons;-><init>()V

    .line 8
    new-instance v0, Llogos/quiz/companies/game/Constants;

    invoke-direct {v0}, Llogos/quiz/companies/game/Constants;-><init>()V

    iput-object v0, p0, Llogos/quiz/companies/game/LogosQuizActivity;->constants:Llogo/quiz/commons/ConstantsProvider;

    .line 9
    new-instance v0, Llogos/quiz/companies/game/ScoreUtilProviderImpl;

    invoke-direct {v0}, Llogos/quiz/companies/game/ScoreUtilProviderImpl;-><init>()V

    iput-object v0, p0, Llogos/quiz/companies/game/LogosQuizActivity;->scoreUtilProvider:Llogo/quiz/commons/ScoreUtilProvider;

    .line 7
    return-void
.end method


# virtual methods
.method protected getCompletedPoints()I
    .locals 1

    .prologue
    .line 16
    invoke-static {p0}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedPoints(Landroid/app/Activity;)I

    move-result v0

    return v0
.end method

.method protected getConstants()Llogo/quiz/commons/ConstantsProvider;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Llogos/quiz/companies/game/LogosQuizActivity;->constants:Llogo/quiz/commons/ConstantsProvider;

    return-object v0
.end method

.method protected getScoreUtilProvider()Llogo/quiz/commons/ScoreUtilProvider;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Llogos/quiz/companies/game/LogosQuizActivity;->scoreUtilProvider:Llogo/quiz/commons/ScoreUtilProvider;

    return-object v0
.end method
