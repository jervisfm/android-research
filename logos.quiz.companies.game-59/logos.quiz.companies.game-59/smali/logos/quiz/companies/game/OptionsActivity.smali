.class public Llogos/quiz/companies/game/OptionsActivity;
.super Lcom/swarmconnect/SwarmActivity;
.source "OptionsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/swarmconnect/SwarmActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/OptionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LogosQuizActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/OptionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 55
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedInstanceState"

    .prologue
    const/16 v5, 0x400

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 24
    invoke-virtual {p0, v3, v3}, Llogos/quiz/companies/game/OptionsActivity;->overridePendingTransition(II)V

    .line 25
    invoke-super {p0, p1}, Lcom/swarmconnect/SwarmActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const-string v3, "GHNSFPPH779R3ZQTGTFJ"

    invoke-static {p0, v3}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, v4}, Llogos/quiz/companies/game/OptionsActivity;->requestWindowFeature(I)Z

    .line 28
    invoke-virtual {p0}, Llogos/quiz/companies/game/OptionsActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 29
    const v3, 0x7f03000a

    invoke-virtual {p0, v3}, Llogos/quiz/companies/game/OptionsActivity;->setContentView(I)V

    .line 31
    const v3, 0x7f060037

    invoke-virtual {p0, v3}, Llogos/quiz/companies/game/OptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 32
    .local v1, soundMute:Landroid/widget/Button;
    const v3, 0x7f060038

    invoke-virtual {p0, v3}, Llogos/quiz/companies/game/OptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 33
    .local v2, vibrate:Landroid/widget/Button;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 34
    .local v0, settings:Landroid/content/SharedPreferences;
    const-string v3, "SOUND"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    const-string v3, "Sounds on"

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :goto_0
    const-string v3, "VIBRATE"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 40
    const-string v3, "Vibrate on"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :goto_1
    return-void

    .line 37
    :cond_0
    const-string v3, "Sounds off"

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 42
    :cond_1
    const-string v3, "Vibrate off"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/swarmconnect/SwarmActivity;->onStop()V

    .line 50
    invoke-static {p0}, Lcom/flurry/android/FlurryAgent;->onEndSession(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public otherApps(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "market://search?q=pub:bubble quiz games"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 101
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/OptionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 103
    return-void
.end method

.method public rate(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 58
    const-string v1, "Rate app"

    invoke-static {v1}, Lcom/flurry/android/FlurryAgent;->onEvent(Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 60
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "market://details?id=logos.quiz.companies.game"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 61
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 62
    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/OptionsActivity;->startActivity(Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method public reset(Landroid/view/View;)V
    .locals 4
    .parameter "v"

    .prologue
    .line 106
    new-instance v1, Llogos/quiz/companies/game/OptionsActivity$1;

    invoke-direct {v1, p0}, Llogos/quiz/companies/game/OptionsActivity$1;-><init>(Llogos/quiz/companies/game/OptionsActivity;)V

    .line 120
    .local v1, dialogClickListener:Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 121
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v2, "Attention! Are you sure you want to reset all settings this app? After click \'reset\' you will lose all your points. Continue?"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Reset"

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 122
    const-string v3, "Cancel"

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 123
    return-void
.end method

.method public soundMute(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    .line 66
    const-string v3, "Sound mute"

    invoke-static {v3}, Lcom/flurry/android/FlurryAgent;->onEvent(Ljava/lang/String;)V

    .line 67
    const v3, 0x7f060037

    invoke-virtual {p0, v3}, Llogos/quiz/companies/game/OptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 68
    .local v2, soundMute:Landroid/widget/Button;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 69
    .local v1, settings:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 70
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "Sounds on"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    const-string v3, "Sounds off"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const-string v3, "SOUND"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 78
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 79
    return-void

    .line 74
    :cond_0
    const-string v3, "Sounds on"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const-string v3, "SOUND"

    const/4 v4, 0x1

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 76
    invoke-virtual {p0}, Llogos/quiz/companies/game/OptionsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f05

    invoke-static {v3, v4}, Llogos/quiz/companies/game/DeviceUtil;->playSound(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public vibrate(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 82
    const v4, 0x7f060038

    invoke-virtual {p0, v4}, Llogos/quiz/companies/game/OptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 83
    .local v2, soundMute:Landroid/widget/Button;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 84
    .local v1, settings:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 85
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "Vibrate on"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    const-string v4, "Vibrate off"

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 87
    const-string v4, "VIBRATE"

    const/4 v5, 0x0

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 94
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 95
    return-void

    .line 89
    :cond_0
    const-string v4, "Vibrate on"

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 90
    const-string v4, "VIBRATE"

    const/4 v5, 0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 91
    const-string v4, "vibrator"

    invoke-virtual {p0, v4}, Llogos/quiz/companies/game/OptionsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    .line 92
    .local v3, vv:Landroid/os/Vibrator;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0
.end method
