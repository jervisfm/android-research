.class public final Llogos/quiz/companies/game/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Llogos/quiz/companies/game/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final MagicTextView:[I = null

.field public static final MagicTextView_background:I = 0xa

.field public static final MagicTextView_foreground:I = 0x9

.field public static final MagicTextView_innerShadowColor:I = 0x0

.field public static final MagicTextView_innerShadowDx:I = 0x2

.field public static final MagicTextView_innerShadowDy:I = 0x3

.field public static final MagicTextView_innerShadowRadius:I = 0x1

.field public static final MagicTextView_outerShadowColor:I = 0x4

.field public static final MagicTextView_outerShadowDx:I = 0x6

.field public static final MagicTextView_outerShadowDy:I = 0x7

.field public static final MagicTextView_outerShadowRadius:I = 0x5

.field public static final MagicTextView_strokeColor:I = 0xd

.field public static final MagicTextView_strokeJoinStyle:I = 0xe

.field public static final MagicTextView_strokeMiter:I = 0xc

.field public static final MagicTextView_strokeWidth:I = 0xb

.field public static final MagicTextView_typeface:I = 0x8


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1566
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Llogos/quiz/companies/game/R$styleable;->MagicTextView:[I

    .line 1527
    return-void

    .line 1566
    :array_0
    .array-data 0x4
        0x0t 0x0t 0x1t 0x7ft
        0x1t 0x0t 0x1t 0x7ft
        0x2t 0x0t 0x1t 0x7ft
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
        0x6t 0x0t 0x1t 0x7ft
        0x7t 0x0t 0x1t 0x7ft
        0x8t 0x0t 0x1t 0x7ft
        0x9t 0x0t 0x1t 0x7ft
        0xat 0x0t 0x1t 0x7ft
        0xbt 0x0t 0x1t 0x7ft
        0xct 0x0t 0x1t 0x7ft
        0xdt 0x0t 0x1t 0x7ft
        0xet 0x0t 0x1t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
