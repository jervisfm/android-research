.class public Llogos/quiz/companies/game/ScoreUtilProviderImpl;
.super Ljava/lang/Object;
.source "ScoreUtilProviderImpl.java"

# interfaces
.implements Llogo/quiz/commons/ScoreUtilProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNewLogosCount()I
    .locals 1

    .prologue
    .line 15
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getNewLogosCount()I

    move-result v0

    return v0
.end method

.method public initLogos(Landroid/app/Activity;)V
    .locals 0
    .parameter "activity"

    .prologue
    .line 10
    invoke-static {p1}, Llogos/quiz/companies/game/ScoreUtil;->initLogos(Landroid/content/Context;)V

    .line 11
    return-void
.end method

.method public setNewLogosCount(I)V
    .locals 0
    .parameter "logosCount"

    .prologue
    .line 20
    invoke-static {p1}, Llogos/quiz/companies/game/ScoreUtil;->setNewLogosCount(I)V

    .line 21
    return-void
.end method
