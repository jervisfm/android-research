.class public Llogos/quiz/companies/game/StatisticsActivity;
.super Llogo/quiz/commons/StatisticsActivityCommons;
.source "StatisticsActivity.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# instance fields
.field final flipAnimator:Llogo/quiz/commons/FlipAnimator;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Llogo/quiz/commons/StatisticsActivityCommons;-><init>()V

    .line 28
    new-instance v0, Llogo/quiz/commons/FlipAnimator;

    const/high16 v1, 0x42b4

    invoke-direct {v0, v1, v2, v2, v2}, Llogo/quiz/commons/FlipAnimator;-><init>(FFFF)V

    iput-object v0, p0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    .line 27
    return-void
.end method

.method private countAllScore()I
    .locals 6

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, allScore:I
    invoke-static {p0}, Llogos/quiz/companies/game/ScoreUtil;->getBrands(Landroid/content/Context;)[Llogo/quiz/commons/BrandTO;

    move-result-object v2

    .line 155
    .local v2, brands:[Llogo/quiz/commons/BrandTO;
    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 158
    return v0

    .line 155
    :cond_0
    aget-object v1, v2, v3

    .line 156
    .local v1, brand:Llogo/quiz/commons/BrandTO;
    invoke-virtual {v1}, Llogo/quiz/commons/BrandTO;->getLevel()I

    move-result v5

    add-int/2addr v0, v5

    .line 155
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private countLevelsCompleted()I
    .locals 8

    .prologue
    .line 125
    const/4 v2, 0x0

    .line 126
    .local v2, levelsComplete:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_0

    .line 133
    return v2

    .line 126
    :cond_0
    aget-object v1, v5, v4

    .line 127
    .local v1, level:Llogos/quiz/companies/game/Level;
    invoke-virtual {v1}, Llogos/quiz/companies/game/Level;->getLogosCount()I

    move-result v3

    .line 128
    .local v3, logosInLevel:I
    invoke-virtual {v1}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v7

    invoke-static {p0, v7}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v0

    .line 129
    .local v0, completeLogosInLevel:I
    if-ne v3, v0, :cond_1

    .line 130
    add-int/lit8 v2, v2, 0x1

    .line 126
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private countLevelsUnlocked(I)I
    .locals 9
    .parameter "score"

    .prologue
    .line 137
    const/4 v3, 0x0

    .line 138
    .local v3, levelsUnclocked:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v5

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_0

    .line 149
    return v3

    .line 138
    :cond_0
    aget-object v1, v5, v4

    .line 139
    .local v1, level:Llogos/quiz/companies/game/Level;
    invoke-virtual {v1}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 140
    add-int/lit8 v3, v3, 0x1

    .line 138
    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 142
    :cond_2
    invoke-virtual {v1}, Llogos/quiz/companies/game/Level;->getUnlockLimit()I

    move-result v2

    .line 143
    .local v2, levelUnlockLimit:I
    invoke-virtual {v1}, Llogos/quiz/companies/game/Level;->getId()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p0, v7}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/content/Context;I)I

    move-result v0

    .line 144
    .local v0, completedBeforeLevelLogosCount:I
    if-lt v0, v2, :cond_1

    .line 145
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private countUsedHints()I
    .locals 9

    .prologue
    .line 111
    const/4 v4, 0x0

    .line 112
    .local v4, usedHints:I
    invoke-static {p0}, Llogos/quiz/companies/game/ScoreUtil;->getBrands(Landroid/content/Context;)[Llogo/quiz/commons/BrandTO;

    move-result-object v2

    .line 113
    .local v2, brands:[Llogo/quiz/commons/BrandTO;
    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_0

    .line 121
    return v4

    .line 113
    :cond_0
    aget-object v1, v2, v5

    .line 114
    .local v1, brand:Llogo/quiz/commons/BrandTO;
    invoke-virtual {p0}, Llogos/quiz/companies/game/StatisticsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v1, v7, v8}, Llogo/quiz/commons/HintsUtil;->getAllHintsForBrand(Llogo/quiz/commons/BrandTO;Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v0

    .line 115
    .local v0, allHintsForBrand:Ljava/util/List;,"Ljava/util/List<Llogo/quiz/commons/Hint;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 113
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 115
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llogo/quiz/commons/Hint;

    .line 116
    .local v3, hint:Llogo/quiz/commons/Hint;
    invoke-virtual {v3}, Llogo/quiz/commons/Hint;->isUsed()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 117
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public back(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Llogos/quiz/companies/game/StatisticsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Llogos/quiz/companies/game/LogosQuizActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Llogos/quiz/companies/game/StatisticsActivity;->startActivity(Landroid/content/Intent;)V

    .line 163
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 47
    .parameter "savedInstanceState"

    .prologue
    .line 32
    const/16 v44, 0x0

    const/16 v45, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Llogos/quiz/companies/game/StatisticsActivity;->overridePendingTransition(II)V

    .line 33
    invoke-super/range {p0 .. p1}, Llogo/quiz/commons/StatisticsActivityCommons;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const-string v44, "GHNSFPPH779R3ZQTGTFJ"

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Lcom/flurry/android/FlurryAgent;->onStartSession(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    const/16 v44, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->requestWindowFeature(I)Z

    .line 36
    invoke-virtual/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->getWindow()Landroid/view/Window;

    move-result-object v44

    const/16 v45, 0x400

    const/16 v46, 0x400

    invoke-virtual/range {v44 .. v46}, Landroid/view/Window;->setFlags(II)V

    .line 37
    const v44, 0x7f03000c

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->setContentView(I)V

    .line 39
    move-object/from16 v0, p0

    iget-object v0, v0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    move-object/from16 v44, v0

    const-wide/16 v45, 0x1f4

    invoke-virtual/range {v44 .. v46}, Llogo/quiz/commons/FlipAnimator;->setDuration(J)V

    .line 40
    move-object/from16 v0, p0

    iget-object v0, v0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    move-object/from16 v44, v0

    const/16 v45, 0x1

    invoke-virtual/range {v44 .. v45}, Llogo/quiz/commons/FlipAnimator;->setFillAfter(Z)V

    .line 41
    move-object/from16 v0, p0

    iget-object v0, v0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    move-object/from16 v44, v0

    new-instance v45, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct/range {v45 .. v45}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual/range {v44 .. v45}, Llogo/quiz/commons/FlipAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 43
    const v44, 0x7f060055

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v44

    check-cast v44, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Llogos/quiz/companies/game/Adserwer;->setAd(Landroid/widget/ImageView;Landroid/content/Context;)V

    .line 44
    const v44, 0x7f060054

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 45
    .local v3, adContainer:Landroid/widget/RelativeLayout;
    invoke-static/range {p0 .. p0}, Llogos/quiz/companies/game/Adserwer;->getAdmob(Landroid/app/Activity;)Lcom/google/ads/AdView;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 47
    invoke-virtual/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v22

    .line 49
    .local v22, settings:Landroid/content/SharedPreferences;
    const v44, 0x7f060042

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 50
    .local v27, statsGuessedLogos:Landroid/widget/TextView;
    const v44, 0x7f060043

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 51
    .local v28, statsGuessedLogosPercent:Landroid/widget/TextView;
    const v44, 0x7f060048

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Landroid/widget/TextView;

    .line 52
    .local v37, statsScore:Landroid/widget/TextView;
    const v44, 0x7f060049

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v38

    check-cast v38, Landroid/widget/TextView;

    .line 53
    .local v38, statsScorePercent:Landroid/widget/TextView;
    const v44, 0x7f06004a

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v33

    check-cast v33, Landroid/widget/TextView;

    .line 54
    .local v33, statsLevelsUnlocked:Landroid/widget/TextView;
    const v44, 0x7f06004b

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/TextView;

    .line 55
    .local v34, statsLevelsUnlockedPercent:Landroid/widget/TextView;
    const v44, 0x7f06004c

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    .line 56
    .local v31, statsLevelsCompleted:Landroid/widget/TextView;
    const v44, 0x7f06004d

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Landroid/widget/TextView;

    .line 57
    .local v32, statsLevelsCompletedPercent:Landroid/widget/TextView;
    const v44, 0x7f06004e

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 58
    .local v23, statsAllHints:Landroid/widget/TextView;
    const v44, 0x7f06004f

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    .line 59
    .local v24, statsAllHintsPercent:Landroid/widget/TextView;
    const v44, 0x7f060050

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/TextView;

    .line 60
    .local v29, statsHints:Landroid/widget/TextView;
    const v44, 0x7f060051

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v30

    check-cast v30, Landroid/widget/TextView;

    .line 61
    .local v30, statsHintsPercent:Landroid/widget/TextView;
    const v44, 0x7f060052

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v39

    check-cast v39, Landroid/widget/TextView;

    .line 62
    .local v39, statsUsedHints:Landroid/widget/TextView;
    const v44, 0x7f060053

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v40

    check-cast v40, Landroid/widget/TextView;

    .line 63
    .local v40, statsUsedHintsPercent:Landroid/widget/TextView;
    const v44, 0x7f060046

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/TextView;

    .line 64
    .local v25, statsGuessTries:Landroid/widget/TextView;
    const v44, 0x7f060047

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 65
    .local v26, statsGuessTriesPercent:Landroid/widget/TextView;
    const v44, 0x7f060044

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Landroid/widget/TextView;

    .line 66
    .local v35, statsPerfectGuessCount:Landroid/widget/TextView;
    const v44, 0x7f060045

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v36

    check-cast v36, Landroid/widget/TextView;

    .line 68
    .local v36, statsPerfectGuessCountPercent:Landroid/widget/TextView;
    invoke-static/range {p0 .. p0}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedLogosCount(Landroid/app/Activity;)I

    move-result v10

    .line 69
    .local v10, guessedLogos:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getBrandsCount()I

    move-result v5

    .line 70
    .local v5, allLogos:I
    int-to-float v0, v10

    move/from16 v44, v0

    int-to-float v0, v5

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v11, v0

    .line 71
    .local v11, guessedLogosPercent:I
    invoke-static/range {p0 .. p0}, Llogos/quiz/companies/game/ScoreUtil;->getCompletedPoints(Landroid/app/Activity;)I

    move-result v20

    .line 72
    .local v20, score:I
    invoke-direct/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->countAllScore()I

    move-result v6

    .line 73
    .local v6, allScore:I
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v44, v0

    int-to-float v0, v6

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v0, v0

    move/from16 v21, v0

    .line 74
    .local v21, scorePercent:I
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->countLevelsUnlocked(I)I

    move-result v16

    .line 75
    .local v16, levelsUnclocked:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getLevelsInfo()[Llogos/quiz/companies/game/Level;

    move-result-object v44

    move-object/from16 v0, v44

    array-length v15, v0

    .line 76
    .local v15, levelsCount:I
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v44, v0

    int-to-float v0, v15

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v0, v0

    move/from16 v17, v0

    .line 77
    .local v17, levelsUnclockedPercent:I
    invoke-direct/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->countLevelsCompleted()I

    move-result v13

    .line 78
    .local v13, levelsComplete:I
    int-to-float v0, v13

    move/from16 v44, v0

    int-to-float v0, v15

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v14, v0

    .line 79
    .local v14, levelsCompletePercent:I
    invoke-static/range {p0 .. p0}, Llogos/quiz/companies/game/ScoreUtil;->getAvailibleHintsCount(Landroid/content/Context;)I

    move-result v42

    .line 80
    .local v42, userHintsCount:I
    invoke-static {}, Llogos/quiz/companies/game/ScoreUtil;->getBrandsCount()I

    move-result v44

    mul-int/lit8 v44, v44, 0x4

    add-int/lit8 v4, v44, -0x1d

    .line 81
    .local v4, allHintsCount:I
    invoke-direct/range {p0 .. p0}, Llogos/quiz/companies/game/StatisticsActivity;->countUsedHints()I

    move-result v43

    .line 82
    .local v43, userUsedHints:I
    add-int v7, v43, v42

    .line 83
    .local v7, allUserHints:I
    move/from16 v0, v43

    int-to-float v0, v0

    move/from16 v44, v0

    int-to-float v0, v7

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v0, v0

    move/from16 v41, v0

    .line 84
    .local v41, usedHintsPercent:I
    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v44, v0

    int-to-float v0, v7

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v12, v0

    .line 85
    .local v12, hintsPercent:I
    int-to-float v0, v7

    move/from16 v44, v0

    int-to-float v0, v4

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v8, v0

    .line 86
    .local v8, allUserHintsPercent:I
    const-string v44, "perfectGuess"

    const/16 v45, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move/from16 v2, v45

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v18

    .line 87
    .local v18, perfectGuess:I
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v44, v0

    int-to-float v0, v10

    move/from16 v45, v0

    div-float v44, v44, v45

    const/high16 v45, 0x42c8

    mul-float v44, v44, v45

    move/from16 v0, v44

    float-to-double v0, v0

    move-wide/from16 v44, v0

    invoke-static/range {v44 .. v45}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    move-wide/from16 v0, v44

    double-to-int v0, v0

    move/from16 v19, v0

    .line 88
    .local v19, perfectGuessPercent:I
    const-string v44, "guessTries"

    const/16 v45, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move/from16 v2, v45

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 90
    .local v9, guessTries:I
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v27

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v28

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v37

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v38

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v33

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v34

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v31

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v32

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v23

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v24

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v42 .. v42}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v30

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v43 .. v43}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v41 .. v41}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v40

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v25

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    const-string v44, ""

    move-object/from16 v0, v26

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "/"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v35

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v45

    invoke-direct/range {v44 .. v45}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, "%"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v36

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 169
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 175
    return-void
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 181
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 0
    .parameter "arg0"

    .prologue
    .line 187
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter "arg0"

    .prologue
    .line 191
    const v1, 0x7f060054

    invoke-virtual {p0, v1}, Llogos/quiz/companies/game/StatisticsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 192
    .local v0, layout:Landroid/widget/RelativeLayout;
    iget-object v1, p0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {p0}, Llogos/quiz/companies/game/StatisticsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llogos/quiz/companies/game/DeviceUtil;->getDeviceSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Llogo/quiz/commons/FlipAnimator;->setmCenterX(F)V

    .line 193
    iget-object v1, p0, Llogos/quiz/companies/game/StatisticsActivity;->flipAnimator:Llogo/quiz/commons/FlipAnimator;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 194
    return-void
.end method
