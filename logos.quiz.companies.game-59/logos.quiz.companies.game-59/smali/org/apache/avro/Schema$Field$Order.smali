.class public final enum Lorg/apache/avro/Schema$Field$Order;
.super Ljava/lang/Enum;
.source "Schema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/Schema$Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Order"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/avro/Schema$Field$Order;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/avro/Schema$Field$Order;

.field public static final enum ASCENDING:Lorg/apache/avro/Schema$Field$Order;

.field public static final enum DESCENDING:Lorg/apache/avro/Schema$Field$Order;

.field public static final enum IGNORE:Lorg/apache/avro/Schema$Field$Order;


# instance fields
.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 391
    new-instance v0, Lorg/apache/avro/Schema$Field$Order;

    const-string v1, "ASCENDING"

    invoke-direct {v0, v1, v2}, Lorg/apache/avro/Schema$Field$Order;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/avro/Schema$Field$Order;->ASCENDING:Lorg/apache/avro/Schema$Field$Order;

    new-instance v0, Lorg/apache/avro/Schema$Field$Order;

    const-string v1, "DESCENDING"

    invoke-direct {v0, v1, v3}, Lorg/apache/avro/Schema$Field$Order;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/avro/Schema$Field$Order;->DESCENDING:Lorg/apache/avro/Schema$Field$Order;

    new-instance v0, Lorg/apache/avro/Schema$Field$Order;

    const-string v1, "IGNORE"

    invoke-direct {v0, v1, v4}, Lorg/apache/avro/Schema$Field$Order;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/avro/Schema$Field$Order;->IGNORE:Lorg/apache/avro/Schema$Field$Order;

    .line 390
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/avro/Schema$Field$Order;

    sget-object v1, Lorg/apache/avro/Schema$Field$Order;->ASCENDING:Lorg/apache/avro/Schema$Field$Order;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/avro/Schema$Field$Order;->DESCENDING:Lorg/apache/avro/Schema$Field$Order;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/avro/Schema$Field$Order;->IGNORE:Lorg/apache/avro/Schema$Field$Order;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/avro/Schema$Field$Order;->$VALUES:[Lorg/apache/avro/Schema$Field$Order;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 393
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/avro/Schema$Field$Order;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/avro/Schema$Field$Order;->name:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1200(Lorg/apache/avro/Schema$Field$Order;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 390
    iget-object v0, p0, Lorg/apache/avro/Schema$Field$Order;->name:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/avro/Schema$Field$Order;
    .locals 1
    .parameter "name"

    .prologue
    .line 390
    const-class v0, Lorg/apache/avro/Schema$Field$Order;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/avro/Schema$Field$Order;

    return-object v0
.end method

.method public static values()[Lorg/apache/avro/Schema$Field$Order;
    .locals 1

    .prologue
    .line 390
    sget-object v0, Lorg/apache/avro/Schema$Field$Order;->$VALUES:[Lorg/apache/avro/Schema$Field$Order;

    invoke-virtual {v0}, [Lorg/apache/avro/Schema$Field$Order;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/avro/Schema$Field$Order;

    return-object v0
.end method
