.class public Lorg/apache/avro/data/Json$Reader;
.super Ljava/lang/Object;
.source "Json.java"

# interfaces
.implements Lorg/apache/avro/io/DatumReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/data/Json;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Reader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/avro/io/DatumReader",
        "<",
        "Lorg/codehaus/jackson/JsonNode;",
        ">;"
    }
.end annotation


# instance fields
.field private resolver:Lorg/apache/avro/io/ResolvingDecoder;

.field private written:Lorg/apache/avro/Schema;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic read(Ljava/lang/Object;Lorg/apache/avro/io/Decoder;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    check-cast p1, Lorg/codehaus/jackson/JsonNode;

    .end local p1
    invoke-virtual {p0, p1, p2}, Lorg/apache/avro/data/Json$Reader;->read(Lorg/codehaus/jackson/JsonNode;Lorg/apache/avro/io/Decoder;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    return-object v0
.end method

.method public read(Lorg/codehaus/jackson/JsonNode;Lorg/apache/avro/io/Decoder;)Lorg/codehaus/jackson/JsonNode;
    .locals 5
    .parameter "reuse"
    .parameter "in"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->written:Lorg/apache/avro/Schema;

    if-nez v1, :cond_0

    .line 83
    invoke-static {p2}, Lorg/apache/avro/data/Json;->read(Lorg/apache/avro/io/Decoder;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 86
    :cond_0
    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->resolver:Lorg/apache/avro/io/ResolvingDecoder;

    if-nez v1, :cond_1

    .line 87
    invoke-static {}, Lorg/apache/avro/io/DecoderFactory;->get()Lorg/apache/avro/io/DecoderFactory;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/avro/data/Json$Reader;->written:Lorg/apache/avro/Schema;

    sget-object v3, Lorg/apache/avro/data/Json;->SCHEMA:Lorg/apache/avro/Schema;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/avro/io/DecoderFactory;->resolvingDecoder(Lorg/apache/avro/Schema;Lorg/apache/avro/Schema;Lorg/apache/avro/io/Decoder;)Lorg/apache/avro/io/ResolvingDecoder;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/avro/data/Json$Reader;->resolver:Lorg/apache/avro/io/ResolvingDecoder;

    .line 88
    :cond_1
    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->resolver:Lorg/apache/avro/io/ResolvingDecoder;

    invoke-virtual {v1, p2}, Lorg/apache/avro/io/ResolvingDecoder;->configure(Lorg/apache/avro/io/Decoder;)Lorg/apache/avro/io/ValidatingDecoder;

    .line 89
    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->resolver:Lorg/apache/avro/io/ResolvingDecoder;

    invoke-static {v1}, Lorg/apache/avro/data/Json;->read(Lorg/apache/avro/io/Decoder;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    .line 90
    .local v0, result:Lorg/codehaus/jackson/JsonNode;
    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->resolver:Lorg/apache/avro/io/ResolvingDecoder;

    invoke-virtual {v1}, Lorg/apache/avro/io/ResolvingDecoder;->drain()V

    goto :goto_0
.end method

.method public setSchema(Lorg/apache/avro/Schema;)V
    .locals 2
    .parameter "schema"

    .prologue
    .line 77
    sget-object v0, Lorg/apache/avro/data/Json;->SCHEMA:Lorg/apache/avro/Schema;

    iget-object v1, p0, Lorg/apache/avro/data/Json$Reader;->written:Lorg/apache/avro/Schema;

    invoke-virtual {v0, v1}, Lorg/apache/avro/Schema;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .end local p1
    :cond_0
    iput-object p1, p0, Lorg/apache/avro/data/Json$Reader;->written:Lorg/apache/avro/Schema;

    .line 78
    return-void
.end method
