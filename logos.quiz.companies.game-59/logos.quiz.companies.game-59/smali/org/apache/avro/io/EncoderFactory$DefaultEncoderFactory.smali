.class Lorg/apache/avro/io/EncoderFactory$DefaultEncoderFactory;
.super Lorg/apache/avro/io/EncoderFactory;
.source "EncoderFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/io/EncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultEncoderFactory"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Lorg/apache/avro/io/EncoderFactory;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/avro/io/EncoderFactory$1;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 326
    invoke-direct {p0}, Lorg/apache/avro/io/EncoderFactory$DefaultEncoderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public configureBlockSize(I)Lorg/apache/avro/io/EncoderFactory;
    .locals 2
    .parameter "size"

    .prologue
    .line 329
    new-instance v0, Lorg/apache/avro/AvroRuntimeException;

    const-string v1, "Default EncoderFactory cannot be configured"

    invoke-direct {v0, v1}, Lorg/apache/avro/AvroRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public configureBufferSize(I)Lorg/apache/avro/io/EncoderFactory;
    .locals 2
    .parameter "size"

    .prologue
    .line 333
    new-instance v0, Lorg/apache/avro/AvroRuntimeException;

    const-string v1, "Default EncoderFactory cannot be configured"

    invoke-direct {v0, v1}, Lorg/apache/avro/AvroRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
