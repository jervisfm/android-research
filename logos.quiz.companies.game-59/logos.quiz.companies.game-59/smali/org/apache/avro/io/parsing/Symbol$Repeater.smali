.class public Lorg/apache/avro/io/parsing/Symbol$Repeater;
.super Lorg/apache/avro/io/parsing/Symbol;
.source "Symbol.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/io/parsing/Symbol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Repeater"
.end annotation


# instance fields
.field public final end:Lorg/apache/avro/io/parsing/Symbol;


# direct methods
.method private varargs constructor <init>(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;)V
    .locals 2
    .parameter "end"
    .parameter "sequenceToRepeat"

    .prologue
    .line 332
    sget-object v0, Lorg/apache/avro/io/parsing/Symbol$Kind;->REPEATER:Lorg/apache/avro/io/parsing/Symbol$Kind;

    invoke-static {p2}, Lorg/apache/avro/io/parsing/Symbol$Repeater;->makeProduction([Lorg/apache/avro/io/parsing/Symbol;)[Lorg/apache/avro/io/parsing/Symbol;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/avro/io/parsing/Symbol;-><init>(Lorg/apache/avro/io/parsing/Symbol$Kind;[Lorg/apache/avro/io/parsing/Symbol;)V

    .line 333
    iput-object p1, p0, Lorg/apache/avro/io/parsing/Symbol$Repeater;->end:Lorg/apache/avro/io/parsing/Symbol;

    .line 334
    iget-object v0, p0, Lorg/apache/avro/io/parsing/Symbol$Repeater;->production:[Lorg/apache/avro/io/parsing/Symbol;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 335
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 328
    invoke-direct {p0, p1, p2}, Lorg/apache/avro/io/parsing/Symbol$Repeater;-><init>(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;)V

    return-void
.end method

.method private static makeProduction([Lorg/apache/avro/io/parsing/Symbol;)[Lorg/apache/avro/io/parsing/Symbol;
    .locals 4
    .parameter "p"

    .prologue
    .line 338
    array-length v1, p0

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [Lorg/apache/avro/io/parsing/Symbol;

    .line 339
    .local v0, result:[Lorg/apache/avro/io/parsing/Symbol;
    const/4 v1, 0x0

    const/4 v2, 0x1

    array-length v3, p0

    invoke-static {p0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 340
    return-object v0
.end method


# virtual methods
.method public flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol$Repeater;
    .locals 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Fixup;",
            ">;>;)",
            "Lorg/apache/avro/io/parsing/Symbol$Repeater;"
        }
    .end annotation

    .prologue
    .local p1, map:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Lorg/apache/avro/io/parsing/Symbol$Sequence;>;"
    .local p2, map2:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;>;"
    const/4 v1, 0x1

    .line 346
    new-instance v6, Lorg/apache/avro/io/parsing/Symbol$Repeater;

    iget-object v0, p0, Lorg/apache/avro/io/parsing/Symbol$Repeater;->end:Lorg/apache/avro/io/parsing/Symbol;

    iget-object v2, p0, Lorg/apache/avro/io/parsing/Symbol$Repeater;->production:[Lorg/apache/avro/io/parsing/Symbol;

    invoke-static {v2, v1}, Lorg/apache/avro/io/parsing/Symbol$Repeater;->flattenedSize([Lorg/apache/avro/io/parsing/Symbol;I)I

    move-result v2

    new-array v2, v2, [Lorg/apache/avro/io/parsing/Symbol;

    invoke-direct {v6, v0, v2}, Lorg/apache/avro/io/parsing/Symbol$Repeater;-><init>(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;)V

    .line 348
    .local v6, result:Lorg/apache/avro/io/parsing/Symbol$Repeater;
    iget-object v0, p0, Lorg/apache/avro/io/parsing/Symbol$Repeater;->production:[Lorg/apache/avro/io/parsing/Symbol;

    iget-object v2, v6, Lorg/apache/avro/io/parsing/Symbol$Repeater;->production:[Lorg/apache/avro/io/parsing/Symbol;

    move v3, v1

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/avro/io/parsing/Symbol$Repeater;->flatten([Lorg/apache/avro/io/parsing/Symbol;I[Lorg/apache/avro/io/parsing/Symbol;ILjava/util/Map;Ljava/util/Map;)V

    .line 349
    return-object v6
.end method

.method public bridge synthetic flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 328
    invoke-virtual {p0, p1, p2}, Lorg/apache/avro/io/parsing/Symbol$Repeater;->flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol$Repeater;

    move-result-object v0

    return-object v0
.end method
