.class public Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;
.super Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;
.source "Symbol.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/io/parsing/Symbol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResolvingAction"
.end annotation


# instance fields
.field public final reader:Lorg/apache/avro/io/parsing/Symbol;

.field public final writer:Lorg/apache/avro/io/parsing/Symbol;


# direct methods
.method private constructor <init>(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;)V
    .locals 1
    .parameter "writer"
    .parameter "reader"

    .prologue
    .line 426
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(Lorg/apache/avro/io/parsing/Symbol$1;)V

    .line 427
    iput-object p1, p0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;->writer:Lorg/apache/avro/io/parsing/Symbol;

    .line 428
    iput-object p2, p0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;->reader:Lorg/apache/avro/io/parsing/Symbol;

    .line 429
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 423
    invoke-direct {p0, p1, p2}, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;-><init>(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;)V

    return-void
.end method


# virtual methods
.method public flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Fixup;",
            ">;>;)",
            "Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;"
        }
    .end annotation

    .prologue
    .line 434
    .local p1, map:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Lorg/apache/avro/io/parsing/Symbol$Sequence;>;"
    .local p2, map2:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;>;"
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;

    iget-object v1, p0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;->writer:Lorg/apache/avro/io/parsing/Symbol;

    invoke-virtual {v1, p1, p2}, Lorg/apache/avro/io/parsing/Symbol;->flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;->reader:Lorg/apache/avro/io/parsing/Symbol;

    invoke-virtual {v2, p1, p2}, Lorg/apache/avro/io/parsing/Symbol;->flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;-><init>(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;)V

    return-object v0
.end method

.method public bridge synthetic flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 423
    invoke-virtual {p0, p1, p2}, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;->flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;

    move-result-object v0

    return-object v0
.end method
