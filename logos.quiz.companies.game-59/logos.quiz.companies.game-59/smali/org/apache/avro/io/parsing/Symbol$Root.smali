.class public Lorg/apache/avro/io/parsing/Symbol$Root;
.super Lorg/apache/avro/io/parsing/Symbol;
.source "Symbol.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/io/parsing/Symbol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Root"
.end annotation


# direct methods
.method private varargs constructor <init>([Lorg/apache/avro/io/parsing/Symbol;)V
    .locals 2
    .parameter "symbols"

    .prologue
    .line 254
    sget-object v0, Lorg/apache/avro/io/parsing/Symbol$Kind;->ROOT:Lorg/apache/avro/io/parsing/Symbol$Kind;

    invoke-static {p1}, Lorg/apache/avro/io/parsing/Symbol$Root;->makeProduction([Lorg/apache/avro/io/parsing/Symbol;)[Lorg/apache/avro/io/parsing/Symbol;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/avro/io/parsing/Symbol;-><init>(Lorg/apache/avro/io/parsing/Symbol$Kind;[Lorg/apache/avro/io/parsing/Symbol;)V

    .line 255
    iget-object v0, p0, Lorg/apache/avro/io/parsing/Symbol$Root;->production:[Lorg/apache/avro/io/parsing/Symbol;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 256
    return-void
.end method

.method synthetic constructor <init>([Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lorg/apache/avro/io/parsing/Symbol$Root;-><init>([Lorg/apache/avro/io/parsing/Symbol;)V

    return-void
.end method

.method private static makeProduction([Lorg/apache/avro/io/parsing/Symbol;)[Lorg/apache/avro/io/parsing/Symbol;
    .locals 6
    .parameter "symbols"

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-static {p0, v1}, Lorg/apache/avro/io/parsing/Symbol$Root;->flattenedSize([Lorg/apache/avro/io/parsing/Symbol;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [Lorg/apache/avro/io/parsing/Symbol;

    .line 260
    .local v2, result:[Lorg/apache/avro/io/parsing/Symbol;
    const/4 v3, 0x1

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lorg/apache/avro/io/parsing/Symbol$Root;->flatten([Lorg/apache/avro/io/parsing/Symbol;I[Lorg/apache/avro/io/parsing/Symbol;ILjava/util/Map;Ljava/util/Map;)V

    .line 263
    return-object v2
.end method
