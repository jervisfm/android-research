.class public abstract Lorg/apache/avro/io/parsing/Symbol;
.super Ljava/lang/Object;
.source "Symbol.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/avro/io/parsing/Symbol$1;,
        Lorg/apache/avro/io/parsing/Symbol$EnumLabelsAction;,
        Lorg/apache/avro/io/parsing/Symbol$UnionAdjustAction;,
        Lorg/apache/avro/io/parsing/Symbol$DefaultStartAction;,
        Lorg/apache/avro/io/parsing/Symbol$FieldOrderAction;,
        Lorg/apache/avro/io/parsing/Symbol$FieldAdjustAction;,
        Lorg/apache/avro/io/parsing/Symbol$SkipAction;,
        Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;,
        Lorg/apache/avro/io/parsing/Symbol$WriterUnionAction;,
        Lorg/apache/avro/io/parsing/Symbol$EnumAdjustAction;,
        Lorg/apache/avro/io/parsing/Symbol$IntCheckAction;,
        Lorg/apache/avro/io/parsing/Symbol$ErrorAction;,
        Lorg/apache/avro/io/parsing/Symbol$Alternative;,
        Lorg/apache/avro/io/parsing/Symbol$Repeater;,
        Lorg/apache/avro/io/parsing/Symbol$Sequence;,
        Lorg/apache/avro/io/parsing/Symbol$Root;,
        Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;,
        Lorg/apache/avro/io/parsing/Symbol$Terminal;,
        Lorg/apache/avro/io/parsing/Symbol$Fixup;,
        Lorg/apache/avro/io/parsing/Symbol$Kind;
    }
.end annotation


# static fields
.field public static final ARRAY_END:Lorg/apache/avro/io/parsing/Symbol;

.field public static final ARRAY_START:Lorg/apache/avro/io/parsing/Symbol;

.field public static final BOOLEAN:Lorg/apache/avro/io/parsing/Symbol;

.field public static final BYTES:Lorg/apache/avro/io/parsing/Symbol;

.field public static final DEFAULT_END_ACTION:Lorg/apache/avro/io/parsing/Symbol;

.field public static final DOUBLE:Lorg/apache/avro/io/parsing/Symbol;

.field public static final ENUM:Lorg/apache/avro/io/parsing/Symbol;

.field public static final FIELD_ACTION:Lorg/apache/avro/io/parsing/Symbol;

.field public static final FIELD_END:Lorg/apache/avro/io/parsing/Symbol;

.field public static final FIXED:Lorg/apache/avro/io/parsing/Symbol;

.field public static final FLOAT:Lorg/apache/avro/io/parsing/Symbol;

.field public static final INT:Lorg/apache/avro/io/parsing/Symbol;

.field public static final ITEM_END:Lorg/apache/avro/io/parsing/Symbol;

.field public static final LONG:Lorg/apache/avro/io/parsing/Symbol;

.field public static final MAP_END:Lorg/apache/avro/io/parsing/Symbol;

.field public static final MAP_KEY_MARKER:Lorg/apache/avro/io/parsing/Symbol;

.field public static final MAP_START:Lorg/apache/avro/io/parsing/Symbol;

.field public static final NULL:Lorg/apache/avro/io/parsing/Symbol;

.field public static final RECORD_END:Lorg/apache/avro/io/parsing/Symbol;

.field public static final RECORD_START:Lorg/apache/avro/io/parsing/Symbol;

.field public static final STRING:Lorg/apache/avro/io/parsing/Symbol;

.field public static final UNION:Lorg/apache/avro/io/parsing/Symbol;

.field public static final UNION_END:Lorg/apache/avro/io/parsing/Symbol;


# instance fields
.field public final kind:Lorg/apache/avro/io/parsing/Symbol$Kind;

.field public final production:[Lorg/apache/avro/io/parsing/Symbol;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 521
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "null"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->NULL:Lorg/apache/avro/io/parsing/Symbol;

    .line 522
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "boolean"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->BOOLEAN:Lorg/apache/avro/io/parsing/Symbol;

    .line 523
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "int"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->INT:Lorg/apache/avro/io/parsing/Symbol;

    .line 524
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "long"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->LONG:Lorg/apache/avro/io/parsing/Symbol;

    .line 525
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "float"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->FLOAT:Lorg/apache/avro/io/parsing/Symbol;

    .line 526
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "double"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->DOUBLE:Lorg/apache/avro/io/parsing/Symbol;

    .line 527
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "string"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->STRING:Lorg/apache/avro/io/parsing/Symbol;

    .line 528
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "bytes"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->BYTES:Lorg/apache/avro/io/parsing/Symbol;

    .line 529
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "fixed"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->FIXED:Lorg/apache/avro/io/parsing/Symbol;

    .line 530
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "enum"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->ENUM:Lorg/apache/avro/io/parsing/Symbol;

    .line 531
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "union"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->UNION:Lorg/apache/avro/io/parsing/Symbol;

    .line 533
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "array-start"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->ARRAY_START:Lorg/apache/avro/io/parsing/Symbol;

    .line 534
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "array-end"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->ARRAY_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 535
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "map-start"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->MAP_START:Lorg/apache/avro/io/parsing/Symbol;

    .line 536
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "map-end"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->MAP_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 537
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "item-end"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->ITEM_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 540
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "field-action"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->FIELD_ACTION:Lorg/apache/avro/io/parsing/Symbol;

    .line 543
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(ZLorg/apache/avro/io/parsing/Symbol$1;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->RECORD_START:Lorg/apache/avro/io/parsing/Symbol;

    .line 544
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;

    invoke-direct {v0, v3, v2}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(ZLorg/apache/avro/io/parsing/Symbol$1;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->RECORD_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 545
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;

    invoke-direct {v0, v3, v2}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(ZLorg/apache/avro/io/parsing/Symbol$1;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->UNION_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 546
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;

    invoke-direct {v0, v3, v2}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(ZLorg/apache/avro/io/parsing/Symbol$1;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->FIELD_END:Lorg/apache/avro/io/parsing/Symbol;

    .line 548
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;

    invoke-direct {v0, v3, v2}, Lorg/apache/avro/io/parsing/Symbol$ImplicitAction;-><init>(ZLorg/apache/avro/io/parsing/Symbol$1;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->DEFAULT_END_ACTION:Lorg/apache/avro/io/parsing/Symbol;

    .line 549
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Terminal;

    const-string v1, "map-key-marker"

    invoke-direct {v0, v1}, Lorg/apache/avro/io/parsing/Symbol$Terminal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/avro/io/parsing/Symbol;->MAP_KEY_MARKER:Lorg/apache/avro/io/parsing/Symbol;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/avro/io/parsing/Symbol$Kind;)V
    .locals 1
    .parameter "kind"

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/avro/io/parsing/Symbol;-><init>(Lorg/apache/avro/io/parsing/Symbol$Kind;[Lorg/apache/avro/io/parsing/Symbol;)V

    .line 79
    return-void
.end method

.method protected constructor <init>(Lorg/apache/avro/io/parsing/Symbol$Kind;[Lorg/apache/avro/io/parsing/Symbol;)V
    .locals 0
    .parameter "kind"
    .parameter "production"

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p2, p0, Lorg/apache/avro/io/parsing/Symbol;->production:[Lorg/apache/avro/io/parsing/Symbol;

    .line 84
    iput-object p1, p0, Lorg/apache/avro/io/parsing/Symbol;->kind:Lorg/apache/avro/io/parsing/Symbol$Kind;

    .line 85
    return-void
.end method

.method static alt([Lorg/apache/avro/io/parsing/Symbol;[Ljava/lang/String;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "symbols"
    .parameter "labels"

    .prologue
    .line 113
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Alternative;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/avro/io/parsing/Symbol$Alternative;-><init>([Lorg/apache/avro/io/parsing/Symbol;[Ljava/lang/String;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method

.method static error(Ljava/lang/String;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "e"

    .prologue
    .line 122
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ErrorAction;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/avro/io/parsing/Symbol$ErrorAction;-><init>(Ljava/lang/String;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method

.method static flatten([Lorg/apache/avro/io/parsing/Symbol;I[Lorg/apache/avro/io/parsing/Symbol;ILjava/util/Map;Ljava/util/Map;)V
    .locals 8
    .parameter "in"
    .parameter "start"
    .parameter "out"
    .parameter "skip"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/avro/io/parsing/Symbol;",
            "I[",
            "Lorg/apache/avro/io/parsing/Symbol;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Fixup;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 187
    .local p4, map:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Lorg/apache/avro/io/parsing/Symbol$Sequence;>;"
    .local p5, map2:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;>;"
    move v0, p1

    .local v0, i:I
    move v1, p3

    .local v1, j:I
    :goto_0
    array-length v6, p0

    if-ge v0, v6, :cond_2

    .line 188
    aget-object v6, p0, v0

    invoke-virtual {v6, p4, p5}, Lorg/apache/avro/io/parsing/Symbol;->flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;

    move-result-object v5

    .line 189
    .local v5, s:Lorg/apache/avro/io/parsing/Symbol;
    instance-of v6, v5, Lorg/apache/avro/io/parsing/Symbol$Sequence;

    if-eqz v6, :cond_1

    .line 190
    iget-object v4, v5, Lorg/apache/avro/io/parsing/Symbol;->production:[Lorg/apache/avro/io/parsing/Symbol;

    .line 191
    .local v4, p:[Lorg/apache/avro/io/parsing/Symbol;
    invoke-interface {p5, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 192
    .local v3, l:Ljava/util/List;,"Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;"
    if-nez v3, :cond_0

    .line 193
    const/4 v6, 0x0

    array-length v7, v4

    invoke-static {v4, v6, p2, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    :goto_1
    array-length v6, v4

    add-int/2addr v1, v6

    .line 187
    .end local v3           #l:Ljava/util/List;,"Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;"
    .end local v4           #p:[Lorg/apache/avro/io/parsing/Symbol;
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    .restart local v3       #l:Ljava/util/List;,"Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;"
    .restart local v4       #p:[Lorg/apache/avro/io/parsing/Symbol;
    :cond_0
    new-instance v6, Lorg/apache/avro/io/parsing/Symbol$Fixup;

    invoke-direct {v6, p2, v1}, Lorg/apache/avro/io/parsing/Symbol$Fixup;-><init>([Lorg/apache/avro/io/parsing/Symbol;I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 199
    .end local v3           #l:Ljava/util/List;,"Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;"
    .end local v4           #p:[Lorg/apache/avro/io/parsing/Symbol;
    :cond_1
    add-int/lit8 v2, v1, 0x1

    .end local v1           #j:I
    .local v2, j:I
    aput-object v5, p2, v1

    move v1, v2

    .end local v2           #j:I
    .restart local v1       #j:I
    goto :goto_2

    .line 202
    .end local v5           #s:Lorg/apache/avro/io/parsing/Symbol;
    :cond_2
    return-void
.end method

.method protected static flattenedSize([Lorg/apache/avro/io/parsing/Symbol;I)I
    .locals 4
    .parameter "symbols"
    .parameter "start"

    .prologue
    .line 213
    const/4 v1, 0x0

    .line 214
    .local v1, result:I
    move v0, p1

    .local v0, i:I
    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 215
    aget-object v3, p0, v0

    instance-of v3, v3, Lorg/apache/avro/io/parsing/Symbol$Sequence;

    if-eqz v3, :cond_0

    .line 216
    aget-object v2, p0, v0

    check-cast v2, Lorg/apache/avro/io/parsing/Symbol$Sequence;

    .line 217
    .local v2, s:Lorg/apache/avro/io/parsing/Symbol$Sequence;
    invoke-virtual {v2}, Lorg/apache/avro/io/parsing/Symbol$Sequence;->flattenedSize()I

    move-result v3

    add-int/2addr v1, v3

    .line 214
    .end local v2           #s:Lorg/apache/avro/io/parsing/Symbol$Sequence;
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 222
    :cond_1
    return v1
.end method

.method static varargs repeat(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "endSymbol"
    .parameter "symsToRepeat"

    .prologue
    .line 106
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Repeater;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/avro/io/parsing/Symbol$Repeater;-><init>(Lorg/apache/avro/io/parsing/Symbol;[Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method

.method static resolve(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "w"
    .parameter "r"

    .prologue
    .line 131
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/avro/io/parsing/Symbol$ResolvingAction;-><init>(Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method

.method static varargs root([Lorg/apache/avro/io/parsing/Symbol;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "symbols"

    .prologue
    .line 91
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Root;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/avro/io/parsing/Symbol$Root;-><init>([Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method

.method static varargs seq([Lorg/apache/avro/io/parsing/Symbol;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 2
    .parameter "production"

    .prologue
    .line 98
    new-instance v0, Lorg/apache/avro/io/parsing/Symbol$Sequence;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/avro/io/parsing/Symbol$Sequence;-><init>([Lorg/apache/avro/io/parsing/Symbol;Lorg/apache/avro/io/parsing/Symbol$1;)V

    return-object v0
.end method


# virtual methods
.method public flatten(Ljava/util/Map;Ljava/util/Map;)Lorg/apache/avro/io/parsing/Symbol;
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Sequence;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/avro/io/parsing/Symbol$Fixup;",
            ">;>;)",
            "Lorg/apache/avro/io/parsing/Symbol;"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, map:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Lorg/apache/avro/io/parsing/Symbol$Sequence;>;"
    .local p2, map2:Ljava/util/Map;,"Ljava/util/Map<Lorg/apache/avro/io/parsing/Symbol$Sequence;Ljava/util/List<Lorg/apache/avro/io/parsing/Symbol$Fixup;>;>;"
    return-object p0
.end method

.method public flattenedSize()I
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    return v0
.end method
