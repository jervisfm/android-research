.class public Lorg/apache/avro/reflect/ReflectData$AllowNull;
.super Lorg/apache/avro/reflect/ReflectData;
.source "ReflectData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/avro/reflect/ReflectData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllowNull"
.end annotation


# static fields
.field private static final INSTANCE:Lorg/apache/avro/reflect/ReflectData$AllowNull;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lorg/apache/avro/reflect/ReflectData$AllowNull;

    invoke-direct {v0}, Lorg/apache/avro/reflect/ReflectData$AllowNull;-><init>()V

    sput-object v0, Lorg/apache/avro/reflect/ReflectData$AllowNull;->INSTANCE:Lorg/apache/avro/reflect/ReflectData$AllowNull;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lorg/apache/avro/reflect/ReflectData;-><init>()V

    return-void
.end method

.method public static get()Lorg/apache/avro/reflect/ReflectData$AllowNull;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lorg/apache/avro/reflect/ReflectData$AllowNull;->INSTANCE:Lorg/apache/avro/reflect/ReflectData$AllowNull;

    return-object v0
.end method


# virtual methods
.method protected createFieldSchema(Ljava/lang/reflect/Field;Ljava/util/Map;)Lorg/apache/avro/Schema;
    .locals 2
    .parameter "field"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/avro/Schema;",
            ">;)",
            "Lorg/apache/avro/Schema;"
        }
    .end annotation

    .prologue
    .line 70
    .local p2, names:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lorg/apache/avro/Schema;>;"
    invoke-super {p0, p1, p2}, Lorg/apache/avro/reflect/ReflectData;->createFieldSchema(Ljava/lang/reflect/Field;Ljava/util/Map;)Lorg/apache/avro/Schema;

    move-result-object v0

    .line 71
    .local v0, schema:Lorg/apache/avro/Schema;
    invoke-static {v0}, Lorg/apache/avro/reflect/ReflectData$AllowNull;->makeNullable(Lorg/apache/avro/Schema;)Lorg/apache/avro/Schema;

    move-result-object v1

    return-object v1
.end method
