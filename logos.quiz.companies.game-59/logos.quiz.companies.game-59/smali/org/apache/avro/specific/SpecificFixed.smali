.class public abstract Lorg/apache/avro/specific/SpecificFixed;
.super Lorg/apache/avro/generic/GenericData$Fixed;
.source "SpecificFixed.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/apache/avro/generic/GenericData$Fixed;-><init>()V

    .line 25
    invoke-static {}, Lorg/apache/avro/specific/SpecificData;->get()Lorg/apache/avro/specific/SpecificData;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/avro/specific/SpecificData;->getSchema(Ljava/lang/reflect/Type;)Lorg/apache/avro/Schema;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/avro/specific/SpecificFixed;->setSchema(Lorg/apache/avro/Schema;)V

    .line 26
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .parameter "bytes"

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/avro/specific/SpecificFixed;-><init>()V

    .line 30
    invoke-virtual {p0, p1}, Lorg/apache/avro/specific/SpecificFixed;->bytes([B)V

    .line 31
    return-void
.end method
