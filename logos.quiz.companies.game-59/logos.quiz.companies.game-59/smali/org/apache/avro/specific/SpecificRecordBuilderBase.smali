.class public abstract Lorg/apache/avro/specific/SpecificRecordBuilderBase;
.super Lorg/apache/avro/data/RecordBuilderBase;
.source "SpecificRecordBuilderBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lorg/apache/avro/specific/SpecificRecord;",
        ">",
        "Lorg/apache/avro/data/RecordBuilderBase",
        "<TT;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lorg/apache/avro/Schema;)V
    .locals 1
    .parameter "schema"

    .prologue
    .line 35
    .local p0, this:Lorg/apache/avro/specific/SpecificRecordBuilderBase;,"Lorg/apache/avro/specific/SpecificRecordBuilderBase<TT;>;"
    invoke-static {}, Lorg/apache/avro/specific/SpecificData;->get()Lorg/apache/avro/specific/SpecificData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/avro/data/RecordBuilderBase;-><init>(Lorg/apache/avro/Schema;Lorg/apache/avro/generic/GenericData;)V

    .line 36
    return-void
.end method

.method protected constructor <init>(Lorg/apache/avro/specific/SpecificRecord;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, this:Lorg/apache/avro/specific/SpecificRecordBuilderBase;,"Lorg/apache/avro/specific/SpecificRecordBuilderBase<TT;>;"
    .local p1, other:Lorg/apache/avro/specific/SpecificRecord;,"TT;"
    invoke-interface {p1}, Lorg/apache/avro/specific/SpecificRecord;->getSchema()Lorg/apache/avro/Schema;

    move-result-object v0

    invoke-static {}, Lorg/apache/avro/specific/SpecificData;->get()Lorg/apache/avro/specific/SpecificData;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/avro/data/RecordBuilderBase;-><init>(Lorg/apache/avro/Schema;Lorg/apache/avro/generic/GenericData;)V

    .line 52
    return-void
.end method

.method protected constructor <init>(Lorg/apache/avro/specific/SpecificRecordBuilderBase;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/avro/specific/SpecificRecordBuilderBase",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, this:Lorg/apache/avro/specific/SpecificRecordBuilderBase;,"Lorg/apache/avro/specific/SpecificRecordBuilderBase<TT;>;"
    .local p1, other:Lorg/apache/avro/specific/SpecificRecordBuilderBase;,"Lorg/apache/avro/specific/SpecificRecordBuilderBase<TT;>;"
    invoke-static {}, Lorg/apache/avro/specific/SpecificData;->get()Lorg/apache/avro/specific/SpecificData;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/avro/data/RecordBuilderBase;-><init>(Lorg/apache/avro/data/RecordBuilderBase;Lorg/apache/avro/generic/GenericData;)V

    .line 44
    return-void
.end method
