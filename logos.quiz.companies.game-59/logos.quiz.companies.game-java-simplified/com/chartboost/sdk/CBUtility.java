package com.chartboost.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class CBUtility
{
  private static String ANDROID_AUID = null;
  public static final int ANDROID_AUID_COOKIE_FLAG = 1;
  public static final String ANDROID_AUID_COOKIE_KEY = "cb_auid";
  public static final String ANDROID_AUID_COOKIE_PREFIX = "android-";
  public static final String AUID_STATIC_EMULATOR = "ffff";
  public static final String AUID_STATIC_ERROR = "unknown";
  public static final String PREFERENCES_FILE = "ChartBoost.cb";
  public static final String PREFERENCES_SDCARD_PATH = "/Android/data/com.chartboost.sdk/files";

  public static String getAUID(Context paramContext)
  {
    if (ANDROID_AUID != null)
      return ANDROID_AUID;
    int i = 1;
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("ChartBoost.cb", 1);
    String str1 = localSharedPreferences.getString("cb_auid", null);
    Object localObject = null;
    if (str1 != null)
      localObject = str1;
    File localFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/com.chartboost.sdk/files" + "/" + "ChartBoost.cb");
    if (localObject == null);
    try
    {
      StringBuffer localStringBuffer;
      FileInputStream localFileInputStream;
      if ((Environment.getExternalStorageState().equals("mounted")) && (localFile.isFile()))
      {
        localStringBuffer = new StringBuffer("");
        localFileInputStream = new FileInputStream(localFile);
      }
      while (true)
      {
        int j = localFileInputStream.read();
        if (j == -1)
        {
          localFileInputStream.close();
          if (localStringBuffer.length() == 40)
          {
            String str3 = localStringBuffer.toString();
            localObject = str3;
            i = 0;
          }
          label170: if (localObject != null)
            break label242;
          localUUID = UUID.randomUUID();
          if (localUUID != null)
            break;
          return "unknown";
        }
        char c = (char)j;
        localStringBuffer.append(c);
      }
    }
    catch (Exception localException)
    {
      UUID localUUID;
      break label170;
      String str2 = localUUID.toString();
      localObject = "android-" + str2.replace("-", "");
    }
    label242: if ((!localSharedPreferences.edit().putString("cb_auid", (String)localObject).commit()) || (i != 0));
    try
    {
      if (localFile.exists())
        localFile.delete();
      while (true)
      {
        if (localFile.createNewFile())
        {
          FileWriter localFileWriter = new FileWriter(localFile, false);
          localFileWriter.write((String)localObject);
          localFileWriter.close();
        }
        label315: ANDROID_AUID = (String)localObject;
        return localObject;
        return "unknown";
        localFile.mkdirs();
      }
    }
    catch (IOException localIOException)
    {
      break label315;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.chartboost.sdk.CBUtility
 * JD-Core Version:    0.6.2
 */