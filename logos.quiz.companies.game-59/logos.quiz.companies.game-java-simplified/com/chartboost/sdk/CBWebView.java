package com.chartboost.sdk;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout.LayoutParams;
import org.json.JSONObject;

public class CBWebView extends WebView
{
  private JSONObject responseContext;
  private CBViewState state;
  private CBViewType type;

  public CBWebView(Context paramContext, CBViewType paramCBViewType)
  {
    super(paramContext);
    setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
    if (paramCBViewType == CBViewType.CBViewTypeMoreApps)
      setBackgroundColor(-13421773);
    while (true)
    {
      getSettings().setJavaScriptEnabled(true);
      this.type = paramCBViewType;
      if (paramCBViewType == CBViewType.CBViewTypeInterstitial)
        setOnTouchListener(new View.OnTouchListener()
        {
          public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
          {
            return paramAnonymousMotionEvent.getAction() == 2;
          }
        });
      return;
      setBackgroundColor(-1728053248);
    }
  }

  public JSONObject getResponseContext()
  {
    return this.responseContext;
  }

  public CBViewState getState()
  {
    return this.state;
  }

  public CBViewType getType()
  {
    return this.type;
  }

  public void setResponseContext(JSONObject paramJSONObject)
  {
    this.responseContext = paramJSONObject;
  }

  public void setState(CBViewState paramCBViewState)
  {
    this.state = paramCBViewState;
  }

  public void setType(CBViewType paramCBViewType)
  {
    this.type = paramCBViewType;
  }

  public static enum CBViewState
  {
    static
    {
      CBViewStateDisplayedByDefaultController = new CBViewState("CBViewStateDisplayedByDefaultController", 2);
      CBViewStateWaitingForDismissal = new CBViewState("CBViewStateWaitingForDismissal", 3);
      CBViewState[] arrayOfCBViewState = new CBViewState[4];
      arrayOfCBViewState[0] = CBViewStateOther;
      arrayOfCBViewState[1] = CBViewStateWaitingForDisplay;
      arrayOfCBViewState[2] = CBViewStateDisplayedByDefaultController;
      arrayOfCBViewState[3] = CBViewStateWaitingForDismissal;
    }
  }

  public static enum CBViewType
  {
    static
    {
      CBViewType[] arrayOfCBViewType = new CBViewType[2];
      arrayOfCBViewType[0] = CBViewTypeInterstitial;
      arrayOfCBViewType[1] = CBViewTypeMoreApps;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.chartboost.sdk.CBWebView
 * JD-Core Version:    0.6.2
 */