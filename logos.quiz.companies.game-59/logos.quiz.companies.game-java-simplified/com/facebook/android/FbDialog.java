package com.facebook.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class FbDialog extends Dialog
{
  static final float[] DIMENSIONS_DIFF_LANDSCAPE = { 20.0F, 60.0F };
  static final float[] DIMENSIONS_DIFF_PORTRAIT = { 40.0F, 60.0F };
  static final String DISPLAY_STRING = "touch";
  static final int FB_BLUE = -9599820;
  static final String FB_ICON = "icon.png";
  static final FrameLayout.LayoutParams FILL = new FrameLayout.LayoutParams(-1, -1);
  static final int MARGIN = 4;
  static final int PADDING = 2;
  private FrameLayout mContent;
  private ImageView mCrossImage;
  private Facebook.DialogListener mListener;
  private ProgressDialog mSpinner;
  private String mUrl;
  private WebView mWebView;

  public FbDialog(Context paramContext, String paramString, Facebook.DialogListener paramDialogListener)
  {
    super(paramContext, 16973840);
    this.mUrl = paramString;
    this.mListener = paramDialogListener;
  }

  private void createCrossImage()
  {
    this.mCrossImage = new ImageView(getContext());
    this.mCrossImage.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        FbDialog.this.mListener.onCancel();
        FbDialog.this.dismiss();
      }
    });
    String str = getContext().getPackageName();
    int i = getContext().getResources().getIdentifier("@drawable/swarm_close", null, str);
    Drawable localDrawable = getContext().getResources().getDrawable(i);
    this.mCrossImage.setImageDrawable(localDrawable);
    this.mCrossImage.setVisibility(4);
  }

  private void setUpWebView(int paramInt)
  {
    LinearLayout localLinearLayout = new LinearLayout(getContext());
    this.mWebView = new CustomWebView(getContext());
    this.mWebView.setVerticalScrollBarEnabled(false);
    this.mWebView.setHorizontalScrollBarEnabled(false);
    this.mWebView.setWebViewClient(new FbWebViewClient(null));
    this.mWebView.getSettings().setJavaScriptEnabled(true);
    this.mWebView.loadUrl(this.mUrl);
    this.mWebView.setLayoutParams(FILL);
    this.mWebView.setVisibility(4);
    localLinearLayout.setPadding(paramInt, paramInt, paramInt, paramInt);
    localLinearLayout.addView(this.mWebView);
    this.mContent.addView(localLinearLayout);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mSpinner = new ProgressDialog(getContext());
    this.mSpinner.requestWindowFeature(1);
    this.mSpinner.setMessage("Loading...");
    requestWindowFeature(1);
    this.mContent = new FrameLayout(getContext());
    createCrossImage();
    setUpWebView(this.mCrossImage.getDrawable().getIntrinsicWidth() / 2);
    this.mContent.addView(this.mCrossImage, new ViewGroup.LayoutParams(-2, -2));
    addContentView(this.mContent, new ViewGroup.LayoutParams(-1, -1));
  }

  public class CustomWebView extends WebView
  {
    public CustomWebView(Context arg2)
    {
      super();
    }

    public CustomWebView(Context paramAttributeSet, AttributeSet arg3)
    {
      super(localAttributeSet);
    }

    public CustomWebView(Context paramAttributeSet, AttributeSet paramInt, int arg4)
    {
      super(paramInt, i);
    }

    public void onWindowFocusChanged(boolean paramBoolean)
    {
      try
      {
        super.onWindowFocusChanged(paramBoolean);
        return;
      }
      catch (NullPointerException localNullPointerException)
      {
      }
    }
  }

  private class FbWebViewClient extends WebViewClient
  {
    private FbWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      FbDialog.this.mSpinner.dismiss();
      FbDialog.this.mContent.setBackgroundColor(0);
      FbDialog.this.mWebView.setVisibility(0);
      FbDialog.this.mCrossImage.setVisibility(0);
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      Log.d("Facebook-WebView", "Webview loading URL: " + paramString);
      super.onPageStarted(paramWebView, paramString, paramBitmap);
      FbDialog.this.mSpinner.show();
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      super.onReceivedError(paramWebView, paramInt, paramString1, paramString2);
      FbDialog.this.mListener.onError(new DialogError(paramString1, paramInt, paramString2));
      FbDialog.this.dismiss();
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      Log.d("Facebook-WebView", "Redirect URL: " + paramString);
      if (paramString.startsWith("fbconnect://success"))
      {
        Bundle localBundle = Util.parseUrl(paramString);
        String str = localBundle.getString("error");
        if (str == null)
          str = localBundle.getString("error_type");
        if (str == null)
          FbDialog.this.mListener.onComplete(localBundle);
        while (true)
        {
          FbDialog.this.dismiss();
          return true;
          if ((str.equals("access_denied")) || (str.equals("OAuthAccessDeniedException")))
            FbDialog.this.mListener.onCancel();
          else
            FbDialog.this.mListener.onFacebookError(new FacebookError(str));
        }
      }
      if (paramString.startsWith("fbconnect://cancel"))
      {
        FbDialog.this.mListener.onCancel();
        FbDialog.this.dismiss();
        return true;
      }
      if (paramString.contains("touch"))
        return false;
      FbDialog.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.facebook.android.FbDialog
 * JD-Core Version:    0.6.2
 */