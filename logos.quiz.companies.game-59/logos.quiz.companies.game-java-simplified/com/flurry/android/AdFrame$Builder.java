package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdFrame$Builder extends SpecificRecordBuilderBase<AdFrame>
  implements RecordBuilder<AdFrame>
{
  private int a;
  private CharSequence b;
  private CharSequence c;
  private AdSpaceLayout d;
  private List<Callback> e;
  private CharSequence f;

  private AdFrame$Builder()
  {
    super(AdFrame.SCHEMA$);
  }

  public AdFrame build()
  {
    try
    {
      AdFrame localAdFrame = new AdFrame();
      int i;
      CharSequence localCharSequence1;
      label42: CharSequence localCharSequence2;
      label63: AdSpaceLayout localAdSpaceLayout;
      label84: List localList;
      if (fieldSetFlags()[0] != 0)
      {
        i = this.a;
        localAdFrame.a = i;
        if (fieldSetFlags()[1] == 0)
          break label154;
        localCharSequence1 = this.b;
        localAdFrame.b = localCharSequence1;
        if (fieldSetFlags()[2] == 0)
          break label172;
        localCharSequence2 = this.c;
        localAdFrame.c = localCharSequence2;
        if (fieldSetFlags()[3] == 0)
          break label190;
        localAdSpaceLayout = this.d;
        localAdFrame.d = localAdSpaceLayout;
        if (fieldSetFlags()[4] == 0)
          break label208;
        localList = this.e;
        label105: localAdFrame.e = localList;
        if (fieldSetFlags()[5] == 0)
          break label226;
      }
      label154: label172: label190: label208: label226: for (CharSequence localCharSequence3 = this.f; ; localCharSequence3 = (CharSequence)defaultValue(fields()[5]))
      {
        localAdFrame.f = localCharSequence3;
        return localAdFrame;
        i = ((Integer)defaultValue(fields()[0])).intValue();
        break;
        localCharSequence1 = (CharSequence)defaultValue(fields()[1]);
        break label42;
        localCharSequence2 = (CharSequence)defaultValue(fields()[2]);
        break label63;
        localAdSpaceLayout = (AdSpaceLayout)defaultValue(fields()[3]);
        break label84;
        localList = (List)defaultValue(fields()[4]);
        break label105;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdGuid()
  {
    this.f = null;
    fieldSetFlags()[5] = 0;
    return this;
  }

  public Builder clearAdSpaceLayout()
  {
    this.d = null;
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Builder clearBinding()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearCallbacks()
  {
    this.e = null;
    fieldSetFlags()[4] = 0;
    return this;
  }

  public Builder clearContent()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearDisplay()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public CharSequence getAdGuid()
  {
    return this.f;
  }

  public AdSpaceLayout getAdSpaceLayout()
  {
    return this.d;
  }

  public Integer getBinding()
  {
    return Integer.valueOf(this.a);
  }

  public List<Callback> getCallbacks()
  {
    return this.e;
  }

  public CharSequence getContent()
  {
    return this.c;
  }

  public CharSequence getDisplay()
  {
    return this.b;
  }

  public boolean hasAdGuid()
  {
    return fieldSetFlags()[5];
  }

  public boolean hasAdSpaceLayout()
  {
    return fieldSetFlags()[3];
  }

  public boolean hasBinding()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasCallbacks()
  {
    return fieldSetFlags()[4];
  }

  public boolean hasContent()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasDisplay()
  {
    return fieldSetFlags()[1];
  }

  public Builder setAdGuid(CharSequence paramCharSequence)
  {
    validate(fields()[5], paramCharSequence);
    this.f = paramCharSequence;
    fieldSetFlags()[5] = 1;
    return this;
  }

  public Builder setAdSpaceLayout(AdSpaceLayout paramAdSpaceLayout)
  {
    validate(fields()[3], paramAdSpaceLayout);
    this.d = paramAdSpaceLayout;
    fieldSetFlags()[3] = 1;
    return this;
  }

  public Builder setBinding(int paramInt)
  {
    validate(fields()[0], Integer.valueOf(paramInt));
    this.a = paramInt;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setCallbacks(List<Callback> paramList)
  {
    validate(fields()[4], paramList);
    this.e = paramList;
    fieldSetFlags()[4] = 1;
    return this;
  }

  public Builder setContent(CharSequence paramCharSequence)
  {
    validate(fields()[2], paramCharSequence);
    this.c = paramCharSequence;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setDisplay(CharSequence paramCharSequence)
  {
    validate(fields()[1], paramCharSequence);
    this.b = paramCharSequence;
    fieldSetFlags()[1] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdFrame.Builder
 * JD-Core Version:    0.6.2
 */