package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.specific.SpecificRecord;
import org.apache.avro.specific.SpecificRecordBase;

class AdFrame extends SpecificRecordBase
  implements SpecificRecord
{
  public static final Schema SCHEMA$ = new Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AdFrame\",\"namespace\":\"com.flurry.android\",\"fields\":[{\"name\":\"binding\",\"type\":\"int\"},{\"name\":\"display\",\"type\":\"string\"},{\"name\":\"content\",\"type\":\"string\"},{\"name\":\"adSpaceLayout\",\"type\":{\"type\":\"record\",\"name\":\"AdSpaceLayout\",\"fields\":[{\"name\":\"adWidth\",\"type\":\"int\"},{\"name\":\"adHeight\",\"type\":\"int\"},{\"name\":\"fix\",\"type\":\"string\"},{\"name\":\"format\",\"type\":\"string\"},{\"name\":\"alignment\",\"type\":\"string\"}]}},{\"name\":\"callbacks\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"Callback\",\"fields\":[{\"name\":\"event\",\"type\":\"string\"},{\"name\":\"actions\",\"type\":{\"type\":\"array\",\"items\":\"string\"}}]}}},{\"name\":\"adGuid\",\"type\":\"string\"}]}");
  public int a;
  public CharSequence b;
  public CharSequence c;
  public AdSpaceLayout d;
  public List<Callback> e;
  public CharSequence f;

  public final Integer a()
  {
    return Integer.valueOf(this.a);
  }

  public final CharSequence b()
  {
    return this.b;
  }

  public final CharSequence c()
  {
    return this.c;
  }

  public final AdSpaceLayout d()
  {
    return this.d;
  }

  public final List<Callback> e()
  {
    return this.e;
  }

  public final CharSequence f()
  {
    return this.f;
  }

  public Object get(int paramInt)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      return Integer.valueOf(this.a);
    case 1:
      return this.b;
    case 2:
      return this.c;
    case 3:
      return this.d;
    case 4:
      return this.e;
    case 5:
    }
    return this.f;
  }

  public Schema getSchema()
  {
    return SCHEMA$;
  }

  public void put(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      this.a = ((Integer)paramObject).intValue();
      return;
    case 1:
      this.b = ((CharSequence)paramObject);
      return;
    case 2:
      this.c = ((CharSequence)paramObject);
      return;
    case 3:
      this.d = ((AdSpaceLayout)paramObject);
      return;
    case 4:
      this.e = ((List)paramObject);
      return;
    case 5:
    }
    this.f = ((CharSequence)paramObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdFrame
 * JD-Core Version:    0.6.2
 */