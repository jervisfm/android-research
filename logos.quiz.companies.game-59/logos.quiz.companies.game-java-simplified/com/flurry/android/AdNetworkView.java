package com.flurry.android;

import android.content.Context;
import java.util.Map;

public abstract class AdNetworkView extends p
{
  public static String sAdNetworkApiKey;
  public AdCreative fAdCreative;

  public AdNetworkView(Context paramContext, AdCreative paramAdCreative)
  {
    super(paramContext);
    this.fAdCreative = paramAdCreative;
  }

  AdNetworkView(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc);
    this.fAdCreative = paramAdCreative;
  }

  public void onAdClicked(Map<String, String> paramMap)
  {
    super.a("clicked", true, paramMap);
  }

  public void onAdClosed(Map<String, String> paramMap)
  {
    super.a("adClosed", true, paramMap);
  }

  public void onAdFilled(Map<String, String> paramMap)
  {
    super.a("filled", true, paramMap);
  }

  public void onAdShown(Map<String, String> paramMap)
  {
    super.a("rendered", true, paramMap);
  }

  public void onAdUnFilled(Map<String, String> paramMap)
  {
    super.a("unfilled", true, paramMap);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdNetworkView
 * JD-Core Version:    0.6.2
 */