package com.flurry.android;

import java.nio.ByteBuffer;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdReportedId$Builder extends SpecificRecordBuilderBase<AdReportedId>
  implements RecordBuilder<AdReportedId>
{
  private int a;
  private ByteBuffer b;

  private AdReportedId$Builder(byte paramByte)
  {
    super(AdReportedId.SCHEMA$);
  }

  public AdReportedId build()
  {
    try
    {
      AdReportedId localAdReportedId = new AdReportedId();
      int i;
      if (fieldSetFlags()[0] != 0)
      {
        i = this.a;
        localAdReportedId.a = i;
        if (fieldSetFlags()[1] == 0)
          break label70;
      }
      label70: for (ByteBuffer localByteBuffer = this.b; ; localByteBuffer = (ByteBuffer)defaultValue(fields()[1]))
      {
        localAdReportedId.b = localByteBuffer;
        return localAdReportedId;
        i = ((Integer)defaultValue(fields()[0])).intValue();
        break;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearId()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearType()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public ByteBuffer getId()
  {
    return this.b;
  }

  public Integer getType()
  {
    return Integer.valueOf(this.a);
  }

  public boolean hasId()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasType()
  {
    return fieldSetFlags()[0];
  }

  public Builder setId(ByteBuffer paramByteBuffer)
  {
    validate(fields()[1], paramByteBuffer);
    this.b = paramByteBuffer;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setType(int paramInt)
  {
    validate(fields()[0], Integer.valueOf(paramInt));
    this.a = paramInt;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdReportedId.Builder
 * JD-Core Version:    0.6.2
 */