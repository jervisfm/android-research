package com.flurry.android;

import java.util.List;
import java.util.Map;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdRequest$Builder extends SpecificRecordBuilderBase<AdRequest>
  implements RecordBuilder<AdRequest>
{
  private CharSequence a;
  private CharSequence b;
  private CharSequence c;
  private long d;
  private List<AdReportedId> e;
  private Location f;
  private boolean g;
  private List<Integer> h;
  private AdViewContainer i;
  private CharSequence j;
  private CharSequence k;
  private CharSequence l;
  private CharSequence m;
  private TestAds n;
  private Map<CharSequence, CharSequence> o;
  private boolean p;

  private AdRequest$Builder(byte paramByte)
  {
    super(AdRequest.SCHEMA$);
  }

  public AdRequest build()
  {
    try
    {
      AdRequest localAdRequest = new AdRequest();
      CharSequence localCharSequence1;
      CharSequence localCharSequence2;
      label42: CharSequence localCharSequence3;
      label63: long l1;
      label84: List localList1;
      label105: Location localLocation;
      label126: boolean bool1;
      label148: List localList2;
      label170: AdViewContainer localAdViewContainer;
      label192: CharSequence localCharSequence4;
      label214: CharSequence localCharSequence5;
      label236: CharSequence localCharSequence6;
      label258: CharSequence localCharSequence7;
      label280: TestAds localTestAds;
      label302: Map localMap;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence1 = this.a;
        localAdRequest.a = localCharSequence1;
        if (fieldSetFlags()[1] == 0)
          break label371;
        localCharSequence2 = this.b;
        localAdRequest.b = localCharSequence2;
        if (fieldSetFlags()[2] == 0)
          break label389;
        localCharSequence3 = this.c;
        localAdRequest.c = localCharSequence3;
        if (fieldSetFlags()[3] == 0)
          break label407;
        l1 = this.d;
        localAdRequest.d = l1;
        if (fieldSetFlags()[4] == 0)
          break label428;
        localList1 = this.e;
        localAdRequest.e = localList1;
        if (fieldSetFlags()[5] == 0)
          break label446;
        localLocation = this.f;
        localAdRequest.f = localLocation;
        if (fieldSetFlags()[6] == 0)
          break label464;
        bool1 = this.g;
        localAdRequest.g = bool1;
        if (fieldSetFlags()[7] == 0)
          break label486;
        localList2 = this.h;
        localAdRequest.h = localList2;
        if (fieldSetFlags()[8] == 0)
          break label505;
        localAdViewContainer = this.i;
        localAdRequest.i = localAdViewContainer;
        if (fieldSetFlags()[9] == 0)
          break label524;
        localCharSequence4 = this.j;
        localAdRequest.j = localCharSequence4;
        if (fieldSetFlags()[10] == 0)
          break label543;
        localCharSequence5 = this.k;
        localAdRequest.k = localCharSequence5;
        if (fieldSetFlags()[11] == 0)
          break label562;
        localCharSequence6 = this.l;
        localAdRequest.l = localCharSequence6;
        if (fieldSetFlags()[12] == 0)
          break label581;
        localCharSequence7 = this.m;
        localAdRequest.m = localCharSequence7;
        if (fieldSetFlags()[13] == 0)
          break label600;
        localTestAds = this.n;
        localAdRequest.n = localTestAds;
        if (fieldSetFlags()[14] == 0)
          break label619;
        localMap = this.o;
        label324: localAdRequest.o = localMap;
        if (fieldSetFlags()[15] == 0)
          break label638;
      }
      label371: label505: label638: boolean bool2;
      for (boolean bool3 = this.p; ; bool3 = bool2)
      {
        localAdRequest.p = bool3;
        return localAdRequest;
        localCharSequence1 = (CharSequence)defaultValue(fields()[0]);
        break;
        localCharSequence2 = (CharSequence)defaultValue(fields()[1]);
        break label42;
        label389: localCharSequence3 = (CharSequence)defaultValue(fields()[2]);
        break label63;
        label407: l1 = ((Long)defaultValue(fields()[3])).longValue();
        break label84;
        label428: localList1 = (List)defaultValue(fields()[4]);
        break label105;
        label446: localLocation = (Location)defaultValue(fields()[5]);
        break label126;
        label464: bool1 = ((Boolean)defaultValue(fields()[6])).booleanValue();
        break label148;
        label486: localList2 = (List)defaultValue(fields()[7]);
        break label170;
        localAdViewContainer = (AdViewContainer)defaultValue(fields()[8]);
        break label192;
        label524: localCharSequence4 = (CharSequence)defaultValue(fields()[9]);
        break label214;
        label543: localCharSequence5 = (CharSequence)defaultValue(fields()[10]);
        break label236;
        label562: localCharSequence6 = (CharSequence)defaultValue(fields()[11]);
        break label258;
        label581: localCharSequence7 = (CharSequence)defaultValue(fields()[12]);
        break label280;
        label600: localTestAds = (TestAds)defaultValue(fields()[13]);
        break label302;
        label619: localMap = (Map)defaultValue(fields()[14]);
        break label324;
        bool2 = ((Boolean)defaultValue(fields()[15])).booleanValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdReportedIds()
  {
    this.e = null;
    fieldSetFlags()[4] = 0;
    return this;
  }

  public Builder clearAdSpaceName()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearAdViewContainer()
  {
    this.i = null;
    fieldSetFlags()[8] = 0;
    return this;
  }

  public Builder clearAgentVersion()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearApiKey()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearBindings()
  {
    this.h = null;
    fieldSetFlags()[7] = 0;
    return this;
  }

  public Builder clearDevicePlatform()
  {
    this.m = null;
    fieldSetFlags()[12] = 0;
    return this;
  }

  public Builder clearKeywords()
  {
    this.o = null;
    fieldSetFlags()[14] = 0;
    return this;
  }

  public Builder clearLocale()
  {
    this.j = null;
    fieldSetFlags()[9] = 0;
    return this;
  }

  public Builder clearLocation()
  {
    this.f = null;
    fieldSetFlags()[5] = 0;
    return this;
  }

  public Builder clearOsVersion()
  {
    this.l = null;
    fieldSetFlags()[11] = 0;
    return this;
  }

  public Builder clearRefresh()
  {
    fieldSetFlags()[15] = 0;
    return this;
  }

  public Builder clearSessionId()
  {
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Builder clearTestAds()
  {
    this.n = null;
    fieldSetFlags()[13] = 0;
    return this;
  }

  public Builder clearTestDevice()
  {
    fieldSetFlags()[6] = 0;
    return this;
  }

  public Builder clearTimezone()
  {
    this.k = null;
    fieldSetFlags()[10] = 0;
    return this;
  }

  public List<AdReportedId> getAdReportedIds()
  {
    return this.e;
  }

  public CharSequence getAdSpaceName()
  {
    return this.c;
  }

  public AdViewContainer getAdViewContainer()
  {
    return this.i;
  }

  public CharSequence getAgentVersion()
  {
    return this.b;
  }

  public CharSequence getApiKey()
  {
    return this.a;
  }

  public List<Integer> getBindings()
  {
    return this.h;
  }

  public CharSequence getDevicePlatform()
  {
    return this.m;
  }

  public Map<CharSequence, CharSequence> getKeywords()
  {
    return this.o;
  }

  public CharSequence getLocale()
  {
    return this.j;
  }

  public Location getLocation()
  {
    return this.f;
  }

  public CharSequence getOsVersion()
  {
    return this.l;
  }

  public Boolean getRefresh()
  {
    return Boolean.valueOf(this.p);
  }

  public Long getSessionId()
  {
    return Long.valueOf(this.d);
  }

  public TestAds getTestAds()
  {
    return this.n;
  }

  public Boolean getTestDevice()
  {
    return Boolean.valueOf(this.g);
  }

  public CharSequence getTimezone()
  {
    return this.k;
  }

  public boolean hasAdReportedIds()
  {
    return fieldSetFlags()[4];
  }

  public boolean hasAdSpaceName()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasAdViewContainer()
  {
    return fieldSetFlags()[8];
  }

  public boolean hasAgentVersion()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasApiKey()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasBindings()
  {
    return fieldSetFlags()[7];
  }

  public boolean hasDevicePlatform()
  {
    return fieldSetFlags()[12];
  }

  public boolean hasKeywords()
  {
    return fieldSetFlags()[14];
  }

  public boolean hasLocale()
  {
    return fieldSetFlags()[9];
  }

  public boolean hasLocation()
  {
    return fieldSetFlags()[5];
  }

  public boolean hasOsVersion()
  {
    return fieldSetFlags()[11];
  }

  public boolean hasRefresh()
  {
    return fieldSetFlags()[15];
  }

  public boolean hasSessionId()
  {
    return fieldSetFlags()[3];
  }

  public boolean hasTestAds()
  {
    return fieldSetFlags()[13];
  }

  public boolean hasTestDevice()
  {
    return fieldSetFlags()[6];
  }

  public boolean hasTimezone()
  {
    return fieldSetFlags()[10];
  }

  public Builder setAdReportedIds(List<AdReportedId> paramList)
  {
    validate(fields()[4], paramList);
    this.e = paramList;
    fieldSetFlags()[4] = 1;
    return this;
  }

  public Builder setAdSpaceName(CharSequence paramCharSequence)
  {
    validate(fields()[2], paramCharSequence);
    this.c = paramCharSequence;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setAdViewContainer(AdViewContainer paramAdViewContainer)
  {
    validate(fields()[8], paramAdViewContainer);
    this.i = paramAdViewContainer;
    fieldSetFlags()[8] = 1;
    return this;
  }

  public Builder setAgentVersion(CharSequence paramCharSequence)
  {
    validate(fields()[1], paramCharSequence);
    this.b = paramCharSequence;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setApiKey(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setBindings(List<Integer> paramList)
  {
    validate(fields()[7], paramList);
    this.h = paramList;
    fieldSetFlags()[7] = 1;
    return this;
  }

  public Builder setDevicePlatform(CharSequence paramCharSequence)
  {
    validate(fields()[12], paramCharSequence);
    this.m = paramCharSequence;
    fieldSetFlags()[12] = 1;
    return this;
  }

  public Builder setKeywords(Map<CharSequence, CharSequence> paramMap)
  {
    validate(fields()[14], paramMap);
    this.o = paramMap;
    fieldSetFlags()[14] = 1;
    return this;
  }

  public Builder setLocale(CharSequence paramCharSequence)
  {
    validate(fields()[9], paramCharSequence);
    this.j = paramCharSequence;
    fieldSetFlags()[9] = 1;
    return this;
  }

  public Builder setLocation(Location paramLocation)
  {
    validate(fields()[5], paramLocation);
    this.f = paramLocation;
    fieldSetFlags()[5] = 1;
    return this;
  }

  public Builder setOsVersion(CharSequence paramCharSequence)
  {
    validate(fields()[11], paramCharSequence);
    this.l = paramCharSequence;
    fieldSetFlags()[11] = 1;
    return this;
  }

  public Builder setRefresh(boolean paramBoolean)
  {
    validate(fields()[15], Boolean.valueOf(paramBoolean));
    this.p = paramBoolean;
    fieldSetFlags()[15] = 1;
    return this;
  }

  public Builder setSessionId(long paramLong)
  {
    validate(fields()[3], Long.valueOf(paramLong));
    this.d = paramLong;
    fieldSetFlags()[3] = 1;
    return this;
  }

  public Builder setTestAds(TestAds paramTestAds)
  {
    validate(fields()[13], paramTestAds);
    this.n = paramTestAds;
    fieldSetFlags()[13] = 1;
    return this;
  }

  public Builder setTestDevice(boolean paramBoolean)
  {
    validate(fields()[6], Boolean.valueOf(paramBoolean));
    this.g = paramBoolean;
    fieldSetFlags()[6] = 1;
    return this;
  }

  public Builder setTimezone(CharSequence paramCharSequence)
  {
    validate(fields()[10], paramCharSequence);
    this.k = paramCharSequence;
    fieldSetFlags()[10] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdRequest.Builder
 * JD-Core Version:    0.6.2
 */