package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdResponse$Builder extends SpecificRecordBuilderBase<AdResponse>
  implements RecordBuilder<AdResponse>
{
  private List<AdUnit> a;
  private List<CharSequence> b;

  private AdResponse$Builder()
  {
    super(AdResponse.SCHEMA$);
  }

  public AdResponse build()
  {
    try
    {
      AdResponse localAdResponse = new AdResponse();
      List localList1;
      if (fieldSetFlags()[0] != 0)
      {
        localList1 = this.a;
        localAdResponse.a = localList1;
        if (fieldSetFlags()[1] == 0)
          break label67;
      }
      label67: for (List localList2 = this.b; ; localList2 = (List)defaultValue(fields()[1]))
      {
        localAdResponse.b = localList2;
        return localAdResponse;
        localList1 = (List)defaultValue(fields()[0]);
        break;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdUnits()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearErrors()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public List<AdUnit> getAdUnits()
  {
    return this.a;
  }

  public List<CharSequence> getErrors()
  {
    return this.b;
  }

  public boolean hasAdUnits()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasErrors()
  {
    return fieldSetFlags()[1];
  }

  public Builder setAdUnits(List<AdUnit> paramList)
  {
    validate(fields()[0], paramList);
    this.a = paramList;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setErrors(List<CharSequence> paramList)
  {
    validate(fields()[1], paramList);
    this.b = paramList;
    fieldSetFlags()[1] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdResponse.Builder
 * JD-Core Version:    0.6.2
 */