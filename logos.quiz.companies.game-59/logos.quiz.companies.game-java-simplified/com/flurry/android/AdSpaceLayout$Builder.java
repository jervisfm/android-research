package com.flurry.android;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdSpaceLayout$Builder extends SpecificRecordBuilderBase<AdSpaceLayout>
  implements RecordBuilder<AdSpaceLayout>
{
  private int a;
  private int b;
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;

  private AdSpaceLayout$Builder()
  {
    super(AdSpaceLayout.SCHEMA$);
  }

  public AdSpaceLayout build()
  {
    try
    {
      AdSpaceLayout localAdSpaceLayout = new AdSpaceLayout();
      int i;
      int j;
      label42: CharSequence localCharSequence1;
      label63: CharSequence localCharSequence2;
      if (fieldSetFlags()[0] != 0)
      {
        i = this.a;
        localAdSpaceLayout.a = i;
        if (fieldSetFlags()[1] == 0)
          break label133;
        j = this.b;
        localAdSpaceLayout.b = j;
        if (fieldSetFlags()[2] == 0)
          break label154;
        localCharSequence1 = this.c;
        localAdSpaceLayout.c = localCharSequence1;
        if (fieldSetFlags()[3] == 0)
          break label172;
        localCharSequence2 = this.d;
        label84: localAdSpaceLayout.d = localCharSequence2;
        if (fieldSetFlags()[4] == 0)
          break label190;
      }
      label133: label154: label172: label190: for (CharSequence localCharSequence3 = this.e; ; localCharSequence3 = (CharSequence)defaultValue(fields()[4]))
      {
        localAdSpaceLayout.e = localCharSequence3;
        return localAdSpaceLayout;
        i = ((Integer)defaultValue(fields()[0])).intValue();
        break;
        j = ((Integer)defaultValue(fields()[1])).intValue();
        break label42;
        localCharSequence1 = (CharSequence)defaultValue(fields()[2]);
        break label63;
        localCharSequence2 = (CharSequence)defaultValue(fields()[3]);
        break label84;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdHeight()
  {
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearAdWidth()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearAlignment()
  {
    this.e = null;
    fieldSetFlags()[4] = 0;
    return this;
  }

  public Builder clearFix()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearFormat()
  {
    this.d = null;
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Integer getAdHeight()
  {
    return Integer.valueOf(this.b);
  }

  public Integer getAdWidth()
  {
    return Integer.valueOf(this.a);
  }

  public CharSequence getAlignment()
  {
    return this.e;
  }

  public CharSequence getFix()
  {
    return this.c;
  }

  public CharSequence getFormat()
  {
    return this.d;
  }

  public boolean hasAdHeight()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasAdWidth()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasAlignment()
  {
    return fieldSetFlags()[4];
  }

  public boolean hasFix()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasFormat()
  {
    return fieldSetFlags()[3];
  }

  public Builder setAdHeight(int paramInt)
  {
    validate(fields()[1], Integer.valueOf(paramInt));
    this.b = paramInt;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setAdWidth(int paramInt)
  {
    validate(fields()[0], Integer.valueOf(paramInt));
    this.a = paramInt;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setAlignment(CharSequence paramCharSequence)
  {
    validate(fields()[4], paramCharSequence);
    this.e = paramCharSequence;
    fieldSetFlags()[4] = 1;
    return this;
  }

  public Builder setFix(CharSequence paramCharSequence)
  {
    validate(fields()[2], paramCharSequence);
    this.c = paramCharSequence;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setFormat(CharSequence paramCharSequence)
  {
    validate(fields()[3], paramCharSequence);
    this.d = paramCharSequence;
    fieldSetFlags()[3] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdSpaceLayout.Builder
 * JD-Core Version:    0.6.2
 */