package com.flurry.android;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.specific.SpecificRecord;
import org.apache.avro.specific.SpecificRecordBase;

class AdSpaceLayout extends SpecificRecordBase
  implements SpecificRecord
{
  public static final Schema SCHEMA$ = new Schema.Parser().parse("{\"type\":\"record\",\"name\":\"AdSpaceLayout\",\"namespace\":\"com.flurry.android\",\"fields\":[{\"name\":\"adWidth\",\"type\":\"int\"},{\"name\":\"adHeight\",\"type\":\"int\"},{\"name\":\"fix\",\"type\":\"string\"},{\"name\":\"format\",\"type\":\"string\"},{\"name\":\"alignment\",\"type\":\"string\"}]}");
  public int a;
  public int b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;

  public final Integer a()
  {
    return Integer.valueOf(this.a);
  }

  public final Integer b()
  {
    return Integer.valueOf(this.b);
  }

  public final CharSequence c()
  {
    return this.c;
  }

  public final CharSequence d()
  {
    return this.d;
  }

  public final CharSequence e()
  {
    return this.e;
  }

  public Object get(int paramInt)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      return Integer.valueOf(this.a);
    case 1:
      return Integer.valueOf(this.b);
    case 2:
      return this.c;
    case 3:
      return this.d;
    case 4:
    }
    return this.e;
  }

  public Schema getSchema()
  {
    return SCHEMA$;
  }

  public void put(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      this.a = ((Integer)paramObject).intValue();
      return;
    case 1:
      this.b = ((Integer)paramObject).intValue();
      return;
    case 2:
      this.c = ((CharSequence)paramObject);
      return;
    case 3:
      this.d = ((CharSequence)paramObject);
      return;
    case 4:
    }
    this.e = ((CharSequence)paramObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdSpaceLayout
 * JD-Core Version:    0.6.2
 */