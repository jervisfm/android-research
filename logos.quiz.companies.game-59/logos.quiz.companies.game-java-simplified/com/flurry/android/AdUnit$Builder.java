package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdUnit$Builder extends SpecificRecordBuilderBase<AdUnit>
  implements RecordBuilder<AdUnit>
{
  private CharSequence a;
  private long b;
  private List<AdFrame> c;
  private int d;

  private AdUnit$Builder()
  {
    super(AdUnit.SCHEMA$);
  }

  public AdUnit build()
  {
    try
    {
      AdUnit localAdUnit = new AdUnit();
      CharSequence localCharSequence;
      long l;
      label42: List localList;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence = this.a;
        localAdUnit.a = localCharSequence;
        if (fieldSetFlags()[1] == 0)
          break label109;
        l = this.b;
        localAdUnit.b = l;
        if (fieldSetFlags()[2] == 0)
          break label130;
        localList = this.c;
        label63: localAdUnit.c = localList;
        if (fieldSetFlags()[3] == 0)
          break label148;
      }
      label109: int i;
      for (int j = this.d; ; j = i)
      {
        localAdUnit.d = j;
        return localAdUnit;
        localCharSequence = (CharSequence)defaultValue(fields()[0]);
        break;
        l = ((Long)defaultValue(fields()[1])).longValue();
        break label42;
        label130: localList = (List)defaultValue(fields()[2]);
        break label63;
        label148: i = ((Integer)defaultValue(fields()[3])).intValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdFrames()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearAdSpace()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearCombinable()
  {
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Builder clearExpiration()
  {
    fieldSetFlags()[1] = 0;
    return this;
  }

  public List<AdFrame> getAdFrames()
  {
    return this.c;
  }

  public CharSequence getAdSpace()
  {
    return this.a;
  }

  public Integer getCombinable()
  {
    return Integer.valueOf(this.d);
  }

  public Long getExpiration()
  {
    return Long.valueOf(this.b);
  }

  public boolean hasAdFrames()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasAdSpace()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasCombinable()
  {
    return fieldSetFlags()[3];
  }

  public boolean hasExpiration()
  {
    return fieldSetFlags()[1];
  }

  public Builder setAdFrames(List<AdFrame> paramList)
  {
    validate(fields()[2], paramList);
    this.c = paramList;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setAdSpace(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setCombinable(int paramInt)
  {
    validate(fields()[3], Integer.valueOf(paramInt));
    this.d = paramInt;
    fieldSetFlags()[3] = 1;
    return this;
  }

  public Builder setExpiration(long paramLong)
  {
    validate(fields()[1], Long.valueOf(paramLong));
    this.b = paramLong;
    fieldSetFlags()[1] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdUnit.Builder
 * JD-Core Version:    0.6.2
 */