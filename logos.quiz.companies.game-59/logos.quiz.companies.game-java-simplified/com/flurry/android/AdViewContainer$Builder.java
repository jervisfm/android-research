package com.flurry.android;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class AdViewContainer$Builder extends SpecificRecordBuilderBase<AdViewContainer>
  implements RecordBuilder<AdViewContainer>
{
  private int a;
  private int b;
  private int c;
  private int d;

  private AdViewContainer$Builder(byte paramByte)
  {
    super(AdViewContainer.SCHEMA$);
  }

  public AdViewContainer build()
  {
    try
    {
      AdViewContainer localAdViewContainer = new AdViewContainer();
      int i;
      int j;
      label42: int k;
      if (fieldSetFlags()[0] != 0)
      {
        i = this.a;
        localAdViewContainer.a = i;
        if (fieldSetFlags()[1] == 0)
          break label112;
        j = this.b;
        localAdViewContainer.b = j;
        if (fieldSetFlags()[2] == 0)
          break label133;
        k = this.c;
        label63: localAdViewContainer.c = k;
        if (fieldSetFlags()[3] == 0)
          break label154;
      }
      label112: int m;
      for (int n = this.d; ; n = m)
      {
        localAdViewContainer.d = n;
        return localAdViewContainer;
        i = ((Integer)defaultValue(fields()[0])).intValue();
        break;
        j = ((Integer)defaultValue(fields()[1])).intValue();
        break label42;
        label133: k = ((Integer)defaultValue(fields()[2])).intValue();
        break label63;
        label154: m = ((Integer)defaultValue(fields()[3])).intValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearScreenHeight()
  {
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Builder clearScreenWidth()
  {
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearViewHeight()
  {
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearViewWidth()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Integer getScreenHeight()
  {
    return Integer.valueOf(this.d);
  }

  public Integer getScreenWidth()
  {
    return Integer.valueOf(this.c);
  }

  public Integer getViewHeight()
  {
    return Integer.valueOf(this.b);
  }

  public Integer getViewWidth()
  {
    return Integer.valueOf(this.a);
  }

  public boolean hasScreenHeight()
  {
    return fieldSetFlags()[3];
  }

  public boolean hasScreenWidth()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasViewHeight()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasViewWidth()
  {
    return fieldSetFlags()[0];
  }

  public Builder setScreenHeight(int paramInt)
  {
    validate(fields()[3], Integer.valueOf(paramInt));
    this.d = paramInt;
    fieldSetFlags()[3] = 1;
    return this;
  }

  public Builder setScreenWidth(int paramInt)
  {
    validate(fields()[2], Integer.valueOf(paramInt));
    this.c = paramInt;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setViewHeight(int paramInt)
  {
    validate(fields()[1], Integer.valueOf(paramInt));
    this.b = paramInt;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setViewWidth(int paramInt)
  {
    validate(fields()[0], Integer.valueOf(paramInt));
    this.a = paramInt;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.AdViewContainer.Builder
 * JD-Core Version:    0.6.2
 */