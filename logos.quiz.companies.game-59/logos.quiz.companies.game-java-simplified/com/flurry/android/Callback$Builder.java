package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class Callback$Builder extends SpecificRecordBuilderBase<Callback>
  implements RecordBuilder<Callback>
{
  private CharSequence a;
  private List<CharSequence> b;

  private Callback$Builder()
  {
    super(Callback.SCHEMA$);
  }

  public Callback build()
  {
    try
    {
      Callback localCallback = new Callback();
      CharSequence localCharSequence;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence = this.a;
        localCallback.a = localCharSequence;
        if (fieldSetFlags()[1] == 0)
          break label67;
      }
      label67: for (List localList = this.b; ; localList = (List)defaultValue(fields()[1]))
      {
        localCallback.b = localList;
        return localCallback;
        localCharSequence = (CharSequence)defaultValue(fields()[0]);
        break;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearActions()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearEvent()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public List<CharSequence> getActions()
  {
    return this.b;
  }

  public CharSequence getEvent()
  {
    return this.a;
  }

  public boolean hasActions()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasEvent()
  {
    return fieldSetFlags()[0];
  }

  public Builder setActions(List<CharSequence> paramList)
  {
    validate(fields()[1], paramList);
    this.b = paramList;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setEvent(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.Callback.Builder
 * JD-Core Version:    0.6.2
 */