package com.flurry.android;

public enum FlurryAdSize
{
  private int a;

  static
  {
    BANNER_BOTTOM = new FlurryAdSize("BANNER_BOTTOM", 1, 2);
    FULLSCREEN = new FlurryAdSize("FULLSCREEN", 2, 3);
    CATALOG = new FlurryAdSize("CATALOG", 3, 4);
    FlurryAdSize[] arrayOfFlurryAdSize = new FlurryAdSize[4];
    arrayOfFlurryAdSize[0] = BANNER_TOP;
    arrayOfFlurryAdSize[1] = BANNER_BOTTOM;
    arrayOfFlurryAdSize[2] = FULLSCREEN;
    arrayOfFlurryAdSize[3] = CATALOG;
  }

  private FlurryAdSize(int paramInt)
  {
    this.a = paramInt;
  }

  final int a()
  {
    return this.a;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.FlurryAdSize
 * JD-Core Version:    0.6.2
 */