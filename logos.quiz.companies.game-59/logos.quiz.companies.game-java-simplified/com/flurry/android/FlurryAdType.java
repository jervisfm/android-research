package com.flurry.android;

public enum FlurryAdType
{
  static
  {
    VIDEO_TAKEOVER = new FlurryAdType("VIDEO_TAKEOVER", 2);
    FlurryAdType[] arrayOfFlurryAdType = new FlurryAdType[3];
    arrayOfFlurryAdType[0] = WEB_BANNER;
    arrayOfFlurryAdType[1] = WEB_TAKEOVER;
    arrayOfFlurryAdType[2] = VIDEO_TAKEOVER;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.FlurryAdType
 * JD-Core Version:    0.6.2
 */