package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ViewGroup;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class FlurryAgent
  implements LocationListener
{
  private static final String[] a = { "9774d56d682e549c", "dead00beef" };
  private static volatile String b = null;
  private static volatile String c = "http://data.flurry.com/aap.do";
  private static volatile String d = "https://data.flurry.com/aap.do";
  private static final FlurryAgent e = new FlurryAgent();
  private static long f = 10000L;
  private static boolean g = true;
  private static boolean h = false;
  private static boolean i = false;
  private static boolean j = true;
  private static Criteria k = null;
  private static boolean l = false;
  private static AtomicInteger n = new AtomicInteger(0);
  private static AtomicInteger o = new AtomicInteger(0);
  private List<byte[]> A;
  private LocationManager B;
  private String C;
  private Map<Integer, ByteBuffer> D = new HashMap();
  private boolean E;
  private long F;
  private List<byte[]> G = new ArrayList();
  private long H;
  private long I;
  private long J;
  private String K = "";
  private String L = "";
  private byte M = -1;
  private String N = "";
  private byte O = -1;
  private Long P;
  private int Q;
  private Location R;
  private Map<String, l> S = new HashMap();
  private List<al> T = new ArrayList();
  private boolean U;
  private int V;
  private List<av> W = new ArrayList();
  private int X;
  private Map<String, List<String>> Y;
  private bf Z = new bf();
  private ay aa = new ay();
  private ae ab = new ae();
  private final Handler m;
  private File p;
  private File q = null;
  private File r = null;
  private volatile boolean s = false;
  private volatile boolean t = false;
  private long u;
  private Map<Context, Context> v = new WeakHashMap();
  private String w;
  private String x;
  private String y;
  private boolean z = true;

  private FlurryAgent()
  {
    HandlerThread localHandlerThread = new HandlerThread("FlurryAgent");
    localHandlerThread.start();
    this.m = new Handler(localHandlerThread.getLooper());
  }

  private static double a(double paramDouble)
  {
    return Math.round(paramDouble * 1000.0D) / 1000.0D;
  }

  static bf a()
  {
    return e.Z;
  }

  // ERROR //
  private static String a(File paramFile)
  {
    // Byte code:
    //   0: new 227	java/io/FileInputStream
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 230	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   8: astore_1
    //   9: new 232	java/lang/StringBuffer
    //   12: dup
    //   13: invokespecial 233	java/lang/StringBuffer:<init>	()V
    //   16: astore_2
    //   17: sipush 1024
    //   20: newarray byte
    //   22: astore 7
    //   24: aload_1
    //   25: aload 7
    //   27: invokevirtual 237	java/io/FileInputStream:read	([B)I
    //   30: istore 8
    //   32: iload 8
    //   34: ifle +55 -> 89
    //   37: aload_2
    //   38: new 86	java/lang/String
    //   41: dup
    //   42: aload 7
    //   44: iconst_0
    //   45: iload 8
    //   47: invokespecial 240	java/lang/String:<init>	([BII)V
    //   50: invokevirtual 244	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   53: pop
    //   54: goto -30 -> 24
    //   57: astore 4
    //   59: ldc 195
    //   61: ldc 246
    //   63: aload 4
    //   65: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   68: pop
    //   69: aload_1
    //   70: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   73: aconst_null
    //   74: astore 6
    //   76: aload_2
    //   77: ifnull +9 -> 86
    //   80: aload_2
    //   81: invokevirtual 260	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   84: astore 6
    //   86: aload 6
    //   88: areturn
    //   89: aload_1
    //   90: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   93: goto -20 -> 73
    //   96: astore 10
    //   98: aconst_null
    //   99: astore_1
    //   100: aload 10
    //   102: astore_3
    //   103: aload_1
    //   104: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   107: aload_3
    //   108: athrow
    //   109: astore_3
    //   110: goto -7 -> 103
    //   113: astore 4
    //   115: aconst_null
    //   116: astore_2
    //   117: aconst_null
    //   118: astore_1
    //   119: goto -60 -> 59
    //   122: astore 4
    //   124: aconst_null
    //   125: astore_2
    //   126: goto -67 -> 59
    //
    // Exception table:
    //   from	to	target	type
    //   17	24	57	java/lang/Throwable
    //   24	32	57	java/lang/Throwable
    //   37	54	57	java/lang/Throwable
    //   0	9	96	finally
    //   9	17	109	finally
    //   17	24	109	finally
    //   24	32	109	finally
    //   37	54	109	finally
    //   59	69	109	finally
    //   0	9	113	java/lang/Throwable
    //   9	17	122	java/lang/Throwable
  }

  private void a(Context paramContext)
  {
    if (!l)
    {
      if (!this.Z.h())
      {
        bd.a("FlurryAgent", "Initializing AppSpot");
        o localo = new o();
        localo.a = this.w;
        localo.b = this.aa;
        localo.c = this.ab;
        localo.d = this.K;
        localo.e = this.L;
        this.Z.a(paramContext, localo);
        this.Z.m();
        bd.a("FlurryAgent", "AppSpot initialized");
      }
      this.Z.a(paramContext, this.H, this.I);
      l = true;
    }
  }

  private void a(Context paramContext, String paramString)
  {
    try
    {
      if ((this.w != null) && (!this.w.equals(paramString)))
        bd.b("FlurryAgent", "onStartSession called with different api keys: " + this.w + " and " + paramString);
      if ((Context)this.v.put(paramContext, paramContext) != null)
        bd.d("FlurryAgent", "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
      if (!this.s)
      {
        bd.a("FlurryAgent", "Initializing Flurry session");
        n.set(0);
        o.set(0);
        this.w = paramString;
        this.q = paramContext.getFileStreamPath(".flurryagent." + Integer.toString(this.w.hashCode(), 16));
        this.p = paramContext.getFileStreamPath(".flurryb.");
        this.r = paramContext.getFileStreamPath(".flurryinstallreceiver." + Integer.toString(e.w.hashCode(), 16));
        if (j)
          Thread.setDefaultUncaughtExceptionHandler(new FlurryAgent.FlurryDefaultExceptionHandler());
        Context localContext = paramContext.getApplicationContext();
        if (this.y == null)
          this.y = d(localContext);
        String str = localContext.getPackageName();
        if ((this.x != null) && (!this.x.equals(str)))
          bd.b("FlurryAgent", "onStartSession called from different application packages: " + this.x + " and " + str);
        this.x = str;
        long l1 = SystemClock.elapsedRealtime();
        if (l1 - this.u <= f)
          break label529;
        bd.a("FlurryAgent", "New session");
        this.H = System.currentTimeMillis();
        this.I = l1;
        this.J = -1L;
        this.N = "";
        this.Q = 0;
        this.R = null;
        this.L = TimeZone.getDefault().getID();
        this.K = (Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry());
        this.S = new HashMap();
        this.T = new ArrayList();
        this.U = true;
        this.W = new ArrayList();
        this.V = 0;
        this.X = 0;
        if (l)
          this.Z.a(paramContext, this.H, this.I);
        a(new e(this, localContext, this.z));
      }
      while (true)
      {
        this.s = true;
        return;
        label529: bd.a("FlurryAgent", "Continuing previous session");
        if (!this.G.isEmpty())
          this.G.remove(-1 + this.G.size());
        if (l)
          this.Z.a(paramContext);
      }
    }
    finally
    {
    }
  }

  private void a(Context paramContext, boolean paramBoolean)
  {
    if (paramContext != null);
    try
    {
      if ((Context)this.v.remove(paramContext) == null)
        bd.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
      if ((this.s) && (this.v.isEmpty()))
      {
        bd.a("FlurryAgent", "Ending session");
        m();
        if (paramContext != null)
          break label207;
      }
      label207: Context localContext;
      for (Object localObject2 = null; ; localObject2 = localContext)
      {
        if (paramContext != null)
        {
          String str = ((Context)localObject2).getPackageName();
          if (!this.x.equals(str))
            bd.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.x + " actual: " + str);
        }
        long l1 = SystemClock.elapsedRealtime();
        this.u = l1;
        this.J = (l1 - this.I);
        if (n() == null)
          bd.b("FlurryAgent", "Not creating report because of bad Android ID or generated ID is null");
        a(new a(this, paramBoolean, (Context)localObject2));
        if (l)
          this.Z.a();
        this.s = false;
        return;
        localContext = paramContext.getApplicationContext();
      }
    }
    finally
    {
    }
  }

  private void a(DataInputStream paramDataInputStream)
  {
    int i1 = 0;
    int i2;
    try
    {
      i2 = paramDataInputStream.readUnsignedShort();
      if (i2 > 2)
      {
        bd.b("FlurryAgent", "Unknown agent file version: " + i2);
        throw new IOException("Unknown agent file version: " + i2);
      }
    }
    finally
    {
    }
    String str1;
    if (i2 >= 2)
    {
      str1 = paramDataInputStream.readUTF();
      bd.a("FlurryAgent", "Loading API key: " + c(this.w));
      if (str1.equals(this.w))
      {
        String str2 = paramDataInputStream.readUTF();
        if (n() == null)
          bd.a("FlurryAgent", "Loading phoneId: " + str2);
        d(str2);
        this.E = paramDataInputStream.readBoolean();
        this.F = paramDataInputStream.readLong();
        bd.a("FlurryAgent", "Loading session reports");
        while (true)
        {
          int i3 = paramDataInputStream.readUnsignedShort();
          if (i3 == 0)
            break;
          byte[] arrayOfByte = new byte[i3];
          paramDataInputStream.readFully(arrayOfByte);
          this.G.add(0, arrayOfByte);
          StringBuilder localStringBuilder = new StringBuilder().append("Session report added: ");
          i1++;
          bd.a("FlurryAgent", i1);
        }
        bd.a("FlurryAgent", "Persistent file loaded");
        this.t = true;
      }
    }
    while (true)
    {
      return;
      bd.a("FlurryAgent", "Api keys do not match, old: " + c(str1) + ", new: " + c(this.w));
      continue;
      bd.d("FlurryAgent", "Deleting old file version: " + i2);
    }
  }

  private void a(Runnable paramRunnable)
  {
    this.m.post(paramRunnable);
  }

  private void a(String paramString)
  {
    try
    {
      Iterator localIterator = this.T.iterator();
      while (localIterator.hasNext())
      {
        al localal = (al)localIterator.next();
        if (localal.a(paramString))
          localal.a(SystemClock.elapsedRealtime() - this.I);
      }
      return;
    }
    finally
    {
    }
  }

  private void a(String paramString1, String paramString2, String paramString3)
  {
    label292: 
    while (true)
    {
      try
      {
        if (this.W == null)
        {
          bd.b("FlurryAgent", "onError called before onStartSession.  Error: " + paramString1);
          return;
        }
        if ((paramString1 != null) && ("uncaught".equals(paramString1)))
        {
          i1 = 1;
          this.Q = (1 + this.Q);
          if (this.W.size() >= 50)
            break label168;
          Long localLong2 = Long.valueOf(System.currentTimeMillis());
          av localav3 = new av(o.incrementAndGet(), localLong2.longValue(), paramString1, paramString2, paramString3);
          this.W.add(localav3);
          bd.a("FlurryAgent", "Error logged: " + localav3.b());
          continue;
        }
      }
      finally
      {
      }
      int i1 = 0;
      continue;
      label168: if (i1 != 0);
      for (int i2 = 0; ; i2++)
      {
        if (i2 >= this.W.size())
          break label292;
        av localav1 = (av)this.W.get(i2);
        if ((localav1.b() != null) && (!"uncaught".equals(localav1.b())))
        {
          Long localLong1 = Long.valueOf(System.currentTimeMillis());
          av localav2 = new av(o.incrementAndGet(), localLong1.longValue(), paramString1, paramString2, paramString3);
          this.W.set(i2, localav2);
          break;
          bd.a("FlurryAgent", "Max errors logged. No more errors logged.");
          break;
        }
      }
    }
  }

  private void a(String paramString, Map<String, String> paramMap, boolean paramBoolean)
  {
    while (true)
    {
      long l1;
      String str;
      l locall1;
      Object localObject2;
      try
      {
        if (this.T == null)
        {
          bd.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + paramString);
          return;
        }
        l1 = SystemClock.elapsedRealtime() - this.I;
        str = z.a(paramString);
        if (str.length() == 0)
          continue;
        locall1 = (l)this.S.get(str);
        if (locall1 != null)
          break label293;
        if (this.S.size() < 100)
        {
          l locall2 = new l();
          locall2.a = 1;
          this.S.put(str, locall2);
          bd.a("FlurryAgent", "Event count incremented: " + str);
          if ((!g) || (this.T.size() >= 200) || (this.V >= 16000))
            break label455;
          if (paramMap != null)
            break label463;
          localObject2 = Collections.emptyMap();
          if (((Map)localObject2).size() <= 10)
            break label335;
          if (!bd.a("FlurryAgent"))
            continue;
          bd.a("FlurryAgent", "MaxEventParams exceeded: " + ((Map)localObject2).size());
          continue;
        }
      }
      finally
      {
      }
      if (bd.a("FlurryAgent"))
      {
        bd.a("FlurryAgent", "Too many different events. Event not counted: " + str);
        continue;
        label293: locall1.a = (1 + locall1.a);
        bd.a("FlurryAgent", "Event count incremented: " + str);
        continue;
        label335: al localal = new al(n.incrementAndGet(), str, (Map)localObject2, l1, paramBoolean);
        if (localal.a().length + this.V <= 16000)
        {
          this.T.add(localal);
          this.V += localal.a().length;
          bd.a("FlurryAgent", "Logged event: " + str);
        }
        else
        {
          this.V = 16000;
          this.U = false;
          bd.a("FlurryAgent", "Event Log size exceeded. No more event details logged.");
          continue;
          label455: this.U = false;
          continue;
          label463: localObject2 = paramMap;
        }
      }
    }
  }

  private boolean a(byte[] paramArrayOfByte)
  {
    String str1 = i();
    boolean bool1;
    if (str1 == null)
      bool1 = false;
    while (true)
    {
      return bool1;
      try
      {
        boolean bool3 = a(paramArrayOfByte, str1);
        bool1 = bool3;
        if ((bool1) || (b != null) || (!h) || (i));
      }
      catch (Exception localException1)
      {
        String str2;
        synchronized (e)
        {
          while (true)
          {
            i = true;
            str2 = i();
            if (str2 != null)
              break;
            return false;
            localException1 = localException1;
            bd.a("FlurryAgent", "Sending report exception: " + localException1.getMessage());
            bool1 = false;
          }
        }
        try
        {
          boolean bool2 = a(paramArrayOfByte, str2);
          return bool2;
          localObject = finally;
          throw localObject;
        }
        catch (Exception localException2)
        {
        }
      }
    }
    return bool1;
  }

  private boolean a(byte[] paramArrayOfByte, String paramString)
  {
    boolean bool = true;
    if ("local".equals(paramString))
      return bool;
    bd.a("FlurryAgent", "Sending report to: " + paramString);
    ByteArrayEntity localByteArrayEntity = new ByteArrayEntity(paramArrayOfByte);
    localByteArrayEntity.setContentType("application/octet-stream");
    HttpPost localHttpPost = new HttpPost(paramString);
    localHttpPost.setEntity(localByteArrayEntity);
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 10000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, 15000);
    localHttpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
    int i1 = this.aa.a(localBasicHttpParams).execute(localHttpPost).getStatusLine().getStatusCode();
    if (i1 == 200);
    while (true)
    {
      try
      {
        bd.a("FlurryAgent", "Report successful");
        this.E = true;
        this.G.removeAll(this.A);
        this.A = null;
        return bool;
      }
      finally
      {
      }
      bd.a("FlurryAgent", "Report failed. HTTP response: " + i1);
      bool = false;
    }
  }

  static String b()
  {
    return e.w;
  }

  private static Map<String, List<String>> b(String paramString)
  {
    HashMap localHashMap = new HashMap();
    String[] arrayOfString1 = paramString.split("&");
    int i1 = arrayOfString1.length;
    int i2 = 0;
    if (i2 < i1)
    {
      String[] arrayOfString2 = arrayOfString1[i2].split("=");
      if (arrayOfString2.length != 2)
        bd.a("FlurryAgent", "Invalid referrer Element: " + arrayOfString1[i2] + " in referrer tag " + paramString);
      while (true)
      {
        i2++;
        break;
        String str1 = URLDecoder.decode(arrayOfString2[0]);
        String str2 = URLDecoder.decode(arrayOfString2[1]);
        if (localHashMap.get(str1) == null)
          localHashMap.put(str1, new ArrayList());
        ((List)localHashMap.get(str1)).add(str2);
      }
    }
    StringBuilder localStringBuilder = new StringBuilder();
    if (localHashMap.get("utm_source") == null)
      localStringBuilder.append("Campaign Source is missing.\n");
    if (localHashMap.get("utm_medium") == null)
      localStringBuilder.append("Campaign Medium is missing.\n");
    if (localHashMap.get("utm_campaign") == null)
      localStringBuilder.append("Campaign Name is missing.\n");
    if (localStringBuilder.length() > 0)
      Log.w("Detected missing referrer keys", localStringBuilder.toString());
    return localHashMap;
  }

  // ERROR //
  private void b(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial 788	com/flurry/android/FlurryAgent:c	(Landroid/content/Context;)Ljava/lang/String;
    //   7: astore_3
    //   8: aload_0
    //   9: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   12: invokevirtual 793	java/io/File:exists	()Z
    //   15: ifeq +337 -> 352
    //   18: ldc 195
    //   20: new 304	java/lang/StringBuilder
    //   23: dup
    //   24: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   27: ldc_w 795
    //   30: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: aload_0
    //   34: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   37: invokevirtual 798	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   40: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   46: invokestatic 800	com/flurry/android/bd:c	(Ljava/lang/String;Ljava/lang/String;)I
    //   49: pop
    //   50: new 509	java/io/DataInputStream
    //   53: dup
    //   54: new 227	java/io/FileInputStream
    //   57: dup
    //   58: aload_0
    //   59: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   62: invokespecial 230	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   65: invokespecial 803	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   68: astore 10
    //   70: aload 10
    //   72: invokevirtual 512	java/io/DataInputStream:readUnsignedShort	()I
    //   75: ldc_w 804
    //   78: if_icmpne +197 -> 275
    //   81: aload_0
    //   82: aload 10
    //   84: invokespecial 806	com/flurry/android/FlurryAgent:a	(Ljava/io/DataInputStream;)V
    //   87: aload 10
    //   89: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   92: aload_0
    //   93: getfield 141	com/flurry/android/FlurryAgent:t	Z
    //   96: ifne +22 -> 118
    //   99: aload_0
    //   100: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   103: invokevirtual 809	java/io/File:delete	()Z
    //   106: ifeq +219 -> 325
    //   109: ldc 195
    //   111: ldc_w 811
    //   114: invokestatic 269	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   117: pop
    //   118: aload_0
    //   119: getfield 141	com/flurry/android/FlurryAgent:t	Z
    //   122: ifne +21 -> 143
    //   125: aload_0
    //   126: iconst_0
    //   127: putfield 537	com/flurry/android/FlurryAgent:E	Z
    //   130: aload_0
    //   131: aload_0
    //   132: getfield 292	com/flurry/android/FlurryAgent:H	J
    //   135: putfield 542	com/flurry/android/FlurryAgent:F	J
    //   138: aload_0
    //   139: iconst_1
    //   140: putfield 141	com/flurry/android/FlurryAgent:t	Z
    //   143: aload_3
    //   144: ifnonnull +233 -> 377
    //   147: invokestatic 815	java/lang/Math:random	()D
    //   150: invokestatic 820	java/lang/Double:doubleToLongBits	(D)J
    //   153: ldc2_w 821
    //   156: invokestatic 825	java/lang/System:nanoTime	()J
    //   159: bipush 37
    //   161: aload_0
    //   162: getfield 274	com/flurry/android/FlurryAgent:w	Ljava/lang/String;
    //   165: invokevirtual 338	java/lang/String:hashCode	()I
    //   168: imul
    //   169: i2l
    //   170: ladd
    //   171: lmul
    //   172: ladd
    //   173: lstore 5
    //   175: new 304	java/lang/StringBuilder
    //   178: dup
    //   179: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   182: ldc_w 827
    //   185: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: lload 5
    //   190: bipush 16
    //   192: invokestatic 830	java/lang/Long:toString	(JI)Ljava/lang/String;
    //   195: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   201: astore 7
    //   203: ldc 195
    //   205: ldc_w 832
    //   208: invokestatic 800	com/flurry/android/bd:c	(Ljava/lang/String;Ljava/lang/String;)I
    //   211: pop
    //   212: aload_0
    //   213: aload 7
    //   215: invokespecial 532	com/flurry/android/FlurryAgent:d	(Ljava/lang/String;)V
    //   218: aload_0
    //   219: getfield 181	com/flurry/android/FlurryAgent:Z	Lcom/flurry/android/bf;
    //   222: aload_0
    //   223: getfield 834	com/flurry/android/FlurryAgent:C	Ljava/lang/String;
    //   226: invokevirtual 836	com/flurry/android/bf:a	(Ljava/lang/String;)V
    //   229: aload_0
    //   230: getfield 181	com/flurry/android/FlurryAgent:Z	Lcom/flurry/android/bf;
    //   233: aload_0
    //   234: getfield 153	com/flurry/android/FlurryAgent:D	Ljava/util/Map;
    //   237: invokevirtual 839	com/flurry/android/bf:a	(Ljava/util/Map;)V
    //   240: aload 7
    //   242: ldc_w 841
    //   245: invokevirtual 844	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   248: ifne +20 -> 268
    //   251: aload_0
    //   252: getfield 351	com/flurry/android/FlurryAgent:p	Ljava/io/File;
    //   255: invokevirtual 793	java/io/File:exists	()Z
    //   258: ifne +10 -> 268
    //   261: aload_0
    //   262: aload_1
    //   263: aload 7
    //   265: invokespecial 846	com/flurry/android/FlurryAgent:b	(Landroid/content/Context;Ljava/lang/String;)V
    //   268: aload_0
    //   269: invokespecial 848	com/flurry/android/FlurryAgent:k	()V
    //   272: aload_0
    //   273: monitorexit
    //   274: return
    //   275: ldc 195
    //   277: ldc_w 850
    //   280: invokestatic 269	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   283: pop
    //   284: goto -197 -> 87
    //   287: astore 12
    //   289: ldc 195
    //   291: ldc 246
    //   293: aload 12
    //   295: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   298: pop
    //   299: aload 10
    //   301: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   304: goto -212 -> 92
    //   307: astore_2
    //   308: aload_0
    //   309: monitorexit
    //   310: aload_2
    //   311: athrow
    //   312: astore 11
    //   314: aconst_null
    //   315: astore 10
    //   317: aload 10
    //   319: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   322: aload 11
    //   324: athrow
    //   325: ldc 195
    //   327: ldc_w 852
    //   330: invokestatic 315	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;)I
    //   333: pop
    //   334: goto -216 -> 118
    //   337: astore 14
    //   339: ldc 195
    //   341: ldc 160
    //   343: aload 14
    //   345: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   348: pop
    //   349: goto -231 -> 118
    //   352: ldc 195
    //   354: ldc_w 854
    //   357: invokestatic 800	com/flurry/android/bd:c	(Ljava/lang/String;Ljava/lang/String;)I
    //   360: pop
    //   361: goto -243 -> 118
    //   364: astore 11
    //   366: goto -49 -> 317
    //   369: astore 12
    //   371: aconst_null
    //   372: astore 10
    //   374: goto -85 -> 289
    //   377: aload_3
    //   378: astore 7
    //   380: goto -168 -> 212
    //
    // Exception table:
    //   from	to	target	type
    //   70	87	287	java/lang/Throwable
    //   275	284	287	java/lang/Throwable
    //   2	50	307	finally
    //   87	92	307	finally
    //   92	118	307	finally
    //   118	143	307	finally
    //   147	212	307	finally
    //   212	268	307	finally
    //   268	272	307	finally
    //   299	304	307	finally
    //   317	325	307	finally
    //   325	334	307	finally
    //   339	349	307	finally
    //   352	361	307	finally
    //   50	70	312	finally
    //   92	118	337	java/lang/Throwable
    //   325	334	337	java/lang/Throwable
    //   70	87	364	finally
    //   275	284	364	finally
    //   289	299	364	finally
    //   50	70	369	java/lang/Throwable
  }

  // ERROR //
  private void b(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: ldc_w 349
    //   7: invokevirtual 347	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   10: putfield 351	com/flurry/android/FlurryAgent:p	Ljava/io/File;
    //   13: aload_0
    //   14: getfield 351	com/flurry/android/FlurryAgent:p	Ljava/io/File;
    //   17: invokestatic 857	com/flurry/android/ae:a	(Ljava/io/File;)Z
    //   20: istore 4
    //   22: iload 4
    //   24: ifne +6 -> 30
    //   27: aload_0
    //   28: monitorexit
    //   29: return
    //   30: new 859	java/io/DataOutputStream
    //   33: dup
    //   34: new 861	java/io/FileOutputStream
    //   37: dup
    //   38: aload_0
    //   39: getfield 351	com/flurry/android/FlurryAgent:p	Ljava/io/File;
    //   42: invokespecial 862	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   45: invokespecial 865	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   48: astore 5
    //   50: aload 5
    //   52: iconst_1
    //   53: invokevirtual 868	java/io/DataOutputStream:writeInt	(I)V
    //   56: aload 5
    //   58: aload_2
    //   59: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   62: aload 5
    //   64: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   67: goto -40 -> 27
    //   70: astore_3
    //   71: aload_0
    //   72: monitorexit
    //   73: aload_3
    //   74: athrow
    //   75: astore 6
    //   77: aconst_null
    //   78: astore 5
    //   80: ldc 195
    //   82: ldc_w 873
    //   85: aload 6
    //   87: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   90: pop
    //   91: aload 5
    //   93: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   96: goto -69 -> 27
    //   99: aload 5
    //   101: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   104: aload 7
    //   106: athrow
    //   107: astore 7
    //   109: goto -10 -> 99
    //   112: astore 6
    //   114: goto -34 -> 80
    //   117: astore 7
    //   119: aconst_null
    //   120: astore 5
    //   122: goto -23 -> 99
    //
    // Exception table:
    //   from	to	target	type
    //   2	22	70	finally
    //   62	67	70	finally
    //   91	96	70	finally
    //   99	107	70	finally
    //   30	50	75	java/lang/Throwable
    //   50	62	107	finally
    //   80	91	107	finally
    //   50	62	112	java/lang/Throwable
    //   30	50	117	finally
  }

  // ERROR //
  private String c(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 469	com/flurry/android/FlurryAgent:n	()Ljava/lang/String;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull +5 -> 11
    //   9: aload_2
    //   10: areturn
    //   11: aload_1
    //   12: invokevirtual 881	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   15: ldc_w 883
    //   18: invokestatic 889	android/provider/Settings$System:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    //   21: astore_3
    //   22: iconst_0
    //   23: istore 4
    //   25: aload_3
    //   26: ifnull +34 -> 60
    //   29: aload_3
    //   30: invokevirtual 630	java/lang/String:length	()I
    //   33: istore 12
    //   35: iconst_0
    //   36: istore 4
    //   38: iload 12
    //   40: ifle +20 -> 60
    //   43: aload_3
    //   44: ldc_w 891
    //   47: invokevirtual 302	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   50: istore 13
    //   52: iconst_0
    //   53: istore 4
    //   55: iload 13
    //   57: ifeq +29 -> 86
    //   60: iload 4
    //   62: ifeq +75 -> 137
    //   65: new 304	java/lang/StringBuilder
    //   68: dup
    //   69: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   72: ldc_w 841
    //   75: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: aload_3
    //   79: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   85: areturn
    //   86: getstatic 92	com/flurry/android/FlurryAgent:a	[Ljava/lang/String;
    //   89: astore 14
    //   91: aload 14
    //   93: arraylength
    //   94: istore 15
    //   96: iconst_0
    //   97: istore 16
    //   99: iload 16
    //   101: iload 15
    //   103: if_icmpge +28 -> 131
    //   106: aload_3
    //   107: aload 14
    //   109: iload 16
    //   111: aaload
    //   112: invokevirtual 302	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   115: istore 17
    //   117: iconst_0
    //   118: istore 4
    //   120: iload 17
    //   122: ifne -62 -> 60
    //   125: iinc 16 1
    //   128: goto -29 -> 99
    //   131: iconst_1
    //   132: istore 4
    //   134: goto -74 -> 60
    //   137: aload_1
    //   138: ldc_w 349
    //   141: invokevirtual 347	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   144: astore 5
    //   146: aload 5
    //   148: invokevirtual 793	java/io/File:exists	()Z
    //   151: ifeq -142 -> 9
    //   154: new 509	java/io/DataInputStream
    //   157: dup
    //   158: new 227	java/io/FileInputStream
    //   161: dup
    //   162: aload 5
    //   164: invokespecial 230	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   167: invokespecial 803	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   170: astore 6
    //   172: aload 6
    //   174: invokevirtual 894	java/io/DataInputStream:readInt	()I
    //   177: pop
    //   178: aload 6
    //   180: invokevirtual 523	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   183: astore 11
    //   185: aload 6
    //   187: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   190: aload 11
    //   192: areturn
    //   193: astore 7
    //   195: aconst_null
    //   196: astore 6
    //   198: ldc 195
    //   200: ldc_w 896
    //   203: aload 7
    //   205: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   208: pop
    //   209: aload 6
    //   211: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   214: aload_2
    //   215: areturn
    //   216: astore 8
    //   218: aconst_null
    //   219: astore 6
    //   221: aload 6
    //   223: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   226: aload 8
    //   228: athrow
    //   229: astore 8
    //   231: goto -10 -> 221
    //   234: astore 7
    //   236: goto -38 -> 198
    //
    // Exception table:
    //   from	to	target	type
    //   154	172	193	java/lang/Throwable
    //   154	172	216	finally
    //   172	185	229	finally
    //   198	209	229	finally
    //   172	185	234	java/lang/Throwable
  }

  private static String c(String paramString)
  {
    if ((paramString != null) && (paramString.length() > 4))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      for (int i1 = 0; i1 < -4 + paramString.length(); i1++)
        localStringBuilder.append('*');
      localStringBuilder.append(paramString.substring(-4 + paramString.length()));
      paramString = localStringBuilder.toString();
    }
    return paramString;
  }

  public static void clearTargetingKeywords()
  {
    e.Z.g();
  }

  private static String d(Context paramContext)
  {
    try
    {
      PackageInfo localPackageInfo = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
      if (localPackageInfo.versionName != null)
        return localPackageInfo.versionName;
      if (localPackageInfo.versionCode != 0)
      {
        String str = Integer.toString(localPackageInfo.versionCode);
        return str;
      }
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
    return "Unknown";
  }

  private void d(String paramString)
  {
    if (paramString != null);
    try
    {
      this.C = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private Location e(Context paramContext)
  {
    if ((paramContext.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) || (paramContext.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0))
    {
      LocationManager localLocationManager = (LocationManager)paramContext.getSystemService("location");
      try
      {
        if (this.B == null)
          this.B = localLocationManager;
        while (true)
        {
          Criteria localCriteria = k;
          if (localCriteria == null)
            localCriteria = new Criteria();
          String str = localLocationManager.getBestProvider(localCriteria, true);
          if (str == null)
            break;
          localLocationManager.requestLocationUpdates(str, 0L, 0.0F, this, Looper.getMainLooper());
          return localLocationManager.getLastKnownLocation(str);
          localLocationManager = this.B;
        }
      }
      finally
      {
      }
    }
    return null;
  }

  public static void endTimedEvent(String paramString)
  {
    try
    {
      e.a(paramString);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "Failed to signify the end of event: " + paramString, localThrowable);
    }
  }

  // ERROR //
  private void f()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 976	java/io/ByteArrayOutputStream
    //   5: dup
    //   6: invokespecial 977	java/io/ByteArrayOutputStream:<init>	()V
    //   9: astore_1
    //   10: new 859	java/io/DataOutputStream
    //   13: dup
    //   14: aload_1
    //   15: invokespecial 865	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   18: astore_2
    //   19: aload_2
    //   20: iconst_1
    //   21: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   24: aload_2
    //   25: aload_0
    //   26: getfield 368	com/flurry/android/FlurryAgent:y	Ljava/lang/String;
    //   29: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   32: aload_2
    //   33: aload_0
    //   34: getfield 292	com/flurry/android/FlurryAgent:H	J
    //   37: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   40: aload_2
    //   41: aload_0
    //   42: getfield 397	com/flurry/android/FlurryAgent:J	J
    //   45: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   48: aload_2
    //   49: lconst_0
    //   50: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   53: aload_2
    //   54: aload_0
    //   55: getfield 162	com/flurry/android/FlurryAgent:K	Ljava/lang/String;
    //   58: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   61: aload_2
    //   62: aload_0
    //   63: getfield 164	com/flurry/android/FlurryAgent:L	Ljava/lang/String;
    //   66: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   69: aload_2
    //   70: aload_0
    //   71: getfield 166	com/flurry/android/FlurryAgent:M	B
    //   74: invokevirtual 986	java/io/DataOutputStream:writeByte	(I)V
    //   77: aload_0
    //   78: getfield 168	com/flurry/android/FlurryAgent:N	Ljava/lang/String;
    //   81: ifnonnull +171 -> 252
    //   84: ldc 160
    //   86: astore 8
    //   88: aload_2
    //   89: aload 8
    //   91: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   94: aload_0
    //   95: getfield 401	com/flurry/android/FlurryAgent:R	Landroid/location/Location;
    //   98: ifnonnull +163 -> 261
    //   101: aload_2
    //   102: iconst_0
    //   103: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   106: aload_2
    //   107: aload_0
    //   108: getfield 429	com/flurry/android/FlurryAgent:X	I
    //   111: invokevirtual 868	java/io/DataOutputStream:writeInt	(I)V
    //   114: aload_2
    //   115: iconst_m1
    //   116: invokevirtual 986	java/io/DataOutputStream:writeByte	(I)V
    //   119: aload_2
    //   120: iconst_m1
    //   121: invokevirtual 986	java/io/DataOutputStream:writeByte	(I)V
    //   124: aload_2
    //   125: aload_0
    //   126: getfield 170	com/flurry/android/FlurryAgent:O	B
    //   129: invokevirtual 986	java/io/DataOutputStream:writeByte	(I)V
    //   132: aload_0
    //   133: getfield 992	com/flurry/android/FlurryAgent:P	Ljava/lang/Long;
    //   136: ifnonnull +186 -> 322
    //   139: aload_2
    //   140: iconst_0
    //   141: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   144: aload_2
    //   145: aload_0
    //   146: getfield 172	com/flurry/android/FlurryAgent:S	Ljava/util/Map;
    //   149: invokeinterface 635 1 0
    //   154: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   157: aload_0
    //   158: getfield 172	com/flurry/android/FlurryAgent:S	Ljava/util/Map;
    //   161: invokeinterface 996 1 0
    //   166: invokeinterface 999 1 0
    //   171: astore 9
    //   173: aload 9
    //   175: invokeinterface 575 1 0
    //   180: ifeq +161 -> 341
    //   183: aload 9
    //   185: invokeinterface 579 1 0
    //   190: checkcast 1001	java/util/Map$Entry
    //   193: astore 16
    //   195: aload_2
    //   196: aload 16
    //   198: invokeinterface 1004 1 0
    //   203: checkcast 86	java/lang/String
    //   206: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   209: aload_2
    //   210: aload 16
    //   212: invokeinterface 1007 1 0
    //   217: checkcast 634	com/flurry/android/l
    //   220: getfield 638	com/flurry/android/l:a	I
    //   223: invokevirtual 868	java/io/DataOutputStream:writeInt	(I)V
    //   226: goto -53 -> 173
    //   229: astore 5
    //   231: aload_2
    //   232: astore 6
    //   234: ldc 195
    //   236: ldc 160
    //   238: aload 5
    //   240: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   243: pop
    //   244: aload 6
    //   246: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   249: aload_0
    //   250: monitorexit
    //   251: return
    //   252: aload_0
    //   253: getfield 168	com/flurry/android/FlurryAgent:N	Ljava/lang/String;
    //   256: astore 8
    //   258: goto -170 -> 88
    //   261: aload_2
    //   262: iconst_1
    //   263: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   266: aload_2
    //   267: aload_0
    //   268: getfield 401	com/flurry/android/FlurryAgent:R	Landroid/location/Location;
    //   271: invokevirtual 1012	android/location/Location:getLatitude	()D
    //   274: invokestatic 1014	com/flurry/android/FlurryAgent:a	(D)D
    //   277: invokevirtual 1018	java/io/DataOutputStream:writeDouble	(D)V
    //   280: aload_2
    //   281: aload_0
    //   282: getfield 401	com/flurry/android/FlurryAgent:R	Landroid/location/Location;
    //   285: invokevirtual 1021	android/location/Location:getLongitude	()D
    //   288: invokestatic 1014	com/flurry/android/FlurryAgent:a	(D)D
    //   291: invokevirtual 1018	java/io/DataOutputStream:writeDouble	(D)V
    //   294: aload_2
    //   295: aload_0
    //   296: getfield 401	com/flurry/android/FlurryAgent:R	Landroid/location/Location;
    //   299: invokevirtual 1025	android/location/Location:getAccuracy	()F
    //   302: invokevirtual 1029	java/io/DataOutputStream:writeFloat	(F)V
    //   305: goto -199 -> 106
    //   308: astore_3
    //   309: aload_2
    //   310: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   313: aload_3
    //   314: athrow
    //   315: astore 4
    //   317: aload_0
    //   318: monitorexit
    //   319: aload 4
    //   321: athrow
    //   322: aload_2
    //   323: iconst_1
    //   324: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   327: aload_2
    //   328: aload_0
    //   329: getfield 992	com/flurry/android/FlurryAgent:P	Ljava/lang/Long;
    //   332: invokevirtual 605	java/lang/Long:longValue	()J
    //   335: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   338: goto -194 -> 144
    //   341: aload_2
    //   342: aload_0
    //   343: getfield 174	com/flurry/android/FlurryAgent:T	Ljava/util/List;
    //   346: invokeinterface 447 1 0
    //   351: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   354: aload_0
    //   355: getfield 174	com/flurry/android/FlurryAgent:T	Ljava/util/List;
    //   358: invokeinterface 570 1 0
    //   363: astore 10
    //   365: aload 10
    //   367: invokeinterface 575 1 0
    //   372: ifeq +23 -> 395
    //   375: aload_2
    //   376: aload 10
    //   378: invokeinterface 579 1 0
    //   383: checkcast 581	com/flurry/android/al
    //   386: invokevirtual 657	com/flurry/android/al:a	()[B
    //   389: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   392: goto -27 -> 365
    //   395: aload_2
    //   396: aload_0
    //   397: getfield 425	com/flurry/android/FlurryAgent:U	Z
    //   400: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   403: iconst_0
    //   404: istore 11
    //   406: iconst_0
    //   407: istore 12
    //   409: iconst_0
    //   410: istore 13
    //   412: iload 11
    //   414: aload_0
    //   415: getfield 176	com/flurry/android/FlurryAgent:W	Ljava/util/List;
    //   418: invokeinterface 447 1 0
    //   423: if_icmpge +43 -> 466
    //   426: iload 12
    //   428: aload_0
    //   429: getfield 176	com/flurry/android/FlurryAgent:W	Ljava/util/List;
    //   432: iload 11
    //   434: invokeinterface 617 2 0
    //   439: checkcast 599	com/flurry/android/av
    //   442: invokevirtual 1033	com/flurry/android/av:a	()[B
    //   445: arraylength
    //   446: iadd
    //   447: istore 12
    //   449: iload 12
    //   451: sipush 16000
    //   454: if_icmpgt +12 -> 466
    //   457: iinc 13 1
    //   460: iinc 11 1
    //   463: goto -51 -> 412
    //   466: aload_2
    //   467: aload_0
    //   468: getfield 399	com/flurry/android/FlurryAgent:Q	I
    //   471: invokevirtual 868	java/io/DataOutputStream:writeInt	(I)V
    //   474: aload_2
    //   475: iload 13
    //   477: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   480: iconst_0
    //   481: istore 14
    //   483: iload 14
    //   485: iload 13
    //   487: if_icmpge +30 -> 517
    //   490: aload_2
    //   491: aload_0
    //   492: getfield 176	com/flurry/android/FlurryAgent:W	Ljava/util/List;
    //   495: iload 14
    //   497: invokeinterface 617 2 0
    //   502: checkcast 599	com/flurry/android/av
    //   505: invokevirtual 1033	com/flurry/android/av:a	()[B
    //   508: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   511: iinc 14 1
    //   514: goto -31 -> 483
    //   517: aload_2
    //   518: iconst_0
    //   519: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   522: aload_2
    //   523: iconst_0
    //   524: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   527: aload_0
    //   528: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   531: aload_1
    //   532: invokevirtual 1036	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   535: invokeinterface 610 2 0
    //   540: pop
    //   541: aload_2
    //   542: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   545: goto -296 -> 249
    //   548: astore_3
    //   549: aconst_null
    //   550: astore_2
    //   551: goto -242 -> 309
    //   554: astore_3
    //   555: aload 6
    //   557: astore_2
    //   558: goto -249 -> 309
    //   561: astore 5
    //   563: aconst_null
    //   564: astore 6
    //   566: goto -332 -> 234
    //
    // Exception table:
    //   from	to	target	type
    //   19	84	229	java/io/IOException
    //   88	106	229	java/io/IOException
    //   106	144	229	java/io/IOException
    //   144	173	229	java/io/IOException
    //   173	226	229	java/io/IOException
    //   252	258	229	java/io/IOException
    //   261	305	229	java/io/IOException
    //   322	338	229	java/io/IOException
    //   341	365	229	java/io/IOException
    //   365	392	229	java/io/IOException
    //   395	403	229	java/io/IOException
    //   412	449	229	java/io/IOException
    //   466	480	229	java/io/IOException
    //   490	511	229	java/io/IOException
    //   517	541	229	java/io/IOException
    //   19	84	308	finally
    //   88	106	308	finally
    //   106	144	308	finally
    //   144	173	308	finally
    //   173	226	308	finally
    //   252	258	308	finally
    //   261	305	308	finally
    //   322	338	308	finally
    //   341	365	308	finally
    //   365	392	308	finally
    //   395	403	308	finally
    //   412	449	308	finally
    //   466	480	308	finally
    //   490	511	308	finally
    //   517	541	308	finally
    //   244	249	315	finally
    //   309	315	315	finally
    //   541	545	315	finally
    //   2	19	548	finally
    //   234	244	554	finally
    //   2	19	561	java/io/IOException
  }

  private static byte[] f(Context paramContext)
  {
    String str;
    if (paramContext.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0)
    {
      TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
      if (localTelephonyManager != null)
      {
        str = localTelephonyManager.getDeviceId();
        if ((str == null) || (str.trim().length() <= 0));
      }
    }
    try
    {
      byte[] arrayOfByte = z.d(str);
      if ((arrayOfByte != null) && (arrayOfByte.length == 20))
        return arrayOfByte;
      bd.b("FlurryAgent", "sha1 is not 20 bytes long: " + Arrays.toString(arrayOfByte));
      label96: return null;
    }
    catch (Exception localException)
    {
      break label96;
    }
  }

  private void g()
  {
    try
    {
      this.X = (1 + this.X);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static boolean getAd(Context paramContext, String paramString, ViewGroup paramViewGroup, FlurryAdSize paramFlurryAdSize, long paramLong)
  {
    if (paramContext == null)
    {
      bd.b("FlurryAgent", "Context passed to getAd was null.");
      return false;
    }
    if (paramString == null)
    {
      bd.b("FlurryAgent", "Ad space name passed to getAd was null.");
      return false;
    }
    if (paramViewGroup == null)
    {
      bd.b("FlurryAgent", "ViewGroup passed to getAd was null.");
      return false;
    }
    e.a(paramContext);
    try
    {
      e.Z.a(paramString, paramFlurryAdSize);
      boolean bool = e.Z.a(paramContext, paramString, paramViewGroup, paramLong);
      return bool;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
    return false;
  }

  public static int getAgentVersion()
  {
    return 129;
  }

  public static boolean getForbidPlaintextFallback()
  {
    return false;
  }

  public static String getPhoneId()
  {
    return e.n();
  }

  public static boolean getUseHttps()
  {
    return h;
  }

  // ERROR //
  private byte[] h()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 1079	com/flurry/android/CrcMessageDigest
    //   5: dup
    //   6: invokespecial 1080	com/flurry/android/CrcMessageDigest:<init>	()V
    //   9: astore_1
    //   10: new 976	java/io/ByteArrayOutputStream
    //   13: dup
    //   14: invokespecial 977	java/io/ByteArrayOutputStream:<init>	()V
    //   17: astore_2
    //   18: new 1082	java/security/DigestOutputStream
    //   21: dup
    //   22: aload_2
    //   23: aload_1
    //   24: invokespecial 1085	java/security/DigestOutputStream:<init>	(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V
    //   27: astore_3
    //   28: new 859	java/io/DataOutputStream
    //   31: dup
    //   32: aload_3
    //   33: invokespecial 865	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   36: astore 4
    //   38: aload 4
    //   40: bipush 23
    //   42: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   45: aload 4
    //   47: iconst_0
    //   48: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   51: aload 4
    //   53: lconst_0
    //   54: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   57: aload 4
    //   59: iconst_0
    //   60: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   63: aload 4
    //   65: iconst_3
    //   66: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   69: aload 4
    //   71: sipush 129
    //   74: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   77: aload 4
    //   79: invokestatic 393	java/lang/System:currentTimeMillis	()J
    //   82: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   85: aload 4
    //   87: aload_0
    //   88: getfield 274	com/flurry/android/FlurryAgent:w	Ljava/lang/String;
    //   91: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   94: aload 4
    //   96: aload_0
    //   97: getfield 368	com/flurry/android/FlurryAgent:y	Ljava/lang/String;
    //   100: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   103: aload 4
    //   105: iconst_1
    //   106: aload_0
    //   107: getfield 153	com/flurry/android/FlurryAgent:D	Ljava/util/Map;
    //   110: invokeinterface 635 1 0
    //   115: iadd
    //   116: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   119: aload 4
    //   121: iconst_0
    //   122: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   125: aload 4
    //   127: aload_0
    //   128: invokespecial 469	com/flurry/android/FlurryAgent:n	()Ljava/lang/String;
    //   131: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   134: aload_0
    //   135: getfield 153	com/flurry/android/FlurryAgent:D	Ljava/util/Map;
    //   138: invokeinterface 460 1 0
    //   143: ifne +122 -> 265
    //   146: aload_0
    //   147: getfield 153	com/flurry/android/FlurryAgent:D	Ljava/util/Map;
    //   150: invokeinterface 996 1 0
    //   155: invokeinterface 999 1 0
    //   160: astore 21
    //   162: aload 21
    //   164: invokeinterface 575 1 0
    //   169: ifeq +96 -> 265
    //   172: aload 21
    //   174: invokeinterface 579 1 0
    //   179: checkcast 1001	java/util/Map$Entry
    //   182: astore 22
    //   184: aload 4
    //   186: aload 22
    //   188: invokeinterface 1004 1 0
    //   193: checkcast 340	java/lang/Integer
    //   196: invokevirtual 1088	java/lang/Integer:intValue	()I
    //   199: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   202: aload 22
    //   204: invokeinterface 1007 1 0
    //   209: checkcast 500	java/nio/ByteBuffer
    //   212: invokevirtual 1091	java/nio/ByteBuffer:array	()[B
    //   215: astore 23
    //   217: aload 4
    //   219: aload 23
    //   221: arraylength
    //   222: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   225: aload 4
    //   227: aload 23
    //   229: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   232: goto -70 -> 162
    //   235: astore 7
    //   237: aload 4
    //   239: astore 8
    //   241: ldc 195
    //   243: ldc_w 1093
    //   246: aload 7
    //   248: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   251: pop
    //   252: aload 8
    //   254: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   257: aconst_null
    //   258: astore 10
    //   260: aload_0
    //   261: monitorexit
    //   262: aload 10
    //   264: areturn
    //   265: aload 4
    //   267: iconst_0
    //   268: invokevirtual 986	java/io/DataOutputStream:writeByte	(I)V
    //   271: aload 4
    //   273: aload_0
    //   274: getfield 542	com/flurry/android/FlurryAgent:F	J
    //   277: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   280: aload 4
    //   282: aload_0
    //   283: getfield 292	com/flurry/android/FlurryAgent:H	J
    //   286: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   289: aload 4
    //   291: bipush 6
    //   293: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   296: aload 4
    //   298: ldc_w 1095
    //   301: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   304: aload 4
    //   306: getstatic 1100	android/os/Build:MODEL	Ljava/lang/String;
    //   309: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   312: aload 4
    //   314: ldc_w 1102
    //   317: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   320: aload 4
    //   322: getstatic 1105	android/os/Build:BRAND	Ljava/lang/String;
    //   325: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   328: aload 4
    //   330: ldc_w 1107
    //   333: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   336: aload 4
    //   338: getstatic 1109	android/os/Build:ID	Ljava/lang/String;
    //   341: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   344: aload 4
    //   346: ldc_w 1111
    //   349: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   352: aload 4
    //   354: getstatic 1116	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   357: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   360: aload 4
    //   362: ldc_w 1118
    //   365: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   368: aload 4
    //   370: getstatic 1121	android/os/Build:DEVICE	Ljava/lang/String;
    //   373: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   376: aload 4
    //   378: ldc_w 1123
    //   381: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   384: aload 4
    //   386: getstatic 1126	android/os/Build:PRODUCT	Ljava/lang/String;
    //   389: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   392: aload_0
    //   393: getfield 1128	com/flurry/android/FlurryAgent:Y	Ljava/util/Map;
    //   396: ifnull +284 -> 680
    //   399: aload_0
    //   400: getfield 1128	com/flurry/android/FlurryAgent:Y	Ljava/util/Map;
    //   403: invokeinterface 1131 1 0
    //   408: invokeinterface 1132 1 0
    //   413: istore 11
    //   415: ldc 195
    //   417: new 304	java/lang/StringBuilder
    //   420: dup
    //   421: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   424: ldc_w 1134
    //   427: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   430: iload 11
    //   432: invokevirtual 517	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   435: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   438: invokestatic 800	com/flurry/android/bd:c	(Ljava/lang/String;Ljava/lang/String;)I
    //   441: pop
    //   442: iload 11
    //   444: ifne +36 -> 480
    //   447: ldc 195
    //   449: new 304	java/lang/StringBuilder
    //   452: dup
    //   453: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   456: ldc_w 1136
    //   459: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   462: aload_0
    //   463: getfield 137	com/flurry/android/FlurryAgent:r	Ljava/io/File;
    //   466: invokevirtual 492	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   469: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   472: invokestatic 800	com/flurry/android/bd:c	(Ljava/lang/String;Ljava/lang/String;)I
    //   475: pop
    //   476: aload_0
    //   477: invokespecial 848	com/flurry/android/FlurryAgent:k	()V
    //   480: aload 4
    //   482: iload 11
    //   484: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   487: aload_0
    //   488: getfield 1128	com/flurry/android/FlurryAgent:Y	Ljava/util/Map;
    //   491: ifnull +195 -> 686
    //   494: aload_0
    //   495: getfield 1128	com/flurry/android/FlurryAgent:Y	Ljava/util/Map;
    //   498: invokeinterface 996 1 0
    //   503: invokeinterface 999 1 0
    //   508: astore 17
    //   510: aload 17
    //   512: invokeinterface 575 1 0
    //   517: ifeq +169 -> 686
    //   520: aload 17
    //   522: invokeinterface 579 1 0
    //   527: checkcast 1001	java/util/Map$Entry
    //   530: astore 18
    //   532: ldc 195
    //   534: new 304	java/lang/StringBuilder
    //   537: dup
    //   538: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   541: ldc_w 1138
    //   544: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   547: aload 18
    //   549: invokeinterface 1004 1 0
    //   554: checkcast 86	java/lang/String
    //   557: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   560: ldc_w 758
    //   563: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   566: aload 18
    //   568: invokeinterface 1007 1 0
    //   573: invokevirtual 492	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   576: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   579: invokestatic 269	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   582: pop
    //   583: aload 4
    //   585: aload 18
    //   587: invokeinterface 1004 1 0
    //   592: checkcast 86	java/lang/String
    //   595: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   598: aload 4
    //   600: aload 18
    //   602: invokeinterface 1007 1 0
    //   607: checkcast 441	java/util/List
    //   610: invokeinterface 447 1 0
    //   615: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   618: aload 18
    //   620: invokeinterface 1007 1 0
    //   625: checkcast 441	java/util/List
    //   628: invokeinterface 570 1 0
    //   633: astore 20
    //   635: aload 20
    //   637: invokeinterface 575 1 0
    //   642: ifeq -132 -> 510
    //   645: aload 4
    //   647: aload 20
    //   649: invokeinterface 579 1 0
    //   654: checkcast 86	java/lang/String
    //   657: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   660: goto -25 -> 635
    //   663: astore 5
    //   665: aload 4
    //   667: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   670: aload 5
    //   672: athrow
    //   673: astore 6
    //   675: aload_0
    //   676: monitorexit
    //   677: aload 6
    //   679: athrow
    //   680: iconst_0
    //   681: istore 11
    //   683: goto -268 -> 415
    //   686: aload_0
    //   687: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   690: invokeinterface 447 1 0
    //   695: istore 14
    //   697: aload 4
    //   699: iload 14
    //   701: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   704: iconst_0
    //   705: istore 15
    //   707: iload 15
    //   709: iload 14
    //   711: if_icmpge +28 -> 739
    //   714: aload 4
    //   716: aload_0
    //   717: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   720: iload 15
    //   722: invokeinterface 617 2 0
    //   727: checkcast 1140	[B
    //   730: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   733: iinc 15 1
    //   736: goto -29 -> 707
    //   739: aload_0
    //   740: new 155	java/util/ArrayList
    //   743: dup
    //   744: aload_0
    //   745: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   748: invokespecial 1143	java/util/ArrayList:<init>	(Ljava/util/Collection;)V
    //   751: putfield 743	com/flurry/android/FlurryAgent:A	Ljava/util/List;
    //   754: aload_3
    //   755: iconst_0
    //   756: invokevirtual 1146	java/security/DigestOutputStream:on	(Z)V
    //   759: aload 4
    //   761: aload_1
    //   762: invokevirtual 1149	com/flurry/android/CrcMessageDigest:getDigest	()[B
    //   765: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   768: aload 4
    //   770: invokevirtual 1152	java/io/DataOutputStream:close	()V
    //   773: aload_2
    //   774: invokevirtual 1036	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   777: astore 16
    //   779: aload 16
    //   781: astore 10
    //   783: aload 4
    //   785: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   788: goto -528 -> 260
    //   791: astore 5
    //   793: aconst_null
    //   794: astore 4
    //   796: goto -131 -> 665
    //   799: astore 5
    //   801: aload 8
    //   803: astore 4
    //   805: goto -140 -> 665
    //   808: astore 7
    //   810: aconst_null
    //   811: astore 8
    //   813: goto -572 -> 241
    //
    // Exception table:
    //   from	to	target	type
    //   38	162	235	java/lang/Throwable
    //   162	232	235	java/lang/Throwable
    //   265	415	235	java/lang/Throwable
    //   415	442	235	java/lang/Throwable
    //   447	480	235	java/lang/Throwable
    //   480	510	235	java/lang/Throwable
    //   510	635	235	java/lang/Throwable
    //   635	660	235	java/lang/Throwable
    //   686	704	235	java/lang/Throwable
    //   714	733	235	java/lang/Throwable
    //   739	779	235	java/lang/Throwable
    //   38	162	663	finally
    //   162	232	663	finally
    //   265	415	663	finally
    //   415	442	663	finally
    //   447	480	663	finally
    //   480	510	663	finally
    //   510	635	663	finally
    //   635	660	663	finally
    //   686	704	663	finally
    //   714	733	663	finally
    //   739	779	663	finally
    //   252	257	673	finally
    //   665	673	673	finally
    //   783	788	673	finally
    //   2	38	791	finally
    //   241	252	799	finally
    //   2	38	808	java/lang/Throwable
  }

  private static String i()
  {
    if (b != null)
      return b;
    if (i)
      return c;
    if (h)
      return d;
    return c;
  }

  public static void initializeAds(Context paramContext)
  {
    if (paramContext == null)
    {
      bd.b("FlurryAgent", "Context passed to initializeAds was null.");
      return;
    }
    e.a(paramContext);
    try
    {
      e.Z.b(paramContext);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static boolean isAdAvailable(Context paramContext, String paramString, FlurryAdSize paramFlurryAdSize, long paramLong)
  {
    if (paramContext == null)
    {
      bd.b("FlurryAgent", "Context passed to isAdAvailable was null.");
      return false;
    }
    if (paramString == null)
    {
      bd.b("FlurryAgent", "Ad space name passed to isAdAvailable was null.");
      return false;
    }
    e.a(paramContext);
    try
    {
      e.Z.a(paramString, paramFlurryAdSize);
      boolean bool = e.Z.a(paramContext, paramString, paramLong);
      return bool;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
    return false;
  }

  protected static boolean isCaptureUncaughtExceptions()
  {
    return j;
  }

  private void j()
  {
    while (true)
    {
      try
      {
        bd.a("FlurryAgent", "generating report");
        byte[] arrayOfByte = h();
        if (arrayOfByte != null)
        {
          if (a(arrayOfByte))
          {
            StringBuilder localStringBuilder = new StringBuilder().append("Done sending ");
            if (!this.s)
              break label115;
            str = "initial ";
            bd.a("FlurryAgent", str + "agent report");
            l();
          }
        }
        else
        {
          bd.a("FlurryAgent", "Error generating report");
          return;
        }
      }
      catch (IOException localIOException)
      {
        bd.a("FlurryAgent", "", localIOException);
        return;
      }
      catch (Throwable localThrowable)
      {
        bd.b("FlurryAgent", "", localThrowable);
      }
      return;
      label115: String str = "";
    }
  }

  private void k()
  {
    if (this.r.exists())
    {
      bd.c("FlurryAgent", "Loading referrer info from file:  " + this.r.getAbsolutePath());
      String str = a(this.r);
      if (str != null)
      {
        bd.c("FlurryAgent", "Parsing referrer map");
        this.Y = b(str);
      }
    }
  }

  // ERROR //
  private void l()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   6: invokestatic 857	com/flurry/android/ae:a	(Ljava/io/File;)Z
    //   9: istore 6
    //   11: iload 6
    //   13: ifne +10 -> 23
    //   16: aconst_null
    //   17: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: new 859	java/io/DataOutputStream
    //   26: dup
    //   27: new 861	java/io/FileOutputStream
    //   30: dup
    //   31: aload_0
    //   32: getfield 135	com/flurry/android/FlurryAgent:q	Ljava/io/File;
    //   35: invokespecial 862	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   38: invokespecial 865	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   41: astore_2
    //   42: aload_2
    //   43: ldc_w 804
    //   46: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   49: aload_2
    //   50: iconst_2
    //   51: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   54: aload_2
    //   55: aload_0
    //   56: getfield 274	com/flurry/android/FlurryAgent:w	Ljava/lang/String;
    //   59: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   62: aload_2
    //   63: aload_0
    //   64: invokespecial 469	com/flurry/android/FlurryAgent:n	()Ljava/lang/String;
    //   67: invokevirtual 871	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   70: aload_2
    //   71: aload_0
    //   72: getfield 537	com/flurry/android/FlurryAgent:E	Z
    //   75: invokevirtual 990	java/io/DataOutputStream:writeBoolean	(Z)V
    //   78: aload_2
    //   79: aload_0
    //   80: getfield 542	com/flurry/android/FlurryAgent:F	J
    //   83: invokevirtual 983	java/io/DataOutputStream:writeLong	(J)V
    //   86: iconst_m1
    //   87: aload_0
    //   88: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   91: invokeinterface 447 1 0
    //   96: iadd
    //   97: istore 7
    //   99: iload 7
    //   101: iflt +66 -> 167
    //   104: aload_0
    //   105: getfield 158	com/flurry/android/FlurryAgent:G	Ljava/util/List;
    //   108: iload 7
    //   110: invokeinterface 617 2 0
    //   115: checkcast 1140	[B
    //   118: astore 8
    //   120: aload 8
    //   122: arraylength
    //   123: istore 9
    //   125: iload 9
    //   127: iconst_2
    //   128: iadd
    //   129: aload_2
    //   130: invokevirtual 1191	java/io/DataOutputStream:size	()I
    //   133: iadd
    //   134: ldc_w 1192
    //   137: if_icmple +47 -> 184
    //   140: ldc 195
    //   142: new 304	java/lang/StringBuilder
    //   145: dup
    //   146: invokespecial 305	java/lang/StringBuilder:<init>	()V
    //   149: ldc_w 1194
    //   152: invokevirtual 310	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: iload 7
    //   157: invokevirtual 517	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   160: invokevirtual 313	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   163: invokestatic 269	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   166: pop
    //   167: aload_2
    //   168: iconst_0
    //   169: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   172: aload_2
    //   173: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   176: goto -156 -> 20
    //   179: astore_3
    //   180: aload_0
    //   181: monitorexit
    //   182: aload_3
    //   183: athrow
    //   184: aload_2
    //   185: iload 9
    //   187: invokevirtual 980	java/io/DataOutputStream:writeShort	(I)V
    //   190: aload_2
    //   191: aload 8
    //   193: invokevirtual 1032	java/io/DataOutputStream:write	([B)V
    //   196: iinc 7 255
    //   199: goto -100 -> 99
    //   202: astore 4
    //   204: aconst_null
    //   205: astore_2
    //   206: ldc 195
    //   208: ldc 160
    //   210: aload 4
    //   212: invokestatic 251	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   215: pop
    //   216: aload_2
    //   217: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   220: goto -200 -> 20
    //   223: aload_2
    //   224: invokestatic 256	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   227: aload_1
    //   228: athrow
    //   229: astore_1
    //   230: goto -7 -> 223
    //   233: astore 4
    //   235: goto -29 -> 206
    //   238: astore_1
    //   239: aconst_null
    //   240: astore_2
    //   241: goto -18 -> 223
    //
    // Exception table:
    //   from	to	target	type
    //   16	20	179	finally
    //   172	176	179	finally
    //   216	220	179	finally
    //   223	229	179	finally
    //   2	11	202	java/lang/Throwable
    //   23	42	202	java/lang/Throwable
    //   42	99	229	finally
    //   104	167	229	finally
    //   167	172	229	finally
    //   184	196	229	finally
    //   206	216	229	finally
    //   42	99	233	java/lang/Throwable
    //   104	167	233	java/lang/Throwable
    //   167	172	233	java/lang/Throwable
    //   184	196	233	java/lang/Throwable
    //   2	11	238	finally
    //   23	42	238	finally
  }

  public static void logEvent(String paramString)
  {
    try
    {
      e.a(paramString, null, false);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "Failed to log event: " + paramString, localThrowable);
    }
  }

  public static void logEvent(String paramString, Map<String, String> paramMap)
  {
    try
    {
      e.a(paramString, paramMap, false);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "Failed to log event: " + paramString, localThrowable);
    }
  }

  public static void logEvent(String paramString, Map<String, String> paramMap, boolean paramBoolean)
  {
    try
    {
      e.a(paramString, paramMap, paramBoolean);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "Failed to log event: " + paramString, localThrowable);
    }
  }

  public static void logEvent(String paramString, boolean paramBoolean)
  {
    try
    {
      e.a(paramString, null, paramBoolean);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "Failed to log event: " + paramString, localThrowable);
    }
  }

  private void m()
  {
    try
    {
      if (this.B != null)
        this.B.removeUpdates(this);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private String n()
  {
    try
    {
      String str = this.C;
      return str;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static void onEndSession(Context paramContext)
  {
    if (paramContext == null)
      throw new NullPointerException("Null context");
    try
    {
      e.a(paramContext, false);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void onError(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      e.a(paramString1, paramString2, paramString3);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void onEvent(String paramString)
  {
    try
    {
      e.a(paramString, null, false);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void onEvent(String paramString, Map<String, String> paramMap)
  {
    try
    {
      e.a(paramString, paramMap, false);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void onPageView()
  {
    try
    {
      e.g();
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void onStartSession(Context paramContext, String paramString)
  {
    if (paramContext == null)
      throw new NullPointerException("Null context");
    if ((paramString == null) || (paramString.length() == 0))
      throw new IllegalArgumentException("Api key not specified");
    try
    {
      e.a(paramContext, paramString);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void removeAd(Context paramContext, String paramString, ViewGroup paramViewGroup)
  {
    if (paramContext == null)
    {
      bd.b("FlurryAgent", "Context passed to removeAd was null.");
      return;
    }
    if (paramString == null)
    {
      bd.b("FlurryAgent", "Ad space name passed to removeAd was null.");
      return;
    }
    if (paramViewGroup == null)
    {
      bd.b("FlurryAgent", "ViewGroup passed to removeAd was null.");
      return;
    }
    try
    {
      e.Z.a(paramContext, paramViewGroup);
      return;
    }
    catch (Throwable localThrowable)
    {
      bd.b("FlurryAgent", "", localThrowable);
    }
  }

  public static void sendAdLogsToServer()
  {
    e.Z.k();
  }

  public static void setAdListener(IListener paramIListener)
  {
    e.Z.a(paramIListener);
  }

  public static void setAdLogUrl(String paramString)
  {
    e.Z.c(paramString);
  }

  public static void setAdServerUrl(String paramString)
  {
    e.Z.b(paramString);
  }

  public static void setAge(int paramInt)
  {
    if ((paramInt > 0) && (paramInt < 110))
    {
      Date localDate = new Date(new Date(System.currentTimeMillis() - 31449600000L * paramInt).getYear(), 1, 1);
      e.P = Long.valueOf(localDate.getTime());
    }
  }

  public static void setCaptureUncaughtExceptions(boolean paramBoolean)
  {
    synchronized (e)
    {
      if (e.s)
      {
        bd.b("FlurryAgent", "Cannot setCaptureUncaughtExceptions after onSessionStart");
        return;
      }
      j = paramBoolean;
      return;
    }
  }

  public static void setContinueSessionMillis(long paramLong)
  {
    if (paramLong < 5000L)
    {
      bd.b("FlurryAgent", "Invalid time set for session resumption: " + paramLong);
      return;
    }
    synchronized (e)
    {
      f = paramLong;
      return;
    }
  }

  public static void setCustomAdNetworkHandler(ICustomAdNetworkHandler paramICustomAdNetworkHandler)
  {
    e.Z.a(paramICustomAdNetworkHandler);
  }

  public static void setGender(byte paramByte)
  {
    switch (paramByte)
    {
    default:
      e.O = -1;
      return;
    case 0:
    case 1:
    }
    e.O = paramByte;
  }

  public static void setLocation(float paramFloat1, float paramFloat2)
  {
    e.Z.a(paramFloat1, paramFloat2);
  }

  public static void setLocationCriteria(Criteria paramCriteria)
  {
    synchronized (e)
    {
      k = paramCriteria;
      return;
    }
  }

  public static void setLogEnabled(boolean paramBoolean)
  {
    FlurryAgent localFlurryAgent = e;
    if (paramBoolean);
    try
    {
      bd.b();
      while (true)
      {
        return;
        bd.a();
      }
    }
    finally
    {
    }
  }

  public static void setLogEvents(boolean paramBoolean)
  {
    synchronized (e)
    {
      g = paramBoolean;
      return;
    }
  }

  public static void setLogLevel(int paramInt)
  {
    synchronized (e)
    {
      bd.a(paramInt);
      return;
    }
  }

  public static void setReportLocation(boolean paramBoolean)
  {
    synchronized (e)
    {
      e.z = paramBoolean;
      return;
    }
  }

  public static void setReportUrl(String paramString)
  {
    b = paramString;
  }

  public static void setTargetingKeywords(Map<String, String> paramMap)
  {
    e.Z.b(paramMap);
  }

  public static void setUseHttps(boolean paramBoolean)
  {
    h = paramBoolean;
  }

  public static void setUserCookies(Map<String, String> paramMap)
  {
    e.Z.c(paramMap);
  }

  public static void setUserId(String paramString)
  {
    synchronized (e)
    {
      e.N = z.a(paramString);
      return;
    }
  }

  public static void setVersionName(String paramString)
  {
    synchronized (e)
    {
      e.y = paramString;
      return;
    }
  }

  final void a(Throwable paramThrowable)
  {
    paramThrowable.printStackTrace();
    String str = "";
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    if ((arrayOfStackTraceElement != null) && (arrayOfStackTraceElement.length > 0))
    {
      StringBuilder localStringBuilder = new StringBuilder();
      if (paramThrowable.getMessage() != null)
        localStringBuilder.append(" (" + paramThrowable.getMessage() + ")\n");
      for (int i1 = 0; i1 < arrayOfStackTraceElement.length; i1++)
      {
        if (i1 != 0)
          localStringBuilder.append('\n');
        StackTraceElement localStackTraceElement = arrayOfStackTraceElement[i1];
        localStringBuilder.append(localStackTraceElement.getClassName()).append(".").append(localStackTraceElement.getMethodName()).append(":").append(localStackTraceElement.getLineNumber());
      }
      str = localStringBuilder.toString();
    }
    while (true)
    {
      onError("uncaught", str, paramThrowable.getClass().toString());
      this.v.clear();
      a(null, true);
      return;
      if (paramThrowable.getMessage() != null)
        str = paramThrowable.getMessage();
    }
  }

  public final void clearUserCookies()
  {
    e.Z.i();
  }

  public final void onLocationChanged(Location paramLocation)
  {
    try
    {
      this.R = paramLocation;
      m();
      return;
    }
    catch (Throwable localThrowable)
    {
      while (true)
        bd.b("FlurryAgent", "", localThrowable);
    }
    finally
    {
    }
  }

  public final void onProviderDisabled(String paramString)
  {
  }

  public final void onProviderEnabled(String paramString)
  {
  }

  public final void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.FlurryAgent
 * JD-Core Version:    0.6.2
 */