package com.flurry.android;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

public final class FlurryFullscreenTakeoverActivity extends Activity
{
  private static final String a = FlurryFullscreenTakeoverActivity.class.getSimpleName();
  private ak b;
  private ax c;

  public final void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  public final void onCreate(Bundle paramBundle)
  {
    setTheme(16973831);
    super.onCreate(paramBundle);
    String str = getIntent().getStringExtra("url");
    if (str == null)
    {
      bf localbf = FlurryAgent.a();
      this.b = new ak(this, localbf, localbf.n(), localbf.o());
      this.b.initLayout(this);
      setContentView(this.b);
      return;
    }
    this.c = new ax(this, str);
    setContentView(this.c);
  }

  protected final void onDestroy()
  {
    super.onDestroy();
  }

  protected final void onPause()
  {
    super.onPause();
  }

  protected final void onRestart()
  {
    super.onRestart();
  }

  protected final void onResume()
  {
    super.onResume();
  }

  public final void onStart()
  {
    super.onStart();
  }

  public final void onStop()
  {
    super.onStop();
    if (this.b != null)
      this.b.a();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.FlurryFullscreenTakeoverActivity
 * JD-Core Version:    0.6.2
 */