package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.ViewGroup;
import java.util.Iterator;
import java.util.List;

public class GetAdAsyncTask extends AsyncTask<Void, Void, AdUnit>
{
  private final String a = getClass().getSimpleName();
  private Context b;
  private String c;
  private ViewGroup d;
  private be e;
  private bf f;
  private u g;
  private u h;

  public GetAdAsyncTask(Context paramContext, String paramString, ViewGroup paramViewGroup, bf parambf)
  {
    this.b = paramContext;
    this.c = paramString;
    this.d = paramViewGroup;
    this.e = parambf.d;
    this.f = parambf;
  }

  protected AdUnit doInBackground(Void[] paramArrayOfVoid)
  {
    int i = z.a(this.b, this.d.getWidth());
    int j = z.a(this.b, this.d.getHeight());
    AdUnit localAdUnit = this.f.d(this.c);
    if (localAdUnit != null);
    do
    {
      return localAdUnit;
      new StringBuilder().append("Making ad request:").append(this.c).append(",").append(i).append("x").append(j).append(",refresh:").append(false).toString();
      this.f.a(this.c, i, j, false, 0L);
      localAdUnit = this.f.d(this.c);
    }
    while (localAdUnit != null);
    return null;
  }

  protected void onPostExecute(AdUnit paramAdUnit)
  {
    if (paramAdUnit == null);
    while (true)
    {
      return;
      r localr = this.f.a(this.b, paramAdUnit);
      if ((localr.b() != null) && (localr.d()))
      {
        this.g.removeAllViews();
        IListener localIListener2 = this.f.e();
        if (((AdFrame)localr.a().c().get(0)).d().d().equals("takeover"));
        for (FlurryAdType localFlurryAdType2 = FlurryAdType.WEB_TAKEOVER; ; localFlurryAdType2 = FlurryAdType.WEB_BANNER)
        {
          if ((localIListener2 == null) || (localIListener2.shouldDisplayAd(this.c, localFlurryAdType2)))
          {
            if (this.g.getParent() == null)
              localr.b().a(this.d, this.g);
            this.g.addView(localr.b());
            this.g.a(0);
          }
          if (!this.g.equals(this.h))
          {
            if (this.h != null)
            {
              this.e.a(this.b, this.h);
              this.d.removeView(this.h);
            }
            this.d.addView(this.g);
          }
          if ((this.g.a() <= 0) || (this.g.b()))
            break;
          this.e.a(this.g);
          return;
        }
      }
      if (localr.c())
      {
        IListener localIListener1 = this.f.e();
        if (((AdFrame)localr.a().c().get(0)).a().intValue() == 3);
        for (FlurryAdType localFlurryAdType1 = FlurryAdType.VIDEO_TAKEOVER; (localIListener1 == null) || (localIListener1.shouldDisplayAd(this.c, localFlurryAdType1)); localFlurryAdType1 = FlurryAdType.WEB_TAKEOVER)
        {
          Intent localIntent = new Intent();
          localIntent.setClass(this.b, FlurryFullscreenTakeoverActivity.class);
          this.b.startActivity(localIntent);
          return;
        }
      }
    }
  }

  protected void onPreExecute()
  {
    this.g = this.e.a(this.b, this.d, this.c);
    if (this.g == null)
    {
      this.g = this.e.a(this.f, this.b, this.d, this.c);
      this.e.b(this.b, this.g);
    }
    this.h = this.e.a(this.d);
    Iterator localIterator = this.e.a(this.b, this.d, this.c).iterator();
    while (localIterator.hasNext())
    {
      u localu = (u)localIterator.next();
      if (!localu.equals(this.h))
      {
        this.e.a(this.b, localu);
        localu.c().removeView(localu);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.GetAdAsyncTask
 * JD-Core Version:    0.6.2
 */