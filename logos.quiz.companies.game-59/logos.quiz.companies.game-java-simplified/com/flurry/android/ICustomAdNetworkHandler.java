package com.flurry.android;

import android.content.Context;

public abstract interface ICustomAdNetworkHandler
{
  public abstract AdNetworkView getAdFromNetwork(Context paramContext, AdCreative paramAdCreative, String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ICustomAdNetworkHandler
 * JD-Core Version:    0.6.2
 */