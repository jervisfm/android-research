package com.flurry.android;

import java.util.Map;

public abstract interface IListener
{
  public abstract void onAdClosed(String paramString);

  public abstract void onApplicationExit(String paramString);

  public abstract void onRenderFailed(String paramString);

  public abstract void onReward(String paramString, Map<String, String> paramMap);

  public abstract boolean shouldDisplayAd(String paramString, FlurryAdType paramFlurryAdType);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.IListener
 * JD-Core Version:    0.6.2
 */