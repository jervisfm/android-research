package com.flurry.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import java.io.File;

public final class InstallReceiver extends BroadcastReceiver
{
  private final Handler a;
  private File b = null;

  public InstallReceiver()
  {
    HandlerThread localHandlerThread = new HandlerThread("InstallReceiver");
    localHandlerThread.start();
    this.a = new Handler(localHandlerThread.getLooper());
  }

  private void a(String paramString)
  {
    try
    {
      aw localaw = new aw(this, paramString);
      this.a.post(localaw);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    bd.c("InstallReceiver", "Received an Install nofication of " + paramIntent.getAction());
    this.b = paramContext.getFileStreamPath(".flurryinstallreceiver." + Integer.toString(FlurryAgent.b().hashCode(), 16));
    if (FlurryAgent.isCaptureUncaughtExceptions())
      Thread.setDefaultUncaughtExceptionHandler(new FlurryAgent.FlurryDefaultExceptionHandler());
    String str = paramIntent.getExtras().getString("referrer");
    if ((str == null) || (!"com.android.vending.INSTALL_REFERRER".equals(paramIntent.getAction())))
      return;
    a(str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.InstallReceiver
 * JD-Core Version:    0.6.2
 */