package com.flurry.android;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class Location$Builder extends SpecificRecordBuilderBase<Location>
  implements RecordBuilder<Location>
{
  private float a;
  private float b;

  private Location$Builder(byte paramByte)
  {
    super(Location.SCHEMA$);
  }

  public Location build()
  {
    try
    {
      Location localLocation = new Location();
      float f1;
      if (fieldSetFlags()[0] != 0)
      {
        f1 = this.a;
        localLocation.a = f1;
        if (fieldSetFlags()[1] == 0)
          break label70;
      }
      label70: float f2;
      for (float f3 = this.b; ; f3 = f2)
      {
        localLocation.b = f3;
        return localLocation;
        f1 = ((Float)defaultValue(fields()[0])).floatValue();
        break;
        f2 = ((Float)defaultValue(fields()[1])).floatValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearLat()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearLon()
  {
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Float getLat()
  {
    return Float.valueOf(this.a);
  }

  public Float getLon()
  {
    return Float.valueOf(this.b);
  }

  public boolean hasLat()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasLon()
  {
    return fieldSetFlags()[1];
  }

  public Builder setLat(float paramFloat)
  {
    validate(fields()[0], Float.valueOf(paramFloat));
    this.a = paramFloat;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setLon(float paramFloat)
  {
    validate(fields()[1], Float.valueOf(paramFloat));
    this.b = paramFloat;
    fieldSetFlags()[1] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.Location.Builder
 * JD-Core Version:    0.6.2
 */