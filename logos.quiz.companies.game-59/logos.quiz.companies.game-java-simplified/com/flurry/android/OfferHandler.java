package com.flurry.android;

import java.util.List;
import java.util.Map;

public class OfferHandler
{
  public List<Map<String, String>> getOfferMapList()
  {
    return null;
  }

  public void onAdClicked(String paramString, Map<String, String> paramMap)
  {
    null.a(paramString, "clicked", true, paramMap);
  }

  public void onAdClosed(String paramString, Map<String, String> paramMap)
  {
    null.a(paramString, "adClosed", true, paramMap);
  }

  public void onAdFilled(String paramString, Map<String, String> paramMap)
  {
    null.a(paramString, "filled", true, paramMap);
  }

  public void onAdShown(String paramString, Map<String, String> paramMap)
  {
    null.a(paramString, "rendered", true, paramMap);
  }

  public void onAdUnFilled(String paramString, Map<String, String> paramMap)
  {
    null.a(paramString, "unfilled", true, paramMap);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.OfferHandler
 * JD-Core Version:    0.6.2
 */