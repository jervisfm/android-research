package com.flurry.android;

import java.util.Map;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class SdkAdEvent$Builder extends SpecificRecordBuilderBase<SdkAdEvent>
  implements RecordBuilder<SdkAdEvent>
{
  private CharSequence a;
  private Map<CharSequence, CharSequence> b;
  private long c;

  private SdkAdEvent$Builder()
  {
    super(SdkAdEvent.SCHEMA$);
  }

  public SdkAdEvent build()
  {
    try
    {
      SdkAdEvent localSdkAdEvent = new SdkAdEvent();
      CharSequence localCharSequence;
      Map localMap;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence = this.a;
        localSdkAdEvent.a = localCharSequence;
        if (fieldSetFlags()[1] == 0)
          break label88;
        localMap = this.b;
        label42: localSdkAdEvent.b = localMap;
        if (fieldSetFlags()[2] == 0)
          break label106;
      }
      label88: label106: long l1;
      for (long l2 = this.c; ; l2 = l1)
      {
        localSdkAdEvent.c = l2;
        return localSdkAdEvent;
        localCharSequence = (CharSequence)defaultValue(fields()[0]);
        break;
        localMap = (Map)defaultValue(fields()[1]);
        break label42;
        l1 = ((Long)defaultValue(fields()[2])).longValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearParams()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearTimeOffset()
  {
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearType()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Map<CharSequence, CharSequence> getParams()
  {
    return this.b;
  }

  public Long getTimeOffset()
  {
    return Long.valueOf(this.c);
  }

  public CharSequence getType()
  {
    return this.a;
  }

  public boolean hasParams()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasTimeOffset()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasType()
  {
    return fieldSetFlags()[0];
  }

  public Builder setParams(Map<CharSequence, CharSequence> paramMap)
  {
    validate(fields()[1], paramMap);
    this.b = paramMap;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setTimeOffset(long paramLong)
  {
    validate(fields()[2], Long.valueOf(paramLong));
    this.c = paramLong;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setType(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.SdkAdEvent.Builder
 * JD-Core Version:    0.6.2
 */