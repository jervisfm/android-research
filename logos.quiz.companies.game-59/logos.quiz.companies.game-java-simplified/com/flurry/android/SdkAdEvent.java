package com.flurry.android;

import java.util.Map;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.specific.SpecificRecord;
import org.apache.avro.specific.SpecificRecordBase;

class SdkAdEvent extends SpecificRecordBase
  implements SpecificRecord
{
  public static final Schema SCHEMA$ = new Schema.Parser().parse("{\"type\":\"record\",\"name\":\"SdkAdEvent\",\"namespace\":\"com.flurry.android\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"params\",\"type\":{\"type\":\"map\",\"values\":\"string\"}},{\"name\":\"timeOffset\",\"type\":\"long\"}]}");
  public CharSequence a;
  public Map<CharSequence, CharSequence> b;
  public long c;

  public final void a(CharSequence paramCharSequence)
  {
    this.a = paramCharSequence;
  }

  public final void a(Long paramLong)
  {
    this.c = paramLong.longValue();
  }

  public final void a(Map<CharSequence, CharSequence> paramMap)
  {
    this.b = paramMap;
  }

  public Object get(int paramInt)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      return this.a;
    case 1:
      return this.b;
    case 2:
    }
    return Long.valueOf(this.c);
  }

  public Schema getSchema()
  {
    return SCHEMA$;
  }

  public void put(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default:
      throw new AvroRuntimeException("Bad index");
    case 0:
      this.a = ((CharSequence)paramObject);
      return;
    case 1:
      this.b = ((Map)paramObject);
      return;
    case 2:
    }
    this.c = ((Long)paramObject).longValue();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.SdkAdEvent
 * JD-Core Version:    0.6.2
 */