package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class SdkAdLog$Builder extends SpecificRecordBuilderBase<SdkAdLog>
  implements RecordBuilder<SdkAdLog>
{
  private long a;
  private CharSequence b;
  private List<SdkAdEvent> c;

  private SdkAdLog$Builder()
  {
    super(SdkAdLog.SCHEMA$);
  }

  public SdkAdLog build()
  {
    try
    {
      SdkAdLog localSdkAdLog = new SdkAdLog();
      long l;
      CharSequence localCharSequence;
      if (fieldSetFlags()[0] != 0)
      {
        l = this.a;
        localSdkAdLog.a = l;
        if (fieldSetFlags()[1] == 0)
          break label91;
        localCharSequence = this.b;
        label42: localSdkAdLog.b = localCharSequence;
        if (fieldSetFlags()[2] == 0)
          break label109;
      }
      label91: label109: for (List localList = this.c; ; localList = (List)defaultValue(fields()[2]))
      {
        localSdkAdLog.c = localList;
        return localSdkAdLog;
        l = ((Long)defaultValue(fields()[0])).longValue();
        break;
        localCharSequence = (CharSequence)defaultValue(fields()[1]);
        break label42;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdLogGUID()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearSdkAdEvents()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearSessionId()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public CharSequence getAdLogGUID()
  {
    return this.b;
  }

  public List<SdkAdEvent> getSdkAdEvents()
  {
    return this.c;
  }

  public Long getSessionId()
  {
    return Long.valueOf(this.a);
  }

  public boolean hasAdLogGUID()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasSdkAdEvents()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasSessionId()
  {
    return fieldSetFlags()[0];
  }

  public Builder setAdLogGUID(CharSequence paramCharSequence)
  {
    validate(fields()[1], paramCharSequence);
    this.b = paramCharSequence;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setSdkAdEvents(List<SdkAdEvent> paramList)
  {
    validate(fields()[2], paramList);
    this.c = paramList;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setSessionId(long paramLong)
  {
    validate(fields()[0], Long.valueOf(paramLong));
    this.a = paramLong;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.SdkAdLog.Builder
 * JD-Core Version:    0.6.2
 */