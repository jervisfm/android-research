package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class SdkLogRequest$Builder extends SpecificRecordBuilderBase<SdkLogRequest>
  implements RecordBuilder<SdkLogRequest>
{
  private CharSequence a;
  private List<AdReportedId> b;
  private List<SdkAdLog> c;
  private long d;
  private boolean e;

  private SdkLogRequest$Builder(byte paramByte)
  {
    super(SdkLogRequest.SCHEMA$);
  }

  public SdkLogRequest build()
  {
    try
    {
      SdkLogRequest localSdkLogRequest = new SdkLogRequest();
      CharSequence localCharSequence;
      List localList1;
      label42: List localList2;
      label63: long l;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence = this.a;
        localSdkLogRequest.a = localCharSequence;
        if (fieldSetFlags()[1] == 0)
          break label130;
        localList1 = this.b;
        localSdkLogRequest.b = localList1;
        if (fieldSetFlags()[2] == 0)
          break label148;
        localList2 = this.c;
        localSdkLogRequest.c = localList2;
        if (fieldSetFlags()[3] == 0)
          break label166;
        l = this.d;
        label84: localSdkLogRequest.d = l;
        if (fieldSetFlags()[4] == 0)
          break label187;
      }
      label130: label148: label166: label187: boolean bool1;
      for (boolean bool2 = this.e; ; bool2 = bool1)
      {
        localSdkLogRequest.e = bool2;
        return localSdkLogRequest;
        localCharSequence = (CharSequence)defaultValue(fields()[0]);
        break;
        localList1 = (List)defaultValue(fields()[1]);
        break label42;
        localList2 = (List)defaultValue(fields()[2]);
        break label63;
        l = ((Long)defaultValue(fields()[3])).longValue();
        break label84;
        bool1 = ((Boolean)defaultValue(fields()[4])).booleanValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdReportedIds()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearAgentTimestamp()
  {
    fieldSetFlags()[3] = 0;
    return this;
  }

  public Builder clearApiKey()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Builder clearSdkAdLogs()
  {
    this.c = null;
    fieldSetFlags()[2] = 0;
    return this;
  }

  public Builder clearTestDevice()
  {
    fieldSetFlags()[4] = 0;
    return this;
  }

  public List<AdReportedId> getAdReportedIds()
  {
    return this.b;
  }

  public Long getAgentTimestamp()
  {
    return Long.valueOf(this.d);
  }

  public CharSequence getApiKey()
  {
    return this.a;
  }

  public List<SdkAdLog> getSdkAdLogs()
  {
    return this.c;
  }

  public Boolean getTestDevice()
  {
    return Boolean.valueOf(this.e);
  }

  public boolean hasAdReportedIds()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasAgentTimestamp()
  {
    return fieldSetFlags()[3];
  }

  public boolean hasApiKey()
  {
    return fieldSetFlags()[0];
  }

  public boolean hasSdkAdLogs()
  {
    return fieldSetFlags()[2];
  }

  public boolean hasTestDevice()
  {
    return fieldSetFlags()[4];
  }

  public Builder setAdReportedIds(List<AdReportedId> paramList)
  {
    validate(fields()[1], paramList);
    this.b = paramList;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setAgentTimestamp(long paramLong)
  {
    validate(fields()[3], Long.valueOf(paramLong));
    this.d = paramLong;
    fieldSetFlags()[3] = 1;
    return this;
  }

  public Builder setApiKey(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }

  public Builder setSdkAdLogs(List<SdkAdLog> paramList)
  {
    validate(fields()[2], paramList);
    this.c = paramList;
    fieldSetFlags()[2] = 1;
    return this;
  }

  public Builder setTestDevice(boolean paramBoolean)
  {
    validate(fields()[4], Boolean.valueOf(paramBoolean));
    this.e = paramBoolean;
    fieldSetFlags()[4] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.SdkLogRequest.Builder
 * JD-Core Version:    0.6.2
 */