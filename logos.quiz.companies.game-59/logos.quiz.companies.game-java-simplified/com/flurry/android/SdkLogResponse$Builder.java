package com.flurry.android;

import java.util.List;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class SdkLogResponse$Builder extends SpecificRecordBuilderBase<SdkLogResponse>
  implements RecordBuilder<SdkLogResponse>
{
  private CharSequence a;
  private List<CharSequence> b;

  private SdkLogResponse$Builder()
  {
    super(SdkLogResponse.SCHEMA$);
  }

  public SdkLogResponse build()
  {
    try
    {
      SdkLogResponse localSdkLogResponse = new SdkLogResponse();
      CharSequence localCharSequence;
      if (fieldSetFlags()[0] != 0)
      {
        localCharSequence = this.a;
        localSdkLogResponse.a = localCharSequence;
        if (fieldSetFlags()[1] == 0)
          break label67;
      }
      label67: for (List localList = this.b; ; localList = (List)defaultValue(fields()[1]))
      {
        localSdkLogResponse.b = localList;
        return localSdkLogResponse;
        localCharSequence = (CharSequence)defaultValue(fields()[0]);
        break;
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearErrors()
  {
    this.b = null;
    fieldSetFlags()[1] = 0;
    return this;
  }

  public Builder clearResult()
  {
    this.a = null;
    fieldSetFlags()[0] = 0;
    return this;
  }

  public List<CharSequence> getErrors()
  {
    return this.b;
  }

  public CharSequence getResult()
  {
    return this.a;
  }

  public boolean hasErrors()
  {
    return fieldSetFlags()[1];
  }

  public boolean hasResult()
  {
    return fieldSetFlags()[0];
  }

  public Builder setErrors(List<CharSequence> paramList)
  {
    validate(fields()[1], paramList);
    this.b = paramList;
    fieldSetFlags()[1] = 1;
    return this;
  }

  public Builder setResult(CharSequence paramCharSequence)
  {
    validate(fields()[0], paramCharSequence);
    this.a = paramCharSequence;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.SdkLogResponse.Builder
 * JD-Core Version:    0.6.2
 */