package com.flurry.android;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.data.RecordBuilder;
import org.apache.avro.specific.SpecificRecordBuilderBase;

public class TestAds$Builder extends SpecificRecordBuilderBase<TestAds>
  implements RecordBuilder<TestAds>
{
  private int a;

  private TestAds$Builder(byte paramByte)
  {
    super(TestAds.SCHEMA$);
  }

  public TestAds build()
  {
    try
    {
      TestAds localTestAds = new TestAds();
      if (fieldSetFlags()[0] != 0);
      int i;
      for (int j = this.a; ; j = i)
      {
        localTestAds.a = j;
        return localTestAds;
        i = ((Integer)defaultValue(fields()[0])).intValue();
      }
    }
    catch (Exception localException)
    {
      throw new AvroRuntimeException(localException);
    }
  }

  public Builder clearAdspacePlacement()
  {
    fieldSetFlags()[0] = 0;
    return this;
  }

  public Integer getAdspacePlacement()
  {
    return Integer.valueOf(this.a);
  }

  public boolean hasAdspacePlacement()
  {
    return fieldSetFlags()[0];
  }

  public Builder setAdspacePlacement(int paramInt)
  {
    validate(fields()[0], Integer.valueOf(paramInt));
    this.a = paramInt;
    fieldSetFlags()[0] = 1;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.TestAds.Builder
 * JD-Core Version:    0.6.2
 */