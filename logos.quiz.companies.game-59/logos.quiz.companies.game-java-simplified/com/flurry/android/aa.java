package com.flurry.android;

import android.util.Log;
import com.mobclix.android.sdk.MobclixFullScreenAdView;
import com.mobclix.android.sdk.MobclixFullScreenAdViewListener;

final class aa
  implements MobclixFullScreenAdViewListener
{
  aa(ad paramad)
  {
  }

  public final String keywords()
  {
    Log.d("FlurryAgent", "Mobclix keyword callback.");
    return null;
  }

  public final void onDismissAd(MobclixFullScreenAdView paramMobclixFullScreenAdView)
  {
    this.a.onAdClosed(null);
    Log.d("FlurryAgent", "Mobclix Interstitial ad dismissed.");
  }

  public final void onFailedLoad(MobclixFullScreenAdView paramMobclixFullScreenAdView, int paramInt)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Mobclix Interstitial ad failed to load.");
  }

  public final void onFinishLoad(MobclixFullScreenAdView paramMobclixFullScreenAdView)
  {
    this.a.onAdFilled(null);
    Log.d("FlurryAgent", "Mobclix Interstitial ad successfully loaded.");
  }

  public final void onPresentAd(MobclixFullScreenAdView paramMobclixFullScreenAdView)
  {
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "Mobclix Interstitial ad successfully shown.");
  }

  public final String query()
  {
    Log.d("FlurryAgent", "Mobclix query callback.");
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.aa
 * JD-Core Version:    0.6.2
 */