package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdView;

final class ab extends AdNetworkView
{
  private static boolean e;

  ab(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc, paramAdCreative);
    try
    {
      ApplicationInfo localApplicationInfo2 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      localApplicationInfo1 = localApplicationInfo2;
      Bundle localBundle = localApplicationInfo1.metaData;
      sAdNetworkApiKey = localBundle.getString("com.flurry.inmobi.MY_APP_ID");
      e = localBundle.getBoolean("com.flurry.inmobi.test");
      if (sAdNetworkApiKey == null)
        Log.d("FlurryAgent", "com.flurry.inmobi.MY_APP_ID not set in manifest");
      setFocusable(true);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Cannot find manifest for app");
        ApplicationInfo localApplicationInfo1 = null;
      }
    }
  }

  public final void initLayout(Context paramContext)
  {
    if (this.fAdCreative.getFormat().equals("takeover"))
    {
      IMAdInterstitial localIMAdInterstitial = new IMAdInterstitial((Activity)paramContext, sAdNetworkApiKey);
      localIMAdInterstitial.setImAdInterstitialListener(new m(this));
      IMAdRequest localIMAdRequest1 = new IMAdRequest();
      if (e)
      {
        Log.d("FlurryAgent", "InMobi Interstitial set to Test Mode.");
        localIMAdRequest1.setTestMode(true);
      }
      localIMAdInterstitial.loadNewAd(localIMAdRequest1);
      return;
    }
    int i = this.fAdCreative.getHeight();
    int j = this.fAdCreative.getWidth();
    int k;
    if ((j >= 728) && (i >= 90))
    {
      Log.d("FlurryAgent", "Determined InMobi AdSize as 728x90");
      k = 11;
    }
    while (k != -1)
    {
      IMAdView localIMAdView = new IMAdView((Activity)paramContext, k, sAdNetworkApiKey);
      localIMAdView.setIMAdListener(new ac(this));
      addView(localIMAdView);
      IMAdRequest localIMAdRequest2 = new IMAdRequest();
      if (e)
      {
        Log.d("FlurryAgent", "InMobi AdView set to Test Mode.");
        localIMAdRequest2.setTestMode(true);
      }
      localIMAdView.setIMAdRequest(localIMAdRequest2);
      localIMAdView.setRefreshInterval(-1);
      localIMAdView.loadNewAd();
      return;
      if ((j >= 468) && (i >= 60))
      {
        Log.d("FlurryAgent", "Determined InMobi AdSize as 468x60");
        k = 12;
      }
      else if ((j >= 320) && (i >= 50))
      {
        Log.d("FlurryAgent", "Determined InMobi AdSize as 320x50");
        k = 15;
      }
      else if ((j >= 300) && (i >= 250))
      {
        Log.d("FlurryAgent", "Determined InMobi AdSize as 300x250");
        k = 10;
      }
      else if ((j >= 120) && (i >= 600))
      {
        Log.d("FlurryAgent", "Determined InMobi AdSize as 120x600");
        k = 13;
      }
      else
      {
        Log.d("FlurryAgent", "Could not find InMobi AdSize that matches size");
        k = -1;
      }
    }
    Log.d("FlurryAgent", "**********Could not load InMobi Ad");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ab
 * JD-Core Version:    0.6.2
 */