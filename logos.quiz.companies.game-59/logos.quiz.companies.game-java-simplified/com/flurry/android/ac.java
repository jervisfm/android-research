package com.flurry.android;

import android.util.Log;
import com.inmobi.androidsdk.IMAdListener;
import com.inmobi.androidsdk.IMAdRequest.ErrorCode;
import com.inmobi.androidsdk.IMAdView;

final class ac
  implements IMAdListener
{
  ac(ab paramab)
  {
  }

  public final void onAdRequestCompleted(IMAdView paramIMAdView)
  {
    this.a.onAdFilled(null);
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "InMobi imAdView ad request completed.");
  }

  public final void onAdRequestFailed(IMAdView paramIMAdView, IMAdRequest.ErrorCode paramErrorCode)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "InMobi imAdView ad request failed.");
  }

  public final void onDismissAdScreen(IMAdView paramIMAdView)
  {
    Log.d("FlurryAgent", "InMobi imAdView dismiss ad.");
  }

  public final void onShowAdScreen(IMAdView paramIMAdView)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "InMobi imAdView ad shown.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ac
 * JD-Core Version:    0.6.2
 */