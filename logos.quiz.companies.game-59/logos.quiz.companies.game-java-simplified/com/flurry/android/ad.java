package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixFullScreenAdView;
import com.mobclix.android.sdk.MobclixIABRectangleMAdView;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;

final class ad extends AdNetworkView
{
  ad(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc, paramAdCreative);
    try
    {
      ApplicationInfo localApplicationInfo2 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      localApplicationInfo1 = localApplicationInfo2;
      String str = localApplicationInfo1.metaData.getString("com.mobclix.APPLICATION_ID");
      sAdNetworkApiKey = str;
      if (str == null)
        Log.d("MobclixTestApp", "com.mobclix.APPLICATION_ID not set in manifest");
      setFocusable(true);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Cannot find manifest for app");
        ApplicationInfo localApplicationInfo1 = null;
      }
    }
  }

  public final void initLayout(Context paramContext)
  {
    if (this.fAdCreative.getFormat().equals("takeover"))
    {
      MobclixFullScreenAdView localMobclixFullScreenAdView = new MobclixFullScreenAdView((Activity)paramContext);
      localMobclixFullScreenAdView.addMobclixAdViewListener(new aa(this));
      localMobclixFullScreenAdView.requestAndDisplayAd();
      return;
    }
    int i = this.fAdCreative.getHeight();
    int j = this.fAdCreative.getWidth();
    Object localObject;
    if ((j >= 320) && (i >= 50))
    {
      Log.d("FlurryAgent", "Determined Mobclix AdSize as BANNER");
      localObject = new MobclixMMABannerXLAdView((Activity)paramContext);
    }
    while (true)
    {
      ((MobclixAdView)localObject).addMobclixAdViewListener(new ai(this));
      addView((View)localObject);
      ((MobclixAdView)localObject).setRefreshTime(-1L);
      return;
      if ((j >= 300) && (i >= 250))
      {
        Log.d("FlurryAgent", "Determined Mobclix AdSize as IAB_RECT");
        localObject = new MobclixIABRectangleMAdView((Activity)paramContext);
      }
      else
      {
        Log.d("FlurryAgent", "Could not find Mobclix AdSize that matches size");
        localObject = null;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ad
 * JD-Core Version:    0.6.2
 */