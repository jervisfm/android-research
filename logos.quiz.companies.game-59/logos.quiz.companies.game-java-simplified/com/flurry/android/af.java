package com.flurry.android;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

final class af extends AsyncTask<Void, Void, String>
{
  private final String a = getClass().getSimpleName();
  private Context b;
  private String c;

  public af(bf parambf, Context paramContext, String paramString)
  {
    this.b = paramContext;
    this.c = paramString;
  }

  private String a()
  {
    int i = 0;
    String str = null;
    HttpResponse localHttpResponse;
    int j;
    if (i < 5)
    {
      if (!Uri.parse(this.c).getScheme().equals("http"))
        break label235;
      if (bf.c(this.b) != true)
        break label215;
      localHttpResponse = z.a(bf.c(this.d), this.c, 10000, 15000, false);
      if (localHttpResponse == null)
        break label186;
      j = localHttpResponse.getStatusLine().getStatusCode();
      if (j != 200)
        break label118;
      new StringBuilder().append("Redirect URL found for: ").append(this.c).toString();
      str = this.c;
    }
    label118: boolean bool;
    label186: label215: label235: 
    do
    {
      return str;
      if ((j >= 300) && (j < 400))
      {
        new StringBuilder().append("NumRedirects: ").append(i + 1).toString();
        if (localHttpResponse.containsHeader("Location"))
          this.c = localHttpResponse.getFirstHeader("Location").getValue();
      }
      while (true)
      {
        i++;
        break;
        new StringBuilder().append("Bad Response status code: ").append(j).toString();
        return null;
        try
        {
          Thread.sleep(100L);
        }
        catch (InterruptedException localInterruptedException)
        {
          localInterruptedException.getMessage();
        }
      }
      bool = bf.a(this.b, this.c, "android.intent.action.VIEW");
      str = null;
    }
    while (!bool);
    return this.c;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.af
 * JD-Core Version:    0.6.2
 */