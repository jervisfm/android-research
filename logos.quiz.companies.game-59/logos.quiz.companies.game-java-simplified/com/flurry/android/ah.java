package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import com.millennialmedia.android.MMAdView;

final class ah extends AdNetworkView
{
  ah(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc, paramAdCreative);
    try
    {
      ApplicationInfo localApplicationInfo2 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      localApplicationInfo1 = localApplicationInfo2;
      String str = localApplicationInfo1.metaData.getString("com.flurry.millennial.MYAPID");
      sAdNetworkApiKey = str;
      if (str == null)
        Log.d("FlurryAgent", "com.flurry.millennial.MYAPID not set in manifest");
      setFocusable(true);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Cannot find manifest for app");
        ApplicationInfo localApplicationInfo1 = null;
      }
    }
  }

  public final void initLayout(Context paramContext)
  {
    if (this.fAdCreative.getFormat().equals("takeover"))
    {
      MMAdView localMMAdView1 = new MMAdView((Activity)paramContext, sAdNetworkApiKey, "MMFullScreenAdTransition", true, null);
      localMMAdView1.setId(1897808289);
      localMMAdView1.setListener(new at(this));
      localMMAdView1.fetch();
      return;
    }
    int i = this.fAdCreative.getHeight();
    int j = this.fAdCreative.getWidth();
    String str;
    if ((j >= 320) && (i >= 50))
    {
      Log.d("FlurryAgent", "Determined Millennial AdSize as MMBannerAdBottom");
      str = "MMBannerAdBottom";
    }
    while (str != null)
    {
      MMAdView localMMAdView2 = new MMAdView((Activity)paramContext, sAdNetworkApiKey, str, 0);
      localMMAdView2.setId(1897808289);
      localMMAdView2.setListener(new ag(this));
      addView(localMMAdView2);
      return;
      if ((j >= 300) && (i >= 250))
      {
        Log.d("FlurryAgent", "Determined Millennial AdSize as MMBannerAdRectangle");
        str = "MMBannerAdRectangle";
      }
      else
      {
        Log.d("FlurryAgent", "Could not find Millennial AdSize that matches size");
        str = null;
      }
    }
    Log.d("FlurryAgent", "**********Could not load Millennial Ad");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ah
 * JD-Core Version:    0.6.2
 */