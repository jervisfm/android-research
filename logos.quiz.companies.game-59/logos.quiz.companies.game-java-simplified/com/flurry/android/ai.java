package com.flurry.android;

import android.util.Log;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;

final class ai
  implements MobclixAdViewListener
{
  ai(ad paramad)
  {
  }

  public final String keywords()
  {
    Log.d("FlurryAgent", "Mobclix keyword callback.");
    return null;
  }

  public final void onAdClick(MobclixAdView paramMobclixAdView)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "Mobclix AdView ad clicked.");
  }

  public final void onCustomAdTouchThrough(MobclixAdView paramMobclixAdView, String paramString)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "Mobclix AdView custom ad clicked.");
  }

  public final void onFailedLoad(MobclixAdView paramMobclixAdView, int paramInt)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Mobclix AdView ad failed to load.");
  }

  public final boolean onOpenAllocationLoad(MobclixAdView paramMobclixAdView, int paramInt)
  {
    Log.d("FlurryAgent", "Mobclix AdView loaded an open allocation ad.");
    return true;
  }

  public final void onSuccessfulLoad(MobclixAdView paramMobclixAdView)
  {
    this.a.onAdFilled(null);
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "Mobclix AdView ad successfully loaded.");
  }

  public final String query()
  {
    Log.d("FlurryAgent", "Mobclix query callback.");
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ai
 * JD-Core Version:    0.6.2
 */