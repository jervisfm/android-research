package com.flurry.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.MediaController;
import android.widget.RelativeLayout.LayoutParams;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class ak extends p
  implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, aj
{
  private final String e = getClass().getSimpleName();
  private ProgressDialog f;
  private au g;
  private WebView h;
  private int i;
  private AdUnit j;
  private int k;
  private bc l;
  private List<AdFrame> m;
  private boolean n;
  private Map<String, AdUnit> o;
  private Map<String, bc> p;
  private Context q;
  private boolean r;
  private bf s;
  private x t;

  ak(Context paramContext, bf parambf, bc parambc, AdUnit paramAdUnit)
  {
    super(paramContext, parambf, parambc);
    this.q = paramContext;
    this.j = paramAdUnit;
    this.k = 0;
    this.l = parambc;
    this.m = this.j.c();
    this.r = false;
    if (this.j.d().intValue() == i1);
    while (true)
    {
      this.n = i1;
      if (this.n)
      {
        this.p = new HashMap();
        this.o = new HashMap();
        this.p.put(parambc.b(), parambc);
        this.o.put(((AdFrame)paramAdUnit.c().get(0)).f().toString(), paramAdUnit);
      }
      this.s = parambf;
      this.t = this.s.c;
      this.c = this.j;
      this.b = this.l;
      return;
      i1 = 0;
    }
  }

  private static String a(List<AdUnit> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("'{\"adComponents\":[");
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      localStringBuilder.append(((AdFrame)((AdUnit)localIterator.next()).c().get(0)).c().toString());
      if (localIterator.hasNext())
        localStringBuilder.append(",");
    }
    localStringBuilder.append("]}'");
    return localStringBuilder.toString();
  }

  private List<AdUnit> a(int paramInt1, int paramInt2)
  {
    List localList = this.t.a(this.j.a().toString(), paramInt1, paramInt2);
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      AdUnit localAdUnit = (AdUnit)localIterator.next();
      if (localAdUnit.c().size() > 0)
        this.o.put(((AdFrame)localAdUnit.c().get(0)).f().toString(), localAdUnit);
    }
    return localList;
  }

  private void a(String paramString, Map<String, String> paramMap)
  {
    new StringBuilder().append("fireEvent(event=").append(paramString).append(",params=").append(paramMap.toString()).toString();
    this.a.a(new bb(paramString, this.q, this.j, this.l, this.k), this);
  }

  private void a(String paramString, Map<String, String> paramMap, AdUnit paramAdUnit, bc parambc, int paramInt)
  {
    this.a.a(new bb(paramString, this.q, paramAdUnit, parambc, paramInt), this);
  }

  private void d()
  {
    if (c().equals("takeover"));
    ViewParent localViewParent2;
    do
    {
      ViewParent localViewParent1;
      do
      {
        try
        {
          ((Activity)this.q).finish();
          this.r = true;
          a("adClosed", Collections.emptyMap());
          return;
        }
        catch (ClassCastException localClassCastException2)
        {
          new StringBuilder().append("caught class cast exception: ").append(localClassCastException2).toString();
          return;
        }
        localViewParent1 = getParent();
      }
      while (localViewParent1 == null);
      localViewParent2 = localViewParent1.getParent();
    }
    while (localViewParent2 == null);
    try
    {
      ViewGroup localViewGroup = (ViewGroup)localViewParent2;
      bf localbf = this.s;
      Context localContext = this.q;
      this.j.a().toString();
      localbf.a(localContext, localViewGroup);
      return;
    }
    catch (ClassCastException localClassCastException1)
    {
      new StringBuilder().append("failed to remove view from holder: ").append(localClassCastException1.getMessage()).toString();
    }
  }

  private int e()
  {
    return ((AdFrame)this.m.get(this.k)).a().intValue();
  }

  private String f()
  {
    return ((AdFrame)this.m.get(this.k)).b().toString();
  }

  private AdFrame g()
  {
    return (AdFrame)this.m.get(this.k);
  }

  final void a()
  {
    if (e() == 3)
    {
      if ((this.f != null) && (this.f.isShowing()))
        this.f.dismiss();
      if ((this.g != null) && (this.g.isPlaying()))
        this.g.stopPlayback();
    }
  }

  // ERROR //
  public final void a(v paramv, bf parambf)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_1
    //   3: getfield 314	com/flurry/android/v:a	Ljava/lang/String;
    //   6: astore 4
    //   8: aload_1
    //   9: getfield 317	com/flurry/android/v:c	Lcom/flurry/android/bb;
    //   12: astore 5
    //   14: aload_1
    //   15: getfield 319	com/flurry/android/v:b	Ljava/util/Map;
    //   18: astore 6
    //   20: new 146	java/lang/StringBuilder
    //   23: dup
    //   24: invokespecial 147	java/lang/StringBuilder:<init>	()V
    //   27: ldc_w 321
    //   30: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: aload 4
    //   35: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: ldc_w 323
    //   41: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: invokevirtual 174	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   47: pop
    //   48: aload_0
    //   49: getfield 81	com/flurry/android/ak:r	Z
    //   52: ifeq +4 -> 56
    //   55: return
    //   56: aload 4
    //   58: ldc_w 325
    //   61: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   64: ifeq +82 -> 146
    //   67: aload_0
    //   68: invokevirtual 265	com/flurry/android/ak:getParent	()Landroid/view/ViewParent;
    //   71: checkcast 327	com/flurry/android/u
    //   74: astore 38
    //   76: aload 38
    //   78: ifnull -23 -> 55
    //   81: aload_1
    //   82: getfield 319	com/flurry/android/v:b	Ljava/util/Map;
    //   85: ldc_w 329
    //   88: invokeinterface 228 2 0
    //   93: checkcast 190	java/lang/String
    //   96: invokestatic 333	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   99: istore 42
    //   101: iload 42
    //   103: istore 40
    //   105: iload 40
    //   107: sipush 1000
    //   110: imul
    //   111: istore 41
    //   113: aload 38
    //   115: iload 41
    //   117: invokevirtual 336	com/flurry/android/u:a	(I)V
    //   120: aload_0
    //   121: getfield 216	com/flurry/android/ak:a	Lcom/flurry/android/bf;
    //   124: getfield 339	com/flurry/android/bf:d	Lcom/flurry/android/be;
    //   127: aload 38
    //   129: invokevirtual 344	com/flurry/android/be:a	(Lcom/flurry/android/u;)V
    //   132: return
    //   133: astore 36
    //   135: aload 36
    //   137: invokevirtual 345	java/lang/ClassCastException:toString	()Ljava/lang/String;
    //   140: pop
    //   141: aload_0
    //   142: invokespecial 347	com/flurry/android/ak:d	()V
    //   145: return
    //   146: aload 4
    //   148: ldc_w 349
    //   151: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   154: ifeq +156 -> 310
    //   157: iconst_1
    //   158: aload_0
    //   159: getfield 69	com/flurry/android/ak:k	I
    //   162: iadd
    //   163: istore 30
    //   165: aload 6
    //   167: ldc_w 351
    //   170: invokeinterface 228 2 0
    //   175: checkcast 190	java/lang/String
    //   178: astore 31
    //   180: aload 31
    //   182: ifnull +121 -> 303
    //   185: aload 31
    //   187: ldc_w 352
    //   190: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   193: ifeq +49 -> 242
    //   196: iconst_1
    //   197: aload_0
    //   198: getfield 69	com/flurry/android/ak:k	I
    //   201: iadd
    //   202: istore 32
    //   204: iload 32
    //   206: aload_0
    //   207: getfield 69	com/flurry/android/ak:k	I
    //   210: if_icmpeq -155 -> 55
    //   213: iload 32
    //   215: aload_0
    //   216: getfield 79	com/flurry/android/ak:m	Ljava/util/List;
    //   219: invokeinterface 185 1 0
    //   224: if_icmpge -169 -> 55
    //   227: aload_0
    //   228: iload 32
    //   230: putfield 69	com/flurry/android/ak:k	I
    //   233: aload_0
    //   234: aload_0
    //   235: getfield 65	com/flurry/android/ak:q	Landroid/content/Context;
    //   238: invokevirtual 356	com/flurry/android/ak:initLayout	(Landroid/content/Context;)V
    //   241: return
    //   242: aload 31
    //   244: ldc_w 358
    //   247: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   250: ifeq +12 -> 262
    //   253: aload_0
    //   254: getfield 69	com/flurry/android/ak:k	I
    //   257: istore 32
    //   259: goto -55 -> 204
    //   262: aload 31
    //   264: invokestatic 333	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   267: istore 35
    //   269: iload 35
    //   271: istore 32
    //   273: goto -69 -> 204
    //   276: astore 33
    //   278: new 146	java/lang/StringBuilder
    //   281: dup
    //   282: invokespecial 147	java/lang/StringBuilder:<init>	()V
    //   285: ldc_w 360
    //   288: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   291: aload 33
    //   293: invokevirtual 361	java/lang/NumberFormatException:getMessage	()Ljava/lang/String;
    //   296: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: invokevirtual 174	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   302: pop
    //   303: iload 30
    //   305: istore 32
    //   307: goto -103 -> 204
    //   310: aload 4
    //   312: ldc_w 363
    //   315: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   318: ifeq +8 -> 326
    //   321: aload_0
    //   322: invokespecial 347	com/flurry/android/ak:d	()V
    //   325: return
    //   326: aload 4
    //   328: ldc_w 365
    //   331: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   334: ifeq +175 -> 509
    //   337: new 367	android/app/AlertDialog$Builder
    //   340: dup
    //   341: aload_0
    //   342: getfield 65	com/flurry/android/ak:q	Landroid/content/Context;
    //   345: invokespecial 369	android/app/AlertDialog$Builder:<init>	(Landroid/content/Context;)V
    //   348: astore 8
    //   350: aload 6
    //   352: ldc_w 371
    //   355: invokeinterface 374 2 0
    //   360: ifeq +125 -> 485
    //   363: aload 6
    //   365: ldc_w 371
    //   368: invokeinterface 228 2 0
    //   373: checkcast 190	java/lang/String
    //   376: astore 9
    //   378: aload 6
    //   380: ldc_w 376
    //   383: invokeinterface 374 2 0
    //   388: ifeq +105 -> 493
    //   391: aload 6
    //   393: ldc_w 376
    //   396: invokeinterface 228 2 0
    //   401: checkcast 190	java/lang/String
    //   404: astore 10
    //   406: aload 6
    //   408: ldc_w 378
    //   411: invokeinterface 374 2 0
    //   416: ifeq +85 -> 501
    //   419: aload 6
    //   421: ldc_w 378
    //   424: invokeinterface 228 2 0
    //   429: checkcast 190	java/lang/String
    //   432: astore 11
    //   434: aload 8
    //   436: aload 9
    //   438: invokevirtual 382	android/app/AlertDialog$Builder:setMessage	(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    //   441: iconst_0
    //   442: invokevirtual 386	android/app/AlertDialog$Builder:setCancelable	(Z)Landroid/app/AlertDialog$Builder;
    //   445: aload 10
    //   447: new 388	com/flurry/android/ap
    //   450: dup
    //   451: aload_0
    //   452: aload 5
    //   454: invokespecial 391	com/flurry/android/ap:<init>	(Lcom/flurry/android/ak;Lcom/flurry/android/bb;)V
    //   457: invokevirtual 395	android/app/AlertDialog$Builder:setPositiveButton	(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    //   460: aload 11
    //   462: new 397	com/flurry/android/am
    //   465: dup
    //   466: aload_0
    //   467: aload 5
    //   469: invokespecial 398	com/flurry/android/am:<init>	(Lcom/flurry/android/ak;Lcom/flurry/android/bb;)V
    //   472: invokevirtual 401	android/app/AlertDialog$Builder:setNegativeButton	(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    //   475: pop
    //   476: aload 8
    //   478: invokevirtual 405	android/app/AlertDialog$Builder:create	()Landroid/app/AlertDialog;
    //   481: invokevirtual 410	android/app/AlertDialog:show	()V
    //   484: return
    //   485: ldc_w 412
    //   488: astore 9
    //   490: goto -112 -> 378
    //   493: ldc_w 414
    //   496: astore 10
    //   498: goto -92 -> 406
    //   501: ldc_w 416
    //   504: astore 11
    //   506: goto -72 -> 434
    //   509: aload 4
    //   511: ldc_w 418
    //   514: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   517: ifeq +80 -> 597
    //   520: aload 6
    //   522: ldc_w 420
    //   525: invokeinterface 374 2 0
    //   530: istore 26
    //   532: iconst_0
    //   533: istore 27
    //   535: iload 26
    //   537: ifeq +35 -> 572
    //   540: aload 6
    //   542: ldc_w 420
    //   545: invokeinterface 228 2 0
    //   550: checkcast 190	java/lang/String
    //   553: ldc_w 422
    //   556: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   559: istore 29
    //   561: iconst_0
    //   562: istore 27
    //   564: iload 29
    //   566: ifeq +6 -> 572
    //   569: iload_3
    //   570: istore 27
    //   572: aload 6
    //   574: ldc_w 420
    //   577: invokeinterface 425 2 0
    //   582: pop
    //   583: aload_0
    //   584: aload 5
    //   586: getfield 426	com/flurry/android/bb:a	Ljava/lang/String;
    //   589: iload 27
    //   591: aload 6
    //   593: invokevirtual 429	com/flurry/android/ak:a	(Ljava/lang/String;ZLjava/util/Map;)V
    //   596: return
    //   597: aload 4
    //   599: ldc_w 431
    //   602: invokevirtual 202	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   605: ifeq +332 -> 937
    //   608: aload 6
    //   610: ldc_w 433
    //   613: invokeinterface 374 2 0
    //   618: ifeq +337 -> 955
    //   621: aload 6
    //   623: ldc_w 435
    //   626: invokeinterface 374 2 0
    //   631: ifeq +324 -> 955
    //   634: aload 6
    //   636: ldc_w 433
    //   639: invokeinterface 228 2 0
    //   644: checkcast 190	java/lang/String
    //   647: invokestatic 333	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   650: istore 24
    //   652: aload 6
    //   654: ldc_w 435
    //   657: invokeinterface 228 2 0
    //   662: checkcast 190	java/lang/String
    //   665: invokestatic 333	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   668: istore 25
    //   670: iload 25
    //   672: istore 13
    //   674: iload 24
    //   676: istore_3
    //   677: aload_0
    //   678: getfield 67	com/flurry/android/ak:j	Lcom/flurry/android/AdUnit;
    //   681: invokevirtual 177	com/flurry/android/AdUnit:a	()Ljava/lang/CharSequence;
    //   684: invokevirtual 127	java/lang/Object:toString	()Ljava/lang/String;
    //   687: pop
    //   688: aload_0
    //   689: iload_3
    //   690: iload 13
    //   692: invokespecial 437	com/flurry/android/ak:a	(II)Ljava/util/List;
    //   695: astore 15
    //   697: aload 15
    //   699: invokeinterface 185 1 0
    //   704: ifle +222 -> 926
    //   707: new 146	java/lang/StringBuilder
    //   710: dup
    //   711: invokespecial 147	java/lang/StringBuilder:<init>	()V
    //   714: ldc_w 439
    //   717: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   720: aload 15
    //   722: invokevirtual 127	java/lang/Object:toString	()Ljava/lang/String;
    //   725: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   728: invokevirtual 174	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   731: pop
    //   732: aload_0
    //   733: getfield 67	com/flurry/android/ak:j	Lcom/flurry/android/AdUnit;
    //   736: invokevirtual 177	com/flurry/android/AdUnit:a	()Ljava/lang/CharSequence;
    //   739: invokevirtual 127	java/lang/Object:toString	()Ljava/lang/String;
    //   742: pop
    //   743: aload 15
    //   745: invokestatic 441	com/flurry/android/ak:a	(Ljava/util/List;)Ljava/lang/String;
    //   748: astore 18
    //   750: aload_0
    //   751: getfield 143	com/flurry/android/ak:h	Landroid/webkit/WebView;
    //   754: new 146	java/lang/StringBuilder
    //   757: dup
    //   758: invokespecial 147	java/lang/StringBuilder:<init>	()V
    //   761: ldc_w 443
    //   764: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   767: aload 18
    //   769: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   772: ldc_w 445
    //   775: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   778: invokevirtual 174	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   781: invokevirtual 451	android/webkit/WebView:loadUrl	(Ljava/lang/String;)V
    //   784: aload_0
    //   785: getfield 143	com/flurry/android/ak:h	Landroid/webkit/WebView;
    //   788: ldc_w 453
    //   791: invokevirtual 451	android/webkit/WebView:loadUrl	(Ljava/lang/String;)V
    //   794: aload 15
    //   796: invokeinterface 157 1 0
    //   801: astore 19
    //   803: aload 19
    //   805: invokeinterface 163 1 0
    //   810: ifeq +107 -> 917
    //   813: aload 19
    //   815: invokeinterface 167 1 0
    //   820: checkcast 73	com/flurry/android/AdUnit
    //   823: astore 20
    //   825: new 95	java/util/HashMap
    //   828: dup
    //   829: invokespecial 98	java/util/HashMap:<init>	()V
    //   832: astore 21
    //   834: aload 21
    //   836: ldc_w 455
    //   839: aload 20
    //   841: invokevirtual 77	com/flurry/android/AdUnit:c	()Ljava/util/List;
    //   844: iconst_0
    //   845: invokeinterface 119 2 0
    //   850: checkcast 121	com/flurry/android/AdFrame
    //   853: invokevirtual 124	com/flurry/android/AdFrame:f	()Ljava/lang/CharSequence;
    //   856: invokevirtual 127	java/lang/Object:toString	()Ljava/lang/String;
    //   859: invokeinterface 113 3 0
    //   864: pop
    //   865: aload_0
    //   866: ldc_w 457
    //   869: aload 21
    //   871: aload 20
    //   873: aload_0
    //   874: getfield 100	com/flurry/android/ak:p	Ljava/util/Map;
    //   877: aload 20
    //   879: invokevirtual 77	com/flurry/android/AdUnit:c	()Ljava/util/List;
    //   882: iconst_0
    //   883: invokeinterface 119 2 0
    //   888: checkcast 121	com/flurry/android/AdFrame
    //   891: invokevirtual 124	com/flurry/android/AdFrame:f	()Ljava/lang/CharSequence;
    //   894: invokeinterface 228 2 0
    //   899: checkcast 104	com/flurry/android/bc
    //   902: iconst_0
    //   903: invokespecial 210	com/flurry/android/ak:a	(Ljava/lang/String;Ljava/util/Map;Lcom/flurry/android/AdUnit;Lcom/flurry/android/bc;I)V
    //   906: goto -103 -> 803
    //   909: astore 23
    //   911: iconst_3
    //   912: istore 13
    //   914: goto -237 -> 677
    //   917: aload_0
    //   918: aload_0
    //   919: getfield 143	com/flurry/android/ak:h	Landroid/webkit/WebView;
    //   922: invokevirtual 461	com/flurry/android/ak:addView	(Landroid/view/View;)V
    //   925: return
    //   926: aload_0
    //   927: ldc_w 463
    //   930: invokestatic 256	java/util/Collections:emptyMap	()Ljava/util/Map;
    //   933: invokespecial 206	com/flurry/android/ak:a	(Ljava/lang/String;Ljava/util/Map;)V
    //   936: return
    //   937: aload_0
    //   938: getfield 216	com/flurry/android/ak:a	Lcom/flurry/android/bf;
    //   941: aload_1
    //   942: aload_2
    //   943: invokevirtual 465	com/flurry/android/bf:a	(Lcom/flurry/android/v;Lcom/flurry/android/bf;)V
    //   946: return
    //   947: astore 39
    //   949: iconst_0
    //   950: istore 40
    //   952: goto -847 -> 105
    //   955: iconst_3
    //   956: istore 13
    //   958: goto -281 -> 677
    //
    // Exception table:
    //   from	to	target	type
    //   67	76	133	java/lang/ClassCastException
    //   81	101	133	java/lang/ClassCastException
    //   113	132	133	java/lang/ClassCastException
    //   262	269	276	java/lang/NumberFormatException
    //   634	670	909	java/lang/NumberFormatException
    //   81	101	947	java/lang/NumberFormatException
  }

  final String b()
  {
    return ((AdFrame)this.m.get(this.k)).c().toString();
  }

  final String c()
  {
    return ((AdFrame)this.m.get(this.k)).d().d().toString();
  }

  public final void initLayout(Context paramContext)
  {
    new StringBuilder().append("initLayout, current frame index: ").append(this.k).append(", binding: ").append(e()).append(", display: ").append(f()).append(", content: ").append(b()).append(", format: ").append(c()).toString();
    removeAllViews();
    int i1 = e();
    try
    {
      Activity localActivity = (Activity)this.q;
      this.i = localActivity.getRequestedOrientation();
      int i5;
      switch (i1)
      {
      case 2:
      default:
        i5 = this.i;
      case 1:
      case 3:
      }
      while (true)
      {
        localActivity.setRequestedOrientation(i5);
        setFocusable(true);
        setFocusableInTouchMode(true);
        switch (e())
        {
        default:
          a("renderFailed", Collections.emptyMap());
          return;
          i5 = 4;
          continue;
          i5 = 0;
        case 3:
        case 1:
        case 2:
        }
      }
    }
    catch (ClassCastException localClassCastException)
    {
      while (true)
        localClassCastException.toString();
      if (this.g == null)
      {
        this.g = new au(paramContext);
        this.g.setOnPreparedListener(this);
        this.g.setOnCompletionListener(this);
        this.g.setOnErrorListener(this);
        this.g.setMediaController(new MediaController(paramContext));
      }
      this.g.setVideoURI(Uri.parse(f()));
      addView(this.g);
      this.f = new ProgressDialog(paramContext);
      this.f.setProgressStyle(0);
      this.f.setMessage("Loading...");
      this.f.setCancelable(false);
      this.f.show();
      return;
    }
    if (this.h == null)
    {
      this.h = new WebView(paramContext);
      this.h.setWebViewClient(new ar(this));
      this.h.getSettings().setJavaScriptEnabled(true);
      this.h.setVerticalScrollBarEnabled(false);
      this.h.setHorizontalScrollBarEnabled(false);
      this.h.setBackgroundColor(0);
    }
    if (e() == 1)
    {
      String str = f();
      new StringBuilder().append("calling WebView.loadUrl ").append(str).toString();
      this.h.loadUrl(str);
    }
    while (true)
    {
      int i2 = g().d().a().intValue();
      int i3 = z.b(this.q, i2);
      int i4 = g().d().b().intValue();
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(i3, z.b(this.q, i4));
      this.h.setLayoutParams(localLayoutParams);
      return;
      if (e() == 2)
      {
        new StringBuilder().append("calling WebView.loadData: ").append(f()).toString();
        this.h.loadDataWithBaseURL("base://url/", f(), "text/html", "utf-8", "base://url/");
      }
    }
  }

  public final void onCompletion(MediaPlayer paramMediaPlayer)
  {
    a("videoCompleted", Collections.emptyMap());
  }

  public final boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    a("renderFailed", Collections.emptyMap());
    return true;
  }

  public final boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    new StringBuilder().append("onKeyDown ").append(paramInt).toString();
    if (paramInt == 4)
    {
      a("adWillClose", Collections.emptyMap());
      return true;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }

  public final void onPrepared(MediaPlayer paramMediaPlayer)
  {
    if (e() == 3)
    {
      this.f.dismiss();
      this.g.start();
      a("rendered", Collections.emptyMap());
      a("videoStarted", Collections.emptyMap());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ak
 * JD-Core Version:    0.6.2
 */