package com.flurry.android;

import java.util.Map;

final class al
{
  private int a;
  private String b;
  private Map<String, String> c;
  private long d;
  private boolean e;
  private long f;

  public al(int paramInt, String paramString, Map<String, String> paramMap, long paramLong, boolean paramBoolean)
  {
    this.a = paramInt;
    this.b = paramString;
    this.c = paramMap;
    this.d = paramLong;
    this.e = paramBoolean;
  }

  public final void a(long paramLong)
  {
    this.f = (paramLong - this.d);
    bd.a("FlurryAgent", "Ended event '" + this.b + "' (" + this.d + ") after " + this.f + "ms");
  }

  public final boolean a(String paramString)
  {
    return (this.e) && (this.f == 0L) && (this.b.equals(paramString));
  }

  // ERROR //
  public final byte[] a()
  {
    // Byte code:
    //   0: new 75	java/io/ByteArrayOutputStream
    //   3: dup
    //   4: invokespecial 76	java/io/ByteArrayOutputStream:<init>	()V
    //   7: astore_1
    //   8: new 78	java/io/DataOutputStream
    //   11: dup
    //   12: aload_1
    //   13: invokespecial 81	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   16: astore_2
    //   17: aload_2
    //   18: aload_0
    //   19: getfield 23	com/flurry/android/al:a	I
    //   22: invokevirtual 85	java/io/DataOutputStream:writeShort	(I)V
    //   25: aload_2
    //   26: aload_0
    //   27: getfield 25	com/flurry/android/al:b	Ljava/lang/String;
    //   30: invokevirtual 89	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   33: aload_0
    //   34: getfield 27	com/flurry/android/al:c	Ljava/util/Map;
    //   37: ifnonnull +41 -> 78
    //   40: aload_2
    //   41: iconst_0
    //   42: invokevirtual 85	java/io/DataOutputStream:writeShort	(I)V
    //   45: aload_2
    //   46: aload_0
    //   47: getfield 29	com/flurry/android/al:d	J
    //   50: invokevirtual 92	java/io/DataOutputStream:writeLong	(J)V
    //   53: aload_2
    //   54: aload_0
    //   55: getfield 34	com/flurry/android/al:f	J
    //   58: invokevirtual 92	java/io/DataOutputStream:writeLong	(J)V
    //   61: aload_2
    //   62: invokevirtual 95	java/io/DataOutputStream:flush	()V
    //   65: aload_1
    //   66: invokevirtual 98	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   69: astore 9
    //   71: aload_2
    //   72: invokestatic 103	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   75: aload 9
    //   77: areturn
    //   78: aload_2
    //   79: aload_0
    //   80: getfield 27	com/flurry/android/al:c	Ljava/util/Map;
    //   83: invokeinterface 109 1 0
    //   88: invokevirtual 85	java/io/DataOutputStream:writeShort	(I)V
    //   91: aload_0
    //   92: getfield 27	com/flurry/android/al:c	Ljava/util/Map;
    //   95: invokeinterface 113 1 0
    //   100: invokeinterface 119 1 0
    //   105: astore 7
    //   107: aload 7
    //   109: invokeinterface 125 1 0
    //   114: ifeq -69 -> 45
    //   117: aload 7
    //   119: invokeinterface 129 1 0
    //   124: checkcast 131	java/util/Map$Entry
    //   127: astore 8
    //   129: aload_2
    //   130: aload 8
    //   132: invokeinterface 134 1 0
    //   137: checkcast 66	java/lang/String
    //   140: invokestatic 137	com/flurry/android/z:a	(Ljava/lang/String;)Ljava/lang/String;
    //   143: invokevirtual 89	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   146: aload_2
    //   147: aload 8
    //   149: invokeinterface 140 1 0
    //   154: checkcast 66	java/lang/String
    //   157: invokestatic 137	com/flurry/android/z:a	(Ljava/lang/String;)Ljava/lang/String;
    //   160: invokevirtual 89	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   163: goto -56 -> 107
    //   166: astore 4
    //   168: aload_2
    //   169: astore 5
    //   171: iconst_0
    //   172: newarray byte
    //   174: astore 6
    //   176: aload 5
    //   178: invokestatic 103	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   181: aload 6
    //   183: areturn
    //   184: astore 11
    //   186: aconst_null
    //   187: astore_2
    //   188: aload 11
    //   190: astore_3
    //   191: aload_2
    //   192: invokestatic 103	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   195: aload_3
    //   196: athrow
    //   197: astore_3
    //   198: goto -7 -> 191
    //   201: astore_3
    //   202: aload 5
    //   204: astore_2
    //   205: goto -14 -> 191
    //   208: astore 10
    //   210: aconst_null
    //   211: astore 5
    //   213: goto -42 -> 171
    //
    // Exception table:
    //   from	to	target	type
    //   17	45	166	java/io/IOException
    //   45	71	166	java/io/IOException
    //   78	107	166	java/io/IOException
    //   107	163	166	java/io/IOException
    //   0	17	184	finally
    //   17	45	197	finally
    //   45	71	197	finally
    //   78	107	197	finally
    //   107	163	197	finally
    //   171	176	201	finally
    //   0	17	208	java/io/IOException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.al
 * JD-Core Version:    0.6.2
 */