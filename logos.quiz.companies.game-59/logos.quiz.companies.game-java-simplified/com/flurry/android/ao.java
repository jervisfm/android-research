package com.flurry.android;

import java.io.DataInput;
import java.io.DataOutput;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class ao
{
  private static List<String> a = Arrays.asList(new String[] { "requested", "filled", "unfilled", "rendered", "clicked", "videoStart", "videoCompleted", "videoProgressed", "sentToUrl", "adClosed" });
  private final String b;
  private final boolean c;
  private final long d;
  private final Map<String, String> e;

  ao(DataInput paramDataInput)
  {
    this.b = paramDataInput.readUTF();
    this.c = paramDataInput.readBoolean();
    this.d = paramDataInput.readLong();
    this.e = new HashMap();
    int i = paramDataInput.readShort();
    for (int j = 0; j < i; j = (short)(j + 1))
      this.e.put(paramDataInput.readUTF(), paramDataInput.readUTF());
  }

  ao(String paramString, boolean paramBoolean, long paramLong, Map<String, String> paramMap)
  {
    if (!a.contains(paramString))
      bd.a("FlurryAgent", "AdEvent initialized with unrecognized type: " + paramString);
    this.b = paramString;
    this.c = paramBoolean;
    this.d = paramLong;
    if (paramMap == null)
    {
      this.e = new HashMap();
      return;
    }
    this.e = paramMap;
  }

  final String a()
  {
    return this.b;
  }

  final void a(DataOutput paramDataOutput)
  {
    paramDataOutput.writeUTF(this.b);
    paramDataOutput.writeBoolean(this.c);
    paramDataOutput.writeLong(this.d);
    paramDataOutput.writeShort(this.e.size());
    Iterator localIterator = this.e.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      paramDataOutput.writeUTF((String)localEntry.getKey());
      paramDataOutput.writeUTF((String)localEntry.getValue());
    }
  }

  final boolean b()
  {
    return this.c;
  }

  final long c()
  {
    return this.d;
  }

  final Map<String, String> d()
  {
    return this.e;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ao
 * JD-Core Version:    0.6.2
 */