package com.flurry.android;

import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdViewListener;

final class aq
  implements JtAdViewListener
{
  aq(g paramg)
  {
  }

  public final void onAdError(JtAdView paramJtAdView, int paramInt1, int paramInt2)
  {
    Log.d("FlurryAgent", "Jumptap Interstitial error.");
  }

  public final void onFocusChange(JtAdView paramJtAdView, int paramInt, boolean paramBoolean)
  {
    Log.d("FlurryAgent", "Jumptap Interstitial focus changed.");
  }

  public final void onInterstitialDismissed(JtAdView paramJtAdView, int paramInt)
  {
    this.a.onAdClosed(null);
    Log.d("FlurryAgent", "Jumptap Interstitial dismissed.");
  }

  public final void onNewAd(JtAdView paramJtAdView, int paramInt, String paramString)
  {
    this.a.onAdFilled(null);
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "Jumptap Interstitial new ad.");
  }

  public final void onNoAdFound(JtAdView paramJtAdView, int paramInt)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Jumptap Interstitial no ad found.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.aq
 * JD-Core Version:    0.6.2
 */