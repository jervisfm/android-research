package com.flurry.android;

import android.util.Log;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdView.MMAdListener;

final class at
  implements MMAdView.MMAdListener
{
  at(ah paramah)
  {
  }

  public final void MMAdCachingCompleted(MMAdView paramMMAdView, boolean paramBoolean)
  {
    Log.d("FlurryAgent", "Millennial Interstitial caching completed.");
  }

  public final void MMAdClickedToNewBrowser(MMAdView paramMMAdView)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "Millennial Interstitial clicked to new browser.");
  }

  public final void MMAdClickedToOverlay(MMAdView paramMMAdView)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "Millennial Interstitial clicked to overlay.");
  }

  public final void MMAdFailed(MMAdView paramMMAdView)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Millennial Interstitial failed to load ad.");
  }

  public final void MMAdOverlayLaunched(MMAdView paramMMAdView)
  {
    Log.d("FlurryAgent", "Millennial Interstitial overlay launched.");
  }

  public final void MMAdRequestIsCaching(MMAdView paramMMAdView)
  {
    Log.d("FlurryAgent", "Millennial Interstitial request is caching.");
  }

  public final void MMAdReturned(MMAdView paramMMAdView)
  {
    paramMMAdView.display();
    this.a.onAdFilled(null);
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "Millennial In returned ad.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.at
 * JD-Core Version:    0.6.2
 */