package com.flurry.android;

import android.content.Context;
import android.widget.VideoView;

final class au extends VideoView
{
  public au(Context paramContext)
  {
    super(paramContext);
    setFocusable(true);
    setFocusableInTouchMode(true);
  }

  public final void seekTo(int paramInt)
  {
    if (paramInt < getCurrentPosition())
      super.seekTo(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.au
 * JD-Core Version:    0.6.2
 */