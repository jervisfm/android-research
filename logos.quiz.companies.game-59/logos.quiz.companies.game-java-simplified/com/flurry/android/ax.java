package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

final class ax extends RelativeLayout
  implements View.OnClickListener
{
  private final String a = getClass().getSimpleName();
  private WebView b;
  private ImageView c;
  private ImageView d;
  private ImageView e;

  public ax(Context paramContext, String paramString)
  {
    super(paramContext);
    this.b = new WebView(paramContext);
    this.b.getSettings().setJavaScriptEnabled(true);
    this.b.setWebViewClient(new b(this));
    this.b.loadUrl(paramString);
    this.c = new ImageView(paramContext);
    this.c.setId(0);
    this.c.setImageDrawable(getResources().getDrawable(17301560));
    this.c.setOnClickListener(this);
    this.d = new ImageView(paramContext);
    this.d.setId(1);
    this.d.setImageDrawable(getResources().getDrawable(17301580));
    this.d.setOnClickListener(this);
    this.e = new ImageView(paramContext);
    this.e.setId(2);
    this.e.setImageDrawable(getResources().getDrawable(17301565));
    this.e.setOnClickListener(this);
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    addView(this.b);
    RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams1.addRule(14);
    addView(this.c, localLayoutParams1);
    RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams2.addRule(9);
    addView(this.d, localLayoutParams2);
    RelativeLayout.LayoutParams localLayoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams3.addRule(11);
    addView(this.e, localLayoutParams3);
  }

  public final void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 0:
      try
      {
        ((Activity)getContext()).finish();
        return;
      }
      catch (ClassCastException localClassCastException)
      {
        localClassCastException.toString();
        return;
      }
    case 2:
      this.b.goForward();
      return;
    case 1:
    }
    this.b.goBack();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ax
 * JD-Core Version:    0.6.2
 */