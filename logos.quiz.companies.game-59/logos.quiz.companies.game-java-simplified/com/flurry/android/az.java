package com.flurry.android;

import android.util.Log;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;

final class az
  implements AdListener
{
  az(y paramy)
  {
  }

  public final void onDismissScreen(Ad paramAd)
  {
    Log.i("FlurryAgent", "Admob AdView dismissed from screen.");
  }

  public final void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Admob AdView failed to receive ad.");
  }

  public final void onLeaveApplication(Ad paramAd)
  {
    this.a.onAdClicked(null);
    Log.i("FlurryAgent", "Admob AdView leave application.");
  }

  public final void onPresentScreen(Ad paramAd)
  {
    Log.d("FlurryAgent", "Admob AdView present on screen.");
  }

  public final void onReceiveAd(Ad paramAd)
  {
    this.a.onAdFilled(null);
    this.a.onAdShown(null);
    Log.d("FlurryAgent", "Admob AdView received ad.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.az
 * JD-Core Version:    0.6.2
 */