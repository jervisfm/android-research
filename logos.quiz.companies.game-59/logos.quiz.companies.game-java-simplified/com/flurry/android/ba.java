package com.flurry.android;

import java.net.Socket;
import java.security.KeyStore;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

final class ba extends org.apache.http.conn.ssl.SSLSocketFactory
{
  private SSLContext a = SSLContext.getInstance("TLS");

  public ba(ay paramay, KeyStore paramKeyStore)
  {
    super(paramKeyStore);
    n localn = new n();
    this.a.init(null, new TrustManager[] { localn }, null);
  }

  public final Socket createSocket()
  {
    return this.a.getSocketFactory().createSocket();
  }

  public final Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
  {
    return this.a.getSocketFactory().createSocket(paramSocket, paramString, paramInt, paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.ba
 * JD-Core Version:    0.6.2
 */