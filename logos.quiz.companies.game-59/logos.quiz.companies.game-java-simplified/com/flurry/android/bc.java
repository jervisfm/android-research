package com.flurry.android;

import java.io.DataInput;
import java.io.DataOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class bc
{
  private static int a = 1;
  private final int b;
  private final long c;
  private final String d;
  private List<ao> e;

  bc(long paramLong, String paramString)
  {
    int i = a;
    a = i + 1;
    this.b = i;
    this.c = paramLong;
    this.d = paramString;
    this.e = new ArrayList();
  }

  bc(DataInput paramDataInput)
  {
    this.b = paramDataInput.readInt();
    this.c = paramDataInput.readLong();
    this.d = paramDataInput.readUTF();
    this.e = new ArrayList();
    int i = paramDataInput.readShort();
    for (int j = 0; j < i; j = (short)(j + 1))
      this.e.add(new ao(paramDataInput));
  }

  final int a()
  {
    return this.b;
  }

  final void a(ao paramao)
  {
    this.e.add(paramao);
  }

  final void a(DataOutput paramDataOutput)
  {
    paramDataOutput.writeInt(this.b);
    paramDataOutput.writeLong(this.c);
    paramDataOutput.writeUTF(this.d);
    paramDataOutput.writeShort(this.e.size());
    Iterator localIterator = this.e.iterator();
    while (localIterator.hasNext())
      ((ao)localIterator.next()).a(paramDataOutput);
  }

  final String b()
  {
    return this.d;
  }

  final long c()
  {
    return this.c;
  }

  final List<ao> d()
  {
    return this.e;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.bc
 * JD-Core Version:    0.6.2
 */