package com.flurry.android;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class be
{
  private static String a = "FlurryAgent";
  private static String b = "FlurryBannerTag";
  private Map<Context, List<u>> c = new HashMap();
  private Context d;

  be(Context paramContext)
  {
    this.d = paramContext;
  }

  final as a(u paramu, long paramLong)
  {
    try
    {
      as localas = paramu.a(paramLong);
      return localas;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final u a(Context paramContext, View paramView, String paramString)
  {
    try
    {
      List localList = (List)this.c.get(paramContext);
      u localu;
      if (localList == null)
        localu = null;
      while (true)
      {
        return localu;
        Iterator localIterator = localList.iterator();
        while (true)
          if (localIterator.hasNext())
          {
            localu = (u)localIterator.next();
            if (paramView.equals(localu.c()))
            {
              boolean bool = paramString.equals(localu.d());
              if (bool)
                break;
            }
          }
        localu = null;
      }
    }
    finally
    {
    }
  }

  final u a(ViewGroup paramViewGroup)
  {
    try
    {
      u localu = (u)paramViewGroup.findViewWithTag(b);
      return localu;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final u a(bf parambf, Context paramContext, ViewGroup paramViewGroup, String paramString)
  {
    try
    {
      u localu = new u(parambf, paramContext, paramString, paramViewGroup);
      localu.setTag(b);
      return localu;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final List<u> a(Context paramContext, ViewGroup paramViewGroup, String paramString)
  {
    ArrayList localArrayList;
    try
    {
      List localList = (List)this.c.get(paramContext);
      localArrayList = new ArrayList();
      if (localList != null)
      {
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
          u localu = (u)localIterator.next();
          if ((localu.c().equals(paramViewGroup)) && (!localu.d().equals(paramString)))
            localArrayList.add(localu);
          if ((!localu.c().equals(paramViewGroup)) && (localu.d().equals(paramString)))
            localArrayList.add(localu);
        }
      }
    }
    finally
    {
    }
    return localArrayList;
  }

  final void a()
  {
    try
    {
      List localList = (List)this.c.get(this.d);
      if (localList != null)
      {
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
          u localu = (u)localIterator.next();
          localu.a(0L);
          if ((localu.a() > 0) && (!localu.b()))
            a(localu);
        }
      }
    }
    finally
    {
    }
  }

  final void a(Context paramContext)
  {
    try
    {
      this.d = paramContext;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final void a(Context paramContext, u paramu)
  {
    try
    {
      List localList = (List)this.c.get(paramContext);
      if (localList != null)
      {
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
          if (((u)localIterator.next()).equals(paramu))
          {
            new StringBuilder().append("BannerHolder removed for adSpace: ").append(paramu.d()).toString();
            localIterator.remove();
          }
      }
      return;
    }
    finally
    {
    }
  }

  final void a(u paramu)
  {
    try
    {
      paramu.postDelayed(new an(this, paramu), paramu.a());
      paramu.a(true);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final void b()
  {
    try
    {
      List localList = (List)this.c.get(this.d);
      if (localList != null)
      {
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
          u localu = (u)localIterator.next();
          if ((localu.a() > 0) && (!localu.b()))
            a(localu);
        }
      }
    }
    finally
    {
    }
  }

  final void b(Context paramContext, u paramu)
  {
    if (paramu != null);
    try
    {
      Object localObject2 = (List)this.c.get(paramContext);
      if (localObject2 == null)
        localObject2 = new ArrayList();
      ((List)localObject2).add(paramu);
      this.c.put(paramContext, localObject2);
      return;
    }
    finally
    {
    }
  }

  final void b(u paramu)
  {
    try
    {
      paramu.a(false);
      if ((paramu.getContext().equals(this.d)) && (((List)this.c.get(this.d)).contains(paramu)))
      {
        new StringBuilder().append("Rotating banner for adSpace: ").append(paramu.d()).toString();
        paramu.a(0L);
        if (paramu.a() > 0)
          a(paramu);
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.be
 * JD-Core Version:    0.6.2
 */