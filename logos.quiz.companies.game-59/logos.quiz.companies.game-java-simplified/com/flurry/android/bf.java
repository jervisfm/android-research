package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

final class bf
  implements aj
{
  static String a = "FlurryAgent";
  static int b = 5;
  private static List<Integer> e;
  private static DecoderFactory f;
  private static w g;
  private String A;
  private long B;
  private long C;
  private long D;
  private Map<Integer, ByteBuffer> E;
  private String F;
  private Handler G;
  private ICustomAdNetworkHandler H;
  private IListener I;
  private volatile boolean J;
  private volatile List<bc> K = new ArrayList();
  private volatile Map<String, bc> L = new HashMap();
  private volatile List<bc> M = new ArrayList();
  private volatile List<String> N = new ArrayList();
  private Map<String, String> O;
  x c;
  be d;
  private ay h;
  private s i;
  private Display j;
  private boolean k = false;
  private File l = null;
  private String m;
  private String n;
  private String o;
  private String p;
  private boolean q = true;
  private boolean r = false;
  private volatile String s = null;
  private volatile String t = null;
  private volatile float u;
  private volatile float v;
  private volatile Map<String, String> w;
  private volatile Map<String, TestAds> x;
  private bc y;
  private AdUnit z;

  bf()
  {
    HandlerThread localHandlerThread = new HandlerThread("FlurryAdThread");
    localHandlerThread.start();
    this.G = new Handler(localHandlerThread.getLooper());
    this.c = new x();
    Integer[] arrayOfInteger = new Integer[6];
    arrayOfInteger[0] = Integer.valueOf(0);
    arrayOfInteger[1] = Integer.valueOf(1);
    arrayOfInteger[2] = Integer.valueOf(2);
    arrayOfInteger[3] = Integer.valueOf(3);
    arrayOfInteger[4] = Integer.valueOf(4);
    arrayOfInteger[5] = Integer.valueOf(5);
    e = Arrays.asList(arrayOfInteger);
    f = new DecoderFactory();
    g = new w(this);
    this.i = new s(this);
  }

  static int a(byte[] paramArrayOfByte)
  {
    CrcMessageDigest localCrcMessageDigest = new CrcMessageDigest();
    localCrcMessageDigest.update(paramArrayOfByte);
    return localCrcMessageDigest.getChecksum();
  }

  private ao a(String paramString, boolean paramBoolean, Map<String, String> paramMap)
  {
    try
    {
      ao localao = new ao(paramString, paramBoolean, d(), paramMap);
      return localao;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private String a(bc parambc, AdUnit paramAdUnit, v paramv, String paramString)
  {
    Pattern localPattern = Pattern.compile(".*?(%\\{\\w+\\}).*?");
    for (Matcher localMatcher = localPattern.matcher(paramString); localMatcher.matches(); localMatcher = localPattern.matcher(paramString))
      paramString = this.i.a(parambc, paramAdUnit, paramString, localMatcher.group(1));
    return paramString;
  }

  private static <A extends SpecificRecordBase> A a(byte[] paramArrayOfByte, Class<A> paramClass)
  {
    BinaryDecoder localBinaryDecoder = f.binaryDecoder(new ByteArrayInputStream(paramArrayOfByte), null);
    try
    {
      SpecificRecordBase localSpecificRecordBase = (SpecificRecordBase)new SpecificDatumReader(paramClass).read(null, localBinaryDecoder);
      return localSpecificRecordBase;
    }
    catch (ClassCastException localClassCastException)
    {
      new StringBuilder().append("ClassCastException in parseAvroBinary:").append(localClassCastException.getMessage()).toString();
    }
    return null;
  }

  private void a(SdkLogResponse paramSdkLogResponse)
  {
    try
    {
      if (paramSdkLogResponse.a().toString().equals("success"))
        this.K.removeAll(this.M);
      while (true)
      {
        return;
        Iterator localIterator = paramSdkLogResponse.b().iterator();
        while (localIterator.hasNext())
          ((CharSequence)localIterator.next()).toString();
      }
    }
    finally
    {
    }
  }

  private void a(List<bc> paramList, DataOutputStream paramDataOutputStream)
  {
    try
    {
      int i1 = paramList.size();
      int i2 = 0;
      while (true)
        if (i2 < i1)
          try
          {
            ((bc)paramList.get(i2)).a(paramDataOutputStream);
            i2++;
          }
          catch (IOException localIOException)
          {
            while (true)
              bd.a(a, "unable to convert adLog to byte[]: " + ((bc)paramList.get(i2)).b());
          }
    }
    finally
    {
    }
  }

  static boolean a(Context paramContext, String paramString1, String paramString2)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    Intent localIntent = new Intent(paramString2);
    localIntent.setData(Uri.parse(paramString1));
    return localPackageManager.queryIntentActivities(localIntent, 65536).size() > 0;
  }

  private boolean a(byte[] paramArrayOfByte, String paramString)
  {
    if (paramString == null)
      return false;
    String str2;
    if (paramString.equals("/v3/getAds.do"))
    {
      StringBuilder localStringBuilder1 = new StringBuilder();
      String str1;
      if (this.s != null)
        str1 = this.s;
      while (true)
      {
        str2 = str1 + paramString;
        new h(this, paramArrayOfByte, str2, paramString).execute(new Void[0]);
        return true;
        if (FlurryAgent.getUseHttps())
          str1 = "https://ads.flurry.com";
        else
          str1 = "http://ads.flurry.com";
      }
    }
    StringBuilder localStringBuilder2 = new StringBuilder();
    String str3;
    if (this.t != null)
      str3 = this.t;
    while (true)
    {
      str2 = str3 + paramString;
      break;
      if (FlurryAgent.getUseHttps())
        str3 = "https://adlog.flurry.com";
      else
        str3 = "http://adlog.flurry.com";
    }
  }

  static byte[] a(InputStream paramInputStream)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    byte[] arrayOfByte = new byte[''];
    while (true)
    {
      int i1 = paramInputStream.read(arrayOfByte);
      if (i1 == -1)
        break;
      localByteArrayOutputStream.write(arrayOfByte, 0, i1);
    }
    return localByteArrayOutputStream.toByteArray();
  }

  private byte[] a(String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    List localList = r();
    AdRequest localAdRequest = AdRequest.a().setApiKey(this.A).setAdSpaceName("").setBindings(e).setAdReportedIds(localList).setLocation(Location.a().setLat(this.u).setLon(this.v).build()).setTestDevice(false).setAgentVersion(Integer.toString(FlurryAgent.getAgentVersion())).setSessionId(this.B).setAdViewContainer(AdViewContainer.a().setScreenHeight(this.j.getHeight()).setScreenWidth(this.j.getWidth()).setViewHeight(paramInt2).setViewWidth(paramInt1).build()).setLocale(this.m).setTimezone(this.n).setOsVersion(this.o).setDevicePlatform(this.p).build();
    if (paramBoolean)
      localAdRequest.a(Boolean.valueOf(paramBoolean));
    while (this.O != null)
    {
      HashMap localHashMap = new HashMap();
      Iterator localIterator = this.O.entrySet().iterator();
      while (true)
        if (localIterator.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)localIterator.next();
          localHashMap.put(localEntry.getKey(), localEntry.getValue());
          continue;
          localAdRequest.a(paramString);
          break;
        }
      localAdRequest.a(localHashMap);
    }
    if (this.x.containsKey(paramString))
      localAdRequest.a((TestAds)this.x.get(paramString));
    new StringBuilder().append("Got ad request  ").append(localAdRequest).toString();
    SpecificDatumWriter localSpecificDatumWriter = new SpecificDatumWriter(AdRequest.class);
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    BinaryEncoder localBinaryEncoder = EncoderFactory.get().directBinaryEncoder(localByteArrayOutputStream, null);
    try
    {
      localSpecificDatumWriter.write(localAdRequest, localBinaryEncoder);
      localBinaryEncoder.flush();
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      localIOException.getMessage();
    }
    return new byte[0];
  }

  private static boolean d(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo != null) && ((localNetworkInfo.isConnected()) || (localNetworkInfo.isRoaming())));
    for (boolean bool = true; ; bool = false)
    {
      if (!bool)
        bd.a(a, "******No connectivity found.");
      return bool;
    }
  }

  private boolean p()
  {
    if (!this.J)
      bd.d(a, "Platform Module is not initialized");
    if (this.F == null)
      bd.d(a, "Cannot identify UDID.");
    return this.J;
  }

  private byte[] q()
  {
    List localList1 = r();
    w localw = g;
    List localList2 = this.K;
    ArrayList localArrayList1 = new ArrayList();
    Iterator localIterator1 = localList2.iterator();
    while (localIterator1.hasNext())
    {
      bc localbc = (bc)localIterator1.next();
      SdkAdLog localSdkAdLog = new SdkAdLog();
      localSdkAdLog.a(Long.valueOf(localbc.c()));
      localSdkAdLog.a(localbc.b());
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator2 = localbc.d().iterator();
      while (localIterator2.hasNext())
      {
        ao localao = (ao)localIterator2.next();
        if (localao.b())
        {
          SdkAdEvent localSdkAdEvent = new SdkAdEvent();
          localSdkAdEvent.a(localao.a());
          localSdkAdEvent.a(Long.valueOf(localao.c()));
          Map localMap = localao.d();
          HashMap localHashMap = new HashMap();
          Iterator localIterator3 = localMap.entrySet().iterator();
          while (localIterator3.hasNext())
          {
            Map.Entry localEntry = (Map.Entry)localIterator3.next();
            localHashMap.put(localEntry.getKey(), localEntry.getValue());
          }
          localSdkAdEvent.a(localHashMap);
          localArrayList2.add(localSdkAdEvent);
        }
      }
      localSdkAdLog.a(localArrayList2);
      localArrayList1.add(localSdkAdLog);
    }
    localw.a.M = localList2;
    if (localArrayList1.size() == 0)
      return null;
    SdkLogRequest localSdkLogRequest = SdkLogRequest.a().setApiKey(this.A).setAdReportedIds(localList1).setSdkAdLogs(localArrayList1).setTestDevice(false).setAgentTimestamp(System.currentTimeMillis()).build();
    new StringBuilder().append("Got ad log request:").append(localSdkLogRequest.toString()).toString();
    SpecificDatumWriter localSpecificDatumWriter = new SpecificDatumWriter(SdkLogRequest.class);
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    BinaryEncoder localBinaryEncoder = EncoderFactory.get().directBinaryEncoder(localByteArrayOutputStream, null);
    try
    {
      localSpecificDatumWriter.write(localSdkLogRequest, localBinaryEncoder);
      localBinaryEncoder.flush();
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      localIOException.getMessage();
    }
    return null;
  }

  private List<AdReportedId> r()
  {
    ArrayList localArrayList = new ArrayList();
    ByteBuffer localByteBuffer = ByteBuffer.wrap(this.F.getBytes());
    localArrayList.add(AdReportedId.a().setId(localByteBuffer).setType(0).build());
    Iterator localIterator = this.E.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localArrayList.add(AdReportedId.a().setId((ByteBuffer)localEntry.getValue()).setType(((Integer)localEntry.getKey()).intValue()).build());
    }
    return localArrayList;
  }

  final bc a(bc parambc, String paramString, boolean paramBoolean, Map<String, String> paramMap)
  {
    try
    {
      parambc.a(a(paramString, paramBoolean, paramMap));
      return parambc;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final bc a(String paramString1, String paramString2, boolean paramBoolean, Map<String, String> paramMap)
  {
    bc localbc = (bc)this.L.get(paramString1);
    if (localbc == null)
      localbc = z.a(this, paramString1);
    return a(localbc, paramString2, true, paramMap);
  }

  final r a(Context paramContext, AdUnit paramAdUnit)
  {
    List localList = paramAdUnit.c();
    AdFrame localAdFrame;
    int i1;
    String str1;
    String str2;
    String str3;
    bc localbc;
    IListener localIListener;
    String str4;
    String str5;
    FlurryAdType localFlurryAdType;
    if (localList.size() > 0)
    {
      localAdFrame = (AdFrame)localList.get(0);
      i1 = localAdFrame.a().intValue();
      str1 = localAdFrame.c().toString();
      str2 = localAdFrame.c().toString();
      str3 = localAdFrame.d().d().toString();
      localbc = a(localAdFrame.f().toString(), "requested", true, null);
      if (this.I == null)
        break label209;
      localIListener = this.I;
      str4 = paramAdUnit.a().toString();
      int i2 = localAdFrame.a().intValue();
      str5 = localAdFrame.d().d().toString();
      if (i2 != 3)
        break label182;
      localFlurryAdType = FlurryAdType.VIDEO_TAKEOVER;
    }
    while (!localIListener.shouldDisplayAd(str4, localFlurryAdType))
    {
      return new r(null, false, false, paramAdUnit);
      return new r(null, false, false, paramAdUnit);
      label182: if (str5.equals("takeover"))
        localFlurryAdType = FlurryAdType.WEB_TAKEOVER;
      else
        localFlurryAdType = FlurryAdType.WEB_BANNER;
    }
    label209: bd.a(a, "Processing ad request for binding: " + i1 + ", networkType: " + str1 + ", format: " + str3);
    boolean bool;
    Object localObject;
    if ((paramAdUnit.d().intValue() == 1) || (i1 == 2) || (i1 == 1) || (i1 == 3))
      if (str3.equals("takeover"))
      {
        this.y = localbc;
        this.z = paramAdUnit;
        new StringBuilder().append("opening takeover activity, display: ").append(str2).append(", content: ").append(str1).toString();
        bool = true;
        localObject = null;
      }
    while (true)
    {
      z.e(str1);
      return new r((p)localObject, bool, true, paramAdUnit);
      ak localak = new ak(paramContext, this, localbc, paramAdUnit);
      localak.initLayout(paramContext);
      localObject = localak;
      bool = false;
      continue;
      if (i1 == 4)
      {
        AdSpaceLayout localAdSpaceLayout = localAdFrame.d();
        AdCreative localAdCreative = new AdCreative(localAdSpaceLayout.b().intValue(), localAdSpaceLayout.a().intValue(), localAdSpaceLayout.d().toString(), localAdSpaceLayout.c().toString(), localAdSpaceLayout.e().toString());
        if (str1.equalsIgnoreCase("Admob"))
        {
          bd.a(a, "Retrieving BannerView for:" + str1);
          localObject = new y(paramContext, this, localbc, localAdCreative);
          ((p)localObject).initLayout(paramContext);
          ((p)localObject).d = 0;
          ((p)localObject).c = paramAdUnit;
          bool = false;
        }
        else if (str1.equalsIgnoreCase("Millennial Media"))
        {
          bd.a(a, "Retrieving BannerView for:" + str1);
          localObject = new ah(paramContext, this, localbc, localAdCreative);
          ((p)localObject).initLayout(paramContext);
          ((p)localObject).d = 0;
          ((p)localObject).c = paramAdUnit;
          bool = false;
        }
        else if (str1.equalsIgnoreCase("InMobi"))
        {
          bd.a(a, "Retrieving BannerView for:" + str1);
          localObject = new ab(paramContext, this, localbc, localAdCreative);
          ((p)localObject).initLayout(paramContext);
          ((p)localObject).d = 0;
          ((p)localObject).c = paramAdUnit;
          bool = false;
        }
        else if (str1.equalsIgnoreCase("Mobclix"))
        {
          bd.a(a, "Retrieving BannerView for:" + str1);
          localObject = new ad(paramContext, this, localbc, localAdCreative);
          ((p)localObject).initLayout(paramContext);
          ((p)localObject).d = 0;
          ((p)localObject).c = paramAdUnit;
          bool = false;
        }
        else if (str1.equalsIgnoreCase("Jumptap"))
        {
          bd.a(a, "Retrieving BannerView for:" + str1);
          localObject = new g(paramContext, this, localbc, localAdCreative);
          ((p)localObject).initLayout(paramContext);
          ((p)localObject).d = 0;
          ((p)localObject).c = paramAdUnit;
          bool = false;
        }
        else
        {
          ICustomAdNetworkHandler localICustomAdNetworkHandler = this.H;
          if (localICustomAdNetworkHandler != null)
          {
            AdNetworkView localAdNetworkView = localICustomAdNetworkHandler.getAdFromNetwork(paramContext, localAdCreative, str1);
            if (localAdNetworkView != null)
            {
              localAdNetworkView.a = this;
              localAdNetworkView.b = localbc;
              localAdNetworkView.initLayout(paramContext);
              localAdNetworkView.d = 0;
              localAdNetworkView.c = paramAdUnit;
              localObject = localAdNetworkView;
              bool = false;
            }
            else
            {
              bd.a(a, "CustomAdNetworkHandler returned null banner view");
              localObject = localAdNetworkView;
              bool = false;
            }
          }
          else
          {
            bd.a(a, "No CustomAdNetworkHandler set");
            bool = false;
            localObject = null;
          }
        }
      }
      else
      {
        bd.a(a, "Do not support binding: " + i1);
        bool = false;
        localObject = null;
      }
    }
  }

  final r a(Context paramContext, String paramString, int paramInt1, int paramInt2, boolean paramBoolean, long paramLong)
  {
    Collections.emptyMap();
    Object localObject1 = new r(null, false, false, null);
    if (!p());
    Object localObject2;
    label193: label198: 
    while (true)
    {
      return localObject1;
      long l1 = System.currentTimeMillis();
      int i1 = 0;
      localObject2 = localObject1;
      if (!z.a(l1 + paramLong))
        break;
      AdUnit localAdUnit1 = d(paramString);
      r localr;
      if (localAdUnit1 != null)
      {
        localr = a(paramContext, localAdUnit1);
        if ((this.c.b(paramString)) || (i1 != 0))
          break label193;
        a(paramString, paramInt1, paramInt2, false, 0L);
        i1 = 1;
        localObject1 = localr;
      }
      while (true)
      {
        while (true)
        {
          if (((r)localObject1).d())
            break label198;
          try
          {
            Thread.sleep(paramLong / 10L);
            localObject2 = localObject1;
            break;
            if (i1 == 0)
            {
              a(paramString, paramInt1, paramInt2, false, paramLong);
              i1 = 1;
            }
            AdUnit localAdUnit2 = d(paramString);
            if (localAdUnit2 != null)
              localObject1 = a(paramContext, localAdUnit2);
          }
          catch (InterruptedException localInterruptedException)
          {
            bd.b(a, "Thread with makeAdRequest interrupted.");
            return localObject1;
          }
        }
        localObject1 = localObject2;
        continue;
        localObject1 = localr;
      }
    }
    return localObject2;
  }

  final void a()
  {
    this.d.a(null);
  }

  final void a(float paramFloat1, float paramFloat2)
  {
    this.u = paramFloat1;
    this.v = paramFloat2;
  }

  final void a(Context paramContext)
  {
    this.d.a(paramContext);
    this.d.b();
  }

  final void a(Context paramContext, long paramLong1, long paramLong2)
  {
    this.B = paramLong1;
    this.C = paramLong2;
    this.D = 0L;
    this.d.a(paramContext);
    this.d.a();
    this.r = false;
  }

  final void a(Context paramContext, ViewGroup paramViewGroup)
  {
    if (!p());
    u localu;
    do
    {
      return;
      localu = this.d.a(paramViewGroup);
    }
    while (localu == null);
    paramViewGroup.removeView(localu);
    this.d.a(paramContext, localu);
  }

  final void a(Context paramContext, o paramo)
  {
    if (!this.J)
    {
      this.A = paramo.a;
      this.d = new be(paramContext);
      this.J = true;
    }
    this.h = paramo.b;
    this.m = paramo.d;
    this.n = paramo.e;
    this.o = Build.VERSION.SDK;
    this.p = Build.DEVICE;
    this.j = ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
    this.l = paramContext.getFileStreamPath(".flurryadlog." + Integer.toString(this.A.hashCode(), 16));
    String str = paramContext.getPackageName();
    this.q = a(paramContext, "market://details?id=" + str, "android.intent.action.VIEW");
    this.w = new HashMap();
    this.x = new HashMap();
  }

  final void a(Context paramContext, String paramString)
  {
    if (paramString.startsWith("market://details?id="))
    {
      String str = paramString.substring("market://details?id=".length());
      if (this.q)
        try
        {
          z.a(paramContext, paramString);
          return;
        }
        catch (Exception localException)
        {
          bd.c(a, "Cannot launch Marketplace url " + paramString, localException);
          return;
        }
      z.a(paramContext, "https://market.android.com/details?id=" + str);
      return;
    }
    bd.d(a, "Unexpected android market url scheme: " + paramString);
  }

  final void a(ICustomAdNetworkHandler paramICustomAdNetworkHandler)
  {
    if (paramICustomAdNetworkHandler != null)
      this.H = paramICustomAdNetworkHandler;
  }

  final void a(IListener paramIListener)
  {
    if (paramIListener != null)
      this.I = paramIListener;
  }

  public final void a(bb parambb, aj paramaj)
  {
    AdFrame localAdFrame = (AdFrame)parambb.c.c().get(parambb.e);
    ArrayList localArrayList = new ArrayList();
    List localList = localAdFrame.e();
    String str1 = parambb.a;
    Iterator localIterator1 = localList.iterator();
    while (localIterator1.hasNext())
    {
      Callback localCallback = (Callback)localIterator1.next();
      if (localCallback.a().toString().equals(str1))
      {
        Iterator localIterator4 = localCallback.b().iterator();
        while (localIterator4.hasNext())
        {
          CharSequence localCharSequence = (CharSequence)localIterator4.next();
          HashMap localHashMap2 = new HashMap();
          String str2 = localCharSequence.toString();
          int i2 = str2.indexOf('?');
          if (i2 != -1)
          {
            str3 = str2.substring(0, i2);
            for (String str4 : str2.substring(i2 + 1).split("&"))
            {
              int i5 = str4.indexOf('=');
              if (i5 != -1)
                localHashMap2.put(str4.substring(0, i5), str4.substring(i5 + 1));
            }
          }
          String str3 = str2;
          localArrayList.add(new v(str3, localHashMap2, parambb));
        }
      }
    }
    if (parambb.a.equals("adWillClose"))
    {
      if (this.I != null)
        this.I.onAdClosed(parambb.c.a().toString());
      HashSet localHashSet = new HashSet();
      localHashSet.addAll(Arrays.asList(new String[] { "closeAd", "processRedirect", "nextFrame", "nextAdUnit", "notifyUser" }));
      Iterator localIterator3 = localArrayList.iterator();
      do
        if (!localIterator3.hasNext())
          break;
      while (!localHashSet.contains(((v)localIterator3.next()).a));
    }
    for (int i1 = 1; ; i1 = 0)
    {
      if (i1 == 0)
        localArrayList.add(new v("closeAd", Collections.emptyMap(), parambb));
      if ((parambb.a.equals("renderFailed")) && (this.I != null))
        this.I.onRenderFailed(parambb.c.a().toString());
      Object localObject = null;
      Iterator localIterator2 = localArrayList.iterator();
      while (localIterator2.hasNext())
      {
        v localv = (v)localIterator2.next();
        if (localv.a.equals("logEvent"))
        {
          localv.b.put("__sendToServer", "true");
          localObject = localv;
        }
        paramaj.a(localv, this);
      }
      if (localObject == null)
      {
        HashMap localHashMap1 = new HashMap();
        localHashMap1.put("__sendToServer", "false");
        paramaj.a(new v("logEvent", localHashMap1, parambb), this);
      }
      return;
    }
  }

  final void a(bc parambc)
  {
    try
    {
      if (this.K.size() < 32767)
      {
        this.K.add(parambc);
        this.L.put(parambc.b(), parambc);
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void a(v paramv, bf parambf)
  {
    String str1 = paramv.a;
    Context localContext = paramv.c.b;
    bc localbc = paramv.c.d;
    AdUnit localAdUnit = paramv.c.c;
    String str3;
    af localaf;
    if (str1.equals("processRedirect"))
      if (paramv.b.containsKey("url"))
      {
        if (this.I != null)
          this.I.onApplicationExit(localAdUnit.a().toString());
        str3 = a(localbc, localAdUnit, paramv, z.c((String)paramv.b.get("url")));
        localaf = new af(this, localContext, str3);
      }
    do
    {
      do
      {
        do
        {
          try
          {
            str4 = (String)localaf.execute(new Void[0]).get();
            if (str4 != null)
            {
              if (p())
                this.G.post(new c(this, str4, localContext, true));
              return;
            }
          }
          catch (InterruptedException localInterruptedException)
          {
            while (true)
            {
              localInterruptedException.printStackTrace();
              str4 = "";
            }
          }
          catch (ExecutionException localExecutionException)
          {
            while (true)
            {
              localExecutionException.printStackTrace();
              String str4 = "";
            }
            bd.a(a, "Redirect URL could not be found for: " + str3);
            return;
          }
          if (!str1.equals("verifyUrl"))
            break;
        }
        while (!paramv.b.containsKey("url"));
        if (a(localContext, a(localbc, localAdUnit, paramv, (String)paramv.b.get("url")), "android.intent.action.VIEW"));
        for (String str2 = "urlVerified"; ; str2 = "urlNotVerified")
        {
          Collections.emptyMap();
          parambf.a(new bb(str2, paramv.c.b, localAdUnit, localbc, paramv.c.e), this);
          return;
        }
        if (!str1.equals("sendUrlAsync"))
          break;
      }
      while (!paramv.b.containsKey("url"));
      if (this.I != null)
        this.I.onApplicationExit(localAdUnit.a().toString());
      d locald = new d(this, localContext, a(localbc, localAdUnit, paramv, (String)paramv.b.get("url")));
      this.G.post(locald);
      return;
    }
    while (!str1.equals("sendAdLogs"));
    k();
  }

  final void a(DataInputStream paramDataInputStream)
  {
    try
    {
      while (paramDataInputStream.readUnsignedShort() != 0)
        this.K.add(new bc(paramDataInputStream));
    }
    finally
    {
    }
    this.k = true;
  }

  final void a(String paramString)
  {
    this.F = paramString;
  }

  final void a(String paramString, int paramInt1, int paramInt2, boolean paramBoolean, long paramLong)
  {
    if (!this.N.contains(paramString))
    {
      if (paramString == null)
        bd.a(a, "ad space name should not be null");
    }
    else
      return;
    if (paramLong > 0L)
    {
      i locali = new i(this, paramString, paramInt1, paramInt2, paramBoolean);
      this.G.post(locali);
      return;
    }
    a(paramString, paramInt1, paramInt2, paramBoolean, "/v3/getAds.do");
  }

  final void a(String paramString1, int paramInt1, int paramInt2, boolean paramBoolean, String paramString2)
  {
    this.N.add(paramString1);
    byte[] arrayOfByte = a(paramString1, paramInt1, paramInt2, paramBoolean);
    if (arrayOfByte != null)
      a(arrayOfByte, paramString2);
    this.N.remove(paramString1);
  }

  final void a(String paramString, FlurryAdSize paramFlurryAdSize)
  {
    if (paramFlurryAdSize != null)
      this.x.put(paramString, TestAds.a().setAdspacePlacement(paramFlurryAdSize.a()).build());
  }

  final void a(Map<Integer, ByteBuffer> paramMap)
  {
    this.E = paramMap;
  }

  final boolean a(Context paramContext, String paramString, long paramLong)
  {
    long l1 = System.currentTimeMillis();
    do
    {
      boolean bool1 = z.a(l1 + paramLong);
      boolean bool2 = false;
      if (bool1)
      {
        if (d(paramString) != null)
          bool2 = true;
      }
      else
        return bool2;
      DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
      a(paramString, localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, false, paramLong);
    }
    while (d(paramString) == null);
    return true;
  }

  final boolean a(Context paramContext, String paramString, ViewGroup paramViewGroup, long paramLong)
  {
    if (!p())
      return false;
    if (paramLong < 1L)
    {
      boolean bool2 = this.c.b(paramString);
      if (d(paramContext) == true)
        new GetAdAsyncTask(paramContext, paramString, paramViewGroup, this).execute(new Void[0]);
      return bool2;
    }
    u localu1 = this.d.a(paramViewGroup);
    u localu2 = this.d.a(paramContext, paramViewGroup, paramString);
    if (localu2 == null)
    {
      localu2 = this.d.a(this, paramContext, paramViewGroup, paramString);
      this.d.b(paramContext, localu2);
    }
    u localu3 = localu2;
    Iterator localIterator = this.d.a(paramContext, paramViewGroup, paramString).iterator();
    while (localIterator.hasNext())
    {
      u localu4 = (u)localIterator.next();
      if (!localu4.equals(localu1))
      {
        this.d.a(paramContext, localu4);
        localu4.c().removeView(localu4);
      }
    }
    as localas = this.d.a(localu3, paramLong);
    if (localas.a())
    {
      if (!localu3.equals(localu1))
      {
        if (localu1 != null)
        {
          this.d.a(paramContext, localu1);
          paramViewGroup.removeView(localu1);
        }
        paramViewGroup.addView(localu3);
      }
      if ((localu3.a() > 0) && (!localu3.b()))
        this.d.a(localu3);
    }
    if ((localas.a()) || (localas.b()));
    for (boolean bool1 = true; ; bool1 = false)
    {
      bd.a(a, "******Ad is being returned: " + bool1);
      return bool1;
    }
  }

  final long b()
  {
    return this.B;
  }

  final void b(Context paramContext)
  {
    bd.c(a, "Init'ing ads.");
    if (!this.r)
    {
      DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
      a("", localDisplayMetrics.widthPixels, localDisplayMetrics.heightPixels, true, 1L);
      this.r = true;
      return;
    }
    bd.c(a, "Ads already init'ed, will not init them again this session");
  }

  final void b(String paramString)
  {
    this.s = paramString;
  }

  final void b(Map<String, String> paramMap)
  {
    if (paramMap != null)
      this.O = paramMap;
  }

  final void b(byte[] paramArrayOfByte)
  {
    SdkLogResponse localSdkLogResponse = (SdkLogResponse)a(paramArrayOfByte, SdkLogResponse.class);
    if (localSdkLogResponse != null)
    {
      new StringBuilder().append("got an AdLogResponse:").append(localSdkLogResponse).toString();
      a(localSdkLogResponse);
    }
  }

  final boolean b(Context paramContext, String paramString)
  {
    int i1 = 0;
    while (true)
      if (i1 < 5)
      {
        if (d(paramContext) == true)
        {
          HttpResponse localHttpResponse = z.a(this.h, paramString, 10000, 15000, true);
          if ((localHttpResponse == null) || (localHttpResponse.getStatusLine().getStatusCode() != 200))
            break label84;
          new StringBuilder().append("URL hit succeeded for: ").append(paramString).toString();
          return true;
        }
        try
        {
          Thread.sleep(100L);
          label84: i1++;
        }
        catch (InterruptedException localInterruptedException)
        {
          while (true)
            localInterruptedException.getMessage();
        }
      }
    return false;
  }

  final String c()
  {
    return this.A;
  }

  final void c(String paramString)
  {
    this.t = paramString;
  }

  final void c(Map<String, String> paramMap)
  {
    if ((paramMap != null) && (paramMap.size() > 0))
    {
      Iterator localIterator = paramMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        this.w.put(localEntry.getKey(), localEntry.getValue());
      }
    }
  }

  final void c(byte[] paramArrayOfByte)
  {
    AdResponse localAdResponse = (AdResponse)a(paramArrayOfByte, AdResponse.class);
    if (localAdResponse != null)
    {
      new StringBuilder().append("got an AdResponse:").append(localAdResponse).toString();
      if (p())
        this.c.a(localAdResponse);
    }
  }

  final long d()
  {
    try
    {
      long l1 = SystemClock.elapsedRealtime() - this.C;
      if (l1 > this.D);
      while (true)
      {
        this.D = l1;
        long l2 = this.D;
        return l2;
        l1 = 1L + this.D;
        this.D = l1;
      }
    }
    finally
    {
    }
  }

  final AdUnit d(String paramString)
  {
    return this.c.a(paramString);
  }

  final IListener e()
  {
    return this.I;
  }

  final void e(String paramString)
  {
    byte[] arrayOfByte = q();
    if (arrayOfByte != null)
      a(arrayOfByte, paramString);
  }

  final String f()
  {
    return this.F;
  }

  final void g()
  {
    this.O = null;
  }

  final boolean h()
  {
    return this.J;
  }

  final void i()
  {
    this.w.clear();
  }

  final Map<String, String> j()
  {
    return this.w;
  }

  final void k()
  {
    try
    {
      f localf = new f(this);
      this.G.post(localf);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  final void l()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 86	com/flurry/android/bf:l	Ljava/io/File;
    //   6: invokestatic 1355	com/flurry/android/ae:a	(Ljava/io/File;)Z
    //   9: istore 6
    //   11: iload 6
    //   13: ifne +10 -> 23
    //   16: aconst_null
    //   17: invokestatic 1358	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: new 1360	java/io/DataOutputStream
    //   26: dup
    //   27: new 1362	java/io/FileOutputStream
    //   30: dup
    //   31: aload_0
    //   32: getfield 86	com/flurry/android/bf:l	Ljava/io/File;
    //   35: invokespecial 1365	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   38: invokespecial 1368	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   41: astore_2
    //   42: aload_2
    //   43: ldc_w 1369
    //   46: invokevirtual 1373	java/io/DataOutputStream:writeShort	(I)V
    //   49: aload_0
    //   50: aload_0
    //   51: getfield 99	com/flurry/android/bf:K	Ljava/util/List;
    //   54: aload_2
    //   55: invokespecial 1375	com/flurry/android/bf:a	(Ljava/util/List;Ljava/io/DataOutputStream;)V
    //   58: aload_2
    //   59: iconst_0
    //   60: invokevirtual 1373	java/io/DataOutputStream:writeShort	(I)V
    //   63: aload_2
    //   64: invokestatic 1358	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   67: goto -47 -> 20
    //   70: astore_3
    //   71: aload_0
    //   72: monitorexit
    //   73: aload_3
    //   74: athrow
    //   75: astore 4
    //   77: aconst_null
    //   78: astore_2
    //   79: getstatic 77	com/flurry/android/bf:a	Ljava/lang/String;
    //   82: ldc_w 402
    //   85: aload 4
    //   87: invokestatic 1377	com/flurry/android/bd:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   90: pop
    //   91: aload_2
    //   92: invokestatic 1358	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   95: goto -75 -> 20
    //   98: aload_2
    //   99: invokestatic 1358	com/flurry/android/z:a	(Ljava/io/Closeable;)V
    //   102: aload_1
    //   103: athrow
    //   104: astore_1
    //   105: goto -7 -> 98
    //   108: astore 4
    //   110: goto -31 -> 79
    //   113: astore_1
    //   114: aconst_null
    //   115: astore_2
    //   116: goto -18 -> 98
    //
    // Exception table:
    //   from	to	target	type
    //   16	20	70	finally
    //   63	67	70	finally
    //   91	95	70	finally
    //   98	104	70	finally
    //   2	11	75	java/lang/Throwable
    //   23	42	75	java/lang/Throwable
    //   42	63	104	finally
    //   79	91	104	finally
    //   42	63	108	java/lang/Throwable
    //   2	11	113	finally
    //   23	42	113	finally
  }

  final void m()
  {
    try
    {
      new k(this).execute(new Void[0]);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  final bc n()
  {
    return this.y;
  }

  final AdUnit o()
  {
    return this.z;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.bf
 * JD-Core Version:    0.6.2
 */