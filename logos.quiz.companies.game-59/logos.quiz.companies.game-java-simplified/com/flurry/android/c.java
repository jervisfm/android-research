package com.flurry.android;

import android.content.Context;
import android.content.Intent;

final class c
  implements Runnable
{
  c(bf parambf, String paramString, Context paramContext, boolean paramBoolean)
  {
  }

  public final void run()
  {
    if (this.a != null)
    {
      if (this.a.startsWith("market://"))
      {
        this.d.a(this.b, this.a);
        return;
      }
      if (this.c)
      {
        Intent localIntent = new Intent(this.b, FlurryFullscreenTakeoverActivity.class);
        localIntent.putExtra("url", this.a);
        this.b.startActivity(localIntent);
        return;
      }
      z.a(this.b, this.a);
      return;
    }
    String str = "Unable to launch intent for: " + this.a;
    bd.d(bf.a, str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.c
 * JD-Core Version:    0.6.2
 */