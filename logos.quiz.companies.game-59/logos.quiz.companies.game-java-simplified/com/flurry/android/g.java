package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import com.jumptap.adtag.JtAdInterstitial;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.JtAdWidgetSettingsFactory;
import com.jumptap.adtag.utils.JtException;

final class g extends AdNetworkView
{
  g(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc, paramAdCreative);
    try
    {
      ApplicationInfo localApplicationInfo2 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      localApplicationInfo1 = localApplicationInfo2;
      String str = localApplicationInfo1.metaData.getString("com.flurry.jumptap.PUBLISHER_ID");
      sAdNetworkApiKey = str;
      if (str == null)
        Log.d("FlurryAgent", "com.flurry.jumptap.PUBLISHER_ID not set in manifest");
      setFocusable(true);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Cannot find manifest for app");
        ApplicationInfo localApplicationInfo1 = null;
      }
    }
  }

  public final void initLayout(Context paramContext)
  {
    if (this.fAdCreative.getFormat().equals("takeover"))
    {
      JtAdWidgetSettings localJtAdWidgetSettings2 = JtAdWidgetSettingsFactory.createWidgetSettings();
      localJtAdWidgetSettings2.setPublisherId(sAdNetworkApiKey);
      localJtAdWidgetSettings2.setRefreshPeriod(0);
      localJtAdWidgetSettings2.setShouldSendLocation(false);
      try
      {
        JtAdInterstitial localJtAdInterstitial1 = new JtAdInterstitial((Activity)paramContext, localJtAdWidgetSettings2);
        localJtAdInterstitial2 = localJtAdInterstitial1;
        localJtAdInterstitial2.setAdViewListener(new aq(this));
        ((Activity)paramContext).setContentView(localJtAdInterstitial2);
        return;
      }
      catch (JtException localJtException2)
      {
        while (true)
        {
          Log.d("FlurryAgent", "Jumptap JtException when creating ad object.");
          JtAdInterstitial localJtAdInterstitial2 = null;
        }
      }
    }
    JtAdWidgetSettings localJtAdWidgetSettings1 = JtAdWidgetSettingsFactory.createWidgetSettings();
    localJtAdWidgetSettings1.setPublisherId(sAdNetworkApiKey);
    localJtAdWidgetSettings1.setRefreshPeriod(0);
    localJtAdWidgetSettings1.setShouldSendLocation(false);
    try
    {
      JtAdView localJtAdView1 = new JtAdView((Activity)paramContext, localJtAdWidgetSettings1);
      localJtAdView2 = localJtAdView1;
      localJtAdView2.setAdViewListener(new t(this));
      addView(localJtAdView2);
      return;
    }
    catch (JtException localJtException1)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Jumptap JtException when creating ad object.");
        JtAdView localJtAdView2 = null;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.g
 * JD-Core Version:    0.6.2
 */