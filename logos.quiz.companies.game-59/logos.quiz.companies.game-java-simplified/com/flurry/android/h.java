package com.flurry.android;

import android.os.AsyncTask;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

final class h extends AsyncTask<Void, Void, Void>
{
  private final String a = getClass().getSimpleName();
  private byte[] b = null;
  private String c = "";
  private String d = "";

  public h(bf parambf, byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    this.b = paramArrayOfByte;
    this.c = paramString1;
    this.d = paramString2;
  }

  private Void a()
  {
    ByteArrayEntity localByteArrayEntity = new ByteArrayEntity(this.b);
    localByteArrayEntity.setContentType("avro/binary");
    HttpPost localHttpPost = new HttpPost(this.c);
    localHttpPost.setEntity(localByteArrayEntity);
    localHttpPost.setHeader("accept", "avro/binary");
    localHttpPost.setHeader("FM-Checksum", Integer.toString(bf.a(this.b)));
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 10000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, 15000);
    localHttpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
    HttpClient localHttpClient = bf.c(this.e).a(localBasicHttpParams);
    int i;
    try
    {
      HttpResponse localHttpResponse = localHttpClient.execute(localHttpPost);
      i = localHttpResponse.getStatusLine().getStatusCode();
      if ((i == 200) && (localHttpResponse.getEntity() != null) && (localHttpResponse.getEntity().getContentLength() != 0L))
      {
        bd.c(this.a, "Request successful");
        byte[] arrayOfByte = bf.a(localHttpResponse.getEntity().getContent());
        localByteArrayEntity.consumeContent();
        String str = Integer.toString(bf.a(arrayOfByte));
        if ((localHttpResponse.containsHeader("FM-Checksum")) && (!localHttpResponse.getFirstHeader("FM-Checksum").getValue().equals(str)))
          break label341;
        if (this.d.equals("/v3/getAds.do"))
          this.e.c(arrayOfByte);
        else
          this.e.b(arrayOfByte);
      }
    }
    catch (IOException localIOException)
    {
      bd.b(this.a, "IOException: " + localIOException.toString());
    }
    bd.b(this.a, "Report failed. HTTP response: " + i);
    label341: return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.h
 * JD-Core Version:    0.6.2
 */