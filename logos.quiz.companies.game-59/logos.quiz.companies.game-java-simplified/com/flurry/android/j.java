package com.flurry.android;

import android.util.Log;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.InterstitialAd;

final class j
  implements AdListener
{
  j(y paramy)
  {
  }

  public final void onDismissScreen(Ad paramAd)
  {
    Log.i("FlurryAgent", "Admob Interstitial dismissed from screen.");
  }

  public final void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "Admob Interstitial failed to receive takeover.");
  }

  public final void onLeaveApplication(Ad paramAd)
  {
    this.a.onAdClicked(null);
    Log.i("FlurryAgent", "Admob Interstitial leave application.");
  }

  public final void onPresentScreen(Ad paramAd)
  {
    Log.d("FlurryAgent", "Admob Interstitial present on screen.");
  }

  public final void onReceiveAd(Ad paramAd)
  {
    this.a.onAdFilled(null);
    Log.d("FlurryAgent", "Admob Interstitial received takeover.");
    this.a.onAdShown(null);
    y.a(this.a).show();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.j
 * JD-Core Version:    0.6.2
 */