package com.flurry.android;

import android.util.Log;
import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdInterstitial.State;
import com.inmobi.androidsdk.IMAdInterstitialListener;
import com.inmobi.androidsdk.IMAdRequest.ErrorCode;

final class m
  implements IMAdInterstitialListener
{
  m(ab paramab)
  {
  }

  public final void onAdRequestFailed(IMAdInterstitial paramIMAdInterstitial, IMAdRequest.ErrorCode paramErrorCode)
  {
    this.a.onAdUnFilled(null);
    Log.d("FlurryAgent", "InMobi imAdView ad request failed.");
  }

  public final void onAdRequestLoaded(IMAdInterstitial paramIMAdInterstitial)
  {
    this.a.onAdFilled(null);
    Log.d("FlurryAgent", "InMobi Interstitial ad request completed.");
    if (IMAdInterstitial.State.READY.equals(paramIMAdInterstitial.getState()))
    {
      this.a.onAdShown(null);
      paramIMAdInterstitial.show();
    }
  }

  public final void onDismissAdScreen(IMAdInterstitial paramIMAdInterstitial)
  {
    Log.d("FlurryAgent", "InMobi Interstitial ad dismissed.");
  }

  public final void onShowAdScreen(IMAdInterstitial paramIMAdInterstitial)
  {
    this.a.onAdClicked(null);
    Log.d("FlurryAgent", "InMobi Interstitial ad shown.");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.m
 * JD-Core Version:    0.6.2
 */