package com.flurry.android;

import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.util.List;
import java.util.Map;

abstract class p extends RelativeLayout
{
  bf a;
  bc b;
  AdUnit c;
  int d;
  private Context e;

  p(Context paramContext)
  {
    super(paramContext);
    a(paramContext, null, null);
  }

  p(Context paramContext, bf parambf, bc parambc)
  {
    super(paramContext);
    a(paramContext, parambf, parambc);
  }

  private void a(Context paramContext, bf parambf, bc parambc)
  {
    this.e = paramContext;
    this.a = parambf;
    this.b = parambc;
  }

  final void a(ViewGroup paramViewGroup, u paramu)
  {
    if ((this.c == null) || (this.c.c().size() < 1))
      return;
    if (!(paramViewGroup instanceof RelativeLayout))
    {
      paramu.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
      return;
    }
    AdSpaceLayout localAdSpaceLayout = ((AdFrame)this.c.c().get(0)).d();
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(z.b(this.e, localAdSpaceLayout.a().intValue()), z.b(this.e, localAdSpaceLayout.b().intValue()));
    String[] arrayOfString = localAdSpaceLayout.e().toString().split("-");
    if (arrayOfString.length == 2)
    {
      if (!arrayOfString[0].equals("b"))
        break label170;
      localLayoutParams.addRule(12);
      if (!arrayOfString[1].equals("c"))
        break label214;
      localLayoutParams.addRule(14);
    }
    while (true)
    {
      paramu.setLayoutParams(localLayoutParams);
      return;
      label170: if (arrayOfString[0].equals("t"))
      {
        localLayoutParams.addRule(10);
        break;
      }
      if (!arrayOfString[0].equals("m"))
        break;
      localLayoutParams.addRule(15);
      break;
      label214: if (arrayOfString[1].equals("l"))
        localLayoutParams.addRule(9);
      else if (arrayOfString[1].equals("r"))
        localLayoutParams.addRule(11);
    }
  }

  final void a(String paramString, boolean paramBoolean, Map<String, String> paramMap)
  {
    new StringBuilder().append("AppSpotBannerView.onEvent ").append(paramString).toString();
    this.a.a(this.b, paramString, paramBoolean, paramMap);
    if (this.c != null)
      this.a.a(new bb(paramString, this.e, this.c, this.b, 0), this.a);
  }

  public abstract void initLayout(Context paramContext);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.p
 * JD-Core Version:    0.6.2
 */