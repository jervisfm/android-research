package com.flurry.android;

final class r
{
  private p a;
  private boolean b;
  private boolean c;
  private int d;
  private AdUnit e;

  r(p paramp, boolean paramBoolean1, boolean paramBoolean2, AdUnit paramAdUnit)
  {
    this.a = paramp;
    this.b = paramBoolean1;
    this.c = paramBoolean2;
    this.d = 0;
    this.e = paramAdUnit;
  }

  final AdUnit a()
  {
    return this.e;
  }

  final p b()
  {
    return this.a;
  }

  final boolean c()
  {
    return this.b;
  }

  final boolean d()
  {
    return this.c;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.r
 * JD-Core Version:    0.6.2
 */