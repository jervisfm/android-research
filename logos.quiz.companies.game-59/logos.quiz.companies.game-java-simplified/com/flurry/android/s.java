package com.flurry.android;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class s
{
  private static String a = "FlurryAgent";
  private bf b;

  s(bf parambf)
  {
    this.b = parambf;
  }

  private static boolean a(String paramString1, String paramString2)
  {
    return paramString2.equals("%{" + paramString1 + "}");
  }

  final String a(bc parambc, AdUnit paramAdUnit, String paramString1, String paramString2)
  {
    if (a("fids", paramString2))
    {
      String str9 = 0 + ":" + this.b.f();
      bd.a(a, "Replacing param fids with: " + str9);
      return paramString1.replace(paramString2, z.b(str9));
    }
    if (a("sid", paramString2))
    {
      String str8 = String.valueOf(this.b.b());
      bd.a(a, "Replacing param sid with: " + str8);
      return paramString1.replace(paramString2, z.b(str8));
    }
    if (a("lid", paramString2))
    {
      String str7 = String.valueOf(parambc.a());
      bd.a(a, "Replacing param lid with: " + str7);
      return paramString1.replace(paramString2, z.b(str7));
    }
    if (a("guid", paramString2))
    {
      String str6 = parambc.b();
      bd.a(a, "Replacing param guid with: " + str6);
      return paramString1.replace(paramString2, z.b(str6));
    }
    if (a("ats", paramString2))
    {
      String str5 = String.valueOf(System.currentTimeMillis());
      bd.a(a, "Replacing param ats with: " + str5);
      return paramString1.replace(paramString2, z.b(str5));
    }
    if (a("apik", paramString2))
    {
      String str4 = this.b.c();
      bd.a(a, "Replacing param apik with: " + str4);
      return paramString1.replace(paramString2, z.b(str4));
    }
    if (a("hid", paramString2))
    {
      String str3 = paramAdUnit.a().toString();
      bd.a(a, "Replacing param hid with: " + str3);
      return paramString1.replace(paramString2, z.b(str3));
    }
    if (a("eso", paramString2))
    {
      String str2 = Long.toString(System.currentTimeMillis() - this.b.b());
      bd.a(a, "Replacing param eso with: " + str2);
      return paramString1.replace(paramString2, z.b(str2));
    }
    if (a("uc", paramString2))
    {
      Iterator localIterator = this.b.j().entrySet().iterator();
      Map.Entry localEntry;
      for (String str1 = ""; localIterator.hasNext(); str1 = str1 + z.b((String)localEntry.getKey()) + "=" + z.b((String)localEntry.getValue()) + "&")
        localEntry = (Map.Entry)localIterator.next();
      bd.a(a, "Replacing param uc with: " + str1);
      return paramString1.replace(paramString2, str1);
    }
    bd.a(a, "Unknown param: " + paramString2);
    return paramString1.replace(paramString2, "");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.s
 * JD-Core Version:    0.6.2
 */