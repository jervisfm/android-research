package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.List;

final class u extends RelativeLayout
{
  private bf a;
  private Context b;
  private String c;
  private ViewGroup d;
  private int e;
  private boolean f;

  u(bf parambf, Context paramContext, String paramString, ViewGroup paramViewGroup)
  {
    super(paramContext);
    this.a = parambf;
    this.b = paramContext;
    this.c = paramString;
    this.d = paramViewGroup;
    this.e = 0;
    this.f = false;
  }

  final int a()
  {
    return this.e;
  }

  final as a(long paramLong)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    int i = z.a(this.b, this.d.getWidth());
    int j = z.a(this.b, this.d.getHeight());
    r localr = this.a.a(this.b, this.c, i, j, false, paramLong);
    IListener localIListener = this.a.e();
    FlurryAdType localFlurryAdType2;
    if ((localr.b() != null) && (localr.d()))
    {
      removeAllViews();
      if (((AdFrame)localr.a().c().get(0)).d().d().equals("takeover"))
      {
        localFlurryAdType2 = FlurryAdType.WEB_TAKEOVER;
        if (localIListener != null)
        {
          boolean bool3 = localIListener.shouldDisplayAd(this.c, localFlurryAdType2);
          bool2 = false;
          if (!bool3);
        }
        else
        {
          if (getParent() == null)
            localr.b().a(this.d, this);
          addView(localr.b());
          this.e = 0;
        }
      }
    }
    while (true)
    {
      return new as(bool1, bool2);
      localFlurryAdType2 = FlurryAdType.WEB_BANNER;
      break;
      if (localr.c())
      {
        if (((AdFrame)localr.a().c().get(0)).a().intValue() == 3);
        for (FlurryAdType localFlurryAdType1 = FlurryAdType.VIDEO_TAKEOVER; ; localFlurryAdType1 = FlurryAdType.WEB_TAKEOVER)
        {
          if (localIListener != null)
            localIListener.shouldDisplayAd(this.c, localFlurryAdType1);
          Intent localIntent = new Intent();
          localIntent.setClass(this.b, FlurryFullscreenTakeoverActivity.class);
          this.b.startActivity(localIntent);
          bool2 = bool1;
          bool1 = false;
          break;
        }
      }
      bool2 = false;
      bool1 = false;
    }
  }

  final void a(int paramInt)
  {
    this.e = paramInt;
  }

  final void a(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }

  final boolean b()
  {
    return this.f;
  }

  final ViewGroup c()
  {
    return this.d;
  }

  final String d()
  {
    return this.c;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.u
 * JD-Core Version:    0.6.2
 */