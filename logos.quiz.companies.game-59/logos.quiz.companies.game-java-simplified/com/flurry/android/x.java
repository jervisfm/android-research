package com.flurry.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class x
{
  private Map<String, List<AdUnit>> a = new HashMap();
  private Map<String, AdUnit> b = new HashMap();

  final AdUnit a(String paramString)
  {
    while (true)
    {
      try
      {
        List localList = (List)this.a.get(paramString);
        if (localList != null)
        {
          Iterator localIterator = localList.iterator();
          if (localIterator.hasNext())
          {
            localAdUnit = (AdUnit)localIterator.next();
            if ((!z.a(localAdUnit.b().longValue())) || (localAdUnit.c().size() <= 0))
              continue;
            localIterator.remove();
            this.b.put(((AdFrame)localAdUnit.c().get(0)).f().toString(), localAdUnit);
            return localAdUnit;
          }
        }
      }
      finally
      {
      }
      AdUnit localAdUnit = null;
    }
  }

  final List<AdUnit> a(String paramString, int paramInt1, int paramInt2)
  {
    ArrayList localArrayList;
    try
    {
      localArrayList = new ArrayList();
      List localList = (List)this.a.get(paramString);
      if (localList != null)
      {
        Iterator localIterator = localList.iterator();
        while ((localIterator.hasNext()) && (localArrayList.size() <= paramInt1) && (localArrayList.size() <= paramInt2))
        {
          AdUnit localAdUnit = (AdUnit)localIterator.next();
          if ((z.a(localAdUnit.b().longValue())) && (localAdUnit.d().intValue() == 1) && (localAdUnit.c().size() > 0))
          {
            localArrayList.add(localAdUnit);
            this.b.put(((AdFrame)localAdUnit.c().get(0)).f().toString(), localAdUnit);
            localIterator.remove();
          }
        }
      }
    }
    finally
    {
    }
    return localArrayList;
  }

  final void a(AdResponse paramAdResponse)
  {
    try
    {
      this.a.clear();
      Iterator localIterator = paramAdResponse.a().iterator();
      while (localIterator.hasNext())
      {
        AdUnit localAdUnit = (AdUnit)localIterator.next();
        String str = localAdUnit.a().toString();
        Object localObject2 = (List)this.a.get(str);
        if (localObject2 == null)
          localObject2 = new ArrayList();
        ((List)localObject2).add(localAdUnit);
        this.a.put(str, localObject2);
      }
    }
    finally
    {
    }
  }

  final boolean b(String paramString)
  {
    while (true)
    {
      try
      {
        List localList = (List)this.a.get(paramString);
        if ((localList != null) && (!localList.isEmpty()))
        {
          Iterator localIterator = localList.iterator();
          if (localIterator.hasNext())
          {
            boolean bool2 = z.a(((AdUnit)localIterator.next()).b().longValue());
            if (!bool2)
              continue;
            bool1 = true;
            return bool1;
          }
        }
      }
      finally
      {
      }
      boolean bool1 = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.x
 * JD-Core Version:    0.6.2
 */