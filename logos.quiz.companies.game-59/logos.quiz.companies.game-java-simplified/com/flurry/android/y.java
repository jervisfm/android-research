package com.flurry.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;

final class y extends AdNetworkView
{
  private static boolean e;
  private InterstitialAd f;

  y(Context paramContext, bf parambf, bc parambc, AdCreative paramAdCreative)
  {
    super(paramContext, parambf, parambc, paramAdCreative);
    try
    {
      ApplicationInfo localApplicationInfo2 = paramContext.getPackageManager().getApplicationInfo(paramContext.getPackageName(), 128);
      localApplicationInfo1 = localApplicationInfo2;
      Bundle localBundle = localApplicationInfo1.metaData;
      sAdNetworkApiKey = localBundle.getString("com.flurry.admob.MY_AD_UNIT_ID");
      e = localBundle.getBoolean("com.flurry.admob.test");
      if (sAdNetworkApiKey == null)
        Log.d("FlurryAgent", "com.flurry.admob.MY_AD_UNIT_ID not set in manifest");
      setFocusable(true);
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        Log.d("FlurryAgent", "Cannot find manifest for app");
        ApplicationInfo localApplicationInfo1 = null;
      }
    }
  }

  public final void initLayout(Context paramContext)
  {
    if (this.fAdCreative.getFormat().equals("takeover"))
    {
      this.f = new InterstitialAd((Activity)paramContext, sAdNetworkApiKey);
      j localj = new j(this);
      this.f.setAdListener(localj);
      AdRequest localAdRequest2 = new AdRequest();
      if (e)
      {
        Log.d("FlurryAgent", "Admob AdView set to Test Mode.");
        localAdRequest2.addTestDevice(AdRequest.TEST_EMULATOR);
      }
      this.f.loadAd(localAdRequest2);
      return;
    }
    int i = this.fAdCreative.getHeight();
    int j = this.fAdCreative.getWidth();
    AdSize localAdSize;
    if ((j >= AdSize.IAB_LEADERBOARD.getWidth()) && (i >= AdSize.IAB_LEADERBOARD.getHeight()))
    {
      Log.d("FlurryAgent", "Determined Admob AdSize as IAB_LEADERBOARD");
      localAdSize = AdSize.IAB_LEADERBOARD;
    }
    while (localAdSize != null)
    {
      AdView localAdView = new AdView((Activity)paramContext, localAdSize, sAdNetworkApiKey);
      localAdView.setAdListener(new az(this));
      addView(localAdView);
      AdRequest localAdRequest1 = new AdRequest();
      if (e)
      {
        Log.d("FlurryAgent", "Admob AdView set to Test Mode.");
        localAdRequest1.addTestDevice(AdRequest.TEST_EMULATOR);
      }
      localAdView.loadAd(localAdRequest1);
      return;
      if ((j >= AdSize.IAB_BANNER.getWidth()) && (i >= AdSize.IAB_BANNER.getHeight()))
      {
        Log.d("FlurryAgent", "Determined Admob AdSize as IAB_BANNER");
        localAdSize = AdSize.IAB_BANNER;
      }
      else if ((j >= AdSize.BANNER.getWidth()) && (i >= AdSize.BANNER.getHeight()))
      {
        Log.d("FlurryAgent", "Determined Admob AdSize as BANNER");
        localAdSize = AdSize.BANNER;
      }
      else if ((j >= AdSize.IAB_MRECT.getWidth()) && (i >= AdSize.IAB_MRECT.getHeight()))
      {
        Log.d("FlurryAgent", "Determined Admob AdSize as IAB_MRECT");
        localAdSize = AdSize.IAB_MRECT;
      }
      else
      {
        Log.d("FlurryAgent", "Could not find Admob AdSize that matches size");
        localAdSize = null;
      }
    }
    Log.d("FlurryAgent", "**********Could not load Admob Ad");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.y
 * JD-Core Version:    0.6.2
 */