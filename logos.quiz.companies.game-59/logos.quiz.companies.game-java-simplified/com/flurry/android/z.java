package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.util.DisplayMetrics;
import java.io.Closeable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

final class z
{
  static int a(Context paramContext, int paramInt)
  {
    DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
    return Math.round(paramInt / localDisplayMetrics.density);
  }

  static bc a(bf parambf, String paramString)
  {
    long l = parambf.b();
    parambf.d();
    bc localbc = new bc(l, paramString);
    parambf.a(localbc);
    return localbc;
  }

  static String a(String paramString)
  {
    if (paramString == null)
      paramString = "";
    while (paramString.length() <= 255)
      return paramString;
    return paramString.substring(0, 255);
  }

  static HttpResponse a(ay paramay, String paramString, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    try
    {
      HttpGet localHttpGet = new HttpGet(paramString);
      BasicHttpParams localBasicHttpParams = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 10000);
      HttpConnectionParams.setSoTimeout(localBasicHttpParams, 15000);
      localBasicHttpParams.setParameter("http.protocol.handle-redirects", Boolean.valueOf(paramBoolean));
      HttpResponse localHttpResponse = paramay.a(localBasicHttpParams).execute(localHttpGet);
      return localHttpResponse;
    }
    catch (UnknownHostException localUnknownHostException)
    {
      bd.a("FlurryAgent", "Unknown host: " + localUnknownHostException.getMessage());
      return null;
    }
    catch (Exception localException)
    {
      bd.a("FlurryAgent", "Failed to hit URL: " + paramString, localException);
    }
    return null;
  }

  static void a(Context paramContext, String paramString)
  {
    bd.a("FlurryAgent", "Launching intent for " + paramString);
    paramContext.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(paramString)));
  }

  static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null);
    try
    {
      paramCloseable.close();
      return;
    }
    catch (Throwable localThrowable)
    {
    }
  }

  static boolean a(long paramLong)
  {
    boolean bool1 = System.currentTimeMillis() < paramLong;
    boolean bool2 = false;
    if (!bool1)
      bool2 = true;
    return bool2;
  }

  static int b(Context paramContext, int paramInt)
  {
    DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
    return Math.round(paramInt * localDisplayMetrics.density);
  }

  static String b(String paramString)
  {
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      bd.d("FlurryAgent", "Cannot encode '" + paramString + "'");
    }
    return "";
  }

  static String c(String paramString)
  {
    try
    {
      String str = URLDecoder.decode(paramString, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      bd.d("FlurryAgent", "Cannot decode '" + paramString + "'");
    }
    return "";
  }

  static byte[] d(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramString.getBytes(), 0, paramString.length());
      byte[] arrayOfByte = localMessageDigest.digest();
      return arrayOfByte;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      bd.b("FlurryAgent", "Unsupported SHA1: " + localNoSuchAlgorithmException.getMessage());
    }
    return null;
  }

  // ERROR //
  static java.util.Map<String, String> e(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 233	org/json/JSONObject
    //   5: dup
    //   6: aload_0
    //   7: invokespecial 234	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   10: astore_2
    //   11: aload_2
    //   12: invokevirtual 238	org/json/JSONObject:keys	()Ljava/util/Iterator;
    //   15: astore 6
    //   17: aload 6
    //   19: invokeinterface 244 1 0
    //   24: ifeq +114 -> 138
    //   27: aload 6
    //   29: invokeinterface 248 1 0
    //   34: checkcast 57	java/lang/String
    //   37: astore 8
    //   39: aload_2
    //   40: aload 8
    //   42: invokevirtual 251	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   45: astore 11
    //   47: new 253	java/util/HashMap
    //   50: dup
    //   51: invokespecial 254	java/util/HashMap:<init>	()V
    //   54: astore 12
    //   56: aload 12
    //   58: aload 8
    //   60: aload 11
    //   62: invokeinterface 260 3 0
    //   67: pop
    //   68: aload 12
    //   70: astore_1
    //   71: goto -54 -> 17
    //   74: astore 9
    //   76: ldc 114
    //   78: new 116	java/lang/StringBuilder
    //   81: dup
    //   82: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   85: ldc_w 262
    //   88: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: aload 8
    //   93: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   99: invokestatic 135	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   102: pop
    //   103: aload_1
    //   104: areturn
    //   105: astore_3
    //   106: aconst_null
    //   107: astore 4
    //   109: ldc 114
    //   111: new 116	java/lang/StringBuilder
    //   114: dup
    //   115: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   118: ldc_w 264
    //   121: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: aload_0
    //   125: invokevirtual 123	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: invokevirtual 130	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   131: invokestatic 135	com/flurry/android/bd:a	(Ljava/lang/String;Ljava/lang/String;)I
    //   134: pop
    //   135: aload 4
    //   137: areturn
    //   138: aload_1
    //   139: areturn
    //   140: astore 7
    //   142: aload_1
    //   143: astore 4
    //   145: goto -36 -> 109
    //   148: astore 13
    //   150: aload 12
    //   152: astore_1
    //   153: goto -77 -> 76
    //
    // Exception table:
    //   from	to	target	type
    //   39	56	74	org/json/JSONException
    //   2	17	105	org/json/JSONException
    //   17	39	140	org/json/JSONException
    //   76	103	140	org/json/JSONException
    //   56	68	148	org/json/JSONException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.flurry.android.z
 * JD-Core Version:    0.6.2
 */