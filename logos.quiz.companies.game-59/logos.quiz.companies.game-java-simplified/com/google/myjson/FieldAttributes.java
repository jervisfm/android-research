package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import com.google.myjson.internal.Pair;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class FieldAttributes
{
  private static final c<Pair<Class<?>, String>, Collection<Annotation>> a = new d(c());
  private final Class<?> b;
  private final Field c;
  private final Class<?> d;
  private final boolean e;
  private final int f;
  private final String g;
  private Type h;
  private Collection<Annotation> i;

  FieldAttributes(Class<?> paramClass, Field paramField)
  {
    this.b = ((Class).Gson.Preconditions.checkNotNull(paramClass));
    this.g = paramField.getName();
    this.d = paramField.getType();
    this.e = paramField.isSynthetic();
    this.f = paramField.getModifiers();
    this.c = paramField;
  }

  private static <T extends Annotation> T a(Collection<Annotation> paramCollection, Class<T> paramClass)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      Annotation localAnnotation = (Annotation)localIterator.next();
      if (localAnnotation.annotationType() == paramClass)
        return localAnnotation;
    }
    return null;
  }

  private static int c()
  {
    try
    {
      int j = Integer.parseInt(System.getProperty("com.google.myjson.annotation_cache_size_hint", String.valueOf(2000)));
      return j;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 2000;
  }

  boolean a()
  {
    return this.e;
  }

  Field b()
  {
    return this.c;
  }

  public <T extends Annotation> T getAnnotation(Class<T> paramClass)
  {
    return a(getAnnotations(), paramClass);
  }

  public Collection<Annotation> getAnnotations()
  {
    if (this.i == null)
    {
      Pair localPair = new Pair(this.b, this.g);
      Collection localCollection = (Collection)a.getElement(localPair);
      if (localCollection == null)
      {
        localCollection = Collections.unmodifiableCollection(Arrays.asList(this.c.getAnnotations()));
        a.addElement(localPair, localCollection);
      }
      this.i = localCollection;
    }
    return this.i;
  }

  public Class<?> getDeclaredClass()
  {
    return this.d;
  }

  public Type getDeclaredType()
  {
    if (this.h == null)
      this.h = this.c.getGenericType();
    return this.h;
  }

  public Class<?> getDeclaringClass()
  {
    return this.b;
  }

  public String getName()
  {
    return this.g;
  }

  public boolean hasModifier(int paramInt)
  {
    return (paramInt & this.f) != 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.FieldAttributes
 * JD-Core Version:    0.6.2
 */