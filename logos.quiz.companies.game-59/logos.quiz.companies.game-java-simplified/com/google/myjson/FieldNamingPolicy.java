package com.google.myjson;

public enum FieldNamingPolicy
{
  private final l a;

  static
  {
    LOWER_CASE_WITH_UNDERSCORES = new FieldNamingPolicy("LOWER_CASE_WITH_UNDERSCORES", 2, new b("_"));
    LOWER_CASE_WITH_DASHES = new FieldNamingPolicy("LOWER_CASE_WITH_DASHES", 3, new b("-"));
    FieldNamingPolicy[] arrayOfFieldNamingPolicy = new FieldNamingPolicy[4];
    arrayOfFieldNamingPolicy[0] = UPPER_CAMEL_CASE;
    arrayOfFieldNamingPolicy[1] = UPPER_CAMEL_CASE_WITH_SPACES;
    arrayOfFieldNamingPolicy[2] = LOWER_CASE_WITH_UNDERSCORES;
    arrayOfFieldNamingPolicy[3] = LOWER_CASE_WITH_DASHES;
  }

  private FieldNamingPolicy(l paraml)
  {
    this.a = paraml;
  }

  l a()
  {
    return this.a;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.FieldNamingPolicy
 * JD-Core Version:    0.6.2
 */