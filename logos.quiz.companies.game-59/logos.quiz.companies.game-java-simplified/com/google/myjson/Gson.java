package com.google.myjson;

import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.internal.ParameterizedTypeHandlerMap;
import com.google.myjson.internal.Primitives;
import com.google.myjson.internal.Streams;
import com.google.myjson.internal.bind.ArrayTypeAdapter;
import com.google.myjson.internal.bind.BigDecimalTypeAdapter;
import com.google.myjson.internal.bind.BigIntegerTypeAdapter;
import com.google.myjson.internal.bind.CollectionTypeAdapterFactory;
import com.google.myjson.internal.bind.DateTypeAdapter;
import com.google.myjson.internal.bind.ExcludedTypeAdapterFactory;
import com.google.myjson.internal.bind.JsonElementReader;
import com.google.myjson.internal.bind.JsonElementWriter;
import com.google.myjson.internal.bind.MapTypeAdapterFactory;
import com.google.myjson.internal.bind.MiniGson;
import com.google.myjson.internal.bind.MiniGson.Builder;
import com.google.myjson.internal.bind.ObjectTypeAdapter;
import com.google.myjson.internal.bind.ReflectiveTypeAdapterFactory;
import com.google.myjson.internal.bind.SqlDateTypeAdapter;
import com.google.myjson.internal.bind.TimeTypeAdapter;
import com.google.myjson.internal.bind.TypeAdapter;
import com.google.myjson.internal.bind.TypeAdapter.Factory;
import com.google.myjson.internal.bind.TypeAdapters;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import com.google.myjson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class Gson
{
  static final ParameterizedTypeHandlerMap a = new ParameterizedTypeHandlerMap().makeUnmodifiable();
  static final q b = new q();
  static final g c = new g(true);
  static final n d = new n(new int[] { 128, 8 });
  static final l e = new f(new k());
  private static final ExclusionStrategy f = a();
  private final ExclusionStrategy g;
  private final ExclusionStrategy h;
  private final ConstructorConstructor i;
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> j;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> k;
  private final boolean l;
  private final boolean m;
  private final boolean n;
  private final boolean o;
  private final MiniGson p;

  public Gson()
  {
    this(f, f, e, a, false, a, a, false, false, true, false, false, LongSerializationPolicy.DEFAULT, Collections.emptyList());
  }

  Gson(ExclusionStrategy paramExclusionStrategy1, ExclusionStrategy paramExclusionStrategy2, final l paraml, ParameterizedTypeHandlerMap<InstanceCreator<?>> paramParameterizedTypeHandlerMap, boolean paramBoolean1, ParameterizedTypeHandlerMap<JsonSerializer<?>> paramParameterizedTypeHandlerMap1, ParameterizedTypeHandlerMap<JsonDeserializer<?>> paramParameterizedTypeHandlerMap2, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, LongSerializationPolicy paramLongSerializationPolicy, List<TypeAdapter.Factory> paramList)
  {
    this.g = paramExclusionStrategy1;
    this.h = paramExclusionStrategy2;
    this.i = new ConstructorConstructor(paramParameterizedTypeHandlerMap);
    this.l = paramBoolean1;
    this.j = paramParameterizedTypeHandlerMap1;
    this.k = paramParameterizedTypeHandlerMap2;
    this.n = paramBoolean3;
    this.m = paramBoolean4;
    this.o = paramBoolean5;
    ReflectiveTypeAdapterFactory local4 = new ReflectiveTypeAdapterFactory(this.i)
    {
      public boolean deserializeField(Class<?> paramAnonymousClass, Field paramAnonymousField, Type paramAnonymousType)
      {
        ExclusionStrategy localExclusionStrategy = Gson.b(Gson.this);
        return (!localExclusionStrategy.shouldSkipClass(paramAnonymousField.getType())) && (!localExclusionStrategy.shouldSkipField(new FieldAttributes(paramAnonymousClass, paramAnonymousField)));
      }

      public String getFieldName(Class<?> paramAnonymousClass, Field paramAnonymousField, Type paramAnonymousType)
      {
        return paraml.translateName(new FieldAttributes(paramAnonymousClass, paramAnonymousField));
      }

      public boolean serializeField(Class<?> paramAnonymousClass, Field paramAnonymousField, Type paramAnonymousType)
      {
        ExclusionStrategy localExclusionStrategy = Gson.a(Gson.this);
        return (!localExclusionStrategy.shouldSkipClass(paramAnonymousField.getType())) && (!localExclusionStrategy.shouldSkipField(new FieldAttributes(paramAnonymousClass, paramAnonymousField)));
      }
    };
    MiniGson.Builder localBuilder = new MiniGson.Builder().withoutDefaultFactories().factory(TypeAdapters.STRING_FACTORY).factory(TypeAdapters.INTEGER_FACTORY).factory(TypeAdapters.BOOLEAN_FACTORY).factory(TypeAdapters.BYTE_FACTORY).factory(TypeAdapters.SHORT_FACTORY).factory(TypeAdapters.newFactory(Long.TYPE, Long.class, a(paramLongSerializationPolicy))).factory(TypeAdapters.newFactory(Double.TYPE, Double.class, a(paramBoolean6))).factory(TypeAdapters.newFactory(Float.TYPE, Float.class, b(paramBoolean6))).factory(new ExcludedTypeAdapterFactory(paramExclusionStrategy2, paramExclusionStrategy1)).factory(TypeAdapters.NUMBER_FACTORY).factory(TypeAdapters.CHARACTER_FACTORY).factory(TypeAdapters.STRING_BUILDER_FACTORY).factory(TypeAdapters.STRING_BUFFER_FACTORY).typeAdapter(BigDecimal.class, new BigDecimalTypeAdapter()).typeAdapter(BigInteger.class, new BigIntegerTypeAdapter()).factory(TypeAdapters.JSON_ELEMENT_FACTORY).factory(ObjectTypeAdapter.FACTORY);
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      localBuilder.factory((TypeAdapter.Factory)localIterator.next());
    localBuilder.factory(new j(this, paramParameterizedTypeHandlerMap1, paramParameterizedTypeHandlerMap2)).factory(new CollectionTypeAdapterFactory(this.i)).factory(TypeAdapters.URL_FACTORY).factory(TypeAdapters.URI_FACTORY).factory(TypeAdapters.UUID_FACTORY).factory(TypeAdapters.LOCALE_FACTORY).factory(TypeAdapters.INET_ADDRESS_FACTORY).factory(TypeAdapters.BIT_SET_FACTORY).factory(DateTypeAdapter.FACTORY).factory(TypeAdapters.CALENDAR_FACTORY).factory(TimeTypeAdapter.FACTORY).factory(SqlDateTypeAdapter.FACTORY).factory(TypeAdapters.TIMESTAMP_FACTORY).factory(new MapTypeAdapterFactory(this.i, paramBoolean2)).factory(ArrayTypeAdapter.FACTORY).factory(TypeAdapters.ENUM_FACTORY).factory(local4);
    this.p = localBuilder.build();
  }

  private static ExclusionStrategy a()
  {
    LinkedList localLinkedList = new LinkedList();
    localLinkedList.add(b);
    localLinkedList.add(c);
    localLinkedList.add(d);
    return new t(localLinkedList);
  }

  private TypeAdapter<Number> a(LongSerializationPolicy paramLongSerializationPolicy)
  {
    if (paramLongSerializationPolicy == LongSerializationPolicy.DEFAULT)
      return TypeAdapters.LONG;
    return new TypeAdapter()
    {
      public Number read(JsonReader paramAnonymousJsonReader)
        throws IOException
      {
        if (paramAnonymousJsonReader.peek() == JsonToken.NULL)
        {
          paramAnonymousJsonReader.nextNull();
          return null;
        }
        return Long.valueOf(paramAnonymousJsonReader.nextLong());
      }

      public void write(JsonWriter paramAnonymousJsonWriter, Number paramAnonymousNumber)
        throws IOException
      {
        if (paramAnonymousNumber == null)
        {
          paramAnonymousJsonWriter.nullValue();
          return;
        }
        paramAnonymousJsonWriter.value(paramAnonymousNumber.toString());
      }
    };
  }

  private TypeAdapter<Number> a(boolean paramBoolean)
  {
    if (paramBoolean)
      return TypeAdapters.DOUBLE;
    return new TypeAdapter()
    {
      public Double read(JsonReader paramAnonymousJsonReader)
        throws IOException
      {
        if (paramAnonymousJsonReader.peek() == JsonToken.NULL)
        {
          paramAnonymousJsonReader.nextNull();
          return null;
        }
        return Double.valueOf(paramAnonymousJsonReader.nextDouble());
      }

      public void write(JsonWriter paramAnonymousJsonWriter, Number paramAnonymousNumber)
        throws IOException
      {
        if (paramAnonymousNumber == null)
        {
          paramAnonymousJsonWriter.nullValue();
          return;
        }
        double d = paramAnonymousNumber.doubleValue();
        Gson.a(Gson.this, d);
        paramAnonymousJsonWriter.value(paramAnonymousNumber);
      }
    };
  }

  private JsonWriter a(Writer paramWriter)
    throws IOException
  {
    if (this.n)
      paramWriter.write(")]}'\n");
    JsonWriter localJsonWriter = new JsonWriter(paramWriter);
    if (this.o)
      localJsonWriter.setIndent("  ");
    localJsonWriter.setSerializeNulls(this.l);
    return localJsonWriter;
  }

  private void a(double paramDouble)
  {
    if ((Double.isNaN(paramDouble)) || (Double.isInfinite(paramDouble)))
      throw new IllegalArgumentException(paramDouble + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialDoubleValues() method.");
  }

  private static void a(Object paramObject, JsonReader paramJsonReader)
  {
    if (paramObject != null)
      try
      {
        if (paramJsonReader.peek() != JsonToken.END_DOCUMENT)
          throw new JsonIOException("JSON document was not fully consumed.");
      }
      catch (MalformedJsonException localMalformedJsonException)
      {
        throw new JsonSyntaxException(localMalformedJsonException);
      }
      catch (IOException localIOException)
      {
        throw new JsonIOException(localIOException);
      }
  }

  private TypeAdapter<Number> b(boolean paramBoolean)
  {
    if (paramBoolean)
      return TypeAdapters.FLOAT;
    return new TypeAdapter()
    {
      public Float read(JsonReader paramAnonymousJsonReader)
        throws IOException
      {
        if (paramAnonymousJsonReader.peek() == JsonToken.NULL)
        {
          paramAnonymousJsonReader.nextNull();
          return null;
        }
        return Float.valueOf((float)paramAnonymousJsonReader.nextDouble());
      }

      public void write(JsonWriter paramAnonymousJsonWriter, Number paramAnonymousNumber)
        throws IOException
      {
        if (paramAnonymousNumber == null)
        {
          paramAnonymousJsonWriter.nullValue();
          return;
        }
        float f = paramAnonymousNumber.floatValue();
        Gson.a(Gson.this, f);
        paramAnonymousJsonWriter.value(paramAnonymousNumber);
      }
    };
  }

  public <T> T fromJson(JsonElement paramJsonElement, Class<T> paramClass)
    throws JsonSyntaxException
  {
    Object localObject = fromJson(paramJsonElement, paramClass);
    return Primitives.wrap(paramClass).cast(localObject);
  }

  public <T> T fromJson(JsonElement paramJsonElement, Type paramType)
    throws JsonSyntaxException
  {
    if (paramJsonElement == null)
      return null;
    return fromJson(new JsonElementReader(paramJsonElement), paramType);
  }

  // ERROR //
  public <T> T fromJson(JsonReader paramJsonReader, Type paramType)
    throws JsonIOException, JsonSyntaxException
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_1
    //   3: invokevirtual 441	com/google/myjson/stream/JsonReader:isLenient	()Z
    //   6: istore 4
    //   8: aload_1
    //   9: iload_3
    //   10: invokevirtual 444	com/google/myjson/stream/JsonReader:setLenient	(Z)V
    //   13: aload_1
    //   14: invokevirtual 386	com/google/myjson/stream/JsonReader:peek	()Lcom/google/myjson/stream/JsonToken;
    //   17: pop
    //   18: iconst_0
    //   19: istore_3
    //   20: aload_0
    //   21: getfield 292	com/google/myjson/Gson:p	Lcom/google/myjson/internal/bind/MiniGson;
    //   24: aload_2
    //   25: invokestatic 450	com/google/myjson/reflect/TypeToken:get	(Ljava/lang/reflect/Type;)Lcom/google/myjson/reflect/TypeToken;
    //   28: invokevirtual 456	com/google/myjson/internal/bind/MiniGson:getAdapter	(Lcom/google/myjson/reflect/TypeToken;)Lcom/google/myjson/internal/bind/TypeAdapter;
    //   31: aload_1
    //   32: invokevirtual 462	com/google/myjson/internal/bind/TypeAdapter:read	(Lcom/google/myjson/stream/JsonReader;)Ljava/lang/Object;
    //   35: astore 10
    //   37: aload_1
    //   38: iload 4
    //   40: invokevirtual 444	com/google/myjson/stream/JsonReader:setLenient	(Z)V
    //   43: aload 10
    //   45: areturn
    //   46: astore 8
    //   48: iload_3
    //   49: ifeq +11 -> 60
    //   52: aload_1
    //   53: iload 4
    //   55: invokevirtual 444	com/google/myjson/stream/JsonReader:setLenient	(Z)V
    //   58: aconst_null
    //   59: areturn
    //   60: new 399	com/google/myjson/JsonSyntaxException
    //   63: dup
    //   64: aload 8
    //   66: invokespecial 402	com/google/myjson/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   69: athrow
    //   70: astore 6
    //   72: aload_1
    //   73: iload 4
    //   75: invokevirtual 444	com/google/myjson/stream/JsonReader:setLenient	(Z)V
    //   78: aload 6
    //   80: athrow
    //   81: astore 7
    //   83: new 399	com/google/myjson/JsonSyntaxException
    //   86: dup
    //   87: aload 7
    //   89: invokespecial 402	com/google/myjson/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   92: athrow
    //   93: astore 5
    //   95: new 399	com/google/myjson/JsonSyntaxException
    //   98: dup
    //   99: aload 5
    //   101: invokespecial 402	com/google/myjson/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   104: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   13	18	46	java/io/EOFException
    //   20	37	46	java/io/EOFException
    //   13	18	70	finally
    //   20	37	70	finally
    //   60	70	70	finally
    //   83	93	70	finally
    //   95	105	70	finally
    //   13	18	81	java/lang/IllegalStateException
    //   20	37	81	java/lang/IllegalStateException
    //   13	18	93	java/io/IOException
    //   20	37	93	java/io/IOException
  }

  public <T> T fromJson(Reader paramReader, Class<T> paramClass)
    throws JsonSyntaxException, JsonIOException
  {
    JsonReader localJsonReader = new JsonReader(paramReader);
    Object localObject = fromJson(localJsonReader, paramClass);
    a(localObject, localJsonReader);
    return Primitives.wrap(paramClass).cast(localObject);
  }

  public <T> T fromJson(Reader paramReader, Type paramType)
    throws JsonIOException, JsonSyntaxException
  {
    JsonReader localJsonReader = new JsonReader(paramReader);
    Object localObject = fromJson(localJsonReader, paramType);
    a(localObject, localJsonReader);
    return localObject;
  }

  public <T> T fromJson(String paramString, Class<T> paramClass)
    throws JsonSyntaxException
  {
    Object localObject = fromJson(paramString, paramClass);
    return Primitives.wrap(paramClass).cast(localObject);
  }

  public <T> T fromJson(String paramString, Type paramType)
    throws JsonSyntaxException
  {
    if (paramString == null)
      return null;
    return fromJson(new StringReader(paramString), paramType);
  }

  public String toJson(JsonElement paramJsonElement)
  {
    StringWriter localStringWriter = new StringWriter();
    toJson(paramJsonElement, localStringWriter);
    return localStringWriter.toString();
  }

  public String toJson(Object paramObject)
  {
    if (paramObject == null)
      return toJson(JsonNull.INSTANCE);
    return toJson(paramObject, paramObject.getClass());
  }

  public String toJson(Object paramObject, Type paramType)
  {
    StringWriter localStringWriter = new StringWriter();
    toJson(paramObject, paramType, localStringWriter);
    return localStringWriter.toString();
  }

  public void toJson(JsonElement paramJsonElement, JsonWriter paramJsonWriter)
    throws JsonIOException
  {
    boolean bool1 = paramJsonWriter.isLenient();
    paramJsonWriter.setLenient(true);
    boolean bool2 = paramJsonWriter.isHtmlSafe();
    paramJsonWriter.setHtmlSafe(this.m);
    boolean bool3 = paramJsonWriter.getSerializeNulls();
    paramJsonWriter.setSerializeNulls(this.l);
    try
    {
      Streams.write(paramJsonElement, paramJsonWriter);
      return;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    finally
    {
      paramJsonWriter.setLenient(bool1);
      paramJsonWriter.setHtmlSafe(bool2);
      paramJsonWriter.setSerializeNulls(bool3);
    }
  }

  public void toJson(JsonElement paramJsonElement, Appendable paramAppendable)
    throws JsonIOException
  {
    try
    {
      toJson(paramJsonElement, a(Streams.writerForAppendable(paramAppendable)));
      return;
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }

  public void toJson(Object paramObject, Appendable paramAppendable)
    throws JsonIOException
  {
    if (paramObject != null)
    {
      toJson(paramObject, paramObject.getClass(), paramAppendable);
      return;
    }
    toJson(JsonNull.INSTANCE, paramAppendable);
  }

  public void toJson(Object paramObject, Type paramType, JsonWriter paramJsonWriter)
    throws JsonIOException
  {
    TypeAdapter localTypeAdapter = this.p.getAdapter(TypeToken.get(paramType));
    boolean bool1 = paramJsonWriter.isLenient();
    paramJsonWriter.setLenient(true);
    boolean bool2 = paramJsonWriter.isHtmlSafe();
    paramJsonWriter.setHtmlSafe(this.m);
    boolean bool3 = paramJsonWriter.getSerializeNulls();
    paramJsonWriter.setSerializeNulls(this.l);
    try
    {
      localTypeAdapter.write(paramJsonWriter, paramObject);
      return;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    finally
    {
      paramJsonWriter.setLenient(bool1);
      paramJsonWriter.setHtmlSafe(bool2);
      paramJsonWriter.setSerializeNulls(bool3);
    }
  }

  public void toJson(Object paramObject, Type paramType, Appendable paramAppendable)
    throws JsonIOException
  {
    try
    {
      toJson(paramObject, paramType, a(Streams.writerForAppendable(paramAppendable)));
      return;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
  }

  public JsonElement toJsonTree(Object paramObject)
  {
    if (paramObject == null)
      return JsonNull.INSTANCE;
    return toJsonTree(paramObject, paramObject.getClass());
  }

  public JsonElement toJsonTree(Object paramObject, Type paramType)
  {
    JsonElementWriter localJsonElementWriter = new JsonElementWriter();
    toJson(paramObject, paramType, localJsonElementWriter);
    return localJsonElementWriter.get();
  }

  public String toString()
  {
    return "{" + "serializeNulls:" + this.l + ",serializers:" + this.j + ",deserializers:" + this.k + ",instanceCreators:" + this.i + "}";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.Gson
 * JD-Core Version:    0.6.2
 */