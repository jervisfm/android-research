package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import com.google.myjson.internal.ParameterizedTypeHandlerMap;
import com.google.myjson.internal.Primitives;
import com.google.myjson.internal.bind.TypeAdapter.Factory;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public final class GsonBuilder
{
  private static final p a = new p();
  private static final i b = new i();
  private static final m c = new m();
  private final Set<ExclusionStrategy> d = new HashSet();
  private final Set<ExclusionStrategy> e = new HashSet();
  private double f;
  private n g;
  private boolean h;
  private boolean i;
  private LongSerializationPolicy j;
  private l k;
  private final ParameterizedTypeHandlerMap<InstanceCreator<?>> l;
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> m;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> n;
  private final List<TypeAdapter.Factory> o = new ArrayList();
  private boolean p;
  private String q;
  private int r;
  private int s;
  private boolean t = false;
  private boolean u;
  private boolean v;
  private boolean w;
  private boolean x;

  public GsonBuilder()
  {
    this.e.add(Gson.b);
    this.e.add(Gson.c);
    this.d.add(Gson.b);
    this.d.add(Gson.c);
    this.f = -1.0D;
    this.h = true;
    this.w = false;
    this.v = true;
    this.g = Gson.d;
    this.i = false;
    this.j = LongSerializationPolicy.DEFAULT;
    this.k = Gson.e;
    this.l = new ParameterizedTypeHandlerMap();
    this.m = new ParameterizedTypeHandlerMap();
    this.n = new ParameterizedTypeHandlerMap();
    this.p = false;
    this.r = 2;
    this.s = 2;
    this.u = false;
    this.x = false;
  }

  private <T> GsonBuilder a(Class<?> paramClass, InstanceCreator<? extends T> paramInstanceCreator, boolean paramBoolean)
  {
    this.l.registerForTypeHierarchy(paramClass, paramInstanceCreator, paramBoolean);
    return this;
  }

  private <T> GsonBuilder a(Class<?> paramClass, JsonDeserializer<T> paramJsonDeserializer, boolean paramBoolean)
  {
    this.n.registerForTypeHierarchy(paramClass, new u(paramJsonDeserializer), paramBoolean);
    return this;
  }

  private <T> GsonBuilder a(Class<?> paramClass, JsonSerializer<T> paramJsonSerializer, boolean paramBoolean)
  {
    this.m.registerForTypeHierarchy(paramClass, paramJsonSerializer, paramBoolean);
    return this;
  }

  private GsonBuilder a(Class<?> paramClass, Object paramObject, boolean paramBoolean)
  {
    if (((paramObject instanceof JsonSerializer)) || ((paramObject instanceof JsonDeserializer)) || ((paramObject instanceof InstanceCreator)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      if ((paramObject instanceof InstanceCreator))
        a(paramClass, (InstanceCreator)paramObject, paramBoolean);
      if ((paramObject instanceof JsonSerializer))
        a(paramClass, (JsonSerializer)paramObject, paramBoolean);
      if ((paramObject instanceof JsonDeserializer))
        a(paramClass, (JsonDeserializer)paramObject, paramBoolean);
      return this;
    }
  }

  private <T> GsonBuilder a(Type paramType, InstanceCreator<? extends T> paramInstanceCreator, boolean paramBoolean)
  {
    this.l.register(paramType, paramInstanceCreator, paramBoolean);
    return this;
  }

  private <T> GsonBuilder a(Type paramType, JsonDeserializer<T> paramJsonDeserializer, boolean paramBoolean)
  {
    this.n.register(paramType, new u(paramJsonDeserializer), paramBoolean);
    return this;
  }

  private <T> GsonBuilder a(Type paramType, JsonSerializer<T> paramJsonSerializer, boolean paramBoolean)
  {
    this.m.register(paramType, paramJsonSerializer, paramBoolean);
    return this;
  }

  private GsonBuilder a(Type paramType, Object paramObject, boolean paramBoolean)
  {
    if (((paramObject instanceof JsonSerializer)) || ((paramObject instanceof JsonDeserializer)) || ((paramObject instanceof InstanceCreator)) || ((paramObject instanceof TypeAdapter.Factory)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      if ((!Primitives.isPrimitive(paramType)) && (!Primitives.isWrapperType(paramType)))
        break;
      throw new IllegalArgumentException("Cannot register type adapters for " + paramType);
    }
    if ((paramObject instanceof InstanceCreator))
      a(paramType, (InstanceCreator)paramObject, paramBoolean);
    if ((paramObject instanceof JsonSerializer))
      a(paramType, (JsonSerializer)paramObject, paramBoolean);
    if ((paramObject instanceof JsonDeserializer))
      a(paramType, (JsonDeserializer)paramObject, paramBoolean);
    if ((paramObject instanceof TypeAdapter.Factory))
      this.o.add((TypeAdapter.Factory)paramObject);
    return this;
  }

  private static <T> void a(Class<?> paramClass, ParameterizedTypeHandlerMap<T> paramParameterizedTypeHandlerMap, T paramT)
  {
    if (!paramParameterizedTypeHandlerMap.hasSpecificHandlerFor(paramClass))
      paramParameterizedTypeHandlerMap.register(paramClass, paramT, false);
  }

  private static void a(String paramString, int paramInt1, int paramInt2, ParameterizedTypeHandlerMap<JsonSerializer<?>> paramParameterizedTypeHandlerMap, ParameterizedTypeHandlerMap<JsonDeserializer<?>> paramParameterizedTypeHandlerMap1)
  {
    o.a locala;
    if ((paramString != null) && (!"".equals(paramString.trim())))
      locala = new o.a(paramString);
    while (true)
    {
      if (locala != null)
      {
        a(java.util.Date.class, paramParameterizedTypeHandlerMap, locala);
        a(java.util.Date.class, paramParameterizedTypeHandlerMap1, locala);
        a(Timestamp.class, paramParameterizedTypeHandlerMap, locala);
        a(Timestamp.class, paramParameterizedTypeHandlerMap1, locala);
        a(java.sql.Date.class, paramParameterizedTypeHandlerMap, locala);
        a(java.sql.Date.class, paramParameterizedTypeHandlerMap1, locala);
      }
      return;
      locala = null;
      if (paramInt1 != 2)
      {
        locala = null;
        if (paramInt2 != 2)
          locala = new o.a(paramInt1, paramInt2);
      }
    }
  }

  GsonBuilder a(l paraml)
  {
    this.k = new f(paraml);
    return this;
  }

  public GsonBuilder addDeserializationExclusionStrategy(ExclusionStrategy paramExclusionStrategy)
  {
    this.e.add(paramExclusionStrategy);
    return this;
  }

  public GsonBuilder addSerializationExclusionStrategy(ExclusionStrategy paramExclusionStrategy)
  {
    this.d.add(paramExclusionStrategy);
    return this;
  }

  public Gson create()
  {
    LinkedList localLinkedList1 = new LinkedList(this.e);
    LinkedList localLinkedList2 = new LinkedList(this.d);
    localLinkedList1.add(this.g);
    localLinkedList2.add(this.g);
    if (!this.h)
    {
      localLinkedList1.add(a);
      localLinkedList2.add(a);
    }
    if (this.f != -1.0D)
    {
      v localv = new v(this.f);
      localLinkedList1.add(localv);
      localLinkedList2.add(localv);
    }
    if (this.i)
    {
      localLinkedList1.add(b);
      localLinkedList2.add(c);
    }
    a(this.q, this.r, this.s, this.m, this.n);
    return new Gson(new t(localLinkedList1), new t(localLinkedList2), this.k, this.l.copyOf().makeUnmodifiable(), this.p, this.m.copyOf().makeUnmodifiable(), this.n.copyOf().makeUnmodifiable(), this.t, this.x, this.v, this.w, this.u, this.j, this.o);
  }

  public GsonBuilder disableHtmlEscaping()
  {
    this.v = false;
    return this;
  }

  public GsonBuilder disableInnerClassSerialization()
  {
    this.h = false;
    return this;
  }

  public GsonBuilder enableComplexMapKeySerialization()
  {
    this.t = true;
    return this;
  }

  public GsonBuilder excludeFieldsWithModifiers(int[] paramArrayOfInt)
  {
    this.g = new n(paramArrayOfInt);
    return this;
  }

  public GsonBuilder excludeFieldsWithoutExposeAnnotation()
  {
    this.i = true;
    return this;
  }

  public GsonBuilder generateNonExecutableJson()
  {
    this.x = true;
    return this;
  }

  public GsonBuilder registerTypeAdapter(Type paramType, Object paramObject)
  {
    return a(paramType, paramObject, false);
  }

  public GsonBuilder registerTypeHierarchyAdapter(Class<?> paramClass, Object paramObject)
  {
    return a(paramClass, paramObject, false);
  }

  public GsonBuilder serializeNulls()
  {
    this.p = true;
    return this;
  }

  public GsonBuilder serializeSpecialFloatingPointValues()
  {
    this.u = true;
    return this;
  }

  public GsonBuilder setDateFormat(int paramInt)
  {
    this.r = paramInt;
    this.q = null;
    return this;
  }

  public GsonBuilder setDateFormat(int paramInt1, int paramInt2)
  {
    this.r = paramInt1;
    this.s = paramInt2;
    this.q = null;
    return this;
  }

  public GsonBuilder setDateFormat(String paramString)
  {
    this.q = paramString;
    return this;
  }

  public GsonBuilder setExclusionStrategies(ExclusionStrategy[] paramArrayOfExclusionStrategy)
  {
    List localList = Arrays.asList(paramArrayOfExclusionStrategy);
    this.d.addAll(localList);
    this.e.addAll(localList);
    return this;
  }

  public GsonBuilder setFieldNamingPolicy(FieldNamingPolicy paramFieldNamingPolicy)
  {
    return a(paramFieldNamingPolicy.a());
  }

  public GsonBuilder setFieldNamingStrategy(FieldNamingStrategy paramFieldNamingStrategy)
  {
    return a(new w(paramFieldNamingStrategy));
  }

  public GsonBuilder setLongSerializationPolicy(LongSerializationPolicy paramLongSerializationPolicy)
  {
    this.j = paramLongSerializationPolicy;
    return this;
  }

  public GsonBuilder setPrettyPrinting()
  {
    this.w = true;
    return this;
  }

  public GsonBuilder setVersion(double paramDouble)
  {
    this.f = paramDouble;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.GsonBuilder
 * JD-Core Version:    0.6.2
 */