package com.google.myjson;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class JsonArray extends JsonElement
  implements Iterable<JsonElement>
{
  private final List<JsonElement> a = new ArrayList();

  public void add(JsonElement paramJsonElement)
  {
    if (paramJsonElement == null)
      paramJsonElement = JsonNull.INSTANCE;
    this.a.add(paramJsonElement);
  }

  public void addAll(JsonArray paramJsonArray)
  {
    this.a.addAll(paramJsonArray.a);
  }

  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof JsonArray)) && (((JsonArray)paramObject).a.equals(this.a)));
  }

  public JsonElement get(int paramInt)
  {
    return (JsonElement)this.a.get(paramInt);
  }

  public BigDecimal getAsBigDecimal()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsBigDecimal();
    throw new IllegalStateException();
  }

  public BigInteger getAsBigInteger()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsBigInteger();
    throw new IllegalStateException();
  }

  public boolean getAsBoolean()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsBoolean();
    throw new IllegalStateException();
  }

  public byte getAsByte()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsByte();
    throw new IllegalStateException();
  }

  public char getAsCharacter()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsCharacter();
    throw new IllegalStateException();
  }

  public double getAsDouble()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsDouble();
    throw new IllegalStateException();
  }

  public float getAsFloat()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsFloat();
    throw new IllegalStateException();
  }

  public int getAsInt()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsInt();
    throw new IllegalStateException();
  }

  public long getAsLong()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsLong();
    throw new IllegalStateException();
  }

  public Number getAsNumber()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsNumber();
    throw new IllegalStateException();
  }

  public short getAsShort()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsShort();
    throw new IllegalStateException();
  }

  public String getAsString()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).getAsString();
    throw new IllegalStateException();
  }

  public int hashCode()
  {
    return this.a.hashCode();
  }

  public Iterator<JsonElement> iterator()
  {
    return this.a.iterator();
  }

  public int size()
  {
    return this.a.size();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonArray
 * JD-Core Version:    0.6.2
 */