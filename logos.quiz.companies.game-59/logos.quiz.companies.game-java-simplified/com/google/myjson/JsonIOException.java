package com.google.myjson;

public final class JsonIOException extends JsonParseException
{
  private static final long serialVersionUID = 1L;

  public JsonIOException(String paramString)
  {
    super(paramString);
  }

  public JsonIOException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonIOException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonIOException
 * JD-Core Version:    0.6.2
 */