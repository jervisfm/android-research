package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class JsonObject extends JsonElement
{
  private final Map<String, JsonElement> a = new LinkedHashMap();

  private JsonElement a(Object paramObject)
  {
    if (paramObject == null)
      return JsonNull.INSTANCE;
    return new JsonPrimitive(paramObject);
  }

  public void add(String paramString, JsonElement paramJsonElement)
  {
    if (paramJsonElement == null)
      paramJsonElement = JsonNull.INSTANCE;
    this.a.put(.Gson.Preconditions.checkNotNull(paramString), paramJsonElement);
  }

  public void addProperty(String paramString, Boolean paramBoolean)
  {
    add(paramString, a(paramBoolean));
  }

  public void addProperty(String paramString, Character paramCharacter)
  {
    add(paramString, a(paramCharacter));
  }

  public void addProperty(String paramString, Number paramNumber)
  {
    add(paramString, a(paramNumber));
  }

  public void addProperty(String paramString1, String paramString2)
  {
    add(paramString1, a(paramString2));
  }

  public Set<Map.Entry<String, JsonElement>> entrySet()
  {
    return this.a.entrySet();
  }

  public boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof JsonObject)) && (((JsonObject)paramObject).a.equals(this.a)));
  }

  public JsonElement get(String paramString)
  {
    if (this.a.containsKey(paramString))
    {
      Object localObject = (JsonElement)this.a.get(paramString);
      if (localObject == null)
        localObject = JsonNull.INSTANCE;
      return localObject;
    }
    return null;
  }

  public JsonArray getAsJsonArray(String paramString)
  {
    return (JsonArray)this.a.get(paramString);
  }

  public JsonObject getAsJsonObject(String paramString)
  {
    return (JsonObject)this.a.get(paramString);
  }

  public JsonPrimitive getAsJsonPrimitive(String paramString)
  {
    return (JsonPrimitive)this.a.get(paramString);
  }

  public boolean has(String paramString)
  {
    return this.a.containsKey(paramString);
  }

  public int hashCode()
  {
    return this.a.hashCode();
  }

  public JsonElement remove(String paramString)
  {
    return (JsonElement)this.a.remove(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonObject
 * JD-Core Version:    0.6.2
 */