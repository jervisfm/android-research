package com.google.myjson;

public class JsonParseException extends RuntimeException
{
  static final long serialVersionUID = -4086729973971783390L;

  public JsonParseException(String paramString)
  {
    super(paramString);
  }

  public JsonParseException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonParseException
 * JD-Core Version:    0.6.2
 */