package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import com.google.myjson.internal.LazilyParsedNumber;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class JsonPrimitive extends JsonElement
{
  private static final Class<?>[] a = arrayOfClass;
  private Object b;

  static
  {
    Class[] arrayOfClass = new Class[16];
    arrayOfClass[0] = Integer.TYPE;
    arrayOfClass[1] = Long.TYPE;
    arrayOfClass[2] = Short.TYPE;
    arrayOfClass[3] = Float.TYPE;
    arrayOfClass[4] = Double.TYPE;
    arrayOfClass[5] = Byte.TYPE;
    arrayOfClass[6] = Boolean.TYPE;
    arrayOfClass[7] = Character.TYPE;
    arrayOfClass[8] = Integer.class;
    arrayOfClass[9] = Long.class;
    arrayOfClass[10] = Short.class;
    arrayOfClass[11] = Float.class;
    arrayOfClass[12] = Double.class;
    arrayOfClass[13] = Byte.class;
    arrayOfClass[14] = Boolean.class;
    arrayOfClass[15] = Character.class;
  }

  public JsonPrimitive(Boolean paramBoolean)
  {
    a(paramBoolean);
  }

  public JsonPrimitive(Character paramCharacter)
  {
    a(paramCharacter);
  }

  public JsonPrimitive(Number paramNumber)
  {
    a(paramNumber);
  }

  JsonPrimitive(Object paramObject)
  {
    a(paramObject);
  }

  public JsonPrimitive(String paramString)
  {
    a(paramString);
  }

  private static boolean a(JsonPrimitive paramJsonPrimitive)
  {
    if ((paramJsonPrimitive.b instanceof Number))
    {
      Number localNumber = (Number)paramJsonPrimitive.b;
      return ((localNumber instanceof BigInteger)) || ((localNumber instanceof Long)) || ((localNumber instanceof Integer)) || ((localNumber instanceof Short)) || ((localNumber instanceof Byte));
    }
    return false;
  }

  private static boolean b(Object paramObject)
  {
    if ((paramObject instanceof String))
      return true;
    Class localClass = paramObject.getClass();
    Class[] arrayOfClass = a;
    int i = arrayOfClass.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        break label47;
      if (arrayOfClass[j].isAssignableFrom(localClass))
        break;
    }
    label47: return false;
  }

  Boolean a()
  {
    return (Boolean)this.b;
  }

  void a(Object paramObject)
  {
    if ((paramObject instanceof Character))
    {
      this.b = String.valueOf(((Character)paramObject).charValue());
      return;
    }
    if (((paramObject instanceof Number)) || (b(paramObject)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      this.b = paramObject;
      return;
    }
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    JsonPrimitive localJsonPrimitive;
    do
    {
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass()))
          return false;
        localJsonPrimitive = (JsonPrimitive)paramObject;
        if (this.b != null)
          break;
      }
      while (localJsonPrimitive.b == null);
      return false;
      if ((!a(this)) || (!a(localJsonPrimitive)))
        break;
    }
    while (getAsNumber().longValue() == localJsonPrimitive.getAsNumber().longValue());
    return false;
    if (((this.b instanceof Number)) && ((localJsonPrimitive.b instanceof Number)))
    {
      double d1 = getAsNumber().doubleValue();
      double d2 = localJsonPrimitive.getAsNumber().doubleValue();
      boolean bool1;
      if (d1 != d2)
      {
        boolean bool2 = Double.isNaN(d1);
        bool1 = false;
        if (bool2)
        {
          boolean bool3 = Double.isNaN(d2);
          bool1 = false;
          if (!bool3);
        }
      }
      else
      {
        bool1 = true;
      }
      return bool1;
    }
    return this.b.equals(localJsonPrimitive.b);
  }

  public BigDecimal getAsBigDecimal()
  {
    if ((this.b instanceof BigDecimal))
      return (BigDecimal)this.b;
    return new BigDecimal(this.b.toString());
  }

  public BigInteger getAsBigInteger()
  {
    if ((this.b instanceof BigInteger))
      return (BigInteger)this.b;
    return new BigInteger(this.b.toString());
  }

  public boolean getAsBoolean()
  {
    if (isBoolean())
      return a().booleanValue();
    return Boolean.parseBoolean(getAsString());
  }

  public byte getAsByte()
  {
    if (isNumber())
      return getAsNumber().byteValue();
    return Byte.parseByte(getAsString());
  }

  public char getAsCharacter()
  {
    return getAsString().charAt(0);
  }

  public double getAsDouble()
  {
    if (isNumber())
      return getAsNumber().doubleValue();
    return Double.parseDouble(getAsString());
  }

  public float getAsFloat()
  {
    if (isNumber())
      return getAsNumber().floatValue();
    return Float.parseFloat(getAsString());
  }

  public int getAsInt()
  {
    if (isNumber())
      return getAsNumber().intValue();
    return Integer.parseInt(getAsString());
  }

  public long getAsLong()
  {
    if (isNumber())
      return getAsNumber().longValue();
    return Long.parseLong(getAsString());
  }

  public Number getAsNumber()
  {
    if ((this.b instanceof String))
      return new LazilyParsedNumber((String)this.b);
    return (Number)this.b;
  }

  public short getAsShort()
  {
    if (isNumber())
      return getAsNumber().shortValue();
    return Short.parseShort(getAsString());
  }

  public String getAsString()
  {
    if (isNumber())
      return getAsNumber().toString();
    if (isBoolean())
      return a().toString();
    return (String)this.b;
  }

  public int hashCode()
  {
    if (this.b == null)
      return 31;
    if (a(this))
    {
      long l2 = getAsNumber().longValue();
      return (int)(l2 ^ l2 >>> 32);
    }
    if ((this.b instanceof Number))
    {
      long l1 = Double.doubleToLongBits(getAsNumber().doubleValue());
      return (int)(l1 ^ l1 >>> 32);
    }
    return this.b.hashCode();
  }

  public boolean isBoolean()
  {
    return this.b instanceof Boolean;
  }

  public boolean isNumber()
  {
    return this.b instanceof Number;
  }

  public boolean isString()
  {
    return this.b instanceof String;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonPrimitive
 * JD-Core Version:    0.6.2
 */