package com.google.myjson;

import com.google.myjson.internal.Streams;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser
  implements Iterator<JsonElement>
{
  private final JsonReader a;
  private final Object b;

  public JsonStreamParser(Reader paramReader)
  {
    this.a = new JsonReader(paramReader);
    this.a.setLenient(true);
    this.b = new Object();
  }

  public JsonStreamParser(String paramString)
  {
    this(new StringReader(paramString));
  }

  public boolean hasNext()
  {
    while (true)
    {
      try
      {
        synchronized (this.b)
        {
          try
          {
            JsonToken localJsonToken1 = this.a.peek();
            JsonToken localJsonToken2 = JsonToken.END_DOCUMENT;
            if (localJsonToken1 != localJsonToken2)
            {
              bool = true;
              return bool;
            }
          }
          catch (MalformedJsonException localMalformedJsonException)
          {
            throw new JsonSyntaxException(localMalformedJsonException);
          }
        }
      }
      catch (IOException localIOException)
      {
        throw new JsonIOException(localIOException);
      }
      boolean bool = false;
    }
  }

  public JsonElement next()
    throws JsonParseException
  {
    if (!hasNext())
      throw new NoSuchElementException();
    try
    {
      JsonElement localJsonElement = Streams.parse(this.a);
      return localJsonElement;
    }
    catch (StackOverflowError localStackOverflowError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localStackOverflowError);
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localOutOfMemoryError);
    }
    catch (JsonParseException localJsonParseException)
    {
      NoSuchElementException localNoSuchElementException;
      if ((localJsonParseException.getCause() instanceof EOFException))
        localNoSuchElementException = new NoSuchElementException();
      throw localNoSuchElementException;
    }
  }

  public void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.JsonStreamParser
 * JD-Core Version:    0.6.2
 */