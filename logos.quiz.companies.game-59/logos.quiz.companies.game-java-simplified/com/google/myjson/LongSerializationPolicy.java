package com.google.myjson;

public enum LongSerializationPolicy
{
  private final a a;

  static
  {
    LongSerializationPolicy[] arrayOfLongSerializationPolicy = new LongSerializationPolicy[2];
    arrayOfLongSerializationPolicy[0] = DEFAULT;
    arrayOfLongSerializationPolicy[1] = STRING;
  }

  private LongSerializationPolicy(a parama)
  {
    this.a = parama;
  }

  public JsonElement serialize(Long paramLong)
  {
    return this.a.serialize(paramLong);
  }

  private static abstract interface a
  {
    public abstract JsonElement serialize(Long paramLong);
  }

  private static class b
    implements LongSerializationPolicy.a
  {
    public JsonElement serialize(Long paramLong)
    {
      return new JsonPrimitive(String.valueOf(paramLong));
    }
  }

  private static class c
    implements LongSerializationPolicy.a
  {
    public JsonElement serialize(Long paramLong)
    {
      return new JsonPrimitive(paramLong);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.LongSerializationPolicy
 * JD-Core Version:    0.6.2
 */