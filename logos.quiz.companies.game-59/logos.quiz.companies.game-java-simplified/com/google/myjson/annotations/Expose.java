package com.google.myjson.annotations;

import java.lang.annotation.Annotation;

public @interface Expose
{
  public abstract boolean deserialize();

  public abstract boolean serialize();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.annotations.Expose
 * JD-Core Version:    0.6.2
 */