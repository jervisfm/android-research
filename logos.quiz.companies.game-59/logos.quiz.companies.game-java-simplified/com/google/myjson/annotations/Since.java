package com.google.myjson.annotations;

import java.lang.annotation.Annotation;

public @interface Since
{
  public abstract double value();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.annotations.Since
 * JD-Core Version:    0.6.2
 */