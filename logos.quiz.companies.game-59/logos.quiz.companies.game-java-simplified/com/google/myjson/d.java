package com.google.myjson;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

final class d<K, V> extends LinkedHashMap<K, V>
  implements c<K, V>
{
  private static final long serialVersionUID = 1L;
  private final int a;

  public d(int paramInt)
  {
    super(paramInt, 0.7F, true);
    this.a = paramInt;
  }

  public void addElement(K paramK, V paramV)
  {
    try
    {
      put(paramK, paramV);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public V getElement(K paramK)
  {
    try
    {
      Object localObject2 = get(paramK);
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      throw localObject1;
    }
  }

  protected boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
  {
    return size() > this.a;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.d
 * JD-Core Version:    0.6.2
 */