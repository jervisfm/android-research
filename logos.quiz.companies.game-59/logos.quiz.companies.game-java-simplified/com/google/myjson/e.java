package com.google.myjson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class e
  implements l
{
  protected abstract String a(String paramString, Type paramType, Collection<Annotation> paramCollection);

  public final String translateName(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.getName(), paramFieldAttributes.getDeclaredType(), paramFieldAttributes.getAnnotations());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.e
 * JD-Core Version:    0.6.2
 */