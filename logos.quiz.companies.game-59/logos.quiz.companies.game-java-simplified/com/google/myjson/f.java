package com.google.myjson;

import com.google.myjson.annotations.SerializedName;

final class f
  implements l
{
  private final l a;

  f(l paraml)
  {
    this.a = paraml;
  }

  public String translateName(FieldAttributes paramFieldAttributes)
  {
    SerializedName localSerializedName = (SerializedName)paramFieldAttributes.getAnnotation(SerializedName.class);
    if (localSerializedName == null)
      return this.a.translateName(paramFieldAttributes);
    return localSerializedName.value();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.f
 * JD-Core Version:    0.6.2
 */