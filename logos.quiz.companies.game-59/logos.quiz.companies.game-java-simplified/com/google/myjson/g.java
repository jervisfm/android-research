package com.google.myjson;

final class g
  implements ExclusionStrategy
{
  private final boolean a;

  g(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return (this.a) && (paramFieldAttributes.a());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.g
 * JD-Core Version:    0.6.2
 */