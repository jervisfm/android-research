package com.google.myjson.internal;

import com.google.myjson.InstanceCreator;
import com.google.myjson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public final class ConstructorConstructor
{
  private final ParameterizedTypeHandlerMap<InstanceCreator<?>> a;

  public ConstructorConstructor()
  {
    this(new ParameterizedTypeHandlerMap());
  }

  public ConstructorConstructor(ParameterizedTypeHandlerMap<InstanceCreator<?>> paramParameterizedTypeHandlerMap)
  {
    this.a = paramParameterizedTypeHandlerMap;
  }

  private <T> ObjectConstructor<T> a(Class<? super T> paramClass)
  {
    try
    {
      final Constructor localConstructor = paramClass.getDeclaredConstructor(new Class[0]);
      if (!localConstructor.isAccessible())
        localConstructor.setAccessible(true);
      ObjectConstructor local2 = new ObjectConstructor()
      {
        public T construct()
        {
          try
          {
            Object localObject = localConstructor.newInstance(null);
            return localObject;
          }
          catch (InstantiationException localInstantiationException)
          {
            throw new RuntimeException("Failed to invoke " + localConstructor + " with no args", localInstantiationException);
          }
          catch (InvocationTargetException localInvocationTargetException)
          {
            throw new RuntimeException("Failed to invoke " + localConstructor + " with no args", localInvocationTargetException.getTargetException());
          }
          catch (IllegalAccessException localIllegalAccessException)
          {
            throw new AssertionError(localIllegalAccessException);
          }
        }
      };
      return local2;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
    }
    return null;
  }

  private <T> ObjectConstructor<T> a(final Type paramType, final Class<? super T> paramClass)
  {
    return new ObjectConstructor()
    {
      private final UnsafeAllocator d = UnsafeAllocator.create();

      public T construct()
      {
        try
        {
          Object localObject = this.d.newInstance(paramClass);
          return localObject;
        }
        catch (Exception localException)
        {
          throw new RuntimeException("Unable to invoke no-args constructor for " + paramType + ". " + "Register an InstanceCreator with Gson for this type may fix this problem.", localException);
        }
      }
    };
  }

  private <T> ObjectConstructor<T> b(Class<? super T> paramClass)
  {
    if (Collection.class.isAssignableFrom(paramClass))
    {
      if (SortedSet.class.isAssignableFrom(paramClass))
        return new ObjectConstructor()
        {
          public T construct()
          {
            return new TreeSet();
          }
        };
      if (Set.class.isAssignableFrom(paramClass))
        return new ObjectConstructor()
        {
          public T construct()
          {
            return new LinkedHashSet();
          }
        };
      if (Queue.class.isAssignableFrom(paramClass))
        return new ObjectConstructor()
        {
          public T construct()
          {
            return new LinkedList();
          }
        };
      return new ObjectConstructor()
      {
        public T construct()
        {
          return new ArrayList();
        }
      };
    }
    if (Map.class.isAssignableFrom(paramClass))
      return new ObjectConstructor()
      {
        public T construct()
        {
          return new LinkedHashMap();
        }
      };
    return null;
  }

  public <T> ObjectConstructor<T> getConstructor(TypeToken<T> paramTypeToken)
  {
    final Type localType = paramTypeToken.getType();
    Class localClass = paramTypeToken.getRawType();
    final InstanceCreator localInstanceCreator = (InstanceCreator)this.a.getHandlerFor(localType, false);
    Object localObject;
    if (localInstanceCreator != null)
      localObject = new ObjectConstructor()
      {
        public T construct()
        {
          return localInstanceCreator.createInstance(localType);
        }
      };
    do
    {
      do
      {
        return localObject;
        localObject = a(localClass);
      }
      while (localObject != null);
      localObject = b(localClass);
    }
    while (localObject != null);
    return a(localType, localClass);
  }

  public String toString()
  {
    return this.a.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.ConstructorConstructor
 * JD-Core Version:    0.6.2
 */