package com.google.myjson.internal;

import java.math.BigInteger;

public final class LazilyParsedNumber extends Number
{
  private final String a;

  public LazilyParsedNumber(String paramString)
  {
    this.a = paramString;
  }

  public double doubleValue()
  {
    return Double.parseDouble(this.a);
  }

  public float floatValue()
  {
    return Float.parseFloat(this.a);
  }

  public int intValue()
  {
    try
    {
      int i = Integer.parseInt(this.a);
      return i;
    }
    catch (NumberFormatException localNumberFormatException1)
    {
      try
      {
        long l = Long.parseLong(this.a);
        return (int)l;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
      }
    }
    return new BigInteger(this.a).intValue();
  }

  public long longValue()
  {
    try
    {
      long l = Long.parseLong(this.a);
      return l;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return new BigInteger(this.a).longValue();
  }

  public String toString()
  {
    return this.a;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.LazilyParsedNumber
 * JD-Core Version:    0.6.2
 */