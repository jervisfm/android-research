package com.google.myjson.internal;

public abstract interface ObjectConstructor<T>
{
  public abstract T construct();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.ObjectConstructor
 * JD-Core Version:    0.6.2
 */