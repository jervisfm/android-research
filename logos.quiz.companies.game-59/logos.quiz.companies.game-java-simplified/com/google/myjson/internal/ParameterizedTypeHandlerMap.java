package com.google.myjson.internal;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ParameterizedTypeHandlerMap<T>
{
  private static final Logger a = Logger.getLogger(ParameterizedTypeHandlerMap.class.getName());
  private final Map<Type, T> b = new HashMap();
  private final Map<Type, T> c = new HashMap();
  private final List<Pair<Class<?>, T>> d = new ArrayList();
  private final List<Pair<Class<?>, T>> e = new ArrayList();
  private boolean f = true;

  private static <T> int a(Class<?> paramClass, List<Pair<Class<?>, T>> paramList)
  {
    for (int i = -1 + paramList.size(); i >= 0; i--)
      if (paramClass.isAssignableFrom((Class)((Pair)paramList.get(i)).first))
        return i;
    return -1;
  }

  private T a(Class<?> paramClass, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      Iterator localIterator2 = this.e.iterator();
      while (localIterator2.hasNext())
      {
        Pair localPair2 = (Pair)localIterator2.next();
        if (((Class)localPair2.first).isAssignableFrom(paramClass))
          return localPair2.second;
      }
    }
    Iterator localIterator1 = this.d.iterator();
    while (localIterator1.hasNext())
    {
      Pair localPair1 = (Pair)localIterator1.next();
      if (((Class)localPair1.first).isAssignableFrom(paramClass))
        return localPair1.second;
    }
    return null;
  }

  private String a(Type paramType)
  {
    return .Gson.Types.getRawType(paramType).getSimpleName();
  }

  private void a(StringBuilder paramStringBuilder, List<Pair<Class<?>, T>> paramList)
  {
    Iterator localIterator = paramList.iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Pair localPair = (Pair)localIterator.next();
      if (i != 0);
      for (int j = 0; ; j = i)
      {
        paramStringBuilder.append(a((Type)localPair.first)).append(':');
        paramStringBuilder.append(localPair.second);
        i = j;
        break;
        paramStringBuilder.append(',');
      }
    }
  }

  private void a(StringBuilder paramStringBuilder, Map<Type, T> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (i != 0);
      for (int j = 0; ; j = i)
      {
        paramStringBuilder.append(a((Type)localEntry.getKey())).append(':');
        paramStringBuilder.append(localEntry.getValue());
        i = j;
        break;
        paramStringBuilder.append(',');
      }
    }
  }

  private static <T> int b(Class<?> paramClass, List<Pair<Class<?>, T>> paramList)
  {
    for (int i = -1 + paramList.size(); i >= 0; i--)
      if (paramClass.equals(((Pair)paramList.get(i)).first))
        return i;
    return -1;
  }

  public ParameterizedTypeHandlerMap<T> copyOf()
  {
    try
    {
      ParameterizedTypeHandlerMap localParameterizedTypeHandlerMap = new ParameterizedTypeHandlerMap();
      localParameterizedTypeHandlerMap.b.putAll(this.b);
      localParameterizedTypeHandlerMap.c.putAll(this.c);
      localParameterizedTypeHandlerMap.d.addAll(this.d);
      localParameterizedTypeHandlerMap.e.addAll(this.e);
      return localParameterizedTypeHandlerMap;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public T getHandlerFor(Type paramType, boolean paramBoolean)
  {
    if (!paramBoolean);
    try
    {
      Object localObject4 = this.c.get(paramType);
      Object localObject2 = localObject4;
      if (localObject2 != null);
      while (true)
      {
        return localObject2;
        localObject2 = this.b.get(paramType);
        if (localObject2 == null)
        {
          Class localClass = .Gson.Types.getRawType(paramType);
          if (localClass != paramType)
          {
            localObject2 = getHandlerFor(localClass, paramBoolean);
            if (localObject2 != null);
          }
          else
          {
            Object localObject3 = a(localClass, paramBoolean);
            localObject2 = localObject3;
          }
        }
      }
    }
    finally
    {
    }
  }

  public boolean hasSpecificHandlerFor(Type paramType)
  {
    try
    {
      if (!this.c.containsKey(paramType))
      {
        boolean bool2 = this.b.containsKey(paramType);
        if (!bool2);
      }
      else
      {
        bool1 = true;
        return bool1;
      }
      boolean bool1 = false;
    }
    finally
    {
    }
  }

  public ParameterizedTypeHandlerMap<T> makeUnmodifiable()
  {
    try
    {
      this.f = false;
      return this;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void register(Type paramType, T paramT, boolean paramBoolean)
  {
    try
    {
      if (!this.f)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    if (hasSpecificHandlerFor(paramType))
      a.log(Level.WARNING, "Overriding the existing type handler for {0}", paramType);
    if (paramBoolean);
    for (Map localMap = this.b; ; localMap = this.c)
    {
      localMap.put(paramType, paramT);
      return;
    }
  }

  public void registerForTypeHierarchy(Pair<Class<?>, T> paramPair, boolean paramBoolean)
  {
    try
    {
      if (!this.f)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    if (paramBoolean);
    for (List localList = this.d; ; localList = this.e)
    {
      int i = b((Class)paramPair.first, localList);
      if (i >= 0)
      {
        a.log(Level.WARNING, "Overriding the existing type handler for {0}", paramPair.first);
        localList.remove(i);
      }
      int j = a((Class)paramPair.first, localList);
      if (j < 0)
        break;
      throw new IllegalArgumentException("The specified type handler for type " + paramPair.first + " hides the previously registered type hierarchy handler for " + ((Pair)localList.get(j)).first + ". Gson does not allow this.");
    }
    localList.add(0, paramPair);
  }

  public void registerForTypeHierarchy(Class<?> paramClass, T paramT, boolean paramBoolean)
  {
    try
    {
      registerForTypeHierarchy(new Pair(paramClass, paramT), paramBoolean);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void registerIfAbsent(ParameterizedTypeHandlerMap<T> paramParameterizedTypeHandlerMap)
  {
    try
    {
      if (!this.f)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    Iterator localIterator1 = paramParameterizedTypeHandlerMap.c.entrySet().iterator();
    while (localIterator1.hasNext())
    {
      Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
      if (!this.c.containsKey(localEntry2.getKey()))
        register((Type)localEntry2.getKey(), localEntry2.getValue(), false);
    }
    Iterator localIterator2 = paramParameterizedTypeHandlerMap.b.entrySet().iterator();
    while (localIterator2.hasNext())
    {
      Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
      if (!this.b.containsKey(localEntry1.getKey()))
        register((Type)localEntry1.getKey(), localEntry1.getValue(), true);
    }
    for (int i = -1 + paramParameterizedTypeHandlerMap.e.size(); ; i--)
      if (i >= 0)
      {
        Pair localPair1 = (Pair)paramParameterizedTypeHandlerMap.e.get(i);
        if (b((Class)localPair1.first, this.e) < 0)
          registerForTypeHierarchy(localPair1, false);
      }
      else
      {
        for (int j = -1 + paramParameterizedTypeHandlerMap.d.size(); j >= 0; j--)
        {
          Pair localPair2 = (Pair)paramParameterizedTypeHandlerMap.d.get(j);
          if (b((Class)localPair2.first, this.d) < 0)
            registerForTypeHierarchy(localPair2, true);
        }
        return;
      }
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("{userTypeHierarchyList:{");
    a(localStringBuilder, this.e);
    localStringBuilder.append("},systemTypeHierarchyList:{");
    a(localStringBuilder, this.d);
    localStringBuilder.append("},userMap:{");
    a(localStringBuilder, this.c);
    localStringBuilder.append("},systemMap:{");
    a(localStringBuilder, this.b);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.ParameterizedTypeHandlerMap
 * JD-Core Version:    0.6.2
 */