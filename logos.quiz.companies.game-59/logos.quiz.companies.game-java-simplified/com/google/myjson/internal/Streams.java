package com.google.myjson.internal;

import com.google.myjson.JsonElement;
import com.google.myjson.JsonIOException;
import com.google.myjson.JsonNull;
import com.google.myjson.JsonParseException;
import com.google.myjson.JsonSyntaxException;
import com.google.myjson.internal.bind.TypeAdapter;
import com.google.myjson.internal.bind.TypeAdapters;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import com.google.myjson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class Streams
{
  public static JsonElement parse(JsonReader paramJsonReader)
    throws JsonParseException
  {
    int i = 1;
    try
    {
      paramJsonReader.peek();
      i = 0;
      JsonElement localJsonElement = (JsonElement)TypeAdapters.JSON_ELEMENT.read(paramJsonReader);
      return localJsonElement;
    }
    catch (EOFException localEOFException)
    {
      if (i != 0)
        return JsonNull.INSTANCE;
      throw new JsonIOException(localEOFException);
    }
    catch (MalformedJsonException localMalformedJsonException)
    {
      throw new JsonSyntaxException(localMalformedJsonException);
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }

  public static void write(JsonElement paramJsonElement, JsonWriter paramJsonWriter)
    throws IOException
  {
    TypeAdapters.JSON_ELEMENT.write(paramJsonWriter, paramJsonElement);
  }

  public static Writer writerForAppendable(Appendable paramAppendable)
  {
    if ((paramAppendable instanceof Writer))
      return (Writer)paramAppendable;
    return new a(paramAppendable, null);
  }

  private static class a extends Writer
  {
    private final Appendable a;
    private final a b = new a();

    private a(Appendable paramAppendable)
    {
      this.a = paramAppendable;
    }

    public void close()
    {
    }

    public void flush()
    {
    }

    public void write(int paramInt)
      throws IOException
    {
      this.a.append((char)paramInt);
    }

    public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
      throws IOException
    {
      this.b.a = paramArrayOfChar;
      this.a.append(this.b, paramInt1, paramInt1 + paramInt2);
    }

    static class a
      implements CharSequence
    {
      char[] a;

      public char charAt(int paramInt)
      {
        return this.a[paramInt];
      }

      public int length()
      {
        return this.a.length;
      }

      public CharSequence subSequence(int paramInt1, int paramInt2)
      {
        return new String(this.a, paramInt1, paramInt2 - paramInt1);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.Streams
 * JD-Core Version:    0.6.2
 */