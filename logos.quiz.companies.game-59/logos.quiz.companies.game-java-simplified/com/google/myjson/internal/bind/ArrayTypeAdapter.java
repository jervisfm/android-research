package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class ArrayTypeAdapter<E> extends TypeAdapter<Object>
{
  public static final TypeAdapter.Factory FACTORY = new e();
  private final Class<E> a;
  private final TypeAdapter<E> b;

  public ArrayTypeAdapter(MiniGson paramMiniGson, TypeAdapter<E> paramTypeAdapter, Class<E> paramClass)
  {
    this.b = new c(paramMiniGson, paramTypeAdapter, paramClass);
    this.a = paramClass;
  }

  public Object read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    paramJsonReader.beginArray();
    while (paramJsonReader.hasNext())
      localArrayList.add(this.b.read(paramJsonReader));
    paramJsonReader.endArray();
    Object localObject = Array.newInstance(this.a, localArrayList.size());
    for (int i = 0; i < localArrayList.size(); i++)
      Array.set(localObject, i, localArrayList.get(i));
    return localObject;
  }

  public void write(JsonWriter paramJsonWriter, Object paramObject)
    throws IOException
  {
    if (paramObject == null)
    {
      paramJsonWriter.nullValue();
      return;
    }
    paramJsonWriter.beginArray();
    int i = 0;
    int j = Array.getLength(paramObject);
    while (i < j)
    {
      Object localObject = Array.get(paramObject, i);
      this.b.write(paramJsonWriter, localObject);
      i++;
    }
    paramJsonWriter.endArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.ArrayTypeAdapter
 * JD-Core Version:    0.6.2
 */