package com.google.myjson.internal.bind;

import com.google.myjson.internal..Gson.Types;
import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.internal.ObjectConstructor;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;

public final class CollectionTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public CollectionTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType1 = paramTypeToken.getType();
    Class localClass = paramTypeToken.getRawType();
    if (!Collection.class.isAssignableFrom(localClass))
      return null;
    Type localType2 = .Gson.Types.getCollectionElementType(localType1, localClass);
    return new a(paramMiniGson, localType2, paramMiniGson.getAdapter(TypeToken.get(localType2)), this.a.getConstructor(paramTypeToken));
  }

  private final class a<E> extends TypeAdapter<Collection<E>>
  {
    private final TypeAdapter<E> b;
    private final ObjectConstructor<? extends Collection<E>> c;

    public a(Type paramTypeAdapter, TypeAdapter<E> paramObjectConstructor, ObjectConstructor<? extends Collection<E>> arg4)
    {
      TypeAdapter localTypeAdapter;
      this.b = new c(paramTypeAdapter, localTypeAdapter, paramObjectConstructor);
      Object localObject;
      this.c = localObject;
    }

    public Collection<E> read(JsonReader paramJsonReader)
      throws IOException
    {
      if (paramJsonReader.peek() == JsonToken.NULL)
      {
        paramJsonReader.nextNull();
        return null;
      }
      Collection localCollection = (Collection)this.c.construct();
      paramJsonReader.beginArray();
      while (paramJsonReader.hasNext())
        localCollection.add(this.b.read(paramJsonReader));
      paramJsonReader.endArray();
      return localCollection;
    }

    public void write(JsonWriter paramJsonWriter, Collection<E> paramCollection)
      throws IOException
    {
      if (paramCollection == null)
      {
        paramJsonWriter.nullValue();
        return;
      }
      paramJsonWriter.beginArray();
      Iterator localIterator = paramCollection.iterator();
      while (localIterator.hasNext())
      {
        Object localObject = localIterator.next();
        this.b.write(paramJsonWriter, localObject);
      }
      paramJsonWriter.endArray();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.CollectionTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */