package com.google.myjson.internal.bind;

import com.google.myjson.ExclusionStrategy;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;

public final class ExcludedTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ExclusionStrategy a;
  private final ExclusionStrategy b;

  public ExcludedTypeAdapterFactory(ExclusionStrategy paramExclusionStrategy1, ExclusionStrategy paramExclusionStrategy2)
  {
    this.a = paramExclusionStrategy1;
    this.b = paramExclusionStrategy2;
  }

  public <T> TypeAdapter<T> create(final MiniGson paramMiniGson, final TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.getRawType();
    final boolean bool1 = this.a.shouldSkipClass(localClass);
    final boolean bool2 = this.b.shouldSkipClass(localClass);
    if ((!bool1) && (!bool2))
      return null;
    return new TypeAdapter()
    {
      private TypeAdapter<T> f;

      private TypeAdapter<T> a()
      {
        TypeAdapter localTypeAdapter1 = this.f;
        if (localTypeAdapter1 != null)
          return localTypeAdapter1;
        TypeAdapter localTypeAdapter2 = paramMiniGson.getNextAdapter(ExcludedTypeAdapterFactory.this, paramTypeToken);
        this.f = localTypeAdapter2;
        return localTypeAdapter2;
      }

      public T read(JsonReader paramAnonymousJsonReader)
        throws IOException
      {
        if (bool2)
        {
          paramAnonymousJsonReader.skipValue();
          return null;
        }
        return a().read(paramAnonymousJsonReader);
      }

      public void write(JsonWriter paramAnonymousJsonWriter, T paramAnonymousT)
        throws IOException
      {
        if (bool1)
        {
          paramAnonymousJsonWriter.nullValue();
          return;
        }
        a().write(paramAnonymousJsonWriter, paramAnonymousT);
      }
    };
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.ExcludedTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */