package com.google.myjson.internal.bind;

import com.google.myjson.JsonArray;
import com.google.myjson.JsonElement;
import com.google.myjson.JsonNull;
import com.google.myjson.JsonObject;
import com.google.myjson.JsonPrimitive;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public final class JsonElementReader extends JsonReader
{
  private static final Reader a = new i();
  private static final Object b = new Object();
  private final List<Object> c = new ArrayList();

  public JsonElementReader(JsonElement paramJsonElement)
  {
    super(a);
    this.c.add(paramJsonElement);
  }

  private Object a()
  {
    return this.c.get(-1 + this.c.size());
  }

  private void a(JsonToken paramJsonToken)
    throws IOException
  {
    if (peek() != paramJsonToken)
      throw new IllegalStateException("Expected " + paramJsonToken + " but was " + peek());
  }

  private Object b()
  {
    return this.c.remove(-1 + this.c.size());
  }

  public void beginArray()
    throws IOException
  {
    a(JsonToken.BEGIN_ARRAY);
    JsonArray localJsonArray = (JsonArray)a();
    this.c.add(localJsonArray.iterator());
  }

  public void beginObject()
    throws IOException
  {
    a(JsonToken.BEGIN_OBJECT);
    JsonObject localJsonObject = (JsonObject)a();
    this.c.add(localJsonObject.entrySet().iterator());
  }

  public void close()
    throws IOException
  {
    this.c.clear();
    this.c.add(b);
  }

  public void endArray()
    throws IOException
  {
    a(JsonToken.END_ARRAY);
    b();
    b();
  }

  public void endObject()
    throws IOException
  {
    a(JsonToken.END_OBJECT);
    b();
    b();
  }

  public boolean hasNext()
    throws IOException
  {
    JsonToken localJsonToken = peek();
    return (localJsonToken != JsonToken.END_OBJECT) && (localJsonToken != JsonToken.END_ARRAY);
  }

  public boolean nextBoolean()
    throws IOException
  {
    a(JsonToken.BOOLEAN);
    return ((JsonPrimitive)b()).getAsBoolean();
  }

  public double nextDouble()
    throws IOException
  {
    JsonToken localJsonToken = peek();
    if ((localJsonToken != JsonToken.NUMBER) && (localJsonToken != JsonToken.STRING))
      throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + localJsonToken);
    double d = ((JsonPrimitive)a()).getAsDouble();
    if ((!isLenient()) && ((Double.isNaN(d)) || (Double.isInfinite(d))))
      throw new NumberFormatException("JSON forbids NaN and infinities: " + d);
    b();
    return d;
  }

  public int nextInt()
    throws IOException
  {
    JsonToken localJsonToken = peek();
    if ((localJsonToken != JsonToken.NUMBER) && (localJsonToken != JsonToken.STRING))
      throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + localJsonToken);
    int i = ((JsonPrimitive)a()).getAsInt();
    b();
    return i;
  }

  public long nextLong()
    throws IOException
  {
    JsonToken localJsonToken = peek();
    if ((localJsonToken != JsonToken.NUMBER) && (localJsonToken != JsonToken.STRING))
      throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + localJsonToken);
    long l = ((JsonPrimitive)a()).getAsLong();
    b();
    return l;
  }

  public String nextName()
    throws IOException
  {
    a(JsonToken.NAME);
    Map.Entry localEntry = (Map.Entry)((Iterator)a()).next();
    this.c.add(localEntry.getValue());
    return (String)localEntry.getKey();
  }

  public void nextNull()
    throws IOException
  {
    a(JsonToken.NULL);
    b();
  }

  public String nextString()
    throws IOException
  {
    JsonToken localJsonToken = peek();
    if ((localJsonToken != JsonToken.STRING) && (localJsonToken != JsonToken.NUMBER))
      throw new IllegalStateException("Expected " + JsonToken.STRING + " but was " + localJsonToken);
    return ((JsonPrimitive)b()).getAsString();
  }

  public JsonToken peek()
    throws IOException
  {
    if (this.c.isEmpty())
      return JsonToken.END_DOCUMENT;
    Object localObject = a();
    if ((localObject instanceof Iterator))
    {
      boolean bool = this.c.get(-2 + this.c.size()) instanceof JsonObject;
      Iterator localIterator = (Iterator)localObject;
      if (localIterator.hasNext())
      {
        if (bool)
          return JsonToken.NAME;
        this.c.add(localIterator.next());
        return peek();
      }
      if (bool)
        return JsonToken.END_OBJECT;
      return JsonToken.END_ARRAY;
    }
    if ((localObject instanceof JsonObject))
      return JsonToken.BEGIN_OBJECT;
    if ((localObject instanceof JsonArray))
      return JsonToken.BEGIN_ARRAY;
    if ((localObject instanceof JsonPrimitive))
    {
      JsonPrimitive localJsonPrimitive = (JsonPrimitive)localObject;
      if (localJsonPrimitive.isString())
        return JsonToken.STRING;
      if (localJsonPrimitive.isBoolean())
        return JsonToken.BOOLEAN;
      if (localJsonPrimitive.isNumber())
        return JsonToken.NUMBER;
      throw new AssertionError();
    }
    if ((localObject instanceof JsonNull))
      return JsonToken.NULL;
    if (localObject == b)
      throw new IllegalStateException("JsonReader is closed");
    throw new AssertionError();
  }

  public void skipValue()
    throws IOException
  {
    if (peek() == JsonToken.NAME)
    {
      nextName();
      return;
    }
    b();
  }

  public String toString()
  {
    return getClass().getSimpleName();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.JsonElementReader
 * JD-Core Version:    0.6.2
 */