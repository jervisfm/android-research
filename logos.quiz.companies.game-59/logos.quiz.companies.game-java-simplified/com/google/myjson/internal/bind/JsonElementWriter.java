package com.google.myjson.internal.bind;

import com.google.myjson.JsonArray;
import com.google.myjson.JsonElement;
import com.google.myjson.JsonNull;
import com.google.myjson.JsonObject;
import com.google.myjson.JsonPrimitive;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonElementWriter extends JsonWriter
{
  private static final Writer a = new h();
  private static final JsonPrimitive b = new JsonPrimitive("closed");
  private final List<JsonElement> c = new ArrayList();
  private String d;
  private JsonElement e = JsonNull.INSTANCE;

  public JsonElementWriter()
  {
    super(a);
  }

  private JsonElement a()
  {
    return (JsonElement)this.c.get(-1 + this.c.size());
  }

  private void a(JsonElement paramJsonElement)
  {
    if (this.d != null)
    {
      if ((!paramJsonElement.isJsonNull()) || (getSerializeNulls()))
        ((JsonObject)a()).add(this.d, paramJsonElement);
      this.d = null;
      return;
    }
    if (this.c.isEmpty())
    {
      this.e = paramJsonElement;
      return;
    }
    JsonElement localJsonElement = a();
    if ((localJsonElement instanceof JsonArray))
    {
      ((JsonArray)localJsonElement).add(paramJsonElement);
      return;
    }
    throw new IllegalStateException();
  }

  public JsonWriter beginArray()
    throws IOException
  {
    JsonArray localJsonArray = new JsonArray();
    a(localJsonArray);
    this.c.add(localJsonArray);
    return this;
  }

  public JsonWriter beginObject()
    throws IOException
  {
    JsonObject localJsonObject = new JsonObject();
    a(localJsonObject);
    this.c.add(localJsonObject);
    return this;
  }

  public void close()
    throws IOException
  {
    if (!this.c.isEmpty())
      throw new IOException("Incomplete document");
    this.c.add(b);
  }

  public JsonWriter endArray()
    throws IOException
  {
    if ((this.c.isEmpty()) || (this.d != null))
      throw new IllegalStateException();
    if ((a() instanceof JsonArray))
    {
      this.c.remove(-1 + this.c.size());
      return this;
    }
    throw new IllegalStateException();
  }

  public JsonWriter endObject()
    throws IOException
  {
    if ((this.c.isEmpty()) || (this.d != null))
      throw new IllegalStateException();
    if ((a() instanceof JsonObject))
    {
      this.c.remove(-1 + this.c.size());
      return this;
    }
    throw new IllegalStateException();
  }

  public void flush()
    throws IOException
  {
  }

  public JsonElement get()
  {
    if (!this.c.isEmpty())
      throw new IllegalStateException("Expected one JSON element but was " + this.c);
    return this.e;
  }

  public JsonWriter name(String paramString)
    throws IOException
  {
    if ((this.c.isEmpty()) || (this.d != null))
      throw new IllegalStateException();
    if ((a() instanceof JsonObject))
    {
      this.d = paramString;
      return this;
    }
    throw new IllegalStateException();
  }

  public JsonWriter nullValue()
    throws IOException
  {
    a(JsonNull.INSTANCE);
    return this;
  }

  public JsonWriter value(double paramDouble)
    throws IOException
  {
    if ((!isLenient()) && ((Double.isNaN(paramDouble)) || (Double.isInfinite(paramDouble))))
      throw new IllegalArgumentException("JSON forbids NaN and infinities: " + paramDouble);
    a(new JsonPrimitive(Double.valueOf(paramDouble)));
    return this;
  }

  public JsonWriter value(long paramLong)
    throws IOException
  {
    a(new JsonPrimitive(Long.valueOf(paramLong)));
    return this;
  }

  public JsonWriter value(Number paramNumber)
    throws IOException
  {
    if (paramNumber == null)
      return nullValue();
    if (!isLenient())
    {
      double d1 = paramNumber.doubleValue();
      if ((Double.isNaN(d1)) || (Double.isInfinite(d1)))
        throw new IllegalArgumentException("JSON forbids NaN and infinities: " + paramNumber);
    }
    a(new JsonPrimitive(paramNumber));
    return this;
  }

  public JsonWriter value(String paramString)
    throws IOException
  {
    if (paramString == null)
      return nullValue();
    a(new JsonPrimitive(paramString));
    return this;
  }

  public JsonWriter value(boolean paramBoolean)
    throws IOException
  {
    a(new JsonPrimitive(Boolean.valueOf(paramBoolean)));
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.JsonElementWriter
 * JD-Core Version:    0.6.2
 */