package com.google.myjson.internal.bind;

import com.google.myjson.JsonElement;
import com.google.myjson.JsonPrimitive;
import com.google.myjson.JsonSyntaxException;
import com.google.myjson.internal..Gson.Types;
import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.internal.ObjectConstructor;
import com.google.myjson.internal.Streams;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class MapTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;
  private final boolean b;

  public MapTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor, boolean paramBoolean)
  {
    this.a = paramConstructorConstructor;
    this.b = paramBoolean;
  }

  private TypeAdapter<?> a(MiniGson paramMiniGson, Type paramType)
  {
    if ((paramType == Boolean.TYPE) || (paramType == Boolean.class))
      return TypeAdapters.BOOLEAN_AS_STRING;
    return paramMiniGson.getAdapter(TypeToken.get(paramType));
  }

  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType = paramTypeToken.getType();
    if (!Map.class.isAssignableFrom(paramTypeToken.getRawType()))
      return null;
    Type[] arrayOfType = .Gson.Types.getMapKeyAndValueTypes(localType, .Gson.Types.getRawType(localType));
    TypeAdapter localTypeAdapter1 = a(paramMiniGson, arrayOfType[0]);
    TypeAdapter localTypeAdapter2 = paramMiniGson.getAdapter(TypeToken.get(arrayOfType[1]));
    ObjectConstructor localObjectConstructor = this.a.getConstructor(paramTypeToken);
    return new a(paramMiniGson, arrayOfType[0], localTypeAdapter1, arrayOfType[1], localTypeAdapter2, localObjectConstructor);
  }

  private final class a<K, V> extends TypeAdapter<Map<K, V>>
  {
    private final TypeAdapter<K> b;
    private final TypeAdapter<V> c;
    private final ObjectConstructor<? extends Map<K, V>> d;

    public a(Type paramTypeAdapter, TypeAdapter<K> paramType1, Type paramTypeAdapter1, TypeAdapter<V> paramObjectConstructor, ObjectConstructor<? extends Map<K, V>> arg6)
    {
      this.b = new c(paramTypeAdapter, paramTypeAdapter1, paramType1);
      TypeAdapter localTypeAdapter;
      this.c = new c(paramTypeAdapter, localTypeAdapter, paramObjectConstructor);
      Object localObject;
      this.d = localObject;
    }

    private String a(JsonElement paramJsonElement)
    {
      if (paramJsonElement.isJsonPrimitive())
      {
        JsonPrimitive localJsonPrimitive = paramJsonElement.getAsJsonPrimitive();
        if (localJsonPrimitive.isNumber())
          return String.valueOf(localJsonPrimitive.getAsNumber());
        if (localJsonPrimitive.isBoolean())
          return Boolean.toString(localJsonPrimitive.getAsBoolean());
        if (localJsonPrimitive.isString())
          return localJsonPrimitive.getAsString();
        throw new AssertionError();
      }
      if (paramJsonElement.isJsonNull())
        return "null";
      throw new AssertionError();
    }

    public Map<K, V> read(JsonReader paramJsonReader)
      throws IOException
    {
      JsonToken localJsonToken = paramJsonReader.peek();
      if (localJsonToken == JsonToken.NULL)
      {
        paramJsonReader.nextNull();
        return null;
      }
      Map localMap = (Map)this.d.construct();
      if (localJsonToken == JsonToken.BEGIN_ARRAY)
      {
        paramJsonReader.beginArray();
        while (paramJsonReader.hasNext())
        {
          paramJsonReader.beginArray();
          Object localObject2 = this.b.read(paramJsonReader);
          if (localMap.put(localObject2, this.c.read(paramJsonReader)) != null)
            throw new JsonSyntaxException("duplicate key: " + localObject2);
          paramJsonReader.endArray();
        }
        paramJsonReader.endArray();
        return localMap;
      }
      paramJsonReader.beginObject();
      while (paramJsonReader.hasNext())
      {
        String str = paramJsonReader.nextName();
        Object localObject1 = this.b.fromJsonElement(new JsonPrimitive(str));
        if (localMap.put(localObject1, this.c.read(paramJsonReader)) != null)
          throw new JsonSyntaxException("duplicate key: " + localObject1);
      }
      paramJsonReader.endObject();
      return localMap;
    }

    public void write(JsonWriter paramJsonWriter, Map<K, V> paramMap)
      throws IOException
    {
      int i = 0;
      if (paramMap == null)
      {
        paramJsonWriter.nullValue();
        return;
      }
      if (!MapTypeAdapterFactory.a(MapTypeAdapterFactory.this))
      {
        paramJsonWriter.beginObject();
        Iterator localIterator2 = paramMap.entrySet().iterator();
        while (localIterator2.hasNext())
        {
          Map.Entry localEntry2 = (Map.Entry)localIterator2.next();
          paramJsonWriter.name(String.valueOf(localEntry2.getKey()));
          this.c.write(paramJsonWriter, localEntry2.getValue());
        }
        paramJsonWriter.endObject();
        return;
      }
      ArrayList localArrayList1 = new ArrayList(paramMap.size());
      ArrayList localArrayList2 = new ArrayList(paramMap.size());
      Iterator localIterator1 = paramMap.entrySet().iterator();
      int j = 0;
      if (localIterator1.hasNext())
      {
        Map.Entry localEntry1 = (Map.Entry)localIterator1.next();
        JsonElement localJsonElement = this.b.toJsonElement(localEntry1.getKey());
        localArrayList1.add(localJsonElement);
        localArrayList2.add(localEntry1.getValue());
        if ((localJsonElement.isJsonArray()) || (localJsonElement.isJsonObject()));
        for (int k = 1; ; k = 0)
        {
          j = k | j;
          break;
        }
      }
      if (j != 0)
      {
        paramJsonWriter.beginArray();
        while (i < localArrayList1.size())
        {
          paramJsonWriter.beginArray();
          Streams.write((JsonElement)localArrayList1.get(i), paramJsonWriter);
          this.c.write(paramJsonWriter, localArrayList2.get(i));
          paramJsonWriter.endArray();
          i++;
        }
        paramJsonWriter.endArray();
        return;
      }
      paramJsonWriter.beginObject();
      while (i < localArrayList1.size())
      {
        paramJsonWriter.name(a((JsonElement)localArrayList1.get(i)));
        this.c.write(paramJsonWriter, localArrayList2.get(i));
        i++;
      }
      paramJsonWriter.endObject();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.MapTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */