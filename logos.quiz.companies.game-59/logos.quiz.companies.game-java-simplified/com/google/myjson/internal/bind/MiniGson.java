package com.google.myjson.internal.bind;

import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class MiniGson
{
  private final ThreadLocal<Map<TypeToken<?>, a<?>>> a = new b(this);
  private final List<TypeAdapter.Factory> b;

  private MiniGson(Builder paramBuilder)
  {
    ConstructorConstructor localConstructorConstructor = new ConstructorConstructor();
    ArrayList localArrayList = new ArrayList();
    if (paramBuilder.a)
    {
      localArrayList.add(TypeAdapters.BOOLEAN_FACTORY);
      localArrayList.add(TypeAdapters.INTEGER_FACTORY);
      localArrayList.add(TypeAdapters.DOUBLE_FACTORY);
      localArrayList.add(TypeAdapters.FLOAT_FACTORY);
      localArrayList.add(TypeAdapters.LONG_FACTORY);
      localArrayList.add(TypeAdapters.STRING_FACTORY);
    }
    localArrayList.addAll(Builder.a(paramBuilder));
    if (paramBuilder.a)
    {
      localArrayList.add(new CollectionTypeAdapterFactory(localConstructorConstructor));
      localArrayList.add(new StringToValueMapTypeAdapterFactory(localConstructorConstructor));
      localArrayList.add(ArrayTypeAdapter.FACTORY);
      localArrayList.add(ObjectTypeAdapter.FACTORY);
      localArrayList.add(new ReflectiveTypeAdapterFactory(localConstructorConstructor));
    }
    this.b = Collections.unmodifiableList(localArrayList);
  }

  public <T> TypeAdapter<T> getAdapter(TypeToken<T> paramTypeToken)
  {
    Map localMap = (Map)this.a.get();
    a locala1 = (a)localMap.get(paramTypeToken);
    if (locala1 != null)
      return locala1;
    a locala2 = new a();
    localMap.put(paramTypeToken, locala2);
    try
    {
      Iterator localIterator = this.b.iterator();
      while (localIterator.hasNext())
      {
        TypeAdapter localTypeAdapter = ((TypeAdapter.Factory)localIterator.next()).create(this, paramTypeToken);
        if (localTypeAdapter != null)
        {
          locala2.setDelegate(localTypeAdapter);
          return localTypeAdapter;
        }
      }
      throw new IllegalArgumentException("This MiniGSON cannot handle " + paramTypeToken);
    }
    finally
    {
      localMap.remove(paramTypeToken);
    }
  }

  public <T> TypeAdapter<T> getAdapter(Class<T> paramClass)
  {
    return getAdapter(TypeToken.get(paramClass));
  }

  public List<TypeAdapter.Factory> getFactories()
  {
    return this.b;
  }

  public <T> TypeAdapter<T> getNextAdapter(TypeAdapter.Factory paramFactory, TypeToken<T> paramTypeToken)
  {
    Iterator localIterator = this.b.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      TypeAdapter.Factory localFactory = (TypeAdapter.Factory)localIterator.next();
      if (i == 0)
      {
        if (localFactory == paramFactory)
          i = 1;
      }
      else
      {
        TypeAdapter localTypeAdapter = localFactory.create(this, paramTypeToken);
        if (localTypeAdapter != null)
          return localTypeAdapter;
      }
    }
    throw new IllegalArgumentException("This MiniGSON cannot serialize " + paramTypeToken);
  }

  public static final class Builder
  {
    boolean a = true;
    private final List<TypeAdapter.Factory> b = new ArrayList();

    public MiniGson build()
    {
      return new MiniGson(this, null);
    }

    public Builder factory(TypeAdapter.Factory paramFactory)
    {
      this.b.add(paramFactory);
      return this;
    }

    public <T> Builder typeAdapter(TypeToken<T> paramTypeToken, TypeAdapter<T> paramTypeAdapter)
    {
      this.b.add(TypeAdapters.newFactory(paramTypeToken, paramTypeAdapter));
      return this;
    }

    public <T> Builder typeAdapter(Class<T> paramClass, TypeAdapter<T> paramTypeAdapter)
    {
      this.b.add(TypeAdapters.newFactory(paramClass, paramTypeAdapter));
      return this;
    }

    public <T> Builder typeHierarchyAdapter(Class<T> paramClass, TypeAdapter<T> paramTypeAdapter)
    {
      this.b.add(TypeAdapters.newTypeHierarchyFactory(paramClass, paramTypeAdapter));
      return this;
    }

    public Builder withoutDefaultFactories()
    {
      this.a = false;
      return this;
    }
  }

  static class a<T> extends TypeAdapter<T>
  {
    private TypeAdapter<T> a;

    public T read(JsonReader paramJsonReader)
      throws IOException
    {
      if (this.a == null)
        throw new IllegalStateException();
      return this.a.read(paramJsonReader);
    }

    public void setDelegate(TypeAdapter<T> paramTypeAdapter)
    {
      if (this.a != null)
        throw new AssertionError();
      this.a = paramTypeAdapter;
    }

    public void write(JsonWriter paramJsonWriter, T paramT)
      throws IOException
    {
      if (this.a == null)
        throw new IllegalStateException();
      this.a.write(paramJsonWriter, paramT);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.MiniGson
 * JD-Core Version:    0.6.2
 */