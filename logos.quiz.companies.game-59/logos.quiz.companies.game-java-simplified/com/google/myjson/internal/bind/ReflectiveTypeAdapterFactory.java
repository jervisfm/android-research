package com.google.myjson.internal.bind;

import com.google.myjson.JsonSyntaxException;
import com.google.myjson.internal..Gson.Types;
import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.internal.ObjectConstructor;
import com.google.myjson.internal.Primitives;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReflectiveTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public ReflectiveTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  private a a(final MiniGson paramMiniGson, final Field paramField, String paramString, final TypeToken<?> paramTypeToken, boolean paramBoolean1, boolean paramBoolean2)
  {
    return new a(paramString, paramBoolean1, paramBoolean2)
    {
      final TypeAdapter<?> a = paramMiniGson.getAdapter(paramTypeToken);

      void a(JsonReader paramAnonymousJsonReader, Object paramAnonymousObject)
        throws IOException, IllegalAccessException
      {
        Object localObject = this.a.read(paramAnonymousJsonReader);
        if ((localObject != null) || (!this.e))
          paramField.set(paramAnonymousObject, localObject);
      }

      void a(JsonWriter paramAnonymousJsonWriter, Object paramAnonymousObject)
        throws IOException, IllegalAccessException
      {
        Object localObject = paramField.get(paramAnonymousObject);
        new c(paramMiniGson, this.a, paramTypeToken.getType()).write(paramAnonymousJsonWriter, localObject);
      }
    };
  }

  private Map<String, a> a(MiniGson paramMiniGson, TypeToken<?> paramTypeToken, Class<?> paramClass)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    if (paramClass.isInterface())
      return localLinkedHashMap;
    Type localType1 = paramTypeToken.getType();
    while (paramClass != Object.class)
    {
      Field[] arrayOfField = paramClass.getDeclaredFields();
      AccessibleObject.setAccessible(arrayOfField, true);
      int i = arrayOfField.length;
      int j = 0;
      if (j < i)
      {
        Field localField = arrayOfField[j];
        boolean bool1 = serializeField(paramClass, localField, localType1);
        boolean bool2 = deserializeField(paramClass, localField, localType1);
        if ((!bool1) && (!bool2));
        a locala2;
        do
        {
          j++;
          break;
          Type localType2 = .Gson.Types.resolve(paramTypeToken.getType(), paramClass, localField.getGenericType());
          a locala1 = a(paramMiniGson, localField, getFieldName(paramClass, localField, localType1), TypeToken.get(localType2), bool1, bool2);
          locala2 = (a)localLinkedHashMap.put(locala1.g, locala1);
        }
        while (locala2 == null);
        throw new IllegalArgumentException(localType1 + " declares multiple JSON fields named " + locala2.g);
      }
      paramTypeToken = TypeToken.get(.Gson.Types.resolve(paramTypeToken.getType(), paramClass, paramClass.getGenericSuperclass()));
      paramClass = paramTypeToken.getRawType();
    }
    return localLinkedHashMap;
  }

  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.getRawType();
    if (!Object.class.isAssignableFrom(localClass))
      return null;
    return new Adapter(this.a.getConstructor(paramTypeToken), a(paramMiniGson, paramTypeToken, localClass), null);
  }

  protected boolean deserializeField(Class<?> paramClass, Field paramField, Type paramType)
  {
    return !paramField.isSynthetic();
  }

  protected String getFieldName(Class<?> paramClass, Field paramField, Type paramType)
  {
    return paramField.getName();
  }

  protected boolean serializeField(Class<?> paramClass, Field paramField, Type paramType)
  {
    return !paramField.isSynthetic();
  }

  public final class Adapter<T> extends TypeAdapter<T>
  {
    private final ObjectConstructor<T> b;
    private final Map<String, ReflectiveTypeAdapterFactory.a> c;

    private Adapter(Map<String, ReflectiveTypeAdapterFactory.a> arg2)
    {
      Object localObject1;
      this.b = localObject1;
      Object localObject2;
      this.c = localObject2;
    }

    public T read(JsonReader paramJsonReader)
      throws IOException
    {
      if (paramJsonReader.peek() == JsonToken.NULL)
      {
        paramJsonReader.nextNull();
        return null;
      }
      Object localObject = this.b.construct();
      try
      {
        paramJsonReader.beginObject();
        while (true)
        {
          if (!paramJsonReader.hasNext())
            break label111;
          String str = paramJsonReader.nextName();
          locala = (ReflectiveTypeAdapterFactory.a)this.c.get(str);
          if ((locala != null) && (locala.i))
            break;
          paramJsonReader.skipValue();
        }
      }
      catch (IllegalStateException localIllegalStateException)
      {
        while (true)
        {
          ReflectiveTypeAdapterFactory.a locala;
          throw new JsonSyntaxException(localIllegalStateException);
          locala.a(paramJsonReader, localObject);
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new AssertionError(localIllegalAccessException);
      }
      label111: paramJsonReader.endObject();
      return localObject;
    }

    public void write(JsonWriter paramJsonWriter, T paramT)
      throws IOException
    {
      if (paramT == null)
      {
        paramJsonWriter.nullValue();
        return;
      }
      paramJsonWriter.beginObject();
      try
      {
        Iterator localIterator = this.c.values().iterator();
        while (localIterator.hasNext())
        {
          ReflectiveTypeAdapterFactory.a locala = (ReflectiveTypeAdapterFactory.a)localIterator.next();
          if (locala.h)
          {
            paramJsonWriter.name(locala.g);
            locala.a(paramJsonWriter, paramT);
          }
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new AssertionError();
      }
      paramJsonWriter.endObject();
    }
  }

  static abstract class a
  {
    final String g;
    final boolean h;
    final boolean i;

    protected a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.g = paramString;
      this.h = paramBoolean1;
      this.i = paramBoolean2;
    }

    abstract void a(JsonReader paramJsonReader, Object paramObject)
      throws IOException, IllegalAccessException;

    abstract void a(JsonWriter paramJsonWriter, Object paramObject)
      throws IOException, IllegalAccessException;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.ReflectiveTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */