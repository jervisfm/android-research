package com.google.myjson.internal.bind;

import com.google.myjson.internal..Gson.Types;
import com.google.myjson.internal.ConstructorConstructor;
import com.google.myjson.internal.ObjectConstructor;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class StringToValueMapTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public StringToValueMapTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType = paramTypeToken.getType();
    if (!(localType instanceof ParameterizedType));
    Type[] arrayOfType;
    do
    {
      Class localClass;
      do
      {
        return null;
        localClass = paramTypeToken.getRawType();
      }
      while (!Map.class.isAssignableFrom(localClass));
      arrayOfType = .Gson.Types.getMapKeyAndValueTypes(localType, localClass);
    }
    while (arrayOfType[0] != String.class);
    return new a(paramMiniGson.getAdapter(TypeToken.get(arrayOfType[1])), this.a.getConstructor(paramTypeToken));
  }

  private final class a<V> extends TypeAdapter<Map<String, V>>
  {
    private final TypeAdapter<V> b;
    private final ObjectConstructor<? extends Map<String, V>> c;

    public a(ObjectConstructor<? extends Map<String, V>> arg2)
    {
      Object localObject1;
      this.b = localObject1;
      Object localObject2;
      this.c = localObject2;
    }

    public Map<String, V> read(JsonReader paramJsonReader)
      throws IOException
    {
      if (paramJsonReader.peek() == JsonToken.NULL)
      {
        paramJsonReader.nextNull();
        return null;
      }
      Map localMap = (Map)this.c.construct();
      paramJsonReader.beginObject();
      while (paramJsonReader.hasNext())
        localMap.put(paramJsonReader.nextName(), this.b.read(paramJsonReader));
      paramJsonReader.endObject();
      return localMap;
    }

    public void write(JsonWriter paramJsonWriter, Map<String, V> paramMap)
      throws IOException
    {
      if (paramMap == null)
      {
        paramJsonWriter.nullValue();
        return;
      }
      paramJsonWriter.beginObject();
      Iterator localIterator = paramMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        paramJsonWriter.name((String)localEntry.getKey());
        this.b.write(paramJsonWriter, localEntry.getValue());
      }
      paramJsonWriter.endObject();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.StringToValueMapTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */