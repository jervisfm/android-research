package com.google.myjson.internal.bind;

import com.google.myjson.reflect.TypeToken;
import java.sql.Time;

class a
  implements TypeAdapter.Factory
{
  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.getRawType() == Time.class)
      return new TimeTypeAdapter();
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.a
 * JD-Core Version:    0.6.2
 */