package com.google.myjson.internal.bind;

import com.google.myjson.JsonSyntaxException;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;

class ad extends TypeAdapter<Number>
{
  ad()
    throws IOException
  {
  }

  public Number read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    try
    {
      Long localLong = Long.valueOf(paramJsonReader.nextLong());
      return localLong;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }

  public void write(JsonWriter paramJsonWriter, Number paramNumber)
    throws IOException
  {
    paramJsonWriter.value(paramNumber);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.ad
 * JD-Core Version:    0.6.2
 */