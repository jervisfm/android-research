package com.google.myjson.internal.bind;

import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;

final class c<T> extends TypeAdapter<T>
{
  private final MiniGson a;
  private final TypeAdapter<T> b;
  private final Type c;

  c(MiniGson paramMiniGson, TypeAdapter<T> paramTypeAdapter, Type paramType)
  {
    this.a = paramMiniGson;
    this.b = paramTypeAdapter;
    this.c = paramType;
  }

  public T read(JsonReader paramJsonReader)
    throws IOException
  {
    return this.b.read(paramJsonReader);
  }

  public void write(JsonWriter paramJsonWriter, T paramT)
    throws IOException
  {
    TypeAdapter localTypeAdapter = this.b;
    Type localType = j.getRuntimeTypeIfMoreSpecific(this.c, paramT);
    if (localType != this.c)
    {
      localTypeAdapter = this.a.getAdapter(TypeToken.get(localType));
      if ((localTypeAdapter instanceof ReflectiveTypeAdapterFactory.Adapter))
        break label51;
    }
    while (true)
    {
      localTypeAdapter.write(paramJsonWriter, paramT);
      return;
      label51: if (!(this.b instanceof ReflectiveTypeAdapterFactory.Adapter))
        localTypeAdapter = this.b;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.c
 * JD-Core Version:    0.6.2
 */