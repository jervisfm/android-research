package com.google.myjson.internal.bind;

import com.google.myjson.reflect.TypeToken;
import java.util.Date;

class d
  implements TypeAdapter.Factory
{
  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.getRawType() == Date.class)
      return new DateTypeAdapter();
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.d
 * JD-Core Version:    0.6.2
 */