package com.google.myjson.internal.bind;

import com.google.myjson.internal..Gson.Types;
import com.google.myjson.reflect.TypeToken;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

class e
  implements TypeAdapter.Factory
{
  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType1 = paramTypeToken.getType();
    if ((!(localType1 instanceof GenericArrayType)) && ((!(localType1 instanceof Class)) || (!((Class)localType1).isArray())))
      return null;
    Type localType2 = .Gson.Types.getArrayComponentType(localType1);
    return new ArrayTypeAdapter(paramMiniGson, paramMiniGson.getAdapter(TypeToken.get(localType2)), .Gson.Types.getRawType(localType2));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.e
 * JD-Core Version:    0.6.2
 */