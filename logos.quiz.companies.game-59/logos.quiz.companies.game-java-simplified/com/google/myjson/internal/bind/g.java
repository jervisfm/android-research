package com.google.myjson.internal.bind;

import com.google.myjson.reflect.TypeToken;

class g
  implements TypeAdapter.Factory
{
  public <T> TypeAdapter<T> create(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.getRawType() == Object.class)
      return new ObjectTypeAdapter(paramMiniGson, null);
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.g
 * JD-Core Version:    0.6.2
 */