package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

class k extends TypeAdapter<Calendar>
{
  k()
    throws IOException
  {
  }

  public Calendar read(JsonReader paramJsonReader)
    throws IOException
  {
    int i = 0;
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    paramJsonReader.beginObject();
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = 0;
    while (paramJsonReader.peek() != JsonToken.END_OBJECT)
    {
      String str = paramJsonReader.nextName();
      int i2 = paramJsonReader.nextInt();
      if ("year".equals(str))
        i1 = i2;
      else if ("month".equals(str))
        n = i2;
      else if ("dayOfMonth".equals(str))
        m = i2;
      else if ("hourOfDay".equals(str))
        k = i2;
      else if ("minute".equals(str))
        j = i2;
      else if ("second".equals(str))
        i = i2;
    }
    paramJsonReader.endObject();
    return new GregorianCalendar(i1, n, m, k, j, i);
  }

  public void write(JsonWriter paramJsonWriter, Calendar paramCalendar)
    throws IOException
  {
    if (paramCalendar == null)
    {
      paramJsonWriter.nullValue();
      return;
    }
    paramJsonWriter.beginObject();
    paramJsonWriter.name("year");
    paramJsonWriter.value(paramCalendar.get(1));
    paramJsonWriter.name("month");
    paramJsonWriter.value(paramCalendar.get(2));
    paramJsonWriter.name("dayOfMonth");
    paramJsonWriter.value(paramCalendar.get(5));
    paramJsonWriter.name("hourOfDay");
    paramJsonWriter.value(paramCalendar.get(11));
    paramJsonWriter.name("minute");
    paramJsonWriter.value(paramCalendar.get(12));
    paramJsonWriter.name("second");
    paramJsonWriter.value(paramCalendar.get(13));
    paramJsonWriter.endObject();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.k
 * JD-Core Version:    0.6.2
 */