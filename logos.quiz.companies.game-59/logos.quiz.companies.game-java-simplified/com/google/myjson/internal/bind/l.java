package com.google.myjson.internal.bind;

import com.google.myjson.JsonArray;
import com.google.myjson.JsonElement;
import com.google.myjson.JsonNull;
import com.google.myjson.JsonObject;
import com.google.myjson.JsonPrimitive;
import com.google.myjson.internal.LazilyParsedNumber;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class l extends TypeAdapter<JsonElement>
{
  l()
    throws IOException
  {
  }

  public JsonElement read(JsonReader paramJsonReader)
    throws IOException
  {
    switch (TypeAdapters.7.a[paramJsonReader.peek().ordinal()])
    {
    default:
      throw new IllegalArgumentException();
    case 3:
      return new JsonPrimitive(paramJsonReader.nextString());
    case 1:
      return new JsonPrimitive(new LazilyParsedNumber(paramJsonReader.nextString()));
    case 2:
      return new JsonPrimitive(Boolean.valueOf(paramJsonReader.nextBoolean()));
    case 4:
      paramJsonReader.nextNull();
      return JsonNull.INSTANCE;
    case 5:
      JsonArray localJsonArray = new JsonArray();
      paramJsonReader.beginArray();
      while (paramJsonReader.hasNext())
        localJsonArray.add(read(paramJsonReader));
      paramJsonReader.endArray();
      return localJsonArray;
    case 6:
    }
    JsonObject localJsonObject = new JsonObject();
    paramJsonReader.beginObject();
    while (paramJsonReader.hasNext())
      localJsonObject.add(paramJsonReader.nextName(), read(paramJsonReader));
    paramJsonReader.endObject();
    return localJsonObject;
  }

  public void write(JsonWriter paramJsonWriter, JsonElement paramJsonElement)
    throws IOException
  {
    if ((paramJsonElement == null) || (paramJsonElement.isJsonNull()))
    {
      paramJsonWriter.nullValue();
      return;
    }
    if (paramJsonElement.isJsonPrimitive())
    {
      JsonPrimitive localJsonPrimitive = paramJsonElement.getAsJsonPrimitive();
      if (localJsonPrimitive.isNumber())
      {
        paramJsonWriter.value(localJsonPrimitive.getAsNumber());
        return;
      }
      if (localJsonPrimitive.isBoolean())
      {
        paramJsonWriter.value(localJsonPrimitive.getAsBoolean());
        return;
      }
      paramJsonWriter.value(localJsonPrimitive.getAsString());
      return;
    }
    if (paramJsonElement.isJsonArray())
    {
      paramJsonWriter.beginArray();
      Iterator localIterator2 = paramJsonElement.getAsJsonArray().iterator();
      while (localIterator2.hasNext())
        write(paramJsonWriter, (JsonElement)localIterator2.next());
      paramJsonWriter.endArray();
      return;
    }
    if (paramJsonElement.isJsonObject())
    {
      paramJsonWriter.beginObject();
      Iterator localIterator1 = paramJsonElement.getAsJsonObject().entrySet().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator1.next();
        paramJsonWriter.name((String)localEntry.getKey());
        write(paramJsonWriter, (JsonElement)localEntry.getValue());
      }
      paramJsonWriter.endObject();
      return;
    }
    throw new IllegalArgumentException("Couldn't write " + paramJsonElement.getClass());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.l
 * JD-Core Version:    0.6.2
 */