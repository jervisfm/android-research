package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.StringTokenizer;

class m extends TypeAdapter<Locale>
{
  m()
    throws IOException
  {
  }

  public Locale read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    StringTokenizer localStringTokenizer = new StringTokenizer(paramJsonReader.nextString(), "_");
    if (localStringTokenizer.hasMoreElements());
    for (String str1 = localStringTokenizer.nextToken(); ; str1 = null)
    {
      if (localStringTokenizer.hasMoreElements());
      for (String str2 = localStringTokenizer.nextToken(); ; str2 = null)
      {
        if (localStringTokenizer.hasMoreElements());
        for (String str3 = localStringTokenizer.nextToken(); ; str3 = null)
        {
          if ((str2 == null) && (str3 == null))
            return new Locale(str1);
          if (str3 == null)
            return new Locale(str1, str2);
          return new Locale(str1, str2, str3);
        }
      }
    }
  }

  public void write(JsonWriter paramJsonWriter, Locale paramLocale)
    throws IOException
  {
    if (paramLocale == null);
    for (String str = null; ; str = paramLocale.toString())
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.m
 * JD-Core Version:    0.6.2
 */