package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;

class o extends TypeAdapter<Character>
{
  o()
    throws IOException
  {
  }

  public Character read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    return Character.valueOf(paramJsonReader.nextString().charAt(0));
  }

  public void write(JsonWriter paramJsonWriter, Character paramCharacter)
    throws IOException
  {
    if (paramCharacter == null);
    for (String str = null; ; str = String.valueOf(paramCharacter))
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.o
 * JD-Core Version:    0.6.2
 */