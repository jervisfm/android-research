package com.google.myjson.internal.bind;

import com.google.myjson.JsonSyntaxException;
import com.google.myjson.internal.LazilyParsedNumber;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;

class p extends TypeAdapter<Number>
{
  p()
    throws IOException
  {
  }

  public Number read(JsonReader paramJsonReader)
    throws IOException
  {
    JsonToken localJsonToken = paramJsonReader.peek();
    switch (TypeAdapters.7.a[localJsonToken.ordinal()])
    {
    case 2:
    case 3:
    default:
      throw new JsonSyntaxException("Expecting number, got: " + localJsonToken);
    case 4:
      paramJsonReader.nextNull();
      return null;
    case 1:
    }
    return new LazilyParsedNumber(paramJsonReader.nextString());
  }

  public void write(JsonWriter paramJsonWriter, Number paramNumber)
    throws IOException
  {
    paramJsonWriter.value(paramNumber);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.p
 * JD-Core Version:    0.6.2
 */