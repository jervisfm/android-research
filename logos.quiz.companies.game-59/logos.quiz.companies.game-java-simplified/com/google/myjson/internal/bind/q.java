package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.net.URL;

class q extends TypeAdapter<URL>
{
  q()
    throws IOException
  {
  }

  public URL read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
      paramJsonReader.nextNull();
    String str;
    do
    {
      return null;
      str = paramJsonReader.nextString();
    }
    while ("null".equals(str));
    return new URL(str);
  }

  public void write(JsonWriter paramJsonWriter, URL paramURL)
    throws IOException
  {
    if (paramURL == null);
    for (String str = null; ; str = paramURL.toExternalForm())
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.q
 * JD-Core Version:    0.6.2
 */