package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;

class s extends TypeAdapter<StringBuilder>
{
  s()
    throws IOException
  {
  }

  public StringBuilder read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    return new StringBuilder(paramJsonReader.nextString());
  }

  public void write(JsonWriter paramJsonWriter, StringBuilder paramStringBuilder)
    throws IOException
  {
    if (paramStringBuilder == null);
    for (String str = null; ; str = paramStringBuilder.toString())
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.s
 * JD-Core Version:    0.6.2
 */