package com.google.myjson.internal.bind;

import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.UUID;

class u extends TypeAdapter<UUID>
{
  u()
    throws IOException
  {
  }

  public UUID read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    return UUID.fromString(paramJsonReader.nextString());
  }

  public void write(JsonWriter paramJsonWriter, UUID paramUUID)
    throws IOException
  {
    if (paramUUID == null);
    for (String str = null; ; str = paramUUID.toString())
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.u
 * JD-Core Version:    0.6.2
 */