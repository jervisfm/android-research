package com.google.myjson.internal.bind;

import com.google.myjson.JsonIOException;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

class w extends TypeAdapter<URI>
{
  w()
    throws IOException
  {
  }

  public URI read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
      paramJsonReader.nextNull();
    while (true)
    {
      return null;
      try
      {
        String str = paramJsonReader.nextString();
        if ("null".equals(str))
          continue;
        URI localURI = new URI(str);
        return localURI;
      }
      catch (URISyntaxException localURISyntaxException)
      {
        throw new JsonIOException(localURISyntaxException);
      }
    }
  }

  public void write(JsonWriter paramJsonWriter, URI paramURI)
    throws IOException
  {
    if (paramURI == null);
    for (String str = null; ; str = paramURI.toASCIIString())
    {
      paramJsonWriter.value(str);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.w
 * JD-Core Version:    0.6.2
 */