package com.google.myjson.internal.bind;

import com.google.myjson.JsonSyntaxException;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonToken;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.util.BitSet;

class y extends TypeAdapter<BitSet>
{
  y()
    throws IOException
  {
  }

  public BitSet read(JsonReader paramJsonReader)
    throws IOException
  {
    if (paramJsonReader.peek() == JsonToken.NULL)
    {
      paramJsonReader.nextNull();
      return null;
    }
    BitSet localBitSet = new BitSet();
    paramJsonReader.beginArray();
    JsonToken localJsonToken = paramJsonReader.peek();
    int i = 0;
    if (localJsonToken != JsonToken.END_ARRAY)
    {
      boolean bool;
      switch (TypeAdapters.7.a[localJsonToken.ordinal()])
      {
      default:
        throw new JsonSyntaxException("Invalid bitset value type: " + localJsonToken);
      case 1:
        if (paramJsonReader.nextInt() != 0)
          bool = true;
        break;
      case 2:
      case 3:
      }
      while (true)
      {
        if (bool)
          localBitSet.set(i);
        i++;
        localJsonToken = paramJsonReader.peek();
        break;
        bool = false;
        continue;
        bool = paramJsonReader.nextBoolean();
        continue;
        String str = paramJsonReader.nextString();
        try
        {
          int j = Integer.parseInt(str);
          if (j != 0)
            bool = true;
          else
            bool = false;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          throw new JsonSyntaxException("Error: Expecting: bitset number value (1, 0), Found: " + str);
        }
      }
    }
    paramJsonReader.endArray();
    return localBitSet;
  }

  public void write(JsonWriter paramJsonWriter, BitSet paramBitSet)
    throws IOException
  {
    if (paramBitSet == null)
    {
      paramJsonWriter.nullValue();
      return;
    }
    paramJsonWriter.beginArray();
    int i = 0;
    if (i < paramBitSet.length())
    {
      if (paramBitSet.get(i));
      for (int j = 1; ; j = 0)
      {
        paramJsonWriter.value(j);
        i++;
        break;
      }
    }
    paramJsonWriter.endArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.internal.bind.y
 * JD-Core Version:    0.6.2
 */