package com.google.myjson;

import com.google.myjson.internal.ParameterizedTypeHandlerMap;
import com.google.myjson.internal.Streams;
import com.google.myjson.internal.bind.MiniGson;
import com.google.myjson.internal.bind.TypeAdapter;
import com.google.myjson.internal.bind.TypeAdapter.Factory;
import com.google.myjson.reflect.TypeToken;
import com.google.myjson.stream.JsonReader;
import com.google.myjson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;

final class j
  implements TypeAdapter.Factory
{
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> a;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> b;
  private final JsonDeserializationContext c;
  private final JsonSerializationContext d;

  public j(final Gson paramGson, ParameterizedTypeHandlerMap<JsonSerializer<?>> paramParameterizedTypeHandlerMap, ParameterizedTypeHandlerMap<JsonDeserializer<?>> paramParameterizedTypeHandlerMap1)
  {
    this.a = paramParameterizedTypeHandlerMap;
    this.b = paramParameterizedTypeHandlerMap1;
    this.c = new JsonDeserializationContext()
    {
      public <T> T deserialize(JsonElement paramAnonymousJsonElement, Type paramAnonymousType)
        throws JsonParseException
      {
        return paramGson.fromJson(paramAnonymousJsonElement, paramAnonymousType);
      }
    };
    this.d = new JsonSerializationContext()
    {
      public JsonElement serialize(Object paramAnonymousObject)
      {
        return paramGson.toJsonTree(paramAnonymousObject);
      }

      public JsonElement serialize(Object paramAnonymousObject, Type paramAnonymousType)
      {
        return paramGson.toJsonTree(paramAnonymousObject, paramAnonymousType);
      }
    };
  }

  public <T> TypeAdapter<T> create(final MiniGson paramMiniGson, final TypeToken<T> paramTypeToken)
  {
    final Type localType = paramTypeToken.getType();
    final JsonSerializer localJsonSerializer = (JsonSerializer)this.a.getHandlerFor(localType, false);
    final JsonDeserializer localJsonDeserializer = (JsonDeserializer)this.b.getHandlerFor(localType, false);
    if ((localJsonSerializer == null) && (localJsonDeserializer == null))
      return null;
    return new TypeAdapter()
    {
      private TypeAdapter<T> g;

      private TypeAdapter<T> a()
      {
        TypeAdapter localTypeAdapter1 = this.g;
        if (localTypeAdapter1 != null)
          return localTypeAdapter1;
        TypeAdapter localTypeAdapter2 = paramMiniGson.getNextAdapter(j.this, paramTypeToken);
        this.g = localTypeAdapter2;
        return localTypeAdapter2;
      }

      public T read(JsonReader paramAnonymousJsonReader)
        throws IOException
      {
        if (localJsonDeserializer == null)
          return a().read(paramAnonymousJsonReader);
        JsonElement localJsonElement = Streams.parse(paramAnonymousJsonReader);
        if (localJsonElement.isJsonNull())
          return null;
        return localJsonDeserializer.deserialize(localJsonElement, localType, j.a(j.this));
      }

      public void write(JsonWriter paramAnonymousJsonWriter, T paramAnonymousT)
        throws IOException
      {
        if (localJsonSerializer == null)
        {
          a().write(paramAnonymousJsonWriter, paramAnonymousT);
          return;
        }
        if (paramAnonymousT == null)
        {
          paramAnonymousJsonWriter.nullValue();
          return;
        }
        Streams.write(localJsonSerializer.serialize(paramAnonymousT, localType, j.b(j.this)), paramAnonymousJsonWriter);
      }
    };
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.j
 * JD-Core Version:    0.6.2
 */