package com.google.myjson;

import com.google.myjson.annotations.Expose;

final class m
  implements ExclusionStrategy
{
  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Expose localExpose = (Expose)paramFieldAttributes.getAnnotation(Expose.class);
    if (localExpose == null)
      return true;
    if (!localExpose.serialize());
    for (boolean bool = true; ; bool = false)
      return bool;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.m
 * JD-Core Version:    0.6.2
 */