package com.google.myjson;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

final class n
  implements ExclusionStrategy
{
  private final Collection<Integer> a = new HashSet();

  public n(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.length;
      for (int j = 0; j < i; j++)
      {
        int k = paramArrayOfInt[j];
        this.a.add(Integer.valueOf(k));
      }
    }
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (paramFieldAttributes.hasModifier(((Integer)localIterator.next()).intValue()))
        return true;
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.n
 * JD-Core Version:    0.6.2
 */