package com.google.myjson;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

final class o
{
  static final class a
    implements JsonSerializer<java.util.Date>, JsonDeserializer<java.util.Date>
  {
    private final DateFormat a;
    private final DateFormat b;
    private final DateFormat c;

    a()
    {
      this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public a(int paramInt1, int paramInt2)
    {
      this(DateFormat.getDateTimeInstance(paramInt1, paramInt2, Locale.US), DateFormat.getDateTimeInstance(paramInt1, paramInt2));
    }

    a(String paramString)
    {
      this(new SimpleDateFormat(paramString, Locale.US), new SimpleDateFormat(paramString));
    }

    a(DateFormat paramDateFormat1, DateFormat paramDateFormat2)
    {
      this.a = paramDateFormat1;
      this.b = paramDateFormat2;
      this.c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
      this.c.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private java.util.Date a(JsonElement paramJsonElement)
    {
      synchronized (this.b)
      {
        try
        {
          java.util.Date localDate3 = this.b.parse(paramJsonElement.getAsString());
          return localDate3;
        }
        catch (ParseException localParseException1)
        {
        }
      }
      try
      {
        java.util.Date localDate2 = this.a.parse(paramJsonElement.getAsString());
        return localDate2;
        localObject = finally;
        throw localObject;
      }
      catch (ParseException localParseException2)
      {
        try
        {
          java.util.Date localDate1 = this.c.parse(paramJsonElement.getAsString());
          return localDate1;
        }
        catch (ParseException localParseException3)
        {
          throw new JsonSyntaxException(paramJsonElement.getAsString(), localParseException3);
        }
      }
    }

    public java.util.Date deserialize(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
      throws JsonParseException
    {
      if (!(paramJsonElement instanceof JsonPrimitive))
        throw new JsonParseException("The date should be a string value");
      java.util.Date localDate = a(paramJsonElement);
      if (paramType == java.util.Date.class)
        return localDate;
      if (paramType == Timestamp.class)
        return new Timestamp(localDate.getTime());
      if (paramType == java.sql.Date.class)
        return new java.sql.Date(localDate.getTime());
      throw new IllegalArgumentException(getClass() + " cannot deserialize to " + paramType);
    }

    public JsonElement serialize(java.util.Date paramDate, Type paramType, JsonSerializationContext paramJsonSerializationContext)
    {
      synchronized (this.b)
      {
        JsonPrimitive localJsonPrimitive = new JsonPrimitive(this.a.format(paramDate));
        return localJsonPrimitive;
      }
    }

    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(a.class.getSimpleName());
      localStringBuilder.append('(').append(this.b.getClass().getSimpleName()).append(')');
      return localStringBuilder.toString();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.o
 * JD-Core Version:    0.6.2
 */