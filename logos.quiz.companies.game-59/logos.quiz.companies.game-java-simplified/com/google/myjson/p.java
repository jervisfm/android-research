package com.google.myjson;

final class p
  implements ExclusionStrategy
{
  private boolean a(Class<?> paramClass)
  {
    return (paramClass.isMemberClass()) && (!b(paramClass));
  }

  private boolean b(Class<?> paramClass)
  {
    return (0x8 & paramClass.getModifiers()) != 0;
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return a(paramClass);
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.getDeclaredClass());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.p
 * JD-Core Version:    0.6.2
 */