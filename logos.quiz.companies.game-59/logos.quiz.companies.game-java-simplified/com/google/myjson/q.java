package com.google.myjson;

final class q
  implements ExclusionStrategy
{
  private boolean a(Class<?> paramClass)
  {
    return (!Enum.class.isAssignableFrom(paramClass)) && ((paramClass.isAnonymousClass()) || (paramClass.isLocalClass()));
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return a(paramClass);
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.getDeclaredClass());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.q
 * JD-Core Version:    0.6.2
 */