package com.google.myjson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class s extends e
{
  private final e[] a;

  public s(e[] paramArrayOfe)
  {
    if (paramArrayOfe == null)
      throw new NullPointerException("naming policies can not be null.");
    this.a = paramArrayOfe;
  }

  protected String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    e[] arrayOfe = this.a;
    int i = arrayOfe.length;
    for (int j = 0; j < i; j++)
      paramString = arrayOfe[j].a(paramString, paramType, paramCollection);
    return paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.s
 * JD-Core Version:    0.6.2
 */