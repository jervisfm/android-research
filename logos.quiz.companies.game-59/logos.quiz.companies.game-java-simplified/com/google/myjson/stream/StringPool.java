package com.google.myjson.stream;

final class StringPool
{
  private final String[] pool = new String[512];

  public String get(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    int j = 0;
    while (i < paramInt1 + paramInt2)
    {
      j = j * 31 + paramArrayOfChar[i];
      i++;
    }
    int k = j ^ (j >>> 20 ^ j >>> 12);
    int m = (k ^ (k >>> 7 ^ k >>> 4)) & -1 + this.pool.length;
    String str1 = this.pool[m];
    int i1;
    if (str1 != null)
    {
      int n = str1.length();
      i1 = 0;
      if (n == paramInt2);
    }
    else
    {
      str1 = new String(paramArrayOfChar, paramInt1, paramInt2);
    }
    do
    {
      this.pool[m] = str1;
      do
      {
        return str1;
        i1++;
      }
      while (i1 >= paramInt2);
    }
    while (str1.charAt(i1) == paramArrayOfChar[(paramInt1 + i1)]);
    String str2 = new String(paramArrayOfChar, paramInt1, paramInt2);
    this.pool[m] = str2;
    return str2;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.stream.StringPool
 * JD-Core Version:    0.6.2
 */