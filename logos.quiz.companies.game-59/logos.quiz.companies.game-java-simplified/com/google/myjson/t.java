package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import java.util.Collection;
import java.util.Iterator;

final class t
  implements ExclusionStrategy
{
  private final Collection<ExclusionStrategy> a;

  t(Collection<ExclusionStrategy> paramCollection)
  {
    this.a = ((Collection).Gson.Preconditions.checkNotNull(paramCollection));
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).shouldSkipClass(paramClass))
        return true;
    return false;
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).shouldSkipField(paramFieldAttributes))
        return true;
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.t
 * JD-Core Version:    0.6.2
 */