package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;
import java.lang.reflect.Type;

final class u<T>
  implements JsonDeserializer<T>
{
  private final JsonDeserializer<T> a;

  u(JsonDeserializer<T> paramJsonDeserializer)
  {
    this.a = ((JsonDeserializer).Gson.Preconditions.checkNotNull(paramJsonDeserializer));
  }

  public T deserialize(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
    throws JsonParseException
  {
    try
    {
      Object localObject = this.a.deserialize(paramJsonElement, paramType, paramJsonDeserializationContext);
      return localObject;
    }
    catch (JsonParseException localJsonParseException)
    {
      throw localJsonParseException;
    }
    catch (Exception localException)
    {
      throw new JsonParseException("The JsonDeserializer " + this.a + " failed to deserialize json object " + paramJsonElement + " given the type " + paramType, localException);
    }
  }

  public String toString()
  {
    return this.a.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.u
 * JD-Core Version:    0.6.2
 */