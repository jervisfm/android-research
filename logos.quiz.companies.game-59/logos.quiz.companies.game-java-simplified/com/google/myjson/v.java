package com.google.myjson;

import com.google.myjson.annotations.Since;
import com.google.myjson.annotations.Until;
import com.google.myjson.internal..Gson.Preconditions;

final class v
  implements ExclusionStrategy
{
  private final double a;

  v(double paramDouble)
  {
    if (paramDouble >= 0.0D);
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      this.a = paramDouble;
      return;
    }
  }

  private boolean a(Since paramSince)
  {
    return (paramSince == null) || (paramSince.value() <= this.a);
  }

  private boolean a(Since paramSince, Until paramUntil)
  {
    return (a(paramSince)) && (a(paramUntil));
  }

  private boolean a(Until paramUntil)
  {
    return (paramUntil == null) || (paramUntil.value() > this.a);
  }

  public boolean shouldSkipClass(Class<?> paramClass)
  {
    return !a((Since)paramClass.getAnnotation(Since.class), (Until)paramClass.getAnnotation(Until.class));
  }

  public boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return !a((Since)paramFieldAttributes.getAnnotation(Since.class), (Until)paramFieldAttributes.getAnnotation(Until.class));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.v
 * JD-Core Version:    0.6.2
 */