package com.google.myjson;

import com.google.myjson.internal..Gson.Preconditions;

final class w
  implements l
{
  private final FieldNamingStrategy a;

  w(FieldNamingStrategy paramFieldNamingStrategy)
  {
    this.a = ((FieldNamingStrategy).Gson.Preconditions.checkNotNull(paramFieldNamingStrategy));
  }

  public String translateName(FieldAttributes paramFieldAttributes)
  {
    return this.a.translateName(paramFieldAttributes.b());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.google.myjson.w
 * JD-Core Version:    0.6.2
 */