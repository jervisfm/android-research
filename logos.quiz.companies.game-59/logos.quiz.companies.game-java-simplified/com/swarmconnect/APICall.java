package com.swarmconnect;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.myjson.Gson;
import com.google.myjson.GsonBuilder;
import com.swarmconnect.utils.AsyncHttp;
import com.swarmconnect.utils.AsyncHttp.AsyncCB;
import com.swarmconnect.utils.Base64;
import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

class APICall
{
  public static final int RESPONSE_ERROR = 0;
  public static final int RESPONSE_LOGOUT = -1;
  private static final Gson a = new Gson();
  private static boolean c = true;
  private static HashSet<Class<? extends APICall>> d = new HashSet();
  public String appId;
  private transient AsyncHttp.AsyncCB b = new ap(this);
  public transient APICallback cb;
  public transient int statusCode;
  public transient String statusMessage;
  public transient String url;

  static
  {
    d.add(as.class);
    d.add(ay.class);
    aq.a(ab.class, 300);
    aq.a(p.class, 300);
    aq.a(l.class, 3600);
    aq.a(GetDeviceAccountsAPI.class, 300);
    aq.a(GetFriendsAPI.class, 300);
    aq.a(GetInventoryAPI.class, 300);
    aq.a(bp.class, 300);
    aq.a(b.class, 300);
    aq.a(cf.class, 300);
    aq.a(a.class, 300);
    aq.a(bo.class, 300);
    aq.a(o.class, ab.class);
    aq.a(bu.class, ab.class);
    aq.a(y.class, p.class);
    aq.a(ce.class, GetDeviceAccountsAPI.class);
    aq.a(w.class, GetDeviceAccountsAPI.class);
    aq.a(br.class, GetFriendsAPI.class);
    aq.a(bt.class, GetFriendsAPI.class);
    aq.a(bd.class, GetInventoryAPI.class);
    aq.a(bd.class, bo.class);
    aq.a(aw.class, GetInventoryAPI.class);
    aq.a(cj.class, bp.class);
    aq.a(bu.class, a.class);
    aq.a(cn.class, a.class);
    aq.a(o.class, a.class);
    aq.a(ce.class, bo.class);
    aq.a(ce.class, GetInventoryAPI.class);
    aq.a(w.class, GetInventoryAPI.class);
    aq.a(w.class, GetInventoryAPI.class);
  }

  public APICall()
  {
  }

  public APICall(APICallback paramAPICallback)
  {
    this.cb = paramAPICallback;
  }

  private APICall b(String paramString)
  {
    try
    {
      APICall localAPICall = (APICall)new GsonBuilder().excludeFieldsWithModifiers(new int[] { 8 }).create().fromJson(paramString, getClass());
      if (localAPICall.statusCode == -1)
      {
        Swarm.logOut();
        localAPICall = null;
      }
      return localAPICall;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  private static void b(String paramString1, String paramString2)
  {
    if ((Swarm.j != null) && (Swarm.user != null))
    {
      int i = Swarm.user.userId;
      String str = Swarm.j.getString("pending_apis_" + i, "");
      BufferedRequestList localBufferedRequestList = null;
      if (str != null)
      {
        int j = str.length();
        localBufferedRequestList = null;
        if (j > 0)
          localBufferedRequestList = (BufferedRequestList)new Gson().fromJson(str, BufferedRequestList.class);
      }
      if ((localBufferedRequestList == null) || (localBufferedRequestList.requests == null))
        localBufferedRequestList = new BufferedRequestList();
      localBufferedRequestList.requests.add(new APICall.BufferedRequestList.BufferedRequest(paramString1, paramString2));
      SharedPreferences.Editor localEditor = Swarm.j.edit();
      localEditor.putString("pending_apis_" + i, new Gson().toJson(localBufferedRequestList));
      localEditor.commit();
    }
  }

  private static String c(String paramString)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      DeflaterOutputStream localDeflaterOutputStream = new DeflaterOutputStream(localByteArrayOutputStream, new Deflater(1, true));
      localDeflaterOutputStream.write(paramString.getBytes());
      localDeflaterOutputStream.close();
      return URLEncoder.encode(Base64.encode(localByteArrayOutputStream.toByteArray()));
    }
    catch (Exception localException)
    {
      throw new RuntimeException(localException);
    }
  }

  protected static boolean c()
  {
    if ((Swarm.j != null) && (Swarm.user != null))
    {
      c = true;
      int i = Swarm.user.userId;
      String str1 = Swarm.j.getString("pending_apis_" + i, "");
      BufferedRequestList localBufferedRequestList = (BufferedRequestList)new Gson().fromJson(str1, BufferedRequestList.class);
      if ((localBufferedRequestList != null) && (localBufferedRequestList.requests != null) && (localBufferedRequestList.requests.size() > 0));
      while (true)
      {
        if ((localBufferedRequestList.requests.size() <= 0) || (!c))
        {
          SharedPreferences.Editor localEditor = Swarm.j.edit();
          localEditor.putString("pending_apis_" + i, new Gson().toJson(localBufferedRequestList));
          localEditor.commit();
          return c;
        }
        APICall.BufferedRequestList.BufferedRequest localBufferedRequest = (APICall.BufferedRequestList.BufferedRequest)localBufferedRequestList.requests.getFirst();
        APIRequest localAPIRequest = new APIRequest();
        localAPIRequest.a = Swarm.i;
        localAPIRequest.u = i;
        localAPIRequest.m = localBufferedRequest.module;
        localAPIRequest.d = localBufferedRequest.payload;
        String str2 = c(a.toJson(localAPIRequest));
        d(localBufferedRequest.module + ":: " + "v=" + 5 + "&req=" + str2);
        AsyncHttp.getBlocking("http://api.swarmconnect.com/api.php?v=5&req=" + str2 + "&t=" + System.currentTimeMillis(), new AsyncHttp.AsyncCB()
        {
          public void gotURL(String paramAnonymousString)
          {
            APICall.this.requests.removeFirst();
          }

          public void requestFailed(Exception paramAnonymousException)
          {
            APICall.a(false);
          }
        });
      }
    }
    return false;
  }

  private static void d(String paramString)
  {
    Swarm.c(paramString);
  }

  private String e()
  {
    return new Gson().toJson(this);
  }

  protected int a()
  {
    if (Swarm.user == null)
      return 0;
    return Swarm.user.userId;
  }

  protected String b()
  {
    return Swarm.i;
  }

  public void run()
  {
    try
    {
      this.appId = Swarm.d;
      APIRequest localAPIRequest = new APIRequest();
      localAPIRequest.a = b();
      localAPIRequest.u = a();
      localAPIRequest.m = bx.a(getClass());
      localAPIRequest.av = Swarm.g;
      localAPIRequest.d = e();
      String str1 = c(a.toJson(localAPIRequest));
      this.url = ("http://api.swarmconnect.com/api.php?v=5&req=" + str1);
      d(bx.a(getClass()) + " :: " + a() + ", " + b() + " :: " + this.url);
      String str2 = aq.a(this);
      if ((str2 != null) && (this.cb != null))
      {
        this.cb.gotAPI(b(str2));
        return;
      }
      aq.a(getClass());
      AsyncHttp.getURL(this.url + "&t=" + System.currentTimeMillis(), this.b);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      Swarm.a();
      aq.a();
    }
  }

  public static abstract class APICallback
  {
    public abstract void gotAPI(APICall paramAPICall);

    public void requestFailed()
    {
    }
  }

  static class APIRequest
  {
    public String a;
    public int av;
    public String d;
    public String m;
    public int u;
  }

  protected static class BufferedRequestList
  {
    public LinkedList<BufferedRequest> requests = new LinkedList();

    protected static class BufferedRequest
    {
      public String module;
      public String payload;

      public BufferedRequest()
      {
      }

      public BufferedRequest(String paramString1, String paramString2)
      {
        this.module = paramString1;
        this.payload = paramString2;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.APICall
 * JD-Core Version:    0.6.2
 */