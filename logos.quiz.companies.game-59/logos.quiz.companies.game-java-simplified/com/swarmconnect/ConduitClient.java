package com.swarmconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import com.swarmconnect.packets.Packet;
import com.swarmconnect.packets.PacketAuth;
import com.swarmconnect.packets.PacketNotification;
import com.swarmconnect.packets.PacketPong;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class ConduitClient
  implements ad
{
  private static ConduitClient k;
  private Context a;
  private String b = null;
  private int c = 5000;
  private Timer d;
  private String e = null;
  private long f = 0L;
  private boolean g = false;
  private boolean h = false;
  private ac i;
  private BroadcastReceiver j;

  protected static ConduitClient a()
  {
    if (k == null)
      k = new ConduitClient();
    return k;
  }

  private static void b(String paramString)
  {
    NotificationService.a(paramString);
  }

  private void d()
  {
    try
    {
      boolean bool = this.h;
      if (!bool);
      while (true)
      {
        return;
        if (this.d != null)
        {
          this.d.cancel();
          this.d.purge();
          this.d = null;
        }
        this.d = new Timer();
        this.d.schedule(new TimerTask()
        {
          public void run()
          {
            ConduitClient.this.connect();
          }
        }
        , this.c);
        this.c = Math.min(21600000, 2 * this.c);
      }
    }
    finally
    {
    }
  }

  protected void a(Context paramContext)
  {
    try
    {
      if ((!this.h) && (paramContext != null))
      {
        this.h = true;
        this.a = paramContext;
        this.b = SwarmIO.a(paramContext);
        if (this.j == null)
        {
          this.j = new a(null);
          paramContext.registerReceiver(this.j, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        connect();
      }
      return;
    }
    finally
    {
    }
  }

  protected void b()
  {
    try
    {
      if (this.h)
      {
        this.h = false;
        if (this.j != null)
        {
          if (this.a != null)
            this.a.unregisterReceiver(this.j);
          this.j = null;
        }
        if (this.i != null)
        {
          this.i.a(new Throwable("ConduitClient.onDestroy"));
          this.i = null;
        }
      }
      return;
    }
    finally
    {
    }
  }

  protected void c()
  {
    this.c = 5000;
  }

  public void connect()
  {
    if ((this.i == null) && (this.h))
    {
      b("ConduitClient.connect(), conn: " + this.i);
      if (System.currentTimeMillis() - this.f > 1800000L)
        this.e = null;
      if (this.e == null)
        getHost();
    }
    else
    {
      return;
    }
    this.i = new ac(this.e, 9210, this);
    new Thread()
    {
      public void run()
      {
        try
        {
          ConduitClient.a(ConduitClient.this).a();
          return;
        }
        catch (Exception localException)
        {
          ConduitClient.a(ConduitClient.this, null);
          ConduitClient.b(ConduitClient.this);
        }
      }
    }
    .start();
  }

  public void getHost()
  {
    if ((this.g) || (!this.h))
      return;
    b("ConduitClient.getHost()...");
    this.g = true;
    AsyncHttp.a("http://nadmin.swarm.io", new AsyncHttp.AsyncCB()
    {
      public void gotURL(String paramAnonymousString)
      {
        ConduitClient.a("getHost(): " + paramAnonymousString);
        ConduitClient.a(ConduitClient.this, System.currentTimeMillis());
        ConduitClient.a(ConduitClient.this, paramAnonymousString);
        ConduitClient.a(ConduitClient.this, false);
        ConduitClient.this.connect();
      }

      public void requestFailed(Exception paramAnonymousException)
      {
        ConduitClient.a(ConduitClient.this, false);
        ConduitClient.b(ConduitClient.this);
      }
    });
  }

  public void onConnected(ac paramac)
  {
    b("ConduitClient.onConnected");
    NotificationService.a();
    c();
  }

  public void onConnectionLost(ac paramac, Throwable paramThrowable)
  {
    try
    {
      b("NotifcationService.onConnectionLost: " + paramThrowable + ", conn: " + this.i);
      if (this.i == paramac)
        this.i = null;
      d();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void onPacketReceived(int paramInt, byte[] paramArrayOfByte)
    throws IOException
  {
    if (this.i == null);
    while (true)
    {
      return;
      b("onPacketReceived: " + paramInt);
      if (paramInt == 2);
      try
      {
        this.i.a(new PacketAuth(this.b).getPayload());
        return;
        if (paramInt == 4)
        {
          this.i.a(new PacketPong().getPayload());
          return;
        }
        if ((paramInt == 6) && (((PacketNotification)Packet.build(paramArrayOfByte, paramInt)).clientId.equals(this.b)))
        {
          NotificationService.a();
          return;
        }
      }
      catch (Exception localException)
      {
      }
    }
  }

  private class a extends BroadcastReceiver
  {
    private a()
    {
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if ((Integer.parseInt(Build.VERSION.SDK) >= 5) && (new a(null).isInitialSticky()))
        return;
      while (true)
      {
        try
        {
          boolean bool = paramIntent.getBooleanExtra("noConnectivity", false);
          i = 0;
          if (bool)
          {
            if (i == 0)
              break;
            ConduitClient.this.c();
            if (ConduitClient.a(ConduitClient.this) == null)
              break;
            ConduitClient.a(ConduitClient.this).a(new Throwable("connectivity changed"));
            return;
          }
        }
        catch (Exception localException)
        {
          return;
        }
        int i = 1;
      }
    }

    private class a
    {
      private a()
      {
      }

      public boolean isInitialSticky()
      {
        try
        {
          boolean bool = ConduitClient.a.this.isInitialStickyBroadcast();
          return bool;
        }
        catch (Exception localException)
        {
        }
        return false;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ConduitClient
 * JD-Core Version:    0.6.2
 */