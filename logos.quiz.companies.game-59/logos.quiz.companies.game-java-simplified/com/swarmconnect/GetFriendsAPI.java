package com.swarmconnect;

import java.util.List;

class GetFriendsAPI extends APICall
{
  public transient List<SwarmUser> friends;
  public transient List<SwarmUser> incomingRequests;
  public FriendStatus status = FriendStatus.ALL;

  public static enum FriendStatus
  {
    static
    {
      ACCEPTED = new FriendStatus("ACCEPTED", 1);
      PENDING = new FriendStatus("PENDING", 2);
      FriendStatus[] arrayOfFriendStatus = new FriendStatus[3];
      arrayOfFriendStatus[0] = ALL;
      arrayOfFriendStatus[1] = ACCEPTED;
      arrayOfFriendStatus[2] = PENDING;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.GetFriendsAPI
 * JD-Core Version:    0.6.2
 */