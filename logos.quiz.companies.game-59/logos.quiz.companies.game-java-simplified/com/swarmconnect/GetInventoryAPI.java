package com.swarmconnect;

import java.util.List;

class GetInventoryAPI extends APICall
{
  public transient List<SwarmInventoryItemTransport> inventory;

  public static class SwarmInventoryItemTransport
  {
    public SwarmStoreItem item;
    public int quantity;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.GetInventoryAPI
 * JD-Core Version:    0.6.2
 */