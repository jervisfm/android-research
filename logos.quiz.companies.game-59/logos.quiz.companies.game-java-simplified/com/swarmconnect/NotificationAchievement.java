package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationAchievement extends SwarmNotification
{
  public SwarmAchievement achievement;

  protected NotificationAchievement(SwarmAchievement paramSwarmAchievement)
  {
    this.achievement = paramSwarmAchievement;
    this.type = SwarmNotification.NotificationType.ACHIEVEMENT;
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_trophy", null, str);
  }

  public String getMessage()
  {
    return this.achievement.title;
  }

  public String getTitle()
  {
    return "Achievement Unlocked!";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationAchievement
 * JD-Core Version:    0.6.2
 */