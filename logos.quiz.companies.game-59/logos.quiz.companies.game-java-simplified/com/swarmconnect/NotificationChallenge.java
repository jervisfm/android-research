package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationChallenge extends SwarmNotification
{
  public SwarmChallenge challenge;

  protected NotificationChallenge()
  {
    this.type = SwarmNotification.NotificationType.CHALLENGE;
  }

  protected void a()
  {
    aa localaa = new aa();
    localaa.challengeId = this.data;
    localaa.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        aa localaa = (aa)paramAnonymousAPICall;
        NotificationChallenge.this.challenge = localaa.challenge;
        if (NotificationChallenge.this.challenge != null)
          Swarm.a(NotificationChallenge.this);
      }
    };
    localaa.run();
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_trophy", null, str);
  }

  public String getMessage()
  {
    return "Challenge text";
  }

  public String getTitle()
  {
    return "Challenge";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationChallenge
 * JD-Core Version:    0.6.2
 */