package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationFriend extends SwarmNotification
{
  public boolean accepted;
  public SwarmUser friend;

  protected NotificationFriend()
  {
    this.type = SwarmNotification.NotificationType.FRIEND;
  }

  protected void a()
  {
    aq.invalidate(GetFriendsAPI.class);
    bt localbt = new bt();
    localbt.friendId = this.data;
    localbt.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bt localbt = (bt)paramAnonymousAPICall;
        NotificationFriend.this.friend = localbt.friend;
        NotificationFriend.this.accepted = localbt.accepted;
        if (NotificationFriend.this.friend != null)
          Swarm.a(NotificationFriend.this);
      }
    };
    localbt.run();
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_friends", null, str);
  }

  public String getMessage()
  {
    if (this.accepted)
      return "Accepted your friend request!";
    return "Wants to be your friend!";
  }

  public String getTitle()
  {
    return this.friend.username;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationFriend
 * JD-Core Version:    0.6.2
 */