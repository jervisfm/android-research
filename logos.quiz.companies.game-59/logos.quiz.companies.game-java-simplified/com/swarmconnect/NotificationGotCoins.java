package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationGotCoins extends SwarmNotification
{
  protected NotificationGotCoins()
  {
    this.type = SwarmNotification.NotificationType.GOT_COINS;
  }

  protected void a()
  {
    aq.invalidate(bo.class);
    bo localbo = new bo();
    localbo.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        ((bo)paramAnonymousAPICall);
        if (Swarm.user != null)
          Swarm.a(NotificationGotCoins.this);
      }
    };
    localbo.run();
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_coin", null, str);
  }

  public String getMessage()
  {
    return this.data + " coins added to your account.";
  }

  public String getTitle()
  {
    return "You earned coins!";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationGotCoins
 * JD-Core Version:    0.6.2
 */