package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationLeaderboard extends SwarmNotification
{
  public SwarmLeaderboardScore score;

  protected NotificationLeaderboard(SwarmLeaderboardScore paramSwarmLeaderboardScore)
  {
    this.score = paramSwarmLeaderboardScore;
    this.type = SwarmNotification.NotificationType.LEADERBOARD;
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_leaderboards", null, str);
  }

  public String getMessage()
  {
    return "You are ranked #" + this.score.rank + " this week";
  }

  public String getTitle()
  {
    return "Score Submitted!";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationLeaderboard
 * JD-Core Version:    0.6.2
 */