package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;
import java.util.List;

public class NotificationMessage extends SwarmNotification
{
  public SwarmMessage message;

  protected NotificationMessage()
  {
    this.type = SwarmNotification.NotificationType.MESSAGE;
  }

  protected void a()
  {
    aq.invalidate(aj.class);
    aq.invalidate(bq.class);
    aq.invalidate(bh.class);
    SwarmMessageThread localSwarmMessageThread = new SwarmMessageThread();
    localSwarmMessageThread.id = this.data;
    localSwarmMessageThread.getMessages(new SwarmMessageThread.GotMessagesCB()
    {
      public void gotMessages(List<SwarmMessage> paramAnonymousList)
      {
        for (int i = -1 + paramAnonymousList.size(); ; i--)
        {
          if (i < 0)
            return;
          NotificationMessage.this.message = ((SwarmMessage)paramAnonymousList.get(i));
          if ((NotificationMessage.this.message.from != null) && (Swarm.user != null) && (NotificationMessage.this.message.from.userId != Swarm.user.userId))
          {
            Swarm.a(NotificationMessage.this);
            return;
          }
        }
      }
    });
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_messages", null, str);
  }

  public String getMessage()
  {
    return this.message.message;
  }

  public String getTitle()
  {
    return "Message from " + this.message.from.username;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationMessage
 * JD-Core Version:    0.6.2
 */