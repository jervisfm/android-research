package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationOnline extends SwarmNotification
{
  public SwarmApplication app;
  public boolean online;
  public SwarmUser user;

  protected NotificationOnline()
  {
    this.type = SwarmNotification.NotificationType.ONLINE;
  }

  protected void a()
  {
    z localz = new z();
    localz.id = this.data;
    localz.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        z localz = (z)paramAnonymousAPICall;
        NotificationOnline.this.user = localz.user;
        NotificationOnline.this.app = localz.app;
        NotificationOnline.this.online = localz.online;
        Swarm.a(NotificationOnline.this);
      }
    };
    localz.run();
  }

  public int getIconId(Context paramContext)
  {
    try
    {
      String str1 = paramContext.getApplicationContext().getPackageName();
      if (this.online);
      for (String str2 = "@drawable/swarm_friend_online"; ; str2 = "@drawable/swarm_friend_offline")
        return paramContext.getApplicationContext().getResources().getIdentifier(str2, null, str1);
    }
    catch (Exception localException)
    {
    }
    return 0;
  }

  public String getMessage()
  {
    if (this.online)
      return "Playing " + this.app.name;
    return "";
  }

  public String getTitle()
  {
    if (this.online);
    for (String str = " came online"; ; str = " went offline")
      return this.user.username + str;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationOnline
 * JD-Core Version:    0.6.2
 */