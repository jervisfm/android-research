package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;

public class NotificationPurchase extends SwarmNotification
{
  public SwarmStoreListing listing;

  protected NotificationPurchase(SwarmStoreListing paramSwarmStoreListing)
  {
    this.listing = paramSwarmStoreListing;
    this.type = SwarmNotification.NotificationType.PURCHASE;
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_coin", null, str);
  }

  public String getMessage()
  {
    return this.listing.title;
  }

  public String getTitle()
  {
    return "Item Purchased!";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationPurchase
 * JD-Core Version:    0.6.2
 */