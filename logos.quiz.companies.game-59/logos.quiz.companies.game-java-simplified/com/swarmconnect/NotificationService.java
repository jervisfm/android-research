package com.swarmconnect;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

public class NotificationService extends Service
{
  private static int a = 1;
  private static Handler b;
  private static Context c;
  private LocalBinder d = new LocalBinder();

  protected static void a()
  {
    try
    {
      b.post(new Runnable()
      {
        public void run()
        {
          af.a(NotificationService.b());
        }
      });
      return;
    }
    catch (Exception localException)
    {
    }
  }

  protected static void a(int paramInt)
  {
    a = paramInt;
  }

  protected static void a(String paramString)
  {
  }

  public IBinder onBind(Intent paramIntent)
  {
    return this.d;
  }

  public void onCreate()
  {
    b = new Handler();
    c = getApplicationContext();
    ConduitClient localConduitClient = ConduitClient.a();
    if (localConduitClient != null)
      localConduitClient.a(this);
  }

  public void onDestroy()
  {
    ConduitClient localConduitClient = ConduitClient.a();
    if (localConduitClient != null)
      localConduitClient.b();
  }

  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    ConduitClient localConduitClient = ConduitClient.a();
    if (localConduitClient != null)
      localConduitClient.a(this);
    return a;
  }

  public class LocalBinder extends Binder
  {
    public LocalBinder()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.NotificationService
 * JD-Core Version:    0.6.2
 */