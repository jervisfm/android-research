package com.swarmconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public abstract class PushReceiver extends BroadcastReceiver
{
  public static final String ACTION_CUSTOM_PUSH_RECEIVED = "com.swarmconnect.CUSTOM_PUSH_RECEIVED";
  public static final String EXTRA_DATA = "data";
  public static final String EXTRA_PACKAGE = "package";

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str1 = paramIntent.getAction();
    String str2 = paramIntent.getStringExtra("package");
    String str3 = paramIntent.getStringExtra("data");
    NotificationService.a("PushReceiver.onReceive(): " + paramIntent + ", " + str1 + ", " + str2 + ", " + str3);
    if ((str1.equals("com.swarmconnect.CUSTOM_PUSH_RECEIVED")) && (str2 != null) && (str2.equalsIgnoreCase(paramContext.getPackageName())))
      pushReceived(paramContext, str3);
  }

  public abstract void pushReceived(Context paramContext, String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.PushReceiver
 * JD-Core Version:    0.6.2
 */