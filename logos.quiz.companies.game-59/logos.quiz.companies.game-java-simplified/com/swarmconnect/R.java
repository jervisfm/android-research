package com.swarmconnect;

public final class R
{
  public static final class attr
  {
  }

  public static final class drawable
  {
    public static final int icon = 2130837504;
    public static final int swarm_add_friend = 2130837505;
    public static final int swarm_background = 2130837506;
    public static final int swarm_bubble_arrow = 2130837507;
    public static final int swarm_bubble_arrow_left = 2130837508;
    public static final int swarm_checkmark = 2130837509;
    public static final int swarm_close = 2130837510;
    public static final int swarm_coin = 2130837511;
    public static final int swarm_coin_small = 2130837512;
    public static final int swarm_facebook_icon = 2130837513;
    public static final int swarm_friend = 2130837514;
    public static final int swarm_friend_offline = 2130837515;
    public static final int swarm_friend_online = 2130837516;
    public static final int swarm_friends = 2130837517;
    public static final int swarm_friends40 = 2130837518;
    public static final int swarm_games = 2130837519;
    public static final int swarm_games40 = 2130837520;
    public static final int swarm_head = 2130837521;
    public static final int swarm_home = 2130837522;
    public static final int swarm_icon = 2130837523;
    public static final int swarm_leaderboards = 2130837524;
    public static final int swarm_leaderboards40 = 2130837525;
    public static final int swarm_logo = 2130837526;
    public static final int swarm_mail = 2130837527;
    public static final int swarm_mail_grey = 2130837528;
    public static final int swarm_messages = 2130837529;
    public static final int swarm_messages40 = 2130837530;
    public static final int swarm_quit = 2130837531;
    public static final int swarm_reply = 2130837532;
    public static final int swarm_settings = 2130837533;
    public static final int swarm_settings40 = 2130837534;
    public static final int swarm_store = 2130837535;
    public static final int swarm_store40 = 2130837536;
    public static final int swarm_trophy = 2130837537;
    public static final int swarm_trophy40 = 2130837538;
    public static final int swarm_trophy_gold = 2130837539;
    public static final int swarm_trophy_grey = 2130837540;
    public static final int swarm_x = 2130837541;
  }

  public static final class id
  {
    public static final int accept = 2130968606;
    public static final int achievement_icon = 2130968576;
    public static final int add = 2130968601;
    public static final int add_friend_username = 2130968600;
    public static final int background = 2130968583;
    public static final int benefits = 2130968595;
    public static final int cancel = 2130968650;
    public static final int change_pass = 2130968661;
    public static final int close = 2130968615;
    public static final int coins = 2130968630;
    public static final int coins_icon = 2130968644;
    public static final int confirm = 2130968587;
    public static final int confirm_error = 2130968586;
    public static final int confirm_password = 2130968658;
    public static final int create = 2130968590;
    public static final int current_password = 2130968656;
    public static final int date = 2130968670;
    public static final int deny = 2130968607;
    public static final int desc = 2130968647;
    public static final int description = 2130968579;
    public static final int email = 2130968589;
    public static final int email_error = 2130968588;
    public static final int empty_list = 2130968597;
    public static final int existing = 2130968651;
    public static final int external_provider = 2130968640;
    public static final int extra = 2130968592;
    public static final int facebook_login = 2130968627;
    public static final int footer = 2130968599;
    public static final int friended_section = 2130968604;
    public static final int get_coins = 2130968664;
    public static final int guest = 2130968652;
    public static final int header = 2130968611;
    public static final int home = 2130968613;
    public static final int icon = 2130968591;
    public static final int image = 2130968646;
    public static final int list = 2130968598;
    public static final int login = 2130968638;
    public static final int logo = 2130968612;
    public static final int logout = 2130968663;
    public static final int lost_password = 2130968637;
    public static final int mail = 2130968629;
    public static final int message = 2130968628;
    public static final int messages = 2130968614;
    public static final int more = 2130968633;
    public static final int name = 2130968581;
    public static final int new_password = 2130968657;
    public static final int notifications = 2130968662;
    public static final int num_items = 2130968666;
    public static final int offline_error = 2130968625;
    public static final int offline_error_underline = 2130968626;
    public static final int online_text = 2130968603;
    public static final int password = 2130968585;
    public static final int password_box = 2130968660;
    public static final int password_error = 2130968584;
    public static final int paypal = 2130968610;
    public static final int pic = 2130968580;
    public static final int points = 2130968577;
    public static final int popup_bottom_half = 2130968648;
    public static final int popup_header = 2130968634;
    public static final int popup_top_half = 2130968645;
    public static final int price = 2130968665;
    public static final int profile_pic = 2130968602;
    public static final int progressbar = 2130968635;
    public static final int publisher = 2130968582;
    public static final int purchase = 2130968649;
    public static final int rank = 2130968631;
    public static final int reply = 2130968667;
    public static final int row = 2130968639;
    public static final int score = 2130968632;
    public static final int select_text = 2130968636;
    public static final int send = 2130968668;
    public static final int set_pass = 2130968659;
    public static final int set_password_box = 2130968655;
    public static final int sheader = 2130968616;
    public static final int sheader_btn1 = 2130968619;
    public static final int sheader_btn1_dropdown = 2130968621;
    public static final int sheader_btn1_text = 2130968620;
    public static final int sheader_btn2 = 2130968622;
    public static final int sheader_btn2_dropdown = 2130968624;
    public static final int sheader_btn2_text = 2130968623;
    public static final int sheader_icon = 2130968617;
    public static final int sheader_title = 2130968618;
    public static final int speech_bubble = 2130968669;
    public static final int submit = 2130968641;
    public static final int success = 2130968642;
    public static final int swarm_purchase_popup = 2130968643;
    public static final int tapjoy = 2130968608;
    public static final int terms = 2130968596;
    public static final int title = 2130968578;
    public static final int unfriended_section = 2130968605;
    public static final int upgrade = 2130968654;
    public static final int upgrade_box = 2130968653;
    public static final int username = 2130968594;
    public static final int username_error = 2130968593;
    public static final int webview = 2130968671;
    public static final int zong = 2130968609;
  }

  public static final class layout
  {
    public static final int swarm_achievement_row = 2130903040;
    public static final int swarm_app_row = 2130903041;
    public static final int swarm_create_account = 2130903042;
    public static final int swarm_dashboard_row = 2130903043;
    public static final int swarm_external_username = 2130903044;
    public static final int swarm_friends = 2130903045;
    public static final int swarm_friends_row = 2130903046;
    public static final int swarm_get_coins = 2130903047;
    public static final int swarm_header = 2130903048;
    public static final int swarm_header_external = 2130903049;
    public static final int swarm_inbox_row = 2130903050;
    public static final int swarm_inventory = 2130903051;
    public static final int swarm_leaderboard_list = 2130903052;
    public static final int swarm_leaderboard_row = 2130903053;
    public static final int swarm_list = 2130903054;
    public static final int swarm_list_section_header = 2130903055;
    public static final int swarm_load_more = 2130903056;
    public static final int swarm_loading_popup = 2130903057;
    public static final int swarm_login = 2130903058;
    public static final int swarm_login_row = 2130903059;
    public static final int swarm_lost_password = 2130903060;
    public static final int swarm_purchase_popup = 2130903061;
    public static final int swarm_select_username = 2130903062;
    public static final int swarm_settings = 2130903063;
    public static final int swarm_store = 2130903064;
    public static final int swarm_store_category = 2130903065;
    public static final int swarm_store_category_row = 2130903066;
    public static final int swarm_store_row = 2130903067;
    public static final int swarm_thread = 2130903068;
    public static final int swarm_thread_row_other = 2130903069;
    public static final int swarm_thread_row_self = 2130903070;
    public static final int swarm_upgrade_guest = 2130903071;
    public static final int swarm_webview = 2130903072;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.R
 * JD-Core Version:    0.6.2
 */