package com.swarmconnect;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SwarmAchievement
{
  protected long a;
  public String description;
  public boolean hidden;
  public int id;
  public int orderId;
  public int points;
  public String title;
  public boolean unlocked;

  public static void getAchievementsList(GotAchievementsListCB paramGotAchievementsListCB)
  {
    p localp = new p();
    localp.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        p localp = (p)paramAnonymousAPICall;
        if (SwarmAchievement.this != null)
          SwarmAchievement.this.gotList(localp.achievements);
      }

      public void requestFailed()
      {
        if (SwarmAchievement.this != null)
          SwarmAchievement.this.gotList(null);
      }
    };
    localp.run();
  }

  public static void getAchievementsMap(GotAchievementsMapCB paramGotAchievementsMapCB)
  {
    p localp = new p();
    localp.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        p localp = (p)paramAnonymousAPICall;
        HashMap localHashMap;
        Iterator localIterator;
        if (SwarmAchievement.this != null)
        {
          localHashMap = new HashMap();
          if (localp.achievements != null)
            localIterator = localp.achievements.iterator();
        }
        while (true)
        {
          if (!localIterator.hasNext())
          {
            SwarmAchievement.this.gotMap(localHashMap);
            return;
          }
          SwarmAchievement localSwarmAchievement = (SwarmAchievement)localIterator.next();
          localHashMap.put(Integer.valueOf(localSwarmAchievement.id), localSwarmAchievement);
        }
      }

      public void requestFailed()
      {
        if (SwarmAchievement.this != null)
          SwarmAchievement.this.gotMap(null);
      }
    };
    localp.run();
  }

  public Date getUnlockDate()
  {
    if ((this.unlocked) && (this.a > 0L))
      return new Date(1000L * this.a);
    return null;
  }

  public void unlock()
  {
    unlock(null);
  }

  public void unlock(final AchievementUnlockedCB paramAchievementUnlockedCB)
  {
    if ((Swarm.isLoggedIn()) && (!this.unlocked))
    {
      this.unlocked = true;
      y localy = new y();
      localy.achievementId = this.id;
      localy.cb = new APICall.APICallback()
      {
        public void gotAPI(APICall paramAnonymousAPICall)
        {
          y localy = (y)paramAnonymousAPICall;
          if (paramAchievementUnlockedCB != null)
            paramAchievementUnlockedCB.achievementUnlocked(localy.unlocked);
          if (localy.unlocked)
            Swarm.a(new NotificationAchievement(SwarmAchievement.this));
          SwarmAchievement.this.unlocked = localy.unlocked;
        }

        public void requestFailed()
        {
          if (paramAchievementUnlockedCB != null)
            paramAchievementUnlockedCB.achievementUnlocked(false);
          SwarmAchievement.this.unlocked = false;
        }
      };
      localy.run();
    }
  }

  public static abstract class AchievementUnlockedCB
  {
    public abstract void achievementUnlocked(boolean paramBoolean);
  }

  public static abstract class GotAchievementsListCB
  {
    public abstract void gotList(List<SwarmAchievement> paramList);
  }

  public static abstract class GotAchievementsMapCB
  {
    public abstract void gotMap(Map<Integer, SwarmAchievement> paramMap);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmAchievement
 * JD-Core Version:    0.6.2
 */