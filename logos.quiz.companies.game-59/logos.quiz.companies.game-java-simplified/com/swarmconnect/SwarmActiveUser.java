package com.swarmconnect;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.ArrayList;
import java.util.List;

public class SwarmActiveUser extends SwarmUser
{
  protected SwarmUserInventory a;
  public int numNewMessages = 0;

  public SwarmActiveUser(SwarmUser paramSwarmUser)
  {
    super(paramSwarmUser);
  }

  protected void a(int paramInt, boolean paramBoolean, final FriendRequestCB paramFriendRequestCB)
  {
    br localbr = new br();
    if (paramBoolean);
    for (int i = 1; ; i = 2)
    {
      localbr.action = i;
      localbr.friendId = paramInt;
      localbr.cb = new APICall.APICallback()
      {
        public void gotAPI(APICall paramAnonymousAPICall)
        {
          br localbr = (br)paramAnonymousAPICall;
          SwarmActiveUser.FriendRequestCB localFriendRequestCB;
          if (paramFriendRequestCB != null)
          {
            localFriendRequestCB = paramFriendRequestCB;
            if (localbr.statusCode == 0)
              break label34;
          }
          label34: for (boolean bool = true; ; bool = false)
          {
            localFriendRequestCB.requestSuccess(bool);
            return;
          }
        }

        public void requestFailed()
        {
          if (paramFriendRequestCB != null)
            paramFriendRequestCB.requestSuccess(false);
        }
      };
      localbr.run();
      return;
    }
  }

  protected void a(GetFriendsAPI.FriendStatus paramFriendStatus, final GotFriendsCB paramGotFriendsCB)
  {
    GetFriendsAPI localGetFriendsAPI = new GetFriendsAPI();
    localGetFriendsAPI.status = paramFriendStatus;
    localGetFriendsAPI.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        GetFriendsAPI localGetFriendsAPI = (GetFriendsAPI)paramAnonymousAPICall;
        if (paramGotFriendsCB != null)
        {
          if (localGetFriendsAPI.friends == null)
            localGetFriendsAPI.friends = new ArrayList();
          if (localGetFriendsAPI.incomingRequests == null)
            localGetFriendsAPI.incomingRequests = new ArrayList();
          paramGotFriendsCB.gotFriends(localGetFriendsAPI.friends, localGetFriendsAPI.incomingRequests);
        }
      }

      public void requestFailed()
      {
        if (paramGotFriendsCB != null)
          paramGotFriendsCB.gotFriends(null, null);
      }
    };
    localGetFriendsAPI.run();
  }

  public void acceptFriendRequest(int paramInt, FriendRequestCB paramFriendRequestCB)
  {
    addFriend(paramInt, paramFriendRequestCB);
  }

  public void addFriend(int paramInt, FriendRequestCB paramFriendRequestCB)
  {
    a(paramInt, true, paramFriendRequestCB);
  }

  public void deleteFriend(int paramInt, FriendRequestCB paramFriendRequestCB)
  {
    a(paramInt, false, paramFriendRequestCB);
  }

  public void getCloudData(final String paramString, final GotCloudDataCB paramGotCloudDataCB)
  {
    if ((paramString == null) || (paramString.length() < 1))
      return;
    co localco = new co();
    localco.key = paramString;
    localco.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        co localco = (co)paramAnonymousAPICall;
        String str = localco.data;
        SharedPreferences localSharedPreferences;
        if (Swarm.b != null)
        {
          localSharedPreferences = Swarm.b.getSharedPreferences("SwarmPrefs", 0);
          if (localco.timestamp <= localSharedPreferences.getLong("clouddata_" + SwarmActiveUser.this.userId + "_" + paramString + "_ts", 0L))
            break label215;
          SharedPreferences.Editor localEditor = localSharedPreferences.edit();
          localEditor.putString("clouddata_" + SwarmActiveUser.this.userId + "_" + paramString, localco.data);
          localEditor.putLong("clouddata_" + SwarmActiveUser.this.userId + "_" + paramString + "_ts", System.currentTimeMillis() / 1000L);
          localEditor.commit();
        }
        while (true)
        {
          if (paramGotCloudDataCB != null)
            paramGotCloudDataCB.gotData(str);
          return;
          label215: str = localSharedPreferences.getString("clouddata_" + SwarmActiveUser.this.userId + "_" + paramString, "");
        }
      }

      public void requestFailed()
      {
        if (paramGotCloudDataCB != null)
        {
          if (Swarm.b != null)
          {
            String str = Swarm.b.getSharedPreferences("SwarmPrefs", 0).getString("clouddata_" + SwarmActiveUser.this.userId + "_" + paramString, null);
            paramGotCloudDataCB.gotData(str);
          }
        }
        else
          return;
        paramGotCloudDataCB.gotData(null);
      }
    };
    localco.run();
  }

  public void getCoins(final GotUserCoinsCB paramGotUserCoinsCB)
  {
    bo localbo = new bo();
    localbo.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bo localbo = (bo)paramAnonymousAPICall;
        if (paramGotUserCoinsCB != null)
          paramGotUserCoinsCB.gotCoins(localbo.coins);
      }

      public void requestFailed()
      {
        if (paramGotUserCoinsCB != null)
          paramGotUserCoinsCB.gotCoins(0);
      }
    };
    localbo.run();
  }

  public void getFriendRequests(GotFriendsCB paramGotFriendsCB)
  {
    a(GetFriendsAPI.FriendStatus.PENDING, paramGotFriendsCB);
  }

  public void getFriends(GotFriendsCB paramGotFriendsCB)
  {
    a(GetFriendsAPI.FriendStatus.ALL, paramGotFriendsCB);
  }

  public void getInventory(GotInventoryCB paramGotInventoryCB)
  {
    if (this.a == null)
    {
      this.a = SwarmUserInventory.a(paramGotInventoryCB);
      return;
    }
    this.a.refresh(paramGotInventoryCB);
  }

  public boolean isGuestAccount()
  {
    return (this.username != null) && ((this.username.matches("guest-[0-9a-f]+")) || (this.username.equals("OfflineGuest")));
  }

  public boolean isOfflineGuest()
  {
    return this.userId == -1;
  }

  public void saveCloudData(String paramString1, String paramString2)
  {
    saveCloudData(paramString1, paramString2, null);
  }

  public void saveCloudData(String paramString1, String paramString2, final SaveCloudDataCB paramSaveCloudDataCB)
  {
    if ((paramString1 == null) || (paramString1.length() < 1));
    do
    {
      return;
      as localas = new as();
      localas.key = paramString1;
      localas.data = paramString2;
      localas.cb = new APICall.APICallback()
      {
        public void gotAPI(APICall paramAnonymousAPICall)
        {
          if (paramSaveCloudDataCB != null)
            paramSaveCloudDataCB.dataSaved();
        }
      };
      localas.run();
    }
    while (Swarm.b == null);
    SharedPreferences.Editor localEditor = Swarm.b.getSharedPreferences("SwarmPrefs", 0).edit();
    localEditor.putString("clouddata_" + this.userId + "_" + paramString1, paramString2);
    localEditor.putLong("clouddata_" + this.userId + "_" + paramString1 + "_ts", System.currentTimeMillis() / 1000L);
    localEditor.commit();
  }

  public void upgradeGuest()
  {
    if (isGuestAccount())
      Swarm.show(15);
  }

  public static abstract class FriendRequestCB
  {
    public abstract void requestSuccess(boolean paramBoolean);
  }

  public static abstract class GotCloudDataCB
  {
    public abstract void gotData(String paramString);
  }

  public static abstract class GotFriendsCB
  {
    public abstract void gotFriends(List<SwarmUser> paramList1, List<SwarmUser> paramList2);
  }

  public static abstract class GotInventoryCB
  {
    public abstract void gotInventory(SwarmUserInventory paramSwarmUserInventory);
  }

  public static abstract class GotUserCoinsCB
  {
    public abstract void gotCoins(int paramInt);
  }

  public static abstract class SaveCloudDataCB
  {
    public abstract void dataSaved();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmActiveUser
 * JD-Core Version:    0.6.2
 */