package com.swarmconnect;

import android.app.Activity;
import android.os.Bundle;

public class SwarmActivity extends Activity
{
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Swarm.setActive(this);
  }

  protected void onPause()
  {
    super.onPause();
    Swarm.setInactive(this);
  }

  protected void onResume()
  {
    super.onResume();
    Swarm.setActive(this);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmActivity
 * JD-Core Version:    0.6.2
 */