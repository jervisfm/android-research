package com.swarmconnect;

class SwarmChallenge
{
  public SwarmApplication app;
  public String data;
  public int id;
  public boolean onlineOnly;
  public ChallengeStatus status = ChallengeStatus.PENDING;
  public ChallengeType type = ChallengeType.CUSTOM;
  public SwarmUser user;

  private void a(boolean paramBoolean, final ModChallengeCB paramModChallengeCB)
  {
    h localh = new h();
    localh.challengeId = this.id;
    if (paramBoolean);
    for (String str = ChallengeStatus.ACCEPTED.name(); ; str = ChallengeStatus.DECLINED.name())
    {
      localh.status = str;
      localh.cb = new APICall.APICallback()
      {
        public void gotAPI(APICall paramAnonymousAPICall)
        {
          if (paramModChallengeCB != null)
            paramModChallengeCB.challengeChanged();
        }
      };
      localh.run();
      return;
    }
  }

  public static void sendChallenge(int paramInt1, String paramString, boolean paramBoolean, int paramInt2, SentChallengeCB paramSentChallengeCB)
  {
    an localan = new an();
    localan.inviteId = paramInt1;
    localan.data = paramString;
    localan.onlineOnly = paramBoolean;
    localan.expiration = paramInt2;
    localan.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        an localan = (an)paramAnonymousAPICall;
        if (SwarmChallenge.this != null)
          SwarmChallenge.this.challengeSent(localan.challengeSent);
      }
    };
    localan.run();
  }

  public void acceptChallenge(ModChallengeCB paramModChallengeCB)
  {
    a(true, paramModChallengeCB);
  }

  public void declineChallenge(ModChallengeCB paramModChallengeCB)
  {
    a(false, paramModChallengeCB);
  }

  public static enum ChallengeStatus
  {
    static
    {
      ACCEPTED = new ChallengeStatus("ACCEPTED", 1);
      DECLINED = new ChallengeStatus("DECLINED", 2);
      COMPLETED = new ChallengeStatus("COMPLETED", 3);
      ChallengeStatus[] arrayOfChallengeStatus = new ChallengeStatus[4];
      arrayOfChallengeStatus[0] = PENDING;
      arrayOfChallengeStatus[1] = ACCEPTED;
      arrayOfChallengeStatus[2] = DECLINED;
      arrayOfChallengeStatus[3] = COMPLETED;
    }
  }

  public static enum ChallengeType
  {
    static
    {
      CUSTOM = new ChallengeType("CUSTOM", 2);
      ChallengeType[] arrayOfChallengeType = new ChallengeType[3];
      arrayOfChallengeType[0] = SCORE_HIGHER;
      arrayOfChallengeType[1] = SCORE_LOWER;
      arrayOfChallengeType[2] = CUSTOM;
    }
  }

  public static abstract class ModChallengeCB
  {
    public abstract void challengeChanged();
  }

  public static abstract class SentChallengeCB
  {
    public abstract void challengeSent(boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmChallenge
 * JD-Core Version:    0.6.2
 */