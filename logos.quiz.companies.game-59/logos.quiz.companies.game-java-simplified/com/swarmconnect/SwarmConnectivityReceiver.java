package com.swarmconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;

public class SwarmConnectivityReceiver extends BroadcastReceiver
{
  private static boolean a = true;

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((!paramIntent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) || ((Integer.parseInt(Build.VERSION.SDK) >= 5) && (new a(null).isInitialSticky())))
      return;
    if (paramIntent.getBooleanExtra("noConnectivity", false))
    {
      a = false;
      Swarm.a();
      return;
    }
    a = true;
    new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          Thread.sleep(2000L);
          label6: if (SwarmConnectivityReceiver.a())
            Swarm.b();
          return;
        }
        catch (Exception localException)
        {
          break label6;
        }
      }
    }).start();
  }

  private class a
  {
    private a()
    {
    }

    public boolean isInitialSticky()
    {
      try
      {
        boolean bool = SwarmConnectivityReceiver.this.isInitialStickyBroadcast();
        return bool;
      }
      catch (Exception localException)
      {
      }
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmConnectivityReceiver
 * JD-Core Version:    0.6.2
 */