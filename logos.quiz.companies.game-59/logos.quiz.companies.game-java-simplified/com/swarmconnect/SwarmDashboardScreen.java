package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.delegates.SwarmLoginListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class SwarmDashboardScreen extends u
{
  private static ArrayList<DashboardItem> t = new ArrayList();
  private DashboardAdapter l = new DashboardAdapter();
  private int m = 0;
  private int n = 0;
  private int o = 0;
  private int p = 0;
  private boolean q = false;
  private boolean r = false;
  private boolean s = false;
  private int u = 0;
  private SwarmLoginListener v = new ai(this);

  private void f()
  {
    this.u = (1 + this.u);
    if (this.u == 3)
    {
      if (this.s)
        t.add(1, new DashboardItem(a("@drawable/swarm_store40"), "Store", 12));
      if (this.r)
        t.add(1, new DashboardItem(a("@drawable/swarm_trophy40"), "Achievements", 1));
      if (this.q)
        t.add(1, new DashboardItem(a("@drawable/swarm_leaderboards40"), "Leaderboards", 8));
      this.l.notifyDataSetChanged();
      c();
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_list"));
    if ((!Swarm.isInitialized()) && (this.c.getPackageName().equalsIgnoreCase("com.swarmconnect")))
      Swarm.init(this.c, 2, "1279has9f7y1972ya", this.v);
    if (Swarm.isInitialized())
    {
      ListView localListView = (ListView)a(a("@id/list"));
      localListView.setAdapter(this.l);
      localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
        {
          SwarmDashboardScreen.DashboardItem localDashboardItem = (SwarmDashboardScreen.DashboardItem)SwarmDashboardScreen.e(SwarmDashboardScreen.this).getItem(paramAnonymousInt);
          if (localDashboardItem != null)
            SwarmDashboardScreen.this.show(localDashboardItem.screenType);
        }
      });
    }
    while (true)
    {
      super.onCreate(paramBundle);
      return;
      finish();
    }
  }

  protected void reload()
  {
    if ((Swarm.user != null) && (!Swarm.user.isOfflineGuest()))
    {
      SwarmMessageThread.getNumNewMessages(new SwarmMessageThread.GotNumMessages()
      {
        public void gotNumMessages(int paramAnonymousInt)
        {
          SwarmDashboardScreen.a(SwarmDashboardScreen.this, paramAnonymousInt);
          SwarmDashboardScreen.e(SwarmDashboardScreen.this).notifyDataSetInvalidated();
        }
      });
      this.n = 0;
      SwarmAchievement.getAchievementsList(new SwarmAchievement.GotAchievementsListCB()
      {
        public void gotList(List<SwarmAchievement> paramAnonymousList)
        {
          Iterator localIterator;
          if ((paramAnonymousList != null) && (paramAnonymousList.size() > 0))
            localIterator = paramAnonymousList.iterator();
          while (true)
          {
            if (!localIterator.hasNext())
            {
              SwarmDashboardScreen.c(SwarmDashboardScreen.this, paramAnonymousList.size());
              SwarmDashboardScreen.e(SwarmDashboardScreen.this).notifyDataSetInvalidated();
              return;
            }
            SwarmAchievement localSwarmAchievement = (SwarmAchievement)localIterator.next();
            if (localSwarmAchievement.unlocked)
            {
              SwarmDashboardScreen localSwarmDashboardScreen = SwarmDashboardScreen.this;
              SwarmDashboardScreen.b(localSwarmDashboardScreen, 1 + SwarmDashboardScreen.c(localSwarmDashboardScreen));
            }
            else if (localSwarmAchievement.hidden)
            {
              localIterator.remove();
            }
          }
        }
      });
      Swarm.user.getFriendRequests(new SwarmActiveUser.GotFriendsCB()
      {
        public void gotFriends(List<SwarmUser> paramAnonymousList1, List<SwarmUser> paramAnonymousList2)
        {
          if (paramAnonymousList2 != null)
          {
            SwarmDashboardScreen.d(SwarmDashboardScreen.this, paramAnonymousList2.size());
            SwarmDashboardScreen.e(SwarmDashboardScreen.this).notifyDataSetInvalidated();
          }
        }
      });
    }
    if ((t.size() == 0) && (Swarm.user != null))
    {
      if (!u.b)
        t.add(new DashboardItem(a("@drawable/swarm_games40"), "Get More Games!", 17));
      t.add(new DashboardItem(a("@drawable/swarm_messages40"), "Messages", 5));
      t.add(new DashboardItem(a("@drawable/swarm_friends40"), "Friends", 3));
      t.add(new DashboardItem(a("@drawable/swarm_settings40"), "Settings", 16));
      if (!Swarm.user.isOfflineGuest())
      {
        b();
        SwarmLeaderboard.getLeaderboardsList(new SwarmLeaderboard.GotLeaderboardsListCB()
        {
          public void gotList(List<SwarmLeaderboard> paramAnonymousList)
          {
            if ((paramAnonymousList != null) && (paramAnonymousList.size() > 0))
              SwarmDashboardScreen.a(SwarmDashboardScreen.this, true);
            SwarmDashboardScreen.f(SwarmDashboardScreen.this);
          }
        });
        SwarmAchievement.getAchievementsList(new SwarmAchievement.GotAchievementsListCB()
        {
          public void gotList(List<SwarmAchievement> paramAnonymousList)
          {
            if ((paramAnonymousList != null) && (paramAnonymousList.size() > 0))
              SwarmDashboardScreen.b(SwarmDashboardScreen.this, true);
            SwarmDashboardScreen.f(SwarmDashboardScreen.this);
          }
        });
        SwarmStore.getStore(new SwarmStore.GotSwarmStoreCB()
        {
          public void gotStore(SwarmStore paramAnonymousSwarmStore)
          {
            Iterator localIterator;
            if ((paramAnonymousSwarmStore != null) && (paramAnonymousSwarmStore.categories != null))
            {
              localIterator = paramAnonymousSwarmStore.categories.iterator();
              if (localIterator.hasNext())
                break label38;
            }
            while (true)
            {
              SwarmDashboardScreen.f(SwarmDashboardScreen.this);
              return;
              label38: SwarmStoreCategory localSwarmStoreCategory = (SwarmStoreCategory)localIterator.next();
              if ((localSwarmStoreCategory.listings == null) || (localSwarmStoreCategory.listings.size() <= 0))
                break;
              SwarmDashboardScreen.c(SwarmDashboardScreen.this, true);
            }
          }
        });
      }
    }
  }

  public class DashboardAdapter extends BaseAdapter
  {
    public DashboardAdapter()
    {
    }

    public int getCount()
    {
      return SwarmDashboardScreen.e().size();
    }

    public Object getItem(int paramInt)
    {
      return SwarmDashboardScreen.e().get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      View localView = View.inflate(SwarmDashboardScreen.this.d(), SwarmDashboardScreen.this.a("@layout/swarm_dashboard_row"), null);
      SwarmDashboardScreen.DashboardItem localDashboardItem = (SwarmDashboardScreen.DashboardItem)getItem(paramInt);
      if (localDashboardItem != null)
        if (paramInt % 2 != 0)
          break label192;
      label192: for (int i = 16777215; ; i = -1996488705)
      {
        localView.setBackgroundColor(i);
        ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/name"))).setText(localDashboardItem.name);
        ((ImageView)localView.findViewById(SwarmDashboardScreen.this.a("@id/icon"))).setImageResource(localDashboardItem.iconResId);
        ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/extra"))).setVisibility(0);
        if ((localDashboardItem.screenType != 5) || (SwarmDashboardScreen.a(SwarmDashboardScreen.this) <= 0))
          break;
        ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/extra"))).setText(SwarmDashboardScreen.a(SwarmDashboardScreen.this) + " new messages");
        return localView;
      }
      if ((localDashboardItem.screenType == 1) && (SwarmDashboardScreen.b(SwarmDashboardScreen.this) > 0))
      {
        ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/extra"))).setText(SwarmDashboardScreen.c(SwarmDashboardScreen.this) + " / " + SwarmDashboardScreen.b(SwarmDashboardScreen.this) + " completed");
        return localView;
      }
      if ((localDashboardItem.screenType == 3) && (SwarmDashboardScreen.d(SwarmDashboardScreen.this) > 0))
      {
        ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/extra"))).setText(SwarmDashboardScreen.d(SwarmDashboardScreen.this) + " friend requests");
        return localView;
      }
      ((TextView)localView.findViewById(SwarmDashboardScreen.this.a("@id/extra"))).setVisibility(8);
      return localView;
    }
  }

  public static class DashboardItem
  {
    public int iconResId;
    public String name;
    public int screenType;

    public DashboardItem(int paramInt1, String paramString, int paramInt2)
    {
      this.iconResId = paramInt1;
      this.name = paramString;
      this.screenType = paramInt2;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmDashboardScreen
 * JD-Core Version:    0.6.2
 */