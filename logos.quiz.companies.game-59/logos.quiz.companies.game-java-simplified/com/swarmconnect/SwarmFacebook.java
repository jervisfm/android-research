package com.swarmconnect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class SwarmFacebook
{
  private static String a = "";
  private static Facebook b;
  private static boolean c = false;

  public static void authorizeCallback(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (b == null)
      return;
    b.authorizeCallback(paramInt1, paramInt2, paramIntent);
  }

  private static boolean b()
  {
    return "289484921072856".equals(a);
  }

  public static void clear(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("facebook-session", 0).edit();
    localEditor.clear();
    localEditor.commit();
  }

  public static void enableSingleSignOn()
  {
    c = true;
  }

  public static Facebook getSession()
  {
    if (!b())
      return b;
    return null;
  }

  public static void initialize(Context paramContext, String paramString)
  {
    if ((paramString == null) || (paramContext == null));
    while ((b != null) || (paramString.equals(a)))
      return;
    a = paramString;
    b = new Facebook(a);
    restore(paramContext);
  }

  public static boolean isValid()
  {
    return (b != null) && (b.isSessionValid());
  }

  public static void l(String paramString)
  {
    Swarm.c("Facebook: " + paramString);
  }

  public static void login(Activity paramActivity, FacebookLoginCB paramFacebookLoginCB)
  {
    login(paramActivity, null, paramFacebookLoginCB);
  }

  public static void login(Activity paramActivity, String[] paramArrayOfString, final FacebookLoginCB paramFacebookLoginCB)
  {
    boolean bool1 = true;
    StringBuilder localStringBuilder = new StringBuilder("call to login... ").append(b()).append(" && ");
    if ((paramActivity instanceof SwarmMainActivity));
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      l(bool2);
      if ((!b()) || ((paramActivity instanceof SwarmMainActivity)))
        break;
      paramFacebookLoginCB.loginError("Invalid Application Context");
      return;
    }
    if ((b == null) || (paramFacebookLoginCB == null))
    {
      paramFacebookLoginCB.loginError("Missing parameters");
      return;
    }
    if (b.isSessionValid())
    {
      paramFacebookLoginCB.loginSuccess(b);
      return;
    }
    if (paramArrayOfString == null)
    {
      paramArrayOfString = new String[bool1];
      paramArrayOfString[0] = "email";
    }
    int i = paramArrayOfString.length;
    Facebook.DialogListener local1;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        bool1 = false;
      while (paramArrayOfString[j].equals("email"))
      {
        if (!bool1)
        {
          String[] arrayOfString = new String[1 + paramArrayOfString.length];
          System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, paramArrayOfString.length);
          arrayOfString[paramArrayOfString.length] = "email";
          paramArrayOfString = arrayOfString;
        }
        local1 = new Facebook.DialogListener()
        {
          public void onCancel()
          {
            paramFacebookLoginCB.loginError("Login Canceled");
          }

          public void onComplete(Bundle paramAnonymousBundle)
          {
            SwarmFacebook.save(SwarmFacebook.this);
            paramFacebookLoginCB.loginSuccess(SwarmFacebook.a());
          }

          public void onError(DialogError paramAnonymousDialogError)
          {
            paramFacebookLoginCB.loginError("FB Login Error: " + paramAnonymousDialogError);
          }

          public void onFacebookError(FacebookError paramAnonymousFacebookError)
          {
            paramFacebookLoginCB.loginError("FB Login FacebookError: " + paramAnonymousFacebookError);
          }
        };
        if (!c)
          break;
        b.authorize(paramActivity, paramArrayOfString, local1);
        return;
      }
    }
    b.authorize(paramActivity, paramArrayOfString, -1, local1);
  }

  public static boolean restore(Context paramContext)
  {
    if ((b == null) || (b()))
      return false;
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("facebook-session", 0);
    b.setAccessToken(localSharedPreferences.getString("access_token", null));
    b.setAccessExpires(localSharedPreferences.getLong("expires_in", 0L));
    return b.isSessionValid();
  }

  public static boolean save(Context paramContext)
  {
    if ((b == null) || (b()))
      return false;
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("facebook-session", 0).edit();
    localEditor.putString("access_token", b.getAccessToken());
    localEditor.putLong("expires_in", b.getAccessExpires());
    return localEditor.commit();
  }

  public static abstract class FacebookLoginCB
  {
    public abstract void loginError(String paramString);

    public abstract void loginSuccess(Facebook paramFacebook);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmFacebook
 * JD-Core Version:    0.6.2
 */