package com.swarmconnect;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class SwarmIO
{
  private static Context a;
  private static boolean b = false;
  private static ArrayList<String> c = new ArrayList();
  private static Timer d;
  private static int e = 0;

  protected static String a(Context paramContext)
  {
    String str = null;
    if (paramContext == null);
    while (true)
    {
      return str;
      try
      {
        SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("SwarmIOClient", 0);
        str = localSharedPreferences.getString("clientId", null);
        if (str != null)
          continue;
        str = UUID.randomUUID().toString();
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        localEditor.putString("clientId", str);
        localEditor.commit();
      }
      finally
      {
      }
    }
  }

  public static void addGroup(String paramString)
  {
    try
    {
      e = 0;
      if (paramString != null)
      {
        String str = paramString.replaceAll("[^a-zA-Z0-9_-]", "");
        if (!c.contains(str))
        {
          c.add(str);
          d();
        }
      }
      return;
    }
    finally
    {
    }
  }

  protected static int b(Context paramContext)
  {
    int i = 0;
    if (paramContext == null);
    while (true)
    {
      return i;
      try
      {
        int j = paramContext.getSharedPreferences("SwarmIOClient", 0).getInt("appId", 0);
        i = j;
      }
      finally
      {
      }
    }
  }

  protected static String c(Context paramContext)
  {
    Object localObject1 = null;
    if (paramContext == null);
    while (true)
    {
      return localObject1;
      try
      {
        String str = paramContext.getSharedPreferences("SwarmIOClient", 0).getString("appAuth", null);
        localObject1 = str;
      }
      finally
      {
      }
    }
  }

  protected static String d(Context paramContext)
  {
    Object localObject1 = null;
    if (paramContext == null);
    while (true)
    {
      return localObject1;
      try
      {
        String str = paramContext.getSharedPreferences("SwarmIOClient", 0).getString("push_receiver_class", null);
        localObject1 = str;
      }
      finally
      {
      }
    }
  }

  private static void d()
  {
    while (true)
    {
      Iterator localIterator;
      try
      {
        Context localContext = a;
        if (localContext == null)
          return;
        localIterator = c.iterator();
        localObject2 = "";
        if (!localIterator.hasNext())
        {
          SharedPreferences.Editor localEditor = a.getSharedPreferences("SwarmIOClient", 0).edit();
          localEditor.putString("groups", (String)localObject2);
          localEditor.commit();
          if (d != null)
          {
            d.cancel();
            d.purge();
            d = null;
          }
          d = new Timer();
          d.schedule(new TimerTask()
          {
            public void run()
            {
              AsyncHttp.a("http://swarm.io/groups.php?clientid=" + SwarmIO.a(SwarmIO.a()) + "&app_id=" + SwarmIO.b(SwarmIO.a()) + "&auth=" + SwarmIO.c(SwarmIO.a()) + "&groups=" + SwarmIO.this, new AsyncHttp.AsyncCB()
              {
                public void gotURL(String paramAnonymous2String)
                {
                  SwarmIO.a(0);
                }

                public void requestFailed(Exception paramAnonymous2Exception)
                {
                  SwarmIO.a(1 + SwarmIO.b());
                  if (SwarmIO.b() <= 5)
                    SwarmIO.c();
                }
              });
            }
          }
          , 1000L + 1000L * ()Math.pow(2.0D, e));
          continue;
        }
      }
      finally
      {
      }
      String str1 = (String)localIterator.next();
      String str2 = localObject2 + str1 + ",";
      Object localObject2 = str2;
    }
  }

  public static void disablePush()
  {
    try
    {
      Context localContext = a;
      if (localContext == null);
      while (true)
      {
        return;
        b = false;
        stopService();
        SharedPreferences.Editor localEditor = a.getSharedPreferences("SwarmIOClient", 0).edit();
        localEditor.putBoolean("pushEnabled", false);
        localEditor.commit();
      }
    }
    finally
    {
    }
  }

  public static void enablePush()
  {
    try
    {
      Context localContext = a;
      if (localContext == null);
      while (true)
      {
        return;
        b = true;
        startService();
        SharedPreferences.Editor localEditor = a.getSharedPreferences("SwarmIOClient", 0).edit();
        localEditor.putBoolean("pushEnabled", true);
        localEditor.commit();
      }
    }
    finally
    {
    }
  }

  public static String[] getGroups()
  {
    try
    {
      if (c.size() > 0)
      {
        String[] arrayOfString2 = new String[c.size()];
        arrayOfString1 = (String[])c.toArray(arrayOfString2);
        return arrayOfString1;
      }
      String[] arrayOfString1 = null;
    }
    finally
    {
    }
  }

  public static boolean getPushEnabled(Context paramContext)
  {
    try
    {
      boolean bool = paramContext.getSharedPreferences("SwarmIOClient", 0).getBoolean("pushEnabled", true);
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static void init(Context paramContext, int paramInt, String paramString)
  {
    while (true)
    {
      int i;
      int j;
      try
      {
        a = paramContext;
        SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("SwarmIOClient", 0);
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        localEditor.putInt("appId", paramInt);
        localEditor.putString("appAuth", paramString);
        localEditor.commit();
        c.clear();
        String[] arrayOfString = localSharedPreferences.getString("groups", "").split(",");
        if ((arrayOfString != null) && (arrayOfString.length > 0))
        {
          i = arrayOfString.length;
          j = 0;
        }
        else
        {
          if (c.size() == 0)
            addGroup("users");
          b = localSharedPreferences.getBoolean("pushEnabled", true);
          if (b)
            enablePush();
          return;
          String str = arrayOfString[j];
          if ((str != null) && (str.length() > 0))
            c.add(str);
          j++;
        }
      }
      finally
      {
      }
      if (j < i);
    }
  }

  public static void removeGroup(String paramString)
  {
    try
    {
      e = 0;
      if (paramString != null)
      {
        String str = paramString.replaceAll("[^a-zA-Z0-9_-]", "");
        if (c.contains(str))
        {
          c.remove(str);
          d();
        }
      }
      return;
    }
    finally
    {
    }
  }

  public static void setPushReciever(Class<? extends PushReceiver> paramClass)
  {
    try
    {
      Context localContext = a;
      if (localContext == null);
      while (true)
      {
        return;
        SharedPreferences.Editor localEditor = a.getSharedPreferences("SwarmIOClient", 0).edit();
        localEditor.putString("push_receiver_class", paramClass.getName());
        localEditor.commit();
      }
    }
    finally
    {
    }
  }

  public static void setStartMode(int paramInt)
  {
    try
    {
      NotificationService.a(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static void startService()
  {
    try
    {
      Context localContext = a;
      if (localContext == null);
      while (true)
      {
        return;
        if (b)
          a.startService(new Intent(a, NotificationService.class));
      }
    }
    finally
    {
    }
  }

  public static void stopService()
  {
    try
    {
      Context localContext = a;
      if (localContext == null);
      while (true)
      {
        return;
        a.stopService(new Intent(a, NotificationService.class));
      }
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmIO
 * JD-Core Version:    0.6.2
 */