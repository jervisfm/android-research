package com.swarmconnect;

import android.app.Activity;
import android.content.Intent;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

public class SwarmLeaderboard
  implements Serializable
{
  public static final String LEADERBOARD_DATE_ALL_TIME = "all";
  public static final String LEADERBOARD_DATE_MONTH = "month";
  public static final String LEADERBOARD_DATE_TODAY = "today";
  public static final String LEADERBOARD_DATE_WEEK = "week";
  public static final String LEADERBOARD_SCOPE_ALL = "all";
  public static final String LEADERBOARD_SCOPE_FRIENDS = "friends";
  public static final String LEADERBOARD_SCOPE_SELF = "self";
  public LeaderboardFormat format;
  public int id;
  public int max_version;
  public int min_version;
  public String name;

  public static void getLeaderboardById(int paramInt, final GotLeaderboardCB paramGotLeaderboardCB)
  {
    if (paramGotLeaderboardCB == null)
      return;
    getLeaderboardsList(new GotLeaderboardsListCB()
    {
      public void gotList(List<SwarmLeaderboard> paramAnonymousList)
      {
        Iterator localIterator;
        if (paramAnonymousList != null)
          localIterator = paramAnonymousList.iterator();
        SwarmLeaderboard localSwarmLeaderboard;
        do
        {
          if (!localIterator.hasNext())
          {
            paramGotLeaderboardCB.gotLeaderboard(null);
            return;
          }
          localSwarmLeaderboard = (SwarmLeaderboard)localIterator.next();
        }
        while (localSwarmLeaderboard.id != this.a);
        paramGotLeaderboardCB.gotLeaderboard(localSwarmLeaderboard);
      }
    });
  }

  public static void getLeaderboardsList(GotLeaderboardsListCB paramGotLeaderboardsListCB)
  {
    b localb = new b();
    localb.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        b localb = (b)paramAnonymousAPICall;
        if (SwarmLeaderboard.this != null)
          SwarmLeaderboard.this.gotList(localb.leaderboards);
      }

      public void requestFailed()
      {
        if (SwarmLeaderboard.this != null)
          SwarmLeaderboard.this.gotList(null);
      }
    };
    localb.run();
  }

  public void getPageOfScores(int paramInt, String paramString1, String paramString2, final GotScoresCB paramGotScoresCB)
  {
    bp localbp = new bp();
    localbp.pageNum = paramInt;
    localbp.leaderboardId = this.id;
    localbp.time = paramString1;
    localbp.scope = paramString2;
    localbp.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bp localbp = (bp)paramAnonymousAPICall;
        if (paramGotScoresCB != null)
          paramGotScoresCB.gotScores(localbp.pageNum, localbp.scores);
      }

      public void requestFailed()
      {
        if (paramGotScoresCB != null)
          paramGotScoresCB.gotScores(-1, null);
      }
    };
    localbp.run();
  }

  public void getPageOfScoresForCurrentUser(String paramString, GotScoresCB paramGotScoresCB)
  {
    getPageOfScores(-1, paramString, "all", paramGotScoresCB);
  }

  public void getScoreForUser(int paramInt, final GotScoreCB paramGotScoreCB)
  {
    cq localcq = new cq();
    localcq.scoreUserId = paramInt;
    localcq.leaderboardId = this.id;
    localcq.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        cq localcq = (cq)paramAnonymousAPICall;
        if (paramGotScoreCB != null)
          paramGotScoreCB.gotScore(localcq.score);
      }

      public void requestFailed()
      {
        if (paramGotScoreCB != null)
          paramGotScoreCB.gotScore(null);
      }
    };
    localcq.run();
  }

  public void getScoreForUser(SwarmUser paramSwarmUser, GotScoreCB paramGotScoreCB)
  {
    if (paramSwarmUser != null)
      getScoreForUser(paramSwarmUser.userId, paramGotScoreCB);
    while (paramGotScoreCB == null)
      return;
    paramGotScoreCB.gotScore(null);
  }

  public void getTopScores(String paramString, GotScoresCB paramGotScoresCB)
  {
    getPageOfScores(0, paramString, "all", paramGotScoresCB);
  }

  public void showLeaderboard()
  {
    if (Swarm.b != null)
    {
      Intent localIntent = new Intent(Swarm.b, SwarmMainActivity.class);
      localIntent.putExtra("screenType", 7);
      localIntent.putExtra("leaderboard", this);
      Swarm.b.startActivity(localIntent);
    }
  }

  public void submitScore(float paramFloat)
  {
    submitScore(paramFloat, "", null);
  }

  public void submitScore(float paramFloat, String paramString)
  {
    submitScore(paramFloat, paramString, null);
  }

  public void submitScore(final float paramFloat, String paramString, final SubmitScoreCB paramSubmitScoreCB)
  {
    if (!Swarm.isLoggedIn())
      return;
    cj localcj = new cj();
    localcj.leaderboardId = this.id;
    localcj.score = paramFloat;
    localcj.data = paramString;
    localcj.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        cj localcj = (cj)paramAnonymousAPICall;
        if (paramSubmitScoreCB != null)
          paramSubmitScoreCB.scoreSubmitted(localcj.rank);
        SwarmLeaderboardScore localSwarmLeaderboardScore = new SwarmLeaderboardScore();
        localSwarmLeaderboardScore.user = Swarm.user;
        localSwarmLeaderboardScore.rank = localcj.rank;
        localSwarmLeaderboardScore.score = paramFloat;
        Swarm.a(new NotificationLeaderboard(localSwarmLeaderboardScore));
      }

      public void requestFailed()
      {
        if (paramSubmitScoreCB != null)
          paramSubmitScoreCB.scoreSubmitted(-1);
      }
    };
    localcj.run();
  }

  public static abstract class GotLeaderboardCB
  {
    public abstract void gotLeaderboard(SwarmLeaderboard paramSwarmLeaderboard);
  }

  public static abstract class GotLeaderboardsListCB
  {
    public abstract void gotList(List<SwarmLeaderboard> paramList);
  }

  public static abstract class GotScoreCB
  {
    public abstract void gotScore(SwarmLeaderboardScore paramSwarmLeaderboardScore);
  }

  public static abstract class GotScoresCB
  {
    public abstract void gotScores(int paramInt, List<SwarmLeaderboardScore> paramList);
  }

  public static enum LeaderboardFormat
  {
    static
    {
      FLOAT = new LeaderboardFormat("FLOAT", 1);
      TIME = new LeaderboardFormat("TIME", 2);
      LeaderboardFormat[] arrayOfLeaderboardFormat = new LeaderboardFormat[3];
      arrayOfLeaderboardFormat[0] = INTEGER;
      arrayOfLeaderboardFormat[1] = FLOAT;
      arrayOfLeaderboardFormat[2] = TIME;
    }
  }

  public static abstract class SubmitScoreCB
  {
    public abstract void scoreSubmitted(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmLeaderboard
 * JD-Core Version:    0.6.2
 */