package com.swarmconnect;

import java.io.Serializable;

public class SwarmLeaderboardScore
  implements Serializable, Comparable<SwarmLeaderboardScore>
{
  public int id;
  public int rank;
  public float score;
  public int timestamp;
  public SwarmUser user;

  public int compareTo(SwarmLeaderboardScore paramSwarmLeaderboardScore)
  {
    if (this.rank < paramSwarmLeaderboardScore.rank)
      return -1;
    if (this.rank == paramSwarmLeaderboardScore.rank)
      return 0;
    return 1;
  }

  public void getData(final GotDataCB paramGotDataCB)
  {
    if (paramGotDataCB == null)
      return;
    bs localbs = new bs();
    localbs.scoreId = this.id;
    localbs.scoreUserId = this.user.userId;
    localbs.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bs localbs = (bs)paramAnonymousAPICall;
        paramGotDataCB.gotData(localbs.data);
      }

      public void requestFailed()
      {
        paramGotDataCB.gotData(null);
      }
    };
    localbs.run();
  }

  public static abstract class GotDataCB
  {
    public abstract void gotData(String paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmLeaderboardScore
 * JD-Core Version:    0.6.2
 */