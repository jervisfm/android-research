package com.swarmconnect;

import com.swarmconnect.delegates.SwarmLoginListener;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class SwarmLoginManager
{
  protected static int a = 0;
  private static LinkedHashSet<SwarmLoginListener> b = new LinkedHashSet();

  protected static void a()
  {
    Iterator localIterator;
    if (a > 0)
    {
      a = 0;
      Swarm.c("login canceled");
      localIterator = ((LinkedHashSet)b.clone()).iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((SwarmLoginListener)localIterator.next()).loginCanceled();
    }
  }

  protected static void a(SwarmActiveUser paramSwarmActiveUser)
  {
    Iterator localIterator;
    if (a < 2)
    {
      Swarm.c("Logged in");
      a = 2;
      localIterator = ((LinkedHashSet)b.clone()).iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((SwarmLoginListener)localIterator.next()).userLoggedIn(paramSwarmActiveUser);
    }
  }

  public static void addLoginListener(SwarmLoginListener paramSwarmLoginListener)
  {
    if (paramSwarmLoginListener != null)
      synchronized (b)
      {
        b.add(paramSwarmLoginListener);
        return;
      }
  }

  protected static void b()
  {
    Iterator localIterator;
    if (a == 0)
    {
      Swarm.c("login started");
      a = 1;
      localIterator = ((LinkedHashSet)b.clone()).iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((SwarmLoginListener)localIterator.next()).loginStarted();
    }
  }

  protected static void c()
  {
    Iterator localIterator;
    if (a > 0)
    {
      Swarm.c("Logged out");
      a = 0;
      localIterator = ((LinkedHashSet)b.clone()).iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((SwarmLoginListener)localIterator.next()).userLoggedOut();
    }
  }

  public static void removeLoginListener(SwarmLoginListener paramSwarmLoginListener)
  {
    if (paramSwarmLoginListener != null)
      synchronized (b)
      {
        b.remove(paramSwarmLoginListener);
        return;
      }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmLoginManager
 * JD-Core Version:    0.6.2
 */