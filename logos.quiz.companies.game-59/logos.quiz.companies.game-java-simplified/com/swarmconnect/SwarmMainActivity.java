package com.swarmconnect;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import java.util.HashMap;

public class SwarmMainActivity extends SwarmActivity
{
  public static final int SCREEN_ACHIEVEMENTS = 1;
  public static final int SCREEN_CREATE_ACCOUNT = 2;
  public static final int SCREEN_DASHBOARD = 0;
  public static final int SCREEN_EXTERNAL_USERNAME = 18;
  public static final int SCREEN_FORTUMO = 22;
  public static final int SCREEN_FRIENDS = 3;
  public static final int SCREEN_GAMES = 17;
  public static final int SCREEN_GET_COINS = 4;
  public static final int SCREEN_INBOX = 5;
  public static final int SCREEN_INVENTORY = 6;
  public static final int SCREEN_LEADERBOARD = 7;
  public static final int SCREEN_LEADERBOARDS = 8;
  public static final int SCREEN_LOGIN = 9;
  public static final int SCREEN_LOST_PASSWORD = 10;
  public static final int SCREEN_MESSAGE_THREAD = 14;
  public static final int SCREEN_PAYPAL = 20;
  public static final int SCREEN_SELECT_USERNAME = 11;
  public static final int SCREEN_SETTINGS = 16;
  public static final int SCREEN_STORE = 12;
  public static final int SCREEN_STORE_CATEGORY = 13;
  public static final int SCREEN_TAPJOY = 19;
  public static final int SCREEN_TRAY_NOTIFICATION = -1000;
  public static final int SCREEN_UPGRADE_GUEST = 15;
  public static final int SCREEN_ZONG = 21;
  public static HashMap<Integer, Class<? extends u>> screenClasses = new HashMap();
  private int a;
  private u b;

  static
  {
    screenClasses.put(Integer.valueOf(0), SwarmDashboardScreen.class);
    screenClasses.put(Integer.valueOf(1), ao.class);
    screenClasses.put(Integer.valueOf(2), bi.class);
    screenClasses.put(Integer.valueOf(3), q.class);
    screenClasses.put(Integer.valueOf(4), ck.class);
    screenClasses.put(Integer.valueOf(5), g.class);
    screenClasses.put(Integer.valueOf(6), ah.class);
    screenClasses.put(Integer.valueOf(7), au.class);
    screenClasses.put(Integer.valueOf(8), k.class);
    screenClasses.put(Integer.valueOf(9), bz.class);
    screenClasses.put(Integer.valueOf(10), d.class);
    screenClasses.put(Integer.valueOf(11), cb.class);
    screenClasses.put(Integer.valueOf(12), bf.class);
    screenClasses.put(Integer.valueOf(13), m.class);
    screenClasses.put(Integer.valueOf(14), t.class);
    screenClasses.put(Integer.valueOf(15), ak.class);
    screenClasses.put(Integer.valueOf(16), i.class);
    screenClasses.put(Integer.valueOf(17), s.class);
    screenClasses.put(Integer.valueOf(18), ar.class);
    screenClasses.put(Integer.valueOf(19), bm.class);
    screenClasses.put(Integer.valueOf(20), cd.class);
    screenClasses.put(Integer.valueOf(21), bw.class);
    screenClasses.put(Integer.valueOf(22), ca.class);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (this.b != null)
      this.b.a(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (this.b != null)
      this.b.onConfigurationChanged(paramConfiguration);
    super.onConfigurationChanged(paramConfiguration);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.a = localBundle.getInt("screenType");
    if (this.a == -1000)
    {
      finish();
      return;
    }
    Class localClass = (Class)screenClasses.get(Integer.valueOf(this.a));
    if (localClass != null);
    try
    {
      this.b = ((u)localClass.newInstance());
      if (this.b != null)
      {
        this.b.c = this;
        this.b.onCreate(paramBundle);
        return;
      }
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
      finish();
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.b != null)
      this.b.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((this.b != null) && (this.b.onKeyDown(paramInt, paramKeyEvent)))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onPause()
  {
    super.onPause();
    if (this.b != null)
      this.b.onPause();
  }

  public void onResume()
  {
    super.onResume();
    if (this.b != null)
      this.b.onResume();
  }

  public void onStop()
  {
    super.onStop();
    if (this.b != null)
      this.b.onStop();
  }

  public void refresh()
  {
    if (this.b != null)
      runOnUiThread(new Runnable()
      {
        public void run()
        {
          SwarmMainActivity.a(SwarmMainActivity.this).refresh();
        }
      });
  }

  public void show(int paramInt)
  {
    Swarm.show(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmMainActivity
 * JD-Core Version:    0.6.2
 */