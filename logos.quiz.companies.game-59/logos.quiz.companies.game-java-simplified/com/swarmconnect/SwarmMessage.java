package com.swarmconnect;

import java.util.Date;

public class SwarmMessage
{
  public SwarmUser from;
  public int id;
  public String message;
  public long timestamp;

  public static void sendMessage(int paramInt, String paramString, SwarmMessageThread.SentMessageCB paramSentMessageCB)
  {
    ay localay = new ay();
    localay.toUserId = paramInt;
    localay.message = paramString;
    localay.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        ay localay = (ay)paramAnonymousAPICall;
        if (SwarmMessage.this != null)
          SwarmMessage.this.sentMessage(localay.newThread, localay.threadId);
      }

      public void requestFailed()
      {
        if (SwarmMessage.this != null)
          SwarmMessage.this.sendFailed();
      }
    };
    localay.run();
  }

  public Date getSentDate()
  {
    return new Date(1000L * this.timestamp);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmMessage
 * JD-Core Version:    0.6.2
 */