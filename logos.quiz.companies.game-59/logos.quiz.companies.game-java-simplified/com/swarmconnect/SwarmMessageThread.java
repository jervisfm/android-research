package com.swarmconnect;

import java.io.Serializable;
import java.util.List;

public class SwarmMessageThread
  implements Serializable
{
  private static final long serialVersionUID = -4205837524433303399L;
  public int id;
  public String lastMessage;
  public SwarmUser otherUser;
  public boolean viewed;

  public static void getAllThreads(GotThreadsCB paramGotThreadsCB)
  {
    bq localbq = new bq();
    localbq.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bq localbq = (bq)paramAnonymousAPICall;
        if (SwarmMessageThread.this != null)
          SwarmMessageThread.this.gotThreads(localbq.threads);
      }

      public void requestFailed()
      {
        if (SwarmMessageThread.this != null)
          SwarmMessageThread.this.gotThreads(null);
      }
    };
    localbq.run();
  }

  public static void getMessages(int paramInt1, int paramInt2, GotMessagesCB paramGotMessagesCB)
  {
    aj localaj = new aj();
    localaj.threadId = paramInt1;
    localaj.otherId = paramInt2;
    localaj.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        aj localaj = (aj)paramAnonymousAPICall;
        if (SwarmMessageThread.this != null)
          SwarmMessageThread.this.gotMessages(localaj.messages);
      }

      public void requestFailed()
      {
        if (SwarmMessageThread.this != null)
          SwarmMessageThread.this.gotMessages(null);
      }
    };
    localaj.run();
  }

  public static void getMessages(int paramInt, GotMessagesCB paramGotMessagesCB)
  {
    getMessages(paramInt, 0, paramGotMessagesCB);
  }

  public static void getNumNewMessages(GotNumMessages paramGotNumMessages)
  {
    bh localbh = new bh();
    localbh.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        if (SwarmMessageThread.this != null)
        {
          bh localbh = (bh)paramAnonymousAPICall;
          SwarmMessageThread.this.gotNumMessages(localbh.newMessages);
        }
      }

      public void requestFailed()
      {
        if (SwarmMessageThread.this != null)
          SwarmMessageThread.this.gotNumMessages(-1);
      }
    };
    localbh.run();
  }

  public void getMessages(GotMessagesCB paramGotMessagesCB)
  {
    getMessages(this.id, 0, paramGotMessagesCB);
  }

  public void markRead()
  {
    if (!this.viewed)
    {
      if (Swarm.user != null)
      {
        SwarmActiveUser localSwarmActiveUser = Swarm.user;
        localSwarmActiveUser.numNewMessages = (-1 + localSwarmActiveUser.numNewMessages);
      }
      this.viewed = true;
      n localn = new n();
      localn.threadId = this.id;
      localn.run();
    }
  }

  public void reply(String paramString, SentMessageCB paramSentMessageCB)
  {
    markRead();
    SwarmMessage.sendMessage(this.otherUser.userId, paramString, paramSentMessageCB);
  }

  public static abstract class GotMessagesCB
  {
    public abstract void gotMessages(List<SwarmMessage> paramList);
  }

  public static abstract class GotNumMessages
  {
    public abstract void gotNumMessages(int paramInt);
  }

  public static abstract class GotThreadsCB
  {
    public abstract void gotThreads(List<SwarmMessageThread> paramList);
  }

  public static abstract class SentMessageCB
  {
    public abstract void sendFailed();

    public abstract void sentMessage(boolean paramBoolean, int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmMessageThread
 * JD-Core Version:    0.6.2
 */