package com.swarmconnect;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import java.util.HashMap;

public class SwarmNotification
{
  private static final HashMap<NotificationType, Class<? extends SwarmNotification>> a = new HashMap();
  public int data;
  public int id;
  public NotificationType type;

  static
  {
    a.put(NotificationType.FRIEND, NotificationFriend.class);
    a.put(NotificationType.MESSAGE, NotificationMessage.class);
    a.put(NotificationType.ONLINE, NotificationOnline.class);
    a.put(NotificationType.ACHIEVEMENT, NotificationAchievement.class);
    a.put(NotificationType.CHALLENGE, NotificationChallenge.class);
    a.put(NotificationType.PURCHASE, NotificationPurchase.class);
    a.put(NotificationType.GOT_COINS, NotificationGotCoins.class);
    a.put(NotificationType.LEADERBOARD, NotificationLeaderboard.class);
  }

  // ERROR //
  protected static SwarmNotification b(int paramInt1, NotificationType paramNotificationType, int paramInt2)
  {
    // Byte code:
    //   0: getstatic 21	com/swarmconnect/SwarmNotification:a	Ljava/util/HashMap;
    //   3: aload_1
    //   4: invokevirtual 76	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   7: checkcast 78	java/lang/Class
    //   10: astore_3
    //   11: aload_3
    //   12: ifnull +45 -> 57
    //   15: aload_3
    //   16: invokevirtual 82	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   19: checkcast 2	com/swarmconnect/SwarmNotification
    //   22: astore 5
    //   24: aload 5
    //   26: iload_0
    //   27: aload_1
    //   28: iload_2
    //   29: invokevirtual 85	com/swarmconnect/SwarmNotification:a	(ILcom/swarmconnect/SwarmNotification$NotificationType;I)V
    //   32: aload 5
    //   34: areturn
    //   35: astore 4
    //   37: aconst_null
    //   38: astore 5
    //   40: aload 4
    //   42: astore 6
    //   44: aload 6
    //   46: invokevirtual 88	java/lang/Exception:printStackTrace	()V
    //   49: aload 5
    //   51: areturn
    //   52: astore 6
    //   54: goto -10 -> 44
    //   57: aconst_null
    //   58: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   15	24	35	java/lang/Exception
    //   24	32	52	java/lang/Exception
  }

  protected View a(Context paramContext)
  {
    try
    {
      GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13421773, -11184811 });
      localGradientDrawable.setShape(0);
      localGradientDrawable.setStroke(2, -13992777);
      localGradientDrawable.setCornerRadius(UiConf.dips(4.0F));
      float[] arrayOfFloat = new float[8];
      arrayOfFloat[0] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[1] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[2] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[3] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[4] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[5] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[6] = ((int)UiConf.dips(10.0F));
      arrayOfFloat[7] = ((int)UiConf.dips(10.0F));
      ShapeDrawable localShapeDrawable = new ShapeDrawable(new RoundRectShape(arrayOfFloat, null, null));
      localShapeDrawable.getPaint().setColor(-1442840576);
      localShapeDrawable.getPaint().setShadowLayer(3.5F, 3.0F, 3.0F, -1442840576);
      LayerDrawable localLayerDrawable = new LayerDrawable(new Drawable[] { new InsetDrawable(localShapeDrawable, (int)UiConf.dips(10.0F)), localGradientDrawable });
      LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams(-2, -2);
      localLayoutParams1.setMargins((int)UiConf.dips(8.0F), (int)UiConf.dips(8.0F), (int)UiConf.dips(8.0F), (int)UiConf.dips(8.0F));
      LinearLayout localLinearLayout1 = new LinearLayout(paramContext);
      localLinearLayout1.setOrientation(1);
      localLinearLayout1.setLayoutParams(localLayoutParams1);
      localLinearLayout1.setBackgroundDrawable(localLayerDrawable);
      localLinearLayout1.setPadding((int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F));
      localLinearLayout1.setGravity(16);
      LinearLayout localLinearLayout2 = new LinearLayout(paramContext);
      localLinearLayout2.setGravity(3);
      localLinearLayout2.setPadding((int)UiConf.dips(5.0F), (int)UiConf.dips(5.0F), (int)UiConf.dips(5.0F), (int)UiConf.dips(5.0F));
      ImageView localImageView1 = new ImageView(paramContext);
      localImageView1.setLayoutParams(new ViewGroup.LayoutParams(-2, (int)UiConf.dips(14.0F)));
      localImageView1.setScaleType(ImageView.ScaleType.FIT_START);
      int i = getLogoIconId(paramContext);
      if (i > 0)
      {
        localImageView1.setImageResource(i);
        localLinearLayout2.addView(localImageView1);
        localLinearLayout1.addView(localLinearLayout2);
      }
      LinearLayout localLinearLayout3 = new LinearLayout(paramContext);
      localLinearLayout3.setOrientation(0);
      localLinearLayout3.setPadding((int)UiConf.dips(10.0F), (int)UiConf.dips(1.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F));
      localLinearLayout3.setGravity(16);
      ImageView localImageView2 = new ImageView(paramContext);
      localImageView2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
      localImageView2.setId(18269491);
      int j = getIconId(paramContext);
      if (j > 0)
        localImageView2.setImageResource(j);
      localLinearLayout3.addView(localImageView2);
      LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(-2, -2);
      localLayoutParams2.setMargins((int)UiConf.dips(10.0F), 0, (int)UiConf.dips(5.0F), 0);
      LinearLayout localLinearLayout4 = new LinearLayout(paramContext);
      localLinearLayout4.setOrientation(1);
      localLinearLayout4.setLayoutParams(localLayoutParams2);
      localLinearLayout3.addView(localLinearLayout4);
      TextView localTextView1 = new TextView(paramContext);
      localTextView1.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
      localTextView1.setId(25683581);
      localTextView1.setTextColor(-1);
      localTextView1.setTypeface(Typeface.DEFAULT_BOLD);
      localTextView1.setText(getTitle());
      localLinearLayout4.addView(localTextView1);
      TextView localTextView2 = new TextView(paramContext);
      localTextView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0F));
      localTextView2.setId(1726481);
      localTextView2.setTextColor(-2236963);
      localTextView2.setText(getMessage());
      localLinearLayout4.addView(localTextView2);
      localLinearLayout1.addView(localLinearLayout3);
      return localLinearLayout1;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  protected void a()
  {
  }

  protected void a(int paramInt1, NotificationType paramNotificationType, int paramInt2)
  {
    this.id = paramInt1;
    this.type = paramNotificationType;
    this.data = paramInt2;
    a();
  }

  public int getIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_icon", null, str);
  }

  public int getLogoIconId(Context paramContext)
  {
    String str = paramContext.getApplicationContext().getPackageName();
    return paramContext.getApplicationContext().getResources().getIdentifier("@drawable/swarm_logo", null, str);
  }

  public String getMessage()
  {
    return "";
  }

  public String getTitle()
  {
    return "";
  }

  public View getView(Context paramContext)
  {
    return a(paramContext);
  }

  public static enum NotificationType
  {
    static
    {
      ACHIEVEMENT = new NotificationType("ACHIEVEMENT", 3);
      CHALLENGE = new NotificationType("CHALLENGE", 4);
      PURCHASE = new NotificationType("PURCHASE", 5);
      GOT_COINS = new NotificationType("GOT_COINS", 6);
      LEADERBOARD = new NotificationType("LEADERBOARD", 7);
      NotificationType[] arrayOfNotificationType = new NotificationType[8];
      arrayOfNotificationType[0] = FRIEND;
      arrayOfNotificationType[1] = MESSAGE;
      arrayOfNotificationType[2] = ONLINE;
      arrayOfNotificationType[3] = ACHIEVEMENT;
      arrayOfNotificationType[4] = CHALLENGE;
      arrayOfNotificationType[5] = PURCHASE;
      arrayOfNotificationType[6] = GOT_COINS;
      arrayOfNotificationType[7] = LEADERBOARD;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmNotification
 * JD-Core Version:    0.6.2
 */