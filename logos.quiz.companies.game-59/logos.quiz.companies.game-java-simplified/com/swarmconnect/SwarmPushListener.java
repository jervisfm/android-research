package com.swarmconnect;

import android.content.Context;
import java.util.Iterator;
import java.util.List;

public class SwarmPushListener extends PushReceiver
{
  protected static void a()
  {
    if ((Swarm.user != null) && (Swarm.user.userId > 0) && (Swarm.b != null))
    {
      ci localci = new ci();
      localci.cb = new APICall.APICallback()
      {
        public void gotAPI(APICall paramAnonymousAPICall)
        {
          ci localci = (ci)paramAnonymousAPICall;
          Iterator localIterator;
          if ((localci.notifications != null) && (localci.notifications.size() > 0))
            localIterator = localci.notifications.iterator();
          while (true)
          {
            if (!localIterator.hasNext())
              return;
            SwarmNotification localSwarmNotification = (SwarmNotification)localIterator.next();
            SwarmNotification.b(localSwarmNotification.id, localSwarmNotification.type, localSwarmNotification.data);
          }
        }
      };
      localci.run();
    }
  }

  public void pushReceived(Context paramContext, String paramString)
  {
    a();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmPushListener
 * JD-Core Version:    0.6.2
 */