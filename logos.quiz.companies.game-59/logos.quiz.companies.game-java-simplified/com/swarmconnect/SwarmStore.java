package com.swarmconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SwarmStore
  implements Serializable
{
  public List<SwarmStoreCategory> categories = new ArrayList();

  public static void getListingById(int paramInt, final GotStoreListingCB paramGotStoreListingCB)
  {
    if (paramGotStoreListingCB == null)
      return;
    getStore(new GotSwarmStoreCB()
    {
      public void gotStore(SwarmStore paramAnonymousSwarmStore)
      {
        if ((paramAnonymousSwarmStore != null) && (paramAnonymousSwarmStore.categories != null));
        SwarmStoreListing localSwarmStoreListing;
        do
        {
          Iterator localIterator1 = paramAnonymousSwarmStore.categories.iterator();
          Iterator localIterator2;
          while (!localIterator2.hasNext())
          {
            SwarmStoreCategory localSwarmStoreCategory;
            do
            {
              if (!localIterator1.hasNext())
              {
                paramGotStoreListingCB.gotStoreListing(null);
                return;
              }
              localSwarmStoreCategory = (SwarmStoreCategory)localIterator1.next();
            }
            while ((localSwarmStoreCategory == null) || (localSwarmStoreCategory.listings == null));
            localIterator2 = localSwarmStoreCategory.listings.iterator();
          }
          localSwarmStoreListing = (SwarmStoreListing)localIterator2.next();
        }
        while ((localSwarmStoreListing == null) || (localSwarmStoreListing.id != this.a));
        paramGotStoreListingCB.gotStoreListing(localSwarmStoreListing);
      }
    });
  }

  public static void getStore(GotSwarmStoreCB paramGotSwarmStoreCB)
  {
    cf localcf = new cf();
    localcf.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        cf localcf = (cf)paramAnonymousAPICall;
        if (SwarmStore.this != null)
          SwarmStore.this.gotStore(localcf.store);
      }

      public void requestFailed()
      {
        if (SwarmStore.this != null)
          SwarmStore.this.gotStore(null);
      }
    };
    localcf.run();
  }

  public static abstract class GotStoreListingCB
  {
    public abstract void gotStoreListing(SwarmStoreListing paramSwarmStoreListing);
  }

  public static abstract class GotSwarmStoreCB
  {
    public abstract void gotStore(SwarmStore paramSwarmStore);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmStore
 * JD-Core Version:    0.6.2
 */