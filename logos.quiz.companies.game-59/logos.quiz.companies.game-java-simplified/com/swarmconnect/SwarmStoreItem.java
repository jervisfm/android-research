package com.swarmconnect;

import java.io.Serializable;

public class SwarmStoreItem
  implements Serializable
{
  public boolean consumable;
  public String description;
  public int id;
  public String name;
  public String payload;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmStoreItem
 * JD-Core Version:    0.6.2
 */