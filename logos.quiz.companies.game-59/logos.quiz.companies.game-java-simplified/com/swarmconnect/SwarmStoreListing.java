package com.swarmconnect;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.swarmconnect.ui.AsyncImageView;
import com.swarmconnect.ui.UiConf;
import java.io.Serializable;
import java.util.HashMap;

public class SwarmStoreListing
  implements Serializable
{
  public static final int PURCHASE_CANCELED = -1000;
  public static final int PURCHASE_FAILED_ALREADY_PURCHASED = -1006;
  public static final int PURCHASE_FAILED_INVALID_ITEM = -1003;
  public static final int PURCHASE_FAILED_INVALID_STORE = -1002;
  public static final int PURCHASE_FAILED_NO_COINS = -1001;
  public static final int PURCHASE_FAILED_NO_INTERNET = -1005;
  public static final int PURCHASE_FAILED_OTHER = -1004;
  public static final int PURCHASE_SUCCESS = 1;
  public int id;
  public String imageUrl;
  public SwarmStoreItem item;
  public int orderId;
  public int price;
  public int quantity;
  public String title;

  static
  {
    ax.a.put(Integer.valueOf(-1000), "Purchase Canceled");
    ax.a.put(Integer.valueOf(-1001), "Purchase Failed: Not enough coins");
    ax.a.put(Integer.valueOf(-1002), "Purchase Failed: Invalid Swarm store");
    ax.a.put(Integer.valueOf(-1003), "Purchase Failed: Invalid item");
    ax.a.put(Integer.valueOf(-1004), "Purchase Failed");
    ax.a.put(Integer.valueOf(-1005), "Purchase Failed: Network unavailable");
    ax.a.put(Integer.valueOf(-1006), "Purchase Failed: You already own that item");
  }

  private void a(Context paramContext, final ItemPurchaseCB paramItemPurchaseCB)
  {
    Swarm.a("Purchasing " + this.title);
    bd localbd = new bd();
    localbd.listingId = this.id;
    localbd.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        Swarm.d();
        bd localbd = (bd)paramAnonymousAPICall;
        if (paramItemPurchaseCB != null)
        {
          if (localbd.purchaseStatus == 1)
          {
            Swarm.a(new NotificationPurchase(SwarmStoreListing.this));
            paramItemPurchaseCB.purchaseSuccess();
          }
        }
        else
          return;
        Swarm.b((String)ax.a.get(Integer.valueOf(localbd.purchaseStatus)));
        paramItemPurchaseCB.purchaseFailed(localbd.purchaseStatus);
      }

      public void requestFailed()
      {
        Swarm.d();
        Swarm.b((String)ax.a.get(Integer.valueOf(-1005)));
        if (paramItemPurchaseCB != null)
          paramItemPurchaseCB.purchaseFailed(-1005);
      }
    };
    localbd.run();
  }

  public void purchase(final Context paramContext, final ItemPurchaseCB paramItemPurchaseCB)
  {
    if (!Swarm.isOnline())
    {
      if (paramItemPurchaseCB != null)
        paramItemPurchaseCB.purchaseFailed(-1005);
      return;
    }
    final Dialog localDialog = new Dialog(paramContext);
    localDialog.requestWindowFeature(1);
    localDialog.setContentView(u.getResource("@layout/swarm_purchase_popup", paramContext));
    localDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
    {
      public void onCancel(DialogInterface paramAnonymousDialogInterface)
      {
        if (paramItemPurchaseCB != null)
          paramItemPurchaseCB.purchaseFailed(-1000);
      }
    });
    ((TextView)localDialog.findViewById(u.getResource("@id/purchase", paramContext))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SwarmStoreListing.a(SwarmStoreListing.this, paramContext, paramItemPurchaseCB);
        localDialog.dismiss();
      }
    });
    ((TextView)localDialog.findViewById(u.getResource("@id/cancel", paramContext))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        localDialog.cancel();
      }
    });
    ((TextView)localDialog.findViewById(u.getResource("@id/title", paramContext))).setText(this.title);
    ((TextView)localDialog.findViewById(u.getResource("@id/desc", paramContext))).setText("Purchase for " + this.price + " coins");
    ((TextView)localDialog.findViewById(u.getResource("@id/coins", paramContext))).setText(this.price);
    if ((this.imageUrl != null) && (this.imageUrl.length() > 0))
      ((AsyncImageView)localDialog.findViewById(u.getResource("@id/image", paramContext))).getUrl(this.imageUrl);
    ((TextView)localDialog.findViewById(u.getResource("@id/purchase", paramContext))).setBackgroundDrawable(UiConf.purchasePopupBuyButton());
    ((TextView)localDialog.findViewById(u.getResource("@id/cancel", paramContext))).setBackgroundDrawable(UiConf.purchasePopupCancelButton());
    ((LinearLayout)localDialog.findViewById(u.getResource("@id/swarm_purchase_popup", paramContext))).setBackgroundDrawable(UiConf.loadingPopupBackground());
    ((RelativeLayout)localDialog.findViewById(u.getResource("@id/popup_header", paramContext))).setBackgroundDrawable(UiConf.customGradient(-13421773, -13421773));
    ((RelativeLayout)localDialog.findViewById(u.getResource("@id/popup_top_half", paramContext))).setBackgroundDrawable(UiConf.customGradient(-7829368, -5592406));
    ((LinearLayout)localDialog.findViewById(u.getResource("@id/popup_bottom_half", paramContext))).setBackgroundDrawable(UiConf.customGradient(-13421773, -13421773));
    final TextView localTextView1 = (TextView)localDialog.findViewById(u.getResource("@id/desc", paramContext));
    final TextView localTextView2 = (TextView)localDialog.findViewById(u.getResource("@id/purchase", paramContext));
    Swarm.user.getCoins(new SwarmActiveUser.GotUserCoinsCB()
    {
      public void gotCoins(int paramAnonymousInt)
      {
        if (paramAnonymousInt < SwarmStoreListing.this.price)
        {
          localTextView1.setText("You need more coins to purchase this. Tap below to get more coins!");
          localTextView2.setText("Get Coins");
          localTextView2.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymous2View)
            {
              Swarm.showGetCoins();
              this.b.cancel();
            }
          });
        }
      }
    });
    localDialog.show();
  }

  public static abstract class ItemPurchaseCB
  {
    public abstract void purchaseFailed(int paramInt);

    public abstract void purchaseSuccess();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmStoreListing
 * JD-Core Version:    0.6.2
 */