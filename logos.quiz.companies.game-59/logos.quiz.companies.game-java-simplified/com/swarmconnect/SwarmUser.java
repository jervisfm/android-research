package com.swarmconnect;

import java.io.Serializable;

public class SwarmUser
  implements Serializable
{
  private static final long serialVersionUID = -4945801927959578594L;
  public int points;
  public int userId;
  public String username;

  public SwarmUser()
  {
  }

  public SwarmUser(int paramInt)
  {
    this.userId = paramInt;
  }

  protected SwarmUser(SwarmUser paramSwarmUser)
  {
    this.userId = paramSwarmUser.userId;
    this.username = paramSwarmUser.username;
    this.points = paramSwarmUser.points;
  }

  protected static void a(int paramInt, String paramString, GotUserCB paramGotUserCB)
  {
    a locala = new a();
    locala.searchId = paramInt;
    locala.searchUsername = paramString;
    locala.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        a locala = (a)paramAnonymousAPICall;
        if (SwarmUser.this != null)
          SwarmUser.this.gotUser(locala.result);
      }
    };
    locala.run();
  }

  public static void getUser(int paramInt, GotUserCB paramGotUserCB)
  {
    a(paramInt, "", paramGotUserCB);
  }

  public static void getUser(String paramString, GotUserCB paramGotUserCB)
  {
    a(0, paramString, paramGotUserCB);
  }

  public void getOnlineStatus(final GotUserStatusCB paramGotUserStatusCB)
  {
    z localz = new z();
    localz.id = this.userId;
    localz.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        if (paramGotUserStatusCB != null)
        {
          z localz = (z)paramAnonymousAPICall;
          paramGotUserStatusCB.gotUserStatus(localz.app, localz.online);
        }
      }
    };
    localz.run();
  }

  public static abstract class GotUserCB
  {
    public abstract void gotUser(SwarmUser paramSwarmUser);
  }

  public static abstract class GotUserStatusCB
  {
    public abstract void gotUserStatus(SwarmApplication paramSwarmApplication, boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmUser
 * JD-Core Version:    0.6.2
 */