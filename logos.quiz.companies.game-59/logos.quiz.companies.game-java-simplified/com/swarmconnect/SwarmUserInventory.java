package com.swarmconnect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SwarmUserInventory
{
  private HashMap<Integer, SwarmStoreItem> a = new HashMap();
  private HashMap<Integer, Integer> b = new HashMap();

  protected static SwarmUserInventory a(SwarmActiveUser.GotInventoryCB paramGotInventoryCB)
  {
    SwarmUserInventory localSwarmUserInventory = new SwarmUserInventory();
    localSwarmUserInventory.refresh(paramGotInventoryCB);
    return localSwarmUserInventory;
  }

  protected SwarmUserInventory a(List<GetInventoryAPI.SwarmInventoryItemTransport> paramList)
  {
    Iterator localIterator;
    if (paramList != null)
      localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return this;
      GetInventoryAPI.SwarmInventoryItemTransport localSwarmInventoryItemTransport = (GetInventoryAPI.SwarmInventoryItemTransport)localIterator.next();
      this.a.put(Integer.valueOf(localSwarmInventoryItemTransport.item.id), localSwarmInventoryItemTransport.item);
      this.b.put(Integer.valueOf(localSwarmInventoryItemTransport.item.id), Integer.valueOf(localSwarmInventoryItemTransport.quantity));
    }
  }

  protected void a(SwarmStoreListing paramSwarmStoreListing)
  {
    if ((paramSwarmStoreListing != null) && (paramSwarmStoreListing.item != null))
    {
      int i = getItemQuantity(paramSwarmStoreListing.item) + paramSwarmStoreListing.quantity;
      if (!this.a.containsKey(Integer.valueOf(paramSwarmStoreListing.item.id)))
        this.a.put(Integer.valueOf(paramSwarmStoreListing.item.id), paramSwarmStoreListing.item);
      this.b.put(Integer.valueOf(paramSwarmStoreListing.item.id), Integer.valueOf(i));
    }
  }

  public void consumeItem(int paramInt)
  {
    consumeItem((SwarmStoreItem)this.a.get(Integer.valueOf(paramInt)));
  }

  public void consumeItem(SwarmStoreItem paramSwarmStoreItem)
  {
    if (paramSwarmStoreItem == null);
    int i;
    do
    {
      do
        return;
      while (!paramSwarmStoreItem.consumable);
      i = getItemQuantity(paramSwarmStoreItem);
    }
    while (i <= 0);
    aw localaw = new aw();
    localaw.itemId = paramSwarmStoreItem.id;
    localaw.run();
    int j = i - 1;
    if (j > 0)
    {
      this.b.put(Integer.valueOf(paramSwarmStoreItem.id), Integer.valueOf(j));
      return;
    }
    this.b.remove(Integer.valueOf(paramSwarmStoreItem.id));
    this.a.remove(Integer.valueOf(paramSwarmStoreItem.id));
  }

  public boolean containsItem(int paramInt)
  {
    return (this.b.containsKey(Integer.valueOf(paramInt))) && (((Integer)this.b.get(Integer.valueOf(paramInt))).intValue() > 0);
  }

  public boolean containsItem(SwarmStoreItem paramSwarmStoreItem)
  {
    if (paramSwarmStoreItem == null)
      return false;
    return containsItem(paramSwarmStoreItem.id);
  }

  public SwarmStoreItem getItem(int paramInt)
  {
    return (SwarmStoreItem)this.a.get(Integer.valueOf(paramInt));
  }

  public List<SwarmStoreItem> getItemList()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(this.a.values());
    return localArrayList;
  }

  public int getItemQuantity(int paramInt)
  {
    if (this.b.containsKey(Integer.valueOf(paramInt)))
      return ((Integer)this.b.get(Integer.valueOf(paramInt))).intValue();
    return 0;
  }

  public int getItemQuantity(SwarmStoreItem paramSwarmStoreItem)
  {
    if (paramSwarmStoreItem == null)
      return 0;
    return getItemQuantity(paramSwarmStoreItem.id);
  }

  public void refresh(final SwarmActiveUser.GotInventoryCB paramGotInventoryCB)
  {
    GetInventoryAPI localGetInventoryAPI = new GetInventoryAPI();
    localGetInventoryAPI.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        GetInventoryAPI localGetInventoryAPI = (GetInventoryAPI)paramAnonymousAPICall;
        if (paramGotInventoryCB != null)
        {
          SwarmUserInventory.this.a(localGetInventoryAPI.inventory);
          paramGotInventoryCB.gotInventory(SwarmUserInventory.this);
        }
      }

      public void requestFailed()
      {
        if (paramGotInventoryCB != null)
          paramGotInventoryCB.gotInventory(null);
      }
    };
    localGetInventoryAPI.run();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.SwarmUserInventory
 * JD-Core Version:    0.6.2
 */