package com.swarmconnect;

import com.swarmconnect.packets.Packets;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

class ac
{
  protected boolean a = false;
  private ad b;
  private String c;
  private int d;
  private final Socket e = new Socket();
  private Timer f;
  private Object g = new Object();
  private boolean h = false;
  private Throwable i = null;
  private ae j;
  private Thread k;

  protected ac(String paramString, int paramInt, ad paramad)
  {
    this.c = paramString;
    this.d = paramInt;
    this.b = paramad;
  }

  protected void a()
    throws IOException
  {
    this.e.connect(new InetSocketAddress(this.c, this.d), 30000);
    this.e.setSoTimeout(0);
    this.j = new ae(this.e.getInputStream(), this.e.getOutputStream());
    this.k = new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          ac.this.b();
          return;
        }
        catch (IOException localIOException)
        {
          ac.this.a(localIOException);
        }
      }
    });
    this.k.setDaemon(true);
    this.k.start();
    if (this.b != null)
    {
      this.a = true;
      this.e.setKeepAlive(true);
      this.e.setSoTimeout(600000);
      this.e.setTcpNoDelay(true);
      if (this.f == null)
      {
        this.f = new Timer();
        this.f.schedule(new TimerTask()
        {
          public void run()
          {
            ac.this.a(Packets.pingPayload);
          }
        }
        , 450000L, 900000L);
      }
      this.b.onConnected(this);
    }
  }

  protected void a(Throwable paramThrowable)
  {
    if ((this.e != null) && (!this.e.isClosed()))
      this.i = paramThrowable;
    try
    {
      this.e.close();
      label29: synchronized (this.g)
      {
        this.h = true;
        this.g.notifyAll();
        if (this.f == null);
      }
      try
      {
        this.f.cancel();
        this.f.purge();
        label72: this.f = null;
        if (this.b != null)
          this.b.onConnectionLost(this, this.i);
        this.a = false;
        return;
        localObject2 = finally;
        throw localObject2;
      }
      catch (Exception localException)
      {
        break label72;
      }
    }
    catch (IOException localIOException)
    {
      break label29;
    }
  }

  protected boolean a(byte[] paramArrayOfByte)
  {
    try
    {
      b(paramArrayOfByte);
      return true;
    }
    catch (Exception localException)
    {
      a(new Throwable("Error in ConnectionManager.sendPacket: " + localException.getCause()));
    }
    return false;
  }

  protected void b()
    throws IOException
  {
    while (true)
    {
      byte[] arrayOfByte = this.j.a();
      if (arrayOfByte == null)
        throw new IOException("Peer sent DISCONNECT");
      if ((0xFF & arrayOfByte[0]) == 0)
        throw new IOException("Server sent Disconnect Packet");
      if (this.b != null)
        this.b.onPacketReceived(0xFF & arrayOfByte[0], arrayOfByte);
    }
  }

  // ERROR //
  protected void b(byte[] paramArrayOfByte)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 39	com/swarmconnect/ac:g	Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 41	com/swarmconnect/ac:h	Z
    //   11: ifne +10 -> 21
    //   14: aload_0
    //   15: getfield 79	com/swarmconnect/ac:j	Lcom/swarmconnect/ae;
    //   18: ifnonnull +28 -> 46
    //   21: new 51	java/io/IOException
    //   24: dup
    //   25: ldc 192
    //   27: invokespecial 184	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   30: aload_0
    //   31: getfield 43	com/swarmconnect/ac:i	Ljava/lang/Throwable;
    //   34: invokevirtual 196	java/io/IOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   37: checkcast 51	java/io/IOException
    //   40: athrow
    //   41: astore_3
    //   42: aload_2
    //   43: monitorexit
    //   44: aload_3
    //   45: athrow
    //   46: aload_0
    //   47: getfield 79	com/swarmconnect/ac:j	Lcom/swarmconnect/ae;
    //   50: aload_1
    //   51: invokevirtual 198	com/swarmconnect/ae:a	([B)V
    //   54: aload_2
    //   55: monitorexit
    //   56: return
    //   57: astore 4
    //   59: aload_0
    //   60: aload 4
    //   62: invokevirtual 178	com/swarmconnect/ac:a	(Ljava/lang/Throwable;)V
    //   65: aload 4
    //   67: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   7	21	41	finally
    //   21	41	41	finally
    //   42	44	41	finally
    //   46	54	41	finally
    //   54	56	41	finally
    //   59	68	41	finally
    //   46	54	57	java/io/IOException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ac
 * JD-Core Version:    0.6.2
 */