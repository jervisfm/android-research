package com.swarmconnect;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build.VERSION;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class af
{
  protected static void a(Context paramContext)
  {
    AsyncHttp.a("http://swarm.io/notifications.php?clientid=" + SwarmIO.a(paramContext) + "&app_id=" + SwarmIO.b(paramContext) + "&auth=" + SwarmIO.c(paramContext), new AsyncHttp.AsyncCB()
    {
      public void gotURL(String paramAnonymousString)
      {
        if ((paramAnonymousString != null) && (paramAnonymousString.length() > 0))
          try
          {
            JSONObject localJSONObject = new JSONObject(paramAnonymousString);
            if (localJSONObject != null)
            {
              JSONArray localJSONArray = localJSONObject.getJSONArray("notifications");
              if ((localJSONArray != null) && (SwarmIO.getPushEnabled(af.this)))
                for (int i = 0; ; i++)
                {
                  if (i >= localJSONArray.length())
                    return;
                  af.a(af.this, new JSONObject(localJSONArray.getString(i)));
                }
            }
          }
          catch (Exception localException)
          {
          }
      }
    });
  }

  private static void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    NotificationService.a("NotificationManager.showNotification()...");
    if (Integer.parseInt(Build.VERSION.SDK) >= 4)
      new a(null).a(paramContext, paramString1, paramString2, paramString3);
  }

  private static void b(Context paramContext, JSONObject paramJSONObject)
    throws JSONException
  {
    String str1 = paramJSONObject.optString("type");
    NotificationService.a("SwarmIONotificationManager.handleNotification(), type: " + str1);
    if (str1.equalsIgnoreCase("tray"))
    {
      JSONObject localJSONObject = paramJSONObject.getJSONObject("data");
      String str4 = localJSONObject.optString("title");
      String str5 = localJSONObject.optString("description");
      String str6 = localJSONObject.optString("action");
      String str7 = null;
      if (str6 != null)
      {
        boolean bool = str6.equalsIgnoreCase("url");
        str7 = null;
        if (bool)
          str7 = localJSONObject.optString("url");
      }
      if ((str4 != null) && (str4.length() > 0))
        a(paramContext, str4, str5, str7);
    }
    while (true)
    {
      return;
      if (!str1.equalsIgnoreCase("custom"))
        continue;
      String str2 = paramJSONObject.optString("data");
      String str3 = SwarmIO.d(paramContext);
      if (str3 == null)
        break;
      try
      {
        Class localClass2 = NotificationService.class.getClassLoader().loadClass(str3);
        localClass1 = localClass2;
        if (localClass1 == null)
          continue;
        NotificationService.a("Sending io.swarm.CUSTOM_PUSH_RECEIVED");
        Intent localIntent = new Intent("com.swarmconnect.CUSTOM_PUSH_RECEIVED");
        localIntent.putExtra("data", str2);
        localIntent.putExtra("package", paramContext.getPackageName());
        paramContext.getApplicationContext().sendBroadcast(localIntent);
        return;
      }
      catch (Exception localException)
      {
        while (true)
        {
          localException.printStackTrace();
          Class localClass1 = null;
        }
      }
    }
  }

  private static class a
  {
    protected void a(Context paramContext, String paramString1, String paramString2, String paramString3)
    {
      NotificationManager localNotificationManager = (NotificationManager)paramContext.getSystemService("notification");
      Notification localNotification = new Notification(paramContext.getApplicationInfo().icon, paramString1, System.currentTimeMillis());
      localNotification.flags = (0x10 | localNotification.flags);
      Intent localIntent;
      if ((paramString3 != null) && (paramString3.length() > 0) && (paramString3.startsWith("http")))
      {
        localIntent = new Intent("android.intent.action.VIEW");
        localIntent.setData(Uri.parse(paramString3));
      }
      while (true)
      {
        PendingIntent localPendingIntent = PendingIntent.getActivity(paramContext, 0, localIntent, 0);
        localNotification.setLatestEventInfo(paramContext.getApplicationContext(), paramString1, paramString2, localPendingIntent);
        localNotificationManager.notify(1283761, localNotification);
        return;
        localIntent = paramContext.getPackageManager().getLaunchIntentForPackage(paramContext.getPackageName());
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.af
 * JD-Core Version:    0.6.2
 */