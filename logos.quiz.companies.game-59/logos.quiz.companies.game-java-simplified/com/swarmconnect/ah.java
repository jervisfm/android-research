package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;

class ah extends u
{
  private a l = new a(null);
  private List<SwarmStoreItem> m = null;
  private TextView n;

  private void e()
  {
    b();
    Swarm.user.getInventory(new SwarmActiveUser.GotInventoryCB()
    {
      public void gotInventory(SwarmUserInventory paramAnonymousSwarmUserInventory)
      {
        ah.this.c();
        ah.a(ah.this, paramAnonymousSwarmUserInventory.getItemList());
        ah.c(ah.this).notifyDataSetChanged();
      }
    });
    Swarm.user.getCoins(new SwarmActiveUser.GotUserCoinsCB()
    {
      public void gotCoins(int paramAnonymousInt)
      {
        ah.d(ah.this).setText("Coins: " + paramAnonymousInt);
      }
    });
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_inventory"));
    this.n = ((TextView)a(a("@id/coins")));
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
      }
    });
    super.onCreate(paramBundle);
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        ah.b(ah.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (ah.a(ah.this) == null)
        return 0;
      return ah.a(ah.this).size();
    }

    public SwarmStoreItem getItem(int paramInt)
    {
      if ((ah.a(ah.this) == null) || (paramInt > ah.a(ah.this).size()))
        return null;
      return (SwarmStoreItem)ah.a(ah.this).get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(ah.this.d(), ah.this.a("@layout/swarm_store_row"), null);
      SwarmStoreItem localSwarmStoreItem = getItem(paramInt);
      if (localSwarmStoreItem != null)
        if (paramInt % 2 != 0)
          break label79;
      label79: for (int i = 16777215; ; i = -1996488705)
      {
        paramView.setBackgroundColor(i);
        ((TextView)paramView.findViewById(ah.this.a("@id/name"))).setText(localSwarmStoreItem.name);
        return paramView;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ah
 * JD-Core Version:    0.6.2
 */