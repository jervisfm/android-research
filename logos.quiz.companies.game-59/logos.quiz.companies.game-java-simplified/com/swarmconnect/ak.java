package com.swarmconnect;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;

class ak extends ba
{
  private EditText m;
  private EditText n;
  private EditText o;
  private EditText p;
  private TextView q;
  private TextView r;
  private TextView s;
  private TextView t;

  private void e()
  {
    this.q.setVisibility(4);
    this.q.setText("");
    this.r.setVisibility(4);
    this.r.setText("");
    this.s.setVisibility(4);
    this.s.setText("");
    this.t.setVisibility(4);
    this.t.setText("");
  }

  private void f()
  {
    e();
    String str1 = this.m.getText().toString();
    String str2 = this.n.getText().toString();
    String str3 = this.o.getText().toString();
    String str4 = this.p.getText().toString();
    b();
    o localo = new o();
    localo.username = str1;
    localo.password = str2;
    localo.confirm = str3;
    localo.email = str4;
    localo.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        ak.this.a(new Runnable()
        {
          public void run()
          {
            ak.this.c();
            o localo = (o)paramAnonymousAPICall;
            if (localo.user != null)
            {
              Swarm.a(localo.user, localo.auth);
              ak.a();
              return;
            }
            if (localo.usernameError.length() > 0)
            {
              ak.b(ak.this).setVisibility(0);
              ak.b(ak.this).setText(localo.usernameError);
              if (localo.passwordError.length() <= 0)
                break label237;
              ak.c(ak.this).setVisibility(0);
              ak.c(ak.this).setText(localo.passwordError);
              label122: if (localo.confirmError.length() <= 0)
                break label269;
              ak.d(ak.this).setVisibility(0);
              ak.d(ak.this).setText(localo.confirmError);
            }
            while (true)
            {
              if (localo.emailError.length() <= 0)
                break label301;
              ak.e(ak.this).setVisibility(0);
              ak.e(ak.this).setText(localo.emailError);
              return;
              ak.b(ak.this).setVisibility(4);
              ak.b(ak.this).setText("");
              break;
              label237: ak.c(ak.this).setVisibility(4);
              ak.c(ak.this).setText("");
              break label122;
              label269: ak.d(ak.this).setVisibility(4);
              ak.d(ak.this).setText("");
            }
            label301: ak.e(ak.this).setVisibility(4);
            ak.e(ak.this).setText("");
          }
        });
      }

      public void requestFailed()
      {
        ak.this.c();
        Swarm.a();
        ak.a();
      }
    };
    localo.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_upgrade_guest"));
    this.m = ((EditText)a(a("@id/username")));
    this.n = ((EditText)a(a("@id/password")));
    this.o = ((EditText)a(a("@id/confirm")));
    this.p = ((EditText)a(a("@id/email")));
    this.q = ((TextView)a(a("@id/username_error")));
    this.q.setTextColor(-7398867);
    this.r = ((TextView)a(a("@id/password_error")));
    this.r.setTextColor(-7398867);
    this.s = ((TextView)a(a("@id/confirm_error")));
    this.s.setTextColor(-7398867);
    this.t = ((TextView)a(a("@id/email_error")));
    this.t.setTextColor(-7398867);
    ((Button)a(a("@id/create"))).setBackgroundDrawable(UiConf.blueButtonBackground());
    ((Button)a(a("@id/create"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ak.a(ak.this);
      }
    });
    super.onCreate(paramBundle);
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ak
 * JD-Core Version:    0.6.2
 */