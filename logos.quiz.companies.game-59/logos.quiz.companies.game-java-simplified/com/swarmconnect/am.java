package com.swarmconnect;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

class am
{
  private static x c;
  private static cm d = null;
  public static String featuredAppURLParams;
  final String a = "Featured App";
  private al b = null;
  private Context e;
  private int f = 5;

  public am(Context paramContext)
  {
    this.e = paramContext;
    d = new cm();
  }

  private boolean a(String paramString)
  {
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    try
    {
      ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes("UTF-8"));
      Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localByteArrayInputStream);
      this.b.cost = av.getNodeTrimValue(localDocument.getElementsByTagName("Cost"));
      String str = av.getNodeTrimValue(localDocument.getElementsByTagName("Amount"));
      if (str != null)
        this.b.amount = Integer.parseInt(str);
      this.b.description = av.getNodeTrimValue(localDocument.getElementsByTagName("Description"));
      this.b.iconURL = av.getNodeTrimValue(localDocument.getElementsByTagName("IconURL"));
      this.b.name = av.getNodeTrimValue(localDocument.getElementsByTagName("Name"));
      this.b.redirectURL = av.getNodeTrimValue(localDocument.getElementsByTagName("RedirectURL"));
      this.b.storeID = av.getNodeTrimValue(localDocument.getElementsByTagName("StoreID"));
      this.b.fullScreenAdURL = av.getNodeTrimValue(localDocument.getElementsByTagName("FullScreenAdURL"));
      bv.i("Featured App", "cost: " + this.b.cost);
      bv.i("Featured App", "amount: " + this.b.amount);
      bv.i("Featured App", "description: " + this.b.description);
      bv.i("Featured App", "iconURL: " + this.b.iconURL);
      bv.i("Featured App", "name: " + this.b.name);
      bv.i("Featured App", "redirectURL: " + this.b.redirectURL);
      bv.i("Featured App", "storeID: " + this.b.storeID);
      bv.i("Featured App", "fullScreenAdURL: " + this.b.fullScreenAdURL);
      if (this.b.fullScreenAdURL != null)
      {
        int i = this.b.fullScreenAdURL.length();
        if (i != 0)
          break label503;
      }
      label503: for (bool = false; ; bool = true)
      {
        if (!bool)
          break label555;
        if (b(this.b.storeID) >= this.f)
          break;
        c.getFeaturedAppResponse(this.b);
        if (!bj.getAppID().equals(this.b.storeID))
          c(this.b.storeID);
        return bool;
      }
    }
    catch (Exception localException)
    {
      boolean bool;
      while (true)
      {
        bv.e("Featured App", "Error parsing XML: " + localException.toString());
        bool = false;
      }
      c.getFeaturedAppResponseFailed("Featured App to display has exceeded display count");
      return bool;
    }
    label555: c.getFeaturedAppResponseFailed("Failed to parse XML file from response");
    return true;
  }

  private int b(String paramString)
  {
    int i = this.e.getSharedPreferences("TapjoyFeaturedAppPrefs", 0).getInt(paramString, 0);
    bv.i("Featured App", "getDisplayCount: " + i + ", storeID: " + paramString);
    return i;
  }

  private void c(String paramString)
  {
    SharedPreferences localSharedPreferences = this.e.getSharedPreferences("TapjoyFeaturedAppPrefs", 0);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    int i = 1 + localSharedPreferences.getInt(paramString, 0);
    bv.i("Featured App", "incrementDisplayCount: " + i + ", storeID: " + paramString);
    localEditor.putInt(paramString, i);
    localEditor.commit();
  }

  public void getFeaturedApp(x paramx)
  {
    bv.i("Featured App", "Getting Featured App");
    c = paramx;
    this.b = new al();
    featuredAppURLParams = bj.getURLParams();
    featuredAppURLParams = featuredAppURLParams + "&publisher_user_id=" + bj.getUserID();
    new Thread(new Runnable()
    {
      public void run()
      {
        String str = am.a().connectToURL("https://ws.tapjoyads.com/get_offers/featured?", am.featuredAppURLParams);
        boolean bool = false;
        if (str != null)
          bool = am.a(am.this, str);
        if (!bool)
          am.b().getFeaturedAppResponseFailed("Error retrieving featured app data from the server.");
      }
    }).start();
  }

  public void getFeaturedApp(String paramString, x paramx)
  {
    bv.i("Featured App", "Getting Featured App userID: " + bj.getUserID() + ", currencyID: " + paramString);
    c = paramx;
    this.b = new al();
    new StringBuilder(String.valueOf(bj.getURLParams() + "&publisher_user_id=" + bj.getUserID())).append("&currency_id=").append(paramString).toString();
    new Thread(new Runnable()
    {
      public void run()
      {
        String str = am.a().connectToURL("https://ws.tapjoyads.com/get_offers/featured?", am.featuredAppURLParams);
        boolean bool = false;
        if (str != null)
          bool = am.a(am.this, str);
        if (!bool)
          am.b().getFeaturedAppResponseFailed("Error retrieving featured app data from the server.");
      }
    }).start();
  }

  public al getFeaturedAppObject()
  {
    return this.b;
  }

  public void setDisplayCount(int paramInt)
  {
    this.f = paramInt;
  }

  public void showFeaturedAppFullScreenAd()
  {
    String str = "";
    if (this.b != null)
      str = this.b.fullScreenAdURL;
    bv.i("Featured App", "Displaying Full Screen AD with URL: " + str);
    if (str.length() != 0)
    {
      Intent localIntent = new Intent(this.e, bb.class);
      localIntent.setFlags(268435456);
      localIntent.putExtra("USER_ID", bj.getUserID());
      localIntent.putExtra("URL_PARAMS", bj.getURLParams());
      localIntent.putExtra("FULLSCREEN_AD_URL", str);
      localIntent.putExtra("CLIENT_PACKAGE", bj.getClientPackage());
      this.e.startActivity(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.am
 * JD-Core Version:    0.6.2
 */