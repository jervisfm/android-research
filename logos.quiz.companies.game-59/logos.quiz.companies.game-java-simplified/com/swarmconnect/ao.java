package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ao extends u
{
  private ListView l;
  private a m = new a(null);
  private List<SwarmAchievement> n = new ArrayList();

  private void e()
  {
    b();
    SwarmAchievement.getAchievementsList(new SwarmAchievement.GotAchievementsListCB()
    {
      public void gotList(List<SwarmAchievement> paramAnonymousList)
      {
        ao.this.c();
        Iterator localIterator;
        if (paramAnonymousList != null)
        {
          ao.a(ao.this, paramAnonymousList);
          localIterator = ao.a(ao.this).iterator();
        }
        while (true)
        {
          if (!localIterator.hasNext())
          {
            ao.c(ao.this).notifyDataSetChanged();
            return;
          }
          SwarmAchievement localSwarmAchievement = (SwarmAchievement)localIterator.next();
          if ((localSwarmAchievement.hidden) && (!localSwarmAchievement.unlocked))
            localIterator.remove();
        }
      }
    });
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_list"));
    this.l = ((ListView)a(a("@id/list")));
    this.l.setAdapter(this.m);
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_trophy"), "Achievements");
  }

  protected void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        ao.b(ao.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      return ao.a(ao.this).size();
    }

    public Object getItem(int paramInt)
    {
      if (ao.a(ao.this).size() > paramInt)
        return ao.a(ao.this).get(paramInt);
      return null;
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(ao.this.d(), ao.this.a("@layout/swarm_achievement_row"), null);
      SwarmAchievement localSwarmAchievement = (SwarmAchievement)getItem(paramInt);
      if (localSwarmAchievement != null)
        if (paramInt % 2 != 0)
          break label200;
      label200: for (int i = 16777215; ; i = -1996488705)
      {
        paramView.setBackgroundColor(i);
        ((TextView)paramView.findViewById(ao.this.a("@id/title"))).setText(localSwarmAchievement.title);
        ((TextView)paramView.findViewById(ao.this.a("@id/description"))).setText(localSwarmAchievement.description);
        ((TextView)paramView.findViewById(ao.this.a("@id/points"))).setText(localSwarmAchievement.points);
        ((TextView)paramView.findViewById(ao.this.a("@id/points"))).setTextColor(-14540254);
        if (!localSwarmAchievement.unlocked)
          break;
        ((ImageView)paramView.findViewById(ao.this.a("@id/achievement_icon"))).setImageResource(ao.this.a("@drawable/swarm_trophy_gold"));
        return paramView;
      }
      ((ImageView)paramView.findViewById(ao.this.a("@id/achievement_icon"))).setImageResource(ao.this.a("@drawable/swarm_trophy_grey"));
      return paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ao
 * JD-Core Version:    0.6.2
 */