package com.swarmconnect;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

class aq
{
  private static TreeSet<a> a = new TreeSet();
  private static HashMap<Class<? extends APICall>, Integer> b = new HashMap();
  private static HashMap<Class<? extends APICall>, LinkedList<Class<? extends APICall>>> c = new HashMap();
  private static HashMap<Class<? extends APICall>, HashMap<String, a>> d = new HashMap();

  protected static String a(APICall paramAPICall)
  {
    HashMap localHashMap = (HashMap)d.get(paramAPICall.getClass());
    if (localHashMap != null)
    {
      a locala = (a)localHashMap.get(paramAPICall.url);
      if (locala != null)
      {
        if (System.currentTimeMillis() < locala.expiration)
          return locala.response;
        localHashMap.remove(paramAPICall.url);
        a.remove(locala);
      }
    }
    return null;
  }

  protected static void a()
  {
    try
    {
      d.clear();
      a.clear();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  protected static void a(APICall paramAPICall, String paramString)
  {
    long l1 = b(paramAPICall.getClass());
    if (l1 > 0L)
    {
      long l2 = System.currentTimeMillis() + l1 * 1000L;
      HashMap localHashMap = (HashMap)d.get(paramAPICall.getClass());
      if (localHashMap != null)
      {
        a locala = new a(paramAPICall.getClass(), paramAPICall.url, paramString, l2);
        localHashMap.put(paramAPICall.url, locala);
        a.add(locala);
      }
    }
    b();
  }

  protected static void a(Class<? extends APICall> paramClass)
  {
    LinkedList localLinkedList = (LinkedList)c.get(paramClass);
    Iterator localIterator;
    if (localLinkedList != null)
      localIterator = localLinkedList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      invalidate((Class)localIterator.next());
    }
  }

  protected static void a(Class<? extends APICall> paramClass, int paramInt)
  {
    if (paramInt > 0)
    {
      b.put(paramClass, Integer.valueOf(paramInt));
      d.put(paramClass, new HashMap());
      return;
    }
    b.remove(paramClass);
    d.remove(paramClass);
  }

  protected static void a(Class<? extends APICall> paramClass1, Class<? extends APICall> paramClass2)
  {
    LinkedList localLinkedList = (LinkedList)c.get(paramClass1);
    if (localLinkedList == null)
    {
      localLinkedList = new LinkedList();
      c.put(paramClass1, localLinkedList);
    }
    localLinkedList.add(paramClass2);
  }

  protected static int b(Class<? extends APICall> paramClass)
  {
    Integer localInteger = (Integer)b.get(paramClass);
    if ((localInteger == null) || (localInteger.intValue() < 0))
      return 0;
    return localInteger.intValue();
  }

  private static void b()
  {
    try
    {
      while (true)
      {
        if (a.size() <= 0)
          return;
        a locala = (a)a.first();
        if (System.currentTimeMillis() < locala.expiration)
          break;
        a.remove(locala);
        HashMap localHashMap = (HashMap)d.get(locala.api);
        if (localHashMap != null)
          localHashMap.remove(locala.url);
      }
    }
    catch (Exception localException)
    {
    }
  }

  public static void invalidate(Class<? extends APICall> paramClass)
  {
    HashMap localHashMap = (HashMap)d.get(paramClass);
    if (localHashMap != null)
      localHashMap.clear();
  }

  private static class a
    implements Comparable
  {
    public Class<? extends APICall> api;
    public long expiration;
    public String response;
    public String url;

    public a(Class<? extends APICall> paramClass, String paramString1, String paramString2, long paramLong)
    {
      this.api = paramClass;
      this.url = paramString1;
      this.response = paramString2;
      this.expiration = paramLong;
    }

    public int compareTo(Object paramObject)
    {
      if (!(paramObject instanceof a))
        throw new ClassCastException("CachedResponse expected.");
      return (int)(this.expiration - ((a)paramObject).expiration);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.aq
 * JD-Core Version:    0.6.2
 */