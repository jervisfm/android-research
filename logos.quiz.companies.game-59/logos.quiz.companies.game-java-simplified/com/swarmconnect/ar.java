package com.swarmconnect;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.swarmconnect.ui.UiConf;

class ar extends ba
{
  public static String prefillUsername = null;
  private Button m;
  private EditText n;
  private TextView o;
  private SwarmFacebook.FacebookLoginCB p = new bg(this);

  public void createAccount()
  {
    this.o.setVisibility(4);
    this.o.setText("");
    String str = this.n.getText().toString();
    b();
    ab localab = new ab();
    localab.username = str;
    localab.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        ar.this.a(new Runnable()
        {
          public void run()
          {
            ar.this.c();
            ab localab;
            if (paramAnonymousAPICall != null)
            {
              localab = (ab)paramAnonymousAPICall;
              if (localab.ok)
              {
                ar.a(ar.this).setVisibility(4);
                ar.a(ar.this).setText("");
                bi.username = localab.username;
                ar.this.show(2);
              }
            }
            else
            {
              return;
            }
            ar.a(ar.this).setVisibility(0);
            ar.a(ar.this).setText(localab.error);
          }
        });
      }

      public void requestFailed()
      {
        ar.this.c();
        Swarm.a();
        ar.a();
      }
    };
    localab.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_external_username"));
    ((TextView)a(a("@id/terms"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW");
        localIntent.setData(Uri.parse("http://api.swarmconnect.com//privacy.html"));
        ar.this.c.startActivity(localIntent);
      }
    });
    this.n = ((EditText)a(a("@id/username")));
    this.m = ((Button)a(a("@id/create")));
    this.m.setBackgroundDrawable(UiConf.greenButtonBackground());
    this.m.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (ar.b(ar.this).getText().toString().length() > 0)
        {
          SwarmFacebook.login(ar.this.c, ar.c(ar.this));
          return;
        }
        Toast.makeText(ar.this.c, "Username required", 0).show();
      }
    });
    this.o = ((TextView)a(a("@id/username_error")));
    this.o.setTextColor(-7398867);
    super.onCreate(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    if (SwarmFacebook.isValid())
    {
      b("Facebook is valid!");
      if (prefillUsername != null)
      {
        this.n.setText(prefillUsername);
        prefillUsername = null;
      }
      return;
    }
    finish();
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ar
 * JD-Core Version:    0.6.2
 */