package com.swarmconnect;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import com.swarmconnect.utils.SeparatedListAdapter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class au extends u
{
  private static String l = "";
  private SwarmLeaderboard m;
  private List<SwarmLeaderboardScore> n = new ArrayList();
  private List<SwarmLeaderboardScore> o = new ArrayList();
  private List<SwarmLeaderboardScore> p = new ArrayList();
  private ListView q;
  private SeparatedListAdapter r;
  private a s = new a(this.n);
  private a t = new a(this.o);
  private a u = new a(this.p);
  private View v;
  private TextView w;
  private Dialog x;

  private void b(int paramInt, String paramString)
  {
    this.v.setVisibility(8);
    if (this.m != null)
    {
      b();
      this.m.getPageOfScores(0, paramString, "self", new SwarmLeaderboard.GotScoresCB()
      {
        public void gotScores(int paramAnonymousInt, List<SwarmLeaderboardScore> paramAnonymousList)
        {
          au.this.c();
          if (paramAnonymousList != null)
          {
            au.d(au.this).clear();
            au.d(au.this).addAll(paramAnonymousList);
            au.e(au.this).notifyDataSetChanged();
          }
        }
      });
      this.m.getPageOfScores(0, paramString, "friends", new SwarmLeaderboard.GotScoresCB()
      {
        public void gotScores(int paramAnonymousInt, List<SwarmLeaderboardScore> paramAnonymousList)
        {
          au.this.c();
          if (paramAnonymousList != null)
          {
            au.f(au.this).clear();
            au.f(au.this).addAll(paramAnonymousList);
            au.e(au.this).notifyDataSetChanged();
          }
        }
      });
      this.m.getPageOfScores(paramInt, paramString, "all", new SwarmLeaderboard.GotScoresCB()
      {
        public void gotScores(int paramAnonymousInt, List<SwarmLeaderboardScore> paramAnonymousList)
        {
          au.this.c();
          View localView;
          int j;
          if (paramAnonymousList != null)
          {
            au.a(au.this).addAll(paramAnonymousList);
            Collections.sort(au.a(au.this));
            au.e(au.this).notifyDataSetChanged();
            if (au.a(au.this).size() <= 0)
              break label132;
            au.g(au.this).setVisibility(0);
            au.h(au.this).setVisibility(8);
            localView = au.i(au.this);
            int i = au.a(au.this).size() % 25;
            j = 0;
            if (i != 0)
              break label125;
          }
          while (true)
          {
            localView.setVisibility(j);
            return;
            label125: j = 8;
          }
          label132: au.g(au.this).setVisibility(8);
          au.h(au.this).setVisibility(0);
          au.h(au.this).setText("No scores");
        }
      });
    }
  }

  private void c(String paramString)
  {
    if (l != paramString)
      l = paramString;
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
      {
        this.n.clear();
        this.o.clear();
        this.p.clear();
        this.r.notifyDataSetChanged();
        a(l, true, new Runnable()
        {
          public void run()
          {
            au.c(au.this);
          }
        });
        b(0, l);
      }
      return;
    }
  }

  private void f()
  {
    if (this.x != null)
    {
      this.x.dismiss();
      this.x = null;
    }
  }

  private void g()
  {
    f();
    this.x = new Dialog(this.c);
    this.x.requestWindowFeature(1);
    LinearLayout localLinearLayout1 = new LinearLayout(this.c);
    localLinearLayout1.setOrientation(1);
    localLinearLayout1.setBackgroundColor(-13421773);
    LinearLayout localLinearLayout2 = new LinearLayout(this.c);
    localLinearLayout2.setOrientation(0);
    localLinearLayout2.setPadding((int)UiConf.dips(10.0F), (int)UiConf.dips(5.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(5.0F));
    localLinearLayout2.setBackgroundColor(-14540254);
    localLinearLayout2.setGravity(16);
    ImageView localImageView = new ImageView(this.c);
    localImageView.setImageResource(a("@drawable/swarm_head"));
    localImageView.setLayoutParams(new LinearLayout.LayoutParams((int)UiConf.dips(17.0F), (int)UiConf.dips(18.0F)));
    TextView localTextView1 = new TextView(this.c);
    localTextView1.setText("Show Scores");
    localTextView1.setTextColor(-3355444);
    localTextView1.setShadowLayer(1.0F, 1.0F, 1.0F, -16777216);
    localTextView1.setTextSize(15.0F);
    localTextView1.setPadding((int)UiConf.dips(5.0F), 0, 0, 0);
    View localView = new View(this.c);
    localView.setLayoutParams(new LinearLayout.LayoutParams((int)UiConf.dips(200.0F), 1));
    localView.setBackgroundColor(-11184811);
    LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams1.setMargins((int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), 0);
    TextView localTextView2 = new TextView(this.c);
    localTextView2.setText("Today");
    localTextView2.setGravity(17);
    localTextView2.setBackgroundDrawable(UiConf.contextualGreyBackground());
    localTextView2.setTextColor(-1);
    localTextView2.setTextSize(17.0F);
    localTextView2.setShadowLayer(1.0F, 1.0F, 1.0F, -16777216);
    localTextView2.setPadding((int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F), (int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F));
    localTextView2.setLayoutParams(localLayoutParams1);
    localTextView2.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        au.a(au.this, "today");
        au.j(au.this).cancel();
      }
    });
    TextView localTextView3 = new TextView(this.c);
    localTextView3.setText("This Week");
    localTextView3.setGravity(17);
    localTextView3.setBackgroundDrawable(UiConf.contextualGreyBackground());
    localTextView3.setTextColor(-1);
    localTextView3.setTextSize(17.0F);
    localTextView3.setShadowLayer(1.0F, 1.0F, 1.0F, -16777216);
    localTextView3.setPadding((int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F), (int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F));
    localTextView3.setLayoutParams(localLayoutParams1);
    localTextView3.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        au.a(au.this, "week");
        au.j(au.this).cancel();
      }
    });
    TextView localTextView4 = new TextView(this.c);
    localTextView4.setText("This Month");
    localTextView4.setGravity(17);
    localTextView4.setBackgroundDrawable(UiConf.contextualGreyBackground());
    localTextView4.setTextColor(-1);
    localTextView4.setTextSize(17.0F);
    localTextView4.setShadowLayer(1.0F, 1.0F, 1.0F, -16777216);
    localTextView4.setPadding((int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F), (int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F));
    localTextView4.setLayoutParams(localLayoutParams1);
    localTextView4.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        au.a(au.this, "month");
        au.j(au.this).cancel();
      }
    });
    LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams2.setMargins((int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F));
    new LinearLayout.LayoutParams(-1, -2).setMargins((int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F), (int)UiConf.dips(10.0F));
    TextView localTextView5 = new TextView(this.c);
    localTextView5.setText("All Time");
    localTextView5.setGravity(17);
    localTextView5.setBackgroundDrawable(UiConf.contextualGreyBackground());
    localTextView5.setTextColor(-1);
    localTextView5.setTextSize(17.0F);
    localTextView5.setShadowLayer(1.0F, 1.0F, 1.0F, -16777216);
    localTextView5.setPadding((int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F), (int)UiConf.dips(20.0F), (int)UiConf.dips(12.0F));
    localTextView5.setLayoutParams(localLayoutParams2);
    localTextView5.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        au.a(au.this, "all");
        au.j(au.this).cancel();
      }
    });
    localLinearLayout2.addView(localImageView);
    localLinearLayout2.addView(localTextView1);
    localLinearLayout1.addView(localLinearLayout2);
    localLinearLayout1.addView(localView);
    localLinearLayout1.addView(localTextView2);
    localLinearLayout1.addView(localTextView3);
    localLinearLayout1.addView(localTextView4);
    localLinearLayout1.addView(localTextView5);
    this.x.setContentView(localLinearLayout1);
    this.x.show();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_leaderboard_list"));
    Bundle localBundle = this.c.getIntent().getExtras();
    if (localBundle != null)
      this.m = ((SwarmLeaderboard)localBundle.getSerializable("leaderboard"));
    this.w = ((TextView)this.c.findViewById(a("@id/empty_list")));
    View localView = View.inflate(this.c, a("@layout/swarm_load_more"), null);
    this.v = localView.findViewById(a("@id/more"));
    localView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        try
        {
          int i = ((SwarmLeaderboardScore)au.a(au.this).get(-1 + au.a(au.this).size())).rank / 25;
          au.a(au.this, i, au.e());
          return;
        }
        catch (Exception localException)
        {
        }
      }
    });
    this.r = new SeparatedListAdapter(this.c, a("@layout/swarm_list_section_header"));
    this.r.addSection("My Scores", this.s);
    this.r.addSection("Friend Scores", this.t);
    this.r.addSection("Top Scores", this.u);
    this.q = ((ListView)a(a("@id/list")));
    this.q.addFooterView(localView);
    this.q.setAdapter(this.r);
    super.onCreate(paramBundle);
    l = "";
    c("week");
    int i = a("@drawable/swarm_leaderboards");
    if (this.m == null);
    for (String str = "Leaderboard"; ; str = this.m.name)
    {
      a(i, str);
      return;
    }
  }

  public void onPause()
  {
    super.onPause();
    f();
  }

  protected void reload()
  {
  }

  private class a extends BaseAdapter
  {
    private List<SwarmLeaderboardScore> b;

    public a()
    {
      Object localObject;
      this.b = localObject;
    }

    public int getCount()
    {
      if (this.b == null)
        return 0;
      return this.b.size();
    }

    public SwarmLeaderboardScore getItem(int paramInt)
    {
      if ((this.b == null) || (paramInt >= this.b.size()))
        return null;
      return (SwarmLeaderboardScore)this.b.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(au.this.d(), au.this.a("@layout/swarm_leaderboard_row"), null);
      SwarmLeaderboardScore localSwarmLeaderboardScore = getItem(paramInt);
      int i;
      if (localSwarmLeaderboardScore != null)
      {
        if (paramInt % 2 != 0)
          break label210;
        i = 16777215;
        paramView.setBackgroundColor(i);
        if ((localSwarmLeaderboardScore.user.userId == Swarm.user.userId) && (this.b == au.a(au.this)))
          paramView.setBackgroundColor(1427736791);
        ((TextView)paramView.findViewById(au.this.a("@id/rank"))).setText(localSwarmLeaderboardScore.rank);
        ((TextView)paramView.findViewById(au.this.a("@id/name"))).setText(localSwarmLeaderboardScore.user.username);
        if (au.b(au.this).format != SwarmLeaderboard.LeaderboardFormat.INTEGER)
          break label217;
        ((TextView)paramView.findViewById(au.this.a("@id/score"))).setText((int)localSwarmLeaderboardScore.score);
      }
      label210: label217: 
      do
      {
        return paramView;
        i = -1996488705;
        break;
        if (au.b(au.this).format == SwarmLeaderboard.LeaderboardFormat.TIME)
        {
          int j = (int)localSwarmLeaderboardScore.score / 3600;
          int k = (int)localSwarmLeaderboardScore.score % 3600 / 60;
          float f = localSwarmLeaderboardScore.score % 60.0F;
          DecimalFormat localDecimalFormat = new DecimalFormat();
          localDecimalFormat.setMinimumIntegerDigits(2);
          localDecimalFormat.setMaximumFractionDigits(3);
          String str = j + ":" + localDecimalFormat.format(k) + ":" + localDecimalFormat.format(f);
          ((TextView)paramView.findViewById(au.this.a("@id/score"))).setText(str);
          return paramView;
        }
      }
      while (au.b(au.this).format != SwarmLeaderboard.LeaderboardFormat.FLOAT);
      ((TextView)paramView.findViewById(au.this.a("@id/score"))).setText(localSwarmLeaderboardScore.score);
      return paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.au
 * JD-Core Version:    0.6.2
 */