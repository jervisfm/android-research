package com.swarmconnect;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class av
{
  public static String SHA256(String paramString)
    throws NoSuchAlgorithmException, UnsupportedEncodingException
  {
    bv.i("TapjoyUtil", "SHA256: " + paramString);
    new byte[40];
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256");
    localMessageDigest.update(paramString.getBytes("iso-8859-1"), 0, paramString.length());
    return a(localMessageDigest.digest());
  }

  private static String a(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    if (i >= paramArrayOfByte.length)
      return localStringBuffer.toString();
    int j = 0xF & paramArrayOfByte[i] >>> 4;
    int k = 0;
    while (true)
    {
      if ((j >= 0) && (j <= 9))
        localStringBuffer.append((char)(j + 48));
      int m;
      int n;
      while (true)
      {
        m = 0xF & paramArrayOfByte[i];
        n = k + 1;
        if (k < 1)
          break label95;
        i++;
        break;
        localStringBuffer.append((char)(97 + (j - 10)));
      }
      label95: k = n;
      j = m;
    }
  }

  public static Document buildDocument(String paramString)
  {
    try
    {
      DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
      ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes("UTF-8"));
      Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localByteArrayInputStream);
      return localDocument;
    }
    catch (Exception localException)
    {
      bv.e("TapjoyUtil", "buildDocument exception: " + localException.toString());
    }
    return null;
  }

  public static String getNodeTrimValue(NodeList paramNodeList)
  {
    int i = 0;
    Element localElement = (Element)paramNodeList.item(0);
    if (localElement != null)
    {
      NodeList localNodeList = localElement.getChildNodes();
      int j = localNodeList.getLength();
      String str = "";
      while (true)
      {
        if (i >= j)
        {
          if ((str == null) || (str.equals("")))
            break;
          return str.trim();
        }
        Node localNode = localNodeList.item(i);
        if (localNode != null)
          str = str + localNode.getNodeValue();
        i++;
      }
      return null;
    }
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.av
 * JD-Core Version:    0.6.2
 */