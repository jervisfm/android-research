package com.swarmconnect;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.swarmconnect.ui.UiConf;
import java.util.ArrayList;

abstract class ba extends u
{
  private static boolean m = false;
  protected SwarmFacebook.FacebookLoginCB l = new az(this);

  protected void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    SwarmFacebook.authorizeCallback(paramInt1, paramInt2, paramIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    SwarmFacebook.initialize(this.c, "289484921072856");
    LinearLayout localLinearLayout = (LinearLayout)a(a("@id/facebook_login"));
    if (localLinearLayout != null)
    {
      localLinearLayout.setBackgroundDrawable(UiConf.facebookLoginButton());
      localLinearLayout.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          SwarmFacebook.login(ba.this.c, ba.this.l);
        }
      });
      if ((this instanceof ar))
        localLinearLayout.setVisibility(8);
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    if ((m) && (i.size() == 0))
    {
      if (!Swarm.isLoggedIn())
        SwarmLoginManager.a();
      m = false;
    }
  }

  public void onResume()
  {
    super.onResume();
    if (!m)
    {
      m = true;
      SwarmLoginManager.b();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ba
 * JD-Core Version:    0.6.2
 */