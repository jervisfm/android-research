package com.swarmconnect;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

class bb extends Activity
{
  final String a = "Featured App";
  private WebView b = null;
  private ProgressBar c;
  private String d = "";
  private String e = "";
  private String f = "";
  private String g = "";

  private void a()
  {
    bv.i("Featured App", "Showing offers (userID = " + this.f + ")");
    Intent localIntent = new Intent(this, SwarmMainActivity.class);
    localIntent.putExtra("screenType", 19);
    localIntent.putExtra("USER_ID", this.f);
    localIntent.putExtra("URL_PARAMS", this.g);
    localIntent.putExtra("CLIENT_PACKAGE", this.d);
    startActivity(localIntent);
  }

  private void b()
  {
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.b != null)
      new a(null).execute(new Void[0]);
  }

  protected void onCreate(Bundle paramBundle)
  {
    Bundle localBundle = getIntent().getExtras();
    this.f = localBundle.getString("USER_ID");
    this.d = localBundle.getString("CLIENT_PACKAGE");
    this.g = localBundle.getString("URL_PARAMS");
    this.e = localBundle.getString("FULLSCREEN_AD_URL");
    this.e = this.e.replaceAll(" ", "%20");
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    RelativeLayout localRelativeLayout = new RelativeLayout(this);
    this.b = new WebView(this);
    this.b.setWebViewClient(new b(null));
    this.b.getSettings().setJavaScriptEnabled(true);
    this.c = new ProgressBar(this, null, 16842874);
    this.c.setVisibility(0);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams.addRule(13);
    this.c.setLayoutParams(localLayoutParams);
    localRelativeLayout.addView(this.b, -1, -1);
    localRelativeLayout.addView(this.c);
    setContentView(localRelativeLayout);
    this.b.loadUrl(this.e);
    bv.i("Featured App", "Opening Full Screen AD URL = [" + this.e + "]");
  }

  private class a extends AsyncTask<Void, Void, Boolean>
  {
    private a()
    {
    }

    protected Boolean a(Void[] paramArrayOfVoid)
    {
      try
      {
        Thread.sleep(200L);
        return Boolean.valueOf(true);
      }
      catch (InterruptedException localInterruptedException)
      {
        while (true)
          localInterruptedException.printStackTrace();
      }
    }

    protected void a(Boolean paramBoolean)
    {
      if (bb.a(bb.this) != null)
        bb.a(bb.this).loadUrl("javascript:window.onorientationchange();");
    }
  }

  private class b extends WebViewClient
  {
    private b()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      bb.b(bb.this).setVisibility(8);
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      bb.b(bb.this).setVisibility(0);
      bb.b(bb.this).bringToFront();
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      bv.i("Featured App", "URL = [" + paramString + "]");
      if (paramString.contains("showOffers"))
      {
        bv.i("Featured App", "show offers");
        bb.c(bb.this);
        return true;
      }
      if (paramString.contains("dismiss"))
      {
        bv.i("Featured App", "dismiss");
        bb.d(bb.this);
        return true;
      }
      if (paramString.indexOf("market") > -1)
      {
        bv.i("Featured App", "Market URL = [" + paramString + "]");
        try
        {
          String[] arrayOfString = paramString.split("q=");
          String str = "http://market.android.com/details?id=" + arrayOfString[1] + "&referrer=" + bb.e(bb.this);
          Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
          bb.this.startActivity(localIntent2);
          bv.i("Featured App", "Open URL of application = [" + str + "]");
          return true;
        }
        catch (Exception localException)
        {
          bv.i("Featured App", "Android market is unavailable at this device. To view this link install market.");
          return true;
        }
      }
      if (paramString.contains("ws.tapjoyads.com"))
      {
        bv.i("Featured App", "Open redirecting URL = [" + paramString + "]");
        paramWebView.loadUrl(paramString);
        return true;
      }
      bv.i("Featured App", "Opening URL in new browser = [" + paramString + "]");
      Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
      bb.this.startActivity(localIntent1);
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bb
 * JD-Core Version:    0.6.2
 */