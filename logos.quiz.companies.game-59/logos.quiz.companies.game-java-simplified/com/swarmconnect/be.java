package com.swarmconnect;

import android.content.Context;
import android.content.Intent;
import java.util.UUID;
import org.w3c.dom.Document;

class be
{
  public static final String TAPJOY_OFFERS = "TapjoyOffers";
  public static final String TAPJOY_POINTS = "TapjoyPoints";
  private static bc d;
  private static f e;
  private static cl f;
  private static cr g;
  String a = null;
  int b = 0;
  Context c;
  private String h = "";
  private String i = "";

  public be(Context paramContext)
  {
    this.c = paramContext;
  }

  private boolean a(String paramString)
  {
    Document localDocument = av.buildDocument(paramString);
    if (localDocument != null)
    {
      String str1 = av.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str1 == null) || (!str1.equals("true")))
        break label150;
      String str2 = av.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
      String str3 = av.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
      if ((str2 != null) && (str3 != null))
      {
        int j = Integer.parseInt(str2);
        int k = bj.getLocalTapPointsTotal();
        if ((g != null) && (k != -9999) && (j > k))
          g.earnedTapPoints(j - k);
        bj.saveTapPointsTotal(Integer.parseInt(str2));
        d.getUpdatePoints(str3, Integer.parseInt(str2));
        return true;
      }
      bv.e("TapjoyPoints", "Invalid XML: Missing tags.");
    }
    while (true)
    {
      return false;
      label150: bv.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
    }
  }

  private boolean b(String paramString)
  {
    Document localDocument = av.buildDocument(paramString);
    String str1;
    if (localDocument != null)
    {
      str1 = av.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str1 == null) || (!str1.equals("true")))
        break label104;
      String str3 = av.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
      String str4 = av.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
      if ((str3 != null) && (str4 != null))
      {
        bj.saveTapPointsTotal(Integer.parseInt(str3));
        e.getSpendPointsResponse(str4, Integer.parseInt(str3));
        return true;
      }
      bv.e("TapjoyPoints", "Invalid XML: Missing tags.");
    }
    while (true)
    {
      return false;
      label104: if ((str1 != null) && (str1.endsWith("false")))
      {
        String str2 = av.getNodeTrimValue(localDocument.getElementsByTagName("Message"));
        bv.i("TapjoyPoints", str2);
        e.getSpendPointsResponseFailed(str2);
        return true;
      }
      bv.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
    }
  }

  private boolean c(String paramString)
  {
    Document localDocument = av.buildDocument(paramString);
    String str1;
    if (localDocument != null)
    {
      str1 = av.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str1 == null) || (!str1.equals("true")))
        break label104;
      String str3 = av.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
      String str4 = av.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
      if ((str3 != null) && (str4 != null))
      {
        bj.saveTapPointsTotal(Integer.parseInt(str3));
        f.getAwardPointsResponse(str4, Integer.parseInt(str3));
        return true;
      }
      bv.e("TapjoyPoints", "Invalid XML: Missing tags.");
    }
    while (true)
    {
      return false;
      label104: if ((str1 != null) && (str1.endsWith("false")))
      {
        String str2 = av.getNodeTrimValue(localDocument.getElementsByTagName("Message"));
        bv.i("TapjoyPoints", str2);
        f.getAwardPointsResponseFailed(str2);
        return true;
      }
      bv.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
    }
  }

  public void awardTapPoints(int paramInt, cl paramcl)
  {
    if (paramInt < 0)
    {
      bv.e("TapjoyPoints", "spendTapPoints error: amount must be a positive number");
      return;
    }
    this.b = paramInt;
    f = paramcl;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = UUID.randomUUID().toString();
        long l = System.currentTimeMillis() / 1000L;
        String str2 = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(bj.getURLParams())).append("&tap_points=").append(be.this.b).toString())).append("&publisher_user_id=").append(bj.getUserID()).toString())).append("&guid=").append(str1).toString())).append("&timestamp=").append(l).toString() + "&verifier=" + bj.getAwardPointsVerifier(l, be.this.b, str1);
        String str3 = new cm().connectToURL("https://ws.tapjoyads.com/points/award?", str2);
        boolean bool = false;
        if (str3 != null)
          bool = be.c(be.this, str3);
        if (!bool)
          be.c().getAwardPointsResponseFailed("Failed to award points.");
      }
    }).start();
  }

  public void getTapPoints(bc parambc)
  {
    d = parambc;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = bj.getURLParams() + "&publisher_user_id=" + bj.getUserID();
        String str2 = new cm().connectToURL("https://ws.tapjoyads.com/get_vg_store_items/user_account?", str1);
        boolean bool = false;
        if (str2 != null)
          bool = be.a(be.this, str2);
        if (!bool)
          be.a().getUpdatePointsFailed("Failed to retrieve points from server");
      }
    }).start();
  }

  public void setEarnedPointsNotifier(cr paramcr)
  {
    g = paramcr;
  }

  public void showOffers()
  {
    bv.i("TapjoyOffers", "Showing offers with userID: " + bj.getUserID());
    Intent localIntent = new Intent(this.c, SwarmMainActivity.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("screenType", 19);
    localIntent.putExtra("USER_ID", bj.getUserID());
    localIntent.putExtra("URL_PARAMS", bj.getURLParams());
    localIntent.putExtra("CLIENT_PACKAGE", bj.getClientPackage());
    this.c.startActivity(localIntent);
  }

  public void showOffersWithCurrencyID(String paramString, boolean paramBoolean)
  {
    bv.i("TapjoyOffers", "Showing offers with currencyID: " + paramString + ", selector: " + paramBoolean + " (userID = " + bj.getUserID() + ")");
    this.h = paramString;
    if (paramBoolean);
    for (String str1 = "1"; ; str1 = "0")
    {
      this.i = str1;
      String str2 = new StringBuilder(String.valueOf(bj.getURLParams())).append("&currency_id=").append(this.h).toString() + "&currency_selector=" + this.i;
      Intent localIntent = new Intent(this.c, SwarmMainActivity.class);
      localIntent.setFlags(268435456);
      localIntent.putExtra("screenType", 19);
      localIntent.putExtra("USER_ID", bj.getUserID());
      localIntent.putExtra("URL_PARAMS", str2);
      localIntent.putExtra("CLIENT_PACKAGE", bj.getClientPackage());
      this.c.startActivity(localIntent);
      return;
    }
  }

  public void spendTapPoints(int paramInt, f paramf)
  {
    if (paramInt < 0)
    {
      bv.e("TapjoyPoints", "spendTapPoints error: amount must be a positive number");
      return;
    }
    this.a = paramInt;
    e = paramf;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = new StringBuilder(String.valueOf(bj.getURLParams())).append("&tap_points=").append(be.this.a).toString() + "&publisher_user_id=" + bj.getUserID();
        String str2 = new cm().connectToURL("https://ws.tapjoyads.com/points/spend?", str1);
        boolean bool = false;
        if (str2 != null)
          bool = be.b(be.this, str2);
        if (!bool)
          be.b().getSpendPointsResponseFailed("Failed to spend points.");
      }
    }).start();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.be
 * JD-Core Version:    0.6.2
 */