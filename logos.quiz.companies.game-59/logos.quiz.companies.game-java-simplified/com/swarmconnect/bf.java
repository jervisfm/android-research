package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import java.util.List;

class bf extends u
{
  private a l = new a(null);
  private SwarmStore m = null;
  private TextView n;

  private void e()
  {
    b();
    SwarmStore.getStore(new SwarmStore.GotSwarmStoreCB()
    {
      public void gotStore(SwarmStore paramAnonymousSwarmStore)
      {
        bf.a(bf.this, paramAnonymousSwarmStore);
        bf.b(bf.this).notifyDataSetChanged();
        bf.this.c();
        if ((paramAnonymousSwarmStore != null) && (paramAnonymousSwarmStore.categories.size() == 1))
        {
          m.category = (SwarmStoreCategory)paramAnonymousSwarmStore.categories.get(0);
          bf.this.show(13);
          bf.this.finish();
        }
      }
    });
    if (Swarm.user != null)
      Swarm.user.getCoins(new SwarmActiveUser.GotUserCoinsCB()
      {
        public void gotCoins(int paramAnonymousInt)
        {
          bf.d(bf.this).setText(paramAnonymousInt);
        }
      });
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_store"));
    this.n = ((TextView)a(a("@id/coins")));
    ((TextView)a(a("@id/get_coins"))).setBackgroundDrawable(UiConf.greenButtonBackground());
    ((TextView)a(a("@id/get_coins"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Swarm.showGetCoins();
      }
    });
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        m.category = bf.b(bf.this).getItem(paramAnonymousInt);
        bf.this.show(13);
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_store"), "Store");
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        bf.c(bf.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if ((bf.a(bf.this) == null) || (bf.a(bf.this).categories == null))
        return 0;
      return bf.a(bf.this).categories.size();
    }

    public SwarmStoreCategory getItem(int paramInt)
    {
      if ((bf.a(bf.this) == null) || (bf.a(bf.this).categories == null) || (paramInt > bf.a(bf.this).categories.size()))
        return null;
      return (SwarmStoreCategory)bf.a(bf.this).categories.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(bf.this.d(), bf.this.a("@layout/swarm_store_row"), null);
      SwarmStoreCategory localSwarmStoreCategory = getItem(paramInt);
      if (localSwarmStoreCategory != null)
        if (paramInt % 2 != 0)
          break label128;
      label128: for (int i = 16777215; ; i = -1996488705)
      {
        paramView.setBackgroundColor(i);
        ((TextView)paramView.findViewById(bf.this.a("@id/name"))).setText(localSwarmStoreCategory.name);
        ((TextView)paramView.findViewById(bf.this.a("@id/num_items"))).setText("(" + localSwarmStoreCategory.listings.size() + " items)");
        return paramView;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bf
 * JD-Core Version:    0.6.2
 */