package com.swarmconnect;

import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.android.Facebook;
import com.swarmconnect.utils.DeviceUtils;

class bg extends SwarmFacebook.FacebookLoginCB
{
  bg(ar paramar)
  {
  }

  public void loginError(String paramString)
  {
    Toast.makeText(this.a.c, "Facebook Login Failed", 0).show();
    this.a.finish();
  }

  public void loginSuccess(Facebook paramFacebook)
  {
    this.a.b();
    ar.a(this.a).setVisibility(4);
    ar.a(this.a).setText("");
    w localw = new w();
    localw.authKey = paramFacebook.getAccessToken();
    localw.device = DeviceUtils.getDeviceId(this.a.c);
    localw.username = ar.b(this.a).getText().toString();
    localw.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        bg.a(bg.this).c();
        w localw = (w)paramAnonymousAPICall;
        if ((localw.user != null) && (localw.user.userId > 0))
        {
          Swarm.a(localw.user, localw.auth, 0);
          ar.a();
          return;
        }
        if ((localw.usernameError != null) && (localw.usernameError.length() > 0))
        {
          ar.a(bg.a(bg.this)).setVisibility(0);
          ar.a(bg.a(bg.this)).setText(localw.usernameError);
          return;
        }
        ar.a(bg.a(bg.this)).setVisibility(0);
        ar.a(bg.a(bg.this)).setText(localw.statusMessage);
      }

      public void requestFailed()
      {
        bg.a(bg.this).c();
        Toast.makeText(bg.a(bg.this).c, "Account Creation Failed", 0).show();
        bg.a(bg.this).finish();
      }
    };
    localw.run();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bg
 * JD-Core Version:    0.6.2
 */