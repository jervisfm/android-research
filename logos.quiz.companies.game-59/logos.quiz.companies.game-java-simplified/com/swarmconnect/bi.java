package com.swarmconnect;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;

class bi extends ba
{
  public static String username;
  private EditText m;
  private EditText n;
  private EditText o;
  private TextView p;
  private TextView q;
  private TextView r;

  private void e()
  {
    this.p.setVisibility(4);
    this.p.setText("");
    this.q.setVisibility(4);
    this.q.setText("");
    this.r.setVisibility(4);
    this.r.setText("");
  }

  private void f()
  {
    e();
    String str1 = this.m.getText().toString();
    String str2 = this.n.getText().toString();
    String str3 = this.o.getText().toString();
    b();
    bu localbu = new bu();
    localbu.username = username;
    localbu.password = str1;
    localbu.confirm = str2;
    localbu.email = str3;
    localbu.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        bi.this.a(new Runnable()
        {
          public void run()
          {
            bi.this.c();
            bu localbu = (bu)paramAnonymousAPICall;
            if (localbu.user != null)
            {
              Swarm.a(localbu.user, localbu.auth);
              bi.a();
              return;
            }
            if (localbu.usernameError.length() > 0)
            {
              Swarm.b("Username: " + localbu.usernameError);
              bi.this.show(11);
              return;
            }
            if (localbu.passwordError.length() > 0)
            {
              bi.b(bi.this).setVisibility(0);
              bi.b(bi.this).setText(localbu.passwordError);
              if (localbu.confirmError.length() <= 0)
                break label241;
              bi.c(bi.this).setVisibility(0);
              bi.c(bi.this).setText(localbu.confirmError);
            }
            while (true)
            {
              if (localbu.emailError.length() <= 0)
                break label273;
              bi.d(bi.this).setVisibility(0);
              bi.d(bi.this).setText(localbu.emailError);
              return;
              bi.b(bi.this).setVisibility(4);
              bi.b(bi.this).setText("");
              break;
              label241: bi.c(bi.this).setVisibility(4);
              bi.c(bi.this).setText("");
            }
            label273: bi.d(bi.this).setVisibility(4);
            bi.d(bi.this).setText("");
          }
        });
      }

      public void requestFailed()
      {
        bi.this.c();
        Swarm.a();
        bi.a();
      }
    };
    localbu.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_create_account"));
    this.m = ((EditText)a(a("@id/password")));
    this.n = ((EditText)a(a("@id/confirm")));
    this.o = ((EditText)a(a("@id/email")));
    this.p = ((TextView)a(a("@id/password_error")));
    this.p.setTextColor(-7398867);
    this.q = ((TextView)a(a("@id/confirm_error")));
    this.q.setTextColor(-7398867);
    this.r = ((TextView)a(a("@id/email_error")));
    this.r.setTextColor(-7398867);
    ((Button)a(a("@id/create"))).setBackgroundDrawable(UiConf.blueButtonBackground());
    ((Button)a(a("@id/create"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        bi.a(bi.this);
      }
    });
    super.onCreate(paramBundle);
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bi
 * JD-Core Version:    0.6.2
 */