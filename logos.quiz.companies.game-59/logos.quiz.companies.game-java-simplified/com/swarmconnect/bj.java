package com.swarmconnect;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import java.util.Timer;
import java.util.TimerTask;
import org.w3c.dom.Document;

class bj
{
  public static final String TAPJOY_CONNECT = "TapjoyConnect";
  private static Context a = null;
  private static bj b = null;
  private static cm c = null;
  private static String d = "";
  private static String e = "";
  private static String f = "";
  private static String g = "";
  private static String h = "";
  private static String i = "";
  private static String j = "";
  private static String k = "";
  private static String l = "";
  private static String m = "";
  private static String n = "";
  private static String o = "";
  private static String p = "";
  private static String q = "";
  private static String r = "";
  private static String s = "";
  private static String t = "";
  private static float v = 1.0F;
  private static String x = null;
  private String u = "";
  private String w = "";
  private long y = 0L;
  private Timer z = null;

  public bj(Context paramContext)
  {
    a = paramContext;
    c = new cm();
    e();
    bv.i("TapjoyConnect", "URL parameters: " + getURLParams());
    new Thread(new Runnable()
    {
      public void run()
      {
        bv.i("TapjoyConnect", "starting connect call...");
        String str1 = bj.getURLParams();
        if (!bj.b(bj.this).equals(""))
          str1 = str1 + "&" + bj.b(bj.this);
        String str2 = bj.c().connectToURL("https://ws.tapjoyads.com/connect?", str1);
        if (str2 != null)
          bj.a(bj.this, str2);
      }
    }).start();
  }

  private boolean a(String paramString)
  {
    Document localDocument = av.buildDocument(paramString);
    if (localDocument != null)
    {
      String str = av.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str != null) && (str.equals("true")))
      {
        bv.i("TapjoyConnect", "Successfully connected to tapjoy site.");
        return true;
      }
      bv.e("TapjoyConnect", "Tapjoy Connect call failed.");
    }
    return false;
  }

  private boolean b(String paramString)
  {
    Document localDocument = av.buildDocument(paramString);
    if (localDocument != null)
    {
      String str = av.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str != null) && (str.equals("true")))
      {
        bv.i("TapjoyConnect", "Successfully sent completed Pay-Per-Action to Tapjoy server.");
        return true;
      }
      bv.e("TapjoyConnect", "Completed Pay-Per-Action call failed.");
    }
    return false;
  }

  private static String d()
  {
    String str = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf("")).append("udid=").append(Uri.encode(d)).append("&").toString())).append("device_name=").append(Uri.encode(e)).append("&").toString())).append("device_type=").append(Uri.encode(f)).append("&").toString())).append("os_version=").append(Uri.encode(g)).append("&").toString())).append("country_code=").append(Uri.encode(h)).append("&").toString())).append("language=").append(Uri.encode(i)).append("&").toString())).append("app_version=").append(Uri.encode(k)).append("&").toString())).append("library_version=").append(Uri.encode(l)).append("&").toString() + "display_multiplier=" + Uri.encode(Float.toString(v));
    if (p.length() > 0)
      str = new StringBuilder(String.valueOf(str)).append("&").toString() + "carrier_name=" + Uri.encode(p);
    if ((q.length() > 0) && (r.length() > 0))
      str = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(str)).append("&").toString())).append("carrier_country_code=").append(Uri.encode(q)).toString())).append("&").toString() + "mobile_country_code=" + Uri.encode(r);
    if ((m.length() > 0) && (n.length() > 0))
      str = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(str)).append("&").toString())).append("screen_density=").append(Uri.encode(m)).append("&").toString() + "screen_layout_size=" + Uri.encode(n);
    return str;
  }

  // ERROR //
  private void e()
  {
    // Byte code:
    //   0: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   3: invokevirtual 245	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   6: astore_1
    //   7: aload_1
    //   8: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   11: invokevirtual 248	android/content/Context:getPackageName	()Ljava/lang/String;
    //   14: iconst_0
    //   15: invokevirtual 254	android/content/pm/PackageManager:getPackageInfo	(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    //   18: getfield 259	android/content/pm/PackageInfo:versionName	Ljava/lang/String;
    //   21: putstatic 66	com/swarmconnect/bj:k	Ljava/lang/String;
    //   24: ldc_w 261
    //   27: putstatic 56	com/swarmconnect/bj:f	Ljava/lang/String;
    //   30: getstatic 266	android/os/Build:MODEL	Ljava/lang/String;
    //   33: putstatic 54	com/swarmconnect/bj:e	Ljava/lang/String;
    //   36: getstatic 271	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   39: putstatic 58	com/swarmconnect/bj:g	Ljava/lang/String;
    //   42: invokestatic 277	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   45: invokevirtual 280	java/util/Locale:getCountry	()Ljava/lang/String;
    //   48: putstatic 60	com/swarmconnect/bj:h	Ljava/lang/String;
    //   51: invokestatic 277	java/util/Locale:getDefault	()Ljava/util/Locale;
    //   54: invokevirtual 283	java/util/Locale:getLanguage	()Ljava/lang/String;
    //   57: putstatic 62	com/swarmconnect/bj:i	Ljava/lang/String;
    //   60: ldc_w 285
    //   63: putstatic 68	com/swarmconnect/bj:l	Ljava/lang/String;
    //   66: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   69: ldc_w 287
    //   72: iconst_0
    //   73: invokevirtual 291	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   76: astore_3
    //   77: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   80: ldc_w 293
    //   83: invokevirtual 297	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   86: checkcast 299	android/telephony/TelephonyManager
    //   89: astore 8
    //   91: aload 8
    //   93: ifnull +35 -> 128
    //   96: aload 8
    //   98: invokevirtual 302	android/telephony/TelephonyManager:getDeviceId	()Ljava/lang/String;
    //   101: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   104: aload 8
    //   106: invokevirtual 305	android/telephony/TelephonyManager:getNetworkOperatorName	()Ljava/lang/String;
    //   109: putstatic 76	com/swarmconnect/bj:p	Ljava/lang/String;
    //   112: aload 8
    //   114: invokevirtual 308	android/telephony/TelephonyManager:getNetworkCountryIso	()Ljava/lang/String;
    //   117: putstatic 78	com/swarmconnect/bj:q	Ljava/lang/String;
    //   120: aload 8
    //   122: invokevirtual 311	android/telephony/TelephonyManager:getNetworkOperator	()Ljava/lang/String;
    //   125: putstatic 80	com/swarmconnect/bj:r	Ljava/lang/String;
    //   128: ldc 8
    //   130: new 107	java/lang/StringBuilder
    //   133: dup
    //   134: ldc_w 313
    //   137: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   140: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   143: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   149: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   152: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   155: ifnonnull +774 -> 929
    //   158: ldc 8
    //   160: ldc_w 315
    //   163: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   166: iconst_1
    //   167: istore 9
    //   169: ldc 8
    //   171: new 107	java/lang/StringBuilder
    //   174: dup
    //   175: ldc_w 317
    //   178: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   181: getstatic 320	android/os/Build$VERSION:SDK	Ljava/lang/String;
    //   184: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   190: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   193: iload 9
    //   195: ifeq +98 -> 293
    //   198: getstatic 320	android/os/Build$VERSION:SDK	Ljava/lang/String;
    //   201: invokestatic 326	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   204: bipush 9
    //   206: if_icmplt +87 -> 293
    //   209: ldc 8
    //   211: ldc_w 328
    //   214: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   217: new 330	com/swarmconnect/j
    //   220: dup
    //   221: invokespecial 331	com/swarmconnect/j:<init>	()V
    //   224: invokevirtual 334	com/swarmconnect/j:getSerial	()Ljava/lang/String;
    //   227: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   230: ldc 8
    //   232: ldc_w 336
    //   235: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   238: ldc 8
    //   240: new 107	java/lang/StringBuilder
    //   243: dup
    //   244: ldc_w 338
    //   247: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   250: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   253: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   256: ldc_w 340
    //   259: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   265: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   268: ldc 8
    //   270: ldc_w 336
    //   273: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   276: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   279: ifnonnull +712 -> 991
    //   282: ldc 8
    //   284: ldc_w 342
    //   287: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   290: iconst_1
    //   291: istore 9
    //   293: iload 9
    //   295: ifeq +53 -> 348
    //   298: new 344	java/lang/StringBuffer
    //   301: dup
    //   302: invokespecial 345	java/lang/StringBuffer:<init>	()V
    //   305: astore 10
    //   307: aload 10
    //   309: ldc_w 347
    //   312: invokevirtual 350	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   315: pop
    //   316: aload_3
    //   317: ldc_w 352
    //   320: aconst_null
    //   321: invokeinterface 358 3 0
    //   326: astore 12
    //   328: aload 12
    //   330: ifnull +891 -> 1221
    //   333: aload 12
    //   335: ldc 50
    //   337: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   340: ifne +881 -> 1221
    //   343: aload 12
    //   345: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   348: getstatic 74	com/swarmconnect/bj:o	Ljava/lang/String;
    //   351: invokevirtual 227	java/lang/String:length	()I
    //   354: ifne +9 -> 363
    //   357: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   360: putstatic 74	com/swarmconnect/bj:o	Ljava/lang/String;
    //   363: getstatic 320	android/os/Build$VERSION:SDK	Ljava/lang/String;
    //   366: invokestatic 326	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   369: iconst_3
    //   370: if_icmple +57 -> 427
    //   373: new 360	com/swarmconnect/cg
    //   376: dup
    //   377: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   380: invokespecial 362	com/swarmconnect/cg:<init>	(Landroid/content/Context;)V
    //   383: astore 7
    //   385: new 107	java/lang/StringBuilder
    //   388: dup
    //   389: invokespecial 363	java/lang/StringBuilder:<init>	()V
    //   392: aload 7
    //   394: invokevirtual 366	com/swarmconnect/cg:getScreenDensity	()I
    //   397: invokevirtual 369	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   400: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   403: putstatic 70	com/swarmconnect/bj:m	Ljava/lang/String;
    //   406: new 107	java/lang/StringBuilder
    //   409: dup
    //   410: invokespecial 363	java/lang/StringBuilder:<init>	()V
    //   413: aload 7
    //   415: invokevirtual 372	com/swarmconnect/cg:getScreenLayoutSize	()I
    //   418: invokevirtual 369	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   421: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   424: putstatic 72	com/swarmconnect/bj:n	Ljava/lang/String;
    //   427: aload_3
    //   428: ldc_w 374
    //   431: aconst_null
    //   432: invokeinterface 358 3 0
    //   437: astore 6
    //   439: aload 6
    //   441: ifnull +19 -> 460
    //   444: aload 6
    //   446: ldc 50
    //   448: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   451: ifne +9 -> 460
    //   454: aload_0
    //   455: aload 6
    //   457: putfield 94	com/swarmconnect/bj:u	Ljava/lang/String;
    //   460: getstatic 44	com/swarmconnect/bj:a	Landroid/content/Context;
    //   463: invokevirtual 248	android/content/Context:getPackageName	()Ljava/lang/String;
    //   466: putstatic 84	com/swarmconnect/bj:t	Ljava/lang/String;
    //   469: ldc 8
    //   471: ldc_w 376
    //   474: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   477: ldc 8
    //   479: new 107	java/lang/StringBuilder
    //   482: dup
    //   483: ldc_w 378
    //   486: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   489: getstatic 64	com/swarmconnect/bj:j	Ljava/lang/String;
    //   492: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   495: ldc_w 340
    //   498: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   501: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   504: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   507: ldc 8
    //   509: new 107	java/lang/StringBuilder
    //   512: dup
    //   513: ldc_w 380
    //   516: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   519: getstatic 84	com/swarmconnect/bj:t	Ljava/lang/String;
    //   522: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   525: ldc_w 340
    //   528: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   531: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   534: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   537: ldc 8
    //   539: new 107	java/lang/StringBuilder
    //   542: dup
    //   543: ldc_w 382
    //   546: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   549: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   552: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   555: ldc_w 340
    //   558: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   561: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   564: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   567: ldc 8
    //   569: new 107	java/lang/StringBuilder
    //   572: dup
    //   573: ldc_w 384
    //   576: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   579: getstatic 54	com/swarmconnect/bj:e	Ljava/lang/String;
    //   582: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   585: ldc_w 340
    //   588: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   591: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   594: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   597: ldc 8
    //   599: new 107	java/lang/StringBuilder
    //   602: dup
    //   603: ldc_w 386
    //   606: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   609: getstatic 56	com/swarmconnect/bj:f	Ljava/lang/String;
    //   612: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   615: ldc_w 340
    //   618: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   621: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   624: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   627: ldc 8
    //   629: new 107	java/lang/StringBuilder
    //   632: dup
    //   633: ldc_w 388
    //   636: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   639: getstatic 68	com/swarmconnect/bj:l	Ljava/lang/String;
    //   642: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   645: ldc_w 340
    //   648: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   651: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   654: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   657: ldc 8
    //   659: new 107	java/lang/StringBuilder
    //   662: dup
    //   663: ldc_w 390
    //   666: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   669: getstatic 58	com/swarmconnect/bj:g	Ljava/lang/String;
    //   672: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   675: ldc_w 340
    //   678: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   681: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   684: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   687: ldc 8
    //   689: new 107	java/lang/StringBuilder
    //   692: dup
    //   693: ldc_w 392
    //   696: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   699: getstatic 60	com/swarmconnect/bj:h	Ljava/lang/String;
    //   702: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: ldc_w 340
    //   708: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   711: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   714: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   717: ldc 8
    //   719: new 107	java/lang/StringBuilder
    //   722: dup
    //   723: ldc_w 394
    //   726: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   729: getstatic 62	com/swarmconnect/bj:i	Ljava/lang/String;
    //   732: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   735: ldc_w 340
    //   738: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   741: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   744: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   747: ldc 8
    //   749: new 107	java/lang/StringBuilder
    //   752: dup
    //   753: ldc_w 396
    //   756: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   759: getstatic 70	com/swarmconnect/bj:m	Ljava/lang/String;
    //   762: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   765: ldc_w 340
    //   768: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   771: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   774: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   777: ldc 8
    //   779: new 107	java/lang/StringBuilder
    //   782: dup
    //   783: ldc_w 398
    //   786: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   789: getstatic 72	com/swarmconnect/bj:n	Ljava/lang/String;
    //   792: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   795: ldc_w 340
    //   798: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   801: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   804: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   807: ldc 8
    //   809: new 107	java/lang/StringBuilder
    //   812: dup
    //   813: ldc_w 400
    //   816: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   819: getstatic 76	com/swarmconnect/bj:p	Ljava/lang/String;
    //   822: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   825: ldc_w 340
    //   828: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   831: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   834: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   837: ldc 8
    //   839: new 107	java/lang/StringBuilder
    //   842: dup
    //   843: ldc_w 402
    //   846: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   849: getstatic 78	com/swarmconnect/bj:q	Ljava/lang/String;
    //   852: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   855: ldc_w 340
    //   858: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   861: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   864: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   867: ldc 8
    //   869: new 107	java/lang/StringBuilder
    //   872: dup
    //   873: ldc_w 404
    //   876: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   879: getstatic 80	com/swarmconnect/bj:r	Ljava/lang/String;
    //   882: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   885: ldc_w 340
    //   888: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   891: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   894: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   897: ldc 8
    //   899: new 107	java/lang/StringBuilder
    //   902: dup
    //   903: ldc_w 406
    //   906: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   909: aload_0
    //   910: getfield 94	com/swarmconnect/bj:u	Ljava/lang/String;
    //   913: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   916: ldc_w 340
    //   919: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   922: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   925: invokestatic 128	com/swarmconnect/bv:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   928: return
    //   929: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   932: invokevirtual 227	java/lang/String:length	()I
    //   935: ifeq +27 -> 962
    //   938: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   941: ldc_w 408
    //   944: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   947: ifne +15 -> 962
    //   950: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   953: ldc_w 410
    //   956: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   959: ifeq +17 -> 976
    //   962: ldc 8
    //   964: ldc_w 412
    //   967: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   970: iconst_1
    //   971: istore 9
    //   973: goto -804 -> 169
    //   976: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   979: invokevirtual 415	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   982: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   985: iconst_0
    //   986: istore 9
    //   988: goto -819 -> 169
    //   991: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   994: invokevirtual 227	java/lang/String:length	()I
    //   997: ifeq +39 -> 1036
    //   1000: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1003: ldc_w 408
    //   1006: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1009: ifne +27 -> 1036
    //   1012: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1015: ldc_w 410
    //   1018: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1021: ifne +15 -> 1036
    //   1024: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1027: ldc_w 417
    //   1030: invokevirtual 174	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1033: ifeq +17 -> 1050
    //   1036: ldc 8
    //   1038: ldc_w 419
    //   1041: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   1044: iconst_1
    //   1045: istore 9
    //   1047: goto -754 -> 293
    //   1050: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1053: invokevirtual 415	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   1056: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1059: iconst_0
    //   1060: istore 9
    //   1062: goto -769 -> 293
    //   1065: iload 13
    //   1067: bipush 32
    //   1069: if_icmplt +92 -> 1161
    //   1072: aload 10
    //   1074: invokevirtual 420	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   1077: invokevirtual 415	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   1080: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1083: aload_3
    //   1084: invokeinterface 424 1 0
    //   1089: astore 14
    //   1091: aload 14
    //   1093: ldc_w 352
    //   1096: getstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1099: invokeinterface 430 3 0
    //   1104: pop
    //   1105: aload 14
    //   1107: invokeinterface 434 1 0
    //   1112: pop
    //   1113: goto -765 -> 348
    //   1116: astore 4
    //   1118: ldc 8
    //   1120: new 107	java/lang/StringBuilder
    //   1123: dup
    //   1124: ldc_w 436
    //   1127: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1130: aload 4
    //   1132: invokevirtual 437	java/lang/Exception:toString	()Ljava/lang/String;
    //   1135: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1138: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1141: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   1144: aconst_null
    //   1145: putstatic 52	com/swarmconnect/bj:d	Ljava/lang/String;
    //   1148: goto -800 -> 348
    //   1151: astore_2
    //   1152: ldc 8
    //   1154: ldc_w 439
    //   1157: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   1160: return
    //   1161: aload 10
    //   1163: ldc_w 441
    //   1166: ldc2_w 442
    //   1169: invokestatic 449	java/lang/Math:random	()D
    //   1172: dmul
    //   1173: d2i
    //   1174: bipush 30
    //   1176: irem
    //   1177: invokevirtual 453	java/lang/String:charAt	(I)C
    //   1180: invokevirtual 456	java/lang/StringBuffer:append	(C)Ljava/lang/StringBuffer;
    //   1183: pop
    //   1184: iinc 13 1
    //   1187: goto -122 -> 1065
    //   1190: astore 5
    //   1192: ldc 8
    //   1194: new 107	java/lang/StringBuilder
    //   1197: dup
    //   1198: ldc_w 458
    //   1201: invokespecial 112	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1204: aload 5
    //   1206: invokevirtual 437	java/lang/Exception:toString	()Ljava/lang/String;
    //   1209: invokevirtual 120	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1212: invokevirtual 123	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1215: invokestatic 180	com/swarmconnect/bv:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   1218: goto -791 -> 427
    //   1221: iconst_0
    //   1222: istore 13
    //   1224: goto -159 -> 1065
    //
    // Exception table:
    //   from	to	target	type
    //   77	91	1116	java/lang/Exception
    //   96	128	1116	java/lang/Exception
    //   128	166	1116	java/lang/Exception
    //   169	193	1116	java/lang/Exception
    //   198	290	1116	java/lang/Exception
    //   298	328	1116	java/lang/Exception
    //   333	348	1116	java/lang/Exception
    //   929	962	1116	java/lang/Exception
    //   962	970	1116	java/lang/Exception
    //   976	985	1116	java/lang/Exception
    //   991	1036	1116	java/lang/Exception
    //   1036	1044	1116	java/lang/Exception
    //   1050	1059	1116	java/lang/Exception
    //   1072	1113	1116	java/lang/Exception
    //   1161	1184	1116	java/lang/Exception
    //   7	77	1151	java/lang/Exception
    //   348	363	1151	java/lang/Exception
    //   427	439	1151	java/lang/Exception
    //   444	460	1151	java/lang/Exception
    //   460	928	1151	java/lang/Exception
    //   1118	1148	1151	java/lang/Exception
    //   1192	1218	1151	java/lang/Exception
    //   363	427	1190	java/lang/Exception
  }

  public static String getAppID()
  {
    return j;
  }

  public static String getAwardPointsVerifier(long paramLong, int paramInt, String paramString)
  {
    try
    {
      String str = av.SHA256(j + ":" + d + ":" + paramLong + ":" + s + ":" + paramInt + ":" + paramString);
      return str;
    }
    catch (Exception localException)
    {
      bv.e("TapjoyConnect", "getAwardPointsVerifier ERROR: " + localException.toString());
    }
    return "";
  }

  public static String getCarrierName()
  {
    return p;
  }

  public static String getClientPackage()
  {
    return t;
  }

  public static Context getContext()
  {
    return a;
  }

  public static String getDeviceID()
  {
    return d;
  }

  public static String getGenericURLParams()
  {
    return new StringBuilder(String.valueOf("")).append("app_id=").append(Uri.encode(j)).append("&").toString() + d();
  }

  public static bj getInstance()
  {
    return b;
  }

  public static int getLocalTapPointsTotal()
  {
    return a.getSharedPreferences("tjcPrefrences", 0).getInt("last_tap_points", -9999);
  }

  public static String getURLParams()
  {
    String str1 = getGenericURLParams() + "&";
    long l1 = System.currentTimeMillis() / 1000L;
    String str2 = getVerifier(l1);
    return new StringBuilder(String.valueOf(str1)).append("timestamp=").append(l1).append("&").toString() + "verifier=" + str2;
  }

  public static String getUserID()
  {
    return o;
  }

  public static String getVerifier(long paramLong)
  {
    try
    {
      String str = av.SHA256(j + ":" + d + ":" + paramLong + ":" + s);
      return str;
    }
    catch (Exception localException)
    {
      bv.e("TapjoyConnect", "getVerifier ERROR: " + localException.toString());
    }
    return "";
  }

  public static void requestTapjoyConnect(Context paramContext, String paramString1, String paramString2)
  {
    j = paramString1;
    s = paramString2;
    b = new bj(paramContext);
  }

  public static void saveTapPointsTotal(int paramInt)
  {
    SharedPreferences.Editor localEditor = a.getSharedPreferences("tjcPrefrences", 0).edit();
    localEditor.putInt("last_tap_points", paramInt);
    localEditor.commit();
  }

  public static void setDebugDeviceID(String paramString)
  {
    d = paramString;
    SharedPreferences.Editor localEditor = a.getSharedPreferences("tjcPrefrences", 0).edit();
    localEditor.putString("emulatorDeviceId", d);
    localEditor.commit();
  }

  public static void setUserID(String paramString)
  {
    o = paramString;
  }

  public void actionComplete(String paramString)
  {
    this.w = (this.w + "app_id=" + paramString + "&");
    this.w += d();
    this.w += "&";
    long l1 = System.currentTimeMillis() / 1000L;
    this.w = (this.w + "timestamp=" + l1 + "&");
    this.w = (this.w + "verifier=" + getVerifier(l1));
    bv.i("TapjoyConnect", "PPA URL parameters: " + this.w);
    new Thread(new Runnable()
    {
      public void run()
      {
        String str = bj.c().connectToURL("https://ws.tapjoyads.com/connect?", bj.c(bj.this));
        if (str != null)
          bj.b(bj.this, str);
      }
    }).start();
  }

  public void enablePaidAppWithActionID(String paramString)
  {
    bv.i("TapjoyConnect", "enablePaidAppWithActionID: " + paramString);
    x = paramString;
    this.y = a.getSharedPreferences("tjcPrefrences", 0).getLong("tapjoy_elapsed_time", 0L);
    bv.i("TapjoyConnect", "paidApp elapsed: " + this.y);
    if (this.y >= 900000L)
      if ((x != null) && (x.length() > 0))
      {
        bv.i("TapjoyConnect", "Calling PPA actionComplete...");
        actionComplete(x);
      }
    while (this.z != null)
      return;
    this.z = new Timer();
    this.z.schedule(new a(null), 10000L, 10000L);
  }

  public float getCurrencyMultiplier()
  {
    return v;
  }

  public void release()
  {
    b = null;
    c = null;
    bv.i("TapjoyConnect", "Releasing core static instance.");
  }

  public void setCurrencyMultiplier(float paramFloat)
  {
    bv.i("TapjoyConnect", "setVirtualCurrencyMultiplier: " + paramFloat);
    v = paramFloat;
  }

  private class a extends TimerTask
  {
    private a()
    {
    }

    public void run()
    {
      bj localbj = bj.this;
      bj.a(localbj, 10000L + bj.a(localbj));
      bv.i("TapjoyConnect", "elapsed_time: " + bj.a(bj.this) + " (" + bj.a(bj.this) / 1000L / 60L + "m " + bj.a(bj.this) / 1000L % 60L + "s)");
      SharedPreferences.Editor localEditor = bj.a().getSharedPreferences("tjcPrefrences", 0).edit();
      localEditor.putLong("tapjoy_elapsed_time", bj.a(bj.this));
      localEditor.commit();
      if (bj.a(bj.this) >= 900000L)
      {
        bv.i("TapjoyConnect", "timer done...");
        if ((bj.b() != null) && (bj.b().length() > 0))
        {
          bv.i("TapjoyConnect", "Calling PPA actionComplete...");
          bj.this.actionComplete(bj.b());
        }
        cancel();
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bj
 * JD-Core Version:    0.6.2
 */