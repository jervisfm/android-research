package com.swarmconnect;

import android.content.Context;
import android.util.Log;

final class bk
{
  public static final String TAPJOY_CONNECT = "TapjoyConnect";
  private static bk a = null;
  private static be b = null;
  private static am c = null;
  private static ag d = null;

  private bk(Context paramContext, String paramString1, String paramString2)
  {
    bj.requestTapjoyConnect(paramContext, paramString1, paramString2);
  }

  public static bk getTapjoyConnectInstance()
  {
    if (a == null)
    {
      Log.e("TapjoyConnect", "----------------------------------------");
      Log.e("TapjoyConnect", "ERROR -- call requestTapjoyConnect before any other Tapjoy methods");
      Log.e("TapjoyConnect", "----------------------------------------");
    }
    return a;
  }

  public static void requestTapjoyConnect(Context paramContext, String paramString1, String paramString2)
  {
    a = new bk(paramContext, paramString1, paramString2);
    b = new be(paramContext);
    c = new am(paramContext);
    d = new ag(paramContext);
  }

  public void actionComplete(String paramString)
  {
    bj.getInstance().actionComplete(paramString);
  }

  public void awardTapPoints(int paramInt, cl paramcl)
  {
    b.awardTapPoints(paramInt, paramcl);
  }

  public void enablePaidAppWithActionID(String paramString)
  {
    bj.getInstance().enablePaidAppWithActionID(paramString);
  }

  public String getAppID()
  {
    return bj.getAppID();
  }

  public float getCurrencyMultiplier()
  {
    return bj.getInstance().getCurrencyMultiplier();
  }

  public void getDisplayAd(by paramby)
  {
    d.getDisplayAd(paramby);
  }

  public void getFeaturedApp(x paramx)
  {
    c.getFeaturedApp(paramx);
  }

  public void getFeaturedAppWithCurrencyID(String paramString, x paramx)
  {
    c.getFeaturedApp(paramString, paramx);
  }

  public void getTapPoints(bc parambc)
  {
    b.getTapPoints(parambc);
  }

  public String getUserID()
  {
    return bj.getUserID();
  }

  public void setBannerAdSize(String paramString)
  {
    d.setBannerAdSize(paramString);
  }

  public void setCurrencyMultiplier(float paramFloat)
  {
    bj.getInstance().setCurrencyMultiplier(paramFloat);
  }

  public void setEarnedPointsNotifier(cr paramcr)
  {
    b.setEarnedPointsNotifier(paramcr);
  }

  public void setFeaturedAppDisplayCount(int paramInt)
  {
    c.setDisplayCount(paramInt);
  }

  public void setUserID(String paramString)
  {
    bj.setUserID(paramString);
  }

  public void showFeaturedAppFullScreenAd()
  {
    c.showFeaturedAppFullScreenAd();
  }

  public void showOffers()
  {
    b.showOffers();
  }

  public void showOffersWithCurrencyID(String paramString, boolean paramBoolean)
  {
    b.showOffersWithCurrencyID(paramString, paramBoolean);
  }

  public void spendTapPoints(int paramInt, f paramf)
  {
    b.spendTapPoints(paramInt, paramf);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bk
 * JD-Core Version:    0.6.2
 */