package com.swarmconnect;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class bm extends u
{
  final String l = "Offers";
  private WebView m = null;
  private String n = null;
  private String o = "";
  private String p = "";
  private String q = "";

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_webview"));
    this.m = ((WebView)a(a("@id/webview")));
    Bundle localBundle = this.c.getIntent().getExtras();
    if (localBundle != null)
    {
      this.p = localBundle.getString("URL_PARAMS");
      this.o = localBundle.getString("CLIENT_PACKAGE");
      this.q = localBundle.getString("USER_ID");
      this.p = (this.p + "&publisher_user_id=" + this.q);
      bv.i("Offers", "urlParams: [" + this.p + "]");
      bv.i("Offers", "clientPackage: [" + this.o + "]");
    }
    while (true)
    {
      this.n = ("https://ws.tapjoyads.com/get_offers/webpage?" + this.p);
      this.n = this.n.replaceAll(" ", "%20");
      this.m.setWebViewClient(new a(null));
      this.m.getSettings().setJavaScriptEnabled(true);
      this.m.loadUrl(this.n);
      this.m.setScrollBarStyle(0);
      bv.i("Offers", "Opening URL = [" + this.n + "]");
      super.onCreate(paramBundle);
      a(a("@drawable/swarm_coin_small"), "Offers by Tapjoy");
      return;
      bv.e("Offers", "Tapjoy offers meta data initialization fail.");
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.m != null)
    {
      this.m.clearCache(true);
      this.m.destroyDrawingCache();
      this.m.destroy();
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.m != null) && (this.m.canGoBack()))
    {
      this.m.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onResume()
  {
    super.onResume();
    if (Swarm.user == null);
    while ((this.n == null) || (this.m == null))
      return;
    this.m.loadUrl(this.n);
  }

  protected void reload()
  {
  }

  private class a extends WebViewClient
  {
    private a()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      bm.this.c();
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      bm.this.b();
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      bv.i("Offers", "URL = [" + paramString + "]");
      if (paramString.indexOf("market") > -1)
      {
        bv.i("Offers", "Market URL = [" + paramString + "]");
        AlertDialog localAlertDialog;
        try
        {
          String[] arrayOfString = paramString.split("q=");
          String str = "http://market.android.com/details?id=" + arrayOfString[1] + "&referrer=" + bm.a(bm.this);
          Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
          bm.this.c.startActivity(localIntent2);
          bv.i("Offers", "Open URL of application = [" + str + "]");
          return true;
        }
        catch (Exception localException1)
        {
          localAlertDialog = new AlertDialog.Builder(bm.this.c).setTitle("").setMessage("Android market is unavailable at this device. To view this link install market.").setPositiveButton("OK", new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              paramAnonymousDialogInterface.dismiss();
            }
          }).create();
        }
        try
        {
          localAlertDialog.show();
          bv.i("Offers", "Android market is unavailable at this device. To view this link install market.");
          return true;
        }
        catch (Exception localException2)
        {
          while (true)
            localException2.printStackTrace();
        }
      }
      if (paramString.contains("ws.tapjoyads.com"))
      {
        bv.i("Offers", "Open redirecting URL = [" + paramString + "]");
        paramWebView.loadUrl(paramString);
        return true;
      }
      bv.i("Offers", "Opening URL in new browser = [" + paramString + "]");
      Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
      bm.this.c.startActivity(localIntent1);
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bm
 * JD-Core Version:    0.6.2
 */