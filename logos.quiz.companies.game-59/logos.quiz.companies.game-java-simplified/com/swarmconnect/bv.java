package com.swarmconnect;

import android.util.Log;

class bv
{
  private static boolean a = false;

  public static void d(String paramString1, String paramString2)
  {
    if (a)
      Log.d(paramString1, paramString2);
  }

  public static void e(String paramString1, String paramString2)
  {
    if (a)
      Log.e(paramString1, paramString2);
  }

  public static void enableLogging(boolean paramBoolean)
  {
    Log.i("TapjoyLog", "enableLogging: " + paramBoolean);
    a = paramBoolean;
  }

  public static void i(String paramString1, String paramString2)
  {
    if (a)
      Log.i(paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2)
  {
    if (a)
      Log.v(paramString1, paramString2);
  }

  public static void w(String paramString1, String paramString2)
  {
    if (a)
      Log.w(paramString1, paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bv
 * JD-Core Version:    0.6.2
 */