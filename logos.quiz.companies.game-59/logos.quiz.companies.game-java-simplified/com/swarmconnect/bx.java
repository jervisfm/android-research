package com.swarmconnect;

import java.util.HashMap;

class bx
{
  private static final HashMap<Class<? extends APICall>, String> a = new HashMap();

  static
  {
    a.put(cj.class, "AddLeaderboardScoreAPI");
    a.put(cp.class, "AddVersionInfoAPI");
    a.put(cc.class, "ChangePasswordAPI");
    a.put(ab.class, "CheckUsernameAPI");
    a.put(aw.class, "ConsumeItemAPI");
    a.put(bu.class, "CreateAccountAPI");
    a.put(cn.class, "CreateGuestAccountAPI");
    a.put(bn.class, "DelMessageThreadAPI");
    a.put(w.class, "FacebookLoginAPI");
    a.put(br.class, "FriendRequestAPI");
    a.put(p.class, "GetAchievementsAPI");
    a.put(l.class, "GetAppsListAPI");
    a.put(aa.class, "GetChallengeAPI");
    a.put(GetDeviceAccountsAPI.class, "GetDeviceAccountsAPI");
    a.put(bt.class, "GetFriendNotificationAPI");
    a.put(GetFriendsAPI.class, "GetFriendsAPI");
    a.put(GetInventoryAPI.class, "GetInventoryAPI");
    a.put(bp.class, "GetLeaderboardAPI");
    a.put(b.class, "GetLeaderboardsAPI");
    a.put(cq.class, "GetLeaderboardScoreAPI");
    a.put(bs.class, "GetLeaderboardScoreDataAPI");
    a.put(aj.class, "GetMessagesAPI");
    a.put(bq.class, "GetMessageThreadsAPI");
    a.put(bh.class, "GetNumNewMessagesAPI");
    a.put(bl.class, "GetPushDataAPI");
    a.put(cf.class, "GetStoreAPI");
    a.put(bo.class, "GetUserCoinsAPI");
    a.put(co.class, "GetUserDataAPI");
    a.put(z.class, "GetUserStatusAPI");
    a.put(ce.class, "LoginAPI");
    a.put(n.class, "MessageThreadViewedAPI");
    a.put(h.class, "ModChallengeAPI");
    a.put(ci.class, "NotificationCheckAPI");
    a.put(bd.class, "PurchaseItemAPI");
    a.put(v.class, "ResetPasswordAPI");
    a.put(an.class, "SendChallengeAPI");
    a.put(ay.class, "SendMessageAPI");
    a.put(as.class, "SetUserDataAPI");
    a.put(y.class, "UnlockAchievementAPI");
    a.put(c.class, "UpdateStatusAPI");
    a.put(o.class, "UpgradeGuestAccountAPI");
    a.put(a.class, "UserSearchAPI");
  }

  protected static String a(Class<? extends APICall> paramClass)
  {
    return (String)a.get(paramClass);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bx
 * JD-Core Version:    0.6.2
 */