package com.swarmconnect;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import com.swarmconnect.utils.DeviceUtils;
import java.util.ArrayList;
import java.util.List;

class bz extends ba
{
  public static String loginUsername;
  private ListView m;
  private a n = new a(null);
  private TextView o;
  private EditText p;
  private EditText q;
  private ArrayList<GetDeviceAccountsAPI.ExternalSwarmUser> r = new ArrayList();

  private void e()
  {
    b();
    this.o.setText("");
    this.o.setVisibility(4);
    String str1 = this.p.getText().toString();
    String str2 = this.q.getText().toString();
    ce localce = new ce();
    localce.device = Swarm.e;
    localce.username = str1;
    localce.password = str2;
    localce.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        bz.this.a(new Runnable()
        {
          public void run()
          {
            bz.this.c();
            if (paramAnonymousAPICall.statusCode == 0)
            {
              bz.f(bz.this).setText(paramAnonymousAPICall.statusMessage);
              bz.f(bz.this).setVisibility(0);
              return;
            }
            ce localce = (ce)paramAnonymousAPICall;
            Swarm.a(localce.user, localce.auth, localce.newMessages);
            bz.a();
          }
        });
      }

      public void requestFailed()
      {
        bz.this.a(new Runnable()
        {
          public void run()
          {
            bz.this.c();
            Swarm.a();
            bz.a();
          }
        });
      }
    };
    localce.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_login"));
    this.p = ((EditText)a(a("@id/username")));
    this.p.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        bz.c(bz.this).notifyDataSetInvalidated();
      }

      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }

      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }
    });
    this.q = ((EditText)a(a("@id/password")));
    this.o = ((TextView)a(a("@id/username_error")));
    this.o.setTextColor(-7398867);
    ((Button)a(a("@id/login"))).setBackgroundDrawable(UiConf.blueButtonBackground());
    ((Button)a(a("@id/login"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        bz.d(bz.this);
      }
    });
    ((Button)a(a("@id/create"))).setBackgroundDrawable(UiConf.greyButtonBackground());
    ((Button)a(a("@id/create"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        cb.checkExistingAccounts = false;
        bz.this.show(11);
      }
    });
    ((TextView)a(a("@id/lost_password"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        bz.this.show(10);
      }
    });
    this.m = ((ListView)a(a("@id/list")));
    this.m.setAdapter(this.n);
    this.m.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        GetDeviceAccountsAPI.ExternalSwarmUser localExternalSwarmUser = bz.c(bz.this).getItem(paramAnonymousInt);
        if (localExternalSwarmUser != null)
        {
          if ((localExternalSwarmUser.provider != null) && (localExternalSwarmUser.provider.equalsIgnoreCase("facebook")))
            SwarmFacebook.login(bz.this.c, bz.this.l);
        }
        else
          return;
        bz.b(bz.this).setText(localExternalSwarmUser.username);
        bz.e(bz.this).requestFocus();
      }
    });
    super.onCreate(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    b();
    GetDeviceAccountsAPI localGetDeviceAccountsAPI = new GetDeviceAccountsAPI();
    localGetDeviceAccountsAPI.device = DeviceUtils.getDeviceId(this.c);
    localGetDeviceAccountsAPI.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        GetDeviceAccountsAPI localGetDeviceAccountsAPI = (GetDeviceAccountsAPI)paramAnonymousAPICall;
        if ((localGetDeviceAccountsAPI.users != null) && (localGetDeviceAccountsAPI.users.size() > 0))
        {
          bz.a(bz.this).clear();
          bz.a(bz.this).addAll(localGetDeviceAccountsAPI.users);
          bz.c(bz.this).notifyDataSetChanged();
          if ((bz.a(bz.this).size() == 1) && (bz.b(bz.this).getText().toString().trim().length() == 0))
          {
            SwarmUser localSwarmUser = (SwarmUser)bz.a(bz.this).get(0);
            bz.b(bz.this).setText(localSwarmUser.username);
          }
        }
        bz.this.c();
      }

      public void requestFailed()
      {
        bz.this.c();
      }
    };
    localGetDeviceAccountsAPI.run();
    String str = Swarm.getLastLogin();
    if (str != null)
      this.p.setText(str);
    while ((loginUsername == null) || (loginUsername.length() <= 0))
      return;
    this.p.setText(loginUsername);
    loginUsername = "";
    this.q.requestFocus();
  }

  protected void reload()
  {
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (bz.a(bz.this) == null)
        return 0;
      return bz.a(bz.this).size();
    }

    public GetDeviceAccountsAPI.ExternalSwarmUser getItem(int paramInt)
    {
      if ((bz.a(bz.this) == null) || (paramInt < 0) || (paramInt > bz.a(bz.this).size()))
        return null;
      return (GetDeviceAccountsAPI.ExternalSwarmUser)bz.a(bz.this).get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(bz.this.d(), bz.this.a("@layout/swarm_login_row"), null);
      String str = bz.b(bz.this).getText().toString();
      GetDeviceAccountsAPI.ExternalSwarmUser localExternalSwarmUser = getItem(paramInt);
      LinearLayout localLinearLayout;
      if (localExternalSwarmUser != null)
      {
        localLinearLayout = (LinearLayout)paramView.findViewById(bz.this.a("@id/row"));
        localLinearLayout.setBackgroundDrawable(UiConf.greyButtonBackground());
        ((TextView)paramView.findViewById(bz.this.a("@id/username"))).setText(localExternalSwarmUser.username);
        ((TextView)paramView.findViewById(bz.this.a("@id/points"))).setText(localExternalSwarmUser.points);
        if ((localExternalSwarmUser.provider == null) || (!localExternalSwarmUser.provider.equalsIgnoreCase("facebook")))
          break label238;
        ((ImageView)paramView.findViewById(bz.this.a("@id/external_provider"))).setImageResource(bz.this.a("@drawable/swarm_facebook_icon"));
        ((ImageView)paramView.findViewById(bz.this.a("@id/external_provider"))).setVisibility(0);
      }
      while (true)
      {
        if ((str != null) && (str.equalsIgnoreCase(localExternalSwarmUser.username)))
          localLinearLayout.setBackgroundDrawable(UiConf.greenButtonBackground());
        return paramView;
        label238: ((ImageView)paramView.findViewById(bz.this.a("@id/external_provider"))).setVisibility(8);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.bz
 * JD-Core Version:    0.6.2
 */