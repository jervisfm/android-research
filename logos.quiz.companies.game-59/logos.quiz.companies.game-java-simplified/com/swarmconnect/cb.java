package com.swarmconnect;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import com.swarmconnect.utils.DeviceUtils;
import java.util.List;

class cb extends ba
{
  public static boolean allowGuestAccounts = true;
  public static boolean checkExistingAccounts = true;
  private Button m;
  private EditText n;
  private TextView o;

  private void e()
  {
    b();
    GetDeviceAccountsAPI localGetDeviceAccountsAPI = new GetDeviceAccountsAPI();
    localGetDeviceAccountsAPI.device = DeviceUtils.getDeviceId(this.c);
    localGetDeviceAccountsAPI.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        cb.this.c();
        GetDeviceAccountsAPI localGetDeviceAccountsAPI = (GetDeviceAccountsAPI)paramAnonymousAPICall;
        if ((localGetDeviceAccountsAPI.users != null) && (localGetDeviceAccountsAPI.users.size() > 0))
        {
          cb.this.show(9);
          cb.this.finish();
        }
      }

      public void requestFailed()
      {
        cb.this.c();
      }
    };
    localGetDeviceAccountsAPI.run();
  }

  public void createAccount()
  {
    this.o.setVisibility(4);
    this.o.setText("");
    String str = this.n.getText().toString();
    b();
    ab localab = new ab();
    localab.username = str;
    localab.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        cb.this.a(new Runnable()
        {
          public void run()
          {
            cb.this.c();
            ab localab;
            if (paramAnonymousAPICall != null)
            {
              localab = (ab)paramAnonymousAPICall;
              if (localab.ok)
              {
                cb.c(cb.this).setVisibility(4);
                cb.c(cb.this).setText("");
                bi.username = localab.username;
                cb.this.show(2);
              }
            }
            else
            {
              return;
            }
            cb.c(cb.this).setVisibility(0);
            cb.c(cb.this).setText(localab.error);
          }
        });
      }

      public void requestFailed()
      {
        cb.this.c();
        Swarm.a();
        cb.a();
      }
    };
    localab.run();
  }

  public void createGuestAccount()
  {
    this.o.setVisibility(4);
    this.o.setText("");
    b();
    cn localcn = new cn();
    localcn.cb = new APICall.APICallback()
    {
      public void gotAPI(final APICall paramAnonymousAPICall)
      {
        cb.this.a(new Runnable()
        {
          public void run()
          {
            cb.this.c();
            cn localcn;
            if (paramAnonymousAPICall != null)
            {
              localcn = (cn)paramAnonymousAPICall;
              if (localcn.user != null)
              {
                Swarm.a(localcn.user, localcn.auth);
                cb.a();
              }
            }
            else
            {
              return;
            }
            cb.c(cb.this).setVisibility(0);
            cb.c(cb.this).setText(localcn.statusMessage);
          }
        });
      }

      public void requestFailed()
      {
        cb.this.c();
        Swarm.a();
        cb.a();
      }
    };
    localcn.run();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Object localObject = "";
    try
    {
      String str = this.n.getText().toString();
      localObject = str;
      label20: onCreate(null);
      if ((this.n != null) && (localObject != null))
        this.n.setText((CharSequence)localObject);
      return;
    }
    catch (Exception localException)
    {
      break label20;
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_select_username"));
    ((TextView)a(a("@id/terms"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW");
        localIntent.setData(Uri.parse("http://api.swarmconnect.com//privacy.html"));
        cb.this.c.startActivity(localIntent);
      }
    });
    this.n = ((EditText)a(a("@id/username")));
    this.m = ((Button)a(a("@id/create")));
    this.m.setBackgroundDrawable(UiConf.greenButtonBackground());
    this.m.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        cb.this.createAccount();
      }
    });
    ((Button)a(a("@id/existing"))).setBackgroundDrawable(UiConf.greyButtonBackground());
    ((Button)a(a("@id/existing"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        String str = cb.a(cb.this).getText().toString();
        if ((str != null) && (str.length() > 0))
          bz.loginUsername = str;
        cb.this.show(9);
      }
    });
    if (allowGuestAccounts)
    {
      ((Button)a(a("@id/guest"))).setBackgroundDrawable(UiConf.blueButtonBackground());
      ((Button)a(a("@id/guest"))).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          cb.this.createGuestAccount();
        }
      });
    }
    while (true)
    {
      this.o = ((TextView)a(a("@id/username_error")));
      this.o.setTextColor(-7398867);
      super.onCreate(paramBundle);
      return;
      ((Button)a(a("@id/guest"))).setVisibility(8);
    }
  }

  public void onResume()
  {
    super.onResume();
    Swarm.autoLogin(new Swarm.a()
    {
      public void loginFailed()
      {
        if (cb.checkExistingAccounts)
          cb.b(cb.this);
      }

      public void loginSuccess()
      {
        cb.a();
      }
    });
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.cb
 * JD-Core Version:    0.6.2
 */