package com.swarmconnect;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class cd extends u
{
  private WebView l = null;

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_webview"));
    this.l = ((WebView)a(a("@id/webview")));
    this.l.setWebViewClient(new a(null));
    this.l.getSettings().setJavaScriptEnabled(true);
    this.l.setScrollBarStyle(0);
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_coin_small"), "Purchase Securely with Paypal");
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.l != null)
    {
      this.l.clearCache(true);
      this.l.destroyDrawingCache();
      this.l.destroy();
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.l != null) && (this.l.canGoBack()) && (!this.l.getUrl().contains("process_paypal.php")))
    {
      this.l.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onResume()
  {
    super.onResume();
    if (Swarm.user == null);
    while (this.l == null)
      return;
    b("Loading: http://www.swarmconnect.com/coins/getcoins.php?userId=" + Swarm.user.userId + "&app_id=" + Swarm.c);
    this.l.loadUrl("http://www.swarmconnect.com/coins/getcoins.php?userId=" + Swarm.user.userId + "&app_id=" + Swarm.c);
    this.l.setBackgroundColor(0);
  }

  protected void reload()
  {
  }

  private class a extends WebViewClient
  {
    private a()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      cd.this.c();
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      cd.this.b();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.cd
 * JD-Core Version:    0.6.2
 */