package com.swarmconnect;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

class cg
{
  private Context a;
  private Configuration b;
  private DisplayMetrics c;

  public cg(Context paramContext)
  {
    this.a = paramContext;
    this.c = new DisplayMetrics();
    ((WindowManager)this.a.getSystemService("window")).getDefaultDisplay().getMetrics(this.c);
    this.b = this.a.getResources().getConfiguration();
  }

  public int getScreenDensity()
  {
    return this.c.densityDpi;
  }

  public int getScreenLayoutSize()
  {
    return 0xF & this.b.screenLayout;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.cg
 * JD-Core Version:    0.6.2
 */