package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.swarmconnect.ui.UiConf;

class ck extends u
{
  private LinearLayout l;
  private LinearLayout m;
  private LinearLayout n;

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_get_coins"));
    this.l = ((LinearLayout)a(a("@id/paypal")));
    this.l.setBackgroundDrawable(UiConf.coinsProviderButtonBackground());
    this.m = ((LinearLayout)a(a("@id/tapjoy")));
    this.m.setBackgroundDrawable(UiConf.coinsProviderButtonBackground());
    this.n = ((LinearLayout)a(a("@id/zong")));
    this.n.setBackgroundDrawable(UiConf.coinsProviderButtonBackground());
    this.l.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        ck.this.show(20);
      }
    });
    this.m.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        bk.requestTapjoyConnect(ck.this.c, "8cce3878-0cbe-4c2f-8253-23fcaa829ab0", "6NGZxbKtaSkdQBh9DLaK");
        bk.getTapjoyConnectInstance().setUserID(Swarm.user.userId + "_" + Swarm.c);
        bk.getTapjoyConnectInstance().showOffers();
      }
    });
    if (u.b)
      this.m.setVisibility(8);
    this.n.setVisibility(8);
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_coin_small"), "Select a coin provider");
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ck
 * JD-Core Version:    0.6.2
 */