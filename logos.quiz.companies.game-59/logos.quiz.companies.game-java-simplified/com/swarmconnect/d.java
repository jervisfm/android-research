package com.swarmconnect;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.swarmconnect.ui.UiConf;
import com.swarmconnect.utils.Text;

class d extends ba
{
  private EditText m;
  private Button n;
  private TextView o;

  private void e()
  {
    b();
    String str = this.m.getText().toString();
    v localv = new v();
    localv.email = str;
    localv.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        d.this.a(new Runnable()
        {
          public void run()
          {
            d.this.c();
            d.c(d.this).setVisibility(8);
            d.d(d.this).setVisibility(0);
          }
        });
      }

      public void requestFailed()
      {
        d.this.c();
        Swarm.a();
        d.a();
      }
    };
    localv.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_lost_password"));
    this.m = ((EditText)a(a("@id/email")));
    this.o = ((TextView)a(a("@id/success")));
    this.n = ((Button)a(a("@id/submit")));
    this.n.setBackgroundDrawable(UiConf.blueButtonBackground());
    this.n.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (d.a(d.this).getText().length() > 0)
        {
          if (Text.isEmailValid(d.a(d.this).getText().toString()))
          {
            d.b(d.this);
            return;
          }
          Toast.makeText(d.this.c, "Please enter a valid email address", 0).show();
          return;
        }
        Toast.makeText(d.this.c, "Please enter your email address", 0).show();
      }
    });
    super.onCreate(paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    this.n.setVisibility(0);
    this.o.setVisibility(4);
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.d
 * JD-Core Version:    0.6.2
 */