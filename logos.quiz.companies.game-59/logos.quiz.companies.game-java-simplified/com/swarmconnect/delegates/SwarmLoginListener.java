package com.swarmconnect.delegates;

import com.swarmconnect.SwarmActiveUser;

public abstract interface SwarmLoginListener
{
  public abstract void loginCanceled();

  public abstract void loginStarted();

  public abstract void userLoggedIn(SwarmActiveUser paramSwarmActiveUser);

  public abstract void userLoggedOut();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.delegates.SwarmLoginListener
 * JD-Core Version:    0.6.2
 */