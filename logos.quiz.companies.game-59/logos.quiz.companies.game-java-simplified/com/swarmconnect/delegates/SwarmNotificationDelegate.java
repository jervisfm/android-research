package com.swarmconnect.delegates;

import com.swarmconnect.SwarmNotification;

public abstract interface SwarmNotificationDelegate
{
  public abstract boolean gotNotification(SwarmNotification paramSwarmNotification);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.delegates.SwarmNotificationDelegate
 * JD-Core Version:    0.6.2
 */