package com.swarmconnect;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

class g extends u
{
  private a l = new a(null);
  private List<SwarmMessageThread> m = new ArrayList();
  private TextView n;
  private AlertDialog o;

  private void e()
  {
    if (this.o != null)
    {
      this.o.dismiss();
      this.o = null;
    }
  }

  public void getThreads()
  {
    b();
    SwarmMessageThread.getAllThreads(new SwarmMessageThread.GotThreadsCB()
    {
      public void gotThreads(List<SwarmMessageThread> paramAnonymousList)
      {
        if ((paramAnonymousList == null) || (paramAnonymousList.size() == 0))
          g.e(g.this).setVisibility(0);
        while (true)
        {
          g.a(g.this, paramAnonymousList);
          g.b(g.this).notifyDataSetChanged();
          g.this.c();
          return;
          g.e(g.this).setVisibility(8);
        }
      }
    });
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_list"));
    this.n = ((TextView)a(a("@id/empty_list")));
    this.n.setText("\n\n\nYou don't have any Swarm messages yet!  Make some friends and start chatting.");
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        SwarmMessageThread localSwarmMessageThread = (SwarmMessageThread)g.b(g.this).getItem(paramAnonymousInt);
        if (localSwarmMessageThread != null)
        {
          localSwarmMessageThread.markRead();
          Intent localIntent = g.this.c(14);
          localIntent.putExtra("thread", localSwarmMessageThread);
          g.this.c.startActivity(localIntent);
        }
      }
    });
    localListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
    {
      public boolean onItemLongClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        final SwarmMessageThread localSwarmMessageThread = (SwarmMessageThread)g.b(g.this).getItem(paramAnonymousInt);
        if (localSwarmMessageThread != null)
        {
          g.c(g.this);
          g.a(g.this, new AlertDialog.Builder(g.this.c).setTitle("Delete Thread?").setMessage("Delete messages from " + localSwarmMessageThread.otherUser.username + "?").setPositiveButton("Delete", new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              g.this.b();
              bn localbn = new bn();
              localbn.threadId = localSwarmMessageThread.id;
              localbn.cb = new APICall.APICallback()
              {
                public void gotAPI(APICall paramAnonymous3APICall)
                {
                  g.this.c();
                  g.this.reload();
                }
              };
              localbn.run();
            }
          }).setNegativeButton("Cancel", null).create());
          g.d(g.this).show();
        }
        return true;
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_messages"), "Message Inbox");
  }

  public void onPause()
  {
    super.onPause();
    e();
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        g.this.getThreads();
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (g.a(g.this) == null)
        return 0;
      return g.a(g.this).size();
    }

    public Object getItem(int paramInt)
    {
      if ((paramInt >= 0) && (paramInt >= 0) && (paramInt < g.a(g.this).size()))
        return g.a(g.this).get(paramInt);
      return null;
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(g.this.d(), g.this.a("@layout/swarm_inbox_row"), null);
      int i;
      SwarmMessageThread localSwarmMessageThread;
      if (paramInt % 2 == 0)
      {
        i = 16777215;
        paramView.setBackgroundColor(i);
        localSwarmMessageThread = (SwarmMessageThread)getItem(paramInt);
        if (localSwarmMessageThread != null)
        {
          if (!localSwarmMessageThread.viewed)
            break label156;
          ((ImageView)paramView.findViewById(g.this.a("@id/mail"))).setImageResource(g.this.a("@drawable/swarm_mail_grey"));
        }
      }
      while (true)
      {
        SwarmUser localSwarmUser = localSwarmMessageThread.otherUser;
        ((TextView)paramView.findViewById(g.this.a("@id/username"))).setText(localSwarmUser.username);
        ((TextView)paramView.findViewById(g.this.a("@id/message"))).setText(localSwarmMessageThread.lastMessage);
        return paramView;
        i = -1996488705;
        break;
        label156: ((ImageView)paramView.findViewById(g.this.a("@id/mail"))).setImageResource(g.this.a("@drawable/swarm_mail"));
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.g
 * JD-Core Version:    0.6.2
 */