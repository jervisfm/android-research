package com.swarmconnect;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.swarmconnect.ui.UiConf;

class i extends u
{
  private TextView l;
  private LinearLayout m;
  private LinearLayout n;
  private LinearLayout o;
  private Button p;
  private Button q;
  private Button r;
  private CheckBox s;
  private TextView t;
  private EditText u;
  private EditText v;
  private EditText w;

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_settings"));
    this.l = ((TextView)a(a("@id/username")));
    this.m = ((LinearLayout)a(a("@id/upgrade_box")));
    this.n = ((LinearLayout)a(a("@id/password_box")));
    this.o = ((LinearLayout)a(a("@id/set_password_box")));
    this.p = ((Button)a(a("@id/upgrade")));
    this.q = ((Button)a(a("@id/change_pass")));
    this.r = ((Button)a(a("@id/set_pass")));
    this.t = ((TextView)a(a("@id/password_error")));
    this.u = ((EditText)a(a("@id/current_password")));
    this.v = ((EditText)a(a("@id/new_password")));
    this.w = ((EditText)a(a("@id/confirm_password")));
    this.t.setTextColor(-7398867);
    this.s = ((CheckBox)a(a("@id/notifications")));
    this.s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
      {
        if (paramAnonymousBoolean)
          SwarmIO.enablePush();
        while (true)
        {
          Swarm.user.saveCloudData("pushEnabled", paramAnonymousBoolean, null);
          return;
          SwarmIO.disablePush();
        }
      }
    });
    this.p.setBackgroundDrawable(UiConf.greyButtonBackground());
    this.p.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Swarm.user.upgradeGuest();
      }
    });
    this.q.setBackgroundDrawable(UiConf.greyButtonBackground());
    this.q.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        i.a(i.this).setVisibility(0);
        i.b(i.this).setVisibility(8);
        i.c(i.this).setText("");
        i.d(i.this).setText("");
        i.e(i.this).setText("");
        i.f(i.this).setText("");
      }
    });
    this.r.setBackgroundDrawable(UiConf.greyButtonBackground());
    this.r.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        String str1 = i.c(i.this).getText().toString();
        String str2 = i.d(i.this).getText().toString();
        String str3 = i.e(i.this).getText().toString();
        if ((str1 != null) && (str1.length() > 0) && (str2 != null) && (str2.length() > 0) && (str3 != null) && (str3.length() > 0))
        {
          if (str2.equals(str3))
          {
            i.this.b();
            cc localcc = new cc();
            localcc.currentPass = str1;
            localcc.newPass = str2;
            localcc.cb = new APICall.APICallback()
            {
              public void gotAPI(APICall paramAnonymous2APICall)
              {
                cc localcc = (cc)paramAnonymous2APICall;
                if (localcc.ok)
                {
                  Toast.makeText(i.this.c, "Password changed", 0).show();
                  i.a(i.this).setVisibility(8);
                  i.b(i.this).setVisibility(0);
                }
                while (true)
                {
                  i.this.c();
                  return;
                  if ((localcc.error != null) && (localcc.error.length() > 0))
                    i.f(i.this).setText(localcc.error);
                  else
                    i.f(i.this).setText("Error saving new password.");
                }
              }

              public void requestFailed()
              {
                i.f(i.this).setText("Error saving new password.");
                i.this.c();
              }
            };
            localcc.run();
            return;
          }
          i.f(i.this).setText("New passwords do not match.");
          return;
        }
        i.f(i.this).setText("All fields are required.");
      }
    });
    ((Button)a(a("@id/logout"))).setBackgroundDrawable(UiConf.greyButtonBackground());
    ((Button)a(a("@id/logout"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if ((Swarm.user != null) && (!Swarm.user.isOfflineGuest()))
        {
          i.this.clearStack();
          i.a();
          Swarm.logOut();
        }
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_settings"), "Settings");
  }

  public void onResume()
  {
    super.onResume();
    if (Swarm.user == null)
      return;
    this.l.setText(Swarm.user.username);
    LinearLayout localLinearLayout1 = this.m;
    if (Swarm.user.isGuestAccount());
    for (int i = 0; ; i = 8)
    {
      localLinearLayout1.setVisibility(i);
      LinearLayout localLinearLayout2 = this.n;
      boolean bool = Swarm.user.isGuestAccount();
      int j = 0;
      if (bool)
        j = 8;
      localLinearLayout2.setVisibility(j);
      this.o.setVisibility(8);
      this.s.setChecked(SwarmIO.getPushEnabled(this.c));
      return;
    }
  }

  protected void reload()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.i
 * JD-Core Version:    0.6.2
 */