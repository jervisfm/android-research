package com.swarmconnect;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;

class k extends u
{
  private a l = new a(null);
  private List<SwarmLeaderboard> m;

  private void e()
  {
    b();
    b localb = new b();
    localb.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        k.this.c();
        b localb = (b)paramAnonymousAPICall;
        if (localb.leaderboards != null)
        {
          k.a(k.this, localb.leaderboards);
          k.b(k.this).notifyDataSetChanged();
          k.this.c();
          if ((k.a(k.this) != null) && (k.a(k.this).size() == 1))
          {
            k.this.a((SwarmLeaderboard)k.a(k.this).get(0));
            k.this.finish();
          }
        }
      }

      public void requestFailed()
      {
        k.this.c();
      }
    };
    localb.run();
  }

  protected void a(SwarmLeaderboard paramSwarmLeaderboard)
  {
    Intent localIntent = c(7);
    localIntent.putExtra("leaderboard", paramSwarmLeaderboard);
    this.c.startActivity(localIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_list"));
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        SwarmLeaderboard localSwarmLeaderboard = k.b(k.this).getItem(paramAnonymousInt);
        if (localSwarmLeaderboard != null)
          k.this.a(localSwarmLeaderboard);
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_leaderboards"), "Leaderboards");
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        k.c(k.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (k.a(k.this) == null)
        return 0;
      return k.a(k.this).size();
    }

    public SwarmLeaderboard getItem(int paramInt)
    {
      if ((k.a(k.this) == null) || (paramInt > k.a(k.this).size()))
        return null;
      return (SwarmLeaderboard)k.a(k.this).get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(k.this.d(), k.this.a("@layout/swarm_store_row"), null);
      SwarmLeaderboard localSwarmLeaderboard = getItem(paramInt);
      if (localSwarmLeaderboard != null)
        if (paramInt % 2 != 0)
          break label79;
      label79: for (int i = 16777215; ; i = -1996488705)
      {
        paramView.setBackgroundColor(i);
        ((TextView)paramView.findViewById(k.this.a("@id/name"))).setText(localSwarmLeaderboard.name);
        return paramView;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.k
 * JD-Core Version:    0.6.2
 */