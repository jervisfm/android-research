package com.swarmconnect;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.AsyncImageView;
import com.swarmconnect.ui.UiConf;
import java.util.List;

class m extends u
{
  public static SwarmStoreCategory category = null;
  private a l = new a(null);
  private SwarmUserInventory m;
  private TextView n;

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_store"));
    this.n = ((TextView)a(a("@id/coins")));
    ((TextView)a(a("@id/get_coins"))).setBackgroundDrawable(UiConf.greenButtonBackground());
    ((TextView)a(a("@id/get_coins"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Swarm.showGetCoins();
      }
    });
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        SwarmStoreListing localSwarmStoreListing = m.b(m.this).getItem(paramAnonymousInt);
        if (localSwarmStoreListing != null)
          localSwarmStoreListing.purchase(m.this.c, new SwarmStoreListing.ItemPurchaseCB()
          {
            public void purchaseFailed(int paramAnonymous2Int)
            {
            }

            public void purchaseSuccess()
            {
            }
          });
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_store"), "Store");
  }

  protected void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        if (Swarm.user != null)
        {
          Swarm.user.getCoins(new SwarmActiveUser.GotUserCoinsCB()
          {
            public void gotCoins(int paramAnonymous2Int)
            {
              m.c(m.this).setText(paramAnonymous2Int);
            }
          });
          Swarm.user.getInventory(new SwarmActiveUser.GotInventoryCB()
          {
            public void gotInventory(SwarmUserInventory paramAnonymous2SwarmUserInventory)
            {
              m.a(m.this, paramAnonymous2SwarmUserInventory);
              m.b(m.this).notifyDataSetChanged();
            }
          });
          m.b(m.this).notifyDataSetChanged();
        }
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if ((m.category == null) || (m.category.listings == null))
        return 0;
      return m.category.listings.size();
    }

    public SwarmStoreListing getItem(int paramInt)
    {
      if ((m.category == null) || (m.category.listings == null) || (paramInt > m.category.listings.size()))
        return null;
      return (SwarmStoreListing)m.category.listings.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(m.this.d(), m.this.a("@layout/swarm_store_category_row"), null);
      SwarmStoreListing localSwarmStoreListing = getItem(paramInt);
      int i;
      if (localSwarmStoreListing != null)
      {
        if (paramInt % 2 != 0)
          break label286;
        i = 16777215;
        paramView.setBackgroundColor(i);
        ((AsyncImageView)paramView.findViewById(m.this.a("@id/pic"))).setScaleType(ImageView.ScaleType.CENTER_CROP);
        ((AsyncImageView)paramView.findViewById(m.this.a("@id/pic"))).getUrl(localSwarmStoreListing.imageUrl);
        ((TextView)paramView.findViewById(m.this.a("@id/name"))).setText(localSwarmStoreListing.title);
        if ((localSwarmStoreListing.item == null) || (localSwarmStoreListing.item.description == null) || (localSwarmStoreListing.item.description.length() <= 0))
          break label293;
        ((TextView)paramView.findViewById(m.this.a("@id/description"))).setVisibility(0);
        ((TextView)paramView.findViewById(m.this.a("@id/description"))).setText(localSwarmStoreListing.item.description);
      }
      while (true)
      {
        if ((m.a(m.this) == null) || (!m.a(m.this).containsItem(localSwarmStoreListing.item)) || (localSwarmStoreListing.item.consumable))
          break label317;
        ((ImageView)paramView.findViewById(m.this.a("@id/coins"))).setVisibility(8);
        ((TextView)paramView.findViewById(m.this.a("@id/price"))).setText("");
        return paramView;
        label286: i = -1996488705;
        break;
        label293: ((TextView)paramView.findViewById(m.this.a("@id/description"))).setVisibility(8);
      }
      label317: ((ImageView)paramView.findViewById(m.this.a("@id/coins"))).setVisibility(0);
      ((TextView)paramView.findViewById(m.this.a("@id/price"))).setText(localSwarmStoreListing.price);
      return paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.m
 * JD-Core Version:    0.6.2
 */