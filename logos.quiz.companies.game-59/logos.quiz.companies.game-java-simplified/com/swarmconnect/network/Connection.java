package com.swarmconnect.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Connection
{
  private InputStream a = null;
  private OutputStream b = null;
  private byte[] c = new byte[2];

  public Connection(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    this.a = paramInputStream;
    this.b = paramOutputStream;
  }

  public byte[] receiveMessage()
    throws IOException
  {
    int i = 0;
    int k;
    while (true)
    {
      if (i >= 2)
      {
        k = (0xFF & this.c[0]) << 8 | 0xFF & this.c[1];
        if (k <= 16384)
          break;
        throw new IOException("Packet too large: " + k);
      }
      int j = this.a.read(this.c, i, 2 - i);
      if (j == -1)
        throw new IOException("End of Stream (in header)");
      i += j;
    }
    byte[] arrayOfByte = new byte[k];
    while (true)
    {
      if (k <= 0)
        return arrayOfByte;
      int m = this.a.read(arrayOfByte, arrayOfByte.length - k, k);
      if (m == -1)
        throw new IOException("End of Stream (in packet)");
      k -= m;
    }
  }

  public void sendMessage(byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      if ((this.b != null) && (paramArrayOfByte != null))
      {
        this.b.write(paramArrayOfByte.length >> 8);
        this.b.write(paramArrayOfByte.length);
        this.b.write(paramArrayOfByte);
      }
      return;
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.network.Connection
 * JD-Core Version:    0.6.2
 */