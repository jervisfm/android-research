package com.swarmconnect.network;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ConnectionManager
{
  private NetworkHandler a;
  private String b;
  private int c;
  public boolean connected = false;
  private final Socket d = new Socket();
  private Object e = new Object();
  private boolean f = false;
  private Throwable g = null;
  private Connection h;
  private Thread i;

  public ConnectionManager(String paramString, int paramInt, NetworkHandler paramNetworkHandler)
    throws IOException
  {
    this.b = paramString;
    this.c = paramInt;
    this.a = paramNetworkHandler;
  }

  private void a(int paramInt)
    throws IOException
  {
    this.d.connect(new InetSocketAddress(this.b, this.c), paramInt);
    this.d.setSoTimeout(0);
  }

  public void close(Throwable paramThrowable)
  {
    this.g = paramThrowable;
    try
    {
      this.d.close();
      label12: synchronized (this.e)
      {
        this.f = true;
        this.e.notifyAll();
      }
      try
      {
        if (this.a != null)
          this.a.onConnectionLost(this, this.g);
        this.connected = false;
        return;
        localObject2 = finally;
        throw localObject2;
      }
      finally
      {
      }
    }
    catch (IOException localIOException)
    {
      break label12;
    }
  }

  public Throwable getCloseReason()
  {
    synchronized (this.e)
    {
      Throwable localThrowable = this.g;
      return localThrowable;
    }
  }

  public void initialize(int paramInt)
    throws IOException
  {
    a(paramInt);
    this.h = new Connection(this.d.getInputStream(), this.d.getOutputStream());
    this.i = new Thread(new Runnable()
    {
      public void run()
      {
        try
        {
          ConnectionManager.this.receiveLoop();
          return;
        }
        catch (IOException localIOException)
        {
          ConnectionManager.this.close(localIOException);
        }
      }
    });
    this.i.setDaemon(true);
    this.i.start();
    if (this.a != null)
    {
      this.connected = true;
      setKeepAlive(true);
      setSoTimeout(15000);
      setTcpNoDelay(true);
      this.a.onConnected(this);
    }
  }

  public void receiveLoop()
    throws IOException
  {
    while (true)
    {
      byte[] arrayOfByte = this.h.receiveMessage();
      if (arrayOfByte == null)
        throw new IOException("Peer sent DISCONNECT");
      if ((0xFF & arrayOfByte[0]) == 0)
        throw new IOException("Server sent Disconnect Packet");
      if (this.a != null)
        this.a.onPacketReceived(0xFF & arrayOfByte[0], arrayOfByte);
    }
  }

  // ERROR //
  public void sendBytes(byte[] paramArrayOfByte)
    throws IOException
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 39	com/swarmconnect/network/ConnectionManager:e	Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield 41	com/swarmconnect/network/ConnectionManager:f	Z
    //   11: ifne +10 -> 21
    //   14: aload_0
    //   15: getfield 95	com/swarmconnect/network/ConnectionManager:h	Lcom/swarmconnect/network/Connection;
    //   18: ifnonnull +28 -> 46
    //   21: new 27	java/io/IOException
    //   24: dup
    //   25: ldc 144
    //   27: invokespecial 134	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   30: aload_0
    //   31: getfield 43	com/swarmconnect/network/ConnectionManager:g	Ljava/lang/Throwable;
    //   34: invokevirtual 148	java/io/IOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   37: checkcast 27	java/io/IOException
    //   40: athrow
    //   41: astore_3
    //   42: aload_2
    //   43: monitorexit
    //   44: aload_3
    //   45: athrow
    //   46: aload_0
    //   47: getfield 95	com/swarmconnect/network/ConnectionManager:h	Lcom/swarmconnect/network/Connection;
    //   50: aload_1
    //   51: invokevirtual 151	com/swarmconnect/network/Connection:sendMessage	([B)V
    //   54: aload_2
    //   55: monitorexit
    //   56: return
    //   57: astore 4
    //   59: aload_0
    //   60: aload 4
    //   62: invokevirtual 153	com/swarmconnect/network/ConnectionManager:close	(Ljava/lang/Throwable;)V
    //   65: aload 4
    //   67: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   7	21	41	finally
    //   21	41	41	finally
    //   42	44	41	finally
    //   46	54	41	finally
    //   54	56	41	finally
    //   59	68	41	finally
    //   46	54	57	java/io/IOException
  }

  public boolean sendPacket(byte[] paramArrayOfByte)
  {
    try
    {
      sendBytes(paramArrayOfByte);
      return true;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
      close(new Throwable("Error in ConnectionManager.sendPacket: " + localIOException.getCause()));
    }
    return false;
  }

  public void setKeepAlive(boolean paramBoolean)
    throws IOException
  {
    this.d.setKeepAlive(paramBoolean);
  }

  public void setSoTimeout(int paramInt)
    throws IOException
  {
    this.d.setSoTimeout(paramInt);
  }

  public void setTcpNoDelay(boolean paramBoolean)
    throws IOException
  {
    this.d.setTcpNoDelay(paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.network.ConnectionManager
 * JD-Core Version:    0.6.2
 */