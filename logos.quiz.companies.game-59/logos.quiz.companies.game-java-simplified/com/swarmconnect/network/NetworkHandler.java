package com.swarmconnect.network;

import java.io.IOException;

public abstract interface NetworkHandler
{
  public abstract void onConnected(ConnectionManager paramConnectionManager);

  public abstract void onConnectionLost(ConnectionManager paramConnectionManager, Throwable paramThrowable);

  public abstract void onPacketReceived(int paramInt, byte[] paramArrayOfByte)
    throws IOException;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.network.NetworkHandler
 * JD-Core Version:    0.6.2
 */