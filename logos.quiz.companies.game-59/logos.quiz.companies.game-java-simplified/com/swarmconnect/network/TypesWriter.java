package com.swarmconnect.network;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public class TypesWriter
{
  byte[] a;
  int b;
  boolean c = false;

  public TypesWriter()
  {
    this.a = new byte[256];
    this.b = 0;
  }

  public TypesWriter(int paramInt)
  {
    this.a = new byte[paramInt];
    this.b = 0;
  }

  public TypesWriter(byte[] paramArrayOfByte)
  {
    this.a = paramArrayOfByte;
    this.b = 0;
    this.c = true;
  }

  private void a(int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    System.arraycopy(this.a, 0, arrayOfByte, 0, this.a.length);
    this.a = arrayOfByte;
  }

  public void getBytes(byte[] paramArrayOfByte)
  {
    System.arraycopy(this.a, 0, paramArrayOfByte, 0, this.b);
  }

  public byte[] getBytes()
  {
    if (this.c)
      return this.a;
    byte[] arrayOfByte = new byte[this.b];
    System.arraycopy(this.a, 0, arrayOfByte, 0, this.b);
    return arrayOfByte;
  }

  public int length()
  {
    return this.b;
  }

  public void writeBoolean(boolean paramBoolean)
  {
    if (1 + this.b > this.a.length)
      a(256 + this.a.length);
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    if (paramBoolean);
    for (int j = 1; ; j = 0)
    {
      arrayOfByte[i] = j;
      return;
    }
  }

  public void writeBoolean(boolean paramBoolean, int paramInt)
  {
    if (paramInt + 1 > this.a.length)
      a(256 + this.a.length);
    byte[] arrayOfByte = this.a;
    if (paramBoolean);
    for (int i = 1; ; i = 0)
    {
      arrayOfByte[paramInt] = i;
      return;
    }
  }

  public void writeByte(int paramInt)
  {
    writeByte(paramInt, this.b);
    this.b = (1 + this.b);
  }

  public void writeByte(int paramInt1, int paramInt2)
  {
    if (paramInt2 + 1 > this.a.length)
      a(paramInt2 + 256);
    this.a[paramInt2] = ((byte)paramInt1);
  }

  public void writeBytes(byte[] paramArrayOfByte)
  {
    writeBytes(paramArrayOfByte, 0, paramArrayOfByte.length);
  }

  public void writeBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.b > this.a.length)
      a(256 + (paramInt2 + this.a.length));
    System.arraycopy(paramArrayOfByte, paramInt1, this.a, this.b, paramInt2);
    this.b = (paramInt2 + this.b);
  }

  public void writeFloat(float paramFloat)
  {
    writeUINT32(Float.floatToIntBits(paramFloat), this.b);
    this.b = (4 + this.b);
  }

  public void writeFloat(float paramFloat, int paramInt)
  {
    writeUINT32(Float.floatToIntBits(paramFloat), paramInt);
  }

  public void writeIntArray(int[] paramArrayOfInt)
  {
    writeUINT32(paramArrayOfInt.length);
    for (int i = 0; ; i++)
    {
      if (i >= paramArrayOfInt.length)
        return;
      writeUINT32(paramArrayOfInt[i]);
    }
  }

  public void writeMPInt(BigInteger paramBigInteger)
  {
    byte[] arrayOfByte = paramBigInteger.toByteArray();
    if ((arrayOfByte.length == 1) && (arrayOfByte[0] == 0))
    {
      writeUINT32(0);
      return;
    }
    writeString(arrayOfByte, 0, arrayOfByte.length);
  }

  public void writeNameList(String[] paramArrayOfString)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    for (int i = 0; ; i++)
    {
      if (i >= paramArrayOfString.length)
      {
        writeString(localStringBuffer.toString());
        return;
      }
      if (i > 0)
        localStringBuffer.append(',');
      localStringBuffer.append(paramArrayOfString[i]);
    }
  }

  public void writeString(String paramString)
  {
    try
    {
      byte[] arrayOfByte2 = paramString.getBytes("ISO-8859-1");
      arrayOfByte1 = arrayOfByte2;
      writeUINT32(arrayOfByte1.length);
      writeBytes(arrayOfByte1, 0, arrayOfByte1.length);
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      while (true)
        byte[] arrayOfByte1 = paramString.getBytes();
    }
  }

  public void writeString(String paramString1, String paramString2)
    throws UnsupportedEncodingException
  {
    if (paramString2 == null);
    for (byte[] arrayOfByte = paramString1.getBytes(); ; arrayOfByte = paramString1.getBytes(paramString2))
    {
      writeUINT32(arrayOfByte.length);
      writeBytes(arrayOfByte, 0, arrayOfByte.length);
      return;
    }
  }

  public void writeString(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    writeUINT32(paramInt2);
    writeBytes(paramArrayOfByte, paramInt1, paramInt2);
  }

  public void writeStringArray(String[] paramArrayOfString)
  {
    int i = 0;
    if (paramArrayOfString != null)
    {
      writeUINT32(paramArrayOfString.length);
      while (true)
      {
        if (i >= paramArrayOfString.length)
          return;
        writeString(paramArrayOfString[i]);
        i++;
      }
    }
    writeUINT32(0);
  }

  public void writeUINT32(int paramInt)
  {
    writeUINT32(paramInt, this.b);
    this.b = (4 + this.b);
  }

  public void writeUINT32(int paramInt1, int paramInt2)
  {
    if (paramInt2 + 4 > this.a.length)
      a(paramInt2 + 256);
    byte[] arrayOfByte1 = this.a;
    int i = paramInt2 + 1;
    arrayOfByte1[paramInt2] = ((byte)(paramInt1 >> 24));
    byte[] arrayOfByte2 = this.a;
    int j = i + 1;
    arrayOfByte2[i] = ((byte)(paramInt1 >> 16));
    byte[] arrayOfByte3 = this.a;
    int k = j + 1;
    arrayOfByte3[j] = ((byte)(paramInt1 >> 8));
    byte[] arrayOfByte4 = this.a;
    (k + 1);
    arrayOfByte4[k] = ((byte)paramInt1);
  }

  public void writeUINT64(long paramLong)
  {
    if (8 + this.b > this.a.length)
      a(256 + this.a.length);
    byte[] arrayOfByte1 = this.a;
    int i = this.b;
    this.b = (i + 1);
    arrayOfByte1[i] = ((byte)(int)(paramLong >> 56));
    byte[] arrayOfByte2 = this.a;
    int j = this.b;
    this.b = (j + 1);
    arrayOfByte2[j] = ((byte)(int)(paramLong >> 48));
    byte[] arrayOfByte3 = this.a;
    int k = this.b;
    this.b = (k + 1);
    arrayOfByte3[k] = ((byte)(int)(paramLong >> 40));
    byte[] arrayOfByte4 = this.a;
    int m = this.b;
    this.b = (m + 1);
    arrayOfByte4[m] = ((byte)(int)(paramLong >> 32));
    byte[] arrayOfByte5 = this.a;
    int n = this.b;
    this.b = (n + 1);
    arrayOfByte5[n] = ((byte)(int)(paramLong >> 24));
    byte[] arrayOfByte6 = this.a;
    int i1 = this.b;
    this.b = (i1 + 1);
    arrayOfByte6[i1] = ((byte)(int)(paramLong >> 16));
    byte[] arrayOfByte7 = this.a;
    int i2 = this.b;
    this.b = (i2 + 1);
    arrayOfByte7[i2] = ((byte)(int)(paramLong >> 8));
    byte[] arrayOfByte8 = this.a;
    int i3 = this.b;
    this.b = (i3 + 1);
    arrayOfByte8[i3] = ((byte)(int)paramLong);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.network.TypesWriter
 * JD-Core Version:    0.6.2
 */