package com.swarmconnect.packets;

import java.io.IOException;

public class Packet
  implements Payload
{
  public static Packet build(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    if ((paramInt < 1) || (paramInt > Packets.reverseNames.length))
      throw new IOException("Invalid Packet Type");
    try
    {
      localPacket = (Packet)Class.forName("com.swarmconnect.packets.Packet" + Packets.reverseNames[paramInt]).newInstance();
      if (localPacket != null)
        localPacket.fromPayload(paramArrayOfByte, paramArrayOfByte.length);
      return localPacket;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localException.printStackTrace();
        Packet localPacket = null;
      }
    }
  }

  public void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
  }

  public byte[] getPayload()
  {
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.Packet
 * JD-Core Version:    0.6.2
 */