package com.swarmconnect.packets;

import java.io.IOException;

public class PacketAuth extends Packet
{
  private static int a = 3;
  public String clientId = "";

  public PacketAuth()
  {
  }

  public PacketAuth(String paramString)
  {
    this.clientId = paramString;
  }

  public void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    TypesReader localTypesReader = new TypesReader(paramArrayOfByte, 0, paramInt);
    int i = localTypesReader.readByte();
    if (i != a)
      throw new IOException("This is not a " + Packets.reverseNames[a] + " Packet! (" + i + ")");
    this.clientId = localTypesReader.readString();
  }

  public byte[] getPayload()
  {
    TypesWriter localTypesWriter = new TypesWriter();
    localTypesWriter.writeByte(a);
    localTypesWriter.writeString(this.clientId);
    return localTypesWriter.getBytes();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.PacketAuth
 * JD-Core Version:    0.6.2
 */