package com.swarmconnect.packets;

import java.io.IOException;

public class PacketDisconnect
  implements Payload
{
  public static final int DISCONNECT_BY_CLIENT = 0;
  public static final int DISCONNECT_BY_SERVER = 1;
  private static int a = 1;
  public String desc;
  public int reason = 0;

  public PacketDisconnect(int paramInt, String paramString)
  {
    this.reason = paramInt;
    this.desc = paramString;
  }

  public PacketDisconnect(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    fromPayload(paramArrayOfByte, paramInt);
  }

  public void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    TypesReader localTypesReader = new TypesReader(paramArrayOfByte, 0, paramInt);
    int i = localTypesReader.readByte();
    if (i != a)
      throw new IOException("This is not a " + Packets.reverseNames[a] + " Packet! (" + i + ")");
    this.reason = localTypesReader.readUINT32();
    this.desc = localTypesReader.readString();
  }

  public byte[] getPayload()
  {
    TypesWriter localTypesWriter = new TypesWriter();
    localTypesWriter.writeByte(a);
    localTypesWriter.writeUINT32(this.reason);
    localTypesWriter.writeString(this.desc);
    return localTypesWriter.getBytes();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.PacketDisconnect
 * JD-Core Version:    0.6.2
 */