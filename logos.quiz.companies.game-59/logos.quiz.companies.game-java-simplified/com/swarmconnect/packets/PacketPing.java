package com.swarmconnect.packets;

import java.io.IOException;

public class PacketPing extends Packet
{
  private static int a = 4;

  public void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    int i = new TypesReader(paramArrayOfByte, 0, paramInt).readByte();
    if (i != a)
      throw new IOException("This is not a " + Packets.reverseNames[a] + " Packet! (" + i + ")");
  }

  public byte[] getPayload()
  {
    TypesWriter localTypesWriter = new TypesWriter();
    localTypesWriter.writeByte(a);
    return localTypesWriter.getBytes();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.PacketPing
 * JD-Core Version:    0.6.2
 */