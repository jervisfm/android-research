package com.swarmconnect.packets;

import java.io.IOException;

public class PacketVersion extends Packet
{
  public int version = 0;

  public PacketVersion()
  {
  }

  public PacketVersion(int paramInt)
  {
    this.version = paramInt;
  }

  public void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException
  {
    TypesReader localTypesReader = new TypesReader(paramArrayOfByte, 0, paramInt);
    int i = localTypesReader.readByte();
    if (i != 2)
      throw new IOException("This is not a " + Packets.reverseNames[2] + " Packet! (" + i + ")");
    this.version = localTypesReader.readByte();
  }

  public byte[] getPayload()
  {
    TypesWriter localTypesWriter = new TypesWriter();
    localTypesWriter.writeByte(2);
    localTypesWriter.writeByte(this.version);
    return localTypesWriter.getBytes();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.PacketVersion
 * JD-Core Version:    0.6.2
 */