package com.swarmconnect.packets;

public class Packets
{
  public static final int AUTH = 3;
  public static final int DISCONNECT = 1;
  public static final int NOTIFICATION = 6;
  public static final int PING = 4;
  public static final int PONG = 5;
  public static final int VERSION = 2;
  public static final byte[] pingPayload = new PacketPing().getPayload();
  public static final byte[] pongPayload = new PacketPong().getPayload();
  public static final String[] reverseNames = { "UNDEFINED", "Disconnect", "Version", "Auth", "Ping", "Pong", "Notification" };
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.Packets
 * JD-Core Version:    0.6.2
 */