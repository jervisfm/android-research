package com.swarmconnect.packets;

import java.io.IOException;

public abstract interface Payload
{
  public abstract void fromPayload(byte[] paramArrayOfByte, int paramInt)
    throws IOException;

  public abstract byte[] getPayload();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.Payload
 * JD-Core Version:    0.6.2
 */