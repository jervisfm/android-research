package com.swarmconnect.packets;

import java.io.IOException;
import java.math.BigInteger;

public class TypesReader
{
  byte[] a;
  int b = 0;
  int c = 0;

  public TypesReader(byte[] paramArrayOfByte)
  {
    this.a = paramArrayOfByte;
    this.b = 0;
    this.c = paramArrayOfByte.length;
  }

  public TypesReader(byte[] paramArrayOfByte, int paramInt)
  {
    this.a = paramArrayOfByte;
    this.b = paramInt;
    this.c = paramArrayOfByte.length;
    if ((this.b < 0) || (this.b > paramArrayOfByte.length))
      throw new IllegalArgumentException("Illegal offset.");
  }

  public TypesReader(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    this.a = paramArrayOfByte;
    this.b = paramInt1;
    this.c = (paramInt1 + paramInt2);
    if ((this.b < 0) || (this.b > paramArrayOfByte.length))
      throw new IllegalArgumentException("Illegal offset.");
    if ((this.c < 0) || (this.c > paramArrayOfByte.length))
      throw new IllegalArgumentException("Illegal length.");
  }

  public boolean readBoolean()
    throws IOException
  {
    if (this.b >= this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    return arrayOfByte[i] != 0;
  }

  public int readByte()
    throws IOException
  {
    if (this.b >= this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte = this.a;
    int i = this.b;
    this.b = (i + 1);
    return 0xFF & arrayOfByte[i];
  }

  public byte[] readByteString()
    throws IOException
  {
    int i = readUINT32();
    if (i + this.b > this.c)
      throw new IOException("Malformed SSH byte string.");
    byte[] arrayOfByte = new byte[i];
    System.arraycopy(this.a, this.b, arrayOfByte, 0, i);
    this.b = (i + this.b);
    return arrayOfByte;
  }

  public void readBytes(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    if (paramInt2 + this.b > this.c)
      throw new IOException("Packet too short.");
    System.arraycopy(this.a, this.b, paramArrayOfByte, paramInt1, paramInt2);
    this.b = (paramInt2 + this.b);
  }

  public byte[] readBytes(int paramInt)
    throws IOException
  {
    if (paramInt + this.b > this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte = new byte[paramInt];
    System.arraycopy(this.a, this.b, arrayOfByte, 0, paramInt);
    this.b = (paramInt + this.b);
    return arrayOfByte;
  }

  public float readFloat()
    throws IOException
  {
    if (4 + this.b > this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte1 = this.a;
    int i = this.b;
    this.b = (i + 1);
    int j = (0xFF & arrayOfByte1[i]) << 24;
    byte[] arrayOfByte2 = this.a;
    int k = this.b;
    this.b = (k + 1);
    int m = j | (0xFF & arrayOfByte2[k]) << 16;
    byte[] arrayOfByte3 = this.a;
    int n = this.b;
    this.b = (n + 1);
    int i1 = m | (0xFF & arrayOfByte3[n]) << 8;
    byte[] arrayOfByte4 = this.a;
    int i2 = this.b;
    this.b = (i2 + 1);
    return Float.intBitsToFloat(i1 | 0xFF & arrayOfByte4[i2]);
  }

  public int[] readIntArray()
    throws IOException
  {
    int i = readUINT32();
    int[] arrayOfInt;
    if (i < 1)
      arrayOfInt = null;
    while (true)
    {
      return arrayOfInt;
      arrayOfInt = new int[i];
      for (int j = 0; j < i; j++)
        arrayOfInt[j] = readUINT32();
    }
  }

  public BigInteger readMPINT()
    throws IOException
  {
    byte[] arrayOfByte = readByteString();
    if (arrayOfByte.length == 0)
      return BigInteger.ZERO;
    return new BigInteger(arrayOfByte);
  }

  public String readString()
    throws IOException
  {
    int i = readUINT32();
    if (i + this.b > this.c)
      throw new IOException("Malformed SSH string.");
    String str = new String(this.a, this.b, i, "ISO-8859-1");
    this.b = (i + this.b);
    return str;
  }

  public String readString(String paramString)
    throws IOException
  {
    int i = readUINT32();
    if (i + this.b > this.c)
      throw new IOException("Malformed SSH string.");
    if (paramString == null);
    for (String str = new String(this.a, this.b, i); ; str = new String(this.a, this.b, i, paramString))
    {
      this.b = (i + this.b);
      return str;
    }
  }

  public String[] readStringArray()
    throws IOException
  {
    int i = readUINT32();
    if (i > 0)
    {
      String[] arrayOfString = new String[i];
      for (int j = 0; ; j++)
      {
        if (j >= i)
          return arrayOfString;
        arrayOfString[j] = readString();
      }
    }
    return null;
  }

  public int readUINT32()
    throws IOException
  {
    if (4 + this.b > this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte1 = this.a;
    int i = this.b;
    this.b = (i + 1);
    int j = (0xFF & arrayOfByte1[i]) << 24;
    byte[] arrayOfByte2 = this.a;
    int k = this.b;
    this.b = (k + 1);
    int m = j | (0xFF & arrayOfByte2[k]) << 16;
    byte[] arrayOfByte3 = this.a;
    int n = this.b;
    this.b = (n + 1);
    int i1 = m | (0xFF & arrayOfByte3[n]) << 8;
    byte[] arrayOfByte4 = this.a;
    int i2 = this.b;
    this.b = (i2 + 1);
    return i1 | 0xFF & arrayOfByte4[i2];
  }

  public long readUINT64()
    throws IOException
  {
    if (8 + this.b > this.c)
      throw new IOException("Packet too short.");
    byte[] arrayOfByte1 = this.a;
    int i = this.b;
    this.b = (i + 1);
    int j = (0xFF & arrayOfByte1[i]) << 24;
    byte[] arrayOfByte2 = this.a;
    int k = this.b;
    this.b = (k + 1);
    int m = j | (0xFF & arrayOfByte2[k]) << 16;
    byte[] arrayOfByte3 = this.a;
    int n = this.b;
    this.b = (n + 1);
    int i1 = m | (0xFF & arrayOfByte3[n]) << 8;
    byte[] arrayOfByte4 = this.a;
    int i2 = this.b;
    this.b = (i2 + 1);
    long l1 = i1 | 0xFF & arrayOfByte4[i2];
    byte[] arrayOfByte5 = this.a;
    int i3 = this.b;
    this.b = (i3 + 1);
    int i4 = (0xFF & arrayOfByte5[i3]) << 24;
    byte[] arrayOfByte6 = this.a;
    int i5 = this.b;
    this.b = (i5 + 1);
    int i6 = i4 | (0xFF & arrayOfByte6[i5]) << 16;
    byte[] arrayOfByte7 = this.a;
    int i7 = this.b;
    this.b = (i7 + 1);
    int i8 = i6 | (0xFF & arrayOfByte7[i7]) << 8;
    byte[] arrayOfByte8 = this.a;
    int i9 = this.b;
    this.b = (i9 + 1);
    long l2 = i8 | 0xFF & arrayOfByte8[i9];
    return l1 << 32 | l2 & 0xFFFFFFFF;
  }

  public int remain()
  {
    return this.c - this.b;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.packets.TypesReader
 * JD-Core Version:    0.6.2
 */