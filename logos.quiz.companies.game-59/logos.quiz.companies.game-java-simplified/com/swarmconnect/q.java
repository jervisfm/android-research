package com.swarmconnect;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.UiConf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

class q extends u
{
  private a l = new a(null);
  private List<SwarmUser> m = new ArrayList();
  private List<SwarmUser> n = new ArrayList();
  private TextView o;
  private HashMap<SwarmUser, SwarmApplication> p = new HashMap();
  private AlertDialog q;

  private void d(int paramInt)
  {
    b();
    Swarm.user.deleteFriend(paramInt, new SwarmActiveUser.FriendRequestCB()
    {
      public void requestSuccess(boolean paramAnonymousBoolean)
      {
        q.this.c();
        if (paramAnonymousBoolean)
          Swarm.b("Friend Deleted");
        while (true)
        {
          q.this.reload();
          return;
          Swarm.b("Request failed.");
        }
      }
    });
  }

  private void e()
  {
    if (this.q != null)
    {
      this.q.dismiss();
      this.q = null;
    }
  }

  private void e(int paramInt)
  {
    b();
    Swarm.user.addFriend(paramInt, new SwarmActiveUser.FriendRequestCB()
    {
      public void requestSuccess(boolean paramAnonymousBoolean)
      {
        q.this.c();
        if (paramAnonymousBoolean)
          Swarm.b("Friend Confirmed");
        while (true)
        {
          q.this.reload();
          return;
          Swarm.b("Request failed.");
        }
      }
    });
  }

  private void f()
  {
    if (Swarm.user == null)
      return;
    b();
    Swarm.user.getFriends(new SwarmActiveUser.GotFriendsCB()
    {
      public void gotFriends(List<SwarmUser> paramAnonymousList1, List<SwarmUser> paramAnonymousList2)
      {
        if ((paramAnonymousList1 == null) || (paramAnonymousList2 == null) || (paramAnonymousList1.size() + paramAnonymousList2.size() == 0))
          q.g(q.this).setVisibility(0);
        while (true)
        {
          q.a(q.this, paramAnonymousList1);
          q.b(q.this, paramAnonymousList2);
          q.h(q.this).notifyDataSetChanged();
          q.this.c();
          q.this.getOnlineStatus();
          return;
          q.g(q.this).setVisibility(8);
        }
      }
    });
  }

  public void getOnlineStatus()
  {
    a(new Runnable()
    {
      public void run()
      {
        try
        {
          Iterator localIterator = q.b(q.this).iterator();
          while (true)
          {
            if (!localIterator.hasNext())
              return;
            final SwarmUser localSwarmUser = (SwarmUser)localIterator.next();
            localSwarmUser.getOnlineStatus(new SwarmUser.GotUserStatusCB()
            {
              public void gotUserStatus(SwarmApplication paramAnonymous2SwarmApplication, boolean paramAnonymous2Boolean)
              {
                if (paramAnonymous2Boolean)
                  q.c(q.this).put(localSwarmUser, paramAnonymous2SwarmApplication);
                while (true)
                {
                  q.h(q.this).notifyDataSetInvalidated();
                  return;
                  q.c(q.this).remove(localSwarmUser);
                }
              }
            });
          }
        }
        catch (Exception localException)
        {
        }
      }
    });
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_friends"));
    this.o = ((TextView)a(a("@id/empty_list")));
    ((ListView)a(a("@id/list"))).setAdapter(this.l);
    ((ImageButton)a(a("@id/add"))).setBackgroundDrawable(UiConf.footerButtonBackground());
    ((ImageButton)a(a("@id/add"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        EditText localEditText = (EditText)q.this.a(q.this.a("@id/add_friend_username"));
        String str = localEditText.getText().toString().trim();
        if (str.length() > 0)
        {
          q.this.b();
          SwarmUser.getUser(str, new SwarmUser.GotUserCB()
          {
            public void gotUser(SwarmUser paramAnonymous2SwarmUser)
            {
              if (paramAnonymous2SwarmUser != null)
              {
                Swarm.user.addFriend(paramAnonymous2SwarmUser.userId, new SwarmActiveUser.FriendRequestCB()
                {
                  public void requestSuccess(boolean paramAnonymous3Boolean)
                  {
                    q.this.c();
                    if (paramAnonymous3Boolean)
                    {
                      Swarm.b("Friend Request Sent");
                      return;
                    }
                    Swarm.b("Error when adding friend!");
                  }
                });
                return;
              }
              q.this.c();
              Swarm.b("User not found.");
            }
          });
        }
        localEditText.setText("");
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_friends"), "Friends");
  }

  public void onPause()
  {
    super.onPause();
    e();
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        q.f(q.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    private int a()
    {
      if (q.b(q.this) != null)
        return q.b(q.this).size();
      return 0;
    }

    private boolean a(int paramInt)
    {
      return paramInt < q.a(q.this).size();
    }

    private int b()
    {
      if (q.a(q.this) != null)
        return q.a(q.this).size();
      return 0;
    }

    public int getCount()
    {
      return a() + b();
    }

    public Object getItem(int paramInt)
    {
      if (paramInt < b())
        return q.a(q.this).get(paramInt);
      int i = paramInt - q.a(q.this).size();
      if (i < a())
        return q.b(q.this).get(i);
      return null;
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(q.this.d(), q.this.a("@layout/swarm_friends_row"), null);
      final SwarmUser localSwarmUser = (SwarmUser)getItem(paramInt);
      int i;
      if (localSwarmUser != null)
      {
        if (paramInt % 2 != 0)
          break label406;
        i = 16777215;
        paramView.setBackgroundColor(i);
        ((TextView)paramView.findViewById(q.this.a("@id/username"))).setText(localSwarmUser.username);
        ((TextView)paramView.findViewById(q.this.a("@id/points"))).setText(localSwarmUser.points);
        ((TextView)paramView.findViewById(q.this.a("@id/points"))).setTextColor(-15103785);
        SwarmApplication localSwarmApplication = (SwarmApplication)q.c(q.this).get(localSwarmUser);
        if (localSwarmApplication == null)
          break label413;
        ((TextView)paramView.findViewById(q.this.a("@id/online_text"))).setText("Online: Playing " + localSwarmApplication.name);
        ((ImageView)paramView.findViewById(q.this.a("@id/profile_pic"))).setImageResource(q.this.a("@drawable/swarm_friend_online"));
      }
      while (true)
      {
        if (!a(paramInt))
          break label465;
        paramView.setClickable(false);
        ((TextView)paramView.findViewById(q.this.a("@id/online_text"))).setText("Wants to be your friend!");
        ((LinearLayout)paramView.findViewById(q.this.a("@id/friended_section"))).setVisibility(8);
        ((LinearLayout)paramView.findViewById(q.this.a("@id/unfriended_section"))).setVisibility(0);
        ((ImageButton)paramView.findViewById(q.this.a("@id/accept"))).setBackgroundDrawable(UiConf.footerButtonBackground());
        ((ImageButton)paramView.findViewById(q.this.a("@id/accept"))).setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            q.a(q.this, localSwarmUser.userId);
          }
        });
        ((ImageButton)paramView.findViewById(q.this.a("@id/deny"))).setBackgroundDrawable(UiConf.footerButtonBackground());
        ((ImageButton)paramView.findViewById(q.this.a("@id/deny"))).setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            q.b(q.this, localSwarmUser.userId);
          }
        });
        return paramView;
        label406: i = -1996488705;
        break;
        label413: ((TextView)paramView.findViewById(q.this.a("@id/online_text"))).setText("Offline");
        ((ImageView)paramView.findViewById(q.this.a("@id/profile_pic"))).setImageResource(q.this.a("@drawable/swarm_friend_offline"));
      }
      label465: ((LinearLayout)paramView.findViewById(q.this.a("@id/friended_section"))).setVisibility(0);
      ((LinearLayout)paramView.findViewById(q.this.a("@id/unfriended_section"))).setVisibility(8);
      paramView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          Intent localIntent = q.this.c(14);
          localIntent.putExtra("otherUser", localSwarmUser);
          q.this.c.startActivity(localIntent);
        }
      });
      paramView.setOnLongClickListener(new View.OnLongClickListener()
      {
        public boolean onLongClick(View paramAnonymousView)
        {
          q.d(q.this);
          q.a(q.this, new AlertDialog.Builder(q.this.c).setTitle("Delete Friend").setMessage("Do you want to remove " + localSwarmUser.username + " from your friends list?").setCancelable(true).setPositiveButton("Remove", new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              q.b(q.this, this.b.userId);
            }
          }).setNegativeButton("Cancel", new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              paramAnonymous2DialogInterface.cancel();
            }
          }).create());
          q.e(q.this).show();
          return true;
        }
      });
      return paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.q
 * JD-Core Version:    0.6.2
 */