package com.swarmconnect;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.swarmconnect.ui.AsyncImageView;
import java.util.ArrayList;
import java.util.List;

class s extends u
{
  private a l = new a(null);
  private List<SwarmApplication> m = new ArrayList();

  private void e()
  {
    b();
    l locall = new l();
    locall.cb = new APICall.APICallback()
    {
      public void gotAPI(APICall paramAnonymousAPICall)
      {
        l locall = (l)paramAnonymousAPICall;
        if (locall.apps != null)
        {
          s.a(s.this).clear();
          s.a(s.this).addAll(locall.apps);
          s.b(s.this).notifyDataSetChanged();
        }
        s.this.c();
      }

      public void requestFailed()
      {
        s.this.c();
      }
    };
    locall.run();
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_list"));
    ListView localListView = (ListView)a(a("@id/list"));
    localListView.setAdapter(this.l);
    localListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        SwarmApplication localSwarmApplication = s.b(s.this).getItem(paramAnonymousInt);
        if (localSwarmApplication != null)
        {
          Intent localIntent = new Intent("android.intent.action.VIEW");
          localIntent.setData(Uri.parse("market://details?id=" + localSwarmApplication.packageName));
          s.this.c.startActivity(localIntent);
        }
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_games"), "Get More Games");
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        s.c(s.this);
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (s.a(s.this) == null)
        return 0;
      return s.a(s.this).size();
    }

    public SwarmApplication getItem(int paramInt)
    {
      if ((s.a(s.this) == null) || (paramInt < 0) || (paramInt > s.a(s.this).size()))
        return null;
      return (SwarmApplication)s.a(s.this).get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = View.inflate(s.this.d(), s.this.a("@layout/swarm_app_row"), null);
      SwarmApplication localSwarmApplication = getItem(paramInt);
      if (localSwarmApplication != null)
        if (paramInt % 2 != 0)
          break label153;
      label153: for (int i = 16777215; ; i = -1996488705)
      {
        paramView.setBackgroundColor(i);
        ((AsyncImageView)paramView.findViewById(s.this.a("@id/pic"))).init(s.this.c);
        ((AsyncImageView)paramView.findViewById(s.this.a("@id/pic"))).getUrl(localSwarmApplication.iconUrl);
        ((TextView)paramView.findViewById(s.this.a("@id/name"))).setText(localSwarmApplication.name);
        ((TextView)paramView.findViewById(s.this.a("@id/publisher"))).setText(localSwarmApplication.publisherName);
        return paramView;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.s
 * JD-Core Version:    0.6.2
 */