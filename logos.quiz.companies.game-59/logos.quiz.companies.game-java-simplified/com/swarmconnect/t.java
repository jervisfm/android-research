package com.swarmconnect;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.swarmconnect.ui.UiConf;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class t extends u
{
  private SwarmMessageThread l = null;
  private SwarmUser m = null;
  private a n = new a(null);
  private List<SwarmMessage> o = new ArrayList();
  private ListView p;

  public void getMessages()
  {
    SwarmMessageThread.GotMessagesCB local2 = new SwarmMessageThread.GotMessagesCB()
    {
      public void gotMessages(List<SwarmMessage> paramAnonymousList)
      {
        if (paramAnonymousList != null)
        {
          t.a(t.this, paramAnonymousList);
          t.d(t.this).notifyDataSetChanged();
          t.e(t.this).postDelayed(new Runnable()
          {
            public void run()
            {
              t.e(t.this).setSelection(t.a(t.this).size());
            }
          }
          , 1L);
        }
        t.this.c();
      }
    };
    if (this.l != null)
    {
      b();
      this.l.getMessages(local2);
    }
    while (this.m == null)
      return;
    b();
    SwarmMessageThread.getMessages(0, this.m.userId, local2);
  }

  public void onCreate(Bundle paramBundle)
  {
    b(a("@layout/swarm_thread"));
    Bundle localBundle = this.c.getIntent().getExtras();
    if (localBundle != null)
    {
      this.l = ((SwarmMessageThread)localBundle.get("thread"));
      this.m = ((SwarmUser)localBundle.get("otherUser"));
    }
    this.p = ((ListView)a(a("@id/list")));
    this.p.setAdapter(this.n);
    ((ImageButton)a(a("@id/send"))).setBackgroundDrawable(UiConf.footerButtonBackground());
    ((ImageButton)a(a("@id/send"))).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        EditText localEditText = (EditText)t.this.a(t.this.a("@id/reply"));
        String str = localEditText.getText().toString().trim();
        SwarmMessageThread.SentMessageCB local1;
        if (str.length() > 0)
        {
          t.this.b();
          local1 = new SwarmMessageThread.SentMessageCB()
          {
            public void sendFailed()
            {
              t.this.c();
              Swarm.b("Unable to send message.");
            }

            public void sentMessage(boolean paramAnonymous2Boolean, int paramAnonymous2Int)
            {
              if (t.b(t.this) == null)
              {
                t.a(t.this, new SwarmMessageThread());
                t.b(t.this).id = paramAnonymous2Int;
                t.b(t.this).otherUser = t.c(t.this);
                t.b(t.this).viewed = true;
              }
              t.this.reload();
              t.this.c();
            }
          };
          if (t.b(t.this) == null)
            break label87;
          t.b(t.this).reply(str, local1);
        }
        while (true)
        {
          localEditText.setText("");
          return;
          label87: if (t.c(t.this) != null)
            SwarmMessage.sendMessage(t.c(t.this).userId, str, local1);
        }
      }
    });
    super.onCreate(paramBundle);
    a(a("@drawable/swarm_messages"), "Messages");
    if (this.m != null)
      a(a("@drawable/swarm_messages"), "Conversation with " + this.m.username);
    while ((this.l == null) || (this.l.otherUser == null))
      return;
    a(a("@drawable/swarm_messages"), "Conversation with " + this.l.otherUser.username);
  }

  public void reload()
  {
    a(new Runnable()
    {
      public void run()
      {
        if ((t.b(t.this) != null) && (!t.b(t.this).viewed))
          t.b(t.this).markRead();
        t.this.getMessages();
      }
    });
  }

  private class a extends BaseAdapter
  {
    private a()
    {
    }

    public int getCount()
    {
      if (t.a(t.this) != null)
        return t.a(t.this).size();
      return 0;
    }

    public Object getItem(int paramInt)
    {
      if ((paramInt >= 0) && (paramInt < t.a(t.this).size()))
        return t.a(t.this).get(paramInt);
      return null;
    }

    public long getItemId(int paramInt)
    {
      return 0L;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      SwarmMessage localSwarmMessage = (SwarmMessage)getItem(paramInt);
      View localView = null;
      if (localSwarmMessage != null)
        if (localSwarmMessage.from.userId != Swarm.user.userId)
          break label223;
      label223: for (localView = View.inflate(t.this.d(), t.this.a("@layout/swarm_thread_row_self"), null); ; localView = View.inflate(t.this.d(), t.this.a("@layout/swarm_thread_row_other"), null))
      {
        localView.findViewById(t.this.a("@id/speech_bubble")).setBackgroundDrawable(UiConf.messageBackground());
        TextView localTextView = (TextView)localView.findViewById(t.this.a("@id/message"));
        localTextView.setText(localSwarmMessage.from.username + ": " + localSwarmMessage.message, TextView.BufferType.SPANNABLE);
        ((Spannable)localTextView.getText()).setSpan(new StyleSpan(1), 0, 1 + localSwarmMessage.from.username.length(), 33);
        Date localDate = localSwarmMessage.getSentDate();
        SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("MMM dd, h:mma");
        ((TextView)localView.findViewById(t.this.a("@id/date"))).setText(localSimpleDateFormat.format(localDate));
        return localView;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.t
 * JD-Core Version:    0.6.2
 */