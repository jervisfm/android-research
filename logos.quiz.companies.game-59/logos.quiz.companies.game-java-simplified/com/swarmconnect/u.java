package com.swarmconnect;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.swarmconnect.delegates.SwarmLoginListener;
import com.swarmconnect.delegates.SwarmNotificationDelegate;
import com.swarmconnect.ui.UiConf;
import java.util.ArrayList;
import java.util.Iterator;

abstract class u
{
  public static final String TAG = "Swarm";
  protected static u a = null;
  protected static boolean b = false;
  protected static final ArrayList<u> i = new ArrayList();
  protected static final ArrayList<u> j = new ArrayList();
  protected static SwarmNotificationDelegate k = new at();
  protected SwarmMainActivity c;
  protected View d;
  protected View e;
  protected View f;
  protected View g;
  protected View h;
  private View l;
  private ImageView m;
  private TextView n;
  private View o;
  private TextView p;
  private View q;
  private View r;
  private TextView s;
  private View t;

  protected static void a()
  {
    Iterator localIterator = ((ArrayList)i.clone()).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((u)localIterator.next()).finish();
    }
  }

  protected static void b(String paramString)
  {
    Swarm.c(paramString);
  }

  private boolean e()
  {
    return this instanceof ba;
  }

  public static int getResource(String paramString, Context paramContext)
  {
    String str = paramContext.getPackageName();
    return paramContext.getResources().getIdentifier(paramString, null, str);
  }

  protected int a(String paramString)
  {
    if (this.c == null)
      return 0;
    return getResource(paramString, this.c.getApplicationContext());
  }

  protected View a(int paramInt)
  {
    if (this.c != null)
      return this.c.findViewById(paramInt);
    return null;
  }

  protected void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
  }

  protected void a(int paramInt, String paramString)
  {
    if ((paramInt != 0) && (paramString != null) && (paramString.length() > 0))
    {
      this.l.setVisibility(0);
      this.m.setImageResource(paramInt);
      this.n.setText(paramString);
    }
  }

  protected void a(Runnable paramRunnable)
  {
    if (this.c != null)
      this.c.runOnUiThread(paramRunnable);
  }

  protected void a(String paramString, boolean paramBoolean, final Runnable paramRunnable)
  {
    View localView;
    int i1;
    if (this.r != null)
    {
      this.r.setVisibility(0);
      this.r.setBackgroundDrawable(UiConf.subHeaderButtonBackground());
      this.s.setText(paramString);
      localView = this.t;
      i1 = 0;
      if (!paramBoolean)
        break label74;
    }
    while (true)
    {
      localView.setVisibility(i1);
      if (paramRunnable != null)
        this.r.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            paramRunnable.run();
          }
        });
      return;
      label74: i1 = 8;
    }
  }

  protected void b()
  {
    c();
    Swarm.c();
  }

  protected void b(int paramInt)
  {
    if (this.c != null)
      this.c.setContentView(paramInt);
  }

  protected Intent c(int paramInt)
  {
    Intent localIntent = new Intent(this.c, SwarmMainActivity.class);
    localIntent.putExtra("screenType", paramInt);
    return localIntent;
  }

  protected void c()
  {
    Swarm.d();
  }

  public void clearStack()
  {
    Iterator localIterator = ((ArrayList)j.clone()).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((u)localIterator.next()).finish();
    }
  }

  protected Context d()
  {
    if (this.c != null)
      return this.c.getApplicationContext();
    return null;
  }

  public void finish()
  {
    if (this.c != null)
    {
      this.c.finish();
      j.remove(this);
    }
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
  }

  public void onCreate(Bundle paramBundle)
  {
    this.d = a(a("@id/background"));
    if (this.d != null)
    {
      this.d.setBackgroundResource(a("@drawable/swarm_background"));
      ((BitmapDrawable)this.d.getBackground()).setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
    }
    this.e = a(a("@id/header"));
    if (this.e != null)
      this.e.setBackgroundDrawable(UiConf.headerBackground());
    this.f = a(a("@id/footer"));
    if (this.f != null)
      this.f.setBackgroundDrawable(UiConf.footerBackground());
    this.g = a(a("@id/offline_error"));
    this.h = a(a("@id/offline_error_underline"));
    if (this.g != null)
      this.g.setBackgroundDrawable(UiConf.headerOfflineErrorBackground());
    this.l = a(a("@id/sheader"));
    this.m = ((ImageView)a(a("@id/sheader_icon")));
    this.n = ((TextView)a(a("@id/sheader_title")));
    this.o = a(a("@id/sheader_btn1"));
    this.p = ((TextView)a(a("@id/sheader_btn1_text")));
    this.q = ((TextView)a(a("@id/sheader_btn1_dropdown")));
    this.r = a(a("@id/sheader_btn2"));
    this.s = ((TextView)a(a("@id/sheader_btn2_text")));
    this.t = ((TextView)a(a("@id/sheader_btn1_dropdown")));
    if (this.l != null)
    {
      this.l.setVisibility(8);
      this.o.setVisibility(8);
      this.r.setVisibility(8);
    }
    try
    {
      ImageButton localImageButton1 = (ImageButton)a(a("@id/home"));
      ImageButton localImageButton2 = (ImageButton)a(a("@id/messages"));
      ImageButton localImageButton3 = (ImageButton)a(a("@id/close"));
      if ((this instanceof SwarmDashboardScreen))
      {
        localImageButton1.setBackgroundDrawable(UiConf.headerButtonPressedBackground());
        if ((!(this instanceof t)) && (!(this instanceof g)))
          break label461;
        localImageButton2.setBackgroundDrawable(UiConf.headerButtonPressedBackground());
      }
      while (true)
      {
        localImageButton3.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            u.this.clearStack();
          }
        });
        return;
        localImageButton1.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            u.this.clearStack();
            u.this.show(0);
          }
        });
        break;
        label461: localImageButton2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            u.this.clearStack();
            u.this.show(5);
          }
        });
      }
    }
    catch (Exception localException)
    {
    }
  }

  public void onDestroy()
  {
    i.remove(this);
    j.remove(this);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    return false;
  }

  public void onPause()
  {
    a = null;
    Swarm.removeNotificationDelegate(k);
  }

  public void onResume()
  {
    a = this;
    Swarm.addNotificationDelegate(k);
    if (e())
      i.add(this);
    while (true)
    {
      refresh();
      return;
      j.add(this);
      if (Swarm.user == null)
      {
        clearStack();
        SwarmLoginManager.addLoginListener(new SwarmLoginListener()
        {
          public void loginCanceled()
          {
            Swarm.removeLoginListener(this);
          }

          public void loginStarted()
          {
          }

          public void userLoggedIn(SwarmActiveUser paramAnonymousSwarmActiveUser)
          {
            Swarm.showDashboard();
            SwarmLoginManager.removeLoginListener(this);
          }

          public void userLoggedOut()
          {
          }
        });
        Swarm.showLogin();
      }
    }
  }

  public void onStop()
  {
  }

  public void refresh()
  {
    int i1 = 8;
    int i2;
    View localView2;
    if (this.g != null)
    {
      View localView1 = this.g;
      if (!Swarm.isOnline())
        break label51;
      i2 = i1;
      localView1.setVisibility(i2);
      localView2 = this.h;
      if (!Swarm.isOnline())
        break label56;
    }
    while (true)
    {
      localView2.setVisibility(i1);
      reload();
      return;
      label51: i2 = 0;
      break;
      label56: i1 = 0;
    }
  }

  protected abstract void reload();

  public void setParent(SwarmMainActivity paramSwarmMainActivity)
  {
    this.c = paramSwarmMainActivity;
  }

  public void show(int paramInt)
  {
    if (this.c != null)
      this.c.show(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.u
 * JD-Core Version:    0.6.2
 */