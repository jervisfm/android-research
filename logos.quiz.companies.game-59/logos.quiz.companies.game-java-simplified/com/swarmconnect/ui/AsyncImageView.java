package com.swarmconnect.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import com.swarmconnect.utils.AsyncImage;
import com.swarmconnect.utils.AsyncImage.AsyncImageCB;

public class AsyncImageView extends LinearLayout
{
  private ProgressBar a;
  private Context b;
  private ImageView.ScaleType c = null;
  private int d = 0;
  public String extra;
  public ImageView imageView;
  public String url;

  public AsyncImageView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }

  public AsyncImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }

  public AsyncImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }

  public AsyncImageView(Context paramContext, String paramString)
  {
    super(paramContext);
    init(paramContext);
    getUrl(paramString);
  }

  public void getUrl(final String paramString)
  {
    this.url = paramString;
    AsyncImage.getImage(paramString, new AsyncImage.AsyncImageCB()
    {
      public void gotImage(Bitmap paramAnonymousBitmap)
      {
        if (paramString == AsyncImageView.this.url)
          AsyncImageView.this.gotImage(paramAnonymousBitmap);
      }

      public void requestFailed(Exception paramAnonymousException)
      {
        if (AsyncImageView.a(AsyncImageView.this) > 0)
        {
          Bitmap localBitmap = BitmapFactory.decodeResource(AsyncImageView.b(AsyncImageView.this).getResources(), AsyncImageView.a(AsyncImageView.this));
          if (localBitmap != null)
            AsyncImageView.this.gotImage(localBitmap);
        }
      }
    });
  }

  public void gotImage(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      if (Integer.parseInt(Build.VERSION.SDK) > 3)
        new a(null).a(paramBitmap, 160);
      this.imageView = new ImageView(this.b);
      this.imageView.setImageBitmap(paramBitmap);
      if (this.c == null)
        break label110;
      this.imageView.setScaleType(this.c);
    }
    while (true)
    {
      if (getLayoutParams() != null)
        this.imageView.setLayoutParams(getLayoutParams());
      this.imageView.setAdjustViewBounds(true);
      removeAllViews();
      addView(this.imageView);
      return;
      label110: this.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }
  }

  public void init(Context paramContext)
  {
    setGravity(17);
    this.b = paramContext;
    removeAllViews();
    this.a = new ProgressBar(paramContext);
    this.a.setIndeterminate(true);
    this.a.setVisibility(0);
    this.a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
    addView(this.a);
  }

  public void setFailureImage(int paramInt)
  {
    this.d = paramInt;
  }

  public void setScaleType(ImageView.ScaleType paramScaleType)
  {
    this.c = paramScaleType;
    if (this.imageView != null)
      this.imageView.setScaleType(paramScaleType);
  }

  private class a
  {
    private a()
    {
    }

    void a(Bitmap paramBitmap, int paramInt)
    {
      if (paramBitmap != null)
        paramBitmap.setDensity(paramInt);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ui.AsyncImageView
 * JD-Core Version:    0.6.2
 */