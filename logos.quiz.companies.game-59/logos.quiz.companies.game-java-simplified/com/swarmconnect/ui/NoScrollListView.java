package com.swarmconnect.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.ListView;

public class NoScrollListView extends ListView
{
  public NoScrollListView(Context paramContext)
  {
    super(paramContext);
  }

  public NoScrollListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public NoScrollListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = 0;
    super.onMeasure(paramInt1, View.MeasureSpec.makeMeasureSpec(0, 0));
    int j = getMeasuredHeight() - (getListPaddingTop() + getListPaddingBottom() + 2 * getVerticalFadingEdgeLength());
    int k = getListPaddingTop() + getListPaddingBottom() + j * getCount();
    int m = 0;
    if (i >= getChildCount())
      if (m == 0)
        break label123;
    label123: for (int n = m + (getListPaddingTop() + getListPaddingBottom()); ; n = k)
    {
      setMeasuredDimension(getMeasuredWidth(), n);
      return;
      View localView = getChildAt(i);
      if (localView != null)
        m += localView.getHeight();
      i++;
      break;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ui.NoScrollListView
 * JD-Core Version:    0.6.2
 */