package com.swarmconnect.ui;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;

public class UiConf
{
  public static final int CORNER_RADIUS = 3;
  public static final int altRowColor = -1996488705;
  public static final int blackTextColor = -14540254;
  public static final int blueTextColor = -15103785;
  public static float density = 0.0F;
  public static final int errorTextColor = -7398867;
  public static final int greyTextColor = -10066330;

  public static final StateListDrawable blueButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -12473354, -15366700 });
    localGradientDrawable1.setStroke(1, -2013265920);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -10372097, -13261335 });
    localGradientDrawable2.setStroke(1, -2013265920);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -14578736, -16423500 });
    localGradientDrawable3.setStroke(1, -2013265920);
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }

  public static final StateListDrawable buttonStates(Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3)
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    if (paramDrawable1 != null)
      localStateListDrawable.addState(new int[] { 16842919, -16842908 }, paramDrawable1);
    if (paramDrawable2 != null)
      localStateListDrawable.addState(new int[] { -16842919, 16842908 }, paramDrawable2);
    if (paramDrawable3 != null)
      localStateListDrawable.addState(new int[] { -16842919, -16842908 }, paramDrawable3);
    return localStateListDrawable;
  }

  public static final StateListDrawable coinsProviderButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13421773, -12303292 });
    localGradientDrawable1.setStroke(1, -4342339);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11184811, -10066330 });
    localGradientDrawable2.setStroke(1, -15103785);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable2, null, localGradientDrawable1);
  }

  public static final GradientDrawable contextualGreyBackground()
  {
    GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11184811, -11184811 });
    localGradientDrawable.setCornerRadius(dips(3.0F));
    return localGradientDrawable;
  }

  public static final GradientDrawable customGradient(int paramInt1, int paramInt2)
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { paramInt1, paramInt2 });
  }

  public static final GradientDrawable customGradient(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { paramInt1, paramInt2 });
    if (paramBoolean)
      localGradientDrawable.setCornerRadius(dips(3.0F));
    return localGradientDrawable;
  }

  public static final float dips(float paramFloat)
  {
    return paramFloat * density;
  }

  public static final StateListDrawable facebookLoginButton()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -7626548, -12821863 });
    localGradientDrawable1.setStroke(1, -2013265920);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11510407, -14338212 });
    localGradientDrawable2.setStroke(1, -2013265920);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable2, localGradientDrawable1, localGradientDrawable1);
  }

  public static final GradientDrawable footerBackground()
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -855310, -3026479 });
  }

  public static final StateListDrawable footerButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -855310, -3026479 });
    localGradientDrawable1.setStroke(1, -4342339);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -2960686, -5131855 });
    localGradientDrawable2.setStroke(1, -6710887);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable2, null, localGradientDrawable1);
  }

  public static final StateListDrawable greenButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11943910, -13069292 });
    localGradientDrawable1.setStroke(1, -2013265920);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -9839300, -11425234 });
    localGradientDrawable2.setStroke(1, -2013265920);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13467373, -14195953 });
    localGradientDrawable3.setStroke(1, -2013265920);
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }

  public static final StateListDrawable greyButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -657931, -4342339 });
    localGradientDrawable1.setStroke(1, -2013265920);
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -1, -3487030 });
    localGradientDrawable2.setStroke(1, -2013265920);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -4473925, -7434610 });
    localGradientDrawable3.setStroke(1, -2013265920);
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }

  public static final GradientDrawable headerBackground()
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -12473354, -15366700 });
  }

  public static final GradientDrawable headerButtonPressedBackground()
  {
    GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13992777, -13992777 });
    localGradientDrawable.setCornerRadius(dips(3.0F));
    return localGradientDrawable;
  }

  public static final GradientDrawable headerOfflineErrorBackground()
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -860142, -4351472 });
  }

  public static final GradientDrawable loadingPopupBackground()
  {
    GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13421773, -11184811 });
    localGradientDrawable.setCornerRadius(dips(3.0F));
    return localGradientDrawable;
  }

  public static final GradientDrawable messageBackground()
  {
    GradientDrawable localGradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -1, -1 });
    localGradientDrawable.setCornerRadius(dips(8.0F));
    return localGradientDrawable;
  }

  public static final StateListDrawable purchasePopupBuyButton()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11420383, -12477919 });
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -9315007, -10376639 });
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -13525759, -14583295 });
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }

  public static final StateListDrawable purchasePopupCancelButton()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11184811, -11184811 });
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -5444865, -6496513 });
    localGradientDrawable2.setStroke((int)dips(2.0F), -1);
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -12303292, -12303292 });
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }

  public static final GradientDrawable purchasePopupLightBlue()
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -7829368, -6710887 });
  }

  public static final GradientDrawable purchasePopupMedBlue()
  {
    return new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -11567716, -10710346 });
  }

  public static final GradientDrawable purchasePopupMedBlueRound()
  {
    GradientDrawable localGradientDrawable = purchasePopupMedBlue();
    localGradientDrawable.setCornerRadius(dips(3.0F));
    return localGradientDrawable;
  }

  public static final StateListDrawable subHeaderButtonBackground()
  {
    GradientDrawable localGradientDrawable1 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -12303292, -11184811 });
    localGradientDrawable1.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -5444865, -6496513 });
    localGradientDrawable2.setCornerRadius(dips(3.0F));
    GradientDrawable localGradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] { -12303292, -12303292 });
    localGradientDrawable3.setCornerRadius(dips(3.0F));
    return buttonStates(localGradientDrawable3, localGradientDrawable2, localGradientDrawable1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.ui.UiConf
 * JD-Core Version:    0.6.2
 */