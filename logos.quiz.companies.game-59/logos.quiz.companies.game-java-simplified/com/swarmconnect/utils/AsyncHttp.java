package com.swarmconnect.utils;

import android.os.Handler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class AsyncHttp
{
  public static void getBlocking(String paramString, AsyncCB paramAsyncCB)
  {
    getBlocking(paramString, paramAsyncCB, null);
  }

  public static void getBlocking(String paramString, AsyncCB paramAsyncCB, Handler paramHandler)
  {
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient(localBasicHttpParams);
    HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, 8192);
    HttpGet localHttpGet = new HttpGet(paramString);
    BasicResponseHandler localBasicResponseHandler = new BasicResponseHandler();
    try
    {
      String str = (String)localDefaultHttpClient.execute(localHttpGet, localBasicResponseHandler);
      if (paramAsyncCB != null)
      {
        if (paramHandler != null)
        {
          paramHandler.post(new a(str, paramAsyncCB));
          return;
        }
        paramAsyncCB.gotURL(str);
        return;
      }
    }
    catch (Exception localException)
    {
      if (paramAsyncCB != null)
      {
        if (paramHandler != null)
        {
          paramHandler.post(new b(localException, paramAsyncCB));
          return;
        }
        paramAsyncCB.requestFailed(localException);
      }
    }
  }

  public static void getURL(String paramString)
  {
    getURL(paramString, null);
  }

  public static void getURL(String paramString, final AsyncCB paramAsyncCB)
  {
    try
    {
      localHandler = new Handler();
      new Thread(new Runnable()
      {
        public void run()
        {
          AsyncHttp.getBlocking(AsyncHttp.this, paramAsyncCB, localHandler);
        }
      }).start();
      return;
    }
    catch (Exception localException)
    {
      while (true)
        final Handler localHandler = null;
    }
  }

  public static abstract class AsyncCB
  {
    public abstract void gotURL(String paramString);

    public void requestFailed(Exception paramException)
    {
    }
  }

  private static class a
    implements Runnable
  {
    private String a;
    private AsyncHttp.AsyncCB b;

    public a(String paramString, AsyncHttp.AsyncCB paramAsyncCB)
    {
      this.a = paramString;
      this.b = paramAsyncCB;
    }

    public void run()
    {
      this.b.gotURL(this.a);
    }
  }

  private static class b
    implements Runnable
  {
    private Exception a;
    private AsyncHttp.AsyncCB b;

    public b(Exception paramException, AsyncHttp.AsyncCB paramAsyncCB)
    {
      this.a = paramException;
      this.b = paramAsyncCB;
    }

    public void run()
    {
      this.b.requestFailed(this.a);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.AsyncHttp
 * JD-Core Version:    0.6.2
 */