package com.swarmconnect.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import java.net.URI;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class AsyncImage
{
  public static final long CACHE_EXPIRE = 3600000L;
  private static int a = 0;
  private static LinkedHashMap<String, CachedImage> b = new LinkedHashMap();

  public static void clearCachedImages()
  {
    b.clear();
    a = 0;
  }

  public static void getBlocking(String paramString, AsyncImageCB paramAsyncImageCB, Handler paramHandler)
  {
    while (true)
    {
      CachedImage localCachedImage1;
      try
      {
        if (!b.containsKey(paramString))
          break label369;
        CachedImage localCachedImage2 = (CachedImage)b.get(paramString);
        if (localCachedImage2 == null)
          break label369;
        if (localCachedImage2.timestamp > System.currentTimeMillis() - 3600000L)
        {
          localObject1 = localCachedImage2.image;
          if (localObject1 == null)
          {
            HttpGet localHttpGet = new HttpGet(URI.create(paramString));
            localBitmap = BitmapFactory.decodeStream(new BufferedHttpEntity(new DefaultHttpClient().execute(localHttpGet).getEntity()).getContent());
            b.put(paramString, new CachedImage(localBitmap, paramString));
            a += 4 * (localBitmap.getWidth() * localBitmap.getHeight());
            if (a <= 4000000L)
              continue;
            if (a <= 2500000L)
              continue;
          }
          else
          {
            if (paramAsyncImageCB == null)
              break label368;
            if (paramHandler == null)
              continue;
            paramHandler.post(new b((Bitmap)localObject1, paramAsyncImageCB));
          }
        }
        else
        {
          b.remove(paramString);
          break label369;
        }
        localIterator = b.keySet().iterator();
        localObject2 = null;
        if (!localIterator.hasNext())
        {
          if (localObject2 == null)
            continue;
          b.remove(localObject2.url);
          a -= 4 * (localObject2.image.getWidth() * localObject2.image.getHeight());
          continue;
        }
      }
      catch (Exception localException)
      {
        Bitmap localBitmap;
        Iterator localIterator;
        if (paramAsyncImageCB != null)
        {
          if (paramHandler != null)
          {
            paramHandler.post(new a(localException, paramAsyncImageCB));
            return;
            String str = (String)localIterator.next();
            localCachedImage1 = (CachedImage)b.get(str);
            if (localObject2 == null)
              break label375;
            if ((localCachedImage1 == null) || (localCachedImage1.timestamp >= localObject2.timestamp))
              continue;
            break label375;
            paramAsyncImageCB.gotImage((Bitmap)localObject1);
            return;
          }
          paramAsyncImageCB.requestFailed(localException);
          return;
          localObject1 = localBitmap;
          continue;
        }
      }
      label368: return;
      label369: Object localObject1 = null;
      continue;
      label375: Object localObject2 = localCachedImage1;
    }
  }

  public static void getImage(String paramString, final AsyncImageCB paramAsyncImageCB)
  {
    try
    {
      localHandler = new Handler();
      if (b.containsKey(paramString))
      {
        CachedImage localCachedImage = (CachedImage)b.get(paramString);
        localCachedImage.timestamp = System.currentTimeMillis();
        paramAsyncImageCB.gotImage(localCachedImage.image);
        return;
      }
    }
    catch (Exception localException)
    {
      final Handler localHandler;
      while (true)
        localHandler = null;
      ThreadPool.execute(new Runnable()
      {
        public void run()
        {
          AsyncImage.getBlocking(AsyncImage.this, paramAsyncImageCB, localHandler);
        }
      });
    }
  }

  public static void removeImage(String paramString)
  {
    if (b.containsKey(paramString))
      b.remove(paramString);
  }

  public static abstract class AsyncImageCB
  {
    public abstract void gotImage(Bitmap paramBitmap);

    public void requestFailed(Exception paramException)
    {
    }
  }

  public static class CachedImage
  {
    public Bitmap image;
    public long timestamp;
    public String url;

    public CachedImage(Bitmap paramBitmap, String paramString)
    {
      this.image = paramBitmap;
      this.url = paramString;
      this.timestamp = System.currentTimeMillis();
    }
  }

  private static class a
    implements Runnable
  {
    private Exception a;
    private AsyncImage.AsyncImageCB b;

    public a(Exception paramException, AsyncImage.AsyncImageCB paramAsyncImageCB)
    {
      this.a = paramException;
      this.b = paramAsyncImageCB;
    }

    public void run()
    {
      this.b.requestFailed(this.a);
    }
  }

  private static class b
    implements Runnable
  {
    private Bitmap a;
    private AsyncImage.AsyncImageCB b;

    public b(Bitmap paramBitmap, AsyncImage.AsyncImageCB paramAsyncImageCB)
    {
      this.a = paramBitmap;
      this.b = paramAsyncImageCB;
    }

    public void run()
    {
      this.b.gotImage(this.a);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.AsyncImage
 * JD-Core Version:    0.6.2
 */