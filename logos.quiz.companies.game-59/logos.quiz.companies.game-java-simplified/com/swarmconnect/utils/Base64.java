package com.swarmconnect.utils;

import java.io.PrintStream;

public class Base64
{
  static byte[] a = new byte[64];
  static String b = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  static
  {
    for (int i = 0; ; i++)
    {
      if (i >= 64)
        return;
      int j = (byte)b.charAt(i);
      a[i] = j;
    }
  }

  public static byte[] decode(String paramString)
  {
    int i;
    if (paramString.endsWith("="))
      i = 1;
    byte[] arrayOfByte;
    while (true)
    {
      if (paramString.endsWith("=="))
        i++;
      arrayOfByte = new byte[3 * ((3 + paramString.length()) / 4) - i];
      int j = 0;
      int k = 0;
      try
      {
        while (true)
        {
          if (j >= paramString.length())
            return arrayOfByte;
          int m = b.indexOf(paramString.charAt(j));
          if (m != -1);
          switch (j % 4)
          {
          case 0:
            arrayOfByte[k] = ((byte)(m << 2));
            break;
          case 1:
            int i2 = k + 1;
            arrayOfByte[k] = ((byte)(arrayOfByte[k] | (byte)(0x3 & m >> 4)));
            arrayOfByte[i2] = ((byte)(m << 4));
            k = i2;
            break;
          case 2:
            int i1 = k + 1;
            arrayOfByte[k] = ((byte)(arrayOfByte[k] | (byte)(0xF & m >> 2)));
            arrayOfByte[i1] = ((byte)(m << 6));
            k = i1;
            break;
          case 3:
            int n = k + 1;
            arrayOfByte[k] = ((byte)(arrayOfByte[k] | (byte)(m & 0x3F)));
            k = n;
            break label232;
            i = 0;
            break;
            return arrayOfByte;
          default:
            label232: j++;
          }
        }
      }
      catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
      {
      }
    }
    return arrayOfByte;
  }

  public static String encode(String paramString)
  {
    return encode(paramString.getBytes());
  }

  public static String encode(byte[] paramArrayOfByte)
  {
    return encode(paramArrayOfByte, 0, paramArrayOfByte.length);
  }

  public static String encode(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    byte[] arrayOfByte = new byte[4 * ((paramInt2 + 2) / 3) + paramInt2 / 72];
    int i = paramInt2 + paramInt1;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    if (paramInt1 >= i)
      switch (m)
      {
      default:
      case 1:
      case 2:
      }
    while (true)
    {
      return new String(arrayOfByte);
      int i1 = paramArrayOfByte[paramInt1];
      int i2 = m + 1;
      int i4;
      int i5;
      switch (i2)
      {
      default:
        i4 = n;
        i5 = i2;
      case 1:
      case 2:
      case 3:
      }
      while (true)
      {
        int i6 = j + 1;
        if (i6 >= 72)
        {
          int i7 = i4 + 1;
          arrayOfByte[i4] = 10;
          i4 = i7;
          i6 = 0;
        }
        paramInt1++;
        j = i6;
        k = i1;
        m = i5;
        n = i4;
        break;
        i4 = n + 1;
        arrayOfByte[n] = a[(0x3F & i1 >> 2)];
        i5 = i2;
        continue;
        i4 = n + 1;
        arrayOfByte[n] = a[(0x30 & k << 4 | 0xF & i1 >> 4)];
        i5 = i2;
        continue;
        int i3 = n + 1;
        arrayOfByte[n] = a[(0x3C & k << 2 | 0x3 & i1 >> 6)];
        i4 = i3 + 1;
        arrayOfByte[i3] = a[(i1 & 0x3F)];
        i5 = 0;
      }
      int i9 = n + 1;
      arrayOfByte[n] = a[(0x30 & k << 4)];
      int i10 = i9 + 1;
      arrayOfByte[i9] = 61;
      (i10 + 1);
      arrayOfByte[i10] = 61;
      continue;
      int i8 = n + 1;
      arrayOfByte[n] = a[(0x3C & k << 2)];
      (i8 + 1);
      arrayOfByte[i8] = 61;
    }
  }

  public static void main(String[] paramArrayOfString)
  {
    System.out.println("encode: " + paramArrayOfString[0] + " -> (" + encode(paramArrayOfString[0]) + ")");
    System.out.println("decode: " + paramArrayOfString[0] + " -> (" + new String(decode(paramArrayOfString[0])) + ")");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.Base64
 * JD-Core Version:    0.6.2
 */