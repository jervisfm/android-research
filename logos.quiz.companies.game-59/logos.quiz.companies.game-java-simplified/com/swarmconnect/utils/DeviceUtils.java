package com.swarmconnect.utils;

import android.os.Build;

public class DeviceUtils
{
  private static String a = null;

  // ERROR //
  public static String getDeviceId(android.content.Context paramContext)
  {
    // Byte code:
    //   0: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   3: ifnull +16 -> 19
    //   6: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   9: invokevirtual 25	java/lang/String:length	()I
    //   12: ifle +7 -> 19
    //   15: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   18: areturn
    //   19: getstatic 30	android/os/Build$VERSION:SDK	Ljava/lang/String;
    //   22: invokestatic 36	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   25: istore_2
    //   26: iload_2
    //   27: bipush 9
    //   29: if_icmplt +40 -> 69
    //   32: new 38	com/swarmconnect/utils/DeviceUtils$SerialGetter
    //   35: dup
    //   36: invokespecial 39	com/swarmconnect/utils/DeviceUtils$SerialGetter:<init>	()V
    //   39: invokevirtual 43	com/swarmconnect/utils/DeviceUtils$SerialGetter:getSerial	()Ljava/lang/String;
    //   42: putstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   45: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   48: ifnull +21 -> 69
    //   51: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   54: ldc 45
    //   56: if_acmpeq +13 -> 69
    //   59: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   62: astore 7
    //   64: aload 7
    //   66: areturn
    //   67: astore 6
    //   69: aload_0
    //   70: ldc 47
    //   72: invokevirtual 53	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   75: checkcast 55	android/telephony/TelephonyManager
    //   78: astore_3
    //   79: aload_3
    //   80: ifnull +47 -> 127
    //   83: aload_3
    //   84: invokevirtual 57	android/telephony/TelephonyManager:getDeviceId	()Ljava/lang/String;
    //   87: putstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   90: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   93: ifnull +34 -> 127
    //   96: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   99: invokevirtual 25	java/lang/String:length	()I
    //   102: ifle +25 -> 127
    //   105: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   108: ldc 59
    //   110: ldc 61
    //   112: invokevirtual 65	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   115: ldc 61
    //   117: invokevirtual 69	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   120: ifne +7 -> 127
    //   123: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   126: areturn
    //   127: aload_0
    //   128: invokevirtual 73	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   131: ldc 75
    //   133: invokestatic 81	android/provider/Settings$Secure:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    //   136: putstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   139: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   142: ifnull +12 -> 154
    //   145: getstatic 10	com/swarmconnect/utils/DeviceUtils:a	Ljava/lang/String;
    //   148: astore 4
    //   150: aload 4
    //   152: areturn
    //   153: astore_1
    //   154: ldc 83
    //   156: areturn
    //   157: astore 5
    //   159: goto -90 -> 69
    //
    // Exception table:
    //   from	to	target	type
    //   32	64	67	java/lang/Exception
    //   19	26	153	java/lang/Exception
    //   69	79	153	java/lang/Exception
    //   83	127	153	java/lang/Exception
    //   127	150	153	java/lang/Exception
    //   32	64	157	java/lang/Error
  }

  public static class SerialGetter
  {
    public String getSerial()
    {
      try
      {
        String str = Build.SERIAL;
        return str;
      }
      catch (Exception localException)
      {
      }
      return null;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.DeviceUtils
 * JD-Core Version:    0.6.2
 */