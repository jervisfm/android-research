package com.swarmconnect.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class SeparatedListAdapter extends BaseAdapter
{
  public static final int TYPE_SECTION_HEADER;
  public final ArrayAdapter<String> headers;
  public final Map<String, BaseAdapter> sections = new LinkedHashMap();

  public SeparatedListAdapter(Context paramContext, int paramInt)
  {
    this.headers = new ArrayAdapter(paramContext, paramInt);
  }

  public void addSection(String paramString, BaseAdapter paramBaseAdapter)
  {
    this.headers.add(paramString);
    this.sections.put(paramString, paramBaseAdapter);
    notifyDataSetChanged();
  }

  public boolean areAllItemsSelectable()
  {
    return false;
  }

  public int getCount()
  {
    Iterator localIterator = this.sections.values().iterator();
    int i = 0;
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      Adapter localAdapter = (Adapter)localIterator.next();
      if (localAdapter.getCount() != 0)
        i += 1 + localAdapter.getCount();
    }
  }

  public Object getItem(int paramInt)
  {
    Iterator localIterator = this.sections.keySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return null;
      Object localObject = localIterator.next();
      Adapter localAdapter = (Adapter)this.sections.get(localObject);
      if (localAdapter.getCount() != 0)
      {
        int i = 1 + localAdapter.getCount();
        if (paramInt == 0)
          return localObject;
        if (paramInt < i)
          return localAdapter.getItem(paramInt - 1);
        paramInt -= i;
      }
    }
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    Iterator localIterator = this.sections.keySet().iterator();
    int i = 1;
    while (true)
    {
      if (!localIterator.hasNext())
        return -1;
      Object localObject = localIterator.next();
      Adapter localAdapter = (Adapter)this.sections.get(localObject);
      if (localAdapter.getCount() != 0)
      {
        int j = 1 + localAdapter.getCount();
        if (paramInt == 0)
          return 0;
        if (paramInt < j)
          return i + localAdapter.getItemViewType(paramInt - 1);
        paramInt -= j;
        i += localAdapter.getViewTypeCount();
      }
    }
  }

  public int getSectionForPosition(int paramInt)
  {
    int i;
    if (paramInt < 0)
      i = -1;
    label94: 
    while (true)
    {
      return i;
      Iterator localIterator = this.sections.keySet().iterator();
      i = 0;
      while (true)
      {
        if (!localIterator.hasNext())
          break label94;
        Object localObject = localIterator.next();
        Adapter localAdapter = (Adapter)this.sections.get(localObject);
        if (localAdapter.getCount() == 0)
        {
          i++;
        }
        else
        {
          paramInt -= 1 + localAdapter.getCount();
          if (paramInt < 0)
            break;
          i++;
        }
      }
    }
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Iterator localIterator = this.sections.keySet().iterator();
    for (int i = 0; ; i++)
    {
      if (!localIterator.hasNext())
        return null;
      Object localObject = localIterator.next();
      Adapter localAdapter = (Adapter)this.sections.get(localObject);
      if (localAdapter.getCount() > 0)
      {
        int j = 1 + localAdapter.getCount();
        if (paramInt == 0)
          return this.headers.getView(i, paramView, paramViewGroup);
        if (paramInt < j)
          return localAdapter.getView(paramInt - 1, paramView, paramViewGroup);
        paramInt -= j;
      }
    }
  }

  public int getViewTypeCount()
  {
    Iterator localIterator = this.sections.values().iterator();
    int i = 1;
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      i += ((Adapter)localIterator.next()).getViewTypeCount();
    }
  }

  public boolean isEnabled(int paramInt)
  {
    return getItemViewType(paramInt) != 0;
  }

  public void notifyDataSetChanged()
  {
    super.notifyDataSetChanged();
    Iterator localIterator = this.sections.values().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((BaseAdapter)localIterator.next()).notifyDataSetChanged();
    }
  }

  public void removeSection(String paramString)
  {
    this.headers.remove(paramString);
    this.sections.remove(paramString);
    notifyDataSetChanged();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.SeparatedListAdapter
 * JD-Core Version:    0.6.2
 */