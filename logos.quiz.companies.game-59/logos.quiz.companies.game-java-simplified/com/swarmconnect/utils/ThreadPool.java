package com.swarmconnect.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool
{
  private static final ExecutorService a = Executors.newFixedThreadPool(5);

  public static void execute(Runnable paramRunnable)
  {
    a.execute(paramRunnable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.swarmconnect.utils.ThreadPool
 * JD-Core Version:    0.6.2
 */