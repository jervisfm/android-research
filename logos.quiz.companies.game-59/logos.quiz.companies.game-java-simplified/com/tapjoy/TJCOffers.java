package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import java.util.UUID;
import org.w3c.dom.Document;

public class TJCOffers
{
  public static final String TAPJOY_OFFERS = "TapjoyOffers";
  public static final String TAPJOY_POINTS = "TapjoyPoints";
  private static TapjoyAwardPointsNotifier tapjoyAwardPointsNotifier;
  private static TapjoyEarnedPointsNotifier tapjoyEarnedPointsNotifier;
  private static TapjoyNotifier tapjoyNotifier;
  private static TapjoySpendPointsNotifier tapjoySpendPointsNotifier;
  int awardTapPoints = 0;
  Context context;
  private String multipleCurrencyID = "";
  private String multipleCurrencySelector = "";
  String spendTapPoints = null;

  public TJCOffers(Context paramContext)
  {
    this.context = paramContext;
  }

  private boolean handleAwardPointsResponse(String paramString)
  {
    Document localDocument = TapjoyUtil.buildDocument(paramString);
    String str1;
    if (localDocument != null)
    {
      str1 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str1 == null) || (!str1.equals("true")))
        break label104;
      String str3 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
      String str4 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
      if ((str3 != null) && (str4 != null))
      {
        TapjoyConnectCore.saveTapPointsTotal(Integer.parseInt(str3));
        tapjoyAwardPointsNotifier.getAwardPointsResponse(str4, Integer.parseInt(str3));
        return true;
      }
      TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing tags.");
    }
    while (true)
    {
      return false;
      label104: if ((str1 != null) && (str1.endsWith("false")))
      {
        String str2 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Message"));
        TapjoyLog.i("TapjoyPoints", str2);
        tapjoyAwardPointsNotifier.getAwardPointsResponseFailed(str2);
        return true;
      }
      TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
    }
  }

  private boolean handleGetPointsResponse(String paramString)
  {
    while (true)
    {
      try
      {
        Document localDocument = TapjoyUtil.buildDocument(paramString);
        if (localDocument == null)
          break label211;
        String str1 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
        if ((str1 != null) && (str1.equals("true")))
        {
          String str2 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
          String str3 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
          if ((str2 != null) && (str3 != null))
            try
            {
              int i = Integer.parseInt(str2);
              int j = TapjoyConnectCore.getLocalTapPointsTotal();
              if ((tapjoyEarnedPointsNotifier != null) && (j != -9999) && (i > j))
              {
                TapjoyLog.i("TapjoyPoints", "earned: " + (i - j));
                tapjoyEarnedPointsNotifier.earnedTapPoints(i - j);
              }
              TapjoyConnectCore.saveTapPointsTotal(Integer.parseInt(str2));
              tapjoyNotifier.getUpdatePoints(str3, Integer.parseInt(str2));
              bool = true;
              return bool;
            }
            catch (Exception localException)
            {
              TapjoyLog.e("TapjoyPoints", "Error parsing XML.");
            }
          else
            TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing tags.");
        }
      }
      finally
      {
      }
      TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
      label211: boolean bool = false;
    }
  }

  private boolean handleSpendPointsResponse(String paramString)
  {
    Document localDocument = TapjoyUtil.buildDocument(paramString);
    String str1;
    if (localDocument != null)
    {
      str1 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Success"));
      if ((str1 == null) || (!str1.equals("true")))
        break label104;
      String str3 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("TapPoints"));
      String str4 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("CurrencyName"));
      if ((str3 != null) && (str4 != null))
      {
        TapjoyConnectCore.saveTapPointsTotal(Integer.parseInt(str3));
        tapjoySpendPointsNotifier.getSpendPointsResponse(str4, Integer.parseInt(str3));
        return true;
      }
      TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing tags.");
    }
    while (true)
    {
      return false;
      label104: if ((str1 != null) && (str1.endsWith("false")))
      {
        String str2 = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Message"));
        TapjoyLog.i("TapjoyPoints", str2);
        tapjoySpendPointsNotifier.getSpendPointsResponseFailed(str2);
        return true;
      }
      TapjoyLog.e("TapjoyPoints", "Invalid XML: Missing <Success> tag.");
    }
  }

  public void awardTapPoints(int paramInt, TapjoyAwardPointsNotifier paramTapjoyAwardPointsNotifier)
  {
    if (paramInt < 0)
    {
      TapjoyLog.e("TapjoyPoints", "spendTapPoints error: amount must be a positive number");
      return;
    }
    this.awardTapPoints = paramInt;
    tapjoyAwardPointsNotifier = paramTapjoyAwardPointsNotifier;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = UUID.randomUUID().toString();
        long l = System.currentTimeMillis() / 1000L;
        String str2 = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(TapjoyConnectCore.getURLParams())).append("&tap_points=").append(TJCOffers.this.awardTapPoints).toString())).append("&publisher_user_id=").append(TapjoyConnectCore.getUserID()).toString())).append("&guid=").append(str1).toString())).append("&timestamp=").append(l).toString() + "&verifier=" + TapjoyConnectCore.getAwardPointsVerifier(l, TJCOffers.this.awardTapPoints, str1);
        String str3 = new TapjoyURLConnection().connectToURL("https://ws.tapjoyads.com/points/award?", str2);
        boolean bool = false;
        if (str3 != null)
          bool = TJCOffers.this.handleAwardPointsResponse(str3);
        if (!bool)
          TJCOffers.tapjoyAwardPointsNotifier.getAwardPointsResponseFailed("Failed to award points.");
      }
    }).start();
  }

  public void getTapPoints(TapjoyNotifier paramTapjoyNotifier)
  {
    tapjoyNotifier = paramTapjoyNotifier;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = TapjoyConnectCore.getURLParams() + "&publisher_user_id=" + TapjoyConnectCore.getUserID();
        String str2 = new TapjoyURLConnection().connectToURL("https://ws.tapjoyads.com/get_vg_store_items/user_account?", str1);
        boolean bool = false;
        if (str2 != null)
          bool = TJCOffers.this.handleGetPointsResponse(str2);
        if (!bool)
          TJCOffers.tapjoyNotifier.getUpdatePointsFailed("Failed to retrieve points from server");
      }
    }).start();
  }

  public void setEarnedPointsNotifier(TapjoyEarnedPointsNotifier paramTapjoyEarnedPointsNotifier)
  {
    tapjoyEarnedPointsNotifier = paramTapjoyEarnedPointsNotifier;
  }

  public void showOffers()
  {
    TapjoyLog.i("TapjoyOffers", "Showing offers with userID: " + TapjoyConnectCore.getUserID());
    Intent localIntent = new Intent(this.context, TJCOffersWebView.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("USER_ID", TapjoyConnectCore.getUserID());
    localIntent.putExtra("URL_PARAMS", TapjoyConnectCore.getURLParams());
    this.context.startActivity(localIntent);
  }

  public void showOffersWithCurrencyID(String paramString, boolean paramBoolean)
  {
    TapjoyLog.i("TapjoyOffers", "Showing offers with currencyID: " + paramString + ", selector: " + paramBoolean + " (userID = " + TapjoyConnectCore.getUserID() + ")");
    this.multipleCurrencyID = paramString;
    if (paramBoolean);
    for (String str1 = "1"; ; str1 = "0")
    {
      this.multipleCurrencySelector = str1;
      String str2 = new StringBuilder(String.valueOf(TapjoyConnectCore.getURLParams())).append("&currency_id=").append(this.multipleCurrencyID).toString() + "&currency_selector=" + this.multipleCurrencySelector;
      Intent localIntent = new Intent(this.context, TJCOffersWebView.class);
      localIntent.setFlags(268435456);
      localIntent.putExtra("USER_ID", TapjoyConnectCore.getUserID());
      localIntent.putExtra("URL_PARAMS", str2);
      this.context.startActivity(localIntent);
      return;
    }
  }

  public void spendTapPoints(int paramInt, TapjoySpendPointsNotifier paramTapjoySpendPointsNotifier)
  {
    if (paramInt < 0)
    {
      TapjoyLog.e("TapjoyPoints", "spendTapPoints error: amount must be a positive number");
      return;
    }
    this.spendTapPoints = paramInt;
    tapjoySpendPointsNotifier = paramTapjoySpendPointsNotifier;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = new StringBuilder(String.valueOf(TapjoyConnectCore.getURLParams())).append("&tap_points=").append(TJCOffers.this.spendTapPoints).toString() + "&publisher_user_id=" + TapjoyConnectCore.getUserID();
        String str2 = new TapjoyURLConnection().connectToURL("https://ws.tapjoyads.com/points/spend?", str1);
        boolean bool = false;
        if (str2 != null)
          bool = TJCOffers.this.handleSpendPointsResponse(str2);
        if (!bool)
          TJCOffers.tapjoySpendPointsNotifier.getSpendPointsResponseFailed("Failed to spend points.");
      }
    }).start();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TJCOffers
 * JD-Core Version:    0.6.2
 */