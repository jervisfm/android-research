package com.tapjoy;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.util.Hashtable;

public class TJCOffersWebView extends Activity
{
  final String TAPJOY_OFFERS = "Offers";
  private String clientPackage = "";
  private Dialog dialog = null;
  private String offersURL = null;
  private ProgressBar progressBar;
  private boolean resumeCalled = false;
  private boolean skipOfferWall = false;
  private String urlParams = "";
  private String userID = "";
  private WebView webView = null;

  protected void onCreate(Bundle paramBundle)
  {
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      if (localBundle.getString("DISPLAY_AD_URL") != null)
      {
        this.skipOfferWall = true;
        this.offersURL = localBundle.getString("DISPLAY_AD_URL");
      }
    while (true)
    {
      this.offersURL = this.offersURL.replaceAll(" ", "%20");
      this.clientPackage = TapjoyConnectCore.getClientPackage();
      TapjoyLog.i("Offers", "clientPackage: [" + this.clientPackage + "]");
      super.onCreate(paramBundle);
      requestWindowFeature(1);
      RelativeLayout localRelativeLayout = new RelativeLayout(this);
      this.webView = new WebView(this);
      this.webView.setWebViewClient(new TapjoyWebViewClient(null));
      this.webView.getSettings().setJavaScriptEnabled(true);
      this.progressBar = new ProgressBar(this, null, 16842874);
      this.progressBar.setVisibility(0);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
      localLayoutParams.addRule(13);
      this.progressBar.setLayoutParams(localLayoutParams);
      localRelativeLayout.addView(this.webView, -1, -1);
      localRelativeLayout.addView(this.progressBar);
      setContentView(localRelativeLayout);
      this.webView.loadUrl(this.offersURL);
      TapjoyLog.i("Offers", "Opening URL = [" + this.offersURL + "]");
      return;
      this.skipOfferWall = false;
      this.urlParams = localBundle.getString("URL_PARAMS");
      this.userID = localBundle.getString("USER_ID");
      this.urlParams = (this.urlParams + "&publisher_user_id=" + this.userID);
      if (TapjoyConnectCore.getVideoParams().length() > 0)
        this.urlParams = (this.urlParams + "&" + TapjoyConnectCore.getVideoParams());
      TapjoyLog.i("Offers", "urlParams: [" + this.urlParams + "]");
      this.offersURL = ("https://ws.tapjoyads.com/get_offers/webpage?" + this.urlParams);
      continue;
      TapjoyLog.e("Offers", "Tapjoy offers meta data initialization fail.");
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.webView != null)
    {
      this.webView.clearCache(true);
      this.webView.destroyDrawingCache();
      this.webView.destroy();
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.webView.canGoBack()))
    {
      this.webView.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    if ((this.offersURL != null) && (this.webView != null))
      this.webView.loadUrl(this.offersURL);
    if ((this.resumeCalled) && (TapjoyConnectCore.getInstance() != null))
    {
      TapjoyLog.i("Offers", "call connect");
      TapjoyConnectCore.getInstance().callConnect();
    }
    this.resumeCalled = true;
  }

  private class TapjoyWebViewClient extends WebViewClient
  {
    private TapjoyWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      TJCOffersWebView.this.progressBar.setVisibility(8);
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      TJCOffersWebView.this.progressBar.setVisibility(0);
      TJCOffersWebView.this.progressBar.bringToFront();
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      TapjoyLog.i("Offers", "URL = [" + paramString + "]");
      Hashtable localHashtable;
      int i;
      int j;
      String str1;
      String str2;
      String str3;
      if (paramString.startsWith("tjvideo://"))
      {
        localHashtable = new Hashtable();
        i = paramString.indexOf("://") + "://".length();
        j = 0;
        str1 = "";
        str2 = "";
        if ((i >= paramString.length()) || (i == -1))
        {
          if ((j == 1) && (str1.length() > 0))
          {
            String str9 = Uri.decode(str1);
            TapjoyLog.i("Offers", "k:v: " + str2 + ", " + str9);
            localHashtable.put(str2, str9);
          }
          str3 = (String)localHashtable.get("video_id");
          String str4 = (String)localHashtable.get("amount");
          String str5 = (String)localHashtable.get("currency_name");
          String str6 = (String)localHashtable.get("click_url");
          String str7 = (String)localHashtable.get("video_complete_url");
          String str8 = (String)localHashtable.get("video_url");
          TapjoyLog.i("Offers", "videoID: " + str3);
          TapjoyLog.i("Offers", "currencyAmount: " + str4);
          TapjoyLog.i("Offers", "currencyName: " + str5);
          TapjoyLog.i("Offers", "clickURL: " + str6);
          TapjoyLog.i("Offers", "webviewURL: " + str7);
          TapjoyLog.i("Offers", "videoURL: " + str8);
          if (!TapjoyVideo.getInstance().startVideo(str3, str5, str4, str6, str7, str8))
            break label534;
          TapjoyLog.i("Offers", "VIDEO");
        }
      }
      while (true)
      {
        return true;
        char c = paramString.charAt(i);
        if (j == 0)
          if (c == '=')
          {
            j = 1;
            str2 = Uri.decode(str1);
            str1 = "";
          }
        while (true)
        {
          i++;
          break;
          str1 = str1 + c;
          continue;
          if (j == 1)
            if (c == '&')
            {
              String str10 = Uri.decode(str1);
              str1 = "";
              TapjoyLog.i("Offers", "k:v: " + str2 + ", " + str10);
              localHashtable.put(str2, str10);
              j = 0;
            }
            else
            {
              str1 = str1 + c;
            }
        }
        label534: TapjoyLog.e("Offers", "Unable to play video: " + str3);
        TJCOffersWebView.this.dialog = new AlertDialog.Builder(TJCOffersWebView.this).setTitle("").setMessage("Unable to play video.").setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            paramAnonymousDialogInterface.dismiss();
          }
        }).create();
        try
        {
          TJCOffersWebView.this.dialog.show();
        }
        catch (Exception localException)
        {
          TapjoyLog.e("Offers", "e: " + localException.toString());
        }
        continue;
        if (paramString.contains("ws.tapjoyads.com"))
        {
          TapjoyLog.i("Offers", "Open redirecting URL = [" + paramString + "]");
          paramWebView.loadUrl(paramString);
        }
        else
        {
          TapjoyLog.i("Offers", "Opening URL in new browser = [" + paramString + "]");
          Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
          TJCOffersWebView.this.startActivity(localIntent);
          TapjoyLog.i("Offers", "skipOfferWall: " + TJCOffersWebView.this.skipOfferWall);
          if (TJCOffersWebView.this.skipOfferWall)
            TJCOffersWebView.this.finish();
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TJCOffersWebView
 * JD-Core Version:    0.6.2
 */