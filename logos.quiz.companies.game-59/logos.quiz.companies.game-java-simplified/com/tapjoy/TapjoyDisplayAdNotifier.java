package com.tapjoy;

import android.view.View;

public abstract interface TapjoyDisplayAdNotifier
{
  public abstract void getDisplayAdResponse(View paramView);

  public abstract void getDisplayAdResponseFailed(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyDisplayAdNotifier
 * JD-Core Version:    0.6.2
 */