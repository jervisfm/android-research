package com.tapjoy;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class TapjoyDisplayMetricsUtil
{
  private Configuration configuration;
  private Context context;
  private DisplayMetrics metrics;

  public TapjoyDisplayMetricsUtil(Context paramContext)
  {
    this.context = paramContext;
    this.metrics = new DisplayMetrics();
    ((WindowManager)this.context.getSystemService("window")).getDefaultDisplay().getMetrics(this.metrics);
    this.configuration = this.context.getResources().getConfiguration();
  }

  public int getScreenDensity()
  {
    return this.metrics.densityDpi;
  }

  public int getScreenLayoutSize()
  {
    return 0xF & this.configuration.screenLayout;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyDisplayMetricsUtil
 * JD-Core Version:    0.6.2
 */