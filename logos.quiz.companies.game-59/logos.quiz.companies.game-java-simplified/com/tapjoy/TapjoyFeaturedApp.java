package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

public class TapjoyFeaturedApp
{
  private static TapjoyFeaturedAppNotifier featuredAppNotifier;
  public static String featuredAppURLParams;
  private static TapjoyURLConnection tapjoyURLConnection = null;
  final String TAPJOY_FEATURED_APP = "Full Screen Ad";
  private Context context;
  private String currencyID;
  private int displayCount = 5;
  private TapjoyFeaturedAppObject featuredAppObject = null;

  public TapjoyFeaturedApp(Context paramContext)
  {
    this.context = paramContext;
    tapjoyURLConnection = new TapjoyURLConnection();
  }

  private boolean buildResponse(String paramString)
  {
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    try
    {
      ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes("UTF-8"));
      Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localByteArrayInputStream);
      this.featuredAppObject.cost = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Cost"));
      String str = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Amount"));
      if (str != null)
        this.featuredAppObject.amount = Integer.parseInt(str);
      this.featuredAppObject.description = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Description"));
      this.featuredAppObject.iconURL = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("IconURL"));
      this.featuredAppObject.name = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("Name"));
      this.featuredAppObject.redirectURL = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("RedirectURL"));
      this.featuredAppObject.storeID = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("StoreID"));
      this.featuredAppObject.fullScreenAdURL = TapjoyUtil.getNodeTrimValue(localDocument.getElementsByTagName("FullScreenAdURL"));
      TapjoyLog.i("Full Screen Ad", "cost: " + this.featuredAppObject.cost);
      TapjoyLog.i("Full Screen Ad", "amount: " + this.featuredAppObject.amount);
      TapjoyLog.i("Full Screen Ad", "description: " + this.featuredAppObject.description);
      TapjoyLog.i("Full Screen Ad", "iconURL: " + this.featuredAppObject.iconURL);
      TapjoyLog.i("Full Screen Ad", "name: " + this.featuredAppObject.name);
      TapjoyLog.i("Full Screen Ad", "redirectURL: " + this.featuredAppObject.redirectURL);
      TapjoyLog.i("Full Screen Ad", "storeID: " + this.featuredAppObject.storeID);
      TapjoyLog.i("Full Screen Ad", "fullScreenAdURL: " + this.featuredAppObject.fullScreenAdURL);
      if (this.featuredAppObject.fullScreenAdURL != null)
      {
        int i = this.featuredAppObject.fullScreenAdURL.length();
        if (i != 0)
          break label503;
      }
      label503: for (bool = false; ; bool = true)
      {
        if (!bool)
          break label555;
        if (getDisplayCountForStoreID(this.featuredAppObject.storeID) >= this.displayCount)
          break;
        featuredAppNotifier.getFeaturedAppResponse(this.featuredAppObject);
        if (!TapjoyConnectCore.getAppID().equals(this.featuredAppObject.storeID))
          incrementDisplayCountForStoreID(this.featuredAppObject.storeID);
        return bool;
      }
    }
    catch (Exception localException)
    {
      boolean bool;
      while (true)
      {
        TapjoyLog.e("Full Screen Ad", "Error parsing XML: " + localException.toString());
        bool = false;
      }
      featuredAppNotifier.getFeaturedAppResponseFailed("Full Screen Ad to display has exceeded display count");
      return bool;
    }
    label555: featuredAppNotifier.getFeaturedAppResponseFailed("Failed to parse XML file from response");
    return true;
  }

  private int getDisplayCountForStoreID(String paramString)
  {
    int i = this.context.getSharedPreferences("TapjoyFeaturedAppPrefs", 0).getInt(paramString, 0);
    TapjoyLog.i("Full Screen Ad", "getDisplayCount: " + i + ", storeID: " + paramString);
    return i;
  }

  private void incrementDisplayCountForStoreID(String paramString)
  {
    SharedPreferences localSharedPreferences = this.context.getSharedPreferences("TapjoyFeaturedAppPrefs", 0);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    int i = 1 + localSharedPreferences.getInt(paramString, 0);
    TapjoyLog.i("Full Screen Ad", "incrementDisplayCount: " + i + ", storeID: " + paramString);
    localEditor.putInt(paramString, i);
    localEditor.commit();
  }

  public void getFeaturedApp(TapjoyFeaturedAppNotifier paramTapjoyFeaturedAppNotifier)
  {
    TapjoyLog.i("Full Screen Ad", "Getting Full Screen Ad");
    getFeaturedApp(null, paramTapjoyFeaturedAppNotifier);
  }

  public void getFeaturedApp(String paramString, TapjoyFeaturedAppNotifier paramTapjoyFeaturedAppNotifier)
  {
    this.currencyID = paramString;
    TapjoyLog.i("Full Screen Ad", "Getting Full Screen Ad userID: " + TapjoyConnectCore.getUserID() + ", currencyID: " + this.currencyID);
    featuredAppNotifier = paramTapjoyFeaturedAppNotifier;
    this.featuredAppObject = new TapjoyFeaturedAppObject();
    featuredAppURLParams = TapjoyConnectCore.getURLParams();
    featuredAppURLParams = featuredAppURLParams + "&publisher_user_id=" + TapjoyConnectCore.getUserID();
    if (this.currencyID != null)
      featuredAppURLParams = featuredAppURLParams + "&currency_id=" + this.currencyID;
    new Thread(new Runnable()
    {
      public void run()
      {
        String str = TapjoyFeaturedApp.tapjoyURLConnection.connectToURL("https://ws.tapjoyads.com/get_offers/featured?", TapjoyFeaturedApp.featuredAppURLParams);
        boolean bool = false;
        if (str != null)
          bool = TapjoyFeaturedApp.this.buildResponse(str);
        if (!bool)
          TapjoyFeaturedApp.featuredAppNotifier.getFeaturedAppResponseFailed("Error retrieving full screen ad data from the server.");
      }
    }).start();
  }

  public TapjoyFeaturedAppObject getFeaturedAppObject()
  {
    return this.featuredAppObject;
  }

  public void setDisplayCount(int paramInt)
  {
    this.displayCount = paramInt;
  }

  public void showFeaturedAppFullScreenAd()
  {
    String str1 = "";
    if (this.featuredAppObject != null)
      str1 = this.featuredAppObject.fullScreenAdURL;
    TapjoyLog.i("Full Screen Ad", "Displaying Full Screen AD with URL: " + str1);
    if (str1.length() != 0)
    {
      String str2 = TapjoyConnectCore.getURLParams();
      if ((this.currencyID != null) && (this.currencyID.length() > 0))
        str2 = str2 + "&currency_id=" + this.currencyID;
      Intent localIntent = new Intent(this.context, TapjoyFeaturedAppWebView.class);
      localIntent.setFlags(268435456);
      localIntent.putExtra("USER_ID", TapjoyConnectCore.getUserID());
      localIntent.putExtra("URL_PARAMS", str2);
      localIntent.putExtra("FULLSCREEN_AD_URL", str1);
      this.context.startActivity(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyFeaturedApp
 * JD-Core Version:    0.6.2
 */