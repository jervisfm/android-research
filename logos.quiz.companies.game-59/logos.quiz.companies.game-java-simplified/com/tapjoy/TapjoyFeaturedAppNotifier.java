package com.tapjoy;

public abstract interface TapjoyFeaturedAppNotifier
{
  public abstract void getFeaturedAppResponse(TapjoyFeaturedAppObject paramTapjoyFeaturedAppObject);

  public abstract void getFeaturedAppResponseFailed(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyFeaturedAppNotifier
 * JD-Core Version:    0.6.2
 */