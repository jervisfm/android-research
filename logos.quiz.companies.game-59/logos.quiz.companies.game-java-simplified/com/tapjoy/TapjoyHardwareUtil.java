package com.tapjoy;

import android.os.Build;
import java.lang.reflect.Field;

public class TapjoyHardwareUtil
{
  public String getSerial()
  {
    String str = null;
    try
    {
      Field localField = Class.forName("android.os.Build").getDeclaredField("SERIAL");
      if (!localField.isAccessible())
        localField.setAccessible(true);
      str = localField.get(Build.class).toString();
      TapjoyLog.i("TapjoyHardwareUtil", "serial: " + str);
      return str;
    }
    catch (Exception localException)
    {
      TapjoyLog.e("TapjoyHardwareUtil", localException.toString());
    }
    return str;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyHardwareUtil
 * JD-Core Version:    0.6.2
 */