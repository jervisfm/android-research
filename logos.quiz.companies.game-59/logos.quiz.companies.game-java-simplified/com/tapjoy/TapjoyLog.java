package com.tapjoy;

import android.util.Log;

public class TapjoyLog
{
  private static final int MAX_STRING_SIZE = 4096;
  private static boolean showLog = false;

  public static void d(String paramString1, String paramString2)
  {
    if (showLog)
      Log.d(paramString1, paramString2);
  }

  public static void e(String paramString1, String paramString2)
  {
    if (showLog)
      Log.e(paramString1, paramString2);
  }

  public static void enableLogging(boolean paramBoolean)
  {
    Log.i("TapjoyLog", "enableLogging: " + paramBoolean);
    showLog = paramBoolean;
  }

  public static void i(String paramString1, String paramString2)
  {
    if (showLog)
    {
      if (paramString2.length() <= 4096);
    }
    else
      for (int i = 0; ; i++)
      {
        if (i > paramString2.length() / 4096)
          return;
        int j = i * 4096;
        int k = 4096 * (i + 1);
        if (k > paramString2.length())
          k = paramString2.length();
        Log.i(paramString1, paramString2.substring(j, k));
      }
    Log.i(paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2)
  {
    if (showLog)
      Log.v(paramString1, paramString2);
  }

  public static void w(String paramString1, String paramString2)
  {
    if (showLog)
      Log.w(paramString1, paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyLog
 * JD-Core Version:    0.6.2
 */