package com.tapjoy;

public abstract interface TapjoyNotifier
{
  public abstract void getUpdatePoints(String paramString, int paramInt);

  public abstract void getUpdatePointsFailed(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyNotifier
 * JD-Core Version:    0.6.2
 */