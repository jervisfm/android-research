package com.tapjoy;

public abstract interface TapjoyReEngagementAdNotifier
{
  public abstract void getReEngagementAdResponse();

  public abstract void getReEngagementAdResponseFailed(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyReEngagementAdNotifier
 * JD-Core Version:    0.6.2
 */