package com.tapjoy;

public abstract interface TapjoySpendPointsNotifier
{
  public abstract void getSpendPointsResponse(String paramString, int paramInt);

  public abstract void getSpendPointsResponseFailed(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoySpendPointsNotifier
 * JD-Core Version:    0.6.2
 */