package com.tapjoy;

import android.net.Uri;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

public class TapjoyURLConnection
{
  private static final String TAPJOY_URL_CONNECTION = "TapjoyURLConnection";
  public static final int TYPE_GET = 0;
  public static final int TYPE_POST = 1;

  public String connectToURL(String paramString)
  {
    return connectToURL(paramString, "");
  }

  public String connectToURL(String paramString1, String paramString2)
  {
    String str1 = null;
    try
    {
      String str2 = (paramString1 + paramString2).replaceAll(" ", "%20");
      TapjoyLog.i("TapjoyURLConnection", "baseURL: " + paramString1);
      TapjoyLog.i("TapjoyURLConnection", "requestURL: " + str2);
      HttpURLConnection localHttpURLConnection = (HttpURLConnection)new URL(str2).openConnection();
      localHttpURLConnection.setConnectTimeout(15000);
      localHttpURLConnection.setReadTimeout(30000);
      str1 = localHttpURLConnection.getResponseMessage();
      localHttpURLConnection.connect();
      localBufferedReader = new BufferedReader(new InputStreamReader(localHttpURLConnection.getInputStream()));
    }
    catch (Exception localException2)
    {
      try
      {
        BufferedReader localBufferedReader;
        StringBuilder localStringBuilder = new StringBuilder();
        try
        {
          while (true)
          {
            String str3 = localBufferedReader.readLine();
            if (str3 == null)
            {
              str1 = localStringBuilder.toString();
              TapjoyLog.i("TapjoyURLConnection", "--------------------");
              TapjoyLog.i("TapjoyURLConnection", "response size: " + str1.length());
              TapjoyLog.i("TapjoyURLConnection", "response: ");
              TapjoyLog.i("TapjoyURLConnection", str1);
              TapjoyLog.i("TapjoyURLConnection", "--------------------");
              return str1;
            }
            localStringBuilder.append(str3 + '\n');
          }
        }
        catch (Exception localException1)
        {
        }
        while (true)
        {
          label261: TapjoyLog.e("TapjoyURLConnection", "Exception: " + localException1.toString());
          return str1;
          localException2 = localException2;
        }
      }
      catch (Exception localException3)
      {
        break label261;
      }
    }
  }

  public String connectToURLwithPOST(String paramString, Hashtable<String, String> paramHashtable1, Hashtable<String, String> paramHashtable2)
  {
    String str1 = null;
    while (true)
    {
      ArrayList localArrayList;
      Iterator localIterator2;
      try
      {
        String str2 = paramString.replaceAll(" ", "%20");
        TapjoyLog.i("TapjoyURLConnection", "baseURL: " + paramString);
        TapjoyLog.i("TapjoyURLConnection", "requestURL: " + str2);
        HttpPost localHttpPost = new HttpPost(str2);
        localArrayList = new ArrayList();
        Iterator localIterator1 = paramHashtable1.entrySet().iterator();
        if (!localIterator1.hasNext())
        {
          str1 = null;
          if (paramHashtable2 != null)
          {
            int i = paramHashtable2.size();
            str1 = null;
            if (i > 0)
            {
              localIterator2 = paramHashtable2.entrySet().iterator();
              if (localIterator2.hasNext())
                break label465;
            }
          }
          localHttpPost.setEntity(new UrlEncodedFormEntity(localArrayList));
          TapjoyLog.i("TapjoyURLConnection", "HTTP POST: " + localHttpPost.toString());
          BasicHttpParams localBasicHttpParams = new BasicHttpParams();
          HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 15000);
          HttpConnectionParams.setSoTimeout(localBasicHttpParams, 30000);
          HttpResponse localHttpResponse = new DefaultHttpClient(localBasicHttpParams).execute(localHttpPost);
          str1 = EntityUtils.toString(localHttpResponse.getEntity());
          TapjoyLog.i("TapjoyURLConnection", "--------------------");
          TapjoyLog.i("TapjoyURLConnection", "response status: " + localHttpResponse.getStatusLine().getStatusCode());
          TapjoyLog.i("TapjoyURLConnection", "response size: " + str1.length());
          TapjoyLog.i("TapjoyURLConnection", "response: ");
          TapjoyLog.i("TapjoyURLConnection", str1);
          TapjoyLog.i("TapjoyURLConnection", "--------------------");
          return str1;
        }
        else
        {
          Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
          localArrayList.add(new BasicNameValuePair((String)localEntry2.getKey(), (String)localEntry2.getValue()));
          TapjoyLog.i("TapjoyURLConnection", "key: " + (String)localEntry2.getKey() + ", value: " + Uri.encode((String)localEntry2.getValue()));
          continue;
        }
      }
      catch (Exception localException)
      {
        TapjoyLog.e("TapjoyURLConnection", "Exception: " + localException.toString());
        return str1;
      }
      label465: Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
      localArrayList.add(new BasicNameValuePair("data[" + (String)localEntry1.getKey() + "]", (String)localEntry1.getValue()));
      TapjoyLog.i("TapjoyURLConnection", "key: " + (String)localEntry1.getKey() + ", value: " + Uri.encode((String)localEntry1.getValue()));
    }
  }

  public String getContentLength(String paramString)
  {
    try
    {
      String str2 = paramString.replaceAll(" ", "%20");
      TapjoyLog.i("TapjoyURLConnection", "requestURL: " + str2);
      HttpURLConnection localHttpURLConnection = (HttpURLConnection)new URL(str2).openConnection();
      localHttpURLConnection.setConnectTimeout(15000);
      localHttpURLConnection.setReadTimeout(30000);
      String str3 = localHttpURLConnection.getHeaderField("content-length");
      str1 = str3;
      TapjoyLog.i("TapjoyURLConnection", "content-length: " + str1);
      return str1;
    }
    catch (Exception localException)
    {
      while (true)
      {
        TapjoyLog.e("TapjoyURLConnection", "Exception: " + localException.toString());
        String str1 = null;
      }
    }
  }

  public TapjoyHttpURLResponse getResponseFromURL(String paramString1, String paramString2)
  {
    return getResponseFromURL(paramString1, paramString2, 0);
  }

  // ERROR //
  public TapjoyHttpURLResponse getResponseFromURL(String paramString1, String paramString2, int paramInt)
  {
    // Byte code:
    //   0: new 260	com/tapjoy/TapjoyHttpURLResponse
    //   3: dup
    //   4: invokespecial 261	com/tapjoy/TapjoyHttpURLResponse:<init>	()V
    //   7: astore 4
    //   9: aconst_null
    //   10: astore 5
    //   12: new 28	java/lang/StringBuilder
    //   15: dup
    //   16: aload_1
    //   17: invokestatic 34	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   20: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   23: aload_2
    //   24: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   30: ldc 47
    //   32: ldc 49
    //   34: invokevirtual 52	java/lang/String:replaceAll	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   37: astore 19
    //   39: ldc 8
    //   41: new 28	java/lang/StringBuilder
    //   44: dup
    //   45: ldc 54
    //   47: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   50: aload_1
    //   51: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   57: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   60: ldc 8
    //   62: new 28	java/lang/StringBuilder
    //   65: dup
    //   66: ldc 62
    //   68: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   71: aload 19
    //   73: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   79: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   82: ldc 8
    //   84: new 28	java/lang/StringBuilder
    //   87: dup
    //   88: ldc_w 263
    //   91: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   94: iload_3
    //   95: invokevirtual 113	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   98: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   101: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   104: new 64	java/net/URL
    //   107: dup
    //   108: aload 19
    //   110: invokespecial 65	java/net/URL:<init>	(Ljava/lang/String;)V
    //   113: invokevirtual 69	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   116: checkcast 71	java/net/HttpURLConnection
    //   119: astore 5
    //   121: aload 5
    //   123: sipush 15000
    //   126: invokevirtual 75	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   129: aload 5
    //   131: sipush 30000
    //   134: invokevirtual 78	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   137: iload_3
    //   138: iconst_1
    //   139: if_icmpne +11 -> 150
    //   142: aload 5
    //   144: ldc_w 265
    //   147: invokevirtual 268	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   150: aload 5
    //   152: invokevirtual 84	java/net/HttpURLConnection:connect	()V
    //   155: aload 4
    //   157: aload 5
    //   159: invokevirtual 271	java/net/HttpURLConnection:getResponseCode	()I
    //   162: putfield 274	com/tapjoy/TapjoyHttpURLResponse:statusCode	I
    //   165: new 86	java/io/BufferedReader
    //   168: dup
    //   169: new 88	java/io/InputStreamReader
    //   172: dup
    //   173: aload 5
    //   175: invokevirtual 92	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   178: invokespecial 95	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   181: invokespecial 98	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   184: astore 7
    //   186: new 28	java/lang/StringBuilder
    //   189: dup
    //   190: invokespecial 99	java/lang/StringBuilder:<init>	()V
    //   193: astore 8
    //   195: aload 7
    //   197: invokevirtual 102	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   200: astore 20
    //   202: aload 20
    //   204: ifnonnull +143 -> 347
    //   207: aload 4
    //   209: aload 8
    //   211: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   214: putfield 277	com/tapjoy/TapjoyHttpURLResponse:response	Ljava/lang/String;
    //   217: aload 5
    //   219: ldc 248
    //   221: invokevirtual 251	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   224: astore 21
    //   226: aload 21
    //   228: ifnull +377 -> 605
    //   231: aload 4
    //   233: aload 21
    //   235: invokestatic 282	java/lang/Integer:valueOf	(Ljava/lang/String;)Ljava/lang/Integer;
    //   238: invokevirtual 285	java/lang/Integer:intValue	()I
    //   241: putfield 288	com/tapjoy/TapjoyHttpURLResponse:contentLength	I
    //   244: aload 8
    //   246: pop
    //   247: aload 7
    //   249: pop
    //   250: ldc 8
    //   252: ldc 104
    //   254: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   257: ldc 8
    //   259: new 28	java/lang/StringBuilder
    //   262: dup
    //   263: ldc 201
    //   265: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   268: aload 4
    //   270: getfield 274	com/tapjoy/TapjoyHttpURLResponse:statusCode	I
    //   273: invokevirtual 113	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   276: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   279: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   282: ldc 8
    //   284: new 28	java/lang/StringBuilder
    //   287: dup
    //   288: ldc 106
    //   290: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   293: aload 4
    //   295: getfield 288	com/tapjoy/TapjoyHttpURLResponse:contentLength	I
    //   298: invokevirtual 113	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   301: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   304: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   307: ldc 8
    //   309: ldc 115
    //   311: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   314: ldc 8
    //   316: new 28	java/lang/StringBuilder
    //   319: dup
    //   320: invokespecial 99	java/lang/StringBuilder:<init>	()V
    //   323: aload 4
    //   325: getfield 277	com/tapjoy/TapjoyHttpURLResponse:response	Ljava/lang/String;
    //   328: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   331: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   334: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   337: ldc 8
    //   339: ldc 104
    //   341: invokestatic 60	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   344: aload 4
    //   346: areturn
    //   347: aload 8
    //   349: new 28	java/lang/StringBuilder
    //   352: dup
    //   353: aload 20
    //   355: invokestatic 34	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   358: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   361: bipush 10
    //   363: invokevirtual 118	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   366: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   369: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: pop
    //   373: goto -178 -> 195
    //   376: astore 6
    //   378: ldc 8
    //   380: new 28	java/lang/StringBuilder
    //   383: dup
    //   384: ldc 120
    //   386: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   389: aload 6
    //   391: invokevirtual 121	java/lang/Exception:toString	()Ljava/lang/String;
    //   394: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   397: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   400: invokestatic 124	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   403: aload 5
    //   405: ifnull +200 -> 605
    //   408: aload 4
    //   410: getfield 277	com/tapjoy/TapjoyHttpURLResponse:response	Ljava/lang/String;
    //   413: ifnonnull +192 -> 605
    //   416: new 86	java/io/BufferedReader
    //   419: dup
    //   420: new 88	java/io/InputStreamReader
    //   423: dup
    //   424: aload 5
    //   426: invokevirtual 291	java/net/HttpURLConnection:getErrorStream	()Ljava/io/InputStream;
    //   429: invokespecial 95	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   432: invokespecial 98	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   435: astore 14
    //   437: new 28	java/lang/StringBuilder
    //   440: dup
    //   441: invokespecial 99	java/lang/StringBuilder:<init>	()V
    //   444: astore 15
    //   446: aload 14
    //   448: invokevirtual 102	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   451: astore 16
    //   453: aload 16
    //   455: ifnonnull +83 -> 538
    //   458: aload 4
    //   460: aload 15
    //   462: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   465: putfield 277	com/tapjoy/TapjoyHttpURLResponse:response	Ljava/lang/String;
    //   468: goto -218 -> 250
    //   471: astore 11
    //   473: ldc 8
    //   475: new 28	java/lang/StringBuilder
    //   478: dup
    //   479: ldc_w 293
    //   482: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   485: aload 11
    //   487: invokevirtual 121	java/lang/Exception:toString	()Ljava/lang/String;
    //   490: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   493: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   496: invokestatic 124	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   499: goto -249 -> 250
    //   502: astore 22
    //   504: ldc 8
    //   506: new 28	java/lang/StringBuilder
    //   509: dup
    //   510: ldc 120
    //   512: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   515: aload 22
    //   517: invokevirtual 121	java/lang/Exception:toString	()Ljava/lang/String;
    //   520: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   523: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   526: invokestatic 124	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   529: aload 8
    //   531: pop
    //   532: aload 7
    //   534: pop
    //   535: goto -285 -> 250
    //   538: aload 15
    //   540: new 28	java/lang/StringBuilder
    //   543: dup
    //   544: aload 16
    //   546: invokestatic 34	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   549: invokespecial 37	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   552: bipush 10
    //   554: invokevirtual 118	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   557: invokevirtual 45	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   560: invokevirtual 41	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   563: pop
    //   564: goto -118 -> 446
    //   567: astore 11
    //   569: aload 8
    //   571: pop
    //   572: aload 7
    //   574: pop
    //   575: goto -102 -> 473
    //   578: astore 11
    //   580: aload 8
    //   582: pop
    //   583: goto -110 -> 473
    //   586: astore 6
    //   588: aconst_null
    //   589: astore 7
    //   591: aconst_null
    //   592: astore 8
    //   594: goto -216 -> 378
    //   597: astore 6
    //   599: aconst_null
    //   600: astore 8
    //   602: goto -224 -> 378
    //   605: aload 8
    //   607: pop
    //   608: aload 7
    //   610: pop
    //   611: goto -361 -> 250
    //
    // Exception table:
    //   from	to	target	type
    //   195	202	376	java/lang/Exception
    //   207	226	376	java/lang/Exception
    //   347	373	376	java/lang/Exception
    //   504	529	376	java/lang/Exception
    //   446	453	471	java/lang/Exception
    //   458	468	471	java/lang/Exception
    //   538	564	471	java/lang/Exception
    //   231	244	502	java/lang/Exception
    //   408	437	567	java/lang/Exception
    //   437	446	578	java/lang/Exception
    //   12	137	586	java/lang/Exception
    //   142	150	586	java/lang/Exception
    //   150	186	586	java/lang/Exception
    //   186	195	597	java/lang/Exception
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyURLConnection
 * JD-Core Version:    0.6.2
 */