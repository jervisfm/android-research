package com.tapjoy;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TapjoyUtil
{
  private static final String TAPJOY_UTIL = "TapjoyUtil";

  public static String SHA1(String paramString)
    throws NoSuchAlgorithmException, UnsupportedEncodingException
  {
    return hashAlgorithm("SHA-1", paramString);
  }

  public static String SHA256(String paramString)
    throws NoSuchAlgorithmException, UnsupportedEncodingException
  {
    return hashAlgorithm("SHA-256", paramString);
  }

  public static Document buildDocument(String paramString)
  {
    try
    {
      DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
      ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes("UTF-8"));
      Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localByteArrayInputStream);
      return localDocument;
    }
    catch (Exception localException)
    {
      TapjoyLog.e("TapjoyUtil", "buildDocument exception: " + localException.toString());
    }
    return null;
  }

  private static String convertToHex(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 0;
    if (i >= paramArrayOfByte.length)
      return localStringBuffer.toString();
    int j = 0xF & paramArrayOfByte[i] >>> 4;
    int m;
    label94: for (int k = 0; ; k = m)
    {
      if ((j >= 0) && (j <= 9))
        localStringBuffer.append((char)(j + 48));
      while (true)
      {
        j = 0xF & paramArrayOfByte[i];
        m = k + 1;
        if (k < 1)
          break label94;
        i++;
        break;
        localStringBuffer.append((char)(97 + (j - 10)));
      }
    }
  }

  public static void deleteFileOrDirectory(File paramFile)
  {
    File[] arrayOfFile;
    int i;
    if (paramFile.isDirectory())
    {
      arrayOfFile = paramFile.listFiles();
      i = arrayOfFile.length;
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        TapjoyLog.i("TapjoyUtil", "****************************************");
        TapjoyLog.i("TapjoyUtil", "deleteFileOrDirectory: " + paramFile.getAbsolutePath());
        TapjoyLog.i("TapjoyUtil", "****************************************");
        paramFile.delete();
        return;
      }
      deleteFileOrDirectory(arrayOfFile[j]);
    }
  }

  public static String getNodeTrimValue(NodeList paramNodeList)
  {
    Element localElement = (Element)paramNodeList.item(0);
    String str1 = "";
    String str2 = null;
    NodeList localNodeList;
    int i;
    if (localElement != null)
    {
      localNodeList = localElement.getChildNodes();
      i = localNodeList.getLength();
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        str2 = null;
        if (str1 != null)
        {
          boolean bool = str1.equals("");
          str2 = null;
          if (!bool)
            str2 = str1.trim();
        }
        return str2;
      }
      Node localNode = localNodeList.item(j);
      if (localNode != null)
        str1 = str1 + localNode.getNodeValue();
    }
  }

  public static String hashAlgorithm(String paramString1, String paramString2)
    throws NoSuchAlgorithmException, UnsupportedEncodingException
  {
    new byte[40];
    MessageDigest localMessageDigest = MessageDigest.getInstance(paramString1);
    localMessageDigest.update(paramString2.getBytes("iso-8859-1"), 0, paramString2.length());
    return convertToHex(localMessageDigest.digest());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyUtil
 * JD-Core Version:    0.6.2
 */