package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TapjoyVideo
{
  public static final String TAPJOY_VIDEO = "TapjoyVideo";
  private static TapjoyVideo tapjoyVideo = null;
  private static TapjoyVideoNotifier tapjoyVideoNotifier;
  private static Bitmap watermarkImage;
  private static final String watermarkURL = "https://s3.amazonaws.com/tapjoy/videos/assets/watermark.png";
  private boolean cache3g = false;
  private boolean cacheAuto = false;
  private boolean cacheWifi = false;
  private Hashtable<String, TapjoyVideoObject> cachedVideos;
  Context context;
  private String imageCacheDir = null;
  private boolean initialized = false;
  private Hashtable<String, TapjoyVideoObject> uncachedVideos;
  private String videoCacheDir = null;
  private int videoCacheLimit = 5;
  private Vector<String> videoQueue;
  private TapjoyVideoObject videoToPlay;

  public TapjoyVideo(Context paramContext)
  {
    this.context = paramContext;
    tapjoyVideo = this;
    this.videoCacheDir = (Environment.getExternalStorageDirectory().toString() + "/tjcache/data/");
    this.imageCacheDir = (Environment.getExternalStorageDirectory().toString() + "/tjcache/tmp/");
    TapjoyUtil.deleteFileOrDirectory(new File(Environment.getExternalStorageDirectory().toString() + "/tapjoy/"));
    TapjoyUtil.deleteFileOrDirectory(new File(this.imageCacheDir));
    this.videoQueue = new Vector();
    this.uncachedVideos = new Hashtable();
    this.cachedVideos = new Hashtable();
    init();
  }

  private void cacheAllVideos()
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        TapjoyLog.i("TapjoyVideo", "--- cacheAllVideos called ---");
        int i = 0;
        while (true)
        {
          if (TapjoyVideo.this.initialized)
          {
            TapjoyLog.i("TapjoyVideo", "cacheVideos connection_type: " + TapjoyConnectCore.getConnectionType());
            TapjoyLog.i("TapjoyVideo", "cache3g: " + TapjoyVideo.this.cache3g);
            TapjoyLog.i("TapjoyVideo", "cacheWifi: " + TapjoyVideo.this.cacheWifi);
            if (((!TapjoyVideo.this.cache3g) || (!TapjoyConnectCore.getConnectionType().equals("mobile"))) && ((!TapjoyVideo.this.cacheWifi) || (!TapjoyConnectCore.getConnectionType().equals("wifi"))))
              break label295;
            if ("mounted".equals(Environment.getExternalStorageState()))
              break;
            TapjoyLog.i("TapjoyVideo", "Media storage unavailable.  Aborting caching videos.");
            TapjoyVideo.videoNotifierError(1);
            return;
          }
          try
          {
            Thread.sleep(500L);
            i += 500;
            if (i > 10000L)
            {
              TapjoyLog.e("TapjoyVideo", "Error during cacheVideos.  Timeout while waiting for initVideos to finish.");
              return;
            }
          }
          catch (Exception localException)
          {
            TapjoyLog.e("TapjoyVideo", "Exception in cacheAllVideos: " + localException.toString());
          }
        }
        while ((TapjoyVideo.this.cachedVideos.size() < TapjoyVideo.this.videoCacheLimit) && (TapjoyVideo.this.videoQueue.size() > 0))
        {
          String str = ((TapjoyVideoObject)TapjoyVideo.this.uncachedVideos.get(TapjoyVideo.this.videoQueue.elementAt(0))).videoURL;
          TapjoyVideo.this.cacheVideo(str);
        }
        while (true)
        {
          TapjoyVideo.this.printCachedVideos();
          return;
          label295: TapjoyLog.i("TapjoyVideo", " * Skipping caching videos because of video flags and connection_type...");
        }
      }
    }).start();
  }

  // ERROR //
  private void cacheVideo(String paramString)
  {
    // Byte code:
    //   0: ldc 8
    //   2: new 63	java/lang/StringBuilder
    //   5: dup
    //   6: ldc 182
    //   8: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   11: aload_1
    //   12: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   18: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   21: invokestatic 194	java/lang/System:currentTimeMillis	()J
    //   24: lstore_2
    //   25: iconst_0
    //   26: istore 4
    //   28: aconst_null
    //   29: astore 5
    //   31: aconst_null
    //   32: astore 6
    //   34: aconst_null
    //   35: astore 7
    //   37: new 196	java/net/URL
    //   40: dup
    //   41: aload_1
    //   42: invokespecial 197	java/net/URL:<init>	(Ljava/lang/String;)V
    //   45: invokevirtual 201	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   48: astore 17
    //   50: aload 17
    //   52: sipush 15000
    //   55: invokevirtual 207	java/net/URLConnection:setConnectTimeout	(I)V
    //   58: aload 17
    //   60: sipush 30000
    //   63: invokevirtual 210	java/net/URLConnection:setReadTimeout	(I)V
    //   66: aload 17
    //   68: invokevirtual 213	java/net/URLConnection:connect	()V
    //   71: new 215	java/io/BufferedInputStream
    //   74: dup
    //   75: aload 17
    //   77: invokevirtual 219	java/net/URLConnection:getInputStream	()Ljava/io/InputStream;
    //   80: invokespecial 222	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   83: astore 18
    //   85: new 71	java/io/File
    //   88: dup
    //   89: aload_0
    //   90: getfield 47	com/tapjoy/TapjoyVideo:videoCacheDir	Ljava/lang/String;
    //   93: invokespecial 96	java/io/File:<init>	(Ljava/lang/String;)V
    //   96: astore 19
    //   98: aload_1
    //   99: iconst_0
    //   100: iconst_1
    //   101: aload_1
    //   102: ldc 224
    //   104: invokevirtual 228	java/lang/String:lastIndexOf	(Ljava/lang/String;)I
    //   107: iadd
    //   108: invokevirtual 232	java/lang/String:substring	(II)Ljava/lang/String;
    //   111: astore 20
    //   113: aload_1
    //   114: iconst_1
    //   115: aload_1
    //   116: ldc 224
    //   118: invokevirtual 228	java/lang/String:lastIndexOf	(Ljava/lang/String;)I
    //   121: iadd
    //   122: invokevirtual 235	java/lang/String:substring	(I)Ljava/lang/String;
    //   125: astore 21
    //   127: aload 21
    //   129: iconst_0
    //   130: aload 21
    //   132: bipush 46
    //   134: invokevirtual 239	java/lang/String:indexOf	(I)I
    //   137: invokevirtual 232	java/lang/String:substring	(II)Ljava/lang/String;
    //   140: astore 22
    //   142: ldc 8
    //   144: new 63	java/lang/StringBuilder
    //   147: dup
    //   148: ldc 241
    //   150: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   153: aload 19
    //   155: invokevirtual 244	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   158: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   161: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   164: ldc 8
    //   166: new 63	java/lang/StringBuilder
    //   169: dup
    //   170: ldc 246
    //   172: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   175: aload 20
    //   177: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   183: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   186: ldc 8
    //   188: new 63	java/lang/StringBuilder
    //   191: dup
    //   192: ldc 248
    //   194: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   197: aload 22
    //   199: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   202: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   205: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   208: aload 19
    //   210: invokevirtual 251	java/io/File:mkdirs	()Z
    //   213: ifeq +28 -> 241
    //   216: ldc 8
    //   218: new 63	java/lang/StringBuilder
    //   221: dup
    //   222: ldc 253
    //   224: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   227: aload 19
    //   229: invokevirtual 256	java/io/File:getPath	()Ljava/lang/String;
    //   232: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   238: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   241: new 71	java/io/File
    //   244: dup
    //   245: aload_0
    //   246: getfield 47	com/tapjoy/TapjoyVideo:videoCacheDir	Ljava/lang/String;
    //   249: aload 22
    //   251: invokespecial 258	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   254: astore 23
    //   256: new 260	java/io/FileOutputStream
    //   259: dup
    //   260: aload 23
    //   262: invokespecial 262	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   265: astore 24
    //   267: ldc 8
    //   269: new 63	java/lang/StringBuilder
    //   272: dup
    //   273: ldc_w 264
    //   276: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   279: aload 23
    //   281: invokevirtual 75	java/io/File:toString	()Ljava/lang/String;
    //   284: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   290: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   293: sipush 1024
    //   296: newarray byte
    //   298: astore 25
    //   300: aload 18
    //   302: aload 25
    //   304: invokevirtual 268	java/io/BufferedInputStream:read	([B)I
    //   307: istore 26
    //   309: iload 26
    //   311: iconst_m1
    //   312: if_icmpne +205 -> 517
    //   315: aload 24
    //   317: invokevirtual 273	java/io/OutputStream:close	()V
    //   320: aload 18
    //   322: invokevirtual 274	java/io/BufferedInputStream:close	()V
    //   325: ldc 8
    //   327: new 63	java/lang/StringBuilder
    //   330: dup
    //   331: ldc_w 276
    //   334: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   337: aload 23
    //   339: invokevirtual 279	java/io/File:length	()J
    //   342: invokevirtual 282	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   345: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   348: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   351: aload 23
    //   353: invokevirtual 279	java/io/File:length	()J
    //   356: lstore 27
    //   358: lload 27
    //   360: lconst_0
    //   361: lcmp
    //   362: ifne +386 -> 748
    //   365: iconst_1
    //   366: istore 9
    //   368: aload 23
    //   370: astore 7
    //   372: aload 24
    //   374: astore 6
    //   376: aload 18
    //   378: astore 5
    //   380: iload 9
    //   382: ifeq +21 -> 403
    //   385: ldc 8
    //   387: ldc_w 284
    //   390: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   393: aload 5
    //   395: invokevirtual 274	java/io/BufferedInputStream:close	()V
    //   398: aload 6
    //   400: invokevirtual 273	java/io/OutputStream:close	()V
    //   403: iload 9
    //   405: ifne +240 -> 645
    //   408: iload 4
    //   410: ifne +235 -> 645
    //   413: aload_0
    //   414: getfield 107	com/tapjoy/TapjoyVideo:videoQueue	Ljava/util/Vector;
    //   417: iconst_0
    //   418: invokevirtual 288	java/util/Vector:elementAt	(I)Ljava/lang/Object;
    //   421: checkcast 77	java/lang/String
    //   424: astore 11
    //   426: aload_0
    //   427: getfield 112	com/tapjoy/TapjoyVideo:uncachedVideos	Ljava/util/Hashtable;
    //   430: aload 11
    //   432: invokevirtual 292	java/util/Hashtable:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   435: checkcast 294	com/tapjoy/TapjoyVideoObject
    //   438: astore 12
    //   440: aload 12
    //   442: aload 7
    //   444: invokevirtual 297	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   447: putfield 300	com/tapjoy/TapjoyVideoObject:dataLocation	Ljava/lang/String;
    //   450: aload_0
    //   451: getfield 114	com/tapjoy/TapjoyVideo:cachedVideos	Ljava/util/Hashtable;
    //   454: aload 11
    //   456: aload 12
    //   458: invokevirtual 304	java/util/Hashtable:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   461: pop
    //   462: aload_0
    //   463: getfield 112	com/tapjoy/TapjoyVideo:uncachedVideos	Ljava/util/Hashtable;
    //   466: aload 11
    //   468: invokevirtual 307	java/util/Hashtable:remove	(Ljava/lang/Object;)Ljava/lang/Object;
    //   471: pop
    //   472: aload_0
    //   473: getfield 107	com/tapjoy/TapjoyVideo:videoQueue	Ljava/util/Vector;
    //   476: iconst_0
    //   477: invokevirtual 310	java/util/Vector:removeElementAt	(I)V
    //   480: aload_0
    //   481: invokespecial 154	com/tapjoy/TapjoyVideo:setVideoIDs	()V
    //   484: ldc 8
    //   486: new 63	java/lang/StringBuilder
    //   489: dup
    //   490: ldc_w 312
    //   493: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   496: invokestatic 194	java/lang/System:currentTimeMillis	()J
    //   499: lload_2
    //   500: lsub
    //   501: invokevirtual 282	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   504: ldc_w 314
    //   507: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   510: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   513: invokestatic 188	com/tapjoy/TapjoyLog:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   516: return
    //   517: aload 24
    //   519: aload 25
    //   521: iconst_0
    //   522: iload 26
    //   524: invokevirtual 318	java/io/OutputStream:write	([BII)V
    //   527: goto -227 -> 300
    //   530: astore 8
    //   532: aload 23
    //   534: astore 7
    //   536: aload 24
    //   538: astore 6
    //   540: aload 18
    //   542: astore 5
    //   544: ldc 8
    //   546: new 63	java/lang/StringBuilder
    //   549: dup
    //   550: ldc_w 320
    //   553: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   556: aload 8
    //   558: invokevirtual 321	java/net/SocketTimeoutException:toString	()Ljava/lang/String;
    //   561: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   564: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   567: invokestatic 324	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   570: iconst_1
    //   571: istore 9
    //   573: iconst_1
    //   574: istore 4
    //   576: goto -196 -> 380
    //   579: astore 16
    //   581: ldc 8
    //   583: new 63	java/lang/StringBuilder
    //   586: dup
    //   587: ldc_w 326
    //   590: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   593: aload 16
    //   595: invokevirtual 327	java/lang/Exception:toString	()Ljava/lang/String;
    //   598: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   601: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   604: invokestatic 324	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   607: iconst_1
    //   608: istore 4
    //   610: iconst_0
    //   611: istore 9
    //   613: goto -233 -> 380
    //   616: astore 10
    //   618: ldc 8
    //   620: new 63	java/lang/StringBuilder
    //   623: dup
    //   624: ldc_w 329
    //   627: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   630: aload 10
    //   632: invokevirtual 327	java/lang/Exception:toString	()Ljava/lang/String;
    //   635: invokevirtual 90	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   638: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   641: invokestatic 324	com/tapjoy/TapjoyLog:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   644: return
    //   645: iconst_2
    //   646: invokestatic 332	com/tapjoy/TapjoyVideo:videoNotifierError	(I)V
    //   649: return
    //   650: astore 15
    //   652: goto -249 -> 403
    //   655: astore 16
    //   657: aload 18
    //   659: astore 5
    //   661: aconst_null
    //   662: astore 6
    //   664: aconst_null
    //   665: astore 7
    //   667: goto -86 -> 581
    //   670: astore 16
    //   672: aload 23
    //   674: astore 7
    //   676: aload 18
    //   678: astore 5
    //   680: aconst_null
    //   681: astore 6
    //   683: goto -102 -> 581
    //   686: astore 16
    //   688: aload 23
    //   690: astore 7
    //   692: aload 24
    //   694: astore 6
    //   696: aload 18
    //   698: astore 5
    //   700: goto -119 -> 581
    //   703: astore 8
    //   705: aconst_null
    //   706: astore 5
    //   708: aconst_null
    //   709: astore 6
    //   711: aconst_null
    //   712: astore 7
    //   714: goto -170 -> 544
    //   717: astore 8
    //   719: aload 18
    //   721: astore 5
    //   723: aconst_null
    //   724: astore 6
    //   726: aconst_null
    //   727: astore 7
    //   729: goto -185 -> 544
    //   732: astore 8
    //   734: aload 23
    //   736: astore 7
    //   738: aload 18
    //   740: astore 5
    //   742: aconst_null
    //   743: astore 6
    //   745: goto -201 -> 544
    //   748: aload 23
    //   750: astore 7
    //   752: aload 24
    //   754: astore 6
    //   756: aload 18
    //   758: astore 5
    //   760: iconst_0
    //   761: istore 4
    //   763: iconst_0
    //   764: istore 9
    //   766: goto -386 -> 380
    //
    // Exception table:
    //   from	to	target	type
    //   267	300	530	java/net/SocketTimeoutException
    //   300	309	530	java/net/SocketTimeoutException
    //   315	358	530	java/net/SocketTimeoutException
    //   517	527	530	java/net/SocketTimeoutException
    //   37	85	579	java/lang/Exception
    //   413	516	616	java/lang/Exception
    //   393	403	650	java/lang/Exception
    //   85	241	655	java/lang/Exception
    //   241	256	655	java/lang/Exception
    //   256	267	670	java/lang/Exception
    //   267	300	686	java/lang/Exception
    //   300	309	686	java/lang/Exception
    //   315	358	686	java/lang/Exception
    //   517	527	686	java/lang/Exception
    //   37	85	703	java/net/SocketTimeoutException
    //   85	241	717	java/net/SocketTimeoutException
    //   241	256	717	java/net/SocketTimeoutException
    //   256	267	732	java/net/SocketTimeoutException
  }

  public static TapjoyVideo getInstance()
  {
    return tapjoyVideo;
  }

  public static Bitmap getWatermarkImage()
  {
    return watermarkImage;
  }

  private boolean handleGetVideosResponse(String paramString)
  {
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    TapjoyLog.i("TapjoyVideo", "========================================");
    while (true)
    {
      int i;
      int k;
      int m;
      try
      {
        ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramString.getBytes("UTF-8"));
        Document localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localByteArrayInputStream);
        localDocument.getDocumentElement().normalize();
        NodeList localNodeList1 = localDocument.getElementsByTagName("TapjoyVideos");
        NodeList localNodeList2 = localNodeList1.item(0).getChildNodes();
        NamedNodeMap localNamedNodeMap = localNodeList1.item(0).getAttributes();
        if ((localNamedNodeMap.getNamedItem("cache_auto") != null) && (localNamedNodeMap.getNamedItem("cache_auto").getNodeValue() != null))
          this.cacheAuto = Boolean.valueOf(localNamedNodeMap.getNamedItem("cache_auto").getNodeValue()).booleanValue();
        if ((localNamedNodeMap.getNamedItem("cache_wifi") != null) && (localNamedNodeMap.getNamedItem("cache_wifi").getNodeValue() != null))
          this.cacheWifi = Boolean.valueOf(localNamedNodeMap.getNamedItem("cache_wifi").getNodeValue()).booleanValue();
        if ((localNamedNodeMap.getNamedItem("cache_mobile") != null) && (localNamedNodeMap.getNamedItem("cache_mobile").getNodeValue() != null))
          this.cache3g = Boolean.valueOf(localNamedNodeMap.getNamedItem("cache_mobile").getNodeValue()).booleanValue();
        TapjoyLog.i("TapjoyVideo", "cacheAuto: " + this.cacheAuto);
        TapjoyLog.i("TapjoyVideo", "cacheWifi: " + this.cacheWifi);
        TapjoyLog.i("TapjoyVideo", "cache3g: " + this.cache3g);
        TapjoyLog.i("TapjoyVideo", "nodelistParent length: " + localNodeList1.getLength());
        TapjoyLog.i("TapjoyVideo", "nodelist length: " + localNodeList2.getLength());
        i = 0;
        int j = localNodeList2.getLength();
        if (i >= j)
        {
          TapjoyLog.i("TapjoyVideo", "========================================");
          return true;
        }
        Node localNode = localNodeList2.item(i);
        TapjoyVideoObject localTapjoyVideoObject = new TapjoyVideoObject();
        if ((localNode != null) && (localNode.getNodeType() == 1))
        {
          Element localElement = (Element)localNode;
          String str1 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("ClickURL"));
          if ((str1 != null) && (!str1.equals("")))
            localTapjoyVideoObject.clickURL = str1;
          String str2 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("OfferID"));
          if ((str2 != null) && (!str2.equals("")))
            localTapjoyVideoObject.offerID = str2;
          String str3 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("Name"));
          if ((str3 != null) && (!str3.equals("")))
            localTapjoyVideoObject.videoAdName = str3;
          String str4 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("Amount"));
          if ((str4 != null) && (!str4.equals("")))
            localTapjoyVideoObject.currencyAmount = str4;
          String str5 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("CurrencyName"));
          if ((str5 != null) && (!str5.equals("")))
            localTapjoyVideoObject.currencyName = str5;
          String str6 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("VideoURL"));
          if ((str6 != null) && (!str6.equals("")))
            localTapjoyVideoObject.videoURL = str6;
          String str7 = TapjoyUtil.getNodeTrimValue(localElement.getElementsByTagName("IconURL"));
          if ((str7 != null) && (!str7.equals("")))
            localTapjoyVideoObject.iconURL = str7;
          TapjoyLog.i("TapjoyVideo", "-----");
          TapjoyLog.i("TapjoyVideo", "videoObject.offerID: " + localTapjoyVideoObject.offerID);
          TapjoyLog.i("TapjoyVideo", "videoObject.videoAdName: " + localTapjoyVideoObject.videoAdName);
          TapjoyLog.i("TapjoyVideo", "videoObject.videoURL: " + localTapjoyVideoObject.videoURL);
          NodeList localNodeList3 = localElement.getElementsByTagName("Buttons").item(0).getChildNodes();
          k = 0;
          if (k >= localNodeList3.getLength())
          {
            this.videoQueue.addElement(localTapjoyVideoObject.offerID);
            this.uncachedVideos.put(localTapjoyVideoObject.offerID, localTapjoyVideoObject);
          }
          else
          {
            NodeList localNodeList4 = localNodeList3.item(k).getChildNodes();
            if (localNodeList4.getLength() == 0)
              break label1157;
            String str8 = "";
            Object localObject = "";
            m = 0;
            if (m >= localNodeList4.getLength())
            {
              TapjoyLog.i("TapjoyVideo", "name: " + str8 + ", url: " + (String)localObject);
              localTapjoyVideoObject.addButton(str8, (String)localObject);
              break label1157;
            }
            if ((Element)localNodeList4.item(m) == null)
              break label1163;
            String str9 = ((Element)localNodeList4.item(m)).getTagName();
            if ((str9.equals("Name")) && (localNodeList4.item(m).getFirstChild() != null))
            {
              str8 = localNodeList4.item(m).getFirstChild().getNodeValue();
            }
            else if ((str9.equals("URL")) && (localNodeList4.item(m).getFirstChild() != null))
            {
              String str10 = localNodeList4.item(m).getFirstChild().getNodeValue();
              localObject = str10;
            }
          }
        }
      }
      catch (Exception localException)
      {
        TapjoyLog.e("TapjoyVideo", "Error parsing XML: " + localException.toString());
        return false;
      }
      i++;
      continue;
      label1157: k++;
      continue;
      label1163: m++;
    }
  }

  private void printCachedVideos()
  {
    TapjoyLog.i("TapjoyVideo", "cachedVideos size: " + this.cachedVideos.size());
    Iterator localIterator = this.cachedVideos.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      TapjoyLog.i("TapjoyVideo", "key: " + (String)localEntry.getKey() + ", name: " + ((TapjoyVideoObject)localEntry.getValue()).videoAdName);
    }
  }

  private void setVideoIDs()
  {
    String str1 = "";
    Enumeration localEnumeration;
    if ((this.cachedVideos != null) && (this.cachedVideos.size() > 0))
      localEnumeration = this.cachedVideos.keys();
    while (true)
    {
      if (!localEnumeration.hasMoreElements())
      {
        TapjoyLog.i("TapjoyVideo", "cachedVideos size: " + this.cachedVideos.size());
        TapjoyLog.i("TapjoyVideo", "videoIDs: [" + str1 + "]");
        TapjoyConnectCore.setVideoIDs(str1);
        return;
      }
      String str2 = (String)localEnumeration.nextElement();
      str1 = str1 + str2;
      if (localEnumeration.hasMoreElements())
        str1 = str1 + ",";
    }
  }

  private boolean validateCachedVideos()
  {
    int i = 1;
    File[] arrayOfFile = new File(this.videoCacheDir).listFiles();
    if (this.uncachedVideos == null)
    {
      TapjoyLog.e("TapjoyVideo", "Error: uncachedVideos is null");
      i = 0;
    }
    if (this.cachedVideos == null)
    {
      TapjoyLog.e("TapjoyVideo", "Error: cachedVideos is null");
      i = 0;
    }
    if (this.videoQueue == null)
    {
      TapjoyLog.e("TapjoyVideo", "Error: videoQueue is null");
      i = 0;
    }
    boolean bool = false;
    int j;
    if (i != 0)
    {
      bool = false;
      if (arrayOfFile != null)
      {
        j = 0;
        if (j < arrayOfFile.length)
          break label94;
        bool = true;
      }
    }
    return bool;
    label94: String str1 = arrayOfFile[j].getName();
    TapjoyLog.i("TapjoyVideo", "-----");
    TapjoyLog.i("TapjoyVideo", "Examining cached file[" + j + "]: " + arrayOfFile[j].getAbsolutePath() + " --- " + arrayOfFile[j].getName());
    TapjoyVideoObject localTapjoyVideoObject;
    if (this.uncachedVideos.containsKey(str1))
    {
      TapjoyLog.i("TapjoyVideo", "Local file found");
      localTapjoyVideoObject = (TapjoyVideoObject)this.uncachedVideos.get(str1);
      if (localTapjoyVideoObject != null);
    }
    while (true)
    {
      j++;
      break;
      String str2 = new TapjoyURLConnection().getContentLength(localTapjoyVideoObject.videoURL);
      TapjoyLog.i("TapjoyVideo", "local file size: " + arrayOfFile[j].length() + " vs. target: " + str2);
      if ((str2 != null) && (Integer.parseInt(str2) == arrayOfFile[j].length()))
      {
        localTapjoyVideoObject.dataLocation = arrayOfFile[j].getAbsolutePath();
        this.cachedVideos.put(str1, localTapjoyVideoObject);
        this.uncachedVideos.remove(str1);
        this.videoQueue.remove(str1);
        TapjoyLog.i("TapjoyVideo", "VIDEO PREVIOUSLY CACHED -- " + str1 + ", location: " + localTapjoyVideoObject.dataLocation);
      }
      else
      {
        TapjoyLog.i("TapjoyVideo", "file size mismatch --- deleting video: " + arrayOfFile[j].getAbsolutePath());
        TapjoyUtil.deleteFileOrDirectory(arrayOfFile[j]);
        continue;
        TapjoyLog.i("TapjoyVideo", "VIDEO EXPIRED? removing video from cache: " + str1 + " --- " + arrayOfFile[j].getAbsolutePath());
        TapjoyUtil.deleteFileOrDirectory(arrayOfFile[j]);
      }
    }
  }

  public static void videoNotifierComplete()
  {
    if (tapjoyVideoNotifier != null)
      tapjoyVideoNotifier.videoComplete();
  }

  public static void videoNotifierError(int paramInt)
  {
    if (tapjoyVideoNotifier != null)
      tapjoyVideoNotifier.videoError(paramInt);
  }

  public static void videoNotifierReady()
  {
    if (tapjoyVideoNotifier != null)
      tapjoyVideoNotifier.videoReady();
  }

  public void enableVideoCache(boolean paramBoolean)
  {
  }

  public TapjoyVideoObject getCurrentVideoData()
  {
    return this.videoToPlay;
  }

  public void init()
  {
    TapjoyLog.i("TapjoyVideo", "initVideoAd");
    if ((TapjoyConnectCore.getFlagValue("disable_video_offers") != null) && (TapjoyConnectCore.getFlagValue("disable_video_offers").equals("true")))
    {
      TapjoyLog.i("TapjoyVideo", "disable_video_offers: " + TapjoyConnectCore.getFlagValue("disable_video_offers") + ". Aborting video initializing... ");
      TapjoyConnectCore.setVideoEnabled(false);
      return;
    }
    setVideoIDs();
    new Thread(new Runnable()
    {
      public void run()
      {
        String str1 = TapjoyConnectCore.getURLParams() + "&publisher_user_id=" + TapjoyConnectCore.getUserID();
        String str2 = new TapjoyURLConnection().connectToURL("https://ws.tapjoyads.com/videos?", str1);
        boolean bool = false;
        if (str2 != null)
        {
          int i = str2.length();
          bool = false;
          if (i > 0)
            bool = TapjoyVideo.this.handleGetVideosResponse(str2);
        }
        if (bool)
        {
          TapjoyVideo.this.validateCachedVideos();
          if (("https://s3.amazonaws.com/tapjoy/videos/assets/watermark.png" != null) && ("https://s3.amazonaws.com/tapjoy/videos/assets/watermark.png".length() > 0));
          try
          {
            URL localURL = new URL("https://s3.amazonaws.com/tapjoy/videos/assets/watermark.png");
            URLConnection localURLConnection = localURL.openConnection();
            localURLConnection.setConnectTimeout(15000);
            localURLConnection.setReadTimeout(25000);
            localURLConnection.connect();
            TapjoyVideo.watermarkImage = BitmapFactory.decodeStream(localURL.openConnection().getInputStream());
            TapjoyVideo.this.setVideoIDs();
            TapjoyVideo.this.initialized = true;
            TapjoyVideo.videoNotifierReady();
            if (TapjoyVideo.this.cacheAuto)
            {
              TapjoyLog.i("TapjoyVideo", "trying to cache because of cache_auto flag...");
              TapjoyVideo.this.cacheAllVideos();
            }
            TapjoyLog.i("TapjoyVideo", "------------------------------");
            TapjoyLog.i("TapjoyVideo", "------------------------------");
            TapjoyLog.i("TapjoyVideo", "INIT DONE!");
            TapjoyLog.i("TapjoyVideo", "------------------------------");
            return;
          }
          catch (Exception localException)
          {
            while (true)
              TapjoyLog.e("TapjoyVideo", "e: " + localException.toString());
          }
        }
        TapjoyVideo.videoNotifierError(2);
      }
    }).start();
    TapjoyConnectCore.setVideoEnabled(true);
  }

  public void initVideoAd(TapjoyVideoNotifier paramTapjoyVideoNotifier)
  {
    initVideoAd(paramTapjoyVideoNotifier, false);
  }

  public void initVideoAd(TapjoyVideoNotifier paramTapjoyVideoNotifier, boolean paramBoolean)
  {
    tapjoyVideoNotifier = paramTapjoyVideoNotifier;
    if (paramTapjoyVideoNotifier == null)
    {
      Log.e("TapjoyVideo", "Error during initVideoAd -- TapjoyVideoNotifier is null");
      return;
    }
    if (this.initialized)
      videoNotifierReady();
    cacheAllVideos();
  }

  public void setVideoCacheCount(int paramInt)
  {
    this.videoCacheLimit = paramInt;
  }

  public boolean startVideo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    int i = 1;
    TapjoyLog.i("TapjoyVideo", "Starting video activity with video: " + paramString1);
    if ((paramString1 == null) || (paramString4 == null) || (paramString5 == null) || (paramString1.length() == 0) || (paramString4.length() == 0) || (paramString5.length() == 0))
    {
      TapjoyLog.i("TapjoyVideo", "aborting video playback... invalid or missing parameter");
      return false;
    }
    this.videoToPlay = ((TapjoyVideoObject)this.cachedVideos.get(paramString1));
    if (!"mounted".equals(Environment.getExternalStorageState()))
    {
      TapjoyLog.e("TapjoyVideo", "Cannot access external storage");
      videoNotifierError(1);
      return false;
    }
    if (this.videoToPlay == null)
    {
      TapjoyLog.i("TapjoyVideo", "video not cached... checking uncached videos");
      this.videoToPlay = ((TapjoyVideoObject)this.uncachedVideos.get(paramString1));
      if (this.videoToPlay == null)
      {
        if ((paramString6 != null) && (paramString6.length() > 0))
        {
          TapjoyVideoObject localTapjoyVideoObject = new TapjoyVideoObject();
          localTapjoyVideoObject.offerID = paramString1;
          localTapjoyVideoObject.currencyName = paramString2;
          localTapjoyVideoObject.currencyAmount = paramString3;
          localTapjoyVideoObject.clickURL = paramString4;
          localTapjoyVideoObject.webviewURL = paramString5;
          localTapjoyVideoObject.videoURL = paramString6;
          this.uncachedVideos.put(paramString1, localTapjoyVideoObject);
          this.videoToPlay = ((TapjoyVideoObject)this.uncachedVideos.get(paramString1));
        }
      }
      else
        i = 0;
    }
    else
    {
      this.videoToPlay.currencyName = paramString2;
      this.videoToPlay.currencyAmount = paramString3;
      this.videoToPlay.clickURL = paramString4;
      this.videoToPlay.webviewURL = paramString5;
      this.videoToPlay.videoURL = paramString6;
      TapjoyLog.i("TapjoyVideo", "videoToPlay: " + this.videoToPlay.offerID);
      TapjoyLog.i("TapjoyVideo", "amount: " + this.videoToPlay.currencyAmount);
      TapjoyLog.i("TapjoyVideo", "currency: " + this.videoToPlay.currencyName);
      TapjoyLog.i("TapjoyVideo", "clickURL: " + this.videoToPlay.clickURL);
      TapjoyLog.i("TapjoyVideo", "location: " + this.videoToPlay.dataLocation);
      TapjoyLog.i("TapjoyVideo", "webviewURL: " + this.videoToPlay.webviewURL);
      TapjoyLog.i("TapjoyVideo", "videoURL: " + this.videoToPlay.videoURL);
      if ((i == 0) || (this.videoToPlay.dataLocation == null))
        break label543;
      File localFile = new File(this.videoToPlay.dataLocation);
      if ((localFile != null) && (localFile.exists()))
        break label543;
      TapjoyLog.e("TapjoyVideo", "video file does not exist.");
      return false;
    }
    TapjoyLog.e("TapjoyVideo", "no video data and no video url - aborting playback...");
    return false;
    label543: Intent localIntent = new Intent(this.context, TapjoyVideoView.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("VIDEO_PATH", paramString1);
    this.context.startActivity(localIntent);
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyVideo
 * JD-Core Version:    0.6.2
 */