package com.tapjoy;

public abstract interface TapjoyVideoNotifier
{
  public abstract void videoComplete();

  public abstract void videoError(int paramInt);

  public abstract void videoReady();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyVideoNotifier
 * JD-Core Version:    0.6.2
 */