package com.tapjoy;

public class TapjoyVideoStatus
{
  public static final int STATUS_MEDIA_STORAGE_UNAVAILABLE = 1;
  public static final int STATUS_NETWORK_ERROR_ON_INIT_VIDEOS = 2;
  public static final int STATUS_UNABLE_TO_PLAY_VIDEO = 3;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     com.tapjoy.TapjoyVideoStatus
 * JD-Core Version:    0.6.2
 */