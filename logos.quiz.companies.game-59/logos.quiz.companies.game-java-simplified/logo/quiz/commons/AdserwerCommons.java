package logo.quiz.commons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AdserwerCommons
{
  private static final String AD_SYSTEM = "adSystem";
  public static final int FREE_HINTS_FOR_FACEBOOK_LIKE = 3;
  public static final int FREE_HINTS_FOR_INSTALL_APP = 5;
  public static final int FREE_HINTS_FOR_RATE_APP = 3;
  public static String adId;
  public static String adUrl;
  static final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);
  private static MyAdCommons[] myAds = arrayOfMyAdCommons;

  static
  {
    adUrl = "market://details?id=flag.quiz.world.national.names.learn";
    adId = "ad3";
    MyAdCommons[] arrayOfMyAdCommons = new MyAdCommons[8];
    arrayOfMyAdCommons[0] = new MyAdCommons(R.drawable.music_bands_logo_quiz_promo, "ad7", "market://details?id=logo.quiz.music.bands.game", "Music Bands Logo Quiz");
    arrayOfMyAdCommons[1] = new MyAdCommons(R.drawable.car_logo_promo, "ad1", "market://details?id=logo.quiz.car.game", "Car Logo Quiz");
    arrayOfMyAdCommons[2] = new MyAdCommons(R.drawable.logo_quiz_football_promo, "ad10", "market://details?id=logos.quiz.football.soccer.clubs", "Football Logo Quiz");
    arrayOfMyAdCommons[3] = new MyAdCommons(R.drawable.flag_quiz_promo, "ad2", "market://details?id=flag.quiz.world.national.names.learn", "Flag Quiz");
    arrayOfMyAdCommons[4] = new MyAdCommons(R.drawable.capitals_quiz_promo, "ad3", "market://details?id=capitals.quiz.world.national.names.learn", "Capitals Quiz");
    arrayOfMyAdCommons[5] = new MyAdCommons(R.drawable.scare_promo, "ad4", "market://details?id=scare.your.friends.prank.maze.halloween", "Scare your friends");
    arrayOfMyAdCommons[6] = new MyAdCommons(R.drawable.classic_logo_quiz_promo, "ad9", "market://details?id=logos.quiz.companies.game", "Classic Logo Quiz");
    arrayOfMyAdCommons[7] = new MyAdCommons(R.drawable.logo_quiz_by_category_promo, "ad8", "market://details?id=logo.quiz.game.category", "Ultimate Logo Quiz");
  }

  public static List<MyAdCommons> getActiveAds(Context paramContext, String paramString)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    boolean bool = localSharedPreferences.getBoolean("firstRun", true);
    if (localSharedPreferences.getBoolean("1hint", false))
    {
      localEditor.putBoolean("1hint", false);
      Toast.makeText(paramContext, "Nice! You get new hint!", 0).show();
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = getAllAds(paramContext, paramString).iterator();
    label198: label219: 
    while (true)
    {
      if (!localIterator.hasNext())
      {
        localEditor.commit();
        return localArrayList;
      }
      MyAdCommons localMyAdCommons = (MyAdCommons)localIterator.next();
      if ((DeviceUtilCommons.isPackageExists(localMyAdCommons.getAdUrl().split("=")[1], paramContext)) && (!localSharedPreferences.getBoolean(localMyAdCommons.getAdId(), false)))
      {
        localEditor.putBoolean(localMyAdCommons.getAdId(), true);
        if (!bool)
          break label198;
        localEditor.putBoolean("firstRun", false);
      }
      while (true)
      {
        if (localSharedPreferences.getBoolean(localMyAdCommons.getAdId(), false))
          break label219;
        localArrayList.add(localMyAdCommons);
        break;
        localEditor.putInt("allHints", 5 + localSharedPreferences.getInt("allHints", 0));
      }
    }
  }

  public static int getAdDelayMillis(Context paramContext)
  {
    return 10;
  }

  public static AdView getAdmob(Activity paramActivity, String paramString)
  {
    AdView localAdView = new AdView(paramActivity, AdSize.BANNER, paramString);
    localAdView.setAdListener((AdListener)paramActivity);
    localAdView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    localAdView.setGravity(1);
    localAdView.loadAd(new AdRequest());
    return localAdView;
  }

  public static List<MyAdCommons> getAllAds(Context paramContext, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    MyAdCommons[] arrayOfMyAdCommons = myAds;
    int i = arrayOfMyAdCommons.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return localArrayList;
      MyAdCommons localMyAdCommons = arrayOfMyAdCommons[j];
      if (!localMyAdCommons.getAdId().equals(paramString))
        localArrayList.add(localMyAdCommons);
    }
  }

  public static Intent getPromoIntent(Context paramContext)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse(adUrl));
    localIntent.addFlags(1073741824);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    localEditor.putBoolean(adId, true);
    localEditor.commit();
    return localIntent;
  }

  public static void getTapjoyAd(boolean paramBoolean, final RelativeLayout paramRelativeLayout, TapjoyKeys paramTapjoyKeys, Activity paramActivity, final String paramString)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramActivity.getApplicationContext());
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    if ((localSharedPreferences.getInt("adSystem", 0) == 0) && (paramString != null));
    for (int i = 1; (paramTapjoyKeys == null) || (i != 0); i = 0)
    {
      paramRelativeLayout.addView(getAdmob(paramActivity, paramString));
      localEditor.putInt("adSystem", 1);
      localEditor.commit();
      return;
    }
    Handler local1 = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        if (paramAnonymousMessage.obj == null)
          paramAnonymousMessage.obj = AdserwerCommons.getAdmob(AdserwerCommons.this, paramString);
        while (true)
        {
          paramRelativeLayout.removeAllViews();
          paramRelativeLayout.addView((View)paramAnonymousMessage.obj);
          return;
          AdserwerCommons.initFlipAnimator(AdserwerCommons.this);
          paramRelativeLayout.startAnimation(AdserwerCommons.flipAnimator);
        }
      }
    };
    TapjoyConnect.requestTapjoyConnect(paramActivity.getApplicationContext(), paramTapjoyKeys.getAppId(), paramTapjoyKeys.getSecret());
    TapjoyDisplayAdNotifier local2 = new TapjoyDisplayAdNotifier()
    {
      public void getDisplayAdResponse(View paramAnonymousView)
      {
        int i = paramAnonymousView.getLayoutParams().width;
        int j = paramAnonymousView.getLayoutParams().height;
        Log.i("EASY_APP", "adView dimensions: " + i + "x" + j);
        int k = paramRelativeLayout.getMeasuredWidth();
        if (k > i)
          k = i;
        paramAnonymousView.setLayoutParams(new ViewGroup.LayoutParams(k, k * j / i));
        Message localMessage = new Message();
        localMessage.obj = paramAnonymousView;
        AdserwerCommons.this.sendMessage(localMessage);
      }

      public void getDisplayAdResponseFailed(String paramAnonymousString)
      {
        Message localMessage = new Message();
        localMessage.obj = null;
        AdserwerCommons.this.sendMessage(localMessage);
      }
    };
    if (paramBoolean)
      TapjoyConnect.getTapjoyConnectInstance().getDisplayAdWithCurrencyID(paramTapjoyKeys.getCurrencyId(), local2);
    while (true)
    {
      localEditor.putInt("adSystem", 0);
      break;
      TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(local2);
    }
  }

  private static void initFlipAnimator(Activity paramActivity)
  {
    flipAnimator.setDuration(500L);
    flipAnimator.setFillAfter(true);
    flipAnimator.setInterpolator(new DecelerateInterpolator());
    flipAnimator.setmCenterX(DeviceUtilCommons.getDeviceSize(paramActivity.getApplicationContext()).x / 2);
  }

  public static void setAd(ImageView paramImageView, Context paramContext, String paramString)
  {
    DeviceUtilCommons.checkInfo(paramContext);
    if (paramImageView != null)
    {
      List localList = getActiveAds(paramContext, paramString);
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      localOptions.inScaled = false;
      try
      {
        if (localList.size() > 0)
        {
          MyAdCommons localMyAdCommons = (MyAdCommons)localList.get((int)(System.currentTimeMillis() % localList.size()));
          paramImageView.setImageBitmap(BitmapFactory.decodeResource(paramContext.getResources(), localMyAdCommons.getImageResource(), localOptions));
          adUrl = localMyAdCommons.getAdUrl();
          adId = localMyAdCommons.getAdId();
          return;
        }
        paramImageView.setImageBitmap(BitmapFactory.decodeResource(paramContext.getResources(), R.drawable.flag_quiz_promo, localOptions));
        adUrl = "market://details?id=flag.quiz.world.national.names.learn";
        adId = "ad1";
        return;
      }
      catch (Exception localException)
      {
        paramImageView.setImageBitmap(BitmapFactory.decodeResource(paramContext.getResources(), R.drawable.flag_quiz_promo, localOptions));
        adUrl = "market://details?id=flag.quiz.world.national.names.learn";
        adId = "ad1";
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.AdserwerCommons
 * JD-Core Version:    0.6.2
 */