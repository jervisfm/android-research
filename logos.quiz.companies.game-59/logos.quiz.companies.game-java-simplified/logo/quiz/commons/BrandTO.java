package logo.quiz.commons;

import java.io.Serializable;

public class BrandTO
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  public int brandPosition;
  public String category;
  public boolean complete = false;
  public int drawable;
  public String hint1;
  public String hint2;
  public String hint3;
  public int level = 1;
  public String[] names;
  public String wikipediaLink;

  public BrandTO(int paramInt1, int paramInt2, String[] paramArrayOfString, boolean paramBoolean, int paramInt3, String paramString1, String paramString2, String paramString3)
  {
    this.brandPosition = paramInt1;
    this.drawable = paramInt2;
    this.names = paramArrayOfString;
    this.complete = paramBoolean;
    this.level = paramInt3;
    this.category = paramString1;
    this.hint1 = paramString2;
    this.hint2 = paramString3;
    this.wikipediaLink = null;
  }

  public BrandTO(int paramInt1, int paramInt2, String[] paramArrayOfString, boolean paramBoolean, int paramInt3, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.brandPosition = paramInt1;
    this.drawable = paramInt2;
    this.names = paramArrayOfString;
    this.complete = paramBoolean;
    this.level = paramInt3;
    this.category = paramString1;
    this.hint1 = paramString2;
    this.hint2 = paramString3;
    this.wikipediaLink = paramString4;
  }

  public BrandTO(int paramInt1, int paramInt2, String[] paramArrayOfString, boolean paramBoolean, int paramInt3, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    this.brandPosition = paramInt1;
    this.drawable = paramInt2;
    this.names = paramArrayOfString;
    this.complete = paramBoolean;
    this.level = paramInt3;
    this.category = paramString1;
    this.hint1 = paramString2;
    this.hint2 = paramString3;
    this.hint3 = paramString4;
    this.wikipediaLink = paramString5;
  }

  public int getBrandPosition()
  {
    return this.brandPosition;
  }

  public String getCategory()
  {
    return this.category;
  }

  public int getDrawable()
  {
    return this.drawable;
  }

  public String getHint1()
  {
    return this.hint1;
  }

  public String getHint2()
  {
    return this.hint2;
  }

  public String getHint3()
  {
    return this.hint3;
  }

  public int getLevel()
  {
    return this.level;
  }

  public String[] getNames()
  {
    return this.names;
  }

  public String getWikipediaLink()
  {
    return this.wikipediaLink;
  }

  public String getbrandName()
  {
    String str = "";
    if (this.names.length > 0)
      str = this.names[0];
    return str;
  }

  public boolean hasWikipediaLink()
  {
    String str = this.wikipediaLink;
    boolean bool1 = false;
    if (str != null)
    {
      boolean bool2 = this.wikipediaLink.equals("");
      bool1 = false;
      if (!bool2)
        bool1 = true;
    }
    return bool1;
  }

  public boolean isComplete()
  {
    return this.complete;
  }

  public void setBrandPosition(int paramInt)
  {
    this.brandPosition = paramInt;
  }

  public void setCategory(String paramString)
  {
    this.category = paramString;
  }

  public void setComplete(boolean paramBoolean)
  {
    this.complete = paramBoolean;
  }

  public void setDrawable(int paramInt)
  {
    this.drawable = paramInt;
  }

  public void setHint1(String paramString)
  {
    this.hint1 = paramString;
  }

  public void setHint2(String paramString)
  {
    this.hint2 = paramString;
  }

  public void setHint3(String paramString)
  {
    this.hint3 = paramString;
  }

  public void setLevel(int paramInt)
  {
    this.level = paramInt;
  }

  public void setNames(String[] paramArrayOfString)
  {
    this.names = paramArrayOfString;
  }

  public void setWikipediaLink(String paramString)
  {
    this.wikipediaLink = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.BrandTO
 * JD-Core Version:    0.6.2
 */