package logo.quiz.commons;

public class ClassNotFoundRuntimeException extends RuntimeException
{
  private static final long serialVersionUID = 1L;

  public ClassNotFoundRuntimeException()
  {
  }

  public ClassNotFoundRuntimeException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.ClassNotFoundRuntimeException
 * JD-Core Version:    0.6.2
 */