package logo.quiz.commons;

import com.swarmconnect.SwarmLeaderboard;

public abstract interface ConstantsProvider
{
  public abstract String getAdmobPubId();

  public abstract String getAdmobRemoveAdId();

  public abstract String getChartboostAppId();

  public abstract String getChartboostAppSignature();

  public abstract String getFlurryKey();

  public abstract SwarmLeaderboard getLeaderboard();

  public abstract int getSwarmconnectAppId();

  public abstract String getSwarmconnectAppKey();

  public abstract int getSwarmconnectHighScoreId();

  public abstract String getTapJoyAppCurrencyId();

  public abstract String getTapJoyAppId();

  public abstract String getTapJoyAppSecret();

  public abstract TapjoyKeys getTapjoyKeys();

  public abstract void setLeaderboard(SwarmLeaderboard paramSwarmLeaderboard);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.ConstantsProvider
 * JD-Core Version:    0.6.2
 */