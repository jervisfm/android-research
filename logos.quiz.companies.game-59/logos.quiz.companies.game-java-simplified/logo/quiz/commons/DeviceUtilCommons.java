package logo.quiz.commons;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.PrintStream;

public class DeviceUtilCommons
{
  public static void checkInfo(Context paramContext)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str = localSharedPreferences.getString("info", null);
    if (str != null)
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putString("info", null);
      localEditor.commit();
      Toast.makeText(paramContext, str, 1).show();
    }
  }

  public static int dip(int paramInt, Context paramContext)
  {
    return (int)TypedValue.applyDimension(1, paramInt, paramContext.getResources().getDisplayMetrics());
  }

  public static Class getClassByName(Context paramContext, String paramString)
  {
    try
    {
      Class localClass = Class.forName(paramContext.getPackageName() + "." + paramString);
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
    }
    throw new ClassNotFoundRuntimeException("ActivityName not exist in LogoQuizCommons!");
  }

  public static Point getDeviceSize(Context paramContext)
  {
    Display localDisplay = getDisplayDevice(paramContext);
    return new Point(localDisplay.getWidth(), localDisplay.getHeight());
  }

  private static Display getDisplayDevice(Context paramContext)
  {
    return ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
  }

  public static Uri getUriFromResource(Context paramContext, int paramInt)
  {
    System.out.println("!!!!!!!" + paramContext.getPackageName());
    return Uri.parse("android.resource://" + paramContext.getPackageName() + "/" + paramInt);
  }

  public static boolean isOnline(Context paramContext)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    return (localConnectivityManager.getActiveNetworkInfo() != null) && (localConnectivityManager.getActiveNetworkInfo().isConnectedOrConnecting());
  }

  public static boolean isPackageExist(String paramString, Context paramContext)
  {
    try
    {
      boolean bool1 = isPackageExists(paramString, paramContext);
      boolean bool2 = false;
      if (bool1)
        bool2 = true;
      return bool2;
    }
    catch (Exception localException)
    {
    }
    return false;
  }

  public static boolean isPackageExists(String paramString, Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      localPackageManager.getPackageInfo(paramString, 128);
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return false;
  }

  public static void playSound(Context paramContext, int paramInt)
  {
    MediaPlayer localMediaPlayer = MediaPlayer.create(paramContext, paramInt);
    if (localMediaPlayer != null)
      localMediaPlayer.start();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.DeviceUtilCommons
 * JD-Core Version:    0.6.2
 */