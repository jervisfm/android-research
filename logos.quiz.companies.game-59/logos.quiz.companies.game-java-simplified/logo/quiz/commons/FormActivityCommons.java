package logo.quiz.commons;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;
import com.swarmconnect.SwarmActivity;
import com.tapjoy.TapjoyConnect;
import java.util.Iterator;
import java.util.List;

public abstract class FormActivityCommons extends SwarmActivity
  implements AdListener
{
  Handler adHandler;
  AdView adView;
  boolean complete = false;
  private Dialog dialog = null;
  final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);
  Activity myActivity;
  private Button tapjoyOfferButton = null;

  private int availibleHints(List<Hint> paramList)
  {
    int i = 0;
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      if (!((Hint)localIterator.next()).isUsed())
        i++;
    }
  }

  private void correctSound()
  {
    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("SOUND", true))
      DeviceUtilCommons.playSound(getApplicationContext(), R.raw.correct);
  }

  private void disableButton(Button paramButton)
  {
  }

  private void rotateView(View paramView)
  {
    paramView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotation));
  }

  private void shakeView(View paramView)
  {
    paramView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
  }

  private int usedHints(List<Hint> paramList)
  {
    int i = 0;
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return i;
      if (((Hint)localIterator.next()).isUsed())
        i++;
    }
  }

  private void vibrateNegative()
  {
    if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("VIBRATE", true))
    {
      Vibrator localVibrator = (Vibrator)getSystemService("vibrator");
      long[] arrayOfLong = new long[4];
      arrayOfLong[1] = 150L;
      arrayOfLong[2] = 100L;
      arrayOfLong[3] = 150L;
      localVibrator.vibrate(arrayOfLong, -1);
    }
  }

  private void vibratePositive()
  {
    if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("VIBRATE", true))
      ((Vibrator)getSystemService("vibrator")).vibrate(500L);
  }

  private void wrongSound()
  {
    if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("SOUND", true))
      DeviceUtilCommons.playSound(getApplicationContext(), R.raw.wrong);
  }

  public void back(View paramView)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("isCorrect", this.complete);
    Integer localInteger = Integer.valueOf(-1);
    if (this.complete)
      localInteger = (Integer)getIntent().getExtras().get("position");
    setResult(localInteger.intValue(), localIntent);
    finish();
  }

  protected abstract void checkLevelCompletedLogos(Activity paramActivity, int paramInt1, int paramInt2, int paramInt3);

  public void ckeck(View paramView)
  {
    String str1 = ((EditText)findViewById(R.id.editTextLogo)).getText().toString().toLowerCase().trim();
    String[] arrayOfString1 = (String[])getIntent().getExtras().get("brandNames");
    int i = arrayOfString1.length;
    int j = 0;
    int k = j;
    int m = 0;
    label62: SharedPreferences.Editor localEditor;
    Integer localInteger;
    int n;
    String[] arrayOfString2;
    StringBuilder localStringBuilder;
    int i5;
    if (k >= i)
    {
      SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
      localEditor = localSharedPreferences.edit();
      localInteger = (Integer)getIntent().getExtras().get("brandPosition");
      localEditor.putInt("guessTries", 1 + localSharedPreferences.getInt("guessTries", 0));
      n = localSharedPreferences.getInt("perfectGuessBrand" + localInteger, 0);
      if (m == 0)
        break label845;
      if (n == 0)
      {
        localEditor.putInt("perfectGuessBrand" + localInteger, 1);
        localEditor.putInt("perfectGuess", 1 + localSharedPreferences.getInt("perfectGuess", 0));
      }
      correctSound();
      vibratePositive();
      BrandTO localBrandTO = (BrandTO)getIntent().getSerializableExtra("brandTO");
      arrayOfString2 = localSharedPreferences.getString("COMPLETE_LOGOS", "0,0").split(",");
      arrayOfString2[localInteger.intValue()] = "1";
      localStringBuilder = new StringBuilder();
      int i4 = arrayOfString2.length;
      i5 = 0;
      label280: if (i5 < i4)
        break label776;
      String str3 = localStringBuilder.toString();
      localEditor.putString("COMPLETE_LOGOS", str3.substring(0, -1 + str3.length()));
      localEditor.putInt("complete_position", ((Integer)getIntent().getExtras().get("position")).intValue());
      localEditor.commit();
      this.complete = true;
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(((TextView)findViewById(R.id.editTextLogo)).getWindowToken(), 0);
      TextView localTextView = (TextView)findViewById(R.id.completeLogoName);
      ((LinearLayout)findViewById(R.id.completeBrandInfos)).setVisibility(0);
      localTextView.setText(localBrandTO.getbrandName());
      if (localBrandTO.hasWikipediaLink())
        ((ImageButton)findViewById(R.id.readMoreButton)).setVisibility(0);
      ((LinearLayout)findViewById(R.id.editLogo)).setVisibility(4);
      ((LinearLayout)findViewById(R.id.editWinLogo)).setVisibility(0);
      ((Button)findViewById(R.id.closeButtonId)).requestFocus();
      ((TextView)findViewById(R.id.scoreId)).setText("Points: +" + localBrandTO.getLevel());
      ((TextView)findViewById(R.id.hintsCountForm)).setText(String.valueOf(getAvailibleHintsCount(this)) + " hints");
      int i6 = localSharedPreferences.getInt("allHints", 0);
      boolean bool1 = isLevelUnlocked();
      int i7 = getCompletedLogosCount(this.myActivity);
      if (i7 == 1)
      {
        boolean bool2 = localSharedPreferences.getBoolean("afterResetApp", false);
        String str4 = getTapJoyPayPerActionCode();
        if ((str4 != null) && (!bool2))
          TapjoyConnect.getTapjoyConnectInstance().actionComplete(str4);
      }
      if (bool1)
        break label802;
      if (i7 % 4 == 0)
      {
        Toast.makeText(this, "You get new hint!", 1).show();
        localEditor.putInt("allHints", i6 + 1);
        localEditor.commit();
        ((TextView)findViewById(R.id.hintsCountForm)).setText(String.valueOf(getAvailibleHintsCount(this)) + " hints");
      }
    }
    while (true)
    {
      localEditor.commit();
      return;
      if (arrayOfString1[j].toLowerCase().replaceAll("[^a-z0-9]+", "").equals(str1.toLowerCase().replaceAll("[^a-z0-9]+", "")))
      {
        m = 1;
        break label62;
      }
      j++;
      break;
      label776: localStringBuilder.append(arrayOfString2[i5]);
      localStringBuilder.append(",");
      i5++;
      break label280;
      label802: ((TextView)findViewById(R.id.hintsCountForm)).setText(String.valueOf(getAvailibleHintsCount(this)) + " hints");
    }
    label845: if (n == 0)
      localEditor.putInt("perfectGuessBrand" + localInteger, -1);
    String str2 = arrayOfString1[0].toLowerCase();
    int i1 = 0;
    int i2 = 0;
    label891: if (i2 >= str2.length())
    {
      int i3 = str2.length() / 2;
      if (i1 <= i3)
        break label990;
      Toast.makeText(this, "Almost good. Try again!", 0).show();
    }
    while (true)
    {
      wrongSound();
      vibrateNegative();
      shakeView((ImageView)findViewById(R.id.imageBrand));
      break;
      if ((i2 < str1.length()) && (str2.charAt(i2) == str1.toLowerCase().charAt(i2)))
        i1++;
      i2++;
      break label891;
      label990: Toast.makeText(this, "Wrong answer", 0).show();
    }
  }

  public void close(View paramView)
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("isCorrect", true);
    setResult(((Integer)getIntent().getExtras().get("position")).intValue(), localIntent);
    finish();
  }

  protected abstract int getAvailibleHintsCount(Activity paramActivity);

  protected abstract int getCompletedLogosCount(Activity paramActivity);

  protected abstract ConstantsProvider getConstants();

  protected String getTapJoyPayPerActionCode()
  {
    return null;
  }

  public void hint(View paramView)
  {
    final SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    this.dialog = new Dialog(this);
    this.dialog.requestWindowFeature(1);
    this.dialog.setTitle("Watch hints");
    this.dialog.setContentView(R.layout.logos_hints);
    this.dialog.setCancelable(true);
    final BrandTO localBrandTO = (BrandTO)getIntent().getSerializableExtra("brandTO");
    int i = localSharedPreferences.getInt("allHints", 0);
    TextView localTextView1 = (TextView)this.dialog.findViewById(R.id.hintsCount);
    StringBuilder localStringBuilder = new StringBuilder("You have ").append(i).append(" hint");
    String str1;
    final LinearLayout localLinearLayout;
    int k;
    Iterator localIterator;
    if (i == 1)
    {
      str1 = "";
      localTextView1.setText(str1);
      List localList = HintsUtil.getAllHintsForBrand(localBrandTO, getApplicationContext(), isCategoryHint());
      int j = availibleHints(localList);
      Button localButton1 = (Button)this.dialog.findViewById(R.id.hintButton);
      String str2 = "";
      if (j > 0)
        str2 = 1 + (localList.size() - j);
      localButton1.setText("Show Hint " + str2);
      if ((j == 0) || (i == 0))
        disableButton(localButton1);
      localLinearLayout = (LinearLayout)this.dialog.findViewById(R.id.hintLayout);
      k = 0;
      localIterator = localList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
      {
        ((ImageButton)this.dialog.findViewById(R.id.backButton)).setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            FreeHintsActivityCommons.tapjoyPointToHints(FormActivityCommons.this.getApplicationContext(), FormActivityCommons.this.getConstants().getTapjoyKeys().getAppId(), FormActivityCommons.this.getConstants().getTapjoyKeys().getSecret());
            DeviceUtilCommons.checkInfo(FormActivityCommons.this.getApplicationContext());
            FormActivityCommons.this.dialog.dismiss();
          }
        });
        ((ImageButton)this.dialog.findViewById(R.id.askOnFbButton)).setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Intent localIntent = new Intent("android.intent.action.SEND");
            localIntent.setType("image/png");
            localIntent.addFlags(524288);
            localIntent.putExtra("android.intent.extra.STREAM", Uri.parse("android.resource://" + FormActivityCommons.this.getPackageName() + "/" + localBrandTO.getDrawable()));
            localIntent.putExtra("android.intent.extra.SUBJECT", "Need help in logo quiz game");
            localIntent.putExtra("android.intent.extra.TEXT", "Does anyone know this logo? Logo quiz game http://goo.gl/bekzQ");
            FormActivityCommons.this.startActivity(Intent.createChooser(localIntent, "Ask on:"));
          }
        });
        final Button localButton2 = (Button)this.dialog.findViewById(R.id.hintButton);
        localButton2.setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            int i = localSharedPreferences.getInt("allHints", 0);
            List localList = HintsUtil.getAllHintsForBrand(localBrandTO, FormActivityCommons.this.getApplicationContext(), FormActivityCommons.this.isCategoryHint());
            int j = FormActivityCommons.this.availibleHints(localList);
            if (j == 0)
            {
              Toast.makeText(FormActivityCommons.this.myActivity.getApplicationContext(), "All hints for this brand have been used", 1).show();
              FormActivityCommons.this.wrongSound();
              FormActivityCommons.this.vibrateNegative();
              FormActivityCommons.this.shakeView(localButton2);
            }
            Hint localHint;
            do
            {
              return;
              Iterator localIterator;
              while (!localIterator.hasNext())
              {
                while (i == 0)
                {
                  Toast.makeText(FormActivityCommons.this.myActivity.getApplicationContext(), "You have no hints.", 1).show();
                  if (FormActivityCommons.this.tapjoyOfferButton != null)
                  {
                    FormActivityCommons.this.shakeView(FormActivityCommons.this.tapjoyOfferButton);
                    return;
                  }
                }
                localIterator = localList.iterator();
              }
              localHint = (Hint)localIterator.next();
            }
            while (localHint.isUsed());
            FormActivityCommons.this.dialog.findViewById(R.id.hintHelp).setVisibility(8);
            TextView localTextView1 = new TextView(FormActivityCommons.this.getApplicationContext());
            localTextView1.setTextColor(Color.parseColor("#575757"));
            localTextView1.setTextSize(16.0F);
            localTextView1.setPadding(0, 0, 0, 10);
            localTextView1.setText(Html.fromHtml("<b>Hint " + (1 + FormActivityCommons.this.usedHints(localList)) + ".</b> " + localHint.getHint()));
            localLinearLayout.addView(localTextView1);
            SharedPreferences.Editor localEditor = localSharedPreferences.edit();
            localEditor.putInt("allHints", i - 1);
            localEditor.putBoolean("isUsedHint" + localHint.getId() + "Brand" + localBrandTO.getBrandPosition(), true);
            localEditor.commit();
            ((TextView)FormActivityCommons.this.findViewById(R.id.hintsCountForm)).setText(String.valueOf(FormActivityCommons.this.getAvailibleHintsCount(FormActivityCommons.this.myActivity)) + " hints");
            TextView localTextView2 = (TextView)FormActivityCommons.this.dialog.findViewById(R.id.hintsCount);
            StringBuilder localStringBuilder = new StringBuilder("You have ").append(i - 1).append(" hint");
            if (i - 1 == 1);
            for (String str1 = ""; ; str1 = "s")
            {
              localTextView2.setText(str1);
              Button localButton = (Button)FormActivityCommons.this.dialog.findViewById(R.id.hintButton);
              String str2 = "";
              if (j > 1)
                str2 = 2 + (localList.size() - j);
              localButton.setText("Show Hint " + str2);
              if ((j - 1 == 0) || (i - 1 == 0))
                FormActivityCommons.this.disableButton(localButton);
              if (j - 1 != 0)
                break;
              localButton.setText("All Hints Used");
              return;
            }
          }
        });
        RelativeLayout localRelativeLayout = (RelativeLayout)this.dialog.findViewById(R.id.tapjoyOffers);
        this.tapjoyOfferButton = FreeHintsActivityCommons.getTapjoyOfferButton(this, getConstants().getTapjoyKeys());
        localRelativeLayout.addView(this.tapjoyOfferButton);
        this.dialog.show();
        return;
        str1 = "s";
        break;
      }
      Hint localHint = (Hint)localIterator.next();
      if (localHint.isUsed())
      {
        k++;
        this.dialog.findViewById(R.id.hintHelp).setVisibility(8);
        TextView localTextView2 = new TextView(getApplicationContext());
        localTextView2.setTextSize(16.0F);
        localTextView2.setPadding(0, 0, 0, 10);
        localTextView2.setText(Html.fromHtml("<b>Hint " + k + ".</b> " + localHint.getHint()));
        localTextView2.setTextColor(Color.parseColor("#575757"));
        localLinearLayout.addView(localTextView2);
      }
    }
  }

  protected boolean isCategoryHint()
  {
    return true;
  }

  protected abstract boolean isLevelUnlocked();

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(R.layout.logos_form);
    this.myActivity = this;
    this.flipAnimator.setDuration(500L);
    this.flipAnimator.setFillAfter(true);
    this.flipAnimator.setInterpolator(new DecelerateInterpolator());
    AdserwerCommons.setAd((ImageView)findViewById(R.id.myAd2), getApplicationContext(), getConstants().getAdmobRemoveAdId());
    BrandTO localBrandTO = (BrandTO)getIntent().getSerializableExtra("brandTO");
    TextView localTextView1;
    StringBuilder localStringBuilder;
    if (localBrandTO.isComplete())
    {
      TextView localTextView2 = (TextView)findViewById(R.id.completeLogoName);
      ((LinearLayout)findViewById(R.id.completeBrandInfos)).setVisibility(0);
      localTextView2.setText(localBrandTO.getbrandName());
      if (localBrandTO.hasWikipediaLink())
      {
        ImageButton localImageButton = (ImageButton)findViewById(R.id.readMoreButton);
        localImageButton.setVisibility(0);
        rotateView(localImageButton);
      }
      ((LinearLayout)findViewById(R.id.editLogo)).setVisibility(4);
      ((LinearLayout)findViewById(R.id.editWinLogo)).setVisibility(0);
      ((Button)findViewById(R.id.closeButtonId)).requestFocus();
      ((TextView)findViewById(R.id.scoreId)).setText("Points: " + localBrandTO.getLevel());
      int i = getAvailibleHintsCount(this);
      localTextView1 = (TextView)findViewById(R.id.hintsCountForm);
      localStringBuilder = new StringBuilder(String.valueOf(String.valueOf(i))).append(" hint");
      if (i != 1)
        break label442;
    }
    label442: for (String str = ""; ; str = "s")
    {
      localTextView1.setText(str);
      ((ImageView)findViewById(R.id.imageBrand)).setImageResource(getIntent().getExtras().getInt("brandDrawable"));
      ((RelativeLayout)this.myActivity.findViewById(R.id.logosListAd2)).addView(AdserwerCommons.getAdmob(this, getConstants().getAdmobPubId()));
      TapjoyConnect.requestTapjoyConnect(getApplicationContext(), getConstants().getTapJoyAppId(), getConstants().getTapJoyAppSecret());
      return;
      ((LinearLayout)findViewById(R.id.editLogo)).setVisibility(0);
      ((LinearLayout)findViewById(R.id.editWinLogo)).setVisibility(4);
      break;
    }
  }

  protected void onDestroy()
  {
    TapjoyConnect.getTapjoyConnectInstance().sendShutDownEvent();
    super.onDestroy();
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public void onReceiveAd(Ad paramAd)
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(R.id.logosListAd2);
    this.flipAnimator.setmCenterX(DeviceUtilCommons.getDeviceSize(getApplicationContext()).x / 2);
    localRelativeLayout.startAnimation(this.flipAnimator);
  }

  protected void onResume()
  {
    super.onResume();
    FreeHintsActivityCommons.tapjoyPointToHints(getApplicationContext(), getConstants().getTapjoyKeys().getAppId(), getConstants().getTapjoyKeys().getSecret(), new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        DeviceUtilCommons.checkInfo(FormActivityCommons.this.getApplicationContext());
        ((TextView)FormActivityCommons.this.findViewById(R.id.hintsCountForm)).setText(String.valueOf(FormActivityCommons.this.getAvailibleHintsCount(jdField_this)) + " hints");
        TextView localTextView;
        StringBuilder localStringBuilder;
        if (FormActivityCommons.this.dialog != null)
        {
          int i = PreferenceManager.getDefaultSharedPreferences(FormActivityCommons.this.getApplicationContext()).getInt("allHints", 0);
          localTextView = (TextView)FormActivityCommons.this.dialog.findViewById(R.id.hintsCount);
          localStringBuilder = new StringBuilder("You have ").append(i).append(" hint");
          if (i != 1)
            break label148;
        }
        label148: for (String str = ""; ; str = "s")
        {
          localTextView.setText(str);
          return;
        }
      }
    });
  }

  public void onStop()
  {
    super.onStop();
  }

  public void promo(View paramView)
  {
    startActivity(AdserwerCommons.getPromoIntent(getApplicationContext()));
  }

  public void readMore(View paramView)
  {
    if (DeviceUtilCommons.isOnline(getApplicationContext()))
    {
      final Dialog localDialog = new Dialog(this, R.style.Dialog_Fullscreen);
      localDialog.requestWindowFeature(1);
      localDialog.setContentView(R.layout.read_more);
      localDialog.setCancelable(true);
      localDialog.show();
      BrandTO localBrandTO = (BrandTO)getIntent().getSerializableExtra("brandTO");
      this.flipAnimator.setDuration(500L);
      this.flipAnimator.setFillAfter(true);
      this.flipAnimator.setInterpolator(new DecelerateInterpolator());
      AdserwerCommons.setAd((ImageView)localDialog.findViewById(R.id.myAdReadMore), getApplicationContext(), getConstants().getAdmobRemoveAdId());
      ((RelativeLayout)localDialog.findViewById(R.id.logosReadMoreAd)).addView(AdserwerCommons.getAdmob(this.myActivity, getConstants().getAdmobPubId()));
      ((ImageView)localDialog.findViewById(R.id.brandImageReadMore)).setImageResource(localBrandTO.getDrawable());
      ((TextView)localDialog.findViewById(R.id.brandNameReadMore)).setText(localBrandTO.getbrandName());
      WebView localWebView = (WebView)localDialog.findViewById(R.id.webBrandInfoReadMore);
      localWebView.getSettings().setJavaScriptEnabled(true);
      localWebView.loadUrl(localBrandTO.getWikipediaLink());
      localWebView.setWebViewClient(new MyWebViewClient(null));
      ((ImageView)localDialog.findViewById(R.id.myAdReadMore)).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          FormActivityCommons.this.promo(null);
        }
      });
      ((ImageButton)localDialog.findViewById(R.id.backButton)).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          localDialog.dismiss();
        }
      });
      return;
    }
    Toast.makeText(getApplicationContext(), "You have to be online!", 1).show();
  }

  private class MyWebViewClient extends WebViewClient
  {
    private MyWebViewClient()
    {
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      paramWebView.loadUrl(paramString);
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.FormActivityCommons
 * JD-Core Version:    0.6.2
 */