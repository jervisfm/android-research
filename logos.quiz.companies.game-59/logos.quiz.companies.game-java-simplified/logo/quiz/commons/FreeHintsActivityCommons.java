package logo.quiz.commons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.chartboost.sdk.ChartBoost;
import com.chartboost.sdk.ChartBoostDelegate;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.swarmconnect.SwarmActivity;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyNotifier;
import java.util.Iterator;
import java.util.List;

public abstract class FreeHintsActivityCommons extends SwarmActivity
  implements AdListener
{
  private static boolean isTapjoyEnable = true;

  private void freeHintsClick(MyAdCommons paramMyAdCommons, int paramInt, boolean paramBoolean)
  {
    if (DeviceUtilCommons.isOnline(getApplicationContext()))
    {
      Intent localIntent = new Intent("android.intent.action.VIEW");
      localIntent.setData(Uri.parse(paramMyAdCommons.getAdUrl()));
      localIntent.addFlags(1073741824);
      if (!paramBoolean)
      {
        SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        localEditor.putBoolean(paramMyAdCommons.getAdId(), true);
        localEditor.putInt("allHints", paramInt + localSharedPreferences.getInt("allHints", 0));
        localEditor.commit();
      }
      startActivity(localIntent);
      return;
    }
    Toast.makeText(getApplicationContext(), "You have to be online", 1).show();
  }

  private LinearLayout getHintText(String paramString1, String paramString2)
  {
    LinearLayout localLinearLayout = new LinearLayout(getApplicationContext());
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2, 1.0F);
    localLayoutParams.setMargins(DeviceUtilCommons.dip(5, getApplicationContext()), DeviceUtilCommons.dip(10, getApplicationContext()), DeviceUtilCommons.dip(5, getApplicationContext()), DeviceUtilCommons.dip(15, getApplicationContext()));
    localLinearLayout.setLayoutParams(localLayoutParams);
    TextView localTextView1 = new TextView(this);
    localTextView1.setText("Get ");
    localTextView1.setTextAppearance(getApplicationContext(), R.style.hintsTextStyleCommons);
    localLinearLayout.addView(localTextView1);
    Button localButton = (Button)getLayoutInflater().inflate(R.layout.hints_button, null);
    localButton.setText(paramString1 + " hints");
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
      }
    });
    localLinearLayout.addView(localButton);
    TextView localTextView2 = new TextView(this);
    localTextView2.setText(paramString2);
    localTextView2.setTextAppearance(getApplicationContext(), R.style.hintsTextStyleCommons);
    localLinearLayout.addView(localTextView2);
    return localLinearLayout;
  }

  public static Button getTapjoyOfferButton(Activity paramActivity, final TapjoyKeys paramTapjoyKeys)
  {
    Context localContext = paramActivity.getApplicationContext();
    Button localButton = (Button)paramActivity.getLayoutInflater().inflate(R.layout.tapjoy_offers_button, null);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2, 1.0F);
    localLayoutParams.setMargins(DeviceUtilCommons.dip(20, localContext), DeviceUtilCommons.dip(0, localContext), DeviceUtilCommons.dip(20, localContext), DeviceUtilCommons.dip(20, localContext));
    localButton.setLayoutParams(localLayoutParams);
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (DeviceUtilCommons.isOnline(FreeHintsActivityCommons.this))
        {
          TapjoyConnect.requestTapjoyConnect(FreeHintsActivityCommons.this, paramTapjoyKeys.getAppId(), paramTapjoyKeys.getSecret());
          TapjoyConnect.getTapjoyConnectInstance().showOffers();
          return;
        }
        Toast.makeText(FreeHintsActivityCommons.this, "You have to be online", 1).show();
      }
    });
    return localButton;
  }

  private void promoAd(LinearLayout paramLinearLayout, final MyAdCommons paramMyAdCommons, final int paramInt, final boolean paramBoolean)
  {
    ImageView localImageView = new ImageView(this);
    localImageView.setLayoutParams(new FrameLayout.LayoutParams(DeviceUtilCommons.dip(320, this), DeviceUtilCommons.dip(50, this), 1));
    localImageView.setPadding(0, 0, 0, 0);
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inScaled = false;
    localImageView.setImageBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), paramMyAdCommons.getImageResource(), localOptions));
    localImageView.setClickable(true);
    localImageView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        FreeHintsActivityCommons.this.freeHintsClick(paramMyAdCommons, paramInt, paramBoolean);
      }
    });
    paramLinearLayout.addView(localImageView);
    LinearLayout localLinearLayout = new LinearLayout(this);
    localLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, DeviceUtilCommons.dip(15, this), 1.0F));
    paramLinearLayout.addView(localLinearLayout);
  }

  private void refreshHints()
  {
    int i = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("allHints", 0);
    TextView localTextView = (TextView)findViewById(R.id.yourHintsInfo);
    StringBuilder localStringBuilder = new StringBuilder("You have ").append(i).append(" hint");
    if (i == 1);
    for (String str = ""; ; str = "s")
    {
      localTextView.setText(str);
      return;
    }
  }

  public static void tapjoyPointToHints(Context paramContext, String paramString1, String paramString2)
  {
    tapjoyPointToHints(paramContext, paramString1, paramString2, null);
  }

  public static void tapjoyPointToHints(Context paramContext, String paramString1, String paramString2, Handler paramHandler)
  {
    if (paramHandler == null)
      paramHandler = new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          DeviceUtilCommons.checkInfo(FreeHintsActivityCommons.this);
        }
      };
    final Handler localHandler = paramHandler;
    try
    {
      TapjoyConnect.requestTapjoyConnect(paramContext, paramString1, paramString2);
      TapjoyConnect.getTapjoyConnectInstance().getTapPoints(new TapjoyNotifier()
      {
        public void getUpdatePoints(String paramAnonymousString, int paramAnonymousInt)
        {
          SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(FreeHintsActivityCommons.this);
          SharedPreferences.Editor localEditor = localSharedPreferences.edit();
          int i = localSharedPreferences.getInt("tapjoyHints", -1);
          if (i == -1)
          {
            localEditor.putInt("tapjoyHints", paramAnonymousInt);
            localEditor.commit();
          }
          int j;
          do
          {
            return;
            j = paramAnonymousInt - i;
          }
          while (j <= 0);
          localEditor.putInt("allHints", j + localSharedPreferences.getInt("allHints", 0));
          localEditor.putInt("tapjoyHints", paramAnonymousInt);
          localEditor.putString("info", "You get " + j + " new hints for app install!");
          localEditor.commit();
          localHandler.sendMessage(new Message());
        }

        public void getUpdatePointsFailed(String paramAnonymousString)
        {
        }
      });
      return;
    }
    catch (Exception localException)
    {
      isTapjoyEnable = false;
    }
  }

  public abstract void back(View paramView);

  protected abstract String getAdmobPubId();

  protected abstract String getAdmobRemoveAdId();

  protected abstract String getChartBoostAppId();

  protected abstract String getChartBoostSignature();

  protected abstract String getFacebookProfileId();

  protected abstract String getGooglePlayUrl();

  public abstract String getRemoveAdId();

  protected TapjoyKeys getTapjoyKeys()
  {
    return null;
  }

  public void like(View paramView)
  {
    try
    {
      getApplicationContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
      localIntent = new Intent("android.intent.action.VIEW", Uri.parse("fb://profile/" + getFacebookProfileId()));
      localIntent.addFlags(1073741824);
      localIntent.addFlags(268435456);
      startActivity(localIntent);
      return;
    }
    catch (Exception localException)
    {
      while (true)
        Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/pages/profile/" + getFacebookProfileId()));
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(R.layout.free_hints);
    final SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    ChartBoost localChartBoost = ChartBoost.getSharedChartBoost(this);
    localChartBoost.setAppId(getChartBoostAppId());
    localChartBoost.setAppSignature(getChartBoostSignature());
    localChartBoost.setDelegate(new ChartBoostDelegate()
    {
      public void didClickInterstitial(View paramAnonymousView)
      {
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        int i = localSharedPreferences.getInt("allHints", 0);
        localEditor.putBoolean("1hint", true);
        localEditor.putInt("allHints", i + 1);
        localEditor.commit();
      }
    });
    localChartBoost.install();
    localChartBoost.showInterstitial();
  }

  protected void onDestroy()
  {
    try
    {
      TapjoyConnect.getTapjoyConnectInstance().sendShutDownEvent();
      label6: super.onDestroy();
      return;
    }
    catch (Exception localException)
    {
      break label6;
    }
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public abstract void onReceiveAd(Ad paramAd);

  protected void onResume()
  {
    super.onResume();
    final Context localContext = getApplicationContext();
    if (getTapjoyKeys() != null)
      tapjoyPointToHints(getApplicationContext(), getTapjoyKeys().getAppId(), getTapjoyKeys().getSecret(), new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          DeviceUtilCommons.checkInfo(FreeHintsActivityCommons.this.getApplicationContext());
          FreeHintsActivityCommons.this.refreshHints();
        }
      });
    final SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    ChartBoost.getSharedChartBoost(this);
    LinearLayout localLinearLayout1 = (LinearLayout)findViewById(R.id.freeHintsContainer);
    localLinearLayout1.removeAllViews();
    int i = 0;
    if ((isTapjoyEnable) && (getTapjoyKeys() != null))
    {
      localLinearLayout1.addView(getHintText("free", " by install and run other apps"));
      localLinearLayout1.addView(getTapjoyOfferButton(this, getTapjoyKeys()));
    }
    List localList = AdserwerCommons.getActiveAds(getApplicationContext(), getRemoveAdId());
    if (localList.size() > 0)
      localLinearLayout1.addView(getHintText("5", " by installing other logo quiz"));
    Iterator localIterator = localList.iterator();
    ImageView localImageView;
    BitmapFactory.Options localOptions;
    label379: int j;
    SharedPreferences.Editor localEditor;
    if (!localIterator.hasNext())
    {
      refreshHints();
      LinearLayout localLinearLayout2 = new LinearLayout(this);
      LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, DeviceUtilCommons.dip(10, this), 1.0F);
      localLinearLayout2.setLayoutParams(localLayoutParams);
      localLinearLayout1.addView(localLinearLayout2);
      MyAdCommons localMyAdCommons2 = new MyAdCommons(R.drawable.rate_logo_quiz, "ad999", getGooglePlayUrl());
      if (!localSharedPreferences.getBoolean(localMyAdCommons2.getAdId(), false))
      {
        localLinearLayout1.addView(getHintText("3", " by rate Logo Quiz"));
        promoAd(localLinearLayout1, localMyAdCommons2, 3, false);
        i++;
      }
      localImageView = new ImageView(this);
      localImageView.setLayoutParams(new FrameLayout.LayoutParams(DeviceUtilCommons.dip(164, this), DeviceUtilCommons.dip(62, this), 1));
      localImageView.setPadding(0, 0, 0, 0);
      localOptions = new BitmapFactory.Options();
      localOptions.inScaled = false;
      if (localSharedPreferences.getBoolean("like_on_fb", false))
        break label535;
      localImageView.setImageBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.like_us_on_fb, localOptions));
      i++;
      localImageView.setClickable(true);
      View.OnClickListener local5 = new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (DeviceUtilCommons.isOnline(localContext))
          {
            if (!localSharedPreferences.getBoolean("like_on_fb", false))
            {
              SharedPreferences.Editor localEditor = localSharedPreferences.edit();
              localEditor.putBoolean("like_on_fb", true);
              localEditor.putInt("allHints", 3 + localSharedPreferences.getInt("allHints", 0));
              localEditor.commit();
            }
            FreeHintsActivityCommons.this.like(null);
            return;
          }
          Toast.makeText(FreeHintsActivityCommons.this.getApplicationContext(), "You have to be online", 1).show();
        }
      };
      localImageView.setOnClickListener(local5);
      localLinearLayout1.addView(localImageView);
      if (localLinearLayout1.getChildCount() == 0)
      {
        TextView localTextView = new TextView(this);
        localTextView.setText("At this moment there are no free hints.");
        localLinearLayout1.addView(localTextView);
      }
      j = localSharedPreferences.getInt("lastActiveAdsCount", -1);
      localEditor = localSharedPreferences.edit();
      if (j != -1)
        break label558;
      localEditor.putInt("lastActiveAdsCount", i);
    }
    while (true)
    {
      localEditor.commit();
      return;
      MyAdCommons localMyAdCommons1 = (MyAdCommons)localIterator.next();
      if (localSharedPreferences.getBoolean(localMyAdCommons1.getAdId(), false))
        break;
      promoAd(localLinearLayout1, localMyAdCommons1, 5, true);
      i++;
      break;
      label535: localImageView.setImageBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.like_us_used, localOptions));
      break label379;
      label558: if (j != i)
      {
        Toast.makeText(this, "Congratulations! You get new hints.", 1).show();
        localEditor.putInt("lastActiveAdsCount", i);
        refreshHints();
      }
    }
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }

  public void promo(View paramView)
  {
    startActivity(AdserwerCommons.getPromoIntent(getApplicationContext()));
  }

  public void rate(View paramView)
  {
    FlurryAgent.onEvent("Rate app");
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
    localIntent.addFlags(1073741824);
    startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.FreeHintsActivityCommons
 * JD-Core Version:    0.6.2
 */