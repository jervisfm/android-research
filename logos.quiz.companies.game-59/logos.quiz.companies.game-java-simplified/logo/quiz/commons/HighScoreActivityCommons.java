package logo.quiz.commons;

import android.os.Bundle;
import android.view.Window;
import com.flurry.android.FlurryAgent;
import com.swarmconnect.NotificationLeaderboard;
import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmActiveUser;
import com.swarmconnect.SwarmActivity;
import com.swarmconnect.SwarmLeaderboard;
import com.swarmconnect.SwarmLeaderboard.GotLeaderboardCB;
import com.swarmconnect.SwarmNotification;
import com.swarmconnect.delegates.SwarmLoginListener;
import com.swarmconnect.delegates.SwarmNotificationDelegate;

public abstract class HighScoreActivityCommons extends SwarmActivity
{
  private SwarmLoginListener mySwarmLoginListener = new SwarmLoginListener()
  {
    SwarmLeaderboard.GotLeaderboardCB callback = new SwarmLeaderboard.GotLeaderboardCB()
    {
      public void gotLeaderboard(SwarmLeaderboard paramAnonymous2SwarmLeaderboard)
      {
        if (paramAnonymous2SwarmLeaderboard != null)
        {
          HighScoreActivityCommons.this.getConstants().setLeaderboard(paramAnonymous2SwarmLeaderboard);
          if (HighScoreActivityCommons.this.getConstants().getLeaderboard() != null)
            HighScoreActivityCommons.this.getConstants().getLeaderboard().showLeaderboard();
        }
      }
    };

    public void loginCanceled()
    {
    }

    public void loginStarted()
    {
    }

    public void userLoggedIn(SwarmActiveUser paramAnonymousSwarmActiveUser)
    {
      SwarmLeaderboard.getLeaderboardById(HighScoreActivityCommons.this.getConstants().getSwarmconnectAppId(), this.callback);
    }

    public void userLoggedOut()
    {
    }
  };

  protected abstract ConstantsProvider getConstants();

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, getConstants().getFlurryKey());
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(R.layout.high_score);
    Swarm.addNotificationDelegate(new SwarmNotificationDelegate()
    {
      public boolean gotNotification(SwarmNotification paramAnonymousSwarmNotification)
      {
        boolean bool1 = false;
        if (paramAnonymousSwarmNotification != null)
        {
          boolean bool2 = paramAnonymousSwarmNotification instanceof NotificationLeaderboard;
          bool1 = false;
          if (bool2)
            bool1 = true;
        }
        return bool1;
      }
    });
    Swarm.init(this, getConstants().getSwarmconnectAppId(), getConstants().getSwarmconnectAppKey(), this.mySwarmLoginListener);
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.HighScoreActivityCommons
 * JD-Core Version:    0.6.2
 */