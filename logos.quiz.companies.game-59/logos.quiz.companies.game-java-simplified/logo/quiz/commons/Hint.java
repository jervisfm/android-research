package logo.quiz.commons;

public class Hint
{
  String hint;
  int id;
  boolean isUsed;

  public Hint(int paramInt, String paramString, boolean paramBoolean)
  {
    this.id = paramInt;
    this.hint = paramString;
    this.isUsed = paramBoolean;
  }

  public String getHint()
  {
    return this.hint;
  }

  public int getId()
  {
    return this.id;
  }

  public boolean isUsed()
  {
    return this.isUsed;
  }

  public void setHint(String paramString)
  {
    this.hint = paramString;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setUsed(boolean paramBoolean)
  {
    this.isUsed = paramBoolean;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.Hint
 * JD-Core Version:    0.6.2
 */