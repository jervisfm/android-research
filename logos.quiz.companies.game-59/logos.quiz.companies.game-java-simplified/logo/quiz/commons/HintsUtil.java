package logo.quiz.commons;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.util.ArrayList;
import java.util.List;

public class HintsUtil
{
  public static List<Hint> getAllHintsForBrand(BrandTO paramBrandTO, Context paramContext, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    if (!paramBrandTO.getHint1().equals(""))
    {
      boolean bool5 = localSharedPreferences.getBoolean("isUsedHint1Brand" + paramBrandTO.getBrandPosition(), false);
      localArrayList.add(new Hint(1, paramBrandTO.getHint1(), bool5));
    }
    if (!paramBrandTO.getHint2().equals(""))
    {
      boolean bool4 = localSharedPreferences.getBoolean("isUsedHint2Brand" + paramBrandTO.getBrandPosition(), false);
      localArrayList.add(new Hint(2, paramBrandTO.getHint2(), bool4));
    }
    if (paramBrandTO.getbrandName().length() > 2)
    {
      boolean bool3 = localSharedPreferences.getBoolean("isUsedHint3Brand" + paramBrandTO.getBrandPosition(), false);
      localArrayList.add(new Hint(3, "Half solution: " + hideCharsInBrandName(paramBrandTO.getbrandName()), bool3));
    }
    if (paramBoolean)
    {
      boolean bool2 = localSharedPreferences.getBoolean("isUsedHint4Brand" + paramBrandTO.getBrandPosition(), false);
      localArrayList.add(new Hint(4, "Brand is from category '" + paramBrandTO.getCategory() + "'.", bool2));
    }
    boolean bool1 = localSharedPreferences.getBoolean("isUsedHint5Brand" + paramBrandTO.getBrandPosition(), false);
    localArrayList.add(new Hint(5, "Full solution: " + paramBrandTO.getbrandName(), bool1));
    return localArrayList;
  }

  private static String hideCharsInBrandName(String paramString)
  {
    String str = "";
    int i = 0;
    char[] arrayOfChar = paramString.toCharArray();
    int j = arrayOfChar.length;
    int k = 0;
    if (k >= j)
      return str;
    char c = arrayOfChar[k];
    if ((i % 2 == 0) || (i == -1 + paramString.length()))
      str = str + c;
    while (true)
    {
      i++;
      k++;
      break;
      if (c == ' ')
        str = str + "  ";
      else
        str = str + "_";
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.HintsUtil
 * JD-Core Version:    0.6.2
 */