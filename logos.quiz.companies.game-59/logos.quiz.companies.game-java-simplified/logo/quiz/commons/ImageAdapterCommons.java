package logo.quiz.commons;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public abstract class ImageAdapterCommons extends BaseAdapter
{
  private BitmapFactory.Options bmpFactory;
  protected Context mContext;
  private LayoutInflater mInflater;

  public ImageAdapterCommons(Context paramContext)
  {
    this.mContext = paramContext;
    this.mInflater = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.bmpFactory = new BitmapFactory.Options();
    this.bmpFactory.inSampleSize = 2;
  }

  protected abstract BrandTO[] getActiveLevelBrands();

  public int getCount()
  {
    return getActiveLevelBrands().length;
  }

  public BrandTO getItem(int paramInt)
  {
    BrandTO[] arrayOfBrandTO = getActiveLevelBrands();
    try
    {
      if (arrayOfBrandTO.length == paramInt)
        return arrayOfBrandTO[(paramInt - 1)];
      BrandTO localBrandTO = arrayOfBrandTO[paramInt];
      return localBrandTO;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    ViewHolder localViewHolder;
    BrandTO localBrandTO;
    if (paramView == null)
    {
      paramView = this.mInflater.inflate(R.layout.logos_list_item, paramViewGroup, false);
      localViewHolder = new ViewHolder();
      localViewHolder.imageView = ((ImageView)paramView.findViewById(R.id.list_item));
      paramView.setTag(localViewHolder);
      localBrandTO = getActiveLevelBrands()[paramInt];
      if (!localBrandTO.isComplete())
        break label193;
      if (localBrandTO.getLevel() != 1)
        break label124;
      localViewHolder.imageView.setBackgroundResource(R.drawable.bg_complete_1);
    }
    label193: 
    while (true)
    {
      localViewHolder.imageView.setImageBitmap(BitmapFactory.decodeResource(this.mContext.getResources(), localBrandTO.getDrawable(), this.bmpFactory));
      return paramView;
      localViewHolder = (ViewHolder)paramView.getTag();
      break;
      label124: if (localBrandTO.getLevel() == 2)
      {
        localViewHolder.imageView.setBackgroundResource(R.drawable.bg_complete_2);
      }
      else if (localBrandTO.getLevel() == 3)
      {
        localViewHolder.imageView.setBackgroundResource(R.drawable.bg_complete_3);
      }
      else if (localBrandTO.getLevel() == 4)
      {
        localViewHolder.imageView.setBackgroundResource(R.drawable.bg_complete_4);
        continue;
        if (localBrandTO.getLevel() == 1)
          localViewHolder.imageView.setBackgroundResource(R.drawable.bg_1);
        else if (localBrandTO.getLevel() == 2)
          localViewHolder.imageView.setBackgroundResource(R.drawable.bg_2);
        else if (localBrandTO.getLevel() == 3)
          localViewHolder.imageView.setBackgroundResource(R.drawable.bg_3);
        else if (localBrandTO.getLevel() == 4)
          localViewHolder.imageView.setBackgroundResource(R.drawable.bg_4);
      }
    }
  }

  static class ViewHolder
  {
    ImageView imageView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.ImageAdapterCommons
 * JD-Core Version:    0.6.2
 */