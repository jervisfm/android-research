package logo.quiz.commons;

import android.content.Context;

public class LevelUtil
{
  private static int activeLevel = 1;

  public static int getActiveLevel()
  {
    return activeLevel;
  }

  public static void setActiveLevel(Context paramContext, int paramInt)
  {
    activeLevel = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.LevelUtil
 * JD-Core Version:    0.6.2
 */