package logo.quiz.commons;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;
import com.flurry.android.FlurryAgent;
import com.swarmconnect.NotificationLeaderboard;
import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmActiveUser;
import com.swarmconnect.SwarmActivity;
import com.swarmconnect.SwarmLeaderboard;
import com.swarmconnect.SwarmLeaderboard.GotLeaderboardCB;
import com.swarmconnect.SwarmNotification;
import com.swarmconnect.delegates.SwarmLoginListener;
import com.swarmconnect.delegates.SwarmNotificationDelegate;
import com.tapjoy.TapjoyConnect;

public abstract class LogoQuizActivityCommons extends SwarmActivity
{
  SwarmLeaderboard.GotLeaderboardCB cb = new SwarmLeaderboard.GotLeaderboardCB()
  {
    public void gotLeaderboard(SwarmLeaderboard paramAnonymousSwarmLeaderboard)
    {
      LogoQuizActivityCommons.this.getConstants().setLeaderboard(paramAnonymousSwarmLeaderboard);
      if (LogoQuizActivityCommons.this.getConstants().getLeaderboard() != null)
      {
        LogoQuizActivityCommons.this.getConstants().getLeaderboard().submitScore(LogoQuizActivityCommons.this.getCompletedPoints());
        LogoQuizActivityCommons.this.getConstants().getLeaderboard().showLeaderboard();
      }
    }
  };
  Activity myActivity;
  private final SwarmLoginListener mySwarmLoginListener = new SwarmLoginListener()
  {
    public void loginCanceled()
    {
    }

    public void loginStarted()
    {
    }

    public void userLoggedIn(SwarmActiveUser paramAnonymousSwarmActiveUser)
    {
      SwarmLeaderboard.getLeaderboardById(LogoQuizActivityCommons.this.getConstants().getSwarmconnectHighScoreId(), LogoQuizActivityCommons.this.cb);
    }

    public void userLoggedOut()
    {
    }
  };

  protected abstract int getCompletedPoints();

  protected abstract ConstantsProvider getConstants();

  public void getFreeHints(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), DeviceUtilCommons.getClassByName(getApplicationContext(), "FreeHintsActivity"));
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  protected abstract ScoreUtilProvider getScoreUtilProvider();

  public void info(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), DeviceUtilCommons.getClassByName(getApplicationContext(), "OptionsActivity"));
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, getConstants().getFlurryKey());
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(R.layout.main);
    this.myActivity = this;
    Swarm.addNotificationDelegate(new SwarmNotificationDelegate()
    {
      public boolean gotNotification(SwarmNotification paramAnonymousSwarmNotification)
      {
        boolean bool1 = false;
        if (paramAnonymousSwarmNotification != null)
        {
          boolean bool2 = paramAnonymousSwarmNotification instanceof NotificationLeaderboard;
          bool1 = false;
          if (bool2)
            bool1 = true;
        }
        return bool1;
      }
    });
    getScoreUtilProvider().initLogos(this);
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    if (localSharedPreferences.getInt("allHints", -1) == -1)
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putInt("allHints", 3);
      localEditor.commit();
    }
    if (getScoreUtilProvider().getNewLogosCount() > 0)
    {
      Toast.makeText(this, getScoreUtilProvider().getNewLogosCount() + " new logos to guess!", 1).show();
      getScoreUtilProvider().setNewLogosCount(0);
    }
    ((Button)findViewById(R.id.my_swarm_button)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (Swarm.isOnline())
        {
          if (!Swarm.isInitialized())
          {
            Swarm.init(LogoQuizActivityCommons.this.myActivity, LogoQuizActivityCommons.this.getConstants().getSwarmconnectAppId(), LogoQuizActivityCommons.this.getConstants().getSwarmconnectAppKey(), LogoQuizActivityCommons.this.mySwarmLoginListener);
            return;
          }
          SwarmLeaderboard.getLeaderboardById(LogoQuizActivityCommons.this.getConstants().getSwarmconnectHighScoreId(), LogoQuizActivityCommons.this.cb);
          return;
        }
        Toast.makeText(LogoQuizActivityCommons.this.myActivity, "You have to be online to see high scores", 1).show();
      }
    });
    try
    {
      TapjoyConnect.requestTapjoyConnect(getApplicationContext(), getConstants().getTapjoyKeys().getAppId(), getConstants().getTapjoyKeys().getSecret());
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      new AlertDialog.Builder(this).setIcon(17301543).setTitle("Exit").setMessage("Are you sure you want to leave?").setNegativeButton(17039360, null).setPositiveButton(17039370, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          LogoQuizActivityCommons.this.moveTaskToBack(true);
        }
      }).show();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    if (getConstants().getTapjoyKeys() != null)
      FreeHintsActivityCommons.tapjoyPointToHints(getApplicationContext(), getConstants().getTapjoyKeys().getAppId(), getConstants().getTapjoyKeys().getSecret(), new Handler()
      {
        public void handleMessage(Message paramAnonymousMessage)
        {
          DeviceUtilCommons.checkInfo(LogoQuizActivityCommons.this.getApplicationContext());
        }
      });
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }

  public void options(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), DeviceUtilCommons.getClassByName(getApplicationContext(), "OptionsActivity"));
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  public void play(View paramView)
  {
    LevelUtil.setActiveLevel(getApplicationContext(), 1);
    Intent localIntent = new Intent(getApplicationContext(), DeviceUtilCommons.getClassByName(getApplicationContext(), "LevelsActivity"));
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  public void stats(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), DeviceUtilCommons.getClassByName(getApplicationContext(), "StatisticsActivity"));
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.LogoQuizActivityCommons
 * JD-Core Version:    0.6.2
 */