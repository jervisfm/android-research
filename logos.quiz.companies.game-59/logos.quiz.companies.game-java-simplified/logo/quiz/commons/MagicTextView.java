package logo.quiz.commons;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.WeakHashMap;

public class MagicTextView extends TextView
{
  private WeakHashMap<String, Pair<Canvas, Bitmap>> canvasStore;
  private Drawable foregroundDrawable;
  private boolean frozen = false;
  private ArrayList<Shadow> innerShadows;
  private int[] lockedCompoundPadding;
  private ArrayList<Shadow> outerShadows;
  private Integer strokeColor;
  private Paint.Join strokeJoin;
  private float strokeMiter;
  private float strokeWidth;
  private Bitmap tempBitmap;
  private Canvas tempCanvas;

  public MagicTextView(Context paramContext)
  {
    super(paramContext);
    init(null);
  }

  public MagicTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramAttributeSet);
  }

  public MagicTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramAttributeSet);
  }

  private void generateTempCanvas()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(getWidth());
    arrayOfObject[1] = Integer.valueOf(getHeight());
    String str = String.format("%dx%d", arrayOfObject);
    Pair localPair = (Pair)this.canvasStore.get(str);
    if (localPair != null)
    {
      this.tempCanvas = ((Canvas)localPair.first);
      this.tempBitmap = ((Bitmap)localPair.second);
      return;
    }
    this.tempCanvas = new Canvas();
    this.tempBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
    this.tempCanvas.setBitmap(this.tempBitmap);
    this.canvasStore.put(str, new Pair(this.tempCanvas, this.tempBitmap));
  }

  public void addInnerShadow(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
  {
    if (paramFloat1 == 0.0F)
      paramFloat1 = 1.0E-004F;
    this.innerShadows.add(new Shadow(paramFloat1, paramFloat2, paramFloat3, paramInt));
  }

  public void addOuterShadow(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
  {
    if (paramFloat1 == 0.0F)
      paramFloat1 = 1.0E-004F;
    this.outerShadows.add(new Shadow(paramFloat1, paramFloat2, paramFloat3, paramInt));
  }

  public void clearInnerShadows()
  {
    this.innerShadows.clear();
  }

  public void clearOuterShadows()
  {
    this.outerShadows.clear();
  }

  public void freeze()
  {
    int[] arrayOfInt = new int[4];
    arrayOfInt[0] = getCompoundPaddingLeft();
    arrayOfInt[1] = getCompoundPaddingRight();
    arrayOfInt[2] = getCompoundPaddingTop();
    arrayOfInt[3] = getCompoundPaddingBottom();
    this.lockedCompoundPadding = arrayOfInt;
    this.frozen = true;
  }

  public int getCompoundPaddingBottom()
  {
    if (!this.frozen)
      return super.getCompoundPaddingBottom();
    return this.lockedCompoundPadding[3];
  }

  public int getCompoundPaddingLeft()
  {
    if (!this.frozen)
      return super.getCompoundPaddingLeft();
    return this.lockedCompoundPadding[0];
  }

  public int getCompoundPaddingRight()
  {
    if (!this.frozen)
      return super.getCompoundPaddingRight();
    return this.lockedCompoundPadding[1];
  }

  public int getCompoundPaddingTop()
  {
    if (!this.frozen)
      return super.getCompoundPaddingTop();
    return this.lockedCompoundPadding[2];
  }

  public Drawable getForeground()
  {
    if (this.foregroundDrawable == null)
      return this.foregroundDrawable;
    return new ColorDrawable(getCurrentTextColor());
  }

  public void init(AttributeSet paramAttributeSet)
  {
    this.outerShadows = new ArrayList();
    this.innerShadows = new ArrayList();
    if (this.canvasStore == null)
      this.canvasStore = new WeakHashMap();
    TypedArray localTypedArray;
    label150: float f1;
    int i;
    float f2;
    Paint.Join localJoin;
    if (paramAttributeSet != null)
    {
      localTypedArray = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.MagicTextView);
      String str = localTypedArray.getString(8);
      if (str != null)
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), String.format("fonts/%s.ttf", new Object[] { str })));
      if (localTypedArray.hasValue(9))
      {
        Drawable localDrawable2 = localTypedArray.getDrawable(9);
        if (localDrawable2 == null)
          break label317;
        setForegroundDrawable(localDrawable2);
      }
      if (localTypedArray.hasValue(10))
      {
        Drawable localDrawable1 = localTypedArray.getDrawable(10);
        if (localDrawable1 == null)
          break label332;
        setBackgroundDrawable(localDrawable1);
      }
      if (localTypedArray.hasValue(0))
        addInnerShadow(localTypedArray.getFloat(1, 0.0F), localTypedArray.getFloat(2, 0.0F), localTypedArray.getFloat(3, 0.0F), localTypedArray.getColor(0, -16777216));
      if (localTypedArray.hasValue(4))
        addOuterShadow(localTypedArray.getFloat(5, 0.0F), localTypedArray.getFloat(6, 0.0F), localTypedArray.getFloat(7, 0.0F), localTypedArray.getColor(4, -16777216));
      if (localTypedArray.hasValue(13))
      {
        f1 = localTypedArray.getFloat(11, 1.0F);
        i = localTypedArray.getColor(13, -16777216);
        f2 = localTypedArray.getFloat(12, 10.0F);
        int j = localTypedArray.getInt(14, 0);
        localJoin = null;
        switch (j)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
      }
    }
    while (true)
    {
      setStroke(f1, i, localJoin, f2);
      return;
      label317: setTextColor(localTypedArray.getColor(9, -16777216));
      break;
      label332: setBackgroundColor(localTypedArray.getColor(10, -16777216));
      break label150;
      localJoin = Paint.Join.MITER;
      continue;
      localJoin = Paint.Join.BEVEL;
      continue;
      localJoin = Paint.Join.ROUND;
    }
  }

  public void invalidate()
  {
    if (!this.frozen)
      super.invalidate();
  }

  public void invalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.frozen)
      super.invalidate(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void invalidate(Rect paramRect)
  {
    if (!this.frozen)
      super.invalidate(paramRect);
  }

  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    freeze();
    Drawable localDrawable = getBackground();
    Drawable[] arrayOfDrawable = getCompoundDrawables();
    int i = getCurrentTextColor();
    setCompoundDrawables(null, null, null, null);
    Iterator localIterator1 = this.outerShadows.iterator();
    TextPaint localTextPaint1;
    Iterator localIterator2;
    if (!localIterator1.hasNext())
    {
      setShadowLayer(0.0F, 0.0F, 0.0F, 0);
      setTextColor(i);
      if ((this.foregroundDrawable != null) && ((this.foregroundDrawable instanceof BitmapDrawable)))
      {
        generateTempCanvas();
        super.onDraw(this.tempCanvas);
        ((BitmapDrawable)this.foregroundDrawable).getPaint().setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        this.foregroundDrawable.setBounds(paramCanvas.getClipBounds());
        this.foregroundDrawable.draw(this.tempCanvas);
        paramCanvas.drawBitmap(this.tempBitmap, 0.0F, 0.0F, null);
        this.tempCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      }
      if (this.strokeColor != null)
      {
        TextPaint localTextPaint2 = getPaint();
        localTextPaint2.setStyle(Paint.Style.STROKE);
        localTextPaint2.setStrokeJoin(this.strokeJoin);
        localTextPaint2.setStrokeMiter(this.strokeMiter);
        setTextColor(this.strokeColor.intValue());
        localTextPaint2.setStrokeWidth(this.strokeWidth);
        super.onDraw(paramCanvas);
        localTextPaint2.setStyle(Paint.Style.FILL);
        setTextColor(i);
      }
      if (this.innerShadows.size() > 0)
      {
        generateTempCanvas();
        localTextPaint1 = getPaint();
        localIterator2 = this.innerShadows.iterator();
      }
    }
    while (true)
    {
      if (!localIterator2.hasNext())
      {
        if (arrayOfDrawable != null)
          setCompoundDrawablesWithIntrinsicBounds(arrayOfDrawable[0], arrayOfDrawable[1], arrayOfDrawable[2], arrayOfDrawable[3]);
        setBackgroundDrawable(localDrawable);
        setTextColor(i);
        unfreeze();
        return;
        Shadow localShadow1 = (Shadow)localIterator1.next();
        setShadowLayer(localShadow1.r, localShadow1.dx, localShadow1.dy, localShadow1.color);
        super.onDraw(paramCanvas);
        break;
      }
      Shadow localShadow2 = (Shadow)localIterator2.next();
      setTextColor(localShadow2.color);
      super.onDraw(this.tempCanvas);
      setTextColor(-16777216);
      localTextPaint1.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
      localTextPaint1.setMaskFilter(new BlurMaskFilter(localShadow2.r, BlurMaskFilter.Blur.NORMAL));
      this.tempCanvas.save();
      this.tempCanvas.translate(localShadow2.dx, localShadow2.dy);
      super.onDraw(this.tempCanvas);
      this.tempCanvas.restore();
      paramCanvas.drawBitmap(this.tempBitmap, 0.0F, 0.0F, null);
      this.tempCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      localTextPaint1.setXfermode(null);
      localTextPaint1.setMaskFilter(null);
      setTextColor(i);
      setShadowLayer(0.0F, 0.0F, 0.0F, 0);
    }
  }

  public void postInvalidate()
  {
    if (!this.frozen)
      super.postInvalidate();
  }

  public void postInvalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.frozen)
      super.postInvalidate(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void requestLayout()
  {
    if (!this.frozen)
      super.requestLayout();
  }

  public void setForegroundDrawable(Drawable paramDrawable)
  {
    this.foregroundDrawable = paramDrawable;
  }

  public void setStroke(float paramFloat, int paramInt)
  {
    setStroke(paramFloat, paramInt, Paint.Join.MITER, 10.0F);
  }

  public void setStroke(float paramFloat1, int paramInt, Paint.Join paramJoin, float paramFloat2)
  {
    this.strokeWidth = paramFloat1;
    this.strokeColor = Integer.valueOf(paramInt);
    this.strokeJoin = paramJoin;
    this.strokeMiter = paramFloat2;
  }

  public void unfreeze()
  {
    this.frozen = false;
  }

  public static class Shadow
  {
    int color;
    float dx;
    float dy;
    float r;

    public Shadow(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
    {
      this.r = paramFloat1;
      this.dx = paramFloat2;
      this.dy = paramFloat3;
      this.color = paramInt;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.MagicTextView
 * JD-Core Version:    0.6.2
 */