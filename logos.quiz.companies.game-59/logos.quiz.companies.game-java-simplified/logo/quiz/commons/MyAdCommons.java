package logo.quiz.commons;

public class MyAdCommons
{
  private String adId;
  private String adName;
  private String adUrl;
  private int imageResource;

  public MyAdCommons(int paramInt, String paramString1, String paramString2)
  {
    this.imageResource = paramInt;
    this.adId = paramString1;
    this.adUrl = paramString2;
  }

  public MyAdCommons(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    this.imageResource = paramInt;
    this.adId = paramString1;
    this.adUrl = paramString2;
    this.adName = paramString3;
  }

  public String getAdId()
  {
    return this.adId;
  }

  public String getAdName()
  {
    return this.adName;
  }

  public String getAdUrl()
  {
    return this.adUrl;
  }

  public int getImageResource()
  {
    return this.imageResource;
  }

  public void setAdId(String paramString)
  {
    this.adId = paramString;
  }

  public void setAdName(String paramString)
  {
    this.adName = paramString;
  }

  public void setAdUrl(String paramString)
  {
    this.adUrl = paramString;
  }

  public void setImageResource(int paramInt)
  {
    this.imageResource = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.MyAdCommons
 * JD-Core Version:    0.6.2
 */