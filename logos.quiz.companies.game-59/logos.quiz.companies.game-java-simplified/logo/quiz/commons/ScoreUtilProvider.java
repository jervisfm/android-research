package logo.quiz.commons;

import android.app.Activity;

public abstract interface ScoreUtilProvider
{
  public abstract int getNewLogosCount();

  public abstract void initLogos(Activity paramActivity);

  public abstract void setNewLogosCount(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.ScoreUtilProvider
 * JD-Core Version:    0.6.2
 */