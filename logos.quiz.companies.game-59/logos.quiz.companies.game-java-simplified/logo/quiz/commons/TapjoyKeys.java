package logo.quiz.commons;

public class TapjoyKeys
{
  String appId;
  String currencyId;
  String secret;

  public TapjoyKeys(String paramString1, String paramString2, String paramString3)
  {
    this.appId = paramString1;
    this.secret = paramString2;
    this.currencyId = paramString3;
  }

  public String getAppId()
  {
    return this.appId;
  }

  public String getCurrencyId()
  {
    return this.currencyId;
  }

  public String getSecret()
  {
    return this.secret;
  }

  public void setAppId(String paramString)
  {
    this.appId = paramString;
  }

  public void setCurrencyId(String paramString)
  {
    this.currencyId = paramString;
  }

  public void setSecret(String paramString)
  {
    this.secret = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logo.quiz.commons.TapjoyKeys
 * JD-Core Version:    0.6.2
 */