package logos.quiz.companies.game;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;
import com.google.ads.AdView;
import java.util.List;
import logo.quiz.commons.AdserwerCommons;
import logo.quiz.commons.MyAdCommons;

public class Adserwer extends AdserwerCommons
{
  public static final String REMOVE_AD_ID = "ad9";

  public static List<MyAdCommons> getActiveAds(Context paramContext)
  {
    return getActiveAds(paramContext, "ad9");
  }

  public static AdView getAdmob(Activity paramActivity)
  {
    return getAdmob(paramActivity, "a14fc12954d4f22");
  }

  public static List<MyAdCommons> getAllAds(Context paramContext)
  {
    return getAllAds(paramContext, "ad9");
  }

  public static void setAd(ImageView paramImageView, Context paramContext)
  {
    setAd(paramImageView, paramContext, "ad9");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.Adserwer
 * JD-Core Version:    0.6.2
 */