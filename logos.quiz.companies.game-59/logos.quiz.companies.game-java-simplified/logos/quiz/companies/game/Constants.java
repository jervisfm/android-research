package logos.quiz.companies.game;

import com.swarmconnect.SwarmLeaderboard;
import logo.quiz.commons.ConstantsProvider;
import logo.quiz.commons.TapjoyKeys;

public class Constants
  implements ConstantsProvider
{
  public static final String ADMOB_PUB_ID = "a14fc12954d4f22";
  public static final String CHARTBOOST_APP_ID = "5074bf9016ba47531e000008";
  public static final String CHARTBOOST_APP_SIGNATURE = "4031b2b8c86f878685ec205855e53c61c60accfd";
  public static final String FLURRY_KEY = "GHNSFPPH779R3ZQTGTFJ";
  public static final int SWARMCONNECT_APP_ID = 791;
  public static final String SWARMCONNECT_APP_KEY = "c4828a20bf2176747643856f966ab231";
  public static final int SWARMCONNECT_HIGH_SCORE_ID = 1121;
  public static final String TAP_JOY_APP_CURRENCY_ID = "";
  public static final String TAP_JOY_APP_ID = "8928eece-ecf9-4708-8be3-22f3c3c61e43";
  public static final String TAP_JOY_APP_SECRET = "KsC3RCwzFcQw6YRS0i5L";
  public static SwarmLeaderboard leaderboard;

  public String getAdmobPubId()
  {
    return "a14fc12954d4f22";
  }

  public String getAdmobRemoveAdId()
  {
    return "ad9";
  }

  public String getChartboostAppId()
  {
    return "5074bf9016ba47531e000008";
  }

  public String getChartboostAppSignature()
  {
    return "4031b2b8c86f878685ec205855e53c61c60accfd";
  }

  public String getFlurryKey()
  {
    return "GHNSFPPH779R3ZQTGTFJ";
  }

  public SwarmLeaderboard getLeaderboard()
  {
    return leaderboard;
  }

  public int getSwarmconnectAppId()
  {
    return 791;
  }

  public String getSwarmconnectAppKey()
  {
    return "c4828a20bf2176747643856f966ab231";
  }

  public int getSwarmconnectHighScoreId()
  {
    return 1121;
  }

  public String getTapJoyAppCurrencyId()
  {
    return "";
  }

  public String getTapJoyAppId()
  {
    return "8928eece-ecf9-4708-8be3-22f3c3c61e43";
  }

  public String getTapJoyAppSecret()
  {
    return "KsC3RCwzFcQw6YRS0i5L";
  }

  public TapjoyKeys getTapjoyKeys()
  {
    return new TapjoyKeys("8928eece-ecf9-4708-8be3-22f3c3c61e43", "KsC3RCwzFcQw6YRS0i5L", "");
  }

  public void setLeaderboard(SwarmLeaderboard paramSwarmLeaderboard)
  {
    leaderboard = paramSwarmLeaderboard;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.Constants
 * JD-Core Version:    0.6.2
 */