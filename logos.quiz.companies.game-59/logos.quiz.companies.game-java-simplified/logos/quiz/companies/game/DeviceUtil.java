package logos.quiz.companies.game;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

public class DeviceUtil
{
  private static final int IMAGE_MAX_SIZE = 70;
  private static final String TAG = "DeviceUtil";

  public static int dip(int paramInt, Context paramContext)
  {
    return (int)TypedValue.applyDimension(1, paramInt, paramContext.getResources().getDisplayMetrics());
  }

  public static Point getDeviceSize(Context paramContext)
  {
    Display localDisplay = getDisplayDevice(paramContext);
    return new Point(localDisplay.getWidth(), localDisplay.getHeight());
  }

  private static Display getDisplayDevice(Context paramContext)
  {
    return ((WindowManager)paramContext.getSystemService("window")).getDefaultDisplay();
  }

  public static boolean isOnline(Context paramContext)
  {
    ConnectivityManager localConnectivityManager = (ConnectivityManager)paramContext.getSystemService("connectivity");
    return (localConnectivityManager.getActiveNetworkInfo() != null) && (localConnectivityManager.getActiveNetworkInfo().isConnectedOrConnecting());
  }

  public static boolean isPackageExists(String paramString, Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      localPackageManager.getPackageInfo(paramString, 128);
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return false;
  }

  public static void playSound(Context paramContext, int paramInt)
  {
    MediaPlayer localMediaPlayer = MediaPlayer.create(paramContext, paramInt);
    if (localMediaPlayer != null)
      localMediaPlayer.start();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.DeviceUtil
 * JD-Core Version:    0.6.2
 */