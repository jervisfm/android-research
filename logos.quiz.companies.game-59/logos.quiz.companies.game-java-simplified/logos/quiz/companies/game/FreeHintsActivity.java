package logos.quiz.companies.game;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.ads.Ad;
import logo.quiz.commons.FlipAnimator;
import logo.quiz.commons.FreeHintsActivityCommons;
import logo.quiz.commons.TapjoyKeys;

public class FreeHintsActivity extends FreeHintsActivityCommons
{
  final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);

  public void back(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), LogosQuizActivity.class);
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  protected String getAdmobPubId()
  {
    return "a14fc12954d4f22";
  }

  protected String getAdmobRemoveAdId()
  {
    return "ad9";
  }

  protected String getChartBoostAppId()
  {
    return "5074bf9016ba47531e000008";
  }

  protected String getChartBoostSignature()
  {
    return "4031b2b8c86f878685ec205855e53c61c60accfd";
  }

  protected String getFacebookProfileId()
  {
    return "519844888042075";
  }

  protected String getGooglePlayUrl()
  {
    return "market://details?id=logos.quiz.companies.game";
  }

  public String getRemoveAdId()
  {
    return "ad9";
  }

  protected TapjoyKeys getTapjoyKeys()
  {
    return new TapjoyKeys("8928eece-ecf9-4708-8be3-22f3c3c61e43", "KsC3RCwzFcQw6YRS0i5L", "");
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }

  public void onReceiveAd(Ad paramAd)
  {
  }

  protected void onResume()
  {
    super.onResume();
  }

  public void onStop()
  {
    super.onStop();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.FreeHintsActivity
 * JD-Core Version:    0.6.2
 */