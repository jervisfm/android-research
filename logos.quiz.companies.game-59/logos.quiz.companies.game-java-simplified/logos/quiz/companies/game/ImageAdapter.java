package logos.quiz.companies.game;

import android.content.Context;
import logo.quiz.commons.BrandTO;
import logo.quiz.commons.ImageAdapterCommons;
import logo.quiz.commons.LevelUtil;

public class ImageAdapter extends ImageAdapterCommons
{
  public ImageAdapter(Context paramContext)
  {
    super(paramContext);
  }

  protected BrandTO[] getActiveLevelBrands()
  {
    int i = LevelUtil.getActiveLevel();
    return ScoreUtil.getBrands(this.mContext, i);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.ImageAdapter
 * JD-Core Version:    0.6.2
 */