package logos.quiz.companies.game;

public class Level
{
  private int from;
  private int id;
  private int levelBackgroundResource;
  private int to;
  private int unlockLimit;

  public Level(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    this.id = paramInt1;
    this.from = paramInt2;
    this.to = paramInt3;
    this.unlockLimit = paramInt4;
    this.levelBackgroundResource = paramInt5;
  }

  public int getFrom()
  {
    return this.from;
  }

  public int getId()
  {
    return this.id;
  }

  public int getLevelBackgroundResource()
  {
    return this.levelBackgroundResource;
  }

  public int getLogosCount()
  {
    return this.to - this.from;
  }

  public int getTo()
  {
    return this.to;
  }

  public int getUnlockLimit()
  {
    return this.unlockLimit;
  }

  public void setFrom(int paramInt)
  {
    this.from = paramInt;
  }

  public void setId(int paramInt)
  {
    this.id = paramInt;
  }

  public void setLevelBackgroundResource(int paramInt)
  {
    this.levelBackgroundResource = paramInt;
  }

  public void setTo(int paramInt)
  {
    this.to = paramInt;
  }

  public void setUnlockLimit(int paramInt)
  {
    this.unlockLimit = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.Level
 * JD-Core Version:    0.6.2
 */