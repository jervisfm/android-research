package logos.quiz.companies.game;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import logo.quiz.commons.LevelUtil;

public class LevelListAdapter extends BaseAdapter
{
  private Context context;
  private Level[] levels;
  private LayoutInflater mInflater;

  public LevelListAdapter(Context paramContext, Level[] paramArrayOfLevel)
  {
    this.mInflater = LayoutInflater.from(paramContext);
    this.context = paramContext;
    this.levels = paramArrayOfLevel;
  }

  private void playLevel(int paramInt, LinearLayout paramLinearLayout)
  {
    if (paramInt == 1)
      startLevel(paramInt);
    do
    {
      return;
      int i = ScoreUtil.getCompletedLogosCount(this.context, paramInt - 1);
      int j = ScoreUtil.getLevelsInfo()[(paramInt - 1)].getUnlockLimit();
      if (i >= j)
      {
        startLevel(paramInt);
        return;
      }
      paramLinearLayout.startAnimation(AnimationUtils.loadAnimation(this.context, 2130968579));
      Toast.makeText(this.context, "Need " + j + " correct answers from level " + (paramInt - 1), 0).show();
    }
    while (!PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean("SOUND", true));
    DeviceUtil.playSound(this.context, 2131034114);
  }

  private void startLevel(int paramInt)
  {
    LevelUtil.setActiveLevel(this.context, paramInt);
    Intent localIntent = new Intent(this.context, LogosListActivity.class);
    localIntent.setFlags(335544320);
    this.context.startActivity(localIntent);
  }

  public int getCount()
  {
    return this.levels.length;
  }

  public Object getItem(int paramInt)
  {
    return Integer.valueOf(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, final View paramView, ViewGroup paramViewGroup)
  {
    ViewHolder localViewHolder;
    final Level localLevel;
    if (paramView == null)
    {
      paramView = this.mInflater.inflate(2130903043, null);
      localViewHolder = new ViewHolder();
      localViewHolder.setLevelLayout(paramView.findViewById(2131099655));
      localViewHolder.setLevelScoreTextView((TextView)paramView.findViewById(2131099658));
      localViewHolder.setLevelLogosCountTextView((TextView)paramView.findViewById(2131099659));
      localViewHolder.setLevelProgressBarlevel((ProgressBar)paramView.findViewById(2131099660));
      localViewHolder.setProgressBarPercentlevel((TextView)paramView.findViewById(2131099661));
      localViewHolder.setButtonLevel((Button)paramView.findViewById(2131099662));
      localViewHolder.setLevel((TextView)paramView.findViewById(2131099657));
      localViewHolder.setLocklevel((ImageView)paramView.findViewById(2131099656));
      paramView.setTag(localViewHolder);
      localLevel = this.levels[paramInt];
      if (paramInt + 1 == this.levels.length)
        paramView.setPadding(10, 10, 10, 10);
      localViewHolder.getLevelLayout().setBackgroundResource(ScoreUtil.getLevelsInfo()[(-1 + localLevel.getId())].getLevelBackgroundResource());
      int i = ScoreUtil.getCompletedLogosCount(this.context, localLevel.getId());
      int j = ScoreUtil.getLevelsInfo()[(-1 + localLevel.getId())].getLogosCount();
      int k = ScoreUtil.getCompletedPoints(this.context, localLevel.getId());
      localViewHolder.getLevel().setText("Level " + localLevel.getId());
      localViewHolder.getLevelScoreTextView().setText(k);
      localViewHolder.getLevelLogosCountTextView().setText(i + "/" + j);
      localViewHolder.getLevelProgressBarlevel().setMax(j);
      localViewHolder.getLevelProgressBarlevel().setProgress(i);
      int m = Math.round(100.0F * (Integer.valueOf(i).floatValue() / Integer.valueOf(j).floatValue()));
      localViewHolder.getProgressBarPercentlevel().setText(m + "%");
      localViewHolder.getLocklevel().setVisibility(0);
      if (localLevel.getId() <= 1)
        break label542;
      if (ScoreUtil.getCompletedLogosCount(this.context, -1 + localLevel.getId()) >= ScoreUtil.getLevelsInfo()[(-1 + localLevel.getId())].getUnlockLimit())
        localViewHolder.getLocklevel().setVisibility(8);
    }
    while (true)
    {
      View.OnClickListener local1 = new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          LevelListAdapter.this.playLevel(localLevel.getId(), (LinearLayout)paramView);
        }
      };
      localViewHolder.getButtonLevel().setOnClickListener(local1);
      localViewHolder.getLevelLayout().setOnClickListener(local1);
      localViewHolder.getLevel().setOnClickListener(local1);
      localViewHolder.getLevelScoreTextView().setOnClickListener(local1);
      localViewHolder.getLevelLogosCountTextView().setOnClickListener(local1);
      localViewHolder.getLevelProgressBarlevel().setOnClickListener(local1);
      return paramView;
      localViewHolder = (ViewHolder)paramView.getTag();
      break;
      label542: localViewHolder.getLocklevel().setVisibility(8);
    }
  }

  static class ViewHolder
  {
    private Button buttonLevel;
    private TextView level;
    private View levelLayout;
    private TextView levelLogosCountTextView;
    private ProgressBar levelProgressBarlevel;
    private TextView levelScoreTextView;
    private ImageView locklevel;
    private TextView progressBarPercentlevel;

    public Button getButtonLevel()
    {
      return this.buttonLevel;
    }

    public TextView getLevel()
    {
      return this.level;
    }

    public View getLevelLayout()
    {
      return this.levelLayout;
    }

    public TextView getLevelLogosCountTextView()
    {
      return this.levelLogosCountTextView;
    }

    public ProgressBar getLevelProgressBarlevel()
    {
      return this.levelProgressBarlevel;
    }

    public TextView getLevelScoreTextView()
    {
      return this.levelScoreTextView;
    }

    public ImageView getLocklevel()
    {
      return this.locklevel;
    }

    public TextView getProgressBarPercentlevel()
    {
      return this.progressBarPercentlevel;
    }

    public void setButtonLevel(Button paramButton)
    {
      this.buttonLevel = paramButton;
    }

    public void setLevel(TextView paramTextView)
    {
      this.level = paramTextView;
    }

    public void setLevelLayout(View paramView)
    {
      this.levelLayout = paramView;
    }

    public void setLevelLogosCountTextView(TextView paramTextView)
    {
      this.levelLogosCountTextView = paramTextView;
    }

    public void setLevelProgressBarlevel(ProgressBar paramProgressBar)
    {
      this.levelProgressBarlevel = paramProgressBar;
    }

    public void setLevelScoreTextView(TextView paramTextView)
    {
      this.levelScoreTextView = paramTextView;
    }

    public void setLocklevel(ImageView paramImageView)
    {
      this.locklevel = paramImageView;
    }

    public void setProgressBarPercentlevel(TextView paramTextView)
    {
      this.progressBarPercentlevel = paramTextView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.LevelListAdapter
 * JD-Core Version:    0.6.2
 */