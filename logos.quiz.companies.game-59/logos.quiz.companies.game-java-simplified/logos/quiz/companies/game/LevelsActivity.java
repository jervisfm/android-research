package logos.quiz.companies.game;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;
import com.swarmconnect.SwarmActivity;
import logo.quiz.commons.FlipAnimator;
import logo.quiz.commons.LevelUtil;

public class LevelsActivity extends SwarmActivity
  implements AdListener
{
  Handler adHandler;
  AdView adView;
  final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);
  Activity myActivity;

  public void back(View paramView)
  {
    Intent localIntent = new Intent(getApplicationContext(), LogosQuizActivity.class);
    localIntent.setFlags(603979776);
    startActivity(localIntent);
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, "GHNSFPPH779R3ZQTGTFJ");
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(2130903044);
    this.myActivity = this;
    this.flipAnimator.setDuration(500L);
    this.flipAnimator.setFillAfter(true);
    this.flipAnimator.setInterpolator(new DecelerateInterpolator());
    Adserwer.setAd((ImageView)findViewById(2131099668), getApplicationContext());
    ((RelativeLayout)this.myActivity.findViewById(2131099667)).addView(Adserwer.getAdmob(this));
    int i = ScoreUtil.getCompletedPoints(this.myActivity);
    ((TextView)findViewById(2131099664)).setText(i);
    int j = ScoreUtil.getCompletedLogosCount(this.myActivity);
    int k = ScoreUtil.getBrandsCount();
    ((TextView)findViewById(2131099665)).setText(j + "/" + k);
    ListView localListView = (ListView)findViewById(2131099666);
    localListView.setAdapter(new LevelListAdapter(getApplicationContext(), ScoreUtil.getLevelsInfo()));
    localListView.setSelection(-1 + LevelUtil.getActiveLevel());
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      Intent localIntent = new Intent(getApplicationContext(), LogosQuizActivity.class);
      localIntent.setFlags(603979776);
      startActivity(localIntent);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public void onReceiveAd(Ad paramAd)
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131099667);
    this.flipAnimator.setmCenterX(DeviceUtil.getDeviceSize(getApplicationContext()).x / 2);
    localRelativeLayout.startAnimation(this.flipAnimator);
  }

  protected void onResume()
  {
    super.onResume();
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }

  public void promo(View paramView)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse(Adserwer.adUrl));
    localIntent.addFlags(1073741824);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
    localEditor.putBoolean(Adserwer.adId, true);
    localEditor.commit();
    startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.LevelsActivity
 * JD-Core Version:    0.6.2
 */