package logos.quiz.companies.game;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;
import logo.quiz.commons.ConstantsProvider;
import logo.quiz.commons.DeviceUtilCommons;
import logo.quiz.commons.FormActivityCommons;
import logo.quiz.commons.LevelUtil;

public class LogosFormActivity extends FormActivityCommons
{
  ConstantsProvider constants = new Constants();

  protected void checkLevelCompletedLogos(Activity paramActivity, int paramInt1, int paramInt2, int paramInt3)
  {
    ScoreUtil.checkLevelCompletedLogos(paramActivity, paramInt1, paramInt2, paramInt3);
  }

  protected int getAvailibleHintsCount(Activity paramActivity)
  {
    return ScoreUtil.getAvailibleHintsCount(this);
  }

  protected int getCompletedLogosCount(Activity paramActivity)
  {
    return ScoreUtil.getCompletedLogosCount(this);
  }

  protected ConstantsProvider getConstants()
  {
    return this.constants;
  }

  protected String getTapJoyPayPerActionCode()
  {
    return "cee84ce5-630f-4e26-ac2f-4ec2cd660b53";
  }

  protected boolean isLevelUnlocked()
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    Level[] arrayOfLevel = ScoreUtil.getLevelsInfo();
    int i = ScoreUtil.getCompletedLogosCount(this, LevelUtil.getActiveLevel());
    boolean bool1 = localSharedPreferences.getBoolean("unlockedLevel" + LevelUtil.getActiveLevel() + "Info", true);
    int j = localSharedPreferences.getInt("allHints", 0);
    if ((bool1) && (LevelUtil.getActiveLevel() < arrayOfLevel.length) && (i >= arrayOfLevel[LevelUtil.getActiveLevel()].getUnlockLimit()));
    for (boolean bool2 = true; ; bool2 = false)
    {
      if (bool2)
      {
        Toast.makeText(this, "Well done! Level " + (1 + LevelUtil.getActiveLevel()) + " unlocked. You get 2 new hints.", 1).show();
        if (localSharedPreferences.getBoolean("SOUND", true))
          DeviceUtilCommons.playSound(getApplicationContext(), 2131034113);
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        localEditor.putBoolean("unlockedLevel" + LevelUtil.getActiveLevel() + "Info", false);
        localEditor.putInt("allHints", j + 2);
        localEditor.commit();
      }
      return bool2;
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.LogosFormActivity
 * JD-Core Version:    0.6.2
 */