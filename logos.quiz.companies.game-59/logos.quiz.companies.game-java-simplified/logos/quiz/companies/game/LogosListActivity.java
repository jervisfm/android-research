package logos.quiz.companies.game;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.swarmconnect.NotificationLeaderboard;
import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmActivity;
import com.swarmconnect.SwarmNotification;
import com.swarmconnect.delegates.SwarmNotificationDelegate;
import java.util.HashMap;
import logo.quiz.commons.BrandTO;
import logo.quiz.commons.FlipAnimator;
import logo.quiz.commons.LevelUtil;

public class LogosListActivity extends SwarmActivity
  implements AdListener
{
  private static final String LOG_TAG = "LogosListActivity";
  Handler adHandler;
  final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);
  Activity myActivity;

  private void completeLogo(int paramInt, GridView paramGridView, boolean paramBoolean)
  {
    BrandTO localBrandTO = (BrandTO)paramGridView.getItemAtPosition(paramInt);
    if (localBrandTO != null)
      localBrandTO.setComplete(paramBoolean);
    paramGridView.invalidateViews();
    ScoreUtil.checkLevelCompletedLogos(this, 2131099693, 2131099692, 2131099691);
    try
    {
      Level[] arrayOfLevel = ScoreUtil.getLevelsInfo();
      int i = ScoreUtil.getCompletedLogosCount(this.myActivity, LevelUtil.getActiveLevel());
      int j = arrayOfLevel[(-1 + LevelUtil.getActiveLevel())].getLogosCount();
      HashMap localHashMap = new HashMap();
      localHashMap.put("Level", String.valueOf(LevelUtil.getActiveLevel()));
      localHashMap.put("Complete logos", i + "/" + j);
      localHashMap.put("Points", String.valueOf(ScoreUtil.getCompletedPoints(this.myActivity, LevelUtil.getActiveLevel())));
      FlurryAgent.onEvent("Level score", localHashMap);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void back(View paramView)
  {
    startActivity(new Intent(getApplicationContext(), LevelsActivity.class));
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    GridView localGridView;
    if (paramInt2 >= 0)
    {
      localGridView = (GridView)findViewById(2131099694);
      if ((paramInt2 != 0) || ((paramIntent != null) && (paramIntent.getBooleanExtra("isCorrect", false))))
        break label89;
      if (paramIntent == null)
        break label59;
      completeLogo(paramInt2, localGridView, paramIntent.getBooleanExtra("isCorrect", false));
    }
    label59: int i;
    do
    {
      return;
      i = PreferenceManager.getDefaultSharedPreferences(this).getInt("complete_position", -1);
    }
    while (i == -1);
    completeLogo(i, localGridView, true);
    return;
    label89: completeLogo(paramInt2, localGridView, true);
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, "GHNSFPPH779R3ZQTGTFJ");
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(2130903047);
    this.myActivity = this;
    this.flipAnimator.setDuration(500L);
    this.flipAnimator.setFillAfter(true);
    this.flipAnimator.setInterpolator(new DecelerateInterpolator());
    Adserwer.setAd((ImageView)findViewById(2131099696), getApplicationContext());
    ((RelativeLayout)this.myActivity.findViewById(2131099695)).addView(Adserwer.getAdmob(this));
    Swarm.addNotificationDelegate(new SwarmNotificationDelegate()
    {
      public boolean gotNotification(SwarmNotification paramAnonymousSwarmNotification)
      {
        boolean bool1 = false;
        if (paramAnonymousSwarmNotification != null)
        {
          boolean bool2 = paramAnonymousSwarmNotification instanceof NotificationLeaderboard;
          bool1 = false;
          if (bool2)
            bool1 = true;
        }
        return bool1;
      }
    });
    GridView localGridView = (GridView)findViewById(2131099694);
    localGridView.setAdapter(new ImageAdapter(getApplicationContext()));
    localGridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        BrandTO localBrandTO = (BrandTO)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
        Intent localIntent = new Intent(LogosListActivity.this.getApplicationContext(), LogosFormActivity.class);
        localIntent.setFlags(603979776);
        localIntent.putExtra("position", paramAnonymousInt);
        localIntent.putExtra("brandPosition", localBrandTO.getBrandPosition());
        localIntent.putExtra("brandNames", localBrandTO.getNames());
        localIntent.putExtra("brandDrawable", localBrandTO.getDrawable());
        localIntent.putExtra("brandTO", localBrandTO);
        LogosListActivity.this.startActivityForResult(localIntent, paramAnonymousInt);
      }
    });
    ScoreUtil.checkLevelCompletedLogos(this, 2131099693, 2131099692, 2131099691);
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      startActivity(new Intent(getApplicationContext(), LevelsActivity.class));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public void onReceiveAd(Ad paramAd)
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131099695);
    this.flipAnimator.setmCenterX(DeviceUtil.getDeviceSize(getApplicationContext()).x / 2);
    localRelativeLayout.startAnimation(this.flipAnimator);
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }

  public void promo(View paramView)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse(Adserwer.adUrl));
    localIntent.addFlags(1073741824);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
    localEditor.putBoolean(Adserwer.adId, true);
    localEditor.commit();
    startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.LogosListActivity
 * JD-Core Version:    0.6.2
 */