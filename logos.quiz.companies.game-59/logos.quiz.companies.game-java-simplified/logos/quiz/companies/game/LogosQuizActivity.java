package logos.quiz.companies.game;

import logo.quiz.commons.ConstantsProvider;
import logo.quiz.commons.LogoQuizActivityCommons;
import logo.quiz.commons.ScoreUtilProvider;

public class LogosQuizActivity extends LogoQuizActivityCommons
{
  ConstantsProvider constants = new Constants();
  ScoreUtilProvider scoreUtilProvider = new ScoreUtilProviderImpl();

  protected int getCompletedPoints()
  {
    return ScoreUtil.getCompletedPoints(this);
  }

  protected ConstantsProvider getConstants()
  {
    return this.constants;
  }

  protected ScoreUtilProvider getScoreUtilProvider()
  {
    return this.scoreUtilProvider;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.LogosQuizActivity
 * JD-Core Version:    0.6.2
 */