package logos.quiz.companies.game;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import com.flurry.android.FlurryAgent;
import com.swarmconnect.SwarmActivity;

public class OptionsActivity extends SwarmActivity
{
  public void back(View paramView)
  {
    startActivity(new Intent(getApplicationContext(), LogosQuizActivity.class));
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, "GHNSFPPH779R3ZQTGTFJ");
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(2130903050);
    Button localButton1 = (Button)findViewById(2131099703);
    Button localButton2 = (Button)findViewById(2131099704);
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    if (localSharedPreferences.getBoolean("SOUND", true))
      localButton1.setText("Sounds on");
    while (localSharedPreferences.getBoolean("VIBRATE", true))
    {
      localButton2.setText("Vibrate on");
      return;
      localButton1.setText("Sounds off");
    }
    localButton2.setText("Vibrate off");
  }

  public void onStop()
  {
    super.onStop();
    FlurryAgent.onEndSession(this);
  }

  public void otherApps(View paramView)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("market://search?q=pub:bubble quiz games"));
    localIntent.addFlags(1073741824);
    startActivity(localIntent);
  }

  public void rate(View paramView)
  {
    FlurryAgent.onEvent("Rate app");
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setData(Uri.parse("market://details?id=logos.quiz.companies.game"));
    localIntent.addFlags(1073741824);
    startActivity(localIntent);
  }

  public void reset(View paramView)
  {
    DialogInterface.OnClickListener local1 = new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        switch (paramAnonymousInt)
        {
        default:
          return;
        case -1:
        }
        ScoreUtil.resetApp(OptionsActivity.this.getApplicationContext());
      }
    };
    new AlertDialog.Builder(this).setMessage("Attention! Are you sure you want to reset all settings this app? After click 'reset' you will lose all your points. Continue?").setPositiveButton("Reset", local1).setNegativeButton("Cancel", local1).show();
  }

  public void soundMute(View paramView)
  {
    FlurryAgent.onEvent("Sound mute");
    Button localButton = (Button)findViewById(2131099703);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    if (localButton.getText().equals("Sounds on"))
    {
      localButton.setText("Sounds off");
      localEditor.putBoolean("SOUND", false);
    }
    while (true)
    {
      localEditor.commit();
      return;
      localButton.setText("Sounds on");
      localEditor.putBoolean("SOUND", true);
      DeviceUtil.playSound(getApplicationContext(), 2131034112);
    }
  }

  public void vibrate(View paramView)
  {
    Button localButton = (Button)findViewById(2131099704);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    if (localButton.getText().equals("Vibrate on"))
    {
      localButton.setText("Vibrate off");
      localEditor.putBoolean("VIBRATE", false);
    }
    while (true)
    {
      localEditor.commit();
      return;
      localButton.setText("Vibrate on");
      localEditor.putBoolean("VIBRATE", true);
      ((Vibrator)getSystemService("vibrator")).vibrate(500L);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.OptionsActivity
 * JD-Core Version:    0.6.2
 */