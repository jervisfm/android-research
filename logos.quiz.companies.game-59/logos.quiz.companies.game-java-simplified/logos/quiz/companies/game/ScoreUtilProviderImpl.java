package logos.quiz.companies.game;

import android.app.Activity;
import logo.quiz.commons.ScoreUtilProvider;

public class ScoreUtilProviderImpl
  implements ScoreUtilProvider
{
  public int getNewLogosCount()
  {
    return ScoreUtil.getNewLogosCount();
  }

  public void initLogos(Activity paramActivity)
  {
    ScoreUtil.initLogos(paramActivity);
  }

  public void setNewLogosCount(int paramInt)
  {
    ScoreUtil.setNewLogosCount(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.ScoreUtilProviderImpl
 * JD-Core Version:    0.6.2
 */