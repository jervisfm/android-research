package logos.quiz.companies.game;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flurry.android.FlurryAgent;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import java.util.Iterator;
import java.util.List;
import logo.quiz.commons.BrandTO;
import logo.quiz.commons.FlipAnimator;
import logo.quiz.commons.Hint;
import logo.quiz.commons.HintsUtil;
import logo.quiz.commons.StatisticsActivityCommons;

public class StatisticsActivity extends StatisticsActivityCommons
  implements AdListener
{
  final FlipAnimator flipAnimator = new FlipAnimator(90.0F, 0.0F, 0.0F, 0.0F);

  private int countAllScore()
  {
    int i = 0;
    BrandTO[] arrayOfBrandTO = ScoreUtil.getBrands(this);
    int j = arrayOfBrandTO.length;
    for (int k = 0; ; k++)
    {
      if (k >= j)
        return i;
      i += arrayOfBrandTO[k].getLevel();
    }
  }

  private int countLevelsCompleted()
  {
    int i = 0;
    Level[] arrayOfLevel = ScoreUtil.getLevelsInfo();
    int j = arrayOfLevel.length;
    for (int k = 0; ; k++)
    {
      if (k >= j)
        return i;
      Level localLevel = arrayOfLevel[k];
      if (localLevel.getLogosCount() == ScoreUtil.getCompletedLogosCount(this, localLevel.getId()))
        i++;
    }
  }

  private int countLevelsUnlocked(int paramInt)
  {
    int i = 0;
    Level[] arrayOfLevel = ScoreUtil.getLevelsInfo();
    int j = arrayOfLevel.length;
    int k = 0;
    if (k >= j)
      return i;
    Level localLevel = arrayOfLevel[k];
    if (localLevel.getId() == 1)
      i++;
    while (true)
    {
      k++;
      break;
      int m = localLevel.getUnlockLimit();
      if (ScoreUtil.getCompletedLogosCount(this, -1 + localLevel.getId()) >= m)
        i++;
    }
  }

  private int countUsedHints()
  {
    int i = 0;
    BrandTO[] arrayOfBrandTO = ScoreUtil.getBrands(this);
    int j = arrayOfBrandTO.length;
    int k = 0;
    if (k >= j)
      return i;
    Iterator localIterator = HintsUtil.getAllHintsForBrand(arrayOfBrandTO[k], getApplicationContext(), true).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        k++;
        break;
      }
      if (((Hint)localIterator.next()).isUsed())
        i++;
    }
  }

  public void back(View paramView)
  {
    startActivity(new Intent(getApplicationContext(), LogosQuizActivity.class));
  }

  public void onCreate(Bundle paramBundle)
  {
    overridePendingTransition(0, 0);
    super.onCreate(paramBundle);
    FlurryAgent.onStartSession(this, "GHNSFPPH779R3ZQTGTFJ");
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(2130903052);
    this.flipAnimator.setDuration(500L);
    this.flipAnimator.setFillAfter(true);
    this.flipAnimator.setInterpolator(new DecelerateInterpolator());
    Adserwer.setAd((ImageView)findViewById(2131099733), getApplicationContext());
    ((RelativeLayout)findViewById(2131099732)).addView(Adserwer.getAdmob(this));
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    TextView localTextView1 = (TextView)findViewById(2131099714);
    TextView localTextView2 = (TextView)findViewById(2131099715);
    TextView localTextView3 = (TextView)findViewById(2131099720);
    TextView localTextView4 = (TextView)findViewById(2131099721);
    TextView localTextView5 = (TextView)findViewById(2131099722);
    TextView localTextView6 = (TextView)findViewById(2131099723);
    TextView localTextView7 = (TextView)findViewById(2131099724);
    TextView localTextView8 = (TextView)findViewById(2131099725);
    TextView localTextView9 = (TextView)findViewById(2131099726);
    TextView localTextView10 = (TextView)findViewById(2131099727);
    TextView localTextView11 = (TextView)findViewById(2131099728);
    TextView localTextView12 = (TextView)findViewById(2131099729);
    TextView localTextView13 = (TextView)findViewById(2131099730);
    TextView localTextView14 = (TextView)findViewById(2131099731);
    TextView localTextView15 = (TextView)findViewById(2131099718);
    TextView localTextView16 = (TextView)findViewById(2131099719);
    TextView localTextView17 = (TextView)findViewById(2131099716);
    TextView localTextView18 = (TextView)findViewById(2131099717);
    int i = ScoreUtil.getCompletedLogosCount(this);
    int j = ScoreUtil.getBrandsCount();
    int k = (int)Math.ceil(100.0F * (i / j));
    int m = ScoreUtil.getCompletedPoints(this);
    int n = countAllScore();
    int i1 = (int)Math.ceil(100.0F * (m / n));
    int i2 = countLevelsUnlocked(m);
    int i3 = ScoreUtil.getLevelsInfo().length;
    int i4 = (int)Math.ceil(100.0F * (i2 / i3));
    int i5 = countLevelsCompleted();
    int i6 = (int)Math.ceil(100.0F * (i5 / i3));
    int i7 = ScoreUtil.getAvailibleHintsCount(this);
    int i8 = -29 + 4 * ScoreUtil.getBrandsCount();
    int i9 = countUsedHints();
    int i10 = i9 + i7;
    int i11 = (int)Math.ceil(100.0F * (i9 / i10));
    int i12 = (int)Math.ceil(100.0F * (i7 / i10));
    int i13 = (int)Math.ceil(100.0F * (i10 / i8));
    int i14 = localSharedPreferences.getInt("perfectGuess", 0);
    int i15 = (int)Math.ceil(100.0F * (i14 / i));
    int i16 = localSharedPreferences.getInt("guessTries", 0);
    localTextView1.setText(i + "/" + j);
    localTextView2.setText(k + "%");
    localTextView3.setText(m + "/" + n);
    localTextView4.setText(i1 + "%");
    localTextView5.setText(i2 + "/" + i3);
    localTextView6.setText(i4 + "%");
    localTextView7.setText(i5 + "/" + i3);
    localTextView8.setText(i6 + "%");
    localTextView9.setText(i10 + "/" + i8);
    localTextView10.setText(i13 + "%");
    localTextView11.setText(i7 + "/" + i10);
    localTextView12.setText(i12 + "%");
    localTextView13.setText(i9 + "/" + i10);
    localTextView14.setText(i11 + "%");
    localTextView15.setText(i16);
    localTextView16.setText("");
    localTextView17.setText(i14 + "/" + i);
    localTextView18.setText(i15 + "%");
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public void onReceiveAd(Ad paramAd)
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)findViewById(2131099732);
    this.flipAnimator.setmCenterX(DeviceUtil.getDeviceSize(getApplicationContext()).x / 2);
    localRelativeLayout.startAnimation(this.flipAnimator);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\logos.quiz.companies.game-59\classes_dex2jar_simplified.jar
 * Qualified Name:     logos.quiz.companies.game.StatisticsActivity
 * JD-Core Version:    0.6.2
 */