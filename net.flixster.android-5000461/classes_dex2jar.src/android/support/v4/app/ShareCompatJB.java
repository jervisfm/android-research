package android.support.v4.app;

import android.text.Html;

class ShareCompatJB
{
  public static String escapeHtml(CharSequence paramCharSequence)
  {
    return Html.escapeHtml(paramCharSequence);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     android.support.v4.app.ShareCompatJB
 * JD-Core Version:    0.6.2
 */