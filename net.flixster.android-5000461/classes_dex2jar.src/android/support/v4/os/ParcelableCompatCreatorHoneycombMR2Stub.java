package android.support.v4.os;

import android.os.Parcelable.Creator;

class ParcelableCompatCreatorHoneycombMR2Stub
{
  static <T> Parcelable.Creator<T> instantiate(ParcelableCompatCreatorCallbacks<T> paramParcelableCompatCreatorCallbacks)
  {
    return new ParcelableCompatCreatorHoneycombMR2(paramParcelableCompatCreatorCallbacks);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     android.support.v4.os.ParcelableCompatCreatorHoneycombMR2Stub
 * JD-Core Version:    0.6.2
 */