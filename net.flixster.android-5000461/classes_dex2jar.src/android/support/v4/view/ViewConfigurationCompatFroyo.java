package android.support.v4.view;

import android.view.ViewConfiguration;

class ViewConfigurationCompatFroyo
{
  public static int getScaledPagingTouchSlop(ViewConfiguration paramViewConfiguration)
  {
    return paramViewConfiguration.getScaledPagingTouchSlop();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     android.support.v4.view.ViewConfigurationCompatFroyo
 * JD-Core Version:    0.6.2
 */