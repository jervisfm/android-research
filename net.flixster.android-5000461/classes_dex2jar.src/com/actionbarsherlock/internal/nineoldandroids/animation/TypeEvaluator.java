package com.actionbarsherlock.internal.nineoldandroids.animation;

public abstract interface TypeEvaluator<T>
{
  public abstract T evaluate(float paramFloat, T paramT1, T paramT2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.actionbarsherlock.internal.nineoldandroids.animation.TypeEvaluator
 * JD-Core Version:    0.6.2
 */