package com.actionbarsherlock.internal.view;

public abstract interface View_HasStateListenerSupport
{
  public abstract void addOnAttachStateChangeListener(View_OnAttachStateChangeListener paramView_OnAttachStateChangeListener);

  public abstract void removeOnAttachStateChangeListener(View_OnAttachStateChangeListener paramView_OnAttachStateChangeListener);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.actionbarsherlock.internal.view.View_HasStateListenerSupport
 * JD-Core Version:    0.6.2
 */