package com.actionbarsherlock.internal.view;

import android.view.View;

public abstract interface View_OnAttachStateChangeListener
{
  public abstract void onViewAttachedToWindow(View paramView);

  public abstract void onViewDetachedFromWindow(View paramView);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.actionbarsherlock.internal.view.View_OnAttachStateChangeListener
 * JD-Core Version:    0.6.2
 */