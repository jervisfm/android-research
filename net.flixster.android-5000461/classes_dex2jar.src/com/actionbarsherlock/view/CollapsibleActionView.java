package com.actionbarsherlock.view;

public abstract interface CollapsibleActionView
{
  public abstract void onActionViewCollapsed();

  public abstract void onActionViewExpanded();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.actionbarsherlock.view.CollapsibleActionView
 * JD-Core Version:    0.6.2
 */