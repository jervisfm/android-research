package com.comscore.analytics;

import android.content.Context;
import com.comscore.applications.EventType;
import java.util.HashMap;

public class Census
{
  private static Census instance = null;
  private String customerID;

  public static Census getInstance()
  {
    if (instance == null)
      instance = new Census();
    return instance;
  }

  public void notifyStart(Context paramContext, String paramString1, String paramString2)
  {
    DAx localDAx = DAx.getInstance();
    localDAx.setAppContext(paramContext);
    if ((paramString1 != null) && (!paramString1.equals("")))
      this.customerID = paramString1;
    if ((paramString2 != null) && (!paramString2.equals("")))
      localDAx.setSalt(paramString2);
    localDAx.setPixelURL("http://b.scorecardresearch.com/p?");
    HashMap localHashMap = new HashMap();
    localHashMap.put("c2", this.customerID);
    localHashMap.put("name", "start");
    localDAx.notify(EventType.Start, localHashMap);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.analytics.Census
 * JD-Core Version:    0.6.2
 */