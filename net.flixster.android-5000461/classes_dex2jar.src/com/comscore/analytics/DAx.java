package com.comscore.analytics;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import com.comscore.applications.Application;
import com.comscore.exceptions.NullApplicationContextException;
import com.comscore.metrics.Analytics;
import com.comscore.utils.API9;
import com.comscore.utils.Date;
import com.comscore.utils.Storage;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DAx extends Analytics
{
  private static DAx instance = null;
  private Boolean anonymous;
  private Context appContext;
  private String appName;
  private Dispatcher dispatcher = new Dispatcher();
  private long genesis = Date.getInstance().unixTime();
  private String pixelURL;
  private Map<String, Application> responders = new HashMap();
  private String salt;
  private String version;
  private String visitorID;

  public static DAx getInstance()
  {
    if (instance == null)
      instance = new DAx();
    return instance;
  }

  private String md5(String paramString)
  {
    byte[] arrayOfByte1 = paramString.getBytes();
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
      localMessageDigest.reset();
      localMessageDigest.update(arrayOfByte1);
      byte[] arrayOfByte2 = localMessageDigest.digest();
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; ; i++)
      {
        if (i >= arrayOfByte2.length)
          return localStringBuffer;
        String str = Integer.toHexString(0xFF & arrayOfByte2[i]);
        if (str.length() == 1)
          localStringBuffer.append('0');
        localStringBuffer.append(str);
      }
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
    }
    return null;
  }

  private void setVisitorId()
  {
    String str1;
    String str2;
    if (this.visitorID == null)
    {
      int i = Integer.valueOf(Build.VERSION.SDK).intValue();
      str1 = null;
      str2 = null;
      if (i >= 9)
      {
        str1 = API9.getSerial();
        str2 = null;
        if (str1 == null);
      }
    }
    try
    {
      boolean bool1 = str1.equals("");
      str2 = null;
      if (!bool1)
      {
        boolean bool2 = str1.equals("unknown");
        str2 = null;
        if (!bool2)
        {
          int j = str1.length();
          str2 = null;
          if (j > 3)
          {
            boolean bool3 = str1.substring(0, 3).equals("***");
            str2 = null;
            if (!bool3)
            {
              boolean bool4 = str1.substring(0, 3).equals("000");
              str2 = null;
              if (!bool4)
              {
                str2 = md5(new StringBuilder(String.valueOf(str1)).append(getSalt()).toString()) + "-s";
                Storage.getInstance().remove("vid");
              }
            }
          }
        }
      }
      label173: if ((str2 == null) && (Integer.valueOf(Build.VERSION.SDK).intValue() >= 3))
      {
        str1 = Settings.Secure.getString(this.appContext.getContentResolver(), "android_id");
        if ((str1 != null) && (("9774d56d682e549c".equals(str1)) || ("unknown".equals(str1)) || ("android_id".equals(str1))))
          str1 = null;
        if ((str1 != null) && (!str1.equals("")))
        {
          str2 = md5(new StringBuilder(String.valueOf(str1)).append(getSalt()).toString()) + "-a";
          Storage.getInstance().remove("vid");
        }
      }
      if ((str1 == null) || (str1.equals("")) || (str2 == null))
      {
        str2 = Storage.getInstance().get("vid");
        if ((str2 == null) || (str2.equals("")))
        {
          str2 = md5(UUID.randomUUID().toString() + getSalt());
          Storage.getInstance().set("vid", str2);
        }
      }
      this.visitorID = str2;
      return;
    }
    catch (Exception localException)
    {
      break label173;
    }
  }

  public Boolean getAnonymous()
  {
    return this.anonymous;
  }

  public Context getAppContext()
  {
    if (this.appContext == null)
      try
      {
        throw new NullApplicationContextException();
      }
      catch (NullApplicationContextException localNullApplicationContextException)
      {
      }
    return this.appContext;
  }

  public String getAppName()
  {
    if ((this.appName == null) || (this.appName.equals("")))
      setAppName((String)this.appContext.getResources().getText(this.appContext.getResources().getIdentifier("app_name", "string", this.appContext.getPackageName())));
    return this.appName;
  }

  public long getGenesis()
  {
    return this.genesis;
  }

  public String getPixelURL()
  {
    return this.pixelURL;
  }

  public String getSalt()
  {
    if ((this.salt == null) || (this.salt.equals("")))
      setSalt(getAppName());
    return this.salt;
  }

  public String getVersion()
  {
    return "1.1108.12";
  }

  public String getVisitorId()
  {
    if (this.visitorID == null)
      setVisitorId();
    return this.visitorID;
  }

  public void notify(com.comscore.applications.EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    ((Application)this.responders.get("Applications")).dispatcher.notify(paramEventType, paramHashMap);
  }

  public void notify(com.comscore.metrics.EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    this.dispatcher.notify(paramEventType, paramHashMap);
  }

  public void notify(String paramString)
  {
    setPixelURL(this.pixelURL);
    ((Application)this.responders.get("Applications")).dispatcher.notify(com.comscore.applications.EventType.View, new HashMap());
  }

  public void notify(String paramString, com.comscore.applications.EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    setPixelURL(paramString);
    notify(paramEventType, paramHashMap);
  }

  public void notify(String paramString, com.comscore.metrics.EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    setPixelURL(paramString);
    this.dispatcher.notify(paramEventType, paramHashMap);
  }

  public void notify(String paramString, HashMap<String, String> paramHashMap)
  {
    setPixelURL(paramString);
    ((Application)this.responders.get("Applications")).dispatcher.notify(com.comscore.applications.EventType.View, paramHashMap);
  }

  public void notify(HashMap<String, String> paramHashMap)
  {
    if (Storage.getInstance().get("PixelURL").equals(""))
      return;
    ((Application)this.responders.get("Applications")).dispatcher.notify(com.comscore.applications.EventType.View, paramHashMap);
  }

  public void setAnonymous(Boolean paramBoolean)
  {
    Storage localStorage = Storage.getInstance();
    if (paramBoolean.booleanValue());
    for (boolean bool = false; ; bool = true)
    {
      localStorage.enabled = Boolean.valueOf(bool);
      this.anonymous = paramBoolean;
      return;
    }
  }

  public DAx setAppContext(Context paramContext)
  {
    this.appContext = paramContext;
    setPixelURL(Storage.getInstance().get("PixelURL"));
    this.responders.put("Applications", new Application());
    return this;
  }

  public void setAppName(String paramString)
  {
    this.appName = paramString;
  }

  public DAx setPixelURL(String paramString)
  {
    if (paramString.contains("&"))
      paramString = paramString.substring(0, paramString.indexOf("&"));
    if ((!paramString.contains("?")) && (!paramString.contains("//")))
    {
      if (this.pixelURL == null)
        break label185;
      if ((this.pixelURL == "") || (!this.pixelURL.contains("?")))
        break label161;
      paramString = this.pixelURL.substring(0, 1 + this.pixelURL.indexOf("?")) + paramString;
    }
    while (true)
    {
      if (paramString.endsWith("?"))
        paramString = paramString + "Application";
      this.pixelURL = paramString;
      Storage.getInstance().set("PixelURL", this.pixelURL);
      return this;
      label161: paramString = paramString + "?";
      continue;
      label185: paramString = paramString + "?";
    }
  }

  public DAx setSalt(String paramString)
  {
    if ((paramString != null) && (!paramString.equals("")))
      this.salt = paramString;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.analytics.DAx
 * JD-Core Version:    0.6.2
 */