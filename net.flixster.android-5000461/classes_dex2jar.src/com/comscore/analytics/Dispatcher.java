package com.comscore.analytics;

import com.comscore.events.Event;
import com.comscore.events.EventArgs;
import com.comscore.metrics.DispatcherInterface;
import com.comscore.metrics.EventType;
import com.comscore.utils.Queue;
import java.util.HashMap;

public class Dispatcher
  implements DispatcherInterface
{
  public void notify(EventArgs paramEventArgs, com.comscore.measurement.Measurement paramMeasurement)
  {
    Queue.getInstance().enqueue(paramMeasurement);
  }

  public void notify(EventType paramEventType)
  {
    notify(paramEventType, new HashMap());
  }

  public void notify(EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    EventArgs localEventArgs = new EventArgs(new Event(paramEventType.toString()), paramHashMap);
    notify(localEventArgs, new Measurement(localEventArgs));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.analytics.Dispatcher
 * JD-Core Version:    0.6.2
 */