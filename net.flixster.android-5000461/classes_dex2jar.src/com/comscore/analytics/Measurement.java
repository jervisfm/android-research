package com.comscore.analytics;

import android.os.Build;
import com.comscore.events.Event;
import com.comscore.events.EventArgs;
import com.comscore.measurement.PrivilegedLabel;
import com.comscore.metrics.EventType;
import com.comscore.utils.Date;
import com.comscore.utils.Storage;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class Measurement extends com.comscore.measurement.Measurement
{
  public Measurement()
  {
    setLabel(new PrivilegedLabel("c1", "19", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c4", DAx.getInstance().getAppName(), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c10", "android", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c12", DAx.getInstance().getVisitorId(), Boolean.valueOf(false)));
    if (Storage.getInstance().has("vid").booleanValue())
      setLabel(new PrivilegedLabel("ns_ap_c12m", "1", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_device", Build.DEVICE, Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_type", EventType.View.toString(), Boolean.valueOf(false)));
    if (!hasLabel("c3").booleanValue())
      setLabel(new PrivilegedLabel("c3", EventType.View.toString(), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ts", Long.toString(Date.getInstance().unixTime()), Boolean.valueOf(false)));
  }

  public Measurement(EventArgs paramEventArgs)
  {
    setLabel(new PrivilegedLabel("c1", "19", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c4", DAx.getInstance().getAppName(), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c10", "android", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c12", DAx.getInstance().getVisitorId(), Boolean.valueOf(false)));
    if (Storage.getInstance().has("vid").booleanValue())
      setLabel(new PrivilegedLabel("ns_ap_c12m", "1", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_device", Build.DEVICE, Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_as", Long.toString(DAx.getInstance().getGenesis()), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_type", paramEventArgs.event.type, Boolean.valueOf(false)));
    if (!hasLabel("c3").booleanValue())
      setLabel(new PrivilegedLabel("c3", paramEventArgs.event.type, Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ts", Long.toString(paramEventArgs.created), Boolean.valueOf(false)));
    Iterator localIterator = paramEventArgs.details.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      setLabel((String)localEntry.getKey(), (String)localEntry.getValue());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.analytics.Measurement
 * JD-Core Version:    0.6.2
 */