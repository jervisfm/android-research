package com.comscore.applications;

import com.comscore.measurement.Label;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class AggregateMeasurement extends Measurement
{
  public AggregateMeasurement(EventArgs paramEventArgs)
  {
    super(paramEventArgs);
    Iterator localIterator = paramEventArgs.details.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      setLabel((String)localEntry.getKey(), (String)localEntry.getValue(), Boolean.valueOf(true));
    }
  }

  private String addValue(String paramString1, String paramString2)
  {
    String str1 = new String(paramString2);
    Iterator localIterator = getElementsFromList(paramString1).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return str1;
      String str2 = (String)localIterator.next();
      if (!str1.contains(str2))
      {
        if (str1.equals(""))
          str1 = str1 + str2 + ":1";
        else
          str1 = str1 + ";" + str2 + ":1";
      }
      else
      {
        String[] arrayOfString1 = str1.split(";");
        for (int i = 0; i < arrayOfString1.length; i++)
          if (arrayOfString1[i].contains(str2))
          {
            String[] arrayOfString2 = arrayOfString1[i].split(":");
            Integer localInteger = Integer.valueOf(1 + new Integer(arrayOfString2[1]).intValue());
            String str3 = arrayOfString2[0] + ":" + localInteger;
            str1 = str1.replace(arrayOfString1[i], str3);
          }
      }
    }
  }

  private Boolean existingString(String paramString1, String paramString2)
  {
    return Boolean.valueOf(paramString1.contains(paramString2));
  }

  private List<String> getElementsFromList(String paramString)
  {
    String[] arrayOfString = paramString.split(",");
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfString.length)
        return localArrayList;
      localArrayList.add(arrayOfString[i]);
    }
  }

  private Boolean isInteger(String paramString)
  {
    for (int i = 0; ; i++)
    {
      if (i >= paramString.length())
        return Boolean.valueOf(true);
      if (!"0123456789".contains(paramString.charAt(i)))
        return Boolean.valueOf(false);
    }
  }

  private Boolean isList(String paramString)
  {
    if (!paramString.contains(","))
      return Boolean.valueOf(false);
    if (!paramString.contains(" "))
      return Boolean.valueOf(true);
    return Boolean.valueOf(false);
  }

  public void aggregateLabels(List<Label> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Label localLabel1 = (Label)localIterator.next();
      List localList = retrieveLabel(localLabel1.name);
      if (localList.isEmpty())
      {
        if (isList(localLabel1.value).booleanValue())
        {
          String str3 = addValue(localLabel1.value, "");
          setLabel(localLabel1.name, str3, Boolean.valueOf(true));
        }
        else
        {
          setLabel(localLabel1);
        }
      }
      else
      {
        Label localLabel2 = (Label)localList.get(0);
        if ((isInteger(localLabel2.value).booleanValue()) && (isInteger(localLabel1.value).booleanValue()))
        {
          Integer localInteger = Integer.valueOf(new Integer(localLabel2.value).intValue() + new Integer(localLabel1.value).intValue());
          setLabel(localLabel2.name, localInteger.toString(), Boolean.valueOf(true));
        }
        else if (isList(localLabel1.value).booleanValue())
        {
          String str2 = addValue(localLabel1.value, localLabel2.value);
          setLabel(localLabel1.name, str2, Boolean.valueOf(true));
        }
        else if (!existingString(localLabel2.value, localLabel1.value).booleanValue())
        {
          String str1 = localLabel2.value + "," + localLabel1.value;
          setLabel(localLabel2.name, str1, Boolean.valueOf(true));
        }
      }
    }
  }

  public void formatLists()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = this.labels.iterator();
    Iterator localIterator2;
    if (!localIterator1.hasNext())
      localIterator2 = localArrayList.iterator();
    while (true)
    {
      if (!localIterator2.hasNext())
      {
        return;
        Label localLabel1 = (Label)localIterator1.next();
        if (!isList(localLabel1.value).booleanValue())
          break;
        localArrayList.add(localLabel1);
        break;
      }
      Label localLabel2 = (Label)localIterator2.next();
      setLabel(localLabel2.name, addValue(localLabel2.value, ""), Boolean.valueOf(true));
    }
  }

  public List<Label> getAggregateLabels()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.labels.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return localArrayList;
      Label localLabel = (Label)localIterator.next();
      if (localLabel.aggregate.booleanValue())
        localArrayList.add(localLabel);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.AggregateMeasurement
 * JD-Core Version:    0.6.2
 */