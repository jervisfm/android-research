package com.comscore.applications;

import android.os.Build.VERSION;
import com.comscore.measurement.PrivilegedLabel;
import com.comscore.utils.Store;
import com.comscore.utils.StoreProp;

public class AppStartMeasurement extends Measurement
{
  public AppStartMeasurement(EventArgs paramEventArgs)
  {
    super(paramEventArgs);
    setLabel(new PrivilegedLabel("ns_ap_install", "yes", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_runs", Store.getInstance().getRuns().getValue(), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_pfm", "android", Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("ns_ap_pfv", Build.VERSION.RELEASE, Boolean.valueOf(false)));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.AppStartMeasurement
 * JD-Core Version:    0.6.2
 */