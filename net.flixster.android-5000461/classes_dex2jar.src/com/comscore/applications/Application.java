package com.comscore.applications;

import com.comscore.metrics.Analytics;
import com.comscore.utils.Store;
import com.comscore.utils.StoreProp;

public class Application extends Analytics
{
  public Dispatcher dispatcher = new Dispatcher();

  public Application()
  {
    if (Store.getInstance().getRuns().getValue().equals(""))
    {
      Store.getInstance().setFirstRun(Boolean.valueOf(true));
      Store.getInstance().getRuns().setValue("0");
    }
    if (!Store.getInstance().getFirstRun().booleanValue())
    {
      Integer localInteger = Integer.valueOf(1 + new Integer(Store.getInstance().getRuns().getValue()).intValue());
      Store.getInstance().getRuns().setValue(localInteger.toString());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.Application
 * JD-Core Version:    0.6.2
 */