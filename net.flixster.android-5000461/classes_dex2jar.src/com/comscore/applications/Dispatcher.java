package com.comscore.applications;

import com.comscore.metrics.DispatcherInterface;
import com.comscore.utils.Queue;
import java.util.HashMap;

public class Dispatcher
  implements DispatcherInterface
{
  public void notify(EventType paramEventType)
  {
    notify(paramEventType, new HashMap());
  }

  public void notify(EventType paramEventType, HashMap<String, String> paramHashMap)
  {
    EventArgs localEventArgs = new EventArgs(paramEventType, new Event(paramEventType), paramHashMap);
    Object localObject;
    if (paramEventType.equals(EventType.Start))
      localObject = new AppStartMeasurement(localEventArgs);
    while (true)
    {
      if (!paramEventType.equals(EventType.Close))
        notify(localEventArgs, (com.comscore.measurement.Measurement)localObject);
      return;
      if (paramEventType.equals(EventType.Aggregate))
      {
        localObject = new AggregateMeasurement(localEventArgs);
      }
      else
      {
        boolean bool = paramEventType.equals(EventType.Close);
        localObject = null;
        if (!bool)
          localObject = new Measurement(localEventArgs);
      }
    }
  }

  public void notify(com.comscore.events.EventArgs paramEventArgs, com.comscore.measurement.Measurement paramMeasurement)
  {
    Queue.getInstance().enqueue(paramMeasurement);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.Dispatcher
 * JD-Core Version:    0.6.2
 */