package com.comscore.applications;

public class Event extends com.comscore.events.Event
{
  public String name;

  public Event(EventType paramEventType)
  {
    this(paramEventType, com.comscore.metrics.EventType.View);
  }

  public Event(EventType paramEventType, com.comscore.metrics.EventType paramEventType1)
  {
    super(paramEventType1.toString());
    this.name = paramEventType.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.Event
 * JD-Core Version:    0.6.2
 */