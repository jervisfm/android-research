package com.comscore.applications;

import java.util.HashMap;

public class EventArgs extends com.comscore.events.EventArgs
{
  public EventType appEventType;
  public String pageName;

  public EventArgs(Event paramEvent)
  {
    this(paramEvent, new HashMap());
  }

  public EventArgs(Event paramEvent, HashMap<String, String> paramHashMap)
  {
    this(EventType.View, paramEvent, paramHashMap);
  }

  public EventArgs(EventType paramEventType)
  {
    this(paramEventType, new Event(paramEventType), new HashMap());
  }

  public EventArgs(EventType paramEventType, Event paramEvent, HashMap<String, String> paramHashMap)
  {
    super(com.comscore.metrics.EventType.View, paramEvent, paramHashMap);
    this.appEventType = paramEventType;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.EventArgs
 * JD-Core Version:    0.6.2
 */