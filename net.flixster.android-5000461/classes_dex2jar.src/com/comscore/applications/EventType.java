package com.comscore.applications;

public enum EventType
{
  static
  {
    Close = new EventType("Close", 2);
    Aggregate = new EventType("Aggregate", 3);
    EventType[] arrayOfEventType = new EventType[4];
    arrayOfEventType[0] = Start;
    arrayOfEventType[1] = View;
    arrayOfEventType[2] = Close;
    arrayOfEventType[3] = Aggregate;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.EventType
 * JD-Core Version:    0.6.2
 */