package com.comscore.applications;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Display;
import android.view.WindowManager;
import com.comscore.analytics.DAx;
import com.comscore.events.Event;
import com.comscore.measurement.PrivilegedLabel;

public class Measurement extends com.comscore.analytics.Measurement
{
  public Measurement()
  {
    super(new com.comscore.events.EventArgs(new Event(com.comscore.metrics.EventType.View.toString())));
  }

  public Measurement(EventArgs paramEventArgs)
  {
    super(new com.comscore.events.EventArgs(new Event(com.comscore.metrics.EventType.View.toString()), paramEventArgs.details));
    if ((paramEventArgs.pageName != null) && (!paramEventArgs.pageName.equals("")))
      DAx.getInstance().setPixelURL(paramEventArgs.pageName);
    setLabel(new PrivilegedLabel("ns_ap_event", paramEventArgs.appEventType.toString(), Boolean.valueOf(false)));
    setLabel(new PrivilegedLabel("c3", paramEventArgs.appEventType.toString(), Boolean.valueOf(false)));
    String str1 = DAx.getInstance().getAppContext().getPackageName();
    try
    {
      str2 = DAx.getInstance().getAppContext().getPackageManager().getPackageInfo(str1, 0).versionName;
      setLabel(new PrivilegedLabel("ns_ap_ver", str2, Boolean.valueOf(false)));
      setLabel(new PrivilegedLabel("ns_ap_usage", Long.toString(paramEventArgs.created - DAx.getInstance().getGenesis()), Boolean.valueOf(false)));
      Display localDisplay = ((WindowManager)DAx.getInstance().getAppContext().getSystemService("window")).getDefaultDisplay();
      setLabel(new PrivilegedLabel("ns_ap_res", Integer.toString(localDisplay.getWidth()) + "x" + Integer.toString(localDisplay.getHeight()), Boolean.valueOf(false)));
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        String str2 = null;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.applications.Measurement
 * JD-Core Version:    0.6.2
 */