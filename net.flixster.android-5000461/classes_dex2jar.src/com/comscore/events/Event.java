package com.comscore.events;

public class Event
{
  public String type;

  public Event()
  {
  }

  public Event(String paramString)
  {
    this.type = paramString;
  }

  public String getType()
  {
    return this.type;
  }

  public void setType(String paramString)
  {
    this.type = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.events.Event
 * JD-Core Version:    0.6.2
 */