package com.comscore.events;

import com.comscore.metrics.EventType;
import com.comscore.utils.Date;
import java.util.HashMap;

public class EventArgs
{
  public Boolean cancel;
  public long created;
  public HashMap<String, String> details;
  public Event event;
  public EventType eventType;

  public EventArgs()
  {
  }

  public EventArgs(Event paramEvent)
  {
    this(paramEvent, new HashMap());
  }

  public EventArgs(Event paramEvent, HashMap<String, String> paramHashMap)
  {
    this.event = paramEvent;
    this.created = Date.getInstance().unixTime();
    this.cancel = Boolean.valueOf(true);
    this.details = paramHashMap;
  }

  public EventArgs(EventType paramEventType, Event paramEvent, HashMap<String, String> paramHashMap)
  {
    this(paramEvent, paramHashMap);
    this.eventType = paramEventType;
  }

  public void cancelEvent()
  {
    this.cancel = Boolean.valueOf(false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.events.EventArgs
 * JD-Core Version:    0.6.2
 */