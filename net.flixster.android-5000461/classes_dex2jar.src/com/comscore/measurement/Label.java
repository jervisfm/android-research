package com.comscore.measurement;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Label
  implements LabelInterface
{
  public Boolean aggregate;
  public String name;
  public String value;

  public Label(String paramString1, String paramString2, Boolean paramBoolean)
  {
    this.name = paramString1;
    this.value = paramString2;
    this.aggregate = paramBoolean;
  }

  public String pack()
  {
    if ((this.name != null) && (this.value != null) && (!this.name.equals("")) && (!this.value.equals("")))
      try
      {
        String str = "&" + this.name + "=" + URLEncoder.encode(this.value, "UTF-8");
        return str;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
      }
    return "";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.measurement.Label
 * JD-Core Version:    0.6.2
 */