package com.comscore.measurement;

import com.comscore.events.EventArgs;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Measurement
  implements MeasurementInterface
{
  protected ArrayList<Label> labels;

  public Measurement()
  {
    this.labels = new ArrayList();
  }

  public Measurement(EventArgs paramEventArgs)
  {
  }

  public Measurement(Map<String, String> paramMap)
  {
    this();
    Iterator localIterator = paramMap.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      setLabel((String)localEntry.getKey(), (String)localEntry.getValue());
    }
  }

  protected void appendLabel(Label paramLabel)
  {
    if (retrieveLabel(paramLabel.name).size() == 0)
      this.labels.add(paramLabel);
  }

  protected void appendLabel(String paramString1, String paramString2, Boolean paramBoolean1, Boolean paramBoolean2)
  {
    if (paramBoolean2.booleanValue())
    {
      appendLabel(new PrivilegedLabel(paramString1, paramString2, paramBoolean1));
      return;
    }
    appendLabel(new Label(paramString1, paramString2, paramBoolean1));
  }

  protected Boolean compareLabel(String paramString, long paramLong)
  {
    return compareLabel(paramString, Long.toString(paramLong));
  }

  protected Boolean compareLabel(String paramString1, String paramString2)
  {
    List localList = retrieveLabel(paramString1);
    if (localList.size() > 0)
      return Boolean.valueOf(((Label)localList.get(0)).value.equals(paramString2));
    return Boolean.valueOf(false);
  }

  public ArrayList<Label> getLabel(String paramString)
  {
    return (ArrayList)retrieveLabel(paramString);
  }

  public Boolean hasLabel(String paramString)
  {
    if (retrieveLabel(paramString).isEmpty());
    for (boolean bool = false; ; bool = true)
      return Boolean.valueOf(bool);
  }

  public String pack()
  {
    String str = "";
    Iterator localIterator = this.labels.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return str;
      str = str.concat(((Label)localIterator.next()).pack());
    }
  }

  protected void removeLabel(String paramString, Boolean paramBoolean1, Boolean paramBoolean2)
  {
    Iterator localIterator = retrieveLabel(paramString).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Label localLabel = (Label)localIterator.next();
      if (((localLabel instanceof PrivilegedLabel)) || (paramBoolean2.booleanValue()) || (paramBoolean1.booleanValue()))
        this.labels.remove(localLabel);
    }
  }

  protected List<Label> retrieveLabel(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.labels.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return localArrayList;
      Label localLabel = (Label)localIterator.next();
      if (localLabel.name.equals(paramString))
        localArrayList.add(localLabel);
    }
  }

  public void setLabel(Label paramLabel)
  {
    removeLabel(paramLabel.name, paramLabel.aggregate, Boolean.valueOf(paramLabel instanceof PrivilegedLabel));
    appendLabel(paramLabel);
  }

  public void setLabel(String paramString1, String paramString2)
  {
    setLabel(paramString1, paramString2, Boolean.valueOf(false));
  }

  public void setLabel(String paramString1, String paramString2, Boolean paramBoolean)
  {
    removeLabel(paramString1, paramBoolean, Boolean.valueOf(false));
    appendLabel(paramString1, paramString2, paramBoolean, Boolean.valueOf(false));
  }

  protected void setLabel(String paramString1, String paramString2, Boolean paramBoolean1, Boolean paramBoolean2)
  {
    removeLabel(paramString1, paramBoolean1, paramBoolean2);
    appendLabel(paramString1, paramString2, paramBoolean1, paramBoolean2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.measurement.Measurement
 * JD-Core Version:    0.6.2
 */