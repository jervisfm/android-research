package com.comscore.measurement;

import java.util.ArrayList;

public abstract interface MeasurementInterface
{
  public abstract ArrayList<Label> getLabel(String paramString);

  public abstract Boolean hasLabel(String paramString);

  public abstract String pack();

  public abstract void setLabel(String paramString1, String paramString2);

  public abstract void setLabel(String paramString1, String paramString2, Boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.measurement.MeasurementInterface
 * JD-Core Version:    0.6.2
 */