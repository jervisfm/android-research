package com.comscore.measurement;

public class PrivilegedLabel extends Label
{
  private Boolean privileged = Boolean.valueOf(true);

  public PrivilegedLabel(String paramString1, String paramString2, Boolean paramBoolean)
  {
    super(paramString1, paramString2, paramBoolean);
  }

  public Boolean getPrivileged()
  {
    return this.privileged;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.measurement.PrivilegedLabel
 * JD-Core Version:    0.6.2
 */