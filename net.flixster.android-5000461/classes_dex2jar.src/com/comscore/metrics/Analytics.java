package com.comscore.metrics;

import com.comscore.analytics.DAx;
import com.comscore.events.EventArgs;
import com.comscore.measurement.Measurement;
import com.comscore.product.Product;

public class Analytics extends Product
  implements DispatcherInterface
{
  public Analytics()
  {
  }

  public Analytics(DAx paramDAx)
  {
    super(paramDAx);
  }

  public void notify(EventArgs paramEventArgs, Measurement paramMeasurement)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.metrics.Analytics
 * JD-Core Version:    0.6.2
 */