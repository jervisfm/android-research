package com.comscore.metrics;

import com.comscore.events.EventArgs;
import com.comscore.measurement.Measurement;

public abstract interface DispatcherInterface
{
  public abstract void notify(EventArgs paramEventArgs, Measurement paramMeasurement);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.metrics.DispatcherInterface
 * JD-Core Version:    0.6.2
 */