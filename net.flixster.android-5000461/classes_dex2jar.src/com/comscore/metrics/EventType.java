package com.comscore.metrics;

public enum EventType
{
  static
  {
    Hidden = new EventType("Hidden", 1);
    EventType[] arrayOfEventType = new EventType[2];
    arrayOfEventType[0] = View;
    arrayOfEventType[1] = Hidden;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.metrics.EventType
 * JD-Core Version:    0.6.2
 */