package com.comscore.metrics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import com.comscore.analytics.DAx;
import com.comscore.measurement.Measurement;
import com.comscore.utils.Permissions;
import com.comscore.utils.Queue;
import com.comscore.utils.Storage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Request extends AsyncTask<URL, Integer, Integer>
  implements RequestInterface
{
  private HttpURLConnection conn;
  private Measurement measurement;

  public Request(Measurement paramMeasurement)
  {
    this.measurement = paramMeasurement;
  }

  public Boolean availableConnection()
  {
    try
    {
      ConnectivityManager localConnectivityManager = (ConnectivityManager)DAx.getInstance().getAppContext().getSystemService("connectivity");
      NetworkInfo localNetworkInfo1 = localConnectivityManager.getNetworkInfo(1);
      NetworkInfo localNetworkInfo2 = localConnectivityManager.getNetworkInfo(0);
      if ((localNetworkInfo1 != null) && (localNetworkInfo1.isConnected()))
        return Boolean.valueOf(true);
      if (localNetworkInfo2 != null)
        return Boolean.valueOf(localNetworkInfo2.isConnected());
      Boolean localBoolean = Boolean.valueOf(false);
      return localBoolean;
    }
    catch (NullPointerException localNullPointerException)
    {
    }
    return Boolean.valueOf(false);
  }

  protected Integer doInBackground(URL[] paramArrayOfURL)
  {
    try
    {
      this.conn = ((HttpURLConnection)paramArrayOfURL[0].openConnection());
      Integer localInteger = Integer.valueOf(this.conn.getResponseCode());
      return localInteger;
    }
    catch (IOException localIOException)
    {
    }
    return Integer.valueOf(400);
  }

  protected void onPostExecute(Integer paramInteger)
  {
    this.conn.disconnect();
    Queue.getInstance().dequeue();
    if (paramInteger.equals(Integer.valueOf(200)))
      Storage.getInstance().set("lastTimeout", this.measurement.pack());
  }

  public URL process()
  {
    return process(DAx.getInstance().getPixelURL());
  }

  public URL process(String paramString)
  {
    String str = paramString.concat(this.measurement.pack());
    if (str.length() > 2048)
      str = str.substring(0, 2048);
    try
    {
      URL localURL = new URL(str);
      return localURL;
    }
    catch (MalformedURLException localMalformedURLException)
    {
    }
    return null;
  }

  public void send()
  {
    if ("sdk".equals(Build.PRODUCT))
    {
      URL[] arrayOfURL2 = new URL[1];
      arrayOfURL2[0] = process();
      execute(arrayOfURL2);
      return;
    }
    if ((!Permissions.check("android.permission.ACCESS_NETWORK_STATE").booleanValue()) || (availableConnection().booleanValue()))
    {
      URL[] arrayOfURL1 = new URL[1];
      arrayOfURL1[0] = process();
      execute(arrayOfURL1);
      return;
    }
    Queue.getInstance().dequeue();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.metrics.Request
 * JD-Core Version:    0.6.2
 */