package com.comscore.metrics;

import java.net.URL;

public abstract interface RequestInterface
{
  public abstract Boolean availableConnection();

  public abstract URL process();

  public abstract URL process(String paramString);

  public abstract void send();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.metrics.RequestInterface
 * JD-Core Version:    0.6.2
 */