package com.comscore.product;

import com.comscore.analytics.DAx;
import com.comscore.analytics.Dispatcher;

public class Product
{
  Dispatcher dispatcher;
  DAx parent;

  public Product()
  {
  }

  public Product(DAx paramDAx)
  {
    this.parent = paramDAx;
  }

  public Dispatcher getDispatcher()
  {
    return this.dispatcher;
  }

  public DAx getParent()
  {
    return this.parent;
  }

  public void setDispatcher(Dispatcher paramDispatcher)
  {
    this.dispatcher = paramDispatcher;
  }

  public void setParent(DAx paramDAx)
  {
    this.parent = paramDAx;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.product.Product
 * JD-Core Version:    0.6.2
 */