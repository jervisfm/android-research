package com.comscore.utils;

import android.os.Build;
import android.os.Build.VERSION;

public class API9
{
  public static String getSerial()
  {
    if (Integer.valueOf(Build.VERSION.SDK).intValue() >= 9)
      return Build.SERIAL;
    return "";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.API9
 * JD-Core Version:    0.6.2
 */