package com.comscore.utils;

public class Date
{
  private static Date instance = null;

  public static Date getInstance()
  {
    if (instance == null)
      instance = new Date();
    return instance;
  }

  public long unixTime()
  {
    return System.currentTimeMillis();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.Date
 * JD-Core Version:    0.6.2
 */