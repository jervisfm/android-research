package com.comscore.utils;

import com.comscore.applications.AggregateMeasurement;
import com.comscore.measurement.Label;
import com.comscore.measurement.Measurement;
import com.comscore.metrics.Request;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Queue extends ConcurrentLinkedQueue
{
  private static Queue instance = null;
  private static final long serialVersionUID = 1L;
  private AggregateMeasurement aggr = null;
  private Request last;

  private void engage()
  {
    if (size() == 1)
      process();
  }

  public static Queue getInstance()
  {
    if (instance == null)
      instance = new Queue();
    return instance;
  }

  public Request dequeue()
  {
    if (size() > 0)
    {
      this.last = ((Request)super.poll());
      process();
      return this.last;
    }
    return null;
  }

  public void enqueue(Measurement paramMeasurement)
  {
    if ((paramMeasurement instanceof AggregateMeasurement))
    {
      if (this.aggr == null)
      {
        this.aggr = ((AggregateMeasurement)paramMeasurement);
        this.aggr.formatLists();
        return;
      }
      this.aggr.aggregateLabels(((AggregateMeasurement)paramMeasurement).getAggregateLabels());
      return;
    }
    Iterator localIterator;
    if (this.aggr != null)
      localIterator = this.aggr.getAggregateLabels().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        this.aggr = null;
        enqueue(new Request(paramMeasurement));
        return;
      }
      paramMeasurement.setLabel((Label)localIterator.next());
    }
  }

  public void enqueue(Request paramRequest)
  {
    add(paramRequest);
    engage();
  }

  public Request getLast()
  {
    return this.last;
  }

  public Request peek()
  {
    if (size() > 0)
      return (Request)super.peek();
    return null;
  }

  public void process()
  {
    Request localRequest = peek();
    if (localRequest != null)
      localRequest.send();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.Queue
 * JD-Core Version:    0.6.2
 */