package com.comscore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.comscore.analytics.DAx;

public class Storage
{
  private static final String PrefsName = "cSPrefs";
  private static Storage instance = null;
  public Boolean enabled = Boolean.valueOf(true);
  private SharedPreferences prefs = DAx.getInstance().getAppContext().getSharedPreferences("cSPrefs", 0);

  public static Storage getInstance()
  {
    if (instance == null)
      instance = new Storage();
    return instance;
  }

  public String get(String paramString)
  {
    if ((this.enabled.booleanValue()) && (has(paramString).booleanValue()))
      return this.prefs.getString(paramString, "default");
    return "";
  }

  public Boolean has(String paramString)
  {
    if (this.enabled.booleanValue())
      return Boolean.valueOf(this.prefs.contains(paramString));
    return Boolean.valueOf(false);
  }

  public void remove(String paramString)
  {
    if (has(paramString).booleanValue())
    {
      SharedPreferences.Editor localEditor = this.prefs.edit();
      localEditor.remove(paramString);
      localEditor.commit();
    }
  }

  public void set(String paramString1, String paramString2)
  {
    if (this.enabled.booleanValue())
    {
      SharedPreferences.Editor localEditor = this.prefs.edit();
      localEditor.putString(paramString1, paramString2);
      localEditor.commit();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.Storage
 * JD-Core Version:    0.6.2
 */