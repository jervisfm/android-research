package com.comscore.utils;

public class Store
{
  private static Store instance = null;
  private Boolean firstRun = Boolean.valueOf(false);
  private StoreProp runs = new StoreProp("runs");

  public static Store getInstance()
  {
    if (instance == null)
      instance = new Store();
    return instance;
  }

  public Boolean getFirstRun()
  {
    return this.firstRun;
  }

  public StoreProp getRuns()
  {
    return this.runs;
  }

  public void setFirstRun(Boolean paramBoolean)
  {
    this.firstRun = paramBoolean;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.Store
 * JD-Core Version:    0.6.2
 */