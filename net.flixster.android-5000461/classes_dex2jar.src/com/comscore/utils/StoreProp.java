package com.comscore.utils;

public class StoreProp
{
  private String name;
  private String value;

  public StoreProp(String paramString)
  {
    this.name = paramString;
  }

  public String getName()
  {
    return this.name;
  }

  public String getValue()
  {
    return Storage.getInstance().get(this.name);
  }

  public void setValue(String paramString)
  {
    if ((paramString != null) && (!paramString.equals("")))
    {
      this.value = paramString;
      Storage.getInstance().set(this.name, this.value);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.comscore.utils.StoreProp
 * JD-Core Version:    0.6.2
 */