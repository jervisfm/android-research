package com.facebook.android;

import android.util.Log;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public abstract class BaseRequestListener
  implements AsyncFacebookRunner.RequestListener
{
  public void onFacebookError(FacebookError paramFacebookError, Object paramObject)
  {
    Log.e("Facebook", paramFacebookError.getMessage());
    paramFacebookError.printStackTrace();
  }

  public void onFileNotFoundException(FileNotFoundException paramFileNotFoundException, Object paramObject)
  {
    Log.e("Facebook", paramFileNotFoundException.getMessage());
    paramFileNotFoundException.printStackTrace();
  }

  public void onIOException(IOException paramIOException, Object paramObject)
  {
    Log.e("Facebook", paramIOException.getMessage());
    paramIOException.printStackTrace();
  }

  public void onMalformedURLException(MalformedURLException paramMalformedURLException, Object paramObject)
  {
    Log.e("Facebook", paramMalformedURLException.getMessage());
    paramMalformedURLException.printStackTrace();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.facebook.android.BaseRequestListener
 * JD-Core Version:    0.6.2
 */