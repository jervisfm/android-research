package com.facebook.android;

import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;

public class FlxFbTrackingHelper
{
  protected static void trackSsoLogin()
  {
    Trackers.instance().track("/facebook/login/sso", null);
  }

  protected static void trackSsoLoginSuccess()
  {
    Trackers.instance().track("/facebook/success/sso", null);
  }

  protected static void trackWebLogin()
  {
    Trackers.instance().track("/facebook/login/web", null);
  }

  protected static void trackWebLoginSuccess()
  {
    Trackers.instance().track("/facebook/success/web", null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.facebook.android.FlxFbTrackingHelper
 * JD-Core Version:    0.6.2
 */