package com.facebook.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionStore
{
  private static final String EXPIRES = "expires_in";
  private static final String KEY = "facebook-session";
  private static final String TOKEN = "access_token";

  public static void clear(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("facebook-session", 0).edit();
    localEditor.clear();
    localEditor.commit();
  }

  public static boolean restore(Facebook paramFacebook, Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("facebook-session", 0);
    paramFacebook.setAccessToken(localSharedPreferences.getString("access_token", null));
    paramFacebook.setAccessExpires(localSharedPreferences.getLong("expires_in", 0L));
    return paramFacebook.isSessionValid();
  }

  public static boolean save(Facebook paramFacebook, Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("facebook-session", 0).edit();
    localEditor.putString("access_token", paramFacebook.getAccessToken());
    localEditor.putLong("expires_in", paramFacebook.getAccessExpires());
    return localEditor.commit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.facebook.android.SessionStore
 * JD-Core Version:    0.6.2
 */