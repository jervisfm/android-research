package com.fiksu.asotracking;

import android.content.Context;

class ConversionEventTracker extends EventTracker
{
  ConversionEventTracker(Context paramContext, String paramString)
  {
    super(paramContext, "Conversion");
    addParameter("tvalue", paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.ConversionEventTracker
 * JD-Core Version:    0.6.2
 */