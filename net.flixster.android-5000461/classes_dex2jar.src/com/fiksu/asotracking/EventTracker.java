package com.fiksu.asotracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class EventTracker
{
  static final String SHARED_PREFERENCES_LOCK = "shared preferences lock";
  private static Context mCachedContext = null;
  protected Context mContext = null;
  private final HashMap<String, String> mParameters = new HashMap();

  public EventTracker(Context paramContext, String paramString)
  {
    this.mParameters.put("event", paramString);
    if (paramContext != null)
    {
      mCachedContext = paramContext;
      this.mContext = paramContext;
      return;
    }
    this.mContext = mCachedContext;
  }

  static void c2dMessageReceived(Context paramContext)
  {
    new Thread(new C2DMessageTimeSaver(paramContext)).start();
  }

  private HashMap<String, String> copyOfParams()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = this.mParameters.keySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return localHashMap;
      String str = (String)localIterator.next();
      localHashMap.put(str, (String)this.mParameters.get(str));
    }
  }

  static SharedPreferences getOurSharedPreferences(Context paramContext)
  {
    if (paramContext == null)
      return null;
    return paramContext.getSharedPreferences("FiksuSharedPreferences", 0);
  }

  protected void addParameter(String paramString1, String paramString2)
  {
    this.mParameters.put(paramString1, paramString2);
  }

  protected void uploadEvent()
  {
    new Thread(new EventUploader(this.mContext, copyOfParams())).start();
  }

  protected void uploadEventSynchronously(long paramLong)
  {
    synchronized (new EventUploader(this.mContext, copyOfParams()))
    {
      new Thread(???).start();
    }
    try
    {
      ???.wait(paramLong);
      label34: return;
      localObject = finally;
      throw localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      break label34;
    }
  }

  private static final class C2DMessageTimeSaver
    implements Runnable
  {
    private final Context mContext;

    C2DMessageTimeSaver(Context paramContext)
    {
      this.mContext = paramContext;
    }

    public void run()
    {
      try
      {
        SharedPreferences localSharedPreferences = EventTracker.getOurSharedPreferences(this.mContext);
        Date localDate = new Date();
        SharedPreferences.Editor localEditor = localSharedPreferences.edit();
        localEditor.putLong("Fiksu.cd2MessageTime", localDate.getTime());
        localEditor.commit();
        return;
      }
      finally
      {
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.EventTracker
 * JD-Core Version:    0.6.2
 */