package com.fiksu.asotracking;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

class EventUploader
  implements Runnable
{
  private static final String FIKSU_SEPARATOR = "<FIKSU>";
  private static final int MAX_FAILED_URLS = 10;
  private final Context mContext;
  private final Map<String, String> mParameters;

  EventUploader(Context paramContext, Map<String, String> paramMap)
  {
    this.mParameters = paramMap;
    this.mContext = paramContext;
  }

  private String buildURL()
  {
    String str10;
    if (this.mContext == null)
    {
      Log.e("FiksuTracking", "Could not find context to use.  Please set it in your main Activity class using EventTracking.setContext().");
      str10 = null;
    }
    while (true)
    {
      return str10;
      String str1 = (String)this.mParameters.get("event");
      Log.d("FiksuTracking", "Event: " + str1);
      String str2 = "https://" + "asotrack1.fluentmobile.com/";
      if (((str1.equals("Launch")) || (str1.equals("Resume"))) && (launchedFromNotification()))
      {
        str2 = "https://" + "asotrack2.fluentmobile.com/";
        str1 = "Notification" + str1;
      }
      String str3 = this.mContext.getPackageName();
      String str4 = str2 + "$Rev: 28663 $".split(" ")[1] + "/android/" + str3 + "/event?";
      try
      {
        String str5 = str4 + "appid=" + this.mContext.getPackageName();
        String str6 = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
        if (str6 == null)
        {
          Log.e("FiksuTracking", "Could not retrieve android_id.  The android_id is not available on emulators running Android 2.1 or below.  Run the code on emulator 2.2 or better or an a device.");
          str6 = "";
        }
        try
        {
          localTelephonyManager = (TelephonyManager)this.mContext.getSystemService("phone");
          if (localTelephonyManager == null)
          {
            Log.e("FiksuTracking", "Could not access telephonyManager.");
            str7 = "";
            String str8 = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(str5)).append("&deviceid=").append(str7).toString())).append("&udid=").append(str6).toString() + "&device=" + encodeParameter(Build.MODEL);
            localObject = str8;
          }
        }
        catch (SecurityException localSecurityException)
        {
          try
          {
            while (true)
            {
              TelephonyManager localTelephonyManager;
              PackageManager localPackageManager = this.mContext.getPackageManager();
              Object localObject = localObject + "&app_version=" + encodeParameter(localPackageManager.getPackageInfo(str3, 0).versionName);
              String str12 = localPackageManager.getApplicationInfo(str3, 0).loadLabel(localPackageManager).toString();
              if (str12 != null)
              {
                String str13 = localObject + "&app_name=" + encodeParameter(str12);
                localObject = str13;
              }
              String str9 = new StringBuilder(String.valueOf(localObject)).append("&system_version=").append(Build.VERSION.RELEASE).toString() + "&system_name=" + encodeParameter(Build.PRODUCT);
              Locale localLocale = this.mContext.getResources().getConfiguration().locale;
              str10 = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(str9)).append("&country=").append(encodeParameter(localLocale.getCountry())).toString())).append("&lang=").append(encodeParameter(localLocale.getLanguage())).toString())).append("&timezone=").append(encodeParameter(TimeZone.getDefault().getDisplayName())).toString() + "&gmtoffset=" + TimeZone.getDefault().getRawOffset() / 1000;
              if (str1 != null)
                str10 = str10 + "&event=" + str1;
              if (this.mParameters.get("username") != null)
                str10 = str10 + "&username=" + encodeParameter((String)this.mParameters.get("username"));
              if (this.mParameters.get("tvalue") != null)
                str10 = str10 + "&tvalue=" + encodeParameter((String)this.mParameters.get("tvalue"));
              if (this.mParameters.get("fvalue") != null)
                str10 = str10 + "&fvalue=" + encodeParameter((String)this.mParameters.get("fvalue"));
              if (this.mParameters.get("ivalue") == null)
                break;
              String str11 = str10 + "&ivalue=" + (String)this.mParameters.get("ivalue");
              return str11;
              str7 = localTelephonyManager.getDeviceId();
              if ((str7 == null) || (str7.length() == 0))
              {
                Log.e("FiksuTracking", "Could not retrieve deviceId. ");
                str7 = "";
              }
            }
            localSecurityException = localSecurityException;
            Log.e("FiksuTracking", "READ_PHONE_STATE permission not granted. Could not retrieve deviceId. ");
            String str7 = "";
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException)
          {
            while (true)
              Log.e("FiksuTracking", "Could not access package: " + str3);
          }
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        Log.e("FiksuTracking", "Problem creating URL", localUnsupportedEncodingException);
      }
    }
    return null;
  }

  private boolean doUpload(String paramString)
    throws MalformedURLException
  {
    URL localURL = new URL(paramString);
    int i;
    do
    {
      try
      {
        i = ((HttpURLConnection)localURL.openConnection()).getResponseCode();
        if (i == 200)
        {
          Log.d("FiksuTracking", "Successfully uploaded tracking information.");
          return true;
        }
      }
      catch (IOException localIOException)
      {
        Log.e("FiksuTracking", "Failed to upload tracking information.");
        return false;
      }
      Log.e("FiksuTracking", "Failed to upload tracking information, bad response: " + i);
    }
    while ((i < 500) || (i > 599));
    return false;
  }

  private String encodeParameter(String paramString)
    throws UnsupportedEncodingException
  {
    return URLEncoder.encode(paramString, "UTF-8");
  }

  private List<String> getSavedUrls()
  {
    ArrayList localArrayList = new ArrayList();
    SharedPreferences localSharedPreferences = EventTracker.getOurSharedPreferences(this.mContext);
    String[] arrayOfString;
    int i;
    if (localSharedPreferences != null)
    {
      String str = localSharedPreferences.getString("Fiksu.savedUrls", "");
      if ((str != null) && (!str.equals("")))
      {
        arrayOfString = str.split("<FIKSU>");
        i = arrayOfString.length;
      }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return localArrayList;
      localArrayList.add(arrayOfString[j]);
    }
  }

  private boolean launchedFromNotification()
  {
    try
    {
      SharedPreferences localSharedPreferences = EventTracker.getOurSharedPreferences(this.mContext);
      return new Date().getTime() - localSharedPreferences.getLong("Fiksu.cd2MessageTime", 0L) < 180000L;
    }
    finally
    {
    }
  }

  private void saveFailedUrls(List<String> paramList)
  {
    if (paramList.size() > 10)
      paramList = new ArrayList(paramList.subList(-10 + paramList.size(), paramList.size()));
    String str = "";
    if (paramList.size() > 0)
      str = str + (String)paramList.get(0);
    for (int i = 1; ; i++)
    {
      if (i >= paramList.size())
      {
        SharedPreferences.Editor localEditor = EventTracker.getOurSharedPreferences(this.mContext).edit();
        localEditor.putString("Fiksu.savedUrls", str);
        localEditor.commit();
        return;
      }
      str = str + "<FIKSU>" + (String)paramList.get(i);
    }
  }

  private void uploadToTracking()
  {
    if (this.mContext == null)
    {
      Log.e("FiksuTracking", "Could not find context to use.  Please set it in your main Activity class using EventTracking.setContext().");
      return;
    }
    String str1 = buildURL();
    while (true)
    {
      ArrayList localArrayList;
      Iterator localIterator;
      try
      {
        List localList = getSavedUrls();
        if (str1 != null)
        {
          localList.add(str1);
          if (((String)this.mParameters.get("event")).equals("Conversion"))
            saveFailedUrls(localList);
        }
        localArrayList = new ArrayList();
        localIterator = localList.iterator();
        if (!localIterator.hasNext())
        {
          saveFailedUrls(localArrayList);
          return;
        }
      }
      finally
      {
      }
      String str2 = (String)localIterator.next();
      try
      {
        if (!doUpload(str2))
        {
          Log.e("FiksuTracking", "Upload failed for url.  Saving it for retry later: " + str2);
          localArrayList.add(str2);
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        Log.e("FiksuTracking", str2);
        Log.e("FiksuTracking", localMalformedURLException.toString());
      }
    }
  }

  // ERROR //
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 412	com/fiksu/asotracking/EventUploader:uploadToTracking	()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: invokevirtual 415	java/lang/Object:notifyAll	()V
    //   10: aload_0
    //   11: monitorexit
    //   12: return
    //   13: astore_1
    //   14: aload_0
    //   15: monitorenter
    //   16: aload_0
    //   17: invokevirtual 415	java/lang/Object:notifyAll	()V
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_1
    //   23: athrow
    //   24: astore_2
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_2
    //   28: athrow
    //   29: astore_3
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_3
    //   33: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   0	4	13	finally
    //   16	22	24	finally
    //   25	27	24	finally
    //   6	12	29	finally
    //   30	32	29	finally
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.EventUploader
 * JD-Core Version:    0.6.2
 */