package com.fiksu.asotracking;

public class FiksuIntegrationError extends RuntimeException
{
  private static final long serialVersionUID = 2468412625444291679L;

  FiksuIntegrationError(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.FiksuIntegrationError
 * JD-Core Version:    0.6.2
 */