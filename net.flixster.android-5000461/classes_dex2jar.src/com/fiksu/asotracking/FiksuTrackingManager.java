package com.fiksu.asotracking;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

public class FiksuTrackingManager
{
  static final String FIKSU_LOG_TAG = "FiksuTracking";

  public static void c2dMessageReceived(Context paramContext)
  {
    EventTracker.c2dMessageReceived(paramContext);
  }

  public static void initialize(Application paramApplication)
  {
    new ForegroundTester(paramApplication, new LaunchEventTracker(paramApplication));
    InstallTracking.checkForFiksuReceiver(paramApplication);
  }

  public static void promptForRating(Activity paramActivity)
  {
    new RatingPrompter(paramActivity).maybeShowPrompt();
  }

  public static void uploadPurchaseEvent(Context paramContext, String paramString1, double paramDouble, String paramString2)
  {
    new PurchaseEventTracker(paramContext, paramString1, Double.valueOf(paramDouble), paramString2).uploadEvent();
  }

  public static void uploadRegistrationEvent(Context paramContext, String paramString)
  {
    new RegistrationEventTracker(paramContext, paramString).uploadEvent();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.FiksuTrackingManager
 * JD-Core Version:    0.6.2
 */