package com.fiksu.asotracking;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.os.Process;
import android.util.Log;
import java.util.Iterator;
import java.util.List;

class ForegroundTester
  implements Runnable
{
  private static boolean sStarted = false;
  private final Application mApplication;
  private final LaunchEventTracker mLaunchEventTracker;
  private boolean mPostedLaunch = false;
  private boolean mWasInForeground = false;

  ForegroundTester(Application paramApplication, LaunchEventTracker paramLaunchEventTracker)
  {
    this.mApplication = paramApplication;
    this.mLaunchEventTracker = paramLaunchEventTracker;
    try
    {
      if (sStarted)
      {
        Log.e("FiksuTracking", "Already initialized!. Only call FiksuTrackingManager.initialize() once.");
        return;
      }
      sStarted = true;
      new Thread(this).start();
      return;
    }
    finally
    {
    }
  }

  private boolean inForeground()
  {
    List localList = ((ActivityManager)this.mApplication.getSystemService("activity")).getRunningAppProcesses();
    if (localList == null);
    ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo;
    do
    {
      Iterator localIterator;
      while (!localIterator.hasNext())
      {
        return false;
        localIterator = localList.iterator();
      }
      localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)localIterator.next();
    }
    while ((localRunningAppProcessInfo == null) || (localRunningAppProcessInfo.importance != 100) || (!this.mApplication.getPackageName().equals(localRunningAppProcessInfo.processName)));
    return true;
  }

  protected void postEvent()
  {
    if (!this.mPostedLaunch)
    {
      this.mPostedLaunch = true;
      this.mLaunchEventTracker.uploadEvent();
      return;
    }
    new ResumeEventTracker(this.mApplication).uploadEvent();
  }

  public void run()
  {
    while (true)
    {
      try
      {
        Log.d("FiksuTracking", "ForegroundTester thread started, process: " + Process.myPid());
        Thread.sleep(6000L);
        Thread.sleep(5000L);
        if ((!this.mWasInForeground) && (inForeground()))
        {
          postEvent();
          this.mWasInForeground = true;
          continue;
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        Log.i("FiksuTracking", "Sleep interrupted");
        return;
      }
      if ((this.mWasInForeground) && (!inForeground()))
        this.mWasInForeground = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.ForegroundTester
 * JD-Core Version:    0.6.2
 */