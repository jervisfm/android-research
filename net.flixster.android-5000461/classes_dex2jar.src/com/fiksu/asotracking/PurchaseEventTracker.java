package com.fiksu.asotracking;

import android.content.Context;

class PurchaseEventTracker extends EventTracker
{
  PurchaseEventTracker(Context paramContext, String paramString1, Double paramDouble, String paramString2)
  {
    super(paramContext, "Purchase");
    addParameter("username", paramString1);
    addParameter("fvalue", paramDouble.toString());
    addParameter("tvalue", paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.PurchaseEventTracker
 * JD-Core Version:    0.6.2
 */