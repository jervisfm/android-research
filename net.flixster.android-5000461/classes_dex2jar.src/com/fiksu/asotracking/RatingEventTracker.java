package com.fiksu.asotracking;

import android.content.Context;

class RatingEventTracker extends EventTracker
{
  RatingEventTracker(Context paramContext, String paramString, int paramInt)
  {
    super(paramContext, "Rating");
    addParameter("tvalue", paramString);
    addParameter("ivalue", new Integer(paramInt).toString());
  }

  public void uploadEvent()
  {
    super.uploadEvent();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.RatingEventTracker
 * JD-Core Version:    0.6.2
 */