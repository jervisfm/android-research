package com.fiksu.asotracking;

import android.content.Context;

class RegistrationEventTracker extends EventTracker
{
  RegistrationEventTracker(Context paramContext, String paramString)
  {
    super(paramContext, "Registration");
    addParameter("username", paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.fiksu.asotracking.RegistrationEventTracker
 * JD-Core Version:    0.6.2
 */