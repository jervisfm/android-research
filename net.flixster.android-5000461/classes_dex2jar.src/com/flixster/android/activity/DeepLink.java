package com.flixster.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.TabHost;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.msk.MskController;
import com.flixster.android.msk.MskController.Step;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.UrlHelper;
import net.flixster.android.Flixster;
import net.flixster.android.Starter;
import net.flixster.android.model.User;

public class DeepLink
{
  private static final String DEEP_LINK_ACTOR_QUERY = "actor";
  private static final String DEEP_LINK_AUTO_LOGIN_PAGE = "loginFlixsterUser";
  private static final String DEEP_LINK_EXTRA_MOVIES = "extramovies";
  private static final String DEEP_LINK_FLX_AUTH_TOKEN_QUERY = "auth_token";
  private static final String DEEP_LINK_FLX_AUTH_USER_QUERY = "auth_user";
  public static final String DEEP_LINK_LASP_PAGE = "lasp";
  private static final String DEEP_LINK_MOVIE_DETAIL_PAGE = "detail";
  private static final String DEEP_LINK_MOVIE_QUERY = "movie";
  private static final String DEEP_LINK_MSK_ACTION_QUERY = "action";
  private static final String DEEP_LINK_MSK_FB_LOGIN_PAGE = "fbLogin";
  private static final String DEEP_LINK_MSK_FB_REQUEST_PAGE = "fbRequest";
  private static final String DEEP_LINK_MSK_LOGIN_REQUIRED_QUERY = "loginRequired";
  public static final String DEEP_LINK_MSK_PAGE = "msk";
  private static final String DEEP_LINK_MSK_PROPERTY_PAGE = "property";
  private static final String DEEP_LINK_MSK_TEXT_REQUEST_PAGE = "textRequest";
  private static final String DEEP_LINK_MSK_TYPE_QUERY = "type";
  private static final String DEEP_LINK_PAGE_QUERY = "page";
  private static final String DEEP_LINK_PHOTOS_PAGE = "photos";
  private static final String DEEP_LINK_PRODUCT_PAGE = "product";
  private static final String DEEP_LINK_TRAILER_PAGE = "trailer";
  private static final String DEEP_LINK_URL = "http://www.flixster.com/mobile/deep-link";
  private static final String DEEP_LINK_URL_QUERY = "url";
  private static final String DEEP_LINK_USERNAME_QUERY = "username";
  private static final String DEEP_LINK_VALUE_MSK_LOGIN_FB = "fb";
  private static final String DEEP_LINK_VALUE_MSK_LOGIN_FLX = "flx";
  private static final String DEEP_LINK_VALUE_MSK_TYPE_PICK = "pick";
  private static final String FLIXSTER_SCHEME = "flixster";
  private static final String HOST_MOVIE = "movie";
  private static final String HOST_MSK = "msk";
  private static final String HOST_MYMOVIES = "mymovies";
  private static final String HOST_SHOWTIMES = "showtimes";
  private static final String HOST_TRAILER = "trailer";
  private static final String QUERY_ID = "id";
  private static final String QUERY_REASON = "reason";
  private static final String QUERY_URL = "url";
  private static final String VALUE_MSK = "mskFinished";

  public static void checkMskFinished(Uri paramUri)
  {
    if (paramUri != null)
    {
      Uri localUri = convertDeepLinkToAndroid(paramUri);
      if (("flixster".equals(localUri.getScheme())) && ("mymovies".equals(localUri.getHost())) && ("mskFinished".equalsIgnoreCase(localUri.getQueryParameter("reason"))))
      {
        Properties.instance().setMskFinishedDeepLinkInitiated();
        Logger.i("FlxMain", "DeepLink.checkMskFinished true");
      }
    }
  }

  public static void clearDeepLinkParams()
  {
    ObjectHolder.instance().remove("lasp");
  }

  private static Uri convertDeepLinkToAndroid(Uri paramUri)
  {
    Uri localUri = paramUri;
    String str1 = paramUri.getQueryParameter("action");
    if ("movieDetails".equals(str1))
    {
      String str7 = paramUri.getQueryParameter("movieId");
      if (str7 != null)
        localUri = Uri.parse("flixster://movie?id=" + str7);
    }
    do
    {
      String str5;
      do
      {
        String str6;
        do
        {
          return localUri;
          if (!"movieShowtimes".equals(str1))
            break;
          str6 = paramUri.getQueryParameter("movieId");
        }
        while (str6 == null);
        return Uri.parse("flixster://showtimes?id=" + str6);
        if (!"playTrailer".equals(str1))
          break;
        String str4 = paramUri.getQueryParameter("highUrl");
        if (str4 == null)
          str4 = paramUri.getQueryParameter("lowUrl");
        if (str4 != null)
          return Uri.parse("flixster://trailer?url=" + str4);
        str5 = paramUri.getQueryParameter("movieId");
      }
      while (str5 == null);
      return Uri.parse("flixster://trailer?id=" + str5);
      if ("myMovies".equals(str1))
      {
        String str2 = paramUri.getQueryParameter("reason");
        StringBuilder localStringBuilder = new StringBuilder("flixster://mymovies");
        if (str2 != null);
        for (String str3 = "?reason=" + str2; ; str3 = "")
          return Uri.parse(str3);
      }
    }
    while (!"msk".equals(str1));
    return Uri.parse("flixster://msk");
  }

  public static MskController.Step convertToMskNativeStep(String paramString)
  {
    boolean bool1 = paramString.startsWith("http://www.flixster.com/mobile/deep-link");
    MskController.Step localStep = null;
    String str;
    if (bool1)
    {
      str = UrlHelper.getSingleQueryValue(paramString, "page");
      if (!"fbLogin".equals(str))
        break label35;
      localStep = MskController.Step.NATIVE_FB_LOGIN;
    }
    label35: boolean bool2;
    do
    {
      return localStep;
      if ("fbRequest".equals(str))
        return MskController.Step.NATIVE_FB_REQUEST;
      if ("textRequest".equals(str))
        return MskController.Step.NATIVE_TEXT_REQUEST;
      bool2 = "lasp".equals(str);
      localStep = null;
    }
    while (!bool2);
    return MskController.Step.NATIVE_LASP;
  }

  public static String convertToPropertyName(String paramString)
  {
    boolean bool1 = paramString.startsWith("http://www.flixster.com/mobile/deep-link");
    String str = null;
    if (bool1)
    {
      boolean bool2 = "property".equals(UrlHelper.getSingleQueryValue(paramString, "page"));
      str = null;
      if (bool2)
        str = UrlHelper.getSingleQueryValue(paramString, "action");
    }
    return str;
  }

  public static String getExtraMovies(String paramString)
  {
    boolean bool = paramString.startsWith("http://www.flixster.com/mobile/deep-link");
    String str = null;
    if (bool)
      str = UrlHelper.getSingleQueryValue(paramString, "extramovies");
    return str;
  }

  public static long getMovieId(String paramString)
  {
    long l = 0L;
    if (paramString.startsWith("http://www.flixster.com/mobile/deep-link"))
    {
      String str = UrlHelper.getSingleQueryValue(paramString, "movie");
      if (str != null)
        l = Long.valueOf(str).longValue();
    }
    return l;
  }

  public static boolean isFacebookLoginRequired(String paramString)
  {
    return "fb".equals(UrlHelper.getSingleQueryValue(paramString, "loginRequired"));
  }

  public static boolean isFlixsterDeepLink(Uri paramUri)
  {
    if ((paramUri != null) && ("flixster".equals(paramUri.getScheme())));
    for (boolean bool = true; ; bool = false)
    {
      Logger.i("FlxMain", "DeepLink.isFlixsterDeepLink uri: " + paramUri + " " + bool);
      return bool;
    }
  }

  public static boolean isFlixsterLoginRequired(String paramString)
  {
    return "flx".equals(UrlHelper.getSingleQueryValue(paramString, "loginRequired"));
  }

  public static boolean isMskAdUrl(String paramString)
  {
    return "msk".equals(UrlHelper.getSingleQueryValue(paramString, "page"));
  }

  public static boolean isMskRequestTypePick(String paramString)
  {
    String str = UrlHelper.getSingleQueryValue(paramString, "type");
    Logger.i("FlxMain", "DeepLink.isRequestTypePick " + paramString + " " + str);
    return "pick".equals(str);
  }

  public static boolean isValid(String paramString)
  {
    boolean bool = paramString.startsWith("http://www.flixster.com/mobile/deep-link");
    Logger.d("FlxMain", "DeepLink " + bool + ": " + paramString);
    return bool;
  }

  public static boolean launch(String paramString, Context paramContext)
  {
    boolean bool = true;
    String str1;
    if (paramString.startsWith("http://www.flixster.com/mobile/deep-link"))
    {
      str1 = UrlHelper.getSingleQueryValue(paramString, "page");
      if (str1 == null)
        break label521;
      if (!"trailer".equals(str1))
        break label65;
      String str11 = UrlHelper.getSingleQueryValue(paramString, "url");
      if (str11 == null)
        break label54;
      Starter.launchAdFreeTrailer(str11, paramContext);
    }
    label521: 
    while (true)
    {
      bool = false;
      label54: label65: String str2;
      label293: String str3;
      do
      {
        do
        {
          do
          {
            return bool;
            Logger.e("FlxMain", "DeepLink error: null trailer URL");
            break;
            if ("detail".equals(str1))
            {
              String str10 = UrlHelper.getSingleQueryValue(paramString, "movie");
              if (str10 != null)
              {
                Starter.launchMovieDetail(Long.valueOf(str10).longValue(), paramContext);
                break;
              }
              Logger.e("FlxMain", "DeepLink error: null movie id");
              break;
            }
            if ("photos".equals(str1))
            {
              String str7 = UrlHelper.getSingleQueryValue(paramString, "movie");
              if (str7 != null)
              {
                Starter.launchMovieGallery(Long.valueOf(str7).longValue(), paramContext);
                break;
              }
              String str8 = UrlHelper.getSingleQueryValue(paramString, "actor");
              if (str8 != null)
              {
                Starter.launchActorGallery(Long.valueOf(str8).longValue(), paramContext);
                break;
              }
              String str9 = UrlHelper.getSingleQueryValue(paramString, "username");
              if (str9 != null)
              {
                Starter.launchUserGallery(str9, paramContext);
                break;
              }
              Logger.e("FlxMain", "DeepLink error: null photos id");
              break;
            }
            if ("product".equals(str1))
            {
              String str6 = UrlHelper.getSingleQueryValue(paramString, "url");
              if (str6 != null)
              {
                Starter.launchBrowser(str6, paramContext);
                break;
              }
              Logger.e("FlxMain", "DeepLink error: null product URL");
              break;
            }
            if (!"property".equals(str1))
              break label293;
          }
          while (!(paramContext instanceof Activity));
          ((Activity)paramContext).setResult(-1, new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
          return bool;
          if ("lasp".equals(str1))
          {
            Trackers.instance().trackEvent("/MSKRedemption", "MSK Redemption", "MSKRedemption", "SuccessfulRedemption");
            if (!AccountManager.instance().hasUserSession())
            {
              String str4 = UrlHelper.getSingleQueryValue(paramString, "auth_user");
              String str5 = UrlHelper.getSingleQueryValue(paramString, "auth_token");
              if ((str4 != null) && (str5 != null))
              {
                AccountManager.instance().createNewUserWithFlxCredential(str4, str5);
                User localUser = AccountManager.instance().getUser();
                localUser.collectionsCount = (1 + localUser.collectionsCount);
                AccountManager.instance().getUser().isMskEligible = false;
              }
            }
            MskController.instance().finishLasp(paramContext);
            return bool;
          }
          if (!"loginFlixsterUser".equals(str1))
            break label462;
        }
        while (AccountManager.instance().hasUserSession());
        str2 = UrlHelper.getSingleQueryValue(paramString, "auth_user");
        str3 = UrlHelper.getSingleQueryValue(paramString, "auth_token");
      }
      while ((str2 == null) || (str3 == null));
      AccountManager.instance().createNewUserWithFlxCredential(str2, str3);
      return bool;
      label462: if ("msk".equals(str1))
      {
        ObjectHolder.instance().put("lasp", Boolean.valueOf(bool));
        Starter.launchMsk(paramContext);
      }
      else
      {
        Logger.e("FlxMain", "DeepLink error: unknown page type '" + str1 + "'");
        continue;
        Logger.e("FlxMain", "DeepLink error: null page query");
      }
    }
  }

  public static void launchFlixsterDeepLink(Uri paramUri, Context paramContext)
  {
    Uri localUri = convertDeepLinkToAndroid(paramUri);
    String str1 = localUri.getHost();
    if ("movie".equals(str1))
    {
      String str6 = localUri.getQueryParameter("id");
      if (str6 != null)
        Starter.launchMovieDetail(Long.valueOf(str6).longValue(), paramContext);
    }
    label168: 
    do
    {
      do
      {
        String str3;
        do
        {
          String str4;
          do
          {
            String str5;
            do
            {
              return;
              if (!"showtimes".equals(str1))
                break;
              str5 = localUri.getQueryParameter("id");
            }
            while (str5 == null);
            Starter.launchMovieShowtimes(Long.valueOf(str5).longValue(), paramContext);
            return;
            if (!"trailer".equals(str1))
              break label168;
            String str2 = localUri.toString();
            int i = str2.indexOf("url");
            if (i <= -1)
              break;
            str4 = str2.substring(1 + (i + "url".length()));
          }
          while (str4 == null);
          Starter.launchAdFreeTrailer(UrlHelper.urlDecode(str4), paramContext);
          return;
          str3 = localUri.getQueryParameter("id");
        }
        while (str3 == null);
        Starter.launchAdFreeTrailer(Long.valueOf(str3).longValue(), paramContext);
        return;
        if (!"mymovies".equals(str1))
          break;
        "mskFinished".equalsIgnoreCase(localUri.getQueryParameter("reason"));
      }
      while (!(paramContext instanceof Flixster));
      ((Flixster)paramContext).getTabHost().setCurrentTab(2);
      return;
    }
    while (!"msk".equals(str1));
    ObjectHolder.instance().put("lasp", Boolean.valueOf(true));
    Starter.launchMsk(paramContext);
  }

  public static String removeLoginRequired(String paramString)
  {
    return paramString.replace("loginRequired", "ignore");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.DeepLink
 * JD-Core Version:    0.6.2
 */