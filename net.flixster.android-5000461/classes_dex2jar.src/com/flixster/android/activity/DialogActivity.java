package com.flixster.android.activity;

import android.app.Dialog;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogEvents;
import com.flixster.android.view.DialogBuilder.DialogListener;

public class DialogActivity extends TopLevelActivity
{
  private Dialog lastDialog;
  private int lastDialogId;

  protected Dialog onCreateDialog(int paramInt)
  {
    this.lastDialogId = paramInt;
    this.lastDialog = DialogBuilder.createDialog(this, paramInt);
    if (this.lastDialog == null)
      this.lastDialog = super.onCreateDialog(paramInt);
    return this.lastDialog;
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    DialogBuilder.DialogEvents.instance().removeListeners();
    if (paramDialogListener != null)
      DialogBuilder.DialogEvents.instance().addListener(paramDialogListener);
    if (this.lastDialog != null)
    {
      removeDialog(this.lastDialogId);
      this.lastDialog = null;
    }
    showDialog(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.DialogActivity
 * JD-Core Version:    0.6.2
 */