package com.flixster.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.activity.decorator.TimerDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.storage.ExternalStorage;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.EpisodeView;
import java.util.Collection;
import java.util.Iterator;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Episode;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Season;
import net.flixster.android.model.User;

public class EpisodesPage extends DecoratedSherlockActivity
{
  public static final String KEY_RIGHT_ID = "KEY_RIGHT_ID";
  public static final String KEY_SEASON_ID = "KEY_SEASON_ID";
  private final Handler canDownloadSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      LockerRight localLockerRight = (LockerRight)paramAnonymousMessage.obj;
      if (localLockerRight.getDownloadsRemain() > 0)
      {
        DownloadHelper.downloadMovie(localLockerRight);
        EpisodesPage.this.scheduleSingleRefresh(localLockerRight.assetId);
        return;
      }
      EpisodesPage.this.showDialog(1000000204, null);
    }
  };
  private LinearLayout contentLayout;
  private final DialogBuilder.DialogListener downloadCancelDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000201));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000201));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      LockerRight localLockerRight = (LockerRight)ObjectHolder.instance().remove(String.valueOf(1000000201));
      DownloadHelper.cancelMovieDownload(localLockerRight.rightId);
      EpisodesPage.this.scheduleSingleRefresh(localLockerRight.assetId);
      Trackers.instance().trackEvent("/download", "Download", "Download", "Cancel");
    }
  };
  private final View.OnClickListener downloadClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LockerRight localLockerRight = (LockerRight)paramAnonymousView.getTag();
      if ((!DownloadHelper.isMovieDownloadInProgress(localLockerRight.rightId)) && (!DownloadHelper.isDownloaded(localLockerRight.rightId)))
      {
        if (!ExternalStorage.isWriteable())
          break label85;
        if (DownloadHelper.findRemainingSpace() > localLockerRight.getDownloadAssetSizeRaw())
        {
          ObjectHolder.instance().put(String.valueOf(1000000200), localLockerRight);
          EpisodesPage.this.showDialog(1000000200, EpisodesPage.this.downloadConfirmDialogListener);
        }
      }
      else
      {
        return;
      }
      EpisodesPage.this.showDialog(1000000205, null);
      return;
      label85: EpisodesPage.this.showDialog(1000000203, null);
    }
  };
  private final DialogBuilder.DialogListener downloadConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      LockerRight localLockerRight = (LockerRight)ObjectHolder.instance().remove(String.valueOf(1000000200));
      Trackers.instance().trackEvent("/download", "Download", "Download", "Confirm");
      ProfileDao.canDownload(EpisodesPage.this.canDownloadSuccessHandler, EpisodesPage.this.errorHandler, localLockerRight);
    }
  };
  private final View.OnClickListener downloadDeleteClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LockerRight localLockerRight = (LockerRight)paramAnonymousView.getTag();
      if (DownloadHelper.isMovieDownloadInProgress(localLockerRight.rightId))
      {
        ObjectHolder.instance().put(String.valueOf(1000000201), localLockerRight);
        EpisodesPage.this.showDialog(1000000201, EpisodesPage.this.downloadCancelDialogListener);
      }
      while (!DownloadHelper.isDownloaded(localLockerRight.rightId))
        return;
      ObjectHolder.instance().put(String.valueOf(1000000202), localLockerRight);
      EpisodesPage.this.showDialog(1000000202, EpisodesPage.this.downloadDeleteDialogListener);
    }
  };
  private final DialogBuilder.DialogListener downloadDeleteDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      LockerRight localLockerRight = (LockerRight)ObjectHolder.instance().remove(String.valueOf(1000000202));
      DownloadHelper.deleteDownloadedMovie(localLockerRight);
      EpisodesPage.this.scheduleSingleRefresh(localLockerRight.assetId);
      Trackers.instance().trackEvent("/download", "Download", "Download", "Delete");
    }
  };
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      EpisodesPage.this.throbber.setVisibility(8);
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, EpisodesPage.this);
    }
  };
  private TextView offlineAlert;
  private final Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (EpisodesPage.this.isFinishing());
      while (true)
      {
        return;
        long l = ((Long)paramAnonymousMessage.obj).longValue();
        for (int i = 0; i < EpisodesPage.this.contentLayout.getChildCount(); i++)
          ((EpisodeView)EpisodesPage.this.contentLayout.getChildAt(i)).refresh(l);
      }
    }
  };
  private Season season;
  private long seasonId;
  private LockerRight seasonRight;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      EpisodesPage.this.throbber.setVisibility(8);
      EpisodesPage.this.season = ((Season)paramAnonymousMessage.obj);
      if (EpisodesPage.this.seasonRight != null)
      {
        EpisodesPage.this.trackPage();
        EpisodesPage.this.initializePage();
      }
    }
  };
  private ProgressBar throbber;
  private TimerDecorator timerDecorator;
  private final View.OnClickListener watchNowClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LockerRight localLockerRight = (LockerRight)paramAnonymousView.getTag();
      if ((!DownloadHelper.isMovieDownloadInProgress(localLockerRight.rightId)) && (DownloadHelper.isDownloaded(localLockerRight.rightId)));
      for (int i = 1; (i == 0) && (DownloadHelper.isMovieDownloadInProgress()); i = 0)
      {
        ObjectHolder.instance().put(String.valueOf(1000000300), localLockerRight);
        EpisodesPage.this.showDialog(1000000300, EpisodesPage.this.watchNowConfirmDialogListener);
        return;
      }
      Drm.manager().playMovie(localLockerRight);
    }
  };
  private final DialogBuilder.DialogListener watchNowConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000300));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000300));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      LockerRight localLockerRight = (LockerRight)ObjectHolder.instance().remove(String.valueOf(1000000300));
      Drm.manager().playMovie(localLockerRight);
    }
  };

  private void initializePage()
  {
    Iterator localIterator;
    if (this.season.getEpisodes() != null)
      localIterator = this.season.getEpisodes().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Episode localEpisode = (Episode)localIterator.next();
      EpisodeView localEpisodeView = new EpisodeView(this);
      localEpisodeView.load(this.seasonRight, localEpisode, this.watchNowClickListener, this.downloadClickListener, this.downloadDeleteClickListener);
      this.contentLayout.addView(localEpisodeView);
    }
  }

  private void scheduleSingleRefresh(long paramLong)
  {
    this.timerDecorator.scheduleTask(new RefreshTimerTask(paramLong, null), 2000L);
  }

  private void trackPage()
  {
    Trackers.instance().track("/episodes/info", "Episodes Info - " + this.season.getTitle());
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    TimerDecorator localTimerDecorator = new TimerDecorator(this);
    this.timerDecorator = localTimerDecorator;
    addDecorator(localTimerDecorator);
    this.timerDecorator.onCreate();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.seasonId = localBundle.getLong("KEY_SEASON_ID");
      long l = localBundle.getLong("KEY_RIGHT_ID");
      User localUser = AccountManager.instance().getUser();
      if ((localUser != null) && (l > 0L))
        this.seasonRight = localUser.getLockerRightFromRightId(l);
    }
    setContentView(2130903078);
    createActionBar();
    setActionBarTitle(2131493286);
    this.contentLayout = ((LinearLayout)findViewById(2131165301));
    this.offlineAlert = ((TextView)findViewById(2131165300));
    this.throbber = ((ProgressBar)findViewById(2131165241));
    if (this.seasonId != 0L)
    {
      this.throbber.setVisibility(0);
      MovieDao.getUserSeasonDetail(this.seasonId, this.successHandler, this.errorHandler);
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    TextView localTextView = this.offlineAlert;
    if (FlixsterApplication.isConnected());
    for (int i = 8; ; i = 0)
    {
      localTextView.setVisibility(i);
      return;
    }
  }

  private class RefreshTimerTask extends TimerTask
  {
    private final long assetId;

    private RefreshTimerTask(long arg2)
    {
      Object localObject;
      this.assetId = localObject;
    }

    public void run()
    {
      EpisodesPage.this.refreshHandler.sendMessage(Message.obtain(null, 0, Long.valueOf(this.assetId)));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.EpisodesPage
 * JD-Core Version:    0.6.2
 */