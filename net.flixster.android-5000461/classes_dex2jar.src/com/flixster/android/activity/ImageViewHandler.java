package com.flixster.android.activity;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.flixster.android.view.ViewTag;
import java.lang.ref.SoftReference;

public class ImageViewHandler extends Handler
{
  private final String imageUrl;
  private final ImageView imageView;
  private final ImageView.ScaleType scaleType;

  public ImageViewHandler(ImageView paramImageView, String paramString)
  {
    this(paramImageView, paramString, null);
  }

  public ImageViewHandler(ImageView paramImageView, String paramString, ImageView.ScaleType paramScaleType)
  {
    this.imageView = paramImageView;
    this.imageUrl = paramString;
    this.scaleType = paramScaleType;
    ViewTag.set(paramImageView, paramString);
  }

  public void handleMessage(Message paramMessage)
  {
    String str = ViewTag.get(this.imageView);
    if ((str != null) && (!str.equals(this.imageUrl)));
    label107: 
    while (true)
    {
      return;
      Bitmap localBitmap;
      if ((paramMessage.obj instanceof Bitmap))
        localBitmap = (Bitmap)paramMessage.obj;
      while (true)
      {
        if (localBitmap == null)
          break label107;
        this.imageView.setImageBitmap(localBitmap);
        if (this.scaleType == null)
          break;
        this.imageView.setScaleType(this.scaleType);
        return;
        boolean bool = paramMessage.obj instanceof SoftReference;
        localBitmap = null;
        if (bool)
          localBitmap = (Bitmap)((SoftReference)paramMessage.obj).get();
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.ImageViewHandler
 * JD-Core Version:    0.6.2
 */