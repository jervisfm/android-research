package com.flixster.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import net.flixster.android.FacebookAuth;
import net.flixster.android.FlixsterLoginPage;

public class LoginActivity extends DecoratedActivity
{
  private static final int CHOICE_FACEBOOK = 0;
  private static final int CHOICE_FLX_CREATE = 2;
  private static final int CHOICE_FLX_LOGIN = 1;
  private static final int LOGIN_REQUEST = 123;
  private String title;

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 123)
      switch (paramInt2)
      {
      default:
        Logger.d("FlxMain", "LoginActivity.onActivityResult LOGIN_REQUEST " + paramInt2);
      case -1:
      case 0:
      }
    while (true)
    {
      finish();
      return;
      Logger.d("FlxMain", "LoginActivity.onActivityResult LOGIN_REQUEST RESULT_OK");
      continue;
      Logger.d("FlxMain", "LoginActivity.onActivityResult LOGIN_REQUEST RESULT_CANCELED");
      continue;
      Logger.w("FlxMain", "LoginActivity.onActivityResult " + paramInt1 + " " + paramInt2);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.title = localBundle.getString("net.flixster.android.EXTRA_TITLE");
    if (this.title != null)
      ObjectHolder.instance().put(String.valueOf(1000000102), this.title);
    showDialog(1000000102, new DialogBuilder.DialogListener()
    {
      public void onNegativeButtonClick(int paramAnonymousInt)
      {
        LoginActivity.this.finish();
      }

      public void onNeutralButtonClick(int paramAnonymousInt)
      {
        LoginActivity.this.finish();
      }

      public void onPositiveButtonClick(int paramAnonymousInt)
      {
        switch (paramAnonymousInt)
        {
        default:
          throw new UnsupportedOperationException();
        case 0:
          Intent localIntent2 = new Intent(LoginActivity.this, FacebookAuth.class);
          LoginActivity.this.startActivityForResult(localIntent2, 123);
          return;
        case 1:
        }
        Intent localIntent1 = new Intent(LoginActivity.this, FlixsterLoginPage.class);
        LoginActivity.this.startActivityForResult(localIntent1, 123);
      }
    });
  }

  protected void onResume()
  {
    super.onResume();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.LoginActivity
 * JD-Core Version:    0.6.2
 */