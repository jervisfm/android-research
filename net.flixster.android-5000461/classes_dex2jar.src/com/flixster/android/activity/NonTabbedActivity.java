package com.flixster.android.activity;

import android.content.Intent;
import android.view.KeyEvent;
import com.flixster.android.utils.Logger;
import net.flixster.android.SearchPage;

public class NonTabbedActivity extends DialogActivity
{
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 84))
    {
      Logger.d("FlxMain", this.className + ".onKeyDown KEYCODE_SEARCH");
      startActivity(new Intent(this, SearchPage.class));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.NonTabbedActivity
 * JD-Core Version:    0.6.2
 */