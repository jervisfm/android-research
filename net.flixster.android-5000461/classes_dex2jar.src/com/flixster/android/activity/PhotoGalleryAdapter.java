package com.flixster.android.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import java.util.List;
import net.flixster.android.model.Photo;

public class PhotoGalleryAdapter extends BaseAdapter
{
  private final Context context;
  private final int mPhotoHeight;
  private final List<Photo> photos;

  public PhotoGalleryAdapter(Context paramContext, List<Photo> paramList)
  {
    this.context = paramContext;
    this.photos = paramList;
    this.mPhotoHeight = paramContext.getResources().getDimensionPixelOffset(2131361849);
  }

  public int getCount()
  {
    return this.photos.size();
  }

  public Object getItem(int paramInt)
  {
    return this.photos.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView == null) || (!(paramView instanceof ImageView)))
      paramView = new ImageView(this.context);
    Bitmap localBitmap = ((Photo)this.photos.get(paramInt)).getThumbnailBitmap((ImageView)paramView);
    if (localBitmap == null)
      ((ImageView)paramView).setImageResource(2130837841);
    while (true)
    {
      ((ImageView)paramView).setScaleType(ImageView.ScaleType.CENTER_CROP);
      ((ImageView)paramView).setMinimumHeight(this.mPhotoHeight);
      return paramView;
      ((ImageView)paramView).setImageBitmap(localBitmap);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.PhotoGalleryAdapter
 * JD-Core Version:    0.6.2
 */