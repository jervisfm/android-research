package com.flixster.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.flixster.android.ScrollGalleryPage;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Photo;

public class PhotoGalleryPage extends DecoratedSherlockActivity
{
  public static final String KEY_PHOTOS = "KEY_PHOTOS";
  public static final String KEY_USERNAME = "KEY_USERNAME";
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.e("FlxMain", "PhotoGalleryPage.errorHandler: " + paramAnonymousMessage);
    }
  };
  private GridView gallery;
  private final AdapterView.OnItemClickListener photoItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if ((Photo)PhotoGalleryPage.this.photos.get(paramAnonymousInt) != null)
      {
        Intent localIntent = new Intent(PhotoGalleryPage.this, ScrollGalleryPage.class);
        localIntent.putExtra("KEY_GENERIC_GALLERY", true);
        localIntent.putExtra("PHOTO_INDEX", paramAnonymousInt);
        localIntent.putExtra("KEY_TITLE", PhotoGalleryPage.this.title);
        PhotoGalleryPage.this.startActivity(localIntent);
      }
    }
  };
  private final List<Photo> photos = new ArrayList();
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (PhotoGalleryPage.this.gallery.getAdapter() == null)
      {
        PhotoGalleryPage.this.gallery.setAdapter(new PhotoGalleryAdapter(PhotoGalleryPage.this, PhotoGalleryPage.this.photos));
        PhotoGalleryPage.this.gallery.setOnItemClickListener(PhotoGalleryPage.this.photoItemClickListener);
        PhotoGalleryPage.this.gallery.setClickable(true);
      }
      ObjectHolder.instance().put("KEY_PHOTOS", Collections.unmodifiableList(PhotoGalleryPage.this.photos));
    }
  };
  private String title;
  private String username;

  private static String createTitle(String paramString, Resources paramResources)
  {
    return paramString.substring(0, 1).toUpperCase() + paramString.substring(1, paramString.length()) + " - " + paramResources.getString(2131492925);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.username = localBundle.getString("KEY_USERNAME");
    setContentView(2130903149);
    createActionBar();
    this.title = createTitle(this.username, getResources());
    setActionBarTitle(this.title);
    this.gallery = ((GridView)findViewById(2131165704));
    this.photos.clear();
    PhotoDao.getUserPhotos(this.username, this.photos, this.successHandler, this.errorHandler);
  }

  protected void onResume()
  {
    super.onResume();
    Trackers.instance().track("/photo/gallery/user", "Phot Gallery - User - " + this.username);
  }

  protected void onStop()
  {
    super.onStop();
    ObjectHolder.instance().remove("KEY_PHOTOS");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.PhotoGalleryPage
 * JD-Core Version:    0.6.2
 */