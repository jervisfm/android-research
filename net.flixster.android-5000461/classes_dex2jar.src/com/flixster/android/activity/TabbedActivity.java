package com.flixster.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.widget.WidgetProvider;
import net.flixster.android.Flixster;
import net.flixster.android.SearchPage;

public class TabbedActivity extends DecoratedActivity
{
  protected final String className = getClass().getSimpleName();

  private Flixster getParentTabActivity()
  {
    Activity localActivity = getParent();
    if ((localActivity instanceof Flixster))
      return (Flixster)localActivity;
    return null;
  }

  protected void checkAndShowLaunchAd()
  {
    Flixster localFlixster = getParentTabActivity();
    if (localFlixster != null)
      localFlixster.checkAndShowLaunchAd();
  }

  protected String getAnalyticsAction()
  {
    return null;
  }

  protected String getAnalyticsCategory()
  {
    return null;
  }

  protected String getAnalyticsTag()
  {
    return null;
  }

  protected String getAnalyticsTitle()
  {
    return null;
  }

  protected void invalidActionBarItems()
  {
    Flixster localFlixster = getParentTabActivity();
    if (localFlixster != null)
      localFlixster.invalidateOptionsMenu();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 84))
    {
      startActivity(new Intent(this, SearchPage.class));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void trackPage()
  {
    Trackers.instance().track(getAnalyticsTag(), getAnalyticsTitle());
    if ((WidgetProvider.isOriginatedFromWidget(getIntent())) && (getAnalyticsCategory() != null))
      Trackers.instance().trackEvent(getAnalyticsTag(), getAnalyticsTitle(), getAnalyticsCategory(), getAnalyticsAction());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.TabbedActivity
 * JD-Core Version:    0.6.2
 */