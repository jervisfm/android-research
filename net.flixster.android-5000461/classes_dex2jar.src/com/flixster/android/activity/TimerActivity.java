package com.flixster.android.activity;

import android.os.Handler;
import android.os.Handler.Callback;
import com.actionbarsherlock.app.SherlockActivity;
import java.util.Timer;
import java.util.TimerTask;

public class TimerActivity extends SherlockActivity
{
  private Timer timer;

  protected void cancelTimer()
  {
    if (this.timer != null)
    {
      this.timer.cancel();
      this.timer.purge();
      this.timer = null;
    }
  }

  protected void onPause()
  {
    super.onPause();
    cancelTimer();
  }

  protected void scheduleTask(Handler.Callback paramCallback, long paramLong)
  {
    if (this.timer == null)
      this.timer = new Timer();
    this.timer.schedule(new HandlerTimerTask(paramCallback, null), paramLong);
  }

  protected void scheduleTask(Handler.Callback paramCallback, long paramLong1, long paramLong2)
  {
    if (this.timer == null)
      this.timer = new Timer();
    this.timer.schedule(new HandlerTimerTask(paramCallback, null), paramLong1, paramLong2);
  }

  protected void scheduleTask(TimerTask paramTimerTask, long paramLong)
  {
    if (this.timer == null)
      this.timer = new Timer();
    this.timer.schedule(paramTimerTask, paramLong);
  }

  private class HandlerTimerTask extends TimerTask
  {
    private final Handler handler;

    private HandlerTimerTask(Handler.Callback arg2)
    {
      Handler.Callback localCallback;
      this.handler = new Handler(localCallback);
    }

    public void run()
    {
      this.handler.sendEmptyMessage(0);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.TimerActivity
 * JD-Core Version:    0.6.2
 */