package com.flixster.android.activity;

import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.Logger;

public class TopLevelActivity extends TimerActivity
{
  protected final String className = getClass().getSimpleName();
  private volatile boolean isPausing;
  private int level;

  protected static int getResumeCtr()
  {
    return TopLevelDecorator.getResumeCtr();
  }

  protected static void incrementResumeCtr()
  {
    TopLevelDecorator.incrementResumeCtr();
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setDisplayShowTitleEnabled(false);
  }

  protected void hideActionBar()
  {
    getSupportActionBar().hide();
  }

  protected boolean isPausing()
  {
    return this.isPausing;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", this.className + ".onCreate");
    this.level = ActivityHolder.instance().setTopLevelActivity(this);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", this.className + ".onDestroy");
    ActivityHolder.instance().removeTopLevelActivity(this.level);
  }

  protected void onPause()
  {
    super.onPause();
    Logger.d("FlxMain", this.className + ".onPause");
    this.isPausing = true;
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", this.className + ".onResume #" + getResumeCtr());
    this.level = ActivityHolder.instance().setTopLevelActivity(this);
    incrementResumeCtr();
    this.isPausing = false;
  }

  protected void setActionBarTitle(int paramInt)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramInt);
  }

  protected void setActionBarTitle(String paramString)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramString);
  }

  protected void showActionBar()
  {
    getSupportActionBar().show();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.TopLevelActivity
 * JD-Core Version:    0.6.2
 */