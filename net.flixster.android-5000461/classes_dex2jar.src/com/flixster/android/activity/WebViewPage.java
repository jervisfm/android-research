package com.flixster.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout.LayoutParams;

public class WebViewPage extends Activity
{
  public static final String KEY_URL = "KEY_URL";
  private static final LinearLayout.LayoutParams LINEAR_LAYOUT_FILL = new LinearLayout.LayoutParams(-1, -1);
  private WebView mWebView;

  private void setUpWebView(String paramString)
  {
    this.mWebView.setWebViewClient(new DefaultWebViewClient(null));
    this.mWebView.getSettings().setJavaScriptEnabled(true);
    this.mWebView.getSettings().setSupportZoom(false);
    this.mWebView.loadUrl(paramString);
    this.mWebView.setLayoutParams(LINEAR_LAYOUT_FILL);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    String str = null;
    if (localBundle != null)
      str = localBundle.getString("KEY_URL");
    setContentView(2130903187);
    this.mWebView = ((WebView)findViewById(2131165927));
    setUpWebView(str);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.mWebView.canGoBack()))
    {
      this.mWebView.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  private final class DefaultWebViewClient extends WebViewClient
  {
    private DefaultWebViewClient()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.WebViewPage
 * JD-Core Version:    0.6.2
 */