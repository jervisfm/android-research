package com.flixster.android.activity.common;

public abstract interface ActionBarActivity
{
  public abstract void setActionBarTitle(int paramInt);

  public abstract void setActionBarTitle(String paramString);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.ActionBarActivity
 * JD-Core Version:    0.6.2
 */