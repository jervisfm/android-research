package com.flixster.android.activity.common;

import android.os.Handler;
import com.flixster.android.utils.Logger;
import net.flixster.android.data.DaoException;

public class DaoHandler extends Handler
{
  private DaoException exception;
  private boolean isActive = true;
  private boolean isMsgSuccess;
  private Object msgObj;

  public DaoHandler activate()
  {
    this.isActive = true;
    return this;
  }

  public void cancel()
  {
    this.isActive = false;
  }

  public DaoException getException()
  {
    return this.exception;
  }

  public Object getMsgObj()
  {
    return this.msgObj;
  }

  public boolean isMsgSuccess()
  {
    return this.isMsgSuccess;
  }

  public void sendExceptionMessage(DaoException paramDaoException)
  {
    if (this.isActive)
    {
      this.isMsgSuccess = false;
      this.exception = paramDaoException;
      sendEmptyMessage(0);
      return;
    }
    Logger.sw("FlxMain", "DaoHandler.sendExceptionMessage cancelled");
  }

  public void sendSuccessMessage()
  {
    if (this.isActive)
    {
      this.isMsgSuccess = true;
      sendEmptyMessage(0);
      return;
    }
    Logger.sw("FlxMain", "DaoHandler.sendSuccessMessage cancelled");
  }

  public void sendSuccessMessage(Object paramObject)
  {
    if (this.isActive)
    {
      this.isMsgSuccess = true;
      this.msgObj = paramObject;
      sendEmptyMessage(0);
      return;
    }
    Logger.sw("FlxMain", "DaoHandler.sendSuccessMessage cancelled");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.DaoHandler
 * JD-Core Version:    0.6.2
 */