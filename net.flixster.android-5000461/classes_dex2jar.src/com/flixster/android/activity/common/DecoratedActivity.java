package com.flixster.android.activity.common;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import com.flixster.android.activity.decorator.ActivityDecorator;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class DecoratedActivity extends Activity
{
  private Collection<ActivityDecorator> decorators;
  protected DialogDecorator dialogDecorator;
  protected TopLevelDecorator topLevelDecorator;

  protected void addDecorator(ActivityDecorator paramActivityDecorator)
  {
    this.decorators.add(paramActivityDecorator);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.decorators = new ArrayList();
    DialogDecorator localDialogDecorator = new DialogDecorator(this);
    this.dialogDecorator = localDialogDecorator;
    addDecorator(localDialogDecorator);
    TopLevelDecorator localTopLevelDecorator = new TopLevelDecorator(this);
    this.topLevelDecorator = localTopLevelDecorator;
    addDecorator(localTopLevelDecorator);
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onCreate();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = this.dialogDecorator.onCreateDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onDestroy();
    }
  }

  protected void onPause()
  {
    super.onPause();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onPause();
    }
  }

  protected void onResume()
  {
    super.onResume();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onResume();
    }
  }

  protected void onStart()
  {
    super.onStart();
  }

  protected void onStop()
  {
    super.onStop();
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    this.dialogDecorator.showDialog(paramInt, paramDialogListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.DecoratedActivity
 * JD-Core Version:    0.6.2
 */