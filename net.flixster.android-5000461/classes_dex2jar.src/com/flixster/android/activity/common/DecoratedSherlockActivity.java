package com.flixster.android.activity.common;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.ActionBarDecorator;
import com.flixster.android.activity.decorator.ActivityDecorator;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.bootstrap.BootstrapActivity;
import com.flixster.android.utils.F;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import net.flixster.android.SearchPage;
import net.flixster.android.SettingsPage;
import net.flixster.android.data.ApiBuilder;

public class DecoratedSherlockActivity extends SherlockActivity
{
  protected ActionBarDecorator actionbarDecorator;
  private Collection<ActivityDecorator> decorators;
  protected DialogDecorator dialogDecorator;
  protected TopLevelDecorator topLevelDecorator;

  protected void addDecorator(ActivityDecorator paramActivityDecorator)
  {
    this.decorators.add(paramActivityDecorator);
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setDisplayShowTitleEnabled(false);
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493298, arrayOfObject));
    return localIntent;
  }

  protected void hideActionBar()
  {
    getSupportActionBar().hide();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.decorators = new ArrayList();
    DialogDecorator localDialogDecorator = new DialogDecorator(this);
    this.dialogDecorator = localDialogDecorator;
    addDecorator(localDialogDecorator);
    TopLevelDecorator localTopLevelDecorator = new TopLevelDecorator(this);
    this.topLevelDecorator = localTopLevelDecorator;
    addDecorator(localTopLevelDecorator);
    ActionBarDecorator localActionBarDecorator = new ActionBarDecorator(this);
    this.actionbarDecorator = localActionBarDecorator;
    addDecorator(localActionBarDecorator);
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onCreate();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = this.dialogDecorator.onCreateDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689489, paramMenu);
    if (F.IS_API_LEVEL_HONEYCOMB)
    {
      paramMenu.removeItem(2131165946);
      this.actionbarDecorator.createCollapsibleSearch(paramMenu);
    }
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onDestroy();
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
      NavUtils.navigateUpTo(this, BootstrapActivity.getMainIntent(this));
      return true;
    case 2131165955:
      startActivity(Intent.createChooser(createShareIntent(), "Share via"));
      return true;
    case 2131165946:
      startActivity(new Intent(this, SearchPage.class));
      return true;
    case 2131165947:
    }
    startActivity(new Intent(this, SettingsPage.class));
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onPause();
    }
  }

  protected void onResume()
  {
    super.onResume();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onResume();
    }
  }

  protected void onStart()
  {
    super.onStart();
  }

  protected void onStop()
  {
    super.onStop();
  }

  protected void setActionBarTitle(int paramInt)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramInt);
  }

  protected void setActionBarTitle(String paramString)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramString);
  }

  protected void showActionBar()
  {
    getSupportActionBar().show();
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    this.dialogDecorator.showDialog(paramInt, paramDialogListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.DecoratedSherlockActivity
 * JD-Core Version:    0.6.2
 */