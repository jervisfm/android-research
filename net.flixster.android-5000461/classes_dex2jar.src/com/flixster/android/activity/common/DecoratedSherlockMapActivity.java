package com.flixster.android.activity.common;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockMapActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.ActivityDecorator;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.activity.decorator.TopLevelDecorator;
import com.flixster.android.bootstrap.BootstrapActivity;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class DecoratedSherlockMapActivity extends SherlockMapActivity
{
  private Collection<ActivityDecorator> decorators;
  protected DialogDecorator dialogDecorator;
  protected TopLevelDecorator topLevelDecorator;

  protected void addDecorator(ActivityDecorator paramActivityDecorator)
  {
    this.decorators.add(paramActivityDecorator);
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayHomeAsUpEnabled(true);
    localActionBar.setDisplayShowTitleEnabled(false);
  }

  protected boolean isRouteDisplayed()
  {
    return false;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.decorators = new ArrayList();
    DialogDecorator localDialogDecorator = new DialogDecorator(this);
    this.dialogDecorator = localDialogDecorator;
    addDecorator(localDialogDecorator);
    TopLevelDecorator localTopLevelDecorator = new TopLevelDecorator(this);
    this.topLevelDecorator = localTopLevelDecorator;
    addDecorator(localTopLevelDecorator);
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onCreate();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = this.dialogDecorator.onCreateDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onDestroy();
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
    }
    NavUtils.navigateUpTo(this, BootstrapActivity.getMainIntent(this));
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onPause();
    }
  }

  protected void onResume()
  {
    super.onResume();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onResume();
    }
  }

  protected void onStart()
  {
    super.onStart();
  }

  protected void onStop()
  {
    super.onStop();
  }

  protected void setActionBarTitle(int paramInt)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramInt);
  }

  protected void setActionBarTitle(String paramString)
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayShowTitleEnabled(true);
    localActionBar.setTitle(paramString);
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    this.dialogDecorator.showDialog(paramInt, paramDialogListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.DecoratedSherlockMapActivity
 * JD-Core Version:    0.6.2
 */