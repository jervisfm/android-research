package com.flixster.android.activity.common;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockTabActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.decorator.ActionBarDecorator;
import com.flixster.android.activity.decorator.ActivityDecorator;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.activity.decorator.TimerDecorator;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.AppRater;
import com.flixster.android.utils.F;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import net.flixster.android.SearchPage;
import net.flixster.android.SettingsPage;
import net.flixster.android.Starter;
import net.flixster.android.data.ApiBuilder;

public class DecoratedSherlockTabActivity extends SherlockTabActivity
{
  protected ActionBarDecorator actionbarDecorator;
  private Collection<ActivityDecorator> decorators;
  protected DialogDecorator dialogDecorator;
  protected TimerDecorator timerDecorater;

  protected void addDecorator(ActivityDecorator paramActivityDecorator)
  {
    this.decorators.add(paramActivityDecorator);
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setDisplayHomeAsUpEnabled(false);
    localActionBar.setDisplayShowTitleEnabled(false);
    localActionBar.setLogo(2130837505);
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493298, arrayOfObject));
    return localIntent;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.decorators = new ArrayList();
    DialogDecorator localDialogDecorator = new DialogDecorator(this);
    this.dialogDecorator = localDialogDecorator;
    addDecorator(localDialogDecorator);
    TimerDecorator localTimerDecorator = new TimerDecorator(this);
    this.timerDecorater = localTimerDecorator;
    addDecorator(localTimerDecorator);
    ActionBarDecorator localActionBarDecorator = new ActionBarDecorator(this);
    this.actionbarDecorator = localActionBarDecorator;
    addDecorator(localActionBarDecorator);
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onCreate();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = this.dialogDecorator.onCreateDialog(paramInt);
    if (localDialog != null)
      return localDialog;
    return super.onCreateDialog(paramInt);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689482, paramMenu);
    if (F.IS_API_LEVEL_HONEYCOMB)
    {
      paramMenu.removeItem(2131165946);
      this.actionbarDecorator.createCollapsibleSearch(paramMenu);
    }
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onDestroy();
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    case 2131165948:
    case 2131165949:
    case 2131165950:
    case 2131165951:
    case 2131165952:
    case 2131165953:
    case 2131165954:
    case 2131165959:
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165946:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "Search");
      startActivity(new Intent(this, SearchPage.class));
      return true;
    case 2131165947:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "Settings");
      startActivity(new Intent(this, SettingsPage.class));
      return true;
    case 2131165955:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "TellAFriend");
      startActivity(Intent.createChooser(createShareIntent(), "Share via"));
      return true;
    case 2131165956:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "RateThisApp");
      AppRater.rateThisApp(this);
      return true;
    case 2131165957:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "FAQ");
      Starter.launchBrowser(ApiBuilder.faq(), this);
      return true;
    case 2131165960:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "PrivacyPolicy");
      showDialog(1000000010, new PrivacyDialogListener(null));
      return true;
    case 2131165958:
    }
    Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "Feedback");
    Starter.launchFlxHtmlPage(ApiBuilder.feedback(), getString(2131493173), this);
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onPause();
    }
  }

  protected void onResume()
  {
    super.onResume();
    Iterator localIterator = this.decorators.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      ((ActivityDecorator)localIterator.next()).onResume();
    }
  }

  protected void onStart()
  {
    super.onStart();
  }

  protected void onStop()
  {
    super.onStop();
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    this.dialogDecorator.showDialog(paramInt, paramDialogListener);
  }

  private class PrivacyDialogListener
    implements DialogBuilder.DialogListener
  {
    private PrivacyDialogListener()
    {
    }

    public void onNegativeButtonClick(int paramInt)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(DecoratedSherlockTabActivity.this.getResources().getString(2131493256)));
      DecoratedSherlockTabActivity.this.startActivity(localIntent);
    }

    public void onNeutralButtonClick(int paramInt)
    {
    }

    public void onPositiveButtonClick(int paramInt)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(DecoratedSherlockTabActivity.this.getResources().getString(2131493255)));
      DecoratedSherlockTabActivity.this.startActivity(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.DecoratedSherlockTabActivity
 * JD-Core Version:    0.6.2
 */