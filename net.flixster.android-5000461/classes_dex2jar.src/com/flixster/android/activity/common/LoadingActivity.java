package com.flixster.android.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.ProgressBar;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.data.MovieDaoNew;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import net.flixster.android.Starter;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.Movie;

public class LoadingActivity extends DecoratedSherlockActivity
{
  private final DaoHandler dataHandler = new DaoHandler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (isMsgSuccess())
      {
        Logger.d("FlxMain", "MovieDetailFragment.dataHandler success");
        LoadingActivity.this.throbber.setVisibility(8);
        LoadingActivity.this.launchTrailer((Movie)getMsgObj());
        return;
      }
      LoadingActivity.this.throbber.setVisibility(8);
      ErrorDialog.handleException(getException(), LoadingActivity.this);
    }
  };
  private ProgressBar throbber;

  private void launchTrailer(Movie paramMovie)
  {
    Starter.launchAdFreeTrailer(paramMovie, this);
    finish();
  }

  private void loadMovie(long paramLong)
  {
    Movie localMovie = MovieDao.getMovie(paramLong);
    if (localMovie.isDetailsApiParsed())
    {
      launchTrailer(localMovie);
      return;
    }
    this.throbber.setVisibility(0);
    MovieDaoNew.fetchMovie(paramLong, this.dataHandler);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903104);
    createActionBar();
    setActionBarTitle(2131493173);
    this.throbber = ((ProgressBar)findViewById(2131165241));
    long l = getIntent().getLongExtra("net.flixster.android.EXTRA_MOVIE_ID", 0L);
    if (l > 0L)
    {
      loadMovie(l);
      return;
    }
    finish();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  public void onStop()
  {
    super.onStop();
    this.dataHandler.cancel();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.LoadingActivity
 * JD-Core Version:    0.6.2
 */