package com.flixster.android.activity.common;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockListFragment;
import com.flixster.android.data.MovieDaoNew;
import com.flixster.android.utils.ErrorDialog;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviActor;
import net.flixster.android.lvi.LviAdapter;
import net.flixster.android.lvi.LviAdapter.LviType;
import net.flixster.android.lvi.LviLoadMore;
import net.flixster.android.lvi.LviLoadMore.Type;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Movie;

public class SearchFragment extends SherlockListFragment
  implements AdapterView.OnItemClickListener
{
  private static final int NUM_PREVIEW_RESULTS = 2;
  private List<Actor> actorResults;
  private final DaoHandler dataHandler = new DaoHandler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (isMsgSuccess())
      {
        SearchFragment.this.onDataFetched();
        return;
      }
      ErrorDialog.handleException(getException(), SearchFragment.this.getActivity(), 0);
    }
  };
  private DisplayMode displayMode = DisplayMode.PREVIEW_ALL;
  private boolean isDataFetched;
  private boolean isViewCreated;
  private List<Lvi> items;
  private List<Movie> movieResults;
  private String query;

  public SearchFragment()
  {
    setRetainInstance(true);
  }

  private List<Lvi> createListItems()
  {
    this.items = new ArrayList();
    this.items.add(new LviSubHeader(2131493124, getActivity()));
    if (this.movieResults.isEmpty())
    {
      this.items.add(createNotFoundMessage());
      this.items.add(new LviSubHeader(2131493125, getActivity()));
      if (!this.actorResults.isEmpty())
        break label310;
      this.items.add(createNotFoundMessage());
    }
    while (true)
    {
      return this.items;
      if ((this.displayMode == DisplayMode.PREVIEW_ALL) || (this.displayMode == DisplayMode.SHOW_ALL_ACTORS))
      {
        int i = this.movieResults.size();
        for (int j = 0; ; j++)
        {
          if (j >= Math.min(i, 2))
          {
            if (i <= 2)
              break;
            List localList1 = this.items;
            String str1 = getString(2131493131);
            Object[] arrayOfObject1 = new Object[1];
            arrayOfObject1[0] = Integer.valueOf(i);
            localList1.add(new LviLoadMore(str1, getString(2131493133, arrayOfObject1), LviLoadMore.Type.MOVIE));
            break;
          }
          this.items.add(new LviMovie((Movie)this.movieResults.get(j)));
        }
      }
      Iterator localIterator2 = this.movieResults.iterator();
      while (localIterator2.hasNext())
      {
        Movie localMovie = (Movie)localIterator2.next();
        this.items.add(new LviMovie(localMovie));
      }
      break;
      label310: if ((this.displayMode == DisplayMode.PREVIEW_ALL) || (this.displayMode == DisplayMode.SHOW_ALL_MOVIES))
      {
        int k = this.actorResults.size();
        for (int m = 0; ; m++)
        {
          if (m >= Math.min(k, 2))
          {
            if (k <= 2)
              break;
            List localList2 = this.items;
            String str2 = getString(2131493132);
            Object[] arrayOfObject2 = new Object[1];
            arrayOfObject2[0] = Integer.valueOf(k);
            localList2.add(new LviLoadMore(str2, getString(2131493133, arrayOfObject2), LviLoadMore.Type.ACTOR));
            break;
          }
          this.items.add(new LviActor((Actor)this.actorResults.get(m), true));
        }
      }
      Iterator localIterator1 = this.actorResults.iterator();
      while (localIterator1.hasNext())
      {
        Actor localActor = (Actor)localIterator1.next();
        this.items.add(new LviActor(localActor, true));
      }
    }
  }

  private Lvi createNotFoundMessage()
  {
    LviMessagePanel localLviMessagePanel = new LviMessagePanel();
    localLviMessagePanel.mMessage = getResources().getString(2131493130);
    return localLviMessagePanel;
  }

  public static SearchFragment newInstance(String paramString)
  {
    return new SearchFragment().setQuery(paramString);
  }

  protected void checkState()
  {
    if (isDataFetch())
    {
      if (!isViewCreated())
        createView();
      return;
    }
    fetchData();
  }

  protected void createView()
  {
    setListAdapter(new LviAdapter(getActivity(), createListItems()));
    getListView().setOnItemClickListener(this);
    setListShown(true);
    this.isViewCreated = true;
  }

  protected void fetchData()
  {
    this.movieResults = new ArrayList();
    this.actorResults = new ArrayList();
    if (this.query.trim().length() == 0)
    {
      onDataFetched();
      return;
    }
    if (("FLX".equalsIgnoreCase(this.query)) || ("REINDEER FLOTILLA".equalsIgnoreCase(this.query)) || ("UUDDLRLRBA".equalsIgnoreCase(this.query)))
    {
      FlixsterApplication.enableAdminMode();
      Toast.makeText(getActivity(), 2131493307, 0).show();
      onDataFetched();
      return;
    }
    if ("DGNS".equalsIgnoreCase(this.query))
    {
      FlixsterApplication.enableDiagnosticMode();
      Toast.makeText(getActivity(), 2131493308, 0).show();
      onDataFetched();
      return;
    }
    MovieDaoNew.searchMovieAndActor(this.query, this.movieResults, this.actorResults, this.dataHandler);
  }

  public void invalidateData()
  {
    this.isViewCreated = false;
    this.isDataFetched = false;
    this.displayMode = DisplayMode.PREVIEW_ALL;
    setListShown(false);
    checkState();
  }

  public void invalidateView()
  {
    this.isViewCreated = false;
    setListShown(false);
    checkState();
  }

  protected boolean isDataFetch()
  {
    return this.isDataFetched;
  }

  protected boolean isViewCreated()
  {
    return this.isViewCreated;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    ((ActionBarActivity)getActivity()).setActionBarTitle(getString(2131492936) + " - " + this.query);
    ListView localListView = getListView();
    localListView.setBackgroundColor(getResources().getColor(2131296267));
    localListView.setOnItemClickListener(this);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  protected void onDataFetched()
  {
    this.isDataFetched = true;
    checkState();
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Lvi localLvi = (Lvi)this.items.get(paramInt);
    int i = localLvi.getItemViewType();
    if (LviAdapter.LviType.MOVIE.ordinal() == i)
      Starter.launchMovieDetail(((LviMovie)localLvi).mMovie.getId(), getActivity());
    do
    {
      return;
      if (LviAdapter.LviType.ACTOR.ordinal() == i)
      {
        Actor localActor = ((LviActor)localLvi).mActor;
        Starter.launchActor(localActor.id, localActor.name, getActivity());
        return;
      }
    }
    while (LviAdapter.LviType.LOAD_MORE.ordinal() != i);
    LviLoadMore.Type localType = ((LviLoadMore)localLvi).type;
    switch ($SWITCH_TABLE$net$flixster$android$lvi$LviLoadMore$Type()[localType.ordinal()])
    {
    default:
      throw new IllegalStateException();
    case 1:
    case 2:
    }
    for (this.displayMode = DisplayMode.SHOW_ALL_MOVIES; ; this.displayMode = DisplayMode.SHOW_ALL_ACTORS)
    {
      invalidateView();
      return;
    }
  }

  public void onResume()
  {
    super.onResume();
    checkState();
  }

  public void onStop()
  {
    super.onStop();
    this.dataHandler.cancel();
  }

  public SearchFragment setQuery(String paramString)
  {
    this.query = paramString;
    return this;
  }

  private static enum DisplayMode
  {
    static
    {
      SHOW_ALL_ACTORS = new DisplayMode("SHOW_ALL_ACTORS", 2);
      DisplayMode[] arrayOfDisplayMode = new DisplayMode[3];
      arrayOfDisplayMode[0] = PREVIEW_ALL;
      arrayOfDisplayMode[1] = SHOW_ALL_MOVIES;
      arrayOfDisplayMode[2] = SHOW_ALL_ACTORS;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.common.SearchFragment
 * JD-Core Version:    0.6.2
 */