package com.flixster.android.activity.decorator;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import net.flixster.android.Starter;

public class ActionBarDecorator
  implements ActivityDecorator, MenuItem.OnActionExpandListener, View.OnFocusChangeListener, TextView.OnEditorActionListener
{
  private final Activity activity;

  public ActionBarDecorator(Activity paramActivity)
  {
    this.activity = paramActivity;
  }

  private void showSoftKeyboard(View paramView)
  {
    ((InputMethodManager)this.activity.getSystemService("input_method")).showSoftInput(paramView, 1);
  }

  public void createCollapsibleSearch(Menu paramMenu)
  {
    MenuItem localMenuItem = paramMenu.add(2131493143);
    localMenuItem.setIcon(2130837711).setActionView(2130903061).setOnActionExpandListener(this).setShowAsAction(10);
    TextView localTextView = (TextView)localMenuItem.getActionView().findViewById(2131165223);
    localTextView.setOnFocusChangeListener(this);
    localTextView.setOnEditorActionListener(this);
    localTextView.setTag(localMenuItem);
  }

  protected void hideSoftKeyboard(View paramView)
  {
    ((InputMethodManager)this.activity.getSystemService("input_method")).hideSoftInputFromWindow(paramView.getWindowToken(), 0);
  }

  public void onCreate()
  {
  }

  public void onDestroy()
  {
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 2)
    {
      hideSoftKeyboard(paramTextView);
      ((MenuItem)paramTextView.getTag()).collapseActionView();
      String str = paramTextView.getText().toString();
      paramTextView.setText("");
      Starter.launchSearch(str, this.activity);
      return true;
    }
    return false;
  }

  public void onFocusChange(final View paramView, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramView.post(new Runnable()
      {
        public void run()
        {
          ActionBarDecorator.this.showSoftKeyboard(paramView);
        }
      });
      return;
    }
    hideSoftKeyboard(paramView);
  }

  public boolean onMenuItemActionCollapse(MenuItem paramMenuItem)
  {
    hideSoftKeyboard(paramMenuItem.getActionView());
    return true;
  }

  public boolean onMenuItemActionExpand(MenuItem paramMenuItem)
  {
    ((TextView)paramMenuItem.getActionView().findViewById(2131165223)).requestFocus();
    return true;
  }

  public void onPause()
  {
  }

  public void onResume()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.decorator.ActionBarDecorator
 * JD-Core Version:    0.6.2
 */