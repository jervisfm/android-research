package com.flixster.android.activity.decorator;

public abstract interface ActivityDecorator
{
  public abstract void onCreate();

  public abstract void onDestroy();

  public abstract void onPause();

  public abstract void onResume();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.decorator.ActivityDecorator
 * JD-Core Version:    0.6.2
 */