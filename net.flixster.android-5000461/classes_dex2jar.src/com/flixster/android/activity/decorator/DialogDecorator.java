package com.flixster.android.activity.decorator;

import android.app.Activity;
import android.app.Dialog;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogEvents;
import com.flixster.android.view.DialogBuilder.DialogListener;

public class DialogDecorator
  implements ActivityDecorator
{
  private final Activity activity;
  private Dialog lastDialog;
  private int lastDialogId;

  public DialogDecorator(Activity paramActivity)
  {
    this.activity = paramActivity;
  }

  public void onCreate()
  {
  }

  public Dialog onCreateDialog(int paramInt)
  {
    this.lastDialogId = paramInt;
    this.lastDialog = DialogBuilder.createDialog(this.activity, paramInt);
    return this.lastDialog;
  }

  public void onDestroy()
  {
  }

  public void onPause()
  {
  }

  public void onResume()
  {
  }

  public void showDialog(int paramInt, DialogBuilder.DialogListener paramDialogListener)
  {
    DialogBuilder.DialogEvents.instance().removeListeners();
    if (paramDialogListener != null)
      DialogBuilder.DialogEvents.instance().addListener(paramDialogListener);
    if (this.lastDialog != null)
    {
      this.activity.removeDialog(this.lastDialogId);
      this.lastDialog = null;
    }
    this.activity.showDialog(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.decorator.DialogDecorator
 * JD-Core Version:    0.6.2
 */