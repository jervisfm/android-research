package com.flixster.android.activity.decorator;

import android.app.Activity;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.Logger;

public class TopLevelDecorator
  implements ActivityDecorator
{
  private static int resumeCtr = -1;
  private final Activity activity;
  protected String className;
  private volatile boolean isPausing;
  private int level;

  public TopLevelDecorator(Activity paramActivity)
  {
    this.activity = paramActivity;
    this.className = paramActivity.getClass().getSimpleName();
  }

  public static int getResumeCtr()
  {
    return resumeCtr;
  }

  public static void incrementResumeCtr()
  {
    resumeCtr = 1 + resumeCtr;
  }

  public boolean isPausing()
  {
    return this.isPausing;
  }

  public void onCreate()
  {
    Logger.d("FlxMain", this.className + ".onCreate");
    this.level = ActivityHolder.instance().setTopLevelActivity(this.activity);
  }

  public void onDestroy()
  {
    Logger.d("FlxMain", this.className + ".onDestroy");
    ActivityHolder.instance().removeTopLevelActivity(this.level);
  }

  public void onPause()
  {
    Logger.d("FlxMain", this.className + ".onPause");
    this.isPausing = true;
  }

  public void onResume()
  {
    Logger.d("FlxMain", this.className + ".onResume #" + getResumeCtr());
    this.level = ActivityHolder.instance().setTopLevelActivity(this.activity);
    resumeCtr = 1 + resumeCtr;
    this.isPausing = false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.decorator.TopLevelDecorator
 * JD-Core Version:    0.6.2
 */