package com.flixster.android.activity.gtv;

public class BoxOfficeFragment extends MovieGalleryFragment
{
  protected String getAnalyticsTag()
  {
    return "/boxoffice";
  }

  protected String getAnalyticsTitle()
  {
    return "Box Office";
  }

  protected MovieGalleryFragment.Type getType()
  {
    return MovieGalleryFragment.Type.BOX_OFFICE;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.BoxOfficeFragment
 * JD-Core Version:    0.6.2
 */