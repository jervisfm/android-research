package com.flixster.android.activity.gtv;

public class DvdFragment extends MovieGalleryFragment
{
  protected String getAnalyticsTag()
  {
    return "/dvds/new-releases";
  }

  protected String getAnalyticsTitle()
  {
    return "Dvds - New Releases";
  }

  protected MovieGalleryFragment.Type getType()
  {
    return MovieGalleryFragment.Type.DVD;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.DvdFragment
 * JD-Core Version:    0.6.2
 */