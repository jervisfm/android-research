package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.os.Bundle;
import com.flixster.android.utils.ObjectHolder;

@TargetApi(11)
public class InstanceStateFragment extends ResultPassingFragment
{
  private final String className = getClass().getName();
  private final int hashCode = hashCode();

  protected Bundle getSavedInstanceState()
  {
    return (Bundle)ObjectHolder.instance().get(this.className + this.hashCode);
  }

  public void onDestroy()
  {
    super.onDestroy();
    ObjectHolder.instance().remove(this.className + this.hashCode);
  }

  public void onDestroyView()
  {
    super.onDestroyView();
    Bundle localBundle = new Bundle();
    ObjectHolder.instance().put(this.className + this.hashCode, localBundle);
    onSaveInstanceState(localBundle);
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.InstanceStateFragment
 * JD-Core Version:    0.6.2
 */