package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import net.flixster.android.FacebookAuth;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;

@TargetApi(11)
public class LoginDialogFragment extends ResultPassingFragment
{
  private final String className = getClass().getName();
  private EditText emailText;
  private TextView errorText;
  private Button facebookButton;
  private Button loginButton;
  private final Handler loginErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DaoException localDaoException = (DaoException)paramAnonymousMessage.obj;
      if (localDaoException.getType() == DaoException.Type.USER_ACCT);
      for (String str = localDaoException.getMessage(); ; str = LoginDialogFragment.this.getString(2131493217))
      {
        LoginDialogFragment.this.errorText.setText(str);
        LoginDialogFragment.this.errorText.setVisibility(0);
        LoginDialogFragment.this.throbber.setVisibility(8);
        return;
      }
    }
  };
  private final Handler loginSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      LoginDialogFragment.this.throbber.setVisibility(8);
      Main localMain = (Main)LoginDialogFragment.this.getActivity();
      if (localMain != null)
        localMain.removeTopFragment();
    }
  };
  private final View.OnClickListener onClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      case 2131165420:
      default:
        return;
      case 2131165419:
        if (FlixsterApplication.hasAcceptedTos())
        {
          LoginDialogFragment.this.tosDialogListener.onPositiveButtonClick(0);
          return;
        }
        DialogBuilder.showTermsOfService(LoginDialogFragment.this.getActivity(), LoginDialogFragment.this.tosDialogListener);
        return;
      case 2131165421:
      }
      LoginDialogFragment.this.facebookLogin();
    }
  };
  private final View.OnKeyListener onKeyListener = new View.OnKeyListener()
  {
    public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      if ((paramAnonymousKeyEvent.getAction() == 0) && (paramAnonymousInt == 61))
        switch (paramAnonymousView.getId())
        {
        case 2131165418:
        default:
        case 2131165416:
        case 2131165417:
        case 2131165419:
        }
      View localView;
      do
      {
        return false;
        localView = paramAnonymousView.focusSearch(130);
      }
      while (localView == null);
      localView.requestFocus();
      return true;
    }
  };
  private EditText passwordText;
  protected ProgressBar throbber;
  private final DialogBuilder.DialogListener tosDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      FlixsterApplication.setTosAccepted(true);
      LoginDialogFragment.this.flixsterLogin();
    }
  };

  private void facebookLogin()
  {
    getActivity().startActivityForResult(new Intent(getActivity(), FacebookAuth.class), 123);
  }

  private void flixsterLogin()
  {
    this.errorText.setVisibility(8);
    this.throbber.setVisibility(0);
    String str1 = this.emailText.getText().toString().trim();
    String str2 = this.passwordText.getText().toString();
    AccountManager.instance().loginToFlixster(str1, str2, this.loginSuccessHandler, this.loginErrorHandler);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", this.className + ".onCreate");
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", this.className + ".onCreateView");
    View localView = paramLayoutInflater.inflate(2130903107, paramViewGroup, false);
    this.loginButton = ((Button)localView.findViewById(2131165419));
    this.loginButton.setOnClickListener(this.onClickListener);
    this.loginButton.setOnKeyListener(this.onKeyListener);
    this.facebookButton = ((Button)localView.findViewById(2131165421));
    this.facebookButton.setOnClickListener(this.onClickListener);
    this.emailText = ((EditText)localView.findViewById(2131165416));
    this.emailText.setOnKeyListener(this.onKeyListener);
    this.passwordText = ((EditText)localView.findViewById(2131165417));
    this.passwordText.setOnKeyListener(this.onKeyListener);
    this.errorText = ((TextView)localView.findViewById(2131165420));
    this.throbber = ((ProgressBar)localView.findViewById(2131165241));
    return localView;
  }

  protected void onFragmentResult(String paramString, int paramInt)
  {
    if ((FacebookAuth.class.getName().equals(paramString)) && (paramInt == -1))
      this.loginSuccessHandler.handleMessage(null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.LoginDialogFragment
 * JD-Core Version:    0.6.2
 */