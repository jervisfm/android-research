package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.bootstrap.Startup;
import com.flixster.android.utils.AppRater;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.google.tv.leftnavbar.LeftNavBar;
import net.flixster.android.FacebookAuth;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.util.ErrorHandler;

@TargetApi(11)
public class Main extends ResultPassingFragmentActivity
{
  private static final int TAB_BOXOFFICE_RESID = 2131493135;
  private static final int TAB_DVD_RESID = 2131493138;
  private static final int TAB_SEARCH_RESID = 2131492936;
  private static final int TAB_STORE_RESID = 2131493139;
  private static final int TAB_UPCOMING_RESID = 2131493137;
  private boolean isActionBarReady;
  private LeftNavBar mLeftNavBar;
  private boolean skipRatePrompt;

  private static int getLastTab()
  {
    String str = FlixsterApplication.getLastTab();
    try
    {
      int i = Integer.valueOf(str).intValue();
      int j = i;
      if (j >= 5)
        j = 1;
      return j;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 1;
  }

  private LeftNavBar getLeftNavBar()
  {
    if (this.mLeftNavBar == null)
      this.mLeftNavBar = new LeftNavBar(this);
    return this.mLeftNavBar;
  }

  private void resetBackStack()
  {
    getFragmentManager().popBackStack("0", 1);
  }

  private void showDiagnosticsOrRateDialog()
  {
    if (Properties.instance().hasUserSessionExpired())
      showDialog(1000000904, null);
    do
    {
      return;
      final Intent localIntent = ErrorHandler.instance().checkForAppCrash(getApplicationContext());
      if (localIntent != null)
      {
        this.skipRatePrompt = true;
        showDialog(1000001000, new DialogBuilder.DialogListener()
        {
          public void onNegativeButtonClick(int paramAnonymousInt)
          {
          }

          public void onNeutralButtonClick(int paramAnonymousInt)
          {
          }

          public void onPositiveButtonClick(int paramAnonymousInt)
          {
            Starter.tryLaunch(Main.this, localIntent);
          }
        });
        return;
      }
    }
    while (this.skipRatePrompt);
    AppRater.showRateDialog(this);
  }

  protected void createActionBar()
  {
    LeftNavBar localLeftNavBar = getLeftNavBar();
    localLeftNavBar.setNavigationMode(2);
    localLeftNavBar.setDisplayShowHomeEnabled(false);
    localLeftNavBar.setDisplayHomeAsUpEnabled(false);
    localLeftNavBar.setDisplayShowTitleEnabled(false);
    localLeftNavBar.showOptionsMenu(false);
    ActionBar.Tab localTab1 = localLeftNavBar.newTab().setText(2131493139).setIcon(2130837835);
    ActionBar.Tab localTab2 = localLeftNavBar.newTab().setText(2131493135).setIcon(2130837631);
    ActionBar.Tab localTab3 = localLeftNavBar.newTab().setText(2131493137).setIcon(2130837976);
    ActionBar.Tab localTab4 = localLeftNavBar.newTab().setText(2131493138).setIcon(2130837684);
    ActionBar.Tab localTab5 = localLeftNavBar.newTab().setText(2131492936).setIcon(2130837889);
    localTab1.setTabListener(new TabListenerImpl(StoreFragment.class, null));
    localTab2.setTabListener(new TabListenerImpl(BoxOfficeFragment.class, null));
    localTab3.setTabListener(new TabListenerImpl(UpcomingFragment.class, null));
    localTab4.setTabListener(new TabListenerImpl(DvdFragment.class, null));
    localTab5.setTabListener(new TabListenerImpl(SearchFragment.class, null));
    localLeftNavBar.addTab(localTab1);
    localLeftNavBar.addTab(localTab2);
    localLeftNavBar.addTab(localTab3);
    localLeftNavBar.addTab(localTab4);
    localLeftNavBar.addTab(localTab5);
    localLeftNavBar.setSelectedNavigationItem(getLastTab());
    this.isActionBarReady = true;
  }

  protected Intent createShareIntent()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = ApiBuilder.mobileUpsell();
    localIntent.putExtra("android.intent.extra.TEXT", getString(2131493298, arrayOfObject));
    return localIntent;
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 123))
      setFragmentResult(FacebookAuth.class.getName(), paramInt2);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getLastNonConfigurationInstance() != null);
    for (boolean bool = true; ; bool = false)
    {
      this.skipRatePrompt = bool;
      setContentView(2130903091);
      createActionBar();
      showDiagnosticsOrRateDialog();
      Startup.instance().onStartup(this);
      return;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131689481, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", "Main.onDestroy");
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165947:
      if (FlixsterApplication.isAdminEnabled())
      {
        Starter.launchSettings(this);
        return true;
      }
      new SettingsDialogFragment().show(getFragmentManager(), "settings_fragment");
      return true;
    case 2131165955:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "TellAFriend");
      startActivity(Intent.createChooser(createShareIntent(), "Share via"));
      return true;
    case 2131165956:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "RateThisApp");
      AppRater.rateThisApp(this);
      return true;
    case 2131165957:
      Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "FAQ");
      Starter.launchBrowser(ApiBuilder.faq(), this);
      return true;
    case 2131165958:
    }
    Trackers.instance().trackEvent("/main/phone", "Main - Phone", "MainPhoneActionBar", "Feedback");
    Starter.launchFlxHtmlPage(ApiBuilder.feedback(), getString(2131493173), this);
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    Logger.d("FlxMain", "Main.onPause");
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "Main.onResume");
  }

  public Object onRetainNonConfigurationInstance()
  {
    if (this.skipRatePrompt)
      return new Object();
    return null;
  }

  protected void onStop()
  {
    super.onStop();
    Logger.d("FlxMain", "Main.onStop");
  }

  protected void removeTopFragment()
  {
    getFragmentManager().popBackStack();
  }

  protected void startFragment(Class<? extends Fragment> paramClass, Bundle paramBundle)
  {
    try
    {
      Fragment localFragment = (Fragment)paramClass.newInstance();
      localFragment.setArguments(paramBundle);
      FragmentManager localFragmentManager = getFragmentManager();
      int i = localFragmentManager.getBackStackEntryCount();
      FragmentTransaction localFragmentTransaction = localFragmentManager.beginTransaction();
      localFragmentTransaction.replace(2131165361, localFragment);
      localFragmentTransaction.addToBackStack(String.valueOf(i));
      localFragmentTransaction.commit();
      return;
    }
    catch (InstantiationException localInstantiationException)
    {
      throw new RuntimeException(localInstantiationException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
  }

  private class TabListenerImpl
    implements ActionBar.TabListener
  {
    private final Class<? extends Fragment> fragmentClass;

    private TabListenerImpl()
    {
      Object localObject;
      this.fragmentClass = localObject;
    }

    public void onTabReselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
    {
    }

    public void onTabSelected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
    {
      paramTab.getIcon().setState(new int[] { 16842913 });
      paramTab.getIcon().invalidateSelf();
      if (Main.this.isActionBarReady)
        FlixsterApplication.setLastTab(String.valueOf(paramTab.getPosition()));
      try
      {
        Fragment localFragment = (Fragment)this.fragmentClass.newInstance();
        paramFragmentTransaction.replace(2131165361, localFragment, null);
        return;
      }
      catch (InstantiationException localInstantiationException)
      {
        throw new RuntimeException(localInstantiationException);
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new RuntimeException(localIllegalAccessException);
      }
    }

    public void onTabUnselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
    {
      Main.this.resetBackStack();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.Main
 * JD-Core Version:    0.6.2
 */