package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.flixster.android.activity.common.DaoHandler;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.data.MovieDaoNew;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.gtv.CriticReview;
import com.flixster.android.view.gtv.MovieDetailHeader;
import com.flixster.android.view.gtv.Subheader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.Starter;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

@TargetApi(11)
public class MovieDetailFragment extends Fragment
{
  private static final int MAX_REVIEW_COUNT = 10;
  private TextView back;
  private final DaoHandler dataHandler = new DaoHandler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (isMsgSuccess())
      {
        Logger.d("FlxMain", "MovieDetailFragment.dataHandler success");
        Activity localActivity = MovieDetailFragment.this.getActivity();
        if ((localActivity == null) || (MovieDetailFragment.this.isRemoving()))
          return;
        MovieDetailFragment.this.throbber.setVisibility(8);
        MovieDetailFragment localMovieDetailFragment1 = MovieDetailFragment.this;
        MovieDetailFragment localMovieDetailFragment2 = MovieDetailFragment.this;
        Movie localMovie = (Movie)getMsgObj();
        localMovieDetailFragment2.movie = localMovie;
        localMovieDetailFragment1.populateFragment(localMovie, localActivity);
        MovieDetailFragment.this.focusSelf();
        return;
      }
      MovieDetailFragment.this.throbber.setVisibility(8);
      ErrorDialog.handleException(getException(), (Main)MovieDetailFragment.this.getActivity());
    }
  };
  private FakeFocusButtonManager fakeButtonManager;
  private Movie movie;
  private String movieTitle;
  private final View.OnClickListener onClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165471:
        ((Main)MovieDetailFragment.this.getActivity()).removeTopFragment();
        return;
      case 2131165472:
        Trackers.instance().track("/trailer", "Trailer - " + MovieDetailFragment.this.movieTitle);
        Starter.launchVideo(MovieDetailFragment.this.movie.getTrailerHigh(), MovieDetailFragment.this.getActivity());
        return;
      case 2131165473:
      }
      Drm.manager().playMovie(MovieDetailFragment.this.right);
    }
  };
  private final View.OnKeyListener onKeyListener = new View.OnKeyListener()
  {
    public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      boolean bool;
      if (1 == paramAnonymousKeyEvent.getAction())
      {
        bool = false;
        switch (paramAnonymousInt)
        {
        default:
        case 21:
        case 22:
        case 23:
        case 62:
        case 66:
        }
      }
      do
      {
        int i;
        do
        {
          return bool;
          return MovieDetailFragment.this.fakeButtonManager.moveFocusLeft();
          return MovieDetailFragment.this.fakeButtonManager.moveFocusRight();
          MovieDetailFragment.this.fakeButtonManager.performClick();
          return true;
          i = paramAnonymousKeyEvent.getAction();
          bool = false;
        }
        while (i != 0);
        switch (paramAnonymousInt)
        {
        default:
          return false;
        case 21:
        }
        bool = MovieDetailFragment.this.fakeButtonManager.canMoveFocusLeft();
      }
      while (bool);
      MovieDetailFragment.this.fakeButtonManager.loseFocusToLeft();
      return bool;
    }
  };
  private LockerRight right;
  private View rootLayout;
  private View scrollView;
  private LinearLayout scrollViewLayout;
  private ProgressBar throbber;
  private TextView title;
  private TextView trailer;
  private TextView watch;
  private TextView year;

  private void focusSelf()
  {
    this.scrollView.requestFocus();
    this.fakeButtonManager.setFocusRightmost();
  }

  private void populateFragment(Movie paramMovie, Context paramContext)
  {
    if (paramMovie.getTrailerHigh() != null)
    {
      this.trailer.setVisibility(0);
      this.fakeButtonManager.addButton(new FakeFocusButton(this.trailer));
    }
    String str = paramMovie.getReleaseYear();
    if (str != null)
      this.year.setText("(" + str + ")");
    MovieDetailHeader localMovieDetailHeader = new MovieDetailHeader(paramContext);
    this.scrollViewLayout.addView(localMovieDetailHeader, 0);
    localMovieDetailHeader.load(paramMovie, this.right);
    ArrayList localArrayList = paramMovie.getCriticReviewsList();
    int i;
    if ((localArrayList != null) && (localArrayList.size() > 0))
    {
      Subheader localSubheader = new Subheader(paramContext);
      localSubheader.setText(getString(2131493160));
      this.scrollViewLayout.addView(localSubheader);
      i = Math.min(10, localArrayList.size());
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        if ((this.right != null) && (Drm.logic().isAssetPlayable(this.right)))
        {
          this.watch.setVisibility(0);
          this.fakeButtonManager.addButton(new FakeFocusButton(this.watch));
        }
        return;
      }
      Review localReview = (Review)localArrayList.get(j);
      CriticReview localCriticReview = new CriticReview(paramContext);
      this.scrollViewLayout.addView(localCriticReview);
      localCriticReview.load(localReview);
    }
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    Bundle localBundle = getArguments();
    long l1 = 0L;
    if (localBundle != null)
    {
      this.movieTitle = localBundle.getString("net.flixster.android.EXTRA_TITLE");
      if (this.movieTitle != null)
        this.title.setText(this.movieTitle);
      l1 = localBundle.getLong("net.flixster.android.EXTRA_ID");
      long l2 = localBundle.getLong("net.flixster.android.EXTRA_RIGHT_ID");
      User localUser = AccountManager.instance().getUser();
      if ((localUser != null) && (l2 > 0L))
        this.right = localUser.getLockerRightFromRightId(l2);
    }
    if (l1 == 0L)
      throw new AssertionError("MovieDetailFragment missing id");
    this.throbber.setVisibility(0);
    MovieDaoNew.fetchMovie(l1, this.dataHandler);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.rootLayout = paramLayoutInflater.inflate(2130903121, paramViewGroup, false);
    this.scrollView = this.rootLayout.findViewById(2131165467);
    this.scrollView.setOnKeyListener(this.onKeyListener);
    this.scrollViewLayout = ((LinearLayout)this.rootLayout.findViewById(2131165469));
    this.title = ((TextView)this.rootLayout.findViewById(2131165465));
    this.year = ((TextView)this.rootLayout.findViewById(2131165466));
    this.throbber = ((ProgressBar)this.rootLayout.findViewById(2131165241));
    this.back = ((TextView)this.rootLayout.findViewById(2131165471));
    this.trailer = ((TextView)this.rootLayout.findViewById(2131165472));
    this.watch = ((TextView)this.rootLayout.findViewById(2131165473));
    this.back.setOnClickListener(this.onClickListener);
    this.trailer.setOnClickListener(this.onClickListener);
    this.watch.setOnClickListener(this.onClickListener);
    this.fakeButtonManager = new FakeFocusButtonManager(null);
    this.fakeButtonManager.addButton(new FakeFocusButton(this.back));
    return this.rootLayout;
  }

  public void onResume()
  {
    super.onResume();
    this.fakeButtonManager.restoreFocus();
    Trackers.instance().track("/movie/info", "Movie Info - " + this.movieTitle);
  }

  private static class FakeFocusButton
  {
    private boolean isFakeFocused;
    private final TextView view;

    FakeFocusButton(TextView paramTextView)
    {
      this.view = paramTextView;
    }

    void clearFakeFocus()
    {
      TextView localTextView = this.view;
      this.isFakeFocused = false;
      localTextView.setPressed(false);
    }

    void fakeFocus()
    {
      TextView localTextView = this.view;
      this.isFakeFocused = true;
      localTextView.setPressed(true);
    }

    boolean isFakeFocused()
    {
      return this.isFakeFocused;
    }

    void performClick()
    {
      this.view.performClick();
    }
  }

  private static class FakeFocusButtonManager
  {
    private final List<MovieDetailFragment.FakeFocusButton> buttons = new ArrayList();
    private int currFocusPos = -1;

    void addButton(MovieDetailFragment.FakeFocusButton paramFakeFocusButton)
    {
      this.buttons.add(paramFakeFocusButton);
    }

    boolean canMoveFocusLeft()
    {
      return this.currFocusPos > 0;
    }

    boolean canMoveFocusRight()
    {
      return 1 + this.currFocusPos < this.buttons.size();
    }

    void loseFocusToLeft()
    {
      ((MovieDetailFragment.FakeFocusButton)this.buttons.get(0)).clearFakeFocus();
      this.currFocusPos = -1;
    }

    boolean moveFocusLeft()
    {
      if (canMoveFocusLeft())
      {
        ((MovieDetailFragment.FakeFocusButton)this.buttons.get(this.currFocusPos)).clearFakeFocus();
        List localList = this.buttons;
        int i = -1 + this.currFocusPos;
        this.currFocusPos = i;
        ((MovieDetailFragment.FakeFocusButton)localList.get(i)).fakeFocus();
        return true;
      }
      return false;
    }

    boolean moveFocusRight()
    {
      if (canMoveFocusRight())
      {
        if (this.currFocusPos > -1)
          ((MovieDetailFragment.FakeFocusButton)this.buttons.get(this.currFocusPos)).clearFakeFocus();
        List localList = this.buttons;
        int i = 1 + this.currFocusPos;
        this.currFocusPos = i;
        ((MovieDetailFragment.FakeFocusButton)localList.get(i)).fakeFocus();
        return true;
      }
      return false;
    }

    void performClick()
    {
      ((MovieDetailFragment.FakeFocusButton)this.buttons.get(this.currFocusPos)).performClick();
    }

    void restoreFocus()
    {
      Iterator localIterator = this.buttons.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
          return;
        MovieDetailFragment.FakeFocusButton localFakeFocusButton = (MovieDetailFragment.FakeFocusButton)localIterator.next();
        if (localFakeFocusButton.isFakeFocused())
          localFakeFocusButton.fakeFocus();
      }
    }

    void setFocusRightmost()
    {
      this.currFocusPos = (-1 + this.buttons.size());
      ((MovieDetailFragment.FakeFocusButton)this.buttons.get(this.currFocusPos)).fakeFocus();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.MovieDetailFragment
 * JD-Core Version:    0.6.2
 */