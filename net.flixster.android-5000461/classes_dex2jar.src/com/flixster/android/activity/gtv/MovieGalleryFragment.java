package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.MovieDaoNew;
import com.flixster.android.model.NamedList;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.gtv.MovieGallery;
import com.flixster.android.view.gtv.MovieGallery.OnMovieClickListener;
import com.flixster.android.view.gtv.Subheader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;

@TargetApi(11)
public abstract class MovieGalleryFragment extends InstanceStateFragment
{
  private static final String KEY_SELECTED_POSITIONS = "KEY_SELECTED_POSITIONS";
  protected final String className = getClass().getName();
  protected final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      MovieGalleryFragment.this.throbber.setVisibility(8);
      if ((paramAnonymousMessage.obj instanceof DaoException))
        ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, (Main)MovieGalleryFragment.this.getActivity());
    }
  };
  private List<MovieGallery<?>> galleries;
  protected final Collection<NamedList<Movie>> movies = new ArrayList();
  private int[] positions;
  protected final Collection<NamedList<LockerRight>> rights = new ArrayList();
  protected LinearLayout rootLayout;
  protected final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxMain", MovieGalleryFragment.this.className + ".successHandler");
      Activity localActivity = MovieGalleryFragment.this.getActivity();
      if ((localActivity == null) || (MovieGalleryFragment.this.isRemoving()))
        return;
      MovieGalleryFragment.this.throbber.setVisibility(8);
      MovieGalleryFragment.this.rootLayout.removeAllViews();
      MovieGalleryFragment.this.galleries = new ArrayList();
      Iterator localIterator2;
      if (MovieGalleryFragment.this.movies.size() > 0)
      {
        localIterator2 = MovieGalleryFragment.this.movies.iterator();
        if (localIterator2.hasNext());
      }
      while (true)
      {
        MovieGalleryFragment.this.focusSelf();
        return;
        NamedList localNamedList2 = (NamedList)localIterator2.next();
        String str2 = localNamedList2.getName();
        List localList2 = localNamedList2.getList();
        if (localList2.size() == 0)
          break;
        if (str2 != null)
        {
          Subheader localSubheader2 = new Subheader(localActivity);
          localSubheader2.setText(MovieGalleryFragment.this.createSectionTitle(localNamedList2, localActivity));
          MovieGalleryFragment.this.rootLayout.addView(localSubheader2);
        }
        MovieGallery localMovieGallery2 = new MovieGallery(localActivity);
        localMovieGallery2.load(localList2, new MovieGalleryFragment.DefaultMovieClickListener(MovieGalleryFragment.this, null));
        MovieGalleryFragment.this.rootLayout.addView(localMovieGallery2);
        MovieGalleryFragment.this.galleries.add(localMovieGallery2);
        break;
        if (MovieGalleryFragment.this.rights.size() > 0)
        {
          Iterator localIterator1 = MovieGalleryFragment.this.rights.iterator();
          while (localIterator1.hasNext())
          {
            NamedList localNamedList1 = (NamedList)localIterator1.next();
            String str1 = localNamedList1.getName();
            List localList1 = localNamedList1.getList();
            if (localList1.size() != 0)
            {
              if (str1 != null)
              {
                Subheader localSubheader1 = new Subheader(localActivity);
                localSubheader1.setText(MovieGalleryFragment.this.createSectionTitle(localNamedList1, localActivity));
                MovieGalleryFragment.this.rootLayout.addView(localSubheader1);
              }
              MovieGallery localMovieGallery1 = new MovieGallery(localActivity);
              localMovieGallery1.load(localList1, new MovieGalleryFragment.DefaultMovieClickListener(MovieGalleryFragment.this, null));
              MovieGalleryFragment.this.rootLayout.addView(localMovieGallery1);
              MovieGalleryFragment.this.galleries.add(localMovieGallery1);
            }
          }
        }
      }
    }
  };
  protected ProgressBar throbber;

  private void focusSelf()
  {
    if ((this.galleries != null) && (this.galleries.size() > 0) && (this.positions != null))
    {
      ((MovieGallery)this.galleries.get(0)).requestFocus();
      ((MovieGallery)this.galleries.get(0)).setSelectedItem(this.positions[0]);
    }
  }

  protected String createSectionTitle(NamedList<?> paramNamedList, Context paramContext)
  {
    return paramContext.getString(Integer.valueOf(paramNamedList.getName()).intValue());
  }

  protected abstract String getAnalyticsTag();

  protected abstract String getAnalyticsTitle();

  protected abstract Type getType();

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    Logger.d("FlxMain", this.className + ".onActivityCreated");
    Bundle localBundle = getSavedInstanceState();
    if (localBundle != null)
      this.positions = localBundle.getIntArray("KEY_SELECTED_POSITIONS");
    Logger.d("FlxMain", this.className + ".onActivityCreated saved positions: " + this.positions);
    if (this.movies.size() > 0)
    {
      this.successHandler.handleMessage(null);
      return;
    }
    switch ($SWITCH_TABLE$com$flixster$android$activity$gtv$MovieGalleryFragment$Type()[getType().ordinal()])
    {
    default:
      return;
    case 2:
      this.throbber.setVisibility(0);
      MovieDaoNew.fetchBoxOffice(this.movies, this.successHandler, this.errorHandler);
      return;
    case 3:
      this.throbber.setVisibility(0);
      MovieDaoNew.fetchUpcoming(this.movies, this.successHandler, this.errorHandler);
      return;
    case 4:
    }
    this.throbber.setVisibility(0);
    MovieDaoNew.fetchDvd(this.movies, this.successHandler, this.errorHandler);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", this.className + ".onCreate");
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", this.className + ".onCreateView");
    View localView = paramLayoutInflater.inflate(2130903125, paramViewGroup, false);
    this.rootLayout = ((LinearLayout)localView.findViewById(2131165492));
    this.throbber = ((ProgressBar)localView.findViewById(2131165241));
    return localView;
  }

  public void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", this.className + ".onDestroy");
    resetFragment();
  }

  public void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", this.className + ".onResume");
    Trackers.instance().track(getAnalyticsTag(), getAnalyticsTitle());
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if ((this.galleries != null) && (this.galleries.size() > 0))
    {
      this.positions = new int[this.galleries.size()];
      for (int i = 0; ; i++)
      {
        if (i >= this.galleries.size())
        {
          Logger.d("FlxMain", this.className + ".onSaveInstanceState selected positions: " + this.positions);
          paramBundle.putIntArray("KEY_SELECTED_POSITIONS", this.positions);
          return;
        }
        this.positions[i] = ((MovieGallery)this.galleries.get(i)).getSelectedItemPosition();
      }
    }
    this.positions = null;
  }

  protected void resetFragment()
  {
    this.movies.clear();
    this.rights.clear();
    this.galleries = null;
    if (this.rootLayout != null)
      this.rootLayout.removeAllViews();
  }

  private class DefaultMovieClickListener<T>
    implements MovieGallery.OnMovieClickListener<T>
  {
    private DefaultMovieClickListener()
    {
    }

    public void onClick(T paramT)
    {
      Main localMain = (Main)MovieGalleryFragment.this.getActivity();
      Bundle localBundle = new Bundle();
      if ((paramT instanceof Movie))
      {
        localBundle.putLong("net.flixster.android.EXTRA_ID", ((Movie)paramT).getId());
        localBundle.putString("net.flixster.android.EXTRA_TITLE", ((Movie)paramT).getTitle());
      }
      while (true)
      {
        localMain.startFragment(MovieDetailFragment.class, localBundle);
        return;
        if (!(paramT instanceof LockerRight))
          break;
        localBundle.putLong("net.flixster.android.EXTRA_ID", ((LockerRight)paramT).assetId);
        localBundle.putLong("net.flixster.android.EXTRA_RIGHT_ID", ((LockerRight)paramT).rightId);
        localBundle.putString("net.flixster.android.EXTRA_TITLE", ((LockerRight)paramT).getTitle());
      }
      throw new IllegalStateException();
    }
  }

  protected static enum Type
  {
    static
    {
      BOX_OFFICE = new Type("BOX_OFFICE", 1);
      UPCOMING = new Type("UPCOMING", 2);
      DVD = new Type("DVD", 3);
      SEARCH = new Type("SEARCH", 4);
      Type[] arrayOfType = new Type[5];
      arrayOfType[0] = STORE;
      arrayOfType[1] = BOX_OFFICE;
      arrayOfType[2] = UPCOMING;
      arrayOfType[3] = DVD;
      arrayOfType[4] = SEARCH;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.MovieGalleryFragment
 * JD-Core Version:    0.6.2
 */