package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Fragment;
import java.util.Map.Entry;

@TargetApi(11)
public class ResultPassingFragment extends Fragment
{
  public static final int RESULT_CANCELED = 0;
  public static final int RESULT_OK = -1;

  protected void onFragmentResult(String paramString, int paramInt)
  {
  }

  public void onResume()
  {
    super.onResume();
    ResultPassingFragmentActivity localResultPassingFragmentActivity = (ResultPassingFragmentActivity)getActivity();
    if (localResultPassingFragmentActivity.hasFragmentResult())
    {
      Map.Entry localEntry = localResultPassingFragmentActivity.getAndClearFragmentResult();
      onFragmentResult((String)localEntry.getKey(), ((Integer)localEntry.getValue()).intValue());
    }
  }

  protected final void setFragmentResult(int paramInt)
  {
    ((ResultPassingFragmentActivity)getActivity()).setFragmentResult(getClass().getName(), paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.ResultPassingFragment
 * JD-Core Version:    0.6.2
 */