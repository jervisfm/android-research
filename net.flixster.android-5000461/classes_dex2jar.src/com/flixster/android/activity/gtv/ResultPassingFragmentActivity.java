package com.flixster.android.activity.gtv;

import com.flixster.android.activity.common.DecoratedActivity;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class ResultPassingFragmentActivity extends DecoratedActivity
{
  private final HashMap<String, Integer> fragmentResultMap = new HashMap();

  public Map.Entry<String, Integer> getAndClearFragmentResult()
  {
    synchronized (this.fragmentResultMap)
    {
      boolean bool = hasFragmentResult();
      Map.Entry localEntry = null;
      if (bool)
      {
        localEntry = (Map.Entry)this.fragmentResultMap.entrySet().iterator().next();
        this.fragmentResultMap.clear();
      }
      return localEntry;
    }
  }

  public boolean hasFragmentResult()
  {
    return this.fragmentResultMap.size() > 0;
  }

  public void setFragmentResult(String paramString, int paramInt)
  {
    synchronized (this.fragmentResultMap)
    {
      this.fragmentResultMap.clear();
      this.fragmentResultMap.put(paramString, Integer.valueOf(paramInt));
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.ResultPassingFragmentActivity
 * JD-Core Version:    0.6.2
 */