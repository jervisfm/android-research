package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.MovieDaoNew;
import com.flixster.android.model.NamedList;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.gtv.Subheader;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterApplication;

@TargetApi(11)
public class SearchFragment extends MovieGalleryFragment
{
  private EditText editText;
  private boolean isEditTextFocusedInitially;
  private final View.OnKeyListener onKeyListener = new View.OnKeyListener()
  {
    public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      if ((paramAnonymousKeyEvent.getAction() == 1) && (paramAnonymousInt == 66))
      {
        SearchFragment.this.query = SearchFragment.this.editText.getText().toString().trim();
        if (("FLX".equalsIgnoreCase(SearchFragment.this.query)) || ("REINDEER FLOTILLA".equalsIgnoreCase(SearchFragment.this.query)) || ("UUDDLRLRBA".equalsIgnoreCase(SearchFragment.this.query)))
        {
          FlixsterApplication.setAdminState(1);
          Toast.makeText(SearchFragment.this.getActivity(), 2131493307, 0).show();
        }
        if ("DGNS".equalsIgnoreCase(SearchFragment.this.query))
        {
          FlixsterApplication.enableDiagnosticMode();
          Toast.makeText(SearchFragment.this.getActivity(), 2131493308, 0).show();
        }
        SearchFragment.this.rootLayout.removeAllViews();
        SearchFragment.this.throbber.setVisibility(0);
        MovieDaoNew.searchMovie(SearchFragment.this.query, SearchFragment.this.movies, SearchFragment.this.searchSuccessHandler, SearchFragment.this.errorHandler);
        Trackers.instance().track(SearchFragment.this.getAnalyticsTag(), SearchFragment.this.getAnalyticsTitle());
        return true;
      }
      return false;
    }
  };
  private String query;
  private final Handler searchSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Activity localActivity = SearchFragment.this.getActivity();
      if ((localActivity == null) || (SearchFragment.this.isRemoving()));
      NamedList localNamedList;
      do
      {
        return;
        SearchFragment.this.successHandler.handleMessage(null);
        localNamedList = (NamedList)SearchFragment.this.movies.iterator().next();
      }
      while (localNamedList.getList().size() != 0);
      Subheader localSubheader = new Subheader(localActivity);
      localSubheader.setText(SearchFragment.this.createSectionTitle(localNamedList, localActivity));
      SearchFragment.this.rootLayout.addView(localSubheader);
    }
  };

  protected String createSectionTitle(NamedList<?> paramNamedList, Context paramContext)
  {
    String str1 = paramNamedList.getName();
    List localList = paramNamedList.getList();
    String str2 = paramContext.getString(Integer.valueOf(str1).intValue());
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(localList.size());
    return String.format(str2, arrayOfObject);
  }

  protected String getAnalyticsTag()
  {
    if (this.query == null)
      return "/search";
    return "/search/movie/results";
  }

  protected String getAnalyticsTitle()
  {
    if (this.query == null)
      return "Search";
    return "Search Movie Results - " + this.query;
  }

  protected MovieGalleryFragment.Type getType()
  {
    return MovieGalleryFragment.Type.SEARCH;
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", this.className + ".onCreateView");
    View localView = paramLayoutInflater.inflate(2130903158, paramViewGroup, false);
    this.rootLayout = ((LinearLayout)localView.findViewById(2131165492));
    this.throbber = ((ProgressBar)localView.findViewById(2131165241));
    this.editText = ((EditText)localView.findViewById(2131165747));
    this.editText.setOnKeyListener(this.onKeyListener);
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    if (!this.isEditTextFocusedInitially)
    {
      this.isEditTextFocusedInitially = true;
      this.editText.requestFocus();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.SearchFragment
 * JD-Core Version:    0.6.2
 */