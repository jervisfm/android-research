package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import net.flixster.android.FlixsterApplication;

@TargetApi(11)
public class SettingsDialogFragment extends DialogFragment
  implements View.OnClickListener
{
  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165749:
    }
    FlixsterApplication.setCaptionsEnabled(((CheckBox)paramView).isChecked());
  }

  public Dialog onCreateDialog(Bundle paramBundle)
  {
    setStyle(1, 0);
    Dialog localDialog = super.onCreateDialog(paramBundle);
    localDialog.setTitle(2131492888);
    return localDialog;
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903160, paramViewGroup);
    CheckBox localCheckBox = (CheckBox)localView.findViewById(2131165749);
    localCheckBox.setChecked(FlixsterApplication.getCaptionsEnabled());
    localCheckBox.setOnClickListener(this);
    return localView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.SettingsDialogFragment
 * JD-Core Version:    0.6.2
 */