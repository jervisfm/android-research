package com.flixster.android.activity.gtv;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.flixster.android.data.AccountManager;
import com.flixster.android.model.NamedList;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.gtv.Subheader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.User;

@TargetApi(11)
public class StoreFragment extends MovieGalleryFragment
  implements View.OnClickListener
{
  private final Handler fetchUserHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((StoreFragment.this.getActivity() == null) || (StoreFragment.this.isRemoving()));
      while (true)
      {
        return;
        StoreFragment.this.throbber.setVisibility(8);
        if (AccountManager.instance().hasUserSession())
        {
          StoreFragment.this.displayUserHeader();
          StoreFragment.this.fetchCollectedMovies();
        }
        while ((paramAnonymousMessage != null) && ((paramAnonymousMessage.obj instanceof DaoException)))
        {
          Logger.d("FlxMain", "StoreFragment.fetchUserHandler", (DaoException)paramAnonymousMessage.obj);
          return;
          StoreFragment.this.showEmptyCollectionPromo(true);
        }
      }
    }
  };
  private final DialogBuilder.DialogListener logoutDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      AccountManager.instance().fbLogout(StoreFragment.this.getActivity());
      StoreFragment.this.resetFragment();
      StoreFragment.this.showEmptyCollectionPromo(true);
      StoreFragment.this.rootView.findViewById(2131165419).requestFocus();
    }
  };
  private View rootView;
  private final Handler storeSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if ((StoreFragment.this.getActivity() == null) || (StoreFragment.this.isRemoving()))
        return;
      StoreFragment.this.rights.clear();
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = AccountManager.instance().getUser().getLockerRights().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          NamedList localNamedList = new NamedList(String.valueOf(2131492977), localArrayList);
          StoreFragment.this.rights.add(localNamedList);
          StoreFragment.this.successHandler.handleMessage(null);
          StoreFragment.this.userRefresh.setVisibility(0);
          if (((NamedList)StoreFragment.this.rights.iterator().next()).getList().size() != 0)
            break;
          StoreFragment.this.showEmptyCollectionPromo(false);
          return;
        }
        LockerRight localLockerRight = (LockerRight)localIterator.next();
        if (((localLockerRight.isMovie()) && ((localLockerRight.isStreamingSupported) || (localLockerRight.isDownloadSupported))) || ((localLockerRight.isSeason()) && (!localLockerRight.isRental)))
          localArrayList.add(localLockerRight);
      }
    }
  };
  private TextView userGreeting;
  private RelativeLayout userHeader;
  private ImageView userPicture;
  private TextView userRefresh;

  private void displayUserHeader()
  {
    User localUser = AccountManager.instance().getUser();
    TextView localTextView = this.userGreeting;
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = localUser.firstName;
    localTextView.setText(localResources.getString(2131493229, arrayOfObject));
    Bitmap localBitmap = localUser.getProfileBitmap(this.userPicture);
    if (localBitmap != null)
      this.userPicture.setImageBitmap(localBitmap);
    this.userHeader.setVisibility(0);
  }

  private void fetchCollectedMovies()
  {
    if (this.rights.size() > 0)
    {
      this.storeSuccessHandler.handleMessage(null);
      return;
    }
    this.throbber.setVisibility(0);
    this.userRefresh.setVisibility(8);
    ProfileDao.getUserLockerRights(this.storeSuccessHandler, this.errorHandler);
  }

  private void showEmptyCollectionPromo(boolean paramBoolean)
  {
    Subheader localSubheader = (Subheader)this.rootView.findViewById(2131165263);
    ImageView localImageView = (ImageView)this.rootView.findViewById(2131165264);
    TextView localTextView = (TextView)this.rootView.findViewById(2131165265);
    Button localButton = (Button)this.rootView.findViewById(2131165419);
    if (paramBoolean)
    {
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
      localLayoutParams.setMargins(0, 0, 0, 0);
      localSubheader.setLayoutParams(localLayoutParams);
      localSubheader.setText(2131493283);
      localTextView.setText(Html.fromHtml(getString(2131493285)));
      localButton.setVisibility(0);
      localButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          ((Main)StoreFragment.this.getActivity()).startFragment(LoginDialogFragment.class, null);
        }
      });
    }
    while (true)
    {
      localSubheader.setVisibility(0);
      localImageView.setVisibility(0);
      localTextView.setVisibility(0);
      return;
      localSubheader.setText(2131493282);
      localTextView.setText(Html.fromHtml(getString(2131493284)));
    }
  }

  protected String createSectionTitle(NamedList<?> paramNamedList, Context paramContext)
  {
    String str1 = paramNamedList.getName();
    List localList = paramNamedList.getList();
    String str2 = paramContext.getString(Integer.valueOf(str1).intValue());
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(localList.size());
    return String.format(str2, arrayOfObject);
  }

  protected String getAnalyticsTag()
  {
    return "/collection";
  }

  protected String getAnalyticsTitle()
  {
    return "Collection";
  }

  protected MovieGalleryFragment.Type getType()
  {
    return MovieGalleryFragment.Type.STORE;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    if (AccountManager.instance().hasUserSession())
    {
      this.fetchUserHandler.handleMessage(null);
      return;
    }
    this.throbber.setVisibility(0);
    ProfileDao.fetchUser(this.fetchUserHandler, this.fetchUserHandler);
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165782:
    case 2131165783:
    }
    do
    {
      do
      {
        do
          return;
        while (!AccountManager.instance().hasUserSession());
        AccountManager.instance().getUser().resetLockerRights();
        this.rights.clear();
        this.rootLayout.removeAllViews();
        fetchCollectedMovies();
        return;
      }
      while (!AccountManager.instance().hasUserSession());
      if (AccountManager.instance().isPlatformFlixster())
      {
        ((Main)getActivity()).showDialog(1000000100, this.logoutDialogListener);
        return;
      }
    }
    while (!AccountManager.instance().isPlatformFacebook());
    ((Main)getActivity()).showDialog(1000000101, this.logoutDialogListener);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", this.className + ".onCreateView");
    this.rootView = paramLayoutInflater.inflate(2130903168, paramViewGroup, false);
    this.rootLayout = ((LinearLayout)this.rootView.findViewById(2131165492));
    this.throbber = ((ProgressBar)this.rootView.findViewById(2131165241));
    this.userHeader = ((RelativeLayout)this.rootView.findViewById(2131165779));
    this.userPicture = ((ImageView)this.rootView.findViewById(2131165780));
    this.userGreeting = ((TextView)this.rootView.findViewById(2131165781));
    this.userRefresh = ((TextView)this.rootView.findViewById(2131165782));
    TextView localTextView = (TextView)this.rootView.findViewById(2131165783);
    this.userRefresh.setClickable(true);
    this.userRefresh.setFocusable(true);
    this.userRefresh.setOnClickListener(this);
    localTextView.setClickable(true);
    localTextView.setFocusable(true);
    localTextView.setOnClickListener(this);
    return this.rootView;
  }

  protected void resetFragment()
  {
    super.resetFragment();
    if (this.userHeader != null)
      this.userHeader.setVisibility(8);
    if (this.userPicture != null)
      this.userPicture.setImageResource(2130837978);
    if (this.userGreeting != null)
      this.userGreeting.setText("Hello!");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.StoreFragment
 * JD-Core Version:    0.6.2
 */