package com.flixster.android.activity.gtv;

public class UpcomingFragment extends MovieGalleryFragment
{
  protected String getAnalyticsTag()
  {
    return "/upcoming";
  }

  protected String getAnalyticsTitle()
  {
    return "Upcoming Movies";
  }

  protected MovieGalleryFragment.Type getType()
  {
    return MovieGalleryFragment.Type.UPCOMING;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.UpcomingFragment
 * JD-Core Version:    0.6.2
 */