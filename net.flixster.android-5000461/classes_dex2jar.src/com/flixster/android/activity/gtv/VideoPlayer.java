package com.flixster.android.activity.gtv;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.VersionedViewHelper;
import net.flixster.android.FlixsterApplication;

public class VideoPlayer extends Activity
  implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener
{
  public static final String KEY_LANDSCAPE = "KEY_LANDSCAPE";
  public static final String KEY_URL = "KEY_URL";
  private int currPosition;
  private boolean isLandscape;
  private String path;
  private ProgressBar throbber;
  private VideoView videoView;

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.isLandscape = localBundle.getBoolean("KEY_LANDSCAPE");
      this.path = localBundle.getString("KEY_URL");
      Logger.d("FlxMain", "VideoPlayer.onCreate path:" + this.path);
    }
    if (this.isLandscape)
    {
      if (FlixsterApplication.getAndroidBuildInt() >= 9)
        break label176;
      setRequestedOrientation(0);
    }
    while (true)
    {
      VersionedViewHelper.hideSystemNavigation(this);
      setContentView(2130903185);
      this.videoView = ((VideoView)findViewById(2131165917));
      this.throbber = ((ProgressBar)findViewById(2131165241));
      this.videoView.setVideoPath(this.path);
      this.videoView.setMediaController(new MediaController(this));
      this.videoView.setOnPreparedListener(this);
      this.videoView.setOnCompletionListener(this);
      this.videoView.setOnErrorListener(this);
      this.videoView.requestFocus();
      return;
      label176: setRequestedOrientation(6);
    }
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    return false;
  }

  protected void onPause()
  {
    super.onPause();
    if (this.videoView.isPlaying())
    {
      this.currPosition = this.videoView.getCurrentPosition();
      this.videoView.pause();
    }
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    this.throbber.setVisibility(8);
    this.videoView.start();
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if ((paramBoolean) && (this.videoView.isPlaying()))
      VersionedViewHelper.hideSystemNavigation(this);
    if (paramBoolean);
    try
    {
      if ((!this.videoView.isPlaying()) && (this.currPosition > 0))
      {
        this.videoView.start();
        if (this.videoView.getCurrentPosition() < this.currPosition)
        {
          this.videoView.seekTo(this.currPosition);
          this.currPosition = 0;
        }
      }
      return;
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "VideoPlayer.onWindowFocusChanged exception", localException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.gtv.VideoPlayer
 * JD-Core Version:    0.6.2
 */