package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;

@TargetApi(11)
public class ActorBiographyFragment extends LviFragment
{
  private static final int DIALOG_ACTOR = 2;
  private static final int DIALOG_NETWORK_FAIL = 1;
  public static final String KEY_ACTOR_ID = "ACTOR_ID";
  public static final String KEY_ACTOR_NAME = "ACTOR_NAME";
  private Actor actor;
  private long actorId;
  private final Handler actorThumbnailHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (ActorBiographyFragment.this.isRemoving());
      ImageView localImageView;
      Actor localActor;
      do
      {
        do
        {
          return;
          localImageView = (ImageView)paramAnonymousMessage.obj;
        }
        while (localImageView == null);
        localActor = (Actor)localImageView.getTag();
      }
      while (localActor == null);
      localImageView.setImageBitmap(localActor.bitmap);
      localImageView.invalidate();
    }
  };
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)ActorBiographyFragment.this.getActivity() == null) || (ActorBiographyFragment.this.isRemoving()));
      do
      {
        return;
        Logger.d("FlxMain", "ActorBiographyPage.updateHandler");
      }
      while (ActorBiographyFragment.this.actor == null);
      ActorBiographyFragment.this.updatePage();
    }
  };

  public static int getFlixFragId()
  {
    return 104;
  }

  public static ActorBiographyFragment newInstance(Bundle paramBundle)
  {
    ActorBiographyFragment localActorBiographyFragment = new ActorBiographyFragment();
    localActorBiographyFragment.setArguments(paramBundle);
    return localActorBiographyFragment;
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local4 = new TimerTask()
    {
      public void run()
      {
        if (ActorBiographyFragment.this.isRemoving())
          return;
        if (ActorBiographyFragment.this.actor == null);
        try
        {
          ActorBiographyFragment.this.actor = ActorDao.getActor(ActorBiographyFragment.this.actorId);
          ActorBiographyFragment.this.updateHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "ActorBiographyPage.scheduleUpdatePageTask (failed to get actor data)", localDaoException);
            ActorBiographyFragment.this.actor = null;
          }
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local4, 100L);
  }

  private void updatePage()
  {
    ImageView localImageView;
    TextView localTextView1;
    label124: TextView localTextView2;
    label193: TextView localTextView3;
    TextView localTextView4;
    if (this.actor != null)
    {
      LinearLayout localLinearLayout = (LinearLayout)getView().findViewById(2131165224);
      localImageView = (ImageView)localLinearLayout.findViewById(2131165227);
      if (this.actor.bitmap == null)
        break label308;
      localImageView.setImageBitmap(this.actor.bitmap);
      ((TextView)localLinearLayout.findViewById(2131165228)).setText(this.actor.name);
      localTextView1 = (TextView)localLinearLayout.findViewById(2131165229);
      if (this.actor.birthDay == null)
        break label378;
      localTextView1.setText("Birthday: " + this.actor.birthDay);
      localTextView1.setVisibility(0);
      localTextView2 = (TextView)localLinearLayout.findViewById(2131165230);
      if ((this.actor.birthplace == null) || ("".equals(this.actor.birthplace)))
        break label388;
      localTextView2.setText("Birthplace: " + this.actor.birthplace);
      localTextView2.setVisibility(0);
      localTextView3 = (TextView)localLinearLayout.findViewById(2131165231);
      localTextView4 = (TextView)localLinearLayout.findViewById(2131165232);
      Logger.d("FlxMain", "ActorBiography.updatePage() actor.biography:" + this.actor.biography);
      if ((this.actor.biography == null) || (this.actor.biography.length() <= 0))
        break label398;
      Logger.d("FlxMain", "ActorBiography.updatePage() non null");
      localTextView3.setVisibility(0);
      localTextView4.setVisibility(0);
      localTextView4.setText(this.actor.biography);
      Logger.d("FlxMain", "ActorBiography.updatePage() text set");
    }
    while (true)
    {
      trackPage();
      return;
      label308: String str = this.actor.largeUrl;
      if (str != null)
      {
        localImageView.setImageResource(2130837841);
        localImageView.setTag(this.actor);
        ImageOrder localImageOrder = new ImageOrder(4, this.actor, str, localImageView, this.actorThumbnailHandler);
        ((Main)getActivity()).orderImage(localImageOrder);
        break;
      }
      localImageView.setImageResource(2130837978);
      break;
      label378: localTextView1.setVisibility(8);
      break label124;
      label388: localTextView2.setVisibility(8);
      break label193;
      label398: localTextView3.setVisibility(8);
      localTextView4.setVisibility(8);
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog((Main)getActivity());
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog((Main)getActivity());
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder((Main)getActivity());
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ActorBiographyFragment.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903062, null);
    if (this.actorId == 0L)
    {
      Bundle localBundle = getArguments();
      this.actorId = localBundle.getLong("ACTOR_ID");
      if (localBundle.getString("ACTOR_NAME") != null)
      {
        String str = localBundle.getString("ACTOR_NAME");
        ((TextView)localView.findViewById(2131165228)).setText(str);
      }
    }
    return localView;
  }

  public void onResume()
  {
    Logger.d("FlxMain", "ActorBiographyPage.onResume");
    super.onResume();
    scheduleUpdatePageTask();
  }

  public void trackPage()
  {
    Trackers.instance().track("/actor/bio", "Actor Actor Page for actor:" + this.actor.id);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.ActorBiographyFragment
 * JD-Core Version:    0.6.2
 */