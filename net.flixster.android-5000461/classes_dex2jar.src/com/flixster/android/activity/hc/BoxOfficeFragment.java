package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.model.Movie;
import net.flixster.android.model.MoviePopcornComparator;
import net.flixster.android.model.MovieRottenScoreComparator;
import net.flixster.android.model.MovieTitleComparator;

@TargetApi(11)
public class BoxOfficeFragment extends LviFragment
{
  public static final String LISTSTATE_SORT = "LISTSTATE_SORT";
  public final int SORT_ALPHA = 3;
  public final int SORT_POPULAR = 1;
  public final int SORT_RATING = 2;
  public final int SORT_START = 0;
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)BoxOfficeFragment.this.getActivity();
      if ((localMain == null) || (BoxOfficeFragment.this.isRemoving()));
      while (true)
      {
        return;
        long l = 0L;
        if (!BoxOfficeFragment.this.mBoxOfficeFeatured.isEmpty())
          l = ((Movie)BoxOfficeFragment.this.mBoxOfficeFeatured.get(0)).getId();
        while (l != 0L)
        {
          Intent localIntent = new Intent("DETAILS", null, localMain, MovieDetailsFragment.class);
          localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", l);
          localMain.startFragment(localIntent, MovieDetailsFragment.class);
          return;
          if (!BoxOfficeFragment.this.mBoxOfficeOtw.isEmpty())
            l = ((Movie)BoxOfficeFragment.this.mBoxOfficeOtw.get(0)).getId();
          else if (!BoxOfficeFragment.this.mBoxOfficeTbo.isEmpty())
            l = ((Movie)BoxOfficeFragment.this.mBoxOfficeTbo.get(0)).getId();
        }
      }
    }
  };
  private final ArrayList<Movie> mBoxOfficeFeatured = new ArrayList();
  private final ArrayList<Movie> mBoxOfficeOthers = new ArrayList();
  private final ArrayList<Movie> mBoxOfficeOtw = new ArrayList();
  private final ArrayList<Movie> mBoxOfficeTbo = new ArrayList();
  private int mSortOption = 1;

  private void ScheduleLoadItemsTask(final int paramInt, long paramLong)
  {
    try
    {
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)BoxOfficeFragment.this.getActivity();
          if ((localMain == null) || (BoxOfficeFragment.this.isRemoving()))
            return;
          Logger.d("FlxMain", "BoxOfficePage.ScheduleLoadItemsTask.run sortOption:" + paramInt);
          while (true)
          {
            try
            {
              if (BoxOfficeFragment.this.mBoxOfficeOtw.isEmpty())
              {
                ArrayList localArrayList1 = BoxOfficeFragment.this.mBoxOfficeFeatured;
                ArrayList localArrayList2 = BoxOfficeFragment.this.mBoxOfficeOtw;
                ArrayList localArrayList3 = BoxOfficeFragment.this.mBoxOfficeTbo;
                ArrayList localArrayList4 = BoxOfficeFragment.this.mBoxOfficeOthers;
                int i = BoxOfficeFragment.this.mRetryCount;
                boolean bool = false;
                if (i == 0)
                  bool = true;
                MovieDao.fetchBoxOffice(localArrayList1, localArrayList2, localArrayList3, localArrayList4, bool);
              }
              switch (paramInt)
              {
              default:
                BoxOfficeFragment.this.trackPage();
                BoxOfficeFragment.this.mUpdateHandler.sendEmptyMessage(0);
                if (localMain != null)
                  localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                if (!BoxOfficeFragment.this.isInitialContentLoaded)
                {
                  BoxOfficeFragment.this.isInitialContentLoaded = true;
                  BoxOfficeFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
                }
                return;
              case 1:
                BoxOfficeFragment.this.mSortOption = 1;
                BoxOfficeFragment.this.setPopularLviList();
                continue;
              case 2:
              case 3:
              }
            }
            catch (DaoException localDaoException)
            {
              Logger.e("FlxMain", "BoxOfficePage.ScheduleLoadItemsTask.run DaoException", localDaoException);
              BoxOfficeFragment.this.retryLogic(localDaoException);
              return;
              BoxOfficeFragment.this.mSortOption = 2;
              BoxOfficeFragment.this.setRatingLviList();
              continue;
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null) && (localMain.mLoadingDialog.isShowing()))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
            BoxOfficeFragment.this.mSortOption = 3;
            BoxOfficeFragment.this.setAlphaLviList();
          }
        }
      };
      Main localMain = (Main)getActivity();
      if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
        localMain.mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
    }
  }

  public static int getFlixFragId()
  {
    return 1;
  }

  private void setAlphaLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(this.mBoxOfficeFeatured);
    localArrayList.addAll(this.mBoxOfficeOtw);
    localArrayList.addAll(this.mBoxOfficeTbo);
    localArrayList.addAll(this.mBoxOfficeOthers);
    Collections.sort(localArrayList, new MovieTitleComparator());
    this.mDataHolder.clear();
    char c1 = '\000';
    Iterator localIterator = localArrayList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
      }
      Movie localMovie = (Movie)localIterator.next();
      char c2 = localMovie.getProperty("title").charAt(0);
      if (!Character.isLetter(c2))
        c2 = '#';
      if (c1 != c2)
      {
        LviSubHeader localLviSubHeader = new LviSubHeader();
        localLviSubHeader.mTitle = String.valueOf(c2);
        this.mDataHolder.add(localLviSubHeader);
        c1 = c2;
      }
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localMovie;
      localLviMovie.mTrailerClick = getTrailerOnClickListener();
      this.mDataHolder.add(localLviMovie);
    }
  }

  private void setPopularLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    this.mDataHolder.clear();
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.add(ListHelper.clone(this.mBoxOfficeFeatured));
    localArrayList1.add(ListHelper.clone(this.mBoxOfficeOtw));
    localArrayList1.add(ListHelper.clone(this.mBoxOfficeTbo));
    localArrayList1.add(ListHelper.clone(this.mBoxOfficeOthers));
    String[] arrayOfString = new String[4];
    arrayOfString[0] = getResources().getString(2131493054);
    arrayOfString[1] = getResources().getString(2131493055);
    arrayOfString[2] = getResources().getString(2131493056);
    arrayOfString[3] = getResources().getString(2131493057);
    Iterator localIterator;
    for (int i = 0; ; i++)
    {
      if (i >= 4)
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
      }
      ArrayList localArrayList2 = (ArrayList)localArrayList1.get(i);
      if (!localArrayList2.isEmpty())
      {
        LviSubHeader localLviSubHeader = new LviSubHeader();
        localLviSubHeader.mTitle = arrayOfString[i];
        this.mDataHolder.add(localLviSubHeader);
        localIterator = localArrayList2.iterator();
        if (localIterator.hasNext())
          break;
      }
    }
    Movie localMovie = (Movie)localIterator.next();
    LviMovie localLviMovie = new LviMovie();
    localLviMovie.mMovie = localMovie;
    localLviMovie.mTrailerClick = getTrailerOnClickListener();
    if (i == 0);
    for (boolean bool = true; ; bool = false)
    {
      localLviMovie.mIsFeatured = bool;
      this.mDataHolder.add(localLviMovie);
      break;
    }
  }

  private void setRatingLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    ArrayList localArrayList = new ArrayList();
    localArrayList.addAll(this.mBoxOfficeFeatured);
    localArrayList.addAll(this.mBoxOfficeOtw);
    localArrayList.addAll(this.mBoxOfficeTbo);
    localArrayList.addAll(this.mBoxOfficeOthers);
    long l;
    Iterator localIterator;
    if (FlixsterApplication.getMovieRatingType() == 1)
    {
      Collections.sort(localArrayList, new MovieRottenScoreComparator());
      this.mDataHolder.clear();
      l = 0L;
      localIterator = localArrayList.iterator();
    }
    while (true)
    {
      if (!localIterator.hasNext())
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
        Collections.sort(localArrayList, new MoviePopcornComparator());
        break;
      }
      Movie localMovie = (Movie)localIterator.next();
      if (l != localMovie.getId())
      {
        l = localMovie.getId();
        LviMovie localLviMovie = new LviMovie();
        localLviMovie.mMovie = localMovie;
        localLviMovie.mTrailerClick = getTrailerOnClickListener();
        this.mDataHolder.add(localLviMovie);
      }
    }
  }

  public void onResume()
  {
    super.onResume();
    ScheduleLoadItemsTask(this.mSortOption, 100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(this.mSortOption, 1000L);
  }

  public void trackPage()
  {
    switch (this.mSortOption)
    {
    default:
      return;
    case 1:
      Trackers.instance().track("/boxoffice/popular", "Box Office - Popular");
      return;
    case 2:
      Trackers.instance().track("/boxoffice/rating", "Box Office - Rating");
      return;
    case 3:
    }
    Trackers.instance().track("/boxoffice/title", "Box Office - Title");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.BoxOfficeFragment
 * JD-Core Version:    0.6.2
 */