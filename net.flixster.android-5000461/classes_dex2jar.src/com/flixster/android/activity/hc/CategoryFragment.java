package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.model.Movie;

@TargetApi(11)
public class CategoryFragment extends LviFragment
{
  public static final String CATEGORY_FILTER = "CATEGORY_FILTER";
  public static final String CATEGORY_TITLE = "CATEGORY_TITLE";
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)CategoryFragment.this.getActivity();
      if ((localMain == null) || (CategoryFragment.this.isRemoving()));
      while (CategoryFragment.this.mCategory.isEmpty())
        return;
      long l = ((Movie)CategoryFragment.this.mCategory.get(0)).getId();
      Intent localIntent = new Intent("DETAILS", null, localMain, MovieDetailsFragment.class);
      localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", l);
      localMain.startFragment(localIntent, MovieDetailsFragment.class);
    }
  };
  private final ArrayList<Movie> mCategory = new ArrayList();
  private String mCategoryFilter = "&filter=flixster-top-100";
  private String mCategoryTitle = "Flixster Top 100";

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)CategoryFragment.this.getActivity();
          if ((localMain == null) || (CategoryFragment.this.isRemoving()));
          do
            while (true)
            {
              return;
              try
              {
                if (CategoryFragment.this.mCategory.isEmpty())
                {
                  ArrayList localArrayList = CategoryFragment.this.mCategory;
                  String str = CategoryFragment.this.mCategoryFilter;
                  if (CategoryFragment.this.mRetryCount == 0)
                  {
                    bool = true;
                    MovieDao.fetchDvdCategory(localArrayList, str, bool);
                  }
                }
                else
                {
                  CategoryFragment.this.setMovieLviList();
                  CategoryFragment.this.mUpdateHandler.sendEmptyMessage(0);
                  if (localMain != null)
                    localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                  if (CategoryFragment.this.isInitialContentLoaded)
                    continue;
                  CategoryFragment.this.isInitialContentLoaded = true;
                  CategoryFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
                  return;
                }
              }
              catch (DaoException localDaoException)
              {
                while (true)
                {
                  Logger.w("FlxMain", "CategoryPage.ScheduleLoadItemsTask.run() mRetryCount:" + CategoryFragment.this.mRetryCount);
                  localDaoException.printStackTrace();
                  CategoryFragment localCategoryFragment = CategoryFragment.this;
                  localCategoryFragment.mRetryCount = (1 + localCategoryFragment.mRetryCount);
                  if (CategoryFragment.this.mRetryCount >= 3)
                    break;
                  CategoryFragment.this.ScheduleLoadItemsTask(1000L);
                  return;
                  boolean bool = false;
                }
                CategoryFragment.this.mRetryCount = 0;
              }
            }
          while (localMain == null);
          localMain.mRemoveDialogHandler.sendEmptyMessage(1);
          localMain.mShowDialogHandler.sendEmptyMessage(2);
        }
      };
      Logger.d("FlxMain", "CategoryPage.ScheduleLoadItemsTask() loading item");
      if (((Main)getActivity()).mPageTimer != null)
        ((Main)getActivity()).mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static int getFlixFragId()
  {
    return 8;
  }

  public static CategoryFragment newInstance(Bundle paramBundle)
  {
    CategoryFragment localCategoryFragment = new CategoryFragment();
    localCategoryFragment.setArguments(paramBundle);
    return localCategoryFragment;
  }

  private void setMovieLviList()
  {
    Logger.d("FlxMain", "CategoryPage.setPopularLviList ");
    this.mDataHolder.clear();
    LviSubHeader localLviSubHeader = new LviSubHeader();
    localLviSubHeader.mTitle = this.mCategoryTitle;
    this.mDataHolder.add(localLviSubHeader);
    Iterator localIterator = this.mCategory.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
      }
      Movie localMovie = (Movie)localIterator.next();
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localMovie;
      localLviMovie.mTrailerClick = getTrailerOnClickListener();
      this.mDataHolder.add(localLviMovie);
    }
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    Bundle localBundle = getArguments();
    if (localBundle != null)
    {
      this.mCategoryFilter = localBundle.getString("CATEGORY_FILTER");
      this.mCategoryTitle = localBundle.getString("CATEGORY_TITLE");
      return localView;
    }
    Logger.e("FlxMain", "CategoryPage.onCreate Missing the bundle extras... ");
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    trackPage();
    ScheduleLoadItemsTask(100L);
  }

  public void trackPage()
  {
    Trackers.instance().track("/dvds/browse/genre", "Category " + this.mCategoryTitle);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.CategoryFragment
 * JD-Core Version:    0.6.2
 */