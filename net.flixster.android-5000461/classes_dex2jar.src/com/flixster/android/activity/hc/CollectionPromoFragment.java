package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.utils.Logger;

@TargetApi(11)
public class CollectionPromoFragment extends FlixsterFragment
{
  protected final String className = getClass().getName();

  public static int getFlixFragId()
  {
    return 130;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", this.className + ".onCreate");
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", this.className + ".onCreateView");
    View localView = paramLayoutInflater.inflate(2130903069, paramViewGroup, false);
    TextView localTextView1 = (TextView)localView.findViewById(2131165263);
    TextView localTextView2 = (TextView)localView.findViewById(2131165265);
    if (AccountFacade.getSocialUser() == null)
      localTextView1.setText(2131493283);
    while (true)
    {
      localTextView2.setText(Html.fromHtml(getString(2131493285)));
      return localView;
      localTextView1.setText(2131493282);
    }
  }

  public void trackPage()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.CollectionPromoFragment
 * JD-Core Version:    0.6.2
 */