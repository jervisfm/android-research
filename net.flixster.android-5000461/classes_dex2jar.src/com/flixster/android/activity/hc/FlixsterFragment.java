package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder;
import java.util.Calendar;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.ads.AdView;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.util.DialogUtils;

@TargetApi(11)
public abstract class FlixsterFragment extends Fragment
{
  protected static final int ACTIVITY_RESULT_EMAIL = 0;
  protected static final int ACTIVITY_RESULT_INFO = 1;
  public static final String BUNDLE = "BUNDLE";
  public static final int DIALOGKEY_DATESELECT = 6;
  public static final int DIALOGKEY_LOADING = 1;
  public static final int DIALOGKEY_NETWORKERROR = 2;
  public static final int FRAGMENTID_ACTOR = 103;
  public static final int FRAGMENTID_ACTORBIO = 104;
  public static final int FRAGMENTID_BOXOFFICELIST = 1;
  public static final int FRAGMENTID_COLLECTION_PROMO = 130;
  public static final int FRAGMENTID_DVDLIST = 4;
  public static final int FRAGMENTID_DVD_CATEGORY_LIST = 8;
  public static final int FRAGMENTID_FLIXSTER = 0;
  public static final int FRAGMENTID_MOVIEDETAILS = 108;
  public static final int FRAGMENTID_MYMOVIECOLLECTION = 10;
  public static final int FRAGMENTID_MYMOVIESLIST = 5;
  public static final int FRAGMENTID_NETFLIXAUTH = 109;
  public static final int FRAGMENTID_NETFLIXQUEUE = 6;
  public static final int FRAGMENTID_REVIEW = 106;
  public static final int FRAGMENTID_SEARCH = 7;
  public static final int FRAGMENTID_SHOWTIMES = 101;
  public static final int FRAGMENTID_THEATERINFO = 102;
  public static final int FRAGMENTID_THEATERLIST = 2;
  public static final int FRAGMENTID_TICKETINFO = 105;
  public static final int FRAGMENTID_TOP_ACTORS = 9;
  public static final int FRAGMENTID_TOP_NEWS = 122;
  public static final int FRAGMENTID_TOP_PHOTOS = 120;
  public static final int FRAGMENTID_UPCOMINGLIST = 3;
  public static final int FRAGMENTID_USERREVIEW = 107;
  public static final String INITIAL_CONTENT_LOADED = "INITIAL_CONTENT_LOADED";
  protected static final String KEY_PLATFORM_ID = "PLATFORM_ID";
  public static final String LISTSTATE_POSITION = "LISTSTATE_POSITION";
  protected static final int SWIPE_MAX_OFF_PATH = 250;
  protected static final int SWIPE_MIN_DISTANCE = 120;
  protected static final int SWIPE_THRESHOLD_VELOCITY = 200;
  protected boolean isInitialContentLoaded;
  protected AdView mListAd;
  protected int mRetryCount = 0;
  protected AdView mStickyBottomAd;
  protected AdView mStickyTopAd;
  private String stackTag;

  public static int getFlixFragId()
  {
    return 0;
  }

  protected void NetworkAttemptsCancelled()
  {
  }

  protected String getStackTag()
  {
    return this.stackTag;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.i("FlxMain", getClass().getSimpleName() + ".onCreate " + toString());
    if ((!this.isInitialContentLoaded) && ((paramBundle == null) || (!paramBundle.getBoolean("INITIAL_CONTENT_LOADED"))));
    for (boolean bool = false; ; bool = true)
    {
      this.isInitialContentLoaded = bool;
      return;
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 3:
    case 4:
    case 5:
    default:
      return null;
    case 1:
      if (Properties.instance().shouldShowSplash())
      {
        Properties.instance().splashShown();
        ((Main)getActivity()).mLoadingDialog = DialogBuilder.createDialog(getActivity(), 999999999);
        return ((Main)getActivity()).mLoadingDialog;
      }
      ((Main)getActivity()).mLoadingDialog = new ProgressDialog(getActivity());
      ((ProgressDialog)((Main)getActivity()).mLoadingDialog).setMessage(getResources().getString(2131493173));
      ((ProgressDialog)((Main)getActivity()).mLoadingDialog).setIndeterminate(true);
      ((Main)getActivity()).mLoadingDialog.setCancelable(true);
      ((Main)getActivity()).mLoadingDialog.setCanceledOnTouchOutside(true);
      return ((Main)getActivity()).mLoadingDialog;
    case 6:
      return new AlertDialog.Builder(getActivity()).setTitle(getResources().getString(2131493167)).setItems(DialogUtils.getShowtimesDateOptions(getActivity()), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT which:" + paramAnonymousInt);
          Calendar localCalendar = Calendar.getInstance();
          localCalendar.add(5, paramAnonymousInt);
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT date:" + localCalendar.getTime());
          FlixsterApplication.setShowtimesDate(localCalendar.getTime());
          Trackers.instance().track("/showtimes/selectDate", "Select Date");
          FlixsterFragment.this.onResume();
          ((Main)FlixsterFragment.this.getActivity()).removeDialog(6);
        }
      }).create();
    case 2:
    }
    return new AlertDialog.Builder(getActivity()).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        FlixsterFragment.this.onResume();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  public void onHiddenChanged(boolean paramBoolean)
  {
    if (!paramBoolean)
      trackPage();
  }

  public void onLowMemory()
  {
    super.onLowMemory();
    Logger.w("FlxMain", "Low memory!");
    ProfileDao.clearBitmapCache();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.i("FlxMain", getClass().getSimpleName() + ".onSaveInstanceState initialContentLoaded=" + this.isInitialContentLoaded);
    paramBundle.putBoolean("INITIAL_CONTENT_LOADED", this.isInitialContentLoaded);
  }

  protected void retryAction()
  {
  }

  protected void retryLogic(DaoException paramDaoException)
  {
    if (!FlixsterApplication.isConnected())
    {
      this.mRetryCount = 0;
      ErrorDialog.handleException(paramDaoException, (Main)getActivity());
      return;
    }
    if (this.mRetryCount < 3)
    {
      this.mRetryCount = (1 + this.mRetryCount);
      retryAction();
      return;
    }
    this.mRetryCount = 0;
    ErrorDialog.handleException(paramDaoException, (Main)getActivity());
  }

  protected void setStackTag(String paramString)
  {
    this.stackTag = paramString;
  }

  public abstract void trackPage();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.FlixsterFragment
 * JD-Core Version:    0.6.2
 */