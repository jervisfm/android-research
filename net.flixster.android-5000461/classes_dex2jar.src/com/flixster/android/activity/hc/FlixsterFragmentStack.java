package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import com.flixster.android.utils.Logger;
import java.util.Stack;

@TargetApi(11)
public class FlixsterFragmentStack
{
  private static final String TAG_PREFIX = "FFS";
  private final FragmentManager fm;
  private final Stack<String> stack;
  private final String uid;

  protected FlixsterFragmentStack(FragmentManager paramFragmentManager, String[] paramArrayOfString)
  {
    this.fm = paramFragmentManager;
    this.uid = String.valueOf(hashCode());
    this.stack = new Stack();
    int i;
    if (paramArrayOfString != null)
    {
      logTags(paramArrayOfString);
      i = paramArrayOfString.length;
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      String str = paramArrayOfString[j];
      this.stack.push(str);
    }
  }

  private String generateStackTag()
  {
    return "FFS" + this.stack.size() + "_" + this.uid;
  }

  private static void logTags(String[] paramArrayOfString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = paramArrayOfString.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        Logger.i("FlxMain", "FFS " + localStringBuilder.toString());
        return;
      }
      localStringBuilder.append(paramArrayOfString[j] + ",");
    }
  }

  public FlixsterFragment elementAt(int paramInt)
  {
    FlixsterFragment localFlixsterFragment;
    try
    {
      boolean bool = this.stack.empty();
      localFlixsterFragment = null;
      if (!bool)
      {
        String str = (String)this.stack.elementAt(paramInt);
        localFlixsterFragment = (FlixsterFragment)this.fm.findFragmentByTag(str);
        if (localFlixsterFragment == null)
          throw new IllegalStateException("FlixsterFragmentStack out of sync with FragmentManager");
      }
    }
    finally
    {
    }
    return localFlixsterFragment;
  }

  public boolean isEmpty()
  {
    return this.stack.isEmpty();
  }

  public FlixsterFragment peek()
  {
    FlixsterFragment localFlixsterFragment;
    try
    {
      boolean bool = this.stack.empty();
      localFlixsterFragment = null;
      if (!bool)
      {
        String str = (String)this.stack.peek();
        localFlixsterFragment = (FlixsterFragment)this.fm.findFragmentByTag(str);
        if (localFlixsterFragment == null)
          throw new IllegalStateException("FlixsterFragmentStack out of sync with FragmentManager");
      }
    }
    finally
    {
    }
    return localFlixsterFragment;
  }

  protected String[] persist()
  {
    try
    {
      String[] arrayOfString = new String[this.stack.size()];
      this.stack.toArray(arrayOfString);
      logTags(arrayOfString);
      return arrayOfString;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public FlixsterFragment pop()
  {
    FlixsterFragment localFlixsterFragment;
    try
    {
      boolean bool = this.stack.empty();
      localFlixsterFragment = null;
      if (!bool)
      {
        String str = (String)this.stack.pop();
        localFlixsterFragment = (FlixsterFragment)this.fm.findFragmentByTag(str);
        if (localFlixsterFragment == null)
          throw new IllegalStateException("FlixsterFragmentStack out of sync with FragmentManager");
      }
    }
    finally
    {
    }
    return localFlixsterFragment;
  }

  public String push(FlixsterFragment paramFlixsterFragment)
  {
    try
    {
      String str = generateStackTag();
      paramFlixsterFragment.setStackTag(str);
      this.stack.push(str);
      return str;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public int size()
  {
    return this.stack.size();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.FlixsterFragmentStack
 * JD-Core Version:    0.6.2
 */