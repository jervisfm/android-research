package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import net.flixster.android.ActorPage;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.TicketInfoPage;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviAdapter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviTheater.TheaterItemHolder;
import net.flixster.android.model.Actor;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Season;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

@TargetApi(11)
public abstract class LviFragment extends FlixsterFragment
{
  public static final String LISTSTATE_NAV = "LISTSTATE_NAV";
  private View.OnClickListener actorClickListener;
  protected ArrayList<Lvi> mData;
  protected ArrayList<Lvi> mDataHolder;
  protected boolean mIsSelectedTheaterFavorite;
  protected int mLastListPosition;
  protected ListView mListView;
  protected BaseAdapter mLviListFragmentAdapter;
  private AdapterView.OnItemClickListener mMovieItemClickListener;
  protected int mPositionLast = 0;
  protected boolean mPositionRecover = false;
  protected Lvi mSelectedLvi = null;
  protected int mSelectedMovieIndex = 0;
  protected double mSelectedTheaterDistance;
  private View.OnClickListener mShowtimesClickListener = null;
  private View.OnClickListener mStarTheaterClickListener = null;
  private View.OnClickListener mTheaterClickListener = null;
  protected View.OnClickListener mTrailerClick;
  protected Handler mUpdateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)LviFragment.this.getActivity() == null) || (LviFragment.this.isRemoving()))
        return;
      Logger.d("FlxMain", "FlixsterLviActivity.mUpdateHandler mDataHolder.size():" + LviFragment.this.mDataHolder.size());
      LviFragment.this.mData.clear();
      LviFragment.this.mData.addAll(LviFragment.this.mDataHolder);
      LviFragment.this.mDataHolder.clear();
      LviFragment.this.mLviListFragmentAdapter.notifyDataSetChanged();
    }
  };

  protected View.OnClickListener getActorClickListener()
  {
    try
    {
      if (this.actorClickListener == null)
        this.actorClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Actor localActor = (Actor)paramAnonymousView.getTag();
            Intent localIntent;
            if (localActor != null)
            {
              localIntent = new Intent("TOP_ACTOR", null, LviFragment.this.getActivity(), ActorPage.class);
              localIntent.putExtra("ACTOR_ID", localActor.id);
              localIntent.putExtra("ACTOR_NAME", localActor.name);
              if (LviFragment.this.mLastListPosition >= localActor.positionId)
                break label104;
              ((Main)LviFragment.this.getActivity()).startFragment(localIntent, ActorFragment.class, 1, LviFragment.this.getClass());
            }
            while (true)
            {
              LviFragment.this.mLastListPosition = localActor.positionId;
              return;
              label104: if (LviFragment.this.mLastListPosition > localActor.positionId)
                ((Main)LviFragment.this.getActivity()).startFragment(localIntent, ActorFragment.class, 3, LviFragment.this.getClass());
            }
          }
        };
      View.OnClickListener localOnClickListener = this.actorClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected AdapterView.OnItemClickListener getMovieItemClickListener()
  {
    try
    {
      if (this.mMovieItemClickListener == null)
        this.mMovieItemClickListener = new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            Lvi localLvi = (Lvi)LviFragment.this.mData.get(paramAnonymousInt);
            if (localLvi.getItemViewType() == Lvi.VIEW_TYPE_MOVIE)
            {
              if (LviFragment.this.mSelectedLvi != null)
                LviFragment.this.mSelectedLvi.setSelected(false);
              LviFragment.this.mSelectedLvi = localLvi;
              LviFragment.this.mSelectedLvi.setSelected(true);
              int i = LviFragment.this.mSelectedMovieIndex;
              LviFragment.this.mSelectedMovieIndex = paramAnonymousInt;
              Movie localMovie = ((LviMovie)localLvi).mMovie;
              LockerRight localLockerRight = ((LviMovie)localLvi).right;
              if (localMovie == null)
                break label274;
              LviFragment.this.mPositionLast = LviFragment.this.mListView.getFirstVisiblePosition();
              LviFragment.this.mPositionRecover = true;
              Intent localIntent2 = new Intent(LviFragment.this.getActivity(), MovieDetailsFragment.class);
              localIntent2.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
              if ((localMovie instanceof Season))
                localIntent2.putExtra("KEY_IS_SEASON", true);
              if (localLockerRight != null)
                localIntent2.putExtra("KEY_RIGHT_ID", localLockerRight.rightId);
              if (paramAnonymousInt < i)
                ((Main)LviFragment.this.getActivity()).startFragment(localIntent2, MovieDetailsFragment.class, 3, LviFragment.this.getClass());
              if (paramAnonymousInt > i)
                ((Main)LviFragment.this.getActivity()).startFragment(localIntent2, MovieDetailsFragment.class, 1, LviFragment.this.getClass());
            }
            while (true)
            {
              ((BaseAdapter)paramAnonymousAdapterView.getAdapter()).notifyDataSetChanged();
              return;
              label274: if (FlixsterApplication.getMovieRatingType() == 1)
              {
                Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(LviFragment.this.getActivity().getResources().getString(2131492948)));
                LviFragment.this.startActivity(localIntent1);
              }
            }
          }
        };
      AdapterView.OnItemClickListener localOnItemClickListener = this.mMovieItemClickListener;
      return localOnItemClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getShowtimesClickListener()
  {
    try
    {
      if (this.mShowtimesClickListener == null)
        this.mShowtimesClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Logger.d("FlxMain", "FlixsterLviActivity.getShowtimesClickListener() pow!");
            LviShowtimes localLviShowtimes = (LviShowtimes)paramAnonymousView.getTag();
            if (localLviShowtimes != null)
            {
              Intent localIntent = new Intent(LviFragment.this.getActivity(), TicketInfoPage.class);
              localIntent.putExtra("MOVIE_ID", localLviShowtimes.mShowtimes.mMovieId);
              localIntent.putExtra("id", localLviShowtimes.mTheater.getId());
              localIntent.putExtra("SHOWTIMES_TITLE", localLviShowtimes.mShowtimes.mShowtimesTitle);
              ((Main)LviFragment.this.getActivity()).startFragment(localIntent, TicketInfoFragment.class);
            }
          }
        };
      View.OnClickListener localOnClickListener = this.mShowtimesClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getStarTheaterClickListener()
  {
    try
    {
      if (this.mStarTheaterClickListener == null)
        this.mStarTheaterClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            CheckBox localCheckBox = (CheckBox)paramAnonymousView;
            Theater localTheater = (Theater)paramAnonymousView.getTag();
            String str;
            if (localTheater != null)
            {
              str = Long.toString(localTheater.getId());
              if (!localCheckBox.isChecked())
                break label62;
              Logger.v("FlxMain", "TheaterInfoList.StarTheaterListener() selected");
              FlixsterApplication.addFavoriteTheater(str);
            }
            while (true)
            {
              ((Main)LviFragment.this.getActivity()).peekListBackStack().onResume();
              return;
              label62: Logger.v("FlxMain", "TheaterInfoList.StarTheaterListener() UNselected");
              FlixsterApplication.removeFavoriteTheater(str);
            }
          }
        };
      View.OnClickListener localOnClickListener = this.mStarTheaterClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getTheaterClickListener()
  {
    try
    {
      if (this.mTheaterClickListener == null)
      {
        Logger.d("FlxMain", "LviFragment.getTheaterClickListener() fresh");
        this.mTheaterClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Logger.d("FlxMain", "LviFragment.getTheaterClickListener().onClick()");
            LviTheater.TheaterItemHolder localTheaterItemHolder = (LviTheater.TheaterItemHolder)paramAnonymousView.getTag();
            double d;
            boolean bool1;
            Intent localIntent;
            int i;
            if (localTheaterItemHolder != null)
            {
              if (LviFragment.this.mSelectedLvi != null)
                LviFragment.this.mSelectedLvi.setSelected(false);
              LviFragment.this.mSelectedLvi = localTheaterItemHolder.lviTheater;
              LviFragment.this.mSelectedLvi.setSelected(true);
              long l = localTheaterItemHolder.theater.getId();
              d = LviFragment.this.mSelectedTheaterDistance;
              LviFragment.this.mSelectedTheaterDistance = localTheaterItemHolder.theater.getMiles();
              bool1 = LviFragment.this.mIsSelectedTheaterFavorite;
              LviFragment.this.mIsSelectedTheaterFavorite = FlixsterApplication.getFavoriteTheatersList().containsKey(Long.toString(l));
              localIntent = new Intent(LviFragment.this.getActivity(), TheaterInfoFragment.class);
              localIntent.putExtra("net.flixster.android.EXTRA_THEATER_ID", l);
              if ((!LviFragment.this.mIsSelectedTheaterFavorite) || (bool1))
                break label201;
              i = 3;
            }
            while (true)
            {
              ((Main)LviFragment.this.getActivity()).startFragment(localIntent, TheaterInfoFragment.class, i, LviFragment.this.getClass());
              LviFragment.this.mLviListFragmentAdapter.notifyDataSetChanged();
              return;
              label201: if ((!LviFragment.this.mIsSelectedTheaterFavorite) && (bool1))
              {
                i = 1;
              }
              else if (LviFragment.this.mSelectedTheaterDistance > d)
              {
                i = 1;
              }
              else
              {
                boolean bool2 = LviFragment.this.mSelectedTheaterDistance < d;
                i = 0;
                if (bool2)
                  i = 3;
              }
            }
          }
        };
      }
      View.OnClickListener localOnClickListener = this.mTheaterClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected View.OnClickListener getTrailerOnClickListener()
  {
    try
    {
      if (this.mTrailerClick == null)
        this.mTrailerClick = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            LviFragment.this.mPositionLast = LviFragment.this.mListView.getFirstVisiblePosition();
            LviFragment.this.mPositionRecover = true;
            Logger.d("FlxMain", "BoxOfficePage - mTrailerClick.onClick");
            Movie localMovie = (Movie)paramAnonymousView.getTag();
            if ((localMovie != null) && (localMovie.hasTrailer()))
              Starter.launchTrailer(localMovie, LviFragment.this.getActivity());
          }
        };
      View.OnClickListener localOnClickListener = this.mTrailerClick;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903109, null);
    this.mListView = ((ListView)localView.findViewById(2131165428));
    this.mListView.setDividerHeight(0);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mData = new ArrayList();
    this.mDataHolder = new ArrayList();
    this.mLviListFragmentAdapter = new LviAdapter(getActivity(), this.mData);
    this.mListView.setAdapter(this.mLviListFragmentAdapter);
    return localView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.LviFragment
 * JD-Core Version:    0.6.2
 */