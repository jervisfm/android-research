package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Images.Media;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.bootstrap.Startup;
import com.flixster.android.utils.AppRater;
import com.flixster.android.utils.ImageTask;
import com.flixster.android.utils.ImageTaskImpl;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.lang.ref.SoftReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.SettingsPage;
import net.flixster.android.Starter;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Photo;
import net.flixster.android.util.DialogUtils;
import net.flixster.android.util.ErrorHandler;

@TargetApi(11)
public class Main extends DecoratedActivity
  implements ImageTask, ActionBar.TabListener
{
  public static final int DIALOGKEY_ADSENSEHACK = 7;
  public static final int DIALOGKEY_APIDOWN = 3;
  public static final int DIALOGKEY_DATESELECT = 6;
  public static final int DIALOGKEY_LOADING = 1;
  public static final int DIALOGKEY_NETWORKERROR = 2;
  public static final int DIALOGKEY_SORTSELECT = 5;
  public static final int DIALOGKEY_UNKOWNERROR = 4;
  public static final int FRAGMENT_TYPE_CONTENT = 2;
  public static final int FRAGMENT_TYPE_LIST = 1;
  private static final String KEY_CONTENT_FRAGMENT_STACK = "KEY_CONTENT_FRAGMENT_STACK";
  private static final String KEY_LAST_TAB_INDEX = "KEY_LAST_TAB_INDEX";
  private static final String KEY_LIST_FRAGMENT_STACK = "KEY_LIST_FRAGMENT_STACK";
  public static final String KEY_MOVIE_ID = "net.flixster.android.EXTRA_MOVIE_ID";
  public static final String KEY_THEATER_ID = "net.flixster.android.EXTRA_THEATER_ID";
  public static final int PANEL_CHILD = 0;
  public static final int PANEL_PARENT = 2;
  public static final int PANEL_SIBLING = 1;
  public static final int[] RATING_LARGE_R = { 2130837917, 2130837918, 2130837919, 2130837920, 2130837921, 2130837922, 2130837923, 2130837924, 2130837925, 2130837926, 2130837927, 2130837869, 2130837865 };
  public static final int[] RATING_SMALL_R = { 2130837928, 2130837929, 2130837930, 2130837931, 2130837932, 2130837933, 2130837934, 2130837935, 2130837936, 2130837937, 2130837938, 2130837866, 2130837862 };
  public static final int SLIDE_HINT_DOWN = 3;
  public static final int SLIDE_HINT_LEFT = 4;
  public static final int SLIDE_HINT_NONE = 0;
  public static final int SLIDE_HINT_RIGHT = 2;
  public static final int SLIDE_HINT_UP = 1;
  private View mActionBarView;
  private FlixsterFragmentStack mContentFragmentStack;
  private FlixFragTab mCurrTab;
  private View.OnClickListener mDateSelectOnClickListener = null;
  private FlixsterFragmentStack mListFragmentStack;
  public Dialog mLoadingDialog;
  protected Timer mPageTimer;
  protected Handler mRemoveDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!Main.this.isFinishing())
        Main.this.removeDialog(paramAnonymousMessage.what);
    }
  };
  protected Handler mShowDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (!Main.this.isFinishing())
        Main.this.showDialog(paramAnonymousMessage.what);
    }
  };
  private View.OnClickListener mStartSettingsClickListener = null;
  private boolean skipRatePrompt;

  private void createActionBar(int paramInt)
  {
    ActionBar localActionBar = getActionBar();
    FlixFragTab[] arrayOfFlixFragTab = FlixFragTab.values();
    int i = arrayOfFlixFragTab.length;
    int j = 0;
    if (j >= i)
    {
      this.mActionBarView = getLayoutInflater().inflate(2130903060, null);
      localActionBar.setCustomView(this.mActionBarView);
      localActionBar.setDisplayOptions(17);
      localActionBar.setNavigationMode(2);
      localActionBar.setDisplayShowHomeEnabled(true);
      return;
    }
    FlixFragTab localFlixFragTab = arrayOfFlixFragTab[j];
    ActionBar.Tab localTab;
    if ((localFlixFragTab.text != 0) && (localFlixFragTab.iconId != 0))
    {
      localTab = localActionBar.newTab().setText(localFlixFragTab.text).setTabListener(this);
      localTab.setIcon(getResources().getDrawable(localFlixFragTab.iconId)).setTag(localFlixFragTab);
      if (localFlixFragTab.ordinal() != paramInt)
        break label151;
    }
    label151: for (boolean bool = true; ; bool = false)
    {
      localActionBar.addTab(localTab, bool);
      j++;
      break;
    }
  }

  private static boolean isContentFragment(Class<? extends FlixsterFragment> paramClass)
  {
    try
    {
      int i = ((Integer)paramClass.getMethod("getFlixFragId", null).invoke(null, new Object[0])).intValue();
      boolean bool = false;
      if (i > 100)
        bool = true;
      return bool;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      localNoSuchMethodException.printStackTrace();
      return false;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localIllegalArgumentException.printStackTrace();
      return false;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
      return false;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      localInvocationTargetException.printStackTrace();
    }
    return false;
  }

  private void removeFragment(FlixsterFragmentStack paramFlixsterFragmentStack, int paramInt1, int paramInt2)
  {
    if (paramFlixsterFragmentStack.size() > 1)
    {
      FlixsterFragment localFlixsterFragment1 = paramFlixsterFragmentStack.pop();
      FlixsterFragment localFlixsterFragment2 = paramFlixsterFragmentStack.peek();
      FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
      localFragmentTransaction.setCustomAnimations(paramInt1, paramInt2);
      localFragmentTransaction.show(localFlixsterFragment2);
      localFragmentTransaction.remove(localFlixsterFragment1);
      localFragmentTransaction.commit();
    }
  }

  private void resetContentBackStack(FragmentTransaction paramFragmentTransaction)
  {
    Logger.i("FlxMain", "Main.resetContentBackStack count=" + this.mContentFragmentStack.size());
    while (true)
    {
      if (this.mContentFragmentStack.isEmpty())
        return;
      tryRemove(paramFragmentTransaction, this.mContentFragmentStack.pop());
    }
  }

  private void resetListBackStack(FragmentTransaction paramFragmentTransaction)
  {
    Logger.i("FlxMain", "Main.resetListBackStack count=" + this.mListFragmentStack.size());
    while (true)
    {
      if (this.mListFragmentStack.isEmpty())
        return;
      tryRemove(paramFragmentTransaction, this.mListFragmentStack.pop());
    }
  }

  private void showDiagnosticsOrRateDialog()
  {
    if (Properties.instance().hasUserSessionExpired())
      showDialog(1000000904, null);
    do
    {
      return;
      final Intent localIntent = ErrorHandler.instance().checkForAppCrash(getApplicationContext());
      if (localIntent != null)
      {
        this.skipRatePrompt = true;
        showDialog(1000001000, new DialogBuilder.DialogListener()
        {
          public void onNegativeButtonClick(int paramAnonymousInt)
          {
          }

          public void onNeutralButtonClick(int paramAnonymousInt)
          {
          }

          public void onPositiveButtonClick(int paramAnonymousInt)
          {
            Starter.tryLaunch(Main.this, localIntent);
          }
        });
        return;
      }
    }
    while (this.skipRatePrompt);
    AppRater.showRateDialog(this);
  }

  // ERROR //
  private void startFragment(Intent paramIntent, Class<? extends FlixsterFragment> paramClass1, int paramInt, Class<? extends FlixsterFragment> paramClass2, FragmentTransaction paramFragmentTransaction)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc_w 292
    //   5: new 294	java/lang/StringBuilder
    //   8: dup
    //   9: ldc_w 376
    //   12: invokespecial 299	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   15: aload_2
    //   16: invokevirtual 379	java/lang/Class:getSimpleName	()Ljava/lang/String;
    //   19: invokevirtual 382	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: invokevirtual 309	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   25: invokestatic 315	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   28: aload 4
    //   30: ifnull +220 -> 250
    //   33: aload 4
    //   35: invokestatic 384	com/flixster/android/activity/hc/Main:isContentFragment	(Ljava/lang/Class;)Z
    //   38: ifne +212 -> 250
    //   41: iconst_1
    //   42: istore 7
    //   44: aload_2
    //   45: invokestatic 384	com/flixster/android/activity/hc/Main:isContentFragment	(Ljava/lang/Class;)Z
    //   48: istore 8
    //   50: iload 8
    //   52: ifeq +298 -> 350
    //   55: aconst_null
    //   56: astore 21
    //   58: aload_2
    //   59: invokevirtual 388	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   62: checkcast 390	com/flixster/android/activity/hc/FlixsterFragment
    //   65: astore 21
    //   67: aload 21
    //   69: aload_1
    //   70: invokevirtual 396	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   73: invokevirtual 400	com/flixster/android/activity/hc/FlixsterFragment:setArguments	(Landroid/os/Bundle;)V
    //   76: aload 21
    //   78: ifnull +140 -> 218
    //   81: aload_0
    //   82: getfield 301	com/flixster/android/activity/hc/Main:mContentFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   85: invokevirtual 319	com/flixster/android/activity/hc/FlixsterFragmentStack:isEmpty	()Z
    //   88: istore 23
    //   90: aconst_null
    //   91: astore 24
    //   93: iload 23
    //   95: ifne +24 -> 119
    //   98: iload 7
    //   100: iconst_1
    //   101: if_icmpne +182 -> 283
    //   104: aload_0
    //   105: getfield 301	com/flixster/android/activity/hc/Main:mContentFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   108: invokevirtual 259	com/flixster/android/activity/hc/FlixsterFragmentStack:pop	()Lcom/flixster/android/activity/hc/FlixsterFragment;
    //   111: astore 24
    //   113: aload_0
    //   114: aload 5
    //   116: invokespecial 402	com/flixster/android/activity/hc/Main:resetContentBackStack	(Landroid/app/FragmentTransaction;)V
    //   119: aload_0
    //   120: getfield 301	com/flixster/android/activity/hc/Main:mContentFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   123: aload 21
    //   125: invokevirtual 406	com/flixster/android/activity/hc/FlixsterFragmentStack:push	(Lcom/flixster/android/activity/hc/FlixsterFragment;)Ljava/lang/String;
    //   128: pop
    //   129: iload 7
    //   131: iconst_1
    //   132: if_icmpne +193 -> 325
    //   135: iload_3
    //   136: tableswitch	default:+28 -> 164, 1:+159->295, 2:+28->164, 3:+174->310
    //   165: iconst_2
    //   166: ldc_w 407
    //   169: ldc_w 408
    //   172: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   175: pop
    //   176: aload 5
    //   178: ldc_w 409
    //   181: aload 21
    //   183: aload 21
    //   185: invokevirtual 412	com/flixster/android/activity/hc/FlixsterFragment:getStackTag	()Ljava/lang/String;
    //   188: invokevirtual 416	android/app/FragmentTransaction:add	(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    //   191: pop
    //   192: aload 5
    //   194: aload 21
    //   196: invokevirtual 282	android/app/FragmentTransaction:show	(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    //   199: pop
    //   200: aload 24
    //   202: ifnull +16 -> 218
    //   205: iload 7
    //   207: iconst_1
    //   208: if_icmpne +132 -> 340
    //   211: aload 5
    //   213: aload 24
    //   215: invokestatic 323	com/flixster/android/activity/hc/Main:tryRemove	(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    //   218: ldc_w 292
    //   221: new 294	java/lang/StringBuilder
    //   224: dup
    //   225: ldc_w 418
    //   228: invokespecial 299	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   231: aload_0
    //   232: getfield 301	com/flixster/android/activity/hc/Main:mContentFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   235: invokevirtual 255	com/flixster/android/activity/hc/FlixsterFragmentStack:size	()I
    //   238: invokevirtual 305	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   241: invokevirtual 309	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   244: invokestatic 315	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   247: aload_0
    //   248: monitorexit
    //   249: return
    //   250: iconst_0
    //   251: istore 7
    //   253: goto -209 -> 44
    //   256: astore 32
    //   258: aload 32
    //   260: invokevirtual 419	java/lang/InstantiationException:printStackTrace	()V
    //   263: goto -187 -> 76
    //   266: astore 6
    //   268: aload_0
    //   269: monitorexit
    //   270: aload 6
    //   272: athrow
    //   273: astore 22
    //   275: aload 22
    //   277: invokevirtual 247	java/lang/IllegalAccessException:printStackTrace	()V
    //   280: goto -204 -> 76
    //   283: aload_0
    //   284: getfield 301	com/flixster/android/activity/hc/Main:mContentFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   287: invokevirtual 262	com/flixster/android/activity/hc/FlixsterFragmentStack:peek	()Lcom/flixster/android/activity/hc/FlixsterFragment;
    //   290: astore 24
    //   292: goto -173 -> 119
    //   295: aload 5
    //   297: ldc_w 420
    //   300: ldc_w 421
    //   303: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   306: pop
    //   307: goto -131 -> 176
    //   310: aload 5
    //   312: ldc_w 422
    //   315: ldc_w 423
    //   318: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   321: pop
    //   322: goto -146 -> 176
    //   325: aload 5
    //   327: ldc_w 407
    //   330: ldc_w 408
    //   333: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   336: pop
    //   337: goto -161 -> 176
    //   340: aload 5
    //   342: aload 24
    //   344: invokestatic 426	com/flixster/android/activity/hc/Main:tryHide	(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    //   347: goto -129 -> 218
    //   350: aconst_null
    //   351: astore 9
    //   353: aload_2
    //   354: invokevirtual 388	java/lang/Class:newInstance	()Ljava/lang/Object;
    //   357: checkcast 390	com/flixster/android/activity/hc/FlixsterFragment
    //   360: astore 9
    //   362: aload 9
    //   364: aload_1
    //   365: invokevirtual 396	android/content/Intent:getExtras	()Landroid/os/Bundle;
    //   368: invokevirtual 400	com/flixster/android/activity/hc/FlixsterFragment:setArguments	(Landroid/os/Bundle;)V
    //   371: aload 9
    //   373: ifnull +137 -> 510
    //   376: aload_0
    //   377: getfield 328	com/flixster/android/activity/hc/Main:mListFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   380: invokevirtual 319	com/flixster/android/activity/hc/FlixsterFragmentStack:isEmpty	()Z
    //   383: istore 11
    //   385: aconst_null
    //   386: astore 12
    //   388: iload 11
    //   390: ifne +24 -> 414
    //   393: iload 7
    //   395: iconst_1
    //   396: if_icmpne +166 -> 562
    //   399: aload_0
    //   400: getfield 328	com/flixster/android/activity/hc/Main:mListFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   403: invokevirtual 259	com/flixster/android/activity/hc/FlixsterFragmentStack:pop	()Lcom/flixster/android/activity/hc/FlixsterFragment;
    //   406: astore 12
    //   408: aload_0
    //   409: aload 5
    //   411: invokespecial 428	com/flixster/android/activity/hc/Main:resetListBackStack	(Landroid/app/FragmentTransaction;)V
    //   414: aload_0
    //   415: getfield 328	com/flixster/android/activity/hc/Main:mListFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   418: aload 9
    //   420: invokevirtual 406	com/flixster/android/activity/hc/FlixsterFragmentStack:push	(Lcom/flixster/android/activity/hc/FlixsterFragment;)Ljava/lang/String;
    //   423: pop
    //   424: iload 7
    //   426: iconst_1
    //   427: if_icmpne +177 -> 604
    //   430: iload_3
    //   431: tableswitch	default:+25 -> 456, 1:+143->574, 2:+25->456, 3:+158->589
    //   457: iconst_2
    //   458: ldc_w 429
    //   461: ldc_w 430
    //   464: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   467: pop
    //   468: aload 5
    //   470: ldc_w 431
    //   473: aload 9
    //   475: aload 9
    //   477: invokevirtual 412	com/flixster/android/activity/hc/FlixsterFragment:getStackTag	()Ljava/lang/String;
    //   480: invokevirtual 416	android/app/FragmentTransaction:add	(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    //   483: pop
    //   484: aload 5
    //   486: aload 9
    //   488: invokevirtual 282	android/app/FragmentTransaction:show	(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    //   491: pop
    //   492: aload 12
    //   494: ifnull +16 -> 510
    //   497: iload 7
    //   499: iconst_1
    //   500: if_icmpne +119 -> 619
    //   503: aload 5
    //   505: aload 12
    //   507: invokestatic 323	com/flixster/android/activity/hc/Main:tryRemove	(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    //   510: ldc_w 292
    //   513: new 294	java/lang/StringBuilder
    //   516: dup
    //   517: ldc_w 433
    //   520: invokespecial 299	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   523: aload_0
    //   524: getfield 328	com/flixster/android/activity/hc/Main:mListFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   527: invokevirtual 255	com/flixster/android/activity/hc/FlixsterFragmentStack:size	()I
    //   530: invokevirtual 305	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   533: invokevirtual 309	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   536: invokestatic 315	com/flixster/android/utils/Logger:i	(Ljava/lang/String;Ljava/lang/String;)V
    //   539: goto -292 -> 247
    //   542: astore 20
    //   544: aload 20
    //   546: invokevirtual 419	java/lang/InstantiationException:printStackTrace	()V
    //   549: goto -178 -> 371
    //   552: astore 10
    //   554: aload 10
    //   556: invokevirtual 247	java/lang/IllegalAccessException:printStackTrace	()V
    //   559: goto -188 -> 371
    //   562: aload_0
    //   563: getfield 328	com/flixster/android/activity/hc/Main:mListFragmentStack	Lcom/flixster/android/activity/hc/FlixsterFragmentStack;
    //   566: invokevirtual 262	com/flixster/android/activity/hc/FlixsterFragmentStack:peek	()Lcom/flixster/android/activity/hc/FlixsterFragment;
    //   569: astore 12
    //   571: goto -157 -> 414
    //   574: aload 5
    //   576: ldc_w 420
    //   579: ldc_w 421
    //   582: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   585: pop
    //   586: goto -118 -> 468
    //   589: aload 5
    //   591: ldc_w 422
    //   594: ldc_w 423
    //   597: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   600: pop
    //   601: goto -133 -> 468
    //   604: aload 5
    //   606: ldc_w 429
    //   609: ldc_w 430
    //   612: invokevirtual 278	android/app/FragmentTransaction:setCustomAnimations	(II)Landroid/app/FragmentTransaction;
    //   615: pop
    //   616: goto -148 -> 468
    //   619: aload 5
    //   621: aload 12
    //   623: invokestatic 426	com/flixster/android/activity/hc/Main:tryHide	(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    //   626: goto -116 -> 510
    //
    // Exception table:
    //   from	to	target	type
    //   58	76	256	java/lang/InstantiationException
    //   2	28	266	finally
    //   33	41	266	finally
    //   44	50	266	finally
    //   58	76	266	finally
    //   81	90	266	finally
    //   104	119	266	finally
    //   119	129	266	finally
    //   164	176	266	finally
    //   176	200	266	finally
    //   211	218	266	finally
    //   218	247	266	finally
    //   258	263	266	finally
    //   275	280	266	finally
    //   283	292	266	finally
    //   295	307	266	finally
    //   310	322	266	finally
    //   325	337	266	finally
    //   340	347	266	finally
    //   353	371	266	finally
    //   376	385	266	finally
    //   399	414	266	finally
    //   414	424	266	finally
    //   456	468	266	finally
    //   468	492	266	finally
    //   503	510	266	finally
    //   510	539	266	finally
    //   544	549	266	finally
    //   554	559	266	finally
    //   562	571	266	finally
    //   574	586	266	finally
    //   589	601	266	finally
    //   604	616	266	finally
    //   619	626	266	finally
    //   58	76	273	java/lang/IllegalAccessException
    //   353	371	542	java/lang/InstantiationException
    //   353	371	552	java/lang/IllegalAccessException
  }

  private static void tryHide(FragmentTransaction paramFragmentTransaction, Fragment paramFragment)
  {
    try
    {
      paramFragmentTransaction.hide(paramFragment);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Logger.w("FlxMain", "Main.tryHide " + localIllegalStateException.getMessage());
    }
  }

  private static void tryRemove(FragmentTransaction paramFragmentTransaction, Fragment paramFragment)
  {
    try
    {
      paramFragmentTransaction.remove(paramFragment);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Logger.w("FlxMain", "Main.tryRemove " + localIllegalStateException.getMessage());
    }
  }

  protected View.OnClickListener getDateSelectOnClickListener()
  {
    try
    {
      if (this.mDateSelectOnClickListener == null)
        this.mDateSelectOnClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            if (!Main.this.isFinishing())
              Main.this.showDialog(6);
          }
        };
      View.OnClickListener localOnClickListener = this.mDateSelectOnClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected Uri getPhotoInfoUri(Photo paramPhoto)
  {
    return Uri.parse("http://flixster.com/mobile/support/flag-photo?image=" + paramPhoto.id);
  }

  protected String getResourceString(int paramInt)
  {
    return getResources().getString(paramInt);
  }

  public View.OnClickListener getStartSettingsClickListener()
  {
    try
    {
      if (this.mStartSettingsClickListener == null)
        this.mStartSettingsClickListener = new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Intent localIntent = new Intent(Main.this, SettingsPage.class);
            Main.this.startActivity(localIntent);
          }
        };
      View.OnClickListener localOnClickListener = this.mStartSettingsClickListener;
      return localOnClickListener;
    }
    finally
    {
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool;
    int i;
    FragmentTransaction localFragmentTransaction;
    int j;
    if (getLastNonConfigurationInstance() != null)
    {
      bool = true;
      this.skipRatePrompt = bool;
      Logger.i("FlxMain", "Main.onCreate");
      ImageTaskImpl.instance().startTask();
      if (this.mPageTimer == null)
        this.mPageTimer = new Timer();
      if (this.mContentFragmentStack == null)
      {
        String[] arrayOfString2 = (String[])null;
        if (paramBundle != null)
          arrayOfString2 = paramBundle.getStringArray("KEY_CONTENT_FRAGMENT_STACK");
        this.mContentFragmentStack = new FlixsterFragmentStack(getFragmentManager(), arrayOfString2);
        Logger.i("FlxMain", "mContentFragmentStack.size=" + this.mContentFragmentStack.size());
      }
      if (this.mListFragmentStack == null)
      {
        String[] arrayOfString1 = (String[])null;
        if (paramBundle != null)
          arrayOfString1 = paramBundle.getStringArray("KEY_LIST_FRAGMENT_STACK");
        this.mListFragmentStack = new FlixsterFragmentStack(getFragmentManager(), arrayOfString1);
        Logger.i("FlxMain", "mListFragmentStack.size=" + this.mListFragmentStack.size());
      }
      i = 0;
      if (paramBundle != null)
      {
        i = paramBundle.getInt("KEY_LAST_TAB_INDEX");
        this.mCurrTab = FlixFragTab.values()[i];
        localFragmentTransaction = getFragmentManager().beginTransaction();
        j = 0;
        label228: if (j < -1 + this.mContentFragmentStack.size())
          break label298;
      }
    }
    for (int k = 0; ; k++)
    {
      if (k >= -1 + this.mListFragmentStack.size())
      {
        localFragmentTransaction.commit();
        setContentView(2130903084);
        createActionBar(i);
        FlixsterApplication.setCurrentDimensions(this);
        showDiagnosticsOrRateDialog();
        Startup.instance().onStartup(this);
        return;
        bool = false;
        break;
        label298: localFragmentTransaction.hide(this.mContentFragmentStack.elementAt(j));
        j++;
        break label228;
      }
      localFragmentTransaction.hide(this.mListFragmentStack.elementAt(k));
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    case 3:
    case 4:
    case 5:
    default:
      return super.onCreateDialog(paramInt);
    case 1:
      if (Properties.instance().shouldShowSplash())
      {
        Properties.instance().splashShown();
        this.mLoadingDialog = DialogBuilder.createDialog(this, 999999999);
        return this.mLoadingDialog;
      }
      this.mLoadingDialog = new ProgressDialog(this);
      ((ProgressDialog)this.mLoadingDialog).setMessage(getResources().getString(2131493173));
      ((ProgressDialog)this.mLoadingDialog).setIndeterminate(true);
      this.mLoadingDialog.setCancelable(true);
      this.mLoadingDialog.setCanceledOnTouchOutside(true);
      return this.mLoadingDialog;
    case 6:
      return new AlertDialog.Builder(this).setTitle(getResources().getString(2131493167)).setItems(DialogUtils.getShowtimesDateOptions(this), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT which:" + paramAnonymousInt);
          Calendar localCalendar = Calendar.getInstance();
          localCalendar.add(5, paramAnonymousInt);
          Logger.d("Flixster", "ShowtimesList DIALOG_DATE_SELECT date:" + localCalendar.getTime());
          FlixsterApplication.setShowtimesDate(localCalendar.getTime());
          Trackers.instance().track("/showtimes/selectDate", "Select Date");
          Main.this.removeDialog(6);
          Main.this.peekContentBackStack().onResume();
        }
      }).create();
    case 2:
    }
    return new AlertDialog.Builder(this).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Main.this.onResume();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131689483, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    FlixsterApplication.stopLocationRequest();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      int i = this.mContentFragmentStack.size();
      int j = this.mListFragmentStack.size();
      Logger.i("FlxMain", "Main.onKeyDown mContentFragmentStack.size=" + i + " mListFragmentStack.size=" + j);
      if (i > 1)
      {
        removeFragment(this.mContentFragmentStack, 2130968586, 2130968588);
        return true;
      }
      if (j > 1)
      {
        removeFragment(this.mListFragmentStack, 2130968587, 2130968589);
        return true;
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onLowMemory()
  {
    super.onLowMemory();
    Logger.w("FlxMain", "Low memory!");
    ProfileDao.clearBitmapCache();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    case 2131165948:
    case 2131165949:
    case 2131165950:
    case 2131165951:
    case 2131165952:
    case 2131165953:
    case 2131165954:
    case 2131165959:
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165946:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "Search");
      this.mCurrTab = FlixFragTab.SEARCH;
      FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
      resetContentBackStack(localFragmentTransaction);
      resetListBackStack(localFragmentTransaction);
      startFragment(new Intent(), SearchFragment.class, 0, null, localFragmentTransaction);
      localFragmentTransaction.commit();
      return true;
    case 2131165947:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "Settings");
      startActivity(new Intent(this, SettingsPage.class));
      return true;
    case 2131165955:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "TellAFriend");
      Intent localIntent = new Intent("android.intent.action.SEND");
      localIntent.setType("text/plain");
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = ApiBuilder.mobileUpsell();
      localIntent.putExtra("android.intent.extra.TEXT", getString(2131493298, arrayOfObject));
      startActivity(Intent.createChooser(localIntent, "Share via"));
      return true;
    case 2131165956:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "RateThisApp");
      AppRater.rateThisApp(this);
      return true;
    case 2131165957:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "FAQ");
      Starter.launchBrowser(ApiBuilder.faq(), this);
      return true;
    case 2131165960:
      Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "PrivacyPolicy");
      showDialog(1000000010, new PrivacyDialogListener(null));
      return true;
    case 2131165958:
    }
    Trackers.instance().trackEvent("/main/tablet", "Main - Tablet", "MainTabletActionBar", "Feedback");
    Starter.launchFlxHtmlPage(ApiBuilder.feedback(), getString(2131493173), this);
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    ImageTaskImpl.instance().stopTask();
    if (this.mPageTimer != null)
    {
      this.mPageTimer.cancel();
      this.mPageTimer.purge();
      this.mPageTimer = null;
    }
  }

  protected void onResume()
  {
    super.onResume();
    if (this.mPageTimer == null)
      this.mPageTimer = new Timer();
    Logger.d("FlxMain", "Main.onResume start ImageTask");
    ImageTaskImpl.instance().startTask();
    if (!FlixsterApplication.sNetflixStartupValidate)
      FlixsterApplication.sNetflixStartupValidate = true;
    try
    {
      if (FlixsterApplication.getNetflixUserId() != null)
      {
        NetflixDao.fetchTitleState("12");
        Trackers.instance().track("/netflix/startupvalidate", "User token valid");
      }
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public Object onRetainNonConfigurationInstance()
  {
    if (this.skipRatePrompt)
      return new Object();
    return null;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putStringArray("KEY_CONTENT_FRAGMENT_STACK", this.mContentFragmentStack.persist());
    paramBundle.putStringArray("KEY_LIST_FRAGMENT_STACK", this.mListFragmentStack.persist());
    paramBundle.putInt("KEY_LAST_TAB_INDEX", this.mCurrTab.ordinal());
  }

  protected void onStart()
  {
    super.onStart();
    FlixsterApplication.sToday = new Date();
  }

  public void onTabReselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
    if (this.mCurrTab == FlixFragTab.SEARCH)
      onTabSelected(paramTab, paramFragmentTransaction);
  }

  public void onTabSelected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
    FlixFragTab localFlixFragTab = (FlixFragTab)paramTab.getTag();
    if (this.mCurrTab == localFlixFragTab)
    {
      Logger.i("FlxMain", "Main.onTabSelected ignored");
      return;
    }
    Logger.i("FlxMain", "Main.onTabSelected currTab=" + this.mCurrTab + " newTab=" + localFlixFragTab);
    this.mCurrTab = localFlixFragTab;
    resetContentBackStack(paramFragmentTransaction);
    resetListBackStack(paramFragmentTransaction);
    startFragment(new Intent(), localFlixFragTab.getFragClass(), 0, null, paramFragmentTransaction);
  }

  public void onTabUnselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
  }

  public void orderImage(ImageOrder paramImageOrder)
  {
    ImageTaskImpl.instance().orderImage(paramImageOrder);
  }

  public void orderImageFifo(ImageOrder paramImageOrder)
  {
    ImageTaskImpl.instance().orderImageFifo(paramImageOrder);
  }

  public void orderImageLifo(ImageOrder paramImageOrder)
  {
    ImageTaskImpl.instance().orderImageLifo(paramImageOrder);
  }

  protected FlixsterFragment peekContentBackStack()
  {
    return this.mContentFragmentStack.peek();
  }

  protected FlixsterFragment peekListBackStack()
  {
    return this.mListFragmentStack.peek();
  }

  protected void popContentBackStack()
  {
    FlixsterFragmentStack localFlixsterFragmentStack = this.mContentFragmentStack;
    if (localFlixsterFragmentStack.size() > 1)
    {
      FlixsterFragment localFlixsterFragment1 = localFlixsterFragmentStack.pop();
      FlixsterFragment localFlixsterFragment2 = localFlixsterFragmentStack.peek();
      FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
      localFragmentTransaction.setCustomAnimations(2130968586, 2130968588);
      localFragmentTransaction.show(localFlixsterFragment2);
      localFragmentTransaction.remove(localFlixsterFragment1);
      localFragmentTransaction.commit();
    }
  }

  protected boolean savePhoto(Photo paramPhoto)
  {
    Logger.d("FlxMain", "FlixsterActivity savePhoto title:");
    String str1 = "photo" + paramPhoto.id + ".png";
    String str2 = "Flixster Movies Photo";
    StringBuilder localStringBuilder = new StringBuilder();
    if ((paramPhoto.caption != null) && (paramPhoto.caption.length() > 1))
    {
      str1 = paramPhoto.caption.replace(" ", "_");
      localStringBuilder.append(paramPhoto.caption);
    }
    if ((paramPhoto.description != null) && (paramPhoto.description.length() > 1))
    {
      if (localStringBuilder.length() > 0)
        localStringBuilder.append(" - ");
      localStringBuilder.append(paramPhoto.description);
    }
    if (localStringBuilder.length() > 0)
      str2 = localStringBuilder.toString();
    try
    {
      Logger.d("FlxMain", "FlixsterActivity savePhoto title:" + str1 + " desc:" + str2);
      String str3 = MediaStore.Images.Media.insertImage(getContentResolver(), (Bitmap)paramPhoto.galleryBitmap.get(), str1, str2);
      return str3 != null;
    }
    catch (Exception localException)
    {
      Logger.w("FlxMain", "Problem saving image", localException);
    }
    return false;
  }

  protected void startFragment(Intent paramIntent, Class<? extends FlixsterFragment> paramClass)
  {
    startFragment(paramIntent, paramClass, 0, null);
  }

  protected void startFragment(Intent paramIntent, Class<? extends FlixsterFragment> paramClass1, int paramInt, Class<? extends FlixsterFragment> paramClass2)
  {
    FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
    startFragment(paramIntent, paramClass1, paramInt, paramClass2, localFragmentTransaction);
    localFragmentTransaction.commit();
  }

  private static enum FlixFragTab
  {
    private final int iconId;
    private final int text;

    static
    {
      DVD = new FlixFragTab("DVD", 3, 2130837723, 2131493138);
      MY_MOVIES = new FlixFragTab("MY_MOVIES", 4, 2130837733, 2131493139);
      SEARCH = new FlixFragTab("SEARCH", 5, 0, 0);
      FlixFragTab[] arrayOfFlixFragTab = new FlixFragTab[6];
      arrayOfFlixFragTab[0] = BOX_OFFICE;
      arrayOfFlixFragTab[1] = THEATERS;
      arrayOfFlixFragTab[2] = UPCOMING;
      arrayOfFlixFragTab[3] = DVD;
      arrayOfFlixFragTab[4] = MY_MOVIES;
      arrayOfFlixFragTab[5] = SEARCH;
    }

    private FlixFragTab(int arg3, int arg4)
    {
      int i;
      this.iconId = i;
      int j;
      this.text = j;
    }

    private Class getFragClass()
    {
      switch ($SWITCH_TABLE$com$flixster$android$activity$hc$Main$FlixFragTab()[ordinal()])
      {
      default:
        return null;
      case 1:
        return BoxOfficeFragment.class;
      case 2:
        return TheaterListFragment.class;
      case 3:
        return UpcomingFragment.class;
      case 4:
        return DvdFragment.class;
      case 5:
      }
      return MyMoviesFragment.class;
    }
  }

  private class PrivacyDialogListener
    implements DialogBuilder.DialogListener
  {
    private PrivacyDialogListener()
    {
    }

    public void onNegativeButtonClick(int paramInt)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(Main.this.getResources().getString(2131493256)));
      Main.this.startActivity(localIntent);
    }

    public void onNeutralButtonClick(int paramInt)
    {
    }

    public void onPositiveButtonClick(int paramInt)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(Main.this.getResources().getString(2131493255)));
      Main.this.startActivity(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.Main
 * JD-Core Version:    0.6.2
 */