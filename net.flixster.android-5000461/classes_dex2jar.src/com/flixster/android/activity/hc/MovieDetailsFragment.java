package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.storage.ExternalStorage;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import com.flixster.android.view.DownloadPanel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.ScrollGalleryPage;
import net.flixster.android.Starter;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixException;
import net.flixster.android.model.NetflixTitleState;
import net.flixster.android.model.Photo;
import net.flixster.android.model.Property;
import net.flixster.android.model.Review;
import net.flixster.android.model.Season;
import net.flixster.android.model.User;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

@TargetApi(11)
public class MovieDetailsFragment extends FlixsterFragment
  implements View.OnClickListener
{
  private static final int DIALOG_NETFLIX_ACTION = 0;
  private static final int DIALOG_NETFLIX_EXCEPTION = 1;
  public static final String KEY_IS_SEASON = "KEY_IS_SEASON";
  public static final String KEY_RIGHT_ID = "KEY_RIGHT_ID";
  private static final int MAX_ACTORS = 3;
  private static final int MAX_DIRECTORS = 3;
  private static final int MAX_PHOTOS = 15;
  private static final int NETFLIX_ACTION_CANCEL = 0;
  private static final int NETFLIX_ACTION_DVD_ADD_BOTTOM = 4;
  private static final int NETFLIX_ACTION_DVD_ADD_TOP = 3;
  private static final int NETFLIX_ACTION_DVD_MOVE_BOTTOM = 10;
  private static final int NETFLIX_ACTION_DVD_MOVE_TOP = 9;
  private static final int NETFLIX_ACTION_DVD_REMOVE = 2;
  private static final int NETFLIX_ACTION_DVD_SAVE = 5;
  private static final int NETFLIX_ACTION_INSTANT_ADD_BOTTOM = 8;
  private static final int NETFLIX_ACTION_INSTANT_ADD_TOP = 7;
  private static final int NETFLIX_ACTION_INSTANT_MOVE_BOTTOM = 12;
  private static final int NETFLIX_ACTION_INSTANT_MOVE_TOP = 11;
  private static final int NETFLIX_ACTION_INSTANT_REMOVE = 6;
  private static final int NETFLIX_ACTION_VIEW_QUEUE = 1;
  static final int RESULTCODE_REVIEW = 1;
  static final int RESULT_CODE_FEED_PERMISSION = 2;
  private final int REVIEW_SUBSET_COUNT = 3;
  private View.OnClickListener actorClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Actor localActor = (Actor)paramAnonymousView.getTag();
      if (localActor != null)
      {
        Intent localIntent = new Intent("TOP_ACTOR", null, MovieDetailsFragment.this.getActivity(), ActorFragment.class);
        localIntent.putExtra("ACTOR_ID", localActor.id);
        localIntent.putExtra("ACTOR_NAME", localActor.name);
        ((Main)MovieDetailsFragment.this.getActivity()).startFragment(localIntent, ActorFragment.class);
      }
    }
  };
  private Handler actorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetailsFragment.this.isRemoving());
      ImageView localImageView;
      Actor localActor;
      do
      {
        do
        {
          return;
          localImageView = (ImageView)paramAnonymousMessage.obj;
        }
        while (localImageView == null);
        localActor = (Actor)localImageView.getTag();
      }
      while (localActor == null);
      localImageView.setImageBitmap(localActor.bitmap);
      localImageView.invalidate();
    }
  };
  private LinearLayout actorsLayout;
  private final Handler canDownloadSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetailsFragment.this.downloadInitDialog != null);
      try
      {
        MovieDetailsFragment.this.downloadInitDialog.dismiss();
        if (MovieDetailsFragment.this.right.getDownloadsRemain() > 0)
        {
          DownloadHelper.downloadMovie(MovieDetailsFragment.this.right);
          MovieDetailsFragment.this.delayedStreamingUiUpdate();
          return;
        }
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
        ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000204, null);
      }
    }
  };
  private LinearLayout directorsLayout;
  private final DialogBuilder.DialogListener downloadCancelDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      DownloadHelper.cancelMovieDownload(MovieDetailsFragment.this.right.rightId);
      MovieDetailsFragment.this.delayedStreamingUiUpdate();
      Trackers.instance().trackEvent("/download", "Download", "Download", "Cancel");
    }
  };
  private final View.OnClickListener downloadClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if ((!DownloadHelper.isMovieDownloadInProgress(MovieDetailsFragment.this.right.rightId)) && (!DownloadHelper.isDownloaded(MovieDetailsFragment.this.right.rightId)))
      {
        if (!ExternalStorage.isWriteable())
          break label113;
        if (DownloadHelper.findRemainingSpace() > MovieDetailsFragment.this.right.getDownloadAssetSizeRaw())
        {
          ObjectHolder.instance().put(String.valueOf(1000000200), MovieDetailsFragment.this.right);
          ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000200, MovieDetailsFragment.this.downloadConfirmDialogListener);
        }
      }
      else
      {
        return;
      }
      ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000205, null);
      return;
      label113: ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000203, null);
    }
  };
  private final DialogBuilder.DialogListener downloadConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000200));
      Trackers.instance().trackEvent("/download", "Download", "Download", "Confirm");
      if (MovieDetailsFragment.this.downloadInitDialog == null)
      {
        MovieDetailsFragment.this.downloadInitDialog = new ProgressDialog((Main)MovieDetailsFragment.this.getActivity());
        MovieDetailsFragment.this.downloadInitDialog.setMessage(MovieDetailsFragment.this.getResources().getString(2131493172));
      }
      MovieDetailsFragment.this.downloadInitDialog.show();
      ProfileDao.canDownload(MovieDetailsFragment.this.canDownloadSuccessHandler, MovieDetailsFragment.this.errorHandler, MovieDetailsFragment.this.right);
    }
  };
  private final View.OnClickListener downloadDeleteClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (DownloadHelper.isMovieDownloadInProgress(MovieDetailsFragment.this.right.rightId))
        ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000201, MovieDetailsFragment.this.downloadCancelDialogListener);
      while (!DownloadHelper.isDownloaded(MovieDetailsFragment.this.right.rightId))
        return;
      ObjectHolder.instance().put(String.valueOf(1000000202), MovieDetailsFragment.this.right);
      ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000202, MovieDetailsFragment.this.downloadDeleteDialogListener);
    }
  };
  private final DialogBuilder.DialogListener downloadDeleteDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      ObjectHolder.instance().remove(String.valueOf(1000000202));
      DownloadHelper.deleteDownloadedMovie(MovieDetailsFragment.this.right);
      MovieDetailsFragment.this.delayedStreamingUiUpdate();
      Trackers.instance().trackEvent("/download", "Download", "Download", "Delete");
    }
  };
  private ProgressDialog downloadInitDialog;
  private DownloadPanel downloadPanel;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetailsFragment.this.downloadInitDialog != null);
      try
      {
        MovieDetailsFragment.this.downloadInitDialog.dismiss();
        if ((paramAnonymousMessage.obj instanceof DaoException))
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, (Main)MovieDetailsFragment.this.getActivity());
        return;
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
      }
    }
  };
  private boolean isSeason;
  private boolean mFromRating = false;
  private LayoutInflater mLayoutInflater;
  private Movie mMovie;
  private long mMovieId;
  private int[] mNetflixMenuToAction = null;
  private NetflixTitleState mNetflixTitleState;
  private Handler mNetflixTitleStateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MovieDetailsFragment.this.getActivity() == null) || (MovieDetailsFragment.this.isRemoving()));
      View localView;
      do
      {
        return;
        localView = MovieDetailsFragment.this.getView();
      }
      while ((localView == null) || (MovieDetailsFragment.this.mNetflixTitleState == null));
      TextView localTextView = (TextView)localView.findViewById(2131165529);
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(Integer.valueOf(1));
      Logger.d("FlxMain", "MovieDetailsFragment.mNetflixTitleStateHandler mNetflixTitleState:" + MovieDetailsFragment.this.mNetflixTitleState);
      localTextView.setText(2131493073);
      if ((0x8 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) != 0)
      {
        localTextView.setText(2131493075);
        localArrayList.add(Integer.valueOf(6));
        localArrayList.add(Integer.valueOf(11));
        localArrayList.add(Integer.valueOf(12));
        label163: if ((0x1 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label386;
        localTextView.setText(2131493075);
        localArrayList.add(Integer.valueOf(2));
        localArrayList.add(Integer.valueOf(9));
        localArrayList.add(Integer.valueOf(10));
        label216: if ((0x9 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label452;
        localTextView.setText(2131493075);
      }
      while (true)
      {
        localArrayList.add(Integer.valueOf(0));
        localTextView.setVisibility(0);
        localTextView.setOnClickListener(MovieDetailsFragment.this);
        localTextView.setFocusable(true);
        int i = localArrayList.size();
        Logger.d("FlxMain", "menuActions:" + localArrayList);
        MovieDetailsFragment.this.mNetflixMenuToAction = new int[i];
        for (int j = 0; j < i; j++)
          MovieDetailsFragment.this.mNetflixMenuToAction[j] = ((Integer)localArrayList.get(j)).intValue();
        break;
        if ((0x10 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label163;
        localArrayList.add(Integer.valueOf(7));
        localArrayList.add(Integer.valueOf(8));
        break label163;
        label386: if ((0x2 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) != 0)
        {
          localArrayList.add(Integer.valueOf(3));
          localArrayList.add(Integer.valueOf(4));
          break label216;
        }
        if ((0x4 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) == 0)
          break label216;
        localArrayList.add(Integer.valueOf(5));
        break label216;
        label452: if ((0x4 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) != 0)
          localTextView.setText(2131493074);
        else if ((0x12 & MovieDetailsFragment.this.mNetflixTitleState.mCategoriesMask) != 0)
          localTextView.setText(2131493073);
      }
    }
  };
  private Boolean mPlayTrailer = null;
  private Review mReview;
  private RelativeLayout moreActorsLayout;
  private View.OnClickListener moreActorsListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((MovieDetailsFragment.this.actorsLayout != null) && (MovieDetailsFragment.this.mMovie != null) && (MovieDetailsFragment.this.mMovie.mActors != null) && (MovieDetailsFragment.this.mMovie.mActors.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= MovieDetailsFragment.this.mMovie.mActors.size())
        {
          if (MovieDetailsFragment.this.moreActorsLayout != null)
            MovieDetailsFragment.this.moreActorsLayout.setVisibility(8);
          return;
        }
        Actor localActor = (Actor)MovieDetailsFragment.this.mMovie.mActors.get(i);
        localView = MovieDetailsFragment.this.getActorView(localActor, localView);
        ImageView localImageView = new ImageView(MovieDetailsFragment.this.getActivity());
        localImageView.setBackgroundResource(2131296282);
        localImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, MovieDetailsFragment.this.getResources().getDimensionPixelOffset(2131361829)));
        MovieDetailsFragment.this.actorsLayout.addView(localImageView);
        MovieDetailsFragment.this.actorsLayout.addView(localView);
      }
    }
  };
  private RelativeLayout moreDirectorsLayout;
  private View.OnClickListener moreDirectorsListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      View localView;
      if ((MovieDetailsFragment.this.directorsLayout != null) && (MovieDetailsFragment.this.mMovie != null) && (MovieDetailsFragment.this.mMovie.mDirectors != null) && (MovieDetailsFragment.this.mMovie.mDirectors.size() > 3))
        localView = null;
      for (int i = 3; ; i++)
      {
        if (i >= MovieDetailsFragment.this.mMovie.mDirectors.size())
        {
          if (MovieDetailsFragment.this.moreDirectorsLayout != null)
            MovieDetailsFragment.this.moreDirectorsLayout.setVisibility(8);
          return;
        }
        Actor localActor = (Actor)MovieDetailsFragment.this.mMovie.mDirectors.get(i);
        localView = MovieDetailsFragment.this.getActorView(localActor, localView);
        ImageView localImageView = new ImageView(MovieDetailsFragment.this.getActivity());
        localImageView.setBackgroundResource(2131296282);
        localImageView.setLayoutParams(new ViewGroup.LayoutParams(-1, MovieDetailsFragment.this.getResources().getDimensionPixelOffset(2131361829)));
        MovieDetailsFragment.this.directorsLayout.addView(localImageView);
        MovieDetailsFragment.this.directorsLayout.addView(localView);
      }
    }
  };
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = MovieDetailsFragment.this.mMovie.mPhotos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Trackers.instance().track(MovieDetailsFragment.this.getGaTagPrefix() + "/photo", MovieDetailsFragment.this.getGaTitlePrefix() + "Photo Page for photo:" + localPhoto.thumbnailUrl);
        Intent localIntent = new Intent("PHOTO", null, MovieDetailsFragment.this.getActivity(), ScrollGalleryPage.class);
        localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", MovieDetailsFragment.this.mMovie.getId());
        localIntent.putExtra("PHOTO_INDEX", i);
        localIntent.putExtra("title", MovieDetailsFragment.this.mMovie.getProperty("title"));
        MovieDetailsFragment.this.startActivity(localIntent);
      }
    }
  };
  private Handler photoHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (MovieDetailsFragment.this.isRemoving());
      ImageView localImageView;
      Photo localPhoto;
      do
      {
        do
        {
          return;
          localImageView = (ImageView)paramAnonymousMessage.obj;
        }
        while (localImageView == null);
        localPhoto = (Photo)localImageView.getTag();
      }
      while ((localPhoto == null) || (localPhoto.bitmap == null));
      localImageView.setImageBitmap(localPhoto.bitmap);
      localImageView.invalidate();
    }
  };
  private final Handler populateStreamingUiHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MovieDetailsFragment.this.getActivity() == null) || (MovieDetailsFragment.this.isRemoving()))
        return;
      MovieDetailsFragment.this.populateStreamingUi();
    }
  };
  private Handler refreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MovieDetailsFragment.this.getActivity() == null) || (MovieDetailsFragment.this.isRemoving()))
        return;
      Logger.d("FlxMain", "MovieDetailsFragment.refreshHandler run...");
      MovieDetailsFragment.this.populatePage();
    }
  };
  private LockerRight right;
  private boolean showGetGlue;
  private final View.OnClickListener watchNowClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (F.IS_NATIVE_HC_DRM_ENABLED)
      {
        Drm.manager().playMovie(MovieDetailsFragment.this.right);
        return;
      }
      if ((!DownloadHelper.isMovieDownloadInProgress(MovieDetailsFragment.this.right.rightId)) && (DownloadHelper.isDownloaded(MovieDetailsFragment.this.right.rightId)));
      for (int i = 1; (i == 0) && (DownloadHelper.isMovieDownloadInProgress()); i = 0)
      {
        ((Main)MovieDetailsFragment.this.getActivity()).showDialog(1000000300, MovieDetailsFragment.this.watchNowConfirmDialogListener);
        return;
      }
      Drm.manager().playMovie(MovieDetailsFragment.this.right);
    }
  };
  private final DialogBuilder.DialogListener watchNowConfirmDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
      Drm.manager().playMovie(MovieDetailsFragment.this.right);
    }
  };
  private TextView watchNowPanel;

  private void NetflixRemoveMenuDialog()
  {
    Message localMessage = Message.obtain();
    localMessage.obj = this;
    localMessage.what = 0;
    ((Main)getActivity()).mRemoveDialogHandler.sendMessage(localMessage);
  }

  private void NetflixShowExceptionDialog()
  {
    Message localMessage = Message.obtain();
    localMessage.obj = this;
    localMessage.what = 1;
    ((Main)getActivity()).mShowDialogHandler.sendMessage(localMessage);
  }

  private void ScheduleAddToQueue(final int paramInt, final String paramString)
  {
    TimerTask local21 = new TimerTask()
    {
      public void run()
      {
        try
        {
          NetflixDao.postQueueItem(MovieDetailsFragment.this.mMovie.getProperty("netflix"), paramInt, paramString);
          MovieDetailsFragment.this.ScheduleNetflixTitleState();
          Trackers.instance().track("/netflix/addtoqueue" + paramString, "position:" + paramInt);
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (NetflixException localNetflixException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue NetflixException message:" + localNetflixException.mMessage, localNetflixException);
          MovieDetailsFragment.this.NetflixShowExceptionDialog();
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
          return;
        }
        catch (JSONException localJSONException)
        {
          localJSONException.printStackTrace();
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local21, 100L);
  }

  private void ScheduleLoadMovieTask(long paramLong)
  {
    TimerTask local24 = new TimerTask()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   4: invokevirtual 25	com/flixster/android/activity/hc/MovieDetailsFragment:getActivity	()Landroid/app/Activity;
        //   7: checkcast 27	com/flixster/android/activity/hc/Main
        //   10: ifnull +13 -> 23
        //   13: aload_0
        //   14: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   17: invokevirtual 31	com/flixster/android/activity/hc/MovieDetailsFragment:isRemoving	()Z
        //   20: ifeq +4 -> 24
        //   23: return
        //   24: ldc 33
        //   26: new 35	java/lang/StringBuilder
        //   29: dup
        //   30: ldc 37
        //   32: invokespecial 40	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
        //   35: aload_0
        //   36: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   39: invokestatic 44	com/flixster/android/activity/hc/MovieDetailsFragment:access$28	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J
        //   42: invokevirtual 48	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
        //   45: invokevirtual 52	java/lang/StringBuilder:toString	()Ljava/lang/String;
        //   48: invokestatic 58	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
        //   51: aload_0
        //   52: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   55: astore_2
        //   56: aload_0
        //   57: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   60: invokestatic 62	com/flixster/android/activity/hc/MovieDetailsFragment:access$29	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z
        //   63: ifeq +114 -> 177
        //   66: aload_0
        //   67: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   70: invokestatic 44	com/flixster/android/activity/hc/MovieDetailsFragment:access$28	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J
        //   73: invokestatic 68	net/flixster/android/data/MovieDao:getSeasonDetailLegacy	(J)Lnet/flixster/android/model/Season;
        //   76: astore_3
        //   77: aload_2
        //   78: aload_3
        //   79: invokestatic 72	com/flixster/android/activity/hc/MovieDetailsFragment:access$30	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Movie;)V
        //   82: invokestatic 77	net/flixster/android/FlixsterApplication:getNetflixUserId	()Ljava/lang/String;
        //   85: ifnull +10 -> 95
        //   88: aload_0
        //   89: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   92: invokestatic 80	com/flixster/android/activity/hc/MovieDetailsFragment:access$26	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)V
        //   95: aload_0
        //   96: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   99: iconst_0
        //   100: putfield 84	com/flixster/android/activity/hc/MovieDetailsFragment:mRetryCount	I
        //   103: aload_0
        //   104: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   107: invokestatic 87	com/flixster/android/activity/hc/MovieDetailsFragment:access$31	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Z
        //   110: ifne +36 -> 146
        //   113: invokestatic 90	net/flixster/android/FlixsterApplication:getPlatformUsername	()Ljava/lang/String;
        //   116: ifnull +30 -> 146
        //   119: invokestatic 93	net/flixster/android/FlixsterApplication:getFlixsterId	()Ljava/lang/String;
        //   122: astore 5
        //   124: aload_0
        //   125: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   128: aload 5
        //   130: aload_0
        //   131: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   134: invokestatic 44	com/flixster/android/activity/hc/MovieDetailsFragment:access$28	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J
        //   137: invokestatic 98	java/lang/Long:toString	(J)Ljava/lang/String;
        //   140: invokestatic 104	net/flixster/android/data/ProfileDao:getUserMovieReview	(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;
        //   143: invokestatic 108	com/flixster/android/activity/hc/MovieDetailsFragment:access$32	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;Lnet/flixster/android/model/Review;)V
        //   146: aload_0
        //   147: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   150: invokestatic 112	com/flixster/android/activity/hc/MovieDetailsFragment:access$33	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)Landroid/os/Handler;
        //   153: iconst_0
        //   154: invokevirtual 118	android/os/Handler:sendEmptyMessage	(I)Z
        //   157: pop
        //   158: return
        //   159: astore_1
        //   160: ldc 33
        //   162: ldc 120
        //   164: aload_1
        //   165: invokestatic 124	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   168: aload_0
        //   169: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   172: aload_1
        //   173: invokevirtual 128	com/flixster/android/activity/hc/MovieDetailsFragment:retryLogic	(Lnet/flixster/android/data/DaoException;)V
        //   176: return
        //   177: aload_0
        //   178: getfield 15	com/flixster/android/activity/hc/MovieDetailsFragment$24:this$0	Lcom/flixster/android/activity/hc/MovieDetailsFragment;
        //   181: invokestatic 44	com/flixster/android/activity/hc/MovieDetailsFragment:access$28	(Lcom/flixster/android/activity/hc/MovieDetailsFragment;)J
        //   184: invokestatic 132	net/flixster/android/data/MovieDao:getMovieDetail	(J)Lnet/flixster/android/model/Movie;
        //   187: astore_3
        //   188: goto -111 -> 77
        //   191: astore 6
        //   193: ldc 33
        //   195: ldc 134
        //   197: aload 6
        //   199: invokestatic 124	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   202: goto -56 -> 146
        //
        // Exception table:
        //   from	to	target	type
        //   51	77	159	net/flixster/android/data/DaoException
        //   77	95	159	net/flixster/android/data/DaoException
        //   95	124	159	net/flixster/android/data/DaoException
        //   146	158	159	net/flixster/android/data/DaoException
        //   177	188	159	net/flixster/android/data/DaoException
        //   193	202	159	net/flixster/android/data/DaoException
        //   124	146	191	net/flixster/android/data/DaoException
      }
    };
    Main localMain = (Main)getActivity();
    if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
      localMain.mPageTimer.schedule(local24, paramLong);
  }

  private void ScheduleMoveToBottom(final String paramString)
  {
    TimerTask local22 = new TimerTask()
    {
      public void run()
      {
        try
        {
          String str = MovieDetailsFragment.this.mMovie.getProperty("netflix");
          Logger.d("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom movieUrl:" + str + " queueType:" + paramString);
          NetflixDao.deleteQueueItem(str.substring(37, str.length()), paramString);
          NetflixDao.postQueueItem(MovieDetailsFragment.this.mMovie.getProperty("netflix"), 0, paramString);
          MovieDetailsFragment.this.ScheduleNetflixTitleState();
          Trackers.instance().track("/netflix/movetobottom" + paramString, "position:");
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (NetflixException localNetflixException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleAddToQueue NetflixException message:" + localNetflixException.mMessage, localNetflixException);
          MovieDetailsFragment.this.NetflixShowExceptionDialog();
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleMoveToBottom IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
          return;
        }
        catch (JSONException localJSONException)
        {
          localJSONException.printStackTrace();
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local22, 100L);
  }

  private void ScheduleNetflixTitleState()
  {
    TimerTask local20 = new TimerTask()
    {
      public void run()
      {
        try
        {
          MovieDetailsFragment.this.mNetflixTitleState = NetflixDao.fetchTitleState(MovieDetailsFragment.this.mMovie.getProperty("netflix"));
          MovieDetailsFragment.this.mNetflixTitleStateHandler.sendEmptyMessage(0);
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleNetflixTitleState OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleNetflixTitleState OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleNetflixTitleState IOException", localIOException);
          return;
        }
        catch (JSONException localJSONException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleNetflixTitleState JSONException", localJSONException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
        }
      }
    };
    Main localMain = (Main)getActivity();
    if ((localMain != null) && (localMain.mPageTimer != null))
      ((Main)getActivity()).mPageTimer.schedule(local20, 100L);
  }

  private void ScheduleRemoveFromQueue(final String paramString)
  {
    TimerTask local23 = new TimerTask()
    {
      public void run()
      {
        try
        {
          String str1 = MovieDetailsFragment.this.mMovie.getProperty("netflix");
          String str2 = str1.substring(37, str1.length());
          Logger.d("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue movieUrl:" + str1 + " movieUrlId:" + str2);
          NetflixDao.deleteQueueItem(str2, paramString);
          Trackers.instance().track("/netflix/removefromqueue" + paramString, "Delete");
          MovieDetailsFragment.this.ScheduleNetflixTitleState();
          return;
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthMessageSignerException", localOAuthMessageSignerException);
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthExpectationFailedException", localOAuthExpectationFailedException);
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue ClientProtocolException", localClientProtocolException);
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
          return;
        }
        catch (IOException localIOException)
        {
          Logger.e("FlxMain", "MovieDetailsFragment.ScheduleRemoveFromQueue IOException", localIOException);
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local23, 100L);
  }

  private void delayedStreamingUiUpdate()
  {
    Main localMain = (Main)getActivity();
    if ((localMain != null) && (localMain.mPageTimer != null))
      localMain.mPageTimer.schedule(new TimerTask()
      {
        public void run()
        {
          MovieDetailsFragment.this.populateStreamingUiHandler.sendEmptyMessage(0);
        }
      }
      , 2000L);
  }

  private View getActorView(Actor paramActor, View paramView)
  {
    View localView = this.mLayoutInflater.inflate(2130903117, null);
    ActorViewHolder localActorViewHolder = new ActorViewHolder(null);
    localActorViewHolder.actorLayout = ((RelativeLayout)localView.findViewById(2131165224));
    localActorViewHolder.nameView = ((TextView)localView.findViewById(2131165228));
    localActorViewHolder.charsView = ((TextView)localView.findViewById(2131165444));
    localActorViewHolder.imageView = ((ImageView)localView.findViewById(2131165227));
    if (paramActor != null)
    {
      String str1 = paramActor.name;
      localActorViewHolder.nameView.setText(str1);
      String str2 = paramActor.chars;
      if (str2 != null)
      {
        localActorViewHolder.charsView.setText(str2);
        localActorViewHolder.charsView.setVisibility(0);
      }
      localActorViewHolder.imageView.setTag(paramActor);
      if (paramActor.bitmap == null)
        break label194;
      localActorViewHolder.imageView.setImageBitmap(paramActor.bitmap);
    }
    while (true)
    {
      localActorViewHolder.actorLayout.setTag(paramActor);
      localActorViewHolder.actorLayout.setId(2130903063);
      localActorViewHolder.actorLayout.setOnClickListener(this.actorClickListener);
      return localView;
      label194: if (paramActor.thumbnailUrl != null)
      {
        localActorViewHolder.imageView.setImageResource(2130837978);
        ImageOrder localImageOrder = new ImageOrder(4, paramActor, paramActor.thumbnailUrl, localActorViewHolder.imageView, this.actorHandler);
        ((Main)getActivity()).orderImageFifo(localImageOrder);
      }
      else
      {
        localActorViewHolder.imageView.setImageResource(2130837978);
      }
    }
  }

  public static int getFlixFragId()
  {
    return 108;
  }

  private String getGaTagPrefix()
  {
    if (this.isSeason)
      return "/tv-season";
    return "/movie";
  }

  private String getGaTitlePrefix()
  {
    if (this.isSeason)
      return "TV Season ";
    return "Movie ";
  }

  private void loadCriticReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = this.mLayoutInflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setVisibility(8);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setText(", " + localReview.source);
      TextView localTextView = (TextView)localView.findViewById(2131165289);
      localTextView.setMaxLines(10);
      localTextView.setText("     " + localReview.comment);
      if (localReview.score < 60)
        ((ImageView)localView.findViewById(2131165286)).setImageResource(2130837741);
      localView.setTag(new Integer(i));
      Logger.d("FlxMain", "MovieDetailsFragment.populatePage()  iTagIndex:" + i);
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  private void loadFriendReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = this.mLayoutInflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setImageResource(Main.RATING_SMALL_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setVisibility(8);
      ((TextView)localView.findViewById(2131165289)).setText(localReview.comment);
      ((ImageView)localView.findViewById(2131165286)).setVisibility(8);
      localView.setTag(Integer.valueOf(i));
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  private void loadUserReviews(int paramInt1, int paramInt2, int paramInt3, LinearLayout paramLinearLayout, ArrayList<Review> paramArrayList)
  {
    int i = paramInt3;
    for (int j = paramInt1; ; j++)
    {
      if (j >= paramInt2)
        return;
      Review localReview = (Review)paramArrayList.get(j);
      View localView = this.mLayoutInflater.inflate(2130903074, paramLinearLayout, false);
      localView.setId(2130903074);
      localView.setFocusable(true);
      ((ImageView)localView.findViewById(2131165288)).setImageResource(Main.RATING_SMALL_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localView.findViewById(2131165285)).setText(localReview.name);
      ((TextView)localView.findViewById(2131165287)).setVisibility(8);
      ((TextView)localView.findViewById(2131165289)).setText(localReview.comment);
      ((ImageView)localView.findViewById(2131165286)).setVisibility(8);
      localView.setTag(Integer.valueOf(i));
      i++;
      localView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(2131165284);
      Bitmap localBitmap = localReview.getReviewerBitmap(localImageView);
      if (localBitmap != null)
        localImageView.setImageBitmap(localBitmap);
      paramLinearLayout.addView(localView);
    }
  }

  public static MovieDetailsFragment newInstance(Bundle paramBundle)
  {
    MovieDetailsFragment localMovieDetailsFragment = new MovieDetailsFragment();
    localMovieDetailsFragment.setArguments(paramBundle);
    return localMovieDetailsFragment;
  }

  private void populatePage()
  {
    Logger.d("FlxMain", "MovieDetailsFragment.populatePage()");
    if (this.mMovie == null)
      Logger.d("FlxMain", "MovieDetailsFragment.populatePage() mMovie == null, skip return early from populatePage()");
    View localView1;
    do
    {
      return;
      localView1 = getView();
    }
    while (localView1 == null);
    String str1;
    label106: TextView localTextView2;
    label134: TextView localTextView3;
    label171: LinearLayout localLinearLayout1;
    TextView localTextView4;
    ArrayList localArrayList1;
    label231: ImageView localImageView2;
    label268: View localView3;
    int i12;
    label332: View localView2;
    int i10;
    label536: String[] arrayOfString1;
    int[] arrayOfInt;
    String[] arrayOfString2;
    int j;
    TextView localTextView6;
    ImageView localImageView3;
    TextView localTextView7;
    label1048: int k;
    label1077: int i6;
    label1127: Object localObject;
    String str6;
    label1170: int i7;
    label1195: int i8;
    label1289: label1296: TextView localTextView8;
    StringBuilder localStringBuilder1;
    label1427: label1443: TextView localTextView9;
    label1489: TextView localTextView10;
    label1535: TextView localTextView11;
    int m;
    label1602: TextView localTextView12;
    label1646: TextView localTextView13;
    label1690: TextView localTextView14;
    label1713: RelativeLayout localRelativeLayout2;
    RelativeLayout localRelativeLayout5;
    LinearLayout localLinearLayout6;
    LinearLayout localLinearLayout8;
    LinearLayout localLinearLayout9;
    RelativeLayout localRelativeLayout9;
    label2025: RelativeLayout localRelativeLayout8;
    label2186: RelativeLayout localRelativeLayout7;
    label2354: label2364: RelativeLayout localRelativeLayout6;
    label2509: TextView localTextView15;
    if (this.right != null)
    {
      str1 = this.right.getTitle();
      if (str1 != null)
      {
        ((TextView)localView1.findViewById(2131165584)).setText(str1);
        ((TextView)localView1.findViewById(2131165511)).setText(str1);
      }
      RelativeLayout localRelativeLayout1 = (RelativeLayout)localView1.findViewById(2131165525);
      if (!this.isSeason)
        break label2635;
      localRelativeLayout1.setVisibility(8);
      localTextView2 = (TextView)localView1.findViewById(2131165523);
      if (!this.mMovie.isMIT)
        break label2752;
      localTextView2.setVisibility(0);
      localTextView3 = (TextView)localView1.findViewById(2131165528);
      if (!this.showGetGlue)
        break label2762;
      localTextView3.setVisibility(0);
      localTextView3.setFocusable(true);
      localTextView3.setOnClickListener(this);
      localLinearLayout1 = (LinearLayout)localView1.findViewById(2131165538);
      localTextView4 = (TextView)localView1.findViewById(2131165524);
      localArrayList1 = this.mMovie.mPhotos;
      localTextView4.setVisibility(8);
      if (localArrayList1 == null)
        break label3021;
      if (!localArrayList1.isEmpty())
        break label2772;
      localTextView4.setVisibility(8);
      localImageView2 = (ImageView)localView1.findViewById(2131165509);
      if (this.right == null)
        break label3064;
      if (this.right.getProfilePoster() != null)
        break label3038;
      localImageView2.setImageResource(2130837839);
      this.directorsLayout = ((LinearLayout)localView1.findViewById(2131165540));
      if ((this.mMovie.mDirectors != null) && (!this.mMovie.mDirectors.isEmpty()))
      {
        ((RelativeLayout)localView1.findViewById(2131165539)).setVisibility(0);
        localView3 = null;
        this.directorsLayout.removeAllViews();
        i12 = 0;
        int i13 = this.mMovie.mDirectors.size();
        if ((i12 < i13) && (i12 < 3))
          break label3111;
        if (this.mMovie.mDirectors.size() > 3)
        {
          this.moreDirectorsLayout = ((RelativeLayout)localView1.findViewById(2131165541));
          this.moreDirectorsLayout.setVisibility(0);
          StringBuilder localStringBuilder4 = new StringBuilder();
          localStringBuilder4.append(this.mMovie.mDirectors.size()).append(" total");
          ((TextView)this.moreDirectorsLayout.findViewById(2131165543)).setText(localStringBuilder4.toString());
          this.moreDirectorsLayout.setFocusable(true);
          this.moreDirectorsLayout.setClickable(true);
          this.moreDirectorsLayout.setOnClickListener(this.moreDirectorsListener);
        }
      }
      this.actorsLayout = ((LinearLayout)localView1.findViewById(2131165451));
      if ((this.mMovie.mActors != null) && (!this.mMovie.mActors.isEmpty()))
      {
        ((RelativeLayout)localView1.findViewById(2131165544)).setVisibility(0);
        localView2 = null;
        this.actorsLayout.removeAllViews();
        i10 = 0;
        int i11 = this.mMovie.mActors.size();
        if ((i10 < i11) && (i10 < 3))
          break label3149;
        if (this.mMovie.mActors.size() > 3)
        {
          this.moreActorsLayout = ((RelativeLayout)localView1.findViewById(2131165545));
          this.moreActorsLayout.setVisibility(0);
          StringBuilder localStringBuilder3 = new StringBuilder();
          localStringBuilder3.append(this.mMovie.mActors.size()).append(" total");
          ((TextView)this.moreActorsLayout.findViewById(2131165547)).setText(localStringBuilder3.toString());
          this.moreActorsLayout.setFocusable(true);
          this.moreActorsLayout.setClickable(true);
          this.moreActorsLayout.setOnClickListener(this.moreActorsListener);
        }
      }
      if (this.mMovie.isDetailsApiParsed())
      {
        arrayOfString1 = new String[] { "genre", "runningTime", "theaterReleaseDate", "dvdReleaseDate", "mpaa", "synopsis" };
        arrayOfInt = new int[] { 2131165534, 2131165535, 2131165536, 2131165537, 2131165533, 2131165532 };
        Resources localResources = getResources();
        arrayOfString2 = new String[6];
        arrayOfString2[0] = localResources.getString(2131492929);
        arrayOfString2[1] = localResources.getString(2131492899);
        arrayOfString2[2] = localResources.getString(2131492900);
        arrayOfString2[3] = localResources.getString(2131492904);
        arrayOfString2[4] = localResources.getString(2131492898);
        arrayOfString2[5] = localResources.getString(2131492897);
        int i = arrayOfString1.length;
        j = 0;
        if (j < i)
          break label3187;
      }
      localTextView6 = (TextView)localView1.findViewById(2131165513);
      localImageView3 = (ImageView)localView1.findViewById(2131165563);
      localTextView7 = (TextView)localView1.findViewById(2131165562);
      if (!this.mMovie.checkIntProperty("rottenTomatoes"))
        break label3319;
      int i9 = this.mMovie.getIntProperty("rottenTomatoes").intValue();
      localTextView6.setText(i9 + getResources().getString(2131493218));
      localTextView7.setText(i9 + "%");
      if (i9 < 60)
      {
        Drawable localDrawable4 = getResources().getDrawable(2130837741);
        localDrawable4.setBounds(0, 0, localDrawable4.getIntrinsicWidth(), localDrawable4.getIntrinsicHeight());
        localTextView6.setCompoundDrawables(localDrawable4, null, null, null);
        localImageView3.setImageResource(2130837740);
      }
      if ((this.mMovie.getTheaterReleaseDate() == null) || (FlixsterApplication.sToday.after(this.mMovie.getTheaterReleaseDate())))
        break label3343;
      k = 0;
      if (!this.mMovie.checkIntProperty("popcornScore"))
        break label3404;
      TextView localTextView19 = (TextView)localView1.findViewById(2131165512);
      int i5 = this.mMovie.getIntProperty("popcornScore").intValue();
      if (i5 >= 60)
        break label3349;
      i6 = 1;
      localObject = getResources().getString(2131493219);
      str6 = getResources().getString(2131493220);
      StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(i5));
      if (k == 0)
        break label3355;
      localTextView19.setText((String)localObject);
      if (k != 0)
        break label3362;
      i7 = 2130837750;
      Drawable localDrawable3 = getResources().getDrawable(i7);
      localDrawable3.setBounds(0, 0, localDrawable3.getIntrinsicWidth(), localDrawable3.getIntrinsicHeight());
      localTextView19.setCompoundDrawables(localDrawable3, null, null, null);
      ((TextView)localView1.findViewById(2131165571)).setText(i5 + "%");
      ImageView localImageView4 = (ImageView)localView1.findViewById(2131165572);
      if (k != 0)
        break label3383;
      i8 = 2130837749;
      localImageView4.setImageResource(i8);
      localTextView8 = (TextView)localView1.findViewById(2131165514);
      if ((this.mMovie.getFriendRatedReviewsList() == null) || (this.mMovie.getFriendRatedReviewsList().isEmpty()))
        break label3455;
      int i4 = this.mMovie.getFriendRatedReviewsList().size();
      Drawable localDrawable2 = getResources().getDrawable(2130837739);
      localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
      localTextView8.setCompoundDrawables(localDrawable2, null, null, null);
      localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append(i4).append(" ");
      if (i4 <= 1)
        break label3436;
      localStringBuilder1.append(getResources().getString(2131493008));
      localTextView8.setText(localStringBuilder1.toString());
      localTextView8.setVisibility(0);
      localTextView9 = (TextView)localView1.findViewById(2131165515);
      if (!this.mMovie.checkProperty("MOVIE_ACTORS_SHORT"))
        break label3636;
      localTextView9.setText(this.mMovie.getProperty("MOVIE_ACTORS_SHORT"));
      localTextView9.setVisibility(0);
      localTextView10 = (TextView)localView1.findViewById(2131165516);
      if (!this.mMovie.checkProperty("meta"))
        break label3646;
      localTextView10.setText(this.mMovie.getProperty("meta"));
      localTextView10.setVisibility(0);
      if ((AccountManager.instance().hasUserSession()) && (this.right != null))
        populateStreamingUi();
      populateSeasonUi();
      localTextView11 = (TextView)localView1.findViewById(2131165579);
      if (0 == 0)
        break label3656;
      m = 1;
      localTextView11.setClickable(true);
      localTextView11.setFocusable(true);
      localTextView11.setOnClickListener(this);
      localTextView11.setVisibility(0);
      localTextView12 = (TextView)localView1.findViewById(2131165580);
      if (m == 0)
        break label3669;
      m = 1;
      localTextView12.setClickable(true);
      localTextView12.setFocusable(true);
      localTextView12.setOnClickListener(this);
      localTextView12.setVisibility(0);
      localTextView13 = (TextView)localView1.findViewById(2131165581);
      if (m == 0)
        break label3679;
      m = 1;
      localTextView13.setClickable(true);
      localTextView13.setFocusable(true);
      localTextView13.setOnClickListener(this);
      localTextView13.setVisibility(0);
      localTextView14 = (TextView)localView1.findViewById(2131165578);
      if (m == 0)
        break label3689;
      localTextView14.setVisibility(0);
      localRelativeLayout2 = (RelativeLayout)localView1.findViewById(2131165560);
      RelativeLayout localRelativeLayout3 = (RelativeLayout)localView1.findViewById(2131165548);
      LinearLayout localLinearLayout2 = (LinearLayout)localView1.findViewById(2131165549);
      LinearLayout localLinearLayout3 = (LinearLayout)localView1.findViewById(2131165550);
      RelativeLayout localRelativeLayout4 = (RelativeLayout)localView1.findViewById(2131165554);
      LinearLayout localLinearLayout4 = (LinearLayout)localView1.findViewById(2131165555);
      LinearLayout localLinearLayout5 = (LinearLayout)localView1.findViewById(2131165556);
      localRelativeLayout5 = (RelativeLayout)localView1.findViewById(2131165569);
      localLinearLayout6 = (LinearLayout)localView1.findViewById(2131165573);
      LinearLayout localLinearLayout7 = (LinearLayout)localView1.findViewById(2131165574);
      localLinearLayout8 = (LinearLayout)localView1.findViewById(2131165564);
      localLinearLayout9 = (LinearLayout)localView1.findViewById(2131165565);
      int n = localLinearLayout2.getChildCount();
      int i1 = 0;
      if (n == 0)
      {
        ArrayList localArrayList5 = this.mMovie.getFriendWantToSeeList();
        i1 = 0;
        if (localArrayList5 != null)
        {
          int i2 = localArrayList5.size();
          i1 = 0;
          if (i2 > 0)
          {
            localLinearLayout2.setVisibility(0);
            localLinearLayout3.setVisibility(8);
            localRelativeLayout3.setVisibility(0);
            loadFriendReviews(0, Math.min(localArrayList5.size(), 3), 0, localLinearLayout2, localArrayList5);
            localRelativeLayout9 = (RelativeLayout)localView1.findViewById(2131165551);
            localRelativeLayout9.setVisibility(8);
            if (localArrayList5.size() <= 3)
              break label3699;
            ((TextView)localView1.findViewById(2131165553)).setText(localArrayList5.size() + " friends total");
            localRelativeLayout9.setOnClickListener(this);
            localRelativeLayout9.setFocusable(true);
            localRelativeLayout9.setVisibility(0);
            i1 = 0 + localArrayList5.size();
          }
        }
      }
      if (localLinearLayout4.getChildCount() == 0)
      {
        ArrayList localArrayList4 = this.mMovie.getFriendRatedReviewsList();
        if ((localArrayList4 != null) && (localArrayList4.size() > 0))
        {
          localLinearLayout4.setVisibility(0);
          localLinearLayout5.setVisibility(8);
          localRelativeLayout4.setVisibility(0);
          loadFriendReviews(0, Math.min(localArrayList4.size(), 3), i1, localLinearLayout4, localArrayList4);
          localRelativeLayout8 = (RelativeLayout)localView1.findViewById(2131165557);
          localRelativeLayout8.setVisibility(8);
          if (localArrayList4.size() <= 3)
            break label3709;
          ((TextView)localView1.findViewById(2131165559)).setText(localArrayList4.size() + " reviews total");
          localRelativeLayout8.setOnClickListener(this);
          localRelativeLayout8.setFocusable(true);
          localRelativeLayout8.setVisibility(0);
          i1 += localArrayList4.size();
        }
      }
      if (localLinearLayout6.getChildCount() == 0)
      {
        ArrayList localArrayList3 = this.mMovie.getUserReviewsList();
        if ((localArrayList3 == null) || (localArrayList3.size() == 0))
          break label3729;
        localRelativeLayout5.setVisibility(0);
        localLinearLayout6.setVisibility(0);
        localLinearLayout7.setVisibility(8);
        localRelativeLayout2.setVisibility(0);
        loadUserReviews(0, Math.min(localArrayList3.size(), 3), i1, localLinearLayout6, localArrayList3);
        localRelativeLayout7 = (RelativeLayout)localView1.findViewById(2131165575);
        localRelativeLayout7.setVisibility(8);
        if (localArrayList3.size() <= 3)
          break label3719;
        ((TextView)localView1.findViewById(2131165577)).setText(localArrayList3.size() + " reviews total");
        localRelativeLayout7.setOnClickListener(this);
        localRelativeLayout7.setFocusable(true);
        localRelativeLayout7.setVisibility(0);
        i1 += localArrayList3.size();
      }
      localRelativeLayout6 = (RelativeLayout)localView1.findViewById(2131165566);
      if (localLinearLayout8.getChildCount() == 0)
      {
        ArrayList localArrayList2 = this.mMovie.getCriticReviewsList();
        if ((localArrayList2 == null) || (localArrayList2.size() == 0))
          break label3756;
        localLinearLayout8.setVisibility(0);
        localLinearLayout9.setVisibility(8);
        localRelativeLayout2.setVisibility(0);
        loadCriticReviews(0, Math.min(3, localArrayList2.size()), i1, localLinearLayout8, localArrayList2);
        if (localArrayList2.size() <= 3)
          break label3746;
        ((TextView)localView1.findViewById(2131165568)).setText(localArrayList2.size() + " reviews total");
        localRelativeLayout6.setOnClickListener(this);
        localRelativeLayout6.setFocusable(true);
        localRelativeLayout6.setVisibility(0);
      }
      localTextView15 = (TextView)localView1.findViewById(2131165522);
      TextView localTextView16 = (TextView)localView1.findViewById(2131165519);
      if ((!this.mMovie.hasTrailer()) || (localTextView16.getVisibility() == 0))
        break label3787;
      localTextView15.setVisibility(0);
      localTextView15.setFocusable(true);
      localTextView15.setOnClickListener(this);
    }
    TextView localTextView18;
    while (true)
    {
      TextView localTextView17 = (TextView)localView1.findViewById(2131165523);
      localTextView17.setFocusable(true);
      localTextView17.setOnClickListener(this);
      localTextView18 = (TextView)localView1.findViewById(2131165529);
      if (FlixsterApplication.getNetflixUserId() == null)
        break label3797;
      localTextView18.setOnClickListener(this);
      localTextView18.setFocusable(true);
      return;
      str1 = this.mMovie.getTitle();
      break;
      label2635: TextView localTextView1 = (TextView)localView1.findViewById(2131165526);
      localTextView1.setFocusable(true);
      localTextView1.setOnClickListener(this);
      ImageView localImageView1 = (ImageView)localView1.findViewById(2131165527);
      if ((this.mReview != null) && (this.mReview.stars != 0.0D) && (FlixsterApplication.getPlatformUsername() != null))
      {
        localImageView1.setVisibility(0);
        localImageView1.setImageResource(Main.RATING_SMALL_R[((int)(2.0D * this.mReview.stars))]);
        localTextView1.setText(2131492927);
        break label106;
      }
      localImageView1.setVisibility(8);
      localTextView1.setText(2131492926);
      break label106;
      label2752: localTextView2.setVisibility(8);
      break label134;
      label2762: localTextView3.setVisibility(8);
      break label171;
      label2772: localLinearLayout1.removeAllViews();
      int i14 = 0;
      int i15 = localArrayList1.size();
      if ((i14 >= i15) || (i14 >= 15))
      {
        localLinearLayout1.setVisibility(0);
        break label231;
      }
      Photo localPhoto = (Photo)localArrayList1.get(i14);
      ImageView localImageView5 = new ImageView(getActivity());
      localImageView5.setLayoutParams(new ViewGroup.LayoutParams(getResources().getDimensionPixelOffset(2131361847), getResources().getDimensionPixelOffset(2131361847)));
      localImageView5.setPadding(0, getResources().getDimensionPixelOffset(2131361836), getResources().getDimensionPixelOffset(2131361835), 0);
      localImageView5.setTag(localPhoto);
      if (localPhoto.bitmap != null)
        localImageView5.setImageBitmap(localPhoto.bitmap);
      while (true)
      {
        localImageView5.setClickable(true);
        localImageView5.setFocusable(true);
        localImageView5.setOnClickListener(this.photoClickListener);
        localLinearLayout1.addView(localImageView5);
        i14++;
        break;
        localImageView5.setImageResource(2130837841);
        if ((localPhoto.thumbnailUrl != null) && (localPhoto.thumbnailUrl.startsWith("http://")))
        {
          ImageOrder localImageOrder = new ImageOrder(3, localPhoto, localPhoto.thumbnailUrl, localImageView5, this.photoHandler);
          ((Main)getActivity()).orderImageFifo(localImageOrder);
        }
      }
      label3021: localLinearLayout1.setVisibility(8);
      localTextView4.setVisibility(8);
      break label231;
      label3038: Bitmap localBitmap2 = this.right.getProfileBitmap(localImageView2);
      if (localBitmap2 == null)
        break label268;
      localImageView2.setImageBitmap(localBitmap2);
      break label268;
      label3064: if (this.mMovie.getThumbnailPoster() == null)
      {
        localImageView2.setImageResource(2130837839);
        break label268;
      }
      Bitmap localBitmap1 = this.mMovie.getThumbnailBackedProfileBitmap(localImageView2);
      if (localBitmap1 == null)
        break label268;
      localImageView2.setImageBitmap(localBitmap1);
      break label268;
      label3111: localView3 = getActorView((Actor)this.mMovie.mDirectors.get(i12), localView3);
      this.directorsLayout.addView(localView3);
      i12++;
      break label332;
      label3149: localView2 = getActorView((Actor)this.mMovie.mActors.get(i10), localView2);
      this.actorsLayout.addView(localView2);
      i10++;
      break label536;
      label3187: String str2 = this.mMovie.getProperty(arrayOfString1[j]);
      TextView localTextView5 = (TextView)localView1.findViewById(arrayOfInt[j]);
      if ((str2 != null) && (str2.length() != 0))
      {
        localTextView5.setText(arrayOfString2[j] + " " + str2, TextView.BufferType.SPANNABLE);
        ((Spannable)localTextView5.getText()).setSpan(new StyleSpan(1), 0, arrayOfString2[j].length(), 33);
        localTextView5.setVisibility(0);
      }
      while (true)
      {
        j++;
        break;
        localTextView5.setVisibility(8);
      }
      label3319: localTextView6.setVisibility(8);
      localImageView3.setVisibility(8);
      localTextView7.setVisibility(8);
      break label1048;
      label3343: k = 1;
      break label1077;
      label3349: i6 = 0;
      break label1127;
      label3355: localObject = str6;
      break label1170;
      label3362: if (i6 != 0)
      {
        i7 = 2130837743;
        break label1195;
      }
      i7 = 2130837738;
      break label1195;
      label3383: if (i6 != 0)
      {
        i8 = 2130837742;
        break label1289;
      }
      i8 = 2130837737;
      break label1289;
      label3404: ((TextView)localView1.findViewById(2131165512)).setVisibility(8);
      ((TextView)localView1.findViewById(2131165571)).setVisibility(4);
      break label1296;
      label3436: localStringBuilder1.append(getResources().getString(2131493007));
      break label1427;
      label3455: if ((this.mMovie.getFriendWantToSeeList() != null) && (!this.mMovie.getFriendWantToSeeList().isEmpty()))
      {
        int i3 = this.mMovie.getFriendWantToSeeList().size();
        Drawable localDrawable1 = getResources().getDrawable(2130837751);
        localDrawable1.setBounds(0, 0, localDrawable1.getIntrinsicWidth(), localDrawable1.getIntrinsicHeight());
        localTextView8.setCompoundDrawables(localDrawable1, null, null, null);
        String str5;
        Object[] arrayOfObject2;
        if (i3 > 1)
        {
          str5 = getResources().getString(2131493222);
          arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Integer.valueOf(i3);
        }
        String str3;
        Object[] arrayOfObject1;
        for (String str4 = String.format(str5, arrayOfObject2); ; str4 = String.format(str3, arrayOfObject1))
        {
          localTextView8.setText(str4);
          localTextView8.setVisibility(0);
          break;
          str3 = getResources().getString(2131493221);
          arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(i3);
        }
      }
      localTextView8.setVisibility(8);
      break label1443;
      label3636: localTextView9.setVisibility(8);
      break label1489;
      label3646: localTextView10.setVisibility(8);
      break label1535;
      label3656: localTextView11.setVisibility(8);
      m = 0;
      break label1602;
      label3669: localTextView12.setVisibility(8);
      break label1646;
      label3679: localTextView13.setVisibility(8);
      break label1690;
      label3689: localTextView14.setVisibility(8);
      break label1713;
      label3699: localRelativeLayout9.setVisibility(8);
      break label2025;
      label3709: localRelativeLayout8.setVisibility(8);
      break label2186;
      label3719: localRelativeLayout7.setVisibility(8);
      break label2354;
      label3729: localRelativeLayout5.setVisibility(8);
      localLinearLayout6.setVisibility(8);
      break label2364;
      label3746: localRelativeLayout6.setVisibility(8);
      break label2509;
      label3756: localLinearLayout8.setVisibility(8);
      localLinearLayout9.setVisibility(8);
      localRelativeLayout2.setVisibility(8);
      localRelativeLayout6.setVisibility(8);
      break label2509;
      label3787: localTextView15.setVisibility(8);
    }
    label3797: localTextView18.setVisibility(8);
  }

  private void populateSeasonUi()
  {
    View localView = getView();
    TextView localTextView1 = (TextView)localView.findViewById(2131165521);
    if (this.isSeason)
    {
      localTextView1.setVisibility(0);
      localTextView1.setOnClickListener(this);
      ((TextView)localView.findViewById(2131165530)).setText(2131493289);
      TextView localTextView2 = (TextView)localView.findViewById(2131165516);
      String str = ((Season)this.mMovie).getNetwork();
      if (str != null)
      {
        localTextView2.setText(str);
        localTextView2.setVisibility(0);
        return;
      }
      localTextView2.setVisibility(8);
      return;
    }
    localTextView1.setVisibility(8);
  }

  private void populateStreamingUi()
  {
    View localView = getView();
    this.watchNowPanel = ((TextView)localView.findViewById(2131165519));
    this.downloadPanel = ((DownloadPanel)localView.findViewById(2131165520));
    if (Drm.logic().isAssetPlayable(this.right))
    {
      this.watchNowPanel.setVisibility(0);
      this.watchNowPanel.setOnClickListener(this.watchNowClickListener);
    }
    while (Drm.logic().isAssetDownloadable(this.right))
    {
      this.downloadPanel.setVisibility(0);
      this.downloadPanel.load(this.right, this.downloadClickListener, this.downloadDeleteClickListener);
      return;
      this.watchNowPanel.setVisibility(8);
    }
    this.downloadPanel.setVisibility(8);
  }

  private void readExtras()
  {
    this.mMovieId = getArguments().getLong("net.flixster.android.EXTRA_MOVIE_ID");
    this.isSeason = getArguments().getBoolean("KEY_IS_SEASON");
    long l = getArguments().getLong("KEY_RIGHT_ID");
    if (this.mPlayTrailer == null)
      this.mPlayTrailer = Boolean.valueOf(getArguments().getBoolean("net.flixster.PLAY_TRAILER"));
    Object localObject;
    if (this.isSeason)
    {
      localObject = MovieDao.getSeason(this.mMovieId);
      this.mMovie = ((Movie)localObject);
      User localUser = AccountManager.instance().getUser();
      if ((localUser != null) && (l > 0L))
        this.right = localUser.getLockerRightFromRightId(l);
      Property localProperty = Properties.instance().getProperties();
      if ((this.right != null) || (localProperty == null) || (!localProperty.isGetGlueEnabled))
        break label163;
    }
    label163: for (boolean bool = true; ; bool = false)
    {
      this.showGetGlue = bool;
      populatePage();
      return;
      localObject = MovieDao.getMovie(this.mMovieId);
      break;
    }
  }

  protected void NetworkAttemptsCancelled()
  {
    Logger.d("FlxMain", "MovieDetailsFragment.NetworkAttemptsCancelled()");
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 == -1) && (paramInt1 == 1))
    {
      this.mFromRating = true;
      Bundle localBundle = paramIntent.getExtras();
      if (this.mReview == null)
      {
        this.mReview = new Review();
        this.mReview.type = 1;
      }
      this.mReview.comment = localBundle.getString("net.flixster.ReviewComment");
      Logger.d("FlxMain", "MovieDetails onActivityResult comment:" + this.mReview.comment);
      this.mReview.stars = localBundle.getDouble("net.flixster.ReviewStars");
    }
  }

  public void onClick(View paramView)
  {
    if (this.mMovie == null);
    do
    {
      View localView;
      do
      {
        do
        {
          do
          {
            return;
            localView = getView();
            switch (paramView.getId())
            {
            case 2131165524:
            case 2131165526:
            case 2131165538:
            default:
              return;
            case 2130903074:
              Integer localInteger = (Integer)paramView.getTag();
              Intent localIntent5 = new Intent("REVIEW_PAGE", null, getActivity(), ReviewFragment.class);
              if (localInteger == null)
                localInteger = Integer.valueOf(0);
              Logger.d("FlxMain", "MovieDetailsFragment reviewIndex:" + localInteger);
              localIntent5.putExtra("REVIEW_INDEX", localInteger);
              localIntent5.putExtra("net.flixster.android.EXTRA_MOVIE_ID", this.mMovie.getId());
              ((Main)getActivity()).startFragment(localIntent5, ReviewFragment.class);
              return;
            case 2131165579:
              Trackers.instance().track(getGaTagPrefix() + "/website/flixster", getGaTitlePrefix() + "Info (Flixster) - " + this.mMovie.getProperty("title"));
              Intent localIntent4 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("flixster")));
              startActivity(localIntent4);
              return;
            case 2131165580:
              Trackers.instance().track(getGaTagPrefix() + "/website/rottentomatoes", getGaTitlePrefix() + "Website (RT) - " + this.mMovie.getProperty("title"));
              Intent localIntent3 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("rottentomatoes")));
              startActivity(localIntent3);
              return;
            case 2131165581:
              Trackers.instance().track(getGaTagPrefix() + "/website/imdb", getGaTitlePrefix() + "Info (IMDB) - " + this.mMovie.getProperty("title"));
              Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(this.mMovie.getProperty("imdb")));
              startActivity(localIntent2);
              return;
            case 2131165522:
            case 2131165523:
            case 2131165528:
            case 2131165566:
            case 2131165575:
            case 2131165551:
            case 2131165557:
            case 2131165529:
            case 2131165521:
            }
          }
          while (this.mMovie == null);
          Starter.launchTrailer(this.mMovie, getActivity());
          return;
          Logger.d("FlxMain", "MovieDetailsFragment.onClick md_showtimes_panel");
        }
        while (this.mMovie == null);
        Logger.d("FlxMain", "MovieDetailsFragment.onClick md_showtimes_panel mMovie:" + this.mMovie);
        Intent localIntent1 = new Intent("DETAILS", null, getActivity(), ShowtimesFragment.class);
        localIntent1.putExtra("net.flixster.android.EXTRA_MOVIE_ID", this.mMovie.getId());
        ((Main)getActivity()).startFragment(localIntent1, ShowtimesFragment.class);
        return;
      }
      while (this.mMovie == null);
      Trackers.instance().trackEvent("/getglue", null, "GetGlue", "Click");
      Starter.launchBrowser(ApiBuilder.getGlue(this.mMovie), getActivity());
      return;
      LinearLayout localLinearLayout4 = (LinearLayout)localView.findViewById(2131165565);
      ArrayList localArrayList2 = this.mMovie.getCriticReviewsList();
      int k = 3 + (this.mMovie.getFriendWantToSeeListSize() + this.mMovie.getFriendRatedReviewsListSize() + this.mMovie.getUserReviewsList().size());
      loadCriticReviews(3, localArrayList2.size(), k, localLinearLayout4, localArrayList2);
      localLinearLayout4.setVisibility(0);
      paramView.setVisibility(8);
      return;
      int j = 3 + (this.mMovie.getFriendWantToSeeListSize() + this.mMovie.getFriendRatedReviewsListSize());
      LinearLayout localLinearLayout3 = (LinearLayout)localView.findViewById(2131165574);
      ArrayList localArrayList1 = this.mMovie.getUserReviewsList();
      loadUserReviews(3, localArrayList1.size(), j, localLinearLayout3, localArrayList1);
      localLinearLayout3.setVisibility(0);
      paramView.setVisibility(8);
      return;
      LinearLayout localLinearLayout2 = (LinearLayout)localView.findViewById(2131165550);
      loadFriendReviews(3, this.mMovie.getFriendWantToSeeListSize(), 3, localLinearLayout2, this.mMovie.getFriendWantToSeeList());
      localLinearLayout2.setVisibility(0);
      paramView.setVisibility(8);
      return;
      int i = 3 + this.mMovie.getFriendWantToSeeListSize();
      LinearLayout localLinearLayout1 = (LinearLayout)localView.findViewById(2131165556);
      loadFriendReviews(3, this.mMovie.getFriendRatedReviewsListSize(), i, localLinearLayout1, this.mMovie.getFriendRatedReviewsList());
      localLinearLayout1.setVisibility(0);
      paramView.setVisibility(8);
      return;
      Logger.d("FlxMain", "MovieDetailsFragment.onClick : md_netflix mNetflixMenuToAction:" + this.mNetflixMenuToAction);
    }
    while (this.mNetflixMenuToAction == null);
    onCreateDialog(0).show();
    return;
    Toast.makeText(getActivity(), "Feature coming soon! In the meantime, please use www.flixster.com to watch episodes.", 1).show();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog;
    switch (paramInt)
    {
    default:
      localDialog = super.onCreateDialog(paramInt);
    case 0:
      String[] arrayOfString1;
      int[] arrayOfInt;
      do
      {
        return localDialog;
        Logger.d("FlxMain", "MovieDetailsFragment.onCreateDialog DIALOG_NETFLIX_ACTION");
        arrayOfString1 = getResources().getStringArray(2131623947);
        arrayOfInt = this.mNetflixMenuToAction;
        localDialog = null;
      }
      while (arrayOfInt == null);
      int i = this.mNetflixMenuToAction.length;
      String[] arrayOfString2 = new String[i];
      for (int j = 0; ; j++)
      {
        if (j >= i)
          return new AlertDialog.Builder(getActivity()).setTitle(2131493070).setItems(arrayOfString2, new DialogInterface.OnClickListener()
          {
            public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
            {
              Logger.d("FlxMain", "MovieDetailsFragment.onCreateDialog mNetflixMenuToAction[which]:" + MovieDetailsFragment.this.mNetflixMenuToAction[paramAnonymousInt]);
              switch (MovieDetailsFragment.this.mNetflixMenuToAction[paramAnonymousInt])
              {
              default:
                Logger.d("FlxMain", "MovieDetailsFragment.onCreateDialog -default-");
              case 0:
              case 1:
              case 3:
              case 9:
              case 4:
              case 10:
              case 7:
              case 11:
              case 8:
              case 12:
              case 5:
              case 2:
              case 6:
              }
              while (true)
              {
                MovieDetailsFragment.this.NetflixRemoveMenuDialog();
                return;
                ((Main)MovieDetailsFragment.this.getActivity()).startFragment(new Intent(), NetflixQueueFragment.class);
                continue;
                MovieDetailsFragment.this.ScheduleAddToQueue(1, "/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetailsFragment.this.ScheduleAddToQueue(0, "/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetailsFragment.this.ScheduleMoveToBottom("/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                continue;
                MovieDetailsFragment.this.ScheduleAddToQueue(1, "/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetailsFragment.this.ScheduleAddToQueue(0, "/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetailsFragment.this.ScheduleMoveToBottom("/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
                continue;
                MovieDetailsFragment.this.ScheduleAddToQueue(0, "/queues/disc/saved");
                FlixsterApplication.sNetflixListIsDirty[3] = true;
                continue;
                MovieDetailsFragment.this.ScheduleRemoveFromQueue("/queues/disc/available");
                FlixsterApplication.sNetflixListIsDirty[1] = true;
                FlixsterApplication.sNetflixListIsDirty[3] = true;
                continue;
                MovieDetailsFragment.this.ScheduleRemoveFromQueue("/queues/instant/available");
                FlixsterApplication.sNetflixListIsDirty[2] = true;
              }
            }
          }).create();
        arrayOfString2[j] = arrayOfString1[this.mNetflixMenuToAction[j]];
      }
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle("Netflix Error");
    localBuilder.setMessage("You can't add movies to your Netflix Queue because it is full.");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("OK", null);
    return localBuilder.create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    return this.mLayoutInflater.inflate(2130903131, null);
  }

  public void onResume()
  {
    super.onResume();
    if (this.mMovie == null)
      readExtras();
    trackPage();
    ScheduleLoadMovieTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadMovieTask(1000L);
  }

  public void trackPage()
  {
    if (this.mMovie != null)
    {
      String str = this.mMovie.getProperty("title");
      Trackers.instance().track(getGaTagPrefix() + "/info", getGaTitlePrefix() + "Info - " + str);
      if (this.showGetGlue)
        Trackers.instance().trackEvent("/getglue", null, "GetGlue", "Impression");
      return;
    }
    Trackers.instance().track(getGaTagPrefix() + "/info", getGaTitlePrefix() + "Info - ");
  }

  private static final class ActorViewHolder
  {
    RelativeLayout actorLayout;
    TextView charsView;
    ImageView imageView;
    TextView nameView;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.MovieDetailsFragment
 * JD-Core Version:    0.6.2
 */