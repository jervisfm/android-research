package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.F;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.DialogBuilder;
import com.flixster.android.view.RefreshBar;
import com.flixster.android.view.UnfulfillableFooter;
import com.flixster.android.view.UvFooter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviWrapper;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;
import net.flixster.android.model.User;

@TargetApi(11)
public class MyMovieCollectionFragment extends LviFragment
{
  private static boolean isNoticeShown;
  private final Handler collectedMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MyMovieCollectionFragment.this.getActivity() == null) || (MyMovieCollectionFragment.this.isRemoving()));
      do
      {
        do
        {
          return;
          MyMovieCollectionFragment.this.setCollectedLviList();
          MyMovieCollectionFragment.this.mUpdateHandler.sendEmptyMessage(0);
          if (!MyMovieCollectionFragment.this.isInitialContentLoaded)
          {
            MyMovieCollectionFragment.this.isInitialContentLoaded = true;
            MyMovieCollectionFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
          }
        }
        while (MyMovieCollectionFragment.isNoticeShown);
        MyMovieCollectionFragment.isNoticeShown = true;
        if ((!F.IS_NATIVE_HC_DRM_ENABLED) && (Drm.manager().isRooted()))
        {
          DialogBuilder.showStreamUnsupportedOnRootedDevices(MyMovieCollectionFragment.this.getActivity());
          return;
        }
        if ((Drm.logic().isDeviceNonWifiStreamBlacklisted()) && (!FlixsterApplication.isWifi()))
        {
          DialogBuilder.showNonWifiStreamUnsupported(MyMovieCollectionFragment.this.getActivity());
          return;
        }
      }
      while ((!Drm.logic().isDeviceStreamBlacklisted()) && (Drm.logic().isDeviceStreamingCompatible()));
      DialogBuilder.showStreamUnsupported(MyMovieCollectionFragment.this.getActivity());
    }
  };
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MyMovieCollectionFragment.this.getActivity() == null) || (MyMovieCollectionFragment.this.isRemoving()));
      do
      {
        return;
        Logger.e("FlxMain", "MyMovieCollection.errorHandler: " + paramAnonymousMessage);
      }
      while (!(paramAnonymousMessage.obj instanceof DaoException));
      ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, (Main)MyMovieCollectionFragment.this.getActivity());
    }
  };
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)MyMovieCollectionFragment.this.getActivity();
      if ((localMain == null) || (MyMovieCollectionFragment.this.isRemoving()));
      while (MyMovieCollectionFragment.this.mUser.getLockerRightsCount() <= 0)
        return;
      LockerRight localLockerRight = (LockerRight)MyMovieCollectionFragment.this.mUser.getLockerRights().iterator().next();
      Intent localIntent = new Intent(localMain, MovieDetailsFragment.class);
      localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localLockerRight.assetId);
      if (localLockerRight.isSeason())
        localIntent.putExtra("KEY_IS_SEASON", true);
      localIntent.putExtra("KEY_RIGHT_ID", localLockerRight.rightId);
      localMain.startFragment(localIntent, MovieDetailsFragment.class);
    }
  };
  private User mUser;
  private View.OnClickListener refreshBarClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Logger.d("FlxMain", "RefreshBar -  refreshBarClickListener.onClick");
      if (MyMovieCollectionFragment.this.mUser != null)
      {
        MyMovieCollectionFragment.this.mUser.refreshRequired = true;
        MyMovieCollectionFragment.this.mUser.resetLockerRights();
        MyMovieCollectionFragment.this.onResume();
      }
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local5 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)MyMovieCollectionFragment.this.getActivity();
          if ((localMain == null) || (MyMovieCollectionFragment.this.isRemoving()));
          while (true)
          {
            return;
            try
            {
              if (MyMovieCollectionFragment.this.mUser != null)
              {
                boolean bool = MyMovieCollectionFragment.this.mUser.refreshRequired;
                if (!bool)
                  break label91;
              }
              try
              {
                MyMovieCollectionFragment.this.mUser = ProfileDao.fetchUser();
                MyMovieCollectionFragment.this.mUser.refreshRequired = false;
                ProfileDao.getUserLockerRights(MyMovieCollectionFragment.this.collectedMoviesSuccessHandler, MyMovieCollectionFragment.this.errorHandler);
                label91: if ((localMain == null) || (localMain.mLoadingDialog == null))
                  continue;
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                return;
              }
              catch (DaoException localDaoException)
              {
                while (true)
                {
                  Logger.e("FlxMain", "MyMovieCollectionFragment.scheduleUpdatePageTask (failed to get user data)", localDaoException);
                  MyMovieCollectionFragment.this.mUser = null;
                }
              }
            }
            catch (Exception localException)
            {
              Logger.w("FlxMain", "MyMovieCollectionFragment.ScheduleLoadItemsTask.run() mRetryCount:" + MyMovieCollectionFragment.this.mRetryCount);
              localException.printStackTrace();
              MyMovieCollectionFragment localMyMovieCollectionFragment = MyMovieCollectionFragment.this;
              localMyMovieCollectionFragment.mRetryCount = (1 + localMyMovieCollectionFragment.mRetryCount);
              if (MyMovieCollectionFragment.this.mRetryCount < 3)
                MyMovieCollectionFragment.this.ScheduleLoadItemsTask(1000L);
              while (true)
              {
                return;
                MyMovieCollectionFragment.this.mRetryCount = 0;
                if (localMain != null)
                  localMain.mShowDialogHandler.sendEmptyMessage(2);
              }
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
          }
        }
      };
      Logger.d("FlxMain", "MyMovieCollectionFragment.ScheduleLoadItemsTask() loading item");
      if (((Main)getActivity()).mPageTimer != null)
        ((Main)getActivity()).mPageTimer.schedule(local5, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static int getFlixFragId()
  {
    return 10;
  }

  private void setCollectedLviList()
  {
    Logger.d("FlxMain", "MyMovieCollection.setCollectedLviList ");
    this.mDataHolder.clear();
    int i = 0;
    int j = 0;
    Iterator localIterator = ListHelper.copy(this.mUser.getLockerRights()).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        this.mDataHolder.add(0, LviWrapper.convertToLvi(new RefreshBar(getActivity()).setListener(this.refreshBarClickListener), Lvi.VIEW_TYPE_REFRESHBAR));
        this.mDataHolder.add(LviWrapper.convertToLvi(new UnfulfillableFooter(getActivity()), Lvi.VIEW_TYPE_FOOTER));
        this.mDataHolder.add(LviWrapper.convertToLvi(new UvFooter(getActivity()), Lvi.VIEW_TYPE_FOOTER));
        return;
      }
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      if (((localLockerRight.isMovie()) && ((localLockerRight.isStreamingSupported) || (localLockerRight.isDownloadSupported))) || ((localLockerRight.isSeason()) && (!localLockerRight.isRental)))
      {
        Movie localMovie = localLockerRight.getAsset();
        LviMovie localLviMovie = new LviMovie();
        localLviMovie.mMovie = localMovie;
        localLviMovie.right = localLockerRight;
        localLviMovie.mIsCollected = true;
        if (DownloadHelper.isDownloaded(localLockerRight))
        {
          this.mDataHolder.add(j, localLviMovie);
          j++;
          i++;
        }
        else
        {
          this.mDataHolder.add(i, localLviMovie);
          i++;
        }
      }
    }
  }

  public void onResume()
  {
    super.onResume();
    Trackers.instance().track("/mymovies/mycollection", null);
    ScheduleLoadItemsTask(100L);
  }

  public void trackPage()
  {
    Trackers.instance().track("/mymovies/mycollection", null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.MyMovieCollectionFragment
 * JD-Core Version:    0.6.2
 */