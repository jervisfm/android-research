package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.SessionEvents;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FacebookAuth;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.FlixsterLoginPage;
import net.flixster.android.FriendsPage;
import net.flixster.android.FriendsRatedPage;
import net.flixster.android.MoviesIWantToSeePage;
import net.flixster.android.MyRatedPage;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;

@TargetApi(11)
public class MyMoviesFragment extends FlixsterFragment
  implements View.OnClickListener
{
  private static final int DIALOG_LOGOUT_FACEBOOK = 0;
  private static final int DIALOG_LOGOUT_FLIXSTER = 1;
  private static final int DIALOG_LOGOUT_NETFLIX = 2;
  private static final int DIALOG_PRIVACY = 4;
  private static final int DIALOG_WINDOWREFRESH = 3;
  private static final boolean TEMP_ENABLE_LOGIN = true;
  private final Handler collectedMoviesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MyMoviesFragment.this.getActivity() == null) || (MyMoviesFragment.this.isRemoving()));
      while (AccountManager.instance().getUser().getLockerNonEpisodesCount() <= 0)
        return;
      MyMoviesFragment.this.mMovieListsText.setVisibility(0);
      MyMoviesFragment.this.mCollectionText.setVisibility(0);
    }
  };
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MyMoviesFragment.this.getActivity() == null) || (MyMoviesFragment.this.isRemoving()));
      do
      {
        return;
        Logger.e("FlxMain", "MyMoviesFragment.errorHandler: " + paramAnonymousMessage);
      }
      while (!(paramAnonymousMessage.obj instanceof DaoException));
      ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, (Main)MyMoviesFragment.this.getActivity());
    }
  };
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)MyMoviesFragment.this.getActivity();
      if ((localMain == null) || (MyMoviesFragment.this.isRemoving()))
        return;
      localMain.startFragment(new Intent(), MyMoviesFragment.this.getInitialFragment());
    }
  };
  TextView mActorsText;
  AsyncFacebookRunner mAsyncRunner;
  TextView mCollectionText;
  ImageView mFacebookIcon;
  RelativeLayout mFacebookLayout;
  TextView mFacebookStatus;
  ImageView mFlixsterIcon;
  RelativeLayout mFlixsterLayout;
  TextView mFlixsterNewsText;
  TextView mFlixsterStatus;
  TextView mFriendRatedText;
  TextView mFriendText;
  RelativeLayout mHeaderLayout;
  TextView mMovieListsText;
  RelativeLayout mNetflixLayout;
  ImageView mNetflixLoginButton;
  TextView mNetflixLoginMessage;
  TextView mNetflixText;
  TextView mPhotosText;
  TextView mRatedText;
  private Handler mRefreshHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)MyMoviesFragment.this.getActivity() == null) || (MyMoviesFragment.this.isRemoving()))
        return;
      MyMoviesFragment.this.populatePage();
    }
  };
  private User mUser = null;
  TextView mWantToSeeText;

  public static int getFlixFragId()
  {
    return 5;
  }

  private void populatePage()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    Logger.d("FlxMain", "MyMoviesPage.populatePage() netflix id:" + FlixsterApplication.getNetflixUserId());
    String str2;
    String str3;
    label234: ImageView localImageView;
    if (FlixsterApplication.getNetflixUserId() != null)
    {
      this.mNetflixText.setVisibility(0);
      this.mNetflixText.setOnClickListener(this);
      this.mNetflixText.setFocusable(true);
      this.mNetflixLoginMessage.setText(getResources().getString(2131493072));
      this.mNetflixLoginButton.setImageResource(2130837818);
      Logger.d("FlxMain", "MyMovies.populatePage mUser:" + this.mUser);
      if (this.mUser == null)
        break label760;
      ProfileDao.getUserLockerRights(this.collectedMoviesSuccessHandler, this.errorHandler);
      str2 = FlixsterApplication.getPlatform();
      Logger.d("FlxMain", "MyMovies.populatePage platform:" + str2);
      str3 = getResources().getString(2131493169);
      if (!"FBK".equals(str2))
        break label598;
      TextView localTextView3 = this.mFacebookStatus;
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = this.mUser.displayName;
      localTextView3.setText(String.format(str3, arrayOfObject2));
      this.mFacebookIcon.setImageResource(2130837648);
      this.mFlixsterLayout.setVisibility(8);
      this.mHeaderLayout.setVisibility(0);
      ((TextView)this.mHeaderLayout.findViewById(2131165346)).setText(this.mUser.displayName);
      localImageView = (ImageView)this.mHeaderLayout.findViewById(2131165344);
      if (!"FLX".equals(str2))
        break label662;
      localImageView.setBackgroundResource(2130837692);
      label294: if (this.mUser.bitmap == null)
        break label682;
      ((ImageView)this.mHeaderLayout.findViewById(2131165343)).setImageBitmap(this.mUser.bitmap);
    }
    TextView localTextView2;
    while (true)
    {
      ((TextView)this.mHeaderLayout.findViewById(2131165349)).setText(this.mUser.friendCount + " " + getResources().getString(2131492994));
      ((TextView)this.mHeaderLayout.findViewById(2131165348)).setText(this.mUser.ratingCount + " " + getResources().getString(2131492995));
      ((TextView)this.mHeaderLayout.findViewById(2131165347)).setText(this.mUser.wtsCount + " " + getResources().getString(2131492996));
      localTextView2 = (TextView)this.mHeaderLayout.findViewById(2131165350);
      int i = this.mUser.collectionsCount;
      if (i <= 0)
        break label752;
      localTextView2.setText(i + " " + getResources().getString(2131492997));
      localTextView2.setVisibility(0);
      return;
      this.mNetflixLoginMessage.setText(getResources().getString(2131493071));
      this.mNetflixLoginButton.setImageResource(2130837815);
      this.mNetflixText.setVisibility(8);
      break;
      label598: if (!"FLX".equals(str2))
        break label234;
      TextView localTextView1 = this.mFlixsterStatus;
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = this.mUser.displayName;
      localTextView1.setText(String.format(str3, arrayOfObject1));
      this.mFlixsterIcon.setImageResource(2130837818);
      this.mFacebookLayout.setVisibility(8);
      break label234;
      label662: if (!"FBK".equals(str2))
        break label294;
      localImageView.setBackgroundResource(2130837833);
      break label294;
      label682: if ((this.mUser.profileImage != null) && (this.mUser.profileImage.startsWith("http://")))
      {
        TimerTask local6 = new TimerTask()
        {
          public void run()
          {
            try
            {
              MyMoviesFragment.this.mUser.fetchProfileBitmap();
              MyMoviesFragment.this.mRefreshHandler.sendEmptyMessage(0);
              return;
            }
            catch (IOException localIOException)
            {
              while (true)
                Logger.e("FlxMain", "Failed to get profile bitmap", localIOException);
            }
          }
        };
        if (((Main)getActivity()).mPageTimer != null)
          ((Main)getActivity()).mPageTimer.schedule(local6, 100L);
      }
    }
    label752: localTextView2.setVisibility(8);
    return;
    label760: String str1 = getResources().getString(2131493071);
    this.mHeaderLayout.setVisibility(8);
    this.mFacebookStatus.setText(str1);
    this.mFacebookIcon.setImageResource(2130837642);
    this.mFacebookLayout.setVisibility(0);
    this.mFlixsterStatus.setText(str1);
    this.mFlixsterIcon.setImageResource(2130837815);
    this.mFlixsterLayout.setVisibility(0);
    this.mFriendText.setVisibility(8);
    this.mMovieListsText.setVisibility(8);
    this.mCollectionText.setVisibility(8);
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local5 = new TimerTask()
    {
      public void run()
      {
        if (((Main)MyMoviesFragment.this.getActivity() == null) || (MyMoviesFragment.this.isRemoving()))
          return;
        try
        {
          Logger.d("FlxMain", "MyMoviesPage.scheduleUpdatePageTask get mUser:" + MyMoviesFragment.this.mUser);
          if (MyMoviesFragment.this.mUser == null)
            MyMoviesFragment.this.mUser = ProfileDao.fetchUser();
          Logger.d("FlxMain", "MyMoviesPage.scheduleUpdatePageTask fetch mUser.bitmap");
          if ((MyMoviesFragment.this.mUser != null) && (MyMoviesFragment.this.mUser.bitmap == null))
          {
            MyMoviesFragment.this.mUser.fetchProfileBitmap();
            Logger.d("FlxMain", "MyMoviesPage.scheduleUpdatePageTask mUser.bitmap:" + MyMoviesFragment.this.mUser.bitmap);
          }
          Logger.d("FlxMain", "MyMoviesPage.scheduleUpdatePageTask - calling refresh");
          MyMoviesFragment.this.mRefreshHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
            Logger.e("FlxMain", "Failed to get mUser");
        }
        catch (IOException localIOException)
        {
          while (true)
            Logger.e("FlxMain", "Failed to get profile bitmap");
        }
      }
    };
    Main localMain = (Main)getActivity();
    if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
      localMain.mPageTimer.schedule(local5, 100L);
  }

  protected Class<? extends FlixsterFragment> getInitialFragment()
  {
    return CollectionPromoFragment.class;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }

  public void onClick(View paramView)
  {
    Logger.d("FlxMain", "MyMovies.onClick view.getId:" + paramView.getId());
    String str = FlixsterApplication.getPlatformUsername();
    switch (paramView.getId())
    {
    default:
      Logger.e("FlxMain", "MyMovies.onClick");
      return;
    case 2131165648:
      Intent localIntent8 = new Intent(getActivity(), NetflixQueueFragment.class);
      localIntent8.setFlags(131072);
      ((Main)getActivity()).startFragment(localIntent8, NetflixQueueFragment.class);
      return;
    case 2131165646:
      Intent localIntent7 = new Intent(getActivity(), MyMovieCollectionFragment.class);
      ((Main)getActivity()).startFragment(localIntent7, MyMovieCollectionFragment.class);
      return;
    case 2131165649:
      Intent localIntent6 = new Intent(getActivity(), MoviesIWantToSeePage.class);
      Logger.d("FlxMain", "MyMoviesPage.onClick username:" + str);
      if (str != null)
        localIntent6.putExtra("net.flixster.IsConnected", true);
      while (true)
      {
        startActivity(localIntent6);
        return;
        localIntent6.putExtra("net.flixster.IsConnected", false);
      }
    case 2131165650:
      Intent localIntent5 = new Intent(getActivity(), MyRatedPage.class);
      if (str != null)
        localIntent5.putExtra("net.flixster.IsConnected", true);
      while (true)
      {
        startActivity(localIntent5);
        return;
        localIntent5.putExtra("net.flixster.IsConnected", false);
      }
    case 2131165651:
      Intent localIntent4 = new Intent(getActivity(), FriendsRatedPage.class);
      if (str != null)
        localIntent4.putExtra("net.flixster.IsConnected", true);
      while (true)
      {
        startActivity(localIntent4);
        return;
        localIntent4.putExtra("net.flixster.IsConnected", false);
      }
    case 2131165652:
      Intent localIntent3 = new Intent(getActivity(), FriendsPage.class);
      if (str != null)
        localIntent3.putExtra("net.flixster.IsConnected", true);
      while (true)
      {
        startActivity(localIntent3);
        return;
        localIntent3.putExtra("net.flixster.IsConnected", false);
      }
    case 2131165386:
      Intent localIntent2 = new Intent();
      localIntent2.putExtra("KEY_PHOTO_FILTER", "top-actresses");
      ((Main)getActivity()).startFragment(localIntent2, PhotoGalleryFragment.class, 0, MyMoviesFragment.class);
      return;
    case 2131165387:
      ((Main)getActivity()).startFragment(new Intent(), TopActorsFragment.class);
      return;
    case 2131165653:
      if ((this.mUser == null) || ("FLX".equals(FlixsterApplication.getPlatform())))
      {
        startActivity(new Intent(getActivity(), FacebookAuth.class));
        return;
      }
      if (FlixsterApplication.sFacebook.isSessionValid())
      {
        SessionEvents.onLogoutBegin();
        if (this.mAsyncRunner == null)
          this.mAsyncRunner = new AsyncFacebookRunner(FlixsterApplication.sFacebook);
      }
      onCreateDialog(0).show();
      return;
    case 2131165658:
      if ((str == null) || ("FBK".equals(FlixsterApplication.getPlatform())))
      {
        startActivity(new Intent(getActivity(), FlixsterLoginPage.class));
        return;
      }
      onCreateDialog(1).show();
      return;
    case 2131165663:
    }
    if (FlixsterApplication.getNetflixUserId() == null)
    {
      Intent localIntent1 = new Intent(getActivity(), NetflixAuthFragment.class);
      ((Main)getActivity()).startFragment(localIntent1, NetflixAuthFragment.class);
      return;
    }
    onCreateDialog(2).show();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 0:
      AlertDialog.Builder localBuilder3 = new AlertDialog.Builder(getActivity());
      String str5 = String.format(getResources().getString(2131493102), new Object[] { "Facebook" });
      String str6 = String.format(getResources().getString(2131493103), new Object[] { "Facebook" });
      localBuilder3.setTitle(str5);
      localBuilder3.setMessage(str6);
      localBuilder3.setCancelable(true);
      localBuilder3.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          AccountManager.instance().fbLogout(MyMoviesFragment.this.getActivity());
          MyMoviesFragment.this.mUser = null;
          ((Main)MyMoviesFragment.this.getActivity()).startFragment(new Intent(), MyMoviesFragment.this.getInitialFragment(), 0, MyMoviesFragment.class);
          MyMoviesFragment.this.populatePage();
        }
      });
      localBuilder3.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder3.create();
    case 1:
      AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(getActivity());
      String str3 = String.format(getResources().getString(2131493102), new Object[] { "Flixster" });
      String str4 = String.format(getResources().getString(2131493103), new Object[] { "Flixster" });
      localBuilder2.setTitle(str3);
      localBuilder2.setMessage(str4);
      localBuilder2.setCancelable(true);
      localBuilder2.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          AccountManager.instance().onFlxLogout();
          MyMoviesFragment.this.mUser = null;
          ((Main)MyMoviesFragment.this.getActivity()).startFragment(new Intent(), MyMoviesFragment.this.getInitialFragment(), 0, MyMoviesFragment.class);
          MyMoviesFragment.this.populatePage();
        }
      });
      localBuilder2.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder2.create();
    case 2:
      AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(getActivity());
      String str1 = String.format(getResources().getString(2131493102), new Object[] { "Netflix" });
      String str2 = String.format(getResources().getString(2131493103), new Object[] { "Netflix" });
      localBuilder1.setTitle(str1);
      localBuilder1.setMessage(str2);
      localBuilder1.setCancelable(true);
      localBuilder1.setPositiveButton(getResources().getString(2131493171), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          FlixsterApplication.setNetflixOAuthToken(null);
          FlixsterApplication.setNetflixOAuthTokenSecret(null);
          FlixsterApplication.setNetflixUserId(null);
          MyMoviesFragment.this.populatePage();
        }
      });
      localBuilder1.setNegativeButton(getResources().getString(2131492938), null);
      return localBuilder1.create();
    case 3:
      return new AlertDialog.Builder(getActivity()).setMessage("Refresh the window").setTitle("Window Refresh").setCancelable(true).setNegativeButton(getResources().getString(2131492938), null).create();
    case 4:
    }
    return new AlertDialog.Builder(getActivity()).setMessage(getResources().getString(2131493259)).setTitle(getResources().getString(2131493258)).setCancelable(true).setNegativeButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
      }
    }).setCancelable(true).setPositiveButton(getResources().getString(2131493258), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(MyMoviesFragment.this.getResources().getString(2131493256)));
        MyMoviesFragment.this.startActivity(localIntent);
      }
    }).create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903138, null);
    this.mNetflixLayout = ((RelativeLayout)localView.findViewById(2131165663));
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()))
    {
      this.mNetflixLayout.setOnClickListener(this);
      this.mNetflixLayout.setFocusable(true);
    }
    while (true)
    {
      this.mNetflixLoginMessage = ((TextView)localView.findViewById(2131165385));
      this.mNetflixLoginButton = ((ImageView)localView.findViewById(2131165384));
      this.mNetflixText = ((TextView)localView.findViewById(2131165648));
      this.mMovieListsText = ((TextView)localView.findViewById(2131165645));
      this.mCollectionText = ((TextView)localView.findViewById(2131165646));
      this.mCollectionText.setOnClickListener(this);
      this.mCollectionText.setFocusable(true);
      this.mWantToSeeText = ((TextView)localView.findViewById(2131165649));
      this.mWantToSeeText.setOnClickListener(this);
      this.mWantToSeeText.setFocusable(true);
      this.mRatedText = ((TextView)localView.findViewById(2131165650));
      this.mRatedText.setOnClickListener(this);
      this.mRatedText.setFocusable(true);
      this.mFriendRatedText = ((TextView)localView.findViewById(2131165651));
      this.mFriendRatedText.setOnClickListener(this);
      this.mFriendRatedText.setFocusable(true);
      this.mFriendText = ((TextView)localView.findViewById(2131165652));
      this.mFriendText.setOnClickListener(this);
      this.mFriendText.setFocusable(true);
      this.mPhotosText = ((TextView)localView.findViewById(2131165386));
      this.mPhotosText.setOnClickListener(this);
      this.mPhotosText.setFocusable(true);
      this.mActorsText = ((TextView)localView.findViewById(2131165387));
      this.mActorsText.setOnClickListener(this);
      this.mActorsText.setFocusable(true);
      this.mFlixsterNewsText = ((TextView)localView.findViewById(2131165388));
      this.mFlixsterNewsText.setVisibility(8);
      this.mFacebookLayout = ((RelativeLayout)localView.findViewById(2131165653));
      this.mFacebookIcon = ((ImageView)this.mFacebookLayout.findViewById(2131165656));
      this.mFacebookStatus = ((TextView)this.mFacebookLayout.findViewById(2131165657));
      this.mFacebookLayout.setOnClickListener(this);
      this.mFacebookLayout.setFocusable(true);
      this.mFlixsterLayout = ((RelativeLayout)localView.findViewById(2131165658));
      this.mFlixsterLayout.setOnClickListener(this);
      this.mFlixsterLayout.setFocusable(true);
      this.mFlixsterIcon = ((ImageView)localView.findViewById(2131165661));
      this.mFlixsterStatus = ((TextView)localView.findViewById(2131165662));
      this.mHeaderLayout = ((RelativeLayout)localView.findViewById(2131165342));
      return localView;
      this.mNetflixLayout.setVisibility(8);
    }
  }

  public void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "MyMoviesPage.onResume()");
    trackPage();
    scheduleUpdatePageTask();
    this.mRefreshHandler.sendEmptyMessage(0);
    populatePage();
    if (!this.isInitialContentLoaded)
    {
      this.isInitialContentLoaded = true;
      this.initialContentLoadingHandler.sendEmptyMessage(0);
    }
  }

  public void trackPage()
  {
    Trackers.instance().track("/mymovies/home", "My Movies - Settings");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.MyMoviesFragment
 * JD-Core Version:    0.6.2
 */