package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.util.Iterator;
import java.util.Set;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.NetflixDao;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.http.HttpParameters;

@TargetApi(11)
public class NetflixAuthFragment extends FlixsterFragment
  implements View.OnClickListener
{
  public static final int LAYOUT_SPLASH = 0;
  public static final String LAYOUT_STATE = "LayoutState";
  public static final int LAYOUT_WEB = 1;
  private final Handler authUrlHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)NetflixAuthFragment.this.getActivity() == null) || (NetflixAuthFragment.this.isRemoving()))
        return;
      StringBuilder localStringBuilder = new StringBuilder((String)paramAnonymousMessage.obj);
      localStringBuilder.append("&application_name=").append("Movies");
      localStringBuilder.append("&oauth_consumer_key=").append("rtuhbkms7jmpsn3sft3va7y6");
      Logger.d("FlxMain", "NetflixAuth.onCreate(.) authUrl:" + localStringBuilder.toString());
      NetflixAuthFragment.this.mWebView.loadUrl(localStringBuilder.toString());
    }
  };
  private volatile OAuthConsumer mConsumer;
  private LayoutInflater mLayoutInflater;
  private int mLayoutState = 0;
  private Button mLoginButton;
  private Button mNewAccountButton;
  private volatile OAuthProvider mProvider;
  private RelativeLayout mSelectLayout;
  private WebView mWebView;
  private final Handler netflixPageFinishedHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixAuthFragment.this.isRemoving())
        return;
      HttpParameters localHttpParameters = NetflixAuthFragment.this.mProvider.getResponseParameters();
      Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient mConsumer.getTokenSecret():" + NetflixAuthFragment.this.mConsumer.getTokenSecret() + " Map:" + localHttpParameters);
      FlixsterApplication.setNetflixOAuthToken(NetflixAuthFragment.this.mConsumer.getToken());
      FlixsterApplication.setNetflixOAuthTokenSecret(NetflixAuthFragment.this.mConsumer.getTokenSecret());
      FlixsterApplication.setNetflixUserId(localHttpParameters.getFirst("user_id"));
      Trackers.instance().track("/netflix/login/success", "Netflix Login Success");
      ((Main)NetflixAuthFragment.this.getActivity()).peekListBackStack().onResume();
      ((Main)NetflixAuthFragment.this.getActivity()).popContentBackStack();
    }
  };
  private final Runnable retrieveTokenRunnable = new Runnable()
  {
    public void run()
    {
      try
      {
        NetflixAuthFragment.this.mProvider.retrieveAccessToken(NetflixAuthFragment.this.mConsumer, null);
        NetflixAuthFragment.this.netflixPageFinishedHandler.sendEmptyMessage(0);
        return;
      }
      catch (OAuthMessageSignerException localOAuthMessageSignerException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthMessageSignerException", localOAuthMessageSignerException);
        return;
      }
      catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthNotAuthorizedException responseBody:" + localOAuthNotAuthorizedException.getResponseBody(), localOAuthNotAuthorizedException);
        return;
      }
      catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthExpectationFailedException", localOAuthExpectationFailedException);
        return;
      }
      catch (OAuthCommunicationException localOAuthCommunicationException)
      {
        Logger.e("FlxMain", "NetflixAuth.onPageFinished OAuthCommunicationException", localOAuthCommunicationException);
      }
    }
  };
  private final Runnable tokenRequestRunnable = new Runnable()
  {
    public void run()
    {
      try
      {
        String str = NetflixAuthFragment.this.mProvider.retrieveRequestToken(NetflixAuthFragment.this.mConsumer, "flixster://netflix/queue");
        Message localMessage = Message.obtain();
        localMessage.obj = str;
        NetflixAuthFragment.this.authUrlHandler.sendMessage(localMessage);
        return;
      }
      catch (OAuthMessageSignerException localOAuthMessageSignerException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthMessageSignerException", localOAuthMessageSignerException);
        return;
      }
      catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthNotAuthorizedException", localOAuthNotAuthorizedException);
        return;
      }
      catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthExpectationFailedException", localOAuthExpectationFailedException);
        return;
      }
      catch (OAuthCommunicationException localOAuthCommunicationException)
      {
        Logger.e("FlxMain", "NetflixAuth.onCreate() OAuthCommunicationException", localOAuthCommunicationException);
      }
    }
  };

  public static int getFlixFragId()
  {
    return 109;
  }

  public static NetflixAuthFragment newInstance(Bundle paramBundle)
  {
    NetflixAuthFragment localNetflixAuthFragment = new NetflixAuthFragment();
    localNetflixAuthFragment.setArguments(paramBundle);
    return localNetflixAuthFragment;
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165684:
      this.mLayoutState = 1;
      this.mSelectLayout.setVisibility(8);
      Trackers.instance().track("/netflix/login/dialog", "Netflix Login");
      return;
    case 2131165685:
    }
    Trackers.instance().track("/netflix/newaccnt", "Start New Account");
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817")));
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    NetflixDao.trackNewAccountImpression();
    this.mConsumer = NetflixDao.getNewConsumer();
    this.mProvider = NetflixDao.getNewProvider();
    WorkerThreads.instance().invokeLater(this.tokenRequestRunnable);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    View localView = this.mLayoutInflater.inflate(2130903142, null);
    this.mWebView = ((WebView)localView.findViewById(2131165681));
    WebSettings localWebSettings = this.mWebView.getSettings();
    localWebSettings.setSupportZoom(false);
    localWebSettings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
    localWebSettings.setJavaScriptEnabled(true);
    NetflixWebViewClient localNetflixWebViewClient = new NetflixWebViewClient(null);
    this.mWebView.setWebViewClient(localNetflixWebViewClient);
    this.mSelectLayout = ((RelativeLayout)localView.findViewById(2131165682));
    this.mLoginButton = ((Button)localView.findViewById(2131165684));
    this.mLoginButton.setOnClickListener(this);
    this.mNewAccountButton = ((Button)localView.findViewById(2131165685));
    this.mNewAccountButton.setOnClickListener(this);
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    if (this.mLayoutState == 1)
    {
      this.mSelectLayout.setVisibility(8);
      return;
    }
    Logger.d("FlxMain", "NetflixAuth onResume() NetflixLinkSynergy()");
    NetflixDao.trackNewAccountReferral();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "NetflixAuth.onSaveInstanceState()");
    paramBundle.putInt("LayoutState", this.mLayoutState);
  }

  public void trackPage()
  {
    Trackers.instance().track("/netflix/invalid", null);
  }

  private class NetflixWebViewClient extends WebViewClient
  {
    private NetflixWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient.onPageFinished(..) url:" + paramString);
      NetflixAuthFragment.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('html')[0].style.backgroundColor='#9A0904';");
      int i = FlixsterApplication.getAndroidBuildInt();
      if ((i >= 5) && (i < 8))
        NetflixAuthFragment.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('input')[6].style.paddingTop='12px';");
      Iterator localIterator1;
      Iterator localIterator2;
      if (paramString.startsWith("flixster:"))
      {
        NetflixAuthFragment.this.mWebView.loadUrl("javascript:window.document.body.innerHTML='<br><br><br><center><h2>Loading...</h2></center>';");
        NetflixAuthFragment.this.mWebView.loadUrl("javascript:window.document.getElementsByTagName('body')[0].style.margin='-10px';");
        Logger.d("FlxMain", "NetflixAuth.FlixsterWebViewClient pre access token mConsumer.getTokenSecret():" + NetflixAuthFragment.this.mConsumer.getTokenSecret());
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() getAccessTokenEndpointUrl:" + NetflixAuthFragment.this.mProvider.getAccessTokenEndpointUrl());
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() isOAuth10a:" + NetflixAuthFragment.this.mProvider.isOAuth10a());
        localIterator1 = NetflixAuthFragment.this.mProvider.getResponseParameters().keySet().iterator();
        if (localIterator1.hasNext())
          break label273;
        localIterator2 = NetflixAuthFragment.this.mConsumer.getRequestParameters().keySet().iterator();
      }
      while (true)
      {
        if (!localIterator2.hasNext())
        {
          WorkerThreads.instance().invokeLater(NetflixAuthFragment.this.retrieveTokenRunnable);
          return;
          label273: String str1 = (String)localIterator1.next();
          Logger.d("FlxMain", "NetflixAuth.onPageFinished() getResponseParameters:" + str1 + " = " + NetflixAuthFragment.this.mProvider.getResponseParameters().get(str1));
          break;
        }
        String str2 = (String)localIterator2.next();
        Logger.d("FlxMain", "NetflixAuth.onPageFinished() getRequestParameters:" + str2 + " = " + NetflixAuthFragment.this.mConsumer.getRequestParameters().get(str2));
      }
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      super.shouldOverrideUrlLoading(paramWebView, paramString);
      Logger.d("FlxMain", "FacebookAuth.FlixsterWebViewClient.shouldOverrideUrlLoading(..) url:" + paramString);
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.NetflixAuthFragment
 * JD-Core Version:    0.6.2
 */