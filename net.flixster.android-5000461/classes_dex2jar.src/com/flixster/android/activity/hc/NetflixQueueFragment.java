package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.NetflixDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixQueueItem;
import net.flixster.android.model.TouchInterceptor;
import net.flixster.android.model.TouchInterceptor.DropListener;
import net.flixster.android.model.TouchInterceptor.RemoveListener;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

@TargetApi(11)
public class NetflixQueueFragment extends FlixsterFragment
  implements View.OnClickListener
{
  private static final int DIALOG_LOADING_MOVIES = 1;
  private static final int DIALOG_NETWORKFAIL = 2;
  public static final int MORE_CLICKFORMORE = 2;
  public static final int MORE_EMPTYQUEUE = 1;
  public static final int MORE_ENDOFQUEUE = 3;
  public static final int MORE_UNKNOWN = 0;
  public static final int NAV_ATHOME = 4;
  public static final int NAV_DVD = 1;
  public static final int NAV_INSTANT = 2;
  public static final int NAV_NONE = 0;
  public static final int NAV_SAVED = 3;
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)NetflixQueueFragment.this.getActivity();
      if ((localMain == null) || (NetflixQueueFragment.this.isRemoving()));
      int i;
      do
      {
        return;
        i = paramAnonymousMessage.what;
      }
      while (NetflixQueueFragment.this.mNavSelect != i);
      int j = NetflixQueueFragment.this.mNavSelect;
      Map localMap = null;
      switch (j)
      {
      default:
      case 1:
      case 2:
      case 3:
      case 4:
      }
      while (true)
      {
        Movie localMovie = null;
        if (localMap != null)
        {
          NetflixQueueItem localNetflixQueueItem = (NetflixQueueItem)localMap.get("netflixQueueItem");
          localMovie = null;
          if (localNetflixQueueItem != null)
            localMovie = localNetflixQueueItem.mMovie;
        }
        if (localMovie == null)
          break;
        Intent localIntent = new Intent("DETAILS", null, localMain, MovieDetailsFragment.class);
        localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
        localMain.startFragment(localIntent, MovieDetailsFragment.class);
        return;
        int i1 = NetflixQueueFragment.this.mNetflixQueueDiscItemHashList.size();
        localMap = null;
        if (i1 > 0)
        {
          localMap = (Map)NetflixQueueFragment.this.mNetflixQueueDiscItemHashList.get(0);
          continue;
          int n = NetflixQueueFragment.this.mNetflixQueueInstantItemHashList.size();
          localMap = null;
          if (n > 0)
          {
            localMap = (Map)NetflixQueueFragment.this.mNetflixQueueInstantItemHashList.get(0);
            continue;
            int m = NetflixQueueFragment.this.mNetflixQueueSavedItemHashList.size();
            localMap = null;
            if (m > 0)
            {
              localMap = (Map)NetflixQueueFragment.this.mNetflixQueueSavedItemHashList.get(0);
              continue;
              int k = NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList.size();
              localMap = null;
              if (k > 0)
                localMap = (Map)NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList.get(0);
            }
          }
        }
      }
    }
  };
  NetflixQueueFragmentAdapter mAdapterSelected;
  NetflixQueueFragmentAdapter mAtHomeAdapter;
  protected HashMap<String, HashMap<String, Object>> mCheckedMap;
  NetflixQueueFragmentAdapter mDiscAdapter;
  private TouchInterceptor.DropListener mDropListener = new TouchInterceptor.DropListener()
  {
    public void drop(int paramAnonymousInt1, int paramAnonymousInt2)
    {
      Logger.d("FlxMain", "NetflixQueuePage.mDropListener.drop(from:" + paramAnonymousInt1 + ", to:" + paramAnonymousInt2 + ") mOffsetSelect[mNavSelect]:" + NetflixQueueFragment.this.mOffsetSelect[NetflixQueueFragment.this.mNavSelect]);
      int i = -3 + NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.size();
      Logger.d("FlxMain", "NetflixQueuePage.mDropListener.drop(from:" + paramAnonymousInt1 + ", to:" + paramAnonymousInt2 + ") lastMovieIndex:" + i);
      if (paramAnonymousInt2 > i)
        paramAnonymousInt2 = i;
      Logger.d("FlxMain", "NetflixQueuePage.mDropListener.drop(from:" + paramAnonymousInt1 + ", to:" + paramAnonymousInt2 + ")");
      if (paramAnonymousInt2 != paramAnonymousInt1)
      {
        Map localMap = (Map)NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.get(paramAnonymousInt1);
        NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.remove(paramAnonymousInt1);
        NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.add(paramAnonymousInt2, localMap);
        NetflixQueueFragment.this.mAdapterSelected.notifyDataSetChanged();
        NetflixQueueItem localNetflixQueueItem = (NetflixQueueItem)localMap.get("netflixQueueItem");
        Logger.d("FlxMain", "NetflixQueuePage.mDropListener.drop calling ScheduleDiscMove");
        NetflixQueueFragment.this.ScheduleDiscMove(localNetflixQueueItem.getProperty("id"), paramAnonymousInt2 + 1);
      }
    }
  };
  private boolean mEditMode = true;
  NetflixQueueFragmentAdapter mInstantAdapter;
  public boolean mIsMoreVisible = true;
  public int mLastPosition = -1;
  public LayoutInflater mLayoutInflater;
  private Map<String, Object> mLogoNetflixItemHash;
  private int[] mMoreIndexSelect = null;
  private Map<String, Object> mMoreNetflixQueueAtHomeItemHash;
  private Map<String, Object> mMoreNetflixQueueDiscItemHash;
  private Map<String, Object> mMoreNetflixQueueInstantItemHash;
  private Map<String, Object> mMoreNetflixQueueSavedItemHash;
  public int[] mMoreStateSelect = new int[5];
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
      case 2131165784:
      case 2131165786:
      case 2131165785:
      case 2131165787:
      }
      do
      {
        do
        {
          do
          {
            do
            {
              return;
              Logger.d("FlxMain", "NetflixQueuePage.navListener - disc");
              Trackers.instance().track("/netflix/queue/dvd", "Netflix DVDs");
              NetflixQueueFragment.this.mNavSelect = 1;
              NetflixQueueFragment.this.mCheckedMap.clear();
              NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList = NetflixQueueFragment.this.mNetflixQueueDiscItemHashList;
              NetflixQueueFragment.this.mAdapterSelected = NetflixQueueFragment.this.mDiscAdapter;
              NetflixQueueFragment.this.mNetflixList.setAdapter(NetflixQueueFragment.this.mAdapterSelected);
              if (FlixsterApplication.sNetflixListIsDirty[1] != 0)
              {
                NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.clear();
                NetflixQueueFragment.this.mMoreIndexSelect[1] = 0;
                NetflixQueueFragment.this.mOffsetSelect[1] = 0;
              }
            }
            while (!NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.isEmpty());
            NetflixQueueFragment.this.showLoadingDialog.sendEmptyMessage(0);
            NetflixQueueFragment.this.ScheduleLoadMoviesTask(NetflixQueueFragment.this.mOffsetSelect[1]);
            return;
            Logger.d("FlxMain", "NetflixQueuePage.navListener - instant");
            Trackers.instance().track("/netflix/queue/instant", "Netflix Instant");
            NetflixQueueFragment.this.mNavSelect = 2;
            NetflixQueueFragment.this.mCheckedMap.clear();
            NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList = NetflixQueueFragment.this.mNetflixQueueInstantItemHashList;
            NetflixQueueFragment.this.mAdapterSelected = NetflixQueueFragment.this.mInstantAdapter;
            NetflixQueueFragment.this.mNetflixList.setAdapter(NetflixQueueFragment.this.mAdapterSelected);
            if (FlixsterApplication.sNetflixListIsDirty[2] != 0)
            {
              NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.clear();
              NetflixQueueFragment.this.mMoreIndexSelect[2] = 0;
              NetflixQueueFragment.this.mOffsetSelect[2] = 0;
            }
          }
          while (!NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.isEmpty());
          NetflixQueueFragment.this.showLoadingDialog.sendEmptyMessage(0);
          NetflixQueueFragment.this.ScheduleLoadMoviesTask(NetflixQueueFragment.this.mOffsetSelect[2]);
          return;
          Logger.d("FlxMain", "NetflixQueuePage.navListener - saved");
          Trackers.instance().track("/netflix/queue/saved", "Netflix Saved");
          NetflixQueueFragment.this.mNavSelect = 3;
          NetflixQueueFragment.this.mCheckedMap.clear();
          NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList = NetflixQueueFragment.this.mNetflixQueueSavedItemHashList;
          NetflixQueueFragment.this.mAdapterSelected = NetflixQueueFragment.this.mSavedAdapter;
          NetflixQueueFragment.this.mNetflixList.setAdapter(NetflixQueueFragment.this.mAdapterSelected);
          if (FlixsterApplication.sNetflixListIsDirty[3] != 0)
          {
            NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.clear();
            NetflixQueueFragment.this.mMoreIndexSelect[3] = 0;
            NetflixQueueFragment.this.mOffsetSelect[3] = 0;
          }
        }
        while (!NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.isEmpty());
        NetflixQueueFragment.this.showLoadingDialog.sendEmptyMessage(0);
        NetflixQueueFragment.this.ScheduleLoadMoviesTask(NetflixQueueFragment.this.mOffsetSelect[3]);
        return;
        Logger.d("FlxMain", "NetflixQueuePage.navListener - saved");
        Trackers.instance().track("/netflix/queue/athome", "Netflix AtHome");
        NetflixQueueFragment.this.mNavSelect = 4;
        NetflixQueueFragment.this.mCheckedMap.clear();
        NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList = NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList;
        NetflixQueueFragment.this.mAdapterSelected = NetflixQueueFragment.this.mAtHomeAdapter;
        NetflixQueueFragment.this.mNetflixList.setAdapter(NetflixQueueFragment.this.mAdapterSelected);
        if (FlixsterApplication.sNetflixListIsDirty[4] != 0)
        {
          NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.clear();
          NetflixQueueFragment.this.mMoreIndexSelect[4] = 0;
          NetflixQueueFragment.this.mOffsetSelect[4] = 0;
        }
      }
      while (!NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.isEmpty());
      NetflixQueueFragment.this.showLoadingDialog.sendEmptyMessage(0);
      NetflixQueueFragment.this.ScheduleLoadMoviesTask(NetflixQueueFragment.this.mOffsetSelect[4]);
    }
  };
  public int mNavSelect = 1;
  private SubNavBar mNavbar;
  private LinearLayout mNavbarHolder;
  private RelativeLayout mNetflixContextMenu;
  private ListView mNetflixList;
  private List<Map<String, Object>> mNetflixQueueAtHomeItemHashList = null;
  private List<Map<String, Object>> mNetflixQueueDiscItemHashList = null;
  private List<Map<String, Object>> mNetflixQueueInstantItemHashList = null;
  private List<Map<String, Object>> mNetflixQueueSavedItemHashList = null;
  private List<Map<String, Object>> mNetflixQueueSelectedItemHashList = null;
  private int[] mOffsetSelect = null;
  private TouchInterceptor.RemoveListener mRemoveListener = new TouchInterceptor.RemoveListener()
  {
    public void remove(int paramAnonymousInt)
    {
      Logger.d("FlxMain", "NetflixQueuePage.RemoveListener.remove()");
    }
  };
  private Button mRemoveQueueButton;
  NetflixQueueFragmentAdapter mSavedAdapter;
  private Timer mTimer;
  private Handler postMovieChangeHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixQueueFragment.this.isRemoving())
        return;
      Logger.d("FlxMain", "NetflixQueuePage.postMovieChangeHandler");
      NetflixQueueFragment.this.removeLoadingDialog.sendEmptyMessage(0);
      NetflixQueueFragment.this.mNetflixList.invalidateViews();
      if (NetflixQueueFragment.this.mCheckedMap.size() == 0)
      {
        NetflixQueueFragment.this.mNetflixContextMenu.setVisibility(8);
        return;
      }
      NetflixQueueFragment.this.mNetflixContextMenu.setVisibility(0);
    }
  };
  private Handler removeLoadingDialog = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixQueueFragment.this.isRemoving());
      Activity localActivity;
      do
      {
        return;
        localActivity = NetflixQueueFragment.this.getActivity();
      }
      while ((localActivity == null) || (localActivity.isFinishing()));
      localActivity.removeDialog(1);
    }
  };
  private Handler removeNetworkFailDialog = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixQueueFragment.this.isRemoving());
      Activity localActivity;
      do
      {
        return;
        localActivity = NetflixQueueFragment.this.getActivity();
      }
      while ((localActivity == null) || (localActivity.isFinishing()));
      localActivity.removeDialog(2);
    }
  };
  private Handler showLoadingDialog = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixQueueFragment.this.isRemoving());
      Activity localActivity;
      do
      {
        return;
        localActivity = NetflixQueueFragment.this.getActivity();
      }
      while ((localActivity == null) || (localActivity.isFinishing()));
      localActivity.showDialog(1);
    }
  };
  private Handler showNetworkFailDialog = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (NetflixQueueFragment.this.isRemoving());
      Activity localActivity;
      do
      {
        return;
        localActivity = NetflixQueueFragment.this.getActivity();
      }
      while ((localActivity == null) || (localActivity.isFinishing()));
      localActivity.showDialog(2);
    }
  };

  private void ScheduleDiscDelete(final String paramString)
  {
    TimerTask local11 = new TimerTask()
    {
      public void run()
      {
        try
        {
          if (NetflixQueueFragment.this.mAdapterSelected == NetflixQueueFragment.this.mDiscAdapter)
            NetflixDao.deleteQueueItem(paramString, "/queues/disc/available");
          while (true)
          {
            NetflixQueueFragment.this.deleteItem(paramString);
            int[] arrayOfInt1 = NetflixQueueFragment.this.mOffsetSelect;
            int i = NetflixQueueFragment.this.mNavSelect;
            arrayOfInt1[i] = (-1 + arrayOfInt1[i]);
            int[] arrayOfInt2 = NetflixQueueFragment.this.mMoreIndexSelect;
            int j = NetflixQueueFragment.this.mNavSelect;
            arrayOfInt2[j] = (-1 + arrayOfInt2[j]);
            return;
            if (NetflixQueueFragment.this.mAdapterSelected != NetflixQueueFragment.this.mInstantAdapter)
              break;
            NetflixDao.deleteQueueItem(paramString, "/queues/instant/available");
          }
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          while (true)
          {
            localOAuthMessageSignerException.printStackTrace();
            return;
            if (NetflixQueueFragment.this.mAdapterSelected == NetflixQueueFragment.this.mSavedAdapter)
              NetflixDao.deleteQueueItem(paramString, "/queues/disc/saved");
          }
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          localOAuthExpectationFailedException.printStackTrace();
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          localClientProtocolException.printStackTrace();
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          localOAuthNotAuthorizedException.printStackTrace();
          return;
        }
        catch (IOException localIOException)
        {
          localIOException.printStackTrace();
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
        }
      }
    };
    Logger.d("FlxMain", "NetflixQueuePage mTimer:" + this.mTimer);
    this.mTimer.schedule(local11, 100L);
  }

  private void ScheduleDiscMove(final String paramString, final int paramInt)
  {
    TimerTask local10 = new TimerTask()
    {
      public void run()
      {
        try
        {
          if (NetflixQueueFragment.this.mAdapterSelected == NetflixQueueFragment.this.mDiscAdapter)
          {
            NetflixDao.postQueueItem("http://api-public.netflix.com/catalog/titles/movies/" + paramString, paramInt, "/queues/disc/available");
            return;
          }
          if (NetflixQueueFragment.this.mAdapterSelected == NetflixQueueFragment.this.mInstantAdapter)
          {
            NetflixDao.postQueueItem("http://api-public.netflix.com/catalog/titles/movies/" + paramString, paramInt, "/queues/instant/available");
            return;
          }
        }
        catch (OAuthMessageSignerException localOAuthMessageSignerException)
        {
          localOAuthMessageSignerException.printStackTrace();
          return;
        }
        catch (OAuthExpectationFailedException localOAuthExpectationFailedException)
        {
          localOAuthExpectationFailedException.printStackTrace();
          return;
        }
        catch (ClientProtocolException localClientProtocolException)
        {
          localClientProtocolException.printStackTrace();
          return;
        }
        catch (OAuthNotAuthorizedException localOAuthNotAuthorizedException)
        {
          localOAuthNotAuthorizedException.printStackTrace();
          return;
        }
        catch (IOException localIOException)
        {
          localIOException.printStackTrace();
          return;
        }
        catch (OAuthCommunicationException localOAuthCommunicationException)
        {
          localOAuthCommunicationException.printStackTrace();
          return;
        }
        catch (JSONException localJSONException)
        {
          localJSONException.printStackTrace();
        }
      }
    };
    Logger.d("FlxMain", "NetflixQueuePage mTimer:" + this.mTimer);
    this.mTimer.schedule(local10, 100L);
  }

  private void ScheduleLoadMoviesTask(int paramInt)
  {
    TimerTask local12 = new TimerTask()
    {
      public void run()
      {
        if (((Main)NetflixQueueFragment.this.getActivity() == null) || (NetflixQueueFragment.this.isRemoving()))
          return;
        Logger.d("FlxMain", "NetflixQueuePage.ScheduleLoadMoviesTask()");
        if (NetflixQueueFragment.this.mMoreStateSelect[this.val$currNavSelect] == 3)
          NetflixQueueFragment.this.mOffsetSelect[this.val$currNavSelect] = 0;
        switch (this.val$currNavSelect)
        {
        default:
        case 1:
        case 2:
        case 3:
        case 4:
        }
        label291: List localList1;
        label410: 
        do
        {
          List localList2;
          do
          {
            List localList3;
            do
            {
              List localList4;
              do
              {
                if (NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList != null)
                  Logger.d("FlxMain", "mNetflixQueueItemList.size():" + NetflixQueueFragment.this.mNetflixQueueSelectedItemHashList.size());
                NetflixQueueFragment.this.postMovieChangeHandler.sendEmptyMessage(0);
                if (NetflixQueueFragment.this.isInitialContentLoaded)
                  break;
                NetflixQueueFragment.this.isInitialContentLoaded = true;
                NetflixQueueFragment.this.initialContentLoadingHandler.sendEmptyMessage(this.val$currNavSelect);
                return;
                localList4 = NetflixQueueFragment.this.getDvdQueue(NetflixQueueFragment.this.mOffsetSelect[1], "/queues/disc/available", 1);
              }
              while (localList4 == null);
              if (localList4.size() > 0)
              {
                if (NetflixQueueFragment.this.mNetflixQueueDiscItemHashList == null)
                  break label291;
                NetflixQueueFragment.this.mNetflixQueueDiscItemHashList.addAll(NetflixQueueFragment.this.mMoreIndexSelect[1], localList4);
              }
              while (true)
              {
                NetflixQueueFragment.this.mMoreIndexSelect[1] = NetflixQueueFragment.this.mNetflixQueueDiscItemHashList.indexOf(NetflixQueueFragment.this.mMoreNetflixQueueDiscItemHash);
                FlixsterApplication.sNetflixListIsDirty[1] = false;
                break;
                NetflixQueueFragment.this.mNetflixQueueDiscItemHashList = localList4;
              }
              localList3 = NetflixQueueFragment.this.getDvdQueue(NetflixQueueFragment.this.mOffsetSelect[2], "/queues/instant/available", 2);
            }
            while (localList3 == null);
            if (localList3.size() > 0)
            {
              if (NetflixQueueFragment.this.mNetflixQueueInstantItemHashList == null)
                break label410;
              NetflixQueueFragment.this.mNetflixQueueInstantItemHashList.addAll(NetflixQueueFragment.this.mMoreIndexSelect[2], localList3);
            }
            while (true)
            {
              NetflixQueueFragment.this.mMoreIndexSelect[2] = NetflixQueueFragment.this.mNetflixQueueInstantItemHashList.indexOf(NetflixQueueFragment.this.mMoreNetflixQueueInstantItemHash);
              FlixsterApplication.sNetflixListIsDirty[2] = false;
              break;
              NetflixQueueFragment.this.mNetflixQueueInstantItemHashList = localList3;
            }
            localList2 = NetflixQueueFragment.this.getDvdQueue(NetflixQueueFragment.this.mOffsetSelect[3], "/queues/disc/saved", 3);
          }
          while (localList2 == null);
          if (localList2.size() > 0)
          {
            if (NetflixQueueFragment.this.mNetflixQueueSavedItemHashList == null)
              break label529;
            NetflixQueueFragment.this.mNetflixQueueSavedItemHashList.addAll(NetflixQueueFragment.this.mMoreIndexSelect[3], localList2);
          }
          while (true)
          {
            NetflixQueueFragment.this.mMoreIndexSelect[3] = NetflixQueueFragment.this.mNetflixQueueSavedItemHashList.indexOf(NetflixQueueFragment.this.mMoreNetflixQueueSavedItemHash);
            FlixsterApplication.sNetflixListIsDirty[3] = false;
            break;
            NetflixQueueFragment.this.mNetflixQueueSavedItemHashList = localList2;
          }
          localList1 = NetflixQueueFragment.this.getDvdQueue(NetflixQueueFragment.this.mOffsetSelect[4], "/at_home", 4);
        }
        while (localList1 == null);
        label529: if (localList1.size() > 0)
        {
          if (NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList == null)
            break label644;
          NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList.addAll(NetflixQueueFragment.this.mMoreIndexSelect[4], localList1);
        }
        while (true)
        {
          NetflixQueueFragment.this.mMoreIndexSelect[4] = NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList.indexOf(NetflixQueueFragment.this.mMoreNetflixQueueAtHomeItemHash);
          FlixsterApplication.sNetflixListIsDirty[4] = false;
          break;
          label644: NetflixQueueFragment.this.mNetflixQueueAtHomeItemHashList = localList1;
        }
      }
    };
    this.mTimer.schedule(local12, 100L);
  }

  private void deleteItem(String paramString)
  {
    try
    {
      Logger.d("FlxMain", "NetflixQueuePage.deleteItem() pre netflixDeleteId:" + paramString + " mNetflixQueueItemHashList.size():" + this.mNetflixQueueSelectedItemHashList.size());
      HashMap localHashMap = (HashMap)this.mCheckedMap.get(paramString);
      this.mCheckedMap.remove(paramString);
      this.mNetflixQueueSelectedItemHashList.remove(localHashMap);
      this.postMovieChangeHandler.sendEmptyMessage(0);
      Logger.d("FlxMain", "NetflixQueuePage.deleteItem() post mNetflixQueueItemHashList.size():" + this.mNetflixQueueSelectedItemHashList.size());
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  private List<Map<String, Object>> getDvdQueue(int paramInt1, String paramString, int paramInt2)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new 319	java/util/ArrayList
    //   5: dup
    //   6: invokespecial 320	java/util/ArrayList:<init>	()V
    //   9: astore 4
    //   11: new 319	java/util/ArrayList
    //   14: dup
    //   15: invokespecial 320	java/util/ArrayList:<init>	()V
    //   18: pop
    //   19: ldc 169
    //   21: ldc_w 322
    //   24: invokestatic 191	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   27: iload_1
    //   28: aload_2
    //   29: invokestatic 328	net/flixster/android/data/NetflixDao:fetchQueue	(ILjava/lang/String;)Lnet/flixster/android/model/NetflixQueue;
    //   32: astore 13
    //   34: ldc 169
    //   36: ldc_w 330
    //   39: invokestatic 191	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   42: aload 13
    //   44: invokevirtual 336	net/flixster/android/model/NetflixQueue:getNetflixQueueItemList	()Ljava/util/ArrayList;
    //   47: invokevirtual 340	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   50: astore 14
    //   52: aload 14
    //   54: invokeinterface 346 1 0
    //   59: ifne +72 -> 131
    //   62: iload_1
    //   63: ifne +33 -> 96
    //   66: iload_3
    //   67: tableswitch	default:+29 -> 96, 1:+152->219, 2:+274->341, 3:+407->474, 4:+522->589
    //   97: getfield 111	com/flixster/android/activity/hc/NetflixQueueFragment:mOffsetSelect	[I
    //   100: iload_3
    //   101: bipush 25
    //   103: aload_0
    //   104: getfield 111	com/flixster/android/activity/hc/NetflixQueueFragment:mOffsetSelect	[I
    //   107: iload_3
    //   108: iaload
    //   109: iadd
    //   110: iastore
    //   111: aload 13
    //   113: invokevirtual 349	net/flixster/android/model/NetflixQueue:getNumberOfResults	()I
    //   116: ifne +573 -> 689
    //   119: aload_0
    //   120: getfield 109	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreStateSelect	[I
    //   123: iload_3
    //   124: iconst_1
    //   125: iastore
    //   126: aload_0
    //   127: monitorexit
    //   128: aload 4
    //   130: areturn
    //   131: aload 14
    //   133: invokeinterface 353 1 0
    //   138: checkcast 355	net/flixster/android/model/NetflixQueueItem
    //   141: astore 35
    //   143: new 291	java/util/HashMap
    //   146: dup
    //   147: invokespecial 356	java/util/HashMap:<init>	()V
    //   150: astore 36
    //   152: aload 36
    //   154: ldc_w 358
    //   157: iconst_0
    //   158: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   161: invokeinterface 370 3 0
    //   166: pop
    //   167: aload 36
    //   169: ldc_w 372
    //   172: aload 35
    //   174: invokeinterface 370 3 0
    //   179: pop
    //   180: aload 4
    //   182: aload 36
    //   184: invokeinterface 375 2 0
    //   189: pop
    //   190: goto -138 -> 52
    //   193: astore 12
    //   195: ldc 169
    //   197: ldc_w 377
    //   200: aload 12
    //   202: invokestatic 381	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   205: ldc 169
    //   207: ldc_w 383
    //   210: invokestatic 385	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   213: aconst_null
    //   214: astore 4
    //   216: goto -90 -> 126
    //   219: aload_0
    //   220: new 291	java/util/HashMap
    //   223: dup
    //   224: invokespecial 356	java/util/HashMap:<init>	()V
    //   227: putfield 240	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueDiscItemHash	Ljava/util/Map;
    //   230: aload_0
    //   231: getfield 240	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueDiscItemHash	Ljava/util/Map;
    //   234: ldc_w 387
    //   237: iload_1
    //   238: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   241: invokeinterface 370 3 0
    //   246: pop
    //   247: aload_0
    //   248: getfield 240	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueDiscItemHash	Ljava/util/Map;
    //   251: ldc_w 358
    //   254: iconst_1
    //   255: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   258: invokeinterface 370 3 0
    //   263: pop
    //   264: aload 4
    //   266: aload_0
    //   267: getfield 240	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueDiscItemHash	Ljava/util/Map;
    //   270: invokeinterface 375 2 0
    //   275: pop
    //   276: aload_0
    //   277: new 291	java/util/HashMap
    //   280: dup
    //   281: invokespecial 356	java/util/HashMap:<init>	()V
    //   284: putfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   287: aload_0
    //   288: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   291: ldc_w 358
    //   294: iconst_2
    //   295: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   298: invokeinterface 370 3 0
    //   303: pop
    //   304: aload 4
    //   306: aload_0
    //   307: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   310: invokeinterface 375 2 0
    //   315: pop
    //   316: goto -220 -> 96
    //   319: astore 11
    //   321: ldc 169
    //   323: ldc_w 391
    //   326: aload 11
    //   328: invokestatic 381	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   331: goto -126 -> 205
    //   334: astore 7
    //   336: aload_0
    //   337: monitorexit
    //   338: aload 7
    //   340: athrow
    //   341: aload_0
    //   342: new 291	java/util/HashMap
    //   345: dup
    //   346: invokespecial 356	java/util/HashMap:<init>	()V
    //   349: putfield 244	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueInstantItemHash	Ljava/util/Map;
    //   352: aload_0
    //   353: getfield 244	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueInstantItemHash	Ljava/util/Map;
    //   356: ldc_w 387
    //   359: iload_1
    //   360: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   363: invokeinterface 370 3 0
    //   368: pop
    //   369: aload_0
    //   370: getfield 244	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueInstantItemHash	Ljava/util/Map;
    //   373: ldc_w 358
    //   376: iconst_1
    //   377: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   380: invokeinterface 370 3 0
    //   385: pop
    //   386: aload 4
    //   388: aload_0
    //   389: getfield 244	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueInstantItemHash	Ljava/util/Map;
    //   392: invokeinterface 375 2 0
    //   397: pop
    //   398: aload_0
    //   399: new 291	java/util/HashMap
    //   402: dup
    //   403: invokespecial 356	java/util/HashMap:<init>	()V
    //   406: putfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   409: aload_0
    //   410: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   413: ldc_w 358
    //   416: iconst_2
    //   417: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   420: invokeinterface 370 3 0
    //   425: pop
    //   426: aload 4
    //   428: aload_0
    //   429: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   432: invokeinterface 375 2 0
    //   437: pop
    //   438: goto -342 -> 96
    //   441: astore 8
    //   443: ldc 169
    //   445: ldc_w 393
    //   448: aload 8
    //   450: invokestatic 381	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   453: aload_0
    //   454: getfield 145	com/flixster/android/activity/hc/NetflixQueueFragment:removeLoadingDialog	Landroid/os/Handler;
    //   457: iconst_0
    //   458: invokevirtual 307	android/os/Handler:sendEmptyMessage	(I)Z
    //   461: pop
    //   462: aload_0
    //   463: getfield 150	com/flixster/android/activity/hc/NetflixQueueFragment:showNetworkFailDialog	Landroid/os/Handler;
    //   466: iconst_0
    //   467: invokevirtual 307	android/os/Handler:sendEmptyMessage	(I)Z
    //   470: pop
    //   471: goto -266 -> 205
    //   474: aload_0
    //   475: new 291	java/util/HashMap
    //   478: dup
    //   479: invokespecial 356	java/util/HashMap:<init>	()V
    //   482: putfield 251	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueSavedItemHash	Ljava/util/Map;
    //   485: aload_0
    //   486: getfield 251	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueSavedItemHash	Ljava/util/Map;
    //   489: ldc_w 387
    //   492: iload_1
    //   493: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   496: invokeinterface 370 3 0
    //   501: pop
    //   502: aload_0
    //   503: getfield 251	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueSavedItemHash	Ljava/util/Map;
    //   506: ldc_w 358
    //   509: iconst_1
    //   510: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   513: invokeinterface 370 3 0
    //   518: pop
    //   519: aload 4
    //   521: aload_0
    //   522: getfield 251	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueSavedItemHash	Ljava/util/Map;
    //   525: invokeinterface 375 2 0
    //   530: pop
    //   531: aload_0
    //   532: new 291	java/util/HashMap
    //   535: dup
    //   536: invokespecial 356	java/util/HashMap:<init>	()V
    //   539: putfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   542: aload_0
    //   543: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   546: ldc_w 358
    //   549: iconst_2
    //   550: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   553: invokeinterface 370 3 0
    //   558: pop
    //   559: aload 4
    //   561: aload_0
    //   562: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   565: invokeinterface 375 2 0
    //   570: pop
    //   571: goto -475 -> 96
    //   574: astore 6
    //   576: ldc 169
    //   578: ldc_w 395
    //   581: aload 6
    //   583: invokestatic 381	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   586: goto -381 -> 205
    //   589: aload_0
    //   590: new 291	java/util/HashMap
    //   593: dup
    //   594: invokespecial 356	java/util/HashMap:<init>	()V
    //   597: putfield 255	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueAtHomeItemHash	Ljava/util/Map;
    //   600: aload_0
    //   601: getfield 255	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueAtHomeItemHash	Ljava/util/Map;
    //   604: ldc_w 387
    //   607: iload_1
    //   608: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   611: invokeinterface 370 3 0
    //   616: pop
    //   617: aload_0
    //   618: getfield 255	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueAtHomeItemHash	Ljava/util/Map;
    //   621: ldc_w 358
    //   624: iconst_1
    //   625: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   628: invokeinterface 370 3 0
    //   633: pop
    //   634: aload 4
    //   636: aload_0
    //   637: getfield 255	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreNetflixQueueAtHomeItemHash	Ljava/util/Map;
    //   640: invokeinterface 375 2 0
    //   645: pop
    //   646: aload_0
    //   647: new 291	java/util/HashMap
    //   650: dup
    //   651: invokespecial 356	java/util/HashMap:<init>	()V
    //   654: putfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   657: aload_0
    //   658: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   661: ldc_w 358
    //   664: iconst_2
    //   665: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   668: invokeinterface 370 3 0
    //   673: pop
    //   674: aload 4
    //   676: aload_0
    //   677: getfield 389	com/flixster/android/activity/hc/NetflixQueueFragment:mLogoNetflixItemHash	Ljava/util/Map;
    //   680: invokeinterface 375 2 0
    //   685: pop
    //   686: goto -590 -> 96
    //   689: aload 13
    //   691: invokevirtual 349	net/flixster/android/model/NetflixQueue:getNumberOfResults	()I
    //   694: aload_0
    //   695: getfield 111	com/flixster/android/activity/hc/NetflixQueueFragment:mOffsetSelect	[I
    //   698: iload_3
    //   699: iaload
    //   700: if_icmpgt +13 -> 713
    //   703: aload_0
    //   704: getfield 109	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreStateSelect	[I
    //   707: iload_3
    //   708: iconst_3
    //   709: iastore
    //   710: goto -584 -> 126
    //   713: aload_0
    //   714: getfield 109	com/flixster/android/activity/hc/NetflixQueueFragment:mMoreStateSelect	[I
    //   717: iload_3
    //   718: iconst_2
    //   719: iastore
    //   720: goto -594 -> 126
    //
    // Exception table:
    //   from	to	target	type
    //   19	52	193	oauth/signpost/exception/OAuthMessageSignerException
    //   52	62	193	oauth/signpost/exception/OAuthMessageSignerException
    //   96	126	193	oauth/signpost/exception/OAuthMessageSignerException
    //   131	190	193	oauth/signpost/exception/OAuthMessageSignerException
    //   219	316	193	oauth/signpost/exception/OAuthMessageSignerException
    //   341	438	193	oauth/signpost/exception/OAuthMessageSignerException
    //   474	571	193	oauth/signpost/exception/OAuthMessageSignerException
    //   589	686	193	oauth/signpost/exception/OAuthMessageSignerException
    //   689	710	193	oauth/signpost/exception/OAuthMessageSignerException
    //   713	720	193	oauth/signpost/exception/OAuthMessageSignerException
    //   19	52	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   52	62	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   96	126	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   131	190	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   219	316	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   341	438	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   474	571	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   589	686	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   689	710	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   713	720	319	oauth/signpost/exception/OAuthExpectationFailedException
    //   2	19	334	finally
    //   19	52	334	finally
    //   52	62	334	finally
    //   96	126	334	finally
    //   131	190	334	finally
    //   195	205	334	finally
    //   205	213	334	finally
    //   219	316	334	finally
    //   321	331	334	finally
    //   341	438	334	finally
    //   443	471	334	finally
    //   474	571	334	finally
    //   576	586	334	finally
    //   589	686	334	finally
    //   689	710	334	finally
    //   713	720	334	finally
    //   19	52	441	java/io/IOException
    //   52	62	441	java/io/IOException
    //   96	126	441	java/io/IOException
    //   131	190	441	java/io/IOException
    //   219	316	441	java/io/IOException
    //   341	438	441	java/io/IOException
    //   474	571	441	java/io/IOException
    //   589	686	441	java/io/IOException
    //   689	710	441	java/io/IOException
    //   713	720	441	java/io/IOException
    //   19	52	574	java/lang/Exception
    //   52	62	574	java/lang/Exception
    //   96	126	574	java/lang/Exception
    //   131	190	574	java/lang/Exception
    //   219	316	574	java/lang/Exception
    //   341	438	574	java/lang/Exception
    //   474	571	574	java/lang/Exception
    //   589	686	574	java/lang/Exception
    //   689	710	574	java/lang/Exception
    //   713	720	574	java/lang/Exception
  }

  public static int getFlixFragId()
  {
    return 6;
  }

  public static MovieDetailsFragment newInstance(Bundle paramBundle)
  {
    MovieDetailsFragment localMovieDetailsFragment = new MovieDetailsFragment();
    localMovieDetailsFragment.setArguments(paramBundle);
    return localMovieDetailsFragment;
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      Logger.d("FlxMain", "NetflixQueuePage.onClick (default)");
      return;
    case 2131165691:
      HashMap localHashMap = (HashMap)paramView.getTag();
      String str = ((NetflixQueueItem)localHashMap.get("netflixQueueItem")).getProperty("id");
      boolean bool = this.mCheckedMap.containsKey(str);
      if (str != null)
      {
        if (!bool)
          break label152;
        this.mCheckedMap.remove(str);
      }
      while (this.mCheckedMap.size() == 0)
      {
        this.mNetflixContextMenu.setVisibility(8);
        return;
        this.mCheckedMap.put(str, localHashMap);
      }
      this.mNetflixContextMenu.setVisibility(0);
      return;
    case 2131165689:
      Iterator localIterator = this.mCheckedMap.keySet().iterator();
      while (localIterator.hasNext())
        ScheduleDiscDelete((String)localIterator.next());
    case 2131165486:
    case 2131165488:
      Logger.d("FlxMain", "Fire up movie details intent");
      NetflixQueueFragmentAdapter.NetflixViewItemHolder localNetflixViewItemHolder = (NetflixQueueFragmentAdapter.NetflixViewItemHolder)paramView.getTag();
      Movie localMovie = localNetflixViewItemHolder.movie;
      Intent localIntent = new Intent("DETAILS", null, getActivity(), MovieDetailsFragment.class);
      localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
      localIntent.putExtra("title", localMovie.getProperty("title"));
      if (this.mNavSelect == 1)
      {
        FlixsterApplication.sNetflixListIsDirty[2] = true;
        this.mNetflixQueueInstantItemHashList.clear();
        this.mMoreIndexSelect[2] = 0;
        this.mOffsetSelect[2] = 0;
        FlixsterApplication.sNetflixListIsDirty[3] = true;
        this.mNetflixQueueSavedItemHashList.clear();
        this.mMoreIndexSelect[3] = 0;
        this.mOffsetSelect[3] = 0;
        if (this.mLastPosition <= localNetflixViewItemHolder.position)
          break label534;
        ((Main)getActivity()).startFragment(localIntent, MovieDetailsFragment.class, 3, getClass());
      }
      while (true)
      {
        this.mLastPosition = localNetflixViewItemHolder.position;
        return;
        if (this.mNavSelect == 2)
        {
          FlixsterApplication.sNetflixListIsDirty[1] = true;
          this.mNetflixQueueDiscItemHashList.clear();
          this.mMoreIndexSelect[1] = 0;
          this.mOffsetSelect[1] = 0;
          FlixsterApplication.sNetflixListIsDirty[3] = true;
          this.mNetflixQueueSavedItemHashList.clear();
          this.mMoreIndexSelect[3] = 0;
          this.mOffsetSelect[3] = 0;
          break;
        }
        if (this.mNavSelect != 3)
          break;
        FlixsterApplication.sNetflixListIsDirty[2] = true;
        this.mNetflixQueueInstantItemHashList.clear();
        this.mMoreIndexSelect[2] = 0;
        this.mOffsetSelect[2] = 0;
        FlixsterApplication.sNetflixListIsDirty[1] = true;
        this.mNetflixQueueDiscItemHashList.clear();
        this.mMoreIndexSelect[1] = 0;
        this.mOffsetSelect[1] = 0;
        break;
        if (this.mLastPosition < localNetflixViewItemHolder.position)
          ((Main)getActivity()).startFragment(localIntent, MovieDetailsFragment.class, 1, getClass());
      }
    case 2130903146:
      label152: Logger.d("FlxMain", "NetflixQueuePage.onClick (MORE)");
      label534: this.showLoadingDialog.sendEmptyMessage(0);
      ScheduleLoadMoviesTask(this.mOffsetSelect[this.mNavSelect]);
      return;
    case 2130903145:
    }
    Logger.d("FlxMain", "NetflixQueuePage.onClick (LOGO)");
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817")));
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      ProgressDialog localProgressDialog = new ProgressDialog(getActivity());
      localProgressDialog.setMessage(getResources().getString(2131493173));
      localProgressDialog.setIndeterminate(true);
      localProgressDialog.setCancelable(true);
      localProgressDialog.setCanceledOnTouchOutside(true);
      localProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          NetflixQueueFragment.this.removeLoadingDialog.sendEmptyMessage(0);
        }
      });
      return localProgressDialog;
    case 2:
    }
    return new AlertDialog.Builder(getActivity()).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        NetflixQueueFragment.this.showLoadingDialog.sendEmptyMessage(0);
        NetflixQueueFragment.this.ScheduleLoadMoviesTask(NetflixQueueFragment.this.mOffsetSelect[NetflixQueueFragment.this.mNavSelect]);
      }
    }).setNegativeButton(getResources().getString(2131492938), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        NetflixQueueFragment.this.removeNetworkFailDialog.sendEmptyMessage(0);
      }
    }).create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    View localView = this.mLayoutInflater.inflate(2130903143, null);
    this.mCheckedMap = new HashMap();
    this.mNetflixList = ((ListView)localView.findViewById(2131165690));
    this.mNetflixList.setOnCreateContextMenuListener(this);
    if (this.mEditMode)
    {
      ((TouchInterceptor)this.mNetflixList).setDropListener(this.mDropListener);
      ((TouchInterceptor)this.mNetflixList).setRemoveListener(this.mRemoveListener);
      this.mNetflixList.setCacheColorHint(0);
    }
    this.mNetflixList.setDivider(getResources().getDrawable(2130837680));
    this.mNetflixList.setDividerHeight(getResources().getDimensionPixelOffset(2131361829));
    this.mNetflixList.setBackgroundResource(2131296267);
    this.mNetflixList.setCacheColorHint(getResources().getColor(2131296267));
    this.mRemoveQueueButton = ((Button)localView.findViewById(2131165689));
    this.mRemoveQueueButton.setOnClickListener(this);
    this.mNetflixContextMenu = ((RelativeLayout)localView.findViewById(2131165688));
    if (this.mTimer == null)
      this.mTimer = new Timer();
    this.mMoreIndexSelect = new int[5];
    this.mMoreIndexSelect[1] = 0;
    this.mMoreIndexSelect[2] = 0;
    this.mMoreIndexSelect[3] = 0;
    this.mMoreIndexSelect[4] = 0;
    this.mOffsetSelect = new int[5];
    this.mOffsetSelect[1] = 0;
    this.mOffsetSelect[2] = 0;
    this.mOffsetSelect[3] = 0;
    this.mOffsetSelect[4] = 0;
    this.mNetflixQueueDiscItemHashList = new ArrayList();
    this.mNetflixQueueInstantItemHashList = new ArrayList();
    this.mNetflixQueueSavedItemHashList = new ArrayList();
    this.mNetflixQueueAtHomeItemHashList = new ArrayList();
    this.mNetflixQueueSelectedItemHashList = this.mNetflixQueueDiscItemHashList;
    this.mDiscAdapter = new NetflixQueueFragmentAdapter(this, this.mNetflixQueueDiscItemHashList, 2130903132, new String[] { "movieTitle", "movieActors", "movieMeta", "movieRelease", "movieThumbnail" }, new int[] { 2131165448, 2131165451, 2131165452, 2131165453, 2131165486 });
    this.mInstantAdapter = new NetflixQueueFragmentAdapter(this, this.mNetflixQueueInstantItemHashList, 2130903132, new String[] { "movieTitle", "movieActors", "movieMeta", "movieRelease", "movieThumbnail" }, new int[] { 2131165448, 2131165451, 2131165452, 2131165453, 2131165486 });
    this.mSavedAdapter = new NetflixQueueFragmentAdapter(this, this.mNetflixQueueSavedItemHashList, 2130903132, new String[] { "movieTitle", "movieActors", "movieMeta", "movieRelease", "movieThumbnail" }, new int[] { 2131165448, 2131165451, 2131165452, 2131165453, 2131165486 });
    this.mAtHomeAdapter = new NetflixQueueFragmentAdapter(this, this.mNetflixQueueAtHomeItemHashList, 2130903132, new String[] { "movieTitle", "movieActors", "movieMeta", "movieRelease", "movieThumbnail" }, new int[] { 2131165448, 2131165451, 2131165452, 2131165453, 2131165486 });
    if (this.mDiscAdapter != null)
    {
      this.mNetflixList.setAdapter(this.mDiscAdapter);
      this.mAdapterSelected = this.mDiscAdapter;
    }
    this.mNavbarHolder = ((LinearLayout)localView.findViewById(2131165687));
    this.mNavbar = new SubNavBar(getActivity());
    this.mNavbar.load(this.mNavListener, 2131493089, 2131493078, 2131493077);
    this.mNavbar.setSelectedButton(2131165784);
    this.mNavbarHolder.addView(this.mNavbar, 0);
    FlixsterApplication.sNetflixListIsDirty[1] = true;
    FlixsterApplication.sNetflixListIsDirty[2] = true;
    FlixsterApplication.sNetflixListIsDirty[3] = true;
    FlixsterApplication.sNetflixListIsDirty[4] = true;
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    if (this.mTimer == null)
      this.mTimer = new Timer();
    Logger.d("FlxMain", "NetflixQueue.onResume()");
    Uri localUri = getActivity().getIntent().getData();
    if (localUri != null)
    {
      String str = localUri.getQueryParameter("oauth_token");
      Logger.d("FlxMain", "NetflixQueue.onResume() oauth_token:" + str + " uri:" + localUri);
      FlixsterApplication.setNetflixOAuthToken(str);
    }
    int i = this.mNavSelect;
    if (FlixsterApplication.sNetflixListIsDirty[i] != 0)
    {
      this.showLoadingDialog.sendEmptyMessage(0);
      this.mNetflixQueueSelectedItemHashList.clear();
      this.mMoreIndexSelect[i] = 0;
      this.mOffsetSelect[i] = 0;
      this.mNetflixList.invalidateViews();
      ScheduleLoadMoviesTask(this.mOffsetSelect[i]);
    }
    trackPage();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "NetflixQueuePage.onSaveInstanceState()");
  }

  public void trackPage()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 1:
      Trackers.instance().track("/netflix/queue/dvd", "Netflix DVDs");
      return;
    case 2:
      Trackers.instance().track("/netflix/queue/instant", "Netflix Instant");
      return;
    case 3:
      Trackers.instance().track("/netflix/queue/saved", "Netflix Saved");
      return;
    case 4:
    }
    Trackers.instance().track("/netflix/queue/athome", "Netflix AtHome");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.NetflixQueueFragment
 * JD-Core Version:    0.6.2
 */