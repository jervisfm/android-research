package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.NewsDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviNewsStory;
import net.flixster.android.lvi.LviPageHeader;
import net.flixster.android.model.NewsStory;

@Deprecated
@TargetApi(11)
public class NewsListFragment extends LviFragment
{
  private AdapterView.OnItemClickListener mNewsStoryClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      Lvi localLvi = (Lvi)NewsListFragment.this.mData.get(paramAnonymousInt);
      if (localLvi.getItemViewType() == Lvi.VIEW_TYPE_NEWSITEM)
      {
        Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(((LviNewsStory)localLvi).mNewsStory.mSource));
        NewsListFragment.this.startActivity(localIntent);
      }
    }
  };
  private Handler mUpdateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (((Main)NewsListFragment.this.getActivity() == null) || (NewsListFragment.this.isRemoving()))
        return;
      NewsListFragment.this.mData.clear();
      NewsListFragment.this.mData.addAll(NewsListFragment.this.mDataHolder);
      NewsListFragment.this.mLviListFragmentAdapter.notifyDataSetChanged();
    }
  };

  private void ScheduleLoadNewsItemsTask(int paramInt)
  {
    ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
    TimerTask local3 = new TimerTask()
    {
      public void run()
      {
        Main localMain = (Main)NewsListFragment.this.getActivity();
        if ((localMain == null) || (NewsListFragment.this.isRemoving()));
        while (true)
        {
          return;
          try
          {
            ArrayList localArrayList = NewsDao.getNewsItemList();
            LviPageHeader localLviPageHeader = new LviPageHeader();
            localLviPageHeader.mTitle = NewsListFragment.this.getResources().getString(2131493088);
            NewsListFragment.this.mData.add(localLviPageHeader);
            NewsListFragment.this.mDataHolder.clear();
            NewsListFragment.this.mDataHolder.add(localLviPageHeader);
            Iterator localIterator = localArrayList.iterator();
            while (true)
            {
              if (!localIterator.hasNext())
              {
                NewsListFragment.this.mUpdateHandler.sendEmptyMessage(0);
                if ((localMain == null) || (localMain.mLoadingDialog == null) || (!localMain.mLoadingDialog.isShowing()))
                  break;
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                return;
              }
              NewsStory localNewsStory = (NewsStory)localIterator.next();
              LviNewsStory localLviNewsStory = new LviNewsStory();
              localLviNewsStory.mNewsStory = localNewsStory;
              NewsListFragment.this.mDataHolder.add(localLviNewsStory);
            }
          }
          catch (DaoException localDaoException)
          {
            while (true)
            {
              localDaoException.printStackTrace();
              NewsListFragment localNewsListFragment = NewsListFragment.this;
              localNewsListFragment.mRetryCount = (1 + localNewsListFragment.mRetryCount);
              if (NewsListFragment.this.mRetryCount < 3)
              {
                NewsListFragment.this.ScheduleLoadNewsItemsTask(1000);
              }
              else
              {
                NewsListFragment.this.mRetryCount = 0;
                if (localMain != null)
                  localMain.mShowDialogHandler.sendEmptyMessage(2);
              }
            }
          }
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local3, 100L);
  }

  public static int getFlixFragId()
  {
    return 122;
  }

  public static NewsListFragment newInstance(Bundle paramBundle)
  {
    return new NewsListFragment();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 1:
    }
    ProgressDialog localProgressDialog = new ProgressDialog(getActivity());
    localProgressDialog.setMessage(getResources().getString(2131493173));
    localProgressDialog.setIndeterminate(true);
    localProgressDialog.setCancelable(true);
    localProgressDialog.setCanceledOnTouchOutside(true);
    return localProgressDialog;
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    this.mListView.setOnItemClickListener(this.mNewsStoryClickListener);
    trackPage();
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    ScheduleLoadNewsItemsTask(10);
  }

  public void trackPage()
  {
    Trackers.instance().track("/topnews", "top news");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.NewsListFragment
 * JD-Core Version:    0.6.2
 */