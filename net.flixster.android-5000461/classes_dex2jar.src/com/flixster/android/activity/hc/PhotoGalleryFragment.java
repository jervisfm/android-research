package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.PhotosListAdapter;
import net.flixster.android.ScrollGalleryPage;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.PhotoDao;
import net.flixster.android.model.Photo;

@TargetApi(11)
public class PhotoGalleryFragment extends FlixsterFragment
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int DIALOG_PHOTOS = 2;
  public static final String KEY_PHOTO_FILTER = "KEY_PHOTO_FILTER";
  private static final int MAX_PHOTOS = 100;
  private String filter;
  private View.OnClickListener headerClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      PhotoGalleryFragment.this.mNavSelect = paramAnonymousView.getId();
      PhotoGalleryFragment.this.onResume();
    }
  };
  private int mNavSelect;
  private SubNavBar mNavbar;
  private View.OnClickListener photoClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Photo localPhoto = (Photo)paramAnonymousView.getTag();
      if (localPhoto != null)
      {
        int i = PhotoGalleryFragment.this.topPhotos.indexOf(localPhoto);
        if (i < 0)
          i = 0;
        Trackers.instance().track("/photo/gallery/" + PhotoGalleryFragment.this.filter, "Top Photo Page for photo:" + localPhoto.thumbnailUrl);
        Intent localIntent = new Intent("TOP_PHOTO", null, PhotoGalleryFragment.this.getActivity(), ScrollGalleryPage.class);
        localIntent.putExtra("KEY_PHOTO_FILTER", PhotoGalleryFragment.this.filter);
        localIntent.putExtra("PHOTO_INDEX", i);
        PhotoGalleryFragment.this.startActivity(localIntent);
      }
    }
  };
  private AdapterView.OnItemClickListener photoItemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      paramAnonymousView.performClick();
    }
  };
  private ArrayList<Photo> photos;
  private ArrayList<Object> topPhotos;
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)PhotoGalleryFragment.this.getActivity();
      if ((localMain == null) || (PhotoGalleryFragment.this.isRemoving()));
      do
      {
        return;
        Logger.d("FlxMain", "PhotoGalleryPage.updateHandler");
        if (PhotoGalleryFragment.this.photos != null)
        {
          PhotoGalleryFragment.this.updatePage();
          return;
        }
      }
      while ((localMain == null) || (localMain.isFinishing()));
      localMain.showDialog(1);
    }
  };

  public static int getFlixFragId()
  {
    return 120;
  }

  public static PhotoGalleryFragment newInstance(Bundle paramBundle)
  {
    PhotoGalleryFragment localPhotoGalleryFragment = new PhotoGalleryFragment();
    localPhotoGalleryFragment.setArguments(paramBundle);
    return localPhotoGalleryFragment;
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local6 = new TimerTask()
    {
      public void run()
      {
        if (((Main)PhotoGalleryFragment.this.getActivity() == null) || (PhotoGalleryFragment.this.isRemoving()))
          return;
        if (PhotoGalleryFragment.this.photos == null);
        try
        {
          PhotoGalleryFragment.this.photos = PhotoDao.getTopPhotos(PhotoGalleryFragment.this.filter);
          PhotoGalleryFragment.this.updateHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.e("FlxMain", "PhotoGalleryPage.scheduleUpdatePageTask:failed to get photos", localDaoException);
            PhotoGalleryFragment.this.photos = null;
          }
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local6, 100L);
  }

  private void updateHeader()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 2131165784:
      this.filter = "top-actresses";
      return;
    case 2131165785:
      this.filter = "top-actors";
      return;
    case 2131165786:
    }
    this.filter = "top-movies";
  }

  private void updatePage()
  {
    GridView localGridView;
    if (this.photos != null)
    {
      localGridView = (GridView)getView().findViewById(2131165903);
      this.topPhotos = new ArrayList();
    }
    for (int i = 0; ; i++)
    {
      if ((i >= this.photos.size()) || (i >= 100))
      {
        localGridView.setAdapter(new PhotosListAdapter(getActivity(), this.topPhotos, this.photoClickListener));
        localGridView.setOnItemClickListener(this.photoItemClickListener);
        localGridView.setClickable(true);
        localGridView.setFocusable(true);
        localGridView.setFocusableInTouchMode(true);
        trackPage();
        return;
      }
      Object localObject = this.photos.get(i);
      this.topPhotos.add(localObject);
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(getActivity());
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(getActivity());
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ((Main)PhotoGalleryFragment.this.getActivity()).showDialog(2);
        PhotoGalleryFragment.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("FlxMain", "PhotoGalleryFragment.onCreateView");
    View localView = paramLayoutInflater.inflate(2130903179, null);
    Bundle localBundle = getArguments();
    if (localBundle != null)
    {
      this.filter = localBundle.getString("KEY_PHOTO_FILTER");
      if ((this.filter == null) || (this.filter.length() <= 0))
        this.filter = "random";
      this.mNavbar = new SubNavBar(getActivity());
      this.mNavbar.load(this.headerClickListener, 2131492911, 2131492912, 2131492913);
      this.mNavbar.setSelectedButton(2131165784);
      ((LinearLayout)localView.findViewById(2131165901)).addView(this.mNavbar, 1);
      updateHeader();
    }
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    this.mNavbar.setSelectedButton(this.mNavSelect);
    updateHeader();
    Logger.d("FlxMain", "PhotoGalleryPage.onCreate");
    this.photos = null;
    scheduleUpdatePageTask();
  }

  public void trackPage()
  {
    Trackers.instance().track("/photos/" + this.filter, "Top Photos Page for filter:" + this.filter);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.PhotoGalleryFragment
 * JD-Core Version:    0.6.2
 */