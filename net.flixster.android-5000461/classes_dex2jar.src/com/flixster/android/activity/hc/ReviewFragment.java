package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

@TargetApi(11)
public class ReviewFragment extends FlixsterFragment
  implements View.OnClickListener, View.OnTouchListener
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int DIALOG_REVIEW = 2;
  private TextView mIndexTextView;
  private LayoutInflater mLayoutInflater;
  public int mListType;
  protected TimerTask mLoadDetailsTask;
  Movie mMovie;
  long mMovieId;
  public int mMovieType;
  Button mNextButton;
  Button mPrevButton;
  private ArrayList<Review> mReviewsList;
  private int mStartReviewIndex;
  private ViewFlipper mViewFlipper;
  private Handler postMovieLoadHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)ReviewFragment.this.getActivity();
      if ((localMain == null) || (ReviewFragment.this.isRemoving()));
      do
      {
        return;
        Logger.d("FlxMain", "ReviewPage.postMovieLoadHandler - mReviewsList:" + ReviewFragment.this.mReviewsList);
        if ((ReviewFragment.this.mMovie != null) && (ReviewFragment.this.mReviewsList != null))
        {
          ReviewFragment.this.populatePage();
          return;
        }
      }
      while ((localMain == null) || (localMain.isFinishing()));
      localMain.showDialog(1);
    }
  };

  private void ScheduleLoadMoviesTask()
  {
    this.mLoadDetailsTask = new TimerTask()
    {
      public void run()
      {
        if (((Main)ReviewFragment.this.getActivity() == null) || (ReviewFragment.this.isRemoving()));
        while ((ReviewFragment.this.mReviewsList != null) && (!ReviewFragment.this.mReviewsList.isEmpty()))
          return;
        try
        {
          Movie localMovie = MovieDao.getMovieDetail(ReviewFragment.this.mMovieId);
          if (localMovie != null)
          {
            localMovie.merge(ReviewFragment.this.mMovie);
            ReviewFragment.this.mMovie = localMovie;
          }
          Logger.d("FlxMain", "ReviewFragment.ScheduleLoadMovieTask() findMovieDetails done.." + ReviewFragment.this.mMovie.getId());
          ReviewFragment.this.mReviewsList = new ArrayList();
          if (ReviewFragment.this.mMovie.getFriendWantToSeeList() != null)
            ReviewFragment.this.mReviewsList.addAll(ReviewFragment.this.mMovie.getFriendWantToSeeList());
          if (ReviewFragment.this.mMovie.getFriendRatedReviewsList() != null)
            ReviewFragment.this.mReviewsList.addAll(ReviewFragment.this.mMovie.getFriendRatedReviewsList());
          if (ReviewFragment.this.mMovie.getUserReviewsList() != null)
            ReviewFragment.this.mReviewsList.addAll(ReviewFragment.this.mMovie.getUserReviewsList());
          if (ReviewFragment.this.mMovie.getCriticReviewsList() != null)
            ReviewFragment.this.mReviewsList.addAll(ReviewFragment.this.mMovie.getCriticReviewsList());
          ReviewFragment.this.postMovieLoadHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          ReviewFragment localReviewFragment = ReviewFragment.this;
          localReviewFragment.mRetryCount = (1 + localReviewFragment.mRetryCount);
          if (ReviewFragment.this.mRetryCount > 4)
          {
            ReviewFragment.this.mRetryCount = 0;
            return;
          }
          ReviewFragment.this.ScheduleLoadMoviesTask();
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(this.mLoadDetailsTask, 100L);
  }

  public static int getFlixFragId()
  {
    return 106;
  }

  public static ReviewFragment newInstance(Bundle paramBundle)
  {
    ReviewFragment localReviewFragment = new ReviewFragment();
    localReviewFragment.setArguments(paramBundle);
    return localReviewFragment;
  }

  private void populatePage()
  {
    Iterator localIterator;
    if ((this.mReviewsList != null) && (!this.mReviewsList.isEmpty()))
    {
      this.mViewFlipper.removeAllViews();
      localIterator = this.mReviewsList.iterator();
      if (localIterator.hasNext())
        break label162;
      Logger.d("FlxMain", "ReviewPage.populatePage() mStartReviewIndex:" + this.mStartReviewIndex);
      this.mViewFlipper.setDisplayedChild(this.mStartReviewIndex);
      this.mIndexTextView.setText(1 + this.mViewFlipper.getDisplayedChild() + " of " + this.mReviewsList.size());
      this.mPrevButton.setVisibility(0);
      this.mNextButton.setVisibility(0);
      Activity localActivity = getActivity();
      if ((localActivity != null) && (!localActivity.isFinishing()))
        localActivity.removeDialog(2);
    }
    return;
    label162: Review localReview = (Review)localIterator.next();
    ScrollView localScrollView = (ScrollView)this.mLayoutInflater.inflate(2130903155, this.mViewFlipper, false);
    ((TextView)localScrollView.findViewById(2131165734)).setText(localReview.name);
    ImageView localImageView1 = (ImageView)localScrollView.findViewById(2131165733);
    Bitmap localBitmap = localReview.getReviewerBitmap(localImageView1);
    if (localBitmap != null)
      localImageView1.setImageBitmap(localBitmap);
    ImageView localImageView2 = (ImageView)localScrollView.findViewById(2131165736);
    ImageView localImageView3 = (ImageView)localScrollView.findViewById(2131165737);
    RelativeLayout localRelativeLayout1 = (RelativeLayout)localScrollView.findViewById(2131165732);
    RelativeLayout localRelativeLayout2 = (RelativeLayout)localScrollView.findViewById(2131165738);
    switch (localReview.type)
    {
    default:
      label312: if (localReview.userId > 0L)
      {
        localRelativeLayout1.setTag(localReview);
        localRelativeLayout1.setFocusable(true);
        localRelativeLayout1.setOnClickListener(this);
      }
      break;
    case 0:
    case 1:
    case 2:
    }
    while (true)
    {
      this.mViewFlipper.addView(localScrollView);
      break;
      localImageView3.setVisibility(8);
      ((TextView)localScrollView.findViewById(2131165739)).setText("     " + localReview.comment);
      ((TextView)localScrollView.findViewById(2131165735)).setText(", " + localReview.source);
      if (localReview.url != null)
      {
        localRelativeLayout2.setTag(localReview);
        localRelativeLayout2.setFocusable(true);
        localRelativeLayout2.setOnClickListener(this);
        localRelativeLayout2.setBackgroundResource(2130837698);
        localRelativeLayout2.setPadding(9, 9, 9, 9);
      }
      if (localReview.score >= 60)
        break label312;
      Drawable localDrawable = getResources().getDrawable(2130837740);
      localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
      localImageView2.setImageDrawable(localDrawable);
      break label312;
      ((TextView)localScrollView.findViewById(2131165739)).setText(localReview.comment);
      localImageView2.setVisibility(8);
      ((TextView)localScrollView.findViewById(2131165735)).setVisibility(8);
      localImageView3.setImageResource(net.flixster.android.Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localScrollView.findViewById(2131165740)).setVisibility(8);
      break label312;
      ((TextView)localScrollView.findViewById(2131165739)).setText(localReview.comment);
      localImageView2.setVisibility(8);
      ((TextView)localScrollView.findViewById(2131165735)).setVisibility(8);
      localImageView3.setImageResource(net.flixster.android.Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localScrollView.findViewById(2131165740)).setVisibility(8);
      break label312;
      localRelativeLayout1.setBackgroundResource(2131296267);
      localRelativeLayout1.setPadding(10, 10, 10, 10);
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165738:
    }
    Review localReview;
    do
    {
      return;
      localReview = (Review)paramView.getTag();
      Trackers.instance().track("/movie/criticreview", "Critic Review - " + this.mMovie.getProperty("title"));
    }
    while ((localReview == null) || (localReview.url == null) || (!localReview.url.startsWith("http://")));
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(localReview.url)));
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(getActivity());
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(getActivity());
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        ReviewFragment.this.ScheduleLoadMoviesTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    View localView = this.mLayoutInflater.inflate(2130903154, null);
    this.mIndexTextView = ((TextView)localView.findViewById(2131165723));
    Bundle localBundle = getArguments();
    if (localBundle != null)
    {
      Logger.d("FlxMain", "ReviewFragment.onCreateView getInt:" + localBundle.getInt("REVIEW_INDEX"));
      Logger.d("FlxMain", "ReviewFragment.onCreateView getLong:" + localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID"));
      this.mMovieId = localBundle.getLong("net.flixster.android.EXTRA_MOVIE_ID");
      this.mMovie = MovieDao.getMovie(this.mMovieId);
      this.mStartReviewIndex = localBundle.getInt("REVIEW_INDEX");
      Bitmap localBitmap = (Bitmap)localBundle.getParcelable("MOVIE_THUMBNAIL");
      if (localBitmap != null)
        this.mMovie.thumbnailSoftBitmap = new SoftReference(localBitmap);
    }
    this.mViewFlipper = ((ViewFlipper)localView.findViewById(2131165730));
    this.mPrevButton = ((Button)localView.findViewById(2131165724));
    this.mNextButton = ((Button)localView.findViewById(2131165722));
    this.mPrevButton.setOnTouchListener(this);
    this.mNextButton.setOnTouchListener(this);
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "MoviePage.onResume()");
    ScheduleLoadMoviesTask();
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 0);
    switch (paramView.getId())
    {
    case 2131165723:
    default:
      return false;
    case 2131165722:
      this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968582));
      this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968583));
      this.mViewFlipper.showNext();
      String str2 = "" + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviewsList.size();
      this.mIndexTextView.setText(str2);
      return true;
    case 2131165724:
    }
    this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968584));
    this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968585));
    this.mViewFlipper.showPrevious();
    String str1 = "" + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviewsList.size();
    this.mIndexTextView.setText(str1);
    return true;
  }

  public void trackPage()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.ReviewFragment
 * JD-Core Version:    0.6.2
 */