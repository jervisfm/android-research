package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviActor;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviSearchPanel;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Movie;

@TargetApi(11)
public class SearchFragment extends LviFragment
  implements TextView.OnEditorActionListener, View.OnClickListener
{
  private static final int ACTOR_SEARCH = 1;
  private static final int MOVIE_SEARCH;
  private static final ArrayList<Actor> sActorResults = new ArrayList();
  private static final ArrayList<Movie> sMovieResults = new ArrayList();
  private static int sSearchType;
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)SearchFragment.this.getActivity();
      if ((localMain == null) || (SearchFragment.this.isRemoving()));
      do
      {
        do
        {
          return;
          switch (SearchFragment.sSearchType)
          {
          default:
            return;
          case 0:
          case 1:
          }
        }
        while (SearchFragment.sMovieResults.isEmpty());
        Movie localMovie = (Movie)SearchFragment.sMovieResults.get(0);
        Intent localIntent2 = new Intent("DETAILS", null, localMain, MovieDetailsFragment.class);
        localIntent2.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
        localMain.startFragment(localIntent2, MovieDetailsFragment.class);
        return;
      }
      while (SearchFragment.sActorResults.isEmpty());
      Actor localActor = (Actor)SearchFragment.sActorResults.get(0);
      Intent localIntent1 = new Intent("TOP_ACTOR", null, localMain, ActorFragment.class);
      localIntent1.putExtra("ACTOR_ID", localActor.id);
      localIntent1.putExtra("ACTOR_NAME", localActor.name);
      localMain.startFragment(localIntent1, ActorFragment.class);
      SearchFragment.this.mLastListPosition = localActor.positionId;
    }
  };
  private SubNavBar mNavbar;
  private LinearLayout mNavbarHolder;
  private String mQueryString;
  private View.OnClickListener searchTypeListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165784:
        SearchFragment.sSearchType = 0;
        SearchFragment.this.ScheduleLoadItemsTask(10L);
        return;
      case 2131165785:
      }
      SearchFragment.sSearchType = 1;
      SearchFragment.this.ScheduleLoadItemsTask(10L);
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      TimerTask local3 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)SearchFragment.this.getActivity();
          if ((localMain == null) || (SearchFragment.this.isRemoving()));
          while (true)
          {
            return;
            try
            {
              switch (SearchFragment.sSearchType)
              {
              default:
              case 0:
                while (true)
                {
                  SearchFragment.this.mUpdateHandler.sendEmptyMessage(0);
                  if (!SearchFragment.this.isInitialContentLoaded)
                  {
                    SearchFragment.this.isInitialContentLoaded = true;
                    SearchFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
                  }
                  return;
                  if ((SearchFragment.sMovieResults.isEmpty()) && (SearchFragment.this.mQueryString != null))
                  {
                    localMain.mShowDialogHandler.sendEmptyMessage(1);
                    MovieDao.searchMovies(SearchFragment.this.mQueryString, SearchFragment.sMovieResults);
                  }
                  if (SearchFragment.sMovieResults.isEmpty())
                    break label350;
                  Tracker localTracker2 = Trackers.instance();
                  StringBuilder localStringBuilder2 = new StringBuilder("Search Movie - ");
                  if (SearchFragment.this.mQueryString == null)
                    break;
                  str2 = SearchFragment.this.mQueryString;
                  localTracker2.track("/search/movie/results", str2);
                  SearchFragment.this.setMovieLviList();
                }
              case 1:
              }
            }
            catch (DaoException localDaoException)
            {
              while (true)
              {
                Logger.w("FlxMain", "SearchPage.ScheduleLoadItemsTask.run() mRetryCount:" + SearchFragment.this.mRetryCount);
                localDaoException.printStackTrace();
                SearchFragment localSearchFragment = SearchFragment.this;
                localSearchFragment.mRetryCount = (1 + localSearchFragment.mRetryCount);
                if (SearchFragment.this.mRetryCount >= 3)
                  break;
                SearchFragment.this.ScheduleLoadItemsTask(1000L);
                label312: return;
                String str2 = "Results";
                continue;
                label350: Trackers.instance().track("/search", "Search");
              }
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null) && (localMain.mLoadingDialog.isShowing()))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
          }
          if ((SearchFragment.sActorResults.isEmpty()) && (SearchFragment.this.mQueryString != null))
          {
            localMain.mShowDialogHandler.sendEmptyMessage(1);
            ActorDao.searchActors(SearchFragment.this.mQueryString, SearchFragment.sActorResults);
          }
          Tracker localTracker1;
          StringBuilder localStringBuilder1;
          if (!SearchFragment.sActorResults.isEmpty())
          {
            localTracker1 = Trackers.instance();
            localStringBuilder1 = new StringBuilder("Search Actor - ");
            if (SearchFragment.this.mQueryString == null)
              break label551;
          }
          label551: for (String str1 = SearchFragment.this.mQueryString; ; str1 = "Results")
          {
            localTracker1.track("/search/actor/results", str1);
            while (true)
            {
              SearchFragment.this.setActorResultsLviList();
              break;
              Trackers.instance().track("/search", "Search");
            }
            SearchFragment.this.mRetryCount = 0;
            if (localMain == null)
              break label312;
            localMain.mShowDialogHandler.sendEmptyMessage(2);
            break label312;
          }
        }
      };
      Logger.d("FlxMain", "SearchPage.ScheduleLoadItemsTask() loading item");
      if (((Main)getActivity()).mPageTimer != null)
        ((Main)getActivity()).mPageTimer.schedule(local3, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void addAd()
  {
  }

  private void addNoResults()
  {
    LviMessagePanel localLviMessagePanel = new LviMessagePanel();
    localLviMessagePanel.mMessage = ((Main)getActivity()).getResourceString(2131493130);
    this.mDataHolder.add(localLviMessagePanel);
  }

  private void addSearchPanel()
  {
    LviSearchPanel localLviSearchPanel = new LviSearchPanel();
    localLviSearchPanel.mOnEditorActionListener = this;
    localLviSearchPanel.mOnClickListener = this;
    Main localMain = (Main)getActivity();
    if (sSearchType == 0);
    for (int i = 2131493126; ; i = 2131493127)
    {
      localLviSearchPanel.mHint = localMain.getResourceString(i);
      this.mDataHolder.add(localLviSearchPanel);
      return;
    }
  }

  public static int getFlixFragId()
  {
    return 7;
  }

  public static SearchFragment newInstance(Bundle paramBundle)
  {
    SearchFragment localSearchFragment = new SearchFragment();
    localSearchFragment.setArguments(paramBundle);
    return localSearchFragment;
  }

  private void setActorResultsLviList()
  {
    Logger.d("FlxMain", "SearchPage.setActorResultsLviList ");
    this.mDataHolder.clear();
    addAd();
    addSearchPanel();
    ArrayList localArrayList = ListHelper.clone(sActorResults);
    int i = (int)System.currentTimeMillis();
    Iterator localIterator = localArrayList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if ((localArrayList.isEmpty()) && (this.mQueryString != null))
          addNoResults();
        this.mQueryString = null;
        return;
      }
      Actor localActor = (Actor)localIterator.next();
      int j = i + 1;
      localActor.positionId = i;
      LviActor localLviActor = new LviActor();
      localLviActor.mActor = localActor;
      localLviActor.mActorClick = getActorClickListener();
      this.mDataHolder.add(localLviActor);
      i = j;
    }
  }

  private void setMovieLviList()
  {
    Logger.d("FlxMain", "SearchPage.setSearchLviList ");
    this.mDataHolder.clear();
    addAd();
    addSearchPanel();
    ArrayList localArrayList = ListHelper.clone(sMovieResults);
    Iterator localIterator;
    if (localArrayList.size() > 0)
    {
      localIterator = localArrayList.iterator();
      if (!localIterator.hasNext())
        this.mDataHolder.add(new LviFooter());
    }
    while (true)
    {
      this.mQueryString = null;
      return;
      Movie localMovie = (Movie)localIterator.next();
      LviMovie localLviMovie = new LviMovie();
      localLviMovie.mMovie = localMovie;
      localLviMovie.mTrailerClick = getTrailerOnClickListener();
      this.mDataHolder.add(localLviMovie);
      break;
      if (this.mQueryString != null)
        addNoResults();
    }
  }

  private void shadeNavButtons()
  {
    if (sSearchType == 0)
      this.mNavbar.setSelectedButton(2131165784);
    while (sSearchType != 1)
      return;
    this.mNavbar.setSelectedButton(2131165785);
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165441:
    }
    Logger.d("FlxMain", "SearchPage.onClick ok then");
    EditText localEditText = (EditText)paramView.getTag();
    localEditText.dispatchKeyEvent(new KeyEvent(0, 66));
    ((InputMethodManager)getActivity().getSystemService("input_method")).hideSoftInputFromWindow(localEditText.getWindowToken(), 0);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mNavbarHolder = ((LinearLayout)localView.findViewById(2131165427));
    this.mNavbar = new SubNavBar(getActivity());
    this.mNavbar.load(this.searchTypeListener, 2131493124, 2131493125);
    this.mNavbar.setSelectedButton(2131165784);
    this.mNavbarHolder.addView(this.mNavbar, 0);
    return localView;
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent != null) && (paramKeyEvent.getAction() == 0))
    {
      Logger.d("FlxMain", "SearchPage.onEditorAction ok (from action)");
      this.mQueryString = ((EditText)paramTextView).getText().toString();
      if (("FLX".equalsIgnoreCase(this.mQueryString)) || ("REINDEER FLOTILLA".equalsIgnoreCase(this.mQueryString)) || ("UUDDLRLRBA".equalsIgnoreCase(this.mQueryString)))
      {
        FlixsterApplication.setAdminState(1);
        Toast.makeText(getActivity(), 2131493307, 0).show();
      }
      if ("DGNS".equalsIgnoreCase(this.mQueryString))
      {
        FlixsterApplication.enableDiagnosticMode();
        Toast.makeText(getActivity(), 2131493308, 0).show();
      }
      switch (sSearchType)
      {
      default:
      case 0:
      case 1:
      }
    }
    while (true)
    {
      this.isInitialContentLoaded = false;
      ScheduleLoadItemsTask(10L);
      return false;
      sMovieResults.clear();
      continue;
      sActorResults.clear();
    }
  }

  public void onResume()
  {
    super.onResume();
    shadeNavButtons();
    ScheduleLoadItemsTask(100L);
  }

  public void trackPage()
  {
    Trackers.instance().track("/search", "Search");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.SearchFragment
 * JD-Core Version:    0.6.2
 */