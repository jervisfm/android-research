package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.GoogleApiDetector;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.Divider;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.MovieMapPage;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviDatePanel;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviIconPanel;
import net.flixster.android.lvi.LviLocationPanel;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviTheater;
import net.flixster.android.lvi.LviWrapper;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

@TargetApi(11)
public class ShowtimesFragment extends LviFragment
{
  private Movie mMovie;
  private List<Theater> mTheaterList = new ArrayList();
  private View.OnClickListener mTheaterMapOnClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent(ShowtimesFragment.this.getActivity(), MovieMapPage.class);
      localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", ShowtimesFragment.this.mMovie.getId());
      localIntent.putExtra("MOVIE_THUMBNAIL", (Parcelable)ShowtimesFragment.this.mMovie.thumbnailSoftBitmap.get());
      localIntent.putExtra("title", ShowtimesFragment.this.mMovie.getProperty("title"));
      localIntent.putExtra("runningTime", ShowtimesFragment.this.mMovie.getProperty("runningTime"));
      localIntent.putExtra("MOVIE_ACTORS_SHORT", ShowtimesFragment.this.mMovie.getProperty("MOVIE_ACTORS_SHORT"));
      localIntent.putExtra("mpaa", ShowtimesFragment.this.mMovie.getProperty("mpaa"));
      localIntent.putExtra("popcornScore", ShowtimesFragment.this.mMovie.getIntProperty("popcornScore"));
      localIntent.putExtra("rottenTomatoes", ShowtimesFragment.this.mMovie.getIntProperty("rottenTomatoes"));
      ShowtimesFragment.this.startActivity(localIntent);
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          if (((Main)ShowtimesFragment.this.getActivity() == null) || (ShowtimesFragment.this.isRemoving()))
            return;
          Logger.d("FlxMain", "ShowtimesPage.ScheduleLoadItemsTask.run");
          while (true)
          {
            HashMap localHashMap;
            try
            {
              localHashMap = FlixsterApplication.getFavoriteTheatersList();
              if (FlixsterApplication.getUseLocationServiceFlag())
              {
                ShowtimesFragment.this.mTheaterList = TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getCurrentLatitude(), FlixsterApplication.getCurrentLongitude(), FlixsterApplication.getShowtimesDate(), ShowtimesFragment.this.mMovie.getId(), localHashMap);
                ShowtimesFragment.this.setShowtimesLviList();
                ShowtimesFragment.this.mUpdateHandler.sendEmptyMessage(0);
                return;
              }
            }
            catch (DaoException localDaoException)
            {
              Logger.e("FlxMain", "ShowtimesPage.ScheduleLoadItemsTask.run DaoException", localDaoException);
              ShowtimesFragment.this.retryLogic(localDaoException);
              return;
            }
            ShowtimesFragment.this.mTheaterList = TheaterDao.findTheatersByMovieLocation(FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), FlixsterApplication.getShowtimesDate(), ShowtimesFragment.this.mMovie.getId(), localHashMap);
          }
        }
      };
      Main localMain = (Main)getActivity();
      if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
        localMain.mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
    }
  }

  public static int getFlixFragId()
  {
    return 101;
  }

  public static ShowtimesFragment newInstance(Bundle paramBundle)
  {
    ShowtimesFragment localShowtimesFragment = new ShowtimesFragment();
    localShowtimesFragment.setArguments(paramBundle);
    return localShowtimesFragment;
  }

  private void setShowtimesLviList()
  {
    Logger.d("FlxMain", "ShowtimesPage.setShowtimesLviList ");
    this.mDataHolder.clear();
    LviMovie localLviMovie = new LviMovie();
    localLviMovie.mMovie = this.mMovie;
    localLviMovie.mForm = 1;
    this.mDataHolder.add(localLviMovie);
    LviLocationPanel localLviLocationPanel = new LviLocationPanel();
    this.mDataHolder.add(localLviLocationPanel);
    LviDatePanel localLviDatePanel = new LviDatePanel();
    localLviDatePanel.mOnClickListener = ((Main)getActivity()).getDateSelectOnClickListener();
    this.mDataHolder.add(localLviDatePanel);
    if (!GoogleApiDetector.instance().isVanillaAndroid())
    {
      LviIconPanel localLviIconPanel = new LviIconPanel();
      localLviIconPanel.mLabel = getResources().getString(2131492971);
      localLviIconPanel.mOnClickListener = this.mTheaterMapOnClickListener;
      this.mDataHolder.add(localLviIconPanel);
    }
    LviSubHeader localLviSubHeader = new LviSubHeader();
    localLviSubHeader.mTitle = getResources().getString(2131492934);
    this.mDataHolder.add(localLviSubHeader);
    if (this.mTheaterList.size() > 0)
    {
      Iterator localIterator = this.mTheaterList.iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          LviFooter localLviFooter = new LviFooter();
          this.mDataHolder.add(localLviFooter);
          return;
        }
        Theater localTheater = (Theater)localIterator.next();
        this.mDataHolder.add(LviWrapper.convertToLvi(new Divider(getActivity()), Lvi.VIEW_TYPE_DIVIDER));
        LviTheater localLviTheater = new LviTheater();
        localLviTheater.mTheater = localTheater;
        localLviTheater.mStarTheaterClick = getStarTheaterClickListener();
        localLviTheater.mTheaterClick = getTheaterClickListener();
        this.mDataHolder.add(localLviTheater);
        ArrayList localArrayList = (ArrayList)localTheater.getShowtimesListByMovieHash().get(Long.valueOf(this.mMovie.getId()));
        if ((localArrayList == null) || (localArrayList.isEmpty()))
        {
          LviShowtimes localLviShowtimes1 = new LviShowtimes();
          localLviShowtimes1.mTheater = localTheater;
          localLviShowtimes1.mShowtimes = null;
          this.mDataHolder.add(localLviShowtimes1);
        }
        else
        {
          int i = localArrayList.size();
          for (int j = 0; j < i; j++)
          {
            Showtimes localShowtimes = (Showtimes)localArrayList.get(j);
            LviShowtimes localLviShowtimes2 = new LviShowtimes();
            localLviShowtimes2.mTheater = localTheater;
            localLviShowtimes2.mShowtimesListSize = i;
            localLviShowtimes2.mShowtimePosition = j;
            localLviShowtimes2.mShowtimes = localShowtimes;
            localLviShowtimes2.mBuyClick = getShowtimesClickListener();
            this.mDataHolder.add(localLviShowtimes2);
          }
        }
      }
    }
    LviMessagePanel localLviMessagePanel = new LviMessagePanel();
    localLviMessagePanel.mMessage = getResources().getString(2131492952);
    this.mDataHolder.add(localLviMessagePanel);
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mMovie = MovieDao.getMovie(getArguments().getLong("net.flixster.android.EXTRA_MOVIE_ID"));
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }

  public void onResume()
  {
    super.onResume();
    trackPage();
    ScheduleLoadItemsTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(1000L);
  }

  public void trackPage()
  {
    Trackers.instance().track("/showtimes", "Showtimes - " + this.mMovie.getTitle());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.ShowtimesFragment
 * JD-Core Version:    0.6.2
 */