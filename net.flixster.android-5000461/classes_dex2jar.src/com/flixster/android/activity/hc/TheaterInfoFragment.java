package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.LocationFacade;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.Divider;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviDatePanel;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviIconPanel;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviPageHeaderTheaterInfo;
import net.flixster.android.lvi.LviShowtimes;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviWrapper;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

@TargetApi(11)
public class TheaterInfoFragment extends LviFragment
{
  public View.OnClickListener mCallTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("android.intent.action.DIAL");
      String str1;
      String str2;
      if (TheaterInfoFragment.this.mTheater.checkProperty("phone"))
      {
        str1 = TheaterInfoFragment.this.mTheater.getProperty("phone");
        if (!str1.contains("FANDANGO"))
          break label97;
        str2 = str1.replaceAll("FANDANGO", "3263264#");
      }
      while (true)
      {
        localIntent.setData(Uri.parse("tel:" + str2));
        try
        {
          ((Main)TheaterInfoFragment.this.getActivity()).startActivity(localIntent);
          return;
          label97: str2 = PhoneNumberUtils.convertKeypadLettersToDigits(str1);
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
        }
      }
    }
  };
  public View.OnClickListener mMapTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("android.intent.action.VIEW");
      String str = TheaterInfoFragment.this.mTheater.getProperty("street") + "+" + TheaterInfoFragment.this.mTheater.getProperty("city") + "+" + TheaterInfoFragment.this.mTheater.getProperty("state") + "+" + TheaterInfoFragment.this.mTheater.getProperty("zip");
      str.replaceAll(" ", "+");
      localIntent.setData(Uri.parse("geo:0,0?q=" + str));
      Trackers.instance().track("/theaters/info/map", "query:" + str);
      try
      {
        ((Main)TheaterInfoFragment.this.getActivity()).startActivity(localIntent);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
      }
    }
  };
  private ArrayList<Movie> mMovieProtectionReference;
  private HashMap<Long, ArrayList<Showtimes>> mShowtimesByMovieHash;
  private Theater mTheater;
  private String mTheaterAddressString;
  private long mTheaterId;
  public View.OnClickListener mYelpTheaterListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      try
      {
        String str2 = "http://lite.yelp.com/search?inboundsrc=flixster&cflt=restaurants&rflt=all&sortby=composite&find_loc=" + URLEncoder.encode(TheaterInfoFragment.getAddressForYelp(TheaterInfoFragment.this.mTheater), "UTF-8");
        str1 = str2;
        Trackers.instance().track("/theaters/info/yelp", "Yelp city:" + TheaterInfoFragment.this.mTheater.getProperty("city"));
        localIntent = new Intent("android.intent.action.VIEW", Uri.parse(str1));
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        try
        {
          Intent localIntent;
          ((Main)TheaterInfoFragment.this.getActivity()).startActivity(localIntent);
          return;
          localUnsupportedEncodingException = localUnsupportedEncodingException;
          localUnsupportedEncodingException.printStackTrace();
          String str1 = null;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          Logger.w("FlxMain", "Intent not found", localActivityNotFoundException);
        }
      }
    }
  };

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      TimerTask local4 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)TheaterInfoFragment.this.getActivity();
          if ((localMain == null) || (TheaterInfoFragment.this.isRemoving()));
          while (true)
          {
            return;
            Logger.d("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask.run mTheaterId:" + TheaterInfoFragment.this.mTheaterId);
            try
            {
              if ((TheaterInfoFragment.this.mTheater == null) || (TheaterInfoFragment.this.mTheater.getId() <= 0L))
              {
                TheaterInfoFragment.this.mTheaterId = TheaterInfoFragment.this.getArguments().getLong("net.flixster.android.EXTRA_THEATER_ID");
                TheaterInfoFragment.this.mTheater = TheaterDao.getTheater(TheaterInfoFragment.this.mTheaterId);
                TheaterInfoFragment.this.makeTheaterAddressString();
              }
              TheaterInfoFragment.this.mShowtimesByMovieHash = TheaterDao.findShowtimesByTheater(FlixsterApplication.getShowtimesDate(), TheaterInfoFragment.this.mTheaterId);
              Logger.d("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask.run() mShowtimesByMovieHash.size:" + TheaterInfoFragment.this.mShowtimesByMovieHash.size());
              TheaterInfoFragment.this.mMovieProtectionReference = new ArrayList();
              Iterator localIterator = TheaterInfoFragment.this.mShowtimesByMovieHash.keySet().iterator();
              while (true)
              {
                if (!localIterator.hasNext())
                {
                  TheaterInfoFragment.this.setTheaterInfoLviList();
                  TheaterInfoFragment.this.mUpdateHandler.sendEmptyMessage(0);
                  return;
                }
                Long localLong = (Long)localIterator.next();
                TheaterInfoFragment.this.mMovieProtectionReference.add(MovieDao.getMovie(localLong.longValue()));
              }
            }
            catch (DaoException localDaoException)
            {
              Logger.e("FlxMain", "TheaterInfoPage.ScheduleLoadItemsTask DaoException", localDaoException);
              TheaterInfoFragment.this.retryLogic(localDaoException);
              return;
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null) && (localMain.mLoadingDialog.isShowing()))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
          }
        }
      };
      Main localMain = (Main)getActivity();
      if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
        localMain.mPageTimer.schedule(local4, paramLong);
      return;
    }
    finally
    {
    }
  }

  private static String getAddressForYelp(Theater paramTheater)
  {
    if (LocationFacade.isFrenchLocale())
      return paramTheater.getProperty("street") + ", " + paramTheater.getProperty("city") + ", France";
    return paramTheater.getProperty("address");
  }

  public static int getFlixFragId()
  {
    return 105;
  }

  private void makeTheaterAddressString()
  {
    this.mTheaterAddressString = (this.mTheater.getProperty("street") + "\n" + this.mTheater.getProperty("city") + ", " + this.mTheater.getProperty("state") + " " + this.mTheater.getProperty("zip"));
  }

  public static TheaterInfoFragment newInstance(Bundle paramBundle)
  {
    TheaterInfoFragment localTheaterInfoFragment = new TheaterInfoFragment();
    localTheaterInfoFragment.setArguments(paramBundle);
    return localTheaterInfoFragment;
  }

  private void setTheaterInfoLviList()
  {
    Main localMain = (Main)getActivity();
    if ((localMain == null) || (isRemoving()))
      return;
    Logger.d("FlxMain", "TheaterInfoPage.setPopularLviList ");
    this.mDataHolder.clear();
    LviPageHeaderTheaterInfo localLviPageHeaderTheaterInfo = new LviPageHeaderTheaterInfo();
    localLviPageHeaderTheaterInfo.mTheater = this.mTheater;
    localLviPageHeaderTheaterInfo.mTitle = this.mTheater.getProperty("name");
    localLviPageHeaderTheaterInfo.mStarTheaterClick = getStarTheaterClickListener();
    this.mDataHolder.add(localLviPageHeaderTheaterInfo);
    LviIconPanel localLviIconPanel1 = new LviIconPanel();
    localLviIconPanel1.mLabel = this.mTheater.getProperty("phone");
    localLviIconPanel1.mDrawableResource = 2130837770;
    localLviIconPanel1.mOnClickListener = this.mCallTheaterListener;
    this.mDataHolder.add(localLviIconPanel1);
    LviIconPanel localLviIconPanel2 = new LviIconPanel();
    localLviIconPanel2.mLabel = this.mTheaterAddressString;
    localLviIconPanel2.mDrawableResource = 2130837767;
    localLviIconPanel2.mOnClickListener = this.mMapTheaterListener;
    this.mDataHolder.add(localLviIconPanel2);
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()) || (LocationFacade.isUkLocale()) || (LocationFacade.isFrenchLocale()))
    {
      LviIconPanel localLviIconPanel3 = new LviIconPanel();
      localLviIconPanel3.mLabel = getResources().getString(2131493084);
      localLviIconPanel3.mDrawableResource = 2130837780;
      localLviIconPanel3.mOnClickListener = this.mYelpTheaterListener;
      this.mDataHolder.add(localLviIconPanel3);
    }
    LviDatePanel localLviDatePanel = new LviDatePanel();
    localLviDatePanel.mOnClickListener = ((Main)getActivity()).getDateSelectOnClickListener();
    this.mDataHolder.add(localLviDatePanel);
    LviSubHeader localLviSubHeader = new LviSubHeader();
    localLviSubHeader.mTitle = getResources().getString(2131492934);
    this.mDataHolder.add(localLviSubHeader);
    if (this.mShowtimesByMovieHash != null)
    {
      Logger.v("FlxMain", "TheaterInfoList.LoadMoviesTask mShowtimesHash, size:" + this.mShowtimesByMovieHash.size());
      if (this.mShowtimesByMovieHash.size() == 0)
      {
        LviMessagePanel localLviMessagePanel2 = new LviMessagePanel();
        localLviMessagePanel2.mMessage = getString(2131492952);
        this.mDataHolder.add(localLviMessagePanel2);
        return;
      }
      HashMap localHashMap = this.mShowtimesByMovieHash;
      Iterator localIterator;
      if (localHashMap != null)
        localIterator = localHashMap.keySet().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          LviFooter localLviFooter = new LviFooter();
          this.mDataHolder.add(localLviFooter);
          return;
        }
        Long localLong = (Long)localIterator.next();
        ArrayList localArrayList1 = this.mDataHolder;
        Divider localDivider = new Divider(localMain);
        localArrayList1.add(LviWrapper.convertToLvi(localDivider, Lvi.VIEW_TYPE_DIVIDER));
        LviMovie localLviMovie = new LviMovie();
        localLviMovie.mMovie = MovieDao.getMovie(localLong.longValue());
        localLviMovie.mTrailerClick = getTrailerOnClickListener();
        this.mDataHolder.add(localLviMovie);
        ArrayList localArrayList2 = (ArrayList)localHashMap.get(localLong);
        int i = localArrayList2.size();
        for (int j = 0; j < i; j++)
        {
          Showtimes localShowtimes = (Showtimes)localArrayList2.get(j);
          LviShowtimes localLviShowtimes = new LviShowtimes();
          localLviShowtimes.mTheater = this.mTheater;
          localLviShowtimes.mShowtimesListSize = i;
          localLviShowtimes.mShowtimePosition = j;
          localLviShowtimes.mShowtimes = localShowtimes;
          localLviShowtimes.mBuyClick = getShowtimesClickListener();
          this.mDataHolder.add(localLviShowtimes);
        }
      }
    }
    LviMessagePanel localLviMessagePanel1 = new LviMessagePanel();
    localLviMessagePanel1.mMessage = "Network Error Occured";
    this.mDataHolder.add(localLviMessagePanel1);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    this.mTheaterId = getArguments().getLong("net.flixster.android.EXTRA_THEATER_ID");
    this.mTheater = TheaterDao.getTheater(this.mTheaterId);
    makeTheaterAddressString();
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    trackPage();
    ScheduleLoadItemsTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(1000L);
  }

  public void trackPage()
  {
    Theater localTheater = this.mTheater;
    String str = null;
    if (localTheater != null)
      str = this.mTheater.getProperty("name");
    Trackers.instance().track("/theater/info", "Theater Info - " + str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.TheaterInfoFragment
 * JD-Core Version:    0.6.2
 */