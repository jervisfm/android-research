package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.SettingsPage;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.lvi.Lvi;
import net.flixster.android.lvi.LviMessagePanel;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.lvi.LviTheater;
import net.flixster.android.model.Theater;
import net.flixster.android.model.TheaterDistanceComparator;
import net.flixster.android.model.TheaterNameComparator;

@TargetApi(11)
public class TheaterListFragment extends LviFragment
{
  public static final String KEY_THEATERS_FILTER = "KEY_THEATERS_FILTER";
  public static TheaterDistanceComparator mTheaterDistanceComparator;
  public static TheaterNameComparator mTheaterNameComparator;
  private final double[] DISTANCES_IMPERIAL = { 1.0D, 3.0D, 5.0D, 10.0D, 15.0D, 20.0D };
  private final double[] DISTANCES_METRIC = { 1.24D, 3.11D, 6.22D, 9.32D, 15.539999999999999D, 21.75D };
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)TheaterListFragment.this.getActivity();
      if ((localMain == null) || (TheaterListFragment.this.isRemoving()));
      while (true)
      {
        return;
        long l = 0L;
        if (!TheaterListFragment.this.mFavorites.isEmpty())
          l = ((LviTheater)TheaterListFragment.this.mFavorites.get(1)).mTheater.getId();
        while (l != 0L)
        {
          Intent localIntent = new Intent("DETAILS", null, localMain, TheaterInfoFragment.class);
          localIntent.putExtra("net.flixster.android.EXTRA_THEATER_ID", l);
          localMain.startFragment(localIntent, TheaterInfoFragment.class);
          return;
          if (!TheaterListFragment.this.mTheaters.isEmpty())
            l = ((Theater)TheaterListFragment.this.mTheaters.get(0)).getId();
        }
      }
    }
  };
  private String mDistanceString = "";
  private final ArrayList<Lvi> mFavorites = new ArrayList();
  private Handler mLocationStringHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (TheaterListFragment.this.isRemoving())
        return;
      TheaterListFragment.this.mLocationStringTextView.setText(TheaterListFragment.this.mDistanceString);
    }
  };
  private TextView mLocationStringTextView;
  private SubNavBar mNavBar;
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
        return;
      case 2131165784:
      case 2131165785:
      }
      TheaterListFragment.this.mNavSelect = paramAnonymousView.getId();
      TheaterListFragment.this.mListView.setOnItemClickListener(TheaterListFragment.this.getMovieItemClickListener());
      TheaterListFragment.this.ScheduleLoadItemsTask(TheaterListFragment.this.mNavSelect, 100L);
    }
  };
  private int mNavSelect;
  private LinearLayout mNavbarHolder;
  private View.OnClickListener mSettingsClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Logger.d("FlxMain", "TheaterListPage.mSettingsClickListener");
      Intent localIntent = new Intent(TheaterListFragment.this.getActivity(), SettingsPage.class);
      TheaterListFragment.this.startActivity(localIntent);
    }
  };
  private final ArrayList<Theater> mTheaters = new ArrayList();

  private void ScheduleLoadItemsTask(final int paramInt, long paramLong)
  {
    try
    {
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local5 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)TheaterListFragment.this.getActivity();
          if ((localMain == null) || (TheaterListFragment.this.isRemoving()))
            return;
          Logger.d("FlxMain", "TheaterListPage.ScheduleLoadItemsTask.run navSelection:" + paramInt);
          while (true)
          {
            try
            {
              if (TheaterListFragment.this.mTheaters.isEmpty())
              {
                localHashMap = FlixsterApplication.getFavoriteTheatersList();
                if (!FlixsterApplication.getUseLocationServiceFlag())
                  continue;
                double d1 = FlixsterApplication.getCurrentLatitude();
                double d2 = FlixsterApplication.getCurrentLongitude();
                TheaterDao.findTheatersLocation(TheaterListFragment.this.mTheaters, d1, d2, localHashMap);
              }
              switch (paramInt)
              {
              default:
                TheaterListFragment.this.mUpdateHandler.sendEmptyMessage(0);
                if (localMain != null)
                  localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                if (!TheaterListFragment.this.isInitialContentLoaded)
                {
                  TheaterListFragment.this.isInitialContentLoaded = true;
                  TheaterListFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
                }
                return;
                if ((FlixsterApplication.getUserLatitude() != 0.0D) && (FlixsterApplication.getUserLongitude() != 0.0D))
                {
                  TheaterDao.findTheatersLocation(TheaterListFragment.this.mTheaters, FlixsterApplication.getUserLatitude(), FlixsterApplication.getUserLongitude(), localHashMap);
                  continue;
                }
                break;
              case 2131165784:
              case 2131165785:
              case 2131165786:
              }
            }
            catch (DaoException localDaoException)
            {
              HashMap localHashMap;
              Logger.e("FlxMain", "TheaterListPage.ScheduleLoadItemsTask DaoException", localDaoException);
              TheaterListFragment.this.retryLogic(localDaoException);
              return;
              TheaterDao.findTheaters(TheaterListFragment.this.mTheaters, FlixsterApplication.getUserLocation(), localHashMap);
              continue;
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null) && (localMain.mLoadingDialog.isShowing()))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
            TheaterListFragment.this.setByDistanceLviList();
            continue;
            TheaterListFragment.this.setByNameLviList();
            continue;
            TheaterListFragment.this.updateMap();
          }
        }
      };
      Main localMain = (Main)getActivity();
      if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
        localMain.mPageTimer.schedule(local5, paramLong);
      return;
    }
    finally
    {
    }
  }

  public static int getFlixFragId()
  {
    return 2;
  }

  private void setByDistanceLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
    this.mDataHolder.clear();
    this.mFavorites.clear();
    ArrayList localArrayList = ListHelper.clone(this.mTheaters);
    Collections.sort(localArrayList, mTheaterDistanceComparator);
    double d1 = 1.0D;
    double[] arrayOfDouble;
    if (FlixsterApplication.getDistanceType() == 0)
      arrayOfDouble = this.DISTANCES_IMPERIAL;
    double d2;
    int i;
    Iterator localIterator;
    while (true)
    {
      d2 = 0.0D;
      i = 0;
      localIterator = localArrayList.iterator();
      if (localIterator.hasNext())
        break label282;
      if (this.mFavorites.size() > 0)
      {
        Logger.d("FlxMain", "TheaterListPage mFavorites.size:" + this.mFavorites.size() + " mDataHolder.size():" + this.mDataHolder.size());
        LviSubHeader localLviSubHeader2 = new LviSubHeader();
        localLviSubHeader2.mTitle = getResources().getString(2131492894);
        this.mFavorites.add(0, localLviSubHeader2);
        this.mDataHolder.addAll(0, this.mFavorites);
        Logger.d("FlxMain", "TheaterListPage postadd mDataHolder.size():" + this.mDataHolder.size());
      }
      if ((localArrayList.size() != 0) || (this.mFavorites.size() != 0))
        break;
      LviMessagePanel localLviMessagePanel = new LviMessagePanel();
      localLviMessagePanel.mMessage = getResources().getString(2131492953);
      this.mDataHolder.add(localLviMessagePanel);
      return;
      arrayOfDouble = this.DISTANCES_METRIC;
      d1 = 1.608999967575073D;
    }
    label282: Theater localTheater = (Theater)localIterator.next();
    double d3 = localTheater.getMiles();
    label309: label320: LviSubHeader localLviSubHeader1;
    String str2;
    Object[] arrayOfObject2;
    if (d3 > d2)
    {
      if (arrayOfDouble[i] < d3)
        break label487;
      localLviSubHeader1 = new LviSubHeader();
      if (i != arrayOfDouble.length)
        break label501;
      d2 = 1000000.0D;
      str2 = getResources().getString(2131493092);
      arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Double.valueOf(d1 * arrayOfDouble[(-1 + arrayOfDouble.length)]);
    }
    label487: label501: String str1;
    Object[] arrayOfObject1;
    for (localLviSubHeader1.mTitle = String.format(str2, arrayOfObject2); ; localLviSubHeader1.mTitle = String.format(str1, arrayOfObject1))
    {
      this.mDataHolder.add(localLviSubHeader1);
      LviTheater localLviTheater1 = new LviTheater();
      localLviTheater1.mTheater = localTheater;
      localLviTheater1.mTheaterClick = getTheaterClickListener();
      this.mDataHolder.add(localLviTheater1);
      if (!localHashMap.containsKey(Long.toString(localTheater.getId())))
        break;
      LviTheater localLviTheater2 = new LviTheater();
      localLviTheater2.mTheater = localTheater;
      localLviTheater2.mTheaterClick = getTheaterClickListener();
      this.mFavorites.add(localLviTheater2);
      break;
      i++;
      if (i != arrayOfDouble.length)
        break label309;
      break label320;
      d2 = arrayOfDouble[i];
      str1 = getResources().getString(2131493091);
      arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Double.valueOf(d1 * arrayOfDouble[i]);
    }
  }

  private void setByNameLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    HashMap localHashMap = FlixsterApplication.getFavoriteTheatersList();
    this.mDataHolder.clear();
    this.mFavorites.clear();
    ArrayList localArrayList = ListHelper.clone(this.mTheaters);
    Collections.sort(localArrayList, mTheaterNameComparator);
    char c1 = '\000';
    Iterator localIterator = localArrayList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        if (this.mFavorites.size() > 0)
        {
          LviSubHeader localLviSubHeader2 = new LviSubHeader();
          localLviSubHeader2.mTitle = getResources().getString(2131492894);
          this.mFavorites.add(0, localLviSubHeader2);
          this.mDataHolder.addAll(0, this.mFavorites);
        }
        if ((localArrayList.size() != 0) || (this.mFavorites.size() != 0))
          break;
        LviMessagePanel localLviMessagePanel = new LviMessagePanel();
        localLviMessagePanel.mMessage = getResources().getString(2131492953);
        this.mDataHolder.add(localLviMessagePanel);
        return;
      }
      Theater localTheater = (Theater)localIterator.next();
      char c2 = localTheater.getProperty("name").charAt(0);
      if (!Character.isLetter(c2))
        c2 = '#';
      if (c1 != c2)
      {
        LviSubHeader localLviSubHeader1 = new LviSubHeader();
        localLviSubHeader1.mTitle = String.valueOf(c2);
        this.mDataHolder.add(localLviSubHeader1);
        c1 = c2;
      }
      LviTheater localLviTheater1 = new LviTheater();
      localLviTheater1.mTheater = localTheater;
      localLviTheater1.mTheaterClick = getTheaterClickListener();
      this.mDataHolder.add(localLviTheater1);
      if (localHashMap.containsKey(Long.toString(localTheater.getId())))
      {
        LviTheater localLviTheater2 = new LviTheater();
        localLviTheater2.mTheater = localTheater;
        localLviTheater2.mTheaterClick = getTheaterClickListener();
        this.mFavorites.add(localLviTheater2);
      }
    }
  }

  private void setLocationString()
  {
    int i = FlixsterApplication.getTheaterDistance();
    String str1;
    if (FlixsterApplication.getUseLocationServiceFlag())
    {
      str1 = FlixsterApplication.getCurrentZip();
      if ((str1 != null) && (str1.length() != 0))
        break label71;
    }
    label71: String str2;
    Object[] arrayOfObject;
    for (this.mDistanceString = getResources().getString(2131493134); ; this.mDistanceString = String.format(str2, arrayOfObject))
    {
      this.mLocationStringHandler.sendEmptyMessage(0);
      return;
      str1 = FlixsterApplication.getUserZip();
      if ((str1 != null) && (str1.length() != 0))
        break;
      str1 = FlixsterApplication.getUserCity();
      break;
      str2 = getResources().getString(2131493094);
      arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(i);
      arrayOfObject[1] = str1;
    }
  }

  private void updateMap()
  {
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView1 = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    mTheaterDistanceComparator = new TheaterDistanceComparator();
    mTheaterNameComparator = new TheaterNameComparator();
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mNavbarHolder = ((LinearLayout)localView1.findViewById(2131165427));
    this.mNavBar = new SubNavBar(getActivity());
    this.mNavBar.load(this.mNavListener, 2131493155, 2131493156);
    this.mNavBar.setSelectedButton(2131165784);
    this.mNavbarHolder.addView(this.mNavBar, 0);
    this.mNavbarHolder = ((LinearLayout)localView1.findViewById(2131165429));
    View localView2 = paramLayoutInflater.inflate(2130903174, null);
    this.mNavbarHolder.addView(localView2);
    this.mLocationStringTextView = ((TextView)localView2.findViewById(2131165792));
    this.mLocationStringTextView.setOnClickListener(this.mSettingsClickListener);
    this.mNavSelect = 2131165784;
    return localView1;
  }

  public void onPause()
  {
    super.onPause();
    this.mTheaters.clear();
  }

  public void onResume()
  {
    super.onResume();
    setLocationString();
    trackPage();
    ScheduleLoadItemsTask(this.mNavSelect, 100L);
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "TheaterListPage.onSaveInstanceState()");
    paramBundle.putInt("LISTSTATE_NAV", this.mNavSelect);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(this.mNavSelect, 1000L);
  }

  public void trackPage()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 2131165784:
      Trackers.instance().track("/theaters/nearby", "Theaters - Nearby");
      return;
    case 2131165785:
      Trackers.instance().track("/theaters/name", "Theaters - By Name");
      return;
    case 2131165786:
    }
    Trackers.instance().track("/theaters/map", "Theaters - By Name");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.TheaterListFragment
 * JD-Core Version:    0.6.2
 */