package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.data.TheaterDao;
import net.flixster.android.model.Listing;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Showtimes;
import net.flixster.android.model.Theater;

@TargetApi(11)
public class TicketInfoFragment extends FlixsterFragment
  implements View.OnClickListener
{
  public static final int DIALOGKEY_LOADING_SHOWTIMES = 1;
  public static final int DIALOGKEY_NETWORKFAIL = 2;
  private Bundle mExtras;
  private LayoutInflater mLayoutInflater;
  private LinearLayout mListingLayout;
  private LinearLayout mListingPastLayout;
  private ProgressDialog mLoadingShowtimesDialog;
  private Movie mMovie;
  private long mMovieId;
  private int mNetworkTrys = 0;
  private ImageView mPosterView;
  private RelativeLayout mShowElapsedShowtimesPanel;
  private String mShowtimesTitle;
  private Handler mStartLoadingShowtimesDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)TicketInfoFragment.this.getActivity();
      if ((localMain == null) || (TicketInfoFragment.this.isRemoving()));
      while (((TicketInfoFragment.this.mLoadingShowtimesDialog != null) && (TicketInfoFragment.this.mLoadingShowtimesDialog.isShowing())) || (localMain == null) || (localMain.isFinishing()))
        return;
      localMain.showDialog(1);
    }
  };
  private Handler mStopLoadingShowtimesDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)TicketInfoFragment.this.getActivity();
      if ((localMain == null) || (TicketInfoFragment.this.isRemoving()));
      while ((TicketInfoFragment.this.mLoadingShowtimesDialog == null) || (!TicketInfoFragment.this.mLoadingShowtimesDialog.isShowing()) || (localMain == null) || (localMain.isFinishing()))
        return;
      localMain.removeDialog(1);
    }
  };
  private Theater mTheater;
  private long mTheaterId;
  private Handler postShowtimesLoadHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)TicketInfoFragment.this.getActivity();
      if ((localMain == null) || (TicketInfoFragment.this.isRemoving()))
        return;
      Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler");
      View localView = TicketInfoFragment.this.getView();
      Object localObject = null;
      Iterator localIterator1 = ((ArrayList)TicketInfoFragment.this.mTheater.getShowtimesListByMovieHash().get(Long.valueOf(TicketInfoFragment.this.mMovieId))).iterator();
      Drawable localDrawable;
      Iterator localIterator2;
      if (!localIterator1.hasNext())
      {
        Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler showtimes:" + localObject);
        localDrawable = localMain.getResources().getDrawable(2130837637);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        TicketInfoFragment.this.mListingLayout.removeAllViews();
        TicketInfoFragment.this.mListingPastLayout.removeAllViews();
        ((TextView)localView.findViewById(2131165813)).setText(localObject.mListings.size() + " showtimes total");
        Logger.d("FlxMain", "TicketInfoPage.postShowtimesLoadHandler showtimes.mListings:" + localObject.mListings);
        localIterator2 = localObject.mListings.iterator();
      }
      while (true)
      {
        if (!localIterator2.hasNext())
        {
          if (TicketInfoFragment.this.mListingPastLayout.getChildCount() != 0)
            break label441;
          TicketInfoFragment.this.mShowElapsedShowtimesPanel.setVisibility(8);
          return;
          Showtimes localShowtimes = (Showtimes)localIterator1.next();
          if (!localShowtimes.mShowtimesTitle.contentEquals(TicketInfoFragment.this.mShowtimesTitle))
            break;
          localObject = localShowtimes;
          break;
        }
        Listing localListing = (Listing)localIterator2.next();
        TextView localTextView = new TextView(localMain);
        ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
        localTextView.setTag(localListing);
        localTextView.setLayoutParams(localLayoutParams);
        localTextView.setGravity(16);
        localTextView.setText(localListing.getDisplayTimeDate());
        localTextView.setTextAppearance(TicketInfoFragment.this.getActivity(), 2131558512);
        localTextView.setBackgroundResource(2130837621);
        localTextView.setCompoundDrawablePadding(10);
        localTextView.setCompoundDrawables(null, null, localDrawable, null);
        localTextView.setPadding(30, 20, 20, 20);
        localTextView.setOnClickListener(TicketInfoFragment.this);
        TicketInfoFragment.this.mListingLayout.addView(localTextView);
      }
      label441: TicketInfoFragment.this.mShowElapsedShowtimesPanel.setVisibility(0);
    }
  };

  private void ScheduleLoadShowtimesTask()
  {
    TimerTask local4 = new TimerTask()
    {
      public void run()
      {
        if (((Main)TicketInfoFragment.this.getActivity() == null) || (TicketInfoFragment.this.isRemoving()))
          return;
        try
        {
          TicketInfoFragment.this.mStartLoadingShowtimesDialogHandler.sendEmptyMessage(0);
          Logger.d("FlxMain", "TheaterDao.Schedule mMovie.getId():" + TicketInfoFragment.this.mMovieId);
          TicketInfoFragment.this.mTheater = TheaterDao.findTheaterByTheaterMoviePair(TicketInfoFragment.this.mTheaterId, TicketInfoFragment.this.mMovieId, FlixsterApplication.getShowtimesDate());
          Logger.d("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask().run() mTheater.getShowtimesHash():" + TicketInfoFragment.this.mTheater.getShowtimesListByMovieHash());
          TicketInfoFragment.this.mStopLoadingShowtimesDialogHandler.sendEmptyMessage(0);
          TicketInfoFragment.this.postShowtimesLoadHandler.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask Exception mNetworkTrys:" + TicketInfoFragment.this.mNetworkTrys, localDaoException);
          if (TicketInfoFragment.this.mNetworkTrys < 3)
          {
            TicketInfoFragment.this.ScheduleLoadShowtimesTask();
            TicketInfoFragment localTicketInfoFragment = TicketInfoFragment.this;
            localTicketInfoFragment.mNetworkTrys = (1 + localTicketInfoFragment.mNetworkTrys);
            return;
          }
          Logger.d("FlxMain", "TicketInfoPage.ScheduleLoadShowtimesTask SHOW NETWORK ERROR DIALOG");
          TicketInfoFragment.this.mStopLoadingShowtimesDialogHandler.sendEmptyMessage(0);
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local4, 100L);
  }

  public static int getFlixFragId()
  {
    return 105;
  }

  public static TicketInfoFragment newInstance(Bundle paramBundle)
  {
    TicketInfoFragment localTicketInfoFragment = new TicketInfoFragment();
    localTicketInfoFragment.setArguments(paramBundle);
    return localTicketInfoFragment;
  }

  private void populateDetails()
  {
    View localView = getView();
    TextView localTextView1 = (TextView)localView.findViewById(2131165807);
    TextView localTextView2 = (TextView)localView.findViewById(2131165804);
    TextView localTextView3 = (TextView)localView.findViewById(2131165805);
    TextView localTextView4 = (TextView)localView.findViewById(2131165808);
    localTextView1.setText(this.mTheater.getProperty("name"));
    localTextView4.setText(this.mTheater.getProperty("address"));
    localTextView2.setText(this.mMovie.getProperty("MOVIE_ACTORS_SHORT"));
    localTextView3.setText(this.mMovie.getProperty("meta"));
    if (this.mMovie.getThumbnailPoster() == null)
      this.mPosterView.setImageResource(2130837839);
    Bitmap localBitmap;
    do
    {
      return;
      localBitmap = this.mMovie.getThumbnailBackedProfileBitmap(this.mPosterView);
    }
    while (localBitmap == null);
    this.mPosterView.setImageBitmap(localBitmap);
  }

  public void onClick(View paramView)
  {
    if (paramView.getClass() == TextView.class)
      Starter.launchBrowser(((Listing)paramView.getTag()).ticketUrl, getActivity());
    while (paramView.getId() != 2131165811)
      return;
    this.mListingPastLayout.setVisibility(0);
    this.mShowElapsedShowtimesPanel.setVisibility(8);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 1:
      this.mLoadingShowtimesDialog = new ProgressDialog(getActivity());
      this.mLoadingShowtimesDialog.setMessage(getResources().getString(2131493173));
      this.mLoadingShowtimesDialog.setIndeterminate(true);
      this.mLoadingShowtimesDialog.setCancelable(true);
      return this.mLoadingShowtimesDialog;
    case 2:
    }
    return new AlertDialog.Builder(getActivity()).setMessage("The network connection failed. Press Retry to make another attempt.").setTitle("Network Error").setCancelable(false).setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        TicketInfoFragment.this.mNetworkTrys = 0;
        TicketInfoFragment.this.ScheduleLoadShowtimesTask();
      }
    }).setNegativeButton(getResources().getString(2131492938), null).create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    View localView = this.mLayoutInflater.inflate(2130903175, null);
    TextView localTextView = (TextView)localView.findViewById(2131165803);
    this.mPosterView = ((ImageView)localView.findViewById(2131165801));
    this.mExtras = getArguments();
    this.mTheaterId = this.mExtras.getLong("id");
    this.mMovieId = this.mExtras.getLong("MOVIE_ID");
    this.mShowtimesTitle = this.mExtras.getString("SHOWTIMES_TITLE");
    this.mTheater = TheaterDao.getTheater(this.mTheaterId);
    this.mMovie = MovieDao.getMovie(this.mMovieId);
    Logger.d("FlxMain", "TicketInfoPage.onCreate mThaterId:" + this.mTheaterId + " mMovieId:" + this.mMovieId + " mShowtimesTitle:" + this.mShowtimesTitle);
    localTextView.setText(this.mShowtimesTitle);
    this.mListingLayout = ((LinearLayout)localView.findViewById(2131165815));
    this.mListingPastLayout = ((LinearLayout)localView.findViewById(2131165814));
    this.mShowElapsedShowtimesPanel = ((RelativeLayout)localView.findViewById(2131165811));
    this.mShowElapsedShowtimesPanel.setFocusable(true);
    this.mShowElapsedShowtimesPanel.setOnClickListener(this);
    return localView;
  }

  public void onResume()
  {
    super.onResume();
    populateDetails();
    trackPage();
    if (this.mTheater.getShowtimesListByMovieHash() == null)
    {
      ScheduleLoadShowtimesTask();
      return;
    }
    this.postShowtimesLoadHandler.sendEmptyMessage(0);
  }

  public void trackPage()
  {
    Trackers.instance().track("/tickets/info", "TicketInfoPage");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.TicketInfoFragment
 * JD-Core Version:    0.6.2
 */