package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.view.SubNavBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.lvi.LviActor;
import net.flixster.android.model.Actor;

@TargetApi(11)
public class TopActorsFragment extends LviFragment
{
  private static final int LIMIT_ACTORS = 25;
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)TopActorsFragment.this.getActivity();
      if ((localMain == null) || (TopActorsFragment.this.isRemoving()));
      while (TopActorsFragment.this.mTopThisWeek.isEmpty())
        return;
      Intent localIntent = new Intent("TOP_ACTOR", null, localMain, ActorFragment.class);
      Actor localActor = (Actor)TopActorsFragment.this.mTopThisWeek.get(0);
      localIntent.putExtra("ACTOR_ID", localActor.id);
      localIntent.putExtra("ACTOR_NAME", localActor.name);
      localMain.startFragment(localIntent, ActorFragment.class);
    }
  };
  private View.OnClickListener mNavListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      switch (paramAnonymousView.getId())
      {
      default:
      case 2131165784:
      case 2131165785:
      }
      while (true)
      {
        TopActorsFragment.this.trackPage();
        TopActorsFragment.this.mListView.setOnItemClickListener(TopActorsFragment.this.getMovieItemClickListener());
        TopActorsFragment.this.ScheduleLoadItemsTask(TopActorsFragment.this.mNavSelect, 100L);
        return;
        TopActorsFragment.this.mNavSelect = 2131165784;
        continue;
        TopActorsFragment.this.mNavSelect = 2131165785;
      }
    }
  };
  private int mNavSelect;
  private SubNavBar mNavbar;
  private LinearLayout mNavbarHolder;
  private ArrayList<Actor> mRandom = new ArrayList();
  private ArrayList<Actor> mTopThisWeek = new ArrayList();

  private void ScheduleLoadItemsTask(final int paramInt, long paramLong)
  {
    try
    {
      Logger.d("FlxMain", "TopActors.PageScheduleLoadItemsTask(...)");
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local3 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)TopActorsFragment.this.getActivity();
          if ((localMain == null) || (TopActorsFragment.this.isRemoving()));
          do
            while (true)
            {
              return;
              try
              {
                switch (paramInt)
                {
                default:
                  TopActorsFragment.this.mUpdateHandler.sendEmptyMessage(0);
                  if (localMain != null)
                    localMain.mRemoveDialogHandler.sendEmptyMessage(1);
                  if (!TopActorsFragment.this.isInitialContentLoaded)
                  {
                    TopActorsFragment.this.isInitialContentLoaded = true;
                    TopActorsFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
                    return;
                  }
                  break;
                case 2131165784:
                case 2131165785:
                }
              }
              catch (DaoException localDaoException)
              {
                while (true)
                {
                  Logger.w("FlxMain", "TopActorsPage.ScheduleLoadItemsTask.run() mRetryCount:" + TopActorsFragment.this.mRetryCount);
                  localDaoException.printStackTrace();
                  TopActorsFragment localTopActorsFragment = TopActorsFragment.this;
                  localTopActorsFragment.mRetryCount = (1 + localTopActorsFragment.mRetryCount);
                  if (TopActorsFragment.this.mRetryCount >= 3)
                    break;
                  TopActorsFragment.this.ScheduleLoadItemsTask(paramInt, 1000L);
                  return;
                  if (TopActorsFragment.this.mTopThisWeek.isEmpty())
                    TopActorsFragment.this.mTopThisWeek = ActorDao.getTopActors("popular", 25);
                  TopActorsFragment.this.setTopThisWeekLviList();
                  continue;
                  if (TopActorsFragment.this.mRandom.isEmpty())
                    TopActorsFragment.this.mRandom = ActorDao.getTopActors("random", 25);
                  TopActorsFragment.this.setRandomLviList();
                }
                TopActorsFragment.this.mRetryCount = 0;
              }
            }
          while (localMain == null);
          localMain.mRemoveDialogHandler.sendEmptyMessage(1);
          localMain.mShowDialogHandler.sendEmptyMessage(2);
        }
      };
      if (((Main)getActivity()).mPageTimer != null)
        ((Main)getActivity()).mPageTimer.schedule(local3, paramLong);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static int getFlixFragId()
  {
    return 9;
  }

  private void setRandomLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()));
    while (true)
    {
      return;
      this.mDataHolder.clear();
      int i = 0;
      Iterator localIterator = this.mRandom.iterator();
      while (localIterator.hasNext())
      {
        Actor localActor = (Actor)localIterator.next();
        int j = i + 1;
        localActor.positionId = i;
        LviActor localLviActor = new LviActor();
        localLviActor.mActor = localActor;
        localLviActor.mActorClick = getActorClickListener();
        this.mDataHolder.add(localLviActor);
        i = j;
      }
    }
  }

  private void setTopThisWeekLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()));
    while (true)
    {
      return;
      this.mDataHolder.clear();
      int i = 0;
      Iterator localIterator = this.mTopThisWeek.iterator();
      while (localIterator.hasNext())
      {
        Actor localActor = (Actor)localIterator.next();
        int j = i + 1;
        localActor.positionId = i;
        LviActor localLviActor = new LviActor();
        localLviActor.mActor = localActor;
        localLviActor.mActorClick = getActorClickListener();
        this.mDataHolder.add(localLviActor);
        i = j;
      }
    }
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    this.mListView.setOnItemClickListener(getMovieItemClickListener());
    this.mNavbarHolder = ((LinearLayout)localView.findViewById(2131165427));
    this.mNavbar = new SubNavBar(getActivity());
    this.mNavbar.load(this.mNavListener, 2131492893, 2131492914);
    this.mNavbar.setSelectedButton(2131165784);
    this.mNavbarHolder.addView(this.mNavbar, 0);
    this.mNavSelect = 2131165784;
    return localView;
  }

  public void onPause()
  {
    super.onPause();
    this.mTopThisWeek.clear();
    this.mRandom.clear();
  }

  public void onResume()
  {
    super.onResume();
    trackPage();
    this.mNavbar.setSelectedButton(this.mNavSelect);
    ScheduleLoadItemsTask(this.mNavSelect, 100L);
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Logger.d("FlxMain", "TopActorsPage.onSaveInstanceState()");
    paramBundle.putInt("LISTSTATE_NAV", this.mNavSelect);
  }

  public void trackPage()
  {
    switch (this.mNavSelect)
    {
    default:
      return;
    case 2131165784:
      Trackers.instance().track("/actor/top", "Top Actors Page for TopThisWeek");
      return;
    case 2131165785:
    }
    Trackers.instance().track("/actor/random", "Top Actors Page for Random");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.TopActorsFragment
 * JD-Core Version:    0.6.2
 */