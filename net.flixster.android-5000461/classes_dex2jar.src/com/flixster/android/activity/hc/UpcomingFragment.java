package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.ListHelper;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.lvi.LviFooter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.lvi.LviSubHeader;
import net.flixster.android.model.Movie;

@TargetApi(11)
public class UpcomingFragment extends LviFragment
{
  private Handler initialContentLoadingHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)UpcomingFragment.this.getActivity();
      if ((localMain == null) || (UpcomingFragment.this.isRemoving()));
      while (UpcomingFragment.this.mUpcoming.isEmpty())
        return;
      long l = ((Movie)UpcomingFragment.this.mUpcoming.get(0)).getId();
      Intent localIntent = new Intent("DETAILS", null, localMain, MovieDetailsFragment.class);
      localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", l);
      localMain.startFragment(localIntent, MovieDetailsFragment.class);
    }
  };
  private final ArrayList<Movie> mUpcoming = new ArrayList();
  private final ArrayList<Movie> mUpcomingFeatured = new ArrayList();

  private void ScheduleLoadItemsTask(long paramLong)
  {
    try
    {
      ((Main)getActivity()).mShowDialogHandler.sendEmptyMessage(1);
      TimerTask local2 = new TimerTask()
      {
        public void run()
        {
          Main localMain = (Main)UpcomingFragment.this.getActivity();
          if ((localMain == null) || (UpcomingFragment.this.isRemoving()));
          while (true)
          {
            return;
            Logger.d("FlxMain", "Upcoming.ScheduleLoadItemsTask.run");
            try
            {
              if (UpcomingFragment.this.mUpcoming.isEmpty())
              {
                ArrayList localArrayList1 = UpcomingFragment.this.mUpcomingFeatured;
                ArrayList localArrayList2 = UpcomingFragment.this.mUpcoming;
                int i = UpcomingFragment.this.mRetryCount;
                boolean bool = false;
                if (i == 0)
                  bool = true;
                MovieDao.fetchUpcoming(localArrayList1, localArrayList2, bool);
              }
              UpcomingFragment.this.setUpcomingLviList();
              UpcomingFragment.this.mUpdateHandler.sendEmptyMessage(0);
              if (localMain != null)
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
              if (!UpcomingFragment.this.isInitialContentLoaded)
              {
                UpcomingFragment.this.isInitialContentLoaded = true;
                UpcomingFragment.this.initialContentLoadingHandler.sendEmptyMessage(0);
              }
              return;
            }
            catch (DaoException localDaoException)
            {
              Logger.e("FlxMain", "UpcomingFragment.ScheduleLoadItemsTask.run DaoException", localDaoException);
              UpcomingFragment.this.retryLogic(localDaoException);
              return;
            }
            finally
            {
              if ((localMain != null) && (localMain.mLoadingDialog != null) && (localMain.mLoadingDialog.isShowing()))
                localMain.mRemoveDialogHandler.sendEmptyMessage(1);
            }
          }
        }
      };
      Main localMain = (Main)getActivity();
      if ((localMain != null) && (!localMain.isFinishing()) && (localMain.mPageTimer != null))
        localMain.mPageTimer.schedule(local2, paramLong);
      return;
    }
    finally
    {
    }
  }

  public static int getFlixFragId()
  {
    return 3;
  }

  private void setUpcomingLviList()
  {
    if (((Main)getActivity() == null) || (isRemoving()))
      return;
    Logger.d("FlxMain", "UpcomingFragment.setPopularLviList ");
    this.mDataHolder.clear();
    ArrayList localArrayList = ListHelper.clone(this.mUpcomingFeatured);
    Iterator localIterator1;
    Object localObject;
    Iterator localIterator2;
    if (!localArrayList.isEmpty())
    {
      LviSubHeader localLviSubHeader1 = new LviSubHeader();
      localLviSubHeader1.mTitle = getResources().getString(2131493054);
      this.mDataHolder.add(localLviSubHeader1);
      localIterator1 = localArrayList.iterator();
      if (localIterator1.hasNext());
    }
    else
    {
      localObject = FlixsterApplication.sToday;
      localIterator2 = ListHelper.clone(this.mUpcoming).iterator();
    }
    while (true)
    {
      if (!localIterator2.hasNext())
      {
        LviFooter localLviFooter = new LviFooter();
        this.mDataHolder.add(localLviFooter);
        return;
        Movie localMovie1 = (Movie)localIterator1.next();
        LviMovie localLviMovie1 = new LviMovie();
        localLviMovie1.mMovie = localMovie1;
        localLviMovie1.mTrailerClick = getTrailerOnClickListener();
        this.mDataHolder.add(localLviMovie1);
        break;
      }
      Movie localMovie2 = (Movie)localIterator2.next();
      String str = localMovie2.getProperty("theaterReleaseDate");
      Date localDate = localMovie2.getTheaterReleaseDate();
      if (((Date)localObject).before(localDate))
      {
        LviSubHeader localLviSubHeader2 = new LviSubHeader();
        localLviSubHeader2.mTitle = str;
        this.mDataHolder.add(localLviSubHeader2);
        localObject = localDate;
      }
      LviMovie localLviMovie2 = new LviMovie();
      localLviMovie2.mMovie = localMovie2;
      this.mDataHolder.add(localLviMovie2);
    }
  }

  public void onPause()
  {
    super.onPause();
    this.mUpcoming.clear();
    this.mUpcomingFeatured.clear();
  }

  public void onResume()
  {
    super.onResume();
    trackPage();
    ScheduleLoadItemsTask(100L);
  }

  protected void retryAction()
  {
    ScheduleLoadItemsTask(1000L);
  }

  public void trackPage()
  {
    Trackers.instance().track("/upcoming", "Upcoming Movies");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.UpcomingFragment
 * JD-Core Version:    0.6.2
 */