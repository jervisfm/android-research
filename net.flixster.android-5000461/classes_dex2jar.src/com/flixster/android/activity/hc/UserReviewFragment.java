package com.flixster.android.activity.hc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.flixster.android.utils.Logger;
import java.lang.ref.SoftReference;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;
import net.flixster.android.model.User;

@TargetApi(11)
public class UserReviewFragment extends FlixsterFragment
  implements View.OnClickListener, View.OnTouchListener
{
  private static final int DIALOG_NETWORK_FAIL = 1;
  private static final int DIALOG_REVIEW = 2;
  public static final String KEY_REVIEW_INDEX = "REVIEW_INDEX";
  public static final String KEY_REVIEW_TYPE = "REVIEW_TYPE";
  public static final int REVIEW_TYPE_MY_RATED = 1;
  public static final int REVIEW_TYPE_MY_REVIEWS = 2;
  public static final int REVIEW_TYPE_MY_WANT_TO_SEE;
  private LayoutInflater mLayoutInflater;
  private Button mNextButton;
  private Button mPreviousButton;
  private int mReviewIndex;
  private TextView mReviewIndexView;
  private int mReviewType;
  private List<Review> mReviews;
  private User mUser;
  private ViewFlipper mViewFlipper;
  private Handler movieImageHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (UserReviewFragment.this.isRemoving());
      ImageView localImageView;
      Movie localMovie;
      do
      {
        do
        {
          return;
          localImageView = (ImageView)paramAnonymousMessage.obj;
        }
        while (localImageView == null);
        localMovie = (Movie)localImageView.getTag();
      }
      while ((localMovie == null) || (localMovie.thumbnailSoftBitmap.get() == null));
      localImageView.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
      localImageView.invalidate();
    }
  };
  private Handler updateHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Main localMain = (Main)UserReviewFragment.this.getActivity();
      if ((localMain == null) || (UserReviewFragment.this.isRemoving()));
      do
      {
        return;
        Logger.d("FlxMain", "UserReviewPage.updateHandler - mReviewsList:" + UserReviewFragment.this.mReviews);
        if ((UserReviewFragment.this.mUser != null) && (UserReviewFragment.this.mReviews != null) && (!UserReviewFragment.this.mReviews.isEmpty()))
        {
          UserReviewFragment.this.updatePage();
          return;
        }
      }
      while ((localMain == null) || (localMain.isFinishing()));
      localMain.showDialog(1);
    }
  };

  public static int getFlixFragId()
  {
    return 107;
  }

  private void scheduleUpdatePageTask()
  {
    TimerTask local4 = new TimerTask()
    {
      public void run()
      {
        if (((Main)UserReviewFragment.this.getActivity() == null) || (UserReviewFragment.this.isRemoving()))
          return;
        if ((UserReviewFragment.this.mUser == null) || (UserReviewFragment.this.mReviews == null) || (UserReviewFragment.this.mReviews.isEmpty()));
        try
        {
          UserReviewFragment.this.mUser = ProfileDao.fetchUser();
          if ((UserReviewFragment.this.mUser == null) || (UserReviewFragment.this.mUser.id == null));
        }
        catch (DaoException localDaoException1)
        {
          try
          {
            switch (UserReviewFragment.this.mReviewType)
            {
            default:
              UserReviewFragment.this.mReviews = UserReviewFragment.this.mUser.reviews;
              if ((UserReviewFragment.this.mReviews != null) && ((!UserReviewFragment.this.mReviews.isEmpty()) || (UserReviewFragment.this.mUser.reviewCount <= 0)))
                break;
            case 0:
              for (UserReviewFragment.this.mReviews = ProfileDao.getUserReviews(UserReviewFragment.this.mUser.id, 50); ; UserReviewFragment.this.mReviews = ProfileDao.getWantToSeeReviews(UserReviewFragment.this.mUser.id, 50))
                do
                {
                  UserReviewFragment.this.updateHandler.sendEmptyMessage(0);
                  return;
                  localDaoException1 = localDaoException1;
                  Logger.e("FlxMain", "UserReviewPage.scheduleUpdatePageTask (failed to get user data)", localDaoException1);
                  UserReviewFragment.this.mUser = null;
                  break;
                  UserReviewFragment.this.mReviews = UserReviewFragment.this.mUser.wantToSeeReviews;
                }
                while ((UserReviewFragment.this.mReviews != null) && ((!UserReviewFragment.this.mReviews.isEmpty()) || (UserReviewFragment.this.mUser.wtsCount <= 0)));
            case 1:
            }
          }
          catch (DaoException localDaoException2)
          {
            while (true)
            {
              Logger.e("FlxMain", "UserReviewPage.scheduleUpdatePageTask (failed to get review data)", localDaoException2);
              continue;
              UserReviewFragment.this.mReviews = UserReviewFragment.this.mUser.ratedReviews;
              if ((UserReviewFragment.this.mReviews == null) || ((UserReviewFragment.this.mReviews.isEmpty()) && (UserReviewFragment.this.mUser.ratingCount > 0)))
                UserReviewFragment.this.mReviews = ProfileDao.getUserRatedReviews(UserReviewFragment.this.mUser.id, 50);
            }
          }
        }
      }
    };
    if (((Main)getActivity()).mPageTimer != null)
      ((Main)getActivity()).mPageTimer.schedule(local4, 100L);
  }

  private void updatePage()
  {
    Logger.d("FlxMain", "UserReviewPage.updatePage reviewIndex:" + this.mReviewIndex);
    Iterator localIterator;
    if (this.mUser != null)
    {
      View localView = getView();
      ImageView localImageView1 = (ImageView)localView.findViewById(2131165642);
      Bitmap localBitmap = this.mUser.getThumbnailBitmap(localImageView1);
      if (localBitmap != null)
        localImageView1.setImageBitmap(localBitmap);
      ((TextView)localView.findViewById(2131165643)).setText(this.mUser.displayName);
      if ((this.mReviews != null) && (!this.mReviews.isEmpty()))
      {
        this.mViewFlipper.removeAllViews();
        localIterator = this.mReviews.iterator();
        if (localIterator.hasNext())
          break label210;
        this.mViewFlipper.setDisplayedChild(this.mReviewIndex);
        String str2 = "" + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviews.size();
        this.mReviewIndexView.setText(str2);
        this.mPreviousButton.setVisibility(0);
        this.mNextButton.setVisibility(0);
      }
    }
    return;
    label210: Review localReview = (Review)localIterator.next();
    Movie localMovie = localReview.getMovie();
    ScrollView localScrollView = (ScrollView)this.mLayoutInflater.inflate(2130903136, this.mViewFlipper, false);
    RelativeLayout localRelativeLayout = (RelativeLayout)localScrollView.findViewById(2131165629);
    localRelativeLayout.setTag(localMovie);
    localRelativeLayout.setFocusable(true);
    localRelativeLayout.setOnClickListener(this);
    ImageView localImageView2 = (ImageView)localScrollView.findViewById(2131165630);
    localImageView2.setTag(localMovie);
    if (localMovie.thumbnailSoftBitmap.get() != null)
      localImageView2.setImageBitmap((Bitmap)localMovie.thumbnailSoftBitmap.get());
    String[] arrayOfString;
    int[] arrayOfInt;
    int i;
    while (true)
    {
      arrayOfString = new String[] { "title", "MOVIE_ACTORS_SHORT", "meta" };
      arrayOfInt = new int[] { 2131165631, 2131165632, 2131165633 };
      i = 0;
      if (i < arrayOfString.length)
        break label515;
      ((ImageView)localScrollView.findViewById(2131165636)).setImageResource(net.flixster.android.Flixster.RATING_LARGE_R[((int)(2.0D * localReview.stars))]);
      ((TextView)localScrollView.findViewById(2131165637)).setText(localReview.comment);
      this.mViewFlipper.addView(localScrollView);
      break;
      String str1 = localMovie.getProperty("thumbnail");
      if ((str1 != null) && (str1.startsWith("http")))
      {
        ImageOrder localImageOrder = new ImageOrder(0, localMovie, str1, localImageView2, this.movieImageHandler);
        ((Main)getActivity()).orderImage(localImageOrder);
      }
      else
      {
        localImageView2.setImageResource(2130837839);
      }
    }
    label515: TextView localTextView = (TextView)localScrollView.findViewById(arrayOfInt[i]);
    if (localMovie.checkProperty(arrayOfString[i]))
    {
      localTextView.setText(localMovie.getProperty(arrayOfString[i]));
      localTextView.setVisibility(0);
    }
    while (true)
    {
      i++;
      break;
      localTextView.setVisibility(8);
    }
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
    case 2131165629:
    }
    Movie localMovie;
    do
    {
      return;
      localMovie = (Movie)paramView.getTag();
    }
    while (localMovie == null);
    Intent localIntent = new Intent("DETAILS", null, getActivity(), MovieDetailsFragment.class);
    localIntent.putExtra("net.flixster.android.EXTRA_MOVIE_ID", localMovie.getId());
    ((Main)getActivity()).startFragment(localIntent, MovieDetailsFragment.class);
  }

  public Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      ProgressDialog localProgressDialog2 = new ProgressDialog(getActivity());
      localProgressDialog2.setMessage(getResources().getString(2131493173));
      localProgressDialog2.setIndeterminate(true);
      localProgressDialog2.setCancelable(true);
      localProgressDialog2.setCanceledOnTouchOutside(true);
      return localProgressDialog2;
    case 2:
      ProgressDialog localProgressDialog1 = new ProgressDialog(getActivity());
      localProgressDialog1.setMessage(getResources().getString(2131493173));
      localProgressDialog1.setIndeterminate(true);
      localProgressDialog1.setCancelable(true);
      localProgressDialog1.setCanceledOnTouchOutside(true);
      return localProgressDialog1;
    case 1:
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setMessage("The network connection failed. Press Retry to make another attempt.");
    localBuilder.setTitle("Network Error");
    localBuilder.setCancelable(false);
    localBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        UserReviewFragment.this.getActivity().showDialog(2);
        UserReviewFragment.this.scheduleUpdatePageTask();
      }
    });
    localBuilder.setNegativeButton(getResources().getString(2131492938), null);
    return localBuilder.create();
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.mLayoutInflater = paramLayoutInflater;
    View localView = this.mLayoutInflater.inflate(2130903137, null);
    this.mReviewIndexView = ((TextView)localView.findViewById(2131165640));
    this.mViewFlipper = ((ViewFlipper)localView.findViewById(2131165644));
    this.mPreviousButton = ((Button)localView.findViewById(2131165641));
    this.mPreviousButton.setOnTouchListener(this);
    this.mNextButton = ((Button)localView.findViewById(2131165639));
    this.mNextButton.setOnTouchListener(this);
    updatePage();
    return localView;
  }

  public void onResume()
  {
    Logger.d("FlxMain", "UserReviewPage.onResume");
    super.onResume();
    scheduleUpdatePageTask();
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 0);
    switch (paramView.getId())
    {
    case 2131165640:
    default:
      return false;
    case 2131165639:
      this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968582));
      this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968583));
      this.mViewFlipper.showNext();
      String str2 = "" + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviews.size();
      this.mReviewIndexView.setText(str2);
      return true;
    case 2131165641:
    }
    this.mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968584));
    this.mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(), 2130968585));
    this.mViewFlipper.showPrevious();
    String str1 = "" + (1 + this.mViewFlipper.getDisplayedChild()) + " of " + this.mReviews.size();
    this.mReviewIndexView.setText(str1);
    return true;
  }

  public void trackPage()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.hc.UserReviewFragment
 * JD-Core Version:    0.6.2
 */