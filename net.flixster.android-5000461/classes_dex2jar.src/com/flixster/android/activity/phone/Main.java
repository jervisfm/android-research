package com.flixster.android.activity.phone;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.SearchPage;
import net.flixster.android.SettingsPage;

@TargetApi(11)
public class Main extends SherlockActivity
  implements ActionBar.TabListener
{
  private boolean isActionBarReady;

  private static int getLastTab()
  {
    String str = FlixsterApplication.getLastTab();
    try
    {
      int i = Integer.valueOf(str).intValue();
      int j = i;
      if (j >= 5)
        j = 1;
      return j;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 1;
  }

  private void resetBackStack()
  {
    getFragmentManager().popBackStack("0", 1);
  }

  private static void setLastTab(int paramInt)
  {
    FlixsterApplication.setLastTab(String.valueOf(paramInt));
  }

  protected void createActionBar()
  {
    ActionBar localActionBar = getSupportActionBar();
    localActionBar.setNavigationMode(2);
    localActionBar.setLogo(2130837505);
    localActionBar.setDisplayShowTitleEnabled(false);
    ActionBar.Tab localTab1 = localActionBar.newTab().setIcon(2130837949);
    ActionBar.Tab localTab2 = localActionBar.newTab().setIcon(2130837947);
    ActionBar.Tab localTab3 = localActionBar.newTab().setIcon(2130837950);
    ActionBar.Tab localTab4 = localActionBar.newTab().setIcon(2130837951);
    ActionBar.Tab localTab5 = localActionBar.newTab().setIcon(2130837948);
    localTab1.setTabListener(this);
    localTab2.setTabListener(this);
    localTab3.setTabListener(this);
    localTab4.setTabListener(this);
    localTab5.setTabListener(this);
    localActionBar.addTab(localTab1);
    localActionBar.addTab(localTab2);
    localActionBar.addTab(localTab3);
    localActionBar.addTab(localTab4);
    localActionBar.addTab(localTab5);
    localActionBar.setSelectedNavigationItem(getLastTab());
    this.isActionBarReady = true;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903116);
    createActionBar();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getSupportMenuInflater().inflate(2131689482, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool = true;
    switch (paramMenuItem.getItemId())
    {
    default:
      bool = super.onOptionsItemSelected(paramMenuItem);
    case 2131165955:
      return bool;
    case 2131165946:
      startActivity(new Intent(this, SearchPage.class));
      return bool;
    case 2131165947:
    }
    startActivity(new Intent(this, SettingsPage.class));
    return bool;
  }

  protected void onResume()
  {
    super.onResume();
  }

  public void onTabReselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
  }

  public void onTabSelected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
    if (this.isActionBarReady)
      setLastTab(paramTab.getPosition());
    Object localObject = paramTab.getTag();
    Class localClass = null;
    if (localObject != null)
      localClass = (Class)localObject;
    if (localClass == null)
      return;
    try
    {
      Fragment localFragment = (Fragment)localClass.newInstance();
      paramFragmentTransaction.replace(2131165443, localFragment, null);
      return;
    }
    catch (InstantiationException localInstantiationException)
    {
      throw new RuntimeException(localInstantiationException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
  }

  public void onTabUnselected(ActionBar.Tab paramTab, FragmentTransaction paramFragmentTransaction)
  {
    resetBackStack();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.phone.Main
 * JD-Core Version:    0.6.2
 */