package com.flixster.android.activity.phone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.common.DecoratedSherlockFragmentActivity;
import com.flixster.android.activity.common.SearchFragment;
import com.flixster.android.activity.decorator.ActionBarDecorator;

public class SearchFragmentActivity extends DecoratedSherlockFragmentActivity
{
  private String query;
  private TextView searchEditText;
  private SearchFragment searchFragment;

  private void performNewSearch(String paramString)
  {
    this.searchEditText.setText(paramString);
    this.searchFragment.setQuery(paramString);
    this.searchFragment.invalidateData();
  }

  private void updateTitle()
  {
    setActionBarTitle(getString(2131492936) + " - " + this.query);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    removeDecorator(this.actionbarDecorator);
    SearchActionBarDecorator localSearchActionBarDecorator = new SearchActionBarDecorator(this, null);
    this.actionbarDecorator = localSearchActionBarDecorator;
    addDecorator(localSearchActionBarDecorator);
    this.actionbarDecorator.onCreate();
    setContentView(2130903159);
    createActionBar();
    if (paramBundle == null);
    for (String str = getIntent().getStringExtra("net.flixster.android.EXTRA_QUERY"); ; str = paramBundle.getString("net.flixster.android.EXTRA_QUERY"))
    {
      this.query = str;
      this.searchFragment = ((SearchFragment)getSupportFragmentManager().findFragmentById(2131165748));
      this.searchFragment.setQuery(this.query);
      updateTitle();
      return;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    paramMenu.removeItem(2131165955);
    this.searchEditText = ((TextView)paramMenu.getItem(0).getActionView().findViewById(2131165223));
    this.searchEditText.setText(this.query);
    return true;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("net.flixster.android.EXTRA_QUERY", this.query);
  }

  private class SearchActionBarDecorator extends ActionBarDecorator
  {
    private SearchActionBarDecorator(Activity arg2)
    {
      super();
    }

    public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
    {
      if (paramInt == 2)
      {
        hideSoftKeyboard(paramTextView);
        ((MenuItem)paramTextView.getTag()).collapseActionView();
        SearchFragmentActivity localSearchFragmentActivity1 = SearchFragmentActivity.this;
        SearchFragmentActivity localSearchFragmentActivity2 = SearchFragmentActivity.this;
        String str = paramTextView.getText().toString();
        localSearchFragmentActivity2.query = str;
        localSearchFragmentActivity1.performNewSearch(str);
        SearchFragmentActivity.this.updateTitle();
        return true;
      }
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.activity.phone.SearchFragmentActivity
 * JD-Core Version:    0.6.2
 */