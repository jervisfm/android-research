package com.flixster.android.ads;

import android.app.Activity;
import com.flixster.android.activity.TabbedActivity;
import com.flixster.android.ads.rule.AdRule;
import com.flixster.android.ads.rule.IntervalCapRule;
import com.flixster.android.ads.rule.LaunchRule;
import com.flixster.android.ads.rule.TimeCapRule;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import java.util.Date;
import net.flixster.android.ActorPage;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.MovieDetails;
import net.flixster.android.ShowtimesPage;
import net.flixster.android.TheaterInfoPage;
import net.flixster.android.model.Property;

public class AdsArbiter
{
  private static final LaunchAdsArbiter LAUNCH = new LaunchAdsArbiter(null);
  private static final TrailerAdsArbiter TRAILER = new TrailerAdsArbiter(null);

  private static boolean isAdsEnabled()
  {
    if (Properties.instance().isHoneycombTablet());
    while ((Properties.instance().isGoogleTv()) || (172800000L + FlixsterApplication.sInstallMsPosixTime > FlixsterApplication.sToday.getTime()))
      return false;
    return true;
  }

  private static boolean isClientLaunchCapped()
  {
    if (FlixsterApplication.isAdAdminLaunchCapDisabled());
    Property localProperty;
    do
    {
      return false;
      localProperty = Properties.instance().getProperties();
    }
    while ((localProperty != null) && (!localProperty.isClientCapped));
    return true;
  }

  private static boolean isClientPostitialCapped()
  {
    if (FlixsterApplication.isAdAdminPostitialCapDisabled());
    Property localProperty;
    do
    {
      return false;
      localProperty = Properties.instance().getProperties();
    }
    while ((localProperty != null) && (!localProperty.isClientCapped));
    return true;
  }

  private static boolean isClientPrerollCapped()
  {
    if (FlixsterApplication.isAdAdminPrerollCapDisabled());
    Property localProperty;
    do
    {
      return false;
      localProperty = Properties.instance().getProperties();
    }
    while ((localProperty != null) && (!localProperty.isClientCapped));
    return true;
  }

  public static boolean isTopLevelActivityOkayToShowInterstitial()
  {
    Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
    if ((!(localActivity instanceof TabbedActivity)) && (!(localActivity instanceof MovieDetails)) && (!(localActivity instanceof TheaterInfoPage)) && (!(localActivity instanceof ShowtimesPage)) && (!(localActivity instanceof ActorPage)));
    for (boolean bool = false; ; bool = true)
    {
      if (!bool)
        Logger.sw("FlxAd", "AdsArbiter.isTopLevelActivityOkayToShowInterstitial false");
      return bool;
    }
  }

  public static LaunchAdsArbiter launch()
  {
    return LAUNCH;
  }

  public static TrailerAdsArbiter trailer()
  {
    return TRAILER;
  }

  protected static class BaseAdsArbiter
  {
    private boolean isEnabled = AdsArbiter.access$3();

    protected boolean isEnabled()
    {
      return this.isEnabled;
    }

    public void onAppStart()
    {
      this.isEnabled = AdsArbiter.access$3();
    }
  }

  public static class LaunchAdsArbiter extends AdsArbiter.BaseAdsArbiter
  {
    private final AdRule launchRule = new LaunchRule();
    private final AdRule timeCapRule = new TimeCapRule(1800000L);

    public void onAppStart()
    {
      super.onAppStart();
      this.launchRule.reset();
    }

    public void reshowLater()
    {
      this.timeCapRule.reset();
      this.launchRule.reset();
    }

    public boolean shouldShow()
    {
      if (isEnabled())
      {
        if (AdsArbiter.access$2())
          if ((!this.timeCapRule.shouldShowAd()) || (!AdsArbiter.isTopLevelActivityOkayToShowInterstitial()));
        while ((this.launchRule.shouldShowAd()) && (AdsArbiter.isTopLevelActivityOkayToShowInterstitial()))
        {
          return true;
          return false;
        }
        return false;
      }
      return false;
    }

    public void shown()
    {
      this.timeCapRule.adShown();
      this.launchRule.adShown();
    }
  }

  public static class TrailerAdsArbiter extends AdsArbiter.BaseAdsArbiter
  {
    private static final int PREROLL_INTERVAL = 3;
    private boolean originatedFromAds;
    private final AdRule postitialTimeCapRule = new TimeCapRule(1800000L);
    private final AdRule prerollIntervalCapRule = new IntervalCapRule(3);
    private boolean prerollShown;

    private void onNewTrailerPlayback()
    {
      this.originatedFromAds = false;
      this.prerollShown = false;
    }

    public void originatedFromAds()
    {
      onNewTrailerPlayback();
      this.originatedFromAds = true;
    }

    public void originatedFromNonAds()
    {
      onNewTrailerPlayback();
    }

    public void postitialShown()
    {
      this.postitialTimeCapRule.adShown();
      onNewTrailerPlayback();
    }

    public void prerollShown()
    {
      this.prerollShown = true;
    }

    public boolean shouldShowPostitial()
    {
      if (isEnabled())
      {
        if (AdsArbiter.access$1())
          if ((this.originatedFromAds) || (this.prerollShown) || (!this.postitialTimeCapRule.shouldShowAd()));
        while ((!this.originatedFromAds) && (!this.prerollShown))
        {
          return true;
          return false;
        }
        return false;
      }
      return false;
    }

    public boolean shouldShowPreroll()
    {
      boolean bool1 = isEnabled();
      boolean bool2 = false;
      if (bool1)
      {
        if (!AdsArbiter.access$0())
          break label52;
        boolean bool4 = this.prerollIntervalCapRule.shouldShowAd();
        bool2 = false;
        if (bool4)
        {
          boolean bool5 = this.originatedFromAds;
          bool2 = false;
          if (!bool5)
            bool2 = true;
        }
      }
      label52: boolean bool3;
      do
      {
        return bool2;
        bool3 = this.originatedFromAds;
        bool2 = false;
      }
      while (bool3);
      return true;
    }

    public boolean shouldShowSponsor()
    {
      return !this.originatedFromAds;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.AdsArbiter
 * JD-Core Version:    0.6.2
 */