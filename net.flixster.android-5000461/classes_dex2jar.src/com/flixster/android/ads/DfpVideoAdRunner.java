package com.flixster.android.ads;

import android.app.Activity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import com.google.ads.interactivemedia.api.AdError;
import com.google.ads.interactivemedia.api.AdErrorEvent;
import com.google.ads.interactivemedia.api.AdErrorListener;
import com.google.ads.interactivemedia.api.AdsLoader;
import com.google.ads.interactivemedia.api.AdsLoader.AdsLoadedEvent;
import com.google.ads.interactivemedia.api.AdsLoader.AdsLoadedListener;
import com.google.ads.interactivemedia.api.AdsManager.AdEvent;
import com.google.ads.interactivemedia.api.AdsManager.AdEventListener;
import com.google.ads.interactivemedia.api.AdsManager.AdEventType;
import com.google.ads.interactivemedia.api.SimpleAdsRequest;
import com.google.ads.interactivemedia.api.SimpleAdsRequest.AdType;
import com.google.ads.interactivemedia.api.VideoAdsManager;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer;
import net.flixster.android.ads.model.DfpAd.DfpCustomTarget;
import net.flixster.android.model.Movie;

public class DfpVideoAdRunner
  implements AdsLoader.AdsLoadedListener, AdErrorListener
{
  private static final String PROD_URL = "http://pubads.g.doubleclick.net/gampad/ads?sz=480x320&iu=%2F6327%2Fmob.flix.android%2Ftrailer_preroll&ciu_szs=&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&m_ast=vast";
  private final Activity activity;
  private AdsLoader adsLoader;
  private VideoAdsManager adsManager;
  private final AdsManager.AdEventListener callback;
  private final Movie movie;
  private final VideoAdPlayer player;

  public DfpVideoAdRunner(Activity paramActivity, VideoAdPlayer paramVideoAdPlayer, AdsManager.AdEventListener paramAdEventListener, Movie paramMovie)
  {
    this.activity = paramActivity;
    this.player = paramVideoAdPlayer;
    this.callback = paramAdEventListener;
    this.movie = paramMovie;
  }

  public void onAdError(AdErrorEvent paramAdErrorEvent)
  {
    String str = paramAdErrorEvent.getError().getMessage();
    Logger.w("FlxAd", "DfpVideoAdRunner.onAdError " + str);
    Trackers.instance().trackEvent("/preroll/dfp", "Preroll - DFP", "Preroll", "AdError", str, 0);
    this.activity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxAd", "DfpVideoAdRunner.onAdError resuming content");
        DfpVideoAdRunner.this.callback.onAdEvent(new AdsManager.AdEvent(AdsManager.AdEventType.CONTENT_RESUME_REQUESTED));
      }
    });
  }

  public void onAdsLoaded(AdsLoader.AdsLoadedEvent paramAdsLoadedEvent)
  {
    Logger.d("FlxAd", "DfpVideoAdRunner.onAdsLoaded playing");
    Trackers.instance().trackEvent("/preroll/dfp", "Preroll - DFP", "Preroll", "AdLoadSuccess");
    this.adsManager = ((VideoAdsManager)paramAdsLoadedEvent.getManager());
    this.adsManager.addAdEventListener(this.callback);
    this.adsManager.addAdErrorListener(this);
    try
    {
      this.adsManager.play(this.player);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Logger.e("FlxAd", "DfpVideoAdRunner.onAdsLoaded", localIllegalStateException);
    }
  }

  public void showAd()
  {
    Logger.d("FlxAd", "DfpVideoAdRunner.showAd requesting");
    Trackers.instance().trackEvent("/preroll/dfp", "Preroll - DFP", "Preroll", "AdRequest");
    this.adsLoader = new AdsLoader(this.activity);
    this.adsLoader.addAdsLoadedListener(this);
    this.adsLoader.addAdErrorListener(this);
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        SimpleAdsRequest localSimpleAdsRequest = new SimpleAdsRequest();
        localSimpleAdsRequest.setAdTagUrl(DfpAd.DfpCustomTarget.addCustomTargetPairTo("http://pubads.g.doubleclick.net/gampad/ads?sz=480x320&iu=%2F6327%2Fmob.flix.android%2Ftrailer_preroll&ciu_szs=&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&m_ast=vast", DfpVideoAdRunner.this.movie, localSimpleAdsRequest));
        localSimpleAdsRequest.setAdType(SimpleAdsRequest.AdType.VIDEO);
        DfpVideoAdRunner.this.adsLoader.requestAds(localSimpleAdsRequest);
      }
    });
  }

  public void unloadAd()
  {
    Logger.d("FlxAd", "DfpVideoAdRunner.unloadAd");
    this.adsManager.unload();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.DfpVideoAdRunner
 * JD-Core Version:    0.6.2
 */