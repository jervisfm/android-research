package com.flixster.android.ads;

import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;
import com.flixster.android.utils.Logger;
import net.flixster.android.Starter;
import net.flixster.android.ads.AdManager;
import net.flixster.android.ads.model.FlixsterAd;

public class HtmlAdBannerView extends WebView
{
  private static final FrameLayout.LayoutParams FRAME_LAYOUT_FILL = new FrameLayout.LayoutParams(-1, -2);
  private final FlixsterAd htmlAd;

  public HtmlAdBannerView(Context paramContext, FlixsterAd paramFlixsterAd)
  {
    super(paramContext);
    this.htmlAd = paramFlixsterAd;
    setVerticalScrollBarEnabled(false);
    setHorizontalScrollBarEnabled(false);
    setWebViewClient(new HtmlAdWebViewClient(null));
    getSettings().setJavaScriptEnabled(true);
    getSettings().setSupportZoom(false);
  }

  private void launchHtmlAd(String paramString)
  {
    Starter.launchAdHtmlPage(paramString, this.htmlAd.title, getContext());
    AdManager.instance().trackEvent(this.htmlAd, "Click");
  }

  public void loadUrl(String paramString)
  {
    super.loadUrl(paramString);
    setLayoutParams(FRAME_LAYOUT_FILL);
    AdManager.instance().trackEvent(this.htmlAd, "Impression");
  }

  private final class HtmlAdWebViewClient extends WebViewClient
  {
    private HtmlAdWebViewClient()
    {
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      super.shouldOverrideUrlLoading(paramWebView, paramString);
      Logger.d("FlxAd", "HtmlAdBannerView.shouldOverrideUrlLoading true " + paramString);
      HtmlAdBannerView.this.launchHtmlAd(paramString);
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.HtmlAdBannerView
 * JD-Core Version:    0.6.2
 */