package com.flixster.android.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flixster.android.activity.DeepLink;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.msk.MskController;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import java.net.MalformedURLException;
import java.net.URL;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.data.ApiBuilder;

public class HtmlAdPage extends DecoratedSherlockActivity
{
  public static final int FLAG_BACK_NAVIGATION = 4;
  public static final int FLAG_CLEAR_COOKIE = 1;
  public static final int FLAG_HTML_TITLE = 2;
  public static final int FLAG_IS_AD = 32;
  public static final int FLAG_LAUNCH_BROWSER = 16;
  public static final int FLAG_SHOW_SKIP = 64;
  public static final int FLAG_SUPPORT_ZOOM = 8;
  public static final String KEY_FLAGS = "KEY_FLAGS";
  public static final String KEY_TITLE = "KEY_TITLE";
  public static final String KEY_URL = "KEY_URL";
  private static final LinearLayout.LayoutParams LINEAR_LAYOUT_FILL = new LinearLayout.LayoutParams(-1, -1);
  private static int instanceCount = 0;
  private boolean clearCookie;
  private FrameLayout customViewLayout;
  private boolean enableBackNavigation;
  private LinearLayout htmlLayout;
  private boolean isAd;
  private boolean launchBrowserForLinks;
  private ProgressDialog mDialog;
  private WebView mWebView;
  private boolean showSkip;
  private boolean supportZoom;
  private final Handler titleHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HtmlAdPage.this.setTitleText(paramAnonymousMessage.obj.toString());
    }
  };
  private boolean useHtmlTitle;

  private static String addTokenToUrl(String paramString)
  {
    try
    {
      if (new URL(paramString).getHost().endsWith("flixster.com"))
      {
        String str = paramString + "#" + FlixsterApplication.addCurrentUserParameters(new StringBuilder()).substring(1);
        paramString = str;
      }
      return paramString;
    }
    catch (MalformedURLException localMalformedURLException)
    {
    }
    return paramString;
  }

  private void setTitleText(String paramString)
  {
    if (!this.isAd)
      setActionBarTitle(paramString);
  }

  @SuppressLint({"SetJavaScriptEnabled"})
  private void setUpWebView(String paramString)
  {
    this.mWebView.setVerticalScrollBarEnabled(false);
    this.mWebView.setHorizontalScrollBarEnabled(false);
    if (FlixsterApplication.getAndroidBuildInt() >= 7)
      this.mWebView.setWebChromeClient(new HtmlAdWebChromeClient(null));
    this.mWebView.setWebViewClient(new HtmlAdWebViewClient(null));
    this.mWebView.getSettings().setJavaScriptEnabled(true);
    this.mWebView.getSettings().setSupportZoom(this.supportZoom);
    this.mWebView.addJavascriptInterface(new FlixsterJavacriptInterface(null), "flixster");
    this.mWebView.loadUrl(paramString);
    this.mWebView.setLayoutParams(LINEAR_LAYOUT_FILL);
    this.mDialog = new ProgressDialog(this);
    this.mDialog.setMessage(getResources().getString(2131493173));
    this.mDialog.setCanceledOnTouchOutside(false);
  }

  protected void onCreate(Bundle paramBundle)
  {
    int i = 1;
    super.onCreate(paramBundle);
    int j;
    String str1;
    String str2;
    boolean bool1;
    label94: boolean bool2;
    label111: boolean bool3;
    label130: boolean bool4;
    label149: boolean bool5;
    label166: boolean bool6;
    if (F.API_LEVEL >= 9)
    {
      j = 7;
      setRequestedOrientation(j);
      instanceCount = 1 + instanceCount;
      Bundle localBundle = getIntent().getExtras();
      str1 = null;
      str2 = null;
      if (localBundle != null)
      {
        str2 = localBundle.getString("KEY_URL");
        str1 = localBundle.getString("KEY_TITLE");
        int k = localBundle.getInt("KEY_FLAGS");
        if (k != 0)
        {
          if ((k & 0x2) != 2)
            break label313;
          bool1 = i;
          this.useHtmlTitle = bool1;
          if ((k & 0x4) != 4)
            break label319;
          bool2 = i;
          this.enableBackNavigation = bool2;
          if ((k & 0x8) != 8)
            break label325;
          bool3 = i;
          this.supportZoom = bool3;
          if ((k & 0x10) != 16)
            break label331;
          bool4 = i;
          this.launchBrowserForLinks = bool4;
          if ((k & 0x1) != i)
            break label337;
          bool5 = i;
          this.clearCookie = bool5;
          if ((k & 0x20) != 32)
            break label343;
          bool6 = i;
          label185: this.isAd = bool6;
          if ((k & 0x40) != 64)
            break label349;
          label201: this.showSkip = i;
        }
      }
      Logger.d("FlxAd", "HtmlAdPage.onCreate " + str2);
      setContentView(2130903094);
      if (!this.isAd)
        break label354;
      hideActionBar();
      label248: this.customViewLayout = ((FrameLayout)findViewById(2131165389));
      this.htmlLayout = ((LinearLayout)findViewById(2131165390));
      this.mWebView = ((WebView)findViewById(2131165391));
      if (!str2.contains("/msk/mobile"))
        break label372;
    }
    while (true)
    {
      setUpWebView(str2);
      return;
      j = i;
      break;
      label313: bool1 = false;
      break label94;
      label319: bool2 = false;
      break label111;
      label325: bool3 = false;
      break label130;
      label331: bool4 = false;
      break label149;
      label337: bool5 = false;
      break label166;
      label343: bool6 = false;
      break label185;
      label349: i = 0;
      break label201;
      label354: createActionBar();
      if (str1 == null)
        break label248;
      setTitleText(str1);
      break label248;
      label372: str2 = addTokenToUrl(str2);
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.showSkip)
      getSupportMenuInflater().inflate(2131689475, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    instanceCount = -1 + instanceCount;
    if (this.mDialog != null);
    try
    {
      this.mDialog.dismiss();
      if ((this.clearCookie) && (instanceCount == 0))
      {
        CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeAllCookie();
        Logger.d("FlxAd", "HtmlAdPage.onDestroy cookies cleared");
      }
      return;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((this.enableBackNavigation) && (paramInt == 4) && (this.mWebView.canGoBack()))
    {
      this.mWebView.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165949:
    }
    MskController.instance().trackSkipFirstLaunchMsk();
    FlixsterApplication.setMskPromptShown();
    finish();
    return true;
  }

  private final class FlixsterJavacriptInterface
  {
    private FlixsterJavacriptInterface()
    {
    }

    public void setPageTitle(String paramString)
    {
      if (paramString != null)
        HtmlAdPage.this.titleHandler.sendMessage(Message.obtain(null, 0, paramString));
    }
  }

  @TargetApi(7)
  private final class HtmlAdWebChromeClient extends WebChromeClient
  {
    private WebChromeClient.CustomViewCallback customCallback;
    private View customView;

    private HtmlAdWebChromeClient()
    {
    }

    public View getVideoLoadingProgressView()
    {
      return new ProgressBar(HtmlAdPage.this);
    }

    public void onHideCustomView()
    {
      Logger.i("FlxAd", "HtmlAdPage.onHideCustomView");
      if (this.customView == null)
        return;
      this.customView.setVisibility(8);
      HtmlAdPage.this.customViewLayout.removeView(this.customView);
      this.customView = null;
      HtmlAdPage.this.customViewLayout.setVisibility(8);
      this.customCallback.onCustomViewHidden();
      this.customCallback = null;
      HtmlAdPage localHtmlAdPage = HtmlAdPage.this;
      if (F.API_LEVEL >= 9);
      for (int i = 7; ; i = 1)
      {
        localHtmlAdPage.setRequestedOrientation(i);
        HtmlAdPage.this.htmlLayout.setVisibility(0);
        return;
      }
    }

    public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
    {
      super.onShowCustomView(paramView, paramCustomViewCallback);
      if ((paramView instanceof FrameLayout))
      {
        FrameLayout localFrameLayout = (FrameLayout)paramView;
        Logger.i("FlxAd", "HtmlAdPage.onShowCustomView " + localFrameLayout.getFocusedChild());
      }
      while (this.customView != null)
      {
        paramCustomViewCallback.onCustomViewHidden();
        this.customView = null;
        return;
        Logger.i("FlxAd", "HtmlAdPage.onShowCustomView");
      }
      this.customView = paramView;
      this.customCallback = paramCustomViewCallback;
      HtmlAdPage.this.customViewLayout.addView(paramView);
      HtmlAdPage.this.customViewLayout.setVisibility(0);
      HtmlAdPage.this.htmlLayout.setVisibility(8);
      HtmlAdPage localHtmlAdPage = HtmlAdPage.this;
      int i = F.API_LEVEL;
      int j = 0;
      if (i >= 9)
        j = 6;
      localHtmlAdPage.setRequestedOrientation(j);
    }
  }

  private final class HtmlAdWebViewClient extends WebViewClient
  {
    private HtmlAdWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      HtmlAdPage.this.mDialog.hide();
      if (paramString.contains("/msk/mobile"))
      {
        if (!paramString.contains("/selection"))
          break label78;
        MskController.instance().trackSelectionPage();
      }
      while (true)
      {
        Logger.d("FlxAd", "HtmlAdPage.onPageFinished " + paramString);
        if (HtmlAdPage.this.useHtmlTitle)
          paramWebView.loadUrl("javascript:window.flixster.setPageTitle(document.title);");
        return;
        label78: if (paramString.contains("/invite"))
          MskController.instance().trackRewardConfirmationPage();
      }
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      super.onPageStarted(paramWebView, paramString, paramBitmap);
      try
      {
        HtmlAdPage.this.mDialog.show();
        Logger.d("FlxAd", "HtmlAdPage.onPageStarted " + paramString);
        return;
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
      }
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      boolean bool1;
      if (DeepLink.isValid(paramString))
      {
        if (DeepLink.launch(paramString, HtmlAdPage.this))
          HtmlAdPage.this.finish();
        bool1 = true;
      }
      while (true)
      {
        Logger.d("FlxAd", "HtmlAdPage.shouldOverrideUrlLoading " + bool1 + " " + paramString);
        return bool1;
        if (HtmlAdPage.this.launchBrowserForLinks)
        {
          Starter.launchBrowser(paramString, HtmlAdPage.this);
          bool1 = true;
        }
        else if (!ApiBuilder.isTrailerUrl(paramString))
        {
          boolean bool2 = ApiBuilder.isVideoLink(paramString);
          bool1 = false;
          if (!bool2);
        }
        else
        {
          Starter.launchVideo(paramString, HtmlAdPage.this, true);
          bool1 = true;
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.HtmlAdPage
 * JD-Core Version:    0.6.2
 */