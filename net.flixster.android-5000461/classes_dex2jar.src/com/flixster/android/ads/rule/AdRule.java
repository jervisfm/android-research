package com.flixster.android.ads.rule;

public abstract interface AdRule
{
  public abstract void adShown();

  public abstract void reset();

  public abstract boolean shouldShowAd();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.rule.AdRule
 * JD-Core Version:    0.6.2
 */