package com.flixster.android.ads.rule;

public class IntervalCapRule
  implements AdRule
{
  private int counter;
  private final int interval;

  public IntervalCapRule(int paramInt)
  {
    this.interval = paramInt;
  }

  public void adShown()
  {
  }

  public void reset()
  {
    this.counter = 0;
  }

  public boolean shouldShowAd()
  {
    int i = this.counter;
    this.counter = (i + 1);
    return i % this.interval == 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.rule.IntervalCapRule
 * JD-Core Version:    0.6.2
 */