package com.flixster.android.ads.rule;

public class LaunchRule
  implements AdRule
{
  private boolean shown;

  public void adShown()
  {
    this.shown = true;
  }

  public void reset()
  {
    this.shown = false;
  }

  public boolean shouldShowAd()
  {
    return !this.shown;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.rule.LaunchRule
 * JD-Core Version:    0.6.2
 */