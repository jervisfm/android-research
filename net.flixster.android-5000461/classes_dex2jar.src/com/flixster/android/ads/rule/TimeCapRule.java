package com.flixster.android.ads.rule;

import java.util.Date;
import net.flixster.android.FlixsterApplication;

public class TimeCapRule
  implements AdRule
{
  private final long intervalMs;
  private long lastShownTimeMs;

  public TimeCapRule(long paramLong)
  {
    this.intervalMs = paramLong;
  }

  public void adShown()
  {
    this.lastShownTimeMs = FlixsterApplication.sToday.getTime();
  }

  public void reset()
  {
    this.lastShownTimeMs = 0L;
  }

  public boolean shouldShowAd()
  {
    return FlixsterApplication.sToday.getTime() - this.lastShownTimeMs > this.intervalMs;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.ads.rule.TimeCapRule
 * JD-Core Version:    0.6.2
 */