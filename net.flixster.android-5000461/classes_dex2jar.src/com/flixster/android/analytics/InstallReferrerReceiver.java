package com.flixster.android.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.UrlHelper;
import java.util.Map;
import net.flixster.android.FlixsterApplication;

public class InstallReferrerReceiver extends BroadcastReceiver
{
  public static void onReceive(Intent paramIntent)
  {
    String str1 = paramIntent.getExtras().getString("referrer");
    Logger.fd("FlxMain", "InstallReferrerReceiver.onReceive referrer: " + str1);
    boolean bool = FlixsterApplication.isSharedPrefInitialized();
    String str2 = null;
    if (bool)
    {
      str2 = FlixsterApplication.getReferrer();
      if (str2 == null)
      {
        Map localMap = UrlHelper.getQueries(str1);
        String str3 = UrlHelper.getSingleQueryValue(localMap, "utm_source");
        String str4 = UrlHelper.getSingleQueryValue(localMap, "utm_medium");
        String str5 = UrlHelper.getSingleQueryValue(localMap, "utm_campaign");
        String str6 = UrlHelper.getSingleQueryValue(localMap, "term");
        FlixsterApplication.setReferrer(str1);
        Trackers.instance().trackInstall(str3, str4, str5, str6);
      }
    }
    StringBuilder localStringBuilder = new StringBuilder("InstallReferrerReceiver.onReceive existingReferrer: ");
    if ((str2 != null) && (str2.equals(str1)))
      str2 = "the same";
    Logger.fd("FlxMain", str2);
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    onReceive(paramIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.analytics.InstallReferrerReceiver
 * JD-Core Version:    0.6.2
 */