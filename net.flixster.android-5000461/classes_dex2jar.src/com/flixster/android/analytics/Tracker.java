package com.flixster.android.analytics;

import android.content.Context;

public abstract interface Tracker
{
  public abstract void start(Context paramContext);

  public abstract void stop();

  public abstract void track(String paramString1, String paramString2);

  public abstract void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4);

  public abstract void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt);

  public abstract void trackInstall(String paramString1, String paramString2, String paramString3, String paramString4);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.analytics.Tracker
 * JD-Core Version:    0.6.2
 */