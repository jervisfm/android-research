package com.flixster.android.analytics;

import android.content.Context;
import android.os.Build;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.MathHelper;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.StringHelper;
import com.flixster.android.utils.UrlHelper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import net.flixster.android.FlixsterApplication;

public class Trackers
  implements Tracker
{
  private static final Trackers INSTANCE = new Trackers();
  private final Tracker gaCustomTracker = new GaCustomTracker(null);
  private final Tracker graphiteCustomTracker = new GraphiteCustomTracker(null);

  public static Tracker instance()
  {
    return INSTANCE;
  }

  public void start(Context paramContext)
  {
  }

  public void stop()
  {
  }

  public void track(String paramString1, String paramString2)
  {
    this.gaCustomTracker.track(paramString1, paramString2);
    if (paramString1.contains("/msk"))
      this.graphiteCustomTracker.track(paramString1, paramString2);
  }

  public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.gaCustomTracker.trackEvent(paramString1, paramString2, paramString3, paramString4);
  }

  public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
  {
    this.gaCustomTracker.trackEvent(paramString1, paramString2, paramString3, paramString4, paramString5, paramInt);
    if (paramString1.contains("/msk"))
      this.graphiteCustomTracker.trackEvent(paramString1, paramString2, paramString3, paramString4, paramString5, paramInt);
  }

  public void trackInstall(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.gaCustomTracker.trackInstall(paramString1, paramString2, paramString3, paramString4);
  }

  private static class GaCustomTracker
    implements Tracker
  {
    private static final String GA_DEV_ACCOUNT = "UA-23007375-1";
    private static final String GA_GTV_ACCOUNT = "UA-19205027-1";
    private static final String GA_HONEYCOMB_ACCOUNT = "UA-22157832-1";
    private static final String GA_PHONE_ACCOUNT = "UA-8284864-1";
    private static final String GA_REFERRAL_ACCOUNT = "UA-21839535-1";
    private final String account;

    private GaCustomTracker()
    {
      String str1 = Build.MODEL.toLowerCase();
      int i;
      boolean bool;
      String str2;
      if ((!str1.contains("kindle")) && (!str1.startsWith("nook")) && (!str1.contains("nexus 7")))
      {
        i = 0;
        bool = Properties.instance().isHoneycombTablet();
        if (!Properties.instance().isGoogleTv())
          break label72;
        str2 = "UA-19205027-1";
      }
      while (true)
      {
        this.account = str2;
        return;
        i = 1;
        break;
        label72: if ((bool) || (i != 0))
          str2 = "UA-22157832-1";
        else
          str2 = "UA-8284864-1";
      }
    }

    private static String createEventTrackingParams(String paramString1, String paramString2, String paramString3, int paramInt)
    {
      if ((paramString1 == null) || (paramString2 == null))
        return "";
      StringBuilder localStringBuilder = new StringBuilder("&utmt=event&utme=5");
      localStringBuilder.append("(").append(paramString1).append("*").append(paramString2);
      if (paramString3 == null)
        localStringBuilder.append(")");
      while (true)
      {
        return localStringBuilder.toString();
        localStringBuilder.append("*").append(StringHelper.removeParentheses(StringHelper.replaceSpace(paramString3))).append(")");
        localStringBuilder.append("(").append(paramInt).append(")");
      }
    }

    private static String createGaUrl(String paramString1, String paramString2, String paramString3)
    {
      return createGaUrl(paramString1, paramString2, null, null, null, 0, null, null, null, null, paramString3);
    }

    private static String createGaUrl(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
    {
      return createGaUrl(paramString1, paramString2, paramString3, paramString4, null, 0, null, null, null, null, paramString5);
    }

    private static String createGaUrl(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt, String paramString6)
    {
      return createGaUrl(paramString1, paramString2, paramString3, paramString4, paramString5, paramInt, null, null, null, null, paramString6);
    }

    private static String createGaUrl(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10)
    {
      if (paramString1 == null)
        paramString1 = "null";
      if (paramString2 == null)
        paramString2 = "null";
      try
      {
        String str6 = URLEncoder.encode(Build.MODEL, "UTF-8");
        String str7 = URLEncoder.encode(paramString2, "UTF-8");
        String str8 = URLEncoder.encode(paramString1, "UTF-8");
        String str9 = createReferrerParams(paramString6, paramString7, paramString8, paramString9);
        String str10 = "__utma%3D239913861." + FlixsterApplication.getHashedUid() + "." + FlixsterApplication.getGaFirstSession() + "." + FlixsterApplication.getGaLastSession() + "." + FlixsterApplication.getGaThisSession() + "." + FlixsterApplication.getGaSessionCount() + "%3B%2B__utmz%3D" + "239913861" + "." + FlixsterApplication.getGaThisSession() + "." + FlixsterApplication.getGaSessionCount() + str9;
        String str11 = URLEncoder.encode(FlixsterApplication.getNetworkType(), "UTF-8");
        StringBuilder localStringBuilder5 = new StringBuilder("http://www.google-analytics.com/__utm.gif?utmwv=4.3");
        localStringBuilder5.append("&utmn=").append(MathHelper.randomId());
        localStringBuilder5.append("&utmhn=www.flixster.com");
        localStringBuilder5.append(createEventTrackingParams(paramString3, paramString4, paramString5, paramInt));
        localStringBuilder5.append("&utmcs=ISO-8859-1&utmsr=").append(FlixsterApplication.getCurrentDimensions());
        localStringBuilder5.append("&utmsc=").append(str11);
        localStringBuilder5.append("&utmul=en-us&utmje=0&utmfl=").append(str6);
        localStringBuilder5.append("&utmdt=").append(str7);
        localStringBuilder5.append("&utmhid=").append(MathHelper.randomId());
        localStringBuilder5.append("&utmr=0&utmp=").append(str8);
        localStringBuilder5.append("&utmac=").append(paramString10);
        localStringBuilder5.append("&utmcc=").append(str10);
        String str12 = localStringBuilder5.toString();
        str1 = str12;
        StringBuilder localStringBuilder1 = new StringBuilder("tag:\"").append(paramString1).append("\" title:\"").append(paramString2).append("\"");
        if (paramString3 != null)
        {
          str2 = " category:\"" + paramString3 + "\" action:\"" + paramString4 + "\"";
          StringBuilder localStringBuilder2 = localStringBuilder1.append(str2);
          if (paramString5 == null)
            break label587;
          str3 = " label:\"" + paramString5 + "\"";
          StringBuilder localStringBuilder3 = localStringBuilder2.append(str3);
          if (paramString5 == null)
            break label594;
          str4 = " value:\"" + paramInt + "\"";
          StringBuilder localStringBuilder4 = localStringBuilder3.append(str4);
          if (paramString6 == null)
            break label601;
          str5 = " source:\"" + paramString6 + "\" medium:\"" + paramString7 + "\" campaign:\"" + paramString8 + "\" keywords:\"" + paramString9 + "\"";
          Logger.d("FlxGa", str5);
          Logger.v("FlxGa", str1);
          return str1;
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        while (true)
        {
          Logger.e("FlxGa", "GaCustomTracker.createGaUrl", localUnsupportedEncodingException);
          String str1 = null;
          continue;
          String str2 = "";
          continue;
          label587: String str3 = "";
          continue;
          label594: String str4 = "";
          continue;
          label601: String str5 = "";
        }
      }
    }

    private static String createReferrerParams(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      if ((paramString1 == null) && (paramString2 == null) && (paramString3 == null) && (paramString4 == null))
        return ".1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)";
      StringBuilder localStringBuilder1 = new StringBuilder(".1.");
      StringBuilder localStringBuilder2 = localStringBuilder1.append("utmcsr=");
      String str1;
      String str2;
      label77: StringBuilder localStringBuilder4;
      if (paramString1 == null)
      {
        str1 = "null";
        localStringBuilder2.append(str1).append("|");
        StringBuilder localStringBuilder3 = localStringBuilder1.append("utmccn=");
        if (paramString3 != null)
          break label187;
        str2 = "null";
        localStringBuilder3.append(str2).append("|");
        localStringBuilder4 = localStringBuilder1.append("utmcmd=");
        if (paramString2 != null)
          break label196;
      }
      label187: label196: for (String str3 = "null"; ; str3 = StringHelper.replaceNonAlphaNumeric(paramString2))
      {
        localStringBuilder4.append(str3).append("|");
        if (paramString4 != null)
          localStringBuilder1.append("utmctr=").append(StringHelper.replaceNonAlphaNumeric(paramString4));
        if (localStringBuilder1.charAt(-1 + localStringBuilder1.length()) == '|')
          localStringBuilder1.deleteCharAt(-1 + localStringBuilder1.length());
        return UrlHelper.urlEncode(localStringBuilder1.toString());
        str1 = StringHelper.replaceNonAlphaNumeric(paramString1);
        break;
        str2 = StringHelper.replaceNonAlphaNumeric(paramString3);
        break label77;
      }
    }

    public void start(Context paramContext)
    {
    }

    public void stop()
    {
    }

    public void track(String paramString1, String paramString2)
    {
      String str = createGaUrl(paramString1, paramString2, this.account);
      if (str != null)
        ImageGetter.instance().get(str, null);
    }

    public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      String str = createGaUrl(paramString1, paramString2, paramString3, paramString4, this.account);
      if (str != null)
        ImageGetter.instance().get(str, null);
    }

    public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
    {
      String str = createGaUrl(paramString1, paramString2, paramString3, paramString4, paramString5, paramInt, this.account);
      if (str != null)
        ImageGetter.instance().get(str, null);
    }

    public void trackInstall(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      String str = createGaUrl("/install", "Install", null, null, null, 0, paramString1, paramString2, paramString3, paramString4, "UA-21839535-1");
      if (str != null)
        ImageGetter.instance().get(str, null);
    }
  }

  private static class GraphiteCustomTracker
    implements Tracker
  {
    private static String createGraphiteUrl(String paramString1, String paramString2)
    {
      StringBuilder localStringBuilder = new StringBuilder("http://www.flixster.com/msk/ga/?value=Android");
      localStringBuilder.append(",").append(paramString1);
      localStringBuilder.append(",").append(paramString2);
      return localStringBuilder.toString();
    }

    private static String createGraphiteUrl(String paramString1, String paramString2, String paramString3, int paramInt)
    {
      StringBuilder localStringBuilder = new StringBuilder("http://www.flixster.com/msk/ga/?value=Android");
      localStringBuilder.append(",").append(paramString1);
      localStringBuilder.append(",").append(paramString2);
      localStringBuilder.append(",").append(paramString3);
      localStringBuilder.append(",").append(paramInt);
      return localStringBuilder.toString();
    }

    public void start(Context paramContext)
    {
    }

    public void stop()
    {
    }

    public void track(String paramString1, String paramString2)
    {
      int i = 1 + paramString1.indexOf("/var");
      int j = paramString1.indexOf("/", i);
      String str = createGraphiteUrl(paramString1.substring(i, j), paramString1.substring(j).replace("/", ""));
      ImageGetter.instance().get(str, null);
    }

    public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4)
    {
    }

    public void trackEvent(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
    {
      int i = 1 + paramString1.indexOf("/var");
      String str = createGraphiteUrl(paramString1.substring(i, paramString1.indexOf("/", i)), paramString4, paramString5.replace("/", ""), paramInt);
      ImageGetter.instance().get(str, null);
    }

    public void trackInstall(String paramString1, String paramString2, String paramString3, String paramString4)
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.analytics.Trackers
 * JD-Core Version:    0.6.2
 */