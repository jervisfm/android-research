package com.flixster.android.bootstrap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.comscore.analytics.Census;
import com.flixster.android.ads.AdsArbiter;
import com.flixster.android.ads.AdsArbiter.LaunchAdsArbiter;
import com.flixster.android.ads.AdsArbiter.TrailerAdsArbiter;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.AppRater;
import com.flixster.android.utils.FirstLaunchMskHelper;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import com.flixster.android.widget.WidgetProvider;
import net.flixster.android.Flixster;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.Starter;
import net.flixster.android.ads.AdManager;
import net.flixster.android.data.MiscDao;
import net.flixster.android.model.Property;

public class BootstrapActivity extends Activity
{
  private final Handler fetchPropertiesSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
      if ((!Properties.instance().isHoneycombTablet()) && (!Properties.instance().isGoogleTv()) && (Properties.instance().getProperties().isMskFirstLaunchEnabled))
      {
        ObjectHolder.instance().put("lasp", Boolean.valueOf(true));
        Starter.launchFirstMsk(localActivity);
      }
    }
  };

  private static Class<?> getGoogleTvTopLevelActivity()
  {
    try
    {
      Class localClass = Class.forName("com.flixster.android.activity.gtv.Main");
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new RuntimeException(localClassNotFoundException);
    }
  }

  private static Class<?> getHoneycombTopLevelActivity()
  {
    try
    {
      Class localClass = Class.forName("com.flixster.android.activity.hc.Main");
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new RuntimeException(localClassNotFoundException);
    }
  }

  public static Intent getMainIntent(Context paramContext)
  {
    boolean bool = Properties.instance().isHoneycombTablet();
    Class localClass;
    if (Properties.instance().isGoogleTv())
      localClass = getGoogleTvTopLevelActivity();
    while (true)
    {
      return new Intent(paramContext, localClass);
      if (bool)
        localClass = getHoneycombTopLevelActivity();
      else
        localClass = getPhoneTopLevelActivity();
    }
  }

  private static Class<?> getPhoneTopLevelActivity()
  {
    return Flixster.class;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxMain", "BootstrapActivity.onCreate");
    if (Flixster.getInstanceCount() == 0)
    {
      FlixsterApplication.setupLocationServices();
      AdsArbiter.launch().onAppStart();
      AdsArbiter.trailer().onAppStart();
      Startup.instance().reset();
      Properties.instance().reset();
      AdManager.instance();
      AppRater.appLaunched(this);
      FirstLaunchMskHelper.appLaunched();
      try
      {
        Census.getInstance().notifyStart(getApplicationContext(), "3002201", "cf451e1a3ba37e25b3bc99931721b0ea");
        MiscDao.fetchProperties(this.fetchPropertiesSuccessHandler, null);
        return;
      }
      catch (Exception localException)
      {
        while (true)
          Logger.e("FlxMain", "BootstrapActivity.onCreate", localException);
      }
    }
    Logger.w("FlxMain", "BootstrapActivity.onCreate existing Flixster instance detected");
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxMain", "BootstrapActivity.onDestroy");
  }

  protected void onResume()
  {
    super.onResume();
    Logger.d("FlxMain", "BootstrapActivity.onResume");
    boolean bool1 = Properties.instance().isHoneycombTablet();
    boolean bool2 = Properties.instance().isGoogleTv();
    StringBuilder localStringBuilder = new StringBuilder("Bootstrapping ");
    String str;
    if (bool1)
      str = "Honeycomb tablet version";
    while (true)
    {
      Logger.i("FlxMain", str);
      Intent localIntent1 = getMainIntent(this);
      Intent localIntent2 = getIntent();
      localIntent1.setData(localIntent2.getData());
      localIntent2.setData(null);
      WidgetProvider.propagateIntentExtras(localIntent2, localIntent1);
      startActivity(localIntent1);
      finish();
      return;
      if (bool2)
        str = "Google TV version";
      else
        str = "phone version";
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.bootstrap.BootstrapActivity
 * JD-Core Version:    0.6.2
 */