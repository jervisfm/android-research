package com.flixster.android.bootstrap;

import android.app.Activity;
import com.facebook.android.Facebook;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.MiscDao;

public class Startup
{
  private static final Startup INSTANCE = new Startup();
  private boolean isFinished;

  private static void extendFbAccessToken(Activity paramActivity)
  {
    if (AccountManager.instance().isPlatformFacebook())
    {
      Logger.d("FlxMain", "Startup.extendFbAccessToken");
      FlixsterApplication.sFacebook.extendAccessTokenIfNeeded(paramActivity, null);
    }
  }

  public static Startup instance()
  {
    return INSTANCE;
  }

  private static void onGtvStartup(Activity paramActivity)
  {
    Logger.d("FlxMain", "Startup.onGtvStartup");
    refreshUserSession();
    registerDevice();
    extendFbAccessToken(paramActivity);
  }

  private static void onHcTabletStartup(Activity paramActivity)
  {
    Logger.d("FlxMain", "Startup.onHcTabletStartup");
    refreshUserSession();
    registerDevice();
    extendFbAccessToken(paramActivity);
  }

  private static void onPhoneStartup(Activity paramActivity)
  {
    Logger.d("FlxMain", "Startup.onPhoneStartup");
    refreshUserSession();
    registerDevice();
    extendFbAccessToken(paramActivity);
  }

  private static void refreshUserSession()
  {
    Logger.d("FlxMain", "Startup.refreshUserSession");
    AccountManager.instance().refreshUserSession();
  }

  private static void registerDevice()
  {
    MiscDao.registerDevice();
  }

  public void onStartup(Activity paramActivity)
  {
    if (this.isFinished)
      return;
    this.isFinished = true;
    boolean bool1 = Properties.instance().isGoogleTv();
    boolean bool2 = Properties.instance().isHoneycombTablet();
    if (bool1)
    {
      onGtvStartup(paramActivity);
      return;
    }
    if (bool2)
    {
      onHcTabletStartup(paramActivity);
      return;
    }
    onPhoneStartup(paramActivity);
  }

  public void reset()
  {
    this.isFinished = false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.bootstrap.Startup
 * JD-Core Version:    0.6.2
 */