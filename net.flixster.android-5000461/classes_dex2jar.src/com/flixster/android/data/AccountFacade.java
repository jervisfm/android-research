package com.flixster.android.data;

import android.os.Handler;
import com.flixster.android.utils.Logger;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;

public class AccountFacade
{
  public static String fetchSocialUserId(Handler paramHandler1, Handler paramHandler2)
  {
    String str = getSocialUserId();
    if ((str == null) && (AccountManager.instance().hasUserCredential()))
    {
      Logger.i("FlxMain", "AccountFacade.fetchSocialUserId fetch required");
      ProfileDao.fetchUser(paramHandler1, paramHandler2);
    }
    return str;
  }

  public static String getNetflixUserId()
  {
    return FlixsterApplication.getNetflixUserId();
  }

  public static User getSocialUser()
  {
    return AccountManager.instance().getUser();
  }

  public static String getSocialUserId()
  {
    User localUser = getSocialUser();
    if (localUser == null)
      return null;
    return localUser.id;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.AccountFacade
 * JD-Core Version:    0.6.2
 */