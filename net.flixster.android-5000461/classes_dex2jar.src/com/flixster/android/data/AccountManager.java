package com.flixster.android.data;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.Facebook;
import com.facebook.android.SessionEvents;
import com.facebook.android.SessionEvents.AuthListener;
import com.facebook.android.SessionEvents.LogoutListener;
import com.facebook.android.SessionStore;
import com.facebook.android.Util;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.WorkerThreads;
import java.util.Date;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.User;

public class AccountManager
{
  private static final AccountManager INSTANCE = new AccountManager();
  public static final String PLATFORM_FBK = "FBK";
  public static final String PLATFORM_FLX = "FLX";
  private boolean skipRefreshUserSession;

  private static void clearCookies()
  {
    Logger.d("FlxMain", "AccountManager.clearCookies clearing cookies");
    try
    {
      Util.clearCookies(FlixsterApplication.getContext());
      return;
    }
    catch (Exception localException)
    {
      Logger.e("FlxMain", "AccountManager.clearCookies", localException);
    }
  }

  public static AccountManager instance()
  {
    return INSTANCE;
  }

  private static void logFbCredential(Facebook paramFacebook)
  {
    boolean bool = paramFacebook.isSessionValid();
    long l = paramFacebook.getAccessExpires();
    if (bool)
    {
      StringBuilder localStringBuilder1 = new StringBuilder("AccountManager.logFbCredential valid expires ");
      if (l == 0L);
      for (Object localObject1 = "0"; ; localObject1 = new Date(l))
      {
        Logger.i("FlxMain", localObject1);
        Logger.si("FlxMain", "AccountManager.logFbCredential fb username: " + FlixsterApplication.getFacebookUsername());
        Logger.si("FlxMain", "AccountManager.logFbCredential flx id: " + FlixsterApplication.getFlixsterId());
        Logger.si("FlxMain", "AccountManager.logFbCredential fb id: " + FlixsterApplication.getFacebookId());
        Logger.si("FlxMain", "AccountManager.logFbCredential fb token: " + paramFacebook.getAccessToken());
        return;
      }
    }
    StringBuilder localStringBuilder2 = new StringBuilder("AccountManager.logFbSession invalid expires ");
    if (l == 0L);
    for (Object localObject2 = "0"; ; localObject2 = new Date(l))
    {
      Logger.w("FlxMain", localObject2);
      break;
    }
  }

  private static void logFlxCredential()
  {
    Logger.si("FlxMain", "AccountManager.logFlxCredential flx username: " + FlixsterApplication.getFlixsterUsername());
    Logger.si("FlxMain", "AccountManager.logFlxCredential flx id: " + FlixsterApplication.getFlixsterId());
    Logger.si("FlxMain", "AccountManager.logFlxCredential flx token: " + FlixsterApplication.getFlixsterSessionKey());
  }

  private void onFbLogout()
  {
    clearUserCredential();
    ProfileDao.resetUser();
    FlixsterApplication.setMigratedToRights(false);
  }

  public void clearUserCredential()
  {
    FlixsterApplication.setFlixsterId(null);
    SessionStore.clear(FlixsterApplication.getContext());
    FlixsterApplication.setFacebookUsername(null);
    FlixsterApplication.setFacebookId(null);
    FlixsterApplication.setFlixsterSessionKey(null);
    FlixsterApplication.setFlixsterUsername(null);
  }

  public void createNewUserWithFlxCredential(String paramString1, String paramString2)
  {
    ProfileDao.resetUser();
    ProfileDao.createNewUser();
    FlixsterApplication.setPlatform("FLX");
    FlixsterApplication.setFlixsterSessionKey(paramString2);
    FlixsterApplication.setFlixsterId(paramString1);
    FlixsterApplication.setFlixsterUsername("Flixster user");
  }

  public void fbLogout(Activity paramActivity)
  {
    Logger.i("FlxMain", "AccountManager.fbLogout");
    onFbLogout();
    Facebook localFacebook = FlixsterApplication.sFacebook;
    if (localFacebook.isSessionValid())
    {
      SessionEvents.onLogoutBegin();
      new AsyncFacebookRunner(localFacebook).logout(FlixsterApplication.getContext(), new LogoutRequestListener(paramActivity, null));
    }
  }

  public User getUser()
  {
    return ProfileDao.getUser();
  }

  public boolean hasFacebookUserSession()
  {
    return (hasUserSession()) && (isPlatformFacebook());
  }

  public boolean hasUserCredential()
  {
    String str = FlixsterApplication.getPlatform();
    if ("FBK".equals(str))
      if ((FlixsterApplication.getFlixsterId() == null) || (FlixsterApplication.getFacebookId() == null) || (FlixsterApplication.sFacebook.getAccessToken() == null));
    do
    {
      return true;
      return false;
      if (!"FLX".equals(str))
        break;
    }
    while ((FlixsterApplication.getFlixsterId() != null) && (FlixsterApplication.getFlixsterSessionKey() != null));
    return false;
    return false;
  }

  public boolean hasUserSession()
  {
    return ProfileDao.getUser() != null;
  }

  public boolean isPlatformFacebook()
  {
    return "FBK".equals(FlixsterApplication.getPlatform());
  }

  public boolean isPlatformFlixster()
  {
    return "FLX".equals(FlixsterApplication.getPlatform());
  }

  public void logUserCredential()
  {
    String str = FlixsterApplication.getPlatform();
    if ("FBK".equals(str))
      logFbCredential(FlixsterApplication.sFacebook);
    while (!"FLX".equals(str))
      return;
    logFlxCredential();
  }

  public void loginToFacebook(String paramString1, String paramString2, Handler paramHandler1, Handler paramHandler2)
  {
  }

  public void loginToFlixster(final String paramString1, final String paramString2, final Handler paramHandler1, final Handler paramHandler2)
  {
    if (!FlixsterApplication.isConnected())
    {
      paramHandler2.handleMessage(Message.obtain(null, 0, DaoException.create(DaoException.Type.NETWORK, "The device has no connection to the Internet. Please verify the settings.")));
      return;
    }
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          if (ProfileDao.loginUser(paramString1, paramString2, "FLX") != null)
          {
            ProfileDao.fetchUser();
            paramHandler1.sendEmptyMessage(0);
            return;
          }
          DaoException localDaoException2 = DaoException.create(DaoException.Type.USER_ACCT, "The email or password you entered is invalid.");
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException2));
          return;
        }
        catch (DaoException localDaoException1)
        {
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException1));
        }
      }
    });
  }

  public void onFbLogin(Facebook paramFacebook, String paramString)
  {
    SessionStore.save(paramFacebook, FlixsterApplication.getContext());
    FlixsterApplication.setPlatform("FBK");
    FlixsterApplication.setFacebookId(paramString);
  }

  public void onFbLogin(String paramString1, String paramString2)
  {
    FlixsterApplication.setFacebookUsername(paramString1);
    FlixsterApplication.setFlixsterId(paramString2);
    logFbCredential(FlixsterApplication.sFacebook);
    System.currentTimeMillis();
  }

  public void onFlxLogin()
  {
  }

  public void onFlxLogout()
  {
    clearUserCredential();
    ProfileDao.resetUser();
    FlixsterApplication.setMigratedToRights(false);
    clearCookies();
  }

  public void refreshUserSession()
  {
    if (!this.skipRefreshUserSession)
    {
      Logger.i("FlxMain", "AccountManager.refreshUserSession necessary");
      ProfileDao.resetUser();
      restoreUserSession();
      return;
    }
    Logger.i("FlxMain", "AccountManager.refreshUserSession not necessary");
    this.skipRefreshUserSession = false;
  }

  public void restoreFbSession()
  {
  }

  public void restoreUserSession()
  {
    if (!hasUserCredential())
    {
      Logger.i("FlxMain", "FlixsterApplication.restoreUserSession credential unavailable");
      return;
    }
    System.currentTimeMillis();
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          User localUser2 = ProfileDao.fetchUser();
          localUser1 = localUser2;
          if (localUser1 != null)
          {
            if ("FBK".equals(FlixsterApplication.getPlatform()))
            {
              Trackers.instance().track("/facebook/login/restore", "Facebook Session Restore");
              Logger.i("FlxMain", "AccountManager.restoreUserSession successful");
            }
          }
          else
            return;
        }
        catch (DaoException localDaoException)
        {
          while (true)
          {
            Logger.w("FlxMain", "AccountManager.restoreUserSession failed", localDaoException);
            DaoException.Type localType1 = DaoException.Type.USER_ACCT;
            DaoException.Type localType2 = localDaoException.getType();
            User localUser1 = null;
            if (localType1 == localType2)
            {
              Properties.instance().setUserSessionExpired();
              AccountManager.this.clearUserCredential();
              localUser1 = null;
              continue;
              if ("FLX".equals(FlixsterApplication.getPlatform()))
                Trackers.instance().track("/flixster/login/restore", "Flixster Session Restore");
            }
          }
        }
      }
    });
  }

  public void setSkipRefreshUserSession()
  {
    this.skipRefreshUserSession = true;
  }

  public static class FbAuthListener
    implements SessionEvents.AuthListener
  {
    public void onAuthFail(String paramString)
    {
      Logger.d("FlxMain", "AccountManager.FbAuthListener.onAuthFail " + paramString);
    }

    public void onAuthSucceed()
    {
      Logger.d("FlxMain", "AccountManager.FbAuthListener.onAuthSucceed");
    }
  }

  public static class FbLogoutListener
    implements SessionEvents.LogoutListener
  {
    public void onLogoutBegin()
    {
      Logger.d("FlxMain", "AccountManager.FbLogoutListener.onLogoutBegin");
    }

    public void onLogoutFinish()
    {
      Logger.d("FlxMain", "AccountManager.FbLogoutListener.onLogoutFinish");
    }
  }

  private static class LogoutRequestListener extends BaseRequestListener
  {
    private final Activity activity;

    private LogoutRequestListener(Activity paramActivity)
    {
      this.activity = paramActivity;
    }

    public void onComplete(String paramString, Object paramObject)
    {
      if ((this.activity == null) || (this.activity.isFinishing()))
        return;
      this.activity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          SessionEvents.onLogoutFinish();
        }
      });
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.AccountManager
 * JD-Core Version:    0.6.2
 */