package com.flixster.android.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.facebook.android.Facebook;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ProfileDao;

public class AccountSessionManager
{
  private static final String FLX_PREFS_FILE = "FlixsterPrefs";
  private static final AccountSessionManager INSTANCE = new AccountSessionManager();
  private static final String PREFS_FB_EXPIRES = "PREFS_FB_EXPIRES";
  private static final String PREFS_FB_SESSIONKEY = "PREFS_FB_SESSIONKEY";
  private static final String PREFS_FB_USERID = "PREFS_FB_USERID";
  private static final String PREFS_FB_USERNAME = "PREFS_FB_USERNAME";
  private static final String PREFS_FLIXSTER_ID = "PREFS_FLIXSTER_ID";
  private static final String PREFS_FLIXSTER_SESSION_KEY = "PREFS_FLIXSTER_SESSION_KEY";
  private static final String PREFS_FLIXSTER_USERNAME = "PREFS_FLIXSTER_USERNAME";
  private static final String PREFS_PLATFORM = "PREFS_PLATFORM";
  private Facebook fb;
  private String fbAccessToken;
  private long fbExpires;
  private String fbId;
  private String fbUsername;
  private String flxId;
  private String flxSessionKey;
  private String flxUsername;
  private Platform platform;

  private void clearFbSession(Context paramContext)
  {
    this.fbAccessToken = null;
    this.fbExpires = 0L;
    this.fbUsername = null;
    this.fbId = null;
    persistFbSession(paramContext);
    this.fb = null;
  }

  private void clearFlxSession(Context paramContext)
  {
    this.flxUsername = null;
    this.flxSessionKey = null;
    this.flxId = null;
    persistFlxSession(paramContext);
  }

  private void clearSession(Context paramContext)
  {
    clearFbSession(paramContext);
    clearFlxSession(paramContext);
    ProfileDao.resetUser();
  }

  public static AccountSessionManager instance()
  {
    return INSTANCE;
  }

  private void persistFbSession(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("FlixsterPrefs", 0).edit();
    localEditor.putString("PREFS_FB_USERNAME", this.fbUsername);
    localEditor.putString("PREFS_FB_SESSIONKEY", this.fbAccessToken);
    localEditor.putString("PREFS_FB_USERID", this.fbId);
    localEditor.putLong("PREFS_FB_EXPIRES", this.fbExpires);
    localEditor.commit();
  }

  private void persistFlxSession(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("FlixsterPrefs", 0).edit();
    localEditor.putString("PREFS_FLIXSTER_USERNAME", this.flxUsername);
    localEditor.putString("PREFS_FLIXSTER_SESSION_KEY", this.flxSessionKey);
    localEditor.putString("PREFS_FLIXSTER_ID", this.flxId);
    localEditor.commit();
  }

  public boolean isSessionValid()
  {
    if (Platform.FBK == this.platform)
      if ((this.fb == null) || (!this.fb.isSessionValid()));
    do
    {
      return true;
      return false;
      if (Platform.FLX != this.platform)
        break;
    }
    while (this.flxSessionKey != null);
    return false;
    return false;
  }

  public void persistSession(Context paramContext)
  {
  }

  public void restoreSession(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("FlixsterPrefs", 0);
    try
    {
      this.platform = Platform.valueOf(localSharedPreferences.getString("PREFS_PLATFORM", Platform.FBK.name()));
      this.fbUsername = localSharedPreferences.getString("PREFS_FB_USERNAME", null);
      this.fbId = localSharedPreferences.getString("PREFS_FB_USERID", null);
      this.fbAccessToken = localSharedPreferences.getString("PREFS_FB_SESSIONKEY", null);
      this.fbExpires = localSharedPreferences.getLong("PREFS_FB_EXPIRES", 0L);
      this.flxUsername = localSharedPreferences.getString("PREFS_FLIXSTER_USERNAME", null);
      this.flxId = localSharedPreferences.getString("PREFS_FLIXSTER_ID", null);
      this.flxSessionKey = localSharedPreferences.getString("PREFS_FLIXSTER_SESSION_KEY", null);
      this.fb = new Facebook(FlixsterApplication.getFbAppId());
      this.fb.setAccessToken(this.fbAccessToken);
      this.fb.setAccessExpires(this.fbExpires);
      if (!isSessionValid())
        clearSession(paramContext);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        this.platform = null;
    }
  }

  public void saveSession(Platform paramPlatform)
  {
    this.platform = paramPlatform;
    if (Platform.FBK == paramPlatform)
    {
      this.fbAccessToken = null;
      this.fbId = null;
      return;
    }
  }

  public static enum Platform
  {
    static
    {
      Platform[] arrayOfPlatform = new Platform[2];
      arrayOfPlatform[0] = FBK;
      arrayOfPlatform[1] = FLX;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.AccountSessionManager
 * JD-Core Version:    0.6.2
 */