package com.flixster.android.data;

import android.content.Context;
import android.os.Bundle;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.flixster.android.utils.Properties;
import net.flixster.android.model.Property;

public class FacebookFacade
{
  public static void sendRequest(Facebook paramFacebook, String paramString, Facebook.DialogListener paramDialogListener, Context paramContext)
  {
    Bundle localBundle = new Bundle();
    if (paramString != null)
      localBundle.putString("to", paramString);
    String str = Properties.instance().getProperties().mskFbRequestBody;
    if (str == null)
      str = paramContext.getString(2131493106);
    localBundle.putString("message", str);
    paramFacebook.dialog(paramContext, "apprequests", localBundle, paramDialogListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.FacebookFacade
 * JD-Core Version:    0.6.2
 */