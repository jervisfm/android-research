package com.flixster.android.data;

import android.os.Handler;
import android.os.Message;
import com.flixster.android.activity.common.DaoHandler;
import com.flixster.android.model.NamedList;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.flixster.android.data.ActorDao;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.Actor;
import net.flixster.android.model.Movie;

public class MovieDaoNew
{
  public static void fetchBoxOffice(Collection<NamedList<Movie>> paramCollection, final Handler paramHandler1, final Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.fetchBoxOffice");
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        ArrayList localArrayList3 = new ArrayList();
        ArrayList localArrayList4 = new ArrayList();
        try
        {
          MovieDao.fetchBoxOffice(localArrayList1, localArrayList2, localArrayList3, localArrayList4, true);
          MovieDaoNew.this.clear();
          localArrayList2.addAll(localArrayList3);
          localArrayList2.addAll(localArrayList4);
          MovieDaoNew.this.add(new NamedList(String.valueOf(2131493299), localArrayList2));
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.fetchBoxOffice", localDaoException);
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void fetchDvd(Collection<NamedList<Movie>> paramCollection, final Handler paramHandler1, final Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.fetchDvd");
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        try
        {
          MovieDao.fetchDvdNewRelease(localArrayList1, localArrayList2, true);
          MovieDaoNew.this.clear();
          MovieDaoNew.this.add(new NamedList(String.valueOf(2131492910), localArrayList2));
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.fetchDvd", localDaoException);
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void fetchMovie(long paramLong, DaoHandler paramDaoHandler)
  {
    paramDaoHandler.activate();
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.fetchMovie " + this.val$id);
        try
        {
          Movie localMovie = MovieDao.getMovieDetail(this.val$id);
          this.val$handler.sendSuccessMessage(localMovie);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.fetchMovie " + this.val$id, localDaoException);
          this.val$handler.sendExceptionMessage(localDaoException);
        }
      }
    });
  }

  public static void fetchUpcoming(Collection<NamedList<Movie>> paramCollection, final Handler paramHandler1, final Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.fetchUpcoming");
        ArrayList localArrayList1 = new ArrayList();
        ArrayList localArrayList2 = new ArrayList();
        try
        {
          MovieDao.fetchUpcoming(localArrayList1, localArrayList2, true);
          MovieDaoNew.this.clear();
          MovieDaoNew.this.add(new NamedList(String.valueOf(2131493137), localArrayList2));
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.fetchUpcoming", localDaoException);
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static Movie getMovie(long paramLong)
  {
    return MovieDao.getMovie(paramLong);
  }

  public static void searchMovie(String paramString, final Collection<NamedList<Movie>> paramCollection, final Handler paramHandler1, final Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.searchMovie: " + MovieDaoNew.this);
        ArrayList localArrayList = new ArrayList();
        try
        {
          MovieDao.searchMovies(MovieDaoNew.this, localArrayList);
          paramCollection.clear();
          paramCollection.add(new NamedList(String.valueOf(2131493129), localArrayList));
          paramHandler1.sendEmptyMessage(0);
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.searchMovie", localDaoException);
          paramHandler2.sendMessage(Message.obtain(null, 0, localDaoException));
        }
      }
    });
  }

  public static void searchMovieAndActor(String paramString, final List<Movie> paramList, final List<Actor> paramList1, final DaoHandler paramDaoHandler)
  {
    paramDaoHandler.activate();
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        Logger.d("FlxMain", "MovieDaoNew.searchMovieAndActor: " + MovieDaoNew.this);
        try
        {
          MovieDao.searchMovies(MovieDaoNew.this, paramList);
          ActorDao.searchActors(MovieDaoNew.this, paramList1);
          paramDaoHandler.sendSuccessMessage();
          return;
        }
        catch (DaoException localDaoException)
        {
          Logger.e("FlxMain", "MovieDaoNew.searchMovieAndActor", localDaoException);
          paramDaoHandler.sendExceptionMessage(localDaoException);
        }
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.MovieDaoNew
 * JD-Core Version:    0.6.2
 */