package com.flixster.android.data;

import android.os.Handler;
import com.flixster.android.model.CarouselItem;
import com.flixster.android.model.PromoItem;
import com.flixster.android.utils.WorkerThreads;
import java.util.List;

public class PromoDao
{
  public static void get(final List<CarouselItem> paramList, final List<PromoItem> paramList1, final Handler paramHandler1, Handler paramHandler2)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      // ERROR //
      public void run()
      {
        // Byte code:
        //   0: new 39	java/net/URL
        //   3: dup
        //   4: invokestatic 45	net/flixster/android/data/ApiBuilder:promos	()Ljava/lang/String;
        //   7: invokespecial 48	java/net/URL:<init>	(Ljava/lang/String;)V
        //   10: iconst_1
        //   11: iconst_1
        //   12: invokestatic 54	net/flixster/android/util/HttpHelper:fetchUrl	(Ljava/net/URL;ZZ)Ljava/lang/String;
        //   15: astore 5
        //   17: new 56	org/json/JSONArray
        //   20: dup
        //   21: aload 5
        //   23: invokespecial 57	org/json/JSONArray:<init>	(Ljava/lang/String;)V
        //   26: aload_0
        //   27: getfield 23	com/flixster/android/data/PromoDao$1:val$featuredItems	Ljava/util/List;
        //   30: aload_0
        //   31: getfield 25	com/flixster/android/data/PromoDao$1:val$hotTodayItems	Ljava/util/List;
        //   34: invokestatic 63	com/flixster/android/model/PromoItem:parseArray	(Lorg/json/JSONArray;Ljava/util/List;Ljava/util/List;)V
        //   37: aload_0
        //   38: getfield 27	com/flixster/android/data/PromoDao$1:val$successHandler	Landroid/os/Handler;
        //   41: iconst_0
        //   42: invokevirtual 69	android/os/Handler:sendEmptyMessage	(I)Z
        //   45: pop
        //   46: return
        //   47: astore 4
        //   49: new 71	java/lang/RuntimeException
        //   52: dup
        //   53: aload 4
        //   55: invokespecial 74	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
        //   58: athrow
        //   59: astore_1
        //   60: aload_1
        //   61: invokestatic 80	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
        //   64: astore_2
        //   65: ldc 82
        //   67: ldc 84
        //   69: aload_2
        //   70: invokestatic 90	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   73: aload_0
        //   74: getfield 21	com/flixster/android/data/PromoDao$1:val$errorHandler	Landroid/os/Handler;
        //   77: aconst_null
        //   78: iconst_0
        //   79: aload_2
        //   80: invokestatic 96	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   83: invokevirtual 100	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   86: pop
        //   87: return
        //   88: astore 6
        //   90: aload 6
        //   92: invokestatic 80	net/flixster/android/data/DaoException:create	(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
        //   95: astore 7
        //   97: ldc 82
        //   99: ldc 84
        //   101: aload 7
        //   103: invokestatic 90	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   106: aload_0
        //   107: getfield 21	com/flixster/android/data/PromoDao$1:val$errorHandler	Landroid/os/Handler;
        //   110: aconst_null
        //   111: iconst_0
        //   112: aload 7
        //   114: invokestatic 96	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
        //   117: invokevirtual 100	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
        //   120: pop
        //   121: return
        //
        // Exception table:
        //   from	to	target	type
        //   0	17	47	java/net/MalformedURLException
        //   0	17	59	java/io/IOException
        //   17	46	88	org/json/JSONException
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.data.PromoDao
 * JD-Core Version:    0.6.2
 */