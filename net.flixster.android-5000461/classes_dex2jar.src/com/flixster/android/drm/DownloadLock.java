package com.flixster.android.drm;

import com.flixster.android.storage.ExternalPreferenceFile;
import com.flixster.android.utils.Logger;

public class DownloadLock extends ExternalPreferenceFile
{
  public static final String EXTENSION = ".dlck";
  private long assetId;
  private long downloadId;
  private boolean exists;
  private long rightId;
  private String rightType;
  private String title;

  public DownloadLock(long paramLong)
  {
    this.rightId = paramLong;
    this.exists = read();
    if (this.exists)
    {
      Logger.d("FlxDrm", "DownloadLock for " + paramLong + " exist");
      return;
    }
    Logger.d("FlxDrm", "DownloadLock for " + paramLong + " does not exist");
  }

  private static String deNull(String paramString)
  {
    if ((paramString == null) || (paramString.equals("")))
      paramString = ".";
    return paramString;
  }

  protected String createFileContent()
  {
    return this.rightId + "ZZZZ" + this.downloadId + "ZZZZ" + deNull(this.title) + "ZZZZ" + deNull(this.rightType) + "ZZZZ" + this.assetId;
  }

  protected String createFileName()
  {
    return this.rightId + ".dlck";
  }

  protected String createFileSubDir()
  {
    return "wv/";
  }

  public boolean exists()
  {
    if (!this.exists)
      Logger.w("FlxDrm", "DownloadLock for " + this.rightId + " does not exist");
    return this.exists;
  }

  public long getAssetId()
  {
    return this.assetId;
  }

  public long getDownloadId()
  {
    return this.downloadId;
  }

  public long getRightId()
  {
    return this.rightId;
  }

  public String getRightType()
  {
    if (this.rightType == null)
      return "";
    return this.rightType;
  }

  public String getTitle()
  {
    if (this.title == null)
      return "";
    return this.title;
  }

  protected boolean parseFileContent(String paramString)
  {
    String[] arrayOfString = split(paramString);
    boolean bool1 = false;
    String str1;
    String str2;
    if (arrayOfString != null)
    {
      int i = arrayOfString.length;
      bool1 = false;
      if (i == 5)
      {
        this.rightId = Long.valueOf(arrayOfString[0]).longValue();
        this.downloadId = Long.valueOf(arrayOfString[1]).longValue();
        if (!".".equals(arrayOfString[2]))
          break label111;
        str1 = null;
        this.title = str1;
        boolean bool2 = ".".equals(arrayOfString[3]);
        str2 = null;
        if (!bool2)
          break label119;
      }
    }
    while (true)
    {
      this.rightType = str2;
      this.assetId = Long.valueOf(arrayOfString[4]).longValue();
      bool1 = true;
      return bool1;
      label111: str1 = arrayOfString[2];
      break;
      label119: str2 = arrayOfString[3];
    }
  }

  public void setAssetId(long paramLong)
  {
    this.assetId = paramLong;
  }

  public void setDownloadId(long paramLong)
  {
    this.downloadId = paramLong;
  }

  public void setRightType(String paramString)
  {
    this.rightType = paramString;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.DownloadLock
 * JD-Core Version:    0.6.2
 */