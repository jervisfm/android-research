package com.flixster.android.drm;

import com.flixster.android.storage.ExternalPreferenceFile;
import com.flixster.android.utils.Logger;

public class DownloadRight extends ExternalPreferenceFile
{
  public static final String EXTENSION = ".rght";
  private String captionsUri;
  private String deviceId;
  private String downloadUri;
  private boolean exists;
  private String licenseProxy;
  private String queueId;
  private final long rightId;

  public DownloadRight(long paramLong)
  {
    this.rightId = paramLong;
    this.exists = read();
    if (this.exists)
    {
      Logger.d("FlxDrm", "DownloadRight for " + paramLong + " exist");
      return;
    }
    Logger.d("FlxDrm", "DownloadRight for " + paramLong + " does not exist");
  }

  private static String deNull(String paramString)
  {
    if ((paramString == null) || (paramString.equals("")))
      paramString = ".";
    return paramString;
  }

  protected String createFileContent()
  {
    return this.rightId + "ZZZZ" + deNull(this.downloadUri) + "ZZZZ" + deNull(this.licenseProxy) + "ZZZZ" + deNull(this.queueId) + "ZZZZ" + deNull(this.deviceId) + "ZZZZ" + deNull(this.captionsUri);
  }

  protected String createFileName()
  {
    return this.rightId + ".rght";
  }

  protected String createFileSubDir()
  {
    return "wv/";
  }

  public boolean exists()
  {
    if (!this.exists)
      Logger.w("FlxDrm", "DownloadRight for " + this.rightId + " does not exist");
    return this.exists;
  }

  public String getCaptionsUri()
  {
    if (this.captionsUri == null)
      return "";
    return this.captionsUri;
  }

  public String getDeviceId()
  {
    if (this.deviceId == null)
      return "";
    return this.deviceId;
  }

  public String getDownloadUri()
  {
    if (this.downloadUri == null)
      return "";
    return this.downloadUri;
  }

  public String getLicenseProxy()
  {
    if (this.licenseProxy == null)
      return "";
    return this.licenseProxy;
  }

  public String getQueueId()
  {
    if (this.queueId == null)
      return "";
    return this.queueId;
  }

  protected boolean parseFileContent(String paramString)
  {
    String[] arrayOfString = split(paramString);
    if ((arrayOfString != null) && (arrayOfString.length == 6))
    {
      String str1;
      String str2;
      label49: String str3;
      label69: String str4;
      label89: String str5;
      if (".".equals(arrayOfString[1]))
      {
        str1 = null;
        this.downloadUri = str1;
        if (!".".equals(arrayOfString[2]))
          break label128;
        str2 = null;
        this.licenseProxy = str2;
        if (!".".equals(arrayOfString[3]))
          break label136;
        str3 = null;
        this.queueId = str3;
        if (!".".equals(arrayOfString[4]))
          break label144;
        str4 = null;
        this.deviceId = str4;
        boolean bool = ".".equals(arrayOfString[5]);
        str5 = null;
        if (!bool)
          break label152;
      }
      while (true)
      {
        this.captionsUri = str5;
        return true;
        str1 = arrayOfString[1];
        break;
        label128: str2 = arrayOfString[2];
        break label49;
        label136: str3 = arrayOfString[3];
        break label69;
        label144: str4 = arrayOfString[4];
        break label89;
        label152: str5 = arrayOfString[5];
      }
    }
    return false;
  }

  public DownloadRight setCaptionsUri(String paramString)
  {
    this.captionsUri = paramString;
    return this;
  }

  public DownloadRight setDeviceId(String paramString)
  {
    this.deviceId = paramString;
    return this;
  }

  public DownloadRight setDownloadUri(String paramString)
  {
    this.downloadUri = paramString;
    return this;
  }

  public DownloadRight setLicenseProxy(String paramString)
  {
    this.licenseProxy = paramString;
    return this;
  }

  public DownloadRight setQueueId(String paramString)
  {
    this.queueId = paramString;
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.DownloadRight
 * JD-Core Version:    0.6.2
 */