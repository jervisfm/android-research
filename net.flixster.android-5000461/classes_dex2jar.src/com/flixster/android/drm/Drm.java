package com.flixster.android.drm;

import com.flixster.android.data.AccountManager;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Properties;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.LockerRight;

public class Drm
{
  private static PlaybackLogic logicInstance;
  private static PlaybackManager managerInstance;

  public static PlaybackLogic logic()
  {
    if (logicInstance == null)
    {
      if (Properties.instance().isGoogleTv())
        logicInstance = new WvNativePlaybackLogic(null);
    }
    else
      return logicInstance;
    if (F.IS_NATIVE_HC_DRM_ENABLED);
    for (Object localObject = new WvNativePlaybackLogic(null); ; localObject = new WvLibPlaybackLogic(null))
    {
      logicInstance = (PlaybackLogic)localObject;
      break;
    }
  }

  public static PlaybackManager manager()
  {
    if (managerInstance == null)
    {
      if (Properties.instance().isGoogleTv())
        managerInstance = new DrmManagerHc();
    }
    else
      return managerInstance;
    if (F.IS_NATIVE_HC_DRM_ENABLED);
    for (Object localObject = new DrmManagerHc(); ; localObject = new DrmManager())
    {
      managerInstance = (PlaybackManager)localObject;
      break;
    }
  }

  private static abstract class BasePlaybackLogic
    implements PlaybackLogic
  {
    private boolean isNonWifiStreamUnsupported;
    private boolean isStreamUnsupported;

    public boolean isAssetDownloadable(LockerRight paramLockerRight)
    {
      return (AccountManager.instance().hasUserSession()) && (paramLockerRight.isDownloadSupported) && (isDeviceDownloadCompatible());
    }

    public boolean isAssetPlayable(LockerRight paramLockerRight)
    {
      return (AccountManager.instance().hasUserSession()) && (((paramLockerRight.isStreamingSupported) && (isDeviceStreamingCompatible())) || ((isDeviceDownloadCompatible()) && (!paramLockerRight.isSeason()) && (DownloadHelper.isDownloaded(paramLockerRight.rightId))));
    }

    public boolean isDeviceNonWifiStreamBlacklisted()
    {
      return this.isNonWifiStreamUnsupported;
    }

    public boolean isDeviceStreamBlacklisted()
    {
      return this.isStreamUnsupported;
    }

    public void setDeviceNonWifiStreamBlacklisted(boolean paramBoolean)
    {
      this.isNonWifiStreamUnsupported = (paramBoolean | this.isNonWifiStreamUnsupported);
    }

    public void setDeviceStreamBlacklisted(boolean paramBoolean)
    {
      this.isStreamUnsupported = (paramBoolean | this.isStreamUnsupported);
    }
  }

  private static class WvLibPlaybackLogic extends Drm.BasePlaybackLogic
  {
    private WvLibPlaybackLogic()
    {
      super();
    }

    public boolean isDeviceDownloadCompatible()
    {
      if (FlixsterApplication.getAndroidBuildInt() < 9);
      while ((Drm.manager().isRooted()) || (!Drm.manager().isInitialized()))
        return false;
      return true;
    }

    public boolean isDeviceStreamingCompatible()
    {
      if (FlixsterApplication.getAndroidBuildInt() < 5);
      while (Drm.manager().isRooted())
        return false;
      return true;
    }

    public boolean isNativeWv()
    {
      return false;
    }
  }

  private static class WvNativePlaybackLogic extends Drm.BasePlaybackLogic
  {
    private WvNativePlaybackLogic()
    {
      super();
    }

    public boolean isDeviceDownloadCompatible()
    {
      return false;
    }

    public boolean isDeviceStreamingCompatible()
    {
      if (FlixsterApplication.getAndroidBuildInt() < 12);
      while ((Properties.instance().isHoneycombTablet()) && (FlixsterApplication.getAndroidBuildInt() < 13))
        return false;
      return true;
    }

    public boolean isNativeWv()
    {
      return true;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.Drm
 * JD-Core Version:    0.6.2
 */