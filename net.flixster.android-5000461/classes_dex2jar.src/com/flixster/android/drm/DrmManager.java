package com.flixster.android.drm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.net.DownloadHelper;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.UrlHelper;
import com.flixster.android.view.DialogBuilder;
import com.widevine.drmapi.android.WVEvent;
import com.widevine.drmapi.android.WVEventListener;
import com.widevine.drmapi.android.WVPlayback;
import com.widevine.drmapi.android.WVStatus;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;

public class DrmManager
  implements PlaybackManager
{
  private static final String[] PORTAL_KEYS = { "warnerbros", "CN", "IDM" };
  private final Handler ackLicenseSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.ackLicenseSuccessHandler: customData " + DrmManager.this.customData);
    }
  };
  private String captionsUri;
  private Context context;
  private String customData;
  private ProgressDialog dialog;
  private String downloadAssetUri;
  private List<LockerRight> downloadedAssetRights;
  private final Handler endDownloadSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      String str = (String)paramAnonymousMessage.obj;
      Logger.d("FlxDrm", "DrmManager.endDownloadSuccessHandler sonicQueueId " + str);
    }
  };
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
      if ((localActivity == null) || (localActivity.isFinishing()));
      while (true)
      {
        return;
        DrmManager.this.trackPlaybackEvent("api_error");
        Logger.d("FlxDrm", "DrmManager.errorHandler: " + paramAnonymousMessage);
        if (DrmManager.this.dialog != null);
        try
        {
          DrmManager.this.dialog.dismiss();
          if (!(paramAnonymousMessage.obj instanceof DaoException))
            continue;
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, ActivityHolder.instance().getTopLevelActivity(), 0);
          return;
        }
        catch (Exception localException)
        {
          while (true)
            localException.printStackTrace();
        }
      }
    }
  };
  private boolean isInitialized;
  private boolean isInitializedOnDemand;
  private Boolean isRooted;
  private boolean isStreamingMode;
  private String playbackAssetUri;
  private int playbackPosition;
  private WidevinePlayer player;
  private LockerRight r;
  private final Handler silentErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.silentErrorHandler: " + paramAnonymousMessage);
    }
  };
  private String streamId;
  private final Handler streamResumeSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HashMap localHashMap = (HashMap)paramAnonymousMessage.obj;
      DrmManager.this.playbackAssetUri = ((String)localHashMap.get("fileLocation"));
      DrmManager.this.streamId = ((String)localHashMap.get("streamId"));
      DrmManager.this.customData = ((String)localHashMap.get("customData"));
      String str1 = (String)localHashMap.get("drmServerUrl");
      String str2 = (String)localHashMap.get("deviceId");
      DrmManager.this.updateDrmCredentials(str1, str2, DrmManager.this.streamId);
    }
  };
  private final Handler streamStartSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HashMap localHashMap = (HashMap)paramAnonymousMessage.obj;
      DrmManager.this.playbackAssetUri = ((String)localHashMap.get("fileLocation"));
      DrmManager.this.playbackPosition = ((Integer)localHashMap.get("playPosition")).intValue();
      DrmManager.this.captionsUri = ((String)localHashMap.get("closedCaptionFileLocation"));
      if (DrmManager.this.r.isSonicProxyMode())
      {
        DrmManager.this.streamId = ((String)localHashMap.get("streamId"));
        DrmManager.this.customData = ((String)localHashMap.get("customData"));
        String str1 = (String)localHashMap.get("drmServerUrl");
        String str2 = (String)localHashMap.get("deviceId");
        DrmManager.this.updateDrmCredentials(str1, str2, DrmManager.this.streamId);
        DrmManager.this.wvPlay(DrmManager.this.playbackPosition);
        return;
      }
      ProfileDao.uvStreamCreate(DrmManager.this.uvStreamCreateSuccessHandler, DrmManager.this.errorHandler, DrmManager.this.r);
    }
  };
  private final Handler streamStopSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.streamStopSuccessHandler " + DrmManager.this.r + " streamId " + DrmManager.this.streamId);
    }
  };
  private final Handler trackLicenseSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.trackLicenseSuccessHandler: streamId " + DrmManager.this.streamId);
    }
  };
  private final Handler updatePlaybackPositionSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.updatePlaybackPositionSuccessHandler");
    }
  };
  private final Handler uvStreamCreateSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DrmManager.this.streamId = ((String)paramAnonymousMessage.obj);
      DrmManager.this.updateDrmCredentials();
      DrmManager.this.wvPlay(DrmManager.this.playbackPosition);
    }
  };
  private final Handler uvStreamDeleteSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManager.uvStreamDeleteSuccessHandler " + DrmManager.this.r + " streamId " + DrmManager.this.streamId);
    }
  };
  private final Handler uvStreamResumeSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DrmManager.this.streamId = ((String)paramAnonymousMessage.obj);
      Logger.d("FlxDrm", "DrmManager.uvStreamResumeSuccessHandler " + DrmManager.this.r + " streamId " + DrmManager.this.streamId);
    }
  };
  private WVPlayback wvPlayback;

  private String eventToString(WVEvent paramWVEvent, HashMap<String, Object> paramHashMap)
  {
    String str;
    switch ($SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[paramWVEvent.ordinal()])
    {
    default:
      str = "" + "onEvent: Error UNKNOWN\n";
      if (paramHashMap.containsKey("WVLicenseDurationRemainingKey"))
        str = str + "WVLicenseDurationRemaining: " + (Long)paramHashMap.get("WVLicenseDurationRemainingKey") + "\n";
      if (paramHashMap.containsKey("WVPurchaseDurationRemainingKey"))
        str = str + "WVPurchaseDurationRemaining: " + (Long)paramHashMap.get("WVPurchaseDurationRemainingKey") + "\n";
      if (paramHashMap.containsKey("WVPlaybackElapsedTimeKey"))
        str = str + "WVPlaybackElapsedTimeKey: " + (Long)paramHashMap.get("WVPlaybackElapsedTimeKey") + "\n";
      if (paramHashMap.containsKey("WVAssetPathKey"))
        str = str + "WVAssetPathKey: " + (String)paramHashMap.get("WVAssetPathKey") + "\n";
      if (paramHashMap.containsKey("WVLicenseTypeKey"))
        str = str + "WVLicenseTypeKey: " + (Integer)paramHashMap.get("WVLicenseTypeKey") + "\n";
      if (paramHashMap.containsKey("WVIsEncryptedKey"))
      {
        if (((Boolean)paramHashMap.get("WVIsEncryptedKey")).booleanValue())
          str = str + "WVIsEncryptedKey: True\n";
      }
      else
        label404: if (paramHashMap.containsKey("WVStatusKey"))
          switch ($SWITCH_TABLE$com$widevine$drmapi$android$WVStatus()[((WVStatus)paramHashMap.get("WVStatusKey")).ordinal()])
          {
          case 22:
          default:
            str = str + "WVStatusKey: Unknown Error\n";
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
          case 11:
          case 12:
          case 13:
          case 14:
          case 15:
          case 16:
          case 17:
          case 18:
          case 19:
          case 20:
          case 21:
          case 23:
          case 24:
          case 25:
          case 26:
          case 27:
          case 28:
          case 29:
          case 30:
          }
      break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    }
    while (true)
    {
      if (paramHashMap.containsKey("WVVersionKey"))
        str = str + "WVVersionKey: " + (String)paramHashMap.get("WVVersionKey") + "\n";
      if (paramHashMap.containsKey("WVAssetTypeKey"))
        str = str + "WVAssetTypeKey: " + (Integer)paramHashMap.get("WVAssetTypeKey") + "\n";
      if (paramHashMap.containsKey("WVSystemIDKey"))
        str = str + "WVSystemIDKey: " + (Long)paramHashMap.get("WVSystemIDKey") + "\n";
      if (paramHashMap.containsKey("WVAssetIDKey"))
        str = str + "WVAssetIDKey: " + (Long)paramHashMap.get("WVAssetIDKey") + "\n";
      if (paramHashMap.containsKey("WVKeyIDKey"))
        str = str + "WVKeyID: " + (Long)paramHashMap.get("WVKeyIDKey") + "\n";
      if (paramHashMap.containsKey("WVErrorKey"))
        str = str + "WVErrorKey: " + (String)paramHashMap.get("WVErrorKey") + "\n";
      return str;
      str = "" + "onEvent: NullEvent\n";
      break;
      str = "" + "onEvent: LicenseReceived\n";
      break;
      str = "" + "onEvent: LicenseRequestFailed\n";
      break;
      str = "" + "onEvent: Playing\n";
      break;
      str = "" + "onEvent: PlayFailed\n";
      break;
      str = "" + "onEvent: Stopped\n";
      break;
      str = "" + "onEvent: QueryStatus\n";
      break;
      str = "" + "onEvent: EndOfList\n";
      break;
      str = "" + "onEvent: Initialized\n";
      break;
      str = "" + "onEvent: InitializeFailed\n";
      break;
      str = "" + "onEvent: Terminated\n";
      break;
      str = "" + "onEvent: LicenseRemoved\n";
      break;
      str = "" + "onEvent: Registered\n";
      break;
      str = "" + "onEvent: Unregistered\n";
      break;
      str = "" + "onEvent: SecureStore\n";
      break;
      str = str + "WVIsEncryptedKey: False\n";
      break label404;
      str = str + "WVStatusKey: OK\n";
      continue;
      str = str + "WVStatusKey: NotInitialized\n";
      continue;
      str = str + "WVStatusKey: AlreadyInitialized\n";
      continue;
      str = str + "WVStatusKey: CantConnectToMediaServer\n";
      continue;
      str = str + "WVStatusKey: BadMedia\n";
      continue;
      str = str + "WVStatusKey: CantConnectToDrmServer\n";
      continue;
      str = str + "WVStatusKey: NotLicensed\n";
      continue;
      str = str + "WVStatusKey: LicenseDenied\n";
      continue;
      str = str + "WVStatusKey: LostConnection\n";
      continue;
      str = str + "WVStatusKey: LicenseExpired\n";
      continue;
      str = str + "WVStatusKey: AssetExpired\n";
      continue;
      str = str + "WVStatusKey: NotLicensedByRegion\n";
      continue;
      str = str + "WVStatusKey: EntitlenentRequestLimitReached\n";
      continue;
      str = str + "WVStatusKey: BadURL\n";
      continue;
      str = str + "WVStatusKey: FileNotPresent\n";
      continue;
      str = str + "WVStatusKey: NotRegistered\n";
      continue;
      str = str + "WVStatusKey: AlreadyRegistered\n";
      continue;
      str = str + "WVStatusKey: NotPlaying\n";
      continue;
      str = str + "WVStatusKey: AlreadyPlaying\n";
      continue;
      str = str + "WVStatusKey: FileSystemError\n";
      continue;
      str = str + "WVStatusKey: AssetDBWasCorrupted\n";
      continue;
      str = str + "WVStatusKey: MandatorySettingAbsent\n";
      continue;
      str = str + "WVStatusKey: SystemCallError\n";
      continue;
      str = str + "WVStatusKey: OutOfMemoryError\n";
      continue;
      str = str + "WVStatusKey: TamperDetected\n";
      continue;
      str = str + "WVStatus: PendingServerNotification\n";
      continue;
      str = str + "WVStatus: HardwareIDAbsent\n";
      continue;
      str = str + "WVStatus: OutOfRange\n";
      continue;
      str = str + "WVStatus: HeartbeatError\n";
    }
  }

  private HashMap<String, Object> getSettings()
  {
    HashMap localHashMap = new HashMap();
    if (this.r == null);
    for (String str = ""; ; str = ApiBuilder.widevineLicense(this.r, this.streamId))
    {
      localHashMap.put("WVDRMServer", str);
      localHashMap.put("WVPortalKey", PORTAL_KEYS[0]);
      localHashMap.put("WVAssetRootKey", "");
      return localHashMap;
    }
  }

  private static boolean isMovieOrEpisodeDownloaded(LockerRight paramLockerRight)
  {
    if (DownloadHelper.isMovieDownloadInProgress(paramLockerRight.rightId));
    while (!DownloadHelper.isDownloaded(paramLockerRight.rightId))
      return false;
    return true;
  }

  private void logEvent(WVEvent paramWVEvent, HashMap<String, Object> paramHashMap)
  {
    Logger.d("FlxDrm", "====================================");
    Logger.d("FlxDrm", eventToString(paramWVEvent, paramHashMap));
    Logger.d("FlxDrm", "====================================");
  }

  private void registerLocalAsset(LockerRight paramLockerRight)
  {
    String str1 = DownloadHelper.findDownloadedMovie(paramLockerRight.rightId);
    if (str1 != null)
    {
      if (str1.startsWith("file://"))
        str1 = str1.substring(7);
      this.r = paramLockerRight;
      this.isStreamingMode = false;
      DownloadRight localDownloadRight = new DownloadRight(paramLockerRight.rightId);
      String str2 = localDownloadRight.getLicenseProxy();
      if ("".equals(str2))
        updateDrmCredentials();
      while (true)
      {
        Logger.d("FlxDrm", "DrmManager.registerLocalAsset " + paramLockerRight);
        wvRegister(str1);
        return;
        paramLockerRight.setSonicProxyMode();
        updateDrmCredentials(str2, localDownloadRight.getDeviceId(), null);
      }
    }
    Logger.w("FlxDrm", "DrmManager.registerLocalAsset cannot find " + paramLockerRight);
    if (this.dialog != null);
    try
    {
      this.dialog.dismiss();
      registerLocalAssetLooper();
      return;
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  private void registerLocalAssetLooper()
  {
    if ((this.downloadedAssetRights != null) && (this.downloadedAssetRights.size() > 0))
    {
      LockerRight localLockerRight = (LockerRight)this.downloadedAssetRights.remove(0);
      Logger.i("FlxDrm", "DrmManager.registerLocalAssetLooper registering " + localLockerRight);
      this.playbackAssetUri = null;
      registerLocalAsset(localLockerRight);
      return;
    }
    if (this.downloadedAssetRights == null)
    {
      Logger.d("FlxDrm", "DrmManager.registerLocalAssetLooper uninitialized");
      return;
    }
    Logger.i("FlxDrm", "DrmManager.registerLocalAssetLooper finished");
    this.downloadedAssetRights = null;
  }

  private String statusToString(WVStatus paramWVStatus)
  {
    switch ($SWITCH_TABLE$com$widevine$drmapi$android$WVStatus()[paramWVStatus.ordinal()])
    {
    default:
      return "Unknown";
    case 1:
      return "WVStatus.OK";
    case 2:
      return "WVStatus.NotInitialized";
    case 3:
      return "WVStatus.AlreadyInitialized";
    case 4:
      return "WVStatus.CantConnectToMediaServer";
    case 5:
      return "WVStatus.BadMedia";
    case 6:
      return "WVStatus.CantConnectToDrmServer";
    case 7:
      return "WVStatus.NotLicensed";
    case 8:
      return "WVStatus.LicenseDenied";
    case 9:
      return "WVStatus.LostConnection";
    case 10:
      return "WVStatus.LicenseExpired";
    case 11:
      return "WVStatus.AssetExpired";
    case 12:
      return "WVStatus.NotLicensedByRegion";
    case 13:
      return "WVStatus.LicenseRequestLimitReached";
    case 14:
      return "WVStatus.BadURL";
    case 15:
      return "WVStatus.FileNotPresent";
    case 16:
      return "WVStatus.NotRegistered";
    case 17:
      return "WVStatus.AlreadyRegistered";
    case 18:
      return "WVStatus.NotPlaying";
    case 19:
      return "WVStatus.AlreadyPlaying";
    case 20:
      return "WVStatus.FileSystemError";
    case 21:
      return "WVStatus.AssetDBWasCorrupted";
    case 22:
      return "WVStatus.ClockTamperDetected";
    case 23:
      return "WVStatus.MandatorySettingAbsent";
    case 24:
      return "WVStatus.SystemCallError";
    case 25:
      return "WVStatus.OutOfMemoryError";
    case 26:
      return "WVStatus.TamperDetected";
    case 27:
      return "WVStatus.PendingServerNotification";
    case 28:
      return "WVStatus.HardwareIDAbsent";
    case 29:
      return "WVStatus.OutOfRange";
    case 30:
    }
    return "WVStatus.HeartbeatError";
  }

  private void trackPlayback()
  {
    Trackers.instance().track(getPlaybackTag(), getPlaybackTitle());
  }

  private void trackPlaybackEvent(String paramString)
  {
    Trackers.instance().trackEvent(getPlaybackTag(), getPlaybackTitle(), "Streaming", UrlHelper.urlEncode(Build.MODEL), paramString, 0);
  }

  private void updateDrmCredentials()
  {
    HashMap localHashMap = getSettings();
    this.wvPlayback.setCredentials(localHashMap);
    Logger.d("FlxDrm", "DrmManager.updateDrmCredentials: proxy URL https://...");
  }

  private void updateDrmCredentials(String paramString1, String paramString2, String paramString3)
  {
    Logger.d("FlxDrm", "DrmManager.updateDrmCredentials: proxy URL " + paramString1);
    HashMap localHashMap = new HashMap();
    localHashMap.put("WVDRMServer", paramString1);
    localHashMap.put("WVPortalKey", PORTAL_KEYS[1]);
    localHashMap.put("WVAssetRootKey", "");
    localHashMap.put("WVDeviceIDKey", paramString2);
    if (paramString3 != null)
      localHashMap.put("WVStreamIDKey", paramString3);
    this.wvPlayback.setCredentials(localHashMap);
  }

  public String getPlaybackTag()
  {
    if (this.isStreamingMode)
      return "/movie/play/streaming";
    return "/movie/play/offline";
  }

  public String getPlaybackTitle()
  {
    String str1;
    StringBuilder localStringBuilder;
    if (this.isStreamingMode)
    {
      str1 = "Movie Play Streaming - ";
      localStringBuilder = new StringBuilder(String.valueOf(str1));
      if (this.r == null)
        break label54;
    }
    label54: for (String str2 = this.r.getTitle(); ; str2 = null)
    {
      return str2;
      str1 = "Movie Play Offline - ";
      break;
    }
  }

  public WidevinePlayer getPlayer()
  {
    return this.player;
  }

  public void initializeDRM(Context paramContext)
  {
    this.context = paramContext;
    if (this.wvPlayback == null)
    {
      this.wvPlayback = new WVPlayback();
      if (isRooted())
        Logger.d("FlxDrm", "WVPlayback.initialize failed on rooted device");
    }
    else
    {
      return;
    }
    HashMap localHashMap = getSettings();
    WVStatus localWVStatus = this.wvPlayback.initialize(this.context, localHashMap, new DrmEventListener(null));
    Logger.d("FlxDrm", "WVPlayback.initialize: " + statusToString(localWVStatus));
  }

  public boolean isInitialized()
  {
    return this.isInitialized;
  }

  public boolean isRooted()
  {
    if (this.isRooted == null)
      if (this.wvPlayback != null)
        break label32;
    label32: for (boolean bool = false; ; bool = this.wvPlayback.isRooted())
    {
      this.isRooted = Boolean.valueOf(bool);
      return this.isRooted.booleanValue();
    }
  }

  public boolean isStreamingMode()
  {
    return this.isStreamingMode;
  }

  public void playMovie(LockerRight paramLockerRight)
  {
    this.r = paramLockerRight;
    Logger.d("FlxDrm", "DrmManager.playMovie " + this.r);
    String str1 = DownloadHelper.findDownloadedMovie(this.r.rightId);
    if ((str1 != null) && (str1.startsWith("file://")))
      str1 = str1.substring(7);
    int i;
    if ((str1 != null) && (!DownloadHelper.isMovieDownloadInProgress(this.r.rightId)))
      i = 1;
    while ((i == 0) || (FlixsterApplication.isConnected()))
      if (this.context == null)
      {
        return;
        i = 0;
      }
      else
      {
        String str2 = this.context.getResources().getString(2131493172);
        Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
        if ((localActivity != null) && (!localActivity.isFinishing()))
        {
          this.dialog = new ProgressDialog(localActivity);
          this.dialog.setMessage(str2);
          this.dialog.setIndeterminate(true);
          this.dialog.setCancelable(false);
          this.dialog.setCanceledOnTouchOutside(false);
          this.dialog.show();
        }
      }
    boolean bool = false;
    if (i != 0);
    while (true)
    {
      this.isStreamingMode = bool;
      trackPlayback();
      if (i == 0)
        break;
      this.playbackAssetUri = str1;
      registerLocalAsset(this.r);
      return;
      bool = true;
    }
    trackPlaybackEvent("stream_create");
    Handler localHandler1 = this.streamStartSuccessHandler;
    Handler localHandler2 = this.errorHandler;
    LockerRight localLockerRight = this.r;
    if (FlixsterApplication.isWifi());
    for (String str3 = "high"; ; str3 = "low")
    {
      ProfileDao.streamStart(localHandler1, localHandler2, localLockerRight, str3);
      return;
    }
  }

  public void registerAllLocalAssets(List<LockerRight> paramList)
  {
    Logger.i("FlxDrm", "DrmManager.registerAllLocalAssets");
    this.downloadedAssetRights = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        registerLocalAssetLooper();
        return;
      }
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      if (((localLockerRight.isMovie()) || (localLockerRight.isEpisode())) && (isMovieOrEpisodeDownloaded(localLockerRight)))
      {
        Logger.i("FlxDrm", "DrmManager.registerAllLocalAssets found downloaded asset " + localLockerRight);
        this.downloadedAssetRights.add(localLockerRight);
      }
    }
  }

  public void registerAssetOnDownloadComplete(LockerRight paramLockerRight)
  {
    this.r = paramLockerRight;
    if (!this.isInitialized)
    {
      this.isInitializedOnDemand = true;
      initializeDRM(FlixsterApplication.getContext());
    }
    while (true)
    {
      DownloadRight localDownloadRight = new DownloadRight(paramLockerRight.rightId);
      String str = localDownloadRight.getQueueId();
      if ((!"".equals(str)) && (AccountFacade.getSocialUser() != null))
      {
        localDownloadRight.setQueueId("");
        localDownloadRight.save();
        ProfileDao.endDownload(this.endDownloadSuccessHandler, this.silentErrorHandler, paramLockerRight.rightId, str);
      }
      return;
      registerLocalAsset(paramLockerRight);
    }
  }

  public void setPlayer(WidevinePlayer paramWidevinePlayer)
  {
    this.player = paramWidevinePlayer;
  }

  public void streamResume()
  {
    Logger.d("FlxDrm", "DrmManager.streamResume");
    trackPlaybackEvent("stream_resume");
    if (this.r.isSonicProxyMode())
    {
      Handler localHandler1 = this.streamResumeSuccessHandler;
      Handler localHandler2 = this.silentErrorHandler;
      LockerRight localLockerRight = this.r;
      if (FlixsterApplication.isWifi());
      for (String str = "high"; ; str = "low")
      {
        ProfileDao.streamStart(localHandler1, localHandler2, localLockerRight, str);
        return;
      }
    }
    ProfileDao.uvStreamCreate(this.uvStreamResumeSuccessHandler, this.silentErrorHandler, this.r);
  }

  public void streamStop(int paramInt)
  {
    Logger.d("FlxDrm", "DrmManager.streamStop");
    trackPlaybackEvent("stream_delete");
    Handler localHandler1 = this.streamStopSuccessHandler;
    Handler localHandler2 = this.silentErrorHandler;
    LockerRight localLockerRight = this.r;
    String str1 = this.streamId;
    if (paramInt >= 0);
    for (String str2 = Integer.toString(paramInt); ; str2 = null)
    {
      ProfileDao.streamStop(localHandler1, localHandler2, localLockerRight, str1, str2);
      if (!this.r.isSonicProxyMode())
        ProfileDao.uvStreamDelete(this.uvStreamDeleteSuccessHandler, this.silentErrorHandler, this.streamId);
      return;
    }
  }

  public void terminateDRM()
  {
    if (this.wvPlayback != null)
    {
      WVStatus localWVStatus = this.wvPlayback.terminate();
      Logger.d("FlxDrm", "WVPlayback.terminate: " + statusToString(localWVStatus));
      this.wvPlayback = null;
    }
  }

  public void unregisterLocalAsset(LockerRight paramLockerRight)
  {
    this.playbackAssetUri = null;
  }

  public void updatePlaybackPosition(int paramInt)
  {
    Logger.d("FlxDrm", "DrmManager.updatePlaybackPosition");
    ProfileDao.updatePlaybackPosition(this.updatePlaybackPositionSuccessHandler, this.silentErrorHandler, this.r, Integer.toString(paramInt));
  }

  protected void wvPlay(int paramInt)
  {
    if ((this.wvPlayback == null) || (this.playbackAssetUri == null) || (this.playbackAssetUri.length() == 0))
      if (this.dialog == null);
    String str;
    while (true)
    {
      try
      {
        this.dialog.dismiss();
        if (this.isStreamingMode)
          streamStop(-1);
        return;
      }
      catch (Exception localException1)
      {
        localException1.printStackTrace();
        continue;
      }
      str = this.wvPlayback.play(this.playbackAssetUri);
      if (str == null)
      {
        if (this.dialog != null);
        try
        {
          this.dialog.dismiss();
          if (this.isStreamingMode)
          {
            streamStop(-1);
            return;
          }
        }
        catch (Exception localException3)
        {
          while (true)
            localException3.printStackTrace();
        }
      }
    }
    Logger.d("FlxDrm", "WVPlayback.play: " + str);
    if (this.dialog != null);
    try
    {
      this.dialog.dismiss();
      Intent localIntent = new Intent();
      localIntent.setAction("android.intent.action.VIEW");
      localIntent.setDataAndType(Uri.parse(str), "video/*");
      localIntent.addFlags(268435456);
      localIntent.putExtra("KEY_RIGHT_ID", this.r.rightId);
      localIntent.putExtra("KEY_PLAY_POSITION", paramInt);
      localIntent.putExtra("KEY_CAPTIONS_URI", this.captionsUri);
      localIntent.setClass(this.context, WidevinePlayer.class);
      this.context.startActivity(localIntent);
      return;
    }
    catch (Exception localException2)
    {
      while (true)
        localException2.printStackTrace();
    }
  }

  protected void wvQuery(String paramString)
  {
    WVStatus localWVStatus = this.wvPlayback.queryAssetStatus(paramString);
    Logger.d("FlxDrm", "WVPlayback.queryAssetStatus: " + paramString + " " + statusToString(localWVStatus));
  }

  protected void wvRegister(String paramString)
  {
    WVStatus localWVStatus = this.wvPlayback.registerAsset(paramString);
    Logger.d("FlxDrm", "WVPlayback.registerAsset: " + paramString + " " + statusToString(localWVStatus));
    this.downloadAssetUri = paramString;
  }

  protected void wvRequestLicense(String paramString)
  {
    WVStatus localWVStatus = this.wvPlayback.requestLicense(paramString);
    Logger.d("FlxDrm", "WVPlayback.requestLicense: " + paramString + " " + statusToString(localWVStatus));
  }

  public Uri wvResume()
  {
    if ((this.wvPlayback == null) || (this.playbackAssetUri == null));
    String str;
    do
    {
      return null;
      str = this.wvPlayback.play(this.playbackAssetUri);
    }
    while (str == null);
    Logger.d("FlxDrm", "WVPlayback.play: " + str);
    return Uri.parse(str);
  }

  public void wvStop()
  {
    if (this.wvPlayback == null)
      return;
    WVStatus localWVStatus = this.wvPlayback.stop();
    Logger.d("FlxDrm", "WVPlayback.stop: " + statusToString(localWVStatus));
  }

  protected void wvUnregister(String paramString)
  {
    WVStatus localWVStatus = this.wvPlayback.unregisterAsset(paramString);
    Logger.d("FlxDrm", "WVPlayback.unregisterAsset: " + paramString + " " + statusToString(localWVStatus));
  }

  private class DrmEventListener
    implements WVEventListener
  {
    private DrmEventListener()
    {
    }

    public WVStatus onEvent(WVEvent paramWVEvent, HashMap<String, Object> paramHashMap)
    {
      DrmManager.this.logEvent(paramWVEvent, paramHashMap);
      boolean bool = paramHashMap.containsKey("WVStatusKey");
      WVStatus localWVStatus = null;
      WidevinePlayer.PlaybackError localPlaybackError;
      if (bool)
      {
        localWVStatus = (WVStatus)paramHashMap.get("WVStatusKey");
        if (localWVStatus != WVStatus.OK)
        {
          if (DrmManager.this.player != null)
          {
            if (localWVStatus != WVStatus.LostConnection)
              break label284;
            localPlaybackError = WidevinePlayer.PlaybackError.NETWORK;
            StringBuilder localStringBuilder2 = new StringBuilder();
            localStringBuilder2.append(localWVStatus.toString());
            if (paramHashMap.containsKey("WVErrorKey"))
              localStringBuilder2.append(" ").append((String)paramHashMap.get("WVErrorKey"));
            DrmManager.this.player.notifyPlaybackFailure(localPlaybackError, localStringBuilder2.toString());
          }
          StringBuilder localStringBuilder1 = new StringBuilder();
          localStringBuilder1.append("WVStatus: ").append(localWVStatus.toString());
          if (paramHashMap.containsKey("WVErrorKey"))
            localStringBuilder1.append(", WVError: ").append((String)paramHashMap.get("WVErrorKey"));
          DrmManager.this.trackPlaybackEvent(localStringBuilder1.toString());
        }
      }
      switch ($SWITCH_TABLE$com$widevine$drmapi$android$WVEvent()[paramWVEvent.ordinal()])
      {
      case 12:
      case 14:
      default:
      case 9:
      case 8:
      case 4:
      case 1:
      case 6:
      case 10:
      case 11:
      case 15:
      case 2:
      case 3:
      case 13:
      case 7:
      case 5:
      }
      while (true)
      {
        return WVStatus.OK;
        label284: if (localWVStatus == WVStatus.NotLicensed)
        {
          localPlaybackError = WidevinePlayer.PlaybackError.LICENSE;
          break;
        }
        if (localWVStatus == WVStatus.TamperDetected)
        {
          localPlaybackError = WidevinePlayer.PlaybackError.SECURITY;
          break;
        }
        localPlaybackError = WidevinePlayer.PlaybackError.UNKNOWN;
        break;
        DrmManager.this.isInitialized = true;
        if (DrmManager.this.isInitializedOnDemand)
          DrmManager.this.registerLocalAsset(DrmManager.this.r);
        return WVStatus.OK;
        if (DrmManager.this.dialog != null);
        try
        {
          DrmManager.this.dialog.dismiss();
          return WVStatus.OK;
        }
        catch (Exception localException3)
        {
          while (true)
            localException3.printStackTrace();
        }
        if ((DrmManager.this.r.isSonicProxyMode()) && (DrmManager.this.isStreamingMode))
        {
          ProfileDao.trackLicense(DrmManager.this.trackLicenseSuccessHandler, DrmManager.this.silentErrorHandler, DrmManager.this.r, DrmManager.this.streamId, true);
          ProfileDao.ackLicense(DrmManager.this.ackLicenseSuccessHandler, DrmManager.this.silentErrorHandler, DrmManager.this.customData);
        }
        return WVStatus.OK;
        return WVStatus.OK;
        if ((DrmManager.this.r.isSonicProxyMode()) && (AccountFacade.getSocialUser() != null))
          ProfileDao.trackLicense(DrmManager.this.trackLicenseSuccessHandler, DrmManager.this.silentErrorHandler, DrmManager.this.r, null, true);
        if ((DrmManager.this.playbackAssetUri != null) && (DrmManager.this.playbackAssetUri.equals(DrmManager.this.downloadAssetUri)))
        {
          DrmManager.this.captionsUri = DownloadHelper.findDownloadCaptionsFile(DrmManager.this.r.rightId);
          DrmManager.this.wvPlay(-1);
        }
        DrmManager.this.registerLocalAssetLooper();
        continue;
        if ((DrmManager.this.r.isSonicProxyMode()) && (AccountFacade.getSocialUser() != null))
          ProfileDao.trackLicense(DrmManager.this.trackLicenseSuccessHandler, DrmManager.this.silentErrorHandler, DrmManager.this.r, null, false);
        if (DrmManager.this.dialog != null);
        try
        {
          DrmManager.this.dialog.dismiss();
          DrmManager.this.registerLocalAssetLooper();
        }
        catch (Exception localException2)
        {
          while (true)
            localException2.printStackTrace();
        }
        DrmManager.this.wvQuery(DrmManager.this.downloadAssetUri);
        continue;
        if (localWVStatus != WVStatus.OK)
        {
          if (FlixsterApplication.isConnected())
          {
            DrmManager.this.wvRequestLicense(DrmManager.this.downloadAssetUri);
          }
          else
          {
            if (DrmManager.this.dialog != null);
            try
            {
              DrmManager.this.dialog.dismiss();
              String str = DrmManager.this.context.getResources().getString(2131493246);
              DrmManager.this.errorHandler.sendMessage(Message.obtain(null, 0, DaoException.create(DaoException.Type.NOT_LICENSED, str)));
              DrmManager.this.registerLocalAssetLooper();
            }
            catch (Exception localException1)
            {
              while (true)
                localException1.printStackTrace();
            }
          }
        }
        else
        {
          if ((DrmManager.this.playbackAssetUri != null) && (DrmManager.this.playbackAssetUri.equals(DrmManager.this.downloadAssetUri)))
          {
            DrmManager.this.captionsUri = DownloadHelper.findDownloadCaptionsFile(DrmManager.this.r.rightId);
            DrmManager.this.wvPlay(-1);
          }
          DrmManager.this.registerLocalAssetLooper();
          continue;
          if ((DrmManager.this.r.isSonicProxyMode()) && (localWVStatus == WVStatus.NotLicensed))
          {
            ProfileDao.trackLicense(DrmManager.this.trackLicenseSuccessHandler, DrmManager.this.silentErrorHandler, DrmManager.this.r, DrmManager.this.streamId, false);
          }
          else if (localWVStatus == WVStatus.NotInitialized)
          {
            Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
            if ((localActivity != null) && (!localActivity.isFinishing()))
              DialogBuilder.showStreamUnsupportedOnRootedDevices(localActivity);
          }
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.DrmManager
 * JD-Core Version:    0.6.2
 */