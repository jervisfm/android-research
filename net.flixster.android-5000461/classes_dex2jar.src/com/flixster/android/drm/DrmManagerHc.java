package com.flixster.android.drm;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.drm.DrmErrorEvent;
import android.drm.DrmEvent;
import android.drm.DrmInfo;
import android.drm.DrmInfoEvent;
import android.drm.DrmInfoRequest;
import android.drm.DrmManagerClient;
import android.drm.DrmManagerClient.OnErrorListener;
import android.drm.DrmManagerClient.OnEventListener;
import android.drm.DrmManagerClient.OnInfoListener;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.ErrorDialog;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.UrlHelper;
import java.util.HashMap;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ApiBuilder;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.User;

@TargetApi(11)
public class DrmManagerHc
  implements PlaybackManager
{
  private static final String[] PORTAL_KEYS = { "warnerbros", "CN", "IDM" };
  private final Handler ackLicenseSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.ackLicenseSuccessHandler: customData " + DrmManagerHc.this.customData);
    }
  };
  private String assetUri;
  private String captionsUri;
  private Context context;
  private String customData;
  private ProgressDialog dialog;
  private DrmManagerClient drmManagerClient;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (ActivityHolder.instance().getTopLevelActivity().isFinishing());
      while (true)
      {
        return;
        DrmManagerHc.this.trackPlaybackEvent("api_error");
        Logger.d("FlxDrm", "DrmManagerHc.errorHandler: " + paramAnonymousMessage);
        if (DrmManagerHc.this.dialog != null);
        try
        {
          DrmManagerHc.this.dialog.dismiss();
          if (!(paramAnonymousMessage.obj instanceof DaoException))
            continue;
          ErrorDialog.handleException((DaoException)paramAnonymousMessage.obj, ActivityHolder.instance().getTopLevelActivity(), 0);
          return;
        }
        catch (Exception localException)
        {
          while (true)
            localException.printStackTrace();
        }
      }
    }
  };
  private int playbackPosition;
  private WidevinePlayer player;
  private LockerRight r;
  private final Handler silentErrorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.silentErrorHandler: " + paramAnonymousMessage);
    }
  };
  private String streamId;
  private final Handler streamResumeSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HashMap localHashMap = (HashMap)paramAnonymousMessage.obj;
      DrmManagerHc.this.assetUri = ((String)localHashMap.get("fileLocation"));
      DrmManagerHc.this.streamId = ((String)localHashMap.get("streamId"));
      DrmManagerHc.this.customData = ((String)localHashMap.get("customData"));
    }
  };
  private final Handler streamStartSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      HashMap localHashMap = (HashMap)paramAnonymousMessage.obj;
      DrmManagerHc.this.assetUri = DrmManagerHc.convertWidevineProtocol((String)localHashMap.get("fileLocation"));
      DrmManagerHc.this.playbackPosition = ((Integer)localHashMap.get("playPosition")).intValue();
      DrmManagerHc.this.captionsUri = ((String)localHashMap.get("closedCaptionFileLocation"));
      if (DrmManagerHc.this.r.isSonicProxyMode())
      {
        DrmManagerHc.this.streamId = ((String)localHashMap.get("streamId"));
        DrmManagerHc.this.customData = ((String)localHashMap.get("customData"));
        String str1 = (String)localHashMap.get("drmServerUrl");
        String str2 = (String)localHashMap.get("deviceId");
        DrmManagerHc.this.wvPlay(DrmManagerHc.this.getDrmInfoRequest(str1, DrmManagerHc.this.streamId, str2), DrmManagerHc.this.playbackPosition);
        return;
      }
      ProfileDao.uvStreamCreate(DrmManagerHc.this.uvStreamCreateSuccessHandler, DrmManagerHc.this.errorHandler, DrmManagerHc.this.r);
    }
  };
  private final Handler streamStopSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.streamStopSuccessHandler " + DrmManagerHc.this.r + " streamId " + DrmManagerHc.this.streamId);
    }
  };
  private final Handler trackLicenseSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.trackLicenseSuccessHandler: streamId " + DrmManagerHc.this.streamId);
    }
  };
  private final Handler updatePlaybackPositionSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.updatePlaybackPositionSuccessHandler");
    }
  };
  private final Handler uvStreamCreateSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DrmManagerHc.this.streamId = ((String)paramAnonymousMessage.obj);
      Logger.d("FlxDrm", "DrmManagerHc.streamCreateSuccessHandler: proxy URL https://...");
      DrmManagerHc.this.wvPlay(DrmManagerHc.this.getDrmInfoRequest(), DrmManagerHc.this.playbackPosition);
    }
  };
  private final Handler uvStreamDeleteSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "DrmManagerHc.uvStreamDeleteSuccessHandler " + DrmManagerHc.this.r + " streamId " + DrmManagerHc.this.streamId);
    }
  };
  private final Handler uvStreamResumeSuccessHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DrmManagerHc.this.streamId = ((String)paramAnonymousMessage.obj);
      Logger.d("FlxDrm", "DrmManagerHc.streamResumeSuccessHandler " + DrmManagerHc.this.r + " streamId " + DrmManagerHc.this.streamId);
    }
  };

  private static String convertWidevineProtocol(String paramString)
  {
    if (FlixsterApplication.getAndroidBuildInt() >= 13)
      paramString = paramString.replace("http:", "widevine:");
    return paramString;
  }

  private DrmInfoRequest getDrmInfoRequest()
  {
    DrmInfoRequest localDrmInfoRequest = new DrmInfoRequest(3, "video/wvm");
    localDrmInfoRequest.put("WVDRMServerKey", ApiBuilder.widevineLicense(this.r, this.streamId));
    localDrmInfoRequest.put("WVAssetURIKey", this.assetUri);
    localDrmInfoRequest.put("WVPortalKey", PORTAL_KEYS[0]);
    localDrmInfoRequest.put("WVLicenseTypeKey", "1");
    String str = getWvDeviceId();
    if (str != null)
    {
      Logger.d("FlxDrm", "DrmManagerHc.getDrmInfoRequest WVDeviceIDKey = " + str);
      localDrmInfoRequest.put("WVDeviceIDKey", str);
    }
    return localDrmInfoRequest;
  }

  private DrmInfoRequest getDrmInfoRequest(String paramString1, String paramString2, String paramString3)
  {
    DrmInfoRequest localDrmInfoRequest = new DrmInfoRequest(3, "video/wvm");
    localDrmInfoRequest.put("WVDRMServerKey", paramString1);
    localDrmInfoRequest.put("WVAssetURIKey", this.assetUri);
    localDrmInfoRequest.put("WVPortalKey", PORTAL_KEYS[1]);
    localDrmInfoRequest.put("WVLicenseTypeKey", "1");
    localDrmInfoRequest.put("WVStreamIDKey", paramString2);
    localDrmInfoRequest.put("WVDeviceIDKey", paramString3);
    return localDrmInfoRequest;
  }

  private static String getWvDeviceId()
  {
    User localUser = AccountManager.instance().getUser();
    String str = null;
    if (localUser != null)
      str = localUser.getId() + "@WVM-" + FlixsterApplication.getHashedUid();
    return str;
  }

  private void trackPlayback()
  {
    Trackers.instance().track(getPlaybackTag(), getPlaybackTitle());
  }

  private void trackPlaybackEvent(String paramString)
  {
    Trackers.instance().trackEvent(getPlaybackTag(), getPlaybackTitle(), "Streaming", UrlHelper.urlEncode(Build.MODEL), paramString, 0);
  }

  public String getPlaybackTag()
  {
    return "/movie/play/streaming";
  }

  public String getPlaybackTitle()
  {
    StringBuilder localStringBuilder = new StringBuilder("Movie Play Streaming - ");
    if (this.r != null);
    for (String str = this.r.getTitle(); ; str = null)
      return str;
  }

  public WidevinePlayer getPlayer()
  {
    return this.player;
  }

  public void initializeDRM(Context paramContext)
  {
    this.context = paramContext;
    if (this.drmManagerClient == null)
    {
      Logger.d("FlxDrm", "DrmManagerHc.initializeDRM: new DrmManagerClient");
      this.drmManagerClient = new DrmManagerClient(paramContext);
      this.drmManagerClient.setOnInfoListener(new DrmManagerClient.OnInfoListener()
      {
        public void onInfo(DrmManagerClient paramAnonymousDrmManagerClient, DrmInfoEvent paramAnonymousDrmInfoEvent)
        {
          if (paramAnonymousDrmInfoEvent.getType() == 3)
            Logger.d("FlxDrm", "Rights installed");
        }
      });
      this.drmManagerClient.setOnEventListener(new DrmManagerClient.OnEventListener()
      {
        public void onEvent(DrmManagerClient paramAnonymousDrmManagerClient, DrmEvent paramAnonymousDrmEvent)
        {
          switch (paramAnonymousDrmEvent.getType())
          {
          default:
          case 1002:
          }
          do
          {
            return;
            Logger.d("FlxDrm", "Info Processed\n");
          }
          while (!DrmManagerHc.this.r.isSonicProxyMode());
          ProfileDao.trackLicense(DrmManagerHc.this.trackLicenseSuccessHandler, DrmManagerHc.this.silentErrorHandler, DrmManagerHc.this.r, DrmManagerHc.this.streamId, true);
          ProfileDao.ackLicense(DrmManagerHc.this.ackLicenseSuccessHandler, DrmManagerHc.this.silentErrorHandler, DrmManagerHc.this.customData);
        }
      });
      this.drmManagerClient.setOnErrorListener(new DrmManagerClient.OnErrorListener()
      {
        public void onError(DrmManagerClient paramAnonymousDrmManagerClient, DrmErrorEvent paramAnonymousDrmErrorEvent)
        {
          switch (paramAnonymousDrmErrorEvent.getType())
          {
          default:
          case 1001:
          case 1002:
          case 2005:
          case 2003:
          case 2004:
          case 2006:
          case 2007:
          case 2001:
          case 2002:
          }
          while (true)
          {
            Logger.e("FlxDrm", "DrmErrorEvent message: " + paramAnonymousDrmErrorEvent.getMessage());
            return;
            Logger.e("FlxDrm", "Remove All Rights failed\n");
            continue;
            Logger.e("FlxDrm", "Info Processed failed\n");
            continue;
            Logger.e("FlxDrm", "No Internet Connection\n");
            continue;
            Logger.e("FlxDrm", "Not Supported\n");
            continue;
            Logger.e("FlxDrm", "Out of Memory\n");
            continue;
            if (DrmManagerHc.this.player != null)
            {
              WidevinePlayer.PlaybackError localPlaybackError = WidevinePlayer.PlaybackError.LICENSE;
              DrmManagerHc.this.player.notifyPlaybackFailure(localPlaybackError, null);
            }
            if (DrmManagerHc.this.r.isSonicProxyMode())
              ProfileDao.trackLicense(DrmManagerHc.this.trackLicenseSuccessHandler, DrmManagerHc.this.silentErrorHandler, DrmManagerHc.this.r, DrmManagerHc.this.streamId, false);
            Logger.e("FlxDrm", "Process DRM Info failed\n");
            continue;
            Logger.e("FlxDrm", "Remove all rights\n");
            continue;
            Logger.e("FlxDrm", "Rights not installed\n");
            continue;
            Logger.e("FlxDrm", "Rights renewal not allowed\n");
          }
        }
      });
    }
  }

  public boolean isInitialized()
  {
    return true;
  }

  public boolean isRooted()
  {
    return false;
  }

  public boolean isStreamingMode()
  {
    return true;
  }

  public void playMovie(LockerRight paramLockerRight)
  {
    this.r = paramLockerRight;
    Logger.d("FlxDrm", "DrmManagerHc.playMovie " + paramLockerRight);
    if (this.context == null)
      return;
    String str1 = this.context.getResources().getString(2131493172);
    Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
    if ((localActivity != null) && (!localActivity.isFinishing()))
    {
      this.dialog = new ProgressDialog(localActivity);
      this.dialog.setMessage(str1);
      this.dialog.setIndeterminate(true);
      this.dialog.setCancelable(false);
      this.dialog.setCanceledOnTouchOutside(false);
      this.dialog.show();
    }
    trackPlayback();
    trackPlaybackEvent("stream_create");
    Handler localHandler1 = this.streamStartSuccessHandler;
    Handler localHandler2 = this.errorHandler;
    LockerRight localLockerRight = this.r;
    if (this.r.isSonicProxyMode());
    for (String str2 = "low"; ; str2 = "adaptive")
    {
      ProfileDao.streamStart(localHandler1, localHandler2, localLockerRight, str2);
      return;
    }
  }

  public void registerAllLocalAssets(List<LockerRight> paramList)
  {
    throw new UnsupportedOperationException();
  }

  public void registerAssetOnDownloadComplete(LockerRight paramLockerRight)
  {
    throw new UnsupportedOperationException();
  }

  public void setPlayer(WidevinePlayer paramWidevinePlayer)
  {
    this.player = paramWidevinePlayer;
  }

  public void streamResume()
  {
    Logger.d("FlxDrm", "DrmManagerHc.streamResume");
    trackPlaybackEvent("stream_resume");
    if (this.r.isSonicProxyMode())
    {
      ProfileDao.streamStart(this.streamResumeSuccessHandler, this.silentErrorHandler, this.r, "adaptive");
      return;
    }
    ProfileDao.uvStreamCreate(this.uvStreamResumeSuccessHandler, this.silentErrorHandler, this.r);
  }

  public void streamStop(int paramInt)
  {
    Logger.d("FlxDrm", "DrmManagerHc.streamStop");
    trackPlaybackEvent("stream_delete");
    Handler localHandler1 = this.streamStopSuccessHandler;
    Handler localHandler2 = this.silentErrorHandler;
    LockerRight localLockerRight = this.r;
    String str1 = this.streamId;
    if (paramInt >= 0);
    for (String str2 = Integer.toString(paramInt); ; str2 = null)
    {
      ProfileDao.streamStop(localHandler1, localHandler2, localLockerRight, str1, str2);
      if (!this.r.isSonicProxyMode())
        ProfileDao.uvStreamDelete(this.uvStreamDeleteSuccessHandler, this.silentErrorHandler, this.streamId);
      return;
    }
  }

  public void terminateDRM()
  {
  }

  public void unregisterLocalAsset(LockerRight paramLockerRight)
  {
    throw new UnsupportedOperationException();
  }

  public void updatePlaybackPosition(int paramInt)
  {
    Logger.d("FlxDrm", "DrmManagerHc.updatePlaybackPosition");
    ProfileDao.updatePlaybackPosition(this.updatePlaybackPositionSuccessHandler, this.silentErrorHandler, this.r, Integer.toString(paramInt));
  }

  protected void wvPlay(DrmInfoRequest paramDrmInfoRequest, int paramInt)
  {
    Logger.d("FlxDrm", "DrmManagerHc.wvPlay: " + this.assetUri);
    String str = this.assetUri;
    DrmInfo localDrmInfo = this.drmManagerClient.acquireDrmInfo(paramDrmInfoRequest);
    try
    {
      this.drmManagerClient.processDrmInfo(localDrmInfo);
      if (this.dialog == null);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      try
      {
        this.dialog.dismiss();
        Intent localIntent = new Intent();
        localIntent.setAction("android.intent.action.VIEW");
        localIntent.setDataAndType(Uri.parse(str), "video/*");
        localIntent.addFlags(268435456);
        localIntent.putExtra("KEY_RIGHT_ID", this.r.rightId);
        localIntent.putExtra("KEY_PLAY_POSITION", paramInt);
        localIntent.putExtra("KEY_CAPTIONS_URI", this.captionsUri);
        localIntent.setClass(this.context, WidevinePlayer.class);
        this.context.startActivity(localIntent);
        do
        {
          return;
          localIllegalArgumentException = localIllegalArgumentException;
          Logger.e("FlxDrm", "DrmManagerClient.processDrmInfo received invalid drmInfo, exiting", localIllegalArgumentException);
        }
        while (this.dialog == null);
        try
        {
          this.dialog.dismiss();
          return;
        }
        catch (Exception localException1)
        {
          localException1.printStackTrace();
          return;
        }
      }
      catch (Exception localException2)
      {
        while (true)
          localException2.printStackTrace();
      }
    }
  }

  public Uri wvResume()
  {
    return null;
  }

  public void wvStop()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.DrmManagerHc
 * JD-Core Version:    0.6.2
 */