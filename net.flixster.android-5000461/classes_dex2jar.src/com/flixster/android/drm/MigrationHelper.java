package com.flixster.android.drm;

import com.flixster.android.storage.ExternalStorage;
import com.flixster.android.utils.Logger;
import java.util.Collection;
import java.util.Iterator;
import net.flixster.android.model.LockerRight.RightType;
import net.flixster.android.model.User;

public class MigrationHelper
{
  public static final String WV_DIR = "wv/";

  private static long extractAssetId(String paramString)
  {
    long l1 = 0L;
    int i = paramString.lastIndexOf("/");
    int j = paramString.lastIndexOf(".");
    String str;
    if ((i > -1) && (j > -1))
      str = paramString.substring(i + 1, j).split("_")[0];
    try
    {
      long l2 = Long.valueOf(str).longValue();
      l1 = l2;
      return l1;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return l1;
  }

  private static void migrateAssets(User paramUser)
  {
    Collection localCollection = ExternalStorage.findFiles("wv/", ".mp4");
    if (localCollection == null);
    while (true)
    {
      return;
      Iterator localIterator = localCollection.iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if ((str.endsWith("_0.mp4")) || (str.endsWith("_1000.mp4")))
        {
          Logger.i("FlxDrm", "MigrationHelper.migrateAssets migrating " + str);
          long l1 = extractAssetId(str);
          if (l1 > 0L)
          {
            long l2 = paramUser.getLockerRightId(l1);
            if (l2 != 0L)
              ExternalStorage.renameFile(str, "wv/", l2 + ".mp4");
            else
              Logger.w("FlxDrm", "MigrationHelper.migrateAssets no locker right for asset " + l1);
          }
          else
          {
            Logger.w("FlxDrm", "MigrationHelper.migrateAssets failed to extract asset id " + str);
          }
        }
        else
        {
          Logger.w("FlxDrm", "MigrationHelper.migrateAssets unknown .mp4 file " + str);
        }
      }
    }
  }

  public static void migrateDownloadedFilesForRightsApi(User paramUser)
  {
    migrateAssets(paramUser);
    migrateLocks(paramUser);
    migrateRights(paramUser);
  }

  private static void migrateLocks(User paramUser)
  {
    Collection localCollection = ExternalStorage.findFiles("wv/", ".lck");
    if (localCollection == null);
    while (true)
    {
      return;
      Iterator localIterator = localCollection.iterator();
      while (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        String str2 = ExternalStorage.readFile(str1);
        String[] arrayOfString = (String[])null;
        if (str2 != null)
          arrayOfString = str2.split("ZZZZ");
        if ((arrayOfString != null) && (arrayOfString.length == 4))
        {
          Logger.i("FlxDrm", "MigrationHelper.migrateLocks migrating " + str1);
          long l1 = Long.valueOf(arrayOfString[1]).longValue();
          String str3 = arrayOfString[2];
          long l2 = paramUser.getLockerRightId(l1);
          if (l2 != 0L)
          {
            DownloadLock localDownloadLock = new DownloadLock(l2);
            localDownloadLock.setDownloadId(Long.valueOf(arrayOfString[0]).longValue());
            localDownloadLock.setTitle(arrayOfString[3]);
            if ("1000".equals(str3));
            for (String str4 = LockerRight.RightType.EPISODE.name(); ; str4 = LockerRight.RightType.MOVIE.name())
            {
              localDownloadLock.setRightType(str4);
              localDownloadLock.setAssetId(l1);
              localDownloadLock.save();
              ExternalStorage.deleteFile(str1);
              break;
            }
          }
          Logger.w("FlxDrm", "MigrationHelper.migrateLocks no locker right for asset " + l1);
        }
        else
        {
          Logger.w("FlxDrm", "MigrationHelper.migrateLocks invalid .lck file " + str1 + " " + str2);
        }
      }
    }
  }

  private static void migrateRights(User paramUser)
  {
    Collection localCollection = ExternalStorage.findFiles("wv/", ".params");
    if (localCollection == null);
    while (true)
    {
      return;
      Iterator localIterator = localCollection.iterator();
      while (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        String str2 = ExternalStorage.readFile(str1);
        String[] arrayOfString = (String[])null;
        if (str2 != null)
          arrayOfString = str2.split("ZZZZ");
        if ((arrayOfString != null) && (arrayOfString.length == 4))
        {
          long l1 = extractAssetId(str1);
          if (l1 > 0L)
          {
            Logger.i("FlxDrm", "MigrationHelper.migrateRights migrating " + str1);
            long l2 = paramUser.getLockerRightId(l1);
            if (l2 != 0L)
            {
              DownloadRight localDownloadRight = new DownloadRight(l2);
              localDownloadRight.setDownloadUri(arrayOfString[0]);
              localDownloadRight.setLicenseProxy(arrayOfString[1]);
              localDownloadRight.setQueueId(arrayOfString[2]);
              localDownloadRight.setDeviceId(arrayOfString[3]);
              localDownloadRight.save();
              ExternalStorage.deleteFile(str1);
            }
            else
            {
              Logger.w("FlxDrm", "MigrationHelper.migrateRights no locker right for asset " + l1);
            }
          }
          else
          {
            Logger.w("FlxDrm", "MigrationHelper.migrateRights failed to extract asset id " + str1);
          }
        }
        else
        {
          Logger.w("FlxDrm", "MigrationHelper.migrateRights invalid .params file " + str1 + " " + str2);
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.MigrationHelper
 * JD-Core Version:    0.6.2
 */