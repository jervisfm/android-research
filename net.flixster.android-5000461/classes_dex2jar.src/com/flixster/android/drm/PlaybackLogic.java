package com.flixster.android.drm;

import net.flixster.android.model.LockerRight;

public abstract interface PlaybackLogic
{
  public abstract boolean isAssetDownloadable(LockerRight paramLockerRight);

  public abstract boolean isAssetPlayable(LockerRight paramLockerRight);

  public abstract boolean isDeviceDownloadCompatible();

  public abstract boolean isDeviceNonWifiStreamBlacklisted();

  public abstract boolean isDeviceStreamBlacklisted();

  public abstract boolean isDeviceStreamingCompatible();

  public abstract boolean isNativeWv();

  public abstract void setDeviceNonWifiStreamBlacklisted(boolean paramBoolean);

  public abstract void setDeviceStreamBlacklisted(boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.PlaybackLogic
 * JD-Core Version:    0.6.2
 */