package com.flixster.android.drm;

import android.content.Context;
import android.net.Uri;
import java.util.List;
import net.flixster.android.model.LockerRight;

public abstract interface PlaybackManager
{
  public abstract String getPlaybackTag();

  public abstract String getPlaybackTitle();

  public abstract WidevinePlayer getPlayer();

  public abstract void initializeDRM(Context paramContext);

  public abstract boolean isInitialized();

  public abstract boolean isRooted();

  public abstract boolean isStreamingMode();

  public abstract void playMovie(LockerRight paramLockerRight);

  public abstract void registerAllLocalAssets(List<LockerRight> paramList);

  public abstract void registerAssetOnDownloadComplete(LockerRight paramLockerRight);

  public abstract void setPlayer(WidevinePlayer paramWidevinePlayer);

  public abstract void streamResume();

  public abstract void streamStop(int paramInt);

  public abstract void terminateDRM();

  public abstract void unregisterLocalAsset(LockerRight paramLockerRight);

  public abstract void updatePlaybackPosition(int paramInt);

  public abstract Uri wvResume();

  public abstract void wvStop();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.PlaybackManager
 * JD-Core Version:    0.6.2
 */