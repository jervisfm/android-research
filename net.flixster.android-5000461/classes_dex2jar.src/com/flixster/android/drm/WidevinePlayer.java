package com.flixster.android.drm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.net.TimedTextElement;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.UrlHelper;
import com.flixster.android.utils.VersionedViewHelper;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.MiscDao;

public class WidevinePlayer extends Activity
{
  private static final int CAPTION_MONITOR_INTERVAL_MS = 300;
  private static final int CAPTION_REGIONS = 4;
  public static final String KEY_CAPTIONS_URI = "KEY_CAPTIONS_URI";
  public static final String KEY_PLAY_POSITION = "KEY_PLAY_POSITION";
  public static final String KEY_RIGHT_ID = "KEY_RIGHT_ID";
  private static final int LOADING_TIME_ETHERNET = 5;
  private static final int LOADING_TIME_MOBILE = 60;
  private static final int LOADING_TIME_WIFI = 20;
  private static final int LOADING_TIME_WIMAX = 30;
  private static final int PROGRESS_MONITOR_INTERVAL_MS = 1000;
  private final Handler captionHandler = new Handler()
  {
    int currentPosition;
    int ttIndex;

    public void handleMessage(Message paramAnonymousMessage)
    {
      int i;
      int j;
      if (WidevinePlayer.this.captions != null)
      {
        i = WidevinePlayer.this.outMetrics.widthPixels;
        j = WidevinePlayer.this.outMetrics.heightPixels;
        if (WidevinePlayer.this.videoView.getCurrentPosition() < this.currentPosition)
          this.ttIndex = 0;
      }
      int k;
      for (int m = 0; ; m++)
      {
        if (m >= 4)
        {
          this.currentPosition = WidevinePlayer.this.videoView.getCurrentPosition();
          k = this.ttIndex;
          if (k < WidevinePlayer.this.captions.size())
            break;
          label100: return;
        }
        WidevinePlayer.this.captionViews[m].setVisibility(8);
      }
      TimedTextElement localTimedTextElement = (TimedTextElement)WidevinePlayer.this.captions.get(k);
      if (localTimedTextElement.end <= this.currentPosition)
      {
        if (WidevinePlayer.this.captionViews[localTimedTextElement.region].getVisibility() == 0)
        {
          Logger.d("FlxDrm", "hiding index " + k + ", text " + localTimedTextElement.text);
          WidevinePlayer.this.captionViews[localTimedTextElement.region].setVisibility(8);
        }
        this.ttIndex = (1 + this.ttIndex);
      }
      while (true)
      {
        k++;
        break;
        if (localTimedTextElement.begin > this.currentPosition)
          break label100;
        if (WidevinePlayer.this.captionViews[localTimedTextElement.region].getVisibility() == 8)
        {
          Logger.d("FlxDrm", "showing index " + k + ", text " + localTimedTextElement.text);
          ((ViewGroup.MarginLayoutParams)WidevinePlayer.this.captionViews[localTimedTextElement.region].getLayoutParams()).setMargins(i * localTimedTextElement.originX / 100, j * localTimedTextElement.originY / 100, 0, 0);
          WidevinePlayer.this.captionViews[localTimedTextElement.region].setText(localTimedTextElement.text);
          WidevinePlayer.this.captionViews[localTimedTextElement.region].setVisibility(0);
        }
      }
    }
  };
  private TextView[] captionViews;
  private List<TimedTextElement> captions;
  private String captionsUri;
  private ProgressDialog dialog;
  private AlertDialog errorDialog;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "WidevinePlayer.errorHandler fetch captions failed");
    }
  };
  private ErrorListener errorListener;
  private boolean hasPlaybackFailed;
  private boolean isPlaybackActive;
  private boolean isPlaybackCompleted;
  private boolean isPlaybackPrepared;
  private DisplayMetrics outMetrics;
  private final Handler progressHandler = new Handler()
  {
    int estimatedLoadingTime = WidevinePlayer.this.getEstimatedLoadingTime();

    public void handleMessage(Message paramAnonymousMessage)
    {
      int i = 5 * (int)(20.0D * ((SystemClock.uptimeMillis() - WidevinePlayer.this.startLoadingTime) / 1000L / this.estimatedLoadingTime));
      if (i < 100)
        WidevinePlayer.this.dialog.setMessage(WidevinePlayer.this.getResources().getString(2131493173) + " " + Integer.toString(i) + "%");
    }
  };
  private long rightId;
  private final Handler showErrorDialogHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (WidevinePlayer.this.isFinishing())
      {
        Logger.d("FlxDrm", "WidevinePlayer.showErrorDialogHandler activity finished");
        return;
      }
      WidevinePlayer.this.dialog.hide();
      int i = paramAnonymousMessage.what;
      WidevinePlayer.PlaybackError localPlaybackError = WidevinePlayer.PlaybackError.UNKNOWN;
      if (i == 0)
        localPlaybackError = (WidevinePlayer.PlaybackError)paramAnonymousMessage.obj;
      String str;
      switch ($SWITCH_TABLE$com$flixster$android$drm$WidevinePlayer$PlaybackError()[localPlaybackError.ordinal()])
      {
      default:
        str = WidevinePlayer.this.getString(2131493243);
      case 2:
      case 3:
      }
      while (true)
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(WidevinePlayer.this);
        localBuilder.setTitle(2131493242).setMessage(str).setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
          {
            WidevinePlayer.this.finish();
          }
        }).setOnCancelListener(new DialogInterface.OnCancelListener()
        {
          public void onCancel(DialogInterface paramAnonymous2DialogInterface)
          {
            WidevinePlayer.this.finish();
          }
        });
        WidevinePlayer.this.errorDialog = localBuilder.create();
        WidevinePlayer.this.errorDialog.show();
        return;
        str = WidevinePlayer.this.getString(2131493244);
        continue;
        str = WidevinePlayer.this.getString(2131493245);
      }
    }
  };
  private long startLoadingTime;
  private int startTime;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Logger.d("FlxDrm", "WidevinePlayer.successHandler fetch captions succeeded");
      WidevinePlayer.this.captions = ((List)paramAnonymousMessage.obj);
    }
  };
  private VideoView videoView;

  private int getEstimatedLoadingTime()
  {
    String str = FlixsterApplication.getConnectionType();
    int i = FlixsterApplication.getContext().getSharedPreferences("estimatedLoadingTime", 0).getInt("loadingTime_" + str, 0);
    int j;
    if (i == 0)
      if (str.toUpperCase().equals("ETHERNET"))
        j = 5;
    while (true)
    {
      Logger.d("FlxDrm", "WidevinePlayer.getEstimatedLoadingTime: " + j + " for network type " + str);
      return j;
      if (str.toUpperCase().equals("WIFI"))
      {
        j = 20;
      }
      else if (str.toUpperCase().equals("WIMAX"))
      {
        j = 30;
      }
      else
      {
        j = 60;
        continue;
        j = i;
      }
    }
  }

  private void trackPlaybackEvent(String paramString, int paramInt)
  {
    Trackers.instance().trackEvent(Drm.manager().getPlaybackTag(), Drm.manager().getPlaybackTitle(), "Streaming", UrlHelper.urlEncode(Build.MODEL), paramString, paramInt);
  }

  public int getPlaybackElapsedDuration()
  {
    if (this.isPlaybackPrepared)
      return (this.videoView.getCurrentPosition() - this.startTime) / 1000;
    return 0;
  }

  public void notifyPlaybackFailure(PlaybackError paramPlaybackError, String paramString)
  {
    this.errorListener.onError(paramPlaybackError, paramString);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Logger.d("FlxDrm", "WidevinePlayer.onCreate");
    int i;
    SharedPreferences localSharedPreferences;
    if (FlixsterApplication.getAndroidBuildInt() < 9)
    {
      setRequestedOrientation(0);
      VersionedViewHelper.hideSystemNavigation(this);
      getWindow().addFlags(128);
      this.outMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(this.outMetrics);
      Drm.manager().setPlayer(this);
      setContentView(2130903186);
      this.videoView = ((VideoView)findViewById(2131165917));
      this.videoView.setMediaController(new MediaController(this));
      VideoView localVideoView = this.videoView;
      ErrorListener localErrorListener = new ErrorListener(null);
      this.errorListener = localErrorListener;
      localVideoView.setOnErrorListener(localErrorListener);
      this.videoView.setOnPreparedListener(new PreparedListener(null));
      this.videoView.setOnCompletionListener(new CompletionListener(null));
      this.videoView.requestFocus();
      this.dialog = new ProgressDialog(this);
      this.dialog.setCancelable(true);
      Intent localIntent = getIntent();
      this.rightId = localIntent.getLongExtra("KEY_RIGHT_ID", 0L);
      i = localIntent.getIntExtra("KEY_PLAY_POSITION", 0);
      this.captionsUri = localIntent.getStringExtra("KEY_CAPTIONS_URI");
      localSharedPreferences = getSharedPreferences("restartTime", 0);
      if (i < 0)
        break label380;
    }
    label380: for (int j = i * 1000; ; j = localSharedPreferences.getInt(String.valueOf(this.rightId), 0))
    {
      this.startTime = j;
      this.isPlaybackActive = true;
      if ((FlixsterApplication.getCaptionsEnabled()) && (this.captionsUri != null))
      {
        this.captionViews = new TextView[4];
        this.captionViews[0] = ((TextView)findViewById(2131165922));
        this.captionViews[1] = ((TextView)findViewById(2131165923));
        this.captionViews[2] = ((TextView)findViewById(2131165924));
        this.captionViews[3] = ((TextView)findViewById(2131165925));
        MiscDao.fetchCaptions(this.successHandler, this.errorHandler, this.captionsUri);
      }
      return;
      setRequestedOrientation(6);
      break;
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    Logger.d("FlxDrm", "WidevinePlayer.onDestroy");
    Drm.manager().setPlayer(null);
    try
    {
      this.dialog.dismiss();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public void onPause()
  {
    super.onPause();
    Logger.d("FlxDrm", "WidevinePlayer.onPause");
    if (this.isPlaybackPrepared)
    {
      SharedPreferences.Editor localEditor = getSharedPreferences("restartTime", 0).edit();
      int j = this.videoView.getCurrentPosition();
      if ((j - this.startTime) / 1000 > 1)
      {
        String str = FlixsterApplication.getConnectionType();
        trackPlaybackEvent("playback_duration_" + str, (j - this.startTime) / 1000);
      }
      if (this.isPlaybackCompleted)
        j = 0;
      this.startTime = j;
      localEditor.putInt(String.valueOf(this.rightId), this.startTime);
      localEditor.commit();
      Logger.d("FlxDrm", "WidevinePlayer.onPause rightId " + this.rightId + ", saving seek time " + this.startTime);
    }
    this.videoView.stopPlayback();
    this.videoView.setVideoURI(null);
    this.videoView.setVisibility(8);
    Drm.manager().wvStop();
    this.isPlaybackActive = false;
    PlaybackManager localPlaybackManager;
    int i;
    if (Drm.manager().isStreamingMode())
    {
      localPlaybackManager = Drm.manager();
      if (this.isPlaybackPrepared)
        i = this.startTime / 1000;
    }
    while (true)
    {
      localPlaybackManager.streamStop(i);
      try
      {
        while (true)
        {
          if (this.errorDialog != null)
            this.errorDialog.dismiss();
          return;
          i = -1;
          break;
          if ((this.isPlaybackPrepared) && (FlixsterApplication.isConnected()))
            Drm.manager().updatePlaybackPosition(this.startTime / 1000);
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
  }

  public void onResume()
  {
    super.onResume();
    Logger.d("FlxDrm", "WidevinePlayer.onResume");
    String str = FlixsterApplication.getConnectionType();
    trackPlaybackEvent("playback_init_" + str, 0);
    if ((!this.isPlaybackActive) && (Drm.manager().isStreamingMode()))
      Drm.manager().streamResume();
    Uri localUri;
    if ((!this.isPlaybackActive) && (!Drm.logic().isNativeWv()))
    {
      localUri = Drm.manager().wvResume();
      if (localUri == null)
        finish();
    }
    while (true)
    {
      this.isPlaybackActive = true;
      this.isPlaybackPrepared = false;
      this.hasPlaybackFailed = false;
      this.videoView.setVisibility(0);
      this.videoView.setVideoURI(localUri);
      this.dialog.setCanceledOnTouchOutside(false);
      this.dialog.setMessage(getResources().getString(2131493173));
      this.dialog.show();
      this.startLoadingTime = SystemClock.uptimeMillis();
      new Thread(new Runnable()
      {
        public void run()
        {
          while (true)
          {
            if ((WidevinePlayer.this.isPlaybackPrepared) || (!WidevinePlayer.this.isPlaybackActive) || (WidevinePlayer.this.hasPlaybackFailed))
              return;
            WidevinePlayer.this.progressHandler.sendEmptyMessage(0);
            try
            {
              Thread.sleep(1000L);
            }
            catch (InterruptedException localInterruptedException)
            {
              localInterruptedException.printStackTrace();
            }
          }
        }
      }).start();
      return;
      localUri = getIntent().getData();
    }
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if ((paramBoolean) && (this.videoView.isPlaying()))
      VersionedViewHelper.hideSystemNavigation(this);
  }

  private class CompletionListener
    implements MediaPlayer.OnCompletionListener
  {
    private CompletionListener()
    {
    }

    public void onCompletion(MediaPlayer paramMediaPlayer)
    {
      WidevinePlayer.this.trackPlaybackEvent("playback_complete", 0);
      Logger.d("FlxDrm", "WidevinePlayer.onCompletion rightId " + WidevinePlayer.this.rightId + ", resetting seek time");
      WidevinePlayer.this.isPlaybackCompleted = true;
      WidevinePlayer.this.finish();
    }
  }

  private class ErrorListener
    implements MediaPlayer.OnErrorListener
  {
    private ErrorListener()
    {
    }

    public void onError(WidevinePlayer.PlaybackError paramPlaybackError, String paramString)
    {
      WidevinePlayer localWidevinePlayer;
      StringBuilder localStringBuilder;
      if (!WidevinePlayer.this.hasPlaybackFailed)
      {
        WidevinePlayer.this.hasPlaybackFailed = true;
        Logger.w("FlxDrm", "WidevinePlayer.onError " + paramPlaybackError + ", error " + paramString);
        String str1 = FlixsterApplication.getConnectionType();
        localWidevinePlayer = WidevinePlayer.this;
        localStringBuilder = new StringBuilder("playback_error_").append(str1).append("_").append(paramPlaybackError);
        if (paramString == null)
          break label138;
      }
      label138: for (String str2 = "_" + paramString; ; str2 = "")
      {
        localWidevinePlayer.trackPlaybackEvent(str2, 0);
        WidevinePlayer.this.showErrorDialogHandler.sendMessage(Message.obtain(null, 0, paramPlaybackError));
        return;
      }
    }

    public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
    {
      if (!WidevinePlayer.this.hasPlaybackFailed)
      {
        WidevinePlayer.this.hasPlaybackFailed = true;
        Logger.w("FlxDrm", "WidevinePlayer.onError: " + paramInt1 + "_" + paramInt2);
        String str = FlixsterApplication.getConnectionType();
        WidevinePlayer.this.trackPlaybackEvent("playback_error_" + str + "_" + Integer.toString(paramInt1) + "_" + Integer.toString(paramInt2), 0);
        WidevinePlayer.this.showErrorDialogHandler.sendEmptyMessage(paramInt1);
      }
      return true;
    }
  }

  private class InfoListener
    implements MediaPlayer.OnInfoListener
  {
    private InfoListener()
    {
    }

    public boolean onInfo(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
    {
      Logger.d("FlxDrm", "WidevinePlayer.onInfo: " + paramInt1);
      switch (paramInt1)
      {
      default:
        return true;
      case 701:
        WidevinePlayer.this.dialog.setCanceledOnTouchOutside(true);
        WidevinePlayer.this.dialog.setMessage(WidevinePlayer.this.getResources().getString(2131493174));
        WidevinePlayer.this.dialog.show();
        return true;
      case 702:
      }
      WidevinePlayer.this.dialog.hide();
      return true;
    }
  }

  public static enum PlaybackError
  {
    static
    {
      PlaybackError[] arrayOfPlaybackError = new PlaybackError[4];
      arrayOfPlaybackError[0] = LICENSE;
      arrayOfPlaybackError[1] = NETWORK;
      arrayOfPlaybackError[2] = SECURITY;
      arrayOfPlaybackError[3] = UNKNOWN;
    }
  }

  private class PreparedListener
    implements MediaPlayer.OnPreparedListener
  {
    private PreparedListener()
    {
    }

    public void onPrepared(MediaPlayer paramMediaPlayer)
    {
      if (WidevinePlayer.this.isFinishing())
        Logger.d("FlxDrm", "WidevinePlayer.onPrepared activity finished");
      do
      {
        do
          return;
        while (WidevinePlayer.this.hasPlaybackFailed);
        String str = FlixsterApplication.getConnectionType();
        int i = (int)(SystemClock.uptimeMillis() - WidevinePlayer.this.startLoadingTime) / 1000;
        WidevinePlayer.this.trackPlaybackEvent("loading_time_" + str, i);
        WidevinePlayer.this.dialog.hide();
        SharedPreferences.Editor localEditor = WidevinePlayer.this.getSharedPreferences("estimatedLoadingTime", 0).edit();
        localEditor.putInt("loadingTime_" + str, i);
        localEditor.commit();
        paramMediaPlayer.setOnInfoListener(new WidevinePlayer.InfoListener(WidevinePlayer.this, null));
        if ((!Properties.instance().isGoogleTv()) && (WidevinePlayer.this.startTime >= WidevinePlayer.this.videoView.getDuration()))
          WidevinePlayer.this.startTime = 0;
        WidevinePlayer.this.videoView.seekTo(WidevinePlayer.this.startTime);
        WidevinePlayer.this.videoView.start();
        WidevinePlayer.this.isPlaybackPrepared = true;
        Logger.d("FlxDrm", "WidevinePlayer.onPrepared rightId " + WidevinePlayer.this.rightId + ", seeking to " + WidevinePlayer.this.startTime);
      }
      while ((!FlixsterApplication.getCaptionsEnabled()) || (WidevinePlayer.this.captionsUri == null));
      new Thread(new Runnable()
      {
        public void run()
        {
          while (true)
          {
            if ((!WidevinePlayer.this.isPlaybackActive) || (WidevinePlayer.this.hasPlaybackFailed))
              return;
            WidevinePlayer.this.captionHandler.sendEmptyMessage(0);
            try
            {
              Thread.sleep(300L);
            }
            catch (InterruptedException localInterruptedException)
            {
              localInterruptedException.printStackTrace();
            }
          }
        }
      }).start();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.drm.WidevinePlayer
 * JD-Core Version:    0.6.2
 */