package com.flixster.android.model;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public abstract interface HeadlineItem
{
  public abstract <T extends ImageView> Bitmap getBitmap(T paramT, ImageView.ScaleType paramScaleType);

  public abstract String getImageUrl();

  public abstract String getLinkUrl();

  public abstract String getTitle();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.model.HeadlineItem
 * JD-Core Version:    0.6.2
 */