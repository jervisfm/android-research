package com.flixster.android.model;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.flixster.android.activity.ImageViewHandler;
import com.flixster.android.utils.ImageGetter;
import java.lang.ref.SoftReference;

public class Image
{
  private Handler callback;
  protected String imageUrl;
  private boolean isLoading;
  private SoftReference<Bitmap> softBitmap;

  protected Image()
  {
  }

  public Image(String paramString)
  {
    this.imageUrl = paramString;
  }

  private void load(Handler paramHandler)
  {
    try
    {
      if ((this.imageUrl != null) && (!this.isLoading))
      {
        this.isLoading = true;
        this.callback = paramHandler;
        ImageGetter.instance().get(this.imageUrl, new ImageHandler(null));
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public <T extends ImageView> Bitmap getBitmap(T paramT)
  {
    return getBitmap(paramT, null);
  }

  public <T extends ImageView> Bitmap getBitmap(T paramT, ImageView.ScaleType paramScaleType)
  {
    SoftReference localSoftReference = this.softBitmap;
    Bitmap localBitmap = null;
    if (localSoftReference != null)
    {
      localBitmap = (Bitmap)this.softBitmap.get();
      if (localBitmap != null);
    }
    else if (paramT != null)
    {
      load(new ImageViewHandler(paramT, this.imageUrl, paramScaleType));
    }
    return localBitmap;
  }

  public Bitmap getBitmap(ImageViewHandler paramImageViewHandler)
  {
    SoftReference localSoftReference = this.softBitmap;
    Bitmap localBitmap = null;
    if (localSoftReference != null)
    {
      localBitmap = (Bitmap)this.softBitmap.get();
      if (localBitmap != null);
    }
    else if (paramImageViewHandler != null)
    {
      load(paramImageViewHandler);
    }
    return localBitmap;
  }

  private class ImageHandler extends Handler
  {
    private ImageHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      Image.this.isLoading = false;
      Bitmap localBitmap = (Bitmap)paramMessage.obj;
      Image.this.softBitmap = new SoftReference(localBitmap);
      if (Image.this.callback != null)
      {
        Image.this.callback.handleMessage(paramMessage);
        Image.this.callback = null;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.model.Image
 * JD-Core Version:    0.6.2
 */