package com.flixster.android.model;

import java.util.List;

public class NamedList<T>
{
  private final List<T> list;
  private final String name;

  public NamedList(String paramString, List<T> paramList)
  {
    this.name = paramString;
    this.list = paramList;
  }

  public List<T> getList()
  {
    return this.list;
  }

  public String getName()
  {
    return this.name;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.model.NamedList
 * JD-Core Version:    0.6.2
 */