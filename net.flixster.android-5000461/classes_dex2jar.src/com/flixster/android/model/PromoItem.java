package com.flixster.android.model;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class PromoItem extends Image
  implements HeadlineItem
{
  protected String header;
  protected String linkUrl;
  protected int sequence;

  protected static PromoItem parse(JSONObject paramJSONObject)
  {
    if ("featured".equals(paramJSONObject.optString("section")));
    for (Object localObject = new CarouselItem(); ; localObject = new PromoItem())
    {
      ((PromoItem)localObject).header = paramJSONObject.optString("header", null);
      ((PromoItem)localObject).imageUrl = paramJSONObject.optString("imageUrl", null);
      ((PromoItem)localObject).linkUrl = paramJSONObject.optString("linkUrl", null);
      ((PromoItem)localObject).sequence = paramJSONObject.optInt("sequence");
      return localObject;
    }
  }

  public static void parseArray(JSONArray paramJSONArray, List<CarouselItem> paramList, List<PromoItem> paramList1)
  {
    int i = 0;
    if (i >= paramJSONArray.length())
      return;
    JSONObject localJSONObject = paramJSONArray.optJSONObject(i);
    PromoItem localPromoItem;
    if (localJSONObject != null)
    {
      localPromoItem = parse(localJSONObject);
      if (!(localPromoItem instanceof CarouselItem))
        break label56;
      paramList.add((CarouselItem)localPromoItem);
    }
    while (true)
    {
      i++;
      break;
      label56: paramList1.add(localPromoItem);
    }
  }

  public String getImageUrl()
  {
    return this.imageUrl;
  }

  public String getLinkUrl()
  {
    return this.linkUrl;
  }

  public int getSequence()
  {
    return this.sequence;
  }

  public String getTitle()
  {
    return this.header;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.model.PromoItem
 * JD-Core Version:    0.6.2
 */