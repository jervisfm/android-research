package com.flixster.android.msk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.Properties;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Property;
import net.flixster.android.model.User;

public class FacebookInvitePage extends DecoratedSherlockActivity
{
  private static final int[] FBINVITE_REWARDS_DEFAULT = { 1, 5, 10 };
  private final Handler friendsIdsHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Object[] arrayOfObject = AccountManager.instance().getUser().friendsIds;
      new FacebookRequestController(FacebookInvitePage.this, arrayOfObject).start();
    }
  };
  private int[] rewardLevels;
  private ImageView sendInvites;
  private final View.OnClickListener sendInvitesClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      ProfileDao.getFacebookFriends(FacebookInvitePage.this.friendsIdsHandler, FacebookInvitePage.this.friendsIdsHandler);
    }
  };
  private TextView signupLevel1;
  private TextView signupLevel2;
  private TextView signupLevel3;

  private void initializeInviteViews()
  {
    TextView localTextView1 = this.signupLevel1;
    String str1 = getString(2131493212);
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(this.rewardLevels[0]);
    localTextView1.setText(String.format(str1, arrayOfObject1));
    TextView localTextView2 = this.signupLevel2;
    String str2 = getString(2131493213);
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Integer.valueOf(this.rewardLevels[1]);
    localTextView2.setText(String.format(str2, arrayOfObject2));
    TextView localTextView3 = this.signupLevel3;
    String str3 = getString(2131493213);
    Object[] arrayOfObject3 = new Object[1];
    arrayOfObject3[0] = Integer.valueOf(this.rewardLevels[2]);
    localTextView3.setText(String.format(str3, arrayOfObject3));
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903080);
    createActionBar();
    setActionBarTitle(2131493200);
    Property localProperty = Properties.instance().getProperties();
    if ((localProperty == null) || (localProperty.rewardsFbInviteArray == null));
    for (int[] arrayOfInt = FBINVITE_REWARDS_DEFAULT; ; arrayOfInt = localProperty.rewardsFbInviteArray)
    {
      this.rewardLevels = arrayOfInt;
      this.signupLevel1 = ((TextView)findViewById(2131165304));
      this.signupLevel2 = ((TextView)findViewById(2131165305));
      this.signupLevel3 = ((TextView)findViewById(2131165306));
      this.sendInvites = ((ImageView)findViewById(2131165307));
      this.sendInvites.setOnClickListener(this.sendInvitesClickListener);
      Trackers.instance().track("/rewards/fb-invites", "Rewards - FBInvites");
      return;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    initializeInviteViews();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.msk.FacebookInvitePage
 * JD-Core Version:    0.6.2
 */