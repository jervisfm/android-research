package com.flixster.android.msk;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.flixster.android.data.FacebookFacade;
import com.flixster.android.utils.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ProfileDao;

public class FacebookRequestController
{
  private static final int MAX_RECIPIENTS_PER_REQUEST = 50;
  private final Activity activity;
  private boolean isTypePick;
  private int numOfRecipientsSent;
  private final List<String> recipientGroups;
  private final Facebook.DialogListener requestListener = new Facebook.DialogListener()
  {
    public void onCancel()
    {
      FacebookRequestController.this.proceed();
    }

    public void onComplete(Bundle paramAnonymousBundle)
    {
      if (paramAnonymousBundle.size() > 0)
      {
        FacebookRequestController localFacebookRequestController = FacebookRequestController.this;
        localFacebookRequestController.numOfRecipientsSent += -1 + paramAnonymousBundle.size();
      }
      Iterator localIterator = paramAnonymousBundle.keySet().iterator();
      while (true)
      {
        if (!localIterator.hasNext())
        {
          boolean bool = FacebookRequestController.wasRequestSent(paramAnonymousBundle);
          if (bool)
          {
            String str3 = paramAnonymousBundle.getString("request");
            if (str3 != null)
              ProfileDao.trackMskFbRequest(str3);
          }
          if ((!bool) || (FacebookRequestController.this.recipientGroups.size() <= 0))
            break;
          String str2 = (String)FacebookRequestController.this.recipientGroups.remove(0);
          FacebookFacade.sendRequest(FlixsterApplication.sFacebook, str2, FacebookRequestController.this.requestListener, FacebookRequestController.this.activity);
          return;
        }
        String str1 = (String)localIterator.next();
        Logger.si("FlxMain", "FacebookRequestController.onComplete key=" + str1 + " value=" + paramAnonymousBundle.get(str1));
      }
      if ((FacebookRequestController.this.activity instanceof MskEntryActivity))
        if (FacebookRequestController.this.isTypePick)
        {
          MskController.instance().trackFbRequestPickEventSent(FacebookRequestController.this.numOfRecipientsSent);
          MskController.instance().trackFbRequestPickEventSentHisto(FacebookRequestController.this.numOfRecipientsSent);
        }
      while (true)
      {
        FacebookRequestController.this.proceed();
        return;
        MskController.instance().trackFbRequestAllEventSent(FacebookRequestController.this.numOfRecipientsSent);
        MskController.instance().trackFbRequestAllEventSentHisto(FacebookRequestController.this.numOfRecipientsSent);
        continue;
        MskController.instance().trackFbRequestInvitesRewardEvent(FacebookRequestController.this.numOfRecipientsSent);
      }
    }

    public void onError(DialogError paramAnonymousDialogError)
    {
      Logger.e("FlxMain", "FacebookRequestController.onError " + paramAnonymousDialogError.getMessage());
      Toast.makeText(FacebookRequestController.this.activity, "Error: " + paramAnonymousDialogError.getMessage(), 0).show();
      FacebookRequestController.this.proceed();
    }

    public void onFacebookError(FacebookError paramAnonymousFacebookError)
    {
      Logger.e("FlxMain", "FacebookRequestController.onFacebookError " + paramAnonymousFacebookError.getMessage());
      Toast.makeText(FacebookRequestController.this.activity, "Facebook Error: " + paramAnonymousFacebookError.getMessage(), 0).show();
      FacebookRequestController.this.proceed();
    }
  };

  protected FacebookRequestController(Activity paramActivity, Object[] paramArrayOfObject)
  {
    this.activity = paramActivity;
    if (paramArrayOfObject == null);
    for (Object localObject = new ArrayList(); ; localObject = convertToRecipientStrings(paramArrayOfObject))
    {
      this.recipientGroups = ((List)localObject);
      return;
    }
  }

  private static List<String> convertToRecipientStrings(Object[] paramArrayOfObject)
  {
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 1; ; i++)
    {
      if (i >= 1 + paramArrayOfObject.length)
        return localArrayList;
      localStringBuilder.append(paramArrayOfObject[(i - 1)]).append(",");
      if ((i % MAX_RECIPIENTS_PER_REQUEST == 0) || (i == paramArrayOfObject.length))
      {
        localArrayList.add(localStringBuilder.toString());
        localStringBuilder = new StringBuilder();
      }
    }
  }

  private void proceed()
  {
    if ((this.activity instanceof MskEntryActivity))
      MskController.instance().handleNativeFbRequestStepCallback((MskEntryActivity)this.activity);
  }

  private static boolean wasRequestSent(Bundle paramBundle)
  {
    return (paramBundle.size() > 1) && (paramBundle.containsKey("request"));
  }

  protected void start()
  {
    if (this.recipientGroups.size() > 0)
    {
      if ((this.activity instanceof MskEntryActivity))
        MskController.instance().trackFbRequestAll();
      String str = (String)this.recipientGroups.remove(0);
      FacebookFacade.sendRequest(FlixsterApplication.sFacebook, str, this.requestListener, this.activity);
      return;
    }
    this.isTypePick = true;
    if ((this.activity instanceof MskEntryActivity))
      MskController.instance().trackFbRequestPick();
    FacebookFacade.sendRequest(FlixsterApplication.sFacebook, null, this.requestListener, this.activity);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.msk.FacebookRequestController
 * JD-Core Version:    0.6.2
 */