package com.flixster.android.msk;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import android.widget.Toast;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Properties;
import com.flixster.android.view.DialogBuilder.DialogListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterListActivity;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Contact;
import net.flixster.android.model.Property;

public class FriendSelectPage extends FlixsterListActivity
{
  private static final int CONTACT_PICKER_RESULT = 1001;
  public static final String KEY_IS_MSK = "KEY_IS_MSK";
  private static final int PHONE_MAX_LENGTH = 11;
  private static final int PHONE_MIN_LENGTH = 7;
  private static final String SMS_BODY_DEFAULT = "I gave you a full-length movie in Flixster! http://tmto.es/NSsIfY";
  private static final int SMS_COMPOSER_RESULT = 1002;
  private static final int[] SMS_REWARDS_DEFAULT = arrayOfInt;
  private ContactsAdapter adapter;
  private final View.OnClickListener addContactClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Intent localIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
      FriendSelectPage.this.startActivityForResult(localIntent, 1001);
    }
  };
  private ImageView addContactView;
  private String body;
  private List<Contact> friends;
  private final View.OnClickListener inviteClickListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (FriendSelectPage.this.selectedCount >= FriendSelectPage.this.rewardLevels[1])
      {
        int i = FriendSelectPage.this.getGiftCount(FriendSelectPage.this.selectedCount);
        if (FriendSelectPage.this.isMsk)
          MskController.instance().trackTextRequestAttemptEvent(i, FriendSelectPage.this.selectedCount);
        StringBuilder localStringBuilder;
        int j;
        while (true)
        {
          localStringBuilder = new StringBuilder("sms:");
          j = 0;
          if (j < FriendSelectPage.this.friends.size())
            break;
          Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(localStringBuilder.toString()));
          localIntent.putExtra("sms_body", FriendSelectPage.this.body);
          FriendSelectPage.this.startActivityForResult(localIntent, 1002);
          return;
          Trackers.instance().trackEvent("/rewards", null, "FlixsterRewards", "SendTextInvites", Integer.toString(i), FriendSelectPage.this.selectedCount);
        }
        if (FriendSelectPage.this.getListView().isItemChecked(j))
          if (localStringBuilder.length() > "sms:".length())
            if ((!F.MANUFACTURER.contains("samsung")) || (F.API_LEVEL >= 14))
              break label251;
        label251: for (String str = ","; ; str = ";")
        {
          localStringBuilder.append(str);
          localStringBuilder.append(((Contact)FriendSelectPage.this.friends.get(j)).number);
          j++;
          break;
        }
      }
      Toast.makeText(FriendSelectPage.this, FriendSelectPage.this.getString(2131493118), 0).show();
    }
  };
  private ImageView inviteView;
  private boolean isMsk;
  private TextView moreView;
  private final DialogBuilder.DialogListener movieEarnedDialogListener = new DialogBuilder.DialogListener()
  {
    public void onNegativeButtonClick(int paramAnonymousInt)
    {
    }

    public void onNeutralButtonClick(int paramAnonymousInt)
    {
      FriendSelectPage.this.finish();
    }

    public void onPositiveButtonClick(int paramAnonymousInt)
    {
    }
  };
  private int[] rewardLevels;
  private int selectedCount;
  private TextView selectedView;

  static
  {
    int[] arrayOfInt = new int[4];
    arrayOfInt[1] = 10;
    arrayOfInt[2] = 15;
    arrayOfInt[3] = 25;
  }

  private int getGiftCount(int paramInt)
  {
    if (paramInt >= this.rewardLevels[3])
      return 3;
    if (paramInt >= this.rewardLevels[2])
      return 2;
    if (paramInt >= this.rewardLevels[1])
      return 1;
    return 0;
  }

  private void updateFriendCountViews()
  {
    int i = getGiftCount(this.selectedCount);
    TextView localTextView1 = this.selectedView;
    StringBuilder localStringBuilder1 = new StringBuilder(String.valueOf(this.selectedCount)).append(" ");
    String str1;
    String str2;
    label85: int j;
    if (this.selectedCount == 1)
    {
      str1 = getString(2131493113);
      StringBuilder localStringBuilder2 = localStringBuilder1.append(str1).append(" = ").append(i).append(" ");
      if (i != 1)
        break label195;
      str2 = getString(2131493115);
      localTextView1.setText(str2);
      if (this.selectedCount < this.rewardLevels[2])
        break label206;
      j = this.rewardLevels[3];
    }
    while (true)
    {
      int k = j - this.selectedCount;
      if (k <= 0)
        break label241;
      TextView localTextView2 = this.moreView;
      String str3 = getString(2131493116);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(k);
      localTextView2.setText(String.format(str3, arrayOfObject));
      this.moreView.setVisibility(0);
      return;
      str1 = getString(2131493112);
      break;
      label195: str2 = getString(2131493114);
      break label85;
      label206: if (this.selectedCount >= this.rewardLevels[1])
        j = this.rewardLevels[2];
      else
        j = this.rewardLevels[1];
    }
    label241: this.moreView.setVisibility(8);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 1001:
      while (true)
      {
        return;
        if ((paramInt2 == -1) && (paramIntent != null))
        {
          Uri localUri = paramIntent.getData();
          if (localUri != null)
          {
            String[] arrayOfString = { "display_name", "data1" };
            try
            {
              Cursor localCursor2 = getContentResolver().query(localUri, arrayOfString, null, null, null);
              localCursor1 = localCursor2;
              if ((localCursor1 != null) && (localCursor1.moveToFirst()))
              {
                int j = localCursor1.getColumnIndex("display_name");
                str2 = localCursor1.getString(j);
                int k = localCursor1.getColumnIndex("data1");
                str3 = localCursor1.getString(k).replace(" ", "").replace("(", "").replace(")", "").replace("-", "").replace("+", "");
                localIterator = this.friends.iterator();
                boolean bool = localIterator.hasNext();
                m = 0;
                if (!bool)
                {
                  if ((m != 0) || (str3.length() < 7) || (str3.length() > 11))
                    break label360;
                  Contact localContact2 = new Contact(str2, null, null, null, str3);
                  this.friends.add(localContact2);
                  getListView().setItemChecked(-1 + this.friends.size(), true);
                  this.selectedCount = (1 + this.selectedCount);
                  updateFriendCountViews();
                }
              }
            }
            catch (Exception localException)
            {
              while (true)
              {
                String str2;
                String str3;
                Iterator localIterator;
                int m;
                if (this.isMsk)
                  MskController.instance().trackTextRequestExceptionEvent();
                localException.printStackTrace();
                Cursor localCursor1 = null;
                continue;
                Contact localContact1 = (Contact)localIterator.next();
                if ((localContact1.name.equals(str2)) || (localContact1.number.equals(str3)))
                  m = 1;
              }
              label360: Toast.makeText(this, getString(2131493117), 0).show();
              return;
            }
          }
        }
      }
    case 1002:
    }
    int i = getGiftCount(this.selectedCount);
    if (this.isMsk)
    {
      MskController.instance().trackTextRequestSuccessEvent(i);
      Intent localIntent = new Intent();
      localIntent.putExtra("moviesEarned", i);
      setResult(-1, localIntent);
      finish();
      return;
    }
    AccountFacade.getSocialUser().isMskSmsEligible = false;
    String str1;
    if (i == 3)
      str1 = "MT1,MT2,MT3";
    while (true)
    {
      Trackers.instance().trackEvent("/rewards", null, "FlixsterRewards", "CompletedReward", "SendTextInvites", i);
      ProfileDao.insertLockerRight(null, null, str1);
      showDialog(1000000401, this.movieEarnedDialogListener);
      return;
      if (i == 2)
        str1 = "MT1,MT2";
      else
        str1 = "MT1";
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903088);
    createActionBar();
    setActionBarTitle(2131493110);
    this.isMsk = getIntent().getBooleanExtra("KEY_IS_MSK", false);
    Property localProperty = Properties.instance().getProperties();
    int[] arrayOfInt2;
    int[] arrayOfInt1;
    label76: String str2;
    if (localProperty != null)
    {
      if (this.isMsk)
      {
        arrayOfInt2 = localProperty.mskTextRequestRewardsArray;
        this.rewardLevels = arrayOfInt2;
        if (arrayOfInt2 != null)
          break label268;
      }
    }
    else
    {
      arrayOfInt1 = SMS_REWARDS_DEFAULT;
      this.rewardLevels = arrayOfInt1;
      if (localProperty != null)
      {
        if (!this.isMsk)
          break label276;
        str2 = localProperty.mskTextRequestBody;
        label98: this.body = str2;
        if (str2 != null)
          break label285;
      }
    }
    label268: label276: label285: for (String str1 = "I gave you a full-length movie in Flixster! http://tmto.es/NSsIfY"; ; str1 = this.body)
    {
      this.body = str1;
      this.friends = new ArrayList();
      this.adapter = new ContactsAdapter(this, this.friends);
      setListAdapter(this.adapter);
      this.selectedView = ((TextView)findViewById(2131165337));
      this.moreView = ((TextView)findViewById(2131165338));
      this.addContactView = ((ImageView)findViewById(2131165336));
      this.addContactView.setOnClickListener(this.addContactClickListener);
      this.inviteView = ((ImageView)findViewById(2131165339));
      this.inviteView.setOnClickListener(this.inviteClickListener);
      updateFriendCountViews();
      if (!this.isMsk)
        Trackers.instance().track("/rewards/select-contacts", "Rewards - SelectContacts");
      return;
      arrayOfInt2 = localProperty.rewardsSmsArray;
      break;
      arrayOfInt1 = this.rewardLevels;
      break label76;
      str2 = localProperty.rewardsSmsBody;
      break label98;
    }
  }

  protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    if (paramListView.isItemChecked(paramInt));
    for (this.selectedCount = (1 + this.selectedCount); ; this.selectedCount = (-1 + this.selectedCount))
    {
      updateFriendCountViews();
      return;
    }
  }

  private class ContactsAdapter extends ArrayAdapter<Contact>
  {
    ContactsAdapter(List<Contact> arg2)
    {
      super(17367056, localList);
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = FriendSelectPage.this.getLayoutInflater().inflate(17367056, paramViewGroup, false);
      TextView localTextView = (TextView)paramView.findViewById(16908308);
      Contact localContact = (Contact)FriendSelectPage.this.friends.get(paramInt);
      SpannableString localSpannableString = new SpannableString(localContact.name);
      int i;
      int j;
      if ((localContact.givenName != null) && (!localContact.givenName.equals("")))
      {
        i = localContact.givenName.length();
        if ((localContact.middleName == null) || (localContact.middleName.equals("")))
          break label238;
        j = 1 + localContact.middleName.length();
        label126: int k = i + j;
        if (k > localContact.name.length())
          break label244;
        localSpannableString.setSpan(new TextAppearanceSpan(FriendSelectPage.this, 2131558512), 0, k, 17);
        if ((localContact.familyName != null) && (!localContact.familyName.equals("")))
          localSpannableString.setSpan(new TextAppearanceSpan(FriendSelectPage.this, 2131558513), k, localContact.name.length(), 17);
      }
      while (true)
      {
        localTextView.setText(localSpannableString, TextView.BufferType.SPANNABLE);
        return paramView;
        i = 0;
        break;
        label238: j = 0;
        break label126;
        label244: localSpannableString.setSpan(new TextAppearanceSpan(FriendSelectPage.this, 2131558512), 0, localContact.name.length(), 17);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.msk.FriendSelectPage
 * JD-Core Version:    0.6.2
 */