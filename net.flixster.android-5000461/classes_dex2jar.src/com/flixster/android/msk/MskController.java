package com.flixster.android.msk;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.widget.Toast;
import com.fiksu.asotracking.FiksuTrackingManager;
import com.flixster.android.activity.DeepLink;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.data.AccountManager;
import com.flixster.android.utils.F;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.ObjectHolder;
import com.flixster.android.utils.Properties;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import net.flixster.android.FacebookAuth;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.FlixsterLoginPage;
import net.flixster.android.Starter;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Property;
import net.flixster.android.model.User;

public class MskController
{
  private static final int REQUEST_CODE_BASE = 1000000145;
  private static boolean firstLaunch;
  private String extraMovies;
  private long movieId;
  private int moviesEarned;
  private Property p = Properties.instance().getProperties();
  private String rewards;

  private static int convertToRequestCode(Step paramStep)
  {
    return 1000000145 + paramStep.ordinal();
  }

  private static Step convertToStep(int paramInt)
  {
    return Step.values()[(paramInt - 1000000145)];
  }

  private static String getHistoLabel(int paramInt)
  {
    if (paramInt < 4)
      return "1-3";
    if (paramInt < 7)
      return "4-6";
    if (paramInt < 11)
      return "7-10";
    if (paramInt < 16)
      return "11-15";
    if (paramInt < 21)
      return "16-20";
    if (paramInt < 51)
      return "21-50";
    if (paramInt < 101)
      return "51-100";
    if (paramInt < 151)
      return "101-150";
    if (paramInt < 201)
      return "151-200";
    if (paramInt < 251)
      return "201-250";
    if (paramInt < 300)
      return "251-300";
    return "300+";
  }

  private void handleLaspStep(MskEntryActivity paramMskEntryActivity)
  {
    finishLasp(paramMskEntryActivity);
  }

  private void handleMobileWebStep(MskEntryActivity paramMskEntryActivity, Step paramStep, String paramString)
  {
    switch ($SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[paramStep.ordinal()])
    {
    default:
      return;
    case 7:
    }
    StringBuilder localStringBuilder = new StringBuilder(paramString);
    String str4;
    String str3;
    label103: String str2;
    if (this.movieId != 0L)
    {
      if (localStringBuilder.indexOf("?") <= 0)
      {
        str4 = "?";
        localStringBuilder.append(str4).append("movie=").append(this.movieId);
      }
    }
    else
    {
      if (this.extraMovies != null)
      {
        if (localStringBuilder.indexOf("?") > 0)
          break label228;
        str3 = "?";
        localStringBuilder.append(str3).append(this.extraMovies);
        this.extraMovies = null;
      }
      if (this.rewards != null)
      {
        if (localStringBuilder.indexOf("?") > 0)
          break label235;
        str2 = "?";
        label144: localStringBuilder.append(str2).append("rewards=").append(this.rewards);
        this.rewards = null;
      }
      if (localStringBuilder.indexOf("?") > 0)
        break label242;
    }
    label228: label235: label242: for (String str1 = "?"; ; str1 = "&")
    {
      localStringBuilder.append(str1).append("purchaseType=MAN");
      FlixsterApplication.addCurrentUserParameters(localStringBuilder);
      Starter.launchMskHtmlPageForResult(localStringBuilder.toString(), firstLaunch, paramMskEntryActivity, convertToRequestCode(Step.MOBILE_WEB));
      return;
      str4 = "&";
      break;
      str3 = "&";
      break label103;
      str2 = "&";
      break label144;
    }
  }

  private void handleNativeFbRequestStep(MskEntryActivity paramMskEntryActivity, Object[] paramArrayOfObject)
  {
    new FacebookRequestController(paramMskEntryActivity, paramArrayOfObject).start();
  }

  private void handleNativeStep(MskEntryActivity paramMskEntryActivity, Step paramStep)
  {
    boolean bool1 = AccountManager.instance().hasUserSession();
    boolean bool2 = AccountManager.instance().hasFacebookUserSession();
    switch ($SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[paramStep.ordinal()])
    {
    default:
      return;
    case 1:
    case 5:
      if (bool1)
      {
        skipStep(paramMskEntryActivity, paramStep);
        return;
      }
      Intent localIntent2 = new Intent(paramMskEntryActivity, FacebookAuth.class);
      localIntent2.putExtra("KEY_SHOW_CONNECTED_DIALOG", false);
      paramMskEntryActivity.startActivityForResult(localIntent2, convertToRequestCode(paramStep));
      return;
    case 3:
      String str = (String)ObjectHolder.instance().remove(Step.NATIVE_FB_REQUEST.name());
      if (bool2)
      {
        ObjectHolder.instance().put(Step.NATIVE_FB_REQUEST.name(), paramMskEntryActivity);
        Handler localHandler = new Handler(new FriendsIdsCallBack(null));
        if (DeepLink.isMskRequestTypePick(str))
        {
          handleNativeFbRequestStep(paramMskEntryActivity, null);
          return;
        }
        ProfileDao.getFacebookFriends(localHandler, localHandler);
        return;
      }
      if (bool1)
      {
        skipStep(paramMskEntryActivity, Step.NATIVE_FB_REQUEST);
        return;
      }
      ObjectHolder.instance().put(Step.NATIVE_FB_REQUEST.name(), str);
      ObjectHolder.instance().put(Step.NATIVE_IMPLICIT_FB_LOGIN.name(), Step.NATIVE_FB_REQUEST);
      handleNativeStep(paramMskEntryActivity, Step.NATIVE_IMPLICIT_FB_LOGIN);
      return;
    case 4:
      Intent localIntent1 = new Intent(paramMskEntryActivity, FriendSelectPage.class);
      localIntent1.putExtra("KEY_IS_MSK", true);
      paramMskEntryActivity.startActivityForResult(localIntent1, convertToRequestCode(paramStep));
      trackTextRequest();
      return;
    case 2:
    case 6:
    }
    if (bool1)
    {
      skipStep(paramMskEntryActivity, paramStep);
      return;
    }
    paramMskEntryActivity.startActivityForResult(new Intent(paramMskEntryActivity, FlixsterLoginPage.class), convertToRequestCode(paramStep));
  }

  private boolean handleStep(MskEntryActivity paramMskEntryActivity, String paramString)
  {
    boolean bool;
    if (paramString != null)
    {
      bool = AccountManager.instance().hasUserSession();
      if ((DeepLink.isFacebookLoginRequired(paramString)) && (!bool))
      {
        String str2 = DeepLink.removeLoginRequired(paramString);
        ObjectHolder.instance().put(Step.NATIVE_IMPLICIT_FB_LOGIN.name(), str2);
        handleNativeStep(paramMskEntryActivity, Step.NATIVE_IMPLICIT_FB_LOGIN);
      }
    }
    else
    {
      return false;
    }
    if ((DeepLink.isFlixsterLoginRequired(paramString)) && (!bool))
    {
      String str1 = DeepLink.removeLoginRequired(paramString);
      ObjectHolder.instance().put(Step.NATIVE_IMPLICIT_FLX_LOGIN.name(), str1);
      handleNativeStep(paramMskEntryActivity, Step.NATIVE_IMPLICIT_FLX_LOGIN);
      return false;
    }
    if (DeepLink.isValid(paramString))
    {
      Step localStep = DeepLink.convertToMskNativeStep(paramString);
      if (localStep == Step.NATIVE_LASP)
      {
        handleLaspStep(paramMskEntryActivity);
        return true;
      }
      if (localStep == Step.NATIVE_FB_REQUEST)
        ObjectHolder.instance().put(Step.NATIVE_FB_REQUEST.name(), paramString);
      handleNativeStep(paramMskEntryActivity, localStep);
      return false;
    }
    handleMobileWebStep(paramMskEntryActivity, Step.MOBILE_WEB, paramString);
    return false;
  }

  public static MskController instance()
  {
    return MskControllerSingletonHolder.INSTANCE;
  }

  public static boolean isRequestCodeValid(int paramInt)
  {
    return (paramInt >= 1000000145) && (paramInt < 1000000145 + Step.values().length);
  }

  private void skipStep(MskEntryActivity paramMskEntryActivity, Step paramStep)
  {
    onActivityResult(paramMskEntryActivity, convertToRequestCode(paramStep), -1, null);
  }

  private static void trackCompletion(Context paramContext)
  {
    Property localProperty = Properties.instance().getProperties();
    Tracker localTracker;
    String str1;
    String str2;
    if (localProperty != null)
    {
      localTracker = Trackers.instance();
      StringBuilder localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label95;
      str1 = "/startup";
      str2 = str1 + "/accept-movie";
      if (!firstLaunch)
        break label120;
    }
    label95: label120: for (String str3 = "StartUpMSK"; ; str3 = "MobileMSK")
    {
      localTracker.trackEvent(str2, null, str3, "SuccessfulRedemption", "SuccessfulRedemption", 0);
      FiksuTrackingManager.uploadPurchaseEvent(paramContext, "", 0.0D, "USD");
      return;
      str1 = "/var" + localProperty.mskVariation;
      break;
    }
  }

  private void trackMobileWebPageExit(String paramString)
  {
    if (paramString.startsWith("msk.selection."))
    {
      if (!"msk.selection.send.exit".equals(paramString))
        break label25;
      trackSelectionPageEventAll();
    }
    label25: 
    do
    {
      return;
      if ("msk.selection.pick.exit".equals(paramString))
      {
        trackSelectionPageEventPick();
        return;
      }
    }
    while (!"msk.selection.skip.exit".equals(paramString));
    trackSelectionPageEventSkip();
  }

  private void trackMskEntry()
  {
    if (firstLaunch)
      Trackers.instance().trackEvent("/msk/startup/msk-entry", null, "StartUpMSK", "Impression", "Impression", 0);
    while (this.p == null)
      return;
    int i;
    if (ObjectHolder.instance().get("lasp") != null)
    {
      i = 1;
      if (i == 0)
        break label110;
    }
    label110: for (String str = "msk"; ; str = this.p.mskAnonEntry.substring(this.p.mskAnonEntry.indexOf("flixster.com") + "flixster.com".length()))
    {
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/msk-entry", null, "MobileMSK", "MSKEntry", str, 0);
      return;
      i = 0;
      break;
    }
  }

  private void trackSelectionPageEventAll()
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/accept-movie", null, "MobileMSK", "AcceptMovie", "AllFriends", 0);
  }

  private void trackSelectionPageEventPick()
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/accept-movie", null, "MobileMSK", "AcceptMovie", "SelectFriends", 0);
  }

  private void trackSelectionPageEventSkip()
  {
    Tracker localTracker;
    String str1;
    String str2;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      StringBuilder localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label78;
      str1 = "/startup";
      str2 = str1 + "/accept-movie";
      if (!firstLaunch)
        break label105;
    }
    label78: label105: for (String str3 = "StartUpMSK"; ; str3 = "MobileMSK")
    {
      localTracker.trackEvent(str2, null, str3, "AcceptMovie", "SkipInvites", 0);
      return;
      str1 = "/var" + this.p.mskVariation;
      break;
    }
  }

  public void finishLasp(Context paramContext)
  {
    trackCompletion(paramContext);
    User localUser = AccountFacade.getSocialUser();
    if (localUser == null)
      Toast.makeText(paramContext, 2131493105, 1).show();
    while (true)
    {
      DeepLink.clearDeepLinkParams();
      return;
      localUser.isMskEligible = false;
      this.p = Properties.instance().getProperties();
      if ((this.p != null) && (this.p.mskTextRequestBody != null) && (this.moviesEarned > 0))
        localUser.isMskSmsEligible = false;
      localUser.isMskUvCreateEligible = false;
      localUser.isMskInstallMobileEligible = false;
      localUser.collectionsCount = (1 + localUser.collectionsCount);
      localUser.resetLockerRights();
      if (ObjectHolder.instance().remove("lasp") != null)
        Starter.deepLinkMyMoviesTab(paramContext);
    }
  }

  protected void handleNativeFbRequestStepCallback(MskEntryActivity paramMskEntryActivity)
  {
    onActivityResult(paramMskEntryActivity, convertToRequestCode(Step.NATIVE_FB_REQUEST), -1, null);
  }

  public boolean isReady()
  {
    Property localProperty = Properties.instance().getProperties();
    if (localProperty != null)
      if (firstLaunch)
      {
        if (localProperty.mskFirstLaunchEntry == null);
      }
      else
        while (localProperty.getMskEntry() != null)
        {
          this.p = localProperty;
          return true;
        }
    Logger.w("FlxMain", "MskController.isReady properties null");
    return false;
  }

  protected boolean onActivityResult(MskEntryActivity paramMskEntryActivity, int paramInt1, int paramInt2, Intent paramIntent)
  {
    Step localStep;
    boolean bool1;
    if ((isRequestCodeValid(paramInt1)) && (paramInt2 == -1))
    {
      localStep = convertToStep(paramInt1);
      Logger.d("FlxMain", "MskController.onActivityResult " + localStep);
      int i = $SWITCH_TABLE$com$flixster$android$msk$MskController$Step()[localStep.ordinal()];
      bool1 = false;
      switch (i)
      {
      default:
      case 1:
      case 3:
      case 4:
      case 2:
      case 5:
      case 6:
      case 7:
      }
    }
    while (true)
    {
      if (bool1)
      {
        this.movieId = 0L;
        this.moviesEarned = 0;
      }
      return bool1;
      if (AccountManager.instance().getUser().isMskEligible)
      {
        handleStep(paramMskEntryActivity, this.p.mskFbLoginExit);
        bool1 = false;
      }
      else
      {
        paramMskEntryActivity.showMskIneligibleDialog();
        bool1 = false;
        continue;
        handleStep(paramMskEntryActivity, this.p.mskFbRequestExit);
        bool1 = false;
        continue;
        this.moviesEarned = paramIntent.getIntExtra("moviesEarned", 0);
        StringBuilder localStringBuilder = new StringBuilder(this.p.mskTextRequestExit);
        if (localStringBuilder.indexOf("?") < 0);
        for (String str3 = "?"; ; str3 = "&")
        {
          localStringBuilder.append(str3);
          localStringBuilder.append("moviesEarned=").append(this.moviesEarned);
          handleStep(paramMskEntryActivity, localStringBuilder.toString());
          bool1 = false;
          break;
        }
        if (AccountManager.instance().getUser().isMskEligible)
        {
          handleStep(paramMskEntryActivity, this.p.mskFlxLoginExit);
          bool1 = false;
        }
        else
        {
          paramMskEntryActivity.showMskIneligibleDialog();
          bool1 = false;
          continue;
          Object localObject = ObjectHolder.instance().remove(localStep.name());
          if (AccountManager.instance().getUser().isMskEligible)
          {
            if ((localObject instanceof Step))
            {
              handleNativeStep(paramMskEntryActivity, (Step)localObject);
              bool1 = false;
            }
            else
            {
              boolean bool2 = localObject instanceof String;
              bool1 = false;
              if (bool2)
              {
                handleStep(paramMskEntryActivity, (String)localObject);
                bool1 = false;
              }
            }
          }
          else
          {
            paramMskEntryActivity.showMskIneligibleDialog();
            bool1 = false;
            continue;
            String str1 = paramIntent.getDataString();
            long l = DeepLink.getMovieId(str1);
            if (l != 0L)
              this.movieId = l;
            this.extraMovies = DeepLink.getExtraMovies(str1);
            String str2 = DeepLink.convertToPropertyName(str1);
            if (str2 != null)
            {
              str1 = this.p.optString(str2);
              trackMobileWebPageExit(str2);
            }
            Logger.d("FlxMain", "MskController.onActivityResult " + str2 + " " + str1 + " " + this.movieId);
            bool1 = handleStep(paramMskEntryActivity, str1);
            continue;
            Logger.w("FlxMain", "MskController.onActivityResult not handled " + paramInt1 + " " + paramInt2);
            bool1 = true;
          }
        }
      }
    }
  }

  public void setFirstLaunchMode(boolean paramBoolean)
  {
    firstLaunch = paramBoolean;
  }

  protected void startFlow(MskEntryActivity paramMskEntryActivity)
  {
    if ((this.p != null) || (isReady()))
    {
      this.rewards = this.p.mskTextRequestRewards;
      if (!firstLaunch)
        break label51;
    }
    label51: for (String str = this.p.mskFirstLaunchEntry; ; str = this.p.getMskEntry())
    {
      handleStep(paramMskEntryActivity, str);
      trackMskEntry();
      return;
    }
  }

  public void trackFbLoginAttempt()
  {
    Tracker localTracker;
    StringBuilder localStringBuilder;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label54;
    }
    label54: for (String str = "/startup"; ; str = "/var" + this.p.mskVariation)
    {
      localTracker.track(str + "/facebook/login/attempt", null);
      return;
    }
  }

  public void trackFbLoginSuccess()
  {
    Tracker localTracker;
    StringBuilder localStringBuilder;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label54;
    }
    label54: for (String str = "/startup"; ; str = "/var" + this.p.mskVariation)
    {
      localTracker.track(str + "/facebook/login/success", null);
      return;
    }
  }

  protected void trackFbRequestAll()
  {
    if (this.p != null)
      Trackers.instance().track("/msk/var" + this.p.mskVariation + "/invite-all", null);
  }

  protected void trackFbRequestAllEventSent(int paramInt)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/invite-all", null, "MobileMSK", "AppRequest", "All", paramInt);
  }

  protected void trackFbRequestAllEventSentHisto(int paramInt)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/invite-all", null, "MobileMSK", "AppRequestAllHisto", getHistoLabel(paramInt), paramInt);
  }

  protected void trackFbRequestInvitesRewardEvent(int paramInt)
  {
    Trackers.instance().trackEvent("/rewards", null, "FlixsterRewards", "FBInvites", "All", paramInt);
  }

  protected void trackFbRequestPick()
  {
    if (this.p != null)
      Trackers.instance().track("/msk/var" + this.p.mskVariation + "/invite-select", null);
  }

  protected void trackFbRequestPickEventSent(int paramInt)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/invite-select", null, "MobileMSK", "AppRequest", "Select", paramInt);
  }

  protected void trackFbRequestPickEventSentHisto(int paramInt)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/invite-select", null, "MobileMSK", "AppRequestSelectHisto", getHistoLabel(paramInt), paramInt);
  }

  public void trackFlxLoginAttempt()
  {
    Tracker localTracker;
    StringBuilder localStringBuilder;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label54;
    }
    label54: for (String str = "/startup"; ; str = "/var" + this.p.mskVariation)
    {
      localTracker.track(str + "/flixster/login/dialog", null);
      return;
    }
  }

  public void trackFlxLoginSuccess()
  {
    Tracker localTracker;
    StringBuilder localStringBuilder;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label54;
    }
    label54: for (String str = "/startup"; ; str = "/var" + this.p.mskVariation)
    {
      localTracker.track(str + "/flixster/login/success", null);
      return;
    }
  }

  public void trackRewardConfirmationPage()
  {
    if (this.p != null)
      Trackers.instance().track("/msk/var" + this.p.mskVariation + "/reward-confirmation", null);
  }

  public void trackSelectionPage()
  {
    Tracker localTracker;
    StringBuilder localStringBuilder;
    if (this.p != null)
    {
      localTracker = Trackers.instance();
      localStringBuilder = new StringBuilder("/msk");
      if (!firstLaunch)
        break label54;
    }
    label54: for (String str = "/startup"; ; str = "/var" + this.p.mskVariation)
    {
      localTracker.track(str + "/accept-movie", null);
      return;
    }
  }

  public void trackSkipFirstLaunchMsk()
  {
    Trackers.instance().trackEvent("/msk/startup/skip", null, "StartUpMSK", "Skip", "Skip", 0);
  }

  protected void trackTextRequest()
  {
    if (this.p != null)
      Trackers.instance().track("/msk/var" + this.p.mskVariation + "/select-contacts", null);
  }

  protected void trackTextRequestAttemptEvent(int paramInt1, int paramInt2)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/text-request", null, "MobileMSK", "SendTextInvites", Integer.toString(paramInt1), paramInt2);
  }

  protected void trackTextRequestExceptionEvent()
  {
    if (this.p != null);
    try
    {
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/text-request", null, "MobileMSK", "PermissionException", URLEncoder.encode(F.MANUFACTURER + "_" + String.valueOf(F.API_LEVEL), "UTF-8"), 0);
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      localUnsupportedEncodingException.printStackTrace();
    }
  }

  protected void trackTextRequestSuccessEvent(int paramInt)
  {
    if (this.p != null)
      Trackers.instance().trackEvent("/msk/var" + this.p.mskVariation + "/text-request", null, "MobileMSK", "AdditionalMovies", "AdditionalMovies", paramInt);
  }

  private class FriendsIdsCallBack
    implements Handler.Callback
  {
    private FriendsIdsCallBack()
    {
    }

    public boolean handleMessage(Message paramMessage)
    {
      Object localObject = ObjectHolder.instance().remove(MskController.Step.NATIVE_FB_REQUEST.name());
      Object[] arrayOfObject = AccountManager.instance().getUser().friendsIds;
      MskController.this.handleNativeFbRequestStep((MskEntryActivity)localObject, arrayOfObject);
      return true;
    }
  }

  private static class MskControllerSingletonHolder
  {
    private static final MskController INSTANCE = new MskController(null);
  }

  public static enum Step
  {
    static
    {
      NATIVE_FB_REQUEST = new Step("NATIVE_FB_REQUEST", 2);
      NATIVE_TEXT_REQUEST = new Step("NATIVE_TEXT_REQUEST", 3);
      NATIVE_IMPLICIT_FB_LOGIN = new Step("NATIVE_IMPLICIT_FB_LOGIN", 4);
      NATIVE_IMPLICIT_FLX_LOGIN = new Step("NATIVE_IMPLICIT_FLX_LOGIN", 5);
      MOBILE_WEB = new Step("MOBILE_WEB", 6);
      NATIVE_LASP = new Step("NATIVE_LASP", 7);
      Step[] arrayOfStep = new Step[8];
      arrayOfStep[0] = NATIVE_FB_LOGIN;
      arrayOfStep[1] = NATIVE_FLX_LOGIN;
      arrayOfStep[2] = NATIVE_FB_REQUEST;
      arrayOfStep[3] = NATIVE_TEXT_REQUEST;
      arrayOfStep[4] = NATIVE_IMPLICIT_FB_LOGIN;
      arrayOfStep[5] = NATIVE_IMPLICIT_FLX_LOGIN;
      arrayOfStep[6] = MOBILE_WEB;
      arrayOfStep[7] = NATIVE_LASP;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.msk.MskController
 * JD-Core Version:    0.6.2
 */