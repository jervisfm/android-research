package com.flixster.android.msk;

import android.content.Intent;
import android.os.Bundle;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.utils.F;
import com.flixster.android.view.DialogBuilder.DialogListener;

public class MskEntryActivity extends DecoratedActivity
{
  public static final String REQUEST_CODE = "MskEntryActivity.REQUEST_CODE";

  private void startMsk()
  {
    MskController.instance().startFlow(this);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (MskController.instance().onActivityResult(this, paramInt1, paramInt2, paramIntent))
      finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (F.API_LEVEL >= 9);
    for (int i = 7; ; i = 1)
    {
      setRequestedOrientation(i);
      setContentView(2130903068);
      startMsk();
      return;
    }
  }

  protected void showMskIneligibleDialog()
  {
    showDialog(1000000400, new MskIneligibleDialogListener(null));
  }

  public void startActivityForResult(Intent paramIntent, int paramInt)
  {
    paramIntent.putExtra("MskEntryActivity.REQUEST_CODE", paramInt);
    super.startActivityForResult(paramIntent, paramInt);
  }

  private class MskIneligibleDialogListener
    implements DialogBuilder.DialogListener
  {
    private MskIneligibleDialogListener()
    {
    }

    public void onNegativeButtonClick(int paramInt)
    {
      MskEntryActivity.this.finish();
    }

    public void onNeutralButtonClick(int paramInt)
    {
      MskEntryActivity.this.finish();
    }

    public void onPositiveButtonClick(int paramInt)
    {
      MskEntryActivity.this.finish();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.msk.MskEntryActivity
 * JD-Core Version:    0.6.2
 */