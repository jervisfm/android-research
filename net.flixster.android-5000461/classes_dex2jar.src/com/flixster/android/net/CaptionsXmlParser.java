package com.flixster.android.net;

import com.flixster.android.utils.Logger;
import java.io.InputStream;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class CaptionsXmlParser
{
  // ERROR //
  private List<TimedTextElement> readTtml(XmlPullParser paramXmlPullParser)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: invokeinterface 20 1 0
    //   8: istore 5
    //   10: aconst_null
    //   11: astore 6
    //   13: aconst_null
    //   14: astore 7
    //   16: iconst_0
    //   17: istore 8
    //   19: aconst_null
    //   20: astore 9
    //   22: aconst_null
    //   23: astore 10
    //   25: aconst_null
    //   26: astore 11
    //   28: aconst_null
    //   29: astore 12
    //   31: aconst_null
    //   32: astore 13
    //   34: aconst_null
    //   35: astore 14
    //   37: goto +351 -> 388
    //   40: aload_1
    //   41: invokeinterface 23 1 0
    //   46: istore 17
    //   48: iload 17
    //   50: istore 5
    //   52: aload_2
    //   53: astore 14
    //   55: goto +333 -> 388
    //   58: new 25	java/util/ArrayList
    //   61: dup
    //   62: invokespecial 26	java/util/ArrayList:<init>	()V
    //   65: astore_2
    //   66: goto -26 -> 40
    //   69: aload_1
    //   70: invokeinterface 30 1 0
    //   75: astore 19
    //   77: aload 19
    //   79: ldc 32
    //   81: invokevirtual 38	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   84: ifeq +84 -> 168
    //   87: aload_1
    //   88: aconst_null
    //   89: ldc 40
    //   91: invokeinterface 44 3 0
    //   96: astore 11
    //   98: aload_1
    //   99: aconst_null
    //   100: ldc 46
    //   102: invokeinterface 44 3 0
    //   107: astore 12
    //   109: aload_1
    //   110: aconst_null
    //   111: ldc 48
    //   113: invokeinterface 44 3 0
    //   118: astore 13
    //   120: ldc 50
    //   122: new 52	java/lang/StringBuilder
    //   125: dup
    //   126: ldc 54
    //   128: invokespecial 57	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   131: aload 11
    //   133: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: ldc 63
    //   138: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   141: aload 12
    //   143: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: ldc 65
    //   148: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: aload 13
    //   153: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   159: invokestatic 74	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   162: aload 14
    //   164: astore_2
    //   165: goto -125 -> 40
    //   168: aload 19
    //   170: ldc 76
    //   172: invokevirtual 38	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   175: ifeq +257 -> 432
    //   178: aload_1
    //   179: aconst_null
    //   180: ldc 78
    //   182: invokeinterface 44 3 0
    //   187: astore 20
    //   189: aload 20
    //   191: aload 6
    //   193: invokevirtual 38	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   196: ifeq +242 -> 438
    //   199: iinc 8 1
    //   202: aload 20
    //   204: astore 6
    //   206: aload_1
    //   207: aconst_null
    //   208: ldc 80
    //   210: invokeinterface 44 3 0
    //   215: astore 7
    //   217: aload_1
    //   218: aconst_null
    //   219: ldc 82
    //   221: invokeinterface 44 3 0
    //   226: astore 9
    //   228: aload 14
    //   230: astore_2
    //   231: aconst_null
    //   232: astore 10
    //   234: goto -194 -> 40
    //   237: aload_1
    //   238: invokeinterface 86 1 0
    //   243: ifne +189 -> 432
    //   246: aload_1
    //   247: invokeinterface 89 1 0
    //   252: astore 18
    //   254: aload 10
    //   256: ifnonnull +13 -> 269
    //   259: aload 18
    //   261: astore 10
    //   263: aload 14
    //   265: astore_2
    //   266: goto -226 -> 40
    //   269: new 52	java/lang/StringBuilder
    //   272: dup
    //   273: aload 10
    //   275: invokestatic 93	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   278: invokespecial 57	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   281: aload 18
    //   283: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   289: astore 10
    //   291: aload 14
    //   293: astore_2
    //   294: goto -254 -> 40
    //   297: aload_1
    //   298: invokeinterface 30 1 0
    //   303: ldc 76
    //   305: invokevirtual 38	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   308: ifeq +124 -> 432
    //   311: new 95	com/flixster/android/net/TimedTextElement
    //   314: dup
    //   315: aload 6
    //   317: aload 7
    //   319: iload 8
    //   321: aload 9
    //   323: aload 10
    //   325: aload 11
    //   327: aload 12
    //   329: aload 13
    //   331: invokespecial 98	com/flixster/android/net/TimedTextElement:<init>	(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   334: astore 15
    //   336: aload 14
    //   338: aload 15
    //   340: invokeinterface 103 2 0
    //   345: pop
    //   346: goto +86 -> 432
    //   349: astore_3
    //   350: aload 14
    //   352: astore_2
    //   353: ldc 50
    //   355: ldc 105
    //   357: aload_3
    //   358: invokestatic 109	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   361: aload_2
    //   362: areturn
    //   363: astore 4
    //   365: ldc 50
    //   367: ldc 105
    //   369: aload 4
    //   371: invokestatic 109	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   374: aload_2
    //   375: areturn
    //   376: astore 4
    //   378: aload 14
    //   380: astore_2
    //   381: goto -16 -> 365
    //   384: astore_3
    //   385: goto -32 -> 353
    //   388: iload 5
    //   390: iconst_1
    //   391: if_icmpne +6 -> 397
    //   394: aload 14
    //   396: areturn
    //   397: iload 5
    //   399: tableswitch	default:+33 -> 432, 0:+-341->58, 1:+33->432, 2:+-330->69, 3:+-102->297, 4:+-162->237
    //   433: dconst_0
    //   434: astore_2
    //   435: goto -395 -> 40
    //   438: iconst_0
    //   439: istore 8
    //   441: goto -239 -> 202
    //
    // Exception table:
    //   from	to	target	type
    //   58	66	349	org/xmlpull/v1/XmlPullParserException
    //   69	162	349	org/xmlpull/v1/XmlPullParserException
    //   168	199	349	org/xmlpull/v1/XmlPullParserException
    //   206	228	349	org/xmlpull/v1/XmlPullParserException
    //   237	254	349	org/xmlpull/v1/XmlPullParserException
    //   269	291	349	org/xmlpull/v1/XmlPullParserException
    //   297	346	349	org/xmlpull/v1/XmlPullParserException
    //   2	10	363	java/io/IOException
    //   40	48	363	java/io/IOException
    //   58	66	376	java/io/IOException
    //   69	162	376	java/io/IOException
    //   168	199	376	java/io/IOException
    //   206	228	376	java/io/IOException
    //   237	254	376	java/io/IOException
    //   269	291	376	java/io/IOException
    //   297	346	376	java/io/IOException
    //   2	10	384	org/xmlpull/v1/XmlPullParserException
    //   40	48	384	org/xmlpull/v1/XmlPullParserException
  }

  public List<TimedTextElement> parse(InputStream paramInputStream)
  {
    try
    {
      XmlPullParser localXmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
      localXmlPullParser.setInput(paramInputStream, null);
      List localList = readTtml(localXmlPullParser);
      return localList;
    }
    catch (XmlPullParserException localXmlPullParserException)
    {
      Logger.w("FlxDrm", "CaptionsXmlParser.parse", localXmlPullParserException);
    }
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.CaptionsXmlParser
 * JD-Core Version:    0.6.2
 */