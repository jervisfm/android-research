package com.flixster.android.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.flixster.android.drm.DownloadLock;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.utils.Logger;
import net.flixster.android.model.LockerRight;

public class DownloadBroadcastReceiver extends BroadcastReceiver
{
  private static long extractRightId(String paramString)
  {
    long l1 = 0L;
    int i = paramString.lastIndexOf("/");
    int j = paramString.lastIndexOf(".");
    String str;
    if ((i > -1) && (j > -1))
      str = paramString.substring(i + 1, j).split("_")[0];
    try
    {
      long l2 = Long.valueOf(str).longValue();
      l1 = l2;
      return l1;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return l1;
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    long l1 = paramIntent.getLongExtra("extra_download_id", 0L);
    Logger.d("FlxMain", "DownloadBroadcastReceiver.onReceive id " + l1 + " " + paramIntent.getAction());
    if ("android.intent.action.DOWNLOAD_COMPLETE".equals(paramIntent.getAction()))
    {
      String str1 = DownloadHelper.queryDownloadManagerForFile(l1);
      if ((str1 != null) && (str1.contains("net.flixster.android")) && (!str1.endsWith(".ttml")))
      {
        long l2 = extractRightId(str1);
        DownloadLock localDownloadLock = new DownloadLock(l2);
        if (!localDownloadLock.exists())
          break label191;
        String str2 = localDownloadLock.getTitle();
        String str3 = localDownloadLock.getRightType();
        long l3 = localDownloadLock.getAssetId();
        Toast.makeText(paramContext, paramContext.getString(2131493265, new Object[] { str2 }), 1).show();
        localDownloadLock.delete();
        Logger.d("FlxMain", "DownloadBroadcastReceiver.onReceive registering");
        LockerRight localLockerRight = LockerRight.getMockInstance(l2, str2, str3, l3);
        Drm.manager().registerAssetOnDownloadComplete(localLockerRight);
      }
    }
    return;
    label191: Logger.w("FlxMain", "DownloadBroadcastReceiver.onReceive missing download lock");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.DownloadBroadcastReceiver
 * JD-Core Version:    0.6.2
 */