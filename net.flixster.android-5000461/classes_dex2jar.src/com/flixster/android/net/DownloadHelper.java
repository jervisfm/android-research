package com.flixster.android.net;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.DownloadLock;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackManager;
import com.flixster.android.storage.ExternalStorage;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.WorkerThreads;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.LockerRight.RightType;
import net.flixster.android.model.User;
import net.flixster.android.util.HttpHelper;

@TargetApi(9)
public class DownloadHelper
{
  public static final String FILE_PROTOCOL = "file://";
  public static final String WV_DIR = "wv/";
  private static final String[] units = { "B", "KB", "MB", "GB", "TB" };

  protected static void cancelDownload(long paramLong)
  {
    ((DownloadManager)FlixsterApplication.getContext().getSystemService("download")).remove(new long[] { paramLong });
  }

  public static void cancelMovieDownload(long paramLong)
  {
    try
    {
      if (isDownloaded(paramLong))
      {
        DownloadLock localDownloadLock = new DownloadLock(paramLong);
        if (localDownloadLock.exists())
        {
          cancelDownload(localDownloadLock.getDownloadId());
          localDownloadLock.delete();
        }
        String str = findDownloadCaptionsFile(paramLong);
        if (str != null)
          ExternalStorage.deleteFile(str);
      }
      return;
    }
    finally
    {
    }
  }

  private static void createAssetDownloadLock(LockerRight paramLockerRight, long paramLong)
  {
    DownloadLock localDownloadLock = new DownloadLock(paramLockerRight.rightId);
    localDownloadLock.setDownloadId(paramLong);
    localDownloadLock.setTitle(paramLockerRight.getTitle());
    localDownloadLock.setRightType(paramLockerRight.getRightType().name());
    localDownloadLock.setAssetId(paramLockerRight.assetId);
    localDownloadLock.save();
  }

  private static String createAssetFileName(long paramLong, String paramString)
  {
    return paramLong + getFileExtension(paramString);
  }

  private static String createCaptionsFileName(long paramLong)
  {
    return paramLong + ".ttml";
  }

  private static void createSubDir(String paramString)
  {
    File localFile = ExternalStorage.getExternalFilesDir(paramString);
    if ((!localFile.exists()) && (localFile.mkdirs()))
      Logger.d("FlxMain", "DownloadHelper.createSubDir created " + localFile);
  }

  public static boolean deleteDownloadedMovie(LockerRight paramLockerRight)
  {
    String str1 = findDownloadCaptionsFile(paramLockerRight.rightId);
    if (str1 != null)
      ExternalStorage.deleteFile(str1);
    String str2 = findDownloadedAsset(paramLockerRight.rightId, false);
    if (str2 == null)
      return false;
    Drm.manager().unregisterLocalAsset(paramLockerRight);
    return ExternalStorage.deleteFile(str2);
  }

  protected static long download(String paramString1, String paramString2, String paramString3)
  {
    if (!ExternalStorage.isWriteable())
      return -1L;
    createSubDir(paramString2);
    Context localContext = FlixsterApplication.getContext();
    DownloadManager localDownloadManager = (DownloadManager)localContext.getSystemService("download");
    DownloadManager.Request localRequest = new DownloadManager.Request(Uri.parse(paramString1));
    localRequest.setDestinationInExternalFilesDir(localContext, null, paramString2 + paramString3);
    long l = localDownloadManager.enqueue(localRequest);
    Logger.d("FlxMain", "DownloadHelper.download " + l + " to " + paramString2 + paramString3 + " from " + paramString1);
    return l;
  }

  public static void downloadMovie(LockerRight paramLockerRight)
  {
    try
    {
      if (isMovieDownloadInProgress(paramLockerRight.rightId))
        Logger.w("FlxMain", "DownloadHelper.downloadMovie aborted due to download in progress");
      while (true)
      {
        return;
        String str1 = paramLockerRight.getDownloadUri();
        if (str1 != null)
        {
          long l = download(str1, "wv/", createAssetFileName(paramLockerRight.rightId, str1));
          if (l != -1L)
          {
            createAssetDownloadLock(paramLockerRight, l);
            String str2 = paramLockerRight.getDownloadCaptionsUri();
            if ((str2 != null) && (!"".equals(str2)))
              download(str2, "wv/", createCaptionsFileName(paramLockerRight.rightId));
          }
        }
      }
    }
    finally
    {
    }
  }

  public static String findDownloadCaptionsFile(long paramLong)
  {
    File localFile = ExternalStorage.getExternalFilesDir("wv/");
    String[] arrayOfString;
    int i;
    if ((localFile.exists()) && (localFile.isDirectory()))
    {
      arrayOfString = localFile.list();
      if (arrayOfString != null)
        i = arrayOfString.length;
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return null;
      String str1 = arrayOfString[j];
      if (str1.equals(createCaptionsFileName(paramLong)))
      {
        String str2 = "file://" + localFile + "/" + str1;
        Logger.d("FlxDrm", "DownloadHelper.findDownloadCaptionsFile found " + str2);
        return str2;
      }
    }
  }

  private static String findDownloadedAsset(long paramLong, boolean paramBoolean)
  {
    return searchForNonLockNonRightFile(createAssetFileName(paramLong, null), "wv/", false);
  }

  public static String findDownloadedMovie(long paramLong)
  {
    return findDownloadedAsset(paramLong, false);
  }

  public static long findDownloadedMovieSize(long paramLong)
  {
    String str = findDownloadedAsset(paramLong, false);
    if (str.startsWith("file://"))
      str = str.substring(7);
    return new File(str).length();
  }

  public static long findRemainingSpace()
  {
    StatFs localStatFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
    long l = localStatFs.getAvailableBlocks() * localStatFs.getBlockSize();
    User localUser = AccountManager.instance().getUser();
    Iterator localIterator;
    if (localUser != null)
      localIterator = localUser.getLockerRights().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return l;
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      if (((localLockerRight.isMovie()) || (localLockerRight.isEpisode())) && (isMovieDownloadInProgress(localLockerRight.rightId)))
        l -= localLockerRight.getDownloadAssetSizeRaw() - findDownloadedMovieSize(localLockerRight.rightId);
    }
  }

  private static String getFileExtension(String paramString)
  {
    String str1 = "";
    if (paramString != null)
    {
      String str2 = paramString.split("\\?")[0];
      int i = str2.lastIndexOf(".");
      if (str2.lastIndexOf("/") < i)
        str1 = str2.substring(i);
    }
    return str1;
  }

  public static String getMovieDownloadProgress(LockerRight paramLockerRight)
  {
    long l1 = paramLockerRight.getDownloadAssetSizeRaw();
    String str1;
    String str2;
    if (l1 > 0L)
    {
      str1 = readableFileSize(l1);
      long l2 = findDownloadedMovieSize(paramLockerRight.rightId);
      if (l2 <= 0L)
        break label122;
      str2 = readableFileSizeCustom(l2, str1.substring(1 + str1.indexOf(" ")));
      label52: if (!str2.contains(str1.substring(1 + str1.indexOf(" "))))
        break label130;
    }
    label130: for (String str3 = str2.substring(0, str2.indexOf(" ")); ; str3 = "0")
    {
      return str3 + " of " + str1;
      str1 = "";
      break;
      label122: str2 = "";
      break label52;
    }
  }

  public static void getRemoteFileSize(String paramString, final Handler paramHandler)
  {
    WorkerThreads.instance().invokeLater(new Runnable()
    {
      public void run()
      {
        int i = -1;
        try
        {
          i = HttpHelper.getRemoteFileSize(new URL(DownloadHelper.this));
          Logger.d("FlxMain", "DownloadHelper.getRemoteFileSize " + DownloadHelper.this + " " + i);
          paramHandler.sendMessage(Message.obtain(null, 0, Integer.valueOf(i)));
          return;
        }
        catch (MalformedURLException localMalformedURLException)
        {
          while (true)
            Logger.e("FlxMain", "DownloadHelper.getRemoteFileSize " + DownloadHelper.this, localMalformedURLException);
        }
        catch (IOException localIOException)
        {
          while (true)
            Logger.e("FlxMain", "DownloadHelper.getRemoteFileSize " + DownloadHelper.this, localIOException);
        }
      }
    });
  }

  public static boolean isDownloaded(long paramLong)
  {
    String str = findDownloadedAsset(paramLong, false);
    boolean bool = false;
    if (str != null)
      bool = true;
    return bool;
  }

  public static boolean isDownloaded(LockerRight paramLockerRight)
  {
    if (paramLockerRight.isSeason())
    {
      Iterator localIterator = paramLockerRight.getChildRightIds().iterator();
      do
        if (!localIterator.hasNext())
          return false;
      while (!isDownloaded(((Long)localIterator.next()).longValue()));
      return true;
    }
    return isDownloaded(paramLockerRight.rightId);
  }

  public static boolean isMovieDownloadInProgress()
  {
    User localUser = AccountManager.instance().getUser();
    Iterator localIterator;
    if (localUser != null)
      localIterator = localUser.getLockerRights().iterator();
    LockerRight localLockerRight;
    do
    {
      if (!localIterator.hasNext())
        return false;
      localLockerRight = (LockerRight)localIterator.next();
    }
    while (((!localLockerRight.isMovie()) && (!localLockerRight.isEpisode())) || (!isMovieDownloadInProgress(localLockerRight.rightId)));
    return true;
  }

  public static boolean isMovieDownloadInProgress(long paramLong)
  {
    if (isDownloaded(paramLong))
      return new DownloadLock(paramLong).exists();
    return false;
  }

  public static boolean isMovieDownloadInProgress(LockerRight paramLockerRight)
  {
    if (paramLockerRight.isSeason())
    {
      Iterator localIterator = paramLockerRight.getChildRightIds().iterator();
      do
        if (!localIterator.hasNext())
          return false;
      while (!isMovieDownloadInProgress(((Long)localIterator.next()).longValue()));
      return true;
    }
    return isMovieDownloadInProgress(paramLockerRight.rightId);
  }

  protected static String queryDownloadManagerForFile(long paramLong)
  {
    DownloadManager localDownloadManager = (DownloadManager)FlixsterApplication.getContext().getSystemService("download");
    DownloadManager.Query localQuery = new DownloadManager.Query();
    localQuery.setFilterById(new long[] { paramLong });
    Cursor localCursor = localDownloadManager.query(localQuery);
    if (localCursor.moveToFirst())
    {
      int i = localCursor.getColumnIndex("status");
      if (8 == localCursor.getInt(i))
      {
        String str = localCursor.getString(localCursor.getColumnIndex("local_uri"));
        Logger.d("FlxMain", "DownloadHelper.queryDownloadManagerForFile found " + str);
        return str;
      }
      Logger.d("FlxMain", "DownloadHelper.queryDownloadManagerForFile non-successful status " + localCursor.getInt(i));
    }
    return null;
  }

  public static String readableFileSize(long paramLong)
  {
    return readableFileSizeCustom(paramLong, null);
  }

  public static String readableFileSizeCustom(long paramLong, String paramString)
  {
    if (paramLong <= 0L)
      return "0";
    int i;
    if (paramString == null)
    {
      i = (int)(Math.log10(paramLong) / Math.log10(1024.0D));
      if (i >= 3)
        break label108;
    }
    label108: for (String str = "##0"; ; str = "#.#")
    {
      return new DecimalFormat(str).format(paramLong / Math.pow(1024.0D, i)) + " " + units[i];
      if (paramString.equals("MB"))
      {
        i = 2;
        break;
      }
      i = 3;
      break;
    }
  }

  private static String searchForNonLockNonRightFile(String paramString1, String paramString2, boolean paramBoolean)
  {
    File localFile = ExternalStorage.getExternalFilesDir(paramString2);
    String[] arrayOfString;
    String str1;
    String str2;
    int j;
    if ((localFile.exists()) && (localFile.isDirectory()))
    {
      arrayOfString = localFile.list();
      if (arrayOfString != null)
      {
        str1 = null;
        str2 = null;
        int i = arrayOfString.length;
        j = 0;
        if (j < i)
          break label81;
        if ((!paramBoolean) || (str1 != null) || (str2 == null))
          break label230;
        Logger.i("FlxMain", "DownloadHelper.searchForNonLockNonRightFile removing lock file in limbo state");
        ExternalStorage.deleteFile(str2);
      }
    }
    label81: label230: 
    while (str1 == null)
    {
      return null;
      String str3 = arrayOfString[j];
      if (str3.startsWith(paramString1))
      {
        if ((str3.endsWith(".dlck")) || (str3.endsWith(".rght")) || (str3.endsWith(".ttml")))
          break label187;
        str1 = "file://" + localFile + "/" + str3;
        Logger.d("FlxMain", "DownloadHelper.searchForNonLockNonRightFile found " + str1);
      }
      while (true)
      {
        j++;
        break;
        if (str3.endsWith(".dlck"))
          str2 = "file://" + localFile + "/" + str3;
      }
    }
    label187: return str1;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.DownloadHelper
 * JD-Core Version:    0.6.2
 */