package com.flixster.android.net;

import java.io.IOException;

public class HttpUnauthorizedException extends IOException
{
  private static final long serialVersionUID = 2304460981362453296L;

  public HttpUnauthorizedException()
  {
  }

  public HttpUnauthorizedException(String paramString)
  {
    super(paramString);
  }

  public HttpUnauthorizedException(String paramString, Throwable paramThrowable)
  {
    super(paramString + paramThrowable.getMessage());
  }

  public int getResponseCode()
  {
    return 401;
  }

  public String getResponseMessage()
  {
    return getMessage();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.HttpUnauthorizedException
 * JD-Core Version:    0.6.2
 */