package com.flixster.android.net;

import com.flixster.android.net.ssl.SecureFlxHttpClient;
import com.flixster.android.utils.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import net.flixster.android.data.ApiBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class RestClient
{
  private final ArrayList<NameValuePair> headers;
  private String message;
  private final ArrayList<NameValuePair> params;
  private String response;
  private int responseCode;
  private final String url;

  public RestClient(String paramString)
  {
    this.url = paramString;
    this.params = new ArrayList();
    this.headers = new ArrayList();
  }

  private static String convertStreamToString(InputStream paramInputStream)
    throws IOException
  {
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramInputStream));
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      str = localBufferedReader.readLine();
      if (str != null);
    }
    finally
    {
      try
      {
        while (true)
        {
          String str;
          paramInputStream.close();
          label39: return localStringBuilder.toString();
          localStringBuilder.append(str + "\n");
        }
        localObject = finally;
        try
        {
          paramInputStream.close();
          label77: throw localObject;
        }
        catch (IOException localIOException1)
        {
          break label77;
        }
      }
      catch (IOException localIOException2)
      {
        break label39;
      }
    }
  }

  private void executeRequest(HttpUriRequest paramHttpUriRequest, String paramString)
    throws IOException
  {
    boolean bool = ApiBuilder.isSecureFlxUrl(paramString);
    if (bool)
    {
      Logger.d("FlxApi", "RestClient.executeRequest https://...");
      if (!bool)
        break label133;
    }
    label133: for (Object localObject = new SecureFlxHttpClient(); ; localObject = new DefaultHttpClient())
    {
      HttpResponse localHttpResponse = ((HttpClient)localObject).execute(paramHttpUriRequest);
      this.responseCode = localHttpResponse.getStatusLine().getStatusCode();
      this.message = localHttpResponse.getStatusLine().getReasonPhrase();
      HttpEntity localHttpEntity = localHttpResponse.getEntity();
      if (localHttpEntity != null)
      {
        InputStream localInputStream = localHttpEntity.getContent();
        this.response = convertStreamToString(localInputStream);
        localInputStream.close();
      }
      return;
      Logger.d("FlxApi", "RestClient.executeRequest " + paramString);
      break;
    }
  }

  public void addHeader(String paramString1, String paramString2)
  {
    this.headers.add(new BasicNameValuePair(paramString1, paramString2));
  }

  public void addParam(String paramString1, String paramString2)
  {
    this.params.add(new BasicNameValuePair(paramString1, paramString2));
  }

  public void addParam(ArrayList<NameValuePair> paramArrayList)
  {
    this.params.addAll(paramArrayList);
  }

  public void execute(RequestMethod paramRequestMethod)
    throws IOException
  {
    switch ($SWITCH_TABLE$com$flixster$android$net$RestClient$RequestMethod()[paramRequestMethod.ordinal()])
    {
    default:
      return;
    case 1:
      String str1 = "";
      Iterator localIterator3;
      HttpGet localHttpGet;
      Iterator localIterator2;
      if (!this.params.isEmpty())
      {
        str1 = str1 + "?";
        localIterator3 = this.params.iterator();
        if (localIterator3.hasNext());
      }
      else
      {
        localHttpGet = new HttpGet(this.url + str1);
        localIterator2 = this.headers.iterator();
      }
      while (true)
      {
        if (!localIterator2.hasNext())
        {
          executeRequest(localHttpGet, this.url);
          return;
          NameValuePair localNameValuePair3 = (NameValuePair)localIterator3.next();
          String str2 = localNameValuePair3.getName() + "=" + URLEncoder.encode(localNameValuePair3.getValue(), "UTF-8");
          if (str1.length() > 1)
          {
            str1 = str1 + "&" + str2;
            break;
          }
          str1 = str1 + str2;
          break;
        }
        NameValuePair localNameValuePair2 = (NameValuePair)localIterator2.next();
        localHttpGet.addHeader(localNameValuePair2.getName(), localNameValuePair2.getValue());
      }
    case 2:
    }
    HttpPost localHttpPost = new HttpPost(this.url);
    Iterator localIterator1 = this.headers.iterator();
    while (true)
    {
      if (!localIterator1.hasNext())
      {
        if (!this.params.isEmpty())
          localHttpPost.setEntity(new UrlEncodedFormEntity(this.params, "UTF-8"));
        executeRequest(localHttpPost, this.url);
        return;
      }
      NameValuePair localNameValuePair1 = (NameValuePair)localIterator1.next();
      localHttpPost.addHeader(localNameValuePair1.getName(), localNameValuePair1.getValue());
    }
  }

  public String getErrorMessage()
  {
    return this.message;
  }

  public String getResponse()
  {
    return this.response;
  }

  public int getResponseCode()
  {
    return this.responseCode;
  }

  public static enum RequestMethod
  {
    static
    {
      RequestMethod[] arrayOfRequestMethod = new RequestMethod[2];
      arrayOfRequestMethod[0] = GET;
      arrayOfRequestMethod[1] = POST;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.RestClient
 * JD-Core Version:    0.6.2
 */