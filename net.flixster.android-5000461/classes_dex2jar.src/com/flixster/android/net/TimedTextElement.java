package com.flixster.android.net;

public class TimedTextElement
{
  protected static final String ATTR_BEGIN = "begin";
  protected static final String ATTR_DROP_MODE = "ttp:dropMode";
  protected static final String ATTR_END = "end";
  protected static final String ATTR_FRAME_RATE = "ttp:frameRate";
  protected static final String ATTR_FRAME_RATE_MULTIPLIER = "ttp:frameRateMultiplier";
  protected static final String ATTR_ORIGIN = "tts:origin";
  public static final String LOCAL_FILE_EXTENSION = ".ttml";
  protected static final String TAG_P = "p";
  protected static final String TAG_TT = "tt";
  public static final String TEST_URL = "http://us1cc.res.com.edgesuite.net/p/prometheus_19cf6ad4_cc_lid_0_1.xml";
  public final int begin;
  public final int end;
  public final int originX;
  public final int originY;
  public final int region;
  public final String text;

  protected TimedTextElement(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7)
  {
    double d = 0.0D;
    if (paramString7 != null)
    {
      String[] arrayOfString4 = paramString7.split(" ");
      int i = Integer.parseInt(arrayOfString4[0]);
      int j = Integer.parseInt(arrayOfString4[1]);
      d = Integer.parseInt(paramString6) * i / j;
    }
    SmpteDropMode localSmpteDropMode = SmpteDropMode.match(paramString5);
    SmpteFrameRate localSmpteFrameRate = SmpteFrameRate.SMPTE_30;
    switch ($SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteDropMode()[localSmpteDropMode.ordinal()])
    {
    default:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      String[] arrayOfString1 = paramString1.split(":");
      this.begin = ((int)convertSmpteToAbsoluteTime(Integer.parseInt(arrayOfString1[0]), Integer.parseInt(arrayOfString1[1]), Integer.parseInt(arrayOfString1[2]), Integer.parseInt(arrayOfString1[3]), localSmpteFrameRate));
      String[] arrayOfString2 = paramString2.split(":");
      this.end = ((int)convertSmpteToAbsoluteTime(Integer.parseInt(arrayOfString2[0]), Integer.parseInt(arrayOfString2[1]), Integer.parseInt(arrayOfString2[2]), Integer.parseInt(arrayOfString2[3]), localSmpteFrameRate));
      this.region = paramInt;
      String[] arrayOfString3 = paramString3.replace("%", "").split(" ");
      this.originX = Integer.parseInt(arrayOfString3[0]);
      this.originY = Integer.parseInt(arrayOfString3[1]);
      this.text = paramString4;
      return;
      localSmpteFrameRate = SmpteFrameRate.SMPTE_2997_DROP;
      continue;
      localSmpteFrameRate = SmpteFrameRate.match(d);
    }
  }

  private long convertSmpteToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4, SmpteFrameRate paramSmpteFrameRate)
  {
    switch ($SWITCH_TABLE$com$flixster$android$net$TimedTextElement$SmpteFrameRate()[paramSmpteFrameRate.ordinal()])
    {
    default:
      return 0L;
    case 1:
      return smpte23_98_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
    case 2:
      return smpte24_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
    case 3:
      return smpte25_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
    case 4:
      return smpte29_97_Drop_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
    case 5:
      return smpte29_97_NonDrop_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
    case 6:
    }
    return smpte30_ToAbsoluteTime(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  private long smpte23_98_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * ()(Math.ceil(3753.75D * paramInt4) + 90090L * (paramInt3 + 60 * (paramInt2 + paramInt1 * 60))) / 90000L;
  }

  private long smpte24_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * (paramInt4 * 3750 + 90000L * (paramInt3 + 60 * (paramInt2 + paramInt1 * 60))) / 90000L;
  }

  private long smpte25_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * (paramInt4 * 3600 + 90000L * (paramInt3 + 60 * (paramInt2 + paramInt1 * 60))) / 90000L;
  }

  private long smpte29_97_Drop_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * (paramInt4 * 3003 + 90090 * paramInt3 + 26999973 * paramInt2 / 5 + 323999676L * paramInt1) / 90000L;
  }

  private long smpte29_97_NonDrop_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * (paramInt4 * 3003 + 90090L * (paramInt3 + 60 * (paramInt2 + paramInt1 * 60))) / 90000L;
  }

  private long smpte30_ToAbsoluteTime(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return 1000L * (paramInt4 * 3000 + 90000L * (paramInt3 + 60 * (paramInt2 + paramInt1 * 60))) / 90000L;
  }

  private static enum SmpteDropMode
  {
    static
    {
      SmpteDropMode[] arrayOfSmpteDropMode = new SmpteDropMode[3];
      arrayOfSmpteDropMode[0] = DROP_NTSC;
      arrayOfSmpteDropMode[1] = DROP_PAL;
      arrayOfSmpteDropMode[2] = NON_DROP;
    }

    protected static SmpteDropMode match(String paramString)
    {
      if ("dropNTSC".equals(paramString))
        return DROP_NTSC;
      if ("dropPAL".equals(paramString))
        return DROP_PAL;
      if ("nonDrop".equals(paramString))
        return NON_DROP;
      return NON_DROP;
    }
  }

  private static enum SmpteFrameRate
  {
    static
    {
      SmpteFrameRate[] arrayOfSmpteFrameRate = new SmpteFrameRate[6];
      arrayOfSmpteFrameRate[0] = SMPTE_2398;
      arrayOfSmpteFrameRate[1] = SMPTE_24;
      arrayOfSmpteFrameRate[2] = SMPTE_25;
      arrayOfSmpteFrameRate[3] = SMPTE_2997_DROP;
      arrayOfSmpteFrameRate[4] = SMPTE_2997_NONDROP;
      arrayOfSmpteFrameRate[5] = SMPTE_30;
    }

    protected static SmpteFrameRate match(double paramDouble)
    {
      switch ((int)Math.floor(paramDouble))
      {
      default:
        return SMPTE_30;
      case 23:
        return SMPTE_2398;
      case 24:
        return SMPTE_24;
      case 25:
        return SMPTE_25;
      case 29:
        return SMPTE_2997_NONDROP;
      case 30:
        return SMPTE_30;
      case 50:
        return SMPTE_25;
      case 60:
        return SMPTE_30;
      case 59:
      }
      return SMPTE_2997_NONDROP;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.TimedTextElement
 * JD-Core Version:    0.6.2
 */