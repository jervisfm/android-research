package com.flixster.android.net.ssl;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import com.flixster.android.utils.ActivityHolder;
import com.flixster.android.utils.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class EasySSLSocketFactory
  implements SocketFactory, LayeredSocketFactory
{
  private static KeyStore addTrustRoot;
  private SSLContext sslContext;

  private static SSLContext createEasySSLContext()
    throws IOException
  {
    try
    {
      SSLContext localSSLContext = SSLContext.getInstance("TLS");
      TrustManager[] arrayOfTrustManager = new TrustManager[1];
      arrayOfTrustManager[0] = new EasyX509TrustManager(getAddTrustRootCert());
      localSSLContext.init(null, arrayOfTrustManager, null);
      return localSSLContext;
    }
    catch (Exception localException)
    {
      throw new IOException(localException.getMessage());
    }
  }

  private static KeyStore getAddTrustRootCert()
    throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
  {
    KeyStore localKeyStore;
    InputStream localInputStream;
    if (addTrustRoot == null)
    {
      Activity localActivity = ActivityHolder.instance().getTopLevelActivity();
      localKeyStore = KeyStore.getInstance("BKS");
      localInputStream = localActivity.getResources().openRawResource(2131099648);
    }
    try
    {
      localKeyStore.load(localInputStream, "flix208utah".toCharArray());
      Logger.d("FlxMain", "EasySSLSocketFactory.getAddTrustRootCert: " + localKeyStore.size());
      localInputStream.close();
      addTrustRoot = localKeyStore;
      return addTrustRoot;
    }
    finally
    {
      localInputStream.close();
    }
  }

  private SSLContext getSSLContext()
    throws IOException
  {
    if (this.sslContext == null)
      this.sslContext = createEasySSLContext();
    return this.sslContext;
  }

  public Socket connectSocket(Socket paramSocket, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, HttpParams paramHttpParams)
    throws IOException, UnknownHostException, ConnectTimeoutException
  {
    int i = HttpConnectionParams.getConnectionTimeout(paramHttpParams);
    int j = HttpConnectionParams.getSoTimeout(paramHttpParams);
    InetSocketAddress localInetSocketAddress = new InetSocketAddress(paramString, paramInt1);
    if (paramSocket != null);
    for (Socket localSocket = paramSocket; ; localSocket = createSocket())
    {
      SSLSocket localSSLSocket = (SSLSocket)localSocket;
      if ((paramInetAddress != null) || (paramInt2 > 0))
      {
        if (paramInt2 < 0)
          paramInt2 = 0;
        localSSLSocket.bind(new InetSocketAddress(paramInetAddress, paramInt2));
      }
      localSSLSocket.connect(localInetSocketAddress, i);
      localSSLSocket.setSoTimeout(j);
      return localSSLSocket;
    }
  }

  public Socket createSocket()
    throws IOException
  {
    return getSSLContext().getSocketFactory().createSocket();
  }

  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
    throws IOException, UnknownHostException
  {
    return getSSLContext().getSocketFactory().createSocket(paramSocket, paramString, paramInt, paramBoolean);
  }

  public boolean equals(Object paramObject)
  {
    return (paramObject != null) && (paramObject.getClass().equals(EasySSLSocketFactory.class));
  }

  public int hashCode()
  {
    return EasySSLSocketFactory.class.hashCode();
  }

  public boolean isSecure(Socket paramSocket)
    throws IllegalArgumentException
  {
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.ssl.EasySSLSocketFactory
 * JD-Core Version:    0.6.2
 */