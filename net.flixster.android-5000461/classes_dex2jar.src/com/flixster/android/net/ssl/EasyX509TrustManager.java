package com.flixster.android.net.ssl;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class EasyX509TrustManager
  implements X509TrustManager
{
  private final X509TrustManager standardTrustManager;

  public EasyX509TrustManager(KeyStore paramKeyStore)
    throws NoSuchAlgorithmException, KeyStoreException
  {
    TrustManagerFactory localTrustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
    localTrustManagerFactory.init(paramKeyStore);
    TrustManager[] arrayOfTrustManager = localTrustManagerFactory.getTrustManagers();
    if (arrayOfTrustManager.length == 0)
      throw new NoSuchAlgorithmException("no trust manager found");
    this.standardTrustManager = ((X509TrustManager)arrayOfTrustManager[0]);
  }

  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
    throws CertificateException
  {
    this.standardTrustManager.checkClientTrusted(paramArrayOfX509Certificate, paramString);
  }

  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
    throws CertificateException
  {
    paramArrayOfX509Certificate.length;
    int i;
    if (paramArrayOfX509Certificate.length > 1)
    {
      i = 0;
      if (i < paramArrayOfX509Certificate.length)
        break label90;
      label19: int n = i + 1;
      X509Certificate localX509Certificate2 = paramArrayOfX509Certificate[(n - 1)];
      Date localDate = new Date();
      if ((localX509Certificate2.getSubjectDN().equals(localX509Certificate2.getIssuerDN())) && (localDate.after(localX509Certificate2.getNotAfter())))
        (n - 1);
    }
    this.standardTrustManager.checkServerTrusted(paramArrayOfX509Certificate, paramString);
    return;
    label90: for (int j = i + 1; ; j++)
    {
      int k = paramArrayOfX509Certificate.length;
      int m = 0;
      if (j >= k);
      while (true)
      {
        if (m == 0)
          break label180;
        i++;
        break;
        if (!paramArrayOfX509Certificate[i].getIssuerDN().equals(paramArrayOfX509Certificate[j].getSubjectDN()))
          break label182;
        m = 1;
        if (j != i + 1)
        {
          X509Certificate localX509Certificate1 = paramArrayOfX509Certificate[j];
          paramArrayOfX509Certificate[j] = paramArrayOfX509Certificate[(i + 1)];
          paramArrayOfX509Certificate[(i + 1)] = localX509Certificate1;
        }
      }
      break label19;
    }
  }

  public X509Certificate[] getAcceptedIssuers()
  {
    return this.standardTrustManager.getAcceptedIssuers();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.ssl.EasyX509TrustManager
 * JD-Core Version:    0.6.2
 */