package com.flixster.android.net.ssl;

import com.flixster.android.utils.Logger;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

public class SecureFlxHttpClient extends DefaultHttpClient
{
  protected ClientConnectionManager createClientConnectionManager()
  {
    Logger.d("FlxMain", "SecureFlxHttpClient.createClientConnectionManager");
    SchemeRegistry localSchemeRegistry = new SchemeRegistry();
    localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    localSchemeRegistry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
    return new SingleClientConnManager(getParams(), localSchemeRegistry);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.net.ssl.SecureFlxHttpClient
 * JD-Core Version:    0.6.2
 */