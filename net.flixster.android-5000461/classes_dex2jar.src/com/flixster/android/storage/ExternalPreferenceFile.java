package com.flixster.android.storage;

public abstract class ExternalPreferenceFile
{
  public static final String DELIMITER = "ZZZZ";
  public static final String NONE = ".";

  protected abstract String createFileContent();

  protected abstract String createFileName();

  protected abstract String createFileSubDir();

  public boolean delete()
  {
    return ExternalStorage.deleteFile(createFileSubDir(), createFileName());
  }

  protected abstract boolean parseFileContent(String paramString);

  public boolean read()
  {
    return parseFileContent(ExternalStorage.readFile(createFileSubDir(), createFileName()));
  }

  public void save()
  {
    ExternalStorage.writeFile(createFileContent(), createFileSubDir(), createFileName());
  }

  public String[] split(String paramString)
  {
    String[] arrayOfString = (String[])null;
    if (paramString != null)
      arrayOfString = paramString.split("ZZZZ");
    return arrayOfString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.storage.ExternalPreferenceFile
 * JD-Core Version:    0.6.2
 */