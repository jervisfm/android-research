package com.flixster.android.storage;

import android.os.Environment;
import com.flixster.android.utils.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

public class ExternalStorage
{
  private static final String EXTERNAL_STORAGE_PATH = "/Android/data/net.flixster.android/files/";

  public static boolean deleteFile(String paramString)
  {
    if ((!isAvailable()) || (isReadOnly()))
      Logger.w("FlxMain", "ExternalStorage.deleteFile storage unavailable or read-only");
    File localFile;
    do
    {
      return false;
      if (paramString.startsWith("file://"))
        paramString = paramString.substring(7);
      localFile = new File(paramString);
    }
    while ((!localFile.exists()) || (!localFile.delete()));
    Logger.d("FlxMain", "ExternalStorage.deleteFile removed " + localFile);
    return true;
  }

  public static boolean deleteFile(String paramString1, String paramString2)
  {
    return deleteFile(Environment.getExternalStorageDirectory() + "/Android/data/net.flixster.android/files/" + paramString1 + paramString2);
  }

  public static boolean findFile(String paramString)
  {
    if (!isAvailable())
    {
      Logger.w("FlxMain", "ExternalStorage.findFile storage unavailable");
      return false;
    }
    if (paramString.startsWith("file://"))
      paramString = paramString.substring(7);
    return new File(paramString).exists();
  }

  public static Collection<String> findFiles(String paramString1, String paramString2)
  {
    Object localObject = null;
    if (!isAvailable())
      Logger.w("FlxMain", "ExternalStorage.findFiles storage unavailable");
    while (true)
    {
      return localObject;
      File localFile1 = Environment.getExternalStorageDirectory();
      File localFile2 = new File(localFile1, "/Android/data/net.flixster.android/files/" + paramString1);
      boolean bool = localFile2.exists();
      localObject = null;
      if (bool)
      {
        String[] arrayOfString = localFile2.list();
        localObject = new ArrayList(arrayOfString.length);
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
        {
          String str = arrayOfString[j];
          if (str.endsWith(paramString2))
            ((ArrayList)localObject).add(localFile1 + "/Android/data/net.flixster.android/files/" + paramString1 + str);
        }
      }
    }
  }

  public static File getExternalFilesDir(String paramString)
  {
    return new File(Environment.getExternalStorageDirectory(), "/Android/data/net.flixster.android/files/" + paramString);
  }

  public static boolean isAvailable()
  {
    return "mounted".equals(Environment.getExternalStorageState());
  }

  public static boolean isReadOnly()
  {
    return "mounted_ro".equals(Environment.getExternalStorageState());
  }

  public static boolean isWriteable()
  {
    return (isAvailable()) && (!isReadOnly());
  }

  // ERROR //
  public static String readFile(String paramString)
  {
    // Byte code:
    //   0: invokestatic 18	com/flixster/android/storage/ExternalStorage:isAvailable	()Z
    //   3: ifeq +9 -> 12
    //   6: invokestatic 21	com/flixster/android/storage/ExternalStorage:isReadOnly	()Z
    //   9: ifeq +12 -> 21
    //   12: ldc 23
    //   14: ldc 131
    //   16: invokestatic 31	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;)V
    //   19: aconst_null
    //   20: areturn
    //   21: aload_0
    //   22: ldc 33
    //   24: invokevirtual 38	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   27: ifeq +10 -> 37
    //   30: aload_0
    //   31: bipush 7
    //   33: invokevirtual 42	java/lang/String:substring	(I)Ljava/lang/String;
    //   36: astore_0
    //   37: new 44	java/io/File
    //   40: dup
    //   41: aload_0
    //   42: invokespecial 47	java/io/File:<init>	(Ljava/lang/String;)V
    //   45: astore_1
    //   46: aload_1
    //   47: invokevirtual 50	java/io/File:exists	()Z
    //   50: ifne +31 -> 81
    //   53: ldc 23
    //   55: new 55	java/lang/StringBuilder
    //   58: dup
    //   59: ldc 133
    //   61: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   64: aload_1
    //   65: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   68: ldc 135
    //   70: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: invokestatic 69	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   79: aconst_null
    //   80: areturn
    //   81: aconst_null
    //   82: astore_2
    //   83: new 137	java/io/BufferedInputStream
    //   86: dup
    //   87: new 139	java/io/FileInputStream
    //   90: dup
    //   91: aload_1
    //   92: invokespecial 142	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   95: invokespecial 145	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   98: astore_3
    //   99: new 35	java/lang/String
    //   102: dup
    //   103: aload_3
    //   104: invokestatic 151	net/flixster/android/util/HttpHelper:streamToByteArray	(Ljava/io/InputStream;)[B
    //   107: invokespecial 154	java/lang/String:<init>	([B)V
    //   110: astore 4
    //   112: aload_3
    //   113: ifnull +219 -> 332
    //   116: aload_3
    //   117: invokevirtual 159	java/io/InputStream:close	()V
    //   120: aload 4
    //   122: astore 5
    //   124: aload 5
    //   126: areturn
    //   127: astore 7
    //   129: ldc 23
    //   131: new 55	java/lang/StringBuilder
    //   134: dup
    //   135: ldc 133
    //   137: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   140: aload_1
    //   141: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   144: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   147: aload 7
    //   149: invokestatic 162	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   152: aconst_null
    //   153: astore 5
    //   155: aload_2
    //   156: ifnull -32 -> 124
    //   159: aload_2
    //   160: invokevirtual 159	java/io/InputStream:close	()V
    //   163: aconst_null
    //   164: astore 5
    //   166: goto -42 -> 124
    //   169: astore 10
    //   171: aconst_null
    //   172: astore 5
    //   174: goto -50 -> 124
    //   177: astore 11
    //   179: ldc 23
    //   181: new 55	java/lang/StringBuilder
    //   184: dup
    //   185: ldc 133
    //   187: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   190: aload_1
    //   191: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   194: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   197: aload 11
    //   199: invokestatic 162	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   202: aconst_null
    //   203: astore 5
    //   205: aload_2
    //   206: ifnull -82 -> 124
    //   209: aload_2
    //   210: invokevirtual 159	java/io/InputStream:close	()V
    //   213: aconst_null
    //   214: astore 5
    //   216: goto -92 -> 124
    //   219: astore 12
    //   221: aconst_null
    //   222: astore 5
    //   224: goto -100 -> 124
    //   227: astore 13
    //   229: ldc 23
    //   231: new 55	java/lang/StringBuilder
    //   234: dup
    //   235: ldc 133
    //   237: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   240: aload_1
    //   241: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   244: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   247: aload 13
    //   249: invokestatic 162	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   252: aconst_null
    //   253: astore 5
    //   255: aload_2
    //   256: ifnull -132 -> 124
    //   259: aload_2
    //   260: invokevirtual 159	java/io/InputStream:close	()V
    //   263: aconst_null
    //   264: astore 5
    //   266: goto -142 -> 124
    //   269: astore 14
    //   271: aconst_null
    //   272: astore 5
    //   274: goto -150 -> 124
    //   277: astore 8
    //   279: aload_2
    //   280: ifnull +7 -> 287
    //   283: aload_2
    //   284: invokevirtual 159	java/io/InputStream:close	()V
    //   287: aload 8
    //   289: athrow
    //   290: astore 6
    //   292: aload 4
    //   294: astore 5
    //   296: goto -172 -> 124
    //   299: astore 9
    //   301: goto -14 -> 287
    //   304: astore 8
    //   306: aload_3
    //   307: astore_2
    //   308: goto -29 -> 279
    //   311: astore 13
    //   313: aload_3
    //   314: astore_2
    //   315: goto -86 -> 229
    //   318: astore 11
    //   320: aload_3
    //   321: astore_2
    //   322: goto -143 -> 179
    //   325: astore 7
    //   327: aload_3
    //   328: astore_2
    //   329: goto -200 -> 129
    //   332: aload 4
    //   334: astore 5
    //   336: goto -212 -> 124
    //
    // Exception table:
    //   from	to	target	type
    //   83	99	127	java/io/FileNotFoundException
    //   159	163	169	java/io/IOException
    //   83	99	177	java/io/IOException
    //   209	213	219	java/io/IOException
    //   83	99	227	java/lang/Exception
    //   259	263	269	java/io/IOException
    //   83	99	277	finally
    //   129	152	277	finally
    //   179	202	277	finally
    //   229	252	277	finally
    //   116	120	290	java/io/IOException
    //   283	287	299	java/io/IOException
    //   99	112	304	finally
    //   99	112	311	java/lang/Exception
    //   99	112	318	java/io/IOException
    //   99	112	325	java/io/FileNotFoundException
  }

  public static String readFile(String paramString1, String paramString2)
  {
    return readFile(Environment.getExternalStorageDirectory() + "/Android/data/net.flixster.android/files/" + paramString1 + paramString2);
  }

  public static boolean renameFile(String paramString1, String paramString2, String paramString3)
  {
    if ((!isAvailable()) || (isReadOnly()))
      Logger.w("FlxMain", "ExternalStorage.readFile storage unavailable or read-only");
    File localFile;
    do
    {
      return false;
      if (paramString1.startsWith("file://"))
        paramString1 = paramString1.substring(7);
      localFile = new File(paramString1);
    }
    while (!localFile.exists());
    return localFile.renameTo(new File(Environment.getExternalStorageDirectory() + "/Android/data/net.flixster.android/files/" + paramString2 + paramString3));
  }

  // ERROR //
  public static boolean writeFile(String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: invokestatic 18	com/flixster/android/storage/ExternalStorage:isAvailable	()Z
    //   3: ifeq +9 -> 12
    //   6: invokestatic 21	com/flixster/android/storage/ExternalStorage:isReadOnly	()Z
    //   9: ifeq +12 -> 21
    //   12: ldc 23
    //   14: ldc 174
    //   16: invokestatic 31	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;)V
    //   19: iconst_0
    //   20: ireturn
    //   21: new 44	java/io/File
    //   24: dup
    //   25: invokestatic 77	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   28: new 55	java/lang/StringBuilder
    //   31: dup
    //   32: ldc 8
    //   34: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   37: aload_1
    //   38: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   44: invokespecial 92	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   47: astore_3
    //   48: aload_3
    //   49: invokevirtual 50	java/io/File:exists	()Z
    //   52: ifne +8 -> 60
    //   55: aload_3
    //   56: invokevirtual 177	java/io/File:mkdirs	()Z
    //   59: pop
    //   60: new 44	java/io/File
    //   63: dup
    //   64: aload_3
    //   65: aload_2
    //   66: invokespecial 92	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   69: astore 4
    //   71: aconst_null
    //   72: astore 5
    //   74: new 179	java/io/FileOutputStream
    //   77: dup
    //   78: aload 4
    //   80: invokespecial 180	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   83: astore 6
    //   85: aload 6
    //   87: aload_0
    //   88: invokevirtual 184	java/lang/String:getBytes	()[B
    //   91: invokevirtual 189	java/io/OutputStream:write	([B)V
    //   94: aload 6
    //   96: invokevirtual 192	java/io/OutputStream:flush	()V
    //   99: iconst_1
    //   100: istore 10
    //   102: aload 6
    //   104: ifnull +216 -> 320
    //   107: aload 6
    //   109: invokevirtual 193	java/io/OutputStream:close	()V
    //   112: new 55	java/lang/StringBuilder
    //   115: dup
    //   116: ldc 195
    //   118: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   121: aload 4
    //   123: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   126: astore 12
    //   128: iload 10
    //   130: ifeq +151 -> 281
    //   133: ldc 197
    //   135: astore 13
    //   137: ldc 23
    //   139: aload 12
    //   141: aload 13
    //   143: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   149: invokestatic 69	com/flixster/android/utils/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   152: iload 10
    //   154: ireturn
    //   155: astore 7
    //   157: ldc 23
    //   159: new 55	java/lang/StringBuilder
    //   162: dup
    //   163: ldc 195
    //   165: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   168: aload 4
    //   170: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   173: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   176: aload 7
    //   178: invokestatic 162	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   181: iconst_0
    //   182: istore 10
    //   184: aload 5
    //   186: ifnull -74 -> 112
    //   189: aload 5
    //   191: invokevirtual 193	java/io/OutputStream:close	()V
    //   194: iconst_0
    //   195: istore 10
    //   197: goto -85 -> 112
    //   200: astore 11
    //   202: iconst_0
    //   203: istore 10
    //   205: goto -93 -> 112
    //   208: astore 14
    //   210: ldc 23
    //   212: new 55	java/lang/StringBuilder
    //   215: dup
    //   216: ldc 195
    //   218: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   221: aload 4
    //   223: invokevirtual 62	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   226: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   229: aload 14
    //   231: invokestatic 162	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   234: iconst_0
    //   235: istore 10
    //   237: aload 5
    //   239: ifnull -127 -> 112
    //   242: aload 5
    //   244: invokevirtual 193	java/io/OutputStream:close	()V
    //   247: iconst_0
    //   248: istore 10
    //   250: goto -138 -> 112
    //   253: astore 15
    //   255: iconst_0
    //   256: istore 10
    //   258: goto -146 -> 112
    //   261: astore 8
    //   263: aload 5
    //   265: ifnull +8 -> 273
    //   268: aload 5
    //   270: invokevirtual 193	java/io/OutputStream:close	()V
    //   273: aload 8
    //   275: athrow
    //   276: astore 16
    //   278: goto -166 -> 112
    //   281: ldc 199
    //   283: astore 13
    //   285: goto -148 -> 137
    //   288: astore 9
    //   290: goto -17 -> 273
    //   293: astore 8
    //   295: aload 6
    //   297: astore 5
    //   299: goto -36 -> 263
    //   302: astore 14
    //   304: aload 6
    //   306: astore 5
    //   308: goto -98 -> 210
    //   311: astore 7
    //   313: aload 6
    //   315: astore 5
    //   317: goto -160 -> 157
    //   320: goto -208 -> 112
    //
    // Exception table:
    //   from	to	target	type
    //   74	85	155	java/io/IOException
    //   189	194	200	java/io/IOException
    //   74	85	208	java/lang/Exception
    //   242	247	253	java/io/IOException
    //   74	85	261	finally
    //   157	181	261	finally
    //   210	234	261	finally
    //   107	112	276	java/io/IOException
    //   268	273	288	java/io/IOException
    //   85	99	293	finally
    //   85	99	302	java/lang/Exception
    //   85	99	311	java/io/IOException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.storage.ExternalStorage
 * JD-Core Version:    0.6.2
 */