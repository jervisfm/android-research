package com.flixster.android.utils;

import android.app.Activity;

public class ActivityHolder
{
  private static final ActivityHolder INSTANCE = new ActivityHolder();
  private int ctr;
  private Activity topLevel;

  public static ActivityHolder instance()
  {
    return INSTANCE;
  }

  public void clear()
  {
    try
    {
      this.topLevel = null;
      this.ctr = 0;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public Activity getTopLevelActivity()
  {
    try
    {
      Activity localActivity = this.topLevel;
      return localActivity;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void removeTopLevelActivity(int paramInt)
  {
    try
    {
      if (paramInt >= this.ctr)
        this.topLevel = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public int setTopLevelActivity(Activity paramActivity)
  {
    try
    {
      this.topLevel = paramActivity;
      int i = 1 + this.ctr;
      this.ctr = i;
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ActivityHolder
 * JD-Core Version:    0.6.2
 */