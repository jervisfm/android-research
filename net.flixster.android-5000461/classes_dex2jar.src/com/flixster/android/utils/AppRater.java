package com.flixster.android.utils;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import net.flixster.android.Starter;

public class AppRater
{
  private static final String AMAZON_APPSTORE_LINK = "http://www.amazon.com/gp/mas/dl/android?p=";
  private static final String APP_PNAME = "net.flixster.android";
  public static final boolean IS_DISABLED = false;
  private static final int LAUNCHES_UNTIL_PROMPT = 10;
  private static final String MARKET_DEEP_LINK = "market://details?id=";

  public static void appLaunched(Context paramContext)
  {
    try
    {
      PackageInfo localPackageInfo2 = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0);
      localPackageInfo1 = localPackageInfo2;
      localSharedPreferences = paramContext.getSharedPreferences("apprater", 0);
      localEditor = localSharedPreferences.edit();
      if (localSharedPreferences.getLong("lastRunVersion", 0L) < localPackageInfo1.versionCode)
      {
        localEditor.putBoolean("dontshowagain", false);
        localEditor.putLong("launch_count", 0L);
        localEditor.putLong("lastRunVersion", localPackageInfo1.versionCode);
        localEditor.commit();
      }
      if (localSharedPreferences.getBoolean("dontshowagain", false))
      {
        Logger.d("FlxMain", "AppRater.appLaunched rate dialog disabled");
        return;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      SharedPreferences localSharedPreferences;
      SharedPreferences.Editor localEditor;
      while (true)
      {
        localNameNotFoundException.printStackTrace();
        PackageInfo localPackageInfo1 = null;
      }
      long l = 1L + localSharedPreferences.getLong("launch_count", 0L);
      Logger.d("FlxMain", "AppRater.appLaunched launch count: " + l);
      localEditor.putLong("launch_count", l);
      localEditor.commit();
    }
  }

  public static void rateThisApp(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("apprater", 0).edit();
    Starter.tryLaunch(paramContext, new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=net.flixster.android")));
    if (localEditor != null)
    {
      localEditor.putBoolean("dontshowagain", true);
      localEditor.commit();
    }
  }

  public static void showRateDialog(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("apprater", 0);
    if (localSharedPreferences.getBoolean("dontshowagain", false));
    while (localSharedPreferences.getLong("launch_count", 0L) < 10L)
      return;
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    Trackers.instance().track("/rating-prompt", "Rating - Prompt");
    new AlertDialog.Builder(paramContext).setCancelable(true).setTitle(2131493291).setMessage(2131493292).setPositiveButton(2131493294, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Trackers.instance().trackEvent("/rating-prompt", "Rating - Prompt", "RatingPrompt", "RateIt");
        AppRater.rateThisApp(AppRater.this);
      }
    }).setNegativeButton(2131493295, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Trackers.instance().trackEvent("/rating-prompt", "Rating - Prompt", "RatingPrompt", "DontAsk");
        if (AppRater.this != null)
        {
          AppRater.this.putBoolean("dontshowagain", true);
          AppRater.this.commit();
        }
      }
    }).setNeutralButton(2131493296, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        Trackers.instance().trackEvent("/rating-prompt", "Rating - Prompt", "RatingPrompt", "Remind");
        if (AppRater.this != null)
        {
          AppRater.this.putLong("launch_count", 0L);
          AppRater.this.commit();
        }
      }
    }).create().show();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.AppRater
 * JD-Core Version:    0.6.2
 */