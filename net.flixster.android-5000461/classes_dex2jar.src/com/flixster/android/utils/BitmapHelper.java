package com.flixster.android.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Shader.TileMode;

public class BitmapHelper
{
  public static Bitmap createReflection(Bitmap paramBitmap)
  {
    try
    {
      int i = (int)(paramBitmap.getWidth() / 4.5D);
      Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), i, Bitmap.Config.ARGB_8888);
      BitmapShader localBitmapShader = new BitmapShader(Bitmap.createBitmap(paramBitmap, 0, paramBitmap.getHeight() - i, paramBitmap.getWidth(), i), Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
      Matrix localMatrix = new Matrix();
      localMatrix.setScale(1.0F, -1.0F);
      localMatrix.preTranslate(0.0F, -i);
      localBitmapShader.setLocalMatrix(localMatrix);
      ComposeShader localComposeShader = new ComposeShader(localBitmapShader, new LinearGradient(0.0F, 0.0F, 0.0F, i, -2130706433, 0, Shader.TileMode.CLAMP), PorterDuff.Mode.DST_IN);
      Paint localPaint = new Paint();
      localPaint.setShader(localComposeShader);
      new Canvas(localBitmap).drawRect(0.0F, 0.0F, localBitmap.getWidth(), localBitmap.getHeight(), localPaint);
      return localBitmap;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      Logger.e("FlxMain", "BitmapHelper.createReflection", localOutOfMemoryError);
    }
    return null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.BitmapHelper
 * JD-Core Version:    0.6.2
 */