package com.flixster.android.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class CryptHelper
{
  private static final String SHARED_KEY = "799741c015681d32";

  public static String decryptAES(byte[] paramArrayOfByte, String paramString)
  {
    if (paramArrayOfByte == null);
    try
    {
      KeyGenerator localKeyGenerator = KeyGenerator.getInstance("AES");
      localKeyGenerator.init(128);
      paramArrayOfByte = localKeyGenerator.generateKey().getEncoded();
      SecretKeySpec localSecretKeySpec = new SecretKeySpec(paramArrayOfByte, "AES");
      Cipher localCipher = Cipher.getInstance("AES");
      localCipher.init(2, localSecretKeySpec);
      String str = new String(localCipher.doFinal(Hex.decodeHex(new String(paramString))));
      return str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      Logger.e("FlxMain", "AES algorithm not available", localNoSuchAlgorithmException);
      return null;
    }
    catch (NoSuchPaddingException localNoSuchPaddingException)
    {
      while (true)
        Logger.e("FlxMain", "AES padding not available", localNoSuchPaddingException);
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      while (true)
        Logger.e("FlxMain", "Invalid key '" + paramArrayOfByte + "'", localInvalidKeyException);
    }
    catch (IllegalBlockSizeException localIllegalBlockSizeException)
    {
      while (true)
        Logger.e("FlxMain", "Illegal block size", localIllegalBlockSizeException);
    }
    catch (BadPaddingException localBadPaddingException)
    {
      while (true)
        Logger.e("FlxMain", "Bad padding", localBadPaddingException);
    }
  }

  public static String encryptAES(byte[] paramArrayOfByte, String paramString)
  {
    if (paramArrayOfByte == null);
    try
    {
      KeyGenerator localKeyGenerator = KeyGenerator.getInstance("AES");
      localKeyGenerator.init(128);
      paramArrayOfByte = localKeyGenerator.generateKey().getEncoded();
      SecretKeySpec localSecretKeySpec = new SecretKeySpec(paramArrayOfByte, "AES");
      Cipher localCipher = Cipher.getInstance("AES");
      localCipher.init(1, localSecretKeySpec);
      String str = Hex.encodeHex(localCipher.doFinal(paramString.getBytes()), false);
      return str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      Logger.e("FlxMain", "AES algorithm not available", localNoSuchAlgorithmException);
      return null;
    }
    catch (NoSuchPaddingException localNoSuchPaddingException)
    {
      while (true)
        Logger.e("FlxMain", "AES padding not available", localNoSuchPaddingException);
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      while (true)
        Logger.e("FlxMain", "Invalid key '" + paramArrayOfByte + "'", localInvalidKeyException);
    }
    catch (IllegalBlockSizeException localIllegalBlockSizeException)
    {
      while (true)
        Logger.e("FlxMain", "Illegal block size", localIllegalBlockSizeException);
    }
    catch (BadPaddingException localBadPaddingException)
    {
      while (true)
        Logger.e("FlxMain", "Bad padding", localBadPaddingException);
    }
  }

  public static String encryptEsig(String paramString)
  {
    return encryptAES("799741c015681d32".getBytes(), paramString);
  }

  public static void test()
    throws Exception
  {
    String str1 = encryptAES("799741c015681d32".getBytes(), "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(),./;'[]");
    String str2 = decryptAES("799741c015681d32".getBytes(), str1);
    Logger.i("FlxMain", "Clear text = " + "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(),./;'[]");
    Logger.i("FlxMain", "Encrypted text (Hex encoded) = " + str1);
    Logger.i("FlxMain", "Decrypted text (Hex decoded) = " + str2);
    if ("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(),./;'[]".equals(str2));
    for (String str3 = "success!"; ; str3 = "failure!")
    {
      Logger.i("FlxMain", str3);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.CryptHelper
 * JD-Core Version:    0.6.2
 */