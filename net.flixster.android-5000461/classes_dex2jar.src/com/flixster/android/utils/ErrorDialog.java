package com.flixster.android.utils;

import android.app.Activity;
import android.app.Dialog;
import com.flixster.android.activity.DialogActivity;
import com.flixster.android.activity.common.DecoratedActivity;
import com.flixster.android.activity.common.DecoratedSherlockActivity;
import com.flixster.android.activity.decorator.DialogDecorator;
import com.flixster.android.view.DialogBuilder;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;

public class ErrorDialog
{
  public static void handleException(DaoException paramDaoException, final Activity paramActivity, int paramInt)
  {
    if ((paramActivity != null) && (!paramActivity.isFinishing()))
    {
      if ((paramActivity instanceof DialogActivity))
        handleException(paramDaoException, (DialogActivity)paramActivity);
    }
    else
      return;
    paramActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (DaoException.Type.NOT_LICENSED == ErrorDialog.this.getType())
          ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000906), paramActivity);
        do
        {
          return;
          if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == ErrorDialog.this.getType()))
          {
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000900), paramActivity);
            return;
          }
          if (DaoException.Type.NETWORK_UNSTABLE == ErrorDialog.this.getType())
          {
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000908), paramActivity);
            return;
          }
          if (DaoException.Type.SERVER_DATA == ErrorDialog.this.getType())
          {
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000901), paramActivity);
            return;
          }
          if (DaoException.Type.SERVER_API == ErrorDialog.this.getType())
          {
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000902), paramActivity);
            return;
          }
          if (DaoException.Type.SERVER_ERROR_MSG == ErrorDialog.this.getType())
          {
            ObjectHolder.instance().put(String.valueOf(1000000903), ErrorDialog.this.getMessage());
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000903), paramActivity);
            return;
          }
          if (DaoException.Type.USER_ACCT == ErrorDialog.this.getType())
          {
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000904), paramActivity);
            return;
          }
          if (DaoException.Type.STREAM_CREATE == ErrorDialog.this.getType())
          {
            ObjectHolder.instance().put(String.valueOf(1000000905), ErrorDialog.this.getMessage());
            ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000905), paramActivity);
            return;
          }
        }
        while (DaoException.Type.UNKNOWN != ErrorDialog.this.getType());
        ErrorDialog.showDialog(DialogBuilder.createDialog(paramActivity, 1000000907), paramActivity);
      }
    });
  }

  public static void handleException(DaoException paramDaoException, Activity paramActivity, final DialogDecorator paramDialogDecorator)
  {
    if ((paramActivity != null) && (!paramActivity.isFinishing()))
      paramActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (DaoException.Type.NOT_LICENSED == ErrorDialog.this.getType())
            paramDialogDecorator.showDialog(1000000906, null);
          do
          {
            return;
            if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == ErrorDialog.this.getType()))
            {
              paramDialogDecorator.showDialog(1000000900, null);
              return;
            }
            if (DaoException.Type.NETWORK_UNSTABLE == ErrorDialog.this.getType())
            {
              paramDialogDecorator.showDialog(1000000908, null);
              return;
            }
            if (DaoException.Type.SERVER_DATA == ErrorDialog.this.getType())
            {
              paramDialogDecorator.showDialog(1000000901, null);
              return;
            }
            if (DaoException.Type.SERVER_API == ErrorDialog.this.getType())
            {
              paramDialogDecorator.showDialog(1000000902, null);
              return;
            }
            if (DaoException.Type.SERVER_ERROR_MSG == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000903), ErrorDialog.this.getMessage());
              paramDialogDecorator.showDialog(1000000903, null);
              return;
            }
            if (DaoException.Type.USER_ACCT == ErrorDialog.this.getType())
            {
              paramDialogDecorator.showDialog(1000000904, null);
              return;
            }
            if (DaoException.Type.STREAM_CREATE == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000905), ErrorDialog.this.getMessage());
              paramDialogDecorator.showDialog(1000000905, null);
              return;
            }
          }
          while (DaoException.Type.UNKNOWN != ErrorDialog.this.getType());
          paramDialogDecorator.showDialog(1000000907, null);
        }
      });
  }

  public static void handleException(DaoException paramDaoException, final DialogActivity paramDialogActivity)
  {
    if ((paramDialogActivity != null) && (!paramDialogActivity.isFinishing()))
      paramDialogActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (DaoException.Type.NOT_LICENSED == ErrorDialog.this.getType())
            paramDialogActivity.showDialog(1000000906, null);
          do
          {
            return;
            if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == ErrorDialog.this.getType()))
            {
              paramDialogActivity.showDialog(1000000900, null);
              return;
            }
            if (DaoException.Type.NETWORK_UNSTABLE == ErrorDialog.this.getType())
            {
              paramDialogActivity.showDialog(1000000908, null);
              return;
            }
            if (DaoException.Type.SERVER_DATA == ErrorDialog.this.getType())
            {
              paramDialogActivity.showDialog(1000000901, null);
              return;
            }
            if (DaoException.Type.SERVER_API == ErrorDialog.this.getType())
            {
              paramDialogActivity.showDialog(1000000902, null);
              return;
            }
            if (DaoException.Type.SERVER_ERROR_MSG == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000903), ErrorDialog.this.getMessage());
              paramDialogActivity.showDialog(1000000903, null);
              return;
            }
            if (DaoException.Type.USER_ACCT == ErrorDialog.this.getType())
            {
              paramDialogActivity.showDialog(1000000904, null);
              return;
            }
            if (DaoException.Type.STREAM_CREATE == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000905), ErrorDialog.this.getMessage());
              paramDialogActivity.showDialog(1000000905, null);
              return;
            }
          }
          while (DaoException.Type.UNKNOWN != ErrorDialog.this.getType());
          paramDialogActivity.showDialog(1000000907, null);
        }
      });
  }

  public static void handleException(DaoException paramDaoException, final DecoratedActivity paramDecoratedActivity)
  {
    if ((paramDecoratedActivity != null) && (!paramDecoratedActivity.isFinishing()))
      paramDecoratedActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (DaoException.Type.NOT_LICENSED == ErrorDialog.this.getType())
            paramDecoratedActivity.showDialog(1000000906, null);
          do
          {
            return;
            if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == ErrorDialog.this.getType()))
            {
              paramDecoratedActivity.showDialog(1000000900, null);
              return;
            }
            if (DaoException.Type.NETWORK_UNSTABLE == ErrorDialog.this.getType())
            {
              paramDecoratedActivity.showDialog(1000000908, null);
              return;
            }
            if (DaoException.Type.SERVER_DATA == ErrorDialog.this.getType())
            {
              paramDecoratedActivity.showDialog(1000000901, null);
              return;
            }
            if (DaoException.Type.SERVER_API == ErrorDialog.this.getType())
            {
              paramDecoratedActivity.showDialog(1000000902, null);
              return;
            }
            if (DaoException.Type.SERVER_ERROR_MSG == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000903), ErrorDialog.this.getMessage());
              paramDecoratedActivity.showDialog(1000000903, null);
              return;
            }
            if (DaoException.Type.USER_ACCT == ErrorDialog.this.getType())
            {
              paramDecoratedActivity.showDialog(1000000904, null);
              return;
            }
            if (DaoException.Type.STREAM_CREATE == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000905), ErrorDialog.this.getMessage());
              paramDecoratedActivity.showDialog(1000000905, null);
              return;
            }
          }
          while (DaoException.Type.UNKNOWN != ErrorDialog.this.getType());
          paramDecoratedActivity.showDialog(1000000907, null);
        }
      });
  }

  public static void handleException(DaoException paramDaoException, final DecoratedSherlockActivity paramDecoratedSherlockActivity)
  {
    if ((paramDecoratedSherlockActivity != null) && (!paramDecoratedSherlockActivity.isFinishing()))
      paramDecoratedSherlockActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (DaoException.Type.NOT_LICENSED == ErrorDialog.this.getType())
            paramDecoratedSherlockActivity.showDialog(1000000906, null);
          do
          {
            return;
            if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == ErrorDialog.this.getType()))
            {
              paramDecoratedSherlockActivity.showDialog(1000000900, null);
              return;
            }
            if (DaoException.Type.NETWORK_UNSTABLE == ErrorDialog.this.getType())
            {
              paramDecoratedSherlockActivity.showDialog(1000000908, null);
              return;
            }
            if (DaoException.Type.SERVER_DATA == ErrorDialog.this.getType())
            {
              paramDecoratedSherlockActivity.showDialog(1000000901, null);
              return;
            }
            if (DaoException.Type.SERVER_API == ErrorDialog.this.getType())
            {
              paramDecoratedSherlockActivity.showDialog(1000000902, null);
              return;
            }
            if (DaoException.Type.SERVER_ERROR_MSG == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000903), ErrorDialog.this.getMessage());
              paramDecoratedSherlockActivity.showDialog(1000000903, null);
              return;
            }
            if (DaoException.Type.USER_ACCT == ErrorDialog.this.getType())
            {
              paramDecoratedSherlockActivity.showDialog(1000000904, null);
              return;
            }
            if (DaoException.Type.STREAM_CREATE == ErrorDialog.this.getType())
            {
              ObjectHolder.instance().put(String.valueOf(1000000905), ErrorDialog.this.getMessage());
              paramDecoratedSherlockActivity.showDialog(1000000905, null);
              return;
            }
          }
          while (DaoException.Type.UNKNOWN != ErrorDialog.this.getType());
          paramDecoratedSherlockActivity.showDialog(1000000907, null);
        }
      });
  }

  private static void showDialog(final Dialog paramDialog, Activity paramActivity)
  {
    if (!paramActivity.isFinishing())
      paramActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (!ErrorDialog.this.isFinishing())
            paramDialog.show();
        }
      });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ErrorDialog
 * JD-Core Version:    0.6.2
 */