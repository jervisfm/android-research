package com.flixster.android.utils;

import android.os.Build;
import android.os.Build.VERSION;

public class F
{
  public static final String ADMIN_UNLOCK_IOS = "UUDDLRLRBA";
  public static final String ADMIN_UNLOCK_LONG = "REINDEER FLOTILLA";
  public static final String ADMIN_UNLOCK_SHORT = "FLX";
  public static final long AD_FETCH_INTERVAL_MS = 1800000L;
  public static final long AD_FREE_PERIOD_MS = 172800000L;
  public static final int API_LEVEL = 0;
  public static final long DAY_MS = 86400000L;
  public static final String DIAGNOSTIC_UNLOCK = "DGNS";
  public static final String EXTRA_ID = "net.flixster.android.EXTRA_ID";
  public static final String EXTRA_MOVIE_ID = "net.flixster.android.EXTRA_MOVIE_ID";
  public static final String EXTRA_QUERY = "net.flixster.android.EXTRA_QUERY";
  public static final String EXTRA_RIGHT_ID = "net.flixster.android.EXTRA_RIGHT_ID";
  public static final String EXTRA_THEATER_ID = "net.flixster.android.EXTRA_THEATER_ID";
  public static final String EXTRA_TITLE = "net.flixster.android.EXTRA_TITLE";
  public static final boolean IS_ADS_ENABLED = true;
  public static final boolean IS_AMAZON_BUILD = false;
  public static final boolean IS_API_LEVEL_HONEYCOMB = false;
  public static final boolean IS_DEV_BUILD = false;
  public static final boolean IS_NATIVE_HC_DRM_ENABLED = false;
  public static final boolean IS_NOOK_BUILD = false;
  public static final boolean IS_THIRD_PARTY_BUILD = false;
  public static final boolean IS_VODAFONE_BUILD = false;
  public static final String MANUFACTURER;
  public static final long MINUTE_MS = 60000L;
  public static final String MODEL;
  public static final long MSK_PROMPT_DURATION = 6800L;
  public static final String PACKAGE = "net.flixster.android";
  public static final long SPLASH_DURATION = 1800L;
  public static final String TAG = "FlxMain";
  public static final String TAG_AD = "FlxAd";
  public static final String TAG_API = "FlxApi";
  public static final String TAG_DRM = "FlxDrm";
  public static final String TAG_GA = "FlxGa";
  public static final String TAG_NETFLIX = "FlxNf";
  public static final String TAG_WIDGET = "FlxWdgt";

  static
  {
    boolean bool1 = true;
    API_LEVEL = Build.VERSION.SDK_INT;
    boolean bool2;
    if (API_LEVEL >= 11)
    {
      bool2 = bool1;
      IS_API_LEVEL_HONEYCOMB = bool2;
      MANUFACTURER = Build.MANUFACTURER.toLowerCase();
      MODEL = Build.MODEL;
      if ((API_LEVEL < 13) || ((!MANUFACTURER.equals("acer")) && (!MANUFACTURER.equals("htc")) && (!MODEL.equals("LG-MS770")) && (!MODEL.equals("LG-P930")) && (!MODEL.equals("XT907")) && (!MODEL.contains("RAZR HD")) && (!MODEL.contains("RAZR MAXX")) && (!MODEL.equals("N861")) && (!MODEL.equals("LG-E970")) && (!MODEL.equals("LG-LS970"))))
        break label165;
    }
    while (true)
    {
      IS_NATIVE_HC_DRM_ENABLED = bool1;
      return;
      bool2 = false;
      break;
      label165: bool1 = false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.F
 * JD-Core Version:    0.6.2
 */