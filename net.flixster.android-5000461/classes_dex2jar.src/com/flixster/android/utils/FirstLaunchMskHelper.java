package com.flixster.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.flixster.android.data.AccountFacade;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.User;

public class FirstLaunchMskHelper
{
  private static final int LAUNCHES_UNTIL_SHOWN = 10;
  private static final int MAX_SHOWN_COUNT = 3;

  public static void appLaunched()
  {
    SharedPreferences localSharedPreferences = FlixsterApplication.getContext().getSharedPreferences("firstLaunchMsk", 0);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    if (localSharedPreferences.getInt("shown_count", 0) >= 3)
      return;
    int i = 1 + localSharedPreferences.getInt("launch_count", 0);
    Logger.d("FlxMain", "FirstLaunchMskHelper.appLaunched count: " + i);
    localEditor.putInt("launch_count", i);
    localEditor.commit();
  }

  public static boolean shouldShow()
  {
    SharedPreferences localSharedPreferences = FlixsterApplication.getContext().getSharedPreferences("firstLaunchMsk", 0);
    String str = Properties.instance().getVisitType();
    User localUser = AccountFacade.getSocialUser();
    boolean bool;
    if ((str.equalsIgnoreCase(Properties.VisitType.INSTALL.name())) || ((str.equalsIgnoreCase(Properties.VisitType.UPGRADE.name())) && ((localUser == null) || (localUser.isMskEligible))))
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putInt("launch_count", 0);
      localEditor.putInt("shown_count", 0);
      localEditor.commit();
      bool = true;
    }
    int j;
    do
    {
      int i;
      do
      {
        return bool;
        i = localSharedPreferences.getInt("shown_count", 0);
        bool = false;
      }
      while (i >= 3);
      j = localSharedPreferences.getInt("launch_count", 0);
      bool = false;
    }
    while (j < 10);
    return true;
  }

  public static void shown()
  {
    SharedPreferences localSharedPreferences = FlixsterApplication.getContext().getSharedPreferences("firstLaunchMsk", 0);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    int i = 1 + localSharedPreferences.getInt("shown_count", 0);
    Logger.d("FlxMain", "FirstLaunchMskHelper.shown count: " + i);
    localEditor.putInt("launch_count", 0);
    localEditor.putInt("shown_count", i);
    localEditor.commit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.FirstLaunchMskHelper
 * JD-Core Version:    0.6.2
 */