package com.flixster.android.utils;

public class GoogleApiDetector
{
  private static GoogleApiDetector INSTANCE = new GoogleApiDetector();
  private Boolean isVanillaAndroid;

  public static GoogleApiDetector instance()
  {
    return INSTANCE;
  }

  public boolean isVanillaAndroid()
  {
    if (this.isVanillaAndroid == null);
    try
    {
      Class.forName("com.google.android.maps.MapActivity");
      this.isVanillaAndroid = Boolean.valueOf(false);
      return this.isVanillaAndroid.booleanValue();
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      while (true)
      {
        Logger.w("FlxMain", "GoogleApiDetector.isVanillaAndroid true", localClassNotFoundException);
        this.isVanillaAndroid = Boolean.valueOf(true);
      }
    }
    catch (Error localError)
    {
      while (true)
      {
        Logger.e("FlxMain", "GoogleApiDetector.isVanillaAndroid true", localError);
        this.isVanillaAndroid = Boolean.valueOf(true);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.GoogleApiDetector
 * JD-Core Version:    0.6.2
 */