package com.flixster.android.utils;

public class Hex
{
  private static final byte[] DIGITS;
  private static final char[] FIRST_CHAR;
  private static final char[] HEX_DIGITS = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70 };
  private static final char[] SECOND_CHAR;

  static
  {
    FIRST_CHAR = new char[256];
    SECOND_CHAR = new char[256];
    int i = 0;
    int j;
    label131: int k;
    if (i >= 256)
    {
      DIGITS = new byte[103];
      j = 0;
      if (j <= 70)
        break label188;
      k = 0;
      label139: if (k < 10)
        break label200;
    }
    for (int m = 0; ; m = (byte)(m + 1))
    {
      if (m >= 6)
      {
        return;
        FIRST_CHAR[i] = HEX_DIGITS[(0xF & i >> 4)];
        SECOND_CHAR[i] = HEX_DIGITS[(i & 0xF)];
        i++;
        break;
        label188: DIGITS[j] = -1;
        j++;
        break label131;
        label200: DIGITS[(k + 48)] = k;
        k = (byte)(k + 1);
        break label139;
      }
      DIGITS[(m + 65)] = ((byte)(m + 10));
      DIGITS[(m + 97)] = ((byte)(m + 10));
    }
  }

  public static byte[] decodeHex(String paramString)
  {
    int i = paramString.length();
    if ((i & 0x1) != 0)
      throw new IllegalArgumentException("Odd number of characters.");
    int j = 0;
    byte[] arrayOfByte = new byte[i >> 1];
    int k = 0;
    int m = 0;
    while (true)
    {
      if (m >= i);
      int i2;
      int i4;
      while (true)
      {
        if (j == 0)
          break label186;
        throw new IllegalArgumentException("Invalid hexadecimal digit: " + paramString);
        int n = m + 1;
        int i1 = paramString.charAt(m);
        if (i1 > 102)
        {
          j = 1;
        }
        else
        {
          i2 = DIGITS[i1];
          if (i2 == -1)
          {
            j = 1;
          }
          else
          {
            m = n + 1;
            int i3 = paramString.charAt(n);
            if (i3 > 102)
            {
              j = 1;
            }
            else
            {
              i4 = DIGITS[i3];
              if (i4 != -1)
                break;
              j = 1;
            }
          }
        }
      }
      arrayOfByte[k] = ((byte)(i4 | i2 << 4));
      k++;
    }
    label186: return arrayOfByte;
  }

  public static String encodeHex(byte[] paramArrayOfByte, boolean paramBoolean)
  {
    char[] arrayOfChar = new char[2 * paramArrayOfByte.length];
    int i = 0;
    for (int j = 0; ; j++)
    {
      if (j >= paramArrayOfByte.length);
      int k;
      do
      {
        return new String(arrayOfChar, 0, i);
        k = 0xFF & paramArrayOfByte[j];
      }
      while ((k == 0) && (paramBoolean));
      int m = i + 1;
      arrayOfChar[i] = FIRST_CHAR[k];
      i = m + 1;
      arrayOfChar[m] = SECOND_CHAR[k];
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.Hex
 * JD-Core Version:    0.6.2
 */