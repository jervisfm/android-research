package com.flixster.android.utils;

import android.os.Handler;
import java.util.LinkedList;
import java.util.Queue;

public class ImageGetter
{
  private static final ImageGetter INSTANCE = new ImageGetter();
  private final Queue<ImageRequest> imageQueue = new LinkedList();

  private ImageGetter()
  {
    new Thread(new ImageRequestConsumer(this.imageQueue)).start();
  }

  public static ImageGetter instance()
  {
    return INSTANCE;
  }

  public void get(String paramString, Handler paramHandler)
  {
    Logger.v("FlxMain", "ImageGetter requesting: " + paramString);
    synchronized (this.imageQueue)
    {
      this.imageQueue.add(new ImageRequest(paramString, paramHandler, null));
      if (this.imageQueue.size() == 1)
        this.imageQueue.notify();
      return;
    }
  }

  private static class ImageRequest
  {
    public final Handler handler;
    public final String url;

    private ImageRequest(String paramString, Handler paramHandler)
    {
      this.url = paramString;
      this.handler = paramHandler;
    }
  }

  private static class ImageRequestConsumer extends ImageGetter.RequestConsumer<ImageGetter.ImageRequest>
  {
    protected ImageRequestConsumer(Queue<ImageGetter.ImageRequest> paramQueue)
    {
      super();
    }

    // ERROR //
    protected void consume(ImageGetter.ImageRequest paramImageRequest)
    {
      // Byte code:
      //   0: new 17	java/net/URL
      //   3: dup
      //   4: aload_1
      //   5: getfield 23	com/flixster/android/utils/ImageGetter$ImageRequest:url	Ljava/lang/String;
      //   8: invokespecial 26	java/net/URL:<init>	(Ljava/lang/String;)V
      //   11: astore_2
      //   12: aload_1
      //   13: getfield 30	com/flixster/android/utils/ImageGetter$ImageRequest:handler	Landroid/os/Handler;
      //   16: ifnonnull +20 -> 36
      //   19: aload_2
      //   20: invokestatic 36	net/flixster/android/util/HttpImageHelper:fetchImageDoNotCache	(Ljava/net/URL;)V
      //   23: return
      //   24: astore 6
      //   26: ldc 38
      //   28: ldc 40
      //   30: aload 6
      //   32: invokestatic 46	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
      //   35: return
      //   36: aload_2
      //   37: invokestatic 50	net/flixster/android/util/HttpImageHelper:fetchImage	(Ljava/net/URL;)Landroid/graphics/Bitmap;
      //   40: astore 4
      //   42: aload_1
      //   43: getfield 30	com/flixster/android/utils/ImageGetter$ImageRequest:handler	Landroid/os/Handler;
      //   46: aconst_null
      //   47: iconst_0
      //   48: aload 4
      //   50: invokestatic 56	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
      //   53: invokevirtual 62	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
      //   56: pop
      //   57: return
      //   58: astore_3
      //   59: ldc 38
      //   61: ldc 40
      //   63: aload_3
      //   64: invokestatic 46	com/flixster/android/utils/Logger:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
      //   67: return
      //
      // Exception table:
      //   from	to	target	type
      //   0	12	24	java/net/MalformedURLException
      //   12	23	58	java/io/IOException
      //   36	57	58	java/io/IOException
    }
  }

  private static abstract class RequestConsumer<T>
    implements Runnable
  {
    private final Queue<T> queue;

    protected RequestConsumer(Queue<T> paramQueue)
    {
      this.queue = paramQueue;
    }

    protected abstract void consume(T paramT);

    // ERROR //
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 17	com/flixster/android/utils/ImageGetter$RequestConsumer:queue	Ljava/util/Queue;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: aload_0
      //   8: getfield 17	com/flixster/android/utils/ImageGetter$RequestConsumer:queue	Ljava/util/Queue;
      //   11: invokeinterface 28 1 0
      //   16: ifne +17 -> 33
      //   19: ldc 30
      //   21: ldc 32
      //   23: invokestatic 38	com/flixster/android/utils/Logger:v	(Ljava/lang/String;Ljava/lang/String;)V
      //   26: aload_0
      //   27: getfield 17	com/flixster/android/utils/ImageGetter$RequestConsumer:queue	Ljava/util/Queue;
      //   30: invokevirtual 41	java/lang/Object:wait	()V
      //   33: aload_0
      //   34: getfield 17	com/flixster/android/utils/ImageGetter$RequestConsumer:queue	Ljava/util/Queue;
      //   37: invokeinterface 45 1 0
      //   42: astore 4
      //   44: aload_2
      //   45: monitorexit
      //   46: ldc 30
      //   48: ldc 47
      //   50: invokestatic 38	com/flixster/android/utils/Logger:v	(Ljava/lang/String;Ljava/lang/String;)V
      //   53: aload_0
      //   54: aload 4
      //   56: invokevirtual 49	com/flixster/android/utils/ImageGetter$RequestConsumer:consume	(Ljava/lang/Object;)V
      //   59: goto -59 -> 0
      //   62: astore_1
      //   63: ldc 30
      //   65: ldc 51
      //   67: aload_1
      //   68: invokestatic 55	com/flixster/android/utils/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
      //   71: goto -71 -> 0
      //   74: astore_3
      //   75: aload_2
      //   76: monitorexit
      //   77: aload_3
      //   78: athrow
      //
      // Exception table:
      //   from	to	target	type
      //   0	7	62	java/lang/InterruptedException
      //   46	59	62	java/lang/InterruptedException
      //   77	79	62	java/lang/InterruptedException
      //   7	33	74	finally
      //   33	46	74	finally
      //   75	77	74	finally
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ImageGetter
 * JD-Core Version:    0.6.2
 */