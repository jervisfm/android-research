package com.flixster.android.utils;

import net.flixster.android.model.ImageOrder;

public abstract interface ImageTask
{
  public abstract void orderImage(ImageOrder paramImageOrder);

  public abstract void orderImageFifo(ImageOrder paramImageOrder);

  public abstract void orderImageLifo(ImageOrder paramImageOrder);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ImageTask
 * JD-Core Version:    0.6.2
 */