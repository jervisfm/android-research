package com.flixster.android.utils;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import net.flixster.android.model.Actor;
import net.flixster.android.model.ImageOrder;
import net.flixster.android.model.Movie;
import net.flixster.android.model.NetflixQueueItem;
import net.flixster.android.model.NewsStory;
import net.flixster.android.model.Photo;
import net.flixster.android.util.HttpImageHelper;

public class ImageTaskImpl
  implements ImageTask
{
  private static ImageTaskImpl instance;
  private final ArrayList<ImageOrder> mImageOrderQueue = new ArrayList();
  private Timer mImageTimer;
  private volatile boolean mRunImageTask;

  public static ImageTaskImpl instance()
  {
    if (instance == null)
      instance = new ImageTaskImpl();
    return instance;
  }

  public boolean isRunning()
  {
    return this.mRunImageTask;
  }

  public void orderImage(ImageOrder paramImageOrder)
  {
    orderImageLifo(paramImageOrder);
  }

  public void orderImageFifo(ImageOrder paramImageOrder)
  {
    if (!this.mRunImageTask)
    {
      Logger.w("FlxMain", "Illegal state: ImageTask has not started");
      return;
    }
    this.mImageOrderQueue.add(paramImageOrder);
  }

  public void orderImageLifo(ImageOrder paramImageOrder)
  {
    if (!this.mRunImageTask)
    {
      Logger.w("FlxMain", "Illegal state: ImageTask has not started");
      return;
    }
    this.mImageOrderQueue.add(0, paramImageOrder);
  }

  public void startTask()
  {
    try
    {
      boolean bool = this.mRunImageTask;
      if (bool);
      while (true)
      {
        return;
        this.mRunImageTask = true;
        if (this.mImageTimer == null)
          this.mImageTimer = new Timer();
        TimerTask local1 = new TimerTask()
        {
          public void run()
          {
            if (!ImageTaskImpl.this.mRunImageTask)
              return;
            while (true)
            {
              ImageOrder localImageOrder;
              Bitmap localBitmap;
              try
              {
                localImageOrder = (ImageOrder)ImageTaskImpl.this.mImageOrderQueue.remove(0);
                if (localImageOrder == null)
                  Thread.sleep(1000L);
              }
              catch (InterruptedException localInterruptedException)
              {
                Logger.e("FlxMain", "sleep exception");
              }
              catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
              {
                continue;
                localBitmap = HttpImageHelper.fetchImage(new URL(localImageOrder.urlString));
                if (localImageOrder.tag != null);
                switch (localImageOrder.type)
                {
                case 1:
                case 2:
                case 5:
                default:
                  if ((localImageOrder.refreshHandler != null) && (localImageOrder.movieView != null))
                    localImageOrder.refreshHandler.sendMessage(Message.obtain(localImageOrder.refreshHandler, 0, localImageOrder.movieView));
                  if (!ImageTaskImpl.this.mImageOrderQueue.isEmpty())
                    continue;
                  break;
                case 0:
                  ((Movie)localImageOrder.tag).thumbnailSoftBitmap = new SoftReference(localBitmap);
                  continue;
                case 3:
                case 8:
                case 6:
                case 9:
                case 4:
                case 7:
                }
              }
              catch (IOException localIOException)
              {
                Logger.e("FlxMain", "Failed to download a movie poster", localIOException);
              }
              break;
              ((Photo)localImageOrder.tag).bitmap = localBitmap;
              continue;
              ((Photo)localImageOrder.tag).galleryBitmap = new SoftReference(localBitmap);
              continue;
              ((NewsStory)localImageOrder.tag).mSoftPhoto = new SoftReference(localBitmap);
              continue;
              Logger.d("FlxMain", "FlixsterActivity mImageOrderQueue url:" + localImageOrder.urlString);
              ((Actor)localImageOrder.tag).bitmap = localBitmap;
              continue;
              ((NetflixQueueItem)localImageOrder.tag).thumbnailBitmap = localBitmap;
            }
          }
        };
        if (this.mImageTimer != null)
          this.mImageTimer.schedule(local1, 300L);
      }
    }
    finally
    {
    }
  }

  public void stopTask()
  {
    try
    {
      this.mRunImageTask = false;
      this.mImageOrderQueue.clear();
      if (this.mImageTimer != null)
      {
        this.mImageTimer.cancel();
        this.mImageTimer.purge();
      }
      this.mImageTimer = null;
      return;
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ImageTaskImpl
 * JD-Core Version:    0.6.2
 */