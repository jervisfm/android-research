package com.flixster.android.utils;

import android.view.View;
import android.view.ViewGroup;

public class L
{
  public static void view(View paramView)
  {
    Logger.i("FlxMain", paramView + " isClickable: " + paramView.isClickable());
    Logger.i("FlxMain", paramView + " isFocusable: " + paramView.isFocusable());
    Logger.i("FlxMain", paramView + " isFocused: " + paramView.isFocused());
  }

  public static void viewGroup(ViewGroup paramViewGroup)
  {
    view(paramViewGroup);
    Logger.i("FlxMain", paramViewGroup + " hasFocusable: " + paramViewGroup.hasFocusable());
    Logger.i("FlxMain", paramViewGroup + " hasFocus: " + paramViewGroup.hasFocus());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.L
 * JD-Core Version:    0.6.2
 */