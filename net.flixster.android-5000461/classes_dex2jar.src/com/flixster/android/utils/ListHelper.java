package com.flixster.android.utils;

import java.util.ArrayList;
import java.util.List;

public class ListHelper
{
  public static <T> ArrayList<T> clone(ArrayList<T> paramArrayList)
  {
    return (ArrayList)paramArrayList.clone();
  }

  public static <T> List<T> copy(List<T> paramList)
  {
    ArrayList localArrayList = new ArrayList(paramList.size());
    localArrayList.addAll(paramList);
    return localArrayList;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ListHelper
 * JD-Core Version:    0.6.2
 */