package com.flixster.android.utils;

import net.flixster.android.FlixsterApplication;

public class LocationFacade
{
  public static double getAvailableLatitude()
  {
    double d = FlixsterApplication.getCurrentLatitude();
    if (d == 0.0D)
      d = FlixsterApplication.getUserLatitude();
    return d;
  }

  public static double getAvailableLongitude()
  {
    double d = FlixsterApplication.getCurrentLongitude();
    if (d == 0.0D)
      d = FlixsterApplication.getUserLongitude();
    return d;
  }

  public static String getAvailableZip()
  {
    String str = FlixsterApplication.getCurrentZip();
    if (str == null)
      str = FlixsterApplication.getUserZip();
    if ("".equals(str))
      str = null;
    return str;
  }

  public static boolean isAustraliaLocale()
  {
    return "au".equals(FlixsterApplication.getCountryCode());
  }

  public static boolean isCanadaLocale()
  {
    return "ca".equals(FlixsterApplication.getCountryCode());
  }

  public static boolean isFrenchLocale()
  {
    return "fr".equals(FlixsterApplication.getCountryCode());
  }

  public static boolean isUkLocale()
  {
    return "uk".equals(FlixsterApplication.getCountryCode());
  }

  public static boolean isUsLocale()
  {
    return "us".equals(FlixsterApplication.getCountryCode());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.LocationFacade
 * JD-Core Version:    0.6.2
 */