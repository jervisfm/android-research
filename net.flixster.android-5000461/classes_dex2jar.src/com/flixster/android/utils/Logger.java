package com.flixster.android.utils;

import android.util.Log;
import net.flixster.android.FlixsterApplication;

public class Logger
{
  public static void d(String paramString1, String paramString2)
  {
    if (isDiagnosticMode())
      Log.d(paramString1, paramString2);
  }

  public static void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if (isDiagnosticMode())
      Log.d(paramString1, paramString2, paramThrowable);
  }

  public static void e(String paramString1, String paramString2)
  {
    Log.e(paramString1, paramString2);
  }

  public static void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    Log.e(paramString1, paramString2, paramThrowable);
  }

  public static void fd(String paramString1, String paramString2)
  {
    Log.d(paramString1, paramString2);
  }

  public static void i(String paramString1, String paramString2)
  {
    Log.i(paramString1, paramString2);
  }

  public static void i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    Log.i(paramString1, paramString2, paramThrowable);
  }

  private static boolean isAdminMode()
  {
    return FlixsterApplication.isAdminEnabled();
  }

  private static boolean isDiagnosticMode()
  {
    return FlixsterApplication.isDiagnosticMode();
  }

  public static void sd(String paramString1, String paramString2)
  {
    if ((isDiagnosticMode()) && (isAdminMode()))
      Log.d(paramString1, paramString2);
  }

  public static void si(String paramString1, String paramString2)
  {
    if ((isDiagnosticMode()) && (isAdminMode()))
      Log.i(paramString1, paramString2);
  }

  public static void sv(String paramString1, String paramString2)
  {
    if ((isDiagnosticMode()) && (isAdminMode()))
      Log.v(paramString1, paramString2);
  }

  public static void sw(String paramString1, String paramString2)
  {
    if ((isDiagnosticMode()) && (isAdminMode()))
      Log.w(paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2)
  {
    if (isDiagnosticMode())
      Log.v(paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if (isDiagnosticMode())
      Log.v(paramString1, paramString2, paramThrowable);
  }

  public static void w(String paramString1, String paramString2)
  {
    Log.w(paramString1, paramString2);
  }

  public static void w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    Log.w(paramString1, paramString2, paramThrowable);
  }

  public static void w(String paramString, Throwable paramThrowable)
  {
    Log.w(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.Logger
 * JD-Core Version:    0.6.2
 */