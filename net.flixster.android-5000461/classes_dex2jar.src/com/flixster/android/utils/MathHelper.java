package com.flixster.android.utils;

import java.util.Random;

public class MathHelper
{
  private static Random r = new Random();

  public static long randomId()
  {
    return Math.abs(r.nextLong());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.MathHelper
 * JD-Core Version:    0.6.2
 */