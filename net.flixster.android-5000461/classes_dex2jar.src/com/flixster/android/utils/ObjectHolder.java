package com.flixster.android.utils;

import java.util.HashMap;
import java.util.Map;

public class ObjectHolder
{
  private static final ObjectHolder INSTANCE = new ObjectHolder();
  private final Map<String, Object> map = new HashMap();

  public static ObjectHolder instance()
  {
    return INSTANCE;
  }

  public void clear()
  {
    try
    {
      this.map.clear();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public Object get(String paramString)
  {
    try
    {
      Object localObject2 = this.map.get(paramString);
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      throw localObject1;
    }
  }

  public void put(String paramString, Object paramObject)
  {
    try
    {
      this.map.put(paramString, paramObject);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public Object remove(String paramString)
  {
    try
    {
      Object localObject2 = this.map.remove(paramString);
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      throw localObject1;
    }
  }

  public int size()
  {
    try
    {
      int i = this.map.size();
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ObjectHolder
 * JD-Core Version:    0.6.2
 */