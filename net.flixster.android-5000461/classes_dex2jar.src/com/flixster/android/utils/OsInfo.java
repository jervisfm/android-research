package com.flixster.android.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import net.flixster.android.FlixsterApplication;

public class OsInfo
{
  private static final OsInfo INSTANCE = new OsInfo();
  private static final String KEY_ANDROID_ID = "sai";
  private static final String KEY_DEVICE_ID = "tdi";
  private static final String KEY_SERIAL = "bsn";
  private static final String PREFERENCE_FILE = "osinfo";
  private ConnectivityManager cm;
  private ContentResolver cr;
  private String encryptedAndroidId;
  private String encryptedDeviceId;
  private String encryptedSerialNumber;
  private TelephonyManager tm;

  public static OsInfo instance()
  {
    return INSTANCE;
  }

  private static String readPreference(String paramString)
  {
    return FlixsterApplication.getContext().getSharedPreferences("osinfo", 0).getString(paramString, null);
  }

  private void retrieveAndEncryptIdentifiers()
  {
    String str1 = VersionedUidHelper.getTelephonyDeviceId(this.tm);
    String str2 = VersionedUidHelper.getAndroidId(this.cr);
    String str3 = VersionedUidHelper.getSerialNumber();
    if (str1 != null)
    {
      String str6 = CryptHelper.encryptEsig(str1);
      this.encryptedDeviceId = str6;
      if (str6 != null)
      {
        writePreference("tdi", this.encryptedDeviceId);
        if (str2 == null)
          break label121;
        String str5 = CryptHelper.encryptEsig(str2);
        this.encryptedAndroidId = str5;
        if (str5 == null)
          break label121;
        writePreference("sai", this.encryptedAndroidId);
      }
    }
    while (true)
    {
      if (str3 == null)
        break label131;
      String str4 = CryptHelper.encryptEsig(str3);
      this.encryptedSerialNumber = str4;
      if (str4 == null)
        break label131;
      writePreference("bsn", this.encryptedSerialNumber);
      return;
      writePreference("tdi", "");
      break;
      label121: writePreference("sai", "");
    }
    label131: writePreference("bsn", "");
  }

  private static void writePreference(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = FlixsterApplication.getContext().getSharedPreferences("osinfo", 0).edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }

  public String getAdu()
  {
    String str = getEncryptedTelephonyDeviceId();
    int i;
    if ("".equals(str))
    {
      i = F.API_LEVEL;
      if (i < 8)
        str = "";
    }
    else
    {
      return str;
    }
    if ((11 <= i) && (i < 14))
      return getEncryptedSerialNumber();
    return getEncryptedAndroidId();
  }

  public String getEncryptedAndroidId()
  {
    if (this.encryptedAndroidId == null)
    {
      String str = readPreference("sai");
      this.encryptedAndroidId = str;
      if (str == null)
        retrieveAndEncryptIdentifiers();
    }
    return this.encryptedAndroidId;
  }

  public String getEncryptedSerialNumber()
  {
    if (this.encryptedSerialNumber == null)
    {
      String str = readPreference("bsn");
      this.encryptedSerialNumber = str;
      if (str == null)
        retrieveAndEncryptIdentifiers();
    }
    return this.encryptedSerialNumber;
  }

  public String getEncryptedTelephonyDeviceId()
  {
    if (this.encryptedDeviceId == null)
    {
      String str = readPreference("tdi");
      this.encryptedDeviceId = str;
      if (str == null)
        retrieveAndEncryptIdentifiers();
    }
    return this.encryptedDeviceId;
  }

  public NetworkSpeed getNetworkSpeed()
  {
    NetworkInfo localNetworkInfo = this.cm.getActiveNetworkInfo();
    if (localNetworkInfo == null)
    {
      Logger.w("FlxMain", "OsInfo.getNetworkSpeed unknown");
      return NetworkSpeed.UNKNOWN;
    }
    Logger.d("FlxMain", "OsInfo.getNetworkSpeed " + localNetworkInfo.getTypeName() + " " + localNetworkInfo.getSubtypeName());
    int i = localNetworkInfo.getType();
    int j = localNetworkInfo.getSubtype();
    switch (i)
    {
    default:
      return NetworkSpeed.UNKNOWN;
    case 1:
      return NetworkSpeed.FASTER_LIKE_WIFI;
    case 6:
      return NetworkSpeed.FAST_LIKE_4G;
    case 0:
    }
    switch (j)
    {
    default:
      return NetworkSpeed.UNKNOWN;
    case 11:
      return NetworkSpeed.SLOW_LIKE_EDGE;
    case 7:
      return NetworkSpeed.SLOW_LIKE_EDGE;
    case 4:
      return NetworkSpeed.SLOW_LIKE_EDGE;
    case 2:
      return NetworkSpeed.SLOW_LIKE_EDGE;
    case 1:
      return NetworkSpeed.SLOW_LIKE_EDGE;
    case 5:
      return NetworkSpeed.BASIC_LIKE_3G;
    case 6:
      return NetworkSpeed.BASIC_LIKE_3G;
    case 10:
      return NetworkSpeed.BASIC_LIKE_3G;
    case 3:
      return NetworkSpeed.BASIC_LIKE_3G;
    case 14:
      return NetworkSpeed.BASIC_LIKE_3G;
    case 12:
      return NetworkSpeed.FAST_LIKE_4G;
    case 8:
      return NetworkSpeed.FAST_LIKE_4G;
    case 9:
      return NetworkSpeed.FAST_LIKE_4G;
    case 15:
      return NetworkSpeed.FASTER_LIKE_WIFI;
    case 13:
    }
    return NetworkSpeed.FASTER_LIKE_WIFI;
  }

  public void initialize(ConnectivityManager paramConnectivityManager, TelephonyManager paramTelephonyManager, ContentResolver paramContentResolver)
  {
    this.cm = paramConnectivityManager;
    this.tm = paramTelephonyManager;
    this.cr = paramContentResolver;
  }

  public boolean isConnected()
  {
    NetworkInfo localNetworkInfo = this.cm.getActiveNetworkInfo();
    return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
  }

  public static enum NetworkSpeed
  {
    static
    {
      SLOW_LIKE_EDGE = new NetworkSpeed("SLOW_LIKE_EDGE", 1);
      BASIC_LIKE_3G = new NetworkSpeed("BASIC_LIKE_3G", 2);
      FAST_LIKE_4G = new NetworkSpeed("FAST_LIKE_4G", 3);
      FASTER_LIKE_WIFI = new NetworkSpeed("FASTER_LIKE_WIFI", 4);
      NetworkSpeed[] arrayOfNetworkSpeed = new NetworkSpeed[5];
      arrayOfNetworkSpeed[0] = UNKNOWN;
      arrayOfNetworkSpeed[1] = SLOW_LIKE_EDGE;
      arrayOfNetworkSpeed[2] = BASIC_LIKE_3G;
      arrayOfNetworkSpeed[3] = FAST_LIKE_4G;
      arrayOfNetworkSpeed[4] = FASTER_LIKE_WIFI;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.OsInfo
 * JD-Core Version:    0.6.2
 */