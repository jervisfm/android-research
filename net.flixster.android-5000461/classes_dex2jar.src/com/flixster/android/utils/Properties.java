package com.flixster.android.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.model.Property;

public class Properties
{
  private static final Properties INSTANCE = new Properties();
  private boolean hasUserSessionExpired;
  private Boolean isAtLeastHdpi;
  private boolean isGtv;
  private boolean isGtvIdentified;
  private boolean isHoneycombTablet;
  private boolean isHoneycombTabletIdentified;
  private boolean isMskFinishedDeepLinkInitiated;
  private Boolean isScreenLarge;
  private Boolean isScreenXlarge;
  private Property property;
  private boolean shouldShowSplash;
  private boolean shouldUseLowBitrateForTrailer;
  private VisitType visitType;

  public static Properties instance()
  {
    return INSTANCE;
  }

  private void resetSplash()
  {
    if ((!this.isHoneycombTablet) && (!this.isGtv));
    for (boolean bool = true; ; bool = false)
    {
      this.shouldShowSplash = bool;
      return;
    }
  }

  private void resetVisitType()
  {
    Object localObject = ObjectHolder.instance().remove(VisitType.class.getName());
    if ((localObject != null) && ((localObject instanceof VisitType)))
    {
      this.visitType = ((VisitType)localObject);
      return;
    }
    this.visitType = VisitType.SESSION;
  }

  public Property getProperties()
  {
    return this.property;
  }

  public String getVisitType()
  {
    if (this.visitType == null)
      return "";
    return this.visitType.name().toLowerCase();
  }

  public boolean hasUserSessionExpired()
  {
    boolean bool = this.hasUserSessionExpired;
    this.hasUserSessionExpired = false;
    return bool;
  }

  public void identifyGoogleTv(Context paramContext)
  {
    if (this.isGtvIdentified)
      return;
    this.isGtv = VersionedDeviceDetector.identifyGtv(paramContext.getResources(), paramContext.getPackageManager());
    this.isGtvIdentified = true;
  }

  public void identifyHoneycombTablet(Context paramContext)
  {
    if (this.isHoneycombTabletIdentified)
      return;
    this.isHoneycombTablet = VersionedDeviceDetector.identifyHcTablet(paramContext.getResources());
    this.isHoneycombTabletIdentified = true;
  }

  public boolean isAtLeastHdpi()
  {
    if (this.isAtLeastHdpi == null)
      this.isAtLeastHdpi = Boolean.valueOf(VersionedDeviceDetector.isAtLeastHdpi(FlixsterApplication.getContext().getResources()));
    return this.isAtLeastHdpi.booleanValue();
  }

  public boolean isGoogleTv()
  {
    return this.isGtv;
  }

  public boolean isHoneycombTablet()
  {
    return (this.isHoneycombTablet) && (!this.isGtv);
  }

  public boolean isMskEnabled()
  {
    return ((FlixsterApplication.isAdminEnabled()) || (FlixsterApplication.isMskEnabled())) && (Drm.logic().isDeviceStreamingCompatible());
  }

  public boolean isMskFinishedDeepLinkInitiated()
  {
    return this.isMskFinishedDeepLinkInitiated;
  }

  public boolean isScreenLarge()
  {
    if (this.isScreenLarge == null)
      this.isScreenLarge = Boolean.valueOf(VersionedDeviceDetector.isScreenLarge(FlixsterApplication.getContext().getResources()));
    return this.isScreenLarge.booleanValue();
  }

  public boolean isScreenXlarge()
  {
    if (this.isScreenXlarge == null)
      this.isScreenXlarge = Boolean.valueOf(VersionedDeviceDetector.isScreenXlarge(FlixsterApplication.getContext().getResources()));
    return this.isScreenXlarge.booleanValue();
  }

  public boolean isTelephonySupported()
  {
    if (F.API_LEVEL < 5)
      return true;
    return new EclairTelephonyDetector(null).isTelephonySupported();
  }

  public void reset()
  {
    resetSplash();
    this.isMskFinishedDeepLinkInitiated = false;
    resetVisitType();
    this.shouldUseLowBitrateForTrailer = false;
  }

  public void setMskFinishedDeepLinkInitiated()
  {
    this.isMskFinishedDeepLinkInitiated = true;
  }

  public void setProperties(Property paramProperty)
  {
    this.property = paramProperty;
    FlixsterApplication.setMskEnabled(paramProperty.isMskEnabled);
    FlixsterApplication.setMskAnonPromoUrl(paramProperty.mskAnonPromoUrl);
    FlixsterApplication.setMskUserPromoUrl(paramProperty.mskUserPromoUrl);
    FlixsterApplication.setMskTooltipUrl(paramProperty.mskTooltipUrl);
    Logger.i("FlxMain", "Properties.setProperties: " + paramProperty);
  }

  public void setUseLowBitrateForTrailer()
  {
    this.shouldUseLowBitrateForTrailer = true;
  }

  public void setUserSessionExpired()
  {
    this.hasUserSessionExpired = true;
  }

  public boolean shouldShowSplash()
  {
    return this.shouldShowSplash;
  }

  public boolean shouldUseLowBitrateForTrailer()
  {
    return this.shouldUseLowBitrateForTrailer;
  }

  public void splashRemoved()
  {
    Logger.i("FlxMain", "Splash removed");
    this.shouldShowSplash = false;
  }

  public void splashShown()
  {
    Logger.i("FlxMain", "Splash shown");
    this.shouldShowSplash = false;
  }

  private static class EclairTelephonyDetector
  {
    private boolean isTelephonySupported()
    {
      return FlixsterApplication.getContext().getPackageManager().hasSystemFeature("android.hardware.telephony");
    }
  }

  public static enum VisitType
  {
    static
    {
      SESSION = new VisitType("SESSION", 2);
      VisitType[] arrayOfVisitType = new VisitType[3];
      arrayOfVisitType[0] = INSTALL;
      arrayOfVisitType[1] = UPGRADE;
      arrayOfVisitType[2] = SESSION;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.Properties
 * JD-Core Version:    0.6.2
 */