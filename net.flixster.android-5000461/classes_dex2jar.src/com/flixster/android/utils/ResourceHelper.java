package com.flixster.android.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class ResourceHelper
{
  public static int dpToPx(float paramFloat, Resources paramResources)
  {
    return (int)(0.5F + paramFloat * paramResources.getDisplayMetrics().density);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.ResourceHelper
 * JD-Core Version:    0.6.2
 */