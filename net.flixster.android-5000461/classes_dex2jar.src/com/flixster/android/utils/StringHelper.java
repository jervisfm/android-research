package com.flixster.android.utils;

import java.text.DecimalFormat;

public class StringHelper
{
  private static final DecimalFormat US_INT_FORMAT = new DecimalFormat("###,###,###,###.##");

  public static String appendPeriod(String paramString)
  {
    if (paramString.endsWith("."))
      return paramString;
    return paramString + ".";
  }

  public static String ellipsize(String paramString, int paramInt)
  {
    if (paramString.length() > paramInt)
    {
      int i = paramString.lastIndexOf(" ", paramInt);
      if (i > -1)
        paramString = paramString.substring(0, i);
      paramString = paramString + "...";
    }
    return paramString;
  }

  public static String formatInt(int paramInt)
  {
    if ((LocationFacade.isUsLocale()) || (LocationFacade.isCanadaLocale()))
      return US_INT_FORMAT.format(paramInt);
    return String.valueOf(paramInt);
  }

  public static String getFirstTwoWords(String paramString)
  {
    if (paramString == null)
      paramString = null;
    int i;
    do
    {
      return paramString;
      i = paramString.indexOf(" ");
      if (i > -1)
        i = paramString.indexOf(" ", i + 1);
    }
    while (i <= -1);
    return paramString.substring(0, i);
  }

  public static String removeNonAlphaNumeric(String paramString)
  {
    return paramString.replaceAll("[^a-zA-Z0-9]", "");
  }

  public static String removeParentheses(String paramString)
  {
    return paramString.replaceAll("\\(", "").replaceAll("\\)", "");
  }

  public static String replaceNonAlphaNumeric(String paramString)
  {
    return paramString.replaceAll("[^a-zA-Z0-9]", "_");
  }

  public static String replaceSpace(String paramString)
  {
    return paramString.replaceAll(" ", "_");
  }

  public static String replaceSpace(String paramString1, String paramString2)
  {
    return paramString1.replaceAll(" ", paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.StringHelper
 * JD-Core Version:    0.6.2
 */