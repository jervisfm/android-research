package com.flixster.android.utils;

import android.content.Context;
import android.content.res.Resources;

public class Translator extends LocalizedHelper
{
  private static Translator instance = null;
  private final String[] englishGenres;
  private final String[] frenchGenres;

  private Translator(Context paramContext)
  {
    this.englishGenres = paramContext.getResources().getStringArray(2131623943);
    this.frenchGenres = paramContext.getResources().getStringArray(2131623944);
  }

  public static void initialize(Context paramContext)
  {
    try
    {
      if (instance == null)
        instance = new Translator(paramContext);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static Translator instance()
  {
    return instance;
  }

  public String translateGenre(String paramString)
  {
    if (!this.isFrance);
    while (true)
    {
      return paramString;
      for (int i = 0; i < this.englishGenres.length; i++)
        if (this.englishGenres[i].equals(paramString))
          return this.frenchGenres[i];
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.Translator
 * JD-Core Version:    0.6.2
 */