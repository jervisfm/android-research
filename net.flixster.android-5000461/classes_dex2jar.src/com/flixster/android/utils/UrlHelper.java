package com.flixster.android.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UrlHelper
{
  public static Map<String, List<String>> getQueries(String paramString)
  {
    HashMap localHashMap = new HashMap();
    String[] arrayOfString1 = paramString.split("\\?");
    String str1;
    String[] arrayOfString2;
    int i;
    if (arrayOfString1.length > 1)
    {
      str1 = arrayOfString1[1];
      arrayOfString2 = str1.split("&");
      i = arrayOfString2.length;
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        return localHashMap;
        str1 = arrayOfString1[0];
        break;
      }
      String[] arrayOfString3 = arrayOfString2[j].split("=");
      if (arrayOfString3.length > 1)
      {
        String str2 = arrayOfString3[0];
        String str3 = urlDecode(arrayOfString3[1]);
        Object localObject = (List)localHashMap.get(str2);
        if (localObject == null)
        {
          localObject = new ArrayList();
          localHashMap.put(str2, localObject);
        }
        ((List)localObject).add(str3);
      }
    }
  }

  public static String getSingleQueryValue(String paramString1, String paramString2)
  {
    List localList = (List)getQueries(paramString1).get(paramString2);
    if ((localList != null) && (localList.size() > 0))
      return (String)localList.get(0);
    return null;
  }

  public static String getSingleQueryValue(Map<String, List<String>> paramMap, String paramString)
  {
    List localList = (List)paramMap.get(paramString);
    if ((localList != null) && (localList.size() > 0))
      return (String)localList.get(0);
    return null;
  }

  public static String urlDecode(String paramString)
  {
    try
    {
      String str = URLDecoder.decode(paramString, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Logger.e("FlxMain", "UrlHelper.urlDecode", localUnsupportedEncodingException);
    }
    return null;
  }

  public static String urlEncode(String paramString)
  {
    return urlEncode(paramString, null);
  }

  public static String urlEncode(String paramString1, String paramString2)
  {
    try
    {
      String str = URLEncoder.encode(paramString1, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Logger.e("FlxGa", "UrlHelper.urlEncode", localUnsupportedEncodingException);
    }
    return paramString2;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.UrlHelper
 * JD-Core Version:    0.6.2
 */