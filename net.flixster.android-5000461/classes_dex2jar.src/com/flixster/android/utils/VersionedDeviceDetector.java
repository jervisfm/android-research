package com.flixster.android.utils;

import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class VersionedDeviceDetector
{
  private static final String FEATURE_GTV = "com.google.android.tv";

  public static boolean identifyGtv(Resources paramResources, PackageManager paramPackageManager)
  {
    if (F.API_LEVEL < 12);
    boolean bool;
    DonutScreenDetector localDonutScreenDetector;
    do
    {
      return false;
      bool = new EclairDetector(null).hasSystemFeature(paramPackageManager, "com.google.android.tv");
      localDonutScreenDetector = new DonutScreenDetector(null);
    }
    while ((!bool) || (!localDonutScreenDetector.isLarge(paramResources)) || (!localDonutScreenDetector.isAtLeastTvdpi(paramResources)));
    return true;
  }

  public static boolean identifyHc7InchesTablet(Resources paramResources)
  {
    if (F.API_LEVEL < 13)
      return false;
    return new DonutScreenDetector(null).isLarge(paramResources);
  }

  public static boolean identifyHcTablet(Resources paramResources)
  {
    if (F.API_LEVEL < 11)
      return false;
    return new DonutScreenDetector(null).isXlarge(paramResources);
  }

  public static boolean isAtLeastHdpi(Resources paramResources)
  {
    if (F.API_LEVEL < 4)
      return false;
    return new DonutScreenDetector(null).isAtLeastHdpi(paramResources);
  }

  public static boolean isScreenLarge(Resources paramResources)
  {
    if (F.API_LEVEL < 4)
      return false;
    return new DonutScreenDetector(null).isLarge(paramResources);
  }

  public static boolean isScreenXlarge(Resources paramResources)
  {
    if (F.API_LEVEL < 4)
      return false;
    return new DonutScreenDetector(null).isXlarge(paramResources);
  }

  private static class DonutScreenDetector
  {
    private boolean isAtLeastHdpi(Resources paramResources)
    {
      return paramResources.getDisplayMetrics().densityDpi >= 240;
    }

    private boolean isAtLeastTvdpi(Resources paramResources)
    {
      Logger.i("FlxMain", "DonutScreenDetector.densityDpi=" + paramResources.getDisplayMetrics().densityDpi);
      return paramResources.getDisplayMetrics().densityDpi >= 213;
    }

    private boolean isLarge(Resources paramResources)
    {
      return (0x3 & paramResources.getConfiguration().screenLayout) == 3;
    }

    private boolean isXlarge(Resources paramResources)
    {
      return (0x4 & paramResources.getConfiguration().screenLayout) == 4;
    }
  }

  private static class EclairDetector
  {
    private boolean hasSystemFeature(PackageManager paramPackageManager, String paramString)
    {
      return paramPackageManager.hasSystemFeature(paramString);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.VersionedDeviceDetector
 * JD-Core Version:    0.6.2
 */