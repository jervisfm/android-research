package com.flixster.android.utils;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class VersionedUidHelper
{
  private static String convertToNumericHash(String paramString)
  {
    if ((paramString != null) && (!"".equals(paramString)))
      return String.valueOf(Math.abs(paramString.hashCode()));
    return DeviceIdentifier.defaultId();
  }

  public static String getAndroidId(ContentResolver paramContentResolver)
  {
    return getOsBasedDeviceIdentifier().getAndroidId(paramContentResolver);
  }

  private static DeviceIdentifier getOsBasedDeviceIdentifier()
  {
    int i = F.API_LEVEL;
    if (i < 8)
      return new CupcakeDeviceIdentifier(null, null);
    if (i < 9)
      return new FroyoDeviceIdentifier(null, null);
    return new GingerbreadDeviceIdentifier(null);
  }

  public static String getSerialNumber()
  {
    return getOsBasedDeviceIdentifier().getSerialNumber();
  }

  public static String getTelephonyDeviceId(TelephonyManager paramTelephonyManager)
  {
    return DeviceIdentifier.getTelephonyId(paramTelephonyManager);
  }

  public static String getUdt(TelephonyManager paramTelephonyManager, ContentResolver paramContentResolver)
  {
    return convertToNumericHash(getOsBasedDeviceIdentifier().getUid(paramTelephonyManager, paramContentResolver));
  }

  private static class CupcakeDeviceIdentifier extends VersionedUidHelper.DeviceIdentifier
  {
    private CupcakeDeviceIdentifier()
    {
      super();
    }

    protected String getAndroidId(ContentResolver paramContentResolver)
    {
      return Settings.Secure.getString(paramContentResolver, "android_id");
    }

    protected String getRecommendedId(ContentResolver paramContentResolver)
    {
      return null;
    }

    protected String getSerialNumber()
    {
      return null;
    }
  }

  private static abstract class DeviceIdentifier
  {
    protected static String defaultId()
    {
      return "0";
    }

    protected static String getTelephonyId(TelephonyManager paramTelephonyManager)
    {
      return paramTelephonyManager.getDeviceId();
    }

    protected abstract String getAndroidId(ContentResolver paramContentResolver);

    protected abstract String getRecommendedId(ContentResolver paramContentResolver);

    protected abstract String getSerialNumber();

    protected String getUid(TelephonyManager paramTelephonyManager, ContentResolver paramContentResolver)
    {
      String str = getTelephonyId(paramTelephonyManager);
      if (str == null)
      {
        str = getRecommendedId(paramContentResolver);
        if (str == null)
          str = defaultId();
      }
      return str;
    }
  }

  private static class FroyoDeviceIdentifier extends VersionedUidHelper.CupcakeDeviceIdentifier
  {
    private FroyoDeviceIdentifier()
    {
      super();
    }

    protected String getRecommendedId(ContentResolver paramContentResolver)
    {
      return getAndroidId(paramContentResolver);
    }
  }

  @TargetApi(9)
  private static class GingerbreadDeviceIdentifier extends VersionedUidHelper.FroyoDeviceIdentifier
  {
    private GingerbreadDeviceIdentifier()
    {
      super();
    }

    protected String getRecommendedId(ContentResolver paramContentResolver)
    {
      return getSerialNumber();
    }

    protected String getSerialNumber()
    {
      return Build.SERIAL;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.VersionedUidHelper
 * JD-Core Version:    0.6.2
 */