package com.flixster.android.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.view.View;
import android.view.Window;

public class VersionedViewHelper
{
  public static void dimSystemNavigation(Activity paramActivity)
  {
    setSystemUiVisibility(paramActivity, 1);
  }

  public static void hideSystemNavigation(Activity paramActivity)
  {
    setSystemUiVisibility(paramActivity, 2);
  }

  protected static void setSystemUiVisibility(Activity paramActivity, int paramInt)
  {
    if (F.API_LEVEL >= 14)
      new HoneycombSystemUiSetter(null).setSystemUiVisibility(paramActivity.getWindow().getDecorView(), paramInt);
  }

  @TargetApi(11)
  private static class HoneycombSystemUiSetter
  {
    private void setSystemUiVisibility(View paramView, int paramInt)
    {
      paramView.setSystemUiVisibility(paramInt);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.VersionedViewHelper
 * JD-Core Version:    0.6.2
 */