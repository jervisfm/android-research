package com.flixster.android.utils;

import android.os.Looper;

public class WorkerThreads
{
  public static WorkerThreads instance()
  {
    return SingletonHolder.INSTANCE;
  }

  public static boolean isNotMainThread()
  {
    return Thread.currentThread().getId() != Looper.getMainLooper().getThread().getId();
  }

  public void invokeLater(Runnable paramRunnable)
  {
    new Thread(paramRunnable).start();
  }

  private static class SingletonHolder
  {
    public static final WorkerThreads INSTANCE = new WorkerThreads(null);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.utils.WorkerThreads
 * JD-Core Version:    0.6.2
 */