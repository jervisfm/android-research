package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.flixster.android.model.CarouselItem;
import com.flixster.android.model.Image;
import com.flixster.android.utils.Logger;
import java.util.Collections;
import java.util.List;

public class Carousel extends RelativeLayout
{
  private TextView caption;
  private final Context context;
  private PageIndicator dots;
  private Gallery gallery;
  private View.OnClickListener galleryOnClickListener;
  private List<CarouselItem> items;

  public Carousel(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
  }

  public Carousel(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
  }

  public Carousel(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
  }

  private void initializeGallery()
  {
    this.gallery.setAdapter(new ImageAdapter(this.context, this.items, null));
    this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        Carousel.this.caption.setText(((CarouselItem)Carousel.this.items.get(paramAnonymousInt)).getTitle());
        Carousel.this.dots.setCurrPageNum(paramAnonymousInt);
      }

      public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView)
      {
      }
    });
    this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        Carousel.this.setTag(Carousel.this.items.get(paramAnonymousInt));
        Carousel.this.galleryOnClickListener.onClick(Carousel.this);
      }
    });
  }

  public void load(List<CarouselItem> paramList, View.OnClickListener paramOnClickListener)
  {
    this.items = Collections.unmodifiableList(paramList);
    this.galleryOnClickListener = paramOnClickListener;
    int i = getResources().getDimensionPixelSize(2131361838);
    int j = getResources().getDimensionPixelSize(2131361856);
    int k = getResources().getDimensionPixelSize(2131361857);
    int m = getResources().getDimensionPixelSize(2131361858);
    this.gallery = new GallerySansFling(this.context);
    this.gallery.setLayoutParams(new RelativeLayout.LayoutParams(-1, i));
    this.gallery.setHorizontalFadingEdgeEnabled(false);
    this.gallery.setUnselectedAlpha(1.0F);
    initializeGallery();
    addView(this.gallery);
    RelativeLayout localRelativeLayout = new RelativeLayout(this.context);
    RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-1, j);
    localLayoutParams1.addRule(12);
    localLayoutParams1.addRule(14);
    localRelativeLayout.setLayoutParams(localLayoutParams1);
    localRelativeLayout.setBackgroundColor(getResources().getColor(2131296283));
    addView(localRelativeLayout);
    this.caption = new TextView(this.context);
    RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams2.addRule(12);
    localLayoutParams2.addRule(14);
    this.caption.setLayoutParams(localLayoutParams2);
    this.caption.setPadding(10, 0, 10, k);
    this.caption.setTextAppearance(this.context, 2131558514);
    this.caption.setTextColor(-1);
    this.caption.setSingleLine();
    this.caption.setEllipsize(TextUtils.TruncateAt.END);
    addView(this.caption);
    this.dots = new PageIndicator(this.context);
    this.dots.setNumOfPages(paramList.size());
    RelativeLayout.LayoutParams localLayoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams3.addRule(12);
    localLayoutParams3.addRule(14);
    this.dots.setLayoutParams(localLayoutParams3);
    this.dots.setPadding(0, 0, 0, m);
    addView(this.dots);
  }

  private static class ImageAdapter extends BaseAdapter
  {
    private final Context context;
    private final List<? extends Image> images;

    private ImageAdapter(Context paramContext, List<? extends Image> paramList)
    {
      this.context = paramContext;
      this.images = paramList;
    }

    public int getCount()
    {
      return this.images.size();
    }

    public Object getItem(int paramInt)
    {
      return Integer.valueOf(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if ((paramView == null) || (!(paramView instanceof ImageView)))
        paramView = new HighlightableImageView(this.context);
      Logger.d("FlxMain", "Carousel.ImageAdapter.getView " + paramInt + " " + paramView.toString());
      paramView.setLayoutParams(new Gallery.LayoutParams(-1, -1));
      Bitmap localBitmap = ((Image)this.images.get(paramInt)).getBitmap((ImageView)paramView, ImageView.ScaleType.CENTER_CROP);
      if (localBitmap == null)
      {
        ((ImageView)paramView).setImageResource(2130837812);
        ((ImageView)paramView).setScaleType(ImageView.ScaleType.CENTER);
        return paramView;
      }
      ((ImageView)paramView).setImageBitmap(localBitmap);
      ((ImageView)paramView).setScaleType(ImageView.ScaleType.CENTER_CROP);
      return paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.Carousel
 * JD-Core Version:    0.6.2
 */