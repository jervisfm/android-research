package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView.LayoutParams;

public class Divider extends View
{
  public Divider(Context paramContext)
  {
    super(paramContext);
    load();
  }

  public Divider(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    load();
  }

  public Divider(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    load();
  }

  public void load()
  {
    setLayoutParams(new AbsListView.LayoutParams(-1, getResources().getDimensionPixelSize(2131361829)));
    setBackgroundResource(2131296282);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.Divider
 * JD-Core Version:    0.6.2
 */