package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.net.DownloadHelper;
import net.flixster.android.model.LockerRight;

public class DownloadPanel extends RelativeLayout
{
  private final Context context;
  private ImageButton downloadDelete;
  private TextView downloadPanel;
  private final Handler downloadSizeCallback = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i = ((Integer)paramAnonymousMessage.obj).intValue();
      DownloadPanel.this.right.setDownloadAssetSize(i);
      Resources localResources = DownloadPanel.this.getResources();
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = DownloadPanel.this.right.getDownloadAssetSize();
      String str = localResources.getString(2131493262, arrayOfObject);
      DownloadPanel.this.downloadPanel.setText(str);
    }
  };
  private LockerRight right;

  public DownloadPanel(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public DownloadPanel(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public DownloadPanel(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903076, this);
    this.downloadPanel = ((TextView)findViewById(2131165292));
    this.downloadDelete = ((ImageButton)findViewById(2131165293));
  }

  public void load(LockerRight paramLockerRight, View.OnClickListener paramOnClickListener1, View.OnClickListener paramOnClickListener2)
  {
    this.right = paramLockerRight;
    this.downloadPanel.setOnClickListener(paramOnClickListener1);
    this.downloadDelete.setOnClickListener(paramOnClickListener2);
    this.downloadPanel.setTag(paramLockerRight);
    this.downloadDelete.setTag(paramLockerRight);
    refresh();
  }

  public void refresh()
  {
    Resources localResources = getResources();
    String str2;
    int i;
    ImageButton localImageButton;
    int j;
    if (DownloadHelper.isMovieDownloadInProgress(this.right.rightId))
    {
      str2 = localResources.getString(2131493263);
      i = 1;
      this.downloadPanel.setText(str2);
      localImageButton = this.downloadDelete;
      j = 0;
      if (i == 0)
        break label236;
    }
    while (true)
    {
      localImageButton.setVisibility(j);
      return;
      if (DownloadHelper.isDownloaded(this.right.rightId))
      {
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = this.right.getDownloadedAssetSize();
        str2 = localResources.getString(2131493264, arrayOfObject);
        i = 1;
        break;
      }
      String str1 = this.right.getDownloadAssetSize();
      if ((str1 == null) || ("".equals(str1)))
      {
        if (this.right.getDownloadUri() == null)
        {
          this.downloadSizeCallback.sendMessage(Message.obtain(null, 0, Integer.valueOf(this.right.getEstimatedDownloadFileSize())));
          str2 = localResources.getString(2131493262, new Object[] { "" });
          i = 0;
          break;
        }
        DownloadHelper.getRemoteFileSize(this.right.getDownloadUri(), this.downloadSizeCallback);
        str2 = localResources.getString(2131493262, new Object[] { "" });
        i = 0;
        break;
      }
      str2 = localResources.getString(2131493262, new Object[] { str1 });
      i = 0;
      break;
      label236: j = 8;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.DownloadPanel
 * JD-Core Version:    0.6.2
 */