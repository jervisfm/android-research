package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.flixster.android.data.AccountManager;
import com.flixster.android.drm.Drm;
import com.flixster.android.drm.PlaybackLogic;
import com.flixster.android.utils.Logger;
import net.flixster.android.model.Episode;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.User;

public class EpisodeView extends LinearLayout
{
  private final Context context;
  private DownloadPanel downloadPanel;
  private long episodeId;
  private TextView subheader;
  private SynopsisView synopsis;
  private TextView title;
  private TextView watchNowPanel;

  public EpisodeView(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public EpisodeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903077, this);
    setBackgroundResource(17301602);
    this.subheader = ((TextView)findViewById(2131165294));
    this.title = ((TextView)findViewById(2131165295));
    this.synopsis = ((SynopsisView)findViewById(2131165296));
    this.watchNowPanel = ((TextView)findViewById(2131165297));
    this.downloadPanel = ((DownloadPanel)findViewById(2131165298));
  }

  public void load(LockerRight paramLockerRight, Episode paramEpisode, View.OnClickListener paramOnClickListener1, View.OnClickListener paramOnClickListener2, View.OnClickListener paramOnClickListener3)
  {
    this.episodeId = paramEpisode.getId();
    int i = paramEpisode.getEpisodeNumber();
    TextView localTextView = this.subheader;
    Resources localResources1 = getResources();
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(i);
    localTextView.setText(localResources1.getString(2131493287, arrayOfObject1));
    this.title.setText(paramEpisode.getTitle());
    this.synopsis.load(paramEpisode.getSynopsis());
    LockerRight localLockerRight = AccountManager.instance().getUser().getLockerRightFromRightId(paramLockerRight.getChildRightId(this.episodeId));
    if (localLockerRight == null)
    {
      Logger.w("FlxMain", "EpisodeView.load missing right for episode id " + this.episodeId + " number " + i);
      Context localContext = this.context;
      Resources localResources2 = getResources();
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Integer.valueOf(i);
      Toast.makeText(localContext, localResources2.getString(2131493290, arrayOfObject2), 0).show();
      this.watchNowPanel.setVisibility(8);
      this.downloadPanel.setVisibility(8);
      return;
    }
    if (Drm.logic().isAssetPlayable(localLockerRight))
    {
      this.watchNowPanel.setVisibility(0);
      this.watchNowPanel.setOnClickListener(paramOnClickListener1);
      this.watchNowPanel.setTag(localLockerRight);
    }
    while (Drm.logic().isAssetDownloadable(localLockerRight))
    {
      this.downloadPanel.setVisibility(0);
      this.downloadPanel.load(localLockerRight, paramOnClickListener2, paramOnClickListener3);
      return;
      this.watchNowPanel.setVisibility(8);
    }
    this.downloadPanel.setVisibility(8);
  }

  public void refresh()
  {
    Logger.d("FlxMain", "EpisodeView.refresh " + this.episodeId);
    if (this.downloadPanel.getVisibility() == 0)
      this.downloadPanel.refresh();
  }

  public void refresh(long paramLong)
  {
    if (this.episodeId == paramLong)
      refresh();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.EpisodeView
 * JD-Core Version:    0.6.2
 */