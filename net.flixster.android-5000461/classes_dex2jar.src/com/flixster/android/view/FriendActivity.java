package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.analytics.Tracker;
import com.flixster.android.analytics.Trackers;
import net.flixster.android.Starter;
import net.flixster.android.lvi.LviMovie;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

public class FriendActivity extends RelativeLayout
{
  private final Context context;
  private ImageView image;
  private RelativeLayout movie;
  private ImageView rating;
  private TextView review;
  private TextView titleAction;
  private TextView titleName;

  public FriendActivity(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public FriendActivity(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public FriendActivity(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903086, this);
    setBackgroundResource(17301602);
    this.image = ((ImageView)findViewById(2131165323));
    this.titleName = ((TextView)findViewById(2131165325));
    this.titleAction = ((TextView)findViewById(2131165326));
    this.rating = ((ImageView)findViewById(2131165327));
    this.review = ((TextView)findViewById(2131165328));
    this.movie = ((RelativeLayout)findViewById(2131165329));
  }

  public void load(Review paramReview)
  {
    Bitmap localBitmap = paramReview.getReviewerBitmap(this.image);
    if (localBitmap != null)
      this.image.setImageBitmap(localBitmap);
    View.OnClickListener local1 = new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Starter.launchUserProfile(this.val$reviewerId, FriendActivity.this.context);
        Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FriendActivityUser");
      }
    };
    this.image.setFocusable(true);
    this.image.setOnClickListener(local1);
    this.titleName.setText(paramReview.name);
    this.titleName.setFocusable(true);
    this.titleName.setOnClickListener(local1);
    this.titleAction.setText(getResources().getString(paramReview.getActionId()));
    this.rating.setImageResource(net.flixster.android.Flixster.RATING_SMALL_R[((int)(2.0D * paramReview.stars))]);
    if ((paramReview.comment != null) && (!paramReview.comment.equals("")))
      this.review.setText(paramReview.comment);
    while (true)
    {
      Movie localMovie = paramReview.getMovie();
      if (localMovie != null)
      {
        LviMovie localLviMovie = new LviMovie();
        localLviMovie.mMovie = localMovie;
        LviMovieViewUpdateHandler localLviMovieViewUpdateHandler = new LviMovieViewUpdateHandler();
        View localView = localLviMovie.getView(0, null, this, this.context, localLviMovieViewUpdateHandler);
        localView.setFocusable(true);
        localView.setBackgroundResource(17301602);
        final long l = localMovie.getId();
        setFocusable(true);
        setOnClickListener(new View.OnClickListener()
        {
          public void onClick(View paramAnonymousView)
          {
            Starter.launchMovieDetail(l, FriendActivity.this.context);
            Trackers.instance().trackEvent("/homepage", "Homepage", "Homepage", "FriendActivityMovie");
          }
        });
        localLviMovieViewUpdateHandler.setView(localView);
        this.movie.addView(localView);
      }
      return;
      this.review.setVisibility(8);
    }
  }

  public static class LviMovieViewUpdateHandler extends Handler
  {
    private View view;

    public void handleMessage(Message paramMessage)
    {
      Bitmap localBitmap = (Bitmap)paramMessage.obj;
      ((ImageView)this.view.findViewById(2131165446)).setImageBitmap(localBitmap);
      this.view.invalidate();
    }

    public void setView(View paramView)
    {
      this.view = paramView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.FriendActivity
 * JD-Core Version:    0.6.2
 */