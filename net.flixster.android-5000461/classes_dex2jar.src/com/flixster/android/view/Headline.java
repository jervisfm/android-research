package com.flixster.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.model.HeadlineItem;

public class Headline extends RelativeLayout
{
  private final Context context;
  private ImageView image;
  private TextView title;

  public Headline(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public Headline(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public Headline(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903092, this);
    setBackgroundResource(17301602);
    this.image = ((ImageView)findViewById(2131165365));
    this.title = ((TextView)findViewById(2131165366));
  }

  public void load(HeadlineItem paramHeadlineItem)
  {
    Bitmap localBitmap = paramHeadlineItem.getBitmap(this.image, ImageView.ScaleType.CENTER_CROP);
    if (localBitmap != null)
    {
      this.image.setImageBitmap(localBitmap);
      this.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }
    this.title.setText(paramHeadlineItem.getTitle());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.Headline
 * JD-Core Version:    0.6.2
 */