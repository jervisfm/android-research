package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class HighlightableImageView extends ImageView
{
  public HighlightableImageView(Context paramContext)
  {
    super(paramContext);
  }

  public HighlightableImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public HighlightableImageView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public void setImageBitmap(Bitmap paramBitmap)
  {
    Resources localResources = getResources();
    Drawable[] arrayOfDrawable = new Drawable[2];
    arrayOfDrawable[0] = new BitmapDrawable(localResources, paramBitmap);
    arrayOfDrawable[1] = localResources.getDrawable(2130837782);
    setImageDrawable(new LayerDrawable(arrayOfDrawable));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.HighlightableImageView
 * JD-Core Version:    0.6.2
 */