package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadMore extends RelativeLayout
{
  private final Context context;
  private TextView count;
  private RelativeLayout root;
  private TextView title;

  public LoadMore(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public LoadMore(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public LoadMore(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903103, this);
    this.root = ((RelativeLayout)findViewById(2131165404));
    this.title = ((TextView)findViewById(2131165405));
    this.count = ((TextView)findViewById(2131165406));
  }

  public void load(String paramString1, String paramString2)
  {
    if (paramString1 != null)
      this.title.setText(paramString1);
    if (paramString2 != null)
    {
      this.count.setText(paramString2);
      this.count.setVisibility(0);
      return;
    }
    this.count.setVisibility(8);
  }

  public void load(String paramString1, String paramString2, boolean paramBoolean)
  {
    load(paramString1, paramString2);
    int i = this.context.getResources().getDimensionPixelSize(2131361843) + this.context.getResources().getDimensionPixelSize(2131361835);
    this.root.setPadding(i, this.root.getPaddingTop(), this.root.getPaddingRight(), this.root.getPaddingBottom());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.LoadMore
 * JD-Core Version:    0.6.2
 */