package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;

public class PageIndicator extends View
{
  private int currPageNum;
  private int dotDistance;
  private Paint glowPaint;
  private Paint normPaint;
  private int numOfPages;
  private int unitLength;

  public PageIndicator(Context paramContext)
  {
    super(paramContext);
    initialize();
  }

  public PageIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initialize();
  }

  public PageIndicator(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initialize();
  }

  private void initialize()
  {
    this.unitLength = getResources().getDimensionPixelSize(2131361859);
    this.dotDistance = (5 * this.unitLength);
    this.glowPaint = new Paint();
    this.glowPaint.setAntiAlias(true);
    this.glowPaint.setColor(-1);
    this.normPaint = new Paint();
    this.normPaint.setAntiAlias(true);
    this.normPaint.setColor(-7829368);
  }

  private int measureHeight(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    int k;
    if (1073741824 == i)
      k = j;
    do
    {
      return k;
      k = 4 * this.unitLength + getPaddingTop() + getPaddingBottom();
    }
    while (-2147483648 != i);
    return Math.min(k, j);
  }

  private int measureWidth(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    int j = View.MeasureSpec.getSize(paramInt);
    int k;
    if (1073741824 == i)
      k = j;
    do
    {
      return k;
      k = 4 * this.unitLength + this.numOfPages * this.dotDistance + getPaddingLeft() + getPaddingRight();
    }
    while (-2147483648 != i);
    return Math.min(k, j);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = this.unitLength + this.unitLength;
    int j = i;
    int k = 0;
    if (k >= this.numOfPages)
      return;
    float f1 = j;
    float f2 = i;
    float f3 = this.unitLength;
    if (k == this.currPageNum);
    for (Paint localPaint = this.glowPaint; ; localPaint = this.normPaint)
    {
      paramCanvas.drawCircle(f1, f2, f3, localPaint);
      j += this.dotDistance;
      k++;
      break;
    }
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    setMeasuredDimension(measureWidth(paramInt1), measureHeight(paramInt2));
  }

  public void setCurrPageNum(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.numOfPages))
      this.currPageNum = paramInt;
    invalidate();
  }

  public void setNumOfPages(int paramInt)
  {
    this.numOfPages = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.PageIndicator
 * JD-Core Version:    0.6.2
 */