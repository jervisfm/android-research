package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.ProfileDao;
import net.flixster.android.model.Movie;
import net.flixster.android.model.Review;

public class QuickRateView extends RelativeLayout
  implements RatingBar.OnRatingBarChangeListener, View.OnClickListener
{
  private final Context context;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
    }
  };
  private TextView friendRtScore;
  private final boolean isWts;
  private Movie movie;
  private TextView movieActors;
  private ImageView moviePoster;
  private TextView movieRtScore;
  private TextView movieTitle;
  private final Handler parentHandler;
  private double rating;
  private RatingBar ratingBar;
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
    }
  };
  private ImageView wtsButton;

  public QuickRateView(Context paramContext, boolean paramBoolean, Handler paramHandler)
  {
    super(paramContext);
    this.context = paramContext;
    this.isWts = paramBoolean;
    this.parentHandler = paramHandler;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903152, this);
    this.moviePoster = ((ImageView)findViewById(2131165446));
    this.movieTitle = ((TextView)findViewById(2131165448));
    this.movieActors = ((TextView)findViewById(2131165451));
    this.movieRtScore = ((TextView)findViewById(2131165450));
    this.friendRtScore = ((TextView)findViewById(2131165589));
    this.ratingBar = ((RatingBar)findViewById(2131165711));
    this.ratingBar.setOnRatingBarChangeListener(this);
    this.wtsButton = ((ImageView)findViewById(2131165712));
    this.wtsButton.setOnClickListener(this);
  }

  private void postReview(double paramDouble)
  {
    String str = FlixsterApplication.getFlixsterId();
    Review localReview = new Review();
    localReview.stars = paramDouble;
    ProfileDao.postUserMovieReview(this.successHandler, this.errorHandler, str, Long.toString(this.movie.getId()), localReview);
  }

  private void setFriendScore()
  {
    if ((this.movie.checkIntProperty("FRIENDS_RATED_COUNT")) && (this.movie.getIntProperty("FRIENDS_RATED_COUNT").intValue() > 0))
    {
      Drawable localDrawable2 = getResources().getDrawable(2130837739);
      localDrawable2.setBounds(0, 0, localDrawable2.getIntrinsicWidth(), localDrawable2.getIntrinsicHeight());
      this.friendRtScore.setCompoundDrawables(localDrawable2, null, null, null);
      int j = this.movie.getIntProperty("FRIENDS_RATED_COUNT").intValue();
      StringBuilder localStringBuilder2 = new StringBuilder();
      localStringBuilder2.append(j).append(" ");
      if (j > 1)
        localStringBuilder2.append(getResources().getString(2131493008));
      while (true)
      {
        this.friendRtScore.setText(localStringBuilder2.toString());
        this.friendRtScore.setVisibility(0);
        return;
        localStringBuilder2.append(getResources().getString(2131493007));
      }
    }
    if ((this.movie.checkIntProperty("FRIENDS_WTS_COUNT")) && (this.movie.getIntProperty("FRIENDS_WTS_COUNT").intValue() > 0))
    {
      Drawable localDrawable1 = getResources().getDrawable(2130837751);
      localDrawable1.setBounds(0, 0, localDrawable1.getIntrinsicWidth(), localDrawable1.getIntrinsicHeight());
      this.friendRtScore.setCompoundDrawables(localDrawable1, null, null, null);
      int i = this.movie.getIntProperty("FRIENDS_WTS_COUNT").intValue();
      StringBuilder localStringBuilder1 = new StringBuilder();
      localStringBuilder1.append(i).append(" ");
      if (i > 1)
        localStringBuilder1.append(getResources().getString(2131493012));
      while (true)
      {
        this.friendRtScore.setText(localStringBuilder1.toString());
        this.friendRtScore.setVisibility(0);
        return;
        localStringBuilder1.append(getResources().getString(2131493011));
      }
    }
    this.friendRtScore.setVisibility(8);
  }

  private void setRtScore()
  {
    if (this.movie.checkIntProperty("rottenTomatoes"))
    {
      int i = this.movie.getIntProperty("rottenTomatoes").intValue();
      int j;
      if (i < 60)
      {
        j = 1;
        this.movieRtScore.setText(i + "%");
        if (j == 0)
          break label119;
      }
      label119: for (int k = 2130837741; ; k = 2130837726)
      {
        Drawable localDrawable = getResources().getDrawable(k);
        localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
        this.movieRtScore.setCompoundDrawables(localDrawable, null, null, null);
        this.movieRtScore.setVisibility(0);
        return;
        j = 0;
        break;
      }
    }
    this.movieRtScore.setVisibility(8);
  }

  public void load(Movie paramMovie)
  {
    this.movie = paramMovie;
    String str;
    if (paramMovie.getProfilePoster() == null)
    {
      this.moviePoster.setImageResource(2130837839);
      this.movieTitle.setText(paramMovie.getTitle());
      str = paramMovie.getProperty("MOVIE_ACTORS_SHORT");
      if ((str != null) && (str.length() != 0))
        break label116;
      this.movieActors.setVisibility(8);
    }
    while (true)
    {
      setRtScore();
      setFriendScore();
      if (!this.isWts)
        break label135;
      this.ratingBar.setVisibility(8);
      this.wtsButton.setVisibility(0);
      return;
      Bitmap localBitmap = paramMovie.getProfileBitmap(this.moviePoster);
      if (localBitmap == null)
        break;
      this.moviePoster.setImageBitmap(localBitmap);
      break;
      label116: this.movieActors.setText(str);
      this.movieActors.setVisibility(0);
    }
    label135: this.ratingBar.setVisibility(0);
    this.wtsButton.setVisibility(8);
  }

  public void onClick(View paramView)
  {
    switch (paramView.getId())
    {
    default:
      return;
    case 2131165712:
    }
    if (this.rating == 0.0D)
    {
      this.wtsButton.setImageResource(2130837982);
      this.rating = 5.5D;
      this.parentHandler.handleMessage(Message.obtain(this.parentHandler, 1));
    }
    while (true)
    {
      postReview(this.rating);
      return;
      this.wtsButton.setImageResource(2130837983);
      this.rating = 0.0D;
      this.parentHandler.handleMessage(Message.obtain(this.parentHandler, -1));
    }
  }

  public void onRatingChanged(RatingBar paramRatingBar, float paramFloat, boolean paramBoolean)
  {
    if (this.rating == 0.0D)
      this.parentHandler.handleMessage(Message.obtain(this.parentHandler, 1));
    while (true)
    {
      this.rating = paramFloat;
      postReview(paramFloat);
      return;
      if (paramFloat == 0.0F)
        this.parentHandler.handleMessage(Message.obtain(this.parentHandler, -1));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.QuickRateView
 * JD-Core Version:    0.6.2
 */