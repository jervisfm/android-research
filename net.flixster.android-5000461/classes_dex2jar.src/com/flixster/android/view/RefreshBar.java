package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RefreshBar extends RelativeLayout
{
  private final Context context;

  public RefreshBar(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    load();
  }

  public RefreshBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    load();
  }

  public RefreshBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    load();
  }

  private void load()
  {
    Drawable localDrawable = getResources().getDrawable(2130837710);
    TextView localTextView = new TextView(this.context);
    localTextView.setText(2131493144);
    localTextView.setCompoundDrawablesWithIntrinsicBounds(localDrawable, null, null, null);
    localTextView.setCompoundDrawablePadding(getResources().getDimensionPixelSize(2131361820));
    localTextView.setTextAppearance(this.context, 2131558511);
    addView(localTextView);
    setBackgroundResource(2130837621);
    setGravity(17);
    setPadding(0, getResources().getDimensionPixelSize(2131361822), 0, getResources().getDimensionPixelSize(2131361823));
  }

  public RefreshBar setListener(View.OnClickListener paramOnClickListener)
  {
    setOnClickListener(paramOnClickListener);
    return this;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.RefreshBar
 * JD-Core Version:    0.6.2
 */