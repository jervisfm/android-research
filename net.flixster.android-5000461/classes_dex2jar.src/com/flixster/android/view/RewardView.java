package com.flixster.android.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.data.AccountFacade;
import com.flixster.android.msk.FacebookInvitePage;
import com.flixster.android.msk.FriendSelectPage;
import com.flixster.android.utils.Properties;
import java.util.Iterator;
import java.util.List;
import net.flixster.android.QuickRatePage;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Property;
import net.flixster.android.model.User;

public class RewardView extends RelativeLayout
{
  public static final int REWARD_FB_INVITE = 6;
  public static final int REWARD_INSTALL_MOBILE = 5;
  public static final int REWARD_RATE = 1;
  public static final int REWARD_SMS = 3;
  public static final int REWARD_UV_CREATE = 4;
  public static final int REWARD_WTS = 2;
  private final Context context;
  private TextView description;
  private ImageView image;
  private TextView incentive;

  public RewardView(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903156, this);
    setBackgroundResource(17301602);
    this.image = ((ImageView)findViewById(2131165598));
    this.description = ((TextView)findViewById(2131165741));
    this.incentive = ((TextView)findViewById(2131165742));
  }

  public void load(final int paramInt, List<LockerRight> paramList)
  {
    User localUser = AccountFacade.getSocialUser();
    Property localProperty = Properties.instance().getProperties();
    switch (paramInt)
    {
    default:
      if (paramList != null)
        break label466;
      if ((paramInt == 3) || (paramInt == 6))
        this.incentive.setText(2131493194);
      break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    }
    while (true)
    {
      setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          User localUser = AccountFacade.getSocialUser();
          switch (paramInt)
          {
          case 4:
          case 5:
          default:
          case 1:
          case 2:
          case 3:
          case 6:
          }
          do
          {
            do
            {
              do
              {
                do
                  return;
                while (!localUser.isMskRateEligible);
                Intent localIntent4 = new Intent(RewardView.this.context, QuickRatePage.class);
                localIntent4.putExtra("KEY_IS_WTS", false);
                RewardView.this.context.startActivity(localIntent4);
                return;
              }
              while (!localUser.isMskWtsEligible);
              Intent localIntent3 = new Intent(RewardView.this.context, QuickRatePage.class);
              localIntent3.putExtra("KEY_IS_WTS", true);
              RewardView.this.context.startActivity(localIntent3);
              return;
            }
            while (!localUser.isMskSmsEligible);
            Intent localIntent2 = new Intent(RewardView.this.context, FriendSelectPage.class);
            localIntent2.putExtra("KEY_IS_MSK", false);
            RewardView.this.context.startActivity(localIntent2);
            return;
          }
          while (!localUser.isMskFbInviteEligible);
          Intent localIntent1 = new Intent(RewardView.this.context, FacebookInvitePage.class);
          RewardView.this.context.startActivity(localIntent1);
        }
      });
      return;
      ImageView localImageView6 = this.image;
      if (localUser.isMskRateEligible);
      for (int i2 = 2130837874; ; i2 = 2130837875)
      {
        localImageView6.setImageResource(i2);
        int i3;
        if (localProperty != null)
        {
          i3 = localProperty.rewardsQuickRate;
          if (i3 != 0);
        }
        else
        {
          i3 = 25;
        }
        TextView localTextView4 = this.description;
        Resources localResources4 = getResources();
        Object[] arrayOfObject4 = new Object[1];
        arrayOfObject4[0] = Integer.valueOf(i3);
        localTextView4.setText(localResources4.getString(2131493197, arrayOfObject4));
        break;
      }
      ImageView localImageView5 = this.image;
      if (localUser.isMskWtsEligible);
      for (int n = 2130837880; ; n = 2130837881)
      {
        localImageView5.setImageResource(n);
        int i1;
        if (localProperty != null)
        {
          i1 = localProperty.rewardsQuickWts;
          if (i1 != 0);
        }
        else
        {
          i1 = 25;
        }
        TextView localTextView3 = this.description;
        Resources localResources3 = getResources();
        Object[] arrayOfObject3 = new Object[1];
        arrayOfObject3[0] = Integer.valueOf(i1);
        localTextView3.setText(localResources3.getString(2131493198, arrayOfObject3));
        break;
      }
      ImageView localImageView4 = this.image;
      if (localUser.isMskSmsEligible);
      for (int m = 2130837876; ; m = 2130837877)
      {
        localImageView4.setImageResource(m);
        this.description.setText(2131493199);
        break;
      }
      ImageView localImageView3 = this.image;
      if (localUser.isMskUvCreateEligible);
      for (int k = 2130837878; ; k = 2130837879)
      {
        localImageView3.setImageResource(k);
        this.description.setText(2131493201);
        break;
      }
      ImageView localImageView2 = this.image;
      if (localUser.isMskInstallMobileEligible);
      for (int j = 2130837872; ; j = 2130837873)
      {
        localImageView2.setImageResource(j);
        this.description.setText(2131493202);
        break;
      }
      ImageView localImageView1 = this.image;
      if (localUser.isMskFbInviteEligible);
      for (int i = 2130837690; ; i = 2130837691)
      {
        localImageView1.setImageResource(i);
        this.description.setText(2131493200);
        break;
      }
      this.incentive.setText(2131493193);
    }
    label466: if ((paramInt == 6) && (paramList.size() < 3))
    {
      TextView localTextView2 = this.incentive;
      Resources localResources2 = getResources();
      Object[] arrayOfObject2 = new Object[1];
      StringBuilder localStringBuilder2 = new StringBuilder(String.valueOf(paramList.size())).append(" ");
      if (paramList.size() == 1);
      for (String str = getResources().getString(2131493115); ; str = getResources().getString(2131493114))
      {
        arrayOfObject2[0] = str;
        localTextView2.setText(localResources2.getString(2131493195, arrayOfObject2));
        break;
      }
    }
    StringBuilder localStringBuilder1 = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
      {
        TextView localTextView1 = this.incentive;
        Resources localResources1 = getResources();
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = localStringBuilder1.toString();
        localTextView1.setText(localResources1.getString(2131493196, arrayOfObject1));
        break;
      }
      LockerRight localLockerRight = (LockerRight)localIterator.next();
      if (localStringBuilder1.length() > 0)
        localStringBuilder1.append(", ");
      localStringBuilder1.append(localLockerRight.getTitle());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.RewardView
 * JD-Core Version:    0.6.2
 */