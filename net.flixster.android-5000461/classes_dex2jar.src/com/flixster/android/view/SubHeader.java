package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class SubHeader extends TextView
{
  private final Context context;

  public SubHeader(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public SubHeader(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public SubHeader(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    int i = getResources().getDimensionPixelSize(2131361878);
    int j = getResources().getDimensionPixelSize(2131361879);
    int k = getResources().getDimensionPixelSize(2131361880);
    int m = getResources().getDimensionPixelSize(2131361881);
    setBackgroundResource(2130837941);
    setGravity(16);
    setPadding(i, m, k, j);
    setTextAppearance(this.context, 2131558527);
  }

  public void setText(CharSequence paramCharSequence, TextView.BufferType paramBufferType)
  {
    super.setText(paramCharSequence.toString().toUpperCase(), paramBufferType);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.SubHeader
 * JD-Core Version:    0.6.2
 */