package com.flixster.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SubNavBar extends LinearLayout
{
  private TextView button1;
  private TextView button2;
  private TextView button3;
  private TextView button4;
  private View.OnClickListener callback;
  private final Context context;
  private final View.OnClickListener internalListener = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      SubNavBar.this.setSelectedButton(paramAnonymousView.getId());
      SubNavBar.this.callback.onClick(paramAnonymousView);
    }
  };

  public SubNavBar(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public SubNavBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public SubNavBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903169, this);
    this.button1 = ((TextView)findViewById(2131165784));
    this.button2 = ((TextView)findViewById(2131165785));
    this.button3 = ((TextView)findViewById(2131165786));
    this.button4 = ((TextView)findViewById(2131165787));
  }

  public void load(View.OnClickListener paramOnClickListener, int paramInt1, int paramInt2)
  {
    load(paramOnClickListener, paramInt1, paramInt2, 0, 0);
  }

  public void load(View.OnClickListener paramOnClickListener, int paramInt1, int paramInt2, int paramInt3)
  {
    load(paramOnClickListener, paramInt1, paramInt2, paramInt3, 0);
  }

  public void load(View.OnClickListener paramOnClickListener, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.callback = paramOnClickListener;
    this.button1.setText(paramInt1);
    this.button2.setText(paramInt2);
    this.button1.setOnClickListener(this.internalListener);
    this.button2.setOnClickListener(this.internalListener);
    if (paramInt3 > 0)
    {
      this.button3.setText(paramInt3);
      this.button3.setOnClickListener(this.internalListener);
      this.button3.setVisibility(0);
    }
    if (paramInt4 > 0)
    {
      this.button4.setText(paramInt4);
      this.button4.setOnClickListener(this.internalListener);
      this.button4.setVisibility(0);
    }
  }

  public void setSelectedButton(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return;
    case 2131165784:
      this.button1.setSelected(true);
      this.button2.setSelected(false);
      this.button3.setSelected(false);
      this.button4.setSelected(false);
      return;
    case 2131165785:
      this.button1.setSelected(false);
      this.button2.setSelected(true);
      this.button3.setSelected(false);
      this.button4.setSelected(false);
      return;
    case 2131165786:
      this.button1.setSelected(false);
      this.button2.setSelected(false);
      this.button3.setSelected(true);
      this.button4.setSelected(false);
      return;
    case 2131165787:
    }
    this.button1.setSelected(false);
    this.button2.setSelected(false);
    this.button3.setSelected(false);
    this.button4.setSelected(true);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.SubNavBar
 * JD-Core Version:    0.6.2
 */