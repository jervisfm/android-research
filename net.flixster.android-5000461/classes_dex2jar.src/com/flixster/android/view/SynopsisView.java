package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.flixster.android.utils.StringHelper;

public class SynopsisView extends TextView
  implements View.OnClickListener
{
  public static final int SYNOPSIS_LONG_LENGTH = 170;
  public static final int SYNOPSIS_SHORT_LENGTH = 70;
  private boolean showLabel;
  private String synopsis;

  public SynopsisView(Context paramContext)
  {
    super(paramContext);
    initialize();
  }

  public SynopsisView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initialize();
  }

  public SynopsisView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initialize();
  }

  private void initialize()
  {
    setOnClickListener(this);
  }

  protected static Spannable setBoldSpan(Spannable paramSpannable, int paramInt1, int paramInt2)
  {
    paramSpannable.setSpan(new StyleSpan(1), paramInt1, paramInt2, 17);
    return paramSpannable;
  }

  protected static Spannable setColorSpan(Spannable paramSpannable, int paramInt1, int paramInt2, int paramInt3)
  {
    paramSpannable.setSpan(new ForegroundColorSpan(paramInt3), paramInt1, paramInt2, 17);
    return paramSpannable;
  }

  public void load(String paramString)
  {
    load(paramString, 70, false);
  }

  public void load(String paramString, int paramInt, boolean paramBoolean)
  {
    this.showLabel = paramBoolean;
    this.synopsis = paramString;
    if ((paramString == null) || (paramString.length() == 0))
    {
      setVisibility(8);
      return;
    }
    if (paramString.length() <= paramInt)
    {
      onClick(this);
      return;
    }
    String str1 = StringHelper.ellipsize(paramString, paramInt) + " More";
    SpannableString localSpannableString = new SpannableString(str1);
    if (paramBoolean)
    {
      String str2 = getResources().getString(2131492897);
      str1 = str2 + " " + str1;
      localSpannableString = new SpannableString(str1);
      setBoldSpan(localSpannableString, 0, str2.length());
    }
    int i = str1.length();
    setColorSpan(localSpannableString, i - 4, i, getResources().getColor(2131296287));
    setText(localSpannableString);
  }

  public void onClick(View paramView)
  {
    if (this.showLabel)
    {
      String str = getResources().getString(2131492897);
      setText(setBoldSpan(new SpannableString(new StringBuilder(String.valueOf(str)).append(" ").toString() + this.synopsis), 0, str.length()));
      return;
    }
    setText(this.synopsis);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.SynopsisView
 * JD-Core Version:    0.6.2
 */