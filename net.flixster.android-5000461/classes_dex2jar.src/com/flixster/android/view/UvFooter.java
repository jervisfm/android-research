package com.flixster.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.TextView;

public class UvFooter extends TextView
{
  private static final float PADDING_10_DP = 10.0F;
  private static final float PADDING_5_DP = 5.0F;
  private final Context context;

  public UvFooter(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    load();
  }

  public UvFooter(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    load();
  }

  public UvFooter(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    load();
  }

  public void load()
  {
    float f = getResources().getDisplayMetrics().density;
    int i = (int)(0.5F + 5.0F * f);
    int j = (int)(0.5F + 10.0F * f);
    setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(2130837979), null, null, null);
    setCompoundDrawablePadding(j);
    setPadding(j, i, i, i);
    setText(Html.fromHtml(getResources().getString(2131493261)));
    setTextAppearance(this.context, 2131558524);
    setMovementMethod(LinkMovementMethod.getInstance());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.UvFooter
 * JD-Core Version:    0.6.2
 */