package com.flixster.android.view;

import android.widget.ImageView;
import com.flixster.android.utils.Logger;

public class ViewTag
{
  public static String get(ImageView paramImageView)
  {
    Object localObject = paramImageView.getTag();
    if ((localObject instanceof String))
      return (String)localObject;
    Logger.w("FlxMain", "ViewTag.get type mismatch: expected String, actual " + localObject);
    return null;
  }

  public static void set(ImageView paramImageView, String paramString)
  {
    paramImageView.setTag(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.ViewTag
 * JD-Core Version:    0.6.2
 */