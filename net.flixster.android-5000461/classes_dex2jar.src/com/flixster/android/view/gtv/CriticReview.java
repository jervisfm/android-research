package com.flixster.android.view.gtv;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.flixster.android.model.Review;

public class CriticReview extends RelativeLayout
{
  private TextView author;
  private final Context context;
  private ImageView icon;
  private TextView review;

  public CriticReview(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public CriticReview(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public CriticReview(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private String getAuthorString(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder("- ");
    if (paramString1 != null)
    {
      localStringBuilder.append(paramString1);
      if (paramString2 != null)
        localStringBuilder.append(", ");
    }
    if (paramString2 != null)
      localStringBuilder.append(paramString2);
    return localStringBuilder.toString();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903073, this);
    this.review = ((TextView)findViewById(2131165282));
    this.author = ((TextView)findViewById(2131165283));
    this.icon = ((ImageView)findViewById(2131165281));
  }

  public void load(Review paramReview)
  {
    this.review.setText(paramReview.comment);
    this.author.setText(getAuthorString(paramReview.name, paramReview.source) + " ");
    if (paramReview.score < 60)
      this.icon.setImageResource(2130837740);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.gtv.CriticReview
 * JD-Core Version:    0.6.2
 */