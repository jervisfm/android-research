package com.flixster.android.view.gtv;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.flixster.android.activity.ImageViewHandler;
import com.flixster.android.utils.BitmapHelper;
import com.flixster.android.utils.StringHelper;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;

public class MovieDetailHeader extends RelativeLayout
{
  private ImageView audienceIcon;
  private LinearLayout audienceLayout;
  private TextView audienceScore;
  private TextView cast;
  private final Context context;
  private TextView mpaaRuntime;
  private ImageView poster;
  private ImageView reflection;
  private TextView synopsis;
  private ImageView tomatometerIcon;
  private LinearLayout tomatometerLayout;
  private TextView tomatometerScore;

  public MovieDetailHeader(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public MovieDetailHeader(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public MovieDetailHeader(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903122, this);
    this.cast = ((TextView)findViewById(2131165476));
    this.mpaaRuntime = ((TextView)findViewById(2131165477));
    this.synopsis = ((TextView)findViewById(2131165296));
    this.tomatometerLayout = ((LinearLayout)findViewById(2131165478));
    this.tomatometerIcon = ((ImageView)findViewById(2131165479));
    this.tomatometerScore = ((TextView)findViewById(2131165480));
    this.audienceLayout = ((LinearLayout)findViewById(2131165481));
    this.audienceIcon = ((ImageView)findViewById(2131165482));
    this.audienceScore = ((TextView)findViewById(2131165483));
    this.poster = ((ImageView)findViewById(2131165474));
    this.reflection = ((ImageView)findViewById(2131165475));
  }

  public void load(Movie paramMovie, LockerRight paramLockerRight)
  {
    this.cast.setText(paramMovie.getCastShort());
    this.mpaaRuntime.setText(paramMovie.getMpaaRuntime());
    int j;
    label179: int k;
    label265: label272: String str2;
    if (paramMovie.getTomatometer() > -1)
    {
      TextView localTextView2 = this.tomatometerScore;
      StringBuilder localStringBuilder3 = new StringBuilder(String.valueOf(paramMovie.getTomatometer())).append(this.context.getString(2131493218)).append(" ");
      Context localContext3 = this.context;
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = StringHelper.formatInt(paramMovie.getNumCriticReviews());
      localTextView2.setText(localContext3.getString(2131493300, arrayOfObject2));
      if (!paramMovie.isFresh())
        this.tomatometerIcon.setImageResource(2130837740);
      int i = paramMovie.getAudienceScore();
      if (i <= 0)
        break label334;
      boolean bool = paramMovie.isReleased();
      TextView localTextView1 = this.audienceScore;
      StringBuilder localStringBuilder1 = new StringBuilder(String.valueOf(i));
      Context localContext1 = this.context;
      if (!bool)
        break label320;
      j = 2131493219;
      StringBuilder localStringBuilder2 = localStringBuilder1.append(localContext1.getString(j)).append(" ");
      Context localContext2 = this.context;
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = StringHelper.formatInt(paramMovie.getNumUserRatings());
      localTextView1.setText(localContext2.getString(2131493301, arrayOfObject1));
      if (bool)
      {
        ImageView localImageView = this.audienceIcon;
        if (!paramMovie.isSpilled())
          break label327;
        k = 2130837742;
        localImageView.setImageResource(k);
      }
      this.synopsis.setText(paramMovie.getSynopsis());
      if (paramLockerRight == null)
        break label391;
      str2 = paramLockerRight.getDetailPoster();
      if (str2 != null)
        break label346;
      this.poster.setImageResource(2130837839);
    }
    label320: label327: label334: label346: ReflectedImageViewHandler localReflectedImageViewHandler1;
    label391: Bitmap localBitmap1;
    do
    {
      ReflectedImageViewHandler localReflectedImageViewHandler2;
      Bitmap localBitmap2;
      do
      {
        return;
        this.tomatometerLayout.setVisibility(8);
        break;
        j = 2131493220;
        break label179;
        k = 2130837737;
        break label265;
        this.audienceLayout.setVisibility(8);
        break label272;
        localReflectedImageViewHandler2 = new ReflectedImageViewHandler(this.poster, str2, this.reflection);
        localBitmap2 = paramLockerRight.getDetailBitmap(localReflectedImageViewHandler2);
      }
      while (localBitmap2 == null);
      localReflectedImageViewHandler2.handleMessage(Message.obtain(null, 0, localBitmap2));
      return;
      String str1 = paramMovie.getDetailPoster();
      if (str1 == null)
      {
        this.poster.setImageResource(2130837839);
        return;
      }
      localReflectedImageViewHandler1 = new ReflectedImageViewHandler(this.poster, str1, this.reflection);
      localBitmap1 = paramMovie.getDetailBitmap(localReflectedImageViewHandler1);
    }
    while (localBitmap1 == null);
    localReflectedImageViewHandler1.handleMessage(Message.obtain(null, 0, localBitmap1));
  }

  private static class ReflectedImageViewHandler extends ImageViewHandler
  {
    private final ImageView reflectionView;

    public ReflectedImageViewHandler(ImageView paramImageView1, String paramString, ImageView paramImageView2)
    {
      super(paramString);
      this.reflectionView = paramImageView2;
    }

    public void handleMessage(Message paramMessage)
    {
      super.handleMessage(paramMessage);
      Bitmap localBitmap = (Bitmap)paramMessage.obj;
      if (localBitmap != null)
        this.reflectionView.setImageBitmap(BitmapHelper.createReflection(localBitmap));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.gtv.MovieDetailHeader
 * JD-Core Version:    0.6.2
 */