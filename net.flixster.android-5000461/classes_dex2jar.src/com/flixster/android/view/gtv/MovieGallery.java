package com.flixster.android.view.gtv;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import java.util.List;
import net.flixster.android.model.LockerRight;
import net.flixster.android.model.Movie;

public class MovieGallery<E> extends RelativeLayout
{
  private OnMovieClickListener<E> callback;
  private final Context context;
  private GridView grid;
  private final AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
    {
      if (MovieGallery.this.callback != null)
        MovieGallery.this.callback.onClick(MovieGallery.this.movies.get(paramAnonymousInt));
    }
  };
  private List<E> movies;

  public MovieGallery(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public MovieGallery(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public MovieGallery(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903124, this);
    this.grid = ((GridView)findViewById(2131165455));
  }

  public int getSelectedItemPosition()
  {
    return this.grid.getSelectedItemPosition();
  }

  public void load(List<E> paramList, OnMovieClickListener<E> paramOnMovieClickListener)
  {
    this.movies = paramList;
    this.callback = paramOnMovieClickListener;
    this.grid.setAdapter(new MovieGalleryAdapter(this.context, paramList));
    this.grid.setOnItemClickListener(this.itemClickListener);
    this.grid.setClickable(true);
  }

  public void setSelectedItem(int paramInt)
  {
    int i = this.grid.getCount();
    if (paramInt >= i)
      paramInt = i - 1;
    this.grid.setSelection(paramInt);
  }

  private static class MovieGalleryAdapter<E> extends BaseAdapter
  {
    private final Context context;
    private final List<E> movies;

    public MovieGalleryAdapter(Context paramContext, List<E> paramList)
    {
      this.context = paramContext;
      this.movies = paramList;
    }

    public int getCount()
    {
      return this.movies.size();
    }

    public Object getItem(int paramInt)
    {
      return this.movies.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if ((paramView == null) || (!(paramView instanceof MovieGalleryItem)))
        paramView = new MovieGalleryItem(this.context);
      Object localObject;
      while (true)
      {
        localObject = this.movies.get(paramInt);
        if (!(localObject instanceof Movie))
          break;
        ((MovieGalleryItem)paramView).load((Movie)localObject);
        return paramView;
        ((MovieGalleryItem)paramView).reset();
      }
      if ((localObject instanceof LockerRight))
      {
        ((MovieGalleryItem)paramView).load((LockerRight)localObject);
        return paramView;
      }
      throw new IllegalStateException();
    }
  }

  public static abstract interface OnMovieClickListener<T>
  {
    public abstract void onClick(T paramT);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.gtv.MovieGallery
 * JD-Core Version:    0.6.2
 */