package com.flixster.android.view.gtv;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class Subheader extends TextView
{
  private final Context context;

  public Subheader(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public Subheader(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public Subheader(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    int i = getResources().getDimensionPixelSize(2131361884);
    int j = getResources().getDimensionPixelSize(2131361885);
    setGravity(16);
    setBackgroundResource(2130837940);
    setPadding(j, i, i, i);
    setShadowLayer(5.0F, 2.0F, 2.0F, 2131296291);
    setTextAppearance(this.context, 2131558513);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, j);
    setLayoutParams(localLayoutParams);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.gtv.Subheader
 * JD-Core Version:    0.6.2
 */