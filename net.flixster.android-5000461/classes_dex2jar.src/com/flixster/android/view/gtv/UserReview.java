package com.flixster.android.view.gtv;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class UserReview extends RelativeLayout
{
  private final Context context;

  public UserReview(Context paramContext)
  {
    super(paramContext);
    this.context = paramContext;
    initialize();
  }

  public UserReview(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.context = paramContext;
    initialize();
  }

  public UserReview(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.context = paramContext;
    initialize();
  }

  private void initialize()
  {
    View.inflate(this.context, 2130903182, this);
  }

  public void load()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.view.gtv.UserReview
 * JD-Core Version:    0.6.2
 */