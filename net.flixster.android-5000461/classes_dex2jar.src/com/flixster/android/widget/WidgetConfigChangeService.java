package com.flixster.android.widget;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.IBinder;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.Properties;
import com.flixster.android.utils.WorkerThreads;

public class WidgetConfigChangeService extends Service
{
  private int broadcastDelay;
  private final Runnable delayedBroadcaster = new Runnable()
  {
    public void run()
    {
      try
      {
        Thread.sleep(WidgetConfigChangeService.this.broadcastDelay);
        label11: Intent localIntent = new Intent(WidgetConfigChangeService.this, WidgetProvider.class);
        localIntent.setAction("flixster.intent.action.widget");
        localIntent.putExtra("KEY_REFRESH", true);
        WidgetConfigChangeService.this.sendBroadcast(localIntent);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        break label11;
      }
    }
  };
  private int oldOrientation;

  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    Logger.i("FlxWdgt", "WidgetConfigChangeService.onStart.onConfigurationChanged");
    if (paramConfiguration.orientation != this.oldOrientation)
    {
      Logger.i("FlxWdgt", "WidgetConfigChangeService.onStart.onConfigurationChanged " + this.oldOrientation + "->" + paramConfiguration.orientation);
      this.oldOrientation = paramConfiguration.orientation;
      WorkerThreads.instance().invokeLater(this.delayedBroadcaster);
    }
  }

  public void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
    this.oldOrientation = getResources().getConfiguration().orientation;
    Logger.i("FlxWdgt", "WidgetConfigChangeService.onStart " + this.oldOrientation);
    Properties.instance().identifyHoneycombTablet(this);
    Properties.instance().identifyGoogleTv(this);
    if (Properties.instance().isHoneycombTablet());
    for (int i = 2500; ; i = 1250)
    {
      this.broadcastDelay = i;
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.widget.WidgetConfigChangeService
 * JD-Core Version:    0.6.2
 */