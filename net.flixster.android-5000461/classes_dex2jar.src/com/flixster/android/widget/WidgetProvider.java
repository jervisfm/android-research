package com.flixster.android.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.RemoteViews;
import com.flixster.android.bootstrap.BootstrapActivity;
import com.flixster.android.utils.ImageGetter;
import com.flixster.android.utils.Logger;
import com.flixster.android.utils.MathHelper;
import com.flixster.android.utils.WorkerThreads;
import java.util.ArrayList;
import java.util.List;
import net.flixster.android.FlixsterApplication;
import net.flixster.android.data.DaoException;
import net.flixster.android.data.DaoException.Type;
import net.flixster.android.data.MovieDao;
import net.flixster.android.model.Movie;

public class WidgetProvider extends AppWidgetProvider
{
  protected static final String ACTION_WIDGET = "flixster.intent.action.widget";
  private static final String KEY_INDEX = "KEY_INDEX";
  protected static final String KEY_REFRESH = "KEY_REFRESH";
  private static final String KEY_TRACK_EVENT = "KEY_TRACK_EVENT";
  private static final String KEY_UP = "KEY_UP";
  private static final int NUM_WIDGET_MOVIES = 10;
  private static final List<Movie> moviesForWidget = new ArrayList();
  private int[] appWidgetIds;
  private AppWidgetManager appWidgetManager;
  private final Runnable boxOfficeMoviesFetcher = new Runnable()
  {
    public void run()
    {
      Logger.d("FlxWdgt", "WidgetProvider.boxOfficeMoviesFetcher");
      try
      {
        MovieDao.fetchBoxOfficeForWidget(WidgetProvider.moviesForWidget, 10, true);
        WidgetProvider.this.successHandler.sendEmptyMessage(0);
        return;
      }
      catch (DaoException localDaoException)
      {
        Logger.e("FlxWdgt", "WidgetProvider.boxOfficeMoviesFetcher DaoException", localDaoException);
      }
      try
      {
        Thread.sleep(500L);
        label44: WidgetProvider.this.errorHandler.sendMessage(Message.obtain(null, 0, localDaoException));
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        break label44;
      }
    }
  };
  private Context context;
  private int currIndex;
  private final Handler errorHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      DaoException localDaoException = (DaoException)paramAnonymousMessage.obj;
      if ((!FlixsterApplication.isConnected()) || (DaoException.Type.NETWORK == localDaoException.getType()));
      for (String str = WidgetProvider.this.context.getResources().getString(2131493224); ; str = WidgetProvider.this.context.getResources().getString(2131493225))
      {
        RemoteViews localRemoteViews = WidgetProvider.createErrorMessageViews(WidgetProvider.this.context, str);
        WidgetProvider.updateWidgets(WidgetProvider.this.appWidgetManager, WidgetProvider.this.appWidgetIds, localRemoteViews);
        return;
      }
    }
  };
  private Boolean isDirectionUp;
  private final Handler posterHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      RemoteViews localRemoteViews = new RemoteViews(WidgetProvider.this.context.getPackageName(), 2130903188);
      localRemoteViews.setImageViewBitmap(2131165932, (Bitmap)paramAnonymousMessage.obj);
      WidgetProvider.updateWidgets(WidgetProvider.this.appWidgetManager, WidgetProvider.this.appWidgetIds, localRemoteViews);
    }
  };
  private final Handler successHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      Movie localMovie = WidgetProvider.this.getMovie();
      if (localMovie != null)
      {
        WidgetProvider.this.updateWidgets(localMovie);
        String str = localMovie.getProfilePoster();
        if (str != null)
          ImageGetter.instance().get(str, WidgetProvider.this.posterHandler);
      }
    }
  };

  private static PendingIntent createAppIntent(Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, BootstrapActivity.class);
    localIntent.putExtra("KEY_TRACK_EVENT", true);
    return PendingIntent.getActivity(paramContext, 0, localIntent, 0);
  }

  private static RemoteViews createErrorMessageViews(Context paramContext, String paramString)
  {
    RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903188);
    toggleLoadingVisibility(localRemoteViews, true);
    localRemoteViews.setTextViewText(2131165934, paramString);
    localRemoteViews.setOnClickPendingIntent(2131165934, createSelfRefreshIntent(paramContext));
    return localRemoteViews;
  }

  private static RemoteViews createLoadingViews(Context paramContext)
  {
    RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903188);
    toggleLoadingVisibility(localRemoteViews, true);
    localRemoteViews.setTextViewText(2131165934, paramContext.getResources().getString(2131493173));
    return localRemoteViews;
  }

  private static PendingIntent createMovieDetailIntent(Context paramContext, long paramLong)
  {
    Intent localIntent = new Intent(paramContext, BootstrapActivity.class);
    localIntent.setData(Uri.parse("http://internal-deeplink.flixster.com?movie=" + paramLong));
    localIntent.putExtra("KEY_TRACK_EVENT", true);
    return PendingIntent.getActivity(paramContext, 0, localIntent, 0);
  }

  private static RemoteViews createMovieViews(Context paramContext, Movie paramMovie, int paramInt)
  {
    RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903188);
    toggleLoadingVisibility(localRemoteViews, false);
    localRemoteViews.setOnClickPendingIntent(2131165933, createMovieDetailIntent(paramContext, paramMovie.getId()));
    localRemoteViews.setOnClickPendingIntent(2131165931, createMovieDetailIntent(paramContext, paramMovie.getId()));
    localRemoteViews.setOnClickPendingIntent(2131165929, createAppIntent(paramContext));
    localRemoteViews.setOnClickPendingIntent(2131165930, createSearchIntent(paramContext));
    localRemoteViews.setOnClickPendingIntent(2131165942, createSelfBrowseIntent(paramContext, paramInt, true));
    localRemoteViews.setOnClickPendingIntent(2131165943, createSelfBrowseIntent(paramContext, paramInt, false));
    localRemoteViews.setTextViewText(2131165935, paramMovie.getTitle());
    int i = paramMovie.getTomatometer();
    if (i > 0)
    {
      localRemoteViews.setTextViewText(2131165937, String.valueOf(i) + "%");
      localRemoteViews.setImageViewBitmap(2131165936, decodeBitmap(paramContext, paramMovie.getTomatometerIconId()));
      int j = paramMovie.getAudienceScore();
      if (j <= 0)
        break label277;
      localRemoteViews.setTextViewText(2131165939, String.valueOf(j) + "%");
      localRemoteViews.setImageViewBitmap(2131165938, decodeBitmap(paramContext, paramMovie.getAudienceScoreIconId()));
    }
    while (true)
    {
      if (paramMovie.getProfilePoster() == null)
        localRemoteViews.setImageViewBitmap(2131165932, decodeBitmap(paramContext, 2130837844));
      String str = paramMovie.getFriendStat(paramContext.getResources());
      if (str == null)
        break label298;
      localRemoteViews.setTextViewText(2131165940, str);
      return localRemoteViews;
      localRemoteViews.setViewVisibility(2131165937, 8);
      localRemoteViews.setViewVisibility(2131165936, 8);
      break;
      label277: localRemoteViews.setViewVisibility(2131165939, 8);
      localRemoteViews.setViewVisibility(2131165938, 8);
    }
    label298: localRemoteViews.setViewVisibility(2131165940, 8);
    return localRemoteViews;
  }

  private static PendingIntent createSearchIntent(Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, BootstrapActivity.class);
    localIntent.setData(Uri.parse("http://internal-deeplink.flixster.com?search=1"));
    localIntent.putExtra("KEY_TRACK_EVENT", true);
    return PendingIntent.getActivity(paramContext, 0, localIntent, 0);
  }

  private static PendingIntent createSelfBrowseIntent(Context paramContext, int paramInt, boolean paramBoolean)
  {
    Intent localIntent = new Intent(paramContext, WidgetProvider.class);
    localIntent.setAction("flixster.intent.action.widget");
    localIntent.putExtra("KEY_INDEX", paramInt);
    localIntent.putExtra("KEY_UP", paramBoolean);
    return PendingIntent.getBroadcast(paramContext, (int)MathHelper.randomId(), localIntent, 0);
  }

  private static PendingIntent createSelfRefreshIntent(Context paramContext)
  {
    Intent localIntent = new Intent(paramContext, WidgetProvider.class);
    localIntent.setAction("flixster.intent.action.widget");
    localIntent.putExtra("KEY_REFRESH", true);
    return PendingIntent.getBroadcast(paramContext, (int)MathHelper.randomId(), localIntent, 0);
  }

  private static Bitmap decodeBitmap(Context paramContext, int paramInt)
  {
    return BitmapFactory.decodeResource(paramContext.getResources(), paramInt);
  }

  private Movie getMovie()
  {
    StringBuilder localStringBuilder = new StringBuilder("WidgetProvider.getMovie ").append(this.currIndex).append(" ");
    String str;
    if (this.isDirectionUp == null)
    {
      str = "null";
      Logger.d("FlxWdgt", str);
    }
    while (true)
    {
      int i;
      synchronized (moviesForWidget)
      {
        i = moviesForWidget.size();
        if (i == 0)
        {
          Logger.w("FlxWdgt", "WidgetProvider.getMovie list empty");
          return null;
          if (this.isDirectionUp.booleanValue())
          {
            str = "up";
            break;
          }
          str = "down";
          break;
        }
        if (1 + this.currIndex > i)
        {
          j = 0;
          this.currIndex = j;
          if (this.isDirectionUp != null)
          {
            if (!this.isDirectionUp.booleanValue())
              break label209;
            int m = -1 + this.currIndex;
            this.currIndex = m;
            if (m < 0)
              this.currIndex = (i - 1);
          }
          Movie localMovie = (Movie)moviesForWidget.get(this.currIndex);
          return localMovie;
        }
      }
      int j = this.currIndex;
      continue;
      label209: int k = 1 + this.currIndex;
      this.currIndex = k;
      if (k + 1 > i)
        this.currIndex = 0;
    }
  }

  public static boolean isOriginatedFromWidget(Intent paramIntent)
  {
    boolean bool = paramIntent.getBooleanExtra("KEY_TRACK_EVENT", false);
    if (bool)
      paramIntent.removeExtra("KEY_TRACK_EVENT");
    return bool;
  }

  public static void propagateIntentExtras(Intent paramIntent1, Intent paramIntent2)
  {
    Bundle localBundle = paramIntent1.getExtras();
    if (localBundle != null)
    {
      paramIntent2.putExtras(localBundle);
      isOriginatedFromWidget(paramIntent1);
    }
  }

  private static void toggleLoadingVisibility(RemoteViews paramRemoteViews, boolean paramBoolean)
  {
    int i = 8;
    int j;
    if (paramBoolean)
    {
      j = 0;
      if (!paramBoolean)
        break label93;
    }
    while (true)
    {
      paramRemoteViews.setViewVisibility(2131165934, j);
      paramRemoteViews.setViewVisibility(2131165942, i);
      paramRemoteViews.setViewVisibility(2131165943, i);
      paramRemoteViews.setViewVisibility(2131165932, i);
      paramRemoteViews.setViewVisibility(2131165935, i);
      paramRemoteViews.setViewVisibility(2131165940, i);
      paramRemoteViews.setViewVisibility(2131165937, i);
      paramRemoteViews.setViewVisibility(2131165936, i);
      paramRemoteViews.setViewVisibility(2131165939, i);
      paramRemoteViews.setViewVisibility(2131165938, i);
      return;
      j = i;
      break;
      label93: i = 0;
    }
  }

  private static void updateWidgets(AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt, RemoteViews paramRemoteViews)
  {
    int i = paramArrayOfInt.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      paramAppWidgetManager.updateAppWidget(paramArrayOfInt[j], paramRemoteViews);
    }
  }

  private void updateWidgets(Movie paramMovie)
  {
    Logger.d("FlxWdgt", "WidgetProvider.updateWidgets");
    RemoteViews localRemoteViews = createMovieViews(this.context, paramMovie, this.currIndex);
    if (localRemoteViews == null)
      return;
    updateWidgets(this.appWidgetManager, this.appWidgetIds, localRemoteViews);
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    super.onReceive(paramContext, paramIntent);
    String str = paramIntent.getAction();
    Logger.d("FlxWdgt", "WidgetProvider.onReceive " + str);
    if (!"flixster.intent.action.widget".equals(str))
      return;
    ComponentName localComponentName = new ComponentName(paramContext, WidgetProvider.class);
    this.context = paramContext;
    this.appWidgetManager = AppWidgetManager.getInstance(paramContext);
    this.appWidgetIds = this.appWidgetManager.getAppWidgetIds(localComponentName);
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle.getBoolean("KEY_REFRESH"))
    {
      onUpdate(paramContext, this.appWidgetManager, this.appWidgetIds);
      return;
    }
    this.currIndex = localBundle.getInt("KEY_INDEX");
    this.isDirectionUp = Boolean.valueOf(localBundle.getBoolean("KEY_UP"));
    if (moviesForWidget.isEmpty())
    {
      onUpdate(paramContext, this.appWidgetManager, this.appWidgetIds);
      return;
    }
    this.successHandler.handleMessage(null);
  }

  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    Logger.d("FlxWdgt", "WidgetProvider.onUpdate");
    this.context = paramContext;
    this.appWidgetManager = paramAppWidgetManager;
    this.appWidgetIds = paramArrayOfInt;
    updateWidgets(paramAppWidgetManager, paramArrayOfInt, createLoadingViews(paramContext));
    synchronized (moviesForWidget)
    {
      moviesForWidget.clear();
      WorkerThreads.instance().invokeLater(this.boxOfficeMoviesFetcher);
      paramContext.startService(new Intent(paramContext, WidgetConfigChangeService.class));
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.flixster.android.widget.WidgetProvider
 * JD-Core Version:    0.6.2
 */