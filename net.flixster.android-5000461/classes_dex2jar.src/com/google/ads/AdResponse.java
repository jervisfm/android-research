package com.google.ads;

import java.util.Map;

abstract interface AdResponse
{
  public abstract void run(Map<String, String> paramMap, GoogleAdView paramGoogleAdView);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdResponse
 * JD-Core Version:    0.6.2
 */