package com.google.ads;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdSenseSpec
  implements AdSpec
{
  private static final String APP_URL_MODEL_WITH_SEPARATORS = ".android.";
  private static final String JS_PARAM = "afma-sdk-a-v3.1";
  private static final String PLATFORM = "Android";
  private boolean mAdTestEnabled;
  private AdType mAdType;
  private String mAlternateAdUrl;
  private String mAlternateColor;
  private String mAppName;
  private String mChannel;
  private String mClientId;
  private String mColorBackground;
  private String mColorBorder;
  private String mColorLink;
  private String mColorText;
  private String mColorUrl;
  private String mCompanyName;
  private ExpandDirection mExpandDirection;
  private AdFormat mFormat;
  private String mKeywords;
  private String mQuery;
  private String mWebEquivalentUrl;

  public AdSenseSpec(String paramString)
  {
    checkNotNullOrEmpty(paramString, "ClientId");
    this.mClientId = paramString;
    this.mAdTestEnabled = true;
    this.mFormat = AdFormat.FORMAT_320x50;
  }

  private void checkNotEmpty(String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString1.length() <= 0))
      throw new IllegalArgumentException(paramString2 + " cannot be empty.");
  }

  private void checkNotNullOrEmpty(String paramString1, String paramString2)
  {
    if (paramString1 == null)
      throw new NullPointerException(paramString2 + " cannot be null.");
    if (paramString1.length() <= 0)
      throw new IllegalArgumentException(paramString2 + " cannot be empty.");
  }

  private String generateAppUrl(Context paramContext)
  {
    String str1 = paramContext.getPackageName();
    try
    {
      int i = paramContext.getPackageManager().getPackageInfo(str1, 0).versionCode;
      String str2 = i + ".android." + str1;
      return str2;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return ".android." + str1;
  }

  public List<AdSpec.Parameter> generateParameters(Context paramContext)
  {
    if (this.mAppName == null)
      throw new IllegalStateException("AppName must be set before calling generateParameters().");
    if (this.mCompanyName == null)
      throw new IllegalStateException("CompanyName must be set before calling generateParameters().");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new AdSpec.Parameter("client", this.mClientId));
    localArrayList.add(new AdSpec.Parameter("app_name", generateAppUrl(paramContext)));
    localArrayList.add(new AdSpec.Parameter("msid", paramContext.getPackageName()));
    localArrayList.add(new AdSpec.Parameter("js", "afma-sdk-a-v3.1"));
    localArrayList.add(new AdSpec.Parameter("platform", "Android"));
    localArrayList.add(new AdSpec.Parameter("an", this.mAppName));
    localArrayList.add(new AdSpec.Parameter("cn", this.mCompanyName));
    localArrayList.add(new AdSpec.Parameter("hl", Locale.getDefault().getLanguage().trim().toLowerCase()));
    if (!this.mAdTestEnabled)
      localArrayList.add(new AdSpec.Parameter("adtest", "off"));
    if (this.mAdType != null)
      localArrayList.add(new AdSpec.Parameter("ad_type", this.mAdType.getTypeName()));
    if (this.mAlternateAdUrl != null)
      localArrayList.add(new AdSpec.Parameter("alternate_ad_url", this.mAlternateAdUrl));
    if (this.mAlternateColor != null)
      localArrayList.add(new AdSpec.Parameter("alt_color", this.mAlternateColor));
    if (this.mChannel != null)
      localArrayList.add(new AdSpec.Parameter("channel", this.mChannel));
    if (this.mColorBackground != null)
      localArrayList.add(new AdSpec.Parameter("color_bg", this.mColorBackground));
    if (this.mColorBorder != null)
      localArrayList.add(new AdSpec.Parameter("color_border", this.mColorBorder));
    if (this.mColorLink != null)
      localArrayList.add(new AdSpec.Parameter("color_link", this.mColorLink));
    if (this.mColorText != null)
      localArrayList.add(new AdSpec.Parameter("color_text", this.mColorText));
    if (this.mColorUrl != null)
      localArrayList.add(new AdSpec.Parameter("color_url", this.mColorUrl));
    if (this.mFormat != null)
      localArrayList.add(new AdSpec.Parameter("format", this.mFormat.getFormatString()));
    if (this.mKeywords != null)
      localArrayList.add(new AdSpec.Parameter("kw", this.mKeywords));
    if (this.mQuery != null)
      localArrayList.add(new AdSpec.Parameter("q", this.mQuery));
    if (this.mWebEquivalentUrl != null)
      localArrayList.add(new AdSpec.Parameter("url", this.mWebEquivalentUrl));
    if (this.mExpandDirection != null)
      localArrayList.add(new AdSpec.Parameter("xdir", this.mExpandDirection.getFormatString()));
    return localArrayList;
  }

  public AdFormat getAdFormat()
  {
    if (this.mFormat == null)
      return AdFormat.FORMAT_320x50;
    return this.mFormat;
  }

  public boolean getAdTestEnabled()
  {
    return this.mAdTestEnabled;
  }

  public AdType getAdType()
  {
    return this.mAdType;
  }

  public String getAdUrl()
  {
    if ((this.mQuery == null) || (this.mQuery.length() == 0))
      return "http://pagead2.googlesyndication.com/pagead/afma_load_ads.js";
    return "http://www.gstatic.com/mobile/ads/safma_load_ads.js";
  }

  public String getAlternateAdUrl()
  {
    return this.mAlternateAdUrl;
  }

  public String getAlternateColor()
  {
    return this.mAlternateColor;
  }

  public String getAppName()
  {
    return this.mAppName;
  }

  public String getChannel()
  {
    return this.mChannel;
  }

  public String getClientId()
  {
    return this.mClientId;
  }

  public String getColorBackground()
  {
    return this.mColorBackground;
  }

  public String getColorBorder()
  {
    return this.mColorBorder;
  }

  public String getColorLink()
  {
    return this.mColorLink;
  }

  public String getColorText()
  {
    return this.mColorText;
  }

  public String getColorUrl()
  {
    return this.mColorUrl;
  }

  public String getCompanyName()
  {
    return this.mCompanyName;
  }

  public boolean getDebugMode()
  {
    return this.mAdTestEnabled;
  }

  public ExpandDirection getExpandDirection()
  {
    return this.mExpandDirection;
  }

  public int getHeight()
  {
    return getAdFormat().getHeight();
  }

  public String getKeywords()
  {
    return this.mKeywords;
  }

  public String getQuery()
  {
    return this.mQuery;
  }

  public String getWebEquivalentUrl()
  {
    return this.mWebEquivalentUrl;
  }

  public int getWidth()
  {
    return getAdFormat().getWidth();
  }

  public AdSenseSpec setAdFormat(AdFormat paramAdFormat)
  {
    this.mFormat = paramAdFormat;
    return this;
  }

  public AdSenseSpec setAdTestEnabled(boolean paramBoolean)
  {
    this.mAdTestEnabled = paramBoolean;
    return this;
  }

  public AdSenseSpec setAdType(AdType paramAdType)
  {
    this.mAdType = paramAdType;
    return this;
  }

  public AdSenseSpec setAlternateAdUrl(String paramString)
  {
    checkNotEmpty(paramString, "AlternateAdUrl");
    this.mAlternateAdUrl = paramString;
    return this;
  }

  public AdSenseSpec setAlternateColor(String paramString)
  {
    checkNotEmpty(paramString, "AlternateColor");
    this.mAlternateColor = paramString;
    return this;
  }

  public AdSenseSpec setAppName(String paramString)
  {
    checkNotNullOrEmpty(paramString, "AppName");
    this.mAppName = paramString;
    return this;
  }

  public AdSenseSpec setChannel(String paramString)
  {
    checkNotEmpty(paramString, "Channel");
    this.mChannel = paramString;
    return this;
  }

  public AdSenseSpec setClientId(String paramString)
  {
    checkNotNullOrEmpty(paramString, "ClientId");
    this.mClientId = paramString;
    return this;
  }

  public AdSenseSpec setColorBackground(String paramString)
  {
    checkNotEmpty(paramString, "ColorBackground");
    this.mColorBackground = paramString;
    return this;
  }

  public AdSenseSpec setColorBorder(String paramString)
  {
    checkNotEmpty(paramString, "ColorBorder");
    this.mColorBorder = paramString;
    return this;
  }

  public AdSenseSpec setColorLink(String paramString)
  {
    checkNotEmpty(paramString, "ColorLink");
    this.mColorLink = paramString;
    return this;
  }

  public AdSenseSpec setColorText(String paramString)
  {
    checkNotEmpty(paramString, "ColorText");
    this.mColorText = paramString;
    return this;
  }

  public AdSenseSpec setColorUrl(String paramString)
  {
    checkNotEmpty(paramString, "ColorUrl");
    this.mColorUrl = paramString;
    return this;
  }

  public AdSenseSpec setCompanyName(String paramString)
  {
    checkNotNullOrEmpty(paramString, "CompanyName");
    this.mCompanyName = paramString;
    return this;
  }

  public AdSenseSpec setExpandDirection(ExpandDirection paramExpandDirection)
  {
    this.mExpandDirection = paramExpandDirection;
    return this;
  }

  public AdSenseSpec setKeywords(String paramString)
  {
    checkNotEmpty(paramString, "Keywords");
    this.mKeywords = paramString;
    return this;
  }

  public AdSenseSpec setQuery(String paramString)
  {
    this.mQuery = paramString;
    return this;
  }

  public AdSenseSpec setWebEquivalentUrl(String paramString)
  {
    checkNotEmpty(paramString, "WebEquivalentUrl");
    this.mWebEquivalentUrl = paramString;
    return this;
  }

  public static enum AdFormat
  {
    private String mFormatString;
    private int mHeight;
    private int mWidth;

    static
    {
      FORMAT_300x250 = new AdFormat("FORMAT_300x250", 1, "300x250_as", 300, 250);
      AdFormat[] arrayOfAdFormat = new AdFormat[2];
      arrayOfAdFormat[0] = FORMAT_320x50;
      arrayOfAdFormat[1] = FORMAT_300x250;
    }

    private AdFormat(String paramString, int paramInt1, int paramInt2)
    {
      this.mFormatString = paramString;
      this.mWidth = paramInt1;
      this.mHeight = paramInt2;
    }

    public String getFormatString()
    {
      return this.mFormatString;
    }

    public int getHeight()
    {
      return this.mHeight;
    }

    public int getWidth()
    {
      return this.mWidth;
    }
  }

  public static enum AdType
  {
    private String mTypeName;

    static
    {
      IMAGE = new AdType("IMAGE", 1, "image");
      TEXT_IMAGE = new AdType("TEXT_IMAGE", 2, "text_image");
      AdType[] arrayOfAdType = new AdType[3];
      arrayOfAdType[0] = TEXT;
      arrayOfAdType[1] = IMAGE;
      arrayOfAdType[2] = TEXT_IMAGE;
    }

    private AdType(String paramString)
    {
      this.mTypeName = paramString;
    }

    private String getTypeName()
    {
      return this.mTypeName;
    }
  }

  public static enum ExpandDirection
  {
    private String mFormatString;

    static
    {
      BOTTOM = new ExpandDirection("BOTTOM", 1, "b");
      ExpandDirection[] arrayOfExpandDirection = new ExpandDirection[2];
      arrayOfExpandDirection[0] = TOP;
      arrayOfExpandDirection[1] = BOTTOM;
    }

    private ExpandDirection(String paramString)
    {
      this.mFormatString = paramString;
    }

    String getFormatString()
    {
      return this.mFormatString;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdSenseSpec
 * JD-Core Version:    0.6.2
 */