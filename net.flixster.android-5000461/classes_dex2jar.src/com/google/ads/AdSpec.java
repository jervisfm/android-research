package com.google.ads;

import android.content.Context;
import java.util.List;

public abstract interface AdSpec
{
  public static final String CONTENT_AD_URL = "http://pagead2.googlesyndication.com/pagead/afma_load_ads.js";
  public static final String SEARCH_AD_URL = "http://www.gstatic.com/mobile/ads/safma_load_ads.js";

  public abstract List<Parameter> generateParameters(Context paramContext);

  public abstract String getAdUrl();

  public abstract boolean getDebugMode();

  public abstract int getHeight();

  public abstract int getWidth();

  public static class Parameter
  {
    private final String mName;
    private final String mValue;

    public Parameter(String paramString1, String paramString2)
    {
      if (paramString1 == null)
        throw new NullPointerException("Parameter name cannot be null.");
      if (paramString2 == null)
        throw new NullPointerException("Parameter value cannot be null.");
      this.mName = paramString1;
      this.mValue = paramString2;
    }

    public boolean equals(Object paramObject)
    {
      if (this == paramObject);
      Parameter localParameter;
      do
      {
        return true;
        if (!(paramObject instanceof Parameter))
          return false;
        localParameter = (Parameter)paramObject;
      }
      while ((this.mName.equals(localParameter.mName)) && (this.mValue.equals(localParameter.mValue)));
      return false;
    }

    public String getName()
    {
      return this.mName;
    }

    public String getValue()
    {
      return this.mValue;
    }

    public int hashCode()
    {
      return 4999 * this.mName.hashCode() + this.mValue.hashCode();
    }

    public String toString()
    {
      return "Parameter(" + this.mName + "," + this.mValue + ")";
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdSpec
 * JD-Core Version:    0.6.2
 */