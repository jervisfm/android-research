package com.google.ads;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

final class AdUtil
{
  private static final String LOGTAG = "Google.AdUtil";
  private static float density = -1.0F;

  static String generateJSONParameters(List<AdSpec.Parameter> paramList)
  {
    Object localObject = new JSONObject();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      AdSpec.Parameter localParameter = (AdSpec.Parameter)localIterator.next();
      String str1 = localParameter.getName();
      String str2 = localParameter.getValue();
      try
      {
        JSONObject localJSONObject = ((JSONObject)localObject).put(str1, str2);
        localObject = localJSONObject;
      }
      catch (JSONException localJSONException)
      {
        Log.w("Google.AdUtil", "Error encoding JSON: " + str1 + "=" + str2);
      }
    }
    return ((JSONObject)localObject).toString();
  }

  static int scaleDipsToPixels(Context paramContext, int paramInt)
  {
    if (density < 0.0F)
      density = paramContext.getResources().getDisplayMetrics().density;
    return (int)(paramInt * density);
  }

  static int scalePixelsToDips(Context paramContext, int paramInt)
  {
    if (density < 0.0F)
      density = paramContext.getResources().getDisplayMetrics().density;
    return (int)(paramInt / density);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdUtil
 * JD-Core Version:    0.6.2
 */