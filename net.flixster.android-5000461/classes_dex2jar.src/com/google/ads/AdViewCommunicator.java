package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class AdViewCommunicator
{
  private static final String AFMA_MESSAGE_HOST = "afma.google.com";
  private static final String JS_PROTOCOL_PREFIX = "javascript: ";
  public static final String JS_SEND_FUNCTION = "adsense.mobileads.afmanotify.receiveMessage";
  private static final String RESPONSE_SCHEME = "gmsg";
  private Map<String, AdResponse> mAdResponses = new HashMap();
  private GoogleAdView mView;

  public AdViewCommunicator(GoogleAdView paramGoogleAdView)
  {
    this.mView = paramGoogleAdView;
  }

  private static Map<String, String> generateParamMap(Uri paramUri)
  {
    Object localObject;
    if (paramUri == null)
      localObject = null;
    String str1;
    do
    {
      return localObject;
      localObject = new HashMap();
      str1 = paramUri.getEncodedQuery();
    }
    while (str1 == null);
    String[] arrayOfString = str1.split("&");
    for (int i = 0; i < arrayOfString.length; i++)
    {
      int j = arrayOfString[i].indexOf('=');
      if (j == -1)
        return null;
      if (arrayOfString[i].indexOf('=', j + 1) != -1)
        return null;
      String str2 = arrayOfString[i].substring(0, j);
      String str3 = arrayOfString[i].substring(j + 1);
      ((HashMap)localObject).put(URLDecoder.decode(str2), URLDecoder.decode(str3));
    }
    return Collections.unmodifiableMap((Map)localObject);
  }

  public static boolean isMessage(Uri paramUri)
  {
    if ((paramUri == null) || (!paramUri.isHierarchical()) || (paramUri.getScheme() == null) || (!paramUri.getScheme().equals("gmsg")));
    String str;
    do
    {
      return false;
      str = paramUri.getAuthority();
    }
    while ((str == null) || (!str.equals("afma.google.com")));
    return true;
  }

  public static void sendJavaScriptMessage(WebView paramWebView, JsMessageAction paramJsMessageAction, List<AdSpec.Parameter> paramList)
  {
    if ((paramList == null) || (paramWebView == null))
      throw new NullPointerException();
    String str1 = AdUtil.generateJSONParameters(paramList);
    String str2 = "adsense.mobileads.afmanotify.receiveMessage(\"" + paramJsMessageAction.getMessageString() + "\", " + str1 + ");";
    paramWebView.loadUrl("javascript: " + str2);
  }

  public AdResponse registerAdResponse(String paramString, AdResponse paramAdResponse)
  {
    return (AdResponse)this.mAdResponses.put(paramString, paramAdResponse);
  }

  public boolean testAndForwardMessage(Uri paramUri)
  {
    if (!isMessage(paramUri))
      throw new IllegalArgumentException("Invalid syntax in forwarded message: " + paramUri);
    String str = paramUri.getPath();
    AdResponse localAdResponse = (AdResponse)this.mAdResponses.get(str);
    if (localAdResponse == null);
    Map localMap;
    do
    {
      return false;
      localMap = generateParamMap(paramUri);
    }
    while (localMap == null);
    localAdResponse.run(localMap, this.mView);
    return true;
  }

  public static enum JsMessageAction
  {
    private String mMessageString;

    static
    {
      JsMessageAction[] arrayOfJsMessageAction = new JsMessageAction[2];
      arrayOfJsMessageAction[0] = JS_OUTSIDE_CLICK_MESSAGE;
      arrayOfJsMessageAction[1] = JS_REPORT_INSTALL_STATE;
    }

    private JsMessageAction(String paramString)
    {
      this.mMessageString = paramString;
    }

    public String getMessageString()
    {
      return this.mMessageString;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdViewCommunicator
 * JD-Core Version:    0.6.2
 */