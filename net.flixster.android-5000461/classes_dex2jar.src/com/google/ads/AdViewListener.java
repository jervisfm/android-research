package com.google.ads;

public abstract interface AdViewListener
{
  public abstract void onAdFetchFailure();

  public abstract void onClickAd();

  public abstract void onFinishFetchAd();

  public abstract void onStartFetchAd();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AdViewListener
 * JD-Core Version:    0.6.2
 */