package com.google.ads;

public abstract interface AppEventListener
{
  public abstract void onAppEvent(Ad paramAd, String paramString1, String paramString2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.AppEventListener
 * JD-Core Version:    0.6.2
 */