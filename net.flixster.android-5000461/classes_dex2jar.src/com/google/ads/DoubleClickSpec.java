package com.google.ads;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class DoubleClickSpec
  implements AdSpec
{
  private String mColorBackground;
  private boolean mCustomSizeDefined = false;
  private int mHeight = 0;
  private String mKeyname;
  private SizeProfile mSizeProfile;
  private int mWidth = 0;

  public DoubleClickSpec(String paramString)
  {
    this.mKeyname = paramString;
  }

  public List<AdSpec.Parameter> generateParameters(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new AdSpec.Parameter("k", this.mKeyname));
    if (this.mColorBackground != null)
      localArrayList.add(new AdSpec.Parameter("color_bg", this.mColorBackground));
    if (this.mSizeProfile != null)
      localArrayList.add(new AdSpec.Parameter("sp", this.mSizeProfile.name().toLowerCase()));
    return localArrayList;
  }

  public String getAdUrl()
  {
    return "http://pagead2.googlesyndication.com/pagead/afma_load_ads.js";
  }

  public String getColorBackground()
  {
    return this.mColorBackground;
  }

  public boolean getDebugMode()
  {
    return false;
  }

  public int getHeight()
  {
    if (this.mCustomSizeDefined)
      return this.mHeight;
    if (this.mSizeProfile != null)
      return this.mSizeProfile.getHeight();
    return SizeProfile.XL.getHeight();
  }

  public String getKeyname()
  {
    return this.mKeyname;
  }

  public SizeProfile getSizeProfile()
  {
    return this.mSizeProfile;
  }

  public int getWidth()
  {
    if (this.mCustomSizeDefined)
      return this.mWidth;
    if (this.mSizeProfile != null)
      return this.mSizeProfile.getWidth();
    return SizeProfile.XL.getWidth();
  }

  public DoubleClickSpec setColorBackground(String paramString)
  {
    this.mColorBackground = paramString;
    return this;
  }

  public DoubleClickSpec setCustomSize(int paramInt1, int paramInt2)
  {
    if (paramInt1 < SizeProfile.S.getWidth())
      throw new IllegalArgumentException("Illegal width: " + paramInt1);
    if (paramInt2 < SizeProfile.S.getHeight())
      throw new IllegalArgumentException("Illegal height: " + paramInt2);
    this.mCustomSizeDefined = true;
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    return this;
  }

  public DoubleClickSpec setKeyname(String paramString)
  {
    this.mKeyname = paramString;
    return this;
  }

  public DoubleClickSpec setSizeProfile(SizeProfile paramSizeProfile)
  {
    this.mSizeProfile = paramSizeProfile;
    return this;
  }

  public static enum SizeProfile
  {
    private int mHeight;
    private int mWidth;

    static
    {
      S = new SizeProfile("S", 1, 120, 20);
      M = new SizeProfile("M", 2, 168, 28);
      L = new SizeProfile("L", 3, 216, 36);
      XL = new SizeProfile("XL", 4, 320, 50);
      SizeProfile[] arrayOfSizeProfile = new SizeProfile[5];
      arrayOfSizeProfile[0] = T;
      arrayOfSizeProfile[1] = S;
      arrayOfSizeProfile[2] = M;
      arrayOfSizeProfile[3] = L;
      arrayOfSizeProfile[4] = XL;
    }

    private SizeProfile(int paramInt1, int paramInt2)
    {
      this.mWidth = paramInt1;
      this.mHeight = paramInt2;
    }

    public int getHeight()
    {
      return this.mHeight;
    }

    public int getWidth()
    {
      return this.mWidth;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.DoubleClickSpec
 * JD-Core Version:    0.6.2
 */