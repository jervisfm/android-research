package com.google.ads;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import java.util.LinkedList;
import java.util.List;

public class InstalledApplications
{
  private static final int NOT_INSTALLED;
  private final PackageManager mPackageManager;

  public InstalledApplications(PackageManager paramPackageManager)
  {
    this.mPackageManager = paramPackageManager;
  }

  public List<AdSpec.Parameter> getInstallationState()
  {
    LinkedList localLinkedList = new LinkedList();
    for (Application localApplication : Application.values())
      localLinkedList.add(new AdSpec.Parameter(localApplication.getTag(), "" + localApplication.getVersionCode(this.mPackageManager)));
    return localLinkedList;
  }

  private static enum Application
  {
    private String mPackageName;
    private String mTag;

    static
    {
      Application[] arrayOfApplication = new Application[1];
      arrayOfApplication[0] = YOUTUBE;
    }

    private Application(String paramString1, String paramString2)
    {
      this.mTag = paramString1;
      this.mPackageName = paramString2;
    }

    private String getTag()
    {
      return this.mTag;
    }

    private int getVersionCode(PackageManager paramPackageManager)
    {
      try
      {
        PackageInfo localPackageInfo = paramPackageManager.getPackageInfo(this.mPackageName, 0);
        if (localPackageInfo == null)
          return 0;
        int i = localPackageInfo.versionCode;
        return i;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
      }
      return 0;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.InstalledApplications
 * JD-Core Version:    0.6.2
 */