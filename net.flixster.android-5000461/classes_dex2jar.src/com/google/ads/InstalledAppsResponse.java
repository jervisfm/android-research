package com.google.ads;

import java.util.List;
import java.util.Map;

public class InstalledAppsResponse
  implements AdResponse
{
  private final AdViewCommunicator mCommunicator;
  private final InstalledApplications mInstalledApps;

  public InstalledAppsResponse(InstalledApplications paramInstalledApplications, AdViewCommunicator paramAdViewCommunicator)
  {
    this.mInstalledApps = paramInstalledApplications;
    this.mCommunicator = paramAdViewCommunicator;
  }

  public void run(Map<String, String> paramMap, GoogleAdView paramGoogleAdView)
  {
    List localList = this.mInstalledApps.getInstallationState();
    AdViewCommunicator.sendJavaScriptMessage(paramGoogleAdView.getWebView(), AdViewCommunicator.JsMessageAction.JS_REPORT_INSTALL_STATE, localList);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.InstalledAppsResponse
 * JD-Core Version:    0.6.2
 */