package com.google.ads;

import android.util.Log;
import java.util.Map;

class LoadAdResponse
  implements AdResponse
{
  private static final String LOGTAG = "LoadAdResponse";

  public void run(Map<String, String> paramMap, GoogleAdView paramGoogleAdView)
  {
    String str = (String)paramMap.get("src");
    if (str == null)
    {
      Log.e("LoadAdResponse", "ERROR: src parameter not found in loadAdUrl");
      return;
    }
    paramGoogleAdView.loadAdFromUrl(str);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.LoadAdResponse
 * JD-Core Version:    0.6.2
 */