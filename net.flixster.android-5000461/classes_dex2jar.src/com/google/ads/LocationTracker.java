package com.google.ads;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import com.google.ads.util.Base64;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

class LocationTracker
{
  static String PROTO_TEMPLATE = "desc < role: 6 producer: 24 historical_role: 1 historical_producer: 12 timestamp: %d latlng < latitude_e7: %d longitude_e7: %d> radius: %d>";
  private Context mContext;

  LocationTracker(Context paramContext)
  {
    this.mContext = paramContext;
  }

  private List<Location> getLastKnownLocations()
  {
    LocationManager localLocationManager = (LocationManager)this.mContext.getSystemService("location");
    List localList = localLocationManager.getProviders(false);
    ArrayList localArrayList = new ArrayList(localList.size());
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Location localLocation = localLocationManager.getLastKnownLocation((String)localIterator.next());
      if ((localLocation != null) && (localLocation.hasAccuracy()))
        localArrayList.add(localLocation);
    }
    return localArrayList;
  }

  String encodeProto(String paramString)
  {
    try
    {
      Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      localCipher.init(1, new SecretKeySpec(new byte[] { 10, 55, -112, -47, -6, 7, 11, 75, -7, -121, 121, 69, 80, -61, 15, 5 }, "AES"));
      byte[] arrayOfByte1 = localCipher.getIV();
      byte[] arrayOfByte2 = localCipher.doFinal(paramString.getBytes());
      byte[] arrayOfByte3 = new byte[arrayOfByte1.length + arrayOfByte2.length];
      System.arraycopy(arrayOfByte1, 0, arrayOfByte3, 0, arrayOfByte1.length);
      System.arraycopy(arrayOfByte2, 0, arrayOfByte3, arrayOfByte1.length, arrayOfByte2.length);
      String str = Base64.encodeToString(arrayOfByte3, 11);
      return str;
    }
    catch (GeneralSecurityException localGeneralSecurityException)
    {
    }
    return null;
  }

  String getLocationParam()
  {
    List localList = getLastKnownLocations();
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    if (i < localList.size())
    {
      String str = encodeProto(protoFromLocation((Location)localList.get(i)));
      if (str != null)
      {
        if (i != 0)
          break label74;
        localStringBuilder.append("e1+");
      }
      while (true)
      {
        localStringBuilder.append(str);
        i++;
        break;
        label74: localStringBuilder.append("+e1+");
      }
    }
    return localStringBuilder.toString();
  }

  String protoFromLocation(Location paramLocation)
  {
    String str = PROTO_TEMPLATE;
    Object[] arrayOfObject = new Object[4];
    arrayOfObject[0] = Long.valueOf(1000L * paramLocation.getTime());
    arrayOfObject[1] = Long.valueOf(()(10000000.0D * paramLocation.getLatitude()));
    arrayOfObject[2] = Long.valueOf(()(10000000.0D * paramLocation.getLongitude()));
    arrayOfObject[3] = Long.valueOf(()(1000.0F * paramLocation.getAccuracy()));
    return String.format(str, arrayOfObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.LocationTracker
 * JD-Core Version:    0.6.2
 */