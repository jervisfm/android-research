package com.google.ads;

import java.util.Map;

class ResizeResponse
  implements AdResponse
{
  private static boolean validateExpandDirection(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return ((paramInt1 > 0) || (paramInt2 > 0) || (paramInt3 > 0) || (paramInt4 > 0)) && (paramInt1 >= 0) && (paramInt2 >= 0) && (paramInt3 >= 0) && (paramInt4 >= 0);
  }

  public void run(Map<String, String> paramMap, GoogleAdView paramGoogleAdView)
  {
    String str1 = (String)paramMap.get("width");
    String str2 = (String)paramMap.get("height");
    String str3 = (String)paramMap.get("left");
    String str4 = (String)paramMap.get("right");
    String str5 = (String)paramMap.get("top");
    String str6 = (String)paramMap.get("bottom");
    if (str1 != null);
    label98: int k;
    label110: int m;
    label122: int n;
    label134: int i2;
    label210: label216: label222: label228: label237: 
    do
    {
      try
      {
        int i = Integer.parseInt(str1);
        int j;
        int i1;
        if (str1 != null)
        {
          j = Integer.parseInt(str2);
          if (str3 == null)
            break label210;
          k = Integer.parseInt(str3);
          if (str4 == null)
            break label216;
          m = Integer.parseInt(str4);
          if (str5 == null)
            break label222;
          n = Integer.parseInt(str5);
          if (str6 == null)
            break label228;
          i1 = Integer.parseInt(str6);
        }
        for (i2 = i1; ; i2 = 0)
        {
          if ((i == paramGoogleAdView.getAdWidth()) && (j == paramGoogleAdView.getAdHeight()))
            break label237;
          if (paramGoogleAdView.isExpanded())
            paramGoogleAdView.closeAdImmediately();
          paramGoogleAdView.resize(i, j);
          return;
          i = paramGoogleAdView.getAdWidth();
          break;
          int i3 = paramGoogleAdView.getAdHeight();
          j = i3;
          break label98;
          k = 0;
          break label110;
          m = 0;
          break label122;
          n = 0;
          break label134;
        }
      }
      catch (NumberFormatException localNumberFormatException)
      {
        return;
      }
      if ((paramGoogleAdView.isExpanded()) && (k == 0) && (m == 0) && (n == 0) && (i2 == 0))
      {
        paramGoogleAdView.retractAd();
        return;
      }
    }
    while ((paramGoogleAdView.isExpanded()) || (!validateExpandDirection(n, i2, k, m)));
    paramGoogleAdView.expandAd(n, i2, k, m);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.ResizeResponse
 * JD-Core Version:    0.6.2
 */