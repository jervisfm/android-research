package com.google.ads.interactivemedia.a;

import android.view.ViewGroup;
import java.util.List;
import java.util.Map;

public abstract interface a
{
  public abstract void a(c paramc);

  public abstract void a(String paramString);

  public abstract void a(String paramString, int paramInt);

  public abstract void a(String paramString, a parama, ViewGroup paramViewGroup);

  public abstract void a(String paramString, b paramb);

  public abstract void a(String paramString1, String paramString2);

  public abstract void a(String paramString, Map<String, String> paramMap);

  public abstract void b(String paramString);

  public static final class a
  {
    private final int a;
    private final String b;
    private final String c;
    private final String d;

    public a(int paramInt, String paramString1, String paramString2, String paramString3)
    {
      this.a = paramInt;
      this.b = paramString1;
      this.c = paramString2;
      this.d = paramString3;
    }

    public a(String paramString1, String paramString2)
    {
      this(0, null, paramString1, paramString2);
    }

    public final int a()
    {
      return this.a;
    }

    public final String b()
    {
      return this.c;
    }

    public final String c()
    {
      return this.d;
    }

    public final boolean equals(Object paramObject)
    {
      if (this == paramObject);
      a locala;
      do
      {
        do
        {
          return true;
          if (paramObject == null)
            return false;
          if (getClass() != paramObject.getClass())
            return false;
          locala = (a)paramObject;
          if (this.d == null)
          {
            if (locala.d != null)
              return false;
          }
          else if (!this.d.equals(locala.d))
            return false;
          if (this.a != locala.a)
            return false;
          if (this.b == null)
          {
            if (locala.b != null)
              return false;
          }
          else if (!this.b.equals(locala.b))
            return false;
          if (this.c != null)
            break;
        }
        while (locala.c == null);
        return false;
      }
      while (this.c.equals(locala.c));
      return false;
    }

    public final int hashCode()
    {
      int i;
      int k;
      label34: int m;
      int n;
      if (this.d == null)
      {
        i = 0;
        int j = 31 * (31 * (i + 31) + this.a);
        if (this.b != null)
          break label73;
        k = 0;
        m = 31 * (k + j);
        String str = this.c;
        n = 0;
        if (str != null)
          break label84;
      }
      while (true)
      {
        return m + n;
        i = this.d.hashCode();
        break;
        label73: k = this.b.hashCode();
        break label34;
        label84: n = this.c.hashCode();
      }
    }

    public final String toString()
    {
      return "AdData [companionId=" + this.a + ", size=" + this.b + ", srcUrl=" + this.c + ", clickthrough=" + this.d + "]";
    }
  }

  public static abstract interface b
  {
    public abstract void a(String paramString1, String paramString2);
  }

  public static abstract interface c
  {
    public abstract void a(String paramString1, String paramString2, a.a parama, Map<String, List<a.a>> paramMap);

    public abstract void a(String paramString1, String paramString2, String paramString3);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.a
 * JD-Core Version:    0.6.2
 */