package com.google.ads.interactivemedia.a.a;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.ViewGroup;
import com.google.ads.interactivemedia.a.a.a;
import com.google.ads.interactivemedia.a.a.b;
import com.google.ads.interactivemedia.a.a.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class a
  implements com.google.ads.interactivemedia.a.a
{
  private final d a;
  private final Context b;
  private a.c c;
  private final Map<String, a.b> d = new HashMap();
  private volatile boolean e = false;
  private final BlockingQueue<b> f = new ArrayBlockingQueue(100);

  public a(Context paramContext)
  {
    this(paramContext, new d(paramContext), "http://s0.2mdn.net/instream/html5/native/native_sdk.html");
  }

  private a(Context paramContext, d paramd, String paramString)
  {
    this.a = paramd;
    this.b = paramContext;
    paramd.a(new d.a()
    {
      public final void a(b paramAnonymousb)
      {
        a.this.a(paramAnonymousb);
      }
    });
    paramd.a(paramString);
  }

  private static Map<String, List<a.a>> a(JSONObject paramJSONObject)
    throws JSONException
  {
    if (!paramJSONObject.has("companions"))
      return new HashMap(0);
    JSONObject localJSONObject1 = paramJSONObject.getJSONObject("companions");
    HashMap localHashMap = new HashMap();
    Iterator localIterator = localJSONObject1.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      JSONArray localJSONArray = localJSONObject1.getJSONArray(str);
      ArrayList localArrayList = new ArrayList();
      for (int i = 0; i < localJSONArray.length(); i++)
      {
        JSONObject localJSONObject2 = localJSONArray.getJSONObject(i);
        localArrayList.add(new a.a(localJSONObject2.getInt("companionId"), str, localJSONObject2.getString("src"), localJSONObject2.getString("clickThru")));
      }
      localHashMap.put(str, localArrayList);
    }
    return localHashMap;
  }

  private void a()
  {
    while (true)
    {
      b localb = (b)this.f.poll();
      if (localb == null)
        break;
      this.a.a(localb);
    }
  }

  public final void a(a.c paramc)
  {
    this.c = paramc;
  }

  protected final void a(b paramb)
  {
    String str1 = paramb.a();
    Map localMap1 = paramb.b();
    if (str1.equals("error"))
    {
      Map localMap3 = paramb.b();
      String str3 = (String)localMap3.get("type");
      if (str3.equals("adLoadError"))
      {
        this.c.a((String)localMap3.get("userContext"), (String)localMap3.get("code"), (String)localMap3.get("message"));
        return;
      }
      if (str3.equals("adPlayError"))
      {
        a.b localb = (a.b)this.d.get(localMap3.get("adsManagerId"));
        if (localb == null)
        {
          Log.w("IMASDK", "Error for unknown manager: " + paramb);
          return;
        }
        localb.a((String)localMap3.get("code"), (String)localMap3.get("message"));
        return;
      }
      Log.w("IMASDK", "Unknown error: " + paramb);
      return;
    }
    if (str1.equals("log"))
    {
      Log.i("IMASDK", (String)localMap1.get("logMessage"));
      return;
    }
    if (str1.equals("webViewLoaded"))
    {
      Log.i("IMASDK", "SDK loaded");
      this.e = true;
      a();
      return;
    }
    if (str1.equals("video"))
      try
      {
        JSONObject localJSONObject = new JSONObject((String)localMap1.get("responseJSON"));
        String str2 = (String)localMap1.get("adsManagerId");
        Map localMap2 = a(localJSONObject);
        a.a locala = new a.a(localJSONObject.getString("url"), localJSONObject.getString("clickThru"));
        this.c.a(localJSONObject.getString("userContext"), str2, locala, localMap2);
        return;
      }
      catch (Exception localException)
      {
        Log.e("IMASDK", "Error parsing video ad response");
        return;
      }
    Log.w("IMASDK", "Unknown message: " + paramb);
  }

  public final void a(String paramString)
  {
    this.a.a(b.a("unloadAd", new String[] { "adsManagerId", paramString }));
    this.d.remove(paramString);
  }

  public final void a(String paramString, int paramInt)
  {
    d locald = this.a;
    String[] arrayOfString = new String[4];
    arrayOfString[0] = "adsManagerId";
    arrayOfString[1] = paramString;
    arrayOfString[2] = "companionId";
    arrayOfString[3] = Integer.toString(paramInt);
    locald.a(b.a("companionViewEvent", arrayOfString));
  }

  public final void a(String paramString, a.a parama, ViewGroup paramViewGroup)
  {
    paramViewGroup.removeAllViews();
    paramViewGroup.addView(new c(paramViewGroup.getContext(), this, paramString, parama));
  }

  public final void a(String paramString, a.b paramb)
  {
    this.d.put(paramString, paramb);
  }

  public final void a(String paramString1, String paramString2)
  {
    this.a.a(b.a("vastEvent", new String[] { "adsManagerId", paramString1, "eventType", paramString2 }));
  }

  public final void a(String paramString, Map<String, String> paramMap)
  {
    String[] arrayOfString = new String[8];
    arrayOfString[0] = "userContext";
    arrayOfString[1] = paramString;
    arrayOfString[2] = "mimeType";
    arrayOfString[3] = "video/mp4";
    arrayOfString[4] = "network";
    String str;
    if (this.b.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0)
    {
      Log.w("IMASDK", "Host application doesn't have ACCESS_NETWORK_STATE permission");
      str = "android:0";
    }
    while (true)
    {
      arrayOfString[5] = str;
      arrayOfString[6] = "env";
      Object[] arrayOfObject2 = new Object[3];
      arrayOfObject2[0] = Build.VERSION.RELEASE;
      arrayOfObject2[1] = "0.1";
      arrayOfObject2[2] = this.b.getPackageName();
      arrayOfString[7] = String.format("android%s:%s:%s", arrayOfObject2);
      b localb = b.a("requestAd", arrayOfString);
      localb.b().putAll(paramMap);
      this.f.add(localb);
      if (this.e)
        a();
      return;
      NetworkInfo localNetworkInfo = ((ConnectivityManager)this.b.getSystemService("connectivity")).getActiveNetworkInfo();
      if (localNetworkInfo == null)
      {
        str = "android:0";
      }
      else
      {
        Object[] arrayOfObject1 = new Object[2];
        arrayOfObject1[0] = Integer.valueOf(localNetworkInfo.getType());
        arrayOfObject1[1] = Integer.valueOf(localNetworkInfo.getSubtype());
        str = String.format("android:%d:%d", arrayOfObject1);
      }
    }
  }

  public final void b(String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    this.b.startActivity(localIntent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.a.a
 * JD-Core Version:    0.6.2
 */