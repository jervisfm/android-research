package com.google.ads.interactivemedia.a.a;

import android.net.Uri;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class b
{
  private final String a;
  private final Map<String, String> b;

  private b(String paramString, Map<String, String> paramMap)
  {
    this.a = paramString;
    this.b = paramMap;
  }

  public static b a(String paramString)
  {
    Uri localUri = Uri.parse(paramString);
    String str1 = localUri.getPath().substring(1);
    HashMap localHashMap = new HashMap();
    String str2 = localUri.getEncodedQuery();
    if (str2 != null)
      for (String str3 : str2.split("&"))
      {
        int k = str3.indexOf('=');
        if (k != -1)
        {
          String str4 = str3.substring(0, k);
          String str5 = str3.substring(k + 1);
          localHashMap.put(URLDecoder.decode(str4), URLDecoder.decode(str5));
        }
      }
    return new b(str1, localHashMap);
  }

  public static b a(String paramString, String[] paramArrayOfString)
  {
    if (paramArrayOfString.length % 2 != 0)
      throw new IllegalArgumentException("paramKeyValues");
    HashMap localHashMap = new HashMap();
    for (int i = 0; i < paramArrayOfString.length; i += 2)
      localHashMap.put(paramArrayOfString[i], paramArrayOfString[(i + 1)]);
    return new b(paramString, localHashMap);
  }

  public final String a()
  {
    return this.a;
  }

  public final Map<String, String> b()
  {
    return this.b;
  }

  public final String c()
  {
    String str = new JSONObject(this.b).toString();
    return "javascript:adsense.mobileads.afmanotify.receiveMessage('" + this.a + "', " + str + ");";
  }

  public final boolean equals(Object paramObject)
  {
    if (this == paramObject);
    b localb;
    do
    {
      do
      {
        return true;
        if (paramObject == null)
          return false;
        if (getClass() != paramObject.getClass())
          return false;
        localb = (b)paramObject;
        if (this.a == null)
        {
          if (localb.a != null)
            return false;
        }
        else if (!this.a.equals(localb.a))
          return false;
        if (this.b != null)
          break;
      }
      while (localb.b == null);
      return false;
    }
    while (this.b.equals(localb.b));
    return false;
  }

  public final int hashCode()
  {
    int i;
    int j;
    int k;
    if (this.a == null)
    {
      i = 0;
      j = 31 * (i + 31);
      Map localMap = this.b;
      k = 0;
      if (localMap != null)
        break label45;
    }
    while (true)
    {
      return j + k;
      i = this.a.hashCode();
      break;
      label45: k = this.b.hashCode();
    }
  }

  public final String toString()
  {
    return "Gmsg [command=" + this.a + ", params=" + this.b + "]";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.a.b
 * JD-Core Version:    0.6.2
 */