package com.google.ads.interactivemedia.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.google.ads.interactivemedia.a.a;
import com.google.ads.interactivemedia.a.a.a;
import java.net.URL;
import java.net.URLConnection;

public final class c extends ImageView
  implements View.OnClickListener
{
  final a a;
  final String b;
  final a.a c;

  public c(Context paramContext, a parama, String paramString, a.a parama1)
  {
    super(paramContext);
    this.a = parama;
    this.b = paramString;
    this.c = parama1;
    setOnClickListener(this);
    new AsyncTask()
    {
      Exception a = null;

      private Bitmap a()
      {
        try
        {
          Bitmap localBitmap = BitmapFactory.decodeStream(new URL(c.this.c.b()).openConnection().getInputStream());
          return localBitmap;
        }
        catch (Exception localException)
        {
          this.a = localException;
        }
        return null;
      }
    }
    .execute(new Void[0]);
  }

  public final void onClick(View paramView)
  {
    this.a.b(this.c.c());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.a.c
 * JD-Core Version:    0.6.2
 */