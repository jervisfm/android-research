package com.google.ads.interactivemedia.a.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public final class d
{
  private a a;
  private WebView b;

  public d(Context paramContext)
  {
    this(new WebView(paramContext));
  }

  private d(WebView paramWebView)
  {
    this.b = paramWebView;
    paramWebView.getSettings().setJavaScriptEnabled(true);
    paramWebView.getSettings().setCacheMode(2);
    paramWebView.setWebViewClient(new WebViewClient()
    {
      public final void onPageFinished(WebView paramAnonymousWebView, String paramAnonymousString)
      {
        new StringBuilder("Finished ").append(paramAnonymousString).toString();
      }

      public final void onPageStarted(WebView paramAnonymousWebView, String paramAnonymousString, Bitmap paramAnonymousBitmap)
      {
        new StringBuilder("Started ").append(paramAnonymousString).toString();
      }

      public final void onReceivedError(WebView paramAnonymousWebView, int paramAnonymousInt, String paramAnonymousString1, String paramAnonymousString2)
      {
        new StringBuilder("Error: ").append(paramAnonymousInt).append(" ").append(paramAnonymousString1).append(" ").append(paramAnonymousString2).toString();
      }

      public final boolean shouldOverrideUrlLoading(WebView paramAnonymousWebView, String paramAnonymousString)
      {
        if (!paramAnonymousString.startsWith("gmsg://"))
          return false;
        d.this.b(paramAnonymousString);
        return true;
      }
    });
  }

  public final void a(b paramb)
  {
    String str = paramb.c();
    new StringBuilder("Sending gmsg: ").append(paramb).append(" as URL ").append(str).toString();
    this.b.loadUrl(str);
  }

  public final void a(a parama)
  {
    this.a = parama;
  }

  public final void a(String paramString)
  {
    this.b.loadUrl(paramString);
  }

  protected final void b(String paramString)
  {
    b localb = b.a(paramString);
    new StringBuilder("Received gmsg: ").append(localb).append(" from URL ").append(paramString).toString();
    this.a.a(localb);
  }

  public static abstract interface a
  {
    public abstract void a(b paramb);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.a.d
 * JD-Core Version:    0.6.2
 */