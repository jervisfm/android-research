package com.google.ads.interactivemedia.a;

import com.google.ads.interactivemedia.api.AdError;
import com.google.ads.interactivemedia.api.AdError.AdErrorCode;
import com.google.ads.interactivemedia.api.AdError.AdErrorType;
import com.google.ads.interactivemedia.api.AdErrorEvent;
import com.google.ads.interactivemedia.api.AdErrorListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class b
{
  private final List<AdErrorListener> a = new ArrayList(1);

  public final void a()
  {
    this.a.clear();
  }

  public final void a(AdErrorListener paramAdErrorListener)
  {
    this.a.add(paramAdErrorListener);
  }

  public final void a(Object paramObject, AdError.AdErrorType paramAdErrorType, AdError.AdErrorCode paramAdErrorCode, String paramString)
  {
    AdErrorEvent localAdErrorEvent = new AdErrorEvent(paramObject, new AdError(paramAdErrorType, paramAdErrorCode, paramString));
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      ((AdErrorListener)localIterator.next()).onAdError(localAdErrorEvent);
  }

  public final void b(AdErrorListener paramAdErrorListener)
  {
    this.a.remove(paramAdErrorListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.b
 * JD-Core Version:    0.6.2
 */