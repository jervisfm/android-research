package com.google.ads.interactivemedia.a;

import com.google.ads.interactivemedia.api.AdsManager.AdEvent;
import com.google.ads.interactivemedia.api.AdsManager.AdEventListener;
import com.google.ads.interactivemedia.api.AdsManager.AdEventType;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer.VideoAdPlayerCallback;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public final class c
  implements VideoAdPlayer.VideoAdPlayerCallback
{
  private static final Map<a, AdsManager.AdEventType> a;
  private final a b;
  private final String c;
  private final String d;
  private final EnumSet<a> e = EnumSet.noneOf(a.class);
  private AdsManager.AdEventListener f;
  private boolean g = false;
  private long h;

  static
  {
    HashMap localHashMap = new HashMap();
    a = localHashMap;
    localHashMap.put(a.q, AdsManager.AdEventType.CLICK);
    a.put(a.f, AdsManager.AdEventType.COMPLETE);
    a.put(a.d, AdsManager.AdEventType.FIRST_QUARTILE);
    a.put(a.c, AdsManager.AdEventType.MIDPOINT);
    a.put(a.i, AdsManager.AdEventType.PAUSED);
    a.put(a.b, AdsManager.AdEventType.STARTED);
    a.put(a.e, AdsManager.AdEventType.THIRD_QUARTILE);
  }

  public c(a parama, String paramString1, String paramString2)
  {
    this.b = parama;
    this.c = paramString1;
    this.d = paramString2;
  }

  private void a(long paramLong1, long paramLong2, double paramDouble, a parama)
  {
    if ((paramLong2 < paramDouble) && (paramLong1 >= paramDouble))
      a(parama);
  }

  private void a(a parama)
  {
    if ((!this.e.contains(parama)) || (parama.a()))
    {
      this.e.add(parama);
      this.b.a(this.c, parama.b());
    }
    if (a.containsKey(parama))
      this.f.onAdEvent(new AdsManager.AdEvent((AdsManager.AdEventType)a.get(parama)));
  }

  public final void a(AdsManager.AdEventListener paramAdEventListener)
  {
    this.f = paramAdEventListener;
  }

  public final void onClick()
  {
    a(a.q);
    this.b.b(this.d);
  }

  public final void onComplete()
  {
    a(a.f);
  }

  public final void onPause()
  {
    a(a.i);
  }

  public final void onPlay()
  {
    a(a.a);
    a(a.b);
  }

  public final void onProgress(long paramLong1, long paramLong2)
  {
    long l = this.h;
    this.h = paramLong1;
    if (paramLong1 < l)
    {
      a(a.j);
      return;
    }
    double d1 = paramLong2 / 4.0D;
    a(paramLong1, l, d1 * 1.0D, a.d);
    a(paramLong1, l, d1 * 2.0D, a.c);
    a(paramLong1, l, d1 * 3.0D, a.e);
  }

  public final void onResume()
  {
    a(a.k);
  }

  public final void onVolumeChanged(int paramInt)
  {
    if ((paramInt == 0) && (!this.g))
    {
      a(a.g);
      this.g = true;
    }
    if ((paramInt != 0) && (this.g))
    {
      a(a.h);
      this.g = false;
    }
  }

  static enum a
  {
    private boolean s = false;
    private String t;

    static
    {
      a[] arrayOfa = new a[18];
      arrayOfa[0] = a;
      arrayOfa[1] = b;
      arrayOfa[2] = c;
      arrayOfa[3] = d;
      arrayOfa[4] = e;
      arrayOfa[5] = f;
      arrayOfa[6] = g;
      arrayOfa[7] = h;
      arrayOfa[8] = i;
      arrayOfa[9] = j;
      arrayOfa[10] = k;
      arrayOfa[11] = l;
      arrayOfa[12] = m;
      arrayOfa[13] = n;
      arrayOfa[14] = o;
      arrayOfa[15] = p;
      arrayOfa[16] = q;
      arrayOfa[17] = r;
    }

    private a(String paramString)
    {
      this.t = paramString;
    }

    private a(String paramString)
    {
      this(str1);
      this.s = true;
    }

    public final boolean a()
    {
      return this.s;
    }

    public final String b()
    {
      return this.t;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.a.c
 * JD-Core Version:    0.6.2
 */