package com.google.ads.interactivemedia.api;

public class AdError extends Exception
{
  private final AdErrorCode a;
  private final AdErrorType b;

  public AdError(AdErrorType paramAdErrorType, AdErrorCode paramAdErrorCode, String paramString)
  {
    super(paramString);
    this.b = paramAdErrorType;
    this.a = paramAdErrorCode;
  }

  public AdErrorCode getErrorCode()
  {
    return this.a;
  }

  public AdErrorType getErrorType()
  {
    return this.b;
  }

  public String getMessage()
  {
    return super.getMessage();
  }

  public static enum AdErrorCode
  {
    private final int a;

    static
    {
      FAILED_TO_REQUEST_ADS = new AdErrorCode("FAILED_TO_REQUEST_ADS", 2, 1004);
      VAST_LOAD_TIMEOUT = new AdErrorCode("VAST_LOAD_TIMEOUT", 3, 3001);
      VAST_INVALID_URL = new AdErrorCode("VAST_INVALID_URL", 4, 3002);
      VAST_MALFORMED_RESPONSE = new AdErrorCode("VAST_MALFORMED_RESPONSE", 5, 3003);
      VAST_MEDIA_ERROR = new AdErrorCode("VAST_MEDIA_ERROR", 6, 3004);
      VAST_TOO_MANY_REDIRECTS = new AdErrorCode("VAST_TOO_MANY_REDIRECTS", 7, 3005);
      VAST_ASSET_MISMATCH = new AdErrorCode("VAST_ASSET_MISMATCH", 8, 3006);
      VAST_ASSET_NOT_FOUND = new AdErrorCode("VAST_ASSET_NOT_FOUND", 9, 3007);
      INVALID_ARGUMENTS = new AdErrorCode("INVALID_ARGUMENTS", 10, 3101);
      COMPANION_AD_LOADING_FAILED = new AdErrorCode("COMPANION_AD_LOADING_FAILED", 11, 3102);
      UNKNOWN_AD_RESPONSE = new AdErrorCode("UNKNOWN_AD_RESPONSE", 12, 3103);
      UNKNOWN_ERROR = new AdErrorCode("UNKNOWN_ERROR", 13, 3104);
      OVERLAY_AD_LOADING_FAILED = new AdErrorCode("OVERLAY_AD_LOADING_FAILED", 14, 3105);
      OVERLAY_AD_PLAYING_FAILED = new AdErrorCode("OVERLAY_AD_PLAYING_FAILED", 15, 3106);
      ADSLOT_NOT_VISIBLE = new AdErrorCode("ADSLOT_NOT_VISIBLE", 16, 3107);
      AdErrorCode[] arrayOfAdErrorCode = new AdErrorCode[17];
      arrayOfAdErrorCode[0] = INTERNAL_ERROR;
      arrayOfAdErrorCode[1] = VIDEO_PLAY_ERROR;
      arrayOfAdErrorCode[2] = FAILED_TO_REQUEST_ADS;
      arrayOfAdErrorCode[3] = VAST_LOAD_TIMEOUT;
      arrayOfAdErrorCode[4] = VAST_INVALID_URL;
      arrayOfAdErrorCode[5] = VAST_MALFORMED_RESPONSE;
      arrayOfAdErrorCode[6] = VAST_MEDIA_ERROR;
      arrayOfAdErrorCode[7] = VAST_TOO_MANY_REDIRECTS;
      arrayOfAdErrorCode[8] = VAST_ASSET_MISMATCH;
      arrayOfAdErrorCode[9] = VAST_ASSET_NOT_FOUND;
      arrayOfAdErrorCode[10] = INVALID_ARGUMENTS;
      arrayOfAdErrorCode[11] = COMPANION_AD_LOADING_FAILED;
      arrayOfAdErrorCode[12] = UNKNOWN_AD_RESPONSE;
      arrayOfAdErrorCode[13] = UNKNOWN_ERROR;
      arrayOfAdErrorCode[14] = OVERLAY_AD_LOADING_FAILED;
      arrayOfAdErrorCode[15] = OVERLAY_AD_PLAYING_FAILED;
      arrayOfAdErrorCode[16] = ADSLOT_NOT_VISIBLE;
    }

    private AdErrorCode(int paramInt)
    {
      this.a = paramInt;
    }

    static AdErrorCode a(int paramInt)
    {
      for (AdErrorCode localAdErrorCode : values())
        if (localAdErrorCode.a == paramInt)
          return localAdErrorCode;
      return UNKNOWN_ERROR;
    }
  }

  public static enum AdErrorType
  {
    static
    {
      AdErrorType[] arrayOfAdErrorType = new AdErrorType[2];
      arrayOfAdErrorType[0] = LOAD;
      arrayOfAdErrorType[1] = PLAY;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdError
 * JD-Core Version:    0.6.2
 */