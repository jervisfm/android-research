package com.google.ads.interactivemedia.api;

public class AdErrorEvent
{
  private final AdError a;
  private final Object b;

  public AdErrorEvent(AdError paramAdError)
  {
    this.a = paramAdError;
    this.b = null;
  }

  public AdErrorEvent(Object paramObject, AdError paramAdError)
  {
    this.a = paramAdError;
    this.b = paramObject;
  }

  public AdError getError()
  {
    return this.a;
  }

  public Object getUserRequestContext()
  {
    return this.b;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdErrorEvent
 * JD-Core Version:    0.6.2
 */