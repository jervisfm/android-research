package com.google.ads.interactivemedia.api;

public abstract interface AdErrorListener
{
  public abstract void onAdError(AdErrorEvent paramAdErrorEvent);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdErrorListener
 * JD-Core Version:    0.6.2
 */