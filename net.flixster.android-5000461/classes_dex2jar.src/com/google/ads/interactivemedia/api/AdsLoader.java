package com.google.ads.interactivemedia.api;

import android.content.Context;
import com.google.ads.interactivemedia.a.a.a;
import com.google.ads.interactivemedia.a.a.c;
import com.google.ads.interactivemedia.a.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AdsLoader
{
  final b a = new b();
  final List<AdsLoadedListener> b = new ArrayList(1);
  final Map<String, AdsRequest<?>> c = new HashMap();
  private final com.google.ads.interactivemedia.a.a d;
  private long e;

  public AdsLoader(Context paramContext)
  {
    this(new com.google.ads.interactivemedia.a.a.a(paramContext), new Random().nextLong());
  }

  private AdsLoader(final com.google.ads.interactivemedia.a.a parama, long paramLong)
  {
    this.d = parama;
    this.e = paramLong;
    parama.a(new a.c()
    {
      public final void a(String paramAnonymousString1, String paramAnonymousString2, a.a paramAnonymousa, Map<String, List<a.a>> paramAnonymousMap)
      {
        AdsRequest localAdsRequest = (AdsRequest)AdsLoader.this.c.remove(paramAnonymousString1);
        VideoAdsManager localVideoAdsManager = new VideoAdsManager(parama, paramAnonymousString2, localAdsRequest, paramAnonymousa, paramAnonymousMap);
        Iterator localIterator = AdsLoader.this.b.iterator();
        while (localIterator.hasNext())
          ((AdsLoader.AdsLoadedListener)localIterator.next()).onAdsLoaded(new AdsLoader.AdsLoadedEvent(localAdsRequest.getUserRequestContext(), localVideoAdsManager));
      }

      public final void a(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3)
      {
        AdsRequest localAdsRequest = (AdsRequest)AdsLoader.this.c.remove(paramAnonymousString1);
        AdError.AdErrorCode localAdErrorCode = AdError.AdErrorCode.a(Integer.parseInt(paramAnonymousString2));
        AdsLoader.this.a.a(localAdsRequest.getUserRequestContext(), AdError.AdErrorType.LOAD, localAdErrorCode, paramAnonymousString3);
      }
    });
  }

  public void addAdErrorListener(AdErrorListener paramAdErrorListener)
  {
    this.a.a(paramAdErrorListener);
  }

  public void addAdsLoadedListener(AdsLoadedListener paramAdsLoadedListener)
  {
    this.b.add(paramAdsLoadedListener);
  }

  public void removeAdErrorListener(AdErrorListener paramAdErrorListener)
  {
    this.a.b(paramAdErrorListener);
  }

  public void removeAdsLoadedListener(AdsLoadedListener paramAdsLoadedListener)
  {
    this.b.remove(paramAdsLoadedListener);
  }

  public void requestAds(AdsRequest<?> paramAdsRequest)
  {
    StringBuilder localStringBuilder = new StringBuilder("and-ctx-");
    long l = this.e;
    this.e = (1L + l);
    String str = l;
    this.c.put(str, paramAdsRequest);
    this.d.a(str, paramAdsRequest.getParameters());
  }

  public void requestAds(AdsRequest<?> paramAdsRequest, Object paramObject)
  {
    paramAdsRequest.setUserRequestContext(paramObject);
    requestAds(paramAdsRequest);
  }

  public static class AdsLoadedEvent
  {
    private final AdsManager a;
    private final Object b;

    public AdsLoadedEvent(Object paramObject, AdsManager paramAdsManager)
    {
      this.a = paramAdsManager;
      this.b = paramObject;
    }

    public AdsManager getManager()
    {
      return this.a;
    }

    public Object getUserRequestContext()
    {
      return this.b;
    }
  }

  public static abstract interface AdsLoadedListener
  {
    public abstract void onAdsLoaded(AdsLoader.AdsLoadedEvent paramAdsLoadedEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdsLoader
 * JD-Core Version:    0.6.2
 */