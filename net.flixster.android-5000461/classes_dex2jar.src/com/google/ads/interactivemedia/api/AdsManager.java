package com.google.ads.interactivemedia.api;

import android.view.ViewGroup;
import com.google.ads.interactivemedia.a.a;
import com.google.ads.interactivemedia.a.a.a;
import com.google.ads.interactivemedia.a.a.b;
import com.google.ads.interactivemedia.a.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AdsManager
{
  final b a = new b();
  private final List<AdEventListener> b = new ArrayList(1);
  private final AdsRequest<?> c;
  private final Map<String, List<a.a>> d;
  private final String e;
  private a f;

  protected AdsManager(a parama, String paramString, AdsRequest<?> paramAdsRequest, Map<String, List<a.a>> paramMap)
  {
    this.f = parama;
    this.e = paramString;
    this.c = paramAdsRequest;
    this.d = paramMap;
    parama.a(paramString, new a.b()
    {
      public final void a(String paramAnonymousString1, String paramAnonymousString2)
      {
        AdError.AdErrorCode localAdErrorCode = AdError.AdErrorCode.a(Integer.parseInt(paramAnonymousString1));
        AdsManager.this.a.a(null, AdError.AdErrorType.PLAY, localAdErrorCode, paramAnonymousString2);
      }
    });
  }

  protected void a()
  {
  }

  protected final void a(AdEvent paramAdEvent)
  {
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
      ((AdEventListener)localIterator.next()).onAdEvent(paramAdEvent);
  }

  protected final void a(AdEventType paramAdEventType)
  {
    a(new AdEvent(paramAdEventType));
  }

  public void addAdErrorListener(AdErrorListener paramAdErrorListener)
  {
    d();
    this.a.a(paramAdErrorListener);
  }

  public void addAdEventListener(AdEventListener paramAdEventListener)
  {
    d();
    this.b.add(paramAdEventListener);
  }

  protected final a b()
  {
    return this.f;
  }

  protected final String c()
  {
    return this.e;
  }

  protected final void d()
  {
    if (this.f == null)
      throw new IllegalStateException("AdsManager is unloaded");
  }

  protected final void e()
  {
    Map localMap = this.c.a();
    Iterator localIterator = localMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((this.d.containsKey(str)) && (!((List)this.d.get(str)).isEmpty()))
        this.f.a(this.e, (a.a)((List)this.d.get(str)).get(0), (ViewGroup)localMap.get(str));
    }
  }

  public void removeAdErrorListener(AdErrorListener paramAdErrorListener)
  {
    d();
    this.a.b(paramAdErrorListener);
  }

  public void removeAdEventListener(AdEventListener paramAdEventListener)
  {
    d();
    this.b.remove(paramAdEventListener);
  }

  public void unload()
  {
    if (this.f == null)
      return;
    a();
    this.f.a(this.e);
    this.a.a();
    this.b.clear();
    this.f = null;
  }

  public static class AdEvent
  {
    private final AdsManager.AdEventType a;

    public AdEvent(AdsManager.AdEventType paramAdEventType)
    {
      this.a = paramAdEventType;
    }

    public boolean equals(Object paramObject)
    {
      if (this == paramObject);
      AdEvent localAdEvent;
      do
      {
        return true;
        if (paramObject == null)
          return false;
        if (getClass() != paramObject.getClass())
          return false;
        localAdEvent = (AdEvent)paramObject;
      }
      while (this.a == localAdEvent.a);
      return false;
    }

    public AdsManager.AdEventType getEventType()
    {
      return this.a;
    }

    public String toString()
    {
      return "<AdEvent " + this.a.toString() + ">";
    }
  }

  public static abstract interface AdEventListener
  {
    public abstract void onAdEvent(AdsManager.AdEvent paramAdEvent);
  }

  public static enum AdEventType
  {
    static
    {
      AdEventType[] arrayOfAdEventType = new AdEventType[9];
      arrayOfAdEventType[0] = CLICK;
      arrayOfAdEventType[1] = COMPLETE;
      arrayOfAdEventType[2] = CONTENT_PAUSE_REQUESTED;
      arrayOfAdEventType[3] = CONTENT_RESUME_REQUESTED;
      arrayOfAdEventType[4] = FIRST_QUARTILE;
      arrayOfAdEventType[5] = MIDPOINT;
      arrayOfAdEventType[6] = PAUSED;
      arrayOfAdEventType[7] = STARTED;
      arrayOfAdEventType[8] = THIRD_QUARTILE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdsManager
 * JD-Core Version:    0.6.2
 */