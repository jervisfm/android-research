package com.google.ads.interactivemedia.api;

import android.view.ViewGroup;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AdsRequest<C extends CompanionAdSlot>
{
  private final Map<String, String> a = new HashMap();
  private Object b;
  private final Map<String, ViewGroup> c = new HashMap();

  final Map<String, ViewGroup> a()
  {
    return this.c;
  }

  public Map<String, String> getParameters()
  {
    return this.a;
  }

  public String getRequestParameter(String paramString)
  {
    return (String)this.a.get(paramString);
  }

  public Object getUserRequestContext()
  {
    return this.b;
  }

  public void setCompanions(Collection<C> paramCollection)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      CompanionAdSlot localCompanionAdSlot = (CompanionAdSlot)localIterator.next();
      if (localCompanionAdSlot.getContainer() == null)
        throw new IllegalArgumentException("Slot " + localCompanionAdSlot + " has no container specified");
      String str = localCompanionAdSlot.getWidth() + "x" + localCompanionAdSlot.getHeight();
      this.c.put(str, localCompanionAdSlot.getContainer());
    }
  }

  public void setRequestParameter(String paramString1, String paramString2)
  {
    this.a.put(paramString1, paramString2);
  }

  public void setUserRequestContext(Object paramObject)
  {
    this.b = paramObject;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.AdsRequest
 * JD-Core Version:    0.6.2
 */