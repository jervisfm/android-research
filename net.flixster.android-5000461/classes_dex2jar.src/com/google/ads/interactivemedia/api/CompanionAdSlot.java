package com.google.ads.interactivemedia.api;

import android.view.ViewGroup;

public class CompanionAdSlot
{
  private int a;
  private int b;
  private ViewGroup c;

  public ViewGroup getContainer()
  {
    return this.c;
  }

  public int getHeight()
  {
    return this.b;
  }

  public int getWidth()
  {
    return this.a;
  }

  public void setContainer(ViewGroup paramViewGroup)
  {
    this.c = paramViewGroup;
  }

  public void setHeight(int paramInt)
  {
    this.b = paramInt;
  }

  public void setSize(int paramInt1, int paramInt2)
  {
    this.a = paramInt1;
    this.b = paramInt2;
  }

  public void setWidth(int paramInt)
  {
    this.a = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.CompanionAdSlot
 * JD-Core Version:    0.6.2
 */