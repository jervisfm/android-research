package com.google.ads.interactivemedia.api;

import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class SimpleAdsRequest extends AdsRequest<CompanionAdSlot>
{
  public SimpleAdsRequest()
  {
    setRequestParameter("companionSizes", "");
  }

  public String getAdTagUrl()
  {
    return getRequestParameter("adTagUrl");
  }

  public AdType getAdType()
  {
    return AdType.valueOf(getRequestParameter("adType").toUpperCase(Locale.ENGLISH));
  }

  public void setAdTagUrl(String paramString)
  {
    setRequestParameter("adTagUrl", paramString);
  }

  public void setAdType(AdType paramAdType)
  {
    setRequestParameter("adType", paramAdType.name().toLowerCase(Locale.ENGLISH));
  }

  public void setCompanions(Collection<CompanionAdSlot> paramCollection)
  {
    super.setCompanions(paramCollection);
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = a().keySet().iterator();
    for (int i = 1; localIterator.hasNext(); i = 0)
    {
      String str = (String)localIterator.next();
      if (i == 0)
        localStringBuilder.append(",");
      localStringBuilder.append(str);
    }
    setRequestParameter("companionSizes", localStringBuilder.toString());
  }

  public static enum AdType
  {
    static
    {
      AdType[] arrayOfAdType = new AdType[1];
      arrayOfAdType[0] = VIDEO;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.SimpleAdsRequest
 * JD-Core Version:    0.6.2
 */