package com.google.ads.interactivemedia.api;

import com.google.ads.interactivemedia.a.a;
import com.google.ads.interactivemedia.a.a.a;
import com.google.ads.interactivemedia.a.c;
import com.google.ads.interactivemedia.api.player.VideoAdPlayer;
import java.util.List;
import java.util.Map;

public class VideoAdsManager extends AdsManager
{
  private final a.a b;
  private VideoAdPlayer c;
  private c d;

  VideoAdsManager(a parama, String paramString, AdsRequest<?> paramAdsRequest, a.a parama1, Map<String, List<a.a>> paramMap)
  {
    super(parama, paramString, paramAdsRequest, paramMap);
    this.b = parama1;
  }

  protected final void a()
  {
    if (this.c == null)
      return;
    this.c.removeCallback(this.d);
    this.c.stopAd();
    a(AdsManager.AdEventType.CONTENT_RESUME_REQUESTED);
  }

  public void play(VideoAdPlayer paramVideoAdPlayer)
  {
    d();
    if (this.c != null)
      throw new IllegalStateException("Ad is already playing on " + this.c);
    this.c = paramVideoAdPlayer;
    this.d = new c(b(), c(), this.b.c());
    this.d.a(new AdsManager.AdEventListener()
    {
      public final void onAdEvent(AdsManager.AdEvent paramAnonymousAdEvent)
      {
        VideoAdsManager.this.a(paramAnonymousAdEvent);
        if (paramAnonymousAdEvent.getEventType() == AdsManager.AdEventType.COMPLETE)
          VideoAdsManager.this.unload();
      }
    });
    a(AdsManager.AdEventType.CONTENT_PAUSE_REQUESTED);
    this.c.addCallback(this.d);
    this.c.playAd(this.b.b());
    e();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.VideoAdsManager
 * JD-Core Version:    0.6.2
 */