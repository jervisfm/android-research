package com.google.ads.interactivemedia.api.player;

public abstract interface VideoAdPlayer
{
  public abstract void addCallback(VideoAdPlayerCallback paramVideoAdPlayerCallback);

  public abstract void playAd(String paramString);

  public abstract void removeCallback(VideoAdPlayerCallback paramVideoAdPlayerCallback);

  public abstract void stopAd();

  public static abstract interface VideoAdPlayerCallback
  {
    public abstract void onClick();

    public abstract void onComplete();

    public abstract void onPause();

    public abstract void onPlay();

    public abstract void onProgress(long paramLong1, long paramLong2);

    public abstract void onResume();

    public abstract void onVolumeChanged(int paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\net.flixster.android-5000461\classes_dex2jar.jar
 * Qualified Name:     com.google.ads.interactivemedia.api.player.VideoAdPlayer
 * JD-Core Version:    0.6.2
 */